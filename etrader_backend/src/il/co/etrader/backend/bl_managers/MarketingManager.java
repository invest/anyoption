package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.MarketingCombinationSeris;
import il.co.etrader.backend.dao_managers.MarketingAffilatesDAO;
import il.co.etrader.backend.dao_managers.MarketingCampaignsDAO;
import il.co.etrader.backend.dao_managers.MarketingCombinationDomainsDAO;
import il.co.etrader.backend.dao_managers.MarketingCombinationsDAO;
import il.co.etrader.backend.dao_managers.MarketingContentsDAO;
import il.co.etrader.backend.dao_managers.MarketingLandingPagesDAO;
import il.co.etrader.backend.dao_managers.MarketingLocationsDAO;
import il.co.etrader.backend.dao_managers.MarketingMediumsDAO;
import il.co.etrader.backend.dao_managers.MarketingOperationsDAO;
import il.co.etrader.backend.dao_managers.MarketingPaymentRecipientDAO;
import il.co.etrader.backend.dao_managers.MarketingPixelsDAO;
import il.co.etrader.backend.dao_managers.MarketingSizesDAO;
import il.co.etrader.backend.dao_managers.MarketingSourcesDAO;
import il.co.etrader.backend.dao_managers.MarketingTerritoriesDAO;
import il.co.etrader.backend.dao_managers.MarketingTypesDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.bl_vos.MarketingCampaignManagers;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingContent;
import il.co.etrader.bl_vos.MarketingLandingPage;
import il.co.etrader.bl_vos.MarketingLocation;
import il.co.etrader.bl_vos.MarketingMedium;
import il.co.etrader.bl_vos.MarketingPaymentRecipient;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.bl_vos.MarketingSize;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.bl_vos.MarketingSubAffiliate;
import il.co.etrader.bl_vos.MarketingTerritory;
import il.co.etrader.bl_vos.MarketingType;
import il.co.etrader.dao_managers.MarketingCombinationsDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.MarketingTrackerUtil;
import com.anyoption.ets.beans.MarketingCampCombinationRequestedObj;


public class MarketingManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(MarketingManager.class);

	/**
	 * Get all campaigns, with filters
	 * @param from  start date campaign
	 * @param to  end date campaign
	 * @param paymentType
	 * @param campaignName
	 * @param writerIdForSkin
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCampaign> getMarketingCampaigns(Date from, Date to, long paymentType,
			String campaignName, String campaignId, long paymentRecipient, long writerIdForSkin, long marketingChannelId, long marketingCampaignManager) throws SQLException {

		Connection con = getSecondConnection();
		ArrayList<MarketingCampaign> list = null;
		try {
			list = MarketingCampaignsDAO.getAll(con, from, to, paymentType, campaignName, campaignId, paymentRecipient, writerIdForSkin, marketingChannelId, marketingCampaignManager);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing campaign
	 * @param mc  MarketingCampaigns instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateCampaign(MarketingCampaign mc) throws SQLException {

    	boolean res = false;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingCampaign: " + ls + mc.toString());
  		 }

    	Connection con = getConnection();

    	try {
				res = validateCampaignName(mc);
				if ( res == false ) {
					return false;
				} else {  // not in use

					// set campaign name
					String source = "";
					String paymentType = CommonUtil.getMessage(Utils.getWriter().getPaymentTypeById(mc.getPaymentTypeId()), null, Utils.getWriterLocale());;
					String paymentRecipient = getMarketingPaymentRecipientById(mc.getPaymentRecipientId());
					String territory = getMarketingTerritoryById(mc.getTerritoryId());

					MarketingSource ms = getMarketingSourceById(mc.getSourceId());

					if (null != ms){
						source = ms.getName();
					}


	    			if ( mc.getId() == 0 ) {  // insert
	    				MarketingCampaignsDAO.insert(con, mc);
	    				mc.setName(paymentRecipient + "_" + territory + "_" + source + "_" + paymentType + "_" + mc.getId());	    				
	    				MarketingCampaignsDAO.updateName(con, mc);
	    				MarketingCampCombinationRequestedObj mccObj = new MarketingCampCombinationRequestedObj();
	    				mccObj.setRequestedType(Constants.ETS_REQUEST_TYPE_CAMPAIGN);
	    				mccObj.setActionType(Constants.ETS_ACTION_TYPE_INSERT);
	    				mccObj.setPrimaryId(mc.getId());
	    				mccObj.setForeignId(mc.getMarketingChannelId());
	    				try {
	    					MarketingTrackerUtil.marketingTrackingEtsCampaignCombination(mccObj);
	    				} catch (Exception e) {
	    					logger.log(Level.DEBUG, "Failed calling ETS servlet for campaign insert!");
	    				}
	    				
	    			} else { // update
	    				if (MarketingCampaignsDAO.isUpdatePriorityId(con, mc.getId(), mc.getMarketingChannelId())) {
		    				MarketingCampCombinationRequestedObj mccObj = new MarketingCampCombinationRequestedObj();
		    				mccObj.setRequestedType(Constants.ETS_REQUEST_TYPE_CAMPAIGN);
		    				mccObj.setActionType(Constants.ETS_ACTION_TYPE_UPDATE);
		    				mccObj.setPrimaryId(mc.getId());
		    				mccObj.setForeignId(mc.getMarketingChannelId());
		    				try {
		    					MarketingTrackerUtil.marketingTrackingEtsCampaignCombination(mccObj);
		    				} catch (Exception e) {
		    					logger.log(Level.DEBUG, "Failed calling ETS servlet for campaign update!");
		    				}
	    				}
	    				
	    				mc.setName(paymentRecipient + "_" + territory + "_" + source + "_" + paymentType + "_" + mc.getId());
	    				MarketingCampaignsDAO.update(con, mc);
	    			}

				}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate campaign name
     * @param name territory
     * @return
     * @throws SQLException
     */
    private static boolean validateCampaignName(MarketingCampaign mc) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingCampaignsDAO.isNameInUse(con, mc);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.campaign.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage(null, fm);

    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all sources, with filters
	 * @param sourceName source name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingSource> getMarketingSources(String sourceName, boolean orderById, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingSource> list = null;
		try {
			list = MarketingSourcesDAO.getAll(con, sourceName, orderById, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Get all sources for this writer
	 * @return source that htis writer have permmison to see
	 * @throws SQLException
	 */
	public static ArrayList<MarketingSource> getMarketingSourcesByWriterSkinId() throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingSource> list = null;
		try {
			list = MarketingSourcesDAO.getMarketingSourcesByWriterSkinId(con);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing source
	 * @param mc  MarketingSources instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateSource(MarketingSource ms) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingSources: " + ls + ms.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateSourceName(ms.getName(), ms.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( ms.getId() == 0 ) {  // insert
	    				MarketingSourcesDAO.insert(con, ms);
	    			} else {
	    				MarketingSourcesDAO.update(con, ms);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate source name
     * @param name source name
     * @return
     * @throws SQLException
     */
    private static boolean validateSourceName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingSourcesDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.source.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("sourcesForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Get payment recipients SI list
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getMarketingPaymentRecipients() throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingPaymentRecipientDAO.getList(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<SelectItem> getMarketingChannels() throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCampaignsDAO.getMarketingChannels(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<SelectItem> getMarketingPaymentRecipientsWriterSkinId(long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingPaymentRecipientDAO.getListWriterSkinId(con, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Get payment recipient name by id
	 * @param id payment recipient lable
	 * @return
	 * @throws SQLException
	 */
	public static String getMarketingPaymentRecipientById(long id) throws SQLException {

		Connection con = getConnection();
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		String lable = "";

		try {
			list = MarketingPaymentRecipientDAO.getList(con);
			for ( SelectItem s : list ) {
				if ( s.getValue().equals(id) ) {
					lable = s.getLabel();
				}
			}
		} finally {
			closeConnection(con);
		}

		return lable;
	}


	/**
	 * Get all mediums, with filters
	 * @param mediumName medium name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingMedium> getMarketingMediums(String mediumName, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingMedium> list = null;
		try {
			list = MarketingMediumsDAO.getAll(con, mediumName, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing medium
	 * @param mc  MarketingMediums instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateMedium(MarketingMedium mm) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingMedium: " + ls + mm.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateMediumName(mm.getName(), mm.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mm.getId() == 0 ) {  // insert
	    				MarketingMediumsDAO.insert(con, mm);
	    			} else {
	    				MarketingMediumsDAO.update(con, mm);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate medium name
     * @param name medium name
     * @return
     * @throws SQLException
     */
    private static boolean validateMediumName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingMediumsDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.medium.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("mediumsForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all contents, with filters
	 * @param contentName content name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingContent> getMarketingContents(String contentName, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingContent> list = null;
		try {
			list = MarketingContentsDAO.getAll(con, contentName, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}


	/**
	 * Insert / update marketing content
	 * @param mc  MarketingContent instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateContent(MarketingContent mc) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingContent: " + ls + mc.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateContentName(mc.getName(), mc.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mc.getId() == 0 ) {  // insert
	    				MarketingContentsDAO.insert(con, mc);
	    			} else {
	    				MarketingContentsDAO.update(con, mc);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }


//    public static boolean uploadImage(MarketingContent mc) {
//
//		if (f.getFile() != null) {
//			String name = f.getFile().getName();
//			while(name.indexOf(".") != -1)
//				name = name.substring(name.indexOf(".") + 1);
//
//			SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
//			name = "user_" + user.getId() + "_doc_" + f.getFileTypeId() + "_" + sd.format(new Date()) + "." + name;
//
//			f.setFileName(name);
//
//			BufferedInputStream bis = new BufferedInputStream(f.getFile().getInputStream(),4096);
//			java.io.File targetFile = new java.io.File(CommonUtil.getProperty(Constants.FILES_PATH) + name); // destination
//			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile),4096);
//			int theChar;
//			while((theChar = bis.read()) != -1) {
//				bos.write(theChar);
//			}
//			bos.close();
//			bis.close();
//		}
//    }


    /**
     * Validate content name
     * @param name content name
     * @return
     * @throws SQLException
     */
    private static boolean validateContentName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingContentsDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.content.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("contentsForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }


    /**
     * Get all sizes, with filters
     * @param verticalSize  vertical size filter
     * @param horizontalSize hotizontal size filter
     * @return
     * @throws SQLException
     */
	public static ArrayList<MarketingSize> getMarketingSizes(String verticalSize, String horizontalSize, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingSize> list = null;
		try {
			list = MarketingSizesDAO.getAll(con, verticalSize, horizontalSize, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing size
	 * @param ms  MarketingSize instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateSize(MarketingSize ms) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingsize: " + ls + ms.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			if ( ms.getId() == 0 ) {  // insert
    				MarketingSizesDAO.insert(con, ms);
    			} else {
    				MarketingSizesDAO.update(con, ms);   // update
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all types, with filters
	 * @param typeName type name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingType> getMarketingTypes(String typeName, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingType> list = null;
		try {
			list = MarketingTypesDAO.getAll(con, typeName, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing type
	 * @param mt  MarketingType instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateType(MarketingType mt) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingType: " + ls + mt.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateTypeName(mt.getName(), mt.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mt.getId() == 0 ) {  // insert
	    				MarketingTypesDAO.insert(con, mt);
	    			} else {
	    				MarketingTypesDAO.update(con, mt);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate type name
     * @param name type name
     * @return
     * @throws SQLException
     */
    private static boolean validateTypeName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingTypesDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.type.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("typesForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all locations, with filters
	 * @param location filter
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingLocation> getMarketingLocations(String location, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingLocation> list = null;
		try {
			list = MarketingLocationsDAO.getAll(con, location, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing location
	 * @param ml  MarketingLocation instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateLocation(MarketingLocation ml) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingLocation: " + ls + ml.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateLocation(ml.getLocation(), ml.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( ml.getId() == 0 ) {  // insert
	    				MarketingLocationsDAO.insert(con, ml);
	    			} else {
	    				MarketingLocationsDAO.update(con, ml);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate location
     * @param location
     * @return
     * @throws SQLException
     */
    private static boolean validateLocation(String location, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingLocationsDAO.isNameInUse(con, location, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.location.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("locationsForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all landing pages, with filters
	 * @param landingName landing page name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingLandingPage> getMarketingLandingPages(String landingName, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingLandingPage> list = null;
		try {
			list = MarketingLandingPagesDAO.getAll(con, landingName, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing landing page
	 * @param ml  MarketingLandingPage instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateLandingPage(MarketingLandingPage ml) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingLanding Page: " + ls + ml.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateLandingPageName(ml.getName(), ml.getId(), ml.getUrl());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( ml.getId() == 0 ) {  // insert
	    				MarketingLandingPagesDAO.insert(con, ml);
	    			} else {
	    				MarketingLandingPagesDAO.update(con, ml);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}
    	
    	return true;
    }

    /**
     * Validate landing page name
     * @param name landing page name
     * @return
     * @throws SQLException
     */
    private static boolean validateLandingPageName(String name, long id, String url) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingLandingPagesDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.landingPage.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("landingPagesForm:name", fm);
    				return false;
    			}
    			boolean urlInUse = MarketingLandingPagesDAO.isUrlInUse(con, url, id);
    			if (urlInUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.landingPage.url.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("landingPagesForm:url", fm);
    				return false;
    			}


    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all combinations, with filters
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCombination> getMarketingCombinations(long skinId, String campaign, String source,
  				long medium, long content, String verticalSize, String horizontalSize,
  						String type, String location, long landingPage, long combId, long writerIdForSkin, long fetchCount) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingCombination> list = null;
		try {
			list = MarketingCombinationsDAO.getAll(con, skinId, campaign, source, medium, content,
					verticalSize, horizontalSize, type, location, landingPage, combId, writerIdForSkin, fetchCount);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	public static ArrayList<MarketingCombinationSeris> getAllSerries(List<Long> mcId) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingCombinationSeris> list = null;
		try {
			list = MarketingCombinationsDAO.getAllSerries(con, mcId);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	/**
	 * Get marketing combination (for reg leads screen for now)
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getCombinationIdWithCampaigns() throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAO.getCombinationIdWithCampaigns(con);
		} finally {
			closeConnection(con);
		}			
	}
	
	
	public static int deletePixelByCampaignIDorSourceID(Long pixelID, Long campaignID, Long sourceID) throws SQLException {
		Connection con = getConnection();
		try {
			int k = MarketingPixelsDAO.deletePixelByCampaignIDorSourceID(con, pixelID, campaignID, sourceID);
			return k;
		} finally {
			closeConnection(con);
		}			
		
	}
	
	
	/**
	 * Get marketing combination(s) for given campaign
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCombination> getCombinationsWithCampaignIDSourceID(Long campaignID, Long sourceID, long writerIdForSkin) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingCombinationsDAO.getCombinationsWithCampaignIDSourceID(con, campaignID, sourceID, writerIdForSkin);
		} finally {
			closeConnection(con);
		}			
	}
	

	/**
	 * Insert / update marketing combination
	 * @param mc  MarketingCombination instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateCombination(MarketingCombination mc, ArrayList<Long> pixelsList) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingCombination: " + ls + mc.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateCombination(mc);
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
    				long mcId = mc.getId();
	    			if ( mc.getId() == 0 ) {  // insert
	    				mcId = insertMarketingCombination(mc, con);
	    			} else {
	    				if (MarketingCombinationsDAO.isUpdateCampaign(con, mc.getId(), mc.getCampaignId())) {
	    					MarketingCampCombinationRequestedObj mccObj = new MarketingCampCombinationRequestedObj();
	    					mccObj.setRequestedType(Constants.ETS_REQUEST_TYPE_COMBINATION);
	    					mccObj.setActionType(Constants.ETS_ACTION_TYPE_UPDATE);
	    					mccObj.setPrimaryId(mc.getId());
	    					mccObj.setForeignId(mc.getCampaignId());
	    					try {
	    						MarketingTrackerUtil.marketingTrackingEtsCampaignCombination(mccObj);
	    					} catch (Exception e) {
	    						logger.log(Level.DEBUG, "Failed calling ETS servlet for combination update!");
	    					}
	    				}
	    				MarketingCombinationsDAO.update(con, mc);   // update
	    			}
	    			// insert new pixel
	    			if (pixelsList.size() > 0) {
	    				MarketingPixelsDAO.insertIntoCombinationPixels(con, mc.getId(), pixelsList);
	    			}
	    			if (mc.getMarketingDomain().getId() > 0) {
	    				logger.debug("About to insert into MarketingCombinationDomains, combinationId:" + mcId + " domainId:" + mc.getMarketingDomain().getId());
	    				MarketingCombinationDomainsDAO.insert(con, mcId, mc.getMarketingDomain().getId());
	    			}
    			}
    	} finally {
    		closeConnection(con);
    	}
    	return true;
    }
    
    public static List<Long> insertMarketingCombination(ArrayList<MarketingCombinationSeris> mcSeries) throws SQLException{
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert  marketingCombinationSeries: " + ls + mcSeries.toString());
  		 }
		List<Long> list = new ArrayList<Long>();
		long mcId = 0;
    	Connection con = getConnection();
    	
    	try {
    		for (MarketingCombinationSeris mc : mcSeries){
    			mcId = MarketingCombinationsDAO.insert(con, mc);
    			logger.debug("About to insert into MarketingCombinationDomains, combinationId:" + mcId + " domainId:" + mc.getMarketingDomain().getId());
    			MarketingCombinationDomainsDAO.insert(con, mcId, mc.getMarketingDomain().getId());
    			list.add(mcId);
    			if (mc.getSeriesPx().size() > 0){
    				MarketingPixelsDAO.insertIntoCombinationPixels(con, mc.getId(), mc.getSeriesPx());
    			}
    			mc.setId(mcId);   
    			if (logger.isEnabledFor(Level.DEBUG)) {
    	  			logger.log(Level.DEBUG,"Inserted  marketingCombinationSeries: " + mcId);
    	  		 }
    		}    		    		
    	} finally {
    		closeConnection(con);
    	}
    	
    	return list;
    }

	private static long insertMarketingCombination(MarketingCombination mc, Connection con) throws SQLException {
		long mcId = MarketingCombinationsDAO.insert(con, mc);
		MarketingCampCombinationRequestedObj mccObj = new MarketingCampCombinationRequestedObj();
		mccObj.setRequestedType(Constants.ETS_REQUEST_TYPE_COMBINATION);
		mccObj.setActionType(Constants.ETS_ACTION_TYPE_INSERT);
		mccObj.setPrimaryId(mc.getId());
		mccObj.setForeignId(mc.getCampaignId());
		try {
			MarketingTrackerUtil.marketingTrackingEtsCampaignCombination(mccObj);
		} catch (Exception e) {
			logger.log(Level.DEBUG, "Failed calling ETS servlet for combination insert!");
		}
		
		return mcId;
	}

    /**
     * Validate combination
     * @param mc MarketingCombination instance
     * @return
     * @throws SQLException
     */
    private static boolean validateCombination(MarketingCombination mc) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			long oldCombination = MarketingCombinationsDAO.isInUse(con, mc);
    			if (oldCombination > 0) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(
    						"error.combination.inuse",
    							null,Utils.getWriterLocale()) + ' ' + oldCombination + '.', null);
    				context.addMessage(null, fm);

    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all payment recipient, with filters
	 * @param name agent name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingPaymentRecipient> getMarketingPaymentRecipients(String name, long writerIdForSkin) throws SQLException {

		Connection con = getSecondConnection();
		ArrayList<MarketingPaymentRecipient> list = null;
		try {
			list = MarketingPaymentRecipientDAO.getAll(con, name, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing payment recipients
	 * @param mp  MarketingPaymentRecipient instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdatePaymentRecipient(MarketingPaymentRecipient mp) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingPaymentRecipients: " + ls + mp.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateAgentName(mp.getAgentName(), mp.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mp.getId() == 0 ) {  // insert
	    				MarketingPaymentRecipientDAO.insert(con, mp);
	    			} else {
	    				MarketingPaymentRecipientDAO.update(con, mp);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate agent name
     * @param name agent name
     * @return
     * @throws SQLException
     */
    private static boolean validateAgentName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingPaymentRecipientDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.payment.recipient.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("paymentRecipientsForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get all terriroties, with filters
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingTerritory> getMarketingTerritories(String name, long writerIdForSkin) throws SQLException {

		Connection con = getSecondConnection();
		ArrayList<MarketingTerritory> list = null;
		try {
			list = MarketingTerritoriesDAO.getAll(con, name, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing territory
	 * @param mp  MarketingTerritory instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdateTerriroty(MarketingTerritory mp) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingTerritory: " + ls + mp.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validateTerritory(mp.getTerritory(), mp.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mp.getId() == 0 ) {  // insert
	    				MarketingTerritoriesDAO.insert(con, mp);
	    			} else {
	    				MarketingTerritoriesDAO.update(con, mp);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate territory
     * @param name territory
     * @return
     * @throws SQLException
     */
    private static boolean validateTerritory(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingTerritoriesDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.territory.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("territoriesForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Get territories SI list
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getMarketingTerritoriesSI() throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingTerritoriesDAO.getList(con);
		} finally {
			closeConnection(con);
		}
	}
	
    /**
     * Get territories SI list by WriterSkinId
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getMarketingTerritoriesSIWriterSkinId(long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingTerritoriesDAO.getListWriterSkinId(con, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Get territory by id
	 * @param id territory id
	 * @return
	 * @throws SQLException
	 */
	public static String getMarketingTerritoryById(long id) throws SQLException {

		Connection con = getConnection();
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		String lable = "";

		try {
			list = MarketingTerritoriesDAO.getList(con);
			for ( SelectItem s : list ) {
				if ( s.getValue().equals(id) ) {
					lable = s.getLabel();
				}
			}
		} finally {
			closeConnection(con);
		}

		return lable;
	}

	/**
	 * Get all pixels, with filters
	 * @param pixelName pixel name
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingPixel> getMarketingPixels(String pixelName, long writerIdForSkin) throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingPixel> list = null;
		try {
			list = MarketingPixelsDAO.getAll(con, pixelName, writerIdForSkin);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Insert / update marketing pixel
	 * @param mc  MarketingSources instance
	 * @return
	 * @throws SQLException
	 */
    public static boolean insertUpdatePixel(MarketingPixel mp) throws SQLException {

    	boolean res = true;

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingPixel: " + ls + mp.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			res = validatePixelName(mp.getName(), mp.getId());
    			if ( res == false ) {
    				return false;
    			} else {  // not in use
	    			if ( mp.getId() == 0 ) {  // insert
	    				MarketingPixelsDAO.insert(con, mp);
	    			} else {
	    				MarketingPixelsDAO.update(con, mp);   // update
	    			}
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Validate pixel name
     * @param name pixel name
     * @return
     * @throws SQLException
     */
    private static boolean validatePixelName(String name, long id) throws SQLException {

    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();

    	try {
    			boolean inUse = MarketingPixelsDAO.isNameInUse(con, name, id);
    			if (inUse) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.pixel.inuse", null,Utils.getWriterLocale()),null);
    				context.addMessage("pixelsForm:name", fm);
    				return false;
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

	/**
	 * Get source by id
	 * @param id source id
	 * @return
	 * @throws SQLException
	 */
	public static MarketingSource getMarketingSourceById(long id) throws SQLException {

		Connection con = getConnection();

		try {
			return(MarketingSourcesDAO.getById(con, id));
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Remove pixel from combination
	 * @param combId
	 * @param pixelId
	 * @throws SQLException
	 */
	public static void deletePixelByCombId(long combId, long pixelId) throws SQLException {
		Connection con = getConnection();
		String ls = System.getProperty("line.separator");
		logger.debug("Going to remove pixel from combination, " + ls +
				"combId: " + combId + ls +
				"pixelId: " + pixelId + ls +
				"writerId: " + Utils.getWriter().getWriter().getId() + ls);
		try {
			MarketingPixelsDAO.deletePixelByCombId(con, combId, pixelId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get all affilates, with filters
	 * @param affName filter
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingAffilate> getMarketingAffilates(String affName, String affKey, long affManagerId) throws SQLException {
		Connection con = getConnection();
		ArrayList<MarketingAffilate> list = null;
		try {
			list = MarketingAffilatesDAO.getAll(con, affName, affKey, affManagerId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Get all sub affilates, with filters
	 * @param subAffName filter
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingSubAffiliate> getMarketingSubAffilates(String subAffName, String affName) throws SQLException {
		Connection con = getConnection();
		ArrayList<MarketingSubAffiliate> list = null;
		try {
			list = MarketingAffilatesDAO.getAllSubAffiliates(con, subAffName, affName);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static boolean insertUpdateAffiliate(MarketingAffilate ma) throws SQLException {

	   	boolean res = true;
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingAffiliate: " + ls + ma.toString());
  		 }

    	Connection con = getConnection();
    	try  {
    			res = validateAffiliate(ma);

    			if (res == false) {
    				return false;
    			} else {  // not in use
	    			if (ma.getId() == 0) {  // insert
	    				MarketingAffilatesDAO.insert(con, ma);
	    				MarketingAffilatesDAO.insertMarketingCode(con, ma);
	    			} else {
		    			MarketingAffilatesDAO.update(con, ma);   // update
			    		MarketingAffilatesDAO.updateMarketingCode(con, ma);	
	    			}
	    			// insert new pixel
	    			if (ma.getPixelId() > 0) {
	    				MarketingPixelsDAO.insertIntoAffiliatePixels(con, ma.getId(), 0, ma.getPixelId(), Utils.getWriter().getWriter().getId());
	    			}
    			}
    	} finally {
    		closeConnection(con);
    	}
    	return true;
    }

	public static boolean insertUpdateSubAffiliate(MarketingSubAffiliate msa) throws SQLException {

	   	boolean res = true;
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update marketingSubAffiliate: " + ls + msa.toString());
  		 }

    	Connection con = getConnection();
    	try  {
    			res = validateSubAffiliate(msa);
    			if (res == false) {
    				return false;
    			} else {  // not in use
	    			if (msa.getId() == 0) {  // insert
	    				MarketingAffilatesDAO.insertSubAffiliate(con, msa);
	    			} else {
	    				MarketingAffilatesDAO.updateMarketingSubAffiliate(con, msa);   // update
	    			}
	    			// insert new pixel
	    			if (msa.getPixelId() > 0) {
	    				MarketingPixelsDAO.insertIntoAffiliatePixels(con, msa.getAffiliateId(), msa.getId(), msa.getPixelId(), Utils.getWriter().getWriter().getId());
	    			}
    			}
    	} finally {
    		closeConnection(con);
    	}
    	return true;
    }

    /**
     * Validate affiliate
     * @param ma MarketingAffilate instance
     * @return
     * @throws SQLException
     */
    private static boolean validateAffiliate(MarketingAffilate ma) throws SQLException {
    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();
    	try {
    			boolean oldAff = MarketingAffilatesDAO.isSpecialCodeInUse(con, ma.getSpecialCode(), ma.getId());
    			if (oldAff) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(
    							"error.affilate.special.code.inuse",
    							null,Utils.getWriterLocale()), null);
    				context.addMessage(null, fm);

    				return false;
    			} else if (MarketingAffilatesDAO.isKeyInUse(con, ma.getKey(), ma.getId())) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(
							"error.affilate.key.inuse",
							null,Utils.getWriterLocale()), null);
    				context.addMessage(null, fm);

    				return false;
    			}
    	} finally {
    		closeConnection(con);
    	}
    	return true;
    }

    public static ArrayList<MarketingLandingPage> getMarketingLandingPagesByWriterSkins() throws SQLException {
		Connection con = getConnection();
		ArrayList<MarketingLandingPage> list = null;
		try {
			list = MarketingLandingPagesDAO.getAllByWriteSkinId(con);
		} finally {
			closeConnection(con);
		}
		return list;
	}


    /**
     * Validate sub affiliate
     * @param msa MarketingSubAffilate instance
     * @return
     * @throws SQLException
     */
    private static boolean validateSubAffiliate(MarketingSubAffiliate msa) throws SQLException {
    	Connection con = getConnection();
    	FacesContext context = FacesContext.getCurrentInstance();
    	try {
    			boolean oldAff = MarketingAffilatesDAO.isSubAffiliateNameInUse(con, msa.getName(), msa.getId());
    			if (oldAff) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(
    							"error.affilate.sub.inuse",
    							null,Utils.getWriterLocale()), null);
    				context.addMessage(null, fm);

    				return false;
    			}
    	} finally {
    		closeConnection(con);
    	}
    	return true;
    }

	/**
	 * Remove pixel from affiliate
	 * @param affiliateId
	 * @param pixelId
	 * @throws SQLException
	 */
	public static void deletePixelByAffiliateId(long affiliateId, long subAffiliateId, long pixelId) throws SQLException {
		Connection con = getConnection();
		String ls = System.getProperty("line.separator");
		logger.debug("Going to remove pixel from combination, " + ls +
				"affiliateId: " + affiliateId + ls +
				"pixelId: " + pixelId + ls +
				"writerId: " + Utils.getWriter().getWriter().getId() + ls);
		try {
			MarketingPixelsDAO.deletePixelByAffiliateId(con, affiliateId, subAffiliateId ,pixelId);
		} finally {
			closeConnection(con);
		}
	}

    /**
     * Get marketing affiliates SI list
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getMarketingAffiliates() throws SQLException {

		Connection con = getConnection();
		try {
			return MarketingAffilatesDAO.getAllMarketingAffiliatesSI(con);
		} finally {
			closeConnection(con);
		}
	}

    public static String updateOrInsertCombintaionPixel(long sourceId, long pixelId, long campaignId, boolean inAdditionToExistingPixel) throws SQLException {
        Connection con = getConnection();
        String msg = "No records was found";
        ArrayList<MarketingCombination> list = new ArrayList<MarketingCombination>();
        logger.debug("going to update Or Insert Combintaion Pixel source id = " + sourceId + " pixelId = " + pixelId + " campaignId = " + campaignId );
        try {
            list = MarketingPixelsDAO.getCombintaionWithoutPixel(con, sourceId, pixelId, campaignId);
            String updateIds = "";
            con.setAutoCommit(false);
            for (MarketingCombination combination : list) {
            	if (inAdditionToExistingPixel){
            		MarketingPixelsDAO.insertIntoCombinationPixels(con, combination.getId(), pixelId);
            		msg = "Inserted successfully Combination Pixel";
            	} else {
	                if (combination.getPixelId() > 0) { //if they got already pixel with the same type need to update
	                    updateIds += combination.getPixelId() + ","; //we store in pixelId the combinationPixelId to update it
	                } else { //insert new Combination Pixel
	                    MarketingPixelsDAO.insertIntoCombinationPixels(con, combination.getId(), pixelId);
	                    msg = "Inserted successfully Combination Pixel";
	                }
            	}
            }
            if (!CommonUtil.isParameterEmptyOrNull(updateIds)) {
                updateIds = updateIds.substring(0, updateIds.length() - 1);
                MarketingPixelsDAO.updateCombintaionPixelByIds(con, pixelId, updateIds);
                msg = "Update successfully Combination Pixel.";
            }
            con.commit();

        } catch (SQLException e) {
            logger.log(Level.ERROR, "updateOrInsertCombintaionPixel:", e);
            try {
                con.rollback();
            } catch (SQLException ie) {
                logger.log(Level.ERROR, "Can't rollback.", ie);
            }
            throw e;
        } finally {
            con.setAutoCommit(true);
            closeConnection(con);
        }
        return msg;
    }

    /**
     * Get marketing combination by id
     * @param combId
     * @return
     * @throws SQLException
     */
    public static MarketingCombination getCombinationId(long combId) throws SQLException {
        Connection con = getConnection();
        try {
            return MarketingCombinationsDAOBase.getById(con, combId);
        } finally {
            closeConnection(con);
        }
    }

    public static ArrayList<SelectItem> getAffiliatesNamesAndKeys()throws SQLException{
		ArrayList<SelectItem> arrayList = null;
		Connection con = null;

		try {
			con = getConnection();
			arrayList = MarketingAffilatesDAO.getAffiliatesNamesAndKeysSI(con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection(con);
		}
		return arrayList;
	}

	public static MarketingCampaign getMarketingCampaignByUserId(long combinationId) throws Exception {
        Connection con = getConnection();
        try {
            return MarketingCampaignsDAO.getMarketingCampaignByUserId(con, combinationId);
        } finally {
            closeConnection(con);
        }
	}

	public static boolean createMarketingOperation(String name, Double duration, Date created) throws Exception{
        Connection con = getConnection();
        try {
            return MarketingOperationsDAO.createMarketingOperations(con, name, duration, created);
        } finally {
            closeConnection(con);
        }
	}
	
//	public static ArrayList getAllZeroOneHunderdSI() throws SQLException{
//		Connection conn = null;
//		try{
//			conn = getConnection();
//			return MarketsDAO.getAllZeroOneHunderdSI(conn);
//		} finally {
//			closeConnection(conn);
//		}
//	}
//	
//	public static ArrayList getZeroOneHunderdById(long id) throws SQLException{
//		Connection conn = null;
//		try{
//			conn = getConnection();
//			return MarketsDAO.getZeroOneHunderdById(conn, id);
//		} finally {
//			closeConnection(conn);
//		}
//	}

	/**
	 * Get all Campaign Managers
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCampaignManagers> getAllMarketingCampaignManagers() throws SQLException {

		Connection con = getConnection();
		ArrayList<MarketingCampaignManagers> list = null;
		try {
			list =MarketingCampaignsDAO.getAllMarketingCampaignManagers(con);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	public static String getRemarketingFlag(long userId) throws SQLException {
        Connection con = getSecondConnection();
        try {
            return MarketingCombinationsDAOBase.getRemarketing(con, userId);
        } finally {
            closeConnection(con);
        }
    }

	/**
	 * Insert Landing Page
	 * @param mlp
	 * @throws SQLException
	 */
	public static void insertLandingPage(MarketingLandingPage mlp) throws SQLException {
		Connection con = getConnection();
		try {
			MarketingLandingPagesDAO.insert(con, mlp);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get marketing landing page names SI
	 * @param landingPageType
	 * @param pathId
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getMarketingLpNamesSI(long landingPageType, long pathId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return MarketingLandingPagesDAO.getMarketingLpNamesSI(con, landingPageType, pathId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get marketing affiliates names and keys
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
    public static ArrayList<SelectItem> getMarketingAffNameKey() throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffilatesDAO.getMarketingAffNameKey(con);
		} finally{
			closeConnection(con);
		}
	}

}

