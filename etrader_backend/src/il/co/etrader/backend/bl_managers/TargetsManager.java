package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.TargetsDAO;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MediaBuyerTarget;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class TargetsManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(UsersManager.class);



    /**
	 * Add/Update targets in the db.
	 */
	public static boolean updateInsertTargets(MediaBuyerTarget mediaBuyer) throws SQLException {

        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.log(Level.DEBUG,"Insert/update target !");
         }

        FacesContext context = FacesContext.getCurrentInstance();
        Connection con = getConnection();
        try {
            if (mediaBuyer.getId() == 0){
                    // insert
                    // setting the month to insert
                    GregorianCalendar gc1 = new GregorianCalendar();
                    Calendar c = Calendar.getInstance();
                    c.setTime(mediaBuyer.getYearTarget());
                    gc1.set(Calendar.YEAR, c.get(Calendar.YEAR));
                    gc1.set(Calendar.MONTH, Integer.valueOf(mediaBuyer.getMonthTarget().getMonth()));
                    gc1.set(Calendar.DAY_OF_MONTH, 1);
                    mediaBuyer.setMonthTarget(gc1.getTime());
                    //check if combination of skin + writer + month exist in MARKETING_MEDIA_BUYER_TARGET table
                    boolean exists = TargetsDAO.validateIsTargetCombinationExist(con, mediaBuyer);
                    if (exists) {
                        //Combination (skin+mediaBuyerId+month) Exists
                        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("targets.target.exist", null, Utils.getWriterLocale(context)),null);
                        context.addMessage(null, fm);
                        return false;
                    }//Combination (skin+mediaBuyerId+month) DOESN'T Exists
                    TargetsDAO.insertTarget(con, mediaBuyer);
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
                    context.addMessage(null, fm);
                    return true;
            } else {
                // update
                boolean isUpdated = TargetsDAO.updateTarget(con, mediaBuyer);
                if (isUpdated) {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
                    context.addMessage(null, fm);
                    return true;
                } else {
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("targets.target.update.failed.date", null, Utils.getWriterLocale(context)),null);
                    context.addMessage(null, fm);
                    return true;
                }
            }
        } finally {
            closeConnection(con);
        }

    }

    public static ArrayList<MediaBuyerTarget> getAllDailyTargets(Date monthDate) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.log(Level.DEBUG,"Getting All Daily Targets...");
         }
        Connection con = getConnection();
        try {
            return TargetsDAO.getAllDailyTargets(con, monthDate);
        } finally {
            closeConnection(con);
        }
    }


    public static ArrayList<MediaBuyerTarget> getAllTargets(long mediaBuyerId, Date monthDate, long skinId, long skinBusinessCaseId) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.log(Level.DEBUG,"Getting All Targets...");
         }
        Connection con = getConnection();
        try {
            return TargetsDAO.getAllTargets(con, mediaBuyerId, monthDate, skinId, skinBusinessCaseId);
        } finally {
            closeConnection(con);
        }
    }
}
