package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.AssetIndexMarket;
import il.co.etrader.backend.dao_managers.AssetIndexMarketDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class AssetIndexMarketManager extends BaseBLManager {

	private static final Logger logger = Logger
			.getLogger(AssetIndexMarketManager.class);

	public static ArrayList<AssetIndexMarket> getAssetIndexMarketsList(String marketName, long skinId, long groupId) throws SQLException {
		Connection con = getConnection();
		try {
			return AssetIndexMarketDAO.getAssetIndexMarketsList(con, marketName, skinId, groupId);
		} finally {
			closeConnection(con);
		}
	}

	public static void update(AssetIndexMarket indexMarket) throws SQLException {
		Connection con = getConnection();
		try {
			AssetIndexMarketDAO.update(con, indexMarket);
		} finally {
			closeConnection(con);
		}
	}

}
