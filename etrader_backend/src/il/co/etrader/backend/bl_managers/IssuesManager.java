package il.co.etrader.backend.bl_managers;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.backend.service.transaction.TransactionChargebackRequest;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.IssueComponent;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.service.userdocuments.UserDocumentsDAO;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.FilesDAO;
import il.co.etrader.backend.dao_managers.IssuesDAO;
import il.co.etrader.backend.dao_managers.PopulationEntriesDAO;
import il.co.etrader.backend.mbeans.IssuesForm;
import il.co.etrader.backend.mbeans.RiskUsersFiles;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.IssueReport;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.ucp.jdbc.HarvestableConnection;

public class IssuesManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(UsersManager.class);



    /**
	 * Add new issue and a new issue action to the db.
	 */
	public static boolean insertIssue(Issue i, IssueAction action, User user, int screenId) throws SQLException {
		boolean res = false;

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update/Insert Issue:  " + ls + "User:" + user.getUserName() + ls + i.toString());
		}

		i.setUserId(user.getId());
		res = IssuesManagerBase.insertIssue(i, action, user.getUtcOffset(), action.getWriterId(), user, screenId);
		if(res) {
	    	if (user.getId() > 0 &&  null != action && null != action.getReachedStatusId()) {
	    		if (action.getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))) {
	    			PopulationsManagerBase.fixPopualtionUserDetails(user.getId(), ConstantsBase.USER_CHANGE_DETAILS_PHONE_NUMBER);
	    		}   		
	    	} 	
		}
		return res;
	}


	/**
	 * update issue and/or insert a new issue action to the db.
	 */
	public static boolean updateIssue(Issue i, IssueAction action, boolean isIssueChanged, User user, int screenId) throws SQLException {
		FacesContext context=FacesContext.getCurrentInstance();
		boolean res = false;

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update/Insert Issue:  " + ls + "User:" + user.getUserName() + ls + i.toString() + ls + action.toString());
		}

		Connection con = getConnection();
		try {
			// if given issue action is not null insert it as a new action to the issue.
			if (action != null){
				action.setIssueId(i.getId());
				action.setActionTime(new Date());
				action.setActionTimeOffset(user.getUtcOffset());

				res = IssuesManagerBase.validateAndInsertAction(con, action, user, i, screenId);
				//IssuesManagerBase.insertAction(con, action);
				//checkAction(i,action);
			}

			if(res && isIssueChanged){
				IssuesDAO.updateIssue(con, i);
			}

			String resStr;
			if (res){
				resStr = "issues.update.success";
			}else{
				resStr = "issues.update.failed";
			}
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(resStr, null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);


			res = true;
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "Issue Updated succesfuly!");
			}

		} catch(Exception e){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.update.failed", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

			logger.debug("Error",e);
		} finally {
			closeConnection(con);
		}

		if(res) {
	    	if (user.getId() > 0 &&  null != action && null != action.getReachedStatusId()) {
	    		if (action.getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))) {
	    			PopulationsManagerBase.fixPopualtionUserDetails(user.getId(), ConstantsBase.USER_CHANGE_DETAILS_PHONE_NUMBER);
	    		}   		
	    	} 	
		}
		
		return res;
	}

	/**
	 * update is_significant field in issue action.
	 */
	public static boolean updateSignificantAction(IssueAction action) throws SQLException {
		Connection con = getConnection();
		try {
			 IssuesDAO.updateSignificantAction(con,action);
		} finally {
			closeConnection(con);
		}

		return true;
	}

	/**
     * Get the issue actions of the issue with the given id
     * @param issue Id
     * @return
     * @throws SQLException
     */
    public static ArrayList<IssueAction> getActionsByIssueId(long id, long relevantToCCList, boolean relevantToId, int issueType) throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAO.getActionsByIssueId(con, id, relevantToCCList, relevantToId, issueType);
		} finally {
			closeConnection(con);
		}

	}

     public static ArrayList<Issue> searchIssues(IssuesForm f,String skins, boolean byWriter, boolean bySignificantNotes, long writerId ,ApplicationData ap, String utcOffset, Locale l) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return IssuesDAO.search(con, f.getPopulationType(), f.getFrom(), f.getTo(), f.getChannelId(), f.getPriorityId(), f.getStatusId(),
					f.getSubjectId(), f.getUserName(), f.getClassId(),skins, byWriter, bySignificantNotes,f.getCallDirectionId(),
					f.getReachedStatusId(), f.getActionTypeId(), writerId, ap, utcOffset, l, f.getComponentId());

		} finally {
			closeConnection(con);
		}

	}


	public static ArrayList<Issue> searchIssuesByUserID(IssuesForm f, long userId,String skins, boolean byWriter,
			boolean bySignificantNotes, long writerId, long issueType) throws SQLException {
		Connection con = getSecondConnection();
		try {
			logger.info("Before searchIssuesByUserID");
			return IssuesDAO.searchByUserID(con, userId, f.getFrom(), f.getTo(), f.getChannelId(), f.getPriorityId(), f.getStatusId(),
					f.getSubjectId(), f.getUserName(), f.getClassId(),skins, byWriter, bySignificantNotes,f.getCallDirectionId(),
					f.getReachedStatusId(), f.getActionTypeId(), writerId, issueType, f.getComponentId());

		} finally {
			logger.info("After searchIssuesByUserID");
			closeConnection(con);
		}

	}

	public static ArrayList<Issue> searchIssuesByPopUserID(IssuesForm f, long populationEntryId,String skins , boolean byWriter,
			boolean bySignificantNotes, long writerId, long issueType) throws SQLException {
		Connection con = getSecondConnection();
		try {
			logger.info("Before searchIssuesByPopUserID");
			return IssuesDAO.searchByPopUserID(con, populationEntryId, f.getFrom(), f.getTo(), f.getChannelId(), f.getPriorityId(), f.getStatusId(),
					f.getSubjectId(), f.getUserName(), f.getClassId(),skins, byWriter, bySignificantNotes,f.getCallDirectionId(), f.getReachedStatusId(),
					f.getActionTypeId(), writerId, issueType, f.getComponentId());

		} finally {
			logger.info("After searchIssuesByPopUserID");
			closeConnection(con);
		}

	}

	// return number of successful insertions
	public static ArrayList<Integer> insertAutoIssues(ArrayList<String> usersList,Issue issue, IssueAction issueAction, int screen, String writerSkinToString) throws Exception{
		if (usersList == null) {
			throw new IllegalArgumentException("List is null");
		}
		
		int insertsCount = 0;
		int marketingCount = 0;
		int errorCount = 0;
		int userInMarketingOrHigherPopulation = 0;
		
		int issueActionTypeId = 0;
		long issueId;

		try{
			issueActionTypeId = Integer.parseInt(issueAction.getActionTypeId());
		}catch (Exception e) {
			issueActionTypeId = 0;
		}


		Connection con = getConnection();
    	con.setAutoCommit(false);
    	HarvestableConnection hc = (HarvestableConnection) con;
		hc.setConnectionHarvestable(false);
		
		try{
				for(int p = 0;p<usersList.size();p++){
					String userId = usersList.get(p);
					logger.debug("Start processing user "+p+" of " +usersList.size());
					boolean isIssueInsert = false;

					if (issueAction.getMarketingOperationId() == 0){
						long marketingNoDisplayEntryId = 0;

						try {
							long userIdLong = Long.parseLong(userId);
							User user = UsersManager.getUserDetails(userIdLong, writerSkinToString);
							if(user!=null) {
								logger.info("insert issue for user: " + userId);
							} else {
								String msg = "Can't load user with id : "+userId +"; writerSkinToString :"+writerSkinToString;
								logger.debug(msg);
								throw new Exception(msg);
							}
							issue.setUserId(userIdLong);
							issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
							issueId = 0;

							if (IssuesManagerBase.ISSUE_ACTION_DISPLAY_MARKETING_USERS == issueActionTypeId){
								// check if user is in marketing with no display
								marketingNoDisplayEntryId = PopulationEntriesDAO.getUserNoDisplayMarketingPopEntryId(con, userIdLong);
								if (marketingNoDisplayEntryId > 0){
									// find issue of marketing pop entry
									issueId = IssuesDAO.searchIssueIdByPopEntryID(con, marketingNoDisplayEntryId);

									// if not found an issue, update new issue fields otherwise update issue id in issue action
									if (0 == issueId){
										issue.setPopulationEntryId(marketingNoDisplayEntryId);
										issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_POPULATION));
									}else {
										issueAction.setIssueId(issueId);
									}
								}else{
									logger.error("Error user is not in marketing no display, user: " + userId);
									isIssueInsert = false;
								}
							}
							
							boolean inserted = false;
							if (isIssueInsert){
								con.setAutoCommit(false);

								// if not found an existing issue add a new one
								if (0 == issueId){
									// Insert Issue
									IssuesDAOBase.insertIssue(con, issue);
									// Set issue id in action.
									issueAction.setIssueId(issue.getId());
								}

								// Insert Issue action
								inserted = IssuesManagerBase.validateAndInsertAction(con, issueAction, user , issue, screen);

								if (marketingNoDisplayEntryId > 0){
									PopulationEntriesDAO.updateUserEntryIsDisplay(con, marketingNoDisplayEntryId, true);
								}
								if (inserted) {
									++insertsCount;
								}

								con.commit();
							}

						} catch (Exception e) {
							errorCount++;
							isIssueInsert = false;
							logger.error("Error in inserting issue for user: " + userId,e);
							try {
	        					con.rollback();
	        				} catch (Throwable it) {
	        					logger.error("Can't rollback.", it);
	        				}
						} finally {
							try {
	                            con.setAutoCommit(true);
	                        } catch (Exception e) {
	                            logger.error("Can't set back to autocommit.", e);
	                        }
	            		}
					// If it's an marketing population insert into undisplayed marketing population
					}else{ 
						try{
							long userIdLong = Long.parseLong(userId);
							if (PopulationsManagerBase.insertMarketingPopulation(con, userIdLong, issueAction, issue)){
								++insertsCount;
								++marketingCount;
							} else{
								userInMarketingOrHigherPopulation ++;
							}
						}catch (Exception e) {
							errorCount++;
							logger.error("Error in inserting into marketing for user: " + userId,e);
							try {
	        					con.rollback();
	        				} catch (Throwable it) {
	        					logger.error("Can't rollback.", it);
	        				}
						} 
					}
				}
		} finally {
			con.setAutoCommit(true);
			hc.setConnectionHarvestable(true);
			closeConnection(con);
		}
		
		ArrayList<Integer> retval = new ArrayList<Integer>();
		retval.add(insertsCount);
		retval.add(marketingCount);
		retval.add(userInMarketingOrHigherPopulation);
		retval.add(errorCount);
		return retval;
	}

	/**
	 * Search for issue action that conflicting with the new issue action time
	 * @param writerId
	 * @param callBackDate
	 * @return issue action of the assign call back
	 * @throws SQLException
	 */
	public static IssueAction searchForAssignCallBackByWriter(long writerId,Date callBackDate) throws SQLException{
		Connection con = getConnection();
		try {
			return IssuesDAO.searchForAssignCallBackByWriter(con, writerId, callBackDate);
		} finally {
    		closeConnection(con);
    	}
	}

	/**
	 * isExistsMarketingOperationId
	 * @param moId - marketing operation id
	 * @return
	 * @throws SQLException
	 */
	public static boolean isExistsMarketingOperationId(long moId) throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAO.isExistsMarketingOperationId(con, moId);
		} finally {
    		closeConnection(con);
    	}
	}

	public static void insertChargeBackIssue(Connection con,ChargeBack cb) throws Exception {
		Issue issue = new Issue();
		long userId = cb.getUserId();

		issue.setUserId(userId);
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CHARGEBACK));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Insert Issue
		IssuesDAOBase.insertIssue(con, issue);
		cb.setIssueId(issue.getId());

		// Insert Issue Action
		IssuesManager.insertChargeBackIssueAction(con,cb);
	}
	
	public static void insertChargeBackIssueNewBE(Connection con, TransactionChargebackRequest cb) throws Exception {
		Issue issue = new Issue();
		long userId = cb.getUserId();

		issue.setUserId(userId);
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CHARGEBACK));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Insert Issue
		IssuesDAOBase.insertIssue(con, issue);
		cb.setIssueId(issue.getId());

		// Insert Issue Action
		IssuesManager.insertChargeBackIssueActionNewBE(con,cb);
	}
	
	public static void insertChargeBackIssueActionNewBE(Connection con, TransactionChargebackRequest cb) throws Exception{
		IssueAction action = new IssueAction();
		long issueId = cb.getIssueId();

		if (issueId != 0){
			action.setIssueId(issueId);
			action.setComments(cb.getDescription());
			action.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
			action.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
			action.setWriterId(cb.getWriterId());
			action.setActionTime(new Date());
			action.setActionTimeOffset(cb.getWriterUtcOffset());

			// first insert cb issue.
			//IssuesManagerBase.validateAndInsertAction(con, action, user, i)
			IssuesManagerBase.insertAction(con, action);
		}else {
			throw new Exception("Problem inserting cb issue action as issue = 0");
		}
	}

	public static void insertChargeBackIssueAction(Connection con,ChargeBack cb) throws Exception{
		IssueAction action = new IssueAction();
		long issueId = cb.getIssueId();

		if (issueId != 0){
			action.setIssueId(issueId);
			action.setComments(cb.getDescription());
			action.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
			action.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
			action.setWriterId(Utils.getWriter().getWriter().getId());
			action.setActionTime(new Date());
			action.setActionTimeOffset(Utils.getWriter().getUtcOffset());

			// first insert cb issue.
			//IssuesManagerBase.validateAndInsertAction(con, action, user, i)
			IssuesManagerBase.insertAction(con, action);
		}else {
			throw new Exception("Problem inserting cb issue action as issue = 0");
		}
	}

	public static HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> getIssuesActionTypes(long writerId, ArrayList<SelectItem> issueChannelsSI) throws SQLException {
    	Connection con = getConnection();
    	HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> issuesActionTypes = null;

    	try {
    		issuesActionTypes = IssuesDAO.getIssueActionTypes(con, writerId, issueChannelsSI);
    	}
    	finally {
    		closeConnection(con);
    	}
    	return issuesActionTypes;
    }

	/**
	 * Get risk issues list
	 * @throws SQLException
	 */
	public static ArrayList<Issue> getRiskIssues(String skins, String priorityId, String statusId, String subjectId,String userName,
												 Date from,Date to, boolean isAdmin, String statuses, long classId, String userId,
												 long riskIssueType , String searchBy) throws SQLException {
    	Connection con = getSecondConnection();
    	try {
    		return IssuesDAO.getRiskIssues(con, skins, priorityId, statusId, subjectId, userName, from, to, isAdmin, statuses, classId, userId, riskIssueType , searchBy);
    	}
    	finally {
    		closeConnection(con);
    	}
    }

	/**
	 * Insert file into file table (generally requested file)
	 * 
	 * @param f
	 * @throws SQLException
	 */
	public static void insertRequestFiles(File f) throws SQLException {
		Connection con = getConnection();
		try {
			FilesManagerBase.updateExpDate(f);
			FilesDAO.insert(con, f);
			UserDocumentsDAO.setCurrentFlagDocs(con, f.getUserId(), f.getFileTypeId(), f.getCcId());
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update Verification call request in Credit card table
	 * @param ccId
	 * @throws SQLException
	 */
	public static void insertVerificationRequest(long  ccId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		 CreditCardsDAO.insertVerificationRequest(con, ccId);
    	}
    	finally {
    		closeConnection(con);
    	}
    }

	/**
	 * Insert document request
	 * @param ccId
	 * @throws SQLException
	 */
	public static void insertDocRequest(long issueActionId, long ccId, String type) throws SQLException {
    	Connection con = getConnection();
    	try {
    		IssuesDAO.insertDocRequest(con, issueActionId, ccId, type);
    	}
    	finally {
    		closeConnection(con);
    	}
    }

	/**
	 * Insert document request
	 * @param ccId
	 * @throws SQLException
	 */
	public static void updateUserDocStatus(long userId, long filesRiskStatusId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		IssuesDAO.updateUserDocStatus(con, userId, filesRiskStatusId);
    	}
    	finally {
    		closeConnection(con);
    	}
    }

	/**
	 * Insert document request
	 * @param ccId
	 * @throws SQLException
	 */
	public static void updateCCDocStatus(long ccId, long filesRiskStatusId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		IssuesDAO.updateCCDocStatus(con, ccId, filesRiskStatusId);
    	}
    	finally {
    		closeConnection(con);
    	}
    }

	/**
	 * Check for risk users files per user/status
	 * @param skins
	 * @param userName
	 * @param userId
	 * @param classId
	 * @param reasonId
	 * @param statuses
	 * @return
	 * @throws SQLException
	 * @throws CryptoException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static ArrayList<RiskUsersFiles> getRiskUsersFiles(	String skins, String userName, String userId, long classId, String reasonId,
																String statuses)	throws SQLException, CryptoException, InvalidKeyException,
																					IllegalBlockSizeException, BadPaddingException,
																					NoSuchAlgorithmException, NoSuchPaddingException,
																					InvalidAlgorithmParameterException {
    	Connection con = getConnection();
    	try {
    		return IssuesDAO.getRiskUsersFiles(con, skins, userName, userId, classId, reasonId, statuses);
    	}
    	finally {
    		closeConnection(con);
    	}
	}


	public static Issue getIssueById(long issueId) throws SQLException {
		Connection con = getConnection();
	    try {
	    	return IssuesDAO.getIssueById(con, issueId);
	    }
	    finally {
	    	closeConnection(con);
	    }
	}

	public static IssueActionType getLastIssueActionTypeByContactId(long contactId) {
		IssueActionType vo = new IssueActionType();
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAO.getLastIssueActionTypeByContactId(con, contactId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}

	public static IssueActionType getLastIssueActionTypeByUserId(long userId) {
		IssueActionType vo = new IssueActionType();
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAO.getLastIssueActionTypeByUserId(con, userId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}

	public static IssueActionType getLastIssueActionTypeByEntryId(long EntrieId) {
		IssueActionType vo = new IssueActionType();
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAO.getLastIssueActionTypeByEntryId(con, EntrieId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}
	
	/**
	 * get issue actions that were updated by representatives
	 * 
	 * @param writerId
	 * @return
	 */
	public static ArrayList<IssueReport> getIssueActionsUpdatedByRepresentatives(Date startDate, Date endDate, String offset, String writerFilter) {
		ArrayList<IssueReport> vo = new ArrayList<IssueReport>();
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAO.getIssueActionsUpdatedByRepresentatives(con, startDate, endDate, offset, writerFilter);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}
	
	/**
	 * Get issue component as hashmap
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, IssueComponent> getIssueComponent() throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAO.getIssueComponent(con);
		} finally {
			closeConnection(con);
		}
	}
	
}
