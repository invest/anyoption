package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.ContactsDAO;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Contact;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import com.anyoption.common.managers.BaseBLManager;

public class ContactsManager extends BaseBLManager {

	
	public static ArrayList<Contact> getSMSContactsFromFile(String usersCSV) throws SQLException{
		Connection conn = getConnection();
		ArrayList<Contact> list = null;
		try {
			list = ContactsDAO.getSMSContactsFromFile(conn, usersCSV);
		}finally{
			closeConnection(conn);
		}
		return list;
	}
	
	public static ArrayList<Contact> getSMSContacts( String skinIds, String countryIds, String campaignIds, String populationType) throws SQLException{
		Connection conn = getConnection();
		ArrayList<Contact> list = null;
		try {
			list =  ContactsDAO.getSMSContacts(conn, skinIds, countryIds, campaignIds, populationType);
		}finally{
			closeConnection(conn);
		}
		return list;
	}
	
	public static void insertDataToPromotionTable(ArrayList<Contact> contactsList, String message, long writerId, String promotionName, HttpSession session) throws SQLException{
		Connection conn = getConnection();
		session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, "0_0");
		try{
			UsersDAO.insertPromotion(conn, null, Constants.SEND_SMS, message, writerId, promotionName);
			ContactsDAO.setContactsToPromotionsTable(conn, contactsList, null, session);
		} finally {
			closeConnection(conn);
		}		
		session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, contactsList.size()+"_100");		
	}

	public static ArrayList<Contact> getContactsListByPromotionId(long promotionId) throws SQLException {
		Connection conn = getConnection();
		ArrayList<Contact> list = null;

		try {
			list = ContactsDAO.getContactsListByPromotionId(conn, promotionId);
		}finally{
			closeConnection(conn);
		}
		return list;	}
	
	public static String getLastWriterIdChat(long contactId) throws SQLException{
		Connection conn = getConnection();
		String writerName = "";
		try {
			writerName = ContactsDAO.getLastWriterIdChat(conn, contactId);
		} finally{
			closeConnection(conn);
		}
		return writerName;
	}


}
