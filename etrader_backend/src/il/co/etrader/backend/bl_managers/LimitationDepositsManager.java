package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.LimitationDepositsDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.managers.LimitationDepositsManagerBase;

public class LimitationDepositsManager extends LimitationDepositsManagerBase {
	//TODO: extends from ReutersQuotesManagerBase
	private static final Logger logger = Logger.getLogger(LimitationDepositsManager.class);

    public static void insert(List<Long> skins, List<Long> currencies, List<Long> countries, List<Long> paymentRecipients,
            List<Long> campaigns, List<Long> affiliates, Long minDeposit, Long minFirstDeposit, Long defaultDeposit, String comments, int arraySize, long remarketing) throws Exception {
        Connection con = getConnection();
        try {
            con.setAutoCommit(false);
            Long groupId = null;
			for (int i = 0; i < arraySize; i++) {
				LimitationDeposits ld = new LimitationDeposits();
				if (null == minDeposit || minDeposit < 1) {
					LimitationDepositsDAO.getLimitDeposits(con, skins.get(i < skins.size() ? i : 0),
							currencies.get(i < currencies.size() ? i : 0), countries.get(i < countries.size() ? i : 0),
							paymentRecipients.get(i < paymentRecipients.size() ? i : 0),
							campaigns.get(i < campaigns.size() ? i : 0), affiliates.get(i < affiliates.size() ? i : 0),
							true, false, ld);
				} else {
					ld.setMinimumDeposit(minDeposit);
				}
				if (null == minFirstDeposit || minFirstDeposit < 1) {
					LimitationDepositsDAO.getLimitDeposits(con, skins.get(i < skins.size() ? i : 0),
							currencies.get(i < currencies.size() ? i : 0), countries.get(i < countries.size() ? i : 0),
							paymentRecipients.get(i < paymentRecipients.size() ? i : 0),
							campaigns.get(i < campaigns.size() ? i : 0), affiliates.get(i < affiliates.size() ? i : 0),
							false, true, ld);
				} else {
					ld.setMinimumFirstDeposit(minFirstDeposit);
				}
				groupId = LimitationDepositsDAO.insert(con, skins.get(i < skins.size() ? i : 0),
						currencies.get(i < currencies.size() ? i : 0), countries.get(i < countries.size() ? i : 0),
						paymentRecipients.get(i < paymentRecipients.size() ? i : 0),
						campaigns.get(i < campaigns.size() ? i : 0), affiliates.get(i < affiliates.size() ? i : 0),
						ld.getMinimumDeposit(), ld.getMinimumFirstDeposit(), defaultDeposit, comments, groupId, remarketing);
			}
            con.commit();
        } catch (Exception e) {
            logger.error("Error in inserting limit deposit", e);
            try {
                con.rollback();
            } catch (Throwable it) {
                logger.error("Can't rollback.", it);
            }
            throw e;
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (Exception e) {
                logger.error("Can't set back to autocommit.", e);
            }
            closeConnection(con);
        }
    }

    public static ArrayList<LimitationDeposits> search(String skins, String currencies, String countries, String paymentRecipients,
            String campaigns, String affiliates, long isActive, boolean isValidate, List<Long> types, long remarketing) throws SQLException {
        Connection con = getConnection();
        try {
            return LimitationDepositsDAO.search(con, skins, currencies, countries, paymentRecipients, campaigns, affiliates, isActive, isValidate, types, remarketing);
        } finally {
            closeConnection(con);
        }
    }

    public static void update(LimitationDeposits limitationDeposit) throws SQLException {
        Connection con = getConnection();
        try {
            LimitationDepositsDAO.update(con, limitationDeposit);
        } finally {
            closeConnection(con);
        }
    }

    public static ArrayList<LimitationDeposits> searchById(long id, boolean isGroupId) throws SQLException {
        Connection con = getConnection();
        try {
            return LimitationDepositsDAO.searchById(con, id, isGroupId);
        } finally {
            closeConnection(con);
        }
    }
}