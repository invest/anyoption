package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.bl_vos.SalesDepositsSummary;
import il.co.etrader.backend.dao_managers.SalesDepositsDAO;

/**
 * 
 * @author Ivan Petkov
 *
 */
public class SalesDepositsManager  extends BaseBLManager {
	
	/**
	 * Returning sales deposits summary list for one page for sales deposits conversion screen
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @param writerId
	 * @param offset
	 * @param pageNo
	 * @param resultsPerPage
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<SalesDepositsSummary> getDepositsConversionPage(long userId, Date startDate, Date endDate, long writerId, long pageNo, long resultsPerPage, long skinId) throws Exception {
		Connection con = getConnection();
		try {
			return SalesDepositsDAO.getDepositsConversionPage(con, userId, startDate, endDate, writerId, pageNo, resultsPerPage, skinId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Returning sales deposits summary list for one page for sales deposits retention screen
	 * @param userId
	 * @param startDate
	 * @param endDate
	 * @param writerId
	 * @param offset
	 * @param pageNo
	 * @param resultsPerPage
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<SalesDepositsSummary> getDepositsRetentionPage(long userId, Date startDate, Date endDate, long writerId, long pageNo, long resultsPerPage, long skinId) throws Exception {
		Connection con = getConnection();
		try {
			return SalesDepositsDAO.getDepositsRetentionPage(con, userId, startDate, endDate, writerId, pageNo, resultsPerPage, skinId);
		} finally {
			closeConnection(con);
		}
	}
}
