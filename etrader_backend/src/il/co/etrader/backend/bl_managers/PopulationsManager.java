package il.co.etrader.backend.bl_managers;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.PopulationEntriesDAO;
import il.co.etrader.backend.dao_managers.PopulationsDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopOpenWithdrawHandler;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;


public class PopulationsManager extends PopulationsManagerBase {

	private static final Logger logger = Logger.getLogger(User.class);
	
	private static HashMap<Long, Writer> salesWritersConversion;
	private static HashMap<Long, Writer> salesWritersRetention;



	/**
	 * Returns population typss
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static HashMap<Long, PopulationType> getPopulationTypes(Connection con) throws SQLException{
		HashMap<Long, PopulationType> list = null;
		try {
			list = PopulationsDAO.getPopulationTypesHash(con);
		}
		finally {
		}
		return list;
	}

	/**
	 * Returns populations
	 * @return a list of Populations
	 * @throws SQLException
	 */
	public static ArrayList<Population> getPopulationsForWriter() throws SQLException{
		Connection con = getConnection();
		ArrayList<Population> list = null;
		try {
			list = PopulationsDAO.getPopulationsForWriter(con);
		}
		finally {
		}
		return list;
	}

	/**
	 * Insert / update population
	 * @param population population instance
	 * @return true if success false if not
	 * @throws SQLException
	 */
	public static void insertUpdatePopulation(Population population) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update population: " + ls + population.toString());
  		 }

		Connection con = getConnection();

    	try {
			if ( population.getId() == 0 ) {  // insert
				PopulationsDAO.insert(con, population);
			} else { // update
				PopulationsDAO.update(con, population);
			}

    	} finally {
    		closeConnection(con);
    	}
	}

	/**
	 * Get retention writers
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getRetentionWriters() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getRetentionWriters(con);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Returns populations count list
	 * @param skin id
	 * @param population type
	 * @return a list of Populations
	 * @throws SQLException
	 */
	public static ArrayList<Population> getCountPopulations(long skinId,long populationType) throws SQLException{
		Connection con = getConnection();
		ArrayList<Population> list = null;
		try {
			list = PopulationsDAO.getCountPopulations(con,skinId,populationType);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Get assigned rep list
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getAssignedRepList(boolean isByDepartment) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getAssignedRepSI(con, isByDepartment);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Get writer rep list
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getSalesWriters(String skins) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getSalesWriters(con,skins);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert new entry to populationEntries (connection wrraper)
	 * @param p
	 * @throws PopulationHandlersException
	 */
	public static void insertIntoPopulationEntries(PopulationEntryBase p, long issueActionId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			PopulationsManagerBase.insertIntoPopulationEntries(con ,p,Utils.getWriter().getWriter().getId(),issueActionId,true);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationsTypesByDeps(String skins) throws SQLException{
		Connection con = getConnection();
		ArrayList<PopulationType> list = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		int deptId = ConstantsBase.DEPARTMENT_RETENTION;

		if (user.isSupportSelected()){
			deptId = ConstantsBase.DEPARTMENT_SUPPORT;
		}

		try {
			list = PopulationsDAO.getPopulationsTypesByDeps(con,skins,deptId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationTypesStrBySalesType(String skins) throws SQLException{
		Connection con = getConnection();
		ArrayList<PopulationType> list = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		Writer w = Utils.getWriter().getWriter();
		long popTypeId = 0;

		if (w.getSales_type() == Constants.SALES_TYPE_CONVERSION) {
			popTypeId = Constants.POPULATION_TYPE_ID_CONVERSION;
		} else if (w.getSales_type() == Constants.SALES_TYPE_RETENTION) {
			popTypeId = Constants.POPULATION_TYPE_ID_RETENTION;
		} else if (w.getSales_type() == Constants.SALES_TYPE_BOTH || w.getSales_type() == 0) {
			if (w.getScreenType() == Constants.SALES_TYPE_CONVERSION) {
				popTypeId = Constants.POPULATION_TYPE_ID_CONVERSION;
			} else {
				popTypeId = Constants.POPULATION_TYPE_ID_RETENTION;
			}
		}

		int deptId = ConstantsBase.DEPARTMENT_RETENTION;

		if (user.isSupportMoreOptionsSelected()) {
			deptId = ConstantsBase.DEPARTMENT_SUPPORT;
			popTypeId = 0;
		}

		try {
			list = PopulationsDAO.getPopulationTypesStrBySalesType(con, skins, deptId, popTypeId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}
	
	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationTypesStrBySalesTypeForTracking(String skins) throws SQLException{
		Connection con = getConnection();
		ArrayList<PopulationType> list = null;
		try {
			list = PopulationsDAO.getPopulationTypesStrBySalesTypeForTracking(con, skins);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Returns population types list by sales type
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationTypesStrBySalesType(String skins, String populations) throws SQLException{
		Connection con = getConnection();
		ArrayList<PopulationType> list = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		int deptId = ConstantsBase.DEPARTMENT_RETENTION;

		if (user.isSupportSelected()){
			deptId = ConstantsBase.DEPARTMENT_SUPPORT;
		}

		try {
			list = PopulationsDAO.getPopulationsTypesByDeps(con,skins,deptId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Gets locked entry by lock History Id
	 * @param writerId the writer for input param
	 * @return true if locking
	 * @throws PopulationHandlersException
	 */
	public static PopulationEntry getLockedEntryByLockHisId(long lockHistoryId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return PopulationsDAO.getLockedEntryByLockHisId(con, lockHistoryId);
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean hasReachedCallInOtherPopulations(long userId, long entryTypeId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationEntriesDAO.hasReachedCallInOtherPopulations(con, userId, entryTypeId);
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing population type ", t);
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<Integer, ArrayList<String>> getTypesIdAndValueArray()throws SQLException{
		HashMap<Integer, ArrayList<String>> hm = null;
		Connection con = null;

		try {
			con = getConnection();
			hm = PopulationsDAO.getTypesIdAndValueArray(con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection(con);
		}
		return hm;
	}

	public static HashMap<Integer, ArrayList<String>> getKeyAndPriorityArray()throws SQLException{
		HashMap<Integer, ArrayList<String>> hm = null;
		Connection con = null;

		try {
			con = getConnection();
			hm = PopulationsDAO.getKeyAndPriorityArray(con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection(con);
		}
		return hm;
	}

	public static ArrayList<Population> getPopulationListByUserId(long userId) throws Exception {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getPopulationListByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void removeFromOpenWithdrawPop(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			
			PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(userId);
			PopOpenWithdrawHandler ph = new PopOpenWithdrawHandler();
			ph.approvePendingWithdraw(con, popUser, Utils.getWebWriterId(), PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0, true);
		} catch (PopulationHandlersException e) {
			logger.error("Could not remove from population");
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<Long, Writer> getSalesWritersBySalesType(String skinsToString, int salesTypeConversion) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getSalesWritersBySalesType(con, skinsToString, salesTypeConversion);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Returning all sales writers of sales type conversion that the sales manager can see 
	 * @return salesWritersConversion
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getSalesWritersConversionSalesType() throws SQLException {
		if (salesWritersConversion == null) {
			try {
				salesWritersConversion = getSalesWritersBySalesType(null, PopulationsManagerBase.SALES_TYPE_CONVERSION);
			} catch (SQLException sqle) {
				logger.error("Can't load sales conversion writers.", sqle);
			}
		}
		return salesWritersConversion;
	}
	
	/**
	 * Returning all sales writers of sales type retention that the sales manager can see 
	 * @return salesWritersRetention
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getSalesWritersRetentionSalesType() throws SQLException {
		if (salesWritersRetention == null) {
			try {
				salesWritersRetention = getSalesWritersBySalesType(null, PopulationsManagerBase.SALES_TYPE_RETENTION);
			} catch (SQLException sqle) {
				logger.error("Can't load sales retention writers.", sqle);
			}
		}
		return salesWritersRetention;
	}
	
	/**
	 * Get assigned rep list
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getAssignedRepListBySalesType(long salesType) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAO.getAssignedRepListBySalesType(con, salesType);
		} finally {
			closeConnection(con);
		}
}
}