package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TradersActionsManager;
import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.jms.BackendLevelsCache;

import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.OpportunityOddsPairTemplate;
import il.co.etrader.backend.bl_vos.OpportunityTotal;
import il.co.etrader.backend.bl_vos.SalesTurnoversDetails;
import il.co.etrader.backend.bl_vos.SalesTurnoversTotals;
import il.co.etrader.backend.bl_vos.StatsResult;
import il.co.etrader.backend.dao_managers.InvestmentsDAO;
import il.co.etrader.backend.dao_managers.MarketsDAO;
import il.co.etrader.backend.dao_managers.OpportunitiesDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.ExchangesHolidays;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.InvestmentsGroup;
import il.co.etrader.bl_vos.OpportunityTemplate;
import il.co.etrader.bl_vos.OptionExpiration;
import il.co.etrader.bl_vos.Taxpayment;
import il.co.etrader.bl_vos.TranInvestment;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.OpportunityOddsTypesDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.DataPage;

/**
 * backend Investments manager.
 *
 * @author Tony
 */
public class InvestmentsManager extends InvestmentsManagerBase {

	private static final Logger logger = Logger.getLogger(InvestmentsManager.class);

	public static void cancelInvestment(long id) throws SQLException {
		Connection con = getConnection();
		FacesContext context = FacesContext.getCurrentInstance();


		try {

			con.setAutoCommit(false);
			boolean res = cancelInvestment(id, AdminManager.getWriterId(), con);

			if ( res == true ) {
				con.commit();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("investments.cancel.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
			}
			else {  // cant cancel investment
				con.rollback();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("transinvestments.cancel.failed", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
			}

		} catch (SQLException e) {
			logger.error("Exception on Cancel Investment!!! " + e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	public static void cancelAllInvestments(ArrayList investments) throws SQLException {
		Connection con = getConnection();
		FacesContext context = FacesContext.getCurrentInstance();

		try {

			con.setAutoCommit(false);

			for (int i = 0; i < investments.size(); i++) {
				Investment inv = (Investment) investments.get(i);
				boolean res = cancelInvestment(inv.getId(), AdminManager.getWriterId(), con);
				if (res == true) {
					AdminManagerBase.addToLog(AdminManager.getWriterId(), "investments", inv.getId(), Constants.LOG_COMMANDS_CANCEL_ALL_INV, "Investments ID: " + inv.getId());
				}
			}

			con.commit();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("investments.cancel.success", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);


		} catch (SQLException e) {
			logger.error("Exception on Cancel all Investments!!! " + e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	/*public static long getTotalWinLoseForTheYear(long userId) throws SQLException {
		Connection con = getConnection();
		try {   // change to new Tax function
			return InvestmentsDAO.getTotalWinLoseForTheYear(con, userId, false);
		} finally {
			closeConnection(con);
		}
	}*/

	public static ArrayList searchGroups() throws SQLException {
		Connection con = getConnection();
		try {
			return MarketsDAO.getAllGroups(con);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList getOpportunities(Date from, String publishedSettled, long groupId, long marketId , long marketTypeId, long statusId, long classId, long opportunityId, long scheduledId, int suspended) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.get(con, from, publishedSettled, groupId, marketId, marketTypeId, statusId, classId, opportunityId, scheduledId, suspended);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchOptionsExpiration(Long marketId, int month, int year) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.searchOptionsExpiration(con, marketId, month, year);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchOddsTypes() throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunityOddsTypesDAO.searchOddsTypes(con);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<InvestmentLimit> searchInvLimits(Date from, Date to, long userId, long active, long marketId) throws SQLException {
		Connection con = getConnection();
		try {
			return InvestmentsDAO.searchInvLimits(con, from, to, userId, active, marketId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchShiftings(Date from, long groupId, long marketId, long shiftId) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.searchShiftings(con, from, groupId, marketId, shiftId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchExchanges() throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.searchExchanges(con);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchExchangesHolidays(Long exchangeId, Date from) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.searchExchangesHolidays(con, exchangeId, from);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateExchange(Exchange e) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update Exchange: " + ls + e.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			if (e.getHalfDayClosingTime().trim().equals("00:00")) {
				e.setHalfDayClosingTime(null);
			}
			// update
			OpportunitiesDAO.updateExchange(con, e);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);


			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Update exchange finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertOptionExpiration(OptionExpiration eh) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "updateInsertOptionExpiration: " + ls + eh.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			if (eh.getId() == 0) {

				if (OpportunitiesDAO.checkForDoubleMarketsPerMonth(con, eh.getMarketId(), eh.getExpiryDate())) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("optionsexpirations.doublemarkets", null, Utils.getWriterLocale(context)),null);
					context.addMessage("optionsExpirationsForm:marketId", fm);
					return false;
				}

				// insert
				OpportunitiesDAO.insertOptionExpiration(con, eh);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);


			} else {
				// update

				OpportunitiesDAO.updateOptionExpiration(con, eh);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Update updateInsertOptionExpiration finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertExchangeHoliday(ExchangesHolidays eh) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update ExchangeHolidays: " + ls + eh.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			if (eh.getId() == 0) {
				// insert
				OpportunitiesDAO.insertExchangeHoliday(con, eh);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				OpportunitiesDAO.updateExchangeHoliday(con, eh);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Update ExchangeHolidays finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static ArrayList searchOppTemplates(long groupId, long marketId, boolean isSAdmin,long isActive, long skinId) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAO.searchOppTemplates(con, groupId, marketId, isSAdmin,isActive, skinId);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateInsertOddsType(OpportunityOddsType o) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/update Odds Type: " + ls + o.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			con.setAutoCommit(false);

			if (o.isDefaultBoolean())
				OpportunityOddsTypesDAO.clearDefaultOddsType(con);

			if (o.getId() == 0) {
				// insert
				OpportunityOddsTypesDAO.insertOddsType(con, o);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				OpportunityOddsTypesDAO.updateOddsType(con, o);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			con.commit();
			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/update Odds Type finished successfully. " + ls);
			}
		} catch (SQLException e) {
			logger.error("Exception on Insert/update Odds Type!!! " + e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;

		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertOppTemplate(OpportunityTemplate o, boolean isService) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/update Opporunity template: " + ls + o.toString());
		}
		Connection con = getConnection();
		try {

			con.setAutoCommit(false);

			if (o.getId() == 0) {
				// insert
				OpportunitiesDAO.insertOppTemplate(con, o);
				if (!isService) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);
				}

			} else {
				// update
				OpportunitiesDAO.updateOppTemplate(con, o);

				//update odds in other templates
				if(o.isChangeAll() || o.isChangeAllScheduled()){
					OpportunitiesDAO.updateOppTemplatesOdds(con, o);
					if (!isService) {
						AdminManagerBase.addToLog(AdminManager.getWriterId(), "markets", o.getMarketId(), Constants.LOG_COMMANDS_CHANGE_OPP_ODS, "Odds Type ID: " + o.getOddsTypeId());
					}
				}

				if (!isService) {
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);
				}
			}

			con.commit();

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/update Opporunity template finished successfully." + ls);
			}

		} catch (SQLException e) {
			logger.error("Exception on Insert/update Opporunity template!!! ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/update Opporunity template finished successfully." + ls);
		}

		return true;
	}

	public static boolean updateInsertInvLimit(InvestmentLimit il) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/update Investment limit: " + ls + il.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			if (il.getId() == 0) {
				// insert
				InvestmentsDAO.insertInvestmentLimit(con, il);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				InvestmentsDAO.updateInvestmentLimit(con, il);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}
			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/update Investment limit finished ok." + ls);
			}
		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateOpportunityOdds(Opportunity o, long oddsTypeId, String oddsPairGroup) throws SQLException {
		boolean res = false;
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update Opportunity: " + ls + o.toString());
		}

		Connection con = getConnection();
		try {
			OpportunitiesDAO.updateOpportunityOdds(con, o, oddsTypeId, oddsPairGroup);
		} catch (SQLException e) {
        	if (logger.isEnabledFor(Level.DEBUG)) {
    			logger.log(Level.ERROR, "Update Opportunity Odds: "+ o.getId() + " failed! ", e);
    		}
        	return res;
		}
		finally {
			closeConnection(con);
		}
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Opportunity Odds changed" + ls);
		}
		return res;
	}

	public static boolean updateOpportunity(Opportunity oppToBeUpdated) throws SQLException {
		// This method is called by a form that is not used anymore.
	    	// ^^^^^^^^^^^^ 	/* DEV-1821 */
		// They do per CORE-2057 and CORE-2176 ....
	    
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Updating Opportunity: " + ls + oppToBeUpdated.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			FacesMessage fm = null;
			String keyMsg = null;
			if (oppToBeUpdated.getOpportunityTypeId() != Opportunity.TYPE_ONE_TOUCH) {  // no need to update the service about one touch opp
				
				Opportunity theOldOpp = OpportunitiesDAO.getById(con, oppToBeUpdated.getId());

				HashMap hm = new HashMap();
				hm.put("market", null);
				hm.put("opportunityTypeDesc", null);
				hm.put("opportunityOddsTypeName", null);

				String changes = CommonUtil.compareObjects(theOldOpp, oppToBeUpdated, hm);
			    if (changes.indexOf("oddsGroup") > -1) {
					BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
					OpportunitiesDAO.updateOpportunity(con, oppToBeUpdated);
	
					if (!levelsCache.notifyForOpportunityOddsChange(oppToBeUpdated.getId())) {
					    OpportunitiesDAO.updateOpportunity(con, theOldOpp);
					    keyMsg = "general.update.faild";
					    fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(keyMsg, null, Utils.getWriterLocale(context)),null);
					    context.addMessage(null, fm);
					    logger.log(Level.ERROR, "Sending update to levels cache with new odds Type ID of opportunity: " + oppToBeUpdated.getId() + " has failed");
					    return false;
					} else {
	        			    keyMsg = "general.update.success";
	        			    AdminManager.addToLog(AdminManager.getWriterId(), "opportunities", theOldOpp.getId(), Constants.LOG_COMMANDS_UPDATE_OPP, changes);
	        			    fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage(keyMsg, null, Utils.getWriterLocale(context)), null);
	        			    context.addMessage(null, fm);
					}
			    } 
			    if (changes.indexOf("suspended") > -1) {
					BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
	
					if (levelsCache.notifyForSuspOpp(oppToBeUpdated.getId(), InvestmentsManager.getOpportunityById(oppToBeUpdated.getId()).getMarket().getFeedName(),
						oppToBeUpdated.isSuspended() ? 1 : 0)) {
					    try {
					    OpportunitiesManager.setOppSuspDB(oppToBeUpdated.getId(), oppToBeUpdated.isSuspended() ? 1 : 0);
						if (oppToBeUpdated.isSuspended()) {
							AdminManagerBase.addToLog(AdminManager.getWriterId(), "opportunities",
									oppToBeUpdated.getId(), Constants.LOG_COMMANDS_SUSPEND_OPP, "");
							TradersActionsManager.refreshUsersCache(AdminManager.getWriterId());
						}
						logger.log(Level.INFO, "Suspended Opportunity: " + oppToBeUpdated.getId());
					    } catch (Exception e) {
						keyMsg = "opportunities.suspended.fail";
						fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(keyMsg, null, Utils.getWriterLocale(context)),null);
						context.addMessage(null, fm);
						logger.log(Level.ERROR, "Can't Suspend Opportunity: " + e);
						return false;
					    }
					} else {
					    keyMsg = "opportunities.suspended.fail";
					    fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(keyMsg, null, Utils.getWriterLocale(context)),null);
					    context.addMessage(null, fm);
					    logger.log(Level.ERROR, "Sending update to levels cache with new odds Type ID of opportunity: "+ oppToBeUpdated.getId() + " has failed");
					    return false;
					}
			    }
			} else {
			    // for one touch update the opportunity
			    OpportunitiesDAO.updateOpportunity(con, oppToBeUpdated);
			    keyMsg = "general.update.success";
			    fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage(keyMsg, null, Utils.getWriterLocale(context)), null);
			    context.addMessage(null, fm);
			}
		} catch (Exception e) {
		    logger.log(Level.ERROR, "Updating opportunity: "+ oppToBeUpdated.getId() + " has failed with exception " + e);
		}
		finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertGroup(MarketGroup f) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update/Insert Market Group: " + ls + f.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			f.setDisplayName(f.getDisplayNameKey());
			if (f.getId() == 0) {
				// insert
				MarketsDAO.insertGroup(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update

				MarketsDAO.updateGroup(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Update/Insert finished ok. " + ls);
			}
		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static ArrayList<Taxpayment> getTaxpayments(int userId) throws SQLException {
		ArrayList<Taxpayment> list = null;
		Connection con = null;
		try {
			con = getConnection();
			list = InvestmentsDAO.getTaxpayments(con, userId);

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "getting taxpayments succeed for user ID: " + userId);
			}
		} catch (SQLException e) {
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "getting taxpayments faild for user ID: " + userId);
			}
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static ArrayList getTransactionsInvestments(long userId, Date from, Date to, long invId, 
			String bonusIdFreeTextFilter, String transIdFreeTextFilter, int transactionTypeId) throws SQLException {
		ArrayList l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			// get investments and transactions for those dates and another extra row only for calculate win/lose to last row.
			l = InvestmentsDAO.getInvAndTransactions(conn, userId, from, to, invId, 
					bonusIdFreeTextFilter, transIdFreeTextFilter, transactionTypeId);

			if (l.size() > 0) {
				// fills the win/lose columns for transactions
				TranInvestment ti = (TranInvestment) l.get(0);
				long newBalance = ti.getBalance();
				long oldBalance = 0;

				for (int i = 1; i < l.size(); i++) {
					ti = (TranInvestment) l.get(i);
					oldBalance = ti.getBalance();
					long diff = newBalance - oldBalance;

					ti = (TranInvestment) l.get(i - 1);
					ti.setAmount(Math.abs(diff));
					if (newBalance - oldBalance > 0) {
						ti.setWin(diff);
					} else {
						ti.setLose(-diff);
					}
					newBalance = oldBalance;
				}

				ti = (TranInvestment) l.get(l.size() - 1);
				oldBalance = 0;
				long diff = newBalance - oldBalance;
				ti.setAmount(Math.abs(diff));
				if (newBalance - oldBalance > 0) {
					ti.setWin(diff);
				} else {
					ti.setLose(-diff);
				}
				
				// if extra row exist, remove it for display results only between 'from' and 'to' dates.
				if ((ti.getTimeCreated()).before(from)) {
					l.remove(l.size() - 1);
				}
			}

		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static String[] getInvestmentsTotals(long userId) throws SQLException {
		String[] l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = InvestmentsDAO.getInvestmentsTotals(conn, userId);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static String[] getInvestmentsTotals(long userId, long apiExternalUserId) throws SQLException {
		String[] l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = InvestmentsDAO.getInvestmentsTotals(conn, userId, apiExternalUserId);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static String getInvestmentsWinLose(long userId, int period) throws SQLException {
		String l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = InvestmentsDAO.getInvestmentsWinLose(conn, userId, period);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static String getUsersWinLose(long userId) throws SQLException {
		String l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = InvestmentsDAO.getUsersWinLose(conn, userId);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static String getUsersWinLose(long userId, long apiExternalUserId) throws SQLException {
		String l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = InvestmentsDAO.getUsersWinLose(conn, userId, apiExternalUserId);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static long getNumOfCancelInvestments(long id1) throws SQLException {
		Connection con = getSecondConnection();
		try {

			return InvestmentsDAO.getNumOfCancelInvestments(con, id1);

		} finally {
			closeConnection(con);
		}
	}

	public static long getNumOfCancelInvestments(long id1, long apiExternalUserId) throws SQLException {
		Connection con = getSecondConnection();
		try {

			return InvestmentsDAO.getNumOfCancelInvestments(con, id1, apiExternalUserId);

		} finally {
			closeConnection(con);
		}
	}

	public static boolean hasInvestments(long userId, boolean settled) throws SQLException {
		Connection con = getConnection();
		try {

			return InvestmentsDAO.hasInvestments(con, userId, settled);

		} finally {
			closeConnection(con);
		}
	}

	public static boolean hasNonCanceledInvestmentsByOppId(long opId) throws SQLException {
		Connection con = getConnection();
		try {
			return InvestmentsDAO.hasNonCanceledInvestmentsByOppId(con, opId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get all Totals for investments page
	 * 	with by filters
	 */
	public static ArrayList<Object> getBaseTotals(long userId,Date from,Date to,String isSettled,
			String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
				long currencyId, String opportunities,String skins,long invType,long invAmount,
				long invAmountBelow ,long invCloseHour,long invCloseMin, long skinBusinessCaseId, String groups, String markets, 
				int houseResult, String orderBy, Date arabicStartDate, String insertedFrom, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId) throws SQLException {

        ArrayList<Object> l = null;
        Connection conn = null;

        try {
            conn = getSecondConnection();
            l = InvestmentsDAO.getBaseTotals(conn,userId,from,to,isSettled,marketGroup, scheduled,
            		timeType,userName,classId, statusId, currencyId, opportunities,skins,invType,
            		invAmount,invAmountBelow,invCloseHour,invCloseMin, skinBusinessCaseId, groups, markets,
            		houseResult, orderBy, arabicStartDate, insertedFrom, apiExternalUserId, copyopInvTypeId, fromUserId, invId);

        } finally {
            closeConnection(conn);
        }
        return l;
    }

	/**
	 * Get all investments with filters
	 * @param startRow
	 * 		start row for rowNum
	 * @param pageSize
	 * 		num of rows from startRow
	 * @param isPaging TODO
	 * @param sumInvestments TODO
	 * @return
	 * 		ArrayList of investments
	 * @throws SQLException
	 */
	public static ArrayList<Investment> getInvestmentsList(long userId,Date from,Date to,String isSettled,
			String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
				long currencyId, String opportunities, int startRow, int pageSize,String skins,long invType,long invAmount,
				long invAmountBelow, long invCloseHour,long invCloseMin, long skinBusinessCaseId, String groups, String markets,
				int houseResult, boolean hoursFilter, String orderType, boolean isPaging, Investment sumInvestments, Date arabicStartDate, 
				String insertedFrom, long countryId, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId) throws SQLException {

		logger.info("Before getInvestmentsList");
        ArrayList<Investment> l = null;
        Connection conn = null;

        try {
            conn = getSecondConnection();
            l = InvestmentsDAO.get(conn, userId, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
            		statusId, currencyId, opportunities, startRow, pageSize, skins, invType, invAmount,
            		invAmountBelow, invCloseHour, invCloseMin, skinBusinessCaseId, groups, markets, houseResult, 
            		hoursFilter, orderType, false, sumInvestments, arabicStartDate, insertedFrom, countryId, apiExternalUserId, copyopInvTypeId, fromUserId, invId);
       logger.info("After getInvestmentsList");
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Get all investments with filters
     * 
     * @param startRow
     *            start row for rowNum
     * @param pageSize
     *            num of rows from startRow
     * @param modelSize
     *            all model size
     * @return DataPage object
     * @throws SQLException
     */
    public static DataPage<Investment> getInvestments(long userId, Date from, Date to, String isSettled,
	    String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
	    long currencyId, String opportunities, int startRow, long startRowSql, int pageSize, int modelSize,
	    String skins, long invType, long invAmount, long invAmountBelow, long invCloseHour, long invCloseMin,
	    long skinBusinessCaseId, String groups, String markets, int houseResult, boolean hoursFilter,
	    String orderType, Date arabicStartDate, String insertedFrom, long apiExternalUserId, int copyopInvTypeId,
	    long fromUserId, long invId) throws SQLException {
	DataPage<Investment> dataPage = null;
	ArrayList<Investment> l = null;

	l = getInvestmentsForDataPage(userId, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
		statusId, currencyId, opportunities, startRowSql, pageSize, skins, invType, invAmount, invAmountBelow,
		invCloseHour, invCloseMin, skinBusinessCaseId, groups, markets, houseResult, hoursFilter, orderType,
		arabicStartDate, insertedFrom, apiExternalUserId, copyopInvTypeId, fromUserId, invId);

	dataPage = new DataPage(modelSize, startRow, l);

	return dataPage;
    }

    public static ArrayList<Investment> getInvestmentsForDataPage(long userId, Date from, Date to, String isSettled,
	    String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
	    long currencyId, String opportunities, long startRowSql, int pageSize, String skins, long invType,
	    long invAmount, long invAmountBelow, long invCloseHour, long invCloseMin, long skinBusinessCaseId,
	    String groups, String markets, int houseResult, boolean hoursFilter, String orderType, Date arabicStartDate,
	    String insertedFrom, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId)
		    throws SQLException {
	Connection conn = null;
	try {
	    conn = getSecondConnection();
	    return InvestmentsDAO.get(conn, userId, from, to, isSettled, marketGroup, scheduled, timeType, userName,
		    classId, statusId, currencyId, opportunities, startRowSql, pageSize, skins, invType, invAmount,
		    invAmountBelow, invCloseHour, invCloseMin, skinBusinessCaseId, groups, markets, houseResult,
		    hoursFilter, orderType, true, null, arabicStartDate, insertedFrom, 0L, apiExternalUserId,
		    copyopInvTypeId, fromUserId, invId);
	} finally {
	    closeConnection(conn);
	}
    }

	public static List<InvestmentsGroup> getInvestmentsGroups(int groupBy, Date fromDate,
			Date toDate, int skinId, long opportunityTypeId,String searchBy,
    		long skinBusinessCaseId,
    		long scheduledId,
    		long groupId,
    		long market,
    		long countryId, String insertedFrom, int copyopInvTypeId, long fromUserId, long statusId) throws SQLException {

        List<InvestmentsGroup> l = null;
        Connection con = null;

        try {
            con = getSecondConnection();
            l = InvestmentsDAO.getInvestmentsGroups(con, groupBy, fromDate, toDate, skinId, opportunityTypeId ,searchBy,
            		 skinBusinessCaseId,
            		 scheduledId,
            		 groupId,
            		 market,
            		 countryId, insertedFrom, copyopInvTypeId, fromUserId, statusId);
        } finally {
            closeConnection(con);
        }
        return l;
	}


	/**
	 * Insert new one touch opportunity to Db.
	 *
	 * @param opportunity
	 * 			the new one touch opportunity
	 * @param firstInvCal
	 * 			Calander of firstInvCal
	 * @param estClosingCal
	 *  		Calander of estClosingCal
	 * @param lastInvalCal
	 * 			Calander of lastInvalCal
	 * @param timeZone
	 * 			String of time zone
	 * @throws SQLException
	 */
	public static void insertOneTouchOpportunity(Opportunity opportunity, Calendar firstInvCal,
			Calendar estClosingCal, Calendar lastInvalCal, String timeZone,
				String firstInv, String estClosing, String lastInval, long skinId) throws SQLException {
		// This method is no longer used.
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Inserting a new one toch opportunity: " + ls + opportunity.toString());
		}

		Connection con = getConnection();
		try {
			OpportunitiesDAO.insertOneTouchOpportunity(con, opportunity, firstInvCal, estClosingCal,
					lastInvalCal, timeZone, firstInv, estClosing, lastInval, skinId );
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * return upDown field for one touch opportunity, for one touch investment.
	 * @param oppId
	 * @return
	 * @throws SQLException
	 */
	public static int getOneTouchUpDown(long oppId) throws SQLException {
        int upDown = 0;

        Connection conn = null;
        try {
            conn = getConnection();
            upDown = InvestmentsDAOBase.getOneTouchFields(conn, oppId ).getUpDown();

        } finally {
            closeConnection(conn);
        }
        return upDown;
    }

	/**
	 * update upDown field for one touch opportunity.
	 * @param oppId
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateOneTouchOpportunity(long oppId, long upDown) throws SQLException {

        Connection conn = null;
        boolean res = false;

        if (logger.isEnabledFor(Level.DEBUG)) {
			logger.log(Level.DEBUG, "Update Opportunity One Touch, id: "+ oppId + ", set upDown: "+upDown);
		}

        try {
            conn = getConnection();
            OpportunitiesDAO.updateOneTouchOpportunity(conn, oppId, upDown);
            if (logger.isEnabledFor(Level.DEBUG)) {
    			logger.log(Level.DEBUG, "Update Opportunity One Touch, id: "+ oppId + " succeed! ");
    		}
            res = true;

        }catch (Exception e) {
        	if (logger.isEnabledFor(Level.DEBUG)) {
    			logger.log(Level.ERROR, "Update Opportunity One Touch, id: "+ oppId + " failed! ");
    		}
        	res = false;
		}

        finally {
            closeConnection(conn);
        }

        return res;
    }
	/**
	 * Calls the DAO method to perform current level update in the Opportunity table for OneTouch Opportunity
	 * 
	 * @param oppId
	 * @param currentLevel
	 * @return 
	 * @throws SQLException
	 */
	public static boolean updateCurrentLevelOneTouchOpportunity(long oppId, double currentLevel) throws SQLException {

	    Connection conn = null;
	    boolean result = false;

	    if (logger.isEnabledFor(Level.DEBUG)) {
		logger.log(Level.DEBUG, "Trying to Update One Touch Opportunity with id: "+ oppId + " and trying to set current level to: " + currentLevel);
	    }

	    try {
		conn = getConnection();
		OpportunitiesDAO.updateCurrentLevelOfOneTouchOpportunity(conn, oppId, currentLevel);
		if (logger.isEnabledFor(Level.DEBUG)) {
		    logger.log(Level.DEBUG, "Update of One Touch Opportunity with id: "+ oppId + " succeeded! ");
		}
		result = true;

	    }catch (Exception e) {
		if (logger.isEnabledFor(Level.DEBUG)) {
		    logger.log(Level.ERROR, "Update of One Touch Opportunity with id: "+ oppId + " has failed! ");
		}
		result = false;
	    }

	    finally {
		closeConnection(conn);
	    }

	    return result;
	}

	public static ArrayList<Opportunity> getOpportunitiesByTemplate(OpportunityTemplate template, int days, boolean allOppsInMarket, boolean published) throws SQLException {
		Connection conn = null;
		ArrayList<Opportunity> list = null;

        if (logger.isEnabledFor(Level.DEBUG)) {
			logger.log(Level.DEBUG, "retrive getOpportunitiesByTemplate start");
		}

		try {
			conn = getConnection();
			list = OpportunitiesDAO.getOpportunitiesByTemplate(conn, template, days, allOppsInMarket, published);
		}catch (SQLException e){
        	if (logger.isEnabledFor(Level.DEBUG)) {
        		logger.log(Level.ERROR, "retrive getOpportunitiesByTemplate failed! ");
        	}
		} finally {
			closeConnection(conn);
		}
		return list;
	}

	/**
	 * Get not published opp for trader tool
	 * @param from
	 * @param settled
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static ArrayList<OpportunityTotal> getTtOpportunities(Date from, String settled,
			long market, long scheduled, long groupId, boolean isZeroOneHundred) throws SQLException, ParseException {

		Connection con = getSecondConnection();
		try {
			return OpportunitiesDAO.getTtOpportunities(con, from, settled, market, scheduled, groupId, isZeroOneHundred);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get one touch opp for trader tool
	 * @param from
	 * @param settled
	 * @param market
	 * @param scheduled
	 * @param groupId
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static ArrayList<OpportunityTotal> getTtOneTouchOpportunities(Date from, String settled,
			long market, long groupId, long upDown) throws SQLException, ParseException {

		Connection con = getSecondConnection();
		try {
			return OpportunitiesDAO.getTtOneTouchOpportunities(con, from, settled, market, groupId, upDown);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get one touch opposite oppurtunity
	 * @param opportunity - the cuurent opportunity
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static Opportunity getOppositeDirectionOpportunity(Opportunity opportunity)throws SQLException {
		Connection con = getConnection();
		Opportunity oppositeOpportunity = null;
		try {
			oppositeOpportunity = OpportunitiesDAO.getOppositeDirectionOpportunity(con, opportunity);
		} finally {
			closeConnection(con);
		}
		return oppositeOpportunity;
	}


	public static List<StatsResult> getStatsAssetProfit(long userId, Date from, Date to, boolean allTypes) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsAssetProfit(con, userId, from, to, allTypes);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsAssetSuccess(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsAssetSuccess(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsAssetGroupProfit(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsAssetGroupProfit(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsAssetGroupSuccess(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsAssetGroupSuccess(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsTimeProfit(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsTimeProfit(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsTimeSuccess(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsTimeSuccess(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStats1TouchProfit(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStats1TouchProfit(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStats1TouchSuccess(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStats1TouchSuccess(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static List<StatsResult> getStatsDayAndProfit(long userId) throws SQLException {
		Connection con = getConnection();
		List<StatsResult> list = null;
		try {
			list = InvestmentsDAO.getStatsDayAndProfit(con, userId);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static long getSumInvestmentsByDateRangeAndUserId(long userId, Date sumOfInvestmentsDateFrom, Date sumOfInvestmentsDateTo) throws Exception {
		Connection con = getConnection();
		try {
			return InvestmentsDAO.getSumInvestmentsByDateRangeAndUserId(con, userId, sumOfInvestmentsDateFrom, sumOfInvestmentsDateTo);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<String> getMarketAndSumOfInvestments(long userId) throws Exception {
		Connection con = getConnection();
		try {
			return InvestmentsDAO.getMarketAndSumOfInvestments(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SalesTurnoversTotals> getSalesTurnovers(Date from, Date to, long writerId, long skinId, long currencyId, long salesTypeDepId) throws Exception {
		Connection con = getSecondConnection();
		try {
			return InvestmentsDAO.getSalesTurnovers(con, from, to, writerId, skinId, currencyId, salesTypeDepId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SalesTurnoversDetails> getSalesTurnoverDetails(Date fromDetails, Date toDetails, long currencyIdDetails, long writerIdDetails, long skinIdDetails, long salesTypeDepIdDetails) throws SQLException {
		Connection con = getConnection();
		try {
			return InvestmentsDAO.getSalesTurnoverDetails(con, fromDetails, toDetails, currencyIdDetails, writerIdDetails, skinIdDetails, salesTypeDepIdDetails);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<OpportunityOddsPair> getOpportunityOddsPair() throws SQLException {
	    Connection con = getConnection();
		try {
			return OpportunitiesDAO.getOpportunityOddsPair(con);
		} finally {
			closeConnection(con);
		}
	}  
	
	public static Boolean isExistOpportunityOddsPair(OpportunityOddsPair op) throws SQLException {
	    Connection con = getConnection();
		try {
			return OpportunitiesDAO.isExistOpportunityOddsPair(con, op);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Boolean isExistOddsGoupsInOddsType(String oddsGroupsPair, long oddsTypeId) throws SQLException {
		String oddsGroupTmp=oddsGroupsPair.replace(" ", ",");
		String oddsGroup=oddsGroupTmp.substring(0,oddsGroupTmp.length() - 1);
	    Connection con = getConnection();
		try {
			return OpportunitiesDAO.isExistOddsGoupsInOddsType(con, oddsGroup, oddsTypeId);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean insertOpportunityOddsPair(OpportunityOddsPair op) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert Opportunity Odds Pair: " + ls + op.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
				// insert
			OpportunitiesDAO.insertOpportunityOddsPair(con, op);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert Opportunity Odds Pair ok." + ls);
			}
		} finally {
			closeConnection(con);
		}
		return true;
	}
	
	public static void updatetOpportunityOddsGroupDefaultValue(long id, long defValue, long writerId) throws SQLException {
	    Connection con = getConnection();
		try {
			OpportunitiesDAO.updatetOpportunityOddsGroupDefaultValue(con, id, defValue, writerId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<OpportunityOddsGroup> getOpportunityOddsGroup() throws SQLException {
	    Connection con = getConnection();
		try {
			return OpportunitiesDAO.getOpportunityOddsGroup(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static long getDefValueByOddsGroup(String oddsGroup) throws SQLException{
		 Connection con = getConnection();
			try {
				return OpportunitiesDAO.getDefValueByOddsGroup(con, oddsGroup);
			} finally {
				closeConnection(con);
			}
	}
	
	public static boolean updateInsertOpportunityOddsGroup( OpportunityOddsGroup og, String group, long writerId, long defValue) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert/Update  OpportunityOddsGroup: " + ls + og.toString());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			if (og.getOddsGroupID() == 0) {
				// insert
				OpportunitiesDAO.insertOpportunityOddsGroup(con, og,group, writerId);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				OpportunitiesDAO.updatetOpportunityOddsGroup(con, og,group, writerId, defValue);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert/Update  OpportunityOddsGroup: finished ok. " + ls);
			}

		} finally {
			closeConnection(con);
		}
		return true;
	}
	
	public static String getOpportunityPairsGroupPct(String oddsGroup) throws SQLException {
	    Connection con = getConnection();
		try {
			return OpportunitiesDAO.getOpportunityPairsGroupPct(con, oddsGroup);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<OpportunityOddsPairTemplate> getOpportunityOddsPairTemplate(String oddsGroupsPair) throws SQLException {
			String oddsGroupTmp=oddsGroupsPair.replace(" ", ",");
			String oddsGroup=oddsGroupTmp.substring(0,oddsGroupTmp.length()-1);
		    Connection con = getConnection();
			try {
				return OpportunitiesDAO.getOpportunityOddsPairTemplate(con, oddsGroup);
			} finally {
				closeConnection(con);
			}
	}

	public static String getOpportunityTypeName(Investment i) {
		String oppName = "";
		if (isTypeBinary0100Above(i)) {
			oppName = Opportunity.TYPE_BINARY_0_100_ABOVE_S;
		} else if (isTypeBinary0100Below(i)) {
			oppName = Opportunity.TYPE_BINARY_0_100_BELOW_S;
		}
		return oppName;
	}

	public static long getBaseProfit(Investment i) {
		return i.getBaseAmount() - getBaseRefund(i);
	}

	public static long getBaseWinLose(Investment i) {
		return (-1) * (i.getBaseWin() - i.getBaseLose());
	}
}