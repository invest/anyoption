package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

import il.co.etrader.util.CommonUtil;

public class DepositsSummary implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long typeId;
	private String depositType;
	private String ccName;
	private String ccLast4Num;
	private long count;
	private Date lastDeposit;
	private long amount;
	private long currency;
	
	
	public long getTypeId() {
		return typeId;
	}
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	public String getDepositType() {
		return depositType;
	}
	public void setDepositType(String depositType) {
		this.depositType = depositType;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	public String getCcLast4Num() {
		return ccLast4Num;
	}
	public void setCcLast4Num(String ccLast4Num) {
		this.ccLast4Num = ccLast4Num;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
		
	public Date getLastDeposit() {
		return lastDeposit;
	}
	public void setLastDeposit(Date lastDeposit) {
		this.lastDeposit = lastDeposit;
	}
	
	public String getLastDepositTxt() {
		return CommonUtil.getDateFormat(lastDeposit);
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getCurrency() {
		return currency;
	}
	public void setCurrency(long currency) {
		this.currency = currency;
	}

}
