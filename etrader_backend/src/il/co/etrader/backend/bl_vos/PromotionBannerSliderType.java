package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class PromotionBannerSliderType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private long  id;
	private String linkType;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PromotionBannerSliderType: "
            + super.toString()
            + "id: " + id + ls
            + "linkType:" + linkType + ls;
    }
}
