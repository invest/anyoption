package il.co.etrader.backend.bl_vos;

import com.anyoption.common.beans.base.Skin;
public class AssetIndexMarket extends com.anyoption.common.beans.AssetIndexMarket implements java.io.Serializable {

	private static final long serialVersionUID = -3531713893236096649L;
	
	private Skin skin;

	public Skin getSkin() {
		return skin;
	}

	public void setSkin(Skin skin) {
		this.skin = skin;
	}
}
