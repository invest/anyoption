package il.co.etrader.backend.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.util.Date;

public class UserRegulationHistory implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6122991550886203416L;
	private long id;
	private long userId;
	private Date timeCreated;
	private String step;
	private String comments;
	private long writerId;
	private String trigerEvent;
	private String writerUserName;
	private String utcOffsetCreated;
	
	public UserRegulationHistory(){
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public String getTrigerEvent() {
		return trigerEvent;
	}
	public void setTrigerEvent(String trigerEvent) {
		this.trigerEvent = trigerEvent;
	}

	public String getWriterUserName() {
		return writerUserName;
	}

	public void setWriterUserName(String writerUserName) {
		this.writerUserName = writerUserName;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}
	
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
	}
}
