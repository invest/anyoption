package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.util.CommonUtil;

import java.io.Serializable;

public class ClearingRoute extends com.anyoption.common.bl_vos.ClearingRoute implements Serializable {

	private static final long serialVersionUID = 9094663238563490995L;
	private String depositProviderName;
	private String withdrawProviderName;
	private String CFTProviderName;
	private String businessSkinName;
	private String currencyName;
	private String creditCardName; //CC type name
	private String countryName;
	private String skinName;
	
	@Override
	public String toString() {
		return "ClearingRoute [depositProviderName=" + depositProviderName
				+ ", withdrawProviderName=" + withdrawProviderName
				+ ", CFTProviderName=" + CFTProviderName
				+ ", businessSkinName=" + businessSkinName + ", currencyName="
				+ currencyName + ", creditCardName=" + creditCardName
				+ ", countryName=" + countryName + ", skinName=" + skinName
				+ "]" + super.toString();
	}
	/**
	 * @return the depositProviderName
	 */
	public String getDepositProviderName() {
		return depositProviderName;
	}
	/**
	 * @param depositProviderName the depositProviderName to set
	 */
	public void setDepositProviderName(String depositProviderName) {
		this.depositProviderName = depositProviderName;
	}
	/**
	 * @return the withdrawProviderName
	 */
	public String getWithdrawProviderName() {
		return withdrawProviderName;
	}
	/**
	 * @param withdrawProviderName the withdrawProviderName to set
	 */
	public void setWithdrawProviderName(String withdrawProviderName) {
		this.withdrawProviderName = withdrawProviderName;
	}
	/**
	 * @return the cFTProviderName
	 */
	public String getCFTProviderName() {
		return CFTProviderName;
	}
	/**
	 * @param cFTProviderName the cFTProviderName to set
	 */
	public void setCFTProviderName(String cFTProviderName) {
		CFTProviderName = cFTProviderName;
	}
	/**
	 * @return the businessSkinName
	 */
	public String getBusinessSkinName() {
		return businessSkinName;
	}
	/**
	 * @param businessSkinName the businessSkinName to set
	 */
	public void setBusinessSkinName(String businessSkinName) {
		this.businessSkinName = businessSkinName;
	}

	/**
	 * @return the creditCardName
	 */
	public String getCreditCardName() {
		return creditCardName;
	}
	/**
	 * @param creditCardName the creditCardName to set
	 */
	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}
	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}
	/**
	 * @return the currencyName
	 */
	public String getCurrencyName() {
		return currencyName;
	}
	/**
	 * @param currencyName the currencyName to set
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	
	/**
	 * @param businessSkinId
	 * @return
	 */
	public String getBusinessSkinNameById(int businessSkinId) {
		return CommonUtil.getMessage(ApplicationData.skinBusinessCases.get(businessSkinId), null);	
	}
	
	/**
	 * @param providerId
	 * @return
	 */
	public String getProviderNameById(int providerId) {
		return ApplicationData.clearingProviders.get(providerId);
	}
}
