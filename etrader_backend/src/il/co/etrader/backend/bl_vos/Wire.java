package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.mbeans.DepositFormBase;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.WireBase;
import il.co.etrader.util.CommonUtil;

import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;

/**
 * Wire VO.
 */
public class Wire extends WireBase {
	/**
	 * 
	 */
	private static final long serialVersionUID = 848814841837689202L;

	private static final Logger logger = Logger.getLogger(WireBase.class);

	private boolean sendReceipt;

    private boolean eftDeposit;
    
    private DepositFormBase depositForm;

	public Wire() throws SQLException {
		depositForm = new DepositFormBase();
		sendReceipt = true;
        eftDeposit = false;
	}


	public String initWireValues() {
		
		id=0;
		bankId="";
		branch="";
		accountNum="";
		accountName="";
		sendReceipt=true;
		return Constants.NAV_WIRE;
	}

	public String insert() throws SQLException{
		//	 prevent identical requests from being processed
        if (depositForm.saveTokenIsInvalid() ) {
            return depositForm.logInvalidSaveAndReturn(logger, null);
        }
        depositForm.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		depositForm.updateUserFieldsDepositForm(user);
		
		
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}
		
        Transaction t;
		if (eftDeposit){
            t = TransactionsManager.insertEFTDeposit(this,user.getId());
        } else {
                t = TransactionsManager.insertWireDeposit(this,user.getId());
            }

		depositForm.reloadIsFirstDepositForm();
		
		
		if (t == null) {
			return null;
		}

		if (sendReceipt) {
			String[] params = new String[1];
			params[0] = String.valueOf(t.getId());

			String receiptNum = t.getReceiptNumTxt();
			if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
				receiptNum = String.valueOf(t.getId());
			}
			UsersManager.sendReceiptEmail(CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()),t.getTimeCreatedTxt(),
					CommonUtil.getMessage("receipt.footer.bank", params, user.getLocale()), receiptNum);
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		return Constants.NAV_EMPTY_SUBTABS;

	}

	public boolean isSendReceipt() {
		return sendReceipt;
	}

	public void setSendReceipt(boolean sendReceipt) {
		this.sendReceipt = sendReceipt;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Wire ( "
	        + super.toString() + TAB
	        + "sendReceipt = " + this.sendReceipt + TAB
	        + " )";

	    return retValue;
	}


    /**
     * @return the eftDeposit
     */
    public boolean isEftDeposit() {
        return eftDeposit;
    }


    /**
     * @param eftDeposit the eftDeposit to set
     */
    public void setEftDeposit(boolean eftDeposit) {
        this.eftDeposit = eftDeposit;
    }
    
    public DepositFormBase getDepositForm() {
		return depositForm;
	}

}