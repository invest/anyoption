package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.Date;

public class SalesTurnoversDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	private String writerName;
	private long skinId;
	private long userId;
	private long sumOfDeposits;
	private long sumOfTurnovers;
	private String campaignId;
	private long currencyId;
	private String populationName;
	private String userRankName;
	private String userStatusName;
	private String depositAmount;
	private long sumOfWithdrawals; // Sum of withdrawals
	private String withdrawDepPercent; // sum withdrawals divided by sum deposits

	
	
	//for details by date screen
	private long tranId;
	private String date;
	private String tranAmount;
	private String baseTranAmount;
	private Date timeCreated;
	
	
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the sumOfDeposits
	 */
	public long getSumOfDeposits() {
		return sumOfDeposits;
	}
	/**
	 * @param sumOfDeposits the sumOfDeposits to set
	 */
	public void setSumOfDeposits(long sumOfDeposits) {
		this.sumOfDeposits = sumOfDeposits;
	}
	/**
	 * @return the sumOfTurnovers
	 */
	public long getSumOfTurnovers() {
		return sumOfTurnovers;
	}
	/**
	 * @param sumOfTurnovers the sumOfTurnovers to set
	 */
	public void setSumOfTurnovers(long sumOfTurnovers) {
		this.sumOfTurnovers = sumOfTurnovers;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the populationName
	 */
	public String getPopulationName() {
		return populationName;
	}
	/**
	 * @param populationName the populationName to set
	 */
	public void setPopulationName(String populationName) {
		this.populationName = populationName;
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	public String getSkinTxt() {
		return ApplicationData.getSkinById(skinId).getName();
	}
	
	public String getCurrencyTxt() {
		return ApplicationData.getCurrencyById(currencyId).getNameKey();//getDisplayName();
	}
	
	public String getSumOfDepositsTxt() {
		if(currencyId != 0) {
			return CommonUtil.displayAmount(sumOfDeposits, currencyId);
		}
		return CommonUtil.displayAmount(sumOfDeposits, ConstantsBase.CURRENCY_BASE_ID);
	}
	
	public String getSumOfTurnoversTxt() {
		if(currencyId != 0) {
			return CommonUtil.displayAmount(sumOfTurnovers, currencyId);
		}
		return CommonUtil.displayAmount(sumOfTurnovers, ConstantsBase.CURRENCY_BASE_ID);
	}
	
	public String getSumOfWithdrawalsTxt() {
		if(currencyId != 0) {
			return CommonUtil.displayAmount(sumOfWithdrawals, currencyId);
		}
		return CommonUtil.displayAmount(sumOfWithdrawals, ConstantsBase.CURRENCY_BASE_ID);
	}
	/**
	 * @return the userRankName
	 */
	public String getUserRankName() {
		return CommonUtil.getMessage(userRankName, null);
	}
	/**
	 * @param userRankName the userRankName to set
	 */
	public void setUserRankName(String userRankName) {
		this.userRankName = userRankName;
	}
	/**
	 * @return the userStatusName
	 */
	public String getUserStatusName() {
		return CommonUtil.getMessage(userStatusName, null);
	}
	/**
	 * @param userStatusName the userStatusName to set
	 */
	public void setUserStatusName(String userStatusName) {
		this.userStatusName = userStatusName;
	}
	public String getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	public long getTranId() {
		return tranId;
	}
	public void setTranId(long tranId) {
		this.tranId = tranId;
	}
	public String getTranAmount() {
		return tranAmount;
	}
	public void setTranAmount(String tranAmount) {
		this.tranAmount = tranAmount;
	}
	public String getBaseTranAmount() {
		return baseTranAmount;
	}
	public void setBaseTranAmount(String baseTranAmount) {
		this.baseTranAmount = baseTranAmount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the sumOfWithdrawals
	 */
	public long getSumOfWithdrawals() {
		return sumOfWithdrawals;
	}
	/**
	 * @param sumOfWithdrawals the sumOfWithdrawals to set
	 */
	public void setSumOfWithdrawals(long sumOfWithdrawals) {
		this.sumOfWithdrawals = sumOfWithdrawals;
	}
	
	
	/**
	 * @return the withdrawDepPercent
	 */
	public String getWithdrawDepPercent() {
		return withdrawDepPercent;
	}
	/**
	 * @param withdrawDepPercent the withdrawDepPercent to set
	 */
	public void setWithdrawDepPercent(String withdrawDepPercent) {
		this.withdrawDepPercent = withdrawDepPercent;
	}

}
