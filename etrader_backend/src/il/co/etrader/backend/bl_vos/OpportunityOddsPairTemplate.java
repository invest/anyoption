package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class OpportunityOddsPairTemplate implements Serializable {
	private long selectorID;
	private boolean isSelected;
	private long returnSelector;
	private long refundSelector;
		
	public OpportunityOddsPairTemplate (){
	}
	
	public long getSelectorID() {
		return selectorID;
	}
	public void setSelectorID(long selectorID) {
		this.selectorID = selectorID;
	}
	
	public boolean getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	public long getReturnSelector() {
		return returnSelector;
	}
	public void setReturnSelector(long returnSelector) {
		this.returnSelector = returnSelector;
	}
	
	public long getRefundSelector() {
		return refundSelector;
	}
	public void setRefundSelector(long refundSelector) {
		this.refundSelector = refundSelector;
	}
	
}
