package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.util.DynamicsUtil;

/**
 * Assign OpportunityQuotes0100 table
 *
 * @author Eyal
 */
public class OpportunityQuotes0100 implements Serializable {
	private static final long serialVersionUID = -7252289266955658494L;

	private long id;
	private Long opportunityId;
	private long marketId;
	private Date timeCreated;
	private long screenType;
	private long writerId;
	private MarketDynamicsQuoteParams quoteParams;
	private String comment;

	//for BE screen
	private Date timeEstClosing;
	private String writerName;

	public OpportunityQuotes0100() {
	}

	public OpportunityQuotes0100(Long opportunityId, long marketId, long screenType, long writerId, MarketDynamicsQuoteParams quoteParams, String comment) {
		this.opportunityId = opportunityId;
		this.marketId = marketId;
		this.screenType = screenType;
		this.writerId = writerId;
		this.quoteParams = quoteParams;
		this.comment = comment;
	}

	public MarketDynamicsQuoteParams getQuoteParams() {
		return quoteParams;
	}

	public void setQuoteParams(MarketDynamicsQuoteParams quoteParams) {
		this.quoteParams = quoteParams;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Long getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}

	public long getScreenType() {
		return screenType;
	}

	public void setScreenType(long screenType) {
		this.screenType = screenType;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public Date getTimeEstClosing() {
		return timeEstClosing;
	}

	public void setTimeEstClosing(Date timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	public String getMarketName() {
		return CommonUtil.getMarketName(marketId);
	}

	public String getTimeEstClosingTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}

	public String getTimeCreateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}

	public String getScreenTypeTxt() {
		String screenName = CommonUtil.getMessage("screen.name.datika", null);
		if (screenType == Constants.SCREEN_TYPE_TT) {
			screenName = CommonUtil.getMessage("screen.name.tt", null);
		}
		return screenName;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getParameters() {
		if (CommonUtil.isParameterEmptyOrNull(comment)) {
			return quoteParams.toString();
		}
		String oldJSON = comment.substring(comment.indexOf("{", 2)); // the comment should be 2 JSON object concatenated. The second { should be the first char of the 2nd JSON
		MarketDynamicsQuoteParams old = DynamicsUtil.parseOpportunityQuoteParams(oldJSON);
		return quoteParams.toString() + " Changed: " + quoteParams.getDifferences(old);
	}
}