package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.util.ConstantsBase;

/**
 * @author EranL
 *
 */
public class ApiIssue implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long writer_id;
	private String utcOffset;
	private long apiExternalUserId;
	private long apiIssueStatusId;
	private long apiIssueChannelId;
	private long apiIssueSubjectId;
	private String comments;
	private Date timeCreated;
	
	private String writerName;
	private String issueStatusName;
	private String issueChannelName;
	private String issueSubjectName;	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the writer_id
	 */
	public long getWriter_id() {
		return writer_id;
	}
	/**
	 * @param writer_id the writer_id to set
	 */
	public void setWriter_id(long writer_id) {
		this.writer_id = writer_id;
	}
	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}
	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}
	/**
	 * @return the apiExternalUserId
	 */
	public long getApiExternalUserId() {
		return apiExternalUserId;
	}
	/**
	 * @param apiExternalUserId the apiExternalUserId to set
	 */
	public void setApiExternalUserId(long apiExternalUserId) {
		this.apiExternalUserId = apiExternalUserId;
	}
	/**
	 * @return the apiIssueStatusId
	 */
	public long getApiIssueStatusId() {
		return apiIssueStatusId;
	}
	/**
	 * @param apiIssueStatusId the apiIssueStatusId to set
	 */
	public void setApiIssueStatusId(long apiIssueStatusId) {
		this.apiIssueStatusId = apiIssueStatusId;
	}
	/**
	 * @return the apiIssueChannelId
	 */
	public long getApiIssueChannelId() {
		return apiIssueChannelId;
	}
	/**
	 * @param apiIssueChannelId the apiIssueChannelId to set
	 */
	public void setApiIssueChannelId(long apiIssueChannelId) {
		this.apiIssueChannelId = apiIssueChannelId;
	}
	/**
	 * @return the apiIssueSubjectId
	 */
	public long getApiIssueSubjectId() {
		return apiIssueSubjectId;
	}
	/**
	 * @param apiIssueSubjectId the apiIssueSubjectId to set
	 */
	public void setApiIssueSubjectId(long apiIssueSubjectId) {
		this.apiIssueSubjectId = apiIssueSubjectId;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	/**
	 * @return the issueStatusName
	 */
	public String getIssueStatusName() {
		return issueStatusName;
	}
	/**
	 * @param issueStatusName the issueStatusName to set
	 */
	public void setIssueStatusName(String issueStatusName) {
		this.issueStatusName = issueStatusName;
	}
	/**
	 * @return the issueChannelName
	 */
	public String getIssueChannelName() {
		return issueChannelName;
	}
	/**
	 * @param issueChannelName the issueChannelName to set
	 */
	public void setIssueChannelName(String issueChannelName) {
		this.issueChannelName = issueChannelName;
	}
	/**
	 * @return the issueSubjectName
	 */
	public String getIssueSubjectName() {
		return issueSubjectName;
	}
	/**
	 * @param issueSubjectName the issueSubjectName to set
	 */
	public void setIssueSubjectName(String issueSubjectName) {
		this.issueSubjectName = issueSubjectName;
	}
	
	/**
	 * Return short comments for summary issues
	 * @return
	 */
	public String getCommentsLittleThenMax() {
		String tempComments = comments;
		if ( tempComments == null ) {
			tempComments = "";
		}
		else if ( tempComments.length() == 0 ) {
			tempComments = "";
		}
		else if ( tempComments.length() > ConstantsBase.MAX_COMMENTS)  {
			tempComments = tempComments.substring(0, ConstantsBase.MAX_COMMENTS - 1 ) + "...";
		}
		return tempComments;
	}
	
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";
	    String retValue = "";
	    retValue = "API Issue ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "writer_id = " + this.writer_id + TAB
	        + "utcOffset = " + this.utcOffset + TAB
	        + "apiExternalUserId = " + this.apiExternalUserId + TAB
	        + "apiIssueStatusId = " + this.apiIssueStatusId + TAB
	        + "apiIssueChannelId = " + this.apiIssueChannelId + TAB
	        + "apiIssueSubjectId = " + this.apiIssueSubjectId + TAB
	        + "comments = " + this.comments + TAB
	        + " )";
	    return retValue;
	}		
}
