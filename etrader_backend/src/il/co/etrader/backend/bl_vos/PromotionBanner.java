package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class PromotionBanner implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private String description;
	private int width;
	private int height;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
    public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PromotionBanner: "
            + super.toString()
            + "id: " + id + ls
            + "name:" + name + ls
            + "description: " + description + ls
            + "width: " + width + ls
            + "height: " + height + ls;
    }

}
