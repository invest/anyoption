package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EranL
 *
 */
public class StaticLandingPageLanguage implements Serializable {
	private static final long serialVersionUID = -6923580562243314918L;

	private long id;
	private String name;
	private String type;
	private String language;
	private long skinId;
	private long languageId;
	private long writerId;
	private Date timeCreated;
	private long staticLandingPageId;
	private long typeId;
	private StaticLandingPagePath staticLandingPagePath;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the staticLandingPageId
	 */
	public long getStaticLandingPageId() {
		return staticLandingPageId;
	}

	/**
	 * @param staticLandingPageId the staticLandingPageId to set
	 */
	public void setStaticLandingPageId(long staticLandingPageId) {
		this.staticLandingPageId = staticLandingPageId;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the staticLandingPagePath
	 */
	public StaticLandingPagePath getStaticLandingPagePath() {
		return staticLandingPagePath;
	}

	/**
	 * @param staticLandingPagePath the staticLandingPagePath to set
	 */
	public void setStaticLandingPagePath(StaticLandingPagePath staticLandingPagePath) {
		this.staticLandingPagePath = staticLandingPagePath;
	}
	
}