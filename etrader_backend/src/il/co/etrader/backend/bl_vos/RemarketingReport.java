package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class RemarketingReport implements Serializable  {

	private static final long serialVersionUID = 1L;
 
	private long firstComb;
    private String firstDP;
    private String firstSource	;
    private long remarketingComb;
    private String remarketingDP;
    private String remarketingSource; 
    private long numOfFTD ;
	
	public long getFirstComb() {
		return firstComb;
	}
	public void setFirstComb(long firstComb) {
		this.firstComb = firstComb;
	}
	public String getFirstDP() {
		return firstDP;
	}
	public void setFirstDP(String firstDP) {
		this.firstDP = firstDP;
	}
	public String getFirstSource() {
		return firstSource;
	}
	public void setFirstSource(String firstSource) {
		this.firstSource = firstSource;
	}
	public long getRemarketingComb() {
		return remarketingComb;
	}
	public void setRemarketingComb(long remarketingComb) {
		this.remarketingComb = remarketingComb;
	}
	public String getRemarketingDP() {
		return remarketingDP;
	}
	public void setRemarketingDP(String remarketingDP) {
		this.remarketingDP = remarketingDP;
	}
	public String getRemarketingSource() {
		return remarketingSource;
	}
	public void setRemarketingSource(String remarketingSource) {
		this.remarketingSource = remarketingSource;
	}
	public long getNumOfFTD() {
		return numOfFTD;
	}
	public void setNumOfFTD(long numOfFTD) {
		this.numOfFTD = numOfFTD;
	}
	
}
