package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EranL
 *
 */
public class StaticLandingPage implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private String type;
	private long writerId;
	private Date timeCreated;
	private long typeId;
	private long staticLandingPagePathId;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the staticLandingPagePathId
	 */
	public long getStaticLandingPagePathId() {
		return staticLandingPagePathId;
	}

	/**
	 * @param staticLandingPagePathId the staticLandingPagePathId to set
	 */
	public void setStaticLandingPagePathId(long staticLandingPagePathId) {
		this.staticLandingPagePathId = staticLandingPagePathId;
	}
	
}
