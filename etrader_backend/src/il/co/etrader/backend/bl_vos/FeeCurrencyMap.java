package il.co.etrader.backend.bl_vos;

import java.util.Date;

public class FeeCurrencyMap {
	
	private long id;	
	private long feeId;	
	private long currencyId;	
	private long amount;
	private long displayAmount;
	private Date timeCreated;	
	private long writerId;
	private String currencyCode;
	private String feeName;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getFeeId() {
		return feeId;
	}
	public void setFeeId(long feeId) {
		this.feeId = feeId;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public long getDisplayAmount() {
		return getAmount()/100;
	}
	public void setDisplayAmount(long displayAmount) {
		setAmount(displayAmount * 100); 
		this.displayAmount = displayAmount;
	}
}
