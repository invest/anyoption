package il.co.etrader.backend.bl_vos;

import il.co.etrader.bl_vos.MarketingCombination;

import java.io.Serializable;
import java.util.ArrayList;

public class MarketingCombinationSeris extends MarketingCombination implements Serializable {
    private static final long serialVersionUID = 1L;

    private long seriesNum;
    private ArrayList<Long> seriesPx;
    
	public MarketingCombinationSeris(MarketingCombination mc, long seriesNum, ArrayList<Long> seriesPx) {
		
		super();
		this.id = mc.getId();
		this.skinId = mc.getSkinId();
		this.campaignId = mc.getCampaignId();
		this.mediumId = mc.getMediumId();
		this.contentId = mc.getContentId();
		this.sizeId = mc.getSizeId();
		this.typeId = mc.getTypeId();
		this.locationId = mc.getLocationId();
		this.landingPageId = mc.getLandingPageId();
		this.writerId = mc.getWriterId();
		this.timeCreated = mc.getTimeCreated();
		this.pixelId = mc.getPixelId();
		this.sourceId = mc.getSourceId();
		this.paymentRecipientId = mc.getPaymentRecipientId();
		this.skinName = mc.getSkinName();
		this.campaignName = mc.getCampaignName();
		this.sourceName = mc.getSourceName();
		this.mediumName = mc.getMediumName();
		this.contentName = mc.getContentName();
		this.typeName = mc.getTypeName();
		this.location = mc.getLocation();
		this.landingPageName = mc.getLandingPageName();
		this.landingPageUrl = mc.getLandingPageUrl();
		this.verticalSize = mc.getVerticalSize();
		this.horizontalSizel = mc.getHorizontalSizel();
		this.combPixels = mc.getCombPixels();
		this.paymentType = mc.getPaymentType();
		this.url = mc.getUrl();
		this.cdnUrl = mc.getCdnUrl();
		this.etsUrl = mc.getEtsUrl();
		this.etsCdnUrl = mc.getEtsCdnUrl();
		this.pixelsSi = mc.getPixelsSi();
		this.campaignPriority = mc.getCampaignPriority();
		this.campaignDesc = mc.getCampaignDesc();
		this.landingPageType = mc.getLandingPageType();
		this.urlSourceTypeId = mc.getUrlSourceTypeId();
		this.dynamicParam = mc.getDynamicParam();
		this.seriesNum = seriesNum;
		this.seriesPx = seriesPx;
		this.marketingDomain.setId(mc.getMarketingDomain().getId());
		this.marketingDomain.setDomain(mc.getMarketingDomain().getDomain());
		this.pathId = mc.getPathId();
	}

	public long getSeriesNum() {
		return seriesNum;
	}

	public void setSeriesNum(long seriesNum) {
		this.seriesNum = seriesNum;
	}

	public ArrayList<Long> getSeriesPx() {
		return seriesPx;
	}

	public void setSeriesPx(ArrayList<Long> seriesPx) {
		this.seriesPx = seriesPx;
	}
}