package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class RewardUserTierDistribution implements Serializable {

	private static final long serialVersionUID = 4290589088562119398L;
	
	private int tierId;
	private int numUsers;
	
	/**
	 * @return the tierId
	 */
	public int getTierId() {
		return tierId;
	}
	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(int tierId) {
		this.tierId = tierId;
	}
	/**
	 * @return the numUsers
	 */
	public int getNumUsers() {
		return numUsers;
	}
	/**
	 * @param numUsers the numUsers to set
	 */
	public void setNumUsers(int numUsers) {
		this.numUsers = numUsers;
	}
	
}
