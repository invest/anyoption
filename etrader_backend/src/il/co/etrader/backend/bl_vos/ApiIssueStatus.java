package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiIssueStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
