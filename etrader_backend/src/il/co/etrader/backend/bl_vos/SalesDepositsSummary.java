package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.annotations.MethodInfoAnnotations;

public class SalesDepositsSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long repId;
	private long userId;
	private long depositAmount;
	private long depositAmountUSD;
	private long withdrawAmount;
	private long withdrawAmountUSD;
	private long depositSumAmountPerRep;
	private long withdrawSumAmountPerRep;
	private long sumAllDeposits;
	private long sumAllWithdraws;
	private long transactionTime;
	private String userRank;
	private String userStatus;
	private String repUserName;
	private long writerId;
	private long totalCount;
	
	public long getRepId() {
		return repId;
	}
	
	public void setRepId(long repId) {
		this.repId = repId;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getDepositAmount() {
		return depositAmount;
	}
	
	public void setDepositAmount(long depositAmount) {
		this.depositAmount = depositAmount;
	}
	
	public long getWithdrawAmount() {
		return withdrawAmount;
	}
	
	public void setWithdrawAmount(long withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}
	
	@MethodInfoAnnotations(type=MethodInfoAnnotations.TYPE_DATE)
	public long getTransactionTime() {
		return transactionTime;
	}
	
	public void setTransactionTime(long transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	@MethodInfoAnnotations(isMsgKey=true)
	public String getUserRank() {
		return userRank;
	}
	
	public void setUserRank(String userRank) {
		this.userRank = userRank;
	}
	
	@MethodInfoAnnotations(isMsgKey=true)
	public String getUserStatus() {
		return userStatus;
	}
	
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	
	public String getRepUserName() {
		return repUserName;
	}
	
	public void setRepUserName(String repUserName) {
		this.repUserName = repUserName;
	}
	
	public long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	@MethodInfoAnnotations(formatType=MethodInfoAnnotations.FORMAT_TYPE_FROM_USD_SMALL_AMOUNT)
	public long getDepositAmountUSD() {
		return depositAmountUSD;
	}
	
	public void setDepositAmountUSD(long depositAmountUSD) {
		this.depositAmountUSD = depositAmountUSD;
	}
	
	@MethodInfoAnnotations(formatType=MethodInfoAnnotations.FORMAT_TYPE_FROM_USD_SMALL_AMOUNT)
	public long getWithdrawAmountUSD() {
		return withdrawAmountUSD;
	}
	
	public void setWithdrawAmountUSD(long withdrawAmountUSD) {
		this.withdrawAmountUSD = withdrawAmountUSD;
	}
	
	public long getDepositSumAmountPerRep() {
		return depositSumAmountPerRep;
	}
	
	public void setDepositSumAmountPerRep(long depositSumAmountPerRep) {
		this.depositSumAmountPerRep = depositSumAmountPerRep;
	}
	
	public long getWithdrawSumAmountPerRep() {
		return withdrawSumAmountPerRep;
	}
	
	public void setWithdrawSumAmountPerRep(long withdrawSumAmountPerRep) {
		this.withdrawSumAmountPerRep = withdrawSumAmountPerRep;
	}

	public long getSumAllDeposits() {
		return sumAllDeposits;
	}

	public void setSumAllDeposits(long sumAllDeposits) {
		this.sumAllDeposits = sumAllDeposits;
	}

	public long getSumAllWithdraws() {
		return sumAllWithdraws;
	}

	public void setSumAllWithdraws(long sumAllWithdraws) {
		this.sumAllWithdraws = sumAllWithdraws;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
