package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.beans.SpecialCare;

public class SalesDepositsDetails implements Serializable {

	private static final long serialVersionUID = 2660860262265526646L;

	private String writerName;
	private String skin;
	private long userId;
	//private boolean isSpecialCare;
	private String depositAmount;
	private Date depositTime;
	private Date actionTime;
	private Date tranQualTime;
	private String turnoverLeft;
	private String currency;
	private String populationName;
	private long transactionId;
	private int salesDepositType; // retention or conversion
	private long capmaignId;
	private String capmaignName;
	private Date regTime;
	private Date popQualTime;
    protected SpecialCare specialCare;

	/**
	 * @return the capmaignId
	 */
	public long getCapmaignId() {
		return capmaignId;
	}

	/**
	 * @param capmaignId the capmaignId to set
	 */
	public void setCapmaignId(long capmaignId) {
		this.capmaignId = capmaignId;
	}

	/**
	 * @return the capmaignName
	 */
	public String getCapmaignName() {
		return capmaignName;
	}

	/**
	 * @param capmaignName the capmaignName to set
	 */
	public void setCapmaignName(String capmaignName) {
		this.capmaignName = capmaignName;
	}

	/**
	 * @return the regTime
	 */
	public Date getRegTime() {
		return regTime;
	}

	/**
	 * @param regTime the regTime to set
	 */
	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}

	public String getDepositTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(depositTime, Utils.getWriter().getUtcOffset());
	}

	public String getActionTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(actionTime, Utils.getWriter().getUtcOffset());
	}

	public String getQualificationTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(tranQualTime, Utils.getWriter().getUtcOffset());
	}

	public String getPopQualTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(popQualTime, Utils.getWriter().getUtcOffset());
	}

	public boolean isTrxQualified(){
		return tranQualTime != null;
	}

	public String getSalesDepositTypeTxt(){
		if (ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION == salesDepositType){
			return CommonUtil.getMessage("sales.deposit.sales.deposits.conversion",null);
		}else if (ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION == salesDepositType){
			return CommonUtil.getMessage("sales.deposit.sales.deposits.retention",null);
		}
		return ConstantsBase.EMPTY_STRING;
	}

	/**
	 * @return the actionTime
	 */
	public Date getActionTime() {
		return actionTime;
	}
	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return CommonUtil.getMessage(currency, null);
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}
	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	/**
	 * @return the depositTime
	 */
	public Date getDepositTime() {
		return depositTime;
	}
	/**
	 * @param depositTime the depositTime to set
	 */
	public void setDepositTime(Date depositTime) {
		this.depositTime = depositTime;
	}

	/**
	 * @return the salesDepositType
	 */
	public int getSalesDepositType() {
		return salesDepositType;
	}

	/**
	 * @param salesDepositType the salesDepositType to set
	 */
	public void setSalesDepositType(int salesDepositType) {
		this.salesDepositType = salesDepositType;
	}

	/**
	 * @return the populationName
	 */
	public String getPopulationName() {
		return populationName;
	}
	/**
	 * @param populationName the populationName to set
	 */
	public void setPopulationName(String populationName) {
		this.populationName = populationName;
	}
	/**
	 * @return the tranQualTime
	 */
	public Date getTranQualTime() {
		return tranQualTime;
	}
	/**
	 * @param tranQualTime the tranQualTime to set
	 */
	public void setTranQualTime(Date qualificationTime) {
		this.tranQualTime = qualificationTime;
	}
	/**
	 * @return the skin
	 */
	public String getSkin() {
		return CommonUtil.getMessage(skin, null);
	}
	/**
	 * @param skin the skin to set
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}
	/**
	 * @return the turnoverLeft
	 */
	public String getTurnoverLeft() {
		return turnoverLeft;
	}
	/**
	 * @param turnoverLeft the turnoverLeft to set
	 */
	public void setTurnoverLeft(String turnoverLeft) {
		this.turnoverLeft = turnoverLeft;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the popQualTime
	 */
	public Date getPopQualTime() {
		return popQualTime;
	}

	/**
	 * @param popQualTime the popQualTime to set
	 */
	public void setPopQualTime(Date popQualTime) {
		this.popQualTime = popQualTime;
	}

//	/**
//	 * @return the isSpecialCare
//	 */
//	public boolean isSpecialCare() {
//		return isSpecialCare;
//	}
//
//	/**
//	 * @param isSpecialCare the isSpecialCare to set
//	 */
//	public void setSpecialCare(boolean isSpecialCare) {
//		this.isSpecialCare = isSpecialCare;
//	}

    /**
     * @return the specialCare
     */
    public SpecialCare getSpecialCare() {
        return specialCare;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCare(SpecialCare specialCare) {
        this.specialCare = specialCare;
    }


}
