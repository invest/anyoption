package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class SalesDepositsTotals implements Serializable {

	private static final long serialVersionUID = 6826132530947353008L;

	private String date;
	private Date realDate;

	// Conversion
	private String conTranMadeQualCount;
	private String conTranMadeQualSum;
	private String conTranMadeQualAvg;
	private String conTranMadeCount;
	private String conTranMadeSum;
	private String conTranMadeAvg;
	private String conTranQualCount;
	private String conTranQualSum;
	private String conTranQualAvg;

	// Retention
	private String retTranMadeCount;
	private String retTranMadeSum;
	private String retTranMadeAvg;
	private String retTranTurnoverSum;
	private String retTranMadeQualCount;
	private String retTranMadeQualSum;
	private String retTranMadeQualAvg;
	private String retTranQualCount;
	private String retTranQualSum;
	private String retTranQualAvg;

	private String writerName;
	private long writerId;
	private int rowType;

	public static final int ROW_TYPE_TOTAL = 0;
	public static final int ROW_TYPE_EMPTY = 1;
	public static final int ROW_TYPE_NEW = 2;

	private String conTranMadeCountMonth;
	private String conTranMadeSumMonth;
	private String conTranMadeAvgMonth;
	private String conDailyStrike;
	private String conMonthlyStrike;

	private long repNumb;

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the conTranMadeCount
	 */
	public String getConTranMadeCount() {
		return conTranMadeCount;
	}
	/**
	 * @param conTranMadeCount the conTranMadeCount to set
	 */
	public void setConTranMadeCount(String conTranMadeCount) {
		this.conTranMadeCount = conTranMadeCount;
	}
	/**
	 * @return the conTranMadeSum
	 */
	public String getConTranMadeSum() {
		return conTranMadeSum;
	}
	/**
	 * @param conTranMadeSum the conTranMadeSum to set
	 */
	public void setConTranMadeSum(String conTranMadeSum) {
		this.conTranMadeSum = conTranMadeSum;
	}
	/**
	 * @return the conTranQualCount
	 */
	public String getConTranQualCount() {
		return conTranQualCount;
	}
	/**
	 * @param conTranQualCount the conTranQualCount to set
	 */
	public void setConTranQualCount(String conTranQualCount) {
		this.conTranQualCount = conTranQualCount;
	}
	/**
	 * @return the conTranQualSum
	 */
	public String getConTranQualSum() {
		return conTranQualSum;
	}
	/**
	 * @param conTranQualSum the conTranQualSum to set
	 */
	public void setConTranQualSum(String conTranQualSum) {
		this.conTranQualSum = conTranQualSum;
	}
	/**
	 * @return the retTranMadeCount
	 */
	public String getRetTranMadeCount() {
		return retTranMadeCount;
	}
	/**
	 * @param retTranMadeCount the retTranMadeCount to set
	 */
	public void setRetTranMadeCount(String retTranMadeCount) {
		this.retTranMadeCount = retTranMadeCount;
	}
	/**
	 * @return the retTranMadeSum
	 */
	public String getRetTranMadeSum() {
		return retTranMadeSum;
	}
	/**
	 * @param retTranMadeSum the retTranMadeSum to set
	 */
	public void setRetTranMadeSum(String retTranMadeSum) {
		this.retTranMadeSum = retTranMadeSum;
	}
	/**
	 * @return the retTranQualCount
	 */
	public String getRetTranQualCount() {
		return retTranQualCount;
	}
	/**
	 * @param retTranQualCount the retTranQualCount to set
	 */
	public void setRetTranQualCount(String retTranQualCount) {
		this.retTranQualCount = retTranQualCount;
	}
	/**
	 * @return the retTranQualSum
	 */
	public String getRetTranQualSum() {
		return retTranQualSum;
	}
	/**
	 * @param retTranQualSum the retTranQualSum to set
	 */
	public void setRetTranQualSum(String retTranQualSum) {
		this.retTranQualSum = retTranQualSum;
	}

	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}

	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the rowType
	 */
	public int getRowType() {
		return rowType;
	}

	/**
	 * @param rowType the rowType to set
	 */
	public void setRowType(int rowType) {
		this.rowType = rowType;
	}
	/**
	 * @return the conDailyStrike
	 */
	public String getConDailyStrike() {
		return conDailyStrike;
	}
	/**
	 * @param conDailyStrike the conDailyStrike to set
	 */
	public void setConDailyStrike(String conDailyStrike) {
		this.conDailyStrike = conDailyStrike;
	}
	/**
	 * @return the conMonthlyStrike
	 */
	public String getConMonthlyStrike() {
		return conMonthlyStrike;
	}
	/**
	 * @param conMonthlyStrike the conMonthlyStrike to set
	 */
	public void setConMonthlyStrike(String conMonthlyStrike) {
		this.conMonthlyStrike = conMonthlyStrike;
	}
	/**
	 * @return the conTranMadeCountMonth
	 */
	public String getConTranMadeCountMonth() {
		return conTranMadeCountMonth;
	}
	/**
	 * @param conTranMadeCountMonth the conTranMadeCountMonth to set
	 */
	public void setConTranMadeCountMonth(String conTranMadeCountMonth) {
		this.conTranMadeCountMonth = conTranMadeCountMonth;
	}
	/**
	 * @return the conTranMadeSumMonth
	 */
	public String getConTranMadeSumMonth() {
		return conTranMadeSumMonth;
	}
	/**
	 * @param conTranMadeSumMonth the conTranMadeSumMonth to set
	 */
	public void setConTranMadeSumMonth(String conTranMadeSumMonth) {
		this.conTranMadeSumMonth = conTranMadeSumMonth;
	}
	/**
	 * @return the repNum
	 */
	public long getRepNumb() {
		return repNumb;
	}
	/**
	 * @param repNum the repNum to set
	 */
	public void setRepNumb(long repNumb) {
		this.repNumb = repNumb;
	}
	/**
	 * @return the conTranMadeQualCount
	 */
	public String getConTranMadeQualCount() {
		return conTranMadeQualCount;
	}
	/**
	 * @param conTranMadeQualCount the conTranMadeQualCount to set
	 */
	public void setConTranMadeQualCount(String conTranMadeQualCount) {
		this.conTranMadeQualCount = conTranMadeQualCount;
	}
	/**
	 * @return the conTranMadeQualSum
	 */
	public String getConTranMadeQualSum() {
		return conTranMadeQualSum;
	}
	/**
	 * @param conTranMadeQualSum the conTranMadeQualSum to set
	 */
	public void setConTranMadeQualSum(String conTranMadeQualSum) {
		this.conTranMadeQualSum = conTranMadeQualSum;
	}
	/**
	 * @return the retTranMadeQualCount
	 */
	public String getRetTranMadeQualCount() {
		return retTranMadeQualCount;
	}
	/**
	 * @param retTranMadeQualCount the retTranMadeQualCount to set
	 */
	public void setRetTranMadeQualCount(String retTranMadeQualCount) {
		this.retTranMadeQualCount = retTranMadeQualCount;
	}
	/**
	 * @return the retTranMadeQualSum
	 */
	public String getRetTranMadeQualSum() {
		return retTranMadeQualSum;
	}
	/**
	 * @param retTranMadeQualSum the retTranMadeQualSum to set
	 */
	public void setRetTranMadeQualSum(String retTranMadeQualSum) {
		this.retTranMadeQualSum = retTranMadeQualSum;
	}
	/**
	 * @return the conTranMadeAvg
	 */
	public String getConTranMadeAvg() {
		return conTranMadeAvg;
	}
	/**
	 * @param conTranMadeAvg the conTranMadeAvg to set
	 */
	public void setConTranMadeAvg(String conTranMadeAvg) {
		this.conTranMadeAvg = conTranMadeAvg;
	}
	/**
	 * @return the conTranMadeQualAvg
	 */
	public String getConTranMadeQualAvg() {
		return conTranMadeQualAvg;
	}
	/**
	 * @param conTranMadeQualAvg the conTranMadeQualAvg to set
	 */
	public void setConTranMadeQualAvg(String conTranMadeQualAvg) {
		this.conTranMadeQualAvg = conTranMadeQualAvg;
	}
	/**
	 * @return the conTranQualAvg
	 */
	public String getConTranQualAvg() {
		return conTranQualAvg;
	}
	/**
	 * @param conTranQualAvg the conTranQualAvg to set
	 */
	public void setConTranQualAvg(String conTranQualAvg) {
		this.conTranQualAvg = conTranQualAvg;
	}
	/**
	 * @return the retTranMadeAvg
	 */
	public String getRetTranMadeAvg() {
		return retTranMadeAvg;
	}
	/**
	 * @param retTranMadeAvg the retTranMadeAvg to set
	 */
	public void setRetTranMadeAvg(String retTranMadeAvg) {
		this.retTranMadeAvg = retTranMadeAvg;
	}
	/**
	 * @return the retTranMadeQualAvg
	 */
	public String getRetTranMadeQualAvg() {
		return retTranMadeQualAvg;
	}
	/**
	 * @param retTranMadeQualAvg the retTranMadeQualAvg to set
	 */
	public void setRetTranMadeQualAvg(String retTranMadeQualAvg) {
		this.retTranMadeQualAvg = retTranMadeQualAvg;
	}
	/**
	 * @return the retTranQualAvg
	 */
	public String getRetTranQualAvg() {
		return retTranQualAvg;
	}
	/**
	 * @param retTranQualAvg the retTranQualAvg to set
	 */
	public void setRetTranQualAvg(String retTranQualAvg) {
		this.retTranQualAvg = retTranQualAvg;
	}
	/**
	 * @return the conTranMadeAvgMonth
	 */
	public String getConTranMadeAvgMonth() {
		return conTranMadeAvgMonth;
	}
	/**
	 * @param conTranMadeAvgMonth the conTranMadeAvgMonth to set
	 */
	public void setConTranMadeAvgMonth(String conTranMadeAvgMonth) {
		this.conTranMadeAvgMonth = conTranMadeAvgMonth;
	}
	public String getRetTranTurnoverSum() {
		return retTranTurnoverSum;
	}
	public void setRetTranTurnoverSum(String retTranTurnoverSum) {
		this.retTranTurnoverSum = retTranTurnoverSum;
	}
	public Date getRealDate() {
		return realDate;
	}
	public void setRealDate(Date realDate) {
		this.realDate = realDate;
	}

}
