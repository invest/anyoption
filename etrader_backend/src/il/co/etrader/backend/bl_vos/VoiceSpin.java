package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class VoiceSpin implements Serializable {
	private static final long serialVersionUID = -2310869214107140187L;
	
	private long entryId;
	private long repId;
	private int reachedStatusId;
	private long issueActionTypeId;
	private boolean isGeneralValidInfo;
	private boolean isActionValidInfo;
	/**
	 * @return the entryId
	 */
	public long getEntryId() {
		return entryId;
	}
	/**
	 * @param entryId the entryId to set
	 */
	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}
	/**
	 * @return the repId
	 */
	public long getRepId() {
		return repId;
	}
	/**
	 * @param repId the repId to set
	 */
	public void setRepId(long repId) {
		this.repId = repId;
	}
	/**
	 * @return the reachedStatusId
	 */
	public int getReachedStatusId() {
		return reachedStatusId;
	}
	/**
	 * @param reachedStatusId the reachedStatusId to set
	 */
	public void setReachedStatusId(int reachedStatusId) {
		this.reachedStatusId = reachedStatusId;
	}
	/**
	 * @return the issueActionTypeId
	 */
	public long getIssueActionTypeId() {
		return issueActionTypeId;
	}
	/**
	 * @param issueActionTypeId the issueActionTypeId to set
	 */
	public void setIssueActionTypeId(long issueActionTypeId) {
		this.issueActionTypeId = issueActionTypeId;
	}
	public boolean isGeneralValidInfo() {
		return isGeneralValidInfo;
	}
	public void setGeneralValidInfo(boolean isGeneralValidInfo) {
		this.isGeneralValidInfo = isGeneralValidInfo;
	}
	public boolean isActionValidInfo() {
		return isActionValidInfo;
	}
	public void setActionValidInfo(boolean isActionValidInfo) {
		this.isActionValidInfo = isActionValidInfo;
	}
	@Override
	public String toString() {
		return "VoiceSpin [entryId=" + entryId + ", repId=" + repId
				+ ", reachedStatusId=" + reachedStatusId
				+ ", issueActionTypeId=" + issueActionTypeId
				+ ", isGeneralValidInfo=" + isGeneralValidInfo
				+ ", isActionValidInfo=" + isActionValidInfo + "]";
	}
	
	
	
}
