package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

import com.anyoption.common.beans.File;

/**
 * @author EranL
 *
 */
public class UserFiles implements Serializable{

	private static final long serialVersionUID = 1L;

	private File userIdFile;
	private String filesRiskStatus;
	private int filesRiskStatusId;
	private boolean needUpdateStatus;

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";
	    String retValue = "";

	    retValue = "CreditCard ( "
	        + super.toString() + TAB
	        + "file = " + this.userIdFile + TAB
	        + "filesRiskStatus = " + this.filesRiskStatus + TAB
	        + "needUpdateStatus = " + this.needUpdateStatus + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the filesRiskStatus
	 */
	public String getFilesRiskStatus() {
		return filesRiskStatus;
	}

	/**
	 * @param filesRiskStatus the filesRiskStatus to set
	 */
	public void setFilesRiskStatus(String filesRiskStatus) {
		this.filesRiskStatus = filesRiskStatus;
	}

	/**
	 * @return the filesRiskStatusId
	 */
	public int getFilesRiskStatusId() {
		return filesRiskStatusId;
	}

	/**
	 * @param filesRiskStatusId the filesRiskStatusId to set
	 */
	public void setFilesRiskStatusId(int filesRiskStatusId) {
		this.filesRiskStatusId = filesRiskStatusId;
	}

	/**
	 * @return the userIdFile
	 */
	public File getUserIdFile() {
		return userIdFile;
	}

	/**
	 * @param userIdFile the userIdFile to set
	 */
	public void setUserIdFile(File userIdFile) {
		this.userIdFile = userIdFile;
	}

	/**
	 * @return the needUpdateStatus
	 */
	public boolean isNeedUpdateStatus() {
		return needUpdateStatus;
	}

	/**
	 * @param needUpdateStatus the needUpdateStatus to set
	 */
	public void setNeedUpdateStatus(boolean needUpdateStatus) {
		this.needUpdateStatus = needUpdateStatus;
	}

}
