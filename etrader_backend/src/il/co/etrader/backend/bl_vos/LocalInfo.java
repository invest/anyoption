package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import javax.faces.context.FacesContext;

public class LocalInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4760133445355522684L;
	protected Locale locale;
	protected String language;
	protected String country;
	protected String align;
	protected String dir;
	protected String imageFolder;

	public LocalInfo(WriterWrapper writer) {

		ArrayList<Integer> languagesList;
//		if (writer.isEtrader()) {
//			locale = new Locale("iw");
//			language = "hebrew";
//			country = "IL";
//			align = "right";
//			dir="rtl";
//			imageFolder="he";
//		} else {
			locale = new Locale("en");
			language = "english";
			country = "US";
			align = "left";
			dir="ltr";
			imageFolder="en";
		//}
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getImageFolder() {
		return imageFolder;
	}

	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public String getLogoFolder() {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper writer = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		if(writer.isEtrader()) {
			return "he";
		}
		return "en";
	}

}
