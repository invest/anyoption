package il.co.etrader.backend.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.util.Date;

import com.anyoption.common.beans.ReutersQuotes;

public class ReutersQuotesBE extends ReutersQuotes {
	private static final long serialVersionUID = -6923580562243314918L;

	private String marketName;
	private long scheduled;
	private Date timeEstClosing;

	public ReutersQuotesBE() {

	}

	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	/**
	 * @return the scheduled
	 */
	public long getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the timeEstClosing
	 */
	public Date gettimeEstClosing() {
		return timeEstClosing;
	}

	/**
	 * @param timeEstClosing the timeEstClosing to set
	 */
	public void settimeEstClosing(Date timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	/**
	 *
	 * @return time text formatted.
	 */
	public String getTimeTxt() {
		return CommonUtil.getTimeFormat(this.getTime(), CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING), "dd/MM/yyyy HH:mm:ss:SSS");

	}

	/**
	 *
	 * @return time Estimation closing text formatted.
	 */
	public String getTimeEstClosingTxt() {
		return CommonUtil.getTimeFormat(timeEstClosing, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING), "dd/MM/yyyy HH:mm:ss:SSS" );
	}

	/**
	 *
	 * @return Scheduled text by number.
	 */
	public String getScheduledTxt() {
		return InvestmentsManagerBase.getScheduledTxt(scheduled);
	}
}
