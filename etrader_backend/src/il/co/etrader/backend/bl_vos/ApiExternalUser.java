package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ApiUser;

/**
 * @author EyalG
 *
 */
public class ApiExternalUser extends com.anyoption.common.beans.base.ApiExternalUser implements Serializable {

	protected static Logger log = Logger.getLogger(ApiExternalUser.class);

	private static final long serialVersionUID = 1L;
	private String ip;
	private boolean isRisky;
	private boolean isUserDisable;
	private String utcOffset;


	private String total;
	private String winLose;
	private String avgAmount;

	private String totalInvestments;
	private String numOfCancelInvestments;
	private UserBase user;
	private ApiUser apiUser;

	public void loadStrip() {
		try {
			this.user = UsersManager.getUserById(getUserId());
			FacesContext context = FacesContext.getCurrentInstance();
			User userSession = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//			userSession.setId(user.getId());
			userSession.setCurrencyId(user.getCurrencyId());
//			userSession.setUserName(user.getUserName());
//			userSession.setUtcOffset(user.getUtcOffset());

			String[] totals = InvestmentsManager.getInvestmentsTotals(Constants.NOT_EXISTS, getId());
//			String winLose = InvestmentsManager.getUsersWinLose(Constants.NOT_EXISTS, getId());
			this.totalInvestments = CommonUtil.displayAmount(totals[2], user.getCurrencyId().intValue());
			this.numOfCancelInvestments = CommonUtil.displayLong(InvestmentsManager.getNumOfCancelInvestments(Constants.NOT_EXISTS, getId()));
			this.total = totals[0];
			this.avgAmount = CommonUtil.displayAmount(totals[1], user.getCurrencyId().intValue());
			this.winLose = CommonUtil.displayAmount(user.getWinLoseHouse(), user.getCurrencyId().intValue());
		} catch (SQLException e) {
			log.fatal("cant fill external user strip", e);
		}

	}

	public boolean isLosing() {
		if (winLose == null) {
			return false;
		}

		return winLose.indexOf("-") > -1;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the isRisky
	 */
	public boolean isRisky() {
		return isRisky;
	}

	/**
	 * @param isRisky the isRisky to set
	 */
	public void setRisky(boolean isRisky) {
		this.isRisky = isRisky;
	}

	/**
	 * @return the apiUser
	 */
	public ApiUser getApiUser() {
		return apiUser;
	}

	/**
	 * @param apiUser the apiUser to set
	 */
	public void setApiUser(ApiUser apiUser) {
		this.apiUser = apiUser;
	}

	/**
	 * @return the avgAmount
	 */
	public String getAvgAmount() {
		return avgAmount;
	}

	/**
	 * @param avgAmount the avgAmount to set
	 */
	public void setAvgAmount(String avgAmount) {
		this.avgAmount = avgAmount;
	}

	/**
	 * @return the numOfCancelInvestments
	 */
	public String getNumOfCancelInvestments() {
		return numOfCancelInvestments;
	}

	/**
	 * @param numOfCancelInvestments the numOfCancelInvestments to set
	 */
	public void setNumOfCancelInvestments(String numOfCancelInvestments) {
		this.numOfCancelInvestments = numOfCancelInvestments;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return the totalInvestments
	 */
	public String getTotalInvestments() {
		return totalInvestments;
	}

	/**
	 * @param totalInvestments the totalInvestments to set
	 */
	public void setTotalInvestments(String totalInvestments) {
		this.totalInvestments = totalInvestments;
	}

	/**
	 * @return the winLose
	 */
	public String getWinLose() {
		return winLose;
	}

	/**
	 * @param winLose the winLose to set
	 */
	public void setWinLose(String winLose) {
		this.winLose = winLose;
	}

	/**
	 * @return the user
	 */
	public UserBase getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserBase user) {
		this.user = user;
	}

	/**
	 * @return the isUserDisable
	 */
	public boolean isUserDisable() {
		return isUserDisable;
	}

	/**
	 * @param isUserDisable the isUserDisable to set
	 */
	public void setUserDisable(boolean isUserDisable) {
		this.isUserDisable = isUserDisable;
	}

	/**
	 * clone the api external user (for change details)
	 * @param aeu api external user to clone
	 */
	public void copyUser(ApiExternalUser aeu) {
		this.setId(aeu.getId());
		this.setApiUserId(aeu.getApiUserId());
		this.setUserId(aeu.getUserId());
		this.setReference(aeu.getReference());
		this.isRisky = aeu.isRisky;
		this.ip = aeu.ip;
		this.isUserDisable = aeu.isUserDisable;
		this.utcOffset = aeu.utcOffset;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	public String getUtcDifference() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(ConstantsBase.BIND_WRITER_WRAPPER).getValue(context);
    	return Utils.getUtcDifference(w.getUtcOffset(), getUtcOffset());
	}
}
