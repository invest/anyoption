package il.co.etrader.backend.bl_vos;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketingAffiliatesManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.copyop.common.dto.Profile;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.TiersManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author IdanZ
 *
 */
public class User extends UserBase {

	private static final long serialVersionUID = 2140243955747592034L;
	private static final Logger logger = Logger.getLogger(User.class);
	private int supportTabIndex;
	private String tmpUserName;
	private String tmpEmail;
	private Long tmpUserId;
	private Long tmpContactId;

	private int menuButtonSelected=0;

	private String total;
	private String winLose;
	private String winLoseHalfYear;
	private String winLoseYear;
	private String avgAmount;
	private String avgDeposit;
	private String avgWithdraw;
	private String totalFees;
	private String avgNetDeposit;
	private double netDeposit;

	private String numOfDeposits;
	private String numOfAvgDeposits;
	private String numOfWithdraws;
	private String totalInvestments;
	private String numOfCancelInvestments;

	protected boolean isChargeBacks;
	protected boolean isFrauds;
	protected boolean bonusAbuser = false;
	protected boolean significant;
	protected int nextinvestonusReceveid;
	protected int nextinvestonusUsed;
	protected int bonusReceived;
	protected int bonusUsed;
	protected boolean haveBonus;
	protected boolean supportMOLocking;

	private Date lastSalesDepositDate;
	private String lastSalesDepositAmount;

	private String maxDepositAmount;
	private String maxDepositDate;
	private String loginsUserAgent;

	private String filesRiskStatus;
	private String deviceCampaign;

	// If it's a short registration form contact needs to disply on strip certain field which
	// are not relevant for other contacts.
	private boolean isShortRegFormContact;

	private int campaignPriority;
	private String campaignDesc;
	private String campaignRemarketing;
	private String affSiteLink;

	private boolean confirmContactDetails;
    private boolean specialCareManualy;
    private String retentionRepName;
    private String contentsName;
    private String sumBonuses;
    private boolean showContactSearch = false;
    private boolean loadUserStrip;
    private String conMobilePhone;
    private String conLandLinePhone;
    private String conLandLinePhonePrefix;
    private String conMobilePhonePrefix;
    private Long conContactId;
    private String conEmail;
    private String  conFirstName;
    private String conLastName;
    private String conTypeId;
    private boolean isPartnerMarketing = false;
    private static final String IS_REGULATED_YES = "userstrip.regulation.is.regulated.yes";
    private static final String IS_REGULATED_YES_ = "userstrip.regulation.is.regulated.yes._";
    private static final String IS_REGULATED_NO = "userstrip.regulation.is.regulated.no";
    public  boolean isRegulated;
    public  String isRegulatedTxt;
    public  String isRegulatedStateTxt;
    private  String controlApprovedDateTxt;
    private String timeSuspendedTxt;
    private String suspendedReasonTxt;
    private final static String SUSPENDED_REASON_2_TXT = "userstrip.suspended.reason.2" ;
    private final static String SUSPENDED_REASON_5_TXT = "userstrip.suspended.reason.5";
    private final static String SUSPENDED_REASON_3_C_TXT = "userstrip.suspended.reason.3.c";
    private final static String SUSPENDED_REASON_3_D_TXT  = "userstrip.suspended.reason.3.d";
    private final static String SUSPENDED_REASON_4_TXT = "userstrip.suspended.reason.4";
    private final static String SUSPENDED_REASON_6_TXT = "userstrip.suspended.reason.6";
    private final static String SUSPENDED_REASON_15_TXT = "userstrip.suspended.reason.15";
    private final static String SUSPENDED_REASON_3_SUFFIX = "- general";
    private int daysAfterFD;
    private String limitTxt;
    private Profile profile;
    private String regOrig;
    private int copyopState;
    private String userLPUrl;
    private boolean isPasswordReset;
    private String docStatus; 
    
    public User() throws SQLException{
		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;

		UsersManager.initValues(this);
		supportTabIndex = 0;
		taxExemption = false;
		setPasswordReset(Utils.getWriter().getWriter().isPasswordReset());
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		// For sales dep. when creating user from contact - need to pass the contact param to register page
		if (null != request && request.getRequestURL().indexOf("register.jsf") > -1) {
			User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
			if (user.isRetentionMSelected() || user.isRetentionSelected()) {
				this.firstName = user.getFirstName();
				this.lastName = user.getLastName();
				this.email = user.getEmail();
				this.mobilePhone = user.getMobilePhone();
				this.countryId = user.getCountryId();
				this.skinId = user.getSkinId();
				confirmContactDetails = true;
			}
		}
		 if( Utils.getWriter().getWriter().getGroupId() == 6 || Utils.getWriter().getWriter().getGroupId() == 3){
			 showContactSearch = true;
		 }
		userRegulation = new UserRegulationBase();
		loadUserStrip = true;
    }

	public boolean isSignificant() {
		return significant;
	}

	public void setSignificant(boolean significant) {
		this.significant = significant;
	}

	public User(boolean other) throws SQLException{
        if (logger.isTraceEnabled()) {
        	logger.trace("new backend user created! no init");
        }
		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;
    }

	public void updateClass(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		setClassId(new Long((String) ev.getNewValue()));
		context.renderResponse();
	}

    public String getTmpUserName() {
		return tmpUserName;
	}

	public void setTmpUserName(String tmpUserName) {
		this.tmpUserName = tmpUserName;
	}

	public String initValues() throws SQLException{

    	UsersManager.initValues(this);

    	return Constants.NAV_REGISTER;
    }

	public String refreshStrip() throws SQLException{

		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;

		if (id != 0){
			tmpUserId = id;
		}
		tmpUserName = userName;

		UsersManager.loadUserStripFields(this, false, null);
    	return Constants.NAV_SUPPORT_TABS;
	}

	public String loadNewUserFromStrip() throws SQLException, ParseException {
		Date arabicStartDate = null;
		long currentWriterId;
		// userRegulation = new UserRegulationBase();
		if (this.isPartnerSelected()) {
			menuButtonSelected = Constants.MENU_BUTTON_PARTNER;
			// For arabic partner writer we want users that created after the arabic partner starting date
			currentWriterId = Utils.getWriter().getWriter().getId();
			if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
				String enumDate = CommonUtil.getEnum(	Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
														Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				arabicStartDate = sdf.parse(enumDate);
			}
		} else {
			menuButtonSelected = Constants.MENU_BUTTON_SUPPORT;
		}
		if (tmpContactId != null) {
			this.contactId = tmpContactId;
		} else {
			this.contactId = 0;
		}
		loadUserStrip = true;
		boolean res = UsersManager.loadUserStripFields(this, false, arabicStartDate);
		supportMOLocking = false;

		if (!res) {
			cleanUserStrip();
			refreshStrip();
		} else if (!this.isPartnerSelected()) {
			// if(userRegulation != null && 0 != this.id) {
			// userRegulation.setUserId(this.id);
			// UserRegulationManager.getUserRegulationBackend(userRegulation);
			// }
			Utils.popupMessage("userstrip.askdetails");
			if (!isUnique()) {
				Utils.popupMessage("userstrip.more.than.one.user");
			}
		}
		if (this.isPartnerSelected()) {
			return null;
		}
		return Constants.NAV_SUPPORT_TABS;
	}

    public void cleanUserStrip() throws SQLException {


//		if (userName != null || id != 0){
//
//			if(tmpContactId!=null||tmpContactId !=0){
//
//				tmpUserName = null;
//        		tmpUserId = null;
//
//
//			}else{
//
//			tmpUserName = userName;
//    		tmpUserId = id;
//
//    	    }
//		}else {
//
//			tmpUserName = null;
//    		tmpUserId = null;
//		}

		tmpUserName = null;
        tmpUserId = null;
        tmpContactId = null;
        tmpEmail = null;
        conMobilePhone = null;
        conLandLinePhone = null;
        conLandLinePhonePrefix = null;
        conMobilePhonePrefix = null;
        conContactId = null;
        conEmail = null;
        conFirstName = null;
        conLastName = null;
        conTypeId = null;
        initForContact();
        this.firstName = null;
		this.lastName = null;
		this.email = null;
		this.mobilePhone = null;
		this.countryId = 0;
		this.skinId = 0;
		this.countryName=null;
		this.skin=null;
		this.contentsName=null;
		this.mobilePhone=null;
		this.landLinePhone=null;
		this.bonusAbuser=null != null;
		this.rankId=0;
		this.getRankTxt();
		confirmContactDetails = true;
		this.isRegulated = false;
		this.isRegulatedStateTxt = null;
		this.controlApprovedDateTxt = null;
		this.isRegulatedTxt = null;
		this.daysAfterFD = 0;
		this.suspendedReasonTxt = null;
	    this.timeSuspendedTxt = null;
	    userRegulation = null;
	    this.limitTxt = null;
	    this.lastRepChat = null;
	    profile = null;
	    this.regOrig = null;
	 }

    public String selectUser() throws SQLException {

    	menuButtonSelected = Constants.MENU_BUTTON_SUPPORT;

    	if (tmpContactId!=null){
        	this.contactId = tmpContactId;
        	loadUserStrip = false;
        	}else{

        		this.contactId=0;
        		loadUserStrip = true;
        	}
    	boolean res = UsersManager.loadUserStripFields(this,false,null);
    	supportMOLocking = false;

    	if (res) {
    		Utils.popupMessage("userstrip.askdetails");
    	} else {
    		cleanUserStrip();
    		refreshStrip();
    	}
    	tmpContactId = null;
    	return Constants.NAV_SUPPORT_TABS;

	}

    public String selectUserNoPopUp() throws SQLException{

    	menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;

    	UsersManager.loadUserStripFields(this, false, null);
    	supportMOLocking = false;

    	return Constants.NAV_SUPPORT_TABS;

	}

    public String selectUserIssues() throws SQLException, ParseException{
    	// clean up old tmpemail value
    	setTmpEmail(null);
    	loadNewUserFromStrip();

    	menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;
    	supportTabIndex = 4;
    	supportMOLocking = false;

    	return Constants.NAV_SUPPORT_TABS;

	}


    public String selectUserNoPopUpById() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	if (null != request && (request.getRequestURL().indexOf("riskUsersFiles") > -1)){//navigate to files menu
    		supportTabIndex = 5;
    	} else {
    		supportTabIndex = 0;
    	}
    	menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;

    	this.tmpUserId = id;
    	this.tmpUserName = UsersManager.getUserDetails(id).getUserName();

    	UsersManager.loadUserStripFields(this, false, null);
    	supportMOLocking = false;

    	return Constants.NAV_SUPPORT_TABS;

	}

    /**
     * Load population entry to session
     * @param userId
     * @param contactId
     * @return
     * @throws SQLException
     */
    public String selectPopulationEntry(long userId, long contactId) throws SQLException {
    	if(userId != 0) {
	    	this.tmpUserId = userId;
	    	this.tmpUserName = UsersManager.getUserDetails(userId).getUserName();
	    	UsersManager.loadUserStripFields(this, false, null);
    	} else {
    		initForContact();
    		this.contactId = contactId;
    		Contact c = ContactsManager.getContactByID(this.contactId);
    		if (c.getType() == Contact.CONTACT_US_SHORT_REG_FORM || c.getType() == Contact.CONTACT_US_REGISTER_ATTEMPTS || c.getType() == Contact.CONTACT_US_QUICK_START_ATTEMPTS){
    			this.firstName = c.getFirstName();
    			this.lastName = c.getLastName();
    			this.mobilePhone = c.getMobilePhone();
    			this.landLinePhone = c.getLandLinePhone();
    			this.isContactByEmail = c.isContactByEmail()?1:0;
    			this.isContactBySMS = c.isContactBySMS()?1:0;
    			this.isShortRegFormContact = true;
    		}else{
    			this.firstName = c.getText();
    	    	this.lastName = "";
    			this.mobilePhone = c.getPhone();
    	    	this.landLinePhone = "";
    			this.setContactByPhone(true);
    			this.setContactBySMS(true);
    			this.isShortRegFormContact = false;
    		}
			this.email = c.getEmail();
			long cSkinId = c.getSkinId();
			this.skinId = cSkinId;
			this.skin = SkinsManagerBase.getById(cSkinId);
			this.countryId = c.getCountryId();
			this.countryName = c.getCountryName();
			this.timeCreated = c.getTimeCreated();
			this.combinationId = c.getCombId();
			this.userLPUrl = null;
			this.dynamicParam = c.getDynamicParameter();
			this.utcOffset = c.getUtcOffset();
            this.ip = c.getIp();
            this.affSub2 = c.getAff_sub2();
            this.lastRepChat = il.co.etrader.backend.bl_managers.ContactsManager.getLastWriterIdChat(contactId);
            this.regOrig = "";
			if (countryId != 0){
				this.landLinePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
			}
			if (countryId != 0){
				this.mobilePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
			}
            MarketingCombination comb = MarketingManager.getCombinationId(this.getCombinationId());
            if (null != comb){
                this.setCampaignName(comb.getCampaignName());
                this.setCampaignForStrip(comb.getCampaignForStrip());
                this.setCampaignPriority(comb.getCampaignPriority());
                this.setCampaignDesc(comb.getCampaignDesc());
            }else{
                this.setCampaignName("Campign not found");
                this.setCampaignPriority(0);
                this.setCampaignDesc(ConstantsBase.EMPTY_STRING);
                logger.error("Error! comb for user "+ userId);
            }
    	}
    	if (menuButtonSelected == Constants.MENU_BUTTON_SUPPORT_MORE_OPTIONS ||
    			menuButtonSelected == Constants.MENU_BUTTON_SUPPORT) {
    		menuButtonSelected = Constants.MENU_BUTTON_SUPPORT;
    		this.supportMOLocking = true;
    		return Constants.NAV_SUPPORT_TABS;
    	} if (menuButtonSelected == Constants.MENU_BUTTON_RETENTION_MANAGER) {
    		this.supportMOLocking = false;
    		return Constants.NAV_RETENTION_MANAGER_TABS;
    	} else {
    		menuButtonSelected = Constants.MENU_BUTTON_RETENTION;
    		this.supportMOLocking = false;
    		return Constants.NAV_RETENTION_TABS;
    	}
    }

    /**
     * Load user by retention session instance
     * @return
     * @throws SQLException
     */
    public String getLoadPopulationEntryUser() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		PopulationEntry entry = (PopulationEntry) context.getApplication().evaluateExpressionGet(context, Constants.BIND_POPULATION_ENTRY, PopulationEntry.class);
		long userId = entry.getUserId();
		if (userId != 0) {
			if (id != userId) { // load user fields in case the userSession.id != entry.userId
		    	this.tmpUserId = userId;
		    	this.tmpUserName = UsersManager.getUserDetails(userId).getUserName();
		    	UsersManager.loadUserStripFields(this, false, null);
		    	if (menuButtonSelected == Constants.MENU_BUTTON_RETENTION_MANAGER) {
		    		return Constants.NAV_RETENTION_MANAGER_TABS;
		    	} else {
		    		return Constants.NAV_RETENTION_TABS;
		    	}
			}
		} else { // load contact
			initForContact();
			long contactId = entry.getContactId();
			if (this.contactId != contactId) {
				this.contactId = contactId;
				Contact c = ContactsManager.getContactByID(this.contactId);
				this.firstName = c.getText();
				this.mobilePhone = c.getPhone();
				this.email = c.getEmail();
				this.skinId = c.getSkinId();
				this.countryId = c.getCountryId();
				this.countryName = c.getCountryName();
				this.timeCreated = c.getTimeCreated();
				this.combinationId = c.getCombId();
				this.userLPUrl = null;
				this.dynamicParam = c.getDynamicParameter();
				this.utcOffset = c.getUtcOffset();
                this.ip = c.getIp();
				if (countryId != 0){
					this.landLinePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
				}
				if (countryId != 0){
					this.mobilePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
				}
				this.lastRepChat = il.co.etrader.backend.bl_managers.ContactsManager.getLastWriterIdChat(contactId);
				this.regOrig = "";
			}
		}
		supportMOLocking = false;
		return null;
    }

    public void initForContact() {
		this.tmpUserId = null;
    	this.tmpUserName = null;
    	this.userName = "";
    	this.id = 0;
    	this.balance = 0;
    	this.winLose = "";
    	this.isActive = "";
    	this.isOpenIssue = false;
    	this.birthDay = "";
    	this.languageName = null;
    	this.idNum = "";
    	this.isFalseAccount = false;
    	this.taxBalance = 0;
    	this.isFrauds = false;
    	this.significant = false;
		this.setIsOpenIssue(false);
		this.setChargeBacks(false);
		this.setBonusUsed(0);
		this.setBonusReceived(0);
		this.setDeposits(0);
		this.setNumOfDeposits("");
		this.setNumOfWithdraws("");
		this.setTotalInvestments("");
		this.setNumOfCancelInvestments("");
		this.setTotal("");
		this.setAvgAmount("");
		this.setWinLose("");
		this.setWinLoseHalfYear("");
		this.setWinLoseYear("");
		this.setAvgDeposit("");
		this.setAvgWithdraw("");
		this.setTotalFees("");
		this.setAvgNetDeposit("");
		this.lastDeclineDate = null;
		this.timeBirthDate = null;
		this.timeLastLogin = null;
		this.mobilePhonePrefix = "";
		this.landLinePhonePrefix = "";
		this.gender = null;
		this.timeCreated = null;
		this.taxExemption = false;
		this.pendingWithdrawals = 0;
		this.combinationId = 0;
		this.userLPUrl = null;
		this.bonusReceived = 0;
		this.bonusUsed = 0;
		this.isDecline = false;
		this.isContactForDaa = false;
		this.setCompanyName("");
		this.setChargeBacks(false);
		this.setHaveBonus(false);
		this.isFalseAccount = false;
		this.withdrawals = 0;
		this.maxDepositAmount = "";
		this.maxDepositDate = "";
		this.deviceUniqueId = null;
		this.loginsUserAgent = null;
		this.setSumBonuses("");

    }

    public int getSupportTabIndex() {
		return supportTabIndex;
	}
	public void setSupportTabIndex(int supportTabIndex) {
		this.supportTabIndex = supportTabIndex;
	}

	/**
	 * Insert new user
	 * @return
	 * @throws SQLException
	 */
    public String insertUser() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		setSkinId(Utils.getUser().getSkinId());
		setLimitId(UsersManager.getLimitId(skinId, currencyId));
	//	setCurrencyId(Utils.getUser().getCurrencyId());
    	if ( null == this.idNum ) {
    		this.idNum = "No-Id"; // for anyOption users
    	}
    	
    	//TODO refactor when insert copyop user from BE will be possible.
    	setPlatformId(Platform.ETRADER);
		if (!CommonUtil.isHebrewSkin(getSkinId())) { // for anyoption
			setMobilePhone(handleLeadingZero(getMobilePhone()));
			setLandLinePhone(handleLeadingZero(getLandLinePhone()));
			setPlatformId(Platform.ANYOPTION);
		}

		if (ApplicationData.getCountry(countryId).isRegulated()) {
			skinId = new Long(SkinsManagerBase.getRegulatedSkinIdByNonRegulatedSkinId(skinId));
		}

		// change user details.
        HashMap<Long, String> userDetailsBeforeChangedHM = null;
        HashMap<Long, String> userDetailsAfterChangedHM = null;
        try {
            userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(skinId, null, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, null, null, ConstantsBase.NON_SELECTED, null, null, null, null);
            userDetailsAfterChangedHM = UsersManager.getUserDetailsHM((long)this.getSkinId(), this.getStreet(), this.getCountryId(), this.getGender(), this.getEmail(), this.getMobilePhone(), 
                    this.getLandLinePhone(), this.getContactByEmail(), this.isContactBySMS(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear(), this.getCityName(), this.getIdNum(), this.getState(), 
                    CommonUtil.capitalizeFirstLetters(this.getFirstName()), CommonUtil.capitalizeFirstLetters(this.getLastName()), this.getPassword(), null, this.getLanguageId(), String.valueOf(this.getClassId()), CommonUtil.getBooleanAsString(this.isIdDocVerify()), this.isAuthorizedMail(), this.isRisky(), (int)ConstantsBase.NON_SELECTED, 
                    this.isStopReverseWithdrawOption(), this.isImmediateWithdraw(), false, false, this.getUserName(), false, UsersManagerBase.USER_DEFAULT_BUBBLES_PRICING_LEVEL);
        } catch (Exception e) {
            logger.error("Error while getting user details HM ", e);
        }
        ArrayList<UsersDetailsHistory> usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
    	boolean res = UsersManager.insertUser(this);

    	if (res==true) {
    		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.REGISTRAITION, id, 0, id, BonusManagerBase.class, (int)writerId);
    		
    		long currentWriterId = Utils.getWriter().getWriter().getId();
    		if (null != usersDetailsHistoryList) {
				UsersManagerBase.insertUsersDetailsHistory(currentWriterId, this.getId(), this.getUserName(), usersDetailsHistoryList, String.valueOf(this.getClassId()));
			}
    		tmpUserName=userName;
    		UsersManager.loadUserStripFields(this, false, null);
    		supportMOLocking = false;
    		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
    		ve.getValue(context.getELContext());
        	ve.setValue(context.getELContext(), this);

        	ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRY, PopulationEntry.class);
			PopulationEntry entry = (PopulationEntry) ve.getValue(context.getELContext());
			if (null != entry && entry.getContactId() == this.contactId && this.contactId != 0){
				entry.setUserId(this.id);
				ve.setValue(context.getELContext(), entry);

				if (isHasRetentionMRole()){
					menuButtonSelected = Constants.MENU_BUTTON_RETENTION_MANAGER;
				}else{
					menuButtonSelected = Constants.MENU_BUTTON_RETENTION;
				}

				return selectPopulationEntry(this.id, this.contactId);
			}
    		return Constants.NAV_SUPPORT_MAIN;
    	}
    	else
    		return null;
    }

    public String getUpdateRightMenu() throws SQLException {

    	FacesContext context=FacesContext.getCurrentInstance();
    	String contextpath=context.getExternalContext().getRequestContextPath();
    	if (context.getExternalContext().getInitParameter("CMS.show.staging").equals("1")) {
    		goCms();
    		return contextpath+"/jsp/cms/cms.jsf";
    	}
    	else {
	    	if (this.isHasSAdminRole()) {
	    		goSadmin();
	    		return contextpath+"/jsp/sadmin/sadmin.jsf";
	    	}
	    	if (this.isHasAdminRole()) {
	    		goAdmin();
	    		return contextpath+"/jsp/admin/admin.jsf";
	    	}
	    	if (this.isHasTraderRole()) {
	    		goTrader();
	    		return contextpath+"/jsp/trader/trader.jsf";
	    	}
	    	if (this.isHasAccountingRole()) {
	    		goAccounting();
	    		return contextpath+"/jsp/accounting/accounting.jsf";
	    	}
	    	if (this.isHasRetentionRole()) {
	    		goRetention();
	    		return contextpath+"/jsp/retention/retention.jsf";
	    	}
	    	if (this.isHasRetentionMRole()) {
	    		goRetentionM();
	    		return contextpath+"/jsp/retentionManager/retentionManager.jsf";
	    	}
	    	if (this.isHasPartnerRole()) {
	    		goPartner();
	    		return contextpath+"/jsp/partner/partner.jsf";
	    	}
	    	if (this.isHasTVRole()) {
	    		return contextpath+"/jsp/tv/tvMenu.jsf";
	    	}
	    	if (this.isHasControlRole()) {
	    		goControl();
	    		return contextpath+"/jsp/control/control.jsf";
	    	}
	    	if (this.isHasAnalyticsTVRole()) {
	    		return contextpath+"/jsp/analyticsTv/analytics.jsf";
	    	}

	    	// support
	    	goSupport();
	    	return contextpath+"/jsp/support/support_main.jsf";
    	}

   }

    public String getRowsByMenu() {

    	if (isSupportSelected()) {
    		return "10";
    	} else {
    		return "15";
    	}

    }

    public int getRowsByMenuInt() {

    	if (isSupportSelected()) {
    		return 10;
    	} else {
    		return 15;
    	}

    }

    /**
     * Get limits list
     * @return
     * @throws SQLException
     */
    public ArrayList getLimits() throws SQLException{
    	return AdminManager.getLimits();
    }

    /**
     * Get limits list by writer skins
     * @return
     * @throws SQLException
     */
    public ArrayList getLimitsBySkins() throws SQLException{
    	return AdminManager.getLimitsBySkins();
    }


	public String goAdmin() {

		menuButtonSelected=Constants.MENU_BUTTON_ADMIN;
		return Constants.NAV_ADMIN_TABS;
	}

	public String goAccounting() {

		menuButtonSelected=Constants.MENU_BUTTON_ACCOUNTING;
		return Constants.NAV_ACCOUNTING_TABS;
	}
	public String goTrader() {
		menuButtonSelected=Constants.MENU_BUTTON_TRADER;
		return Constants.NAV_TRADER_TABS;
	}

	public String goSadmin() {
		menuButtonSelected=Constants.MENU_BUTTON_SADMIN;
		return Constants.NAV_SADMIN_TABS;
	}

	public String goCms() {
		menuButtonSelected=Constants.MENU_BUTTON_CMS;
		return Constants.NAV_CMS_TABS;
	}

	public String goRetention() {
		menuButtonSelected=Constants.MENU_BUTTON_RETENTION;
		return Constants.NAV_RETENTION_TABS;
	}

	public String goRetentionM() {
		menuButtonSelected=Constants.MENU_BUTTON_RETENTION_MANAGER;
		return Constants.NAV_RETENTION_MANAGER_TABS;
	}

	public String goPartner() {
		menuButtonSelected=Constants.MENU_BUTTON_PARTNER;
		return Constants.NAV_PARTNER_TABS;
	}

	public String goSupportMoreOptions() {
		menuButtonSelected = Constants.MENU_BUTTON_SUPPORT_MORE_OPTIONS;
		return Constants.NAV_SUPPORT_MORE_TABS;
	}

	public String goMarketing() {
		menuButtonSelected = Constants.MENU_BUTTON_MARKETING;
		return Constants.NAV_MARKETING_TABS;
	}

	public String goProduct() {
		menuButtonSelected = Constants.MENU_BUTTON_PRODUCT;
		return Constants.NAV_PRODUCT_TABS;
	}

	public String goSupport() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		if (userName!=null && !userName.trim().equals(""))
			UsersManager.loadUserStripFields(this, false, null);
		else {
			FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.not.selected",null,Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

		}
		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;

		supportTabIndex=0;

		return Constants.NAV_SUPPORT_TABS;
	}

	public String goControl() {
		menuButtonSelected=Constants.MENU_BUTTON_CONTROL;
		return Constants.NAV_CONTROL_TABS;
	}

	public String getGoAdmin() {
		menuButtonSelected=Constants.MENU_BUTTON_ADMIN;
		return "";
	}

	public String getGoAccounting() {

		menuButtonSelected=Constants.MENU_BUTTON_ACCOUNTING;
		return "";
	}

	public String getGoTrader() {
		menuButtonSelected=Constants.MENU_BUTTON_TRADER;
		return "";
	}

	public String getGoSadmin() {
		menuButtonSelected=Constants.MENU_BUTTON_SADMIN;
		return "";
	}

	public String getGoCms() {
		menuButtonSelected=Constants.MENU_BUTTON_CMS;
		return "";
	}

	public String getGoRetention() {
		menuButtonSelected=Constants.MENU_BUTTON_RETENTION;
		return "";
	}

	public String getGoRetentionM() {
		menuButtonSelected=Constants.MENU_BUTTON_RETENTION_MANAGER;
		return "";
	}

	public String getGoPartner() {
		menuButtonSelected=Constants.MENU_BUTTON_PARTNER;
		return "";
	}

	public String getGoSupport() throws SQLException {
		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT;
		return "";
	}

	public String getGoMarketing() throws SQLException {
		menuButtonSelected = Constants.MENU_BUTTON_MARKETING;
		return "";
	}

	public String getGoProduct() throws SQLException {
		menuButtonSelected = Constants.MENU_BUTTON_PRODUCT;
		return "";
	}

	public String getGoControl() throws SQLException {
		menuButtonSelected = Constants.MENU_BUTTON_CONTROL;
		return "";
	}

	public String getSupportMoreOptions() throws SQLException {
		menuButtonSelected=Constants.MENU_BUTTON_SUPPORT_MORE_OPTIONS;
		return "";
	}

	public boolean isSupportSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_SUPPORT;
	}

	public boolean isSupportMoreOptionsSelected() {
		return menuButtonSelected == Constants.MENU_BUTTON_SUPPORT_MORE_OPTIONS;
	}

	public boolean isAdminSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_ADMIN;
	}

	public boolean isTraderSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_TRADER;
	}

	public boolean isAccountingSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_ACCOUNTING;
	}

	public boolean isSadminSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_SADMIN;
	}

	public boolean isCmsSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_CMS;
	}

	public boolean isRetentionSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_RETENTION;
	}

	public boolean isMarketingSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_MARKETING;
	}

	public boolean isRetentionMSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_RETENTION_MANAGER;
	}

	public boolean isProductSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_PRODUCT;
	}

	public boolean isPartnerMarketing() {
		isPartnerMarketing=true;
		return isPartnerMarketing;
	}

	public boolean isNotPartnerMarketing() {
		isPartnerMarketing=false;
		return isPartnerMarketing;
	}

	public boolean isPartnerMarketingSelected() {
		return isPartnerMarketing;
	}

	public boolean isPartnerSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_PARTNER;
	}

	public boolean isControlSelected() {
		return menuButtonSelected==Constants.MENU_BUTTON_CONTROL;
	}

	public String getAvgAmount() {
		return avgAmount;
	}

	public void setAvgAmount(String avgAmount) {
		this.avgAmount = avgAmount;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getWinLose() {
		return winLose;
	}
	public boolean isLosing() {
		if (winLose==null)
			return false;

		return winLose.indexOf("-")>-1;
	}
	
	public boolean isLosingHouse() {
		return winLoseHouse<0;
	}

	public boolean isLosingYear() {
		if (winLoseYear==null)
			return false;

		return winLoseYear.indexOf("-")>-1;
	}

	public boolean isLosingHalfYear() {
		if (winLoseHalfYear==null)
			return false;

		return winLoseHalfYear.indexOf("-")>-1;
	}

	public void setWinLose(String winLose) {
		this.winLose = winLose;
	}

	public String getAvgDeposit() {
		return avgDeposit;
	}

	public void setAvgDeposit(String avgDeposit) {
		this.avgDeposit = avgDeposit;
	}

	public String getAvgWithdraw() {
		return avgWithdraw;
	}

	public void setAvgWithdraw(String avgWithdraw) {
		this.avgWithdraw = avgWithdraw;
	}
	public boolean isChargeBacks() {
		return isChargeBacks;
	}
	public void setChargeBacks(boolean isChargeBacks) {
		this.isChargeBacks = isChargeBacks;
	}

	public boolean getContactBySMS() {
		return (isContactBySMS==1);
	}

	/**
	 * @param isContactBySMS the isContactBySMS to set
	 */
	public void setContactBySMS(boolean isContactBySMS) {
		this.isContactBySMS = (isContactBySMS) ? 1 : 0;
	}

	public String getIsChargeBacksTxt() {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if (isChargeBacks==true)
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
		else
			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	}

	public String getIsFraudsTxt() {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if (isFrauds==true)
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
		else
			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	}

	public String getSignificantTxt() {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if (significant) {
			return CommonUtil.getMessage("yes", null, Utils.getWriterLocale(context));
		} else {
			return CommonUtil.getMessage("no", null, Utils.getWriterLocale(context));
		}
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "User ( "
	        + super.toString() + TAB
	        + "supportTabIndex = " + this.supportTabIndex + TAB
	        + "tmpUserName = " + this.tmpUserName + TAB
	        + "total = " + this.total + TAB
	        + "winLose = " + this.winLose + TAB
	        + "winLoseHalfYear = " + this.winLoseYear + TAB
	        + "winLoseYear = " + this.winLoseHalfYear + TAB
	        + "avgAmount = " + this.avgAmount + TAB
	        + "avgDeposit = " + this.avgDeposit + TAB
	        + "avgWithdraw = " + this.avgWithdraw + TAB
	        + "isChargeBacks = " + this.isChargeBacks + TAB
	        + "isPasswordReset = " + this.isPasswordReset + TAB

	        + " )";

	    return retValue;
	}

	public boolean isHasSupportRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_SUPPORT));
	}

	public boolean isHasSSupportRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_SSUPPORT));
	}

	public boolean isHasAdminRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_ADMIN));
	}

	public boolean isHasTraderRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_TRADER));
	}

	public boolean isHasSTraderRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_STRADER));
	}

	public boolean isHasASTraderRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_ASTRADER));
	}

	public boolean isHasAccountingRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_ACCOUNTING));
	}

	public boolean isHasRetentionRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_RETENTION));
	}

	public boolean isHasSAdminRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_SADMIN));
	}

	public boolean isHasCmsRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_CMS));
	}

	public boolean isHasMarketingRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_MARKETING));
	}

	public boolean isHasRetentionMRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_RETENTIONM));
	}

	public boolean isHasPartnerRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_PARTNER));
	}

	public boolean isHasTVRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_TV));
	}

	public boolean isHasBTraderRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_BTRADER));
	}

	public boolean isHasControlRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_CONTROL));
	}

	public boolean isHasPartnerMarketingRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_PARTNER_MARKETING));
	}

	public boolean isHasProductRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_PRODUCT));
	}
	
	public boolean isHasUploadLPRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_UPLOAD_LP));
	}
	
	public boolean isHasCopyopDetailsRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_COPYOP_DETAILS));
	}
	
	public boolean isHasRegrantCopyopCoinsRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_REGRANT_COPYOP_COINS));
	}
	
	public boolean isHasRoutingRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_ROUTING));
	}
	
	public boolean isHasAnalyticsTVRole() {
		return (this.getRoles().contains(ConstantsBase.ROLE_ANALYTICS_TV));
	}
	
	public boolean isHasSalesMExt() {
		return (this.getRoles().contains(ConstantsBase.ROLE_SALESM_EXT));
	}

	public boolean isHasSalesDeposit() {
		return (this.getRoles().contains(ConstantsBase.ROLE_SALES_DEPOSIT));
	}	
	
	public boolean isHasLimitationDeposit() {
		return (this.getRoles().contains(ConstantsBase.ROLE_LIMITATION_DEPOSIT));
	}
	
	public boolean isHasMarketingBulkIssue() {
		return (this.getRoles().contains(ConstantsBase.ROLE_MARKETING_BULK_ISSUE));
	}
	
	public boolean isHasConversionType() {
		long currentWriterId = Utils.getWriter().getWriter().getId(); //Why long?
		boolean res = false;
		if (Utils.getWriter().getSalesReps().get(currentWriterId) != null) {
			res = (Utils.getWriter().getSalesReps().get(currentWriterId).getSales_type() == Constants.SALES_TYPE_CONVERSION);
		}
		return res;
	}
	
	public String getNumOfCancelInvestments() {
		return numOfCancelInvestments;
	}
	public void setNumOfCancelInvestments(String numOfCancelInvestments) {
		this.numOfCancelInvestments = numOfCancelInvestments;
	}
	public String getNumOfDeposits() {
		return numOfDeposits;
	}
	public void setNumOfDeposits(String numOfDeposits) {
		this.numOfDeposits = numOfDeposits;
	}
	public String getNumOfWithdraws() {
		return numOfWithdraws;
	}
	public void setNumOfWithdraws(String numOfWithdraws) {
		this.numOfWithdraws = numOfWithdraws;
	}
	public String getTotalInvestments() {
		return totalInvestments;
	}
	public void setTotalInvestments(String totalInvestments) {
		this.totalInvestments = totalInvestments;
	}

	public boolean isFrauds() {
		return isFrauds;
	}

	public void setFrauds(boolean isFrauds) {
		this.isFrauds = isFrauds;
	}

	/**
	 * @return the winLoseHalfYear
	 */
	public String getWinLoseHalfYear() {
		return winLoseHalfYear;
	}

	/**
	 * @param winLoseHalfYear the winLoseHalfYear to set
	 */
	public void setWinLoseHalfYear(String winLoseHalfYear) {
		this.winLoseHalfYear = winLoseHalfYear;
	}

	/**
	 * @return the winLoseYear
	 */
	public String getWinLoseYear() {
		return winLoseYear;
	}

	/**
	 * @param winLoseYear the winLoseYear to set
	 */
	public void setWinLoseYear(String winLoseYear) {
		this.winLoseYear = winLoseYear;
	}

	public void setNextinvestonusReceveid(int nextinvestonusReceveid) {
		this.nextinvestonusReceveid = nextinvestonusReceveid;
	}

	public void setNextinvestonusUsed(int nextinvestonusUsed) {
		this.nextinvestonusUsed = nextinvestonusUsed;
	}

	/**
	 * @return the bonusReceived
	 */
	public int getBonusReceived() {
		return bonusReceived;
	}

	/**
	 * @param bonusReceived the bonusReceived to set
	 */
	public void setBonusReceived(int bonusReceived) {
		this.bonusReceived = bonusReceived;
	}

	public void setLastSalesDepositDate(Date date) {
		lastSalesDepositDate = date;
	}

	public Date getLastSalesDepositDate() {
		return lastSalesDepositDate;
	}

	public String getLastSalesDepositDateTxt() {
		return CommonUtil.getDateFormat(lastSalesDepositDate);
	}

	public void setLastSalesDepositAmount(String amount) {
		lastSalesDepositAmount = amount;
	}

	public String getLastSalesDepositAmount() {
		return lastSalesDepositAmount;
	}

	public String getBonusTxt() {
		String received = CommonUtil.getMessage("userstrip.nextinvestonusreceived", null, Utils.getWriter().getLocalInfo().getLocale());
		String used = CommonUtil.getMessage("userstrip.nextinvestonusused", null, Utils.getWriter().getLocalInfo().getLocale());
		return received + " (" + bonusReceived + ")" + " / " + used + " (" + bonusUsed + ")";
	}

	public int getNextinvestonusReceveid() {
		return nextinvestonusReceveid;
	}

	public int getNextinvestonusUsed() {
		return nextinvestonusUsed;
	}

	public String getCurrencySymbol() {
		return CommonUtil.getMessage(CommonUtil.getCurrencySymbol(currencyId.intValue()), null, new Locale("iw"));
	}

	/**
	 * Get user locale by skin language
	 * @return
	 * 		Locale object
	 * @throws SQLException
	 */
	@Override
	public Locale getLocale() {
		long langId = skin.getDefaultLanguageId();
		String langCode = null;
		try {
			langCode = LanguagesManagerBase.getCodeById(langId);
			return new Locale(langCode);
		} catch (SQLException e) {
			logger.error("Can't get language code", e);
			throw new IllegalArgumentException(e);
		}
	}

	public String getCityName() {
		return cityName;
	}

	@Override
	public void setCityName(String name) {
		if (isEtrader() && null!=name) {
			try {
				cityId = AdminManagerBase.getCityIdByName(name);
			} catch (SQLException e) {
				logger.error("Can't get city id", e);
				throw new IllegalArgumentException(e);
			}
		}
		this.cityName = name;
	}

	/**
	 * @return the totalFees
	 */
	public String getTotalFees() {
		return totalFees;
	}

	/**
	 * @param totalFees the totalFees to set
	 */
	public void setTotalFees(String totalFees) {
		this.totalFees = totalFees;
	}
	
	public double getNetDeposit() {
		return netDeposit;
	}

	public void setNetDeposit(double netDeposit) {
		this.netDeposit = netDeposit;
	}
	
	public String getAvgNetDeposit() {
		return avgNetDeposit;
	}

	public void setAvgNetDeposit(String avgNetDeposit) {
		this.avgNetDeposit = avgNetDeposit;
	}	
	
	public String getNumOfAvgDeposits() {
		return numOfAvgDeposits;
	}
	
	public String setNumOfAvgDeposits(String numOfAvgDeposits) {
		return this.numOfAvgDeposits = numOfAvgDeposits;
	}

	/**
	 * Removing leading zero
	 * @param phone
	 * @return
	 */
	public String handleLeadingZero(String phone) {
		String newPhone = phone;
		if ( !CommonUtil.isParameterEmptyOrNull(phone) && phone.indexOf("0") == 0 ) {   // leading zero (index 0)
			newPhone = phone.substring(1);
		}
		return newPhone;
	}

	/**
	 * @return the tmpUserId
	 */
	public Long getTmpUserId() {
		return tmpUserId;
	}

	/**
	 * @param tmpUserId the tmpUserId to set
	 */
	public void setTmpUserId(Long tmpUserId) {
		this.tmpUserId = tmpUserId;
	}

	public String getDir() {
		if (skinId == Skin.SKIN_ARABIC || CommonUtil.isHebrewSkin(skinId)){
			return "rtl";
		}
		return "ltr";
	}

//	/**
//	 * Override getContactId function
//	 * for taking contactId from sessionUser in case we are in retention mode.
//	 */
//	public long getContactId() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		User sessionUser = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//		if(this.contactId == 0 && null != sessionUser && (sessionUser.isRetentionSelected() ||
//				sessionUser.isRetentionMSelected())&& sessionUser.getContactId() != 0 ) {
//			return sessionUser.getContactId();
//		}
//		return contactId;
//	}

	/**
	 * @param setIsContactByPhone the isContactBySMS to set
	 */
	public void setContactByPhone(boolean isContactByPhone) {
		this.isContactByPhone = (isContactByPhone) ? 1 : 0;
	}

	public String getRolesToString() {
		String list = ConstantsBase.EMPTY_STRING;
		List<String> rolesA = getRoles();
		for (int i = 0 ; i < rolesA.size(); i++) {
			list += rolesA.get(i) + ",";
		}
		return list.substring(0,list.length()-1);
	}

	/**
	 * @return the haveBonus
	 */
	public boolean isHaveBonus() {
		return haveBonus;
	}

	/**
	 * @param haveBonus the haveBonus to set
	 */
	public void setHaveBonus(boolean haveBonus) {
		this.haveBonus = haveBonus;
	}

	public String getContactByPhoneTxt() {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if (isContactByPhone==1)
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
		else
			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	}

	public String getFalseAccountTxt() {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if (isFalseAccount)
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
		else
			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	}

	/**
	 * Update tier for user
	 * @return
	 * @throws SQLException
	 */
	public String updateTier() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		boolean res = TiersManager.updateUserTier(id, this.tierUser.getTierId(), Utils.getWriter().getWriter().getId(),0);
		if (res) {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("update.tier.succeed",null),null);
			context.addMessage(null, fm);

			UsersManager.loadUserStripFields(this, true, null);
			logger.debug("updating user session details.");
			return Constants.NAV_EMPTY_SUBTABS;
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("update.tier.failed",null),null);
			context.addMessage(null, fm);

			return null;
		}
	}

	/**
	 * @return the bonusUsed
	 */
	public int getBonusUsed() {
		return bonusUsed;
	}

	/**
	 * @param bonusUsed the bonusUsed to set
	 */
	public void setBonusUsed(int bonusUsed) {
		this.bonusUsed = bonusUsed;
	}

	/**
	 * @return the Date Txt
	 */
	public String getWriterDateTxt() throws SQLException {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, Utils.getWriter().getUtcOffset() );
	}

	public long getReferenceIdLong() {
		return referenceId.longValue();
	}

	/**
	 * @return the supportMOLocking
	 */
	public boolean isSupportMOLocking() {
		return supportMOLocking;
	}

	/**
	 * @param supportMOLocking the supportMOLocking to set
	 */
	public void setSupportMOLocking(boolean supportMOLocking) {
		this.supportMOLocking = supportMOLocking;
	}

	public boolean isSupportSelectedWithMOLocking() {
		if (isSupportSelected() && isSupportMOLocking() ) {
			return true;
		}
		return false;
	}

	/**
	 * is a partner writer
	 * @return
	 */
	public boolean isPartnerWriter() {
		WriterWrapper w = Utils.getWriter();
		if (null != w.getSkins() &&
					w.getSkins().size() == 1) {
			if (ApplicationData.getSkinById(w.getSkins().get(0)).
					getBusinessCaseId() == SkinsManagerBase.BUSINESS_CASE_PARTNERS) {
				return true;
			}
		}
		return false;
	}

	/**
     * Return Mobile phone to copy from user strip
     * @return
     */

	public String getMobileNumberToCopy(){
		if (countryId != 0){
			this.mobilePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
		}
		String number=null;
		if (!Utils.isParameterEmptyOrNull(mobilePhone)){
			if (mobilePhonePrefix.equals("972")){
				return mobilePhone;
			}
		number = mobilePhonePrefix.concat(mobilePhone);
		}
		return number;
	}

    /**
     * Return Land Line phone to copy from user strip
     * @return
     */

	public String getLandLineNumberToCopy(){
		if (countryId != 0){
			this.landLinePhonePrefix = CommonUtil.getPrefixFromCountry(countryId);
		}
		String number=null;
		if (!Utils.isParameterEmptyOrNull(landLinePhone)){
			if (landLinePhonePrefix.equals("972")){
				return landLinePhone;
			}
			number = landLinePhonePrefix.concat(landLinePhone);
		}
		return number;
	}

	/**
	 * @return
	 */
	public int getScreenId(){
		int screenId = 0;
		if (this.isRetentionSelected()) {
			screenId = ConstantsBase.SCREEN_SALE;
		} else if (this.isRetentionMSelected()) {
			screenId = ConstantsBase.SCREEN_SALEM;
		} else if (this.isSupportSelected()) {
			screenId = ConstantsBase.SCREEN_SUPPORT;
		} else if (this.isSupportMoreOptionsSelected()){
			screenId = ConstantsBase.SCREEN_SUPPORTMO;
		} else if (this.isAdminSelected()){
			screenId = ConstantsBase.SCREEN_ADMIN;
		}
		return screenId;
	}

	public ArrayList<SelectItem> getAllCcList() throws SQLException{
		return TransactionsManager.getAllCreditCardsByUserList(id);
	}

	/**
	 * @return the maxDepositDate
	 */
	public String getMaxDepositDate() {
		return maxDepositDate;
	}

	/**
	 * @param maxDepositDate the maxDepositDate to set
	 */
	public void setMaxDepositDate(String maxDepositDate) {
		this.maxDepositDate = maxDepositDate;
	}

	/**
	 * @return the maxDepositAmount
	 */
	public String getMaxDepositAmount() {
		return maxDepositAmount;
	}

	/**
	 * @param maxDepositAmount the maxDepositAmount to set
	 */
	public void setMaxDepositAmount(String maxDepositAmount) {
		this.maxDepositAmount = maxDepositAmount;
	}

	/**
	 * @return the filesRiskStatus
	 */
	public String getFilesRiskStatus() {
		return filesRiskStatus;
	}

	/**
	 * @param filesRiskStatus the filesRiskStatus to set
	 */
	public void setFilesRiskStatus(String filesRiskStatus) {
		this.filesRiskStatus = filesRiskStatus;
	}

		/**
	 * @return the loginsUserAgent
	 */
	public String getLoginsUserAgent() {
		return loginsUserAgent;
	}

	/**
	 * @param loginsUserAgent the loginsUserAgent to set
	 */
	public void setLoginsUserAgent(String loginsUserAgent) {
		this.loginsUserAgent = loginsUserAgent;
	}

	public String getMobileType() {
		/*String mobileType = super.getMobileType();   // get mobile type by user record
		if (CommonUtil.IsParameterEmptyOrNull(mobileType) ||
				(!CommonUtil.IsParameterEmptyOrNull(mobileType) && mobileType.equals(Constants.MOBILE_DES_OTHER))) {
			*/
			// get mobile type by logins records
			return getMobileType(loginsUserAgent);
		/*} else {
			return mobileType;
		}*/
	}
    
	public String getDeviceFamily(){ 
		return super.getDeviceFamily();
	} 

	public String getWriterName() {
		if (writerId != 0) {
			return Utils.getWriter().getAllWritersMap().get(new Long(writerId)).getUserName();
		} else {
			return "";
		}
	}

	/**
	 * @return the deviceCampaign
	 */
	public String getDeviceCampaign() {
		return deviceCampaign;
	}

	/**
	 * @param deviceCampaign the deviceCampaign to set
	 */
	public void setDeviceCampaign(String deviceCampaign) {
		this.deviceCampaign = deviceCampaign;
	}

	/**
	 * @return the isShortRegFormContact
	 */
	public boolean isShortRegFormContact() {
		return isShortRegFormContact;
	}

	/**
	 * @return the campaignProrityText
	 */
	public String getCampaignPriorityText() {
		switch (campaignPriority) {
		case 1:
			return "LOW";
		case 2:
			return "MED";
		case 3:
			return "HIGH";
		default:
			break;
		}
		return "";
	}

	/**
	 * @param campaignPriority the campaignPriority to set
	 */
	public void setCampaignPriority(int campaignPrority) {
		this.campaignPriority = campaignPrority;
	}

	/**
	 * @return the campaignDesc
	 */
	public String getCampaignDesc() {
		return campaignDesc;
	}

	/**
	 * @param campaignDesc the campaignDesc to set
	 */
	public void setCampaignDesc(String campaignDesc) {
		this.campaignDesc = campaignDesc;
	}

    public String getTimeCreatedWithWritterOffset() {
        return CommonUtil.getDateAndTimeFormat(timeCreated, Utils.getWriter().getUtcOffset());
    }

    public String getTimeLastLoginWithWritterOffset() {
        return CommonUtil.getDateAndTimeFormat(timeLastLogin, Utils.getWriter().getUtcOffset());
    }

    public String getLastDeclineWritterOffset() {
        return CommonUtil.getDateTimeFormatDisplay(this.lastDeclineDate, Utils.getWriter().getUtcOffset() );
    }

    public String getLastSalesDpstDateWithWritterOffset() {
        return CommonUtil.getDateFormat(this.lastSalesDepositDate, Utils.getWriter().getUtcOffset() );
    }

	/**
	 * @return the confirmContactDetails
	 */
	public boolean isConfirmContactDetails() {
		return confirmContactDetails;
	}

	/**
	 * @param confirmContactDetails the confirmContactDetails to set
	 */
	public void setConfirmContactDetails(boolean confirmContactDetails) {
		this.confirmContactDetails = confirmContactDetails;
	}

    /**
     * @return the specialCareManualy
     */
    public boolean isSpecialCareManualy() {
        return specialCareManualy;
    }

    /**
     * @param specialCareManualy the specialCareManualy to set
     */
    public void setSpecialCareManualy(boolean specialCareManualy) {
        this.specialCareManualy = specialCareManualy;
    }

    public long getWriterSalesType() {
    	return Utils.getWriter().getWriter().getSales_type();
    }

	/**
	 * @return the retentionRepName
	 */
	public String getRetentionRepName() {
		return retentionRepName;
	}

	/**
	 * @param retentionRepName the retentionRepName to set
	 */
	public void setRetentionRepName(String retentionRepName) {
		this.retentionRepName = retentionRepName;
	}

	/**
	 * @return the contentsName
	 */
	public String getContentsName() {
		return contentsName;
	}

	/**
	 * @param contentsName the contentsName to set
	 */
	public void setContentsName(String contentsName) {
		this.contentsName = contentsName;
	}

	/**
	 * @return the sumBonuses
	 */
	public String getSumBonuses() {
		return sumBonuses;
	}

	/**
	 * @param sumBonuses the sumBonuses to set
	 */
	public void setSumBonuses(String sumBonuses) {
		this.sumBonuses = sumBonuses;
	}

	/**
	 * @return the tmpContactId
	 */
	public Long getTmpContactId() {
		return tmpContactId;
	}

	/**
	 * @param tmpContactId the tmpContactId to set
	 */
	public void setTmpContactId(Long tmpContactId) {
		this.tmpContactId = tmpContactId;
	}


	public boolean isShowContactSearch() {
		return showContactSearch;
	}

	public void setShowContactSearch(boolean showContactSearch) {
		this.showContactSearch = showContactSearch;
	}

	public boolean isLoadUserStrip() {
		return loadUserStrip;
	}

	public void setLoadUserStrip(boolean loadUserStrip) {
		this.loadUserStrip = loadUserStrip;
	}

	public String getConMobilePhone() {
		return conMobilePhone;
	}

	public void setConMobilePhone(String conMobilePhone) {
		this.conMobilePhone = conMobilePhone;
	}

	public String getConLandLinePhone() {
		return conLandLinePhone;
	}

	public void setConLandLinePhone(String conLandLinePhone) {
		this.conLandLinePhone = conLandLinePhone;
	}

	public String getConLandLinePhonePrefix() {
		return conLandLinePhonePrefix;
	}

	public void setConLandLinePhonePrefix(String conLandLinePhonePrefix) {
		this.conLandLinePhonePrefix = conLandLinePhonePrefix;
	}

	public String getConMobilePhonePrefix() {
		return conMobilePhonePrefix;
	}

	public void setConMobilePhonePrefix(String conMobilePhonePrefix) {
		this.conMobilePhonePrefix = conMobilePhonePrefix;
	}

	public String getConEmail() {
		return conEmail;
	}

	public void setConEmail(String conEmail) {
		this.conEmail = conEmail;
	}

	public String getConFirstName() {
		return conFirstName;
	}

	public void setConFirstName(String conFirstName) {
		this.conFirstName = conFirstName;
	}

	public String getConLastName() {
		return conLastName;
	}

	public void setConLastName(String conLastName) {
		this.conLastName = conLastName;
	}

	public Long getConContactId() {
		return conContactId;
	}

	public void setConContactId(Long conContactId) {
		this.conContactId = conContactId;
	}

	public String getConTypeId() {
		return conTypeId;
	}

	public void setConTypeId(String conTypeId) {
		this.conTypeId = conTypeId;
	}

	public String getBonusAbuserString() {
		return isBonusAbuser()?"Yes":"No";
	}

    public String getCampaignRemarketing() {
        return campaignRemarketing;
    }

    public void setCampaignRemarketing(String campaignRemarketing) {
        this.campaignRemarketing = campaignRemarketing;
    }

	public  String getIsRegulatedTxt() {
		if (CommonUtil.isUserSkinRegulated()) {

			setIsRegulatedTxt(IS_REGULATED_YES);
		} else {
			setIsRegulatedTxt(IS_REGULATED_NO);
		}
		return isRegulatedTxt;
	}

	public  void setIsRegulatedTxt(String isRegulatedTxt) {
		this.isRegulatedTxt = isRegulatedTxt;
	}

	public String getIsRegulatedStateTxt() throws SQLException {

	     try {
	            if (null!= userRegulation && null!= userRegulation.getApprovedRegulationStep() && 
	            		(userRegulation.getApprovedRegulationStep() == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER)){
	                setIsRegulatedStateTxt(IS_REGULATED_YES_);
	                setControlApprovedDateTxt(CommonUtil.getDateFormat(userRegulation.getControlApprovedDate()));
	            } else {
	                setIsRegulatedStateTxt(IS_REGULATED_NO);
	            }
	            return isRegulatedStateTxt;
	     } catch (Exception e) {
	         setIsRegulatedStateTxt(IS_REGULATED_NO);
	         return isRegulatedStateTxt;
	     }
	}

	public  void setIsRegulatedStateTxt(String isRegulatedStateTxt) {
        this.isRegulatedStateTxt = isRegulatedStateTxt;
	}

	public  boolean getIsRegulated() {
		return CommonUtil.isUserSkinRegulated();
	}

	public  void setIsRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}

	public String getMandatoryQuestionnaireTxt() throws SQLException {
	   	FacesContext context=FacesContext.getCurrentInstance();
	   	if(null != userRegulation) {
	   	if ((null != userRegulation.getApprovedRegulationStep() && userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE)
	   			|| (null != userRegulation.getSuspendedReasonId() && userRegulation.getSuspendedReasonId() ==  UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT)){
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
	   	} else{

			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	   	}

	   	}else {

	   		return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	   	}
	}
	
	public String getQuestionnaireStatusTxt() throws SQLException {
	   	FacesContext context = FacesContext.getCurrentInstance();
	   	String msg = Constants.EMPTY_STRING; 
	   	if(null != userRegulation && null != userRegulation.getApprovedRegulationStep() && this.skinId != Skin.SKIN_ETRADER) {
	   		
	   		if(userRegulation.isKnowledgeQuestion() && userRegulation.getScoreGroup() != null
					&& userRegulation.isRestrictedGroup()){
	   			if(userRegulation.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID) {
	   				msg = CommonUtil.getMessage("regulation.user.success.after.failed", null, Utils.getWriterLocale(context));
	   			} else {	   				
	   				msg = CommonUtil.getMessage("regulation.user.success.after.restricted", null, Utils.getWriterLocale(context));
	   			}
	   		
	   		}else if(userRegulation.isRestricted()){
	   			msg = CommonUtil.getMessage("regulation.user.isrestricted", null, Utils.getWriterLocale(context));	   		
	   		
	   		}else if(userRegulation.getApprovedRegulationStep() != null 
	   				&& userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
	   				 && !userRegulation.isKnowledgeQuestion()
	   				  && userRegulation.getSuspendedReasonId() != UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT){
	   			msg = CommonUtil.getMessage("regulation.user.success.questionnaire", null, Utils.getWriterLocale(context));	   		
	   		
	   		}else if(userRegulation.isSuspended() 
	   				&& userRegulation.getSuspendedReasonId() == UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT){
	   			msg = CommonUtil.getMessage("regulation.user.failed.questionnaire", null, Utils.getWriterLocale(context));	   		
	   		
	   		}else if((UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
					&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION))
					|| (userRegulation.getApprovedRegulationStep() < UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE && !userRegulation.isSuspended()) ){
	   			msg = CommonUtil.getMessage("regulation.user.notcomplete.questionnaire", null, Utils.getWriterLocale(context));	   		
	   		}
	   	}
	   	return msg;	   	
	}

	public String getOptionalQuestionnaireTxt() throws SQLException {
	   	FacesContext context=FacesContext.getCurrentInstance();
		if(null != userRegulation) {
	   	if (userRegulation.isOptionalQuestionnaireDone()){
			return CommonUtil.getMessage("yes",null,Utils.getWriterLocale(context));
	   	} else{

			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));
	   	}

		}else {

			return CommonUtil.getMessage("no",null,Utils.getWriterLocale(context));

		}
	}

	public String getStatusTxt() {
		if (statusId == UsersManagerBase.USER_STATUS_ACTIVE) {
			return CommonUtil.getMessage("user.status.active", null);
		} else if (statusId == UsersManagerBase.USER_STATUS_SLEEPER) {
			return CommonUtil.getMessage("user.status.sleeper", null);
		} if (statusId == UsersManagerBase.USER_STATUS_COMA) {
			return CommonUtil.getMessage("user.status.coma", null);
		} else {
			return "";
		}
	}
	
	public String getPepStatusTxt() throws SQLException {
	   	String msg = "";
	   	if(null != userRegulation && userRegulation.getApprovedRegulationStep() != null 
	   				&& userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_FIRST_DEPOSIT) {
		   	if(userRegulation.getPepState() == UserRegulationBase.PEP_NON_PROHIBITED){
		   		msg = CommonUtil.getMessage("user.pep.stayus.no", null);
		   	} else if(userRegulation.getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED 
		   				|| userRegulation.getPepState() == UserRegulationBase.PEP_WRITER_PROHIBITED){
				msg = CommonUtil.getMessage("user.pep.stayus.suspended", null);
		   	} else if(userRegulation.getPepState() == UserRegulationBase.PEP_APPROVED_BY_COMPLIANCE){
				msg = CommonUtil.getMessage("user.pep.stayus.approved", null);
		   	} else if(userRegulation.getPepState() == UserRegulationBase.PEP_FALSE_CLASSIFICATION){
				msg = CommonUtil.getMessage("user.pep.stayus.no", null);
		   	}
	   	}
	   	return msg;
	}
	
	public String getPepStatusColor() throws SQLException {
	   	String color = "user_strip_value";
	   	if(null != userRegulation && userRegulation.getApprovedRegulationStep() != null 
	   				&& userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_FIRST_DEPOSIT) {
	   		if(userRegulation.getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED 
		   				|| userRegulation.getPepState() == UserRegulationBase.PEP_WRITER_PROHIBITED){
	   			color = "user_strip_value_red";
	   		} 
	   	}
	   	return color;
	}

 public UserRegulationBase getUserRegulation() {
	return userRegulation;
}

  public void setUserRegulation(UserRegulationBase userRegulation) {
	this.userRegulation = userRegulation;
 }

  public String getTimeSuspendedTxt() {
		if(null!= userRegulation && null!= userRegulation.getTimeSuspended() ) {
		timeSuspendedTxt = CommonUtil.getDateAndTimeFormat(userRegulation.getTimeSuspended(),Utils.getWriter().getUtcOffset());
		}
		return timeSuspendedTxt;
	}

	public void setTimeSuspendedTxt(String timeSuspendedTxt) {
		this.timeSuspendedTxt = timeSuspendedTxt;
	}

	public String getSuspendedReasonTxt() {
		if(null!= userRegulation) {
			if(null!= userRegulation.getSuspendedReasonId()) {
				if(userRegulation.getSuspendedReasonId() == 0 || userRegulation.getSuspendedReasonId() == 1 ) {
					if(userRegulation.isTresholdBlock()){
						String[] params = new String[1];
						params[0] = CommonUtil.displayAmount(userRegulation.getThresholdBlock(), true, this.getCurrencyId(), false , "###,###,##0");						
						suspendedReasonTxt = CommonUtil.getMessage("userstrip.suspended.reason.treshold.block", params);
					} else {
						if(userRegulation.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID && userRegulation.isRestricted()) {
							suspendedReasonTxt = CommonUtil.getMessage("userstrip.suspended.reason.5", null);
						} else {
							suspendedReasonTxt = userRegulation.getSuspendedReasonTxt();
						}
					}				   
				}
				
				if(userRegulation.getSuspendedReasonId() == 2) {
					suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_2_TXT,null);
				}
				
				if(userRegulation.getSuspendedReasonId() == 4) {
					suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_4_TXT,null);
				}
				
				if(userRegulation.getSuspendedReasonId() == 5) {
					suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_5_TXT,null);
				}
				
				if(userRegulation.getSuspendedReasonId() == 6) {
					suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_6_TXT,null);
				}
				
				if(userRegulation.getSuspendedReasonId() == 15) {
					suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_15_TXT,null);
				}
												
				if(userRegulation.getSuspendedReasonId() == 3) {
					try {
						suspendedReasonTxt = userRegulation.getSuspendedReasonTxt();
						Hashtable<String,Integer> filesStatus = UsersManager.getFirstIssueFilesStatusByUserId(this.id);
						if((filesStatus.get(FilesDAOBase.fileCount) == filesStatus.get(FilesDAOBase.supportApprove))
								&& (filesStatus.get(FilesDAOBase.controlApprove) == filesStatus.get(FilesDAOBase.fileCount))
						          && getDaysAfterFD() == 0 && userRegulation.getApprovedRegulationStep() != UserRegulationBase.REGULATION_CONTROL_APPROVED_USER) {
							suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_3_D_TXT,null);
						}
						if((filesStatus.get(FilesDAOBase.fileCount) > filesStatus.get(FilesDAOBase.supportApprove))
								&& getDaysAfterFD() == 0 ) {
							suspendedReasonTxt = CommonUtil.getMessage(SUSPENDED_REASON_3_C_TXT,null);
						}
					} catch (SQLException e) {
						logger.error("Error while getting details for users regulation files.userId: " + this.id);
						e.printStackTrace();
					}
	
				}
	
			  } else {
				//Non regualtion
				if(isNonRegSuspend){
					suspendedReasonTxt = "Manually suspended";	
				} else {
					suspendedReasonTxt = "Currently not suspended";
				}
			  }
			}
		return suspendedReasonTxt;
	}

	public void setSuspendedReasonTxt(String suspendedReasonTxt) {
		this.suspendedReasonTxt = suspendedReasonTxt;
	}

	public int getDaysAfterFD() {

		  int result = 0;
		  if(null!= userRegulation && null != userRegulation.getQualificationTime()) {
		  Date now = Calendar.getInstance().getTime();
		  Date daysFromQualifiedTime = CommonUtil.addDays(userRegulation.getQualificationTime(), UserRegulationBase.DAYS_FROM_QUALIFIED_TIME);
		  int daysUntilSuspend = CommonUtil.daysBetween(daysFromQualifiedTime, now);
		  result =  Math.max(daysUntilSuspend, 0);
		  }
		  return result;
	}

	public void setDaysAfterFD(int daysAfterFD) {
		this.daysAfterFD = daysAfterFD;
	}

	public String getTmpEmail() {
		return tmpEmail;
	}

	public void setTmpEmail(String tmpEmail) {
		this.tmpEmail = tmpEmail;
	}

	/**
	 * @param email the email to set
	 * overwrite the base method to set the username with the email
	 */
	public void setEmailRegister(String email) {
		this.email = email.trim();
		this.userName = email.trim();
	}

	public String getEmailRegister() {
		return email;
	}

	/**
	 * check if the user is class type 3
	 * @return true if api user else false
	 */
	public boolean isApiUser() {
		if (skinId == Skin.SKIN_API) {
			return true;
		}
		return false;
	}

    /**
     * @return the limitTxt
     */
    public String getLimitTxt() {
        return limitTxt;
    }

    /**
     * @param limitTxt the limitTxt to set
     */
    public void setLimitTxt(String limitTxt) {
        this.limitTxt = limitTxt;
    }

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the regOrig
	 */
	public String getRegOrig() {
		return regOrig;
	}

	/**
	 * @param regOrig the regOrig to set
	 */
	public void setRegOrig(String regOrig) {
		this.regOrig = regOrig;
	}

	public int getCopyopState() {
		return copyopState;
	}

	public void setCopyopState(int copyopState) {
		this.copyopState = copyopState;
	}
	
	public String isRegularReportMailTxt() {
		if (userRegulation == null) {
			return "";
		} else {
			if (userRegulation.isRegularReportMail()) {
				return "Yes";
			} else {
				return "No";
			}
		}
	}
	
	public String getKnowledgeQuestionAnswer() {
		if (userRegulation == null || userRegulation.getUserId() == 0) {
			return "";
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER) {
				return ("");
			} else if (userRegulation.isKnowledgeQuestion()) {
				return CommonUtil.getMessage("yes", null, Utils.getWriterLocale(context));
			} else {
				return CommonUtil.getMessage("no", null, Utils.getWriterLocale(context));
			}
		}
	}
	
	public String getScoreAfterKnowledgeQuestionAnswer() {
		if (userRegulation == null || userRegulation.getUserId() == 0) {
			return "";
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE) {
				return ("");
			} else if (userRegulation.getScoreGroup() == UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID) {
				return "High (" + userRegulation.getScore() + ")";
			} else if (userRegulation.getScoreGroup() == UserRegulationBase.ET_REGULATION_MEDIUM_SCORE_GROUP_ID) {
				return "Medium (" + userRegulation.getScore() + ")";
			} else {
				return "Low (" + userRegulation.getScore() + ")";
			}
		}
	}

	public boolean isEtRegulated() {
		boolean isRegulated = false;
		if (userRegulation == null || userRegulation.getUserId() == 0) {
			return isRegulated;
		} else {
			if (userRegulation.getApprovedRegulationStep() != null && isEtrader()) {
				isRegulated = true;
			}
		}
		return isRegulated;
	}
	
	public boolean showSingleRegQuestionnaire() {
		if (userRegulation != null && userRegulation.getUserId() != 0 && userRegulation.getRegulationVersion() != null) {
			if (userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION && !isEtrader()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean showRegulationHistoryTabAOUsers(){
		return getSkin().isRegulated() && !isEtrader();
	}

	public String getEtRegSuspendedIdTxt() {
		String reason = "";
		if (userRegulation != null) {
			if (userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP) {
				reason = "Low score";
			} else if (userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP) {
				reason = "Low score group threshold";
			} else if (userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
				reason = "Medium score group threshold";
			}
		}
		return reason;
	}
	
	public String getEtUserSuspendMessage() {
		if (!getEtRegSuspendedIdTxt().equals("")) {
			return "The customer must activate his account before any further deposits";
		}
		return "";
	}
	
	/**
	 * check if we can find the LP the user come from
	 * only if he came from media 
	 * @return true if we can find else false
	 */
	public boolean IsAvalibleLPUrl() {
		if (id > 0) {
			try {
				if (UsersManager.isUserFromMediaChannel(id)) {
					//use this function for reuse we send only combId
					ArrayList<MarketingCombination> list;
					
						list = MarketingManager.getMarketingCombinations(0, null, null, 0, 0,
								null, null, null, null, 0, combinationId, 0, 1);
						if (list.size() > 0 && 
								(list.get(0).getLandingPageType() != Constants.MARKETING_LANDING_PAGE_DYNAMIC)) {
							userLPUrl = list.get(0).getUrl();;
							return true;
						}
				}
			} catch (SQLException e) {
				logger.error("can't get the lp that user come from user. user id = " + id, e);
			}			
			userLPUrl = null;
		}
		return false;
	}

	public String getUserLPUrl() {
		return userLPUrl;
	}

	public void setUserLPUrl(String userLPUrl) {
		this.userLPUrl = userLPUrl;
	}

	public String getControlApprovedDateTxt() {
		return controlApprovedDateTxt;
	}

	public void setControlApprovedDateTxt(String controlApprovedDateTxt) {
		this.controlApprovedDateTxt = controlApprovedDateTxt;
	}
	
    public String getAffSiteLink() {
		return affSiteLink;
	}

	public void setAffSiteLink(String affSiteLink) {
		this.affSiteLink = affSiteLink;
	}

	public boolean isPasswordReset() {
		return isPasswordReset;
	}

	public void setPasswordReset(boolean isPasswordReset) {
		this.isPasswordReset = isPasswordReset;
	}
	
	/**
	 * Check if the affiliate is segmob
	 * @return boolean
	 */
	public boolean isAffiliateSegmob() {
		long affKeySegmob;
		try {
			affKeySegmob = Long.valueOf(CommonUtil.getEnum(	MarketingAffiliatesManagerBase.ENUM_MARKETING_AFFILIATE_KEY_ENUMERATOR,
					MarketingAffiliatesManagerBase.ENUM_MARKETING_AFFILIATE_KEY_SEGMOB_CODE));
			if (affiliateKey == affKeySegmob) {
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while checking if the affiliate is segmob. ", e);
		}
		return false;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
	
}