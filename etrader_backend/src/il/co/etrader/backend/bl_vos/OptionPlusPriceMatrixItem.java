package il.co.etrader.backend.bl_vos;

import java.util.ArrayList;


/**
 * OptionPlusPrice class.
 *
 * @author Kobi
 */
public class OptionPlusPriceMatrixItem extends OptionPlusPrice {
	private static final long serialVersionUID = 1L;

	private long itemType;
	private String value;

	public static final long TYPE_ROW_HEADER = 1;
	public static final long TYPE_COLUMN_HEADER = 2;
	public static final long TYPE_ROW_VALUE = 3;
	public static final long TYPE_COLUMN_VALUE = 4;
	public static final long TYPE_MATRIX_VALUE = 5;
	public static final long TYPE_EMPLY_VALUE = 6;

	private ArrayList<OptionPlusPriceMatrixItem> items;

	public OptionPlusPriceMatrixItem() {

	}

	public OptionPlusPriceMatrixItem(long itemType, double price, long id) {
		this.itemType = itemType;
		this.price = price;
		this.id = id;
		this.value = String.valueOf(price);
	}

	/**
	 * @return the itemType
	 */
	public long getItemType() {
		return itemType;
	}

	/**
	 * @param itemType the itemType to set
	 */
	public void setItemType(long itemType) {
		this.itemType = itemType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the items
	 */
	public ArrayList<OptionPlusPriceMatrixItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<OptionPlusPriceMatrixItem> items) {
		this.items = items;
	}

}
