package il.co.etrader.backend.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.util.Date;

public class BlackList implements Serializable{

	private static final long serialVersionUID = 4193315795138629972L;

	long userId;
	String userName;
	String actionTypeName;
	String publisher;
	private Date actionTime;
	private String actionTimeOffset;

	/**
	 * @return the actionTime
	 */
	public Date getActionTime() {
		return actionTime;
	}
	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
	/**
	 * @return the actionTimeOffset
	 */
	public String getActionTimeOffset() {
		return actionTimeOffset;
	}
	/**
	 * @param actionTimeOffset the actionTimeOffset to set
	 */
	public void setActionTimeOffset(String actionTimeOffset) {
		this.actionTimeOffset = actionTimeOffset;
	}
	/**
	 * @return the actionTypeName
	 */
	public String getActionTypeName() {
		return actionTypeName;
	}
	/**
	 * @param actionTypeName the actionTypeName to set
	 */
	public void setActionTypeName(String actionTypeName) {
		this.actionTypeName = actionTypeName;
	}
	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return publisher;
	}
	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getActionTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(actionTime, CommonUtil.getUtcOffset(actionTimeOffset));
	}
}
