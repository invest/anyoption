package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class Representative implements Serializable {
	
	protected static Logger log = Logger.getLogger(Representative.class);
	
	private long repId;
	private String repName;
	
	public long getRepId() {
		return repId;
	}
	public void setRepId(long repId) {
		this.repId = repId;
	}
	public String getRepName() {
		return repName;
	}
	public void setRepName(String repName) {
		this.repName = repName;
	}
}
