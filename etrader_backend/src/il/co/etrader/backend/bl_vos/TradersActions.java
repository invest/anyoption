package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.sql.Date;

public class TradersActions implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long traderId;
	private String traderName;
	private String action;
	private long actionDate;
	private String assetType;	
	private long totalCount;
	
	public long getTraderId() {
		return traderId;
	}
	
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	
	public String getTraderName() {
		return traderName;
	}
	
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public long getActionDate() {
		return actionDate;
	}
	
	public void setActionDate(long actionDate) {
		this.actionDate = actionDate;
	}
	
	public String getAssetType() {
		return assetType;
	}
	
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}	
}
