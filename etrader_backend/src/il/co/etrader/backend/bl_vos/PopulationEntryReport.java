package il.co.etrader.backend.bl_vos;

import java.util.Date;

public class PopulationEntryReport extends PopulationEntry{
	private static final long serialVersionUID = 1L;
	private long actionId;
	private String channelName;
	private String writerName;
	private String UserName;
	private String usersMobilePhone;
	private String usersLandLinePhone;
	private String usersEmail;
	private long contactId;
	private String contactsMobilePhone;
	private String contactLandLinePhone;
	private String contactEmail;
	private Date actionTime;
	
	/**
	 * @return the issueActionId
	 */
	public long getActionId() {
		return actionId;
	}
	/**
	 * @param issueActionId the issueActionId to set
	 */
	public void setActionId(long actionId) {
		this.actionId = actionId;
	}
	/**
	 * @return the channelName
	 */
	public String getChannelName() {
		return channelName;
	}
	/**
	 * @param channelName the channelName to set
	 */
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return UserName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}
	/**
	 * @return the usersMobilePhone
	 */
	public String getUsersMobilePhone() {
		return usersMobilePhone;
	}
	/**
	 * @param usersMobilePhone the usersMobilePhone to set
	 */
	public void setUsersMobilePhone(String usersMobilePhone) {
		this.usersMobilePhone = usersMobilePhone;
	}
	/**
	 * @return the usersLandLinePhone
	 */
	public String getUsersLandLinePhone() {
		return usersLandLinePhone;
	}
	/**
	 * @param usersLandLinePhone the usersLandLinePhone to set
	 */
	public void setUsersLandLinePhone(String usersLandLinePhone) {
		this.usersLandLinePhone = usersLandLinePhone;
	}
	/**
	 * @return the usersEmail
	 */
	public String getUsersEmail() {
		return usersEmail;
	}
	/**
	 * @param usersEmail the usersEmail to set
	 */
	public void setUsersEmail(String usersEmail) {
		this.usersEmail = usersEmail;
	}
	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the contactsMobilePhone
	 */
	public String getContactsMobilePhone() {
		return contactsMobilePhone;
	}
	/**
	 * @param contactsMobilePhone the contactsMobilePhone to set
	 */
	public void setContactsMobilePhone(String contactsMobilePhone) {
		this.contactsMobilePhone = contactsMobilePhone;
	}
	/**
	 * @return the contactLandLinePhone
	 */
	public String getContactLandLinePhone() {
		return contactLandLinePhone;
	}
	/**
	 * @param contactLandLinePhone the contactLandLinePhone to set
	 */
	public void setContactLandLinePhone(String contactLandLinePhone) {
		this.contactLandLinePhone = contactLandLinePhone;
	}
	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}
	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	/**
	 * @return the actionTime
	 */
	public Date getActionTime() {
		return actionTime;
	}
	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
}
