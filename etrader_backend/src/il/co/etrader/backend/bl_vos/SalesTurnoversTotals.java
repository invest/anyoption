package il.co.etrader.backend.bl_vos;


import java.io.Serializable;

public class SalesTurnoversTotals implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	private String date;

	private String retInvMadeCount;
	private String retInvMadeSum;
	private String retInvNumOfUsers;
	
	private String retTranMadeCount;
	private String retTranMadeSum;
	private String retTranNumOfUsers;
	
	private String retTranWithdrawMadeSum; // Sum of withdrawals
	private String retTranWithdrawDepMadeSum; // sum withdrawals divided by sum deposits	

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the retInvMadeCount
	 */
	public String getRetInvMadeCount() {
		return retInvMadeCount;
	}
	/**
	 * @param retInvMadeCount the retInvMadeCount to set
	 */
	public void setRetInvMadeCount(String retInvMadeCount) {
		this.retInvMadeCount = retInvMadeCount;
	}
	/**
	 * @return the retInvMadeSum
	 */
	public String getRetInvMadeSum() {
		return retInvMadeSum;
	}
	/**
	 * @param retInvMadeSum the retInvMadeSum to set
	 */
	public void setRetInvMadeSum(String retInvMadeSum) {
		this.retInvMadeSum = retInvMadeSum;
	}
	/**
	 * @return the retInvNumOfUsers
	 */
	public String getRetInvNumOfUsers() {
		return retInvNumOfUsers;
	}
	/**
	 * @param retInvNumOfUsers the retInvNumOfUsers to set
	 */
	public void setRetInvNumOfUsers(String retInvNumOfUsers) {
		this.retInvNumOfUsers = retInvNumOfUsers;
	}
	/**
	 * @return the retTranMadeCount
	 */
	public String getRetTranMadeCount() {
		return retTranMadeCount;
	}
	/**
	 * @param retTranMadeCount the retTranMadeCount to set
	 */
	public void setRetTranMadeCount(String retTranMadeCount) {
		this.retTranMadeCount = retTranMadeCount;
	}
	/**
	 * @return the retTranMadeSum
	 */
	public String getRetTranMadeSum() {
		return retTranMadeSum;
	}
	/**
	 * @param retTranMadeSum the retTranMadeSum to set
	 */
	public void setRetTranMadeSum(String retTranMadeSum) {
		this.retTranMadeSum = retTranMadeSum;
	}
	/**
	 * @return the retTranNumOfUsers
	 */
	public String getRetTranNumOfUsers() {
		return retTranNumOfUsers;
	}
	/**
	 * @param retTranNumOfUsers the retTranNumOfUsers to set
	 */
	public void setRetTranNumOfUsers(String retTranNumOfUsers) {
		this.retTranNumOfUsers = retTranNumOfUsers;
	}
	/**
	 * @return the retTranWithdrawMadeSum
	 */
	public String getRetTranWithdrawMadeSum() {
		return retTranWithdrawMadeSum;
	}
	/**
	 * @param retTranWithdrawMadeSum the retTranWithdrawMadeSum to set
	 */
	public void setRetTranWithdrawMadeSum(String retTranWithdrawMadeSum) {
		this.retTranWithdrawMadeSum = retTranWithdrawMadeSum;
	}
	/**
	 * @return the retTranWithdrawDepMadeSum
	 */
	public String getRetTranWithdrawDepMadeSum() {
		return retTranWithdrawDepMadeSum;
	}
	/**
	 * @param retTranWithdrawDepMadeSum the retTranWithdrawDepMadeSum to set
	 */
	public void setRetTranWithdrawDepMadeSum(String retTranWithdrawDepMadeSum) {
		this.retTranWithdrawDepMadeSum = retTranWithdrawDepMadeSum;
	}
}
