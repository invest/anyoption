package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.util.Date;

import com.anyoption.common.beans.Transaction;



public class TransactionTotal extends Transaction {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4069638241756483966L;
	private long totalCreditAmount;
    private long totalDebtAmount;
    private long totalprofitAmount;
    private boolean isBookBack;

	public TransactionTotal(int totalLine, long currencyId, Date timeCreated, String utcffsetCreated) {
    	super(totalLine);

        setCurrency(ApplicationData.getCurrencyById(currencyId));
        setTimeCreated(timeCreated);
        setUtcOffsetCreated(utcffsetCreated);
	}

    /**
     * @return the totalCreditAmount
     */
    public long getTotalCreditAmount() {
        return totalCreditAmount;
    }

    public String getTotalCreditAmountTxt() {
        return CommonUtil.displayAmount(totalCreditAmount, getCurrency());
    }


    public String getBaseTotalCreditAmountTxt() {
        return CommonUtil.displayAmount(totalCreditAmount, ConstantsBase.CURRENCY_BASE_ID);
    }
    /**
     * @param totalCreditAmount the totalCreditAmount to set
     */
    public void setTotalCreditAmount(long totalCreditAmount) {
        this.totalCreditAmount = totalCreditAmount;
    }

    /**
     * @return the totalDebtAmount
     */
    public long getTotalDebtAmount() {
        return totalDebtAmount;
    }

    public String getTotalDebtAmountTxt() {
        return CommonUtil.displayAmount(totalDebtAmount, getCurrency());
    }


    public String getBaseTotalDebtAmountTxt() {
        return CommonUtil.displayAmount(totalDebtAmount, ConstantsBase.CURRENCY_BASE_ID);
    }

    /**
     * @param totalDebtAmount the totalDebtAmount to set
     */
    public void setTotalDebtAmount(long totalDebtAmount) {
        this.totalDebtAmount = totalDebtAmount;
    }

    /**
     * @return the totalprofitAmount
     */
    public long getTotalprofitAmount() {
        return totalprofitAmount;
    }

    public String getTotalprofitAmountTxt() {
        return CommonUtil.displayAmount(totalprofitAmount, getCurrency());
    }


    public String getBaseTotalprofitAmountTxt() {
        return CommonUtil.displayAmount(totalprofitAmount, ConstantsBase.CURRENCY_BASE_ID);
    }

    /**
     * @param totalprofitAmount the totalprofitAmount to set
     */
    public void setTotalprofitAmount(long totalprofitAmount) {
        this.totalprofitAmount = totalprofitAmount;
    }

    public String getStyleColor() {
        return getStyleColor(totalprofitAmount);
    }

    public String getStyleColor(long total) {
        if ( total > 0 ) {
            return "user_strip_value_darkgreen";
        } else {
            return "user_strip_value_red";
        }
    }
    
    /**
     * @return if the transaction has bookBack
     */
    public boolean isBookBack() {
		return isBookBack;
	}
    
    /**
     * @param isBookBack the isBookBack to set
     */
	public void setBookBack(boolean isBookBack) {
		this.isBookBack = isBookBack;
	}

	
}