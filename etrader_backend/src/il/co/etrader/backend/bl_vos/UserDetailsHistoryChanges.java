package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

import il.co.etrader.util.CommonUtil;

public class UserDetailsHistoryChanges implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String field;
	private String oldValue;
	private String newValue;
	private String changedBy;
	private Date timeDate;
	private long writerId;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public Date getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(Date timeDate) {
		this.timeDate = timeDate;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormat(timeDate);
	}

}
