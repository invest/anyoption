package il.co.etrader.backend.bl_vos;


import java.io.Serializable;

import com.anyoption.common.util.ClearingUtil;

/**
 * CaptureInfo class
 * for saving daily capture information
 *
 * @author Kobi
 */
public class CaptureDailyInfo implements Serializable  {
	private static final long serialVersionUID = 415005142412294274L;

	private long userId;
	private String userName;
	private long transactionId;
	private String ccNumber;
	private String currencyCode;
	private long amount;
	private String skinName;
	private String providerName;


	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	public String getAmountTxt() {
		return ClearingUtil.formatAmount(amount);
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * @return the ccNumber
	 */
	public String getCcNumber() {
		return ccNumber;
	}

	/**
	 * @param ccNumber the ccNumber to set
	 */
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}

	/**
	 * @param providerName the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String ls = System.getProperty("line.seperator");
	    return "CaptureDailyInfo ( "
	        + super.toString() + ls +
	    	"userId: " + userId + ls +
	    	"userName: " + userName + ls +
	    	"transactionId: " + transactionId + ls +
	    	"ccNumber: " + ccNumber + ls +
	    	"currencyCode: " + currencyCode + ls +
	    	"amount: " + amount + ls +
	    	"skinName: " + skinName + ls +
	    	"providerName: " + providerName + ls +
	         " )";
	}

}