package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * PopulationUsersAssignmentInfo
 * @author eyal.ohana
 *
 */
public class PopulationUsersAssignmentInfo implements Serializable {
	private static final long serialVersionUID = -523997744476256246L;
	
	private boolean isAssignmentOk;
	private boolean isReachedTotalLimit;

	/**
	 * @return the isReachedTotalLimit
	 */
	public boolean isReachedTotalLimit() {
		return isReachedTotalLimit;
	}

	/**
	 * @param isReachedTotalLimit the isReachedTotalLimit to set
	 */
	public void setReachedTotalLimit(boolean isReachedTotalLimit) {
		this.isReachedTotalLimit = isReachedTotalLimit;
	}

	/**
	 * @return the isAssignmentOk
	 */
	public boolean isAssignmentOk() {
		return isAssignmentOk;
	}

	/**
	 * @param isAssignmentOk the isAssignmentOk to set
	 */
	public void setAssignmentOk(boolean isAssignmentOk) {
		this.isAssignmentOk = isAssignmentOk;
	}
}
