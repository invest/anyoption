package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.ApiIssue;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;



/**
 * @author EranL
 *
 */
public class ApiIssuesDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(ApiIssuesDAO.class);
	  
	/**
	 * Insert API issue 
	 * @param con
	 * @param apiIssue
	 * @throws SQLException
	 */
	public static void insertApiIssue(Connection  con, ApiIssue apiIssue) throws SQLException { 
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql =" INSERT INTO " +
                              " api_issues(id, time_created, writer_id, utc_offset, api_external_user_id, api_issue_status_id, api_issue_channel_id, " +
                              " 			api_issue_subject_id, comments) " +
                          " VALUES " +
                              " (seq_api_issues.NEXTVAL, sysdate, ?, ?, ?, ?, ?, ?, ?) ";
              ps = con.prepareStatement(sql);
              ps.setLong(1, apiIssue.getWriter_id());             
              ps.setString(2, apiIssue.getUtcOffset());
              ps.setLong(3, apiIssue.getApiExternalUserId());
              ps.setLong(4, apiIssue.getApiIssueStatusId());
              ps.setLong(5, apiIssue.getApiIssueChannelId());
              ps.setLong(6, apiIssue.getApiIssueSubjectId());
              ps.setString(7, apiIssue.getComments());                           
              ps.executeUpdate();
              apiIssue.setId(getSeqCurValue(con, "seq_api_issues"));              
        } finally {
              closeStatement(ps);
              closeResultSet(rs);
        }
	}
	
	/**
	 * Update API issue 
	 * @param con
	 * @param apiIssue
	 * @throws SQLException
	 */
	public static void updateApiIssue(Connection  con, ApiIssue apiIssue) throws SQLException { 
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql =" UPDATE " +
                          " 	api_issues " +
                          "	SET " +
                          "		writer_id = ?, " +
                          "		utc_offset = ?, " +
                          "		api_issue_status_id = ?, " +
                          "		comments = ? " +
                          " WHERE " +
                          "		id = ? ";                
              ps = con.prepareStatement(sql);
              ps.setLong(1, apiIssue.getWriter_id());             
              ps.setString(2, apiIssue.getUtcOffset());
              ps.setLong(3, apiIssue.getApiIssueStatusId());
              ps.setString(4, apiIssue.getComments());                           
              ps.setLong(5, apiIssue.getId());
              ps.executeUpdate();              
        } finally {
              closeStatement(ps);
              closeResultSet(rs);
        }
	}
	
	/**
	 * Search API issues for external user
	 * @param con
	 * @param apiExternalUserId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ApiIssue> searchApiIssues(Connection con, long apiExternalUserId, Date from, Date to, long statusId, long channelId, long subjectId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ApiIssue> list = new  ArrayList<ApiIssue>();
		int index = 1;
		try {
			String sql =" SELECT " +
		    			"	i.*, " +
		    			"	sub.name issue_subject_name, " +
		    			"	stat.name issue_status_name, " +
		    			"	chan.name issue_channel_name, " +
		    			"	w.user_name writer_name " +
		    			" FROM " +
		    			"	api_issues i, " +
		    			"	api_issue_subject sub, " +
		    			"	api_issue_status stat, " +
		    			"	api_issue_channel chan, " +
		    			"	writers w " +
		    			" WHERE " +
		    			"	i.api_issue_status_id = stat.id " +
		    			"	AND i.api_issue_channel_id = chan.id " +
		    			"	AND i.api_issue_subject_id = sub.id " +
		    			"	AND i.writer_id = w.id " +
		    			"	AND i.api_external_user_id = ? " +	    			
		    			"	AND i.time_created BETWEEN ? AND ? ";
						if (statusId != 0) {
					sql +="	AND i.api_issue_status_id = ? ";							
						}
						if (subjectId != 0) {
					sql +="	AND i.api_issue_subject_id = ? ";							
						}
						if (channelId != 0) {
					sql +="	AND i.api_issue_channel_id = ? ";							
						}						
					sql+=" ORDER BY " +
		    			"	i.id DESC ";
		    ps = con.prepareStatement(sql);
		    ps.setLong(index++, apiExternalUserId);
		    ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(from));
		    ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(to));
		    if (statusId != 0) {
		    	ps.setLong(index++, statusId);
		    }
		    if (subjectId != 0) {
		    	ps.setLong(index++, subjectId);
		    }
		    if (channelId != 0) {
		    	ps.setLong(index++, channelId);
		    }
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(getVO(rs));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get API issue VO
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static ApiIssue getVO(ResultSet rs) throws SQLException {
		ApiIssue vo = new ApiIssue();
		vo.setId(rs.getLong("ID"));
		vo.setApiExternalUserId(rs.getLong("API_EXTERNAL_USER_ID"));
		vo.setApiIssueChannelId(rs.getLong("API_ISSUE_CHANNEL_ID"));
		vo.setApiIssueStatusId(rs.getLong("API_ISSUE_STATUS_ID"));
		vo.setApiIssueSubjectId(rs.getLong("API_ISSUE_SUBJECT_ID"));
		vo.setComments(rs.getString("COMMENTS"));
		vo.setWriter_id(rs.getLong("WRITER_ID"));
		vo.setUtcOffset(rs.getString("UTC_OFFSET"));		
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setWriterName(rs.getString("WRITER_NAME"));
		vo.setIssueChannelName(rs.getString("ISSUE_CHANNEL_NAME"));
		vo.setIssueSubjectName(rs.getString("ISSUE_SUBJECT_NAME"));
		vo.setIssueStatusName(rs.getString("ISSUE_STATUS_NAME"));		
		return vo;
	}
	  
	/**
	 * Get All API issue channels into a hash table
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getApiIssueChannelSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	api_issue_channel ";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get All API issue subjects into a hash table
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getApiIssueSubjectSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	api_issue_subject ";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get All API issue statuses into a hash table
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getApiIssueStatusSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	api_issue_status ";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
				}
			} finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
			return list;
	}		

}
