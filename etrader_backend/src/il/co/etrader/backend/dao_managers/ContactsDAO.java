package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;

public class ContactsDAO extends DAOBase {
	

	public static ArrayList<Contact> getSMSContactsFromFile(Connection con, String usersCSV) throws SQLException {
			PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        ArrayList<Contact> list = new ArrayList<Contact>();
	        try {
	            String sql = "SELECT " +
							            "c.id, " +
							            "c.mobile_phone, " +
							            "c.skin_id, " +
							            "c.country_id, " +
							            "c.utc_offset, " +
							            "c.is_contact_by_sms " +
					         "FROM " +
							            " contacts c " +
					         "WHERE " +
					         			" c.id in ( " + usersCSV + " ) " +
					         "AND " +
					         			" is_contact_by_sms = 1 " ;
					         	

	            pstmt = con.prepareStatement(sql);
	            rs = pstmt.executeQuery();

	            while (rs.next()) {
	            	Contact vo = new Contact();
	                vo.setId(rs.getLong("id"));
	                vo.setMobilePhone(rs.getString("mobile_phone"));
	                vo.setSkinId(rs.getLong("skin_id"));
	                vo.setCountryId(rs.getLong("country_id"));
	                vo.setUtcOffset(rs.getString("utc_offset"));
	                vo.setContactBySMS(rs.getLong("is_contact_by_sms")== 1);
	                list.add(vo);
	            }

	        } finally {
	            closeStatement(pstmt);
	        }

	        return list;
		}
	
	

	public static ArrayList<Contact> getSMSContacts(Connection conn, String skinIds, String countryIds, String campaignIds, String populationType) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Contact> list = new ArrayList<Contact>();
        try {
            String sql = 
						"SELECT " +
									"c.id, " +
									"c.mobile_phone, " +
									"c.skin_id, " +
									"c.country_id, " +
									"c.utc_offset, " +
									"c.is_contact_by_sms " +
						"FROM " +
									" contacts c " +
									"LEFT JOIN Countries cn ON cn.id = c.id " + 
									"LEFT JOIN population_users pu ON pu.contact_id = c.id " + 
									"LEFT JOIN population_entries pe ON pe.id = pu.curr_population_entry_id " + 
									"LEFT JOIN Populations p ON p.id = pe.population_Id " +
									"LEFT JOIN marketing_combinations mco ON mco.id = c.combination_id " +
						"WHERE  " +
									"c.is_contact_by_sms = 1 " +
									"AND EXISTS (SELECT 1 FROM countries cn WHERE cn.Id = c.country_id AND cn.is_sms_available = 1) " +
									"AND c.user_Id = 0 " +
									"AND c.mobile_phone IS NOT NULL " ;
						            if (null != countryIds && !countryIds.equals("0")) {
						                sql += "AND c.country_id in (" + countryIds + " )  ";
						            }
									if (null != skinIds && !skinIds.equals(Skin.SKINS_ALL)) {
										sql += "AND c.skin_id in ("+skinIds+") "; 
									}
									if (null != campaignIds && !campaignIds.equals("0")) {
										sql += "AND mco.campaign_id in ("+campaignIds+") ";
									}
									if (null != populationType && !populationType.contains("0")) {
										sql += "AND p.population_type_id in (" + populationType + ")  ";
									}
            
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	Contact vo = new Contact();
                vo.setId(rs.getLong("id"));
                vo.setMobilePhone(rs.getString("mobile_phone"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setCountryId(rs.getLong("country_id"));
                vo.setUtcOffset(rs.getString("utc_offset"));
                vo.setContactBySMS(rs.getLong("is_contact_by_sms")== 1);
                list.add(vo);
            }
        } finally {
            closeStatement(pstmt);
        }
        return list;
	}



	public static void setContactsToPromotionsTable( Connection conn, ArrayList<Contact> contactsList, Long templateId, HttpSession session) throws SQLException {
	    	int sumOfContactsList = contactsList.size();
	    	final int batchSize = 1000;
			int count = 0;
			double i=0;	
			String percentage = "";
			String sql = "INSERT INTO " +
					  						"promotions_entries_contacts " +
					  						"(" +
					  							"id, " +
					  							"contact_id, " +
					  							"promotion_id, " +
					  							"status_id, " +
					  							"time_created " +
					  						" ) " +
					  	"VALUES" +
					  						"(" +
					  							"SEQ_PROMO_ENTRIES_CONTACTS.nextval, " +
					  							" ? ," +
					  							"SEQ_PROMOTION_ID.currval, " +
					  							Constants.PROMOTION_STATUS_ID_CREATED + ", " +
					  							"sysdate " +
					  						")";
			PreparedStatement ps = conn.prepareStatement(sql);
			try{
				for(Contact c: contactsList){			
					ps.setLong(1 , c.getId());
					ps.addBatch();
					count++;
					i++;
				}
				percentage = Double.toString(((i/sumOfContactsList)*100));
	        	session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS,sumOfContactsList+ "_" + percentage.substring(0, percentage.indexOf(".")) );
	        	
				if(count % batchSize == 0) {
			        ps.executeBatch();
			    }
				
			} finally {
				ps.executeBatch();
				ps.close();
			}

	}
	
	public static ArrayList<Contact> getContactsListByPromotionId(Connection con, long promotionId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Contact> list = new ArrayList<Contact>();
        try {
            String sql = 	
				           "SELECT " +
				    				"c.id, " +
				    				"c.mobile_phone, " +
				    				"c.skin_id, " +
				    				"c.country_id, " +
				    				"c.name, " +
				    				"c.utc_offset " +
				    		"FROM  " +
				    				"contacts c, " +
				    				"promotions p, " +
				    				"promotions_entries_contacts pe, " +
				    				"countries cn " +
				    		"WHERE " +
				    				" c.id = pe.contact_id AND " +
				    				" p.id = pe.promotion_id AND " +
				    				" c.country_id = cn.id AND " +
				    				" cn.is_sms_available = 1 AND " +
				    				" p.id = ? ";
            
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, promotionId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	Contact vo = new Contact();
                vo.setId(rs.getLong("id"));
                vo.setMobilePhone(rs.getString("mobile_phone"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setCountryId(rs.getLong("country_id"));
                vo.setUtcOffset(rs.getString("utc_offset"));
                vo.setContactBySMS(true);
                list.add(vo);
            }

        } finally {
            closeStatement(pstmt);
        }

        return list;
	}
	
	/**
	 * @param con
	 * @param contactId
	 * @return
	 * @throws SQLException
	 */
	public static String getLastWriterIdChat(Connection con, long contactId) throws SQLException {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        String writerName = "";
        
        try {
            String sql =
	            	"SELECT " +
	        		"		last_chat_writer_id, " +
	        		"		w.user_name " +
	            	"FROM " +
	        		"		contacts_additional_info cai " +
	        		"	INNER JOIN " +
	        		"		writers w " +
	        		"	ON " +
	        		"		cai.last_chat_writer_id =  w.id " +
	            	"WHERE " +
	        		"		cai.contact_id = ? " +
	        		"ORDER BY " +
	        		"		cai.time_created desc ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, contactId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	writerName = rs.getString("user_name");
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return writerName;
    }

}