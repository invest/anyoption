package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.managers.BaseBLManager;

public class MarketsManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(MarketsManager.class);

	private static LocalDate dateMarketsAndGroups;
	private static LocalDate dateActiveMarketsAndGroups;
	private static LocalDate dateActiveBinary0100MarketsAndGroups;
	private static LocalDate dateActiveMarketsOneTouch;
	private static LocalDate dateActiveMarkets;
	private static LocalDate dateMarketsByAgreement;
	private static LocalDate dateAllMarkets;
	private static LocalDate dateMarketGroups;
	private static LocalDate dateMarketsZeroOneHundred;
	private static LocalDate dateMarketsDynamics;
	private static LocalDate dateMarketsOptionPlus;
	private static LocalDate dateMarketsByGroups;
	
	private static HashMap<String, String> marketsAndGroups;
	private static HashMap<String, String> activeMarketsAndGroups;
	private static HashMap<String, String> activeBinary0100MarketsAndGroups;
	private static HashMap<String, String> activeMarketsOneTouch;
	private static HashMap<Long, String> activeMarkets;
	private static HashMap<Long, String> marketsByAgreement;
	private static LinkedHashMap<Long, String> allMarkets;
	private static HashMap<String, String> marketGroups;
	private static HashMap<String, String> marketsZeroOneHundred;
	private static HashMap<Long, String> marketsDynamics;
	private static HashMap<Long, String> marketsOptionPlus;
	private static LinkedHashMap<Long, MarketGroup> marketsByGroups;

	public static HashMap<String, String> getMarketsAndGroups() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsAndGroups || currentDate.isAfter(dateMarketsAndGroups) || null == marketsAndGroups) {
			try {
				con = getConnection();
				if (null == marketsAndGroups) {
					marketsAndGroups = new HashMap<String, String>();
				}
				marketsAndGroups = MarketsDAO.getMarketsAndGroups(con);
				dateMarketsAndGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load markets and groups", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsAndGroups;
	}

	public static HashMap<String, String> getActiveMarketsAndGroups() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateActiveMarketsAndGroups || currentDate.isAfter(dateActiveMarketsAndGroups)
				|| null == activeMarketsAndGroups) {
			try {
				con = getConnection();
				if (null == activeMarketsAndGroups) {
					activeMarketsAndGroups = new HashMap<String, String>();
				}
				activeMarketsAndGroups = MarketsDAO.getActiveMarketsAndGroups(con);
				dateActiveMarketsAndGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load active markets and groups", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return activeMarketsAndGroups;
	}

	public static HashMap<String, String> getActiveBinary0100MarketsAndGroups() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateActiveBinary0100MarketsAndGroups || currentDate.isAfter(dateActiveBinary0100MarketsAndGroups)
				|| null == activeBinary0100MarketsAndGroups) {
			try {
				con = getConnection();
				if (null == activeBinary0100MarketsAndGroups) {
					activeBinary0100MarketsAndGroups = new HashMap<String, String>();
				}
				activeBinary0100MarketsAndGroups = MarketsDAO.getActiveBinary0100MarketsAndGroups(con);
				dateActiveBinary0100MarketsAndGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load active binary 0100 markets and groups", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return activeBinary0100MarketsAndGroups;
	}

	public static HashMap<String, String> getActiveMarketsAndGroupsWithOneTouch() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateActiveMarketsOneTouch || currentDate.isAfter(dateActiveMarketsOneTouch)
				|| null == activeMarketsOneTouch) {
			try {
				con = getConnection();
				if (null == activeMarketsOneTouch) {
					activeMarketsOneTouch = new HashMap<String, String>();
				}
				activeMarketsOneTouch = MarketsDAO.getActiveMarketsAndGroupsWithOneTouch(con);
				dateActiveMarketsOneTouch = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load active markets and groups with one touch", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return activeMarketsOneTouch;
	}

	public static HashMap<Long, String> getActiveMarkets(long opportunityTypeId) {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateActiveMarkets || currentDate.isAfter(dateActiveMarkets) || null == activeMarkets) {
			try {
				con = getConnection();
				if (null == activeMarkets) {
					activeMarkets = new HashMap<Long, String>();
				}
				activeMarkets = MarketsDAO.getActiveMarkets(con, opportunityTypeId);
				dateActiveMarkets = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load active markets ", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return activeMarkets;
	}

	public static HashMap<Long, String> getMarketsByAgreement() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsByAgreement || currentDate.isAfter(dateMarketsByAgreement)
				|| null == marketsByAgreement) {
			try {
				con = getConnection();
				if (null == marketsByAgreement) {
					marketsByAgreement = new HashMap<Long, String>();
				}
				marketsByAgreement = MarketsDAO.getMarketsByAgreement(con);
				dateMarketsByAgreement = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load markets by agreements", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsByAgreement;
	}

	public static HashMap<Long, String> getAllMarkets() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateAllMarkets || currentDate.isAfter(dateAllMarkets) || null == allMarkets) {
			try {
				con = getConnection();
				if (null == allMarkets) {
					allMarkets = new LinkedHashMap<Long, String>();
				}
				HashMap<Long, Market> map = new HashMap<Long, Market>();
				map = MarketsDAO.getAllMarkets(con);

				for (Map.Entry<Long, Market> entry : map.entrySet()) {
					if (allMarkets.get(entry.getKey()) == null) {
						allMarkets.put(entry.getKey(), entry.getValue().getDisplayNameKey());
					}
				}

			} catch (SQLException sqle) {
				logger.error("Cannot load all markets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return allMarkets;
	}

	public static HashMap<String, String> getMarketGroups() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketGroups || currentDate.isAfter(dateMarketGroups) || null == marketGroups) {
			try {
				con = getConnection();
				if (null == marketGroups) {
					marketGroups = new HashMap<String, String>();
				}
				HashMap<Long, MarketGroup> map = MarketsDAO.getMarketGroups(con);
				for (Map.Entry<Long, MarketGroup> entry : map.entrySet()) {
					marketGroups.put(entry.getKey().toString(), entry.getValue().getDisplayName());
				}
				dateMarketGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load market groups", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketGroups;
	}

	public static HashMap<String, String> getAllZeroOneHundredMarkets() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsZeroOneHundred || currentDate.isAfter(dateMarketsZeroOneHundred)
				|| null == marketsZeroOneHundred) {
			try {
				con = getConnection();
				if (null == marketsZeroOneHundred) {
					marketsZeroOneHundred = new HashMap<String, String>();
				}
				HashMap<Long, Market> map = MarketsDAO.getAllZeroOneHundredMarkets(con);
				for (Map.Entry<Long, Market> entry : map.entrySet()) {
					marketsZeroOneHundred.put(entry.getKey().toString(), entry.getValue().getDisplayNameKey());
				}
				dateMarketsZeroOneHundred = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load zero one hundred markets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsZeroOneHundred;
	}
	
	public static HashMap<Long, String> getDynamicsMarkets() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsDynamics || currentDate.isAfter(dateMarketsDynamics)	|| null == marketsDynamics) {
			try {
				con = getConnection();
				if (null == marketsDynamics) {
					marketsDynamics = new HashMap<Long, String>();
				}
				marketsDynamics = MarketsDAO.getAllMarketsByType(con, Market.PRODUCT_TYPE_DYNAMICS);
				dateMarketsDynamics = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load dynamics markets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsDynamics;
	}
	
	public static HashMap<Long, String> getOptionPlusMarkets() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsOptionPlus || currentDate.isAfter(dateMarketsOptionPlus)	|| null == marketsOptionPlus) {
			try {
				con = getConnection();
				if (null == marketsOptionPlus) {
					marketsOptionPlus = new HashMap<Long, String>();
				}
				marketsOptionPlus = MarketsDAO.getAllMarketsByType(con, Market.PRODUCT_TYPE_OPTION_PLUS);
				dateMarketsOptionPlus = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load option plus markets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsOptionPlus;
	}
	
	public static LinkedHashMap<Long, MarketGroup> getMarketsByGroups() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketsByGroups || currentDate.isAfter(dateMarketsByGroups) || null == marketsByGroups) {
			try {
				con = getConnection();
				if (null == marketsByGroups) {
					marketsByGroups = new LinkedHashMap<Long, MarketGroup>();
				}
				marketsByGroups = MarketsDAO.getMarketsByGroups(con);
				dateMarketsByGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load zero one hundred markets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return marketsByGroups;
	}
	
	public static long getMarketIdByOppId (long oppId) {
		Connection con = null;
		long marketId = 0;
		try {
			con = getConnection();
			marketId = MarketsDAO.getMarketIdByOppId(con, oppId);
		} catch (SQLException sqle) {
			logger.error("Cannot load zero one hundred markets", sqle);
		} finally {
			closeConnection(con);
		}
		return marketId;
	}
}
