package il.co.etrader.backend.dao_managers;

import java.util.Date;

public class FilesDAOFilter {

	long userId;
	long fileTypeId;
	int uploaded;
	int supportApproved;
	Date receivedAtFrom;
	Date receivedAtTo;
	boolean isCurrentFiles;
	
	public FilesDAOFilter() {
		
	}
	
	public FilesDAOFilter(long userId, long fileTypeId, int uploaded,
			int approved, Date receivedAtFrom, Date receivedAtTo) {
		super();
		this.userId = userId;
		this.fileTypeId = fileTypeId;
		this.uploaded = uploaded;
		this.supportApproved = approved;
		this.receivedAtFrom = receivedAtFrom;
		this.receivedAtTo = receivedAtTo;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getFileTypeId() {
		return fileTypeId;
	}
	public void setFileTypeId(long fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	public int getUploaded() {
		return uploaded;
	}
	public void setUploaded(int uploaded) {
		this.uploaded = uploaded;
	}
	public Date getReceivedAtFrom() {
		return receivedAtFrom;
	}
	public void setReceivedAtFrom(Date receivedAtFrom) {
		this.receivedAtFrom = receivedAtFrom;
	}
	public Date getReceivedAtTo() {
		return receivedAtTo;
	}
	public void setReceivedAtTo(Date receivedAtTo) {
		this.receivedAtTo = receivedAtTo;
	}
	public int getSupportApproved() {
		return supportApproved;
	}
	public void setSupportApproved(int supportApproved) {
		this.supportApproved = supportApproved;
	}

	public boolean isCurrentFiles() {
		return isCurrentFiles;
	}

	public void setCurrentFiles(boolean isCurrentFiles) {
		this.isCurrentFiles = isCurrentFiles;
	}
}
