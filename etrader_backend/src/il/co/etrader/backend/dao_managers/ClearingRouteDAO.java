package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.ClearingRoute;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.daos.DAOBase;

/**
 * @author liors
 *
 */
public class ClearingRouteDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(ClearingRouteDAO.class);
	
 	public static ArrayList<ClearingRoute> getClearingRoutes(Connection conn) throws SQLException {
 		ArrayList<ClearingRoute> clearingRoute = new ArrayList<ClearingRoute>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                "	cr.id as cr_id " +
                "	,cr.* " +
                "	,cp_deposit.name as  cp_deposit_name " +
                "	,cp_withdraw.name as  cp_withdraw_name " +
                "	,cp_cft.name as  cp_cft_name " +
                "	,sbc.description as sbc_description " +
                "	,cu.display_name as cu_display_name " +
                "	,cct.name as cct_name " +
                "	,co.country_name " +
                "	,s.display_name as s_display_name " +
                "	,p.id as p_id " +
                "	,p.name as p_name " +
                "	,p.display_name as p_display_name " +
                "	,p.comments as p_comments " +
				"FROM " +
			   	"	clearing_routes cr " +
			   	"	left join skin_business_cases sbc on cr.business_skin_id = sbc.id " +
			   	"	left join clearing_providers cp_deposit on cr.deposit_clearing_provider_id = cp_deposit.id " +
			   	"	left join clearing_providers cp_withdraw on cr.withdraw_clearing_provider_id = cp_withdraw.id " +
			   	"	left join clearing_providers cp_cft on cr.cft_clearing_provider_id = cp_cft.id " +
			   	"	left join currencies cu on cr.currency_id = cu.id " +
			   	"	left join credit_card_types cct on cr.cc_type = cct.id " +
			   	"	left join countries co on cr.country_id = co.id " +
			   	"	left join skins s on cr.skin_id = s.id " +
			   	"	left join platforms p  on cr.platform_id = p.id " +
				"ORDER BY " +
			   	" 	cr.start_bin " +
                " 	,cr.currency_id " +
			   	" 	,cr.cc_type " +
                " 	,cr.country_id " +
                " 	,cr.skin_id " +
                " 	,cr.business_skin_id ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	ClearingRoute cr = new ClearingRoute();
            	cr.setId(rs.getLong("cr_id"));            	
            	cr.setStartBIN(rs.getString("start_bin"));
            	cr.setEndBIN(rs.getString("end_bin"));
            	cr.setCurrnecyId(rs.getLong("currency_id"));
            	cr.setCcType(rs.getLong("cc_type"));
            	cr.setCountryId(rs.getLong("country_id"));
            	cr.setSkinId(rs.getLong("skin_id"));
            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
            	cr.setComments(rs.getString("comments"));
            	cr.setActive(rs.getBoolean("is_active"));
            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
            	cr.setDepositProviderName(rs.getString("cp_deposit_name"));
            	cr.setWithdrawProviderName(rs.getString("cp_withdraw_name"));
            	cr.setCFTProviderName(rs.getString("cp_cft_name"));
            	cr.setBusinessSkinName(rs.getString("sbc_description"));
            	cr.setCurrencyName(rs.getString("cu_display_name"));
            	cr.setCreditCardName(rs.getString("cct_name"));
            	cr.setCountryName(rs.getString("country_name"));
            	cr.setSkinName(rs.getString("s_display_name"));
            	cr.setPlatform(new Platform(rs.getInt("p_id"), rs.getString("p_name"), rs.getString("p_comments")));
            	clearingRoute.add(cr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        
        return clearingRoute;
    }

	public static boolean insertClearingRoute(Connection connection, ClearingRoute clearingRoute) throws SQLException {
		logger.info("about to insert record to clearing_routes;  " + clearingRoute.toString());
        PreparedStatement pstmt = null;
        int index = 0;
        try {
            String sql = 
            		"INSERT into clearing_routes" +
            		"	(" +
            		"	id" +
            		"	,start_bin" +
            		"	,end_bin" +
            		"	,currency_id" +
            		"	,cc_type" +
            		"	,country_id" +
            		"	,skin_id" +
            		"	,business_skin_id" +
            		"	,deposit_clearing_provider_id" +
            		"	,withdraw_clearing_provider_id" +
            		"	,cft_clearing_provider_id" +
            		"	,comments" +
            		"	,is_active" +
            		//"	,time_last_success" +
            		//"	,time_last_reroute" +
            		"	,platform_id" +
            		"	,time_created" +
            		"	)" +
            		"VALUES" +
            		"	(" +
            		"	seq_clearing_routes.nextval" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,?" +
            		"	,sysdate" +
        			")";

            pstmt = connection.prepareStatement(sql);
            
            pstmt.setString(++index, clearingRoute.getStartBIN());
            pstmt.setString(++index, clearingRoute.getEndBIN());
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCurrnecyId() != 0) {
            	pstmt.setLong(index, clearingRoute.getCurrnecyId());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCcType() != 0) {
            	pstmt.setLong(index, clearingRoute.getCcType());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCountryId() != 0) {
            	pstmt.setLong(index, clearingRoute.getCountryId());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getSkinId() != 0) {
            	pstmt.setLong(index, clearingRoute.getSkinId());
            }
            pstmt.setLong(++index, clearingRoute.getBusinessSkinId());
            pstmt.setLong(++index, clearingRoute.getDepositClearingProviderId());
            pstmt.setLong(++index, clearingRoute.getWithdrawClearingProviderId());
            pstmt.setLong(++index, clearingRoute.getCFTClearingProviderId());
            pstmt.setString(++index, clearingRoute.getComments());
            pstmt.setBoolean(++index, clearingRoute.isActive());
            pstmt.setInt(++index, clearingRoute.getPlatform().getId());
            
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        
		return true;
	}

	public static boolean updateClearingRoute(Connection connection, ClearingRoute clearingRoute) throws SQLException {
		logger.info("about to update record to clearing_routes;  " + clearingRoute.toString());
        PreparedStatement pstmt = null;
        int index = 0;
        try {
            String sql = 
            		"UPDATE " +
            		"	clearing_routes " +
            		"SET " +
            		"	start_bin = ?" +
            		"	,end_bin = ?" +
            		"	,currency_id = ?" +
            		"	,cc_type = ?" +
            		"	,country_id = ?" +
            		"	,skin_id = ?" +
            		"	,business_skin_id = ?" +
            		"	,deposit_clearing_provider_id = ?" +
            		"	,withdraw_clearing_provider_id = ?" +
            		"	,cft_clearing_provider_id = ?" +
            		"	,comments = ?" +
            		"	,is_active = ?" +
            		"	,platform_id = ? " +
            		"WHERE " +
            		"	id = ? ";

            pstmt = connection.prepareStatement(sql);
            
            pstmt.setString(++index, clearingRoute.getStartBIN());
            pstmt.setString(++index, clearingRoute.getEndBIN());
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCurrnecyId() != 0) {
            	pstmt.setLong(index, clearingRoute.getCurrnecyId());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCcType() != 0) {
            	pstmt.setLong(index, clearingRoute.getCcType());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getCountryId() != 0) {
            	pstmt.setLong(index, clearingRoute.getCountryId());
            }
            pstmt.setNull(++index, Types.NUMERIC);
            if (clearingRoute.getSkinId() != 0) {
            	pstmt.setLong(index, clearingRoute.getSkinId());
            }
            pstmt.setLong(++index, clearingRoute.getBusinessSkinId());
            pstmt.setLong(++index, clearingRoute.getDepositClearingProviderId());
            pstmt.setLong(++index, clearingRoute.getWithdrawClearingProviderId());
            pstmt.setLong(++index, clearingRoute.getCFTClearingProviderId());
            pstmt.setString(++index, clearingRoute.getComments());
            pstmt.setBoolean(++index, clearingRoute.isActive());
            pstmt.setInt(++index, clearingRoute.getPlatform().getId());
            pstmt.setLong(++index, clearingRoute.getId());
            
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        
		return true;
	}
}