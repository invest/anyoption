package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.dao_managers.MarketingPixelsDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

public class MarketingPixelsDAO extends MarketingPixelsDAOBase {


	/**
	 * Insert a new pixel
	 * @param con  db connection
	 * @param vo MarketingPixel instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingPixel vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_pixels(id,name,http_code,https_code,writer_id,time_created,type_id) " +
						     "values(seq_mar_pixels.nextval,?,?,?,?,sysdate,?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setString(2, vo.getHttpCode());
				ps.setString(3, vo.getHttpsCode());
				ps.setLong(4, vo.getWriterId());
				ps.setLong(5, vo.getTypeId());
				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_pixels"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing pixel
	   * @param con db connection
	   * @param vo  MarketingPixel instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingPixel vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_pixels set name=?,http_code=?,https_code=?,writer_id=?,type_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setString(2, vo.getHttpCode());
				ps.setString(3, vo.getHttpsCode());
				ps.setLong(4, vo.getWriterId());
				ps.setLong(5, vo.getTypeId());
				ps.setLong(6, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all pixels
	   * @param con  db connection
	   * @param pixelName  pixel name filter
	   * @return
	   *	ArrayList of MarketingPixel
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingPixel> getAll(Connection con, String pixelName, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingPixel> list = new ArrayList<MarketingPixel>();

		  try {

				String sql = "SELECT " +
								"mp.*, " +
								"pt.description " +
							 "FROM " +
							 	"marketing_pixels mp, " +
							 	"marketing_pixel_types pt " +
							 "WHERE " +
							 	"mp.type_id = pt.id ";


				if ( !CommonUtil.isParameterEmptyOrNull(pixelName) ) {
					sql += "AND upper(mp.name) like '%" + pixelName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and mp.writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}


				sql += "order by mp.id";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingPixel vo = getVO(rs);
					vo.setTypeId(rs.getLong("type_id"));
					vo.setTypeName(rs.getString("description"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if pixel name in use
	   * @param con   db connection
	   * @param name  pixel name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_pixels " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get all pixel types
	   * @param con
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getAllPixelTypes(Connection con) throws SQLException {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
	        try{
	            String sql =
	            	"SELECT * " +
	            	"FROM marketing_pixel_types " +
	            	"ORDER BY code";
	            ps = con.prepareStatement(sql);
	            rs = ps.executeQuery();
	            while ( rs.next() ) {
	                hm.add(new SelectItem(rs.getLong("id"), rs.getString("description")));
	            }
	        } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	        }
	        return hm;
	    }

	  /**
	   * Get pixels list by combId
	   * @param con
	   * @param combId
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getPixelsByCombIdSI(Connection con, long combId) throws SQLException {
		  PreparedStatement ps = null;
	      ResultSet rs = null;
	      ArrayList<SelectItem> list = new ArrayList<SelectItem>();
	      try{
	            String sql =
	            	"SELECT " +
	            		"mp.id, " +
	            		"mp.name " +
	            	"FROM " +
	            		"marketing_combination_pixels mcp, " +
	            		"marketing_pixels mp " +
	            	"WHERE " +
	            		"mcp.combination_id = ? AND " +
	            		"mcp.pixel_id = mp.id ";

	            ps = con.prepareStatement(sql);
	            ps.setLong(1, combId);
	            rs = ps.executeQuery();
	            while (rs.next()) {
	            	list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
	            }
	      } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	      }
	      return list;
	  }

	  /**
	   * Insert new pixel for combination
	   * @param con
	   * @param combId combination id for inserting the new pixel
	   * @param pixelId
	   * @throws SQLException
	   */
	  public static void insertIntoCombinationPixels(Connection con, long combId, long pixelId ) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  try {
					String sql = "insert into marketing_combination_pixels(id, combination_id, pixel_id) " +
							     "values(seq_marketing_comb_pixels.nextval,?,?)";

					ps = con.prepareStatement(sql);
					ps.setLong(1, combId);
					ps.setLong(2, pixelId);
					ps.executeUpdate();
			  } finally {
					closeStatement(ps);
					closeResultSet(rs);
				}
		}
	  
	  public static void insertIntoCombinationPixels(Connection con, long combId, ArrayList<Long> pixelsList ) throws SQLException {
	      for (Long pixel : pixelsList){
	          if (pixel != 0){
	              insertIntoCombinationPixels(con, combId, pixel);   
	          }
	      }
	  }

	/**
	 * Delete pixel for combination
	 * @param con
	 * @param combId  combination id
	 * @param pixelId pixel id to remove
	 * @throws SQLException
	 */
	public static void deletePixelByCombId(Connection con, long combId, long pixelId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "DELETE " +
						 "FROM " +
						 	"marketing_combination_pixels " +
						 "WHERE " +
						 	"combination_id = ? AND " +
						 	"pixel_id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, combId);
			ps.setLong(2, pixelId);
			ps.executeUpdate();
	  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Get pixels list by affiliateId
	   * @param con
	   * @param combId
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getPixelsByAffiliateSI(Connection con, long affiliateId) throws SQLException {
		  PreparedStatement ps = null;
	      ResultSet rs = null;
	      ArrayList<SelectItem> list = new ArrayList<SelectItem>();
	      try{
	            String sql =
	            	"SELECT " +
	            		"mp.id, " +
	            		"mp.name " +
	            	"FROM " +
	            		"marketing_affilate_pixels map, " +
	            		"marketing_pixels mp " +
	            	"WHERE " +
	            		"map.affiliate_id = ? AND " +
	            		"map.pixel_id = mp.id ";

	            ps = con.prepareStatement(sql);
	            ps.setLong(1, affiliateId);
	            rs = ps.executeQuery();
	            while (rs.next()) {
	            	list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
	            }
	      } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	      }
	      return list;
	  }

	  /**
	   * Get pixels list by affiliateId
	   * @param con
	   * @param combId
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getPixelsBySubAffiliateSI(Connection con, long subAffiliateId) throws SQLException {
		  PreparedStatement ps = null;
	      ResultSet rs = null;
	      ArrayList<SelectItem> list = new ArrayList<SelectItem>();
	      try{
	            String sql =
	            	"SELECT " +
	            		"mp.id, " +
	            		"mp.name " +
	            	"FROM " +
	            		"marketing_sub_affiliate_pixels msap, " +
	            		"marketing_pixels mp " +
	            	"WHERE " +
	            		"msap.sub_affiliate_id = ? AND " +
	            		"msap.pixel_id = mp.id ";

	            ps = con.prepareStatement(sql);
	            ps.setLong(1, subAffiliateId);
	            rs = ps.executeQuery();
	            while (rs.next()) {
	            	list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
	            }
	      } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	      }
	      return list;
	  }
	  
	  
	  public static int deletePixelByCampaignIDorSourceID(Connection con, Long pixelId, Long campaignID, Long sourceID) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;
			int result = 0;
			try {
				String sql = "delete from marketing_combination_pixels where " +
						" pixel_id = "+pixelId+" and " +
						" combination_id in " +
												"(SELECT co.id " +
												"FROM marketing_combinations co, marketing_campaigns ca, marketing_sources so WHERE ";
												if(campaignID!=0)
													sql +=" ca.id = "+campaignID+" and ";
												if(sourceID!=0)
													sql +=" ca.source_id = "+sourceID+" and ";
												
												sql +=" co.campaign_id = ca.id and ca.source_id = so.id)";
				ps = con.prepareStatement(sql);
				result = ps.executeUpdate();
			}finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	      }
	      return result;
		  
		  
	  }

		/**
		 * Delete pixel for Affiliate/subAffiliate
		 * @param con
		 * @param affiliateId  subaffiliateId
		 * @param pixelId pixel id to remove
		 * @throws SQLException
		 */
		public static void deletePixelByAffiliateId(Connection con, long affiliateId,long subaffiliateId, long pixelId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = "DELETE " +
							 "FROM ";
							  if (affiliateId > 0) {
								sql +="marketing_affilate_pixels ";
							  }
							  else {
								sql +="marketing_sub_affiliate_pixels ";
							  }
					   sql +="WHERE ";
					          if (affiliateId > 0) {
					        	sql +="affiliate_id = ? AND ";
							  }
					          else{
					        	sql +="sub_affiliate_id = ? AND ";
					          }
					   sql +="pixel_id = ? ";

				ps = con.prepareStatement(sql);
			if (affiliateId > 0){
				ps.setLong(1, affiliateId);
			}
			else{
				ps.setLong(1, subaffiliateId);
			}
				ps.setLong(2, pixelId);
				ps.executeUpdate();
		  } finally {
					closeStatement(ps);
					closeResultSet(rs);
				}

		  }

		  /**
		   * Insert new pixel for affiliate/sub affiliate
		   * @param con
		   * @param affiliateId/subAffiliateId for inserting the new pixel
		   * @param pixelId
		   * @throws SQLException
		   */
		  public static void insertIntoAffiliatePixels(Connection con, long affiliateId, long subAffiliateId, long pixelId, long writerId) throws SQLException {
				  PreparedStatement ps = null;
				  ResultSet rs = null;
				  String sql;
				  try {
					    if (affiliateId > 0 && subAffiliateId == 0){
						 sql = "insert into marketing_affilate_pixels(id, affiliate_id, pixel_id, writer_id) " +
								     "values(seq_m_aff_pixels.nextval,?,?,?)";
					    }
					    else {
						 sql = "insert into marketing_sub_affiliate_pixels(id, affiliate_id, pixel_id, writer_id,sub_affiliate_id) " +
									 "values(seq_m_sub_affiliate_pixels.nextval,?,?,?,?)";
					    }
						ps = con.prepareStatement(sql);
						ps.setLong(1, affiliateId);
						ps.setLong(2, pixelId);
						ps.setLong(3, writerId);
					if (subAffiliateId > 0){
						ps.setLong(4, subAffiliateId);
					}
						ps.executeUpdate();
				  } finally {
						closeStatement(ps);
						closeResultSet(rs);
					}
			}

        public static void updateCombintaionPixelByIds(Connection con, long pixelId, String CombintaionPixelIds) throws SQLException {
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                String sql = "UPDATE " +
                                 "marketing_combination_pixels " +
                             "SET " +
                                 "pixel_id = ? " +
                             "WHERE " +
                                 "id in ( " + CombintaionPixelIds + " )";

                  ps = con.prepareStatement(sql);
                  ps.setLong(1, pixelId);
                  //ps.setString(2, campaignIds);


                  ps.executeUpdate();

            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
            }
        }

        /**
         * find all the combination that dont have this pixel search by source and/or campaign
         * @param con
         * @param sourceId
         * @param pixelId
         * @param campaignId
         * @param pixelType
         * @throws SQLException
         */
        public static ArrayList<MarketingCombination> getCombintaionWithoutPixel(Connection con, long sourceId, long pixelId, long campaignId) throws SQLException {
            PreparedStatement ps = null;
            ResultSet rs = null;
            ArrayList<MarketingCombination> list = new ArrayList<MarketingCombination>();

            try {
                String sql = "SELECT " +
                                "mco1.id, " +
                                "mc1.name, " +
                                "CASE WHEN comb_pix.id IS NOT NULL THEN comb_pix.id ELSE 0 END AS HAVE_PIXEL " +
                             "FROM " +
                                "marketing_combinations mco1 " +
                                "LEFT JOIN (SELECT " +
                                                "mcp.combination_id, " +
                                                "mcp.id " +
                                           "FROM " +
                                                "marketing_combination_pixels mcp, " +
                                                "marketing_pixels mp " +
                                           "WHERE " +
                                                "mp.id = mcp.pixel_id AND " +
                                                "mp.type_id in (SELECT mp1.type_id FROM marketing_pixels mp1 WHERE mp1.id = ?) " +
                                           ") comb_pix on comb_pix.combination_id = mco1.id, " +
                                "marketing_campaigns mc1 " +
                             "WHERE ";
                             if (campaignId != 0) {
                                sql += "mc1.id = " + campaignId + " AND ";
                             }
                             if (sourceId != 0) {
                                sql += "mc1.source_id = " + sourceId + " AND ";
                             }
                                sql += "mco1.campaign_id = mc1.id ";

                  ps = con.prepareStatement(sql);
                  ps.setLong(1, pixelId);

                  rs = ps.executeQuery();

                  while (rs.next()) {
                    MarketingCombination vo = new MarketingCombination();
                    vo.setId(rs.getLong("id"));
                    vo.setCampaignName(rs.getString("name"));
                    vo.setPixelId(rs.getLong("have_pixel"));
                    list.add(vo);
                  }
                  ps.executeUpdate();

            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
            }

            return list;
        }
}