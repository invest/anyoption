package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_vos.SalesDepositsSummary;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

/**
 * 
 * @author Ivan Petkov
 *
 */
public class SalesDepositsDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(SalesDepositsDAO.class);
	
	public static ArrayList<SalesDepositsSummary> getDepositsConversionPage(Connection con, long userId, Date startDate, Date endDate, long writerId, long pageNo, long resultsPerPage, long skinId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SalesDepositsSummary> summary = new ArrayList<SalesDepositsSummary>();
		SalesDepositsSummary vo = null;
		int index = 1;
		try {
			String sql = " SELECT " +
						 	" * " +
						 " FROM " +
								" (SELECT " +
				 						" ROW_NUMBER() OVER (order by ti.time_inserted) AS curr_row, " +
				 						" w.user_name, " +
				 						" w.id writer_id, " +
				 						" t.user_id, " + 
				 						" ROUND(msc.group_merged_tx_sum_USD) AS usd_dep_amount, " +
				 						" t.time_created time_inserted, " +
				 						" sum(ROUND(msc.group_merged_tx_sum_USD)) OVER (PARTITION BY w.id) AS sum_dep_amount, " +
				 						" sum(ROUND(msc.group_merged_tx_sum_USD)) OVER () AS sum_all_dep_amount, " +
				 						" COUNT(t.id) OVER () AS num_rows " +
				 				" FROM " + 
				 						" merged_sales_conversions msc, " +
				 						" transactions t, " +  
				 						" issue_actions ia, " +  
				 						" transactions_issues ti, " +  
				 						" transaction_types tt, " +  	  
				 						" users u, " +  	  
				 						" writers w " +  	  
				 				" WHERE " +  
				 						" msc.group_tx_id = ti.transaction_id  " +
				 						" AND t.id = ti.transaction_id " +
				 						" AND ti.issue_action_id = ia.id " + 
				 						" AND t.type_id = tt.id " +   
				 						" AND t.user_id = u.id " +  
				 						" AND w.id = ia.writer_id " + 
				 						" AND ti.is_first_deposit = 1 " +
				 						" AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST +
				 						" AND t.status_id in ( " + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + " ) " +
				 						" AND tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT +				 						
				 						" AND t.time_created > ? " +
				 						" AND t.time_created <= ? ";
			if (writerId > 0) {
				sql += 					" AND w.id = ? ";
			}
			if (userId > 0) {
				sql += 					" AND u.id = ? ";
			}
			if (skinId > 0) {
				sql += 					" AND u.skin_id = ? "; 
			}
			sql += 				" ORDER BY ti.time_settled ASC) ";
			if (pageNo > 0) {
				sql += " WHERE " +
							" curr_row BETWEEN ? AND ? ";
			}
						 
			
			ps = con.prepareStatement(sql);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(startDate));
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(endDate));
			if (writerId > 0) {
				ps.setLong(index++, writerId);
			}
			if (userId > 0) {
				ps.setLong(index++, userId);
			}
			if (skinId > 0) {
				ps.setLong(index++, skinId);
			}
			if (pageNo > 0) {
				ps.setLong(index++, ((pageNo - 1)*resultsPerPage) + 1);
				ps.setLong(index++, (pageNo*resultsPerPage));
			}
			
			rs=ps.executeQuery();
			while (rs.next()) {
				vo = new SalesDepositsSummary();
				vo.setRepUserName(rs.getString("user_name"));
				vo.setWriterId(rs.getLong("writer_id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setDepositAmountUSD(rs.getLong("usd_dep_amount"));
				vo.setTransactionTime(convertToDate(rs.getTimestamp("time_inserted")).getTime());
				vo.setDepositSumAmountPerRep(rs.getLong("sum_dep_amount"));
				vo.setSumAllDeposits(rs.getLong("sum_all_dep_amount"));
				vo.setTotalCount(rs.getLong("num_rows"));
				summary.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return summary;
	}
	
	public static ArrayList<SalesDepositsSummary> getDepositsRetentionPage(Connection con, long userId, Date startDate, Date endDate, long writerId, long pageNo, long resultsPerPage, long skinId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SalesDepositsSummary> summary = new ArrayList<SalesDepositsSummary>();
		SalesDepositsSummary vo = null;
		int index = 1;
		try {
			String sql = " SELECT " +
							" * " + 
						" FROM " +
								" (SELECT " +
							 		  " ROW_NUMBER() OVER (order by wcd.time_created) AS curr_row, " +
								      " w.USER_NAME, " +  
								      " wcd.writer_id, " +   
								      " t.USER_ID, " + 						      
								      " DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT + ", ROUND(t.amount * t.rate), 0) AS usd_dep_amount, " + 
								      " DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS + ", ROUND(wcd.amount * t.rate), 0) AS usd_withdraw_amount, " +
								      " wcd.time_created AS time_created_com, " +
								      " ur.rank_name, " +  
								      " us.status_name, " +  						      
								      " sum(DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT + ", ROUND(t.amount * t.rate), 0)) OVER (PARTITION BY wcd.writer_id) AS dep_writer_sum_amount_usd, " +
								      " sum(DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS + ", ROUND(wcd.amount * t.rate), 0)) OVER (PARTITION BY wcd.writer_id) AS withdraw_writer_sum_amount_usd, " +
								      " sum(DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT + ", ROUND(t.amount * t.rate), 0)) OVER () AS sum_total_dep_amount_usd, " +
								      " sum(DECODE(tt.class_type, " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS + ", ROUND(wcd.amount * t.rate), 0)) OVER () AS sum_total_withdraw_amount_usd, " +
								      " COUNT(t.id) OVER () AS num_rows " +
						      	 " FROM " +  
								      " transactions t, " +  
								      " writers_commission_dep wcd, " +  
								      " transaction_types tt, " +  
								      " writers w, " +
								      " users u " +
								      "		LEFT JOIN users_rank   ur ON ur.id = u.rank_id " +
								      "		LEFT JOIN users_status us ON us.id = u.status_id " +						       
						      	 " WHERE " +  
								      " t.id = wcd.transaction_id " +  
								      " AND t.type_id = tt.id " +   
								      " AND t.user_id = u.id " +  
								      " AND w.id = wcd.writer_id " +
								      " AND wcd.is_valid = 1 " +
								      " AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST +
								      " AND t.status_id IN ( " + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + " ) " + 
								      " AND tt.class_type in (" + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT + "," + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS + ") " + 
								      " AND wcd.time_created > ? " +
								      " AND wcd.time_created <= ? ";
			if (writerId > 0) {
				sql += 	  			  " AND wcd.writer_id = ? ";
			}
			if (userId > 0) {
				sql += 	  			  " AND u.id = ? ";
			}
			if (skinId > 0) {
				sql += 				  " AND u.skin_id = ? "; 
			}
			sql += 	 			 " ORDER by time_created_com ASC) ";
			if (pageNo > 0) {
				sql += " WHERE " +
							" curr_row BETWEEN ? AND ? ";
			}
	
			
			ps = con.prepareStatement(sql);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(startDate));
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(endDate));
			if (writerId > 0) {
				ps.setLong(index++, writerId);
			}
			if (userId > 0) {
				ps.setLong(index++, userId);
			}
			if (skinId > 0) {
				ps.setLong(index++, skinId);
			}
			if (pageNo > 0) {
				ps.setLong(index++, ((pageNo - 1)*resultsPerPage) + 1);
				ps.setLong(index++, (pageNo*resultsPerPage));
			}
			
			rs=ps.executeQuery();
			while (rs.next()) {
				vo = new SalesDepositsSummary();
				vo.setRepUserName(rs.getString("user_name"));
				vo.setWriterId(rs.getLong("writer_id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setDepositAmountUSD(rs.getLong("usd_dep_amount"));
				vo.setWithdrawAmountUSD(rs.getLong("usd_withdraw_amount"));
				vo.setTransactionTime(convertToDate(rs.getTimestamp("time_created_com")).getTime());
				vo.setUserRank(rs.getString("rank_name"));
				vo.setUserStatus(rs.getString("status_name"));
				vo.setSumAllWithdraws(rs.getLong("sum_total_withdraw_amount_usd"));
				vo.setSumAllDeposits(rs.getLong("sum_total_dep_amount_usd"));
				vo.setTotalCount(rs.getLong("num_rows"));
				
				summary.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return summary;
	}
}
