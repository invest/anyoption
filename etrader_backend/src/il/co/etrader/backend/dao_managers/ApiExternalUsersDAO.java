package il.co.etrader.backend.dao_managers;


import il.co.etrader.backend.bl_vos.ApiExternalUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;


/**
 * ApiExternalUsersDAO class.
 *
 * @author Eyal G
 */
public class ApiExternalUsersDAO extends DAOBase {

	public static ApiExternalUser getApiExternalUser(Connection con, long id, String reference, long apiUserId) throws SQLException {
		ApiExternalUser user = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try	{

		    String sql = " SELECT " +
		    				" aeu.*, " +
		    				"CASE WHEN EXISTS ( " +
                            "	SELECT " +
                            "		1 " +
                            "	FROM " +
                            " 	user_market_disable umd " +
                            " WHERE " +
                            "     umd.is_active = 1 AND " +
                            "     umd.is_dev3 = 1 AND " +
                            "     umd.start_date <= sysdate AND " +
                            "     umd.end_date > sysdate AND " +
                            "     umd.user_id = aeu.user_id) " +
                        	"THEN 1 ELSE 0 END AS userDisable " +
		    			 " FROM " +
		    			 	" api_external_users aeu " +
		    			 " WHERE " +
		    		  		" aeu.id = ? OR " +
		    				" (aeu.reference = ? AND " +
		    		  		" aeu.api_user_id = ?) ";

		    ps = con.prepareStatement(sql);
	    	ps.setLong(1, id);
	    	ps.setString(2, reference);
	    	ps.setLong(3, apiUserId);


			rs = ps.executeQuery();

			if (rs.next()) {
				user = getVO(rs);
				user.setUserDisable(rs.getLong("userDisable") == 1 ? true : false);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return user;
	}

	private static ApiExternalUser getVO(ResultSet rs) throws SQLException {
		ApiExternalUser user = new ApiExternalUser();

		user.setId(rs.getLong("id"));
		user.setApiUserId(rs.getLong("api_user_id"));
		user.setUserId(rs.getLong("user_id"));
		user.setReference(rs.getString("reference"));
		user.setIp(rs.getString("ip"));
		user.setRisky(rs.getLong("is_risky") == 1 ? true : false);
		user.setUtcOffset(rs.getString("utc_offset"));

		return user;
	}

	/**
	 * update external user details currently we can update only is_risky
	 * @param con
	 * @param id external user id
	 * @param apiExternalUser the user details
	 * @return true if success
	 * @throws SQLException
	 */
	public static boolean updateUserDetails(Connection con, ApiExternalUser apiExternalUser) throws SQLException {
		PreparedStatement ps = null;
		boolean isUpdate = false;

		try	{

		String sql = " UPDATE " +
	    			 	" api_external_users " +
	    			 " SET " +
	    			 	" is_risky = ? " +
	    			 " WHERE " +
	    			 	" id = ? ";

		ps = con.prepareStatement(sql);
		ps.setLong(1, apiExternalUser.isRisky() ? 1 : 0);
		ps.setLong(2, apiExternalUser.getId());

		isUpdate = ps.executeUpdate() > 0 ? true : false;

		} finally {
			closeStatement(ps);
		}

		return isUpdate;
	}
}
