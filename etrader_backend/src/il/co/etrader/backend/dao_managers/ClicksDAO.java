package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.dao_managers.ClicksDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

public class ClicksDAO extends ClicksDAOBase{
	  public static ArrayList<String[]> search(Connection con,Date from,Date to,long campaignId) throws SQLException {
		  FacesContext context=FacesContext.getCurrentInstance();
		  User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList <String[]> list=new ArrayList<String[]>();

		  try {
			  String sql = " SELECT " +
			  					" mca.name name, sum(ccd.clicks_num) counts " +
			  			   " FROM " +
			  			   		" clicks_comb_day ccd, " +
			  			   		" marketing_campaigns mca, " +
			  			   		" marketing_combinations mc " +
			  			   " WHERE " +
			  			   		" mc.campaign_id = mca.id AND " +
			  			   		" ccd.marketing_combination_id = mc.id AND " +
			  			   		" ccd.day >= ? AND " +
				  			   	" ccd.day <= ? AND " +
			  			   		" mc.skin_id IN (" + Utils.getWriter().getSkinsToString() + ") ";

				if (campaignId != 0) {
						sql +=  " AND mca.id = ? ";
				}

					sql +=  " GROUP BY mca.name " +
							" ORDER BY upper(mca.name) ";



				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, CommonUtil.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), CommonUtil.getAppData().getUtcOffset())));
				if (campaignId != 0) {
					ps.setLong(3, campaignId);
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					String st[] = new String[2];
					st[0] =	rs.getString("name");
					st[1] =	rs.getString("counts");
					list.add(st);
				}
			} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
	  }
}