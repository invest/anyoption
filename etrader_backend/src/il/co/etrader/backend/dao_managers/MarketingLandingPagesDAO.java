package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingLandingPage;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;


public class MarketingLandingPagesDAO extends DAOBase {


	/**
	 * Insert a new landing page
	 * @param con  db connection
	 * @param vo MarketingLandingPage instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingLandingPage vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_landing_pages(id,name,url,writer_id,landing_page_type,time_created, static_landing_page_id) " +
						     "values(seq_mar_landing_pages.nextval,?,?,?,?,sysdate,?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setString(2, vo.getUrl());
				ps.setLong(3, vo.getWriterId());
				ps.setLong(4, vo.getType());
				if (vo.getStaticLandingPageId() > 0) {
					ps.setLong(5, vo.getStaticLandingPageId());
				} else {
					ps.setString(5, null);					
				}
				
				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_landing_pages"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing landing page
	   * @param con db connection
	   * @param vo  MarketingLandingPage instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingLandingPage vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_landing_pages set name=?,url=?,writer_id=?, landing_page_type=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setString(2, vo.getUrl());
				ps.setLong(3, vo.getWriterId());
				ps.setLong(4, vo.getType());
				ps.setLong(5, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all landing pages
	   * @param con  db connection
	   * @param landingName  landing name filter
	   * @return
	   *	ArrayList of MarketingLandingPage
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingLandingPage> getAll(Connection con, String landingName, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingLandingPage> list = new ArrayList<MarketingLandingPage>();

		  try {

				String sql = "select * " +
							 "from marketing_landing_pages where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(landingName) ) {
					sql += " and upper(name) like '%" + landingName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}


				sql += "order by upper(name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingLandingPage vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	  /**
	   * Get all landing pages by writer skin id
	   * @param con  db connection
	   * @param landingName  landing name filter
	   * @return
	   *	ArrayList of MarketingLandingPage
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingLandingPage> getAllByWriteSkinId(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingLandingPage> list = new ArrayList<MarketingLandingPage>();

		  try {
			  	String skins = Utils.getWriter().getSkinsToString();

				String sql = "SELECT " +
							   "mlp.* " +
							 "FROM " +
							   "marketing_landing_pages mlp, " +
							   "marketing_combinations mc " +
							 "WHERE " +
							   "mlp.id = mc.landing_page_id AND " +
							   "mc.skin_id in (" + skins + ") " +
							 "GROUP BY " +
							   "mlp.id, mlp.name, mlp.time_created, mlp.url, mlp.writer_id " +
							 "ORDER BY " +
							   "mlp.name ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingLandingPage vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if landing page name in use
	   * @param con   db connection
	   * @param name  landing name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_landing_pages " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Check if landing page url in use
	   * @param con   db connection
	   * @param url  landing page url
	   * @return true if url in use
	   * @throws SQLException
	   */
	  public static boolean isUrlInUse(Connection con, String url, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_landing_pages " +
			  			   "where url like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, url);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingLandingPage object
	   * @throws SQLException
	   */
	  private static MarketingLandingPage getVO(ResultSet rs) throws SQLException {

		  MarketingLandingPage vo = new MarketingLandingPage();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setUrl(rs.getString("url"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setType(rs.getLong("landing_page_type"));

			return vo;
	  }
	  
	  /**
	   * Get marketing landing page names
	   * @param con
	   * @param landingPageType
	   * @param pathId
	   * @return ArrayList<SelectItem>
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getMarketingLpNamesSI(Connection con, long landingPageType, long pathId) throws SQLException {
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql =  " SELECT " +
					  			" mlp.id, " +
					  			" mlp.name " +
					  		" FROM " +
					  			" marketing_landing_pages mlp " +
					  			" LEFT JOIN static_landing_pages slp on mlp.static_landing_page_id = slp.id  " +
							" WHERE " +
								" mlp.landing_page_type = ? " +
								" and 1 = DECODE(?, 0, 1, " +
													" slp.static_landing_page_path_id, 1, 0)" +
							" ORDER BY " +
								" upper(mlp.name) ";
			  int index = 1;
			  ps = con.prepareStatement(sql);
			  ps.setLong(index++, landingPageType);
			  ps.setLong(index++, pathId);
			  rs = ps.executeQuery();
			  while (rs.next()) {
				  list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	  }
}

