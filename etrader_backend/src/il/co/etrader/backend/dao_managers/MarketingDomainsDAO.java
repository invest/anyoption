package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;

/**
 * MarketingDomainsDAO
 * @author eyalo
 */
public class MarketingDomainsDAO extends DAOBase {
	
	/**
	 * Get Marketing Domains By filters SI
	 * @param con
	 * @return list as ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getMarketingDomainsByfiltersSI(Connection con, long skinId, long urlSourceTypeId, long marketingLandingpageId) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
							" md.id, " +
							" md.domain " +
						" FROM " +
							" MARKETING_DOMAINS md, " +
							" (SELECT " +
								" domains_rules.id " +
							" FROM " +
								" (SELECT " +
									" mdr.id " +
								" FROM " +
									" MARKETING_DOMAINS_RULES mdr " +
								" WHERE " +
									" (mdr.SKIN_ID is null or mdr.SKIN_ID = ?) " +
									" AND (mdr.MARKETING_URL_SOURCE_TYPE_ID is null or mdr.MARKETING_URL_SOURCE_TYPE_ID = ?) " +
									" AND (mdr.MARKETING_LANDING_PAGE_TYPE_ID is null or mdr.MARKETING_LANDING_PAGE_TYPE_ID = (SELECT " +
																																" mlp.LANDING_PAGE_TYPE " +
																															" FROM " +
																																" MARKETING_LANDING_PAGES mlp " +
																															" WHERE " +
																																" mlp.id = ?)) " +
									" AND (mdr.STATIC_LANDING_PAGE_PATH_ID is null or mdr.STATIC_LANDING_PAGE_PATH_ID = (SELECT " +
																															" slp.STATIC_LANDING_PAGE_PATH_ID " +
																														" FROM " +
																															" MARKETING_LANDING_PAGES mlp, " +
																															" static_landing_pages slp " +
																														" WHERE " +
																															" mlp.id = ? " +
																															" and mlp.static_landing_page_id = slp.id)) " +
								" ORDER BY " +
									" mdr.MARKETING_URL_SOURCE_TYPE_ID, " +
									" mdr.MARKETING_LANDING_PAGE_TYPE_ID, " +
									" mdr.STATIC_LANDING_PAGE_PATH_ID, " +
									" mdr.SKIN_ID) domains_rules " +
								" WHERE " +
									" ROWNUM = 1) first_domains_rule " +
							" WHERE " +
								" first_domains_rule.id = md.MARKETING_DOMAINS_RULE_ID";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			ps.setLong(2, urlSourceTypeId);
			ps.setLong(3, marketingLandingpageId);
			ps.setLong(4, marketingLandingpageId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("domain")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}
