package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.MarketsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class MarketsDAO extends MarketsDAOBase {

	public static ArrayList<MarketGroup> getAllGroups(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<MarketGroup> list = new ArrayList<>();
		try {
			String sql = "select * from market_groups order by id";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getGroupVO(con,rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

      public static ArrayList<Market> searchMarkets(Connection con,String name,long groupId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Market> list = new ArrayList<>();
		  try {
			    String sql="select m.*,mg.display_name mgdisplayname,e.name exchangename,inv.GROUP_NAME invlimitgroupname " +
		    		 	   "from markets m,market_groups mg,exchanges e,INVESTMENT_LIMITS_GROUPS inv " +
		    		 	   "where m.market_group_id=mg.id " +
	                              "and m.exchange_id=e.id " +
	                              "and m.INVESTMENT_LIMITS_GROUP_ID=inv.id " +
	                              "and m.id not in " +
	                              "(" + 
        	                              Constants.MARKET_ID_ABU_DHABI +  "," +  Constants.MARKET_ID_AIR_CHINA_LIMITEDI +  "," +  Constants.MARKET_ID_AFRICA +  "," +
        	                              Constants.MARKET_ID_AMERICA_MOVIL +  "," +  Constants.MARKET_ID_AMERICA_MOVIL_UP +  "," +  Constants.MARKET_ID_BRITISH_AIRWAYS +  "," +
        	                              Constants.MARKET_ID_CMB +  "," +  Constants.MARKET_ID_COCA_COLA +  "," +  Constants.MARKET_ID_CONTROLADORA +  "," +
        	                              Constants.MARKET_ID_FREDDIE_MAC +  "," +  Constants.MARKET_ID_GENERAL_ELECTRIC +  "," +
        	                              Constants.MARKET_ID_IBERDROLA +  "," +  Constants.MARKET_ID_INDITEX +  "," +  Constants.MARKET_ID_ISE30 +  "," +
        	                              Constants.MARKET_ID_ISE100 +  "," +  Constants.MARKET_ID_ISE30FUT +  "," +  Constants.MARKET_ID_ISE_FINANCIAL +  "," +
        	                              Constants.MARKET_ID_ISE_INDUSTRIAL +  "," +  Constants.MARKET_ID_JOHNSON_JOHNSON +  "," +  Constants.MARKET_ID_KLSE +  "," +
        	                              Constants.MARKET_ID_KWAIT_SE_KSE +  "," +  Constants.MARKET_ID_MAOF_INDEX +  "," +  Constants.MARKET_ID_MARKS_SPENSER +  "," +
        	                              Constants.MARKET_ID_NASDAQ100_INDEX +  "," +  Constants.MARKET_ID_NIKE +  "," +  Constants.MARKET_ID_PETROCHINA +  "," + 
        	                              Constants.MARKET_ID_PETROLEO_BRASILEIRO + "," + Constants.MARKET_ID_REPSOL_YPF + "," +
        	                              Constants.MARKET_ID_TADAWUL +  "," +  Constants.MARKET_ID_TEL_REALSTATE +  "," +  Constants.MARKET_ID_TELMEX +  "," +
        	                              Constants.MARKET_ID_VOLKSWAGEN +  "," +  Constants.MARKET_ID_WAL_MART +  "," +  Constants.MARKET_ID_WAL_MART_MX +  "," +
        	                              Constants.MARKET_ID_ZYNGA  
	                              + ")" ;

			    				/*	"and  m.id In   ( select smgm.market_id " +
	                            		  		   "from skin_market_groups smg, skin_market_group_markets smgm " +
	                            		  		   "where smg.id = smgm.skin_market_group_id " +
	                            		  		   "and smg.skin_id IN (" + Utils.getWriter().getSkinsToString() + ") ";

			    if (groupId>0) {
		    		sql+="and smg.market_group_id="+groupId;
			    }

			    sql+= " ) ";*/
			    
			    if (groupId > 0) {
		    		sql+=" and m.market_group_id=" + groupId;
			    }

			    if (name!=null && !name.trim().equals("")) {
		    			name=name.toUpperCase();
			    		sql+=" and upper(m.name) like '%"+name+"%' ";
		    	}


		    	sql += " order by m.name";
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					Market m = getMarketVO(con, rs);
					m.setGroupName(rs.getString("mgdisplayname"));
					m.setExchangeName(rs.getString("exchangename"));
					m.setInvestmentLimitsGroupName(rs.getString("invlimitgroupname"));
					// by default null should be accepted as true in that case
					m.setLiveFakeInvestments(rs.getInt("use_for_fake_investments") == 1 || rs.wasNull());
					list.add(m);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }

	public static LinkedHashMap<String, String> getMarketsAndGroups(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
		try {
			/*
			 * String sql=
			 * "(select smg.market_group_id||','||m.id id,m.display_name display_name "
			 * +
			 * "from markets m,skin_market_group_markets smgm, skin_market_groups smg "
			 * +
			 * "where m.id= smgm.market_id and smgm.skin_market_group_id=smg.id and smg.skin_id="
			 * +ConstantsBase.SKIN_ETRADER + ")" + " union " +
			 * "(select mg.id||',0' id,mg.display_name display_name from market_groups mg)"
			 * + " order by id";
			 */
			String sql = "select '0'||','||m.id id, m.id marketId, m.display_name display_name "
						+ "from markets m "
						+ "order by upper(name)";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				hm.put(rs.getString("id"),
						com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(rs.getString("id"),
						MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("marketId")),
						rs.getString("display_name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

      public static LinkedHashMap<String, String> getActiveMarketsAndGroups(Connection con) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;
          LinkedHashMap<String, String>  hm = new LinkedHashMap<String, String> ();
          try {
                String sql = "SELECT " +
                                "'0'||','||m.id id, m.id marketId, m.display_name display_name " +
                             "FROM " +
                                "markets m " +
                             "WHERE " +
                                "EXISTS (SELECT 1 FROM opportunity_templates ot WHERE ot.market_id = m.id AND ot.scheduled < 5 and ot.is_active = 1) " +
                             "ORDER BY " +
                                "upper(name)";

                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                	hm.put(rs.getString("id"),
                			com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(rs.getString("id"),
    						MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("marketId")),
    						rs.getString("display_name")));
                }
            }
            finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return hm;
      }

      public static LinkedHashMap<String, String> getActiveMarketsAndGroupsWithOneTouch(Connection con) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;
          LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
          try {
                String sql = "SELECT " +
                                "'0'||','||m.id id, m.id marketId, m.display_name display_name " +
                             "FROM " +
                                "markets m " +
                             "WHERE " +
                             "EXISTS (SELECT 1 " +
	                                 "FROM opportunity_templates ot " +
	                                 "WHERE ot.market_id = m.id AND ot.is_active = 1) " +
	                                " or m.id = " + ConstantsBase.MARKET_FACEBOOK_ID + " " +
	                         "ORDER BY " +
                             "upper(name)";

                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
			while (rs.next()) {
				hm.put(rs.getString("id"),
						com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(rs.getString("id"),
						MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("marketId")),
						rs.getString("display_name")));
			}
            }
            finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return hm;
      }
      
      /**
       * get active markets from specific opportuinty type
       * @param con
       * @param opportunityTypeId the type of the opportunity type the markets should have
       * @return select item of the markets id, display name
       * @throws SQLException
       */
      public static LinkedHashMap<Long, String> getActiveMarkets(Connection con, long opportunityTypeId) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;
          LinkedHashMap<Long, String> hm = new LinkedHashMap<Long, String>();
          try {
                String sql = "SELECT " +
                                "m.id id, m.display_name display_name " +
                             "FROM " +
                                "markets m " +
                             "WHERE " +
                                "EXISTS (SELECT 1 FROM opportunity_templates ot WHERE ot.market_id = m.id AND ot.opportunity_type_id = ? AND ot.is_active = 1) " +
                             "ORDER BY " +
                                "upper(name)";

                ps = con.prepareStatement(sql);
                ps.setLong(1, opportunityTypeId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    hm.put(rs.getLong("id"), MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("id")));
                }
            }
            finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return hm;
      }

	  public static LinkedHashMap<Long, String> getMarketsByAgreement(Connection con) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  LinkedHashMap<Long,String> hm = new LinkedHashMap<Long,String>();
		  try
			{
			    String sql="select m.id id,m.display_name display_name from markets m where IS_HAS_FUTURE_AGREEMENT=1 order by id ";
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					hm.put(rs.getLong("id"),
					com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(rs.getString("id"),
							MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("id")),
							rs.getString("display_name")));
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hm;
	  }

	  /**
	   * Return display name by market id
	   * this is for one touch messages format
	   * @param con
	   * 		the Db connection
	   * @param marketId
	   * 		market id
	   * @return
	   * @throws SQLException
	   */
	  public static String getDisplayNameById(Connection con, long marketId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try
			{
			    String sql = "select display_name "+
			    			 "from markets "+
			    			 "where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, marketId);
				rs = ps.executeQuery();

				if ( rs.next() ) {
					return (rs.getString("display_name") );
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
	  }

	  public static LinkedHashMap<Long, Market> getAllMarkets(Connection con) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			LinkedHashMap<Long, Market> hm = new LinkedHashMap<Long,Market>();
			try {
					String sql = " select * "+
								 "from markets m  order by " +
								 "upper(name)";

					/*"from markets m,skin_market_group_markets smgm, skin_market_groups smg " +
		    		"where m.id= smgm.market_id and smgm.skin_market_group_id=smg.id and smg.skin_id="+ConstantsBase.SKIN_ETRADER +
		    		" order by smg.id,m.id";
		    		 */

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while(rs.next()) {
					hm.put(rs.getLong("id"), getMarketVO(con, rs, true));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hm;
		}

	  public static LinkedHashMap<Long, Market> getAllZeroOneHundredMarkets(Connection con) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			LinkedHashMap<Long, Market> hm = new LinkedHashMap<Long,Market>();
			try {
					String sql = "select * "+
								 "from markets m " +
								 "where feed_name like '%Binary0-100' or feed_name like '%Dynamicssss'" +
								 "order by upper(name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while(rs.next()) {
					Market market = getMarketVOShort(rs);
					market.setTypeId(rs.getLong("type_id"));
					hm.put(rs.getLong("id"), market);
					String ext;
					if (market.getTypeId() == Market.PRODUCT_TYPE_DYNAMICS) {
						 ext = " Dynamics";
					} else {
						ext = " 0-100";
					}
					market.setDisplayNameKey(market.getDisplayNameKey() + ext);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hm;
		}

	  public static void updateSuspendedMarket(Connection con, String feedName, boolean suspended, String msg) throws SQLException{
	  		PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				String sql = "update markets " +
							 "set is_suspended = ?, suspended_message = ? " +
							 "where FEED_NAME = ?";

				pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, suspended ? 1 : 0);
				pstmt.setString(2, msg);
				pstmt.setString(3, feedName);
				pstmt.executeUpdate();
			} finally {
				closeResultSet(rs);
				closeStatement(pstmt);
			}
	  }

	  public static void updateSuspendedMarketByType(Connection con, long type, boolean suspended, String msg) throws SQLException{
	  		PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				String sql = "update markets " +
							 "set is_suspended = ?, suspended_message = ? " +
							 "where type_id = ?";

				pstmt = con.prepareStatement(sql);
				pstmt.setInt(1, suspended ? 1 : 0);
				pstmt.setString(2, msg);
				pstmt.setLong(3, type);
				pstmt.executeUpdate();
			} finally {
				closeResultSet(rs);
				closeStatement(pstmt);
			}
	  }
	  
	  /**
	   * get markets select list by market group id
	   * @param con
	   * @param groupId market group id
	   * @return array list of select Item with market id and display name
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getMarketsBygroupId(Connection con, String groupId) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try
			{

			  	String sql = "SELECT '0'||','||m.id id,m.display_name display_name "+
							 "FROM markets m " +
                             "WHERE " +
                                 "EXISTS (SELECT 1 FROM opportunity_templates ot WHERE ot.market_id = m.id AND ot.is_active = 1) ";
				if (groupId.indexOf("0") == -1) { //if its 0 bring all markets
					sql += "AND m.market_group_id IN (" + groupId + ") ";
				}
				sql += "ORDER BY upper(name)";

				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					list.add(new SelectItem (rs.getString("id"), rs.getString("display_name")));
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }

	  public static ArrayList<SelectItem> getShiftGroupSI(Connection con, long oppId) throws SQLException {
			 PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
			  try
				{

				  	String sql = "SELECT " +
								 	" o.id, " +
								 	" m.display_name, " +
								 	" m.id market_id " +
								 "FROM " +
								 	"opportunities o, " +
								 	"markets m " +
								 "WHERE " +
								 	"EXISTS (SELECT " +
								 				"o3.scheduled " +
								 			"FROM " +
								 				"opportunities o3 " +
								 			"WHERE " +
								 				"o3.id = ? AND " +
								 				"o3.is_published = 1) AND " +
								  	"o.scheduled = (SELECT " +
						                          		"o1.scheduled " +
						                          	"FROM " +
						                          		"opportunities o1 " +
						                          	"WHERE " +
						                          		"o1.id = ?) AND " +
						            "m.shifting_group_id = (SELECT " +
						                                    	"m1.shifting_group_id " +
						                                    "FROM " +
						                                    	"markets m1, " +
						                                    	"opportunities o2 " +
						                                    "WHERE " +
						                                    	"m1.id = o2.market_id AND " +
						                                    	"o2.id = ?) AND " +
						            "m.shifting_group_id != 0 AND " +
								    "o.market_id = m.id AND " +
								    "o.is_published = 1";

					ps = con.prepareStatement(sql);
					ps.setLong(1, oppId);
					ps.setLong(2, oppId);
					ps.setLong(3, oppId);
					rs = ps.executeQuery();
					while (rs.next()) {
						list.add(new SelectItem(String.valueOf(rs.getLong("id")),
								MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id"))));
					}
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
		}
	  
	  
	  public static ArrayList<SelectItem> getShiftGroupSIPublished(Connection con, long oppId, Date from) throws SQLException {
			 PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
			  try
				{

				  	String sql =" SELECT " +
				  							" o.id as id, " +
			  								" m.display_name, " +
				  							" m.id market_id " +
  								" FROM " +
				  							" opportunities o, " +
				  							" markets m " +
	  							" WHERE " +
								  			" EXISTS (SELECT o3.scheduled FROM opportunities o3 WHERE o3.id = ?  AND o3.is_published = 0 AND o3.is_settled = 0) " +
								  			" AND o.scheduled = (SELECT o1.scheduled FROM opportunities o1 WHERE o1.id = ?) " +
								  			" AND m.shifting_group_id = (SELECT m1.shifting_group_id FROM markets m1, opportunities o2 WHERE m1.id = o2.market_id AND o2.id = ? ) " +
								  			" AND m.shifting_group_id      != 0 " +
								  			" AND o.market_Id               = m.Id " +
								  			" AND TRUNC(o.time_est_closing) = TRUNC(?) " +
								  			" AND o.Is_Published            = 0 " +
								  			" AND o.is_settled              = 0 " ;

					ps = con.prepareStatement(sql);
					ps.setLong(1, oppId);
					ps.setLong(2, oppId);
					ps.setLong(3, oppId);
					Date d = CommonUtil.getDateTimeFormaByTz(from, Utils.getAppData().getUtcOffset());
					ps.setTimestamp(4, CommonUtil.convertToTimeStamp(d));
					rs = ps.executeQuery();
					while (rs.next()) {
						list.add(new SelectItem(String.valueOf(rs.getLong("id")),
								MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id"))));
					}
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
		}

//	  public static ArrayList<Market> getAllZeroOneHunderdSI(Connection conn) throws SQLException{
//		  ArrayList<Market> zeroHundredList =  new ArrayList<Market>();
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  try{
//			  String sql = "SELECT m.* " +
//			  			   "FROM markets m " +
//			  			   "WHERE feed_name like '%Binary0-100'";
//			  ps = conn.prepareStatement(sql);
//			  rs = ps.executeQuery();
//			  while(rs.next()){
//				  Market m = getMarketVO(conn, rs);
//				  m.setZeroOneHundred(new BinaryZeroOneHundred(rs.getString("quote_params")));
//				  zeroHundredList.add(m);
//			  }
//		  }finally {
//			  closeResultSet(rs);
//			  closeStatement(ps);
//		  }
//		return zeroHundredList;
//	  }
//
//	  public static ArrayList<Market> getZeroOneHunderdById(Connection conn, long id) throws SQLException{
//		  ArrayList<Market> zeroHundredList =  new ArrayList<Market>();
//		  PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  try{
//			  String sql = "SELECT m.* " +
//			  			   "FROM markets m " +
//			  			   "WHERE feed_name like '%Binary0-100' " +
//			  			   "AND id = ? ";
//			  ps = conn.prepareStatement(sql);
//			  ps.setLong(1, id);
//			  rs = ps.executeQuery();
//			  while(rs.next()){
//				  Market m = getMarketVO(conn, rs);
//				  m.setQuoteParams(new MarketDynamicsQuoteParams(rs.getString("quote_params")));
//				  zeroHundredList.add(m);
//			  }
//		  }finally {
//			  closeResultSet(rs);
//			  closeStatement(ps);
//		  }
//		return zeroHundredList;
//	  }
	  
	public static Market getDynamicsMarketQuoteParams(Connection con, long marketId) throws SQLException {
		Market market = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT m.* " +
						 " FROM markets m " +
						 " WHERE type_id = ? " +
						 " AND id = ? ";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, Market.PRODUCT_TYPE_DYNAMICS);
			ps.setLong(2, marketId);
			rs = ps.executeQuery();
			while (rs.next()) {
				market = getMarketVO(con, rs);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return market;
	}

	  public static LinkedHashMap<String, String> getActiveBinary0100MarketsAndGroups(Connection con) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;
          LinkedHashMap<String, String> hm = new LinkedHashMap<String, String>();
          try {
                String sql = "SELECT " +
                                "'0'||','||m.id id, " +
                                " m.id marketId, " +
                                "m.display_name display_name, " +
                                "m.type_id type_id " +
                             "FROM " +
                                "markets m " +
                             "WHERE " +
//                             	" m.feed_name like '%Binary0-100' AND " +
                                "EXISTS (" +
                                	"SELECT " +
                                		"1 " +
                                	"FROM " +
                                		"opportunity_templates ot " +
                                	"WHERE " +
                                		"ot.market_id = m.id AND " +
                                		"ot.opportunity_type_id in (" + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_DYNAMICS + ") AND " +
                                		"ot.is_active = 1) " +
                             "ORDER BY " +
                                "upper(name)";

                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                	hm.put(rs.getString("id"),
										com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(rs.getString("id"),
																										MarketsManagerBase.getMarketName(	Skins.SKIN_REG_EN,
																																			rs.getLong("marketId")),
																										rs.getString("display_name")));
                }
            }
            finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return hm;
      }
	  
	  public static LinkedHashMap<Long, com.anyoption.common.beans.MarketGroup> getMarketsByGroups(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedHashMap<Long, com.anyoption.common.beans.MarketGroup> hm = new LinkedHashMap<Long, com.anyoption.common.beans.MarketGroup>();
		try {
			String sql = "SELECT m.*, mg.name group_name, mg.display_name group_display_name " + "FROM markets m " + "JOIN market_groups mg ON "
					+ "market_group_id = mg.id " + "ORDER BY m.market_group_id, m.name ASC";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				com.anyoption.common.beans.base.Market m = new com.anyoption.common.beans.base.Market();
				m.setDisplayName(rs.getString("name"));
				m.setId(rs.getLong("id"));
				m.setExchangeId(rs.getLong("exchange_id"));
				long groupId = rs.getLong("market_group_id");
				m.setDecimalPoint(rs.getLong("decimal_point"));
				m.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
				m.setGroupId(groupId);
				m.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				int marketType = rs.getInt("type_id");
				m.setProductTypeId(marketType);

				if (null == hm.get(groupId)) {
					MarketGroup mg = new MarketGroup();
					mg.setId(groupId);
					mg.setName(rs.getString("group_name"));
					String label = CommonUtil.getMessage(rs.getString("group_display_name"), null, new Locale("en"));
					mg.setDisplayName(label);
					mg.setMarkets(new ArrayList<com.anyoption.common.beans.base.Market>());
					hm.put(groupId, mg);
				}
				hm.get(groupId).getMarkets().add(m);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	  }
	  
	public static long getMarketIdByOppId(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		String sql = "SELECT market_id FROM opportunities o " +
				     " WHERE o.id = ?";

		ps = con.prepareStatement(sql);
		ps.setLong(1, oppId);
		rs = ps.executeQuery();
		if (rs.next()) {
			id = rs.getLong("market_id");
		}
		return id;
	}
	
	public static HashMap<Long, String> getAllMarketsByType(Connection con, long typeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, String> dynamicsMarkets = new HashMap<Long, String>();
		String sql = "SELECT m.id FROM markets m " +
				     " WHERE m.type_id = ?";

		ps = con.prepareStatement(sql);
		ps.setLong(1, typeId);
		rs = ps.executeQuery();
		while (rs.next()) {
			dynamicsMarkets.put(rs.getLong("id"), MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, rs.getLong("id")));
		}
		return dynamicsMarkets;
	}
}
