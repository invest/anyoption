package il.co.etrader.backend.dao_managers;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FilesAdditionalInfo;
import com.anyoption.common.beans.FilesKeesingMatch;
import com.anyoption.common.beans.FilesKeesingResponse;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_vos.UserFiles;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.util.CommonUtil;
import oracle.jdbc.OracleTypes;

public class FilesDAO extends FilesDAOBase {

	private static final Logger log = Logger.getLogger(FilesDAO.class);

	  public static void insert(Connection con,File vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql=
					" INSERT INTO " +
						" files (id, user_id, file_type_id, writer_id, time_created, reference, file_name, " +
							" utc_offset_created, cc_id,is_approved, file_status_id, time_updated," +
							" issue_action_id, id_number, first_name, last_name, exp_date, country_id) " +
					" VALUES" +
						" (seq_files.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getUserId());
				ps.setLong(2, Long.valueOf(vo.getFileTypeId()).longValue());
				ps.setLong(3, vo.getWriterId());
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
				ps.setString(5,vo.getReference());
				ps.setString(6,vo.getFileName());
				ps.setString(7, vo.getUtcOffsetCreated());

				if (vo.getCcId() != 0){
					ps.setLong(8, vo.getCcId());
				}else{
					ps.setString(8, null);
				}
				ps.setInt(9, vo.isApproved()?1:0);
				ps.setLong(10, vo.getFileStatusId());
				ps.setTimestamp(11, CommonUtil.convertToTimeStamp(vo.getTimeUpdated()));
				ps.setLong(12, vo.getIssueActionsId());
				ps.setString(13, vo.getNumber());
				ps.setString(14, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
				ps.setString(15, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
				ps.setTimestamp(16, convertToTimestamp(vo.getExpDate()));
				ps.setLong(17, vo.getCountryId());
				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_files"));
		  	} finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static ArrayList<File> getByUserId(Connection con, FilesDAOFilter filter) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<File> list = new ArrayList<File>();

		  File vo = null;
		  try {

			  String sql=
			  " select " +
			  		" f.*," +
			  		" t.name," +
			  		" w.user_name, " +
			  		" w1.user_name uploader, " +
			  		" cc.cc_number," +
			  		" cct.name cc_type_name, " +
			  		" case when nvl(f.reject_reason_id,0) > 0 then supp_rej.reason_name else '' end as supp_rej, " + 
			  		" case when nvl(f.control_rejection_reason_id,0) > 0  then control_rej.reason_name else '' end as contr_rej," +
			  		" decode(sw.id,0,'N/A',sw.user_name) support_writer, " +
			  		" decode(cw.id,0,'N/A',cw.user_name) control_writer " +
			  " from " +
			  		" File_types t," +
			  		" writers w, " +
			  		" Files f " +
			  			" left join credit_cards cc on cc.id = f.cc_id " +
			  				" left join credit_card_types cct on cct.id = cc.type_id " +
			  					" LEFT JOIN writers w1 ON w1.id = f.uploader_id " +
			  					  " LEFT JOIN file_reject_reasons supp_rej ON supp_rej.id = f.reject_reason_id " +
			  					  	" LEFT JOIN file_reject_reasons control_rej ON control_rej.id = f.control_rejection_reason_id " +
			  					       " LEFT JOIN writers sw ON decode(f.is_approved,1,f.support_approved_writer_id,f.rejection_writer_id) = sw.id" +
				  					     " LEFT JOIN writers cw ON decode(f.is_approved_control,1,f.control_approved_writer_id,f.control_reject_writer_id) = cw.id " +
			  " where " +
			  		" f.file_type_id=t.id " +
			  		" and f.writer_id=w.id " +
			  		" and f.user_id = ? " + 
			  		" and t.id <> 24 "; //CC Back
			  if(filter.getFileTypeId()>0){
				  sql += " and f.file_type_id = " + filter.fileTypeId;
			  }
			  if(filter.getSupportApproved()>0){
				  if(filter.getSupportApproved()==1) { // YES
					  sql += " and f.is_approved = 1 ";
				  }	
				  if(filter.getSupportApproved()==2) { // NO
					  sql += " and f.is_approved = 0 ";
				  }	
			  }
			  if(filter.getUploaded()>0){
				  sql += " and f.file_name ";
				  if(filter.getUploaded()==1){ // YES
					  sql += " is not null ";
				  }
				  if(filter.getUploaded()==2){ // NO
					  sql += " is null ";
				  }
			  }
			  if(filter.getReceivedAtTo()!=null && filter.getReceivedAtFrom() != null ){
				  sql += " and f.time_created between ? and ? ";
			  }
			  if(filter.isCurrentFiles){
				  sql += " and f.is_current = 1 ";
			  }
			  //" and f.file_status_id = ? " +
			  sql +=" order by f.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, filter.getUserId());
				 if(filter.getReceivedAtTo()!=null && filter.getReceivedAtFrom() != null ){
					 ps.setTimestamp(2, convertToTimestamp(filter.getReceivedAtFrom()));
					 ps.setTimestamp(3, convertToTimestamp((CommonUtil.addDay(filter.getReceivedAtTo()))));
				  }
				//ps.setInt(2, Constants.FILE_STATUS_DONE);

				rs = ps.executeQuery();

				while (rs.next()) {
					FacesContext context = FacesContext.getCurrentInstance();
					Locale locale = Utils.getWriterLocale(context);
					vo = getVO(rs, locale);
					String ccNumber = rs.getString("cc_number");
					vo.setUploaderName(rs.getString("uploader"));
					vo.setSupportRejectReasonTxt(rs.getString("supp_rej"));
					vo.setControlRejectReasonTxt(rs.getString("contr_rej"));
					vo.setSupportWriter(rs.getString("support_writer"));
					vo.setControlWriter(rs.getString("control_writer"));
					vo.setWriterName(rs.getString("user_name"));					
					if (!CommonUtil.isParameterEmptyOrNull(ccNumber)){
						String ccName = CommonUtil.getMessage(rs.getString("cc_type_name"),null);
						try {
							ccNumber = AESUtil.decrypt(ccNumber);
					} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
								| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
						// FIXME very bad way of handling exceptions
						throw new SQLException(ce.getMessage());
					}

						int l = ccNumber.length();
						if (l>4){
							vo.setCcName(ccNumber.substring(l - 4, l) + "  " + ccName);
						}
					}
					list.add(vo);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }

	  public static File getRequestedFileByUserId(Connection con,long userId, long creditCardId, long fileTypeId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  File vo = null;
		  try{

			  String sql=
			  " select " +
			  		" f.*," +
			  		" t.name," +
			  		" w.user_name, " +
			  		" cc.cc_number," +
			  		" cct.name cc_type_name " +
			  " from " +
			  		" File_types t," +
			  		" writers w, " +
			  		" Files f " +
			  			" left join credit_cards cc on cc.id = f.cc_id " +
			  				" left join credit_card_types cct on cct.id = cc.type_id " +
			  " where " +
			  		" f.file_type_id=t.id " +
			  		" and f.writer_id=w.id " +
			  		" and f.user_id = ? " +
			  		" and f.file_status_id = ? " +
					" and f.file_type_id = ? ";
			  	if (creditCardId != 0){
			  sql+= " and f.cc_id = ? ";
			  	}
		sql+= " order by " +
			  		" f.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setInt(2, File.STATUS_REQUESTED);
				ps.setLong(3, fileTypeId);
				if (creditCardId != 0){
					ps.setLong(4, creditCardId);
				}
				rs = ps.executeQuery();
				while (rs.next()) {
					FacesContext context = FacesContext.getCurrentInstance();
					Locale locale = Utils.getWriterLocale(context);
					vo = getVO(rs, locale);
					vo.setWriterName(rs.getString("user_name"));
					String ccNumber = rs.getString("cc_number");

					if (!CommonUtil.isParameterEmptyOrNull(ccNumber)){
						String ccName = CommonUtil.getMessage(rs.getString("cc_type_name"),null);
						try {
							ccNumber = AESUtil.decrypt(ccNumber);
					} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
								| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
						// FIXME very bad way of handling exceptions
						throw new SQLException(ce.getMessage());
					}

						int l = ccNumber.length();
						if (l>4){
							vo.setCcName(ccNumber.substring(l - 4, l) + "  " + ccName);
						}
					}
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }	  

	  public static File getSpecificFile(Connection con, long userId, long fileTypeId, long ccId, long actionId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  File vo = null;
		  try
			{
			    String sql=	" SELECT " +
								" f.*, " +
								" ft.name," +
								" w.user_name " +
							" FROM " +
								" files f," +
								" file_types ft, " +
								" writers w " +
							" WHERE " +
								" ft.id = f.file_type_id " +
								" AND f.writer_id = w.id " +
								" AND f.user_id = ? " +
								" AND f.file_type_id = ? ";
							if (ccId != 0) {
					   sql+= 	" AND f.cc_id = ? ";
							}
							if (actionId != 0) {
					   sql+= 	" AND f.issue_action_id = " + actionId;
						    }

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, fileTypeId);
				if (ccId != 0){
					ps.setLong(3, ccId);
				}

				rs = ps.executeQuery();

				if (rs.next()) {
					FacesContext context = FacesContext.getCurrentInstance();
					Locale locale = Utils.getWriterLocale(context);
					vo = getVO(rs, locale);
					vo.setWriterName(rs.getString("user_name"));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;

	  }

	/**
	 * Get user's files (id copy for now)
	 * @param con
	 * @param userId
	 * @param fileTypeId
	 * @param actionId
	 * @return
	 * @throws SQLException
	 */
	public static UserFiles getUserFiles(Connection con, long issueId, long reasonId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  UserFiles vo = null;
		  try {

			  String sql=" SELECT " +
							" f.*, " +
							" ft.name," +
							" w.user_name, " +
							" frs.name file_status_name, " +
							" frs.id risk_files_status_id " +
			  			 " FROM " +
			  			 	" issue_risk_doc_req ird LEFT JOIN issue_actions ia ON ird.issue_action_id = ia.id " +
			  			 		" LEFT JOIN issues i ON i.id = ia.issue_id " +
			  			 		"   LEFT JOIN files f ON f.issue_action_id = ia.id " +
			  			 		" 	LEFT JOIN file_types ft ON ft.id = f.file_type_id AND f.file_type_id = ? " +
			  			 		"	LEFT JOIN writers w ON f.writer_id = w.id " +
			  			 		"	LEFT JOIN users u ON u.id = i.user_id " +
			  			 		"	LEFT JOIN files_risk_status frs ON u.files_risk_status_id = frs.id " +
			  			 " WHERE " +
			  			 	" i.id = ? " +
			  			 	" AND ird.doc_type = ? ";
			  		if (reasonId != 0){
			  		sql+=	" AND ia.risk_reason_id = ? ";
			  		}

				ps = con.prepareStatement(sql);
				ps.setLong(1, FileType.USER_ID_COPY);
				ps.setLong(2, issueId);
				ps.setLong(3, Constants.FILE_TYPE_ID_COPY);
		  		if (reasonId != 0){
		  			ps.setLong(4, reasonId);
			  	}
				rs = ps.executeQuery();

				if (rs.next()) {
					FacesContext context = FacesContext.getCurrentInstance();
					Locale locale = Utils.getWriterLocale(context);
					File f = getVO(rs, locale);
					if (null != f){
						f.setWriterName(rs.getString("user_name"));
						vo = new UserFiles();
						vo.setUserIdFile(f);
						vo.setFilesRiskStatusId(rs.getInt("risk_files_status_id"));
					    String fileStatusName = rs.getString("file_status_name");
					    if (!CommonUtil.isParameterEmptyOrNull(fileStatusName)){
					    	vo.setFilesRiskStatus( CommonUtil.getMessage(fileStatusName, null));
					    } else {
					    	vo.setFilesRiskStatus(null);
					    }
					}
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }


	/**
	 * Get files additional info to HashMap
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, FilesAdditionalInfo> getFilesAdditionalInfo(Connection con) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  HashMap<Long, FilesAdditionalInfo> hm = new HashMap<Long, FilesAdditionalInfo>();
		  try {
			    String sql = " SELECT " +
			    			 	" * " +
			    			 " FROM " +
			    			 	" FILES_ADDITIONAL_INFO ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					FilesAdditionalInfo vo = new FilesAdditionalInfo();
					vo.setFileType(rs.getInt("FILE_TYPE"));
					vo.setIdNumber(rs.getInt("ID_NUMBER") == 1 ? true : false);
					vo.setFirstName(rs.getInt("FIRST_NAME") == 1 ? true : false);
					vo.setLastName(rs.getInt("LAST_NAME") == 1 ? true : false);
					vo.setExpDate(rs.getInt("EXP_DATE") == 1 ? true : false);
					vo.setCcExpDate(rs.getInt("CC_EXP_DATE") == 1 ? true : false);
					vo.setCountryId(rs.getInt("COUNTRY") == 1 ? true : false);
					vo.setDateOfBirth(rs.getInt("DATE_OF_BIRTH") == 1 ? true : false);
					hm.put(rs.getLong("FILE_TYPE"), vo);
				}
			}
			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hm;
	  }

	  /**
	   * Get file reject reason
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getFileRejectReasonList(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "SELECT" +
										" * " +
							 " FROM " +
							 			" file_reject_reasons ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("reason_name")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }
	  
	public static HashMap<Long, ArrayList<SelectItem>> getFileRejectReasonsByFileType(Connection con)
			throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, ArrayList<SelectItem>> rejectReasonsByType = new HashMap<Long, ArrayList<SelectItem>>();
		ArrayList<SelectItem> reasons = new ArrayList<SelectItem>();

		try {

			String sql = " SELECT " +
			 		" ftrr.*, " +
			 		" frr.reason_name " +
			 " FROM " +
			 		" file_type_reject_reasons ftrr, " +
			 		" file_reject_reasons frr " +
			 " WHERE " +
			 		" frr.id = ftrr.reject_reason_id " +
			 " ORDER BY " + 
			 		" file_type_id, " +
			 		" reject_reason_id ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				if (rejectReasonsByType.get(rs.getLong("file_type_id")) == null) {
					reasons = new ArrayList<SelectItem>();
					reasons.add(new SelectItem(new Long(rs.getLong("reject_reason_id")), rs.getString("reason_name")));
					rejectReasonsByType.put(rs.getLong("file_type_id"), reasons);
				} else {
					reasons = rejectReasonsByType.get(rs.getLong("file_type_id"));
					reasons.add(new SelectItem(new Long(rs.getLong("reject_reason_id")), rs.getString("reason_name")));
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return rejectReasonsByType;
	}
	  	 
	  public static boolean checkIsRiskIssue(Connection con, long userId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try
			{
			  String sql=	  " SELECT * " +
			    			  " FROM   issue_actions ia, " +
			    			  		 " issues iss, " +
			    			         " issue_risk_doc_req ird " +
			    			  " WHERE iss.user_id = ?" +
			    			  " AND ia.issue_id   = iss.id " +
			    			  " AND ird.doc_type  = 1 "+
			    			  " AND ia.id = ird.issue_action_id " +
			    			  " AND iss.subject_id = 47";
			    			  
			    ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

				if (rs.next()) {
					return true;
				}
			}
		    finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
	  }
	  
	  public static HashMap<Long, FileType>  getFileTypes(Connection con) throws SQLException {
		  	HashMap<Long, FileType> hm = new HashMap<Long, FileType>();
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.load_file_types(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					FileType vo = new FileType();
					vo.setId(rs.getLong("id"));
					vo.setName(rs.getString("name"));
					vo.setCreditCardFile(rs.getInt("IS_CREDIT_CARD_FILE") == 1 ? true : false);
					vo.setKeesingType(rs.getInt("is_keesing_type") == 1 ? true : false);
					hm.put(rs.getLong("id"), vo);
				}
			} finally {
				closeStatement(cstmt);			
			}
			return hm;
	  }
	  
	  public static HashMap<Long, String> getRejectReasons(Connection con) throws SQLException {

		  	HashMap<Long, String> hm = new HashMap<Long, String>();
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.load_reject_reason_types(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					hm.put(rs.getLong("reject_reason_id"), rs.getString("reason_name"));
				}
			} finally {
				closeStatement(cstmt);			
			}
			return hm;
	  }
	  
	  public static HashMap<Long, ArrayList<HashMap<Long, String>>> getFileRejectReasons(Connection con) throws SQLException {

		  HashMap<Long, ArrayList<HashMap<Long, String>>> hm = new HashMap<Long, ArrayList<HashMap<Long, String>>>();
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.load_reject_reason_types(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					long fileTypeId = rs.getLong("file_type_id");
					
					if(hm.get(fileTypeId) == null){
						hm.put(fileTypeId, new ArrayList<>());
					}
					
					HashMap<Long, String> hmR = new HashMap<Long, String>();
					hmR.put(rs.getLong("reject_reason_id"), rs.getString("reason_name"));
					hm.get(fileTypeId).add(hmR);
				}
			} finally {
				closeStatement(cstmt);			
			}
			return hm;
	  }
	  
	  public static File getFileById(Connection con, long fileId, Locale locale, long previousNext) throws SQLException {
		  	File file = null;
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.get_file(?, ?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setLong(index++, fileId);
				cstmt.setLong(index++, previousNext);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					file = getVO(rs, locale);
					file.setKsStatusId(rs.getLong("KS_STATUS_ID"));
					file.setKsStatusDate(rs.getTimestamp("KS_STATUS_DATE"));
					file.setWriterName(rs.getString("writer_name"));
					file.setUploaderName(rs.getString("uploader_name"));
					
				}
			} finally {
				closeStatement(cstmt);			
			}
			return file;
	  }
	  
	  public static FilesKeesingMatch getFilesKeesingMatch(Connection con, long fileId) throws SQLException {
		  	FilesKeesingMatch fkm = null;
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.get_file_diffs(?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setLong(index++, fileId);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					fkm = new FilesKeesingMatch( rs.getLong("ks_response_id"),
							rs.getString("u_first_name"), rs.getString("k_first_name"), rs.getInt("flag_first_name"), 
							rs.getString("u_last_name"), rs.getString("k_last_name"), rs.getInt("flag_last_name"), 
							rs.getString("u_country"), rs.getString("k_country"), rs.getInt("flag_country"), 
							rs.getTimestamp("u_date_of_birth"), rs.getTimestamp("k_date_of_birth"), rs.getInt("flag_date_of_birth"), 
							rs.getString("u_gender"), rs.getString("k_gender"), rs.getInt("flag_gender"));					
				}
			} finally {
				closeStatement(cstmt);			
			}
			return fkm;
	  }
	  
	  
	  public static void updateFileKeesingMatch(Connection con, FilesKeesingMatch fkm, long writerId) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
			try {
				if(fkm != null && fkm.getKsResponseId() != null){
					String sql = "{call pkg_userfiles.update_ks_match(?, ?, ?, ?, ?, ?, ?)}";
					cstmt = con.prepareCall(sql);
					cstmt.setLong(index++, fkm.getKsResponseId());
					if(fkm.getFlagFirstName() != null){
						cstmt.setInt(index++, fkm.getFlagFirstName());	
					} else {
						cstmt.setNull(index++, OracleTypes.INTEGER);
					}
					
					if(fkm.getFlagLastName() != null){
						cstmt.setInt(index++, fkm.getFlagLastName());	
					} else {
						cstmt.setNull(index++, OracleTypes.INTEGER);
					}
					
					if(fkm.getFlagCountry() != null){
						cstmt.setInt(index++, fkm.getFlagCountry());	
					} else {
						cstmt.setNull(index++, OracleTypes.INTEGER);
					}
					
					if(fkm.getFlagDOB() != null){
						cstmt.setInt(index++, fkm.getFlagDOB());	
					} else {
						cstmt.setNull(index++, OracleTypes.INTEGER);
					}
					
					if(fkm.getFlagGender() != null){
						cstmt.setInt(index++, fkm.getFlagGender());	
					} else {
						cstmt.setNull(index++, OracleTypes.INTEGER);
					}	
					cstmt.setLong(index++, writerId);		
					cstmt.execute();
				}
			
			} finally {
				closeStatement(cstmt);			
			}
	  }
	  
		public static long insertFileBE(Connection conn, File file) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
			long fileId = 0;
			try {
				String sql = "{call pkg_userfiles.create_file(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
															  + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
				cstmt = conn.prepareCall(sql);
				cstmt.registerOutParameter(index++, Types.INTEGER);
				cstmt.setLong(index++, file.getUserId());
				cstmt.setLong(index++, Long.valueOf(file.getFileTypeId()).longValue());
				cstmt.setLong(index++, file.getWriterId());
				cstmt.setString(index++, file.getReference());
				cstmt.setString(index++, file.getFileName());

				if (file.getCcId() != 0){
					cstmt.setLong(index++, file.getCcId());
				}else{
					cstmt.setNull(index++, OracleTypes.INTEGER);
				}
				cstmt.setInt(index++, file.isApproved() ? 1:0);
				cstmt.setLong(index++, file.getFileStatusId());
				cstmt.setLong(index++, file.getIssueActionsId());
				cstmt.setString(index++, file.getNumber());
				cstmt.setString(index++, CommonUtil.capitalizeFirstLetters(file.getFirstName()));
				cstmt.setString(index++, CommonUtil.capitalizeFirstLetters(file.getLastName()));
				cstmt.setTimestamp(index++, convertToTimestamp(file.getExpDate()));
				cstmt.setLong(index++, file.getCountryId());
				cstmt.setInt(index++, file.isControlApproved() ? 1 : 0);
				cstmt.setInt(index++, file.isControlReject() ? 1 : 0);
				cstmt.setInt(index++, file.isSupportReject() ? 1 : 0);
				cstmt.setInt(index++, file.getRejectReason());
				cstmt.setString(index++, file.getComment());	
				cstmt.setLong(index++, file.getRejectionWriterId());
				cstmt.setTimestamp(index++, convertToTimestamp(file.getRejectionDate()));
				cstmt.setLong(index++, file.getUploaderId());
				cstmt.setLong(index++, file.getSupportApprovedWriterId());
				cstmt.setTimestamp(index++, convertToTimestamp(file.getTimeSupportApproved()));
				cstmt.setLong(index++, file.getControlApprovedWriterId());
				cstmt.setTimestamp(index++, convertToTimestamp(file.getTimeControlApproved()));	
				cstmt.setLong(index++, file.getControlRejectWriterId());
				cstmt.setTimestamp(index++, convertToTimestamp(file.getTimeControlRejected()));
				if (file.getControlRejectReason() != null) {
					cstmt.setInt(index++, file.getControlRejectReason());
				} else {
					cstmt.setNull(index++, OracleTypes.NULL);
				}
				cstmt.setString(index++, file.getControlRejectComment());
				cstmt.setTimestamp(index++, convertToTimestamp(file.getUploadDate()));
				
				cstmt.executeQuery();
				fileId = cstmt.getInt(1);		
				file.setId(fileId);
				log.debug("The fileId:" + fileId + " was inserted");
			} finally {
				closeStatement(cstmt);
			}			
			return fileId;
		}
		
		public static FilesKeesingResponse getFilesKeesingResponse(Connection con, long fileId) throws SQLException {
		  FilesKeesingResponse fkr = null;
		  CallableStatement cstmt = null;
		  ResultSet rs = null;
		  int index = 1;
		  try {
			  String sql = "{call pkg_userfiles.get_active_ks_response(?, ?)}";
			  cstmt = con.prepareCall(sql);
			  cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			  cstmt.setLong(index++, fileId);
			  cstmt.executeQuery();				
			  rs = (ResultSet) cstmt.getObject(1);
			  if (rs.next()) {
				  fkr = new FilesKeesingResponse(rs.getLong("ks_response_id"), 
						  rs.getBoolean("is_active"), rs.getString("first_name"), rs.getString("last_name"), 
						  rs.getString("nationality"), rs.getString("issuing_country"), rs.getString("doc_number"), 
						  rs.getTimestamp("expiry_date"), rs.getLong("personal_id"), rs.getTimestamp("date_of_birth"), 
						  rs.getString("gender"), rs.getLong("doc_id"), rs.getLong("check_status"), rs.getLong("reason_nok"));
			  }
		  } finally {
			  closeStatement(cstmt);			
		  }
		  return fkr;
		}
}

