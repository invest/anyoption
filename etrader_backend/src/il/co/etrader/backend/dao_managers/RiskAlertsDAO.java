package il.co.etrader.backend.dao_managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.RiskAlertsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.RiskAlertCCDiffCountriesDetails;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.RiskAlert;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.RiskAlertsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * RiskAlertsDAO class.
 * @author Eran
 */
public class RiskAlertsDAO extends RiskAlertsDAOBase {
	
	private static HashMap<Long, RiskAlertCCDiffCountriesDetails> ccDiffCountDataHM;
	private static final Logger logger = Logger.getLogger(RiskAlertsDAO.class);

	/**
	 * Get Risk Alert List
	 * @param con
	 * @param from
	 * @param to
	 * @param timeType
	 * @param userName
	 * @param skinId
	 * @param classId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RiskAlert> getRiskAlertList(Connection con, Date from, Date to, String timeType,
			String userName, ArrayList<Integer> skinIds, long classId, ArrayList<Long> riskAlertsTypeDescriptions,
			long succeededTrx, int transactionTypeId, int statusTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		ArrayList<RiskAlert> riskArrayList = new ArrayList<RiskAlert>();
		ccDiffCountDataHM = null;
		try {
			String sql ="SELECT " +
							"ra.id ra_id, " +
							"ra.investment_id, " +
							"ra.transaction_id, " +
							"ra.was_reviewed, " +
							"ra.reviewed_by_writer_id, " +
							"ra.time_reviewed, " +
				            "risk_user_id.time_created, " +
				            "risk_user_id.time_settled, " +
							"u.user_name, " +
							"u.first_name, " +
							"u.last_name, " +
							"u.id_num, " +
							"u.skin_id, " +
							"u.id u_id, " +
							"uc.country_name u_ccountry_name, " +
							"sbc.description as business_case_name, " +
							"rat.id rat_id, " +
							"rat.description rat_desc, " +
							"ract.id ract_id, " +
							"ract.description ract_desc, " +
							"mcamp.id campaign_id, ";
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_FIRST_INVESTMENT_IS_1T))) {
				sql +=		"bh.balance balance_after_investment, ";
			}
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER))) {
			      sql += " CASE " +
					         " WHEN NVL(ra.transaction_id, 0) > 0 THEN " +
					          " (SELECT cc.holder_name " +
					             " FROM credit_cards cc, transactions tc "+
					            " WHERE cc.id = tc.credit_card_id "+
					              " AND tc.id = ra.transaction_id "+
					              " AND cc.is_documents_sent = 0) " +
					         " ELSE "+
					          " NULL "+
					       " END AS cc_holder_name, ";
			}
			
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_CC_USED_ON_WEBSITE))) {
			      sql += " CASE " +
					         " WHEN NVL(ra.transaction_id, 0) > 0 THEN " +
					          " (SELECT cc.cc_number " +
					             " FROM credit_cards cc, transactions tc "+
					            " WHERE cc.id = tc.credit_card_id "+
					              " AND tc.id = ra.transaction_id) "+
					         " ELSE "+
					          " NULL "+
					       " END as cc_number, ";
			}
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY))) {
				
		      sql += " CASE " +
		    		  	 " WHEN NVL(ra.transaction_id, 0) > 0 THEN " +
		    		  	  " (SELECT cc.country_id " +
		    		  	     " FROM credit_cards cc, transactions tc "+
		    		  	    " WHERE cc.id = tc.credit_card_id "+
		    		  	      " AND tc.id = ra.transaction_id "+
		    		  	      " AND cc.is_documents_sent = 0) " +
		    		  	  " ELSE "+
		    		  	   " NULL "+
		    		  	" END AS cc_country_id, ";
		      
				sql +=		"u.time_created as user_time_registered, ";
			}
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY)) || 
					riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_REGISTRATION_FROM_COUNTRY_DIFF_THAN_IP_COUNTRY))) {
				sql +=		"u.country_id as user_country_id, ";
				sql +=		"u.ip as user_ip, ";
			}
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_SINGLE_LOGIN))) {
				sql += 		"u.currency_id currency_id,";
				sql +=		"(select amount from transactions where id = u.FIRST_DEPOSIT_ID) ftd_amount, ";
				sql +=		"(select decode(nvl(credit_card_id, 0), 0, 0, 1) from transactions where id = u.FIRST_DEPOSIT_ID) ftd_is_cc, ";
			}
			sql +=			"(CASE WHEN u_suc_trx.user_id IS null THEN 0 ELSE 1 END) as suc_trx " +
						"FROM " +
							"risk_alerts ra, " +
							"risk_alerts_types rat, " +
								"(SELECT " +
									"i.user_id, ra.id , i.time_created, i.time_settled " +
								 "FROM " +
								 	"investments i,risk_alerts ra " +
								 "WHERE i.id=ra.investment_id " +

								 "UNION " +

								 "SELECT " +
								 	"t.user_id,ra.id ,t.time_created, t.time_settled " +
								 "FROM " +
								 	"transactions t,risk_alerts ra, transaction_types tt " +
								 "WHERE " +
								 	"t.id = ra.transaction_id and t.type_id= tt.id ";
			if (transactionTypeId > 0) {
				if (transactionTypeId == TransactionsManager.TRANS_TYPE_ALL_DEPOSIT) {
					sql +=	"AND tt.class_type=" + TransactionsManager.TRANS_CLASS_DEPOSIT + " and t.type_id<>" + TransactionsManager.TRANS_STATUS_REVERSE_WITHDRAW +" ";
					if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_GREATER_THAN_X_NO_DOCUMENT))) {
						sql += "AND tt.id=" + TransactionsManager.TRANS_TYPE_CC_DEPOSIT + " ";
					}
				} else if (transactionTypeId == TransactionsManager.TRANS_TYPE_ALL_WITHDRAW) {
					sql +=	"AND (tt.class_type=" + TransactionsManager.TRANS_CLASS_WITHDRAW + " or " +
					"tt.class_type=" + TransactionsManager.TRANS_CLASS_ADMIN_WITHDRAWALS + " or " +
					"(tt.class_type=" + TransactionsManager.TRANS_CLASS_FIX_BALANCE + " AND tt.id= " + TransactionsManager.TRANS_TYPE_FIX_BALANCE_WITHDRAW  + ") or " +
					"tt.class_type=" + TransactionsManager.TRANS_CLASS_BONUS_WITHDRAWALS + ") ";
				} else {
					sql +=	"AND t.type_id=" + transactionTypeId + " ";
				}
			}

			sql += 	")  risk_user_id " +

								 	"LEFT JOIN (SELECT " +
								 					"t2.user_id " +
								 				"FROM " +
								 					"transactions t2, transaction_types tt2 " +
								 				"WHERE " +
								 					"t2.type_id = tt2.id " +
								 					"AND t2.status_id IN ("+TransactionsManagerBase.TRANS_STATUS_SUCCEED +','+TransactionsManagerBase.TRANS_STATUS_PENDING +") " +
								 					"AND tt2.class_type=" + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
								 				"GROUP BY t2.user_id ) u_suc_trx ON risk_user_id.user_id = u_suc_trx.user_id " +
								 		"LEFT JOIN users u  ON risk_user_id.user_id = u.id " +
								 			"LEFT JOIN marketing_combinations mcomb ON mcomb.id = u.combination_id " +
								 				"LEFT JOIN marketing_campaigns mcamp ON mcamp.id = mcomb.campaign_id, ";
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_FIRST_INVESTMENT_IS_1T))) {
				sql += 				"balance_history bh, ";
			}
			
			sql += 					"risk_alerts_class_types ract, " +
								 	"skins s, " +
								 	"skin_business_cases sbc, " +
								 	"countries uc " +
						"WHERE " +
							"risk_user_id.id = ra.id " +
							"AND rat.class_type_id = ract.id " +
							"AND s.id = u.skin_id " +
							"AND uc.id = u.country_id " +
							"AND sbc.id = s.business_case_id " +
							"AND ra.type_id=rat.id ";
			if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_FIRST_INVESTMENT_IS_1T))) {
				sql +=		"AND bh.key_value = ra.investment_id " +
							"AND bh.command=" + ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT;
			}
			
			sql +=			"AND risk_user_id." + timeType + " BETWEEN to_date('"+ fmt.format(from) + "','dd/MM/yyyy') " +
							"AND to_date('"+ fmt.format(to) + "','dd/MM/yyyy') +1 ";
			if (!CommonUtil.isParameterEmptyOrNull(userName)){
				sql +=	"AND u.user_name = '" + userName.toUpperCase() +"' ";
			}
			if (!skinIds.isEmpty() && !skinIds.contains(Skins.SKINS_ALL_VALUE)){
				String skinIdsList = String.valueOf(skinIds.get(0));
				for(int i = 1; i < skinIds.size(); i++) {
					skinIdsList += "," + skinIds.get(i);
				}
				sql +=	"AND u.skin_id in (" + skinIdsList + ") ";
			}
			if ( classId != ConstantsBase.USER_CLASS_ALL )   {
				if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
					sql +=  "AND (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE +
						"OR  u.class_id="+ConstantsBase.USER_CLASS_COMPANY+") ";
				} else {
					sql +=	"AND u.class_id =" + classId + " ";
				}
			}
			if (riskAlertsTypeDescriptions.size() > 0 && !riskAlertsTypeDescriptions.contains(new Long(0))){
				String riskAlertsTypeDescription = String.valueOf(riskAlertsTypeDescriptions.get(0));
				for(int i = 1; i < riskAlertsTypeDescriptions.size(); i++) {
					riskAlertsTypeDescription += "," + riskAlertsTypeDescriptions.get(i);
				}
				sql +=	"AND rat.id in (" + riskAlertsTypeDescription + ") ";
			}
			if (transactionTypeId == RiskAlertsManager.TYPE_INVESTMENT) {
				sql +=	"AND rat.class_type_id=" + RiskAlertsManager.RISK_ALERT_INVESTMENT_CLASS_TYPE + " ";
			} else if (transactionTypeId != 0){
				sql +=	"AND rat.class_type_id <> " + RiskAlertsManager.RISK_ALERT_INVESTMENT_CLASS_TYPE + " ";
			}
			if(statusTypeId != RiskAlertsManagerBase.STATUS_TYPE_ALL) {
				sql += 	"AND was_reviewed = " + statusTypeId + " ";
			}
			sql+="ORDER BY " +
					"ra.id DESC ";

			pstmt = con.prepareStatement(sql);
	        rs = pstmt.executeQuery();
            while (rs.next()) {
            	RiskAlert ra = new RiskAlert();
            	if (succeededTrx != 0){
            		//Succeeded trx
	            	if (succeededTrx == ConstantsBase.GENERAL_YES && rs.getLong("suc_trx") != Constants.SUCCEEDED_TRANSACTION ){
	            		continue;
	            	}//Unsucceeded trx
	            	else if (succeededTrx == ConstantsBase.GENERAL_NO && rs.getLong("suc_trx") == Constants.SUCCEEDED_TRANSACTION){
	            		continue;
	            	}
            	}
            	ra.setTransactionId(rs.getLong("transaction_id"));
            	ra.setInvestmentId(rs.getLong("investment_id"));
            	ra.setId(rs.getLong("ra_id"));
            	ra.setWasReviewed(rs.getInt("was_reviewed"));
            	ra.setWriterReviewedId(rs.getLong("reviewed_by_writer_id"));
            	ra.setTimeReviewed(convertToDate(rs.getTimestamp("time_reviewed")));
            	ra.getUser().setId(rs.getLong("u_id"));
            	ra.getUser().setUserName(rs.getString("user_name"));
            	ra.getUser().setFirstName(rs.getString("first_name"));
            	ra.getUser().setLastName(rs.getString("last_name"));
            	ra.getUser().setIdNum(AESUtil.decrypt(rs.getString("id_num")));
            	ra.getUser().setSkinId(rs.getLong("skin_id"));
            	ra.getUser().setCountryName(rs.getString("u_ccountry_name"));
            	ra.setRiskAlertType(rs.getLong("rat_id"));
            	ra.setRiskAlertClassType(rs.getLong("ract_id"));
            	ra.setRiskAlertTypeDecription(rs.getString("rat_desc"));
            	ra.setRiskAlertClassTypeDecription(rs.getString("ract_desc"));
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY))) {
            		ra.setTimeCreated(convertToDate(rs.getTimestamp("user_time_registered")));
            	} else {
            		ra.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	}
            	ra.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
            	ra.setSucceededTrx(rs.getInt("suc_trx"));
            	ra.setCampaign_id(rs.getInt("campaign_id"));
            	ra.setBusinessCaseName(rs.getString("business_case_name"));
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_FIRST_INVESTMENT_IS_1T))) {
            		ra.setBalanceAfter1TouchFirstInvestment(rs.getLong("balance_after_investment"));
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER))) {
            		ra.setCcHolderName(rs.getString("cc_holder_name"));
            		ra.setAccountHolderNameVsCcHolderNameTxt(ra.getUser().getFirstName() + " " + ra.getUser().getLastName() + " --- " + ra.getCcHolderName());
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_CC_USED_ON_WEBSITE))) {
            		long ccNumber = Long.valueOf(AESUtil.decrypt(rs.getString("cc_number")));
            		ArrayList<com.anyoption.common.beans.User> users = CreditCardsDAO.checkCardExistence(con, ccNumber, ra.getUser().getId(), ra.getUser().getSkinId());
            		if(null != users) {
            			String[] usersArray = new String[users.size()];
            			for (int i = 0; i<users.size(); i++) {
            				usersArray[i] = users.get(i).getUserName();
            			}
            			String firstUserWhoUsedSameCC = "";
            			String usersWhoUsedSameCC = "";
            			if(usersArray.length > 0) {
            				firstUserWhoUsedSameCC = usersArray[0];
            				for(int i = 1; i < usersArray.length; i++) {
            					if(usersWhoUsedSameCC.equals("")) {
            						usersWhoUsedSameCC = usersArray[i];
            					} else {
            						usersWhoUsedSameCC += "<br/>" + usersArray[i];
            					}
            				}
            			}
            			ra.setFirstUserWhoUsedSameCC(firstUserWhoUsedSameCC);
                		ra.setMoreUsersWhoUsedSameCC(usersWhoUsedSameCC);
            		}           		
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY))) {
            		long ccCountryId = rs.getLong("cc_country_id");
            		long userCountryId = rs.getLong("user_country_id");
            		String ccCountryName = "";
            		if(ccCountryId != 0) {
            			ccCountryName = CommonUtil.getMessage(CountryManagerBase.getById(ccCountryId).getDisplayName(), null);
            		}
            		String userCountryName = "";
            		if(userCountryId != 0) {
            			userCountryName = CommonUtil.getMessage(CountryManagerBase.getById(userCountryId).getDisplayName(), null);
            		}
            		ra.setUserCountryVsCcCountryTxt(userCountryName + " --- " + ccCountryName);
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_REGISTRATION_FROM_COUNTRY_DIFF_THAN_IP_COUNTRY))) {
            		long userCountryId = rs.getLong("user_country_id");
            		String userCountryName = "";
            		if(userCountryId != 0) {
            			userCountryName = CommonUtil.getMessage(CountryManagerBase.getById(userCountryId).getDisplayName(), null);
            		}
            		String userIp = rs.getString("user_ip");
            		String ipCountryCode = Utils.getCountryCodeByIp(userIp);
            		long ipCountryId = CountryManagerBase.getIdByCode(ipCountryCode);
            		String ipCountryName = CommonUtil.getMessage(CountryManagerBase.getById(ipCountryId).getDisplayName(), null);
            		ra.setUserCountryVsIpCountryTxt(userCountryName + " --- " + ipCountryName);
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES))) {
            		RiskAlertCCDiffCountriesDetails rdata = getCCDiffCountriesDetailsCache(con, ra.getUser().getId());
            		ra.setCcCountries(rdata.getCcCountries());
            		ra.setLast4CCDep(rdata.getCcLast4digits());
            		ra.setLastDateCCDep(rdata.getLastCCdepositDate());
            	}
            	if(riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_SINGLE_LOGIN))) {
            		ra.setFtdAmount(rs.getLong("ftd_amount"));
            		ra.setFTDCC(rs.getBoolean("ftd_is_cc"));
            		ra.setCurrencyId(rs.getLong("currency_id"));
            	}
            	
            	riskArrayList.add(ra);
            }
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | NumberFormatException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return riskArrayList;
	}


	/**
	 * Get risk alert description for drop down
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getRiskAlertDescription(Connection con) throws SQLException{
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        try {
        	String sql ="SELECT " +
        					"rat.id,rat.description " +
        				"FROM " +
        					"risk_alerts_types rat " +
        				"ORDER BY " +
        					"description ";
		    pstmt = con.prepareStatement(sql);
	        rs = pstmt.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("description")));
			}

	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }
	    return list;
	}


	public static void updateReviewedRiskAlert(Connection con, long riskAlertId) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql = "UPDATE " +
							"risk_alerts " +
						 "SET " +
						 	"was_reviewed = 1, " +
						 	"reviewed_by_writer_id = ?, " +
						 	"time_reviewed = sysdate " +
						 "WHERE " +
						 	"id = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Utils.getWriter().getWriter().getId());
			pstmt.setLong(2, riskAlertId);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
	
	public static RiskAlertCCDiffCountriesDetails getCCDiffCountriesDetails(Connection conn, long userID) throws SQLException {
		CallableStatement cstmt = null;
		RiskAlertCCDiffCountriesDetails riskData = new RiskAlertCCDiffCountriesDetails();
		int index = 1;
		try {
			String sql = "{call pkg_ccalert.get_user_alert_countries(?, ?, ?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, Types.VARCHAR);
			cstmt.registerOutParameter(index++, Types.VARCHAR);
			cstmt.registerOutParameter(index++, Types.DATE);
			cstmt.setLong(index++, userID);
			cstmt.executeQuery();				
				
			riskData.setCcCountries(cstmt.getString(1));
			try {
				String ccNum = Long.valueOf(AESUtil.decrypt(cstmt.getString(2))) + "";
				int l = String.valueOf(ccNum).length();
				riskData.setCcLast4digits(ccNum.substring(l - 4, l));
			} catch (Exception e) {
				logger.error("Can't get CC Num");
			}
			riskData.setLastCCdepositDate(cstmt.getTimestamp(3));
		} finally {
			closeStatement(cstmt);
		}			
		return riskData;
	}
	
	private static RiskAlertCCDiffCountriesDetails getCCDiffCountriesDetailsCache(Connection conn, long userID){
		if(ccDiffCountDataHM == null){
			ccDiffCountDataHM = new HashMap<>();
			//ccDiffCountDataHM.put(userID, new RiskAlertCCDiffCountriesDetails());
		}
		
		if(ccDiffCountDataHM.get(userID) == null){
			try {
				ccDiffCountDataHM.put(userID, getCCDiffCountriesDetails(conn, userID));
			} catch (SQLException e) {
				logger.error("Can't getCCDiffCountriesDetails ", e);
			}
		}
		return ccDiffCountDataHM.get(userID);
	}
}
