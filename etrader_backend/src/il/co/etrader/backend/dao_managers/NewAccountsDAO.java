package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.NewAccounts;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.daos.DAOBase;


public class NewAccountsDAO extends DAOBase{

	//private static final Logger logger = Logger.getLogger(NewAccountsDAO.class);

	public static ArrayList<NewAccounts> getNewAccounts(Connection con, Date from, Date to, long campaignId,
			String skins,int depositsStatus, long countryId, long skinBusinessCaseId, long writerId, Date arabicStartDate, int verifiedUsers,
            long currencyId, String amountAbove, String amountBelow) throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ArrayList<NewAccounts> ans = new ArrayList<NewAccounts>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		NewAccounts vo = null;
		try	{
			String sql = " select " +
							" users.ID userID, users.USER_NAME username, users.TIME_CREATED registrationDate	, ca.ID campaignId, " +
							" users.TIME_SC_TURNOVER, " +
							" users.TIME_SC_HOUSE_RESULT, " +
							" ca.NAME campaignName,c.display_name country, c.id countryId, s.display_name skin, t1.depositsNum, t2.successDepositsNum, users.writer_id, " +
                            " t1.highest_failed_dep_amount, " +
                            " t1.first_deposit_amount, " +
                            " users.currency_id " +
						 " from countries c,skins s,marketing_combinations co, marketing_campaigns ca, " +
						 	" users " +
						 		" left join (select " +
						 		                " users.id as id1 ," +
                                                " count(*) as depositsNum , " +
                                                " MAX(case when t.status_id = " + TransactionsManagerBase.TRANS_STATUS_FAILED + " then t.amount else 0 end) highest_failed_dep_amount, " +
                                                " MAX(case when users.first_deposit_id = t.id then t.amount else 0 end) first_deposit_amount " +
			                               " from users ,transactions t, transaction_types tt " +
			                               " where t.type_id = tt.id and tt.class_type = ? " +
			                              		" and users.id = t.user_id " +
			                               " group by users.id ) t1 on users.id = t1.id1 " +
                                " left join (select users.id as id2 ,count(*) as successDepositsNum " +
			                              	" from users,transactions t, transaction_types tt " +
			                              	" where t.type_id = tt.id and tt.class_type = ? " +
			                               		" and t.status_id in ("  + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
					                            " and users.id = t.user_id " +
			                                " group by users.id) t2 on users.id = t2.id2 " +
						 " where " +
						 	" users.TIME_CREATED >= ? " +
						 	" and users.TIME_CREATED <= ?  " +
						 	" and users.COMBINATION_ID = co.ID  "+
						 	" and co.campaign_id = ca.id  "+
						 	" and users.CLASS_ID > 0 " +
						 	" and users.skin_id=s.id " +
						 	" and c.id=users.country_id ";

			if (!user.isPartnerSelected()){
		    	sql+=		" and co.skin_id in (" + Utils.getWriter().getSkinsToString() + ") ";
			}

		    sql+=    		" and users.SKIN_ID in (" + skins + ") ";

		    if (writerId != 0) {
				sql += 		" and users.writer_id = " + writerId + " ";
			}

            if(null != arabicStartDate){
            	sql += " AND users.time_created >= ? ";
			}

			if (campaignId != 0) {
				sql += " and ca.ID = " + campaignId + " ";
			}

			if (countryId > 0) {
				sql += " AND c.id = " + countryId + " ";
			}

			if (skinBusinessCaseId > 0) {
				sql += " AND users.SKIN_ID in (SELECT id FROM skins WHERE business_case_id = " +skinBusinessCaseId+ ") ";
			}

			if (verifiedUsers == ConstantsBase.GENERAL_YES){
				sql += 		" AND users.is_authorized_mail = 1 ";
			}else if (verifiedUsers == ConstantsBase.GENERAL_NO){
				sql += 		" AND users.is_authorized_mail = 0 ";
			}

            if (currencyId > 0) {
                sql += " AND users.currency_id  = " +currencyId+ " ";
            }


            if (!CommonUtil.isParameterEmptyOrNull(amountAbove)){
                long amountAboveLong = Long.valueOf(amountAbove) * 100;
                sql += " AND ((t1.first_deposit_amount  > 0 AND t1.first_deposit_amount >=  " +amountAboveLong+ ") " +
                         "     OR " +
                         "    (t1.first_deposit_amount  = 0  AND t1.highest_failed_dep_amount >= " +amountAboveLong+ ")) " ;
            }

            if (!CommonUtil.isParameterEmptyOrNull(amountBelow)){
                long amountBelowLong = Long.valueOf(amountBelow) * 100;
                sql += " AND ((t1.first_deposit_amount  > 0 AND t1.first_deposit_amount <=  " +amountBelowLong+ ") " +
                "     OR " +
                "    (t1.first_deposit_amount  = 0  AND t1.highest_failed_dep_amount <= " +amountBelowLong+ ")) " ;
            }

			sql += 		" order by users.TIME_CREATED desc";

			ps = (PreparedStatement)con.prepareStatement(sql);

			ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

			if(null != arabicStartDate){
				ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getAppData().getUtcOffset())));
			}

			rs = (ResultSet)ps.executeQuery();

			while (rs.next()){
				vo = getNewAccountsVO(con, rs);
                SpecialCare specialCare = new SpecialCare();
				vo.setWriterId(rs.getLong("writer_id"));
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                vo.setSpecialCare(specialCare);
				//vo.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
				if(depositsStatus == ConstantsBase.ALL_DEPOSIT || depositsStatus == vo.getHasDepositsStatus()){
					ans.add(vo);
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return ans;
	}

	private static NewAccounts getNewAccountsVO(Connection con, ResultSet rs) throws SQLException{

		NewAccounts vo = new NewAccounts();

		vo.setCampaignId(rs.getLong("campaignId"));
		vo.setCampaignName(rs.getString("campaignName"));
		if (rs.getString("depositsNum") != null)
			vo.setHasDeposits(true);
		else
			vo.setHasDeposits(false);
		if (rs.getString("successDepositsNum") != null)
			vo.setHasSuccessDeposits(true);
		else
			vo.setHasSuccessDeposits(false);
		vo.setRegistrationDate(rs.getTimestamp("registrationDate"));
		vo.setUserID(rs.getInt("userID"));
		vo.setUsername(rs.getString("username"));

		vo.setSkin(CommonUtil.getMessage(rs.getString("skin"),null));
		vo.setCountry(CommonUtil.getMessage(rs.getString("country"),null));
		vo.setCountryId(rs.getLong("countryId"));
        vo.setFirstDepositAmount(rs.getLong("first_deposit_amount"));
        vo.setHighestFailedDeposit(rs.getLong("highest_failed_dep_amount"));
        vo.setCurrencyId(rs.getLong("currency_id"));

		return vo;
	}


}
