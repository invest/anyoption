package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.dao_managers.MailBoxUsersDAOBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;


/**
 * MailBoxUsersDAO class.
 *
 * @author Kobi
 */
public class MailBoxUsersDAO extends MailBoxUsersDAOBase {

	/**
	 * get all user emails
	 * @param con db connection
	 * @return ArrayList of all bonuses
	 * @throws SQLException
	 */
	public static ArrayList<MailBoxUser> getAll(Connection con, long userId) throws SQLException {
		ArrayList<MailBoxUser> list = new ArrayList<MailBoxUser>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
		    String sql =
		    	  "SELECT " +
	                  "mbu.*, " +
	                  "mbt.subject, " +
	                  "mbt.type_id, " +
	                  "mbt.template, " +
	                  "mbs.status, " +
	                  "mbpt.name popup_type_name " +
	              "FROM " +
	                  "mailbox_users mbu, " +
	                  "mailbox_templates mbt, " +
	                  "mailbox_email_statuses mbs, " +
	                  "mailbox_popup_types mbpt " +
	              "WHERE " +
	                  "mbu.user_id = ? AND " +
	                  "mbu.template_id = mbt.id AND " +
	                  "mbu.status_id = mbs.id AND " +
	                  "mbu.popup_type_id = mbpt.id " +
	              "ORDER BY " +
	              	"mbu.time_created desc ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			while ( rs.next() ) {
				MailBoxUser vo = getVO(rs);
				vo.setStatusName(rs.getString("status"));
				vo.setPopupTypeIdTxt(rs.getString("popup_type_name"));
				MailBoxTemplate template = new MailBoxTemplate();
				template.setTemplate(rs.getString("template"));
				template.setTypeId(rs.getLong("type_id"));
				String params = rs.getString("template_parameters");
		    	if (params != null) {
		    		template.setTemplate(MailBoxTemplatesManager.insertParameters(template.getTemplate(), params));
		    	}
				vo.setTemplate(template);
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static MailBoxUser getVO(ResultSet rs) throws SQLException {
		MailBoxUser vo = new MailBoxUser();
		vo.setId(rs.getLong("id"));
		vo.setTemplateId(rs.getLong("template_id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setStatusId(rs.getLong("status_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setTimeRead(convertToDate(rs.getTimestamp("time_read")));
		vo.setUtcOffsetRead(rs.getString("utc_offset_read"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setFreeText(rs.getString("free_text"));
		vo.setSenderId(rs.getLong("sender_id"));
		WriterWrapper w = Utils.getWriter();
		vo.setSenderName(w.getMailBoxSenderById(vo.getSenderId()));
		vo.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
		vo.setSubject(rs.getString("subject"));
		vo.setPopupTypeId(rs.getLong("popup_type_id"));
		return vo;
	}
}
