package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Fraud;
import il.co.etrader.bl_vos.FraudType;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.context.FacesContext;

import com.anyoption.common.daos.DAOBase;

public class FraudsDAO extends DAOBase{
	  public static void insert(Connection con,Fraud vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql="insert into frauds(id,user_id,type_id,writer_id,time_created,comments,utc_offset_created" +
						") values(seq_frauds.nextval,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getUserId());
				ps.setLong(2, Long.valueOf(vo.getTypeId()).longValue());
				ps.setLong(3, vo.getWriterId());
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
				ps.setString(5,vo.getComments());
				ps.setString(6, vo.getUtcOffsetCreated());
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_frauds"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static void update(Connection con,Fraud vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql=" update Frauds set user_id=?,type_id=?,writer_id=?," +
			  			 " comments=? where id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getUserId());
				ps.setLong(2, Long.valueOf(vo.getTypeId()).longValue());
				ps.setLong(3, vo.getWriterId());
				ps.setString(4,vo.getComments());
				ps.setLong(5,vo.getId());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static ArrayList<Fraud> search(Connection con,String typeId,long id) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Fraud> list = new ArrayList<Fraud>();
		  Fraud vo = null;
		  try {
			  String sql=" select f.*,t.name,w.user_name " +
					  " from Frauds f,Fraud_types t,writers w " +
					  " where f.type_id=t.id and f.writer_id=w.id and f.user_id=? ";

			  if (typeId!=null && !typeId.equals("")) sql+=" and f.type_id="+typeId;
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);
			  rs = ps.executeQuery();
			  while (rs.next()) {
				  vo=getVO(rs);
				  list.add(vo);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	  }

	  public static boolean checkFrauds(Connection con,long userid) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql=" select * from Frauds f where f.user_id=? ";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userid);
			  rs = ps.executeQuery();
			  if (rs.next()) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return false;
	  }

	  public static HashMap<Long,FraudType> getFraudTypes(Connection con) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  HashMap<Long,FraudType> hm = new HashMap<Long,FraudType>();
		  try {
			  String sql="select * from Fraud_types order by id";
			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();
			  while (rs.next()) {
				  FraudType vo = new FraudType();
				  vo.setId(rs.getLong("id"));
				  vo.setName(rs.getString("name"));
				  hm.put(rs.getLong("id"), vo);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return hm;
	  }

		  private static Fraud getVO(ResultSet rs) throws SQLException{
				FacesContext context = FacesContext.getCurrentInstance();
				Fraud vo=new Fraud();

				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setTypeId(rs.getString("type_id"));
				vo.setWriterId(rs.getLong("writer_id"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
				vo.setComments(rs.getString("comments"));
				vo.setTypeName(CommonUtil.getMessage(rs.getString("name"),null, Utils.getWriterLocale(context)));
				vo.setWriterName(rs.getString("user_name"));
				return vo;
		  }
}