package il.co.etrader.backend.dao_managers;

import il.co.etrader.dao_managers.LiveGlobeCityDAOBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.LiveGlobeCity;

public class LiveGlobeCityDAO extends LiveGlobeCityDAOBase {

	public static void insertLiveGlobeCity(Connection con,
			LiveGlobeCity vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " insert into live_globe_cities (id,city_name,country_id,globe_latitude,globe_longtitude,is_active,CURRENCY_ID) " + 
		                 " values(SEQ_LIVE_GLOBE_CITIES.nextval,?,?,?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getCityName());
			ps.setLong(2, vo.getCountryId());
			ps.setString(3, vo.getGlobeLatitude());
			ps.setString(4, vo.getGlobeLongtitude());
			ps.setInt(5, vo.isActive() ? 1 : 0);
			ps.setLong(6, vo.getCurrencyId());
			ps.executeUpdate();

			vo.setId(getSeqCurValue(con, "SEQ_LIVE_GLOBE_CITIES"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateLiveGlobeCity(Connection con,
			LiveGlobeCity vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " update live_globe_cities " + 
		                 " set " +
		                 " city_name=? ," +
		                 " country_id=? ," +
		                 " globe_latitude=? ," +
		                 " globe_longtitude=? ," +
		                 " is_active=? ," +
		                 " CURRENCY_ID=? " +
		                 " where id=? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getCityName());
			ps.setLong(2, vo.getCountryId());
			ps.setString(3, vo.getGlobeLatitude());
			ps.setString(4, vo.getGlobeLongtitude());
			ps.setInt(5, vo.isActive() ? 1 : 0);
			ps.setLong(6, vo.getCurrencyId());
			ps.setLong(7, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
}