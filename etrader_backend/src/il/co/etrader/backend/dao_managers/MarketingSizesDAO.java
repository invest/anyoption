package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingSize;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


public class MarketingSizesDAO extends DAOBase {


	/**
	 * Insert a new size
	 * @param con  db connection
	 * @param vo MarketingSize instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingSize vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_sizes(id,size_vertical,size_horizontal,writer_id,time_created) " +
						     "values(seq_mar_sizes.nextval,?,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getVerticalSize());
				ps.setLong(2, vo.getHorizontalSize());
				ps.setLong(3, vo.getWriterId());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_sizes"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing size
	   * @param con db connection
	   * @param vo  MarketingSize instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingSize vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_sizes set size_vertical=?,size_horizontal=?,writer_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setLong(1, vo.getVerticalSize());
				ps.setLong(2, vo.getHorizontalSize());
				ps.setLong(3, vo.getWriterId());
				ps.setLong(4, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all sizes
	   * @param con  db connection
	   * @param verticalSize  vertical size filter
	   * @param verticalSize  horizontal size filter
	   * @return
	   *	ArrayList of MarketingSize
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingSize> getAll(Connection con, String verticalSize, String horizontalSize, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingSize> list = new ArrayList<MarketingSize>();

		  try {

				String sql = "select * " +
							 "from marketing_sizes where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(verticalSize) ) {
					sql += " and size_vertical=" + verticalSize + " " ;
				}

				if ( !CommonUtil.isParameterEmptyOrNull(horizontalSize))  {
					sql += " and size_horizontal=" + horizontalSize + " " ;			
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}


				sql += "order by size_vertical,size_horizontal";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingSize vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingSize object
	   * @throws SQLException
	   */
	  private static MarketingSize getVO(ResultSet rs) throws SQLException {

		  MarketingSize vo = new MarketingSize();

			vo.setId(rs.getLong("id"));
			vo.setVerticalSize(rs.getLong("size_vertical"));
			vo.setHorizontalSize(rs.getLong("size_horizontal"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));

			return vo;
	  }

}

