package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.AssetIndexMarket;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.base.AssetIndexInfo;
import com.anyoption.common.beans.base.AssetIndexLevelCalc;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.AssetIndexGroupType;

public class AssetIndexMarketDAO extends com.anyoption.common.daos.AssetIndexMarketDAO {
	
	private static final Logger log = Logger.getLogger(AssetIndexMarketDAO.class);

	public static ArrayList<AssetIndexMarket> getAssetIndexMarketsList(Connection con, String marketName, long skinId, long groupId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<AssetIndexMarket> list = new ArrayList<AssetIndexMarket>();
		try {
            String sql = 
            		" SELECT  " +
					    " market_description , " +
	            		" case when datas.indx_type = 1 then 'asset.index.market.groups.indices' " +
		            		" when datas.indx_type = 2 then 'asset.index.market.groups.currency' " +
		            		" when datas.indx_type = 3 then 'asset.index.market.groups.commodities' " +
		            		" when datas.indx_type = 4 then 'asset.index.market.groups.stocks' " +
		            		" when datas.indx_type = 5 then 'asset.index.market.groups.stocks' " +
		            		" when datas.indx_type = 6 then 'asset.index.market.groups.type.binary.0.100' " +
	            		" end as indx_type_name, " +
		                 " datas.*  " + 
				    " FROM " +
					    " (SELECT case when order_data.market_group_id = 2 and order_data.opp_type = 1 then " +
					    " 1 " +
					    " when order_data.market_group_id = 4 and order_data.opp_type = 1 then " +
					    " 2 " +
					    " when order_data.market_group_id = 5 and order_data.opp_type = 1 then " +
					    " 3 " +
					    " when order_data.market_group_id  in(3,6) and order_data.opp_type = 1 then " +
					    " 4 " +
					    " when order_data.opp_type = 3 then " +
					    " 5 " +
					    " when order_data.opp_type = 4 then " +
					    " 6 " +
					    " end as indx_type, " +
					    " order_data.* " +
						    " FROM " +
						    " (SELECT " +
							    " base_date.*, " +
							    " case " +
							    " when base_date.is_BO = 1 then  1 " +
							    " when base_date.is_oplus = 1 then 3 " +
							    " else  4  end as opp_type " +
							  " FROM (" +
							    " SELECT DISTINCT " +
								    " m.display_name as market_display_name, " +
								    " s.id as asset_skin_id, " +
								    " s.name as asset_skin_name, " +
								    " m.feed_Name as market_feed_Name, " +
								    " m.id as marketId, " +
								    " m.name as marketName, " +
								    " m.TIME_CREATED as MARKET_TIME_CREATED, " +
								    " ot.time_zone, " +
								    " (select count(scheduled) " +
									    " from opportunity_templates ot1 " +
									    " where m.id = ot1.market_id " +
									    " and ot1.scheduled = 4 " +
									    " and ot1.is_active = 1) as monthly, " +
									" (select count(scheduled) " +
									    " from opportunity_templates ot1 " +
									    " where m.id = ot1.market_id " +
									    " and ot1.scheduled = 3 " +
									    " and ot1.is_active = 1) as weekly, " +
									" (select count(scheduled) " +
									    " from opportunity_templates ot1 " +
									    " where m.id = ot1.market_id " +
									    " and ot1.scheduled = 2 " +
									    " and ot1.is_active = 1) as dayly, " +
									" (select count(scheduled) " +
									    " from opportunity_templates ot1 " +
									    " where m.id = ot1.market_id " +
									    " and ot1.scheduled = 1 " +
									    " and ot1.is_active = 1) as hourly, " +
								    " sm.market_group_id, " +
								    " mg.display_name as group_name, " +
								    " mdg.display_name as display_group_name_key, " +
								    " nvl(smg.skin_display_group_id, 0) as country_groups, " +
								    " (SELECT count(1) " +
									    " FROM skin_market_group_markets smgm2, " +
									    " opportunity_templates     ot1 " +
									    " WHERE smgm2.skin_market_group_id = smg.skin_market_group_id " +
									    " AND smgm2.market_id = ot1.market_id " +
									    " AND ot1.scheduled = 2 " +
									    " AND ot1.is_active = 1 " +
									    " AND (ot1.sun_f + ot1.mon_f + ot1.tue_f + ot1.wed_f + ot1.thu_f + ot1.fri_f + ot1.sat_f) > 0 ) " + 
									    " AS group_markets_counter, " +
								    " mns.name as market_name_by_skin, " +
								    " m.option_plus_market_id, " +
								    " case " +
								    	" when ot.opportunity_type_id in (1) then " +
								    	"  1 " +
								    	" else " +
								    	" 0  end is_BO, " +
								    " case " +
								    	" when ot.opportunity_type_id in (3) then " +
								    	" 1 " +
								    	" else " +
								    	" 0 end is_oplus, " +
								    " case " +
								    	" when ot.opportunity_type_id in (4, 5) then " +
								    	" 1 " +
								    	" else " +
								    	" 0 end is_0_100, " +
								    " aii.id as indexs_info_id, " +
								    " sm.skin_id as indx_skin_id, " +
								    " (select i.is_24_7 from  asset_index i where i.market_id = m.id) as is_24_7, " +
								    " aii.MARKET_DESCRIPTION_PAGE as MARKET_DESCRIPTION_IN_PAGE, " +
								    " aii.additional_text, aii.ID, aii.market_id, aii.page_description, aii.page_keywords, aii.page_title, aii.skin_id " +
								    " FROM markets m, " +
								    " opportunity_templates ot, " +
								    " skin_market_groups sm, " +
								    " skins s, " +
								    " skin_market_group_markets smg " +
								    " LEFT JOIN market_display_groups mdg " +
								    " ON smg.skin_display_group_id = mdg.id, market_groups mg, " +
								    " market_name_skin mns, asset_indexs_info aii " +
								    " WHERE (ot.scheduled = 2 OR ot.opportunity_type_id = 3 OR " +
								    		" ot.opportunity_type_id in (4, 5)) " +
								    	" AND ot.is_active = 1 " +
								    	" AND m.id = ot.market_id " +
								    	" AND sm.id = smg.skin_market_group_id " +
								    	" AND sm.skin_id = s.id " +
								    	" AND smg.market_id = m.id " +
								    	" AND mg.id = sm.market_group_id " +
								    	" AND mns.skin_id = sm.skin_id " +
								    	" AND mns.market_id = m.id " +
								    	" AND m.id = aii.market_id " +
								    	" AND sm.skin_id = aii.skin_id) base_date) order_data " +
				" ) datas, asset_indexs_info ai WHERE   ai.market_id = datas.marketid and ai.skin_id = datas.skin_id ";
            if(skinId > 0){
            	sql = sql + " AND asset_skin_id = " + skinId;
            }
            if(groupId > 0){
            	sql = sql + " AND indx_type = " + groupId;
            }
            if(!CommonUtil.isParameterEmptyOrNull(marketName)){
            	marketName  = marketName.toUpperCase();
            	sql = sql + " AND upper(market_name_by_skin) like  '%" + marketName + "%' ";
            }
            
            sql = sql + " ORDER BY " +
            " UPPER(market_name_by_skin), " +
			" datas.indx_type , " +      
			" datas.asset_skin_name ";		


			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();						
			
			while(rs.next()) {
				AssetIndexMarket assetIndex = new AssetIndexMarket();
				Market market = new Market();
				AssetIndexInfo assetIndexInfo = new AssetIndexInfo();
				
				market.setDisplayNameKey(rs.getString("market_display_name"));
				market.setFeedName(rs.getString("market_feed_Name"));
				market.setId(rs.getLong("marketId"));
				market.setName(rs.getString("marketName"));
				
												
				String feedName = market.getFeedName();
				if( feedName.equals("NQ") || feedName.equals("TRF30") || feedName.equals("FKLI") || feedName.equals(".KLSE") || feedName.equals("SPX")){
				  feedName += "c1";
				  market.setFeedName(feedName);
				  }
				else if (feedName.equals("SI") || feedName.equals("GC") || feedName.equals("CL") || feedName.equals("HG")) {
				    feedName += "v1";
				    market.setFeedName(feedName);
				  }
				market.setGroupName(rs.getString("indx_type_name"));

				
				//0100 markets has option_plus_market_id
				if (market.getFeedName().indexOf("Binary0-100") > -1) {
					market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 12));
				} else if (market.isOptionPlus()) {
					market.setFeedName(market.getFeedName().substring(0, market.getFeedName().length() - 8));
				}
				
				assetIndex.setMarket(market);
				
				assetIndexInfo.setId(rs.getLong("indexs_info_id"));
				assetIndexInfo.setMarketId(rs.getLong("marketId"));
				assetIndexInfo.setSkinId(rs.getLong("indx_skin_id"));
				assetIndexInfo.setMarketDescription(CommonUtil.clobToString(rs.getClob(("MARKET_DESCRIPTION"))));
				assetIndexInfo.setAdditionalText(rs.getString("ADDITIONAL_TEXT"));
				assetIndexInfo.setPageTitle(rs.getString("PAGE_TITLE"));
				assetIndexInfo.setPageKeyWords(rs.getString("PAGE_KEYWORDS"));
				assetIndexInfo.setPageDescription(rs.getString("PAGE_DESCRIPTION"));
				assetIndexInfo.setMarketDescriptionPage(rs.getString("MARKET_DESCRIPTION_IN_PAGE"));
				assetIndexInfo.setAssetIndexLevelCalc(getAssetIndexsLevelCalc(con, rs.getLong("indexs_info_id")));
				
				assetIndex.setAssetIndexInfo(assetIndexInfo);
				assetIndex.setAssetIndexGroupType(AssetIndexGroupType.MARKET_GROUP.getId());
				
				Skin skin = ApplicationDataBase.getSkinById(assetIndex.getAssetIndexInfo().getSkinId());
				assetIndex.setSkin(skin);                
                list.add(assetIndex);                
			}			

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		log.debug("End init AssetIndexMarkets");
		return list;
	}
	
	public static void update(Connection con, AssetIndexMarket vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					" UPDATE asset_indexs_info " +
					   " SET " +
						   " market_description = ?, " +
						   " additional_text = ?, " +
						   " page_title = ?, " +
						   " page_keywords = ?, " +
						   " page_description = ?, " +
						   " market_description_page = ? " +
				   " WHERE id = ?";

			ps = con.prepareStatement(sql);
			ps.setCharacterStream(1,  new StringReader(vo.getAssetIndexInfo().getMarketDescription()), vo.getAssetIndexInfo().getMarketDescription().length());
			ps.setString(2, vo.getAssetIndexInfo().getAdditionalText());
			ps.setString(3, vo.getAssetIndexInfo().getPageTitle());
			ps.setString(4, vo.getAssetIndexInfo().getPageKeyWords());
			ps.setString(5, vo.getAssetIndexInfo().getPageDescription());
			ps.setString(6, vo.getAssetIndexInfo().getMarketDescriptionPage());
            ps.setLong(7, vo.getAssetIndexInfo().getId());

			ps.executeUpdate();
			closeStatement(ps);
			
        	for (AssetIndexLevelCalc calc : vo.getAssetIndexInfo().getAssetIndexLevelCalc()){
				String sqlCalc =
						" UPDATE asset_indexs_level_calc " +
						   " SET " +
						       " reuters_field = ?, " +
						       " expiry_formula = ? " +
						" WHERE id = ? ";
				ps = con.prepareStatement(sqlCalc);
				ps.setString(1, calc.getReutersFieldText());
				ps.setString(2, calc.getExpiryFormulaText());
				ps.setLong(3, calc.getId());	            

	            ps.executeUpdate();
	            closeStatement(ps);
        	}	
		} finally {
			closeStatement(ps);
		}
	}
}
