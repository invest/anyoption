package il.co.etrader.backend.dao_managers;


import il.co.etrader.backend.bl_vos.OptionPlusMinRange;
import il.co.etrader.backend.bl_vos.OptionPlusPrice;
import il.co.etrader.backend.bl_vos.OptionPlusPromileRange;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


/**
 * OptionPlusDAO class.
 *
 * @author Kobi
 */
public class OptionPlusDAO extends DAOBase {

	public static ArrayList<OptionPlusMinRange> getOptionPlusMinRanges(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OptionPlusMinRange> list = new ArrayList<OptionPlusMinRange>();
		try {
			String sql =
				"SELECT * " +
				"FROM " +
					"options_plus_minutes_range " +
				"ORDER BY " +
					"id ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				OptionPlusMinRange item = new OptionPlusMinRange();
				item.setId(rs.getLong("id"));
				item.setMin(rs.getLong("min_minutes"));
				item.setMax(rs.getLong("max_minutes"));
				list.add(item);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static ArrayList<OptionPlusPromileRange> getOptionPlusPromileRanges(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OptionPlusPromileRange> list = new ArrayList<OptionPlusPromileRange>();
		try {
			String sql =
				"SELECT * " +
				"FROM " +
					"options_plus_promile_range " +
				"ORDER BY " +
					"id ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				OptionPlusPromileRange item = new OptionPlusPromileRange();
				item.setId(rs.getLong("id"));
				item.setMin(rs.getLong("min_promile"));
				item.setMax(rs.getLong("max_promile"));
				list.add(item);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static ArrayList<OptionPlusPrice> getOptionPlusPrices(Connection con, long marketId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OptionPlusPrice> list = new ArrayList<OptionPlusPrice>();
		try {
			String sql =
				"SELECT " +
			      "p.id, " +
			      "p.promile_range_id, " +
			      "p.minutes_range_id, " +
			      "r.min_promile as minProm, " +
			      "r.max_promile as maxProm, " +
			      "m.min_minutes as minM, " +
			      "m.max_minutes as maxM, " +
			      "(p.price * 100) as price " +
				"FROM " +
				      "options_plus_prices p, " +
				      "options_plus_minutes_range m, " +
				      "options_plus_promile_range r " +
				"WHERE " +
				      "p.minutes_range_id = m.id AND " +
				      "p.promile_range_id = r.id AND " +
				      "m.market_id = ? AND " +
				      "r.market_id = ? " +
				"ORDER BY " +
				      "r.min_promile desc, " +
				      "m.min_minutes ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, marketId);
			ps.setLong(2, marketId);
			rs = ps.executeQuery();

			while (rs.next()) {
				OptionPlusPrice item = new OptionPlusPrice();
				item.setId(rs.getLong("id"));
				item.setMinRangeId(rs.getLong("minutes_range_id"));
				item.setPromileRangeId(rs.getLong("promile_range_id"));
				item.setPrice(rs.getDouble("price"));
				item.setMinutesRangeStr(rs.getDouble("minM") + "-" + rs.getDouble("maxM"));
				item.setPromileRangeStr(rs.getDouble("minProm") + "-" + rs.getDouble("maxProm"));
				list.add(item);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static void updatePrice(Connection con, long id, double price) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"UPDATE " +
					"options_plus_prices " +
				"SET " +
					"price = ? " +
				"WHERE " +
					"id = ?";
			ps = con.prepareStatement(sql);
			ps.setDouble(1, price);
			ps.setLong(2, id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

}
