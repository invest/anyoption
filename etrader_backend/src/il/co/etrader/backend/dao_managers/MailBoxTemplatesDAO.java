package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.dao_managers.MailBoxTemplatesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.MailBoxTemplate;

/**
 * MailBoxTemplatesDAO class.
 *
 * @author Kobi
 */
public class MailBoxTemplatesDAO extends MailBoxTemplatesDAOBase {

	/**
	 * get all templates
	 * @param con db connection
	 * @return ArrayList of all bonuses
	 * @throws SQLException
	 */
	public static ArrayList<MailBoxTemplate> getAll(Connection con, int skinId, long typeId, long senderId) throws SQLException {
		ArrayList<MailBoxTemplate> list = new ArrayList<MailBoxTemplate>();
		Statement ps = null;
		ResultSet rs = null;
		String skins = Utils.getWriter().getSkinsToString();
		try	{
		    String sql =
		    	"SELECT " +
			      "mt.*, " +
			      "ms.sender, " +
			      "met.type " +
				"FROM " +
				      "mailbox_templates mt, " +
				      "mailbox_senders ms, " +
				      "mailbox_email_types met " +
				"WHERE " +
				      "mt.sender_id = ms.id AND " +
				      "mt.type_id = met.id AND " +
				      "mt.skin_id in (" + skins + ") ";

		    if (skinId > 0) {
		    	sql +=
		    		 "AND mt.skin_id = " + skinId + " ";
		    }

		    if (typeId > 0) {
		    	sql +=
		    		 "AND mt.type_id = " + typeId + " ";
		    }

		    if (senderId > 0) {
		    	sql +=
		    		 "AND mt.sender_id = " + senderId + " ";
		    }

		    sql +=
		    	"ORDER BY " +
		              "mt.time_created";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while ( rs.next() ) {
				MailBoxTemplate vo = getVO(rs);
				vo.setSenderName(rs.getString("sender"));
				vo.setTypeName(rs.getString("type"));
				vo.setSkinName(ApplicationData.getSkinById(rs.getLong("skin_id")).getDisplayName());
				vo.setLanguageName(ApplicationData.getLanguage(rs.getLong("language_id")).getDisplayName());
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * insert template
	 * @param con db connection
	 * @throws SQLException
	 */
	public static void insert(Connection con, MailBoxTemplate template) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql="INSERT INTO " +
						  "mailbox_templates " +
						  		"(id, " +
						  		"name, " +
						  		"subject, " +
						  		"type_id, " +
						  		"time_created, " +
						  		"writer_id, " +
						  		"sender_id, " +
						  		"template, " +
						  		"is_high_priority, " +
						  		"skin_id, " +
						  		"language_id," +
						  		"popup_type_id) " +
						"VALUES" +
                                "(SEQ_MAILBOX_TEMPLATES.nextval,?,?,?,sysdate,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(sql);
			ps.setString(1, template.getName());
			ps.setString(2, template.getSubject());
			ps.setLong(3, template.getTypeId());
			ps.setLong(4, template.getWriterId());
			ps.setLong(5, template.getSenderId());
			ps.setString(6, template.getTemplate());
			ps.setLong(7, template.getIsHighPriority() ? 1 : 0);
			ps.setLong(8, template.getSkinId());
			ps.setLong(9, template.getLanguageId());
			ps.setLong(10, template.getPopupTypeId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

    /**
     * update template
     * @param con db connection
     * @throws SQLException
     */
    public static void update(Connection con, MailBoxTemplate template) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql=	"UPDATE " +
            				"mailbox_templates " +
            			"SET " +
	                       "name = ?, " +
	                       "subject = ?, " +
	                       "type_id = ?, " +
	                       "writer_id = ?, " +
	                       "sender_id = ?, " +
	                       "template = ?, " +
	                       "is_high_priority = ?, " +
	                       "skin_id = ?, " +
	                       "language_id = ?, " +
	                       "popup_type_id = ? " +
                        "WHERE " +
                            "id = ?";

            ps = con.prepareStatement(sql);
            ps.setString(1, template.getName());
			ps.setString(2, template.getSubject());
			ps.setLong(3, template.getTypeId());
			ps.setLong(4, template.getWriterId());
			ps.setLong(5, template.getSenderId());
			ps.setString(6, template.getTemplate());
			ps.setLong(7, template.getIsHighPriority() ? 1 : 0);
			ps.setLong(8, template.getSkinId());
			ps.setLong(9, template.getLanguageId());
			ps.setLong(10, template.getPopupTypeId());
			ps.setLong(11, template.getId());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    /**
     * Get all mailbox templates types
     * @param con
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getTypes(Connection con) throws SQLException {
		Statement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =
				"SELECT " +
					"id, " +
					"type " +
				"FROM " +
					"mailbox_email_types " +
				"ORDER BY " +
					"type";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("type")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
    }

    /**
     * Get all senders
     * @param con
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getSenders(Connection con) throws SQLException {
		Statement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =
				"SELECT " +
					"id, " +
					"sender " +
				"FROM " +
					"mailbox_senders " +
				"ORDER BY " +
					"sender";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), CommonUtil.getMessage(rs.getString("sender"), null, Locale.ENGLISH)));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
    }

    /**
     * Get all templates list by writer skins
     * @param con
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getTemplatesListByWriterAndUser(Connection con) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String skins = Utils.getWriter().getSkinsToString();
		User u = Utils.getUser();
		try	{
		    String sql =
		    	"SELECT " +
			      "mt.id, " +
			      "mt.name " +
				"FROM " +
				      "mailbox_templates mt, " +
				      "mailbox_email_types met " +
				"WHERE " +
				      "mt.skin_id in (" + skins + ") AND " +
				      "mt.skin_id = " + u.getSkinId() + " AND " +
				      "mt.type_id = met.id AND " +
				      "met.is_displayed = 1 " +
		        "ORDER BY " +
		              "mt.name";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while ( rs.next() ) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

    /**
     * Get all templates list by writer skins
     * @param con
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getTemplatesListByWriter(Connection con) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String skins = Utils.getWriter().getSkinsToString();
		try	{
		    String sql =
		    	"SELECT " +
			      "mt.id, " +
			      "mt.name " +
				"FROM " +
				      "mailbox_templates mt, " +
				      "mailbox_email_types met " +
				"WHERE " +
				      "mt.skin_id in (" + skins + ") AND " +
				      "mt.type_id <> ? AND " +
				      "mt.type_id = met.id AND " +
				      "met.is_displayed = 1 " +
		        "ORDER BY " +
		              "mt.name";

			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.MAILBOX_TYPE_FREE_TEXT);
			rs = ps.executeQuery();

			while ( rs.next() ) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static MailBoxTemplate getTemplateById(Connection con, long templateId) throws SQLException {
		MailBoxTemplate t = new MailBoxTemplate();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
		    String sql =
		    	"SELECT " +
			      "mt.*, " +
			      "ms.sender, " +
			      "met.type " +
				"FROM " +
				      "mailbox_templates mt, " +
				      "mailbox_senders ms, " +
				      "mailbox_email_types met " +
				"WHERE " +
				      "mt.sender_id = ms.id AND " +
				      "mt.type_id = met.id AND " +
				      "mt.id = ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, templateId);
			rs = ps.executeQuery();

			if (rs.next()) {
				t = getVO(rs);
				t.setSenderName(rs.getString("sender"));
				t.setTypeName(rs.getString("type"));
				t.setSkinName(ApplicationData.getSkinById(rs.getLong("skin_id")).getDisplayName());
				t.setLanguageName(ApplicationData.getLanguage(rs.getLong("language_id")).getDisplayName());
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return t;
	}

    public static ArrayList<SelectItem> getPopupTypes(Connection con) throws SQLException {
		Statement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =
				"SELECT " +
					"id, " +
					"name " +
				"FROM " +
					"mailbox_popup_types " +
				"ORDER BY " +
					"name";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
    }

}
