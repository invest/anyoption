package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingType;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


public class MarketingTypesDAO extends DAOBase {


	/**
	 * Insert a new type
	 * @param con  db connection
	 * @param vo MarketingType instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingType vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_types(id,name,writer_id,time_created) " +
						     "values(seq_mar_types.nextval,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_types"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing type
	   * @param con db connection
	   * @param vo  MarketingType instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingType vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_types set name=?,writer_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setLong(3, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all types
	   * @param con  db connection
	   * @param typeName  type name filter
	   * @return
	   *	ArrayList of MarketingMedium
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingType> getAll(Connection con, String typeName, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingType> list = new ArrayList<MarketingType>();

		  try {

				String sql = "select * " +
							 "from marketing_types where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(typeName) ) {
					sql += " and upper(name) like '%" + typeName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}


				sql += "order by upper(name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingType vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if type name in user
	   * @param con   db connection
	   * @param name  type name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_types " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingType object
	   * @throws SQLException
	   */
	  private static MarketingType getVO(ResultSet rs) throws SQLException {

		  MarketingType vo = new MarketingType();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));

			return vo;
	  }

}

