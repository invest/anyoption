package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.Analytics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * AnalyticsManager
 * @author eyalo
 */
public class AnalyticsManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(AnalyticsManager.class);
	
	/**
	 * Get analytics FTD
	 * @return ArrayList<Analytics>
	 * @throws SQLException
	 */
	public static ArrayList<Analytics> getAnalyticsFtd() throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return AnalyticsDAO.getAnalyticsFtd(con);
		} finally {
			closeConnection(con);
		}
	}
}
