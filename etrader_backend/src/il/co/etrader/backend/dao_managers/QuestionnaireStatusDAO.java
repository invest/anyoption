package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.QuestionnaireStatus;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.DAOBase;

public class QuestionnaireStatusDAO extends DAOBase {

    public static ArrayList<QuestionnaireStatus> getQuestionnaireNotFilled(Connection con, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId)
            throws SQLException {

        ArrayList<QuestionnaireStatus> list = new ArrayList<QuestionnaireStatus>();
        Statement ps = null;
        ResultSet rs = null;

        try {
            String sql = buildSqlQNF(false, userId, userName, interested, daysConditionDeposit,reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            while (rs.next()) {
                QuestionnaireStatus vo = getQuestionnaireStatusVO(rs);
                list.add(vo);
            }

        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return list;
    }

    public static int getQuestionnaireNotFilledCount(Connection con, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int qualified, int daysConditionQualified, int skinId)
            throws SQLException {

        int count = 0;
        Statement ps = null;
        ResultSet rs = null;

        try {
            String sql = buildSqlQNF(true, userId, userName, interested, daysConditionDeposit, reached, -1, -1, qualified, daysConditionQualified,  skinId);

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("c");
            }

        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return count;
    }
    
    private static String buildSqlQNF(boolean count, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId) {
        String sql = "" ;
        boolean paging = firstRow > -1 && rowsPerPage > -1;
    
        if(paging) {
            sql += "SELECT * FROM ( select a.*, ROWNUM rnum FROM  ( ";
        }
        sql += 
            "SELECT ";
            if(count){
                sql += " COUNT(*) as c ";
            } else {
                sql += " * ";
            }
            sql +="FROM " + 
                        " ( " +
                            " SELECT " + 
                                    " ur.user_id, " +
                                    " u.user_name, " +
                                    " u.mobile_phone, " +
                                    " u.land_line_phone," +
                                    " s.name as skin_name, " +
                                    " t.time_created first_deposit_date, " +
                                    " reached.action_time as last_time_reached , " +
                                    " reached.user_name as last_caller, " +
                                    " ur.qualified_time qualified_time, " +
                                    " (select count(*) from  issue_actions triss, issues istr where triss.issue_action_type_id = " + IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_TREATMENT + " and triss.issue_id=istr.id and istr.user_id=u.id) as treatment_issue, " +
                                    " DECODE((SELECT COUNT(ia.id) FROM issues i, issue_actions ia WHERE i.user_id = u.id AND ia.issue_id=i.id and ia.issue_action_type_id =  " + IssuesManagerBase.ISSUE_ACTION_NOT_COOPERATIVE + " and i.subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_QUESTIONNAIRE + "), 0, 2, 1) as interested, " +
                                    " pn_docs.TR_CL_DOCS AS last_time_reched_pen_docs " +
                             " FROM " + 
                                    " users_regulation ur, " +
                                    " users u, " +
                                    " skins s, " +
                                    " transactions t ," +
                                    " (select ia.id,w.user_name,ia.action_time from " +
                                           " issue_actions ia, " +
                                           " writers w " +
                                           " where ia.writer_id=w.id) reached, " +
                                    " (SELECT  issues.user_id, " +
                                    "  		MAX(issue_actions.action_time) AS TR_CL_DOCS " +
                                    "  FROM issues, " +
                                    " 		issue_actions, " +
                                    " 		issue_action_types " +
                                    "  WHERE issues.subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
                                    "  		AND issues.id = issue_actions.issue_id " +
                                    "		AND issue_action_types.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL +
                                    "		AND issue_action_types.id=issue_actions.issue_action_type_id " +
                                    "  GROUP BY  issues.user_id) pn_docs " +
                                    " WHERE " +
                                    " ur.user_id=u.id " +
                                    " and u.skin_id=s.id ";
                                    if (skinId != 0) {
			                            sql += " AND u.skin_id = " + skinId;	
			                        }else {
				                        sql += " AND u.skin_id in (select ws.skin_id " +
			 			                       " from WRITERS_SKIN ws " +
			 			                       " where ws.writer_id = " + Utils.getWriter().getWriter().getId() + " ) "; 
			                        }
                                    sql +=
                                    "  AND u.id=pn_docs.user_id(+) " +
                                    " and t.id = u.first_deposit_id " +
                                    " and ur.approved_regulation_step=2 " +
                                    " and reached.id(+) = REGULATION.GET_ISSID_QUEST_REACHED(u.id) " +
                                    " and ur.suspended_reason_id != " + UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT;
									if(qualified != 2) {
										sql += " AND ur.qualified = " + qualified;
									}
                                    if(userId != null && userId > 0){
                                        sql += " AND u.id = "+userId;
                                    }
                                    if(userName != null && userName.trim().length() > 0){
                                        sql += " AND u.user_name like \'%"+userName.toUpperCase()+"%\'";
                                    }                                   
                                    
                                    sql += ") Where treatment_issue = 0 ";
                                    if( daysConditionDeposit == 1) {
    									sql += " AND sysdate - first_deposit_date < 30 ";
    								}
    								if( daysConditionDeposit == 2) {
    									sql += " AND sysdate - first_deposit_date >= 30 AND sysdate - first_deposit_date < 60 ";
    								}
    								if( daysConditionDeposit == 3) {
    									sql += " AND sysdate - first_deposit_date >= 60 AND sysdate - first_deposit_date < 90 ";
    								}
    								if( daysConditionDeposit == 4) {
    									sql += " AND sysdate - first_deposit_date >= 90 ";
    								}
    								
    								if( daysConditionQualified == 1) {
    									sql += " AND sysdate - qualified_time < 30 ";
    								}
    								if( daysConditionQualified == 2) {
    									sql += " AND sysdate - qualified_time >= 30 AND sysdate - qualified_time < 60 ";
    								}
    								if( daysConditionQualified == 3) {
    									sql += " AND sysdate - qualified_time >= 60 AND sysdate - qualified_time < 90 ";
    								}
    								if( daysConditionQualified == 4) {
    									sql += " AND sysdate - qualified_time >= 90 ";
    								}
                                    if(interested > 0) {
                                        sql += " AND interested = " + interested;
                                    }
                                    
                                    if(reached == 1) {
                                        sql += " AND last_time_reached is not null and  last_time_reached is not null ";
                                    }
                                    
                                    if(reached == 2) {
                                        sql += " AND last_time_reached is null and  last_time_reached is null ";
                                    }
                                    
                                    sql +=" Order by 1";
                if(paging) {
                    sql +=") a where rownum <= " + (firstRow+rowsPerPage) + " ) where rnum  >=" + firstRow;
                }                   
        return sql;
    }
    
    
    public static ArrayList<QuestionnaireStatus> getQuestionnaireFilledIncorrectly(Connection con, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId)
            throws SQLException {

        ArrayList<QuestionnaireStatus> list = new ArrayList<QuestionnaireStatus>();
        Statement ps = null;
        ResultSet rs = null;

        try {
            String sql = buildSqlQFI(false, userId, userName, interested, daysConditionDeposit,reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            while (rs.next()) {
                QuestionnaireStatus vo = getQuestionnaireStatusVO(rs);
                list.add(vo);
            }

        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return list;
    }
    
    public static int getQuestionnaireFilledIncorrectlyCount(Connection con, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int qualified, int daysConditionQualified, int skinId)
            throws SQLException {

        int count = 0;
        Statement ps = null;
        ResultSet rs = null;

        try {
            String sql = buildSqlQFI(true, userId, userName, interested, daysConditionDeposit, reached, -1, -1, qualified, daysConditionQualified, skinId);

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            if (rs.next()) {
                return rs.getInt("c");
            }

        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return count;
    }
    
    private static String buildSqlQFI(boolean count, Long userId, String userName, int interested, int daysConditionDeposit, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId) {
        String sql = "" ;
        boolean paging = firstRow > -1 && rowsPerPage > -1;
    
        if(paging) {
            sql += "SELECT * FROM ( select a.*, ROWNUM rnum FROM  ( ";
        }
        sql += 
            "SELECT ";
            if(count){
                sql += " COUNT(*) as c ";
            } else {
                sql += " * ";
            }
            sql +="FROM " + 
                        " ( " +
                            " SELECT " + 
                                    " ur.user_id, " +
                                    " u.user_name, " +
                                    " u.mobile_phone, " +
                                    " u.land_line_phone," +
                                    " s.name as skin_name, " +
                                    " t.time_created first_deposit_date, " +
                                    " reached.action_time as last_time_reached , " +
                                    " reached.user_name as last_caller, " +
                                    " ur.qualified_time qualified_time, " +
                                    " (select count(*) from  issue_actions triss, issues istr where triss.issue_action_type_id = " + IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_TREATMENT + " and triss.issue_id=istr.id and istr.user_id=u.id) as treatment_issue, " +
                                    " DECODE((SELECT COUNT(ia.id) FROM issues i, issue_actions ia WHERE i.user_id = u.id AND ia.issue_id=i.id and ia.issue_action_type_id =  " + IssuesManagerBase.ISSUE_ACTION_NOT_COOPERATIVE + " and i.subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_QUESTIONNAIRE + "), 0, 2, 1) as interested, " +
                                    " pn_docs.TR_CL_DOCS AS last_time_reched_pen_docs "+
                             " FROM " + 
                                    " users_regulation ur, " +
                                    " users u, " +
                                    " skins s, " +
                                    " transactions t ," +
                                    " (select ia.id,w.user_name,ia.action_time from " +
                                           " issue_actions ia, " +
                                           " writers w " +
                                           " where ia.writer_id=w.id) reached, " +
                                    " (SELECT  issues.user_id, " +
                                    "  		MAX(issue_actions.action_time) AS TR_CL_DOCS " +
                                    "  FROM issues, " +
                                    " 		issue_actions, " +
                                    " 		issue_action_types " +
                                    "  WHERE issues.subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
                                    "  		AND issues.id = issue_actions.issue_id " +
                                    "		AND issue_action_types.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL +
                                    "		AND issue_action_types.id=issue_actions.issue_action_type_id " +
                                    "  GROUP BY  issues.user_id) pn_docs " +
                             " WHERE " +
                                    " ur.user_id=u.id " +
                                    " and u.skin_id=s.id ";
                                    if (skinId != 0) {
			                             sql += " AND u.skin_id = " + skinId;	
			                        }else {
				                         sql += " AND u.skin_id in (select ws.skin_id " +
			 			                        " from WRITERS_SKIN ws " +
			 			                        " where ws.writer_id = " + Utils.getWriter().getWriter().getId() + " ) "; 
			                        }
                                    sql +=
                                    " and t.id = u.first_deposit_id " +
                                    " and reached.id(+) = REGULATION.GET_ISSID_QUEST_REACHED(u.id) " +
                                    " and ur.suspended_reason_id = " + UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT +
                                    " and u.id = pn_docs.user_id(+) ";
									if(qualified != 2) {
										sql += " AND ur.qualified = " + qualified;
									}
            
                                    if(userId != null && userId > 0){
                                        sql += " AND u.id = "+userId;
                                    }
                                    if(userName != null && userName.trim().length() > 0){
                                        sql += " AND u.user_name like \'%"+userName.toUpperCase()+"%\'";
                                    }                                   
                                    
                                    sql += ") Where treatment_issue = 0  ";
                                    if( daysConditionDeposit == 1) {
                                        sql += " AND sysdate - first_deposit_date < 30 ";
                                    }
                                    if( daysConditionDeposit == 2) {
                                        sql += " AND sysdate - first_deposit_date >= 30 AND sysdate - first_deposit_date <= 60 ";
                                    }
                                    if( daysConditionDeposit == 3) {
                                        sql += " AND sysdate - first_deposit_date > 60 ";
                                    }
                                    if( daysConditionQualified == 1) {
    									sql += " AND sysdate - first_deposit_date < 30 ";
    								}
    								if( daysConditionQualified == 2) {
    									sql += " AND sysdate - first_deposit_date >= 30 AND sysdate - first_deposit_date < 60 ";
    								}
    								if( daysConditionQualified == 3) {
    									sql += " AND sysdate - first_deposit_date >= 60 AND sysdate - first_deposit_date < 90 ";
    								}
    								if( daysConditionQualified == 4) {
    									sql += " AND sysdate - first_deposit_date >= 90 ";
    								}
                                    if(interested > 0) {
                                        sql += " AND interested = " + interested;
                                    }
                                    
                                    if(reached == 1) {
                                        sql += " AND last_time_reached is not null and  last_time_reached is not null ";
                                    }
                                    
                                    if(reached == 2) {
                                        sql += " AND last_time_reached is null and  last_time_reached is null ";
                                    }
                                    
                                    sql +=" Order by 1";
                if(paging) {
                    sql +=") a where rownum <= " + (firstRow+rowsPerPage) + " ) where rnum  >=" + firstRow;
                }                   
        return sql;
    }
    private static QuestionnaireStatus getQuestionnaireStatusVO(ResultSet rs) throws SQLException {
          QuestionnaireStatus vo = new QuestionnaireStatus();         
          vo.setUserId(rs.getLong("user_id"));
          vo.setUserName(rs.getString("user_name"));
          vo.setMobilePhone(rs.getString("mobile_phone"));
          vo.setLandLinePhone(rs.getString("land_line_phone"));
          vo.setSkinName(rs.getString("skin_name"));
          vo.setFirstDepositDate(rs.getDate("first_deposit_date"));
          vo.setLastCaller(rs.getString("last_caller"));
          vo.setLastTimeReached(rs.getDate("last_time_reached"));
          vo.setUserNotInterested(rs.getString("interested").equals("1"));
          Date now = Calendar.getInstance().getTime();
          Date thirtyDaysFromFirstDeposit = CommonUtil.addDays(vo.getFirstDepositDate(), 30);
          int daysUntilSuspend = CommonUtil.daysBetween(thirtyDaysFromFirstDeposit, now);
          vo.setDaysUntilSuspend(Math.max(daysUntilSuspend, 0));
          vo.setLastTimeCall(rs.getDate("last_time_reched_pen_docs"));
          
          return vo;
      }


}
