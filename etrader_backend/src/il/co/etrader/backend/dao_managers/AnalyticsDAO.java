package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.Analytics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

/**
 * AnalyticsDAO
 * @author eyalo
 */
public class AnalyticsDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(AnalyticsDAO.class);
	
	/**
	 * Get analytics FTD
	 * @param con
	 * @return ArrayList<Analytics>
	 * @throws SQLException
	 */
	public static ArrayList<Analytics> getAnalyticsFtd(Connection con) throws SQLException {
		ArrayList<Analytics> list = new ArrayList<Analytics>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =" SELECT " +
						" 	* " +
						" FROM " +
						"	table(etrader.ANALYTICS.GET_ANALYTICS_DEPOSITS)";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Analytics vo = new Analytics();
				vo.setPlatformId(rs.getLong("platform_id"));
				vo.setPlatformName(rs.getString("platform_name"));
				vo.setFtdCount(rs.getLong("count_ftds"));
				vo.setDepositAmount(rs.getDouble("deposits_amount"));
				vo.setTimeDataFetched(convertToDate(rs.getTimestamp("time_data_fetched")));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}