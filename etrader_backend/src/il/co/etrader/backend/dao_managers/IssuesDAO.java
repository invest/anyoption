package il.co.etrader.backend.dao_managers;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.IssueChannel;
import com.anyoption.common.beans.IssueComponent;
import com.anyoption.common.beans.IssuePriority;
import com.anyoption.common.beans.IssueReachedStatus;
import com.anyoption.common.beans.IssueReaction;
import com.anyoption.common.beans.IssueStatus;
import com.anyoption.common.beans.IssueSubject;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.mbeans.RiskUsersFiles;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.FilesRiskStatus;
import il.co.etrader.bl_vos.IssueReport;
import il.co.etrader.bl_vos.RiskReason;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class IssuesDAO extends IssuesDAOBase{
	  public static void updateSignificantAction(Connection con,IssueAction action) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {

			  String sql="update issue_actions " +
			  			 "set is_significant=? " +
			  			 "where id=?";

				ps = con.prepareStatement(sql);
				ps.setInt(1,(!action.isSignificant())? 1:0);
				ps.setLong(2,action.getActionId());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

		  public static ArrayList<IssueAction> getActionsByIssueId(Connection con, long id, long relevantToCardId, boolean relevantToId, int issueType) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  IssueAction vo = null;
			  ArrayList<IssueAction> issueActions = new ArrayList<IssueAction>();

			  try
				{
				  String sql="select ia.id actionId, ia.writer_id actionWriterId, ia.action_time actionTime, ia.action_time_offset actionTimeOffset, " +
							 	"ia.is_significant actionIsSignificant, iat.channel_id actionCHid, iat.reached_status_id actionRSid, " +
							 	"ia.transaction_id actionTid, ia.comments actionComments, ia.direction_id actionCallDirID,ia.callback_time,ia.callback_time_offset, " +
							 	"c.name cname,  rs.name rsname, w.user_name wname, ia.issue_action_type_id, iat.name actionTypeName ";
							 	if (issueType == ConstantsBase.ISSUE_TYPE_RISK){
						sql+= 	" ,rt.name reasonType, rt.id reasonId ";
								}
						sql+= " from issue_actions ia, issue_channels c,issue_reached_statuses rs, writers w, issue_action_types iat ";
							 if (relevantToCardId != 0 || relevantToId){
						sql+= " ,issue_risk_doc_req rdk ";
							 }
							 if (issueType == ConstantsBase.ISSUE_TYPE_RISK){
						sql+= " ,issue_action_reason_type rt ";
							 }
						sql+="where ia.issue_id = ? " +
							 	"and ia.issue_action_type_id = iat.id(+) " +
							   	"and iat.channel_id=c.id " +
								"and iat.reached_status_id=rs.id(+) " +
								"and ia.writer_id = w.id ";
						 	if (issueType == ConstantsBase.ISSUE_TYPE_RISK){
						sql+=   "and rt.id(+) = ia.risk_reason_id ";
						 	}
						 if (relevantToCardId != 0 || relevantToId){
						sql+=   "and rdk.issue_action_id(+) = ia.id ";
						 }
						 if (relevantToCardId != 0 && relevantToId){
						sql+=   "and (rdk.cc_id = ? " +
									" or (rdk.doc_type = 1)) ";
						 } else {
							if (relevantToId){
						sql+=   "and rdk.doc_type = 1 ";
							}
							else if (relevantToCardId != 0){
						sql+=   "and rdk.cc_id = ? ";
							}
						 }

						 sql+=  "order by ia.action_time desc, ia.id desc ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);
					if (relevantToCardId != 0){
						ps.setLong(2, relevantToCardId);
					}

					rs=ps.executeQuery();

					while (rs.next()) {
						vo = getIssueActionVO(rs);
						vo.setCallBackDate(convertToDate(rs.getTimestamp("callback_time")));
						vo.setCallBackTimeOffset(rs.getString("callback_time_offset"));
						vo.setIssueId(id);
						if (issueType == Constants.ISSUE_TYPE_RISK){
							String type = rs.getString("reasonType");
							if (!CommonUtil.isParameterEmptyOrNull(type)){
								vo.setRiskReasonName(CommonUtil.getMessage(type, null));
								vo.setRiskReasonId(rs.getString("reasonId"));
							}
						}
						issueActions.add(vo);
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return issueActions;
		  }

		  /**
		   * Search issues
		   * @param byWriter - search writer in comments
		   * @return
		   * @throws SQLException
		   */
		  public static ArrayList<Issue> search(Connection con,long populationType ,Date from,Date to,
				  String channelId,String priorityId,String statusId,String subjectId,
				  	String userName,long classId,String skins, boolean byWriter, boolean bySignificantNotes,
				  	  long callDirectionId,String reachedStatusId, String actionTypeId, long writerId, ApplicationData ap, String utcOffset, Locale l, long componentId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Issue> issuesList = new ArrayList<Issue>();

			  ApplicationData app = ap;
			  Issue issueVo = null; 
			  int index = 1;
			  try {
				  String sql = "select i.id iid, i.user_id iuserid, (select count(*) from issue_actions ia2 where ia2.issue_id = i.id) actions_count, i.subject_id isubjectid, i.priority_id ipriorityid, i.status_id istatusid, " +
			  				 	" ia.id actionId, ia.writer_id actionWriterId, ia.action_time actionTime, ia.action_time_offset actionTimeOffset, ia.issue_action_type_id, iat.name actionTypeName, " +
			  				 	" ia.is_significant actionIsSignificant, iat.channel_id actionCHid, iat.reached_status_id actionRSid, " +
			  				 	" ia.transaction_id actionTid, ia.comments actionComments, ia.direction_id actionCallDirID, i.is_significant isIssueSignificant, " +
			  				 	" s.name sname, c.name cname, case when ia.risk_reason_id is not null then (select name from issue_subjects where id = iart.ia_subjuct_id) else b.name end bname, " +
			  				 	" p.name pname, u.user_name username, u.skin_id,  rs.name rsname, w.user_name wname, i.population_entry_id popEntryId, s.type iStatusType, i.type issueType, i.component_id " +
			  				 " from issues i, issue_actions ia, issue_action_types iat, issue_statuses s,issue_channels c,issue_subjects b,issue_priorities p,users u, issue_action_reason_type iart, " +
			  				 	" issue_reached_statuses rs, writers w,(select pu.user_id, pp.population_type_id from population_users pu, population_entries pe, populations pp where pe.id= pu.curr_population_entry_id and pe.population_id=pp.id) pop " +
			  				 " where ia.id = i.last_action_id " +
			  				 	" and ia.issue_action_type_id = iat.id(+) " +
				  			   	" and i.id = ia.issue_id  " +
				  			   	" and i.status_id=s.id " +
							   	" and iat.channel_id=c.id " +
								" and i.subject_id=b.id " +
								" and i.priority_id=p.id " +
								" and i.user_id=u.id " +
								" and iat.reached_status_id=rs.id(+) " +
								" and ia.writer_id = w.id " +
								" AND u.id = pop.user_id(+)" +
								" AND ia.writer_id <> 0 " +
								" AND iart.id(+) = ia.risk_reason_id " +
								" AND i.type = ? ";

				  sql+=" and u.skin_id in (" + skins + ") ";

				  sql+=" and ia.action_time between ? and ? ";

				  	if ( priorityId!=null && !priorityId.equals("") ) {
				  		sql+=" and i.priority_id="+priorityId;
				  	}

				  	if ( statusId!=null && !statusId.equals("") ) {
				  		sql+=" and i.status_id="+statusId;
				  	}

				  	if ( subjectId!=null && !subjectId.equals("") ) {
				  		sql+=" and i.subject_id="+subjectId;
				  	}

				  	if ( channelId!=null && !channelId.equals("") ) {
					    	sql+=" and iat.channel_id="+channelId;
					}

				  	if ( !CommonUtil.isParameterEmptyOrNull(actionTypeId)) {
				    	sql+=" and ia.issue_action_type_id = " + actionTypeId + " ";
				  	}

					if ( callDirectionId!=0) {
				  		sql+=" and ia.direction_id="+callDirectionId;
				  	}

					if ( reachedStatusId!=null && !reachedStatusId.equals("") ) {
				  		sql+=" and iat.reached_status_id="+reachedStatusId;
				  	}

				  	if ( userName!=null && !userName.equals("") ) {
				  		sql+=" and u.user_name like '%"+userName+"%'";
				  	}

				  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {

				  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
				  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
				  		}
				  		else {
				  				sql += " and u.class_id ="+classId;
				  		}
				  	}

				  	if ( byWriter ) {
				  		sql += " and ia.writer_id = " + Utils.getWriter().getWriter().getId() + " ";
				  	}

				  	if (bySignificantNotes){
				  		sql += " and i.is_significant > 0 ";
				  	}


				  	if (writerId != ConstantsBase.ALL_FILTER_ID){
				  		sql += " and ia.writer_id = " + writerId + " ";
				  	}
				  	
				  	if (populationType!=0) {
				  		sql += " and pop.population_type_id = " + populationType + " ";
				  	}
				  	
				  	if (componentId != 0) {
				  		sql += " and i.component_id = ? ";
				  	}

				  	sql+=" order by ia.action_time desc";

					ps = con.prepareStatement(sql);

					ps.setInt(index++ , ConstantsBase.ISSUE_TYPE_REGULAR);
					ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
					ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));
					if (componentId != 0) {
						ps.setLong(index++, componentId);
					}
					rs = ps.executeQuery();
				while (rs.next()) {
					issueVo = getIssueVO(rs, l);
					issueVo.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(), null, l));
					issueVo.setUsername(rs.getString("username"));
					issueVo.setType(rs.getInt("issueType"));
					issuesList.add(issueVo);
	
					issueVo.setIssueActionsCount(rs.getLong("actions_count"));
				}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return issuesList;
		  }

		  /**
		   * Search issues
		   * @param byWriter - search writer in comments
		   * @return
		   * @throws SQLException
		   */
		  public static ArrayList<Issue> searchByUserID(Connection con,long userId,Date from,Date to,
				  String channelId,String priorityId,String statusId,String subjectId,
				  	String userName,long classId,String skins, boolean byWriter, boolean bySignificantNotes,
				  	  long callDirectionId,String reachedStatusId, String actionTypeId, long writerId, long issueType, long componentId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Issue> issuesList = new ArrayList<Issue>();

			  FacesContext context = FacesContext.getCurrentInstance();
			  ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			  Issue issueVo = null;
			  IssueAction issueActionVo = null;
			  long lastIssueId = 0;
			  long currentIssueId = 0;
			  int index = 1;
			  try {
				  String sql=" select i.id iid, i.user_id iuserid, (select count(*) from issue_actions ia2 where ia2.issue_id = i.id) actions_count, " +
						  		" i.subject_id isubjectid, i.priority_id ipriorityid, i.status_id istatusid, i.is_significant isIssueSignificant, " +
			  				 	" ia.id actionId, ia.writer_id actionWriterId, ia.action_time actionTime, ia.action_time_offset actionTimeOffset, " +
			  				 	" ia.is_significant actionIsSignificant, iat.channel_id actionCHid, iat.reached_status_id actionRSid, " +
			  				 	" ia.issue_action_type_id, iat.name actionTypeName, " +
			  				 	" ia.transaction_id actionTid, ia.comments actionComments, ia.direction_id actionCallDirID, lastAction.action_time actionTime, " +
			  				 	" s.name sname, c.name cname, case when ia.RISK_REASON_ID is not null " +
			  				 				" then (select name from ISSUE_SUBJECTS where id = iart.ia_subjuct_id) else b.name end bname, " +
			  				 	" p.name pname, u.user_name username, u.skin_id,  rs.name rsname,  w.user_name wname, " +
			  				 	" i.population_entry_id popEntryId, lpc.chat, lpct.name as chatType, s.type iStatusType, i.type issueType, ss.name ssname, i.component_id " +
			  				 " from " +
			  				 	" issues i, " +
			  				 	" issue_actions ia " +
			  				 		"   LEFT JOIN issue_action_reason_type iart on iart.id = ia.risk_reason_id " +
			  				 		" LEFT JOIN liveperson_chats lpc on ia.chat_id = lpc.id " +
		  				 				" LEFT JOIN liveperson_chat_types lpct on lpc.type_id = lpct.id " +
		  				 			" LEFT JOIN sms sm on ia.sms_id = sm.id " +
		  				 				" LEFT JOIN sms_statuses ss on sm.sms_status_id = ss.id, " +
			  				 	" issue_action_types iat " +
			  				 		" LEFT JOIN issue_reached_statuses rs on iat.reached_status_id=rs.id, " +
			  				 	" issue_statuses s,issue_channels c,issue_subjects b,issue_priorities p,users u, " +
			  				 	" writers w, " +
			  				 	" issue_actions lastAction " +
			  				 " where i.user_id= ?" +
			  				 	" and i.last_action_id = lastAction.id " +
			  				 	" and ia.issue_action_type_id = iat.id " +
			  				 	" and i.user_id = u.id " +
				  			   	" and i.id = ia.issue_id " +
							   	" and iat.channel_id=c.id " +
								" and i.subject_id=b.id " +
								" and i.priority_id=p.id " +
								" and i.status_id=s.id " +
								" and ia.writer_id = w.id " +
								" and lastAction.action_time >=? and lastAction.action_time <=? ";

				  sql+="and u.skin_id in (" + skins + ") ";

				  	if ( priorityId!=null && !priorityId.equals("") ) {
				  		sql+=" and i.priority_id="+priorityId;
				  	}

				  	if ( statusId!=null && !statusId.equals("") ) {
				  		sql+=" and i.status_id="+statusId;
				  	}

				  	if ( subjectId!=null && !subjectId.equals("") ) {
				  		sql+=" and i.subject_id="+subjectId;
				  	}

				  	if ( channelId!=null && !channelId.equals("") ) {
					    	sql+=" and iat.channel_id="+channelId;
					}

				  	if ( !CommonUtil.isParameterEmptyOrNull(actionTypeId)) {
				    	sql+=" and ia.issue_action_type_id = " + actionTypeId + " ";
				  	}

					if ( callDirectionId!=0) {
				  		sql+=" and ia.direction_id="+callDirectionId;
				  	}

					if ( reachedStatusId!=null && !reachedStatusId.equals("") ) {
				  		sql+=" and iat.reached_status_id="+reachedStatusId;
				  	}


				  	if ( userName!=null && !userName.equals("") ) {
				  		sql+=" and u.user_name like '%"+userName+"%'";
				  	}

				  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {

				  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
				  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
				  		}
				  		else {
				  				sql += " and u.class_id ="+classId;
				  		}
				  	}

				  	if ( byWriter ) {
				  		sql += " and ia.writer_id = " + Utils.getWriter().getWriter().getId() + " ";
				  	}

				  	if (writerId != ConstantsBase.ALL_FILTER_ID){
				  		sql += " and ia.writer_id = " + writerId + " ";
				  	}

				  	if (bySignificantNotes){
				  		sql += " and i.is_significant > 0 ";
				  	}

				  	if (issueType != 0){
				  		sql += " and i.type = ? ";
				  	}
				  	
				  	if (componentId != 0) {
				  		sql += " and i.component_id = ? ";
				  	}

				  	sql+=" order by lastAction.action_time desc, ia.action_time desc, ia.id desc";

					ps = con.prepareStatement(sql);

					ps.setLong(index++, userId);
					ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
					ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
					if (issueType != 0){
						ps.setLong(index++, issueType);
				  	}
					if (componentId != 0) {
						ps.setLong(index++, componentId);
					}
					
					rs = ps.executeQuery();

					while (rs.next()) {
						currentIssueId = rs.getLong("iid");
						if (currentIssueId != lastIssueId){
							lastIssueId = currentIssueId;
							issueVo=getIssueVO(rs);
							issueVo.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
							issueVo.setUsername(rs.getString("username"));
							issueVo.setType(rs.getInt("issueType"));
							issuesList.add(issueVo);
						}

						issueActionVo = getIssueActionVO(rs);
						issueActionVo.setIssueId(currentIssueId);
						if (null != rs.getString("chat")) {
							issueVo.setChat(rs.getString("chat"));
							if (null != rs.getString("chatType")) {
								issueVo.setChatType(rs.getString("chatType"));
							}
						}
						if (null != rs.getString("ssname")) {
							issueVo.setSmsStatus(rs.getString("ssname"));
						}
						issueVo.setComponentId(rs.getInt("component_id"));
						issueVo.addActionToIssueActionsList(issueActionVo);
						issueVo.setIssueActionsCount(rs.getLong("actions_count"));
						
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return issuesList;
		  }

		  /**
		   * Search issues
		   * @param byWriter - search writer in comments
		   * @return
		   * @throws SQLException
		   */
		  public static ArrayList<Issue> searchByPopUserID(Connection con,long populationUsersId,Date from,Date to,
				  String channelId,String priorityId,String statusId,String subjectId,
				  	String userName,long classId,String skins, boolean byWriter, boolean bySignificantNotes,
				  	  long callDirectionId,String reachedStatusId, String actionTypeId, long writerId, long issueType, long componentId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Issue> issuesList = new ArrayList<Issue>();
			  Issue issueVo = null;
			  IssueAction issueActionVo = null;
			  long lastIssueId = 0;
			  long currentIssueId = 0;
			  int index = 1;

			  try {

				  String sql=" select " +
				  				" i.id iid, i.user_id iuserid, i.subject_id isubjectid, i.priority_id ipriorityid, i.status_id istatusid, i.is_significant isIssueSignificant, " +
			  				 	" ia.id actionId, ia.writer_id actionWriterId, ia.action_time actionTime, ia.action_time_offset actionTimeOffset,  " +
			  				 	" ia.is_significant actionIsSignificant, iat.channel_id actionCHid, iat.reached_status_id actionRSid, ia.issue_action_type_id, iat.name actionTypeName, " +
			  				 	" ia.transaction_id actionTid, ia.comments actionComments, ia.direction_id actionCallDirID, lastAction.action_time actionTime,  " +
			  				 	" s.name sname, c.name cname, case when ia.RISK_REASON_ID is not null " +
			  				 							" then (select name from ISSUE_SUBJECTS where id = iart.ia_subjuct_id) " +
			  				 							" else b.name end bname, " +
			  				 	" p.name pname, rs.name rsname, w.user_name wname, i.population_entry_id popEntryId, s.type iStatusType, i.type issueType, i.component_id " +
			  				 " from " +
			  				 	" population_entries pe, issues i, issue_actions ia, issue_statuses s,issue_channels c,issue_subjects b,issue_priorities p, issue_action_types iat, " +
			  				 	" issue_reached_statuses rs, writers w,issue_actions lastAction, issue_action_reason_type iart " +
			  				 " where pe.population_users_id = ? " +
			  				 	" and i.last_action_id = lastAction.id " +
			  				 	" and ia.issue_action_type_id = iat.id(+) " +
			  				 	" and i.population_entry_id = pe.id " +
				  			   	" and i.id = ia.issue_id " +
							   	" and iat.channel_id=c.id " +
								" and i.subject_id=b.id " +
								" and i.priority_id=p.id " +
								" and i.status_id=s.id " +
								" and iat.reached_status_id=rs.id(+) " +
								" and ia.writer_id = w.id " +
								" and iart.id = ia.risk_reason_id(+) " +
								" and lastAction.action_time >=? and lastAction.action_time <=? ";

//				  sql+="and u.skin_id in (" + skins + ") ";

				  	if ( priorityId!=null && !priorityId.equals("") ) {
				  		sql+=" and i.priority_id="+priorityId;
				  	}

				  	if ( statusId!=null && !statusId.equals("") ) {
				  		sql+=" and i.status_id="+statusId;
				  	}

				  	if ( subjectId!=null && !subjectId.equals("") ) {
				  		sql+=" and i.subject_id="+subjectId;
				  	}

				  	if ( channelId!=null && !channelId.equals("") ) {
					    	sql+=" and iat.channel_id="+channelId;
				    }

				  	if ( !CommonUtil.isParameterEmptyOrNull(actionTypeId)) {
				    	sql+=" and ia.issue_action_type_id = " + actionTypeId + " ";
				  	}

					if ( callDirectionId!=0) {
				  		sql+=" and ia.direction_id="+callDirectionId;
				  	}

					if ( reachedStatusId!=null && !reachedStatusId.equals("") ) {
				  		sql+=" and iat.reached_status_id="+reachedStatusId;
				  	}

				  	if ( userName!=null && !userName.equals("") ) {
				  		sql+=" and u.user_name like '%"+userName+"%'";
				  	}

//				  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {
//
//				  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
//				  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
//				  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
//				  		}
//				  		else {
//				  				sql += " and u.class_id ="+classId;
//				  		}
//				  	}

				  	if ( byWriter ) {
				  		sql += " and ia.writer_id = " + Utils.getWriter().getWriter().getId() + " ";
				  	}

				  	if (writerId != ConstantsBase.ALL_FILTER_ID){
				  		sql += " and ia.writer_id = " + writerId + " ";
				  	}

				  	if (bySignificantNotes){
				  		sql += " and i.is_significant > 0 ";
				  	}

				  	if (issueType != 0){
				  		sql += " and i.type = ? ";
				  	}

				  	if (componentId != 0) {
				  		sql += " and i.component_id = ? ";
				  	}
				  	
				  	sql+=" order by lastAction.action_time desc, ia.action_time desc, ia.id desc ";

					ps = con.prepareStatement(sql);

					ps.setLong(index++, populationUsersId);
					ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
					ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
					if (issueType != 0){
						ps.setLong(index++, issueType);
				  	}
					if (componentId != 0){
						ps.setLong(index++, componentId);
				  	}

					rs = ps.executeQuery();
					while (rs.next()) {
						currentIssueId = rs.getLong("iid");
						if (currentIssueId != lastIssueId){
							lastIssueId = currentIssueId;
							issueVo=getIssueVO(rs);
							issueVo.setType(rs.getInt("issueType"));
//							test issueVo.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
							issuesList.add(issueVo);
						}

						issueActionVo = getIssueActionVO(rs);
						issueActionVo.setIssueId(currentIssueId);
						issueVo.setComponentId(rs.getInt("component_id"));
						issueVo.addActionToIssueActionsList(issueActionVo);

					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return issuesList;

		  }

		  public static boolean checkOpenIssues(Connection con,long userId) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  try
				{
				    String sql="select * from issues where user_id=? and status_id!=?";

					ps = con.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setLong(2, IssuesManagerBase.ISSUE_STATUS_FINISHED);

					rs=ps.executeQuery();

					if (rs.next()) {
						return true;
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return false;

		  }

		  public static boolean checkSignificantIssues(Connection con,int userId) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  try
				{
				    String sql=	"select * " +
				    			"from issues i, issue_actions ia " +
								"where user_id=? " +
									"and i.id = ia.issue_id "+
									"and ia.is_significant=1";

					ps = con.prepareStatement(sql);
					ps.setInt(1, userId);
					rs=ps.executeQuery();

					if (rs.next()) {
						return true;
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return false;

		  }


		  public static LinkedHashMap<Long,IssueSubject> getIssueSubjects(Connection con, int type) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  LinkedHashMap<Long,IssueSubject> hm = new LinkedHashMap<Long,IssueSubject>();

			  try
				{
				  	// Get all subjects but population.
				    String sql=" select * " +
				    			" from issue_subjects " +
				    			" where is_displayed = ? " +
				    			" order by id";

					ps = con.prepareStatement(sql);
					ps.setInt(1, type);
					rs=ps.executeQuery();

					while (rs.next()) {
						IssueSubject vo=new IssueSubject();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						hm.put(rs.getLong("id"),vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;

		  }

		  public static LinkedHashMap<Long, RiskReason> getRiskReasons(Connection con) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  LinkedHashMap<Long, RiskReason> hm = new LinkedHashMap<Long, RiskReason>();

			  try {
				    String sql =" SELECT " +
				    				" * " +
				    			" FROM " +
				    				" issue_action_reason_type " +
				    			" WHERE " +
				    				" is_active = 1 " +
				    			" ORDER BY " +
				    				" id ";

					ps = con.prepareStatement(sql);
					rs=ps.executeQuery();

					while (rs.next()) {
						RiskReason vo = new RiskReason();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						hm.put(rs.getLong("id"),vo);
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;
		  }

		  public static LinkedHashMap<Long,IssueReachedStatus> getIssueReachedStatuses(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  LinkedHashMap<Long,IssueReachedStatus> hm=new LinkedHashMap<Long,IssueReachedStatus>();

			  try
				{
				    String sql="select * " +
				    			"from issue_reached_statuses " +
				    			"where id != 3 " + // remove false account from display
				    			"order by id";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						IssueReachedStatus vo=new IssueReachedStatus();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						hm.put(rs.getLong("id"),vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;

		  }

		  public static LinkedHashMap<Long,IssueActionType> getIssueActionTypes(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  LinkedHashMap<Long,IssueActionType> hm=new LinkedHashMap<Long,IssueActionType>();

			  try
				{
				    String sql= " select * " +
				    		    " from issue_action_types " +
				    		    " where is_active = 1 " +
				    		    " order by id";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						IssueActionType vo = new IssueActionType();
						long actionTypeId = rs.getLong("id");
						vo.setId(actionTypeId);
						vo.setName(rs.getString("name"));
						vo.setDepositSearchType(rs.getInt("deposit_search_type"));
						vo.setChannelId(rs.getLong("channel_id"));
						vo.setReachedStatusId(rs.getLong("Reached_Status_Id"));
						hm.put(actionTypeId,vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;

		  }

		  public static ArrayList<IssueActionType> getIssueActionTypesList(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<IssueActionType> list=new  ArrayList<IssueActionType>();

			  try
				{
				    String sql= " select * " +
				    			" from issue_action_types " +
				    			" where is_active = 1 " +
				    			" order by name";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						IssueActionType vo = new IssueActionType();
						long actionTypeId = rs.getLong("id");
						vo.setId(actionTypeId);
						vo.setName(rs.getString("name"));
						vo.setDepositSearchType(rs.getInt("deposit_search_type"));
						vo.setChannelId(rs.getLong("channel_id"));
						vo.setReachedStatusId(rs.getLong("Reached_Status_Id"));
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  public static HashMap<Long,IssuePriority> getIssuePrioritues(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  HashMap<Long,IssuePriority> hm=new HashMap<Long,IssuePriority>();

			  try
				{
				    String sql="select * from issue_priorities order by id";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						IssuePriority vo=new IssuePriority();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						hm.put(rs.getLong("id"),vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;

		  }
		  public static HashMap<Long,IssueStatus> getIssueStatus(Connection con, int type) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  HashMap<Long,IssueStatus> hm=new HashMap<Long,IssueStatus>();

			  try
				{
				    String sql = " select " +
				    				" * " +
				    			 " from " +
				    			 	" issue_statuses " +
				    			 " where " +
				    			 	" type = ? " +
				    			 " order by id ";

					ps = con.prepareStatement(sql);
					ps.setInt(1, type);
					rs=ps.executeQuery();

					while (rs.next()) {
						IssueStatus vo = new IssueStatus();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setType(rs.getInt("type"));
						vo.setSupportStatus(rs.getInt("support_status")==0?false:true);
						hm.put(rs.getLong("id"),vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return hm;

		  }
		  public static HashMap<Long,IssueChannel> getIssueChannels(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  HashMap<Long,IssueChannel> hm=new HashMap<Long,IssueChannel>();

			  try
				{
				    String sql="select * from issue_channels order by id";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						IssueChannel vo=new IssueChannel();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						hm.put(rs.getLong("id"), vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}

				return hm;

		  }

		  /**
		   * Return the issueSubject of last issue with subjectid 12 of a specified userid
		   * @param con DB connection
		   * @param userId - user id
		   * @return Return the issueSubject of last issue with subjectid 12 of a specified userid
		   * @throws SQLException
		   */
		  public static IssueStatus getIdDocumentStatusByUserId(Connection con, long userId) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  IssueStatus issueStat=null;

			  try
				{
				    String sql="select s.id, s.name from issues i, issue_statuses s where i.status_id=s.id and i.user_id="+userId+" and i.subject_id="+ConstantsBase.ISSUE_ID_DOCUMENT +" and rownum=1";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						issueStat = new IssueStatus();
						issueStat.setId(rs.getLong("id"));
						issueStat.setName(CommonUtil.getMessage(rs.getString("name"), null));
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}

				return issueStat;

		  }

		  private static Issue getIssueVO(ResultSet rs, Locale l) throws SQLException{
				Issue vo=new Issue();
				// Get Issue Fields
				vo.setId(rs.getLong("iid"));
				vo.setUserId(rs.getLong("iuserid"));
				vo.setSubjectId(rs.getString("isubjectid"));
				vo.setPriorityId(rs.getString("ipriorityid"));
				vo.setStatusId(rs.getString("istatusid"));
				vo.setPopulationEntryId(rs.getLong("popEntryId"));

				IssueStatus is=new IssueStatus();
				is.setId(rs.getLong("istatusid"));
				is.setName(CommonUtil.getMessage(rs.getString("sname"),null, l));
				is.setType(rs.getInt("iStatusType"));
				vo.setStatus(is);

				IssueSubject ib=new IssueSubject();
				ib.setId(rs.getLong("isubjectid"));
				// if subject id is population subject id enter the population type instead of subject name + (P).
				if (IssuesManagerBase.ISSUE_SUBJ_POPULATION == ib.getId()){
					if (!CommonUtil.isParameterEmptyOrNull(PopulationsManagerBase.getNameByPopEntryID(vo.getPopulationEntryId()))) {
						ib.setName(PopulationsManagerBase.getNameByPopEntryID(vo.getPopulationEntryId()) +
							ConstantsBase.POPULATION_SIGN);
					} else {
						ib.setName(CommonUtil.getMessage("issuesubject.population.type",null, l));
					}
				}else{
					ib.setName(CommonUtil.getMessage(rs.getString("bname"),null, l));
				}
				vo.setSubject(ib);

				IssuePriority p=new IssuePriority();
				p.setId(rs.getLong("ipriorityid"));
				p.setName(CommonUtil.getMessage(rs.getString("pname"),null, l));
				vo.setPriority(p);


				// Get last issue action fields.
				vo.setLastActionTime(convertToDate(rs.getTimestamp("actionTime")));
				vo.setLastActionTimeOffset(rs.getString("actionTimeOffset"));
				vo.setLastSignificant(rs.getInt("isIssueSignificant")>0);
				vo.setLastChannelId(rs.getString("actionCHid"));
				vo.setLastReachedStatusId(rs.getString("actionRSid"));
				vo.setLastComments(rs.getString("actionComments"));
				vo.setLastCallDirectionId(rs.getLong("actionCallDirID"));
				vo.setLastIssueActionType(CommonUtil.getMessage(rs.getString("actionTypeName"),null, l));

				IssueChannel ic=new IssueChannel();
				ic.setId(rs.getLong("actionCHid"));
				ic.setName(CommonUtil.getMessage(rs.getString("cname"),null, l));
				vo.setLastChannel(ic);

				IssueReachedStatus irs = new IssueReachedStatus();
				irs.setId(rs.getLong("actionRSid"));
				irs.setKey(rs.getString("rsname"));
				irs.setName(CommonUtil.getMessage(rs.getString("rsname"),null, l));
				vo.setLastReachedStatus(irs);

				return vo;
		  }

		  private static Issue getIssueVO(ResultSet rs) throws SQLException{
			  return getIssueVO(rs, Utils.getWriter().getLocalInfo().getLocale());
		  }

		  private static IssueAction getIssueActionVO(ResultSet rs) throws SQLException{
				IssueAction vo=new IssueAction();
				vo.setActionId(rs.getLong("actionId"));
				vo.setWriterId(rs.getLong("actionWriterId"));
				vo.setWriterName(rs.getString("wname"));
				vo.setActionTime(convertToDate(rs.getTimestamp("actionTime")));
				vo.setActionTimeOffset(rs.getString("actionTimeOffset"));
				vo.setSignificant(rs.getInt("actionIsSignificant")==1);
				vo.setReachedStatusId(rs.getString("actionRSid"));
				vo.setComments(rs.getString("actionComments"));
				vo.setTransactionId(rs.getLong("actionTid"));
				vo.setCallDirectionId(rs.getLong("actionCallDirID"));
				vo.setActionTypeId(rs.getString("issue_action_type_id"));
				vo.setActionTypeName(CommonUtil.getMessage(rs.getString("actionTypeName"),null, Utils.getWriter().getLocalInfo().getLocale()));

				IssueChannel ic=new IssueChannel();
				ic.setId(rs.getLong("actionCHid"));
				ic.setName(CommonUtil.getMessage(rs.getString("cname"),null, Utils.getWriter().getLocalInfo().getLocale()));
				vo.setChannel(ic);

				IssueReachedStatus irs = new IssueReachedStatus();
				irs.setId(rs.getLong("actionRSid"));
				irs.setKey(rs.getString("rsname"));
				irs.setName(CommonUtil.getMessage(rs.getString("rsname"),null, Utils.getWriter().getLocalInfo().getLocale()));
				vo.setReachedStatus(irs);

				return vo;
		  }

	/**
	 * Search for issue action that conflicting with the new issue action time
	 * @param con
	 * @param writerId
	 * @param callBackDate
	 * @return Issue action
	 * @throws SQLException
	 */
	public static IssueAction searchForAssignCallBackByWriter(Connection con,long writerId,Date callBackDate) throws SQLException{

		PreparedStatement ps = null;
		ResultSet rs = null;

		IssueAction issueAction = null;
		Date currnetIaCbTimeBefore;
		Date currnetIaCbTimeAfter;
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(callBackDate);
		//set 15 minutes interval for partner writers
		if (Utils.getUser().isPartnerWriter()){
			gc.add(Calendar.MINUTE,-(Constants.CALL_BACK_INTERVAL_PARTNERS-1));
		} else {
			gc.add(Calendar.MINUTE,-(Constants.CALL_BACK_INTERVAL-1));
		}
		currnetIaCbTimeBefore = gc.getTime();
		gc.setTime(callBackDate);
		//set 15 minutes interval for partner writers
		if (Utils.getUser().isPartnerWriter()){
			gc.add(Calendar.MINUTE,(Constants.CALL_BACK_INTERVAL_PARTNERS-1));
		} else {
			gc.add(Calendar.MINUTE,(Constants.CALL_BACK_INTERVAL-1));
		}
		currnetIaCbTimeAfter = gc.getTime();

		try {
			String sql ="SELECT " +
							"ia.* " +
						"FROM " +
							"issue_actions ia " +
						"WHERE " +
							"ia.issue_id IN (SELECT " +
												"i.id " +
											  "FROM " +
											  	"population_users pu " +
												  	"LEFT JOIN issue_actions ia ON ia.id = pu.entry_type_action_id " +
												  		"LEFT JOIN issues i ON i.id=ia.issue_id " +
											  "WHERE " +
											  	"pu.curr_assigned_writer_id = ? " +
											  	"AND pu.entry_type_id = ?) " +
			  				"AND ia.callback_time is not null " +
				            "AND ia.callback_time between ? AND ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1,writerId);
			ps.setInt(2,ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
			ps.setTimestamp(3,CommonUtil.convertToTimeStamp(currnetIaCbTimeBefore));
			ps.setTimestamp(4,CommonUtil.convertToTimeStamp(currnetIaCbTimeAfter));
			rs = ps.executeQuery();

			if (rs.next()) {
				issueAction = new IssueAction();
				issueAction.setWriterId(rs.getLong("writer_id"));
				issueAction.setCallBackDate(convertToDate(rs.getTimestamp("callback_time")));
				issueAction.setCallBackTimeOffset(rs.getString("CALLBACK_TIME_OFFSET"));
				issueAction.setActionId(rs.getLong("id"));
				issueAction.setIssueId(rs.getLong("issue_id"));
			}
		}
		finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
       return issueAction;
	}

	  /**
	   * isExistsMarketingOperationId
	   * @param moId - marketing operation id
	   * @return
	   * @throws SQLException
	   */
	  public static boolean isExistsMarketingOperationId(Connection con,long moId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql="select * " +
		  				 "from marketing_operations " +
		  				 "where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1,moId);
				rs = ps.executeQuery();
				if (rs.next()) {
					return true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
	  }

	  /**
	   *
	   * @param moId - marketing operation id
	   * @return
	   * @throws SQLException
	   */
	  public static HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> getIssueActionTypes(Connection con, long writerId, ArrayList<SelectItem> issueChannelsSI) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;


		  HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> hash = new HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>>();

		  hash.put(ConstantsBase.SCREEN_SALE, new HashMap<Long, ArrayList<IssueActionType>>());
		  hash.put(ConstantsBase.SCREEN_SALEM, new HashMap<Long, ArrayList<IssueActionType>>());
		  hash.put(ConstantsBase.SCREEN_SUPPORT, new HashMap<Long, ArrayList<IssueActionType>>());
		  hash.put(ConstantsBase.SCREEN_SUPPORTMO, new HashMap<Long, ArrayList<IssueActionType>>());
		  hash.put(ConstantsBase.SCREEN_ACCOUNTING, new HashMap<Long, ArrayList<IssueActionType>>());
		  hash.put(ConstantsBase.SCREEN_ADMIN, new HashMap<Long, ArrayList<IssueActionType>>());

		  for (SelectItem ic: issueChannelsSI){
			 hash.get(ConstantsBase.SCREEN_SALE).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
			 hash.get(ConstantsBase.SCREEN_SALEM).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
			 hash.get(ConstantsBase.SCREEN_SUPPORT).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
			 hash.get(ConstantsBase.SCREEN_SUPPORTMO).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
			 hash.get(ConstantsBase.SCREEN_ACCOUNTING).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
			 hash.get(ConstantsBase.SCREEN_ADMIN).put(Long.valueOf(ic.getValue().toString()),new ArrayList<IssueActionType>());
		  }

		  try {

			  String sql =	"SELECT DISTINCT " +
			  					"iat.*, iar.screen_id " +
			  				"FROM " +
			  					"writers w, " +
			  					"roles r, " +
			  					"role_types rt, " +
			  					"issue_action_roles iar, " +
			  					"issue_action_types iat " +
			  				"WHERE " +
			  					" w.user_name = r.user_name " +
			  					"AND r.role = rt.name " +
			  					"AND iar.role_id = rt.id " +
			  					"AND iat.id = iar.issue_action_type_id " +
			  					"AND iat.id not in (" + getWriterIssueActiotPermission() +") ";
			  
			  if (writerId == Constants.GADI_USER_ID || writerId == Constants.MARCELO_USER_ID || writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.DANYD_USER_ID || writerId == Constants.MARYVS_USER_ID || writerId == Constants.OANAD_USER_ID || writerId == Constants.OFIRS_USER_ID) { //marcelo & gadi & Adi K & Dany D get actiontype id 40 & 41 			
				  sql +=	"AND ((iat.is_active = 1 ";
			  } else {
				  sql +=	"AND (iat.is_active = 1 ";
			  }
			  	  sql +=				"AND w.id = ? ) ";

			  if (writerId == Constants.GADI_USER_ID || writerId == Constants.MARCELO_USER_ID || writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.DANYD_USER_ID || writerId == Constants.MARYVS_USER_ID || writerId == Constants.OANAD_USER_ID || writerId == Constants.OFIRS_USER_ID) { //marcelo & gadi & Adi K & Dany D get actiontype id 40 & 41
				  sql += " OR (iar.issue_action_type_id = iat.id " +
					  	 " AND (iat.id = ? " +
					  	 " OR iat.id = ? )))";
			  }

				ps = con.prepareStatement(sql);
				ps.setLong(1, writerId);
				ps.setLong(2, writerId);
				if (writerId == Constants.GADI_USER_ID || writerId == Constants.MARCELO_USER_ID || writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.DANYD_USER_ID || writerId == Constants.MARYVS_USER_ID || writerId == Constants.OANAD_USER_ID || writerId == Constants.OFIRS_USER_ID) { //marcelo & gadi & Adi K & Dany D get actiontype id 40 & 41
					ps.setLong(3, Constants.ISSUE_ACTION_REQUEST_DOCUMENTS);
					ps.setLong(4, Constants.ISSUE_ACTION_REQUEST_CONTACT_USER);
				}
				rs=ps.executeQuery();
				while (rs.next()) {
					IssueActionType vo = new IssueActionType();
					long channelId = rs.getLong("channel_id");
					int screenId= rs.getInt("screen_Id");
					vo.setId(rs.getLong("id"));
					vo.setChannelId(channelId);
					vo.setName(rs.getString("name"));
					vo.setReachedStatusId(rs.getLong("reached_status_id"));
					vo.setScreenId(screenId);
					if (null != hash.get(screenId)){
						hash.get(screenId).get(channelId).add(vo);
					}
				}

			}


			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hash;

	  }
	  
	  private static String getWriterIssueActiotPermission() {
		  String sql ="Select issue_action_type_id "
		  		   + " From " +
				  		" (SELECT distinct a.issue_action_type_id, "
				  			+ " nvl(prem.issue_action_type_id, -1) w_prem " +
				  		 " FROM " + 
				  		 " issue_action_writer_permission a, " +
				  		 " (select issue_action_type_id  " + 
				  		  " from " +
				  		 	" issue_action_writer_permission " + 
				  		 	" where " +
				  		    " writer_id = ? and is_active = 1) prem " +
				  		 " WHERE " + 
				  		 " a.issue_action_type_id = prem.issue_action_type_id(+) " +
				  		 " and a.is_active = 1 " +
				  		 " ) " +
				   " Where w_prem = -1 ";
		  return sql;

		
	}

	  /**
	   * getCallCenterHours
	   * @return call center work hours
	   * @throws SQLException
	   */
	  public static HashMap<Integer, ArrayList<String>> getCallCenterHours(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
		  ArrayList<String> weekList;
		  String dailyHours = null;
		  String startHour = null;

		  try {

			  String sql =	" SELECT *  " +
			  				" FROM CALL_CENTER_HOURS " +
			  				" ORDER BY SKIN_ID, DAY_OF_WEEK ";

			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();

			  while (rs.next()) {
				  weekList = new ArrayList<String>();

				  for (int i=1; i<=7 ; i++){

					  startHour = rs.getString("START_HOUR");
					  if (null != startHour){
						  dailyHours = startHour + "-" + rs.getString("END_HOUR");
					  }else{
						  dailyHours = null;
					  }
					  weekList.add(dailyHours);

					  if (i != 7){
						  rs.next();
					  }
				  }
				  hm.put(rs.getInt("SKIN_ID"), weekList);
			  }

		  }	finally	{
				closeResultSet(rs);
				closeStatement(ps);
		  }
		  return hm;
	  }


	  public static ArrayList<Issue> getRiskIssues(Connection con, String skins, String priorityId, String statusId, String subjectId,
			  										String userName, Date from,Date to, boolean isAdmin, String statuses, long classId,
			  										String userId, long riskIssueType , String searchBy) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Issue> issuesList = new ArrayList<Issue>();

		  FacesContext context = FacesContext.getCurrentInstance();
		  ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		  Issue issueVo = null;
		  int index = 1;

		  try {

			  String sql =	" SELECT " +
			  					" i.id iid, " +
			  					" i.user_id iuserid, " +
			  					" i.subject_id isubjectid, " +
			  					" i.priority_id ipriorityid, " +
			  					" i.status_id istatusid, " +
			  					" i.time_created, " +
			  					" i.type, " +
			  					" ia.id actionId, " +
			  					" ia.writer_id actionWriterId, " +
			  					" ia.action_time actionTime, " +
			  					" ia.action_time_offset actionTimeOffset, " +
			  					" ia.issue_action_type_id, " +
			  					" iat.name actionTypeName, " +
			  					" ia.is_significant actionIsSignificant, " +
			  					" iat.channel_id actionCHid, " +
			  					" iat.reached_status_id actionRSid, " +
			  					" ia.transaction_id actionTid, " +
			  					" ia.comments actionComments, " +
			  					" ia.direction_id actionCallDirID, " +
			  					" i.is_significant isIssueSignificant, " +
			  					" s.name sname, " +
			  					" s.id istatusid, " +
			  					" s.type iStatusType, " +
			  					" c.name cname, " +
			  					" b.name bname, " +
			  					" p.name pname, " +
			  					" u.user_name username, " +
			  					" u.skin_id,  " +
			  					" rs.name rsname, " +
			  					" w.user_name wname, " +
			  					" i.population_entry_id popEntryId," +
			  					" trunc( ia.action_time - i.time_created ) days_till_u,  " +
			  					" trunc( sysdate - ia.action_time ) days_without_t," +
			  					" coun.display_name country, " +
			  					" last_reached.action_time last_reached_date, " +
			  					" last_call.action_time last_call_date " +
			  				" FROM " +
			  					" issues i, " +
			  					" issue_actions last_reached, " +
			  					" issue_actions last_call, " +
			  					" issue_actions ia, " +
			  					" issue_action_types iat, " +
			  					" issue_statuses s, " +
			  					" issue_channels c, " +
			  					" issue_subjects b, " +
			  					" issue_priorities p, " +
			  					" users u, " +
			  					" issue_reached_statuses rs, " +
			  					" writers w, " +
			  					" countries coun " +
			  				" WHERE " +
			  					" ia.id = i.last_action_id " +
			  					" AND last_reached.id(+) = i.last_reached_call_action_id " +
			  					" AND last_call.id(+) = i.last_call_action_id " +
			  					" AND ia.issue_action_type_id = iat.id(+) " +
			  					" AND i.id = ia.issue_id  " +
							   	" AND i.status_id=s.id " +
							   	" AND iat.channel_id=c.id " +
								" AND i.subject_id=b.id " +
								" AND i.priority_id=p.id " +
								" AND i.user_id=u.id " +
								" AND iat.reached_status_id=rs.id(+) " +
								" AND ia.writer_id = w.id " +
								" AND i.type = " + ConstantsBase.ISSUE_TYPE_RISK +
								" AND coun.id = u.country_id " +
								//" AND ia.writer_id <> 0 " +
								" AND u.skin_id in (" + skins + ") ";
			  
			  
			  if(searchBy.equals(ConstantsBase.ISSUES_TIME_CREATED)){
					sql += " AND i.time_created  >= ? AND i.time_created <= ? ";
				}else {
					sql += " AND ia.action_time >= ? AND ia.action_time <= ? ";
				}

			if ( priorityId != null && !priorityId.equals("") ) {
				  		sql +=	" AND i.priority_id = " + priorityId;
			}

			if (riskIssueType == IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER){
						sql+=	" AND i.id in ( SELECT " +
												" ia2.issue_id " +
											  " FROM " +
											  	" issue_actions ia2, issue_risk_doc_req rq " +
											  " WHERE " +
											  	" ia2.id = rq.issue_action_id " +
											  	" AND rq.doc_type = " + Constants.RISK_DOC_REQ_CONTACT_USER + " ) ";
				
			} else if(riskIssueType == IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT){ // need document
						sql+=	" AND i.id in ( SELECT " +
												" irs.issue_id " +
											  " FROM " +
											  	" issue_req_status irs " +
											  " WHERE " +
											  	" irs.req_type <> " + Constants.RISK_DOC_REQ_CONTACT_USER + " ";
								if (statuses.equals("0")){
									if (!isAdmin){ // all status for support
										sql+=	" AND irs.req_status in (?,?,?,?) ";
									}
								} else {
										sql+=	" AND irs.req_status in (" + statuses + ") ";
								}
								sql+=		  	") ";
			}
			if (statuses.equals("0")){
				if (!isAdmin){ // all status for support
					sql+=	" AND i.status_id in (?,?,?,?) ";
				}
			} else {
					sql+=	" AND i.status_id in (" + statuses + ") ";
			}

								if ( classId != ConstantsBase.USER_CLASS_ALL )   {
									if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  		sql +=  " AND (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
				  				  " OR u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
									} else {
				  		sql +=  " AND u.class_id ="+classId;
									}
								}

							  	if ( subjectId != null && !subjectId.equals("") ) {
						sql+=	" AND i.id in  (  SELECT " +
													" i2.id " +
												" FROM " +
													" issue_actions ia2, issues i2 " +
												" WHERE " +
													" i2.id = i.id " +
													" AND ia2.issue_id = i.id " +
													" AND ia2.risk_reason_id = " + subjectId + ") ";
							  	}
							  	if ( userName!=null && !userName.equals("") ) {
						sql+=	" AND u.user_name like '%" + userName.toUpperCase() + "%'";
							  	}

								if (!CommonUtil.isParameterEmptyOrNull(userId)) {
						sql +=  " AND u.id = " + userId;
								}

						sql+=" ORDER BY " +
								" i.priority_id desc, ia.action_time desc  ";

				ps = con.prepareStatement(sql);

				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

				if (statuses.equals("0") && !isAdmin) {
					ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_NEW_ISSUE));
					ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_ADDITIONAL_REQUEST));
					ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_IN_PROGRESS));
					ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_NEED_APPROVE));
					if (riskIssueType == IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT) {
						ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_NEW_ISSUE));
						ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_ADDITIONAL_REQUEST));
						ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_IN_PROGRESS));
						ps.setString(index++, String.valueOf(IssuesManagerBase.RISK_STATUS_NEED_APPROVE));
					}
				}
				rs = ps.executeQuery();

				while (rs.next()) {
					issueVo = getIssueVO(rs);
					issueVo.setDaysInProgress(rs.getInt("days_till_u"));
					issueVo.setDaysWithoutTreatment(rs.getInt("days_without_t"));
					issueVo.setCountry(CommonUtil.getMessage(rs.getString("country"), null));
					issueVo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
					issueVo.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
					issueVo.setUsername(rs.getString("username"));
					issueVo.setLastCallDate(convertToDate(rs.getTimestamp("last_call_date")));
					issueVo.setLastReachedCallDate(convertToDate(rs.getTimestamp("last_reached_date")));
					issueVo.setType(rs.getInt("type"));
					issuesList.add(issueVo);
				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
		return issuesList;
	  }


	/**
	 * check if there are open risk issues
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean checkOpenRiskIssues(Connection con,long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		    String sql =" SELECT " +
		    				" 1 " +
		    			" FROM " +
		    				" issues i," +
		    				" issue_statuses ist " +
		    			" WHERE " +
		    				" ist.id = i.status_id " +
		    				" AND ist.type = ? " +
		    				" AND i.user_id = ? " +
		    				" AND i.status_id != ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, IssueStatus.ISSUE_STATUS_TYPE_RISK);
			ps.setLong(2, userId);
			ps.setLong(3, IssuesManagerBase.RISK_STATUS_CLOSED);

			rs=ps.executeQuery();

			if (rs.next()) {
				return true;
			}
		}

		finally	{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	  }


	/**
	 * Check if need to change risk issue status to need approve
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean needFilesApprove(Connection con,long userId, long ccId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		    String sql =" SELECT " +
		    				" 1 " +
		    			" FROM " +
		    				" files f " +
		    			" WHERE " +
		    				" f.user_id = ? " +
		    				" AND f.cc_id =  ? " +
		    				" AND f.file_status_id != ? ";

		    ps = con.prepareStatement(sql);
		    ps.setLong(1, userId);
			ps.setLong(2, ccId);
			ps.setLong(3, File.STATUS_DONE);

			rs=ps.executeQuery();

			if (rs.next()) {
					return false;

			}
		}

		finally	{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return true;
	}

	/**
	 * Insert Document request
	 * @param con
	 * @param ccId
	 * @throws SQLException
	 */
	public static void insertDocRequest(Connection con, long issueActionId, long ccId, String type) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql =" INSERT INTO " +
								" issue_risk_doc_req(ID, ISSUE_ACTION_ID, DOC_TYPE, CC_ID) " +
							" VALUES " +
								" (seq_issue_risk_doc_req.nextval,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, issueActionId);
				ps.setString(2, type);
				if (ccId != 0){
					ps.setLong(3, ccId);
				} else {
					ps.setString(3, null);
				}
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	}

	/**
	 * Update user document request
	 * @param con
	 * @param ccId
	 * @throws SQLException
	 */
	public static void updateUserDocStatus(Connection con, long userId, long filesRiskStatusId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql =" UPDATE " +
								" users " +
							" SET " +
								" files_risk_status_id = ? " +
							" WHERE " +
								" id = ? ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, filesRiskStatusId);
				ps.setLong(2, userId);
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	}

	/**
	 * Update credit card document request
	 * @param con
	 * @param ccId
	 * @throws SQLException
	 */
	public static void updateCCDocStatus(Connection con, long ccId, long filesRiskStatusId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql =" UPDATE " +
								" credit_cards " +
							" SET " +
								" files_risk_status_id = ? " +
							" WHERE " +
								" id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, filesRiskStatusId);
				ps.setLong(2, ccId);
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	}

	  public static HashMap<Long,FilesRiskStatus> getFilesRiskStatus(Connection con) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  HashMap<Long,FilesRiskStatus> hm=new HashMap<Long,FilesRiskStatus>();

		  try
			{
			    String sql = " SELECT " +
			    				" * " +
			    			 " FROM " +
			    			 	" files_risk_status ";


				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();

				while (rs.next()) {
					FilesRiskStatus vo = new FilesRiskStatus();
					vo.setId(rs.getLong("id"));
					vo.setName(rs.getString("name"));
					vo.setPriority(rs.getInt("priority"));
					vo.setSupportStatus(rs.getInt("support_status")==0?false:true);
					hm.put(rs.getLong("id"),vo);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return hm;
	  }

	public static	ArrayList<RiskUsersFiles>
			getRiskUsersFiles(	Connection con, String skins, String userName, String userId, long classId, String reasonId,
								String statuses)	throws SQLException, CryptoException, InvalidKeyException, IllegalBlockSizeException,
													BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException,
													InvalidAlgorithmParameterException {
		  	PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<RiskUsersFiles> userRiskList = new ArrayList<RiskUsersFiles>();
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

			try {
				String sql =" SELECT " +
								" c.user_id, " +
								" u.user_name, " +
								" frs.name as status, " +
								" u.skin_id, " +
								" c.cc_number as cc_num_id " +
							" FROM " +
								" credit_cards c " +
									" LEFT JOIN users u ON u.id = c.user_id " +
									" LEFT JOIN files_risk_status frs ON frs.id = c. files_risk_status_id " +
							" WHERE " +
								" c.files_risk_status_id is not null" +
								" AND c.files_risk_status_id <> 0 " +
								" AND u.skin_id in (" + skins + ") ";
							if (!statuses.equals("0")){
						sql+=	" AND c.files_risk_status_id in (" +  statuses + ") ";
								}
							if ( classId != ConstantsBase.USER_CLASS_ALL )   {
								if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  		sql +=  " AND (u.class_id = "+ConstantsBase.USER_CLASS_PRIVATE+" "+
				  				  " OR u.class_id = "+ConstantsBase.USER_CLASS_COMPANY+") ";
								} else {
				  		sql +=  " AND u.class_id = " + classId +" ";
								}
							}

							if ( userName!=null && !userName.equals("") ) {
						sql+=	" AND u.user_name like '%" + userName.toUpperCase() + "%'";
							}

							if (!CommonUtil.isParameterEmptyOrNull(userId)) {
						sql +=  " AND u.id = " + userId;
							}


						sql +=	" UNION " +

							" SELECT " +
								" u.id, " +
								" u.user_name, " +
								" frs.name as status, " +
								" u.skin_id, " +
								" null as cc_num_id " +
							" FROM " +
								" users u " +
									" LEFT JOIN files_risk_status frs ON frs.id = u. files_risk_status_id " +
							" WHERE " +
								" u.files_risk_status_id is not null " +
								" AND u.files_risk_status_id <> 0 " +
								" AND u.skin_id in (" + skins + ") ";
							if (!statuses.equals("0")){
						sql+=	" AND u.files_risk_status_id in (" +  statuses + ") ";
							}
							if ( classId != ConstantsBase.USER_CLASS_ALL )   {
								if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
				  		sql +=  " AND (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+" "+
				  				  " OR u.class_id="+ConstantsBase.USER_CLASS_COMPANY+") ";
								} else {
				  		sql +=  " AND u.class_id = " + classId + " ";
								}
							}

							if ( userName!=null && !userName.equals("") ) {
						sql+=	" AND u.user_name like '%" + userName.toUpperCase() + "%'";
						  	}
							if (!CommonUtil.isParameterEmptyOrNull(userId)) {
						sql +=  " AND u.id = " + userId;
							}


			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				RiskUsersFiles vo = new RiskUsersFiles();
				vo.setUserId(rs.getLong("user_id"));
				vo.setUserName(rs.getString("user_name"));
				vo.setSkinId(rs.getInt("skin_id"));
				vo.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
				vo.setFilesRiskStatus(CommonUtil.getMessage(rs.getString("status"), null));
				String cc_num = rs.getString("cc_num_id");
				if (CommonUtil.isParameterEmptyOrNull(cc_num)){
					vo.setType(Constants.FILE_TYPE_ID_COPY);
					vo.setCcNumber(CommonUtil.getMessage(("risk.id.copy"), null));

				} else {
					vo.setType(Constants.FILE_TYPE_CC);
					vo.setCcNumber(AESUtil.decrypt(cc_num));
				}
				userRiskList.add(vo);
				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
				return userRiskList;
		}


	  /**
	   * get issue by issue id
	   * @param con db connection
	   * @param issueId
	   * @return Issue instance
	   * @throws SQLException
	   */
	  public static Issue getIssueById(Connection con,long issueId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  Issue vo = null;

		  try {
			  String sql = " SELECT * " +
		  				   " FROM issues i " +
						   " WHERE id = ? ";


				ps = con.prepareStatement(sql);
				ps.setLong(1, issueId);

				rs = ps.executeQuery();

				if (rs.next()) {
					vo = new Issue();
					vo.setId(rs.getLong("id"));
					vo.setUserId(rs.getLong("user_id"));
					vo.setContactId(rs.getLong("contact_id"));
					vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
					vo.setPopulationEntryId(rs.getLong("population_entry_id"));
					vo.setType(rs.getInt("type"));
					vo.setSubjectId(rs.getString("subject_id"));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }

	  /**
	   * get issue actions that were updated by representatives
	   * @param con
	   * @param startDate
	   * @param endDate
	   * @param offset
	   * @param writerFilter
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<IssueReport> getIssueActionsUpdatedByRepresentatives(Connection con, Date startDate, Date endDate, String offset, String writerFilter) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  IssueReport vo = null;
		  ArrayList<IssueReport> issueReport = new ArrayList<IssueReport>();

		  try
			{
				String sql = " SELECT " +
								" i.id IssueId, " +
								" ia.id IssueActionId, " +
								" ia.action_time DateOfTheCall, " +
								" ic.name Channel, " +
								" w.user_name Rep, " +
								" u.id UserId, " +
								" u.user_name UserName, " +
								" irs.name reachedStatuses, " +
								" isu.name Subjects, " +
								" ist.name issueStats, " +
								" iat.name ReactionActivity, " +
								" ia.comments Comments, " +
								" u.mobile_phone UsersMobilePhone, " +
								" u.land_line_phone UsersLandLinePhone, " +
								" u.email UsersEmail, " +
								" c.id  ContactId, " +
								" c.mobile_phone ContactsMobilePhone, " +
								" c.land_line_phone ContactLandLinePhone, " +
								" c.email ContactEmail " +
							" FROM " +
								" issues i " +
								" LEFT JOIN users u on i.user_id=u.id " +
								" and  u.class_id <> 0 " +
								" left join contacts c on i.contact_id=c.id, " +
								" issue_actions ia, " +
								" writers w, " +
								" issue_action_types iat, " +
								" issue_channels ic, " +
								" issue_statuses ist, " +
								" issue_reached_statuses irs, " +
								" issue_subjects isu " +
							" WHERE " +
								" i.id = ia.issue_id " +
								" AND w.id = ia.writer_id " +
								" AND iat.id = ia.issue_action_type_id " +
								" AND ic.id = iat.channel_id " +
								" AND ist.id = i.status_id " +
								" AND irs.id = iat.reached_status_id " +
								" AND isu.id = i.subject_id " +
								" AND w.dept_id in ( " + Constants.DEPARTMENT_SALES + ", " + Constants.DEPARTMENT_SUPPORT + ")";
							if (!writerFilter.equals(ConstantsBase.ALL_REP)){
								sql += " AND w.id in (" + writerFilter + ") ";
							}
							sql += "AND i.time_created  between ? and ? ";							
			  
			  ps = con.prepareStatement(sql);
			  ps.setTimestamp(1, CommonUtil.convertToTimeStamp(startDate));
			  ps.setTimestamp(2 , CommonUtil.convertToTimeStamp(endDate));
			  rs=ps.executeQuery();
			  while (rs.next()) {
					vo = new IssueReport();
					vo.setIssueId(rs.getLong("IssueId"));
					vo.setActionId(rs.getLong("IssueActionId"));
					vo.setActionTime(rs.getDate("DateOfTheCall"));
					// set Issue Channel's name
					IssueChannel ic=new IssueChannel();
					ic.setName(rs.getString("Channel"));
					vo.setChannel(ic);
					vo.setWriterName(rs.getString("Rep"));
					vo.setUserId(rs.getLong("UserId"));
					vo.setUserName(rs.getString("UserName"));
					// set Issue Reached Status's name
					IssueReachedStatus irs = new IssueReachedStatus();
					irs.setName(rs.getString("reachedStatuses"));
					vo.setReachedStatus(irs);
					vo.setIssueSubject(rs.getString("Subjects"));
					vo.setIssueStats(rs.getString("issueStats"));
					IssueReaction ir = new IssueReaction();
					ir.setName(rs.getString("ReactionActivity"));
					vo.setReaction(ir);
					vo.setComments(rs.getString("Comments"));
					vo.setUsersMobilePhone(rs.getString("UsersMobilePhone"));
					vo.setUsersLandLinePhone(rs.getString("UsersLandLinePhone"));
					vo.setUsersEmail(rs.getString("UsersEmail"));
					vo.setContactId(rs.getLong("ContactId"));
					vo.setContactsMobilePhone(rs.getString("ContactsMobilePhone"));
					vo.setContactLandLinePhone(rs.getString("ContactLandLinePhone"));
					vo.setContactEmail(rs.getString("ContactEmail"));
					issueReport.add(vo);
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return issueReport;
	  }
	    
	/**
	 * Get issue component as Hashmap
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, IssueComponent> getIssueComponent(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedHashMap<Long, IssueComponent> hm = new LinkedHashMap<Long, IssueComponent>();
		try {
			String sql =" SELECT " + 
						"	* " + 
						" FROM " + 
						"	issue_component ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				IssueComponent vo = new IssueComponent();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				hm.put(rs.getLong("id"), vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;		
	}

}