package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.PendingDocument;
import il.co.etrader.backend.util.Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.DAOBase;

public class PendingDocumentsDAO  extends DAOBase {
	
	public static ArrayList<PendingDocument> getPendingDocumentsNotRegulated(Connection con, PendingDocumentsDAOFilter params) throws SQLException {		
		
		ArrayList<PendingDocument> list = new ArrayList<PendingDocument>();
		Statement ps = null;
		ResultSet rs = null;

		try	{
			String sql = buildPendingDocsSql( false, params);
		    
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				PendingDocument vo = getPendingDocumentVO(rs, false, false);
				list.add(vo);
			}

		} finally {
					closeStatement(ps);
					closeResultSet(rs);
		}
		return list;
	}
	
	public static int getPendingDocumentsNotRegulatedCount(Connection con, PendingDocumentsDAOFilter p) throws SQLException {
		
		int count = 0 ;
		Statement ps = null;
		ResultSet rs = null;

		try	{
			p.setFirstRow(-1);
			p.setRowsPerPage(-1);
			p.setOrderBy(0);
			
			String sql = buildPendingDocsSql( true, p);
		    
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery(sql);

			if (rs.next()) {
				return rs.getInt("c");
			}

		} finally {
					closeStatement(ps);
					closeResultSet(rs);
		}
		return count;
	}
		
	public static ArrayList<PendingDocument> getPendingDocumentsRegulated(Connection con, PendingDocumentsDAOFilter p) throws SQLException {
		
		ArrayList<PendingDocument> list = new ArrayList<PendingDocument>();
		Statement ps = null;
		ResultSet rs = null;

		try	{
			String sql = buildPendingDocsSql(false, p);
		 	ps = con.prepareStatement(sql);
			rs = ps.executeQuery(sql);

			while (rs.next()) {
				PendingDocument vo = getPendingDocumentVO(rs, true, p.isSupportScreen());
				list.add(vo);
			}

		} finally {
					closeStatement(ps);
					closeResultSet(rs);
		}
		return list;
	}

	public static int getPendingDocumentsRegulatedCount(Connection con, PendingDocumentsDAOFilter p) throws SQLException {
		
		int count = 0;
		Statement ps = null;
		ResultSet rs = null;

		try	{
			p.setFirstRow(-1);
			p.setRowsPerPage(-1);
			p.setOrderBy(0);
			
		    String sql = buildPendingDocsSql(true, p);
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery(sql);

			if (rs.next()) {
				return rs.getInt("c");
			}

		} finally {
					closeStatement(ps);
					closeResultSet(rs);
		}
		return count;
	}
	
	private static String buildPendingDocsSql(boolean count, PendingDocumentsDAOFilter p) {
		String sql = "" ;
		boolean paging = p.getFirstRow() > -1 && p.getRowsPerPage() > -1;		
		if(paging) {
			sql += "SELECT * FROM ( select a.*, ROWNUM rnum FROM  ( ";
		}
		sql += 
			"SELECT ";
			if(count){
				sql += " count(support_rejected) as c ";
			} else {
				sql += " * ";
			}
			sql +="FROM " + 
						" ( " +
							" SELECT d.issue_id, " +
								" d.user_id u_id, " +
								" u.user_name u_name, " +
								" u.mobile_phone m_phone, " +
								" u.land_line_phone l_phone, " +
								" u.is_active is_active, " +
								" s.name skin_name, " +
								" u.time_last_login last_login, " +
								" d.reject_control control_rejected, " +
								" d.reject_support support_rejected, " +
								" d.pending_docs," +
								" d.pending_docs_control, " +
								" d.last_action_time_by_writer, " +
								" uad.sum_deposits," +
								" d.last_upload_file_time, " +
								" d.uploaded_docs, " +
								" d.total_files_count " +
							" FROM " +
	 							" users_pending_docs d, " +
								" users u, " +
								" users_regulation ur, " +
								" skins s, " +
								" users_active_data uad," +
								" writers_skin ws " +
							" WHERE " +
								" d.user_id = u.id " +
								" and d.user_id = ur.user_id " +
								" and d.user_id = uad.user_id " +
								" and u.skin_id = s.id " +
								" and ws.skin_id = s.id" +
								" and ws.writer_id = " + Utils.getWriter().getWriter().getId() +
								" and (NVL(ur.score_group,0) != 4 OR ( NVL(ur.score_group,0) = 4 AND NVL(ur.is_knowledge_question,0) = 1)) " + 
								" and ur.suspended_reason_id <> " + UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT;
								//Screens Logic (Before/After Regulation; Support/Control Screen; Support/Control Reject)
								if(!p.isRegulatedUsers()){
									sql += " and d.is_main_regulation_issue = 1 ";
									if(p.isSupportScreen()){
										//Support Screen
										sql += " and ur.approved_regulation_step < " + UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL;
									} else {
										//Else Control Screen
										sql += " and ur.approved_regulation_step = " + UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL;
									}
								} else{
									//After Regulation step = 7
									sql += " and d.is_main_regulation_issue = 0 " +
										   " and ur.approved_regulation_step = " + UserRegulationBase.REGULATION_CONTROL_APPROVED_USER +
										   " and pending_docs > 0 "; 
								}
								
								if(p.supportRejected){
									sql += " and d.reject_support > 0";
								}
								
								if(p.controlRejected){
									sql += " and d.reject_control > 0";
								}
					       
								
								//FILTERS
																
								if(p.getUserName() != null && p.getUserName().trim().length() > 0){
									sql += " AND u.user_name like \'%"+p.getUserName().toUpperCase()+"%\'";
								}
								if(p.getUserId() != null && p.getUserId() > 0) {
									sql += " AND u.id = " + p.getUserId();
								}														
						 		if(p.getIsUserActive() > -1){
						 			sql += " AND u.is_active = " + p.getIsUserActive();
						 		}
						 		
						 		if(p.getIsUserBlocked() == 1){
						 			sql += " and ur.suspended_reason_id = " + UserRegulationBase.SUSPENDED_DUE_DOCUMENTS;
						 		}
						 		
						 		if(p.getIsUserBlocked() == 0){
						 			sql += " and ur.suspended_reason_id <> " + UserRegulationBase.SUSPENDED_DUE_DOCUMENTS;
						 		}
						 		
						 		if (!p.getTotalDepositAmountFormated().equals("")) {
						 			sql += " and uad.sum_deposits " + p.getTotalDepositAmountFormated();
						 		}
								
								//  last login - 3 month and less
								if ( p.lastLogin == 1){
									sql += " AND sysdate - u.time_last_login < 90 ";
								}
								//  last login - 3-6 month
								if ( p.lastLogin == 2){
									sql += " AND sysdate - u.time_last_login >= 90 AND sysdate - u.time_last_login < 180 ";
								}
								//  last login 6 month and more
								if ( p.lastLogin == 3){
									sql += " AND sysdate - u.time_last_login >= 180";
								}
								
								if (p.getSkinId() != 0) {
								    sql += " AND u.skin_id = " + p.getSkinId();	
								}
								
								if (p.getUploadedDocs() == 1) {
								    sql += " AND d.uploaded_docs = d.total_files_count ";	
								}
								
								if (p.getUploadedDocs() == 2) {
								    sql += " AND d.uploaded_docs < d.total_files_count AND d.uploaded_docs > 0 ";	
								}
								
								if (p.getUploadedDocs() == 3) {
								    sql += " AND d.uploaded_docs = 0 ";	
								}
								
						 		if(p.isExcludeTesters()){
						 			sql += " AND u.class_id <> 0 ";
						 		}						 		
						 		
						 		sql += " ORDER BY uad.sum_deposits DESC, d.issue_id ) ";

				if(paging) {
					sql +=" ) a where rownum <= " + (p.getFirstRow()+p.getRowsPerPage()) + " ) where rnum  > " + p.getFirstRow();
				}					
		return sql;
	}

	private static PendingDocument getPendingDocumentVO(ResultSet rs, boolean regulated, boolean supportScreen) throws SQLException {
		  PendingDocument vo = new PendingDocument();
		  vo.setIssueId(rs.getLong("issue_id"));
		  vo.setUserId(rs.getLong("u_id"));
		  vo.setUserName(rs.getString("u_name"));
		  vo.setMobilePhone(rs.getString("m_phone"));
		  vo.setLandLinePhone(rs.getString("l_phone"));
		  vo.setSkinName(rs.getString("skin_name"));
		  vo.setPendingDocs(rs.getInt("pending_docs"));
		  vo.setPendingDocsControl(rs.getInt("pending_docs_control"));
		  vo.setIssueLastUpdate(convertToDate(rs.getTimestamp("last_action_time_by_writer")));
		  vo.setTotalDepositAmount(rs.getLong("sum_deposits"));
		  vo.setLastUploadFileTime(convertToDate(rs.getTimestamp("last_upload_file_time")));
		  if(vo.getIssueLastUpdate() != null && vo.getLastUploadFileTime() != null && vo.getIssueLastUpdate().before(vo.getLastUploadFileTime())){
			  vo.setHotUser(true);
		  }
		  return vo;
	  }
	
	public static long getPendingDocsNum(Connection con, long userId) throws SQLException {
		long res = 0;		  
		ResultSet rs = null;
		PreparedStatement ps = null;
		  
	  try {
	
		  String sql = "select pending_docs " +
		  		      " from users_pending_docs  " +
		  		      " where user_id = ?" +
		  		      " and is_main_regulation_issue = 1";
		  
		  ps = con.prepareStatement(sql);
		  ps.setLong(1, userId);
	
		  rs = ps.executeQuery();
	
		  if (rs.next()) {
			  res = rs.getLong("pending_docs");
		  }
	  } finally {
		  closeResultSet(rs);
		  closeStatement(ps);
	  }
		  return res;
	}
}
