package il.co.etrader.backend.dao_managers;

public class PendingDocumentsDAOFilter {
	Long userId; 
	String userName; 
	int interested; // All, yes, no 
	int daysConditionQualified; // < 30 days, 30-60 days, > 60 days
	int allDocsUploaded; 
	int uploadedDocs; // number of uploaded docs 0-6
	int reached;
	int orderBy; 
	boolean supportRejected; 
	boolean controlRejected; 
	int firstRow; 
	int rowsPerPage;
	boolean supportScreen;
	int skinId; 
	char equality;
	int lastLogin;// < 3 months, 3-6 months, > 6 months
	boolean excludeTesters;
	int isUserActive;
	boolean regulatedUsers;
	int isUserBlocked;
	String totalDepositAmount;
	
	
	public PendingDocumentsDAOFilter(Long userId, String userName,
			int interested, int reached,
			int allDocsUploaded, int uploadedDocs, int orderBy,
			boolean supportRejected, boolean controlRejected, int firstRow,
			int rowsPerPage, boolean supportScreen, int daysConditionQualified,char equality, 
			int skinId, int lastLogin, boolean excludeTesters, int isUserActive, boolean regulatedUsers, int isUserBlocked, String totalDepositAmount) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.interested = interested;
		this.allDocsUploaded = allDocsUploaded;
		this.uploadedDocs = uploadedDocs;
		this.reached = reached;
		this.orderBy = orderBy;
		this.supportRejected = supportRejected;
		this.controlRejected = controlRejected;
		this.firstRow = firstRow;
		this.rowsPerPage = rowsPerPage;
		this.supportScreen = supportScreen;
		this.daysConditionQualified = daysConditionQualified;
		this.skinId = skinId;
		this.equality = equality;
		this.lastLogin = lastLogin;
		this.excludeTesters = excludeTesters;
		this.isUserActive = isUserActive;
		this.regulatedUsers = regulatedUsers;
		this.isUserBlocked = isUserBlocked;
		this.setTotalDepositAmount(totalDepositAmount);
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getInterested() {
		return interested;
	}
	public void setInterested(int interested) {
		this.interested = interested;
	}
	public int getAllDocsUploaded() {
		return allDocsUploaded;
	}
	public void setAllDocsUploaded(int allDocsUploaded) {
		this.allDocsUploaded = allDocsUploaded;
	}
	public int getUploadedDocs() {
		return uploadedDocs;
	}
	public void setUploadedDocs(int uploadedDocs) {
		this.uploadedDocs = uploadedDocs;
	}
	public int getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}
	public boolean isSupportRejected() {
		return supportRejected;
	}
	public void setSupportRejected(boolean supportRejected) {
		this.supportRejected = supportRejected;
	}
	public boolean isControlRejected() {
		return controlRejected;
	}
	public void setControlRejected(boolean controlRejected) {
		this.controlRejected = controlRejected;
	}
	public int getFirstRow() {
		return firstRow;
	}
	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}
	public int getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public boolean isSupportScreen() {
		return supportScreen;
	}
	public void setSupportScreen(boolean supportScreen) {
		this.supportScreen = supportScreen;
	}
	public int getReached() {
		return reached;
	}
	public void setReached(int reached) {
		this.reached = reached;
	}
	public int getDaysConditionQualified() {
		return daysConditionQualified;
	}
	public void setDaysConditionQualified(int daysConditionQualified) {
		this.daysConditionQualified = daysConditionQualified;
	}
	public int getSkinId() {
		return skinId;
	}
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	public char getEquality() {
		return equality;
	}
	public void setEquality(char equality) {
		this.equality = equality;
	}
	public int getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(int lastLogin) {
		this.lastLogin = lastLogin;
	}
	public boolean isExcludeTesters() {
		return excludeTesters;
	}
	public void setExcludeTesters(boolean excludeTesters) {
		this.excludeTesters = excludeTesters;
	}
	public int getIsUserActive() {
		return isUserActive;
	}
	public void setIsUserActive(int isUserActive) {
		this.isUserActive = isUserActive;
	}
	public boolean isRegulatedUsers() {
		return regulatedUsers;
	}
	public void setRegulatedUsers(boolean regulatedUsers) {
		this.regulatedUsers = regulatedUsers;
	}
	public int getIsUserBlocked() {
		return isUserBlocked;
	}
	public void setIsUserBlocked(int isUserBlocked) {
		this.isUserBlocked = isUserBlocked;
	}
	public String getTotalDepositAmount() {
		return totalDepositAmount;
	}
	public void setTotalDepositAmount(String totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}
	
	public String getTotalDepositAmountFormated() {
		String formatedDepositAmount = "";
		if (totalDepositAmount != null && !totalDepositAmount.equals("")) {
			totalDepositAmount = totalDepositAmount.replaceAll("\\s", "");// remove all white spaces and special tabs
			if(totalDepositAmount.matches("[0-9<>=]+")){// only numbers and special characters "<", ">", "="
				if (totalDepositAmount.startsWith("<") || totalDepositAmount.startsWith(">") || totalDepositAmount.startsWith("=")) {
					String prefix = totalDepositAmount.substring(0, 1);
					long totalDepositAmountNumber = Long.valueOf(totalDepositAmount.substring(1)) * 100;
					formatedDepositAmount = prefix + totalDepositAmountNumber;
				}
			}
		} else {
			return formatedDepositAmount;
		}
		return formatedDepositAmount;
	}
	
	
}
