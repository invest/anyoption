package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationEntryReport;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.SalesEntriesForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.WritersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.PopulationEntriesDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.context.FacesContext;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.managers.PopulationUsersAssignTypesManagerBase;
import com.anyoption.common.util.DbmsOutput;

import com.anyoption.common.daos.PopulationsDAOBase;

public class PopulationEntriesDAO extends PopulationEntriesDAOBase {

	private static final Logger logger = Logger.getLogger(PopulationEntriesDAO.class);

	/**
	 * get population Entry object
	 *
	 * @param rs
	 *            ResultSet instance
	 * @return
	 * @throws SQLException
	 */
	private static PopulationEntry getEntryVO(ResultSet rs) throws SQLException {
		PopulationEntry vo = new PopulationEntry();

		PopulationsDAOBase.getEntryBaseVO(rs, vo);

		vo.setCurrPopulationName(rs.getString("population_name"));
		vo.setCountryId(rs.getLong("country_id"));
		vo.setLanguageId(rs.getLong("language_id"));
		vo.setCountryOffset(rs.getString("country_offset"));

		if (vo.getUserId() != 0){
			vo.setPopulationUserName(rs.getString("name_user"));
		} else{
			vo.setPopulationUserName(rs.getString("name_contact"));
		}

		return vo;
	}



	/**
	 * get all the population entries from DB
	 * @param contactId TODO
	 * @return population entries list
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getPopulationEntriesByEntryType(Connection con,
			long skinId, long assignedWriterId, int entryType, Date fromDate,
			Date toDate, Date fromLogin, Date toLogin, int actionTypeId,  boolean isAssigned, long userId,
			String userName, long contactId, String entryTypesList, String priorityId, WriterWrapper wr, long lastLoginInXMinutes, long campaignId,
			String timeZone, long userStatusId, String countriesGroup, String populationTypes, boolean balanceBelowMinInvestAmount,
			boolean madeDepositButdidntMakeInv24HAfter, int colorType, long salesType, long userRankId, long retentionTeamId, String affiliateKey) throws SQLException {

		ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, Writer> writersMap = wr.getAllWritersMap();
		String cbLastCallTimeColumn = "";
	    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String writerSkins = wr.getSkinsToString();
		int index = 1;
		String lastConnection = " last_connection_action_id ";

		try {

			 if(ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryType){
				 cbLastCallTimeColumn += " ,cb_last_calls.cb_last_call_time, tab.time_c ";
				 lastConnection = " last_call_action_id ";
			 }

			String sql = " select " +
							" ent.* " +
							cbLastCallTimeColumn +
						 " from (select * " +
								 " from " +
								   " (SELECT " +
								        " entries_table.*, rownum as row_num " +
								    " FROM " +
								        " (SELECT " +
								        	  " pu.id population_users_id, " +
								              " pu.lock_history_id, " +
									          " pu.curr_population_entry_id, " +
								        	  " pu.curr_assigned_writer_id, " +
									          " CASE WHEN pu.user_id is null THEN c.user_id ELSE pu.user_id END as user_id, " +
									          " (u.first_name || ' ' || u.last_name) name_user, " +
									          " u.TIME_SC_TURNOVER, " +
											  " u.TIME_SC_HOUSE_RESULT, " +
									          " c.name name_contact, " +
									          " c.dynamic_parameter as contact_dynamic_param, " +
									          " u.dynamic_param as user_dynamic_param, " +
									          " u.currency_id as user_currency, " +
									          " u.balance as user_balance, " +
									          " pu.contact_id, " +
									          " pu.entry_type_id, " +
									          " pu.entry_type_action_id, " +
									          " pu.delay_id, " +
									          " pu.time_control, " +
									          " pe.qualification_time, " +
									          " pe.group_id, " +
									          " pe.population_id, " +
									          " pe.is_displayed, " +
									          " p.name population_name, " +
									          " p.population_type_id, " +
									          " cn.id country_id , " +
									          " cn.gmt_offset country_offset, " +
									          " ia.action_time entry_type_action_time, " +
									          " ia.callback_time, " +
									          " ia.callback_time_offset, " +
									          " iat.name action_type_name_key, " +
									          " ia.issue_id entry_issue_id, " +
									          " ia.comments, " +
									          " ia.writer_id old_pe_writer_id, " +
									          " u.language_id, " +
									          " CASE WHEN pu.user_id is null THEN c.skin_id ELSE u.skin_id END as skin_id, " +
									          " i_old_pop.population_entry_id AS old_pop_ent_id, " +
									          " p_old_pop.name p_old_name, " +
								              " p_old_pop.population_type_id pe_old_population_type_id, " +
								              " pe_old_pop.id pe_old_id, " +
								              " u.time_last_login as time_last_login, " +
								              " u.dep_no_inv_24h, " +
								              " us.status_name as user_status_name, " +
								              " ur.rank_name as user_rank_name, " +
								              " us.id as user_status_id, " +
								              " ur.id as user_rank_id, " +
//								              " total_inv.totalamount as total_turnover," +
								              " pt.type_id population_sales_type_id, " +
								              " ia_last_connection.action_time as last_connection_time, " +
								              " u.class_id, " +
										      " u.user_name, " +
										      " (SELECT usr.land_line_phone FROM users usr WHERE usr.id = NVL(pu.user_id, c.user_id)) AS user_land_line_phone, "+
						                      " (SELECT usr.mobile_phone FROM users usr WHERE usr.id = NVL(pu.user_id, c.user_id)) AS user_mobile_phone, "+
						                      " (SELECT usr.country_id FROM users usr, countries cc WHERE usr.country_id = cc.id and usr.id= NVL(pu.user_id, c.user_id)) AS user_country_id, "+
						                      " (SELECT cc.gmt_offset FROM users usr, countries cc WHERE usr.country_id = cc.id and usr.id= NVL(pu.user_id, c.user_id)) AS user_country_offset, "+
						                      " c.mobile_phone as contact_mobile_phone, " +
						                      " c.land_line_phone as contact_land_line_phone, " +
						                      " c.phone as contact_phone, " +
						                      " c.type as contact_type " +
								         " FROM " +
								             " population_users pu " +
								             	 " LEFT JOIN contacts c on pu.contact_id = c.id " +
						                         " LEFT JOIN users u on pu.user_id = u.id " +
						                         " LEFT JOIN users_active_data uad on u.id = uad.user_id " +
						                         " LEFT JOIN marketing_combinations mco on (u.combination_id = mco.id OR (u.combination_id is null AND c.combination_id = mco.id)) " +
						                         " LEFT JOIN marketing_campaigns mca on mco.campaign_id = mca.id " +
						                         " LEFT JOIN countries cn on c.country_id = cn.id " +
						                         " LEFT JOIN issue_actions ia on pu.entry_type_action_id = ia.id " +
						                         	" LEFT JOIN issue_action_types iat on iat.id = ia.issue_action_type_id " +
						                         	" LEFT JOIN issues i_old_pop on ia.issue_id = i_old_pop.id " +
						                             	" LEFT JOIN population_entries pe_old_pop on i_old_pop.population_entry_id = pe_old_pop.id " +
						    				 				" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
						    				 				" LEFT JOIN issues i_last_connection on pu.curr_population_entry_id = i_last_connection.population_entry_id " +
						    				 				" LEFT JOIN issue_actions ia_last_connection on i_last_connection." + lastConnection + " = ia_last_connection.id " +
						    				 				" LEFT JOIN issue_action_types iat_last_connection on iat_last_connection.id = ia_last_connection.issue_action_type_id " +
						                         " LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
						                         	" LEFT JOIN populations p on pe.population_id = p.id " +
						                         		" LEFT JOIN population_types pt on p.population_type_id = pt.id " +
						                         		" LEFT JOIN users_status us on us.id = u.status_id " +
						                         		" LEFT JOIN users_rank ur on ur.id = u.rank_id " ;
//						       	         	  		    " LEFT JOIN (SELECT i.user_id user_id, " +
//						    	         	  		    "			  SUM(amount) totalamount " +
//						    	         	  		    "			  FROM investments i " +
//						    	         	  		    "			  WHERE   is_canceled=0 " +
//						    	         	  		    "			  group by i.user_id) total_inv on total_inv.user_id = u.id ";
							      sql += " WHERE " +
							 					" ((u.skin_id is not null and u.skin_id in (" + writerSkins + ")) " +
							 					" OR " +
							 					" (c.skin_id is not null and c.skin_id in (" + writerSkins + "))) " +
							 					" AND (u.class_id != 0 OR u.class_id IS NULL) ";

								    if (entryType != 0){
										sql += " AND pu.entry_type_id  = " + entryType + " ";
										if (entryType == ConstantsBase.POP_ENTRY_TYPE_GENERAL){
											sql += " AND pu.curr_population_entry_id IS NOT NULL " +
												   " AND pe.is_displayed = 1 ";
										}
									} else {
										sql += " AND pu.entry_type_id != 5 ";
									}


							 	    if (isAssigned){
							 			if (assignedWriterId != 0){
							 				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
							 			} else {
							 				sql += " AND pu.curr_assigned_writer_id is not null ";
							 			}
							 		} else {  // ignore when it's decline after approved page
							 			if (assignedWriterId != 0) {
							 				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
							 			} else {
							 				sql += " AND pu.curr_assigned_writer_id is null ";
							 			}
							 		}

							 	    if (salesType != 0) {
							 	    	if (salesType == PopulationsManagerBase.SALES_TYPE_RETENTION) {
							 	    		sql += " AND pt.type_id in (3," + salesType + ")";
							 	    	} else {
							 	    		sql += " AND pt.type_id = " + salesType;
							 	    	}
							 	    }

							 		if (skinId != 0){
							 			sql += " AND ((u.skin_id is not null and u.skin_id in (" + skinId + ")) " +
							 						" OR" +
							 						" (c.skin_id is not null and c.skin_id in (" + skinId + "))) ";
							 		}

							 		if (null != entryTypesList) {
							 			sql += " AND pu.entry_type_id in (" + entryTypesList + ") ";
							 		}

							 		if (actionTypeId != 0){
							 			sql += " AND ia.issue_action_type_id = " + actionTypeId + " ";
							 		}

							 		if (userId != 0){
							 			sql += " AND u.id = " + userId + " ";
							 		}

							 		if (userName != null && !userName.equals("")){
							 			sql += " AND u.user_name = '" + userName.toUpperCase() + "' ";
							 		}

							 		if (contactId != 0){
							 			sql += " AND c.id = " + contactId + " ";
							 		}

							 		if (!CommonUtil.isParameterEmptyOrNull(affiliateKey)) {
							 			sql += " AND ((u.affiliate_key is not null and u.affiliate_key like '%" + affiliateKey + "%') " +
						 						" OR" +
						 						" (c.affiliate_key is not null and c.affiliate_key like '%" + affiliateKey + "%')) ";
							 		}

							 	    if (null != fromDate && null != toDate) {
							             if (entryType == ConstantsBase.POP_ENTRY_TYPE_TRACKING && lastLoginInXMinutes != PopulationsManagerBase.POP_ENT_ONLINE) {
							                 sql += "AND ia.action_time between to_date('"+ fmt.format(fromDate) + "','dd/MM/yyyy') " +
							                                          " and to_date('"+ fmt.format(toDate) + "','dd/MM/yyyy') +1 ";
							             } else if (entryType == ConstantsBase.POP_ENTRY_TYPE_CALLBACK) {
							                 sql += "AND ia.callback_time between to_date('"+ fmt.format(fromDate) + "','dd/MM/yyyy') " +
							                 								" and to_date('"+ fmt.format(toDate) + "','dd/MM/yyyy') +1 ";
							             } else {
							                 sql += "AND (pe.qualification_time IS NULL " +
							                 			" OR " +
							                 		   " pe.qualification_time between to_date('"+ fmt.format(fromDate) + "','dd/MM/yyyy') " +
							     											     " and to_date('"+ fmt.format(toDate) + "','dd/MM/yyyy') +1) ";
							             }
							 	    }

							 	    if (!CommonUtil.isParameterEmptyOrNull(priorityId)) {
							 	    	sql += "AND i_old_pop.priority_id = '" + priorityId + "' " ;
							 	    }

								    if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
								    	sql += " AND u.time_last_login >= ? " ;
								    }
								    
								    if(fromLogin != null ){
							    		sql += " AND u.time_last_login >= ? " ;
							    	}
							    	if (toLogin != null ){
							    		sql += " AND u.time_last_login <= ? ";
							    	}

									if (campaignId != 0){
										sql += " AND mco.campaign_id = " + campaignId + " ";
									}

									if (!CommonUtil.isParameterEmptyOrNull(timeZone) && !timeZone.equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)) {

										sql += " AND cn.gmt_offset in (" + timeZone + ") ";
									}

									if (userStatusId != 0) {
										sql += " AND u.status_id = " + userStatusId + " ";
									}

									if (userRankId != 0) {
										sql += " AND u.rank_id = " + userRankId + " ";
									}

								    if (!countriesGroup.equals("0")){
								    	sql += " AND exists (SELECT " +
								    							"1 " +
								    						"FROM " +
								    							"countries_group cg " +
								    						"WHERE " +
								    							"cg.country_id = cn.id " +
								    							"AND cg.countries_group_type_id  = " + countriesGroup + " ) ";
								    }

									if (!CommonUtil.isParameterEmptyOrNull(populationTypes)){
										sql += " AND p.population_type_id in (" + populationTypes + ") ";
									}

								    if (balanceBelowMinInvestAmount) {
								    	sql += " AND u.balance < ( SELECT " +
								    			"						ilgc.value " +
							                    "				   FROM " +
							                    "						investment_limit_group_curr ilgc " +
							                    "				   WHERE " +
							                    "						u.currency_id = ilgc.currency_id AND " +
							                    "						ilgc.investment_limit_group_id = " + Constants.INVESTMENT_LIMITS_GROUP_ID + " )";
								    }

								    if (madeDepositButdidntMakeInv24HAfter) {
								    	sql += " u.dep_no_inv_24h = 1 ";
								    }

									switch (colorType){
									case ConstantsBase.COLOR_BLACK_ID: //  Users who entered to the list within the last 1 hour
										sql += " AND pe.qualification_time >= sysdate - 1/24 " +
											   " AND NVL(i_old_pop.reached_calls_num,0) = 0 ";
										break;
									case ConstantsBase.COLOR_BLUE_ID: // Users who entered to the list more than last hour ago
										sql += " AND pe.qualification_time < sysdate - 1/24 " +
										   	   " AND NVL(i_old_pop.reached_calls_num,0) = 0 ";
										break;
									case ConstantsBase.COLOR_GREEN_ID: // Green – Users who were called and reached
										sql += " AND NVL(i_old_pop.reached_calls_num,0) > 0 ";
										break;
								}
									
									
								    if (retentionTeamId != ConstantsBase.ALL_FILTER_ID) {
								    	String retentionTeamsCondition = " > ";
								    	if (retentionTeamId == WritersManagerBase.RETENTION_TEAMS_UPGRADE) {
								    		retentionTeamsCondition = " = ";
								    	}
								    	sql += " AND uad.deposits_count " 
								    				+ retentionTeamsCondition + 
													" (SELECT " +
														" enum.value " +
													" FROM " +
														" enumerators enum " +
													" WHERE " +
														" enum.enumerator LIKE '" + ConstantsBase.ENUM_COUNT_DEPOSIT_ENUMERATOR + "' " +
														" AND enum.code LIKE '" + ConstantsBase.ENUM_COUNT_DEPOSIT_CODE + "')";
								    }

							      sql += " ORDER BY pe.qualification_time desc NULLS LAST ) entries_table) " +

							      " ) ent ";

			 if(ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryType){
				    sql += 	" LEFT JOIN (SELECT " +
                            "                  t.user_id , " +
                            "                  max(t.time_created) as time_c " +
                            "            FROM " +
                            "                  transactions t, " +
                            "                  transaction_types tt " +
                            "            WHERE " +
                            "                  t.type_id = tt.id AND " +
                            "                  tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT +
                            "            GROUP BY " +
                            "                  t.user_id) tab on tab.user_id = ent.user_id ";
					sql += 		" LEFT JOIN (select " +
									            " pu2.id pop_user_id, " +
									            " max(ia2.action_time) cb_last_call_time " +
									         " from  " +
									         	" population_users pu2, " +
									            " population_entries pe2, " +
									            " issues i2, " +
									         	" issue_actions ia2, " +
									         	" issue_actions cb_call" +
									         " where  " +
									            " pu2.entry_type_id = " + ConstantsBase.POP_ENTRY_TYPE_CALLBACK + " " +
									            " and cb_call.id = pu2.entry_type_action_id " +
									            " and pu2.id = pe2.population_users_id " +
									            " and pe2.id = i2.population_entry_id " +
									            " and i2.id = ia2.issue_id " +
									            " and ia2.action_time > cb_call.action_time " +
									         "  group by pu2.id) cb_last_calls on ent.population_users_id = cb_last_calls.pop_user_id " +
						 " order by ent.qualification_time desc NULLS LAST ";

				}


 			ps = con.prepareStatement(sql);

			Calendar cal = null;
			cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, -(int)lastLoginInXMinutes);
			Date timeForHighlight = cal.getTime();

			if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
				ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(timeForHighlight));
			}
			
			if (fromLogin != null) {
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(fromLogin));
			}
			
			if (toLogin != null) {
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(toLogin));
			}

			rs = ps.executeQuery();
			HashMap<Long, Long> investmentLimitsGroupAmountPerCurrency = ApplicationData.getInvLimitisGroupsMap().get(Constants.INVESTMENT_LIMITS_GROUP_ID).getAmountPerCurrency();
			// Time for highlight
			cal = Calendar.getInstance();
			int lastLoginLevelOne = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_ONE_CODE));
			cal.add(Calendar.MINUTE, -lastLoginLevelOne);
			Date timeForHighlightLevelOne = cal.getTime();
			cal = Calendar.getInstance();
			int lastLoginLevelTwo = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_TWO_CODE));
			cal.add(Calendar.MINUTE, -lastLoginLevelTwo);
			Date timeForHighlightLevelTwo = cal.getTime();
			while (rs.next()) {
				PopulationEntry vo = getEntryVO(rs);
				SpecialCare specialCare = new SpecialCare();
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                vo.setSpecialCare(specialCare);
				//vo.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
				vo.setCallBackTime(convertToDate(rs.getTimestamp("callback_time")));
				vo.setCallBackTimeOffset(rs.getString("callback_time_offset"));
				vo.setActionTypeName(rs.getString("action_type_name_key"));
				vo.setComments(rs.getString("comments"));
				vo.setEntryTypeActionTime(convertToDate(rs.getTimestamp("entry_type_action_time")));
				vo.setOldPopulationName(rs.getString("p_old_name"));
				vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				if (rs.getInt("user_id") != 0) {
                	vo.setDynamicParam(rs.getString("user_dynamic_param"));
                } else {
                	vo.setDynamicParam(rs.getString("contact_dynamic_param"));
                }
				long oldPeWriterId = rs.getLong("old_pe_writer_id");
				vo.setOldPeWriterId(oldPeWriterId);

				if (oldPeWriterId > 0){
					vo.setOldPeWriter((writersMap.get(oldPeWriterId).getUserName()));
				}

				vo.setLastCallTime(convertToDate(rs.getTimestamp("last_connection_time")));

				if(ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryType){
					vo.setCbLastCallTime(convertToDate(rs.getTimestamp("cb_last_call_time")));
					vo.setLastDepositTime(rs.getDate("time_c"));
				}
                Date timeLastLogin = convertToDate(rs.getTimestamp("time_last_login"));
                vo.setTimeLastLogin(timeLastLogin);
                vo.setLastLoginInXMinutes(ConstantsBase.ALL_FILTER_ID);
                if (timeLastLogin != null) {
                	vo.setTimeLastLogin(timeLastLogin);
                    if (timeLastLogin.after(timeForHighlightLevelOne)) {
                    	vo.setLastLoginInXMinutes(lastLoginLevelOne);
                    } else if (timeLastLogin.after(timeForHighlightLevelTwo)) {
                    	vo.setLastLoginInXMinutes(lastLoginLevelTwo);
                    }
                }
                //vo.setTotalTurnover(rs.getLong("total_turnover"));
                vo.setTotalTurnoverTxt(CommonUtil.displayAmount(vo.getTotalTurnover(), rs.getLong("user_currency")));
                if (rs.getLong("dep_no_inv_24h") == 1) {
                	vo.setMadeDepositButdidntMakeInv24HAfter(true);
                } else {
                	vo.setMadeDepositButdidntMakeInv24HAfter(false);
                }

                vo.setUserStatusId(rs.getLong("user_status_id"));
                vo.setUserRankId(rs.getLong("user_rank_id"));
                vo.setUserStatusTxt(rs.getString("user_status_name"));
                vo.setUserRankTxt(rs.getString("user_rank_name"));
                vo.setPopulationSalesTypeId(rs.getLong("population_sales_type_id"));
                vo.setUserName(rs.getString("user_name"));
                vo.setUserClassId(rs.getLong("class_id"));
                if (rs.getInt("user_id") != 0) {
                    vo.setMobilePhone(rs.getString("user_mobile_phone"));
                    vo.setLandLindPhone(rs.getString("user_land_line_phone"));
                    vo.setCountryId(rs.getLong("user_country_id"));
                    vo.setCountryOffset(rs.getString("user_country_offset"));
                 } else {
                    long contactType = rs.getLong("contact_type");
                    String mobilePhone = rs.getString("contact_phone");
                    String landLinePhone = "";
                    if (contactType == Contact.CONTACT_US_SHORT_REG_FORM || contactType == Contact.CONTACT_US_REGISTER_ATTEMPTS || contactType == Contact.CONTACT_US_QUICK_START_ATTEMPTS) {
                        mobilePhone = rs.getString("contact_mobile_phone");
                        landLinePhone = rs.getString("contact_land_line_phone");
                    }
                    vo.setMobilePhone(mobilePhone);
                    vo.setLandLindPhone(landLinePhone);
                    
                }

				// If new qualification color record
				if (entryType != ConstantsBase.ALL_FILTER_ID_INT
						&& vo.getOldPopEntryId() != vo.getCurrEntryId()
						&& vo.getCurrEntryId() !=0){

					setEntryColor(vo, true);
				}

				list.add(vo);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * get all the population entries from DB
	 * @param isNotCalledPops - if true get all updatable pops (call me, decline whice not called since qualification)
	 * @param verifiedUsers TODO
	 * @param lastSalesDepRepId TODO
	 * @param campaignPriority
	 * @param isSpecialCare
	 * @param sortAmountColumn 
	 * @return population entries list
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getPopulationEntries(Connection con,
			String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId,   long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			int startRow, int pageSize, long supportMoreOptionsType, String timeZone, String priorityId,
			String excludeCountries, boolean hideNoAnswer,String callsCount, String countriesGroup,
			boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId,
			int campaignPriority, WriterWrapper wr, boolean isSpecialCare,Date fromLogin , Date toLogin, long lastLoginInXMinutes,
			boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long populationEntryStatusId, long populationDept,
			long userRankId, long lastCallerWriterId, boolean qualificationTimeFilter,  int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {

		ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, Writer> writersMap = wr.getAllWritersMap();
		int index = 1;
	   
	        String ls = System.getProperty("line.separator");
	        ls = ls + "getPopulationEntriesInParams" + ls
	            + "populationTypes: " + populationTypes + ls
	            + "skinIds: " + skinIds + ls
	            + "businessSkin: " + businessSkin + ls
	            + "languageId: " + languageId + ls
	            + "assignedWriterId: " + assignedWriterId + ls
	            + "campaignId: " + campaignId + ls
	            + "countries: " + countries + ls
	            + "fromDate: " + fromDate + ls
	            + "toDate: " + toDate + ls
	            + "colorType: " + colorType + ls
	            + "isAssigned: " + isAssigned + ls
	            + "userId: " + userId + ls
	            + "userName: " + userName + ls
	            + "contactId = " + contactId + ls
	            + "supportMoreOptionsType: " + supportMoreOptionsType + ls
	            + "timeZone: " + timeZone + ls
	            + "priorityId: " + priorityId + ls
	            + "excludeCountries: " + excludeCountries + ls
	            + "hideNoAnswer: " + hideNoAnswer + ls
	            + "callsCount: " + callsCount + ls
	            + "countriesGroup: " + countriesGroup + ls
	            + "isNotCalledPops: " + isNotCalledPops + ls
	            + "includeAffiliates: " + includeAffiliates + ls
	            + "excludeAffiliates = " + excludeAffiliates + ls
	            + "verifiedUsers: " + verifiedUsers + ls            
	            + "lastSalesDepRepId: " + lastSalesDepRepId + ls
	            + "campaignPriority: " + campaignPriority + ls
	            + "isSpecialCare: " + isSpecialCare + ls
	            + "fromLogin: " + fromLogin + ls
	            + "toLogin: " + toLogin + ls            
	            + "lastLoginInXMinutes: " + lastLoginInXMinutes + ls
	            + "balanceBelowMinInvestAmount: " + balanceBelowMinInvestAmount + ls
	            + "madeDepositButdidntMakeInv24HAfter: " + madeDepositButdidntMakeInv24HAfter + ls
	            + "populationEntryStatusId: " + populationEntryStatusId + ls            
	            + "populationDept: " + populationDept + ls
	            + "userRankId: " + userRankId + ls
	            + "lastCallerWriterId: " + lastCallerWriterId + ls
	            + "qualificationTimeFilter: " + qualificationTimeFilter + ls
	            + "issueActionTypeId: " + issueActionTypeId + ls
	            + "lastChatWriterId: " + lastChatWriterId + ls	
	            + "reachCustomerFlag: " + reachCustomerFlag + ls
	            + "retentionTeamId: " + retentionTeamId + ls
	            + "everCalled: " + everCalled + ls
	            + "userPlatform: " + userPlatform + ls
	            + "countryGroupTierId: " + countryGroupTierId + ls;
	        logger.debug(ls);
	    
		
		try {

			String sql = " select " +
							" ent.* " +
						 " from (select * " +
								 " from " + getEntriesTableString(populationTypes,
										 						  skinIds,
										 						  businessSkin,
										 						  languageId,
										 						  assignedWriterId,
										 						  campaignId,
										 						  countries,
										 						  fromDate,
										 						  toDate,
										 						  colorType,
										 						  isAssigned,
										 						  userId,
										 						  userName,
										 						  contactId,
										 						  supportMoreOptionsType,
										 						  timeZone,
										 						  priorityId,
										 						  excludeCountries,
										 						  hideNoAnswer,
										 						  callsCount,
										 						  countriesGroup,
										 						  isNotCalledPops,
										 						  includeAffiliates,
										 						  excludeAffiliates,
										 						  verifiedUsers,
										 						  lastSalesDepRepId,
										 						  campaignPriority,
										 						  isSpecialCare,
										 						  fromLogin,
										 						  toLogin,
										 						  lastLoginInXMinutes,
										 						  balanceBelowMinInvestAmount,
										 						  madeDepositButdidntMakeInv24HAfter,
										 						  populationEntryStatusId,
										 						  populationDept,
										 						  userRankId,
										 						  false,
										 						  lastCallerWriterId,
										 						  qualificationTimeFilter,
										 						  issueActionTypeId,
										 						  lastChatWriterId,
										 						  reachCustomerFlag,
										 						  retentionTeamId,
										 						  everCalled,
										 						  sortAmountColumn,
										 						  userPlatform,
										 						  countryGroupTierId);

			if (pageSize != 0){
				sql +=   " where   " +
						         " row_num > ? " +
						         " and row_num <= ? ";
			}

			sql +=			") ent ";

			ps = con.prepareStatement(sql);

			ps.setLong(index++ , wr.getWriter().getId());
			ps.setLong(index++ , wr.getWriter().getId());
			if(fromLogin != null){
				ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(fromLogin,  Utils.getWriter().getUtcOffset())));
			}
			if(toLogin != null){
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(toLogin),  Utils.getWriter().getUtcOffset())));
			}
			// get current time.
			Calendar cal = null;
			cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, -(int)lastLoginInXMinutes);
			Date timeForHighlight = cal.getTime();
			if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
				ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(timeForHighlight));
			}
			if (pageSize != 0){
				ps.setInt(index++, startRow);
				ps.setInt(index++, startRow + pageSize);
			}

			rs = ps.executeQuery();
			HashMap<Long, Long> investmentLimitsGroupAmountPerCurrency = ApplicationData.getInvLimitisGroupsMap().get(new Long(Constants.INVESTMENT_LIMITS_GROUP_ID)).getAmountPerCurrency();
			long currencyId = 0;
			// Time for highlight
			cal = Calendar.getInstance();
			int lastLoginLevelOne = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_ONE_CODE));
			cal.add(Calendar.MINUTE, -lastLoginLevelOne);
			Date timeForHighlightLevelOne = cal.getTime();
			cal = Calendar.getInstance();
			int lastLoginLevelTwo = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_TWO_CODE));
			cal.add(Calendar.MINUTE, -lastLoginLevelTwo);
			Date timeForHighlightLevelTwo = cal.getTime();
			while (rs.next()) {
				PopulationEntry vo = getEntryVO(rs);
				SpecialCare specialCare = new SpecialCare();
				long lastCallerWriterIdRS = rs.getLong("last_caller_writer_id");
				vo.setLastCallWriterId(lastCallerWriterIdRS);
				if (lastCallerWriterIdRS > 0){
					vo.setLastCallWriter((writersMap.get(lastCallerWriterIdRS).getUserName()));
				}
				vo.setLastCallTime(convertToDate(rs.getTimestamp("last_call_time")));
				vo.setCallsCounts(rs.getInt("calls_num"));
				vo.setReachedCalls(rs.getLong("reached_call"));
				vo.setNotReachedCallsCounts(vo.getCallsCounts() - vo.getReachedCallsCounts());
				vo.setCampaignId(rs.getLong("campaign_id"));
				vo.setCampaignName(rs.getString("campaign_name"));
                vo.setCampaignPriorityId(rs.getInt("campaign_priority_id"));
                vo.setAffiliateKey(rs.getString("affiliate_key"));
                if (rs.getInt("user_id") != 0) {
                	vo.setDynamicParam(rs.getString("user_dynamic_param"));
                } else {
                	vo.setDynamicParam(rs.getString("contact_dynamic_param"));
                }
                currencyId = rs.getLong("user_currency");
                if (currencyId > 0) {
	                if (rs.getLong("user_balance") < investmentLimitsGroupAmountPerCurrency.get(currencyId)) {
	                	vo.setBalanceBelowMinInvestAmount(true);
	                } else {
	                	vo.setBalanceBelowMinInvestAmount(false);
	                }
	                if (rs.getLong("dep_no_inv_24h") == 1) {
	                	vo.setMadeDepositButdidntMakeInv24HAfter(true);
	                } else {
	                	vo.setMadeDepositButdidntMakeInv24HAfter(false);
	                }
                }

				vo.setLastSalesDepRep(rs.getString("last_sales_dep_rep"));
				vo.setLastChatRep(rs.getString("last_chat_writer_id"));
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                vo.setSpecialCare(specialCare);
				//vo.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
                Date timeLastLogin = convertToDate(rs.getTimestamp("time_last_login"));
                vo.setLastLoginInXMinutes(ConstantsBase.ALL_FILTER_ID);
                if (timeLastLogin != null) {
                	vo.setTimeLastLogin(timeLastLogin);
                    if (timeLastLogin.after(timeForHighlightLevelOne)) {
                    	vo.setLastLoginInXMinutes(lastLoginLevelOne);
                    } else if (timeLastLogin.after(timeForHighlightLevelTwo)) {
                    	vo.setLastLoginInXMinutes(lastLoginLevelTwo);
                    }
                }

//                if (populationDept == PopulationsManagerBase.SALES_TYPE_RETENTION) {
//                    vo.setTotalTurnover(rs.getLong("total_turnover"));
//                }
                vo.setFirstDeposit(CommonUtil.displayAmount(rs.getLong("first_deposit"), rs.getLong("user_currency")));
                vo.setUserBalance(CommonUtil.displayAmount(rs.getLong("balance"), rs.getLong("user_currency")));
                vo.setCountDeposit(String.valueOf(rs.getLong("count_transaction")));
                vo.setCbLastCallTime(rs.getDate("last_call_time"));
                vo.setTotalTurnoverTxt(CommonUtil.displayAmount(vo.getTotalTurnover(), rs.getLong("user_currency")));
                vo.setUserStatusId(rs.getLong("user_status_id"));
                vo.setUserRankId(rs.getLong("user_rank_id"));
                vo.setUserRankTxt(rs.getString("user_rank_name"));
                vo.setUserStatusTxt(rs.getString("user_status_name"));
                vo.setPopulationSalesTypeId(rs.getLong("type_id"));
                vo.setPopulationTypeId(rs.getLong("population_type_id"));
                vo.setUserName(rs.getString("user_name"));
                vo.setUserClassId(rs.getLong("class_id"));
                vo.setUserBrandId(rs.getLong("platform_id"));
                vo.setFirstDeclineAmountUsd(rs.getDouble("first_decline_amount_usd"));
                if (rs.getInt("user_id") != 0) {
                    vo.setMobilePhone(rs.getString("user_mobile_phone"));
                    vo.setLandLindPhone(rs.getString("user_land_line_phone"));
                    vo.setCountryId(rs.getLong("user_country_id"));
                    vo.setCountryOffset(rs.getString("user_country_offset"));
                 } else {
                    long contactType = rs.getLong("contact_type");
                    String mobilePhone = rs.getString("contact_phone");
                    String landLinePhone = "";
                    if (contactType == Contact.CONTACT_US_SHORT_REG_FORM || contactType == Contact.CONTACT_US_REGISTER_ATTEMPTS || contactType == Contact.CONTACT_US_QUICK_START_ATTEMPTS) {
                        mobilePhone = rs.getString("contact_mobile_phone");
                        landLinePhone = rs.getString("contact_land_line_phone");
                    }
                    vo.setMobilePhone(mobilePhone);
                    vo.setLandLindPhone(landLinePhone);
                }
                
                vo.setReachCustomerFlag(rs.getLong("flag"));
                vo.setAssignTypeId(rs.getLong("assign_type_id"));
                vo.setTimeAssign(convertToDate(rs.getTimestamp("time_assign")));

				// Fill colors  if General screen or anyother screen when user is in new population
				if (pageSize != 0){
					setEntryColor(vo, true);
				}
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	/**
	 * get the number of the population entries from DB
	 * @param isNotCalledPops - if true get all updatable pops (call me, decline whice not called since qualification)
	 * @param verifiedUsers TODO
	 * @param lastSalesDepRepId TODO
	 * @param campaignPriority
	 * @param isSpecialCare
	 * @param lastCallerWriterId
	 * @param sortAmountColumn 
	 * @return population entries count
	 * @throws SQLException
	 */
	public static int getPopulationEntriesCount(Connection con,
			String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId,  String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountries,
			boolean hideNoAnswer, String callsCount, String countriesGroup, boolean isNotCalledPops,
			String includeAffiliates, String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId, int campaignPriority,
			boolean isSpecialCare,Date fromLogin ,Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount,
			boolean madeDepositButdidntMakeInv24HAfter, long populationEntryStatusId, long populationDept, long userRankId, 
			long lastCallerWriterId, boolean qualificationTimeFilter, int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {

		int rowsCount = 0;
		int index = 1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

		try {

			String sql = " select count(*) as rows_number " +
						 " from " + getEntriesTableString(populationTypes,
								 						  skinIds,
								 						  businessSkin,
								 						  languageId,
								 						  assignedWriterId,
								 						  campaignId,
								 						  countries,
								 						  fromDate,
								 						  toDate,
								 						  colorType,
								 						  isAssigned,
								 						  userId,
								 						  userName,
								 						  contactId,
								 						  supportMoreOptionsType,
								 						  timeZone,
								 						  priorityId,
								 						  excludeCountries,
								 						  hideNoAnswer,
								 						  callsCount,
								 						  countriesGroup,
								 						  isNotCalledPops,
								 						  includeAffiliates,
								 						  excludeAffiliates,
								 						  verifiedUsers,
								 						  lastSalesDepRepId,
								 						  campaignPriority,
								 						  isSpecialCare,
						 								  fromLogin,
						 								  toLogin,
						 								  lastLoginInXMinutes,
						 								  balanceBelowMinInvestAmount,
						 								  madeDepositButdidntMakeInv24HAfter,
						 								  populationEntryStatusId,
						 								  populationDept,
						 								  userRankId,
						 								  false,
						 								  lastCallerWriterId,
						 								  qualificationTimeFilter,
						 								  issueActionTypeId,
						 								  lastChatWriterId,
						 								  reachCustomerFlag,
						 								  retentionTeamId,
						 								  everCalled,
						 								  sortAmountColumn,
						 								  userPlatform,
						 								  countryGroupTierId);

			ps = con.prepareStatement(sql);
			ps.setLong(index++, wr.getWriter().getId());
			ps.setLong(index++, wr.getWriter().getId());
			if(fromLogin != null){
				ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(fromLogin,  Utils.getWriter().getUtcOffset())));
			}
			if(toLogin != null){
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(toLogin),  Utils.getWriter().getUtcOffset())));
			}
            // get current time.
           Calendar cal = null;
           cal = Calendar.getInstance();
           cal.add(Calendar.MINUTE, -(int)lastLoginInXMinutes);
           Date timeForHighlight = cal.getTime();
           if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
               ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(timeForHighlight));
           }
		
			rs = ps.executeQuery();

			while (rs.next()) {
				rowsCount = rs.getInt("rows_number");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return rowsCount;
	}

	/**
	 * Get writer's locked entry
	 * @param con db connection
	 * @param writerId writer for function input
	 * @return
	 * @throws SQLException
	 */
	public static PopulationEntry getWriterLockedEntry(Connection con, long writerId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		PopulationEntry vo = null;

		try {
			String sql = " SELECT " +
							" pu.id population_users_id, " +
						  	" pu.*, " +
						  	" pe.id curr_population_entry_id," +
						  	" pe.*," +
						  	" p.skin_id, " +
						  	" p.population_type_id, " +
						  	" p_old_pop.name p_old_name, " +
				            " p_old_pop.population_type_id pe_old_population_type_id, " +
				            " pe_old_pop.id pe_old_id," +
				            " cn.id country_id, " +
				            " cn.gmt_offset country_offset, " +
				            " c.dynamic_parameter as contact_dynamic_param, " +
					        " u.dynamic_param as user_dynamic_param " +
						 " FROM " +
						 	" population_users pu " +
			             	 	" LEFT JOIN contacts c on pu.contact_id = c.id " +
			             	 	" LEFT JOIN users u on pu.user_id = u.id " +" " +
			             	 			" LEFT JOIN countries cn on (u.country_id = cn.id OR (u.country_id is null AND c.country_id = cn.id)) " +
							 	" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
					 				" LEFT JOIN populations p on pe.population_id = p.id, " +
						 	" population_entries_hist peh "+
						 		" LEFT JOIN population_entries pe_old_pop on peh.population_entry_id = pe_old_pop.id " +
						 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
						 " WHERE " +
						 	" pu.lock_history_id = peh.id " +
						 	" AND peh.writer_id = ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, writerId);

			rs = ps.executeQuery();

			if (rs.next()) {
				vo = new PopulationEntry();
				PopulationsDAOBase.getEntryBaseVO(rs, vo);
				vo.setOldPopulationName(rs.getString("p_old_name"));
				vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setCountryOffset(rs.getString("country_offset"));
				if (rs.getInt("user_id") != 0) {
                	vo.setDynamicParam(rs.getString("user_dynamic_param"));
                } else {
                	vo.setDynamicParam(rs.getString("contact_dynamic_param"));
                }
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}

	/**
	 * get Master Search Users
	 * @param con db connection
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getMasterSearchUsers(Connection con,
			long userId, String username, long contactId, boolean isLockedEntriesSearch) throws SQLException{

		ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

		try {
			String sql = " SELECT " +
							" peh.writer_id as lockingWriter, " +
							" pu.id population_users_id, " +
						  	" pu.*, " +
						  	" pe.*," +
					        " CASE WHEN pe.is_displayed = 0 THEN p.name || ' Not displayed' ELSE p.name END population_name, " +
						  	" (CASE WHEN u.skin_id is not null THEN u.skin_id ELSE c.skin_id END) skin_id, " +
						  	" p.population_type_id, " +
					        " p_old_pop.name p_old_name, " +
					        " p_old_pop.population_type_id pe_old_population_type_id, " +
					        " pe_old_pop.id pe_old_id," +
					        " pet.name entry_type_name, " +
					        " iat.name action_type_name_key, " +
					        " c.dynamic_parameter as contact_dynamic_param, " +
					        " u.dynamic_param as user_dynamic_param, " +
					        " pt.type_id population_sales_type_id, " +
					        " u.rank_id as user_rank, " +
					        " u.status_id user_status, " +
					        " u.class_id, " +
					        " u.user_name " +
						 " FROM " +
						 	 " population_entry_types pet, " +
							 " population_users pu " +
							 " LEFT JOIN population_entries_hist peh on pu.lock_history_id=peh.id " +
							    " LEFT JOIN users u on pu.user_id = u.id " +
							    " LEFT JOIN contacts c on pu.contact_id = c.id " +
						 		" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
						 			" LEFT JOIN populations p on pe.population_id = p.id " +
						 				" LEFT JOIN population_types pt on p.population_type_id = pt.id " +
					 			" LEFT JOIN issue_actions ia on pu.entry_type_action_id = ia.id " +
						 			" LEFT JOIN issue_action_types iat on ia.issue_action_type_id = iat.id " +
                                 	" LEFT JOIN issues i_old_pop on ia.issue_id = i_old_pop.id " +
                                 		" LEFT JOIN population_entries pe_old_pop on i_old_pop.population_entry_id = pe_old_pop.id " +
                                 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
						 " WHERE " +
						 	" pu.entry_type_id = pet.id " +
						 	" AND (u.skin_id is not null and u.skin_id in (select ws.skin_id from WRITERS_SKIN ws where ws.writer_id =?) " +
							 	 " OR " +
								 " c.skin_id is not null and c.skin_id in  (select ws.skin_id from WRITERS_SKIN ws where ws.writer_id =?)) ";


			if (isLockedEntriesSearch &&  !(userId > 0) &&  !(contactId > -1) && !(null != username)){
				sql += " AND pu.lock_history_id is not null ";
			}else if( isLockedEntriesSearch && ( userId > 0 ||  contactId > -1 || null != username)) {

				if (userId > 0){
					sql += " AND u.id = " + userId + " " + 
						   " AND u.is_active = 1 ";
				} else if (contactId > -1){
					sql += " AND nvl(c.id,0) = " + contactId + " ";
				} else if (null != username){
					sql += " AND upper(u.user_name) = '" + username.toUpperCase() + "' " +
						   " AND u.is_active = 1 ";
				}

				sql += " AND NOT (pu.entry_type_id = 1 AND pu.curr_population_entry_id is null) ";
				sql += " AND pu.lock_history_id is not null ";
			}else {

				if (userId > 0){
					sql += " AND u.id = " + userId + " " +
						   " AND u.is_active = 1 ";
				} else if (contactId > -1){
					sql += " AND nvl(c.id,0) = " + contactId + " ";
				} else if (null != username){
					sql += " AND upper(u.user_name) = '" + username.toUpperCase() + "' " +
						   " AND u.is_active = 1 ";
				}

				sql += " AND NOT (pu.entry_type_id = 1 AND pu.curr_population_entry_id is null) ";
			}

			ps = con.prepareStatement(sql);
			ps.setLong(1, w.getWriter().getId());
			ps.setLong(2, w.getWriter().getId());
			rs = ps.executeQuery();

			while (rs.next()) {
				PopulationEntry vo = new PopulationEntry();

				PopulationsDAOBase.getEntryBaseVO(rs, vo);

				vo.setCurrPopulationName(rs.getString("population_name"));
				vo.setOldPopulationName(rs.getString("p_old_name"));
				vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				vo.setLockingWriter(rs.getLong("lockingWriter"));
				vo.setActionTypeName(rs.getString("action_type_name_key"));
				if (rs.getInt("user_id") != 0) {
                	vo.setDynamicParam(rs.getString("user_dynamic_param"));
                } else {
                	vo.setDynamicParam(rs.getString("contact_dynamic_param"));
                }
				vo.setPopulationSalesTypeId(rs.getLong("population_sales_type_id"));
				vo.setUserStatusId(rs.getLong("user_status"));
				vo.setUserRankId(rs.getLong("user_rank"));
                vo.setUserName(rs.getString("user_name"));
                vo.setUserClassId(rs.getLong("class_id"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Check if user is in marketing with no disply
	 * @param userId
	 * @return marketing pop entry id
	 * @throws SQLException
	 */
	public static long getUserNoDisplayMarketingPopEntryId(Connection con,long userId) throws SQLException  {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =" Select " +
							" pe.id " +
						" From " +
							" population_users pu," +
							" population_entries pe," +
							" populations p" +
							"  " +
						" Where " +
							" pu.curr_population_entry_id = pe.id " +
							" and pe.population_id = p.id " +
							" and p.population_type_id = " + PopulationsManagerBase.POP_TYPE_MARKETING + " " +
							" and pe.is_displayed = 0 " +
							" and pu.user_id = " + userId ;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("id");
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

	private static void setEntryColor(PopulationEntry vo, boolean checkFDDPreviouscall) throws NumberFormatException, SQLException {
		Writer w = Utils.getWriter().getWriter();
		// If current user's population was reached color in green
		// Else check current user's popualtion qualification time
		if (vo.getReachedCallsCounts() > 0){
			if(w.getScreenType() == PopulationsManagerBase.SALES_TYPE_CONVERSION) {
				vo.setRowColor(ConstantsBase.COLOR_GREEN);
				vo.setRowColorId(ConstantsBase.COLOR_GREEN_ID);
			} else {
				if (vo.getAssignWriterId() == vo.getLastCallWriterId()) {
					vo.setRowColor(ConstantsBase.COLOR_GREEN);
					vo.setRowColorId(ConstantsBase.COLOR_GREEN_ID);
				} else {
					vo.setRowColor(ConstantsBase.COLOR_BLACK);
					vo.setRowColorId(ConstantsBase.COLOR_BLACK_ID);
				}
			}
		} else {
			if(w.getScreenType() == PopulationsManagerBase.SALES_TYPE_CONVERSION) {
				GregorianCalendar gc=new GregorianCalendar();
				gc.add(GregorianCalendar.HOUR, -1);
				Date anHourAgo = gc.getTime();

				if (null != vo.getQualificationTime()){
					// If qualification time is in the last hour count black otherwise blue
					if (vo.getQualificationTime().after(anHourAgo)) {
						vo.setRowColor(ConstantsBase.COLOR_BLACK);
						vo.setRowColorId(ConstantsBase.COLOR_BLACK_ID);
					} else {
						vo.setRowColor(ConstantsBase.COLOR_BLUE);
						vo.setRowColorId(ConstantsBase.COLOR_BLUE_ID);
					}
				}
				if (checkFDDPreviouscall) {
					isFitForFDDPreviouscallIndication(vo);
				}
			} else {
				vo.setRowColor(ConstantsBase.COLOR_BLACK);
				vo.setRowColorId(ConstantsBase.COLOR_BLACK_ID);
			}
		}
		Date timeAssign = vo.getTimeAssign();
		if (timeAssign != null) {
			Calendar now = Calendar.getInstance();
			Calendar timeHighlightAssign = Calendar.getInstance();
			timeHighlightAssign.setTime(timeAssign);
			timeHighlightAssign.add(Calendar.HOUR_OF_DAY, Integer.valueOf(CommonUtil.getEnum(PopulationUsersAssignTypesManagerBase.ENUM_LAST_BULK_REASSIGN_ENUMERATOR, PopulationUsersAssignTypesManagerBase.ENUM_LAST_BULK_REASSIGN_CODE)));
			if (vo.getAssignTypeId() == PopulationUsersAssignTypesManagerBase.TYPE_BULK_REASSIGN && now.before(timeHighlightAssign)) {
				vo.setRowColor(ConstantsBase.COLOR_BLACK);
				vo.setRowColorId(ConstantsBase.COLOR_BLACK_ID);
				vo.setBackgroundColor(ConstantsBase.COLOR_BLACK);
			}
		}
	}

	private static void isFitForFDDPreviouscallIndication(PopulationEntry vo) {
		try {
			if (vo.getCurrPopualtionTypeId() == Constants.POPULATION_TYPE_ID_FDD) {
				if(PopulationsManager.hasReachedCallInOtherPopulations(vo.getUserId(), vo.getCurrEntryId()))
					vo.setFddPreviousCallIndicator(true);
			} else {
				vo.setFddPreviousCallIndicator(false);
			}
		} catch (PopulationHandlersException e) {
			e.printStackTrace();
		}
	}

	public static boolean hasReachedCallInOtherPopulations(Connection con, long userId , long entryTypeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT " +
						 "	    ia.id " +
						 " FROM " +
						 "		population_entries pe, " +
						 "      population_users pu, " +
						 "		issues i, " +
						 "		issue_actions ia, " +
						 "		issue_action_types iat " +
						 " WHERE pe.population_users_id = pu.id " +
						 "		AND i.user_id = ? " +
						 "		AND i.user_id = pu.user_id " +
						 "      AND ia.issue_id = i.id " +
						 "      AND pe.id <> ? " +
						 "      AND ia.action_time between (sysdate - 4/24) AND sysdate " +
						 "      AND ia.issue_action_type_id = iat.id " +
						 "      AND iat.reached_status_id = 2 ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, entryTypeId);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}


	public static boolean reachedTotalEntries(Connection con, long writerId, long skinId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
			String sql = " SELECT " +
						 "		max_assignments.max_res, " +
						 "		current_assigments.curr_res " +
						 " FROM " +
						 "		(SELECT " +
						 "				ws.assign_limit as max_res" +
						 "		 FROM " +
						 "				writers_skin ws " +
						 "		 WHERE " +
						 "				ws.writer_id = ? " +
						 "				AND ws.skin_id = ?) max_assignments, " +
						 "		(SELECT " +
						 "			count(pu.id) as curr_res " +
						 "		 FROM " +
						 "	   		population_users pu, " +
						 "			population_entries pe, " +
						 "			users u " +
						 "		 WHERE " +
						 "			pu.curr_population_entry_id = pe.id " +
					     "     		AND pu.user_id = u.id " +
					     "			AND pu.curr_assigned_writer_id = ? " +
					     "			AND u.skin_id = ? ) current_assigments " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, writerId);
			ps.setLong(2, skinId);
			ps.setLong(3, writerId);
			ps.setLong(4, skinId);
			rs = ps.executeQuery();

    		if ( rs.next() ) {
    			long maxRes = rs.getLong("max_res");
    			long currRes = rs.getLong("curr_res");
    			if (maxRes == 0) { //writer don't have any limits to this current skin
    				return false;
    			}
    			if(currRes >= maxRes) {
    				logger.info("Reached total limit for writerId=" + writerId + " limit=" + maxRes + " sum of entries=" + currRes);
    				return true;
    			}
    		}
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
		}
		return false;
	}


	public static boolean checkAssignLimitByRankAndStatus(Connection con, long writerId, long skinId, long userRankId, long userStatusId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
			String sql =" SELECT " +
						"	limits.res writer_limit, " +
						"	assigns.res sum_assign, " +
						" 	(limits.res - assigns.res) as res " +
						" FROM " +
						"	(SELECT " +
						"    	ws.assign_limit * srd.percentage * ssd.percentage as res " +
						" 	 FROM " +
					    " 		writers w," +
					    "		writers_skin ws, " +
                        "		writer_rank_distribution wrd, " +
                        "		sales_rank_distribution srd, " +
                        "		sales_status_distribution ssd " +
					    "	WHERE " +
					    "	    w.id = ws.writer_id " +
						"		AND wrd.writer_skin_id = ws.id " +
						"		AND wrd.rank_distribution_group_id = srd.group_id " +
						"		AND srd.status_distribution_group_id = ssd.group_id " +
					    "	    AND ws.skin_id = ? " +
					    "       AND w.id = ? " +
					    "	    AND srd.rank_id = ? " +
					    "       AND ssd.status_id = ?)  limits, " +
					    "	(SELECT " +
					    "		count(pu.id) as res " +
					    "	 FROM " +
					    "	   	population_users pu, " +
					    "		population_entries pe, " +
					    "		users u " +
					    "	 WHERE " +
					    "		pu.curr_population_entry_id = pe.id " +
					    "     	AND pu.user_id = u.id " +
					    "		AND u.skin_id = ? " +
					    "		AND pu.curr_assigned_writer_id = ? " +
					    "	    AND u.rank_id = ? " +
					    "     	AND u.status_id = ?) assigns ";

		ps = con.prepareStatement(sql);
		ps.setLong(1, skinId);
		ps.setLong(2, writerId);
		ps.setLong(3, userRankId);
		ps.setLong(4, userStatusId);
		ps.setLong(5, skinId);
		ps.setLong(6, writerId);
		ps.setLong(7, userRankId);
		ps.setLong(8, userStatusId);
		rs = ps.executeQuery();
		if (rs.next()) {
			double result = rs.getDouble("res");
			double writerLimit = rs.getDouble("writer_limit");
			double sumAssigns = rs.getDouble("sum_assign");
			logger.info("Limits for writerId=" + writerId + " skinId=" + skinId + " userRankId=" + userRankId +
						" userStatusId=" + userStatusId + " writerLimit=" + writerLimit + " sumAssigns=" + sumAssigns + " result=" + result);
			if(Math.round(result) >= 1) {
				return true;
			}
			return false;
		}
		logger.info("No limits for writerId=" + writerId + " skinId=" + skinId + " userRankId=" + userRankId +  " userStatusId=" + userStatusId);
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
		}
		// writer not found for limits
		return true;
	}


	public static ArrayList<PopulationEntryReport> getConversionLeadCalled(Connection con, Date startDate, Date endDate, String writerFilter) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  PopulationEntryReport vo = null;
		  ArrayList<PopulationEntryReport> populationEntryReport = new ArrayList<PopulationEntryReport>();

		  try
		  {
			  String sql = " SELECT " +
								" ia.id IssueActionId, " +
								" ic.name Channel, " +
								" w.user_name Rep, " +
								" u.id UserId, " +
								" u.user_name UserName, " +
								" u.mobile_phone UsersMobilePhone, " +
								" u.land_line_phone UsersLandLinePhone, " +
								" u.email UsersEmail, " +
								" c.id  ContactId, " +
								" c.mobile_phone ContactsMobilePhone, " +
								" c.land_line_phone ContactLandLinePhone, " +
								" c.email ContactEmail, " +
								" ia.action_time DateOfTheCall " +
							" FROM " +
								" population_users pu " +
								" LEFT JOIN users u on pu.user_id=u.id " +
								" AND  u.class_id <> 0 " +
								" LEFT JOIN contacts c on pu.contact_id=c.id, " +
								" issues i, " +
								" population_entries pe, " +
								" populations p, " +
								" issue_actions ia, " +
								" population_types pt, " +
								" writers w, " +
								" issue_action_types iat, " +
								" issue_channels ic " +
							" WHERE " +
								" i.population_entry_id = pe.id " +
								" AND i.id = ia.issue_id " +
								" AND pu.id = pe.population_users_id " +
								" AND p.id = pe.population_id " +
								" AND pt.id = p.population_type_id " +
								" AND w.id = ia.writer_id " +
								" AND iat.id = ia.issue_action_type_id " +
								" AND ic.id = iat.channel_id " +
								" AND pt.type_id = " + Constants.POPULATION_TYPE_ID_FDD;
			if (!writerFilter.equals(ConstantsBase.ALL_REP)){
				sql += " AND w.id in (" + writerFilter + ") ";
			}
			sql += " AND i.time_created  between ? and ? ";

			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(startDate));
			ps.setTimestamp(2 , CommonUtil.convertToTimeStamp(endDate));
			rs=ps.executeQuery();
			while (rs.next()) {
				vo = new PopulationEntryReport();
				vo.setActionId(rs.getLong("IssueActionId"));
				vo.setChannelName(rs.getString("Channel"));
				vo.setWriterName(rs.getString("Rep"));
				vo.setUserId(rs.getLong("UserId"));
				vo.setUserName(rs.getString("UserName"));
				vo.setUsersMobilePhone(rs.getString("UsersMobilePhone"));
				vo.setUsersLandLinePhone(rs.getString("UsersLandLinePhone"));
				vo.setUsersEmail(rs.getString("UsersEmail"));
				vo.setContactId(rs.getLong("ContactId"));
				vo.setContactsMobilePhone(rs.getString("ContactsMobilePhone"));
				vo.setContactLandLinePhone(rs.getString("ContactLandLinePhone"));
				vo.setContactEmail(rs.getString("ContactEmail"));
				vo.setActionTime(rs.getDate("DateOfTheCall"));
				populationEntryReport.add(vo);
			}
		}
		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return populationEntryReport;
	}
	
	/**
	 * Get count success deposit by user
	 * @param con
	 * @param userId
	 * @return count success deposit by user
	 * @throws SQLException
	 */
	public static long getCountSuccessDepositByUser(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
			String sql = " SELECT " +
							 " t.user_id, " +
							 " COUNT(t.id) count_dep " +
						" FROM " +
							 " transactions t, " +
							 " transaction_types tt " +
						" WHERE       " +
							 " t.type_id = tt.id " +
							 " AND t.user_id = ? " +
							 " AND tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT +
							 " AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
						" GROUP BY t.user_id";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("count_dep");
			}
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
		}
		// user do not have success deposit.
		return ConstantsBase.COUNT_NO_DEPOSIT;
	}
	
	/**
	 * Get entries conversion for sales.
	 * @param con
	 * @param entriesForm
	 * @return list as ArrayList<PopulationEntry>
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getEntriesConversion(Connection con, SalesEntriesForm entriesForm) throws SQLException {
		/* INITIAL LISTS */
		ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();
		logger.info("Create descriptors VARCHAR_ARR for each Oracle collection type required.");
        ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_ARR", con);
        logger.info("Cast the Java arrays into Oracle arrays VARCHAR_ARR.");
        ArrayList<String> timeZone = entriesForm.getTimeZoneList();
        ARRAY oracleTimeZone = new ARRAY (oracleVarcharCollection, con, timeZone.toArray(new String[timeZone.size()]));
        ArrayList<String> countries = entriesForm.getSelectedCountriesList();
        ARRAY oracleCountries = new ARRAY (oracleVarcharCollection, con, countries.toArray(new String[countries.size()]));
        ArrayList<String> populationTypes = entriesForm.getPopulationTypesList();
        ARRAY oraclePopulationTypes = new ARRAY (oracleVarcharCollection, con, populationTypes.toArray(new String[populationTypes.size()]));
        ArrayList<String> affiliatesKeys = entriesForm.getAffiliatesKeysList();
        ARRAY oracleAffiliatesKeys = new ARRAY (oracleVarcharCollection, con, affiliatesKeys.toArray(new String[affiliatesKeys.size()]));
        logger.info("End Casting into VARCHAR_ARR.");
        /* CallableStatement AND DbmsOutput*/
        CallableStatement stmt = null;
		String sql = "{? = call SALES.GET_ENTRIES_CONVERSION (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		DbmsOutput dbmsOutput = new DbmsOutput( con );
        dbmsOutput.enable();
		stmt = con.prepareCall(sql);
		int index = 1;
        /* Bind the output array, this will contain any exception indexes. */
        stmt.registerOutParameter(index++, OracleTypes.CURSOR);
        /* Bind the input data. */
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        stmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(entriesForm.getFromDate()));
        stmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(entriesForm.getToDate()));
        stmt.setLong(index++, entriesForm.getCampaignId());
        stmt.setObject(index++, oracleTimeZone);
        stmt.setInt(index++, entriesForm.getRetentionStatusColor());
        stmt.setLong(index++, entriesForm.getWriterId());
        stmt.setInt(index++, entriesForm.getIssueActionTypeId());
        stmt.setString(index++, entriesForm.getUsername());
        stmt.setBoolean(index++, entriesForm.isHideNoAnswer());
        stmt.setLong(index++, entriesForm.getCountriesGroupId());
        stmt.setLong(index++, entriesForm.getLastCallerWriterId());
        stmt.setLong(index++, entriesForm.getCampaignPriority());
        Date fromLogin = entriesForm.getTempFromLogin();
        Date toLogin = entriesForm.getTempToLogin();
        stmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(fromLogin));
        stmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(toLogin));
        stmt.setInt(index++, entriesForm.getMinCallsCountCurrPop());
        stmt.setInt(index++, entriesForm.getMaxCallsCountCurrPop());
        stmt.setInt(index++, PopulationEntriesManager.MAX_CALLS_COUNT_CURR_POP);
        stmt.setInt(index++, entriesForm.getMinCallsCountGeneral());
        stmt.setInt(index++, entriesForm.getMaxCallsCountGeneral());
        stmt.setInt(index++, PopulationEntriesManager.MAX_CALLS_COUNT_GENERAL);
        stmt.setLong(index++, entriesForm.getLanguageId());
        stmt.setLong(index++, wr.getWriter().getId());
        stmt.setObject(index++, oracleCountries);
        stmt.setObject(index++, oraclePopulationTypes);
        stmt.setObject(index++, oracleAffiliatesKeys);
        
        logger.info("Execute query.");
        stmt.execute();
        logger.info("Start display dbmsOutput.");
        dbmsOutput.show();
        dbmsOutput.close();
        logger.info("End display dbmsOutput");
        /* INITIAL PARAMETERS */
        Calendar cal = null;
        int lastLoginMinutes = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_ONE_CODE));
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -(int)lastLoginMinutes);
		Date timeForHighlight = cal.getTime();
		HashMap<Long, Writer> writersMap = wr.getAllWritersMap();
		logger.info("Get ResultSet.");
		ResultSet rs = (ResultSet)stmt.getObject(1);
		logger.info("Start to set PopulationEntry by ResultSet.");
		if (!rs.isBeforeFirst() ) {
			logger.info("No data.");
		}
		// Time for highlight
		cal = Calendar.getInstance();
		int lastLoginLevelOne = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_ONE_CODE));
		cal.add(Calendar.MINUTE, -lastLoginLevelOne);
		Date timeForHighlightLevelOne = cal.getTime();
		cal = Calendar.getInstance();
		int lastLoginLevelTwo = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_TWO_CODE));
		cal.add(Calendar.MINUTE, -lastLoginLevelTwo);
		Date timeForHighlightLevelTwo = cal.getTime();
        while (rs.next()) {
        	PopulationEntry vo = new PopulationEntry();
        	long userId = rs.getLong("user_id");
        	vo.setUserId(userId);
        	vo.setLockHistoryId(rs.getLong("lock_history_id"));
        	vo.setContactId(rs.getLong("contact_id"));
        	vo.setCurrEntryId(rs.getLong("curr_population_entry_id"));
        	vo.setQualificationTime(convertToDate(rs.getTimestamp("qualification_time")));
        	vo.setCurrPopulationName(rs.getString("population_name"));
        	vo.setLanguageId(rs.getLong("language_id"));
            vo.setCountryId(rs.getLong("country_id"));
            vo.setCountryOffset(rs.getString("gmt_offset"));
            vo.setSkinId(rs.getLong("skin_Id"));
            vo.setPopulationSalesTypeId(rs.getLong("type_id"));
            vo.setCurrPopualtionTypeId(rs.getLong("population_type_id"));
            vo.setAssignWriterId(rs.getLong("curr_assigned_writer_id"));
            vo.setEntryTypeId(rs.getInt("entry_type_id"));
            vo.setPopulationUserId(rs.getLong("population_users_id"));
            vo.setUserName(rs.getString("user_name"));
            vo.setUserClassId(rs.getLong("class_id"));
            vo.setCallsCounts(rs.getInt("calls_num"));
            vo.setDynamicParam(rs.getString("dynamic_param"));
            long lastCallerWriterIdRS = rs.getLong("last_caller_writer_id");
			vo.setLastCallWriterId(lastCallerWriterIdRS);
			if (lastCallerWriterIdRS > 0){
				vo.setLastCallWriter((writersMap.get(lastCallerWriterIdRS).getUserName()));
			}
			vo.setLastCallTime(convertToDate(rs.getTimestamp("last_call_time")));
            Date timeLastLogin = convertToDate(rs.getTimestamp("time_last_login"));
            vo.setLastLoginInXMinutes(ConstantsBase.ALL_FILTER_ID);
            if (timeLastLogin != null) {
            	vo.setTimeLastLogin(timeLastLogin);
                if (timeLastLogin.after(timeForHighlightLevelOne)) {
                	vo.setLastLoginInXMinutes(lastLoginLevelOne);
                } else if (timeLastLogin.after(timeForHighlightLevelTwo)) {
                	vo.setLastLoginInXMinutes(lastLoginLevelTwo);
                }
            }
            if (userId != 0) {
                vo.setMobilePhone(rs.getString("user_mobile_phone"));
                vo.setLandLindPhone(rs.getString("user_land_line_phone"));
                vo.setCountryId(rs.getLong("user_country_id"));
             } else {
                long contactType = rs.getLong("contact_type");
                String mobilePhone = rs.getString("contact_phone");
                String landLinePhone = "";
                if (contactType == Contact.CONTACT_US_SHORT_REG_FORM || contactType == Contact.CONTACT_US_REGISTER_ATTEMPTS || contactType == Contact.CONTACT_US_QUICK_START_ATTEMPTS) {
                    mobilePhone = rs.getString("contact_mobile_phone");
                    landLinePhone = rs.getString("contact_land_line_phone");
                }
                vo.setMobilePhone(mobilePhone);
                vo.setLandLindPhone(landLinePhone);
            }
            setEntryColor(vo, false);
        	list.add(vo);
        }
        logger.info("End to set PopulationEntry by ResultSet.");
		return list;
	}
	
	/**
	 * Get population entries for reassign
	 * @param con
	 * @param writerId
	 * @param salesType
	 * @param entryTypesList
	 * @param userRankId
	 * @param userStatus
	 * @return ArrayList<PopulationEntry>
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getPopulationEntriesForReassign(Connection con, long writerId, long salesType, String entryTypesList, long userRankId, String userStatus) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();
		int index = 1;
		try {
			String sql =
		    	" SELECT " +
		    		" pu.id as population_user_id, " +
					" pu.*, " +
					" u.*, " +
					" i_old_pop.population_entry_id AS old_pop_ent_id, " +
					" CASE WHEN pu.user_id is null THEN c.skin_id ELSE u.skin_id END as skin_id " +
				" FROM " +
					" writers w, " +
				    " population_users pu " +
				    " LEFT JOIN contacts c on pu.contact_id = c.id " +
                    " LEFT JOIN users u on pu.user_id = u.id " +
                    " LEFT JOIN issue_actions ia on pu.entry_type_action_id = ia.id " +
                 	" LEFT JOIN issue_action_types iat on iat.id = ia.issue_action_type_id " +
                 	" LEFT JOIN issues i_old_pop on ia.issue_id = i_old_pop.id " +
				" WHERE " +
					" pu.curr_assigned_writer_id = w.id " +
					" AND pu.curr_assigned_writer_id is not null " +
					" AND EXISTS ( select 1 " +
								" from roles r " +
								" where  w.user_name = r.user_name and " +
								" r.role like ? or r.role like ?) " +
		    		" AND w.id = ? " +
		    		" AND pu.entry_type_id in ( " + entryTypesList + ") " +
		    		" AND pu.curr_population_entry_id in " +
	    					" (SELECT " +
	    						" pe.id " +
	    					" FROM " +
	    						" population_users ppu " +
	    						" join population_entries pe on  ppu.curr_population_entry_id = pe.id " +
	    						" join populations p on  pe.population_id =p.id " +
	    						" join population_types pt on  p.population_type_id=pt.id " +
	    						" left join users u on u.id = ppu.user_id " +
	    						" left join contacts cts on cts.id = ppu.contact_id " +
	    					" WHERE " +
	    						" (u.class_id  != 0 OR u.class_id  IS NULL) " +
	    						" AND((u.skin_id IS NOT NULL AND u.skin_id  IN " +
	    							" (SELECT " +
	    									" ws.skin_id " +
	    								" FROM " +
	    									" writers_skin ws " +
	    								" WHERE ws.writer_id = w.id )) " +
	    							" OR (cts.skin_id IS NOT NULL AND cts.skin_id IN " +
	    								" (SELECT " +
	    										" ws.skin_id " +
	    									" FROM " +
	    										" writers_skin ws " +
	    									" WHERE " +
	    										" ws.writer_id = w.id ))) ";
		    if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION) {
		    	sql += " AND (pt.type_id = "+ PopulationsManagerBase.SALES_TYPE_CONVERSION +
		    				" OR (pt.id  = " + PopulationsManagerBase.POP_TYPE_CALLME +
		    					" AND u.first_deposit_id IS NULL)) " +
		    			" AND pe.is_displayed = 1 " +
		    			" AND pe.group_id = 1) ";
		    } else if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION) {
		    	sql += " AND pt.type_id = " + ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION + ")" +
		    			" AND u.rank_id = " + userRankId +
		    			" AND u.status_id in (" + userStatus +") ";
		    }
			ps = con.prepareStatement(sql);
			ps.setString(index++, ConstantsBase.ROLE_RETENTION);
			ps.setString(index++, ConstantsBase.ROLE_RETENTIONM);
			ps.setLong(index++, writerId);
			rs = ps.executeQuery();
			while (rs.next()) {
				PopulationEntry pe = new PopulationEntry();
				pe.setSkinId(rs.getLong("skin_id"));
				pe.setUserRankId(rs.getLong("rank_id"));
				pe.setUserStatusId(rs.getLong("status_id"));
				pe.setCurrEntryId(rs.getLong("curr_population_entry_id"));
				pe.setEntryTypeId(rs.getInt("entry_type_id"));
				pe.setAssignWriterId(rs.getLong("curr_assigned_writer_id"));
				pe.setPopulationUserId(rs.getLong("population_user_id"));
				pe.setOldPopEntryId(rs.getLong("old_pop_ent_id"));
				pe.setUserId(rs.getLong("user_id"));
				pe.setUserName(rs.getString("user_name"));
				pe.setUserClassId(rs.getLong("class_id"));
				list.add(pe);
			}
		} finally	{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get total representative's portfolio
	 * @param con
	 * @param writerId
	 * @return
	 * @throws SQLException
	 */
	public static long getTotalRepPortfolio(Connection con, long writerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		long totalAssignments = 0;
		try {
			String sql = " SELECT " +
						 	" count(pu.id) as total_assignments " +
						 " FROM " +
						 	" population_users pu, " +
						 	" population_entries pe " +
						 " WHERE " +
						 	" pu.curr_population_entry_id = pe.id " +
						 	" and pu.curr_assigned_writer_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, writerId);
			rs = ps.executeQuery();
			if (rs.next()) {
				totalAssignments = rs.getLong("total_assignments");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return totalAssignments;
	}
	
	/**
	 * Get entries for collect
	 * @param con
	 * @param skinId
	 * @param writerToCollect
	 * @param skins
	 * @param salesType
	 * @param entryTypesList
	 * @return ArrayList<PopulationEntry>
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntry> getEntriesForCollect(Connection con, long skinId, long writerToCollect, String skins, long salesType, String entryTypesList) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<PopulationEntry> list = new ArrayList<PopulationEntry>();

		  try {
			  String sql =
				" SELECT " +
			        " pu.lock_history_id, " +
			        " pu.entry_type_id, " +
			        " pu.curr_population_entry_id, " +
					" pu.curr_assigned_writer_id, " +
					" pu.id population_users_id, " +
					" CASE WHEN pu.user_id is null THEN c.user_id ELSE pu.user_id END as user_id, " +
					" u.user_name, " +
					" u.class_id, " +
					" pe_old_pop.id pe_old_id " +
				" FROM " +
				    " writers w, " +
				    " population_users pu " +
					    " LEFT JOIN contacts c on pu.contact_id = c.id " +
                        " LEFT JOIN users u on pu.user_id = u.id " +
                        " LEFT JOIN issue_actions ia on pu.entry_type_action_id = ia.id " +
                     	" LEFT JOIN issues i_old_pop on ia.issue_id = i_old_pop.id " +
                        " LEFT JOIN population_entries pe_old_pop on i_old_pop.population_entry_id = pe_old_pop.id " +
                " WHERE " +
				    WritersDAO.getPartialQueryOverview(skinId, writerToCollect, skins, salesType);
		    if (null != entryTypesList) {
		    	sql += " AND pu.entry_type_id in (" + entryTypesList + ") ";
		    }

			ps = con.prepareStatement(sql);
			ps.setString(1, ConstantsBase.ROLE_RETENTION);
			ps.setString(2, ConstantsBase.ROLE_RETENTIONM);
			rs = ps.executeQuery();
			while (rs.next()) {
				PopulationEntry vo = new PopulationEntry();
				vo.setLockHistoryId(rs.getLong("lock_history_id"));
				vo.setEntryTypeId(rs.getInt("entry_type_id"));
				vo.setCurrEntryId(rs.getLong("curr_population_entry_id"));
				vo.setAssignWriterId(rs.getLong("curr_assigned_writer_id"));
	        	vo.setPopulationUserId(rs.getLong("population_users_id"));
	        	vo.setUserId(rs.getLong("user_id"));
	        	vo.setUserName(rs.getString("user_name"));
	        	vo.setUserClassId(rs.getLong("class_id"));
	        	vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				list.add(vo);
			}
		} finally	{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}
