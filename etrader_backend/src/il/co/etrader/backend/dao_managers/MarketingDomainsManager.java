package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * MarketingDomainsManager
 * @author eyalo
 */
public class MarketingDomainsManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(MarketingDomainsManager.class);
	
	public static final String UPDATE_DOMAINS_BY_COMPONENT_SKIN_ID = "skinId";
	public static final String UPDATE_DOMAINS_BY_COMPONENT_LANDING_PAGE_ID = "landingPageId";
	public static final String UPDATE_DOMAINS_BY_COMPONENT_URL_SOURCE = "urlSource";
	
	/**
	 * Get Marketing Domains By filters SI
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getMarketingDomainsByfiltersSI(long skinId, long urlSourceTypeId, long marketingLandingpageId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return MarketingDomainsDAO.getMarketingDomainsByfiltersSI(con, skinId, urlSourceTypeId, marketingLandingpageId);
		} finally {
			closeConnection(con);
		}
	}
}
