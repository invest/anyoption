package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import il.co.etrader.backend.bl_vos.LoginProductBean;
import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductDAO extends il.co.etrader.dao_managers.LoginProductDAO {

	public static Map<Long, LoginProductBean> getLoginProductMap(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, LoginProductBean> result = new HashMap<>();
		try {
			String sql = "select * from login_product_map";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				LoginProductBean product = new LoginProductBean();
				product.setSkinId(rs.getLong("skin_id"));
				product.setLoginProduct(LoginProductEnum.getById(rs.getInt("login_product_type_id")));
				product.setEnabled(rs.getBoolean("is_enabled"));
				product.setWriterId(rs.getLong("writer_id"));
				if (rs.getTimestamp("time_updated") != null) {
					product.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")));
				}
				result.put(product.getSkinId(), product);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result;
	}

	public static boolean updateLoginProducts(	Connection con, List<Long> skinIds, LoginProductEnum product, boolean enabled,
											long writerId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update login_product_map set is_enabled = ?, writer_id = ?, time_updated = sysdate, login_product_type_id = ? "
						+ "where skin_id member of ?";
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, enabled);
			ps.setLong(2, writerId);
			ps.setInt(3, product.getId());
			ps.setArray(4, getPreparedSqlArray(con, skinIds));
			return ps.executeUpdate() > 0;
		} finally {
			closeStatement(ps);
		}
	}
}