package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.RegLeadsDetails;
import il.co.etrader.backend.bl_vos.RegLeadsTotals;
import il.co.etrader.backend.bl_vos.RemarketingReport;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.MarketingReport;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;

public class MarketingReportDAO extends DAOBase {


	  /**
	   * Get all MarketingRepor data
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingReport
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingReport> getAll(Connection con, long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, String offset,long sourceId, long landingPage, long marketingCampaignManager, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();

		  try {
			    String sql ="Select * from table( MARKETING_REPORTS.GET_MARKETING_BASE_REPORT(?, ?, ?) ) " +
			    			" Where " +
			    			" decode(?,0,0,?)=decode(?,0,0,fi_source_id) " +
			    			" and decode(?,0,0,?)=decode(?,0,0,fi_campaign_manager_id) ";
			    if(writerIdForSkin>0){
			    	sql+=" and skin_id IN ( select skin_id from writers_skin where writer_id= "+ writerIdForSkin +" ) ";
			    }
				ps = con.prepareStatement(sql);

				Timestamp startTs = CommonUtil.convertToTimeStamp(startDate);
				Timestamp endTs = CommonUtil.convertToTimeStamp(endDate);
				
				ps.setTimestamp(1, startTs);
				ps.setTimestamp(2, endTs);
				ps.setLong(3, skinId);
				
				ps.setLong(4, sourceId);
				ps.setLong(5, sourceId);
				ps.setLong(6, sourceId);
								
				ps.setLong(7, marketingCampaignManager);
				ps.setLong(8, marketingCampaignManager);
				ps.setLong(9, marketingCampaignManager);
		
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingReport vo = new MarketingReport();
					
					vo.setCampaignManager(rs.getString("campaign_manager"));
					vo.setCombId(rs.getLong("combination_id"));
					vo.setCampaignName(rs.getString("campaign_name"));
					vo.setSourceName(rs.getString("source_name"));
					vo.setMedium(rs.getString("medium_name"));
					vo.setContent(rs.getString("content_name"));
					vo.setMarketSizeHorizontal(rs.getLong("marketing_size_horizontal"));
					vo.setMarketSizeVertical(rs.getLong("marketing_size_vertical"));
					vo.setLocation(rs.getString("m_location"));
					vo.setLandingPageName(rs.getString("landing_page_name"));
					vo.setSkinId(rs.getLong("skin_id"));
					vo.setShortReg(rs.getLong("short_reg"));
					vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
					vo.setFtd(rs.getLong("ftd"));
					vo.setRfd(rs.getLong("rfd"));
					vo.setFirstRemDepNum(rs.getLong("first_remarketing_dep_num"));
					vo.setHouseWin(rs.getLong("house_win"));
					vo.setFailedUsers(rs.getLong("failed_users"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }
	  
    public static ArrayList<MarketingReport> getBaseReportByDate(Connection con,long skinId, long skinBusinessCaseId, Date startDate, Date endDate, String offset, int reportType, long sourceId, long marketingCampaignManager, long writerIdForSkin)
            throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();

        try {
            String sql = "Select * from table( MARKETING_REPORTS.GET_MARKETING_BASE_REP_BY_DATE(?, ?, ?, ?, ?) ) ";

            if (writerIdForSkin > 0) {
                sql += " where skin_id IN ( select skin_id from writers_skin where writer_id= "
                        + writerIdForSkin + " ) ";
            }

            ps = con.prepareStatement(sql);
            Timestamp startTs = CommonUtil.convertToTimeStamp(startDate);
            Timestamp endTs = CommonUtil.convertToTimeStamp(endDate);

            ps.setTimestamp(1, startTs);
            ps.setTimestamp(2, endTs);
            ps.setLong(3, skinId);
            ps.setLong(4, sourceId);
            ps.setLong(5, marketingCampaignManager);

            rs = ps.executeQuery();

            while (rs.next()) {
                MarketingReport vo = new MarketingReport();

                vo.setDates(rs.getDate("dates"));
                vo.setCampaignManager(rs.getString("campaign_manager"));
                vo.setCombId(rs.getLong("combination_id"));
                vo.setCampaignName(rs.getString("campaign_name"));
                vo.setSourceName(rs.getString("source_name"));
                vo.setMedium(rs.getString("medium_name"));
                vo.setContent(rs.getString("content_name"));
                vo.setMarketSizeHorizontal(rs.getLong("marketing_size_horizontal"));
                vo.setMarketSizeVertical(rs.getLong("marketing_size_vertical"));
                vo.setLocation(rs.getString("m_location"));
                vo.setLandingPageName(rs.getString("landing_page_name"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setShortReg(rs.getLong("short_reg"));
                vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
                vo.setFtd(rs.getLong("ftd"));
                vo.setRfd(rs.getLong("rfd"));
                vo.setFirstRemDepNum(rs.getLong("first_remarketing_dep_num"));
                vo.setHouseWin(rs.getLong("house_win"));
                vo.setFailedUsers(rs.getLong("failed_users"));
                list.add(vo);
            }

        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }

        return list;
    }
	  
	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	 * @param isCurrecnySplit TODO
	   * @return
	   * 	MarketingReport object
	   * @throws SQLException
	   */
	  private static MarketingReport getVO(ResultSet rs, boolean isCurrecnySplit) throws SQLException {

		  MarketingReport vo = new MarketingReport();
		  getBaseVO(rs,vo);

		  vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
		  vo.setClicksNum(rs.getLong("clicks_num"));
		  vo.setFirstRegDepositorsNum(rs.getLong("first_reg_depositers_num"));
		  vo.setFirstDepositorsNum(rs.getLong("first_depositers_num"));
		  vo.setFirstRemDepNum(rs.getLong("first_rem_depositors_num"));
		  vo.setLandingPageName(rs.getString("landing_page_name"));
		  vo.setSourceName(rs.getString("source_name"));
		  if (isCurrecnySplit){
			  if (CommonUtil.isHebrewSkin(vo.getSkinId())){
				  vo.setSumDeposits(rs.getLong("sumDepositsEt"));
				  vo.setTurnOver(rs.getLong("turnOverEt"));
				  vo.setHouseWin(rs.getLong("houseWinEt"));
			  }else {
				  vo.setSumDeposits(rs.getLong("sumDepositsAo"));
				  vo.setTurnOver(rs.getLong("turnOverAo"));
				  vo.setHouseWin(rs.getLong("houseWinAo"));
			}
		  }else{
			  vo.setSumDeposits(rs.getLong("sumDeposits"));
			  vo.setTurnOver(rs.getLong("turnOver"));
			  vo.setHouseWin(rs.getLong("houseWin"));
		  }


		  return vo;
	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingReport object
	   * @throws SQLException
	   */
	  private static void getBaseVO(ResultSet rs,MarketingReport vo) throws SQLException {
		  vo.setCombId(rs.getLong("comb_id"));
		  vo.setCampaignName(rs.getString("campaign_name"));
		  vo.setMedium(rs.getString("medium"));
		  vo.setContent(rs.getString("content"));
		  vo.setMarketSizeHorizontal(rs.getLong("market_size_horizontal"));
		  vo.setMarketSizeVertical(rs.getLong("market_size_vertical"));
		  vo.setMarketType(rs.getString("market_type"));
		  vo.setLocation(rs.getString("location"));
		  vo.setSkinId(rs.getLong("skin_id"));
	  }

	  /**
	   * getHouseWinReport data
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingReport
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingReport> getHouseWinReport(Connection con, long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, String offset,long sourceId, long landingPage) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();
		  String skinStr = null;


		  if (skinId != 0){
			  skinStr = " = " + skinId + " ";
		  }else{
			  skinStr = " in (select s.id from skins s where " + skinBusinessCaseId + " = s.business_case_id) ";
		  }


		  try {
				String sql = " SELECT " +
						            " distinct mcom.id  as comb_id, " +
								    " mca.name  campaign_name, " +
								    " mmed.name medium , " +
								    " mcon.name content, " +
								    " ms.size_horizontal market_size_horizontal, " +
								    " ms.size_vertical market_size_vertical, " +
								    " mt.name market_type, " +
								    " mloc.location location, " +
								    " mcom.skin_id skin_id," +
								    " sums_investments.head_count, " +
								    " sums_investments.house_win " +
							  " FROM " +
							  		" marketing_campaigns mca, " +
								    " marketing_mediums mmed, " +
								    " marketing_contents mcon, " +
								    " marketing_sources msource, " +
								    " marketing_combinations mcom " +
								    	" LEFT JOIN marketing_landing_pages mlp on mcom.landing_page_id = mlp.id " +
								    	" LEFT JOIN marketing_sizes ms on mcom.size_id = ms.id " +
								    	" LEFT JOIN marketing_locations mloc on mcom.location_id = mloc.id " +
								        " LEFT JOIN marketing_types mt on mcom.type_id = mt.id " +
                                        " LEFT JOIN " +
                                        	" (SELECT " +
                                        		" u.combination_id, " +
                                        		" count(distinct u.id) head_count, " +
                                        		" SUM(CASE WHEN u.skin_id = 1 THEN i.house_result " +
                                        									" ELSE i.house_result*i.rate END)/100 as house_win " +
                                             " FROM " +
                                            	" users u, " +
                                            	" investments i " +
                                             " WHERE  " +
                                            	" u.id = i.user_id  " +
                                                " AND u.class_id <> 0 " +
                                                " AND u.skin_id " + skinStr +
                                                " AND i.is_settled = 1 " +
                                                " AND i.is_canceled = 0 " +
                                                " AND i.time_settled  between ? and ? " +
                                            " GROUP BY u.combination_id) sums_investments on mcom.id = sums_investments.combination_id " +
							  " WHERE " +
							  		" mcom.campaign_id = mca.id " +
									" AND mcom.medium_id = mmed.id " +
									" AND mcom.content_id = mcon.id " +
									" AND msource.id = mca.source_id " +
									" AND mcom.skin_id " + skinStr;
				if (sourceId != 0 ){
					sql +=		    " AND mca.source_id = " + sourceId + " ";
				}
				if (landingPage != 0 ){
					sql +=		    " AND mcom.landing_page_id =  " + landingPage + " ";
				}
				sql +=	     " ORDER BY mcom.id ";

				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(startDate));
				ps.setTimestamp(2 , CommonUtil.convertToTimeStamp(endDate));

				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingReport vo = new MarketingReport();
					getBaseVO(rs, vo);

					vo.setHeadCount(rs.getLong("head_count"));
					vo.setHouseWin(rs.getLong("house_win"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	  /**
	   * Get all MarketingRepor data
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingReport
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingReport> getGoogleReport(Connection con, long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, String offset, int reportType, long sourceId, long marketingCampaignManager, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();

		  try {
				String sql ="Select * from table( MARKETING_REPORTS.GET_MARKETING_DP_REPORT(?, ?, ?, ?, ?) ) ";
			    
			    if(writerIdForSkin>0){
			    	sql+=" where skin_id IN ( select skin_id from writers_skin where writer_id= "+ writerIdForSkin +" ) ";
			    }

				ps = con.prepareStatement(sql);
				Timestamp startTs = CommonUtil.convertToTimeStamp(startDate);
				Timestamp endTs = CommonUtil.convertToTimeStamp(endDate);
				
				ps.setTimestamp(1, startTs);
				ps.setTimestamp(2, endTs);
				ps.setLong(3, skinId);
				ps.setLong(4, sourceId);
				ps.setLong(5, marketingCampaignManager);
 
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingReport vo = new MarketingReport();
					
					vo.setDynamicParam(rs.getString("dp"));
					vo.setCampaignManager(rs.getString("campaign_manager"));
					vo.setCombId(rs.getLong("combination_id"));
					vo.setCampaignName(rs.getString("campaign_name"));
					vo.setSourceName(rs.getString("source_name"));
					vo.setMedium(rs.getString("medium_name"));
					vo.setContent(rs.getString("content_name"));
					vo.setMarketSizeHorizontal(rs.getLong("marketing_size_horizontal"));
					vo.setMarketSizeVertical(rs.getLong("marketing_size_vertical"));
					vo.setLocation(rs.getString("m_location"));
					vo.setLandingPageName(rs.getString("landing_page_name"));
					vo.setSkinId(rs.getLong("skin_id"));
					vo.setShortReg(rs.getLong("short_reg"));
					vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
					vo.setFtd(rs.getLong("ftd"));
					vo.setRfd(rs.getLong("rfd"));
					vo.setFirstRemDepNum(rs.getLong("first_remarketing_dep_num"));
					vo.setHouseWin(rs.getLong("house_win"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

		  return list;
	  }
	  
	  public static ArrayList<MarketingReport> getMarketingHttpRefererReport(Connection con, 
				Date startDate,
				Date endDate,
			  	long skinId,
			  	long sourceId,
				long marketingCampaignManager,
				long writerIdForSkin
				) throws SQLException {

					PreparedStatement ps = null;
					ResultSet rs = null;
					ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();
					
					try {
					String sql ="Select * from table( MARKETING_REPORTS.GET_MARKETING_HTTP_REF_REPORT(?, ?, ?, ?, ?) ) ";
					
				    if(writerIdForSkin>0){
				    	sql+=" where skin_id IN ( select skin_id from writers_skin where writer_id= "+ writerIdForSkin +" ) ";
				    }
					
					ps = con.prepareStatement(sql);
					Timestamp startTs = CommonUtil.convertToTimeStamp(startDate);
					Timestamp endTs = CommonUtil.convertToTimeStamp(endDate);
					
					ps.setTimestamp(1, startTs);
					ps.setTimestamp(2, endTs);
					ps.setLong(3, skinId);
					ps.setLong(4, sourceId);
					ps.setLong(5, marketingCampaignManager);
					
					rs = ps.executeQuery();
					
					while (rs.next()) {
						MarketingReport vo = new MarketingReport();
						
						vo.setHttpReferer(rs.getString("http_referer"));
						vo.setDsq(rs.getString("dsq"));
						vo.setDynamicParam(rs.getString("dp"));
						vo.setCampaignManager(rs.getString("campaign_manager"));
						vo.setCombId(rs.getLong("combination_id"));
						vo.setCampaignName(rs.getString("campaign_name"));
						vo.setSourceName(rs.getString("source_name"));
						vo.setMedium(rs.getString("medium_name"));
						vo.setSkinId(rs.getLong("skin_id"));
						vo.setShortReg(rs.getLong("short_reg"));
						vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
						vo.setFtd(rs.getLong("ftd"));
						vo.setFirstRemDepNum(rs.getLong("first_remarketing_dep_num"));
						
						list.add(vo);
					}
					
					} finally {
					closeResultSet(rs);
					closeStatement(ps);
					}
					
					return list;
}
	  
	  
	  
	  
	  /**
	   * Get all RegLeadsTotals data
	   * @param con  db connection
	 * @param campaignId TODO
	 * @param combId TODO
	   * @return ArrayList of RegLeadsTotals
	   * @throws SQLException
	   */
	  public static ArrayList<RegLeadsTotals> getRegLeadsTotals(Connection con, long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, long campaignId, long combId, long writerIdForSkin) throws SQLException {

		  FacesContext context = FacesContext.getCurrentInstance();
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<RegLeadsTotals> list = new ArrayList<RegLeadsTotals>();

		  String combStr = "";
		  String campStr = "";
		  String skinStr = "";
		  String skinStrWrtierId="";
		  boolean isCheckSkin = false;
		  boolean isBusinessSkin;
		  long skinParam = 0;

		  if (skinId > 0 || skinBusinessCaseId > 0){
			  isCheckSkin = true;
			  isBusinessSkin = skinBusinessCaseId > 0;

			  if (skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER){
				  skinId = Skin.SKIN_ETRADER;
				  isBusinessSkin = false;
			  }

			  if (isBusinessSkin){
				  skinStr = " and mco.skin_id in (select s.id from skins s where ? = s.business_case_id) ";
				  skinParam = skinBusinessCaseId;
			  }else{
				  skinStr = " and mco.skin_id = ? ";
				  skinParam = skinId;
			  }
		  }
		  
		  if (writerIdForSkin > 0){
			  skinStrWrtierId = " and mco.skin_id in  ( select skin_id from writers_skin where writer_id= "+ writerIdForSkin +" ) ";
		  }

		  if (campaignId > 0){
			  campStr = " and mco.campaign_id = ? ";
		  }

		  if (combId > 0){
			  combStr = " and mco.id = ? ";
		  }

		  try {
			String sql =
				" select " +
					" A.campaign_id, " +
					" A.camp_name, " +
					" A.comb_id, " +
					" A.entry_type_id, " +
					" A.is_depositor, " +
					" sum(A.camp_entries) camp_entries, " +
					" sum(A.users_num) users_num, " +
					" sum(A.short_reg_num) short_reg_num, " +
					" sum(A.reg_short_reg_num) reg_short_reg_num, " +
					" sum(A.call_me_num) call_me_num, " +
					" sum(A.reg_call_me_num) reg_call_me_num, " +
					" sum(A.direct_reg_num) direct_reg_num, " +
					" sum(A.first_reg_dep) first_reg_dep " +
				" from ( " +
			        " select  " +
					  	" mco.campaign_id, " +
					  	" mca.name camp_name, " +
					  	" mco.id comb_id, " +
					    " count(*) camp_entries, " +
					    " count(u.id) users_num, " +
					    " pu.entry_type_id, " +
					    " (case when u.first_deposit_id is not null then 1 else 0 end) is_depositor, " +
					    " sum(case when c.type = " + Contact.CONTACT_US_SHORT_REG_FORM + " then 1 else 0 end) short_reg_num, " +
					    " sum(case when c.type = " + Contact.CONTACT_US_SHORT_REG_FORM + " and u.id is not null then 1 else 0 end) reg_short_reg_num, " +
					    " sum(case when c.type = " + Contact.CONTACT_ME_TYPE + " then 1 else 0 end) call_me_num, " +
					    " sum(case when c.type = " + Contact.CONTACT_ME_TYPE + " and u.id is not null then 1 else 0 end) reg_call_me_num, " +
					    " 0 direct_reg_num,  " +
					    " count(u.first_deposit_id) first_reg_dep " +
					" from " +
						" marketing_combinations mco, " +
						" marketing_campaigns mca, " +
					    " contacts c " +
					    	" left join users u on c.user_id = u.id " +
						    " left join population_users pu on ( c.user_id = 0 and pu.contact_id = c.id) " +
						    " left join population_users pur on ( c.user_id <> 0 and c.user_id = pur.user_id) " +
					" where  " +
					     " c.combination_id = mco.id " +
					     " and mco.campaign_id = mca.id " +
					     " and c.time_created between ? and ? " +
					     " and (u.id is null OR u.time_created > c.time_created) " +
					     " and c.type in (" + Contact.CONTACT_ME_TYPE + " , " + Contact.CONTACT_US_SHORT_REG_FORM + ") " +
					     skinStr +
					     skinStrWrtierId +
					     campStr +
					     combStr +
					" group by " +
					     " mco.campaign_id, " +
					     " mca.name, " +
					     " mco.id," +
					     " pu.entry_type_id," +
					     " case when u.first_deposit_id is not null then 1 else 0 end " +

					" union  " +

					" select  " +
						" mco.campaign_id, " +
						" mca.name camp_name, " +
					  	" mco.id comb_id, " +
						" count(*) camp_entries, " +
					    " count(u.id) users_num, " +
					    " pu.entry_type_id, " +
					    " case when u.first_deposit_id is not null then 1 else 0 end is_depositor, " +
					    " 0 short_reg_num, " +
					    " 0 reg_short_reg_num, " +
					    " 0 call_me_num, " +
					    " 0 reg_call_me_num, " +
					    " count(*) direct_reg_num,  " +
					    " count(u.first_deposit_id) first_reg_dep " +
					" from  " +
					    " marketing_campaigns mca, " +
					    " marketing_combinations mco, " +
					    " users u " +
					    	" left join population_users pu on pu.user_id = u.id " +
					" where  " +
					    " mco.id = u.combination_id " +
					    " and mco.campaign_id = mca.id " +
					    " and u.time_created between ? and ? " +
					    " and not exists(select * " +
					    			   " from contacts c " +
					    			   " where " +
					    			  	  " c.user_id = u.id " +
					    			  	  " and c.time_created between ? and u.time_created " +
					    			  	  " and c.type in (" + Contact.CONTACT_ME_TYPE + " , " + Contact.CONTACT_US_SHORT_REG_FORM + ")" +
					    			   " ) " +
					    " and u.class_id != 0 " +
					    " and u.is_active = 1 " +
					    skinStr +
					    skinStrWrtierId +
					    campStr +
					    combStr +
					" group by " +
					    " mco.campaign_id, " +
					    " mca.name, " +
					    " mco.id," +
					    " pu.entry_type_id, " +
					    " case when u.first_deposit_id is not null then 1 else 0 end " +
					" )A " +
				" group by " +
					" A.campaign_id, " +
					" A.camp_name, " +
					" A.comb_id," +
					" A.entry_type_id, " +
					" A.is_depositor " +
			    " order by " +
			    	" A.campaign_id ";

				ps = con.prepareStatement(sql);
				int paramIdx = 1;
				ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));
				ps.setTimestamp(paramIdx++ , CommonUtil.convertToTimeStamp(endDate));

				if (isCheckSkin){
					ps.setLong(paramIdx++, skinParam);
				}
				if (campaignId > 0){
					ps.setLong(paramIdx++, campaignId);
				}
				if (combId > 0){
					ps.setLong(paramIdx++, combId);
				}

				ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));
				ps.setTimestamp(paramIdx++ , CommonUtil.convertToTimeStamp(endDate));
				ps.setTimestamp(paramIdx++ , CommonUtil.convertToTimeStamp(startDate));

				if (isCheckSkin){
					ps.setLong(paramIdx++, skinParam);
				}
				if (campaignId > 0){
					ps.setLong(paramIdx++, campaignId);
				}
				if (combId > 0){
					ps.setLong(paramIdx++, combId);
				}

				rs = ps.executeQuery();

				long tempCampId = 0;
				long currCampId = 0;
				long tempCombId = 0;
				long currCombId = 0;
				int currEntryTypeId = 0;
				RegLeadsTotals totals = null;
				RegLeadsTotals vo = null;

				while (rs.next()) {
					currCampId = rs.getLong("campaign_id");
					currCombId = rs.getLong("comb_id");
					currEntryTypeId = rs.getInt("entry_type_id");

					if (currCombId != tempCombId){
						tempCombId = currCombId;

						if (currCampId != tempCampId){
							tempCampId = currCampId;

							totals = new RegLeadsTotals();
							list.add(totals);
							totals.setCampaignId(currCampId);
							totals.setCampaignName(rs.getString("camp_name"));
						}

						vo = new RegLeadsTotals();
						vo.setCombId(currCombId);
						vo.setCampaignId(0);
						vo.setCampaignName("");
						list.add(vo);


					}

					int regUsersNum = rs.getInt("users_num");
					int callMeNum = rs.getInt("call_me_num");
					int regCallMeNum = rs.getInt("reg_call_me_num");
					int shortRegNum = rs.getInt("short_reg_num");
					int regShortRegNum = rs.getInt("reg_short_reg_num");

					// Only for display entry types names on screen
					if (context != null){
						vo.getCallMeEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(callMeNum);
						vo.getRegCallMeEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regCallMeNum);
						vo.getShortRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(shortRegNum);
						vo.getRegShortRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regShortRegNum);
						vo.getTotalRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regUsersNum);

						totals.getCallMeEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(callMeNum);
						totals.getRegCallMeEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regCallMeNum);
						totals.getShortRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(shortRegNum);
						totals.getRegShortRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regShortRegNum);
						totals.getTotalRegEntryTypesDiv().get(currEntryTypeId).addEntryTypesLeadsNum(regUsersNum);

						if (rs.getInt("is_depositor")==1){
							vo.getTotalRegEntryTypesDiv().get(currEntryTypeId).setDepositorsNum(regUsersNum);
							totals.getTotalRegEntryTypesDiv().get(currEntryTypeId).addDepositorsNum(regUsersNum);
						}else{
							vo.getTotalRegEntryTypesDiv().get(currEntryTypeId).setNonDepositorsNum(regUsersNum);
							totals.getTotalRegEntryTypesDiv().get(currEntryTypeId).addNonDepositorsNum(regUsersNum);
						}
					}

					vo.setCampaignEntries(vo.getCampaignEntries() + rs.getLong("camp_entries"));
					vo.setCallMeNum(vo.getCallMeNum() + callMeNum);
					vo.setRegCallMeNum(vo.getRegCallMeNum() + regCallMeNum);
					vo.setShortRegMum(vo.getShortRegMum() + shortRegNum);
					vo.setRegShortRegNum(vo.getRegShortRegNum() + regShortRegNum);
					vo.setDirectRegNum(vo.getDirectRegNum() + rs.getLong("direct_reg_num"));
					vo.setTotalRegNum(vo.getTotalRegNum() + regUsersNum);
					vo.setFirstRegDep(vo.getFirstRegDep() + rs.getLong("first_reg_dep"));

					totals.setCampaignEntries(totals.getCampaignEntries() + rs.getLong("camp_entries"));
					totals.setCallMeNum(totals.getCallMeNum() + callMeNum);
					totals.setRegCallMeNum(totals.getRegCallMeNum() + regCallMeNum);
					totals.setShortRegMum(totals.getShortRegMum() + shortRegNum);
					totals.setRegShortRegNum(totals.getRegShortRegNum() + regShortRegNum);
					totals.setDirectRegNum(totals.getDirectRegNum() + rs.getLong("direct_reg_num"));
					totals.setTotalRegNum(totals.getTotalRegNum() + regUsersNum);
					totals.setFirstRegDep(totals.getFirstRegDep() + rs.getLong("first_reg_dep"));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

		  return list;
	  }

	  /**
	   * Get all RegLeadsTotals data
	   * @param con  db connection
	   * @param campaignId
	   * @param combId
	   * @param regLeadsType - type of users
	   * @param isForReport
	   * @return ArrayList of RegLeadsTotals
	   * @throws SQLException
	   */
	  public static ArrayList<RegLeadsDetails> getRegLeadsDetails(Connection con, long skinId, long skinBusinessCaseId,
			  											Date startDate, Date endDate, long campaignId, long combId, int regLeadsType, boolean isForReport) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<RegLeadsDetails> list = new ArrayList<RegLeadsDetails>();

		  String condStr1 = "";
		  String condStr2 = "";
		  String combStr = "";
		  String campStr = "";
		  String skinStr = "";
		  boolean isCheckSkin = false;
		  boolean isBusinessSkin;
		  long skinParam = 0;

		  switch (regLeadsType) {
			case Constants.REG_LEADS_TYPE_CALL_ME:
				condStr1 = " and c.type = " + Contact.CONTACT_ME_TYPE + " ";
				condStr2 = " and 0=1 ";
				break;
			case Constants.REG_LEADS_TYPE_REG_CALL_ME:
				condStr1 = " and c.type = " + Contact.CONTACT_ME_TYPE + " and u.id is not null ";
				condStr2 = " and 0=1 ";
				break;
			case Constants.REG_LEADS_TYPE_SHORT_REG:
				condStr1 = " and c.type = " + Contact.CONTACT_US_SHORT_REG_FORM + " ";
				condStr2 = " and 0=1 ";
				break;
			case Constants.REG_LEADS_TYPE_REG_SHORT_REG:
				condStr1 = " and c.type = " + Contact.CONTACT_ME_TYPE + " and u.id is not null ";
				condStr2 = " and 0=1";
				break;
			case Constants.REG_LEADS_TYPE_DIRECT_REG:
				condStr1 = " and 0=1 ";
				break;
			case Constants.REG_LEADS_TYPE_USERS_NUM:
				condStr1 = " and u.id is not null ";
				break;
			case Constants.REG_LEADS_TYPE_FIRST_REG_DEP:
				condStr1 = " and u.first_deposit_id is not null ";
				condStr2 = " and u.first_deposit_id is not null ";
				break;
			default:
				break;
		  }


		  if (skinId > 0 || skinBusinessCaseId > 0){
			  isCheckSkin = true;
			  isBusinessSkin = skinBusinessCaseId > 0;

			  if (skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER){
				  skinId = Skin.SKIN_ETRADER;
				  isBusinessSkin = false;
			  }

			  if (isBusinessSkin){
				  skinStr = " and mco.skin_id in (select s.id from skins s where ? = s.business_case_id) ";
				  skinParam = skinBusinessCaseId;
			  }else{
				  skinStr = " and mco.skin_id = ? ";
				  skinParam = skinId;
			  }
		  }

		  if (campaignId > 0){
			  campStr = " and mco.campaign_id = ? ";
		  }

		  if (combId > 0){
			  combStr = " and mco.id = ? ";
		  }


		  try {
			String sql =
				" select " +
					" A.campaign_id, " +
					" A.camp_name, " +
					" A.comb_id, " +
					" A.user_id, " +
					" A.contact_id, " +
					" A.time_created, " +
					" A.entry_type_id, " +
					" pop_iat.name entry_type_action_key, " +
					" A.original_pop,  " +
					" case when A.first_deposit_id is not null then 1 else 0 end is_depositor, " +
					" case when ti.transaction_id is not null then 1 else 0 end is_sales_deposit, " +
					" ti_w.user_name sales_rep_name ";
			if (isForReport){
				sql +=
					" , " +
					" A.issue_id, " +
					" A.issue_pop_entry_id, " +
					" curr_p.name curr_pop, " +
					" ia.id action_id, " +
		            " ia.action_time, " +
		            " ic.name channel_name, " +
		            " wd.dept_name rep_dep, " +
		            " w.user_name rep_name, " +
		            " A.subject_id, " +
		            " isu.name subject_name, " +
		            " i_p.name issue_pop_name, " +
		            " iat.name action_type_key, " +
		            " irs.name reached_status_key, " +
		            " ia.comments ";
			}
			sql +=
				" from ( " +
			        " select  " +
					  	" mco.campaign_id, " +
					  	" mca.name camp_name, " +
					  	" mco.id comb_id, " +
					    " c.user_id, " +
					    " c.id contact_id, " +
					    " c.time_created, " +
					    " pu.entry_type_id, " +
					    " pu.entry_type_action_id, " +
					    " p.name original_pop, " +
					    " pu.curr_population_entry_id, " +
					    " u.first_deposit_id first_deposit_id ";
			if (isForReport){
				sql +=  " ," +
					    " i.id issue_id, " +
					    " i.subject_id, " +
					    " i.population_entry_id issue_pop_entry_id ";
			}
			sql +=  " from " +
						" marketing_combinations mco, " +
						" marketing_campaigns mca, " +
					    " contacts c " +
					    	" left join users u on c.user_id = u.id " +
					    	" left join population_users pu on c.user_id = pu.user_id or (c.user_id = 0 and pu.contact_id = c.id) " +
						        " left join population_entries_hist peh on peh.id = pu.first_his_id " +
						           " left join population_entries pe on peh.population_entry_id = pe.id " +
						               " left join populations p on pe.population_id = p.id ";
			if (isForReport){
				sql +=
					        " left join population_entries i_pe on pu.id = i_pe.population_users_id " +
                               	  " left join issues i on i.population_entry_id = i_pe.id ";
			}
			sql +=	" where  " +
					     " c.combination_id = mco.id " +
					     " and mco.campaign_id = mca.id " +
					     " and c.time_created between ? and ? " +
					     " and (u.id is null OR u.time_created > c.time_created) " +
					     " and c.type in (" + Contact.CONTACT_ME_TYPE + " , " + Contact.CONTACT_US_SHORT_REG_FORM + ") " +
					     condStr1 +
					     skinStr +
					     campStr +
					     combStr +

					" union  " +

					" select  " +
						" mco.campaign_id, " +
						" mca.name camp_name, " +
						" mco.id comb_id, " +
						" u.id user_id, " +
						" u.contact_id, " +
						" u.time_created, " +
					    " pu.entry_type_id, " +
					    " pu.entry_type_action_id, " +
					    " p.name original_pop, " +
					    " pu.curr_population_entry_id, " +
					    " u.first_deposit_id ";
			if (isForReport){
				sql +=  " ," +
					    " i.id issue_id, " +
					    " i.subject_id, " +
					    " i.population_entry_id issue_pop_entry_id ";
			}
			sql +=  " from  " +
						" marketing_campaigns mca, " +
					    " marketing_combinations mco, " +
					    " users u " +
					    	" left join population_users pu on pu.user_id = u.id " +
						        " left join population_entries_hist peh on peh.id = pu.first_his_id " +
						           " left join population_entries pe on peh.population_entry_id = pe.id " +
						               " left join populations p on pe.population_id = p.id ";
			if (isForReport){
				sql +=		" left join issues i on i.user_id = u.id ";
			}
			sql +=  " where  " +
					    " mco.id = u.combination_id " +
					    " and mco.campaign_id = mca.id " +
					    " and u.time_created between ? and ? " +
					    " and not exists(select * " +
					    			   " from contacts c " +
					    			   " where " +
					    			  	  " c.user_id = u.id " +
					    			  	  " and c.time_created between ? and u.time_created " +
					    			  	  " and c.type in (" + Contact.CONTACT_ME_TYPE + " , " + Contact.CONTACT_US_SHORT_REG_FORM + ")" +
					    			   " ) " +
					    " and u.class_id != 0 " +
					    " and u.is_active = 1 " +
					    condStr2 +
					    skinStr +
					    campStr +
					    combStr +
					" )A " +
		        		" left join issue_actions pop_ia on pop_ia.id = A.entry_type_action_id " +
		        			" left join issue_action_types pop_iat on pop_iat.id = pop_ia.issue_action_type_id " +
		        		" left join transactions t on t.id = A.first_deposit_id " +
		        		   " left join transactions_issues ti on ti.transaction_id = t.id " +
			        	      " left join issue_actions ti_ia on ti_ia.id = ti.issue_action_id " +
			        	         " left join writers ti_w on ti_w.id = ti_ia.writer_id ";

			if (isForReport){
				sql +=
			        	   " left join issue_subjects isu on isu.id = A.subject_id " +
			        	   " left join issue_actions ia on A.issue_id = ia.issue_id " +
			               " left join writers w on w.id = ia.writer_id " +
			               	  " left join departments wd on wd.id = w.dept_id " +
			               " left join issue_action_types iat on iat.id = ia.issue_action_type_id " +
			                  " left join issue_channels ic on ic.id = iat.channel_id " +
			               	  " left join issue_reached_statuses irs on irs.id = iat.reached_status_id " +
			            " left join population_entries i_pe on i_pe.id = A.issue_pop_entry_id " +
			               " left join populations i_p on i_pe.population_id = i_p.id " +
			            " left join population_entries curr_pe on A.curr_population_entry_id = curr_pe.id " +
			               " left join populations curr_p on curr_pe.population_id = curr_p.id ";
			}

			sql +=
			    " order by " +
			    	" A.campaign_id ";
			if (isForReport){
				sql +=
			    	" , " +
			    	" A.user_id, " +
		            " A.contact_id, " +
		            " ia.action_time ";
			}

			ps = con.prepareStatement(sql);
			int paramIdx = 1;
			ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));
			ps.setTimestamp(paramIdx++ , CommonUtil.convertToTimeStamp(endDate));

			if (isCheckSkin){
				ps.setLong(paramIdx++, skinParam);
			}
			if (campaignId > 0){
				ps.setLong(paramIdx++, campaignId);
			}
			if (combId > 0){
				ps.setLong(paramIdx++, combId);
			}

			ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));
			ps.setTimestamp(paramIdx++ , CommonUtil.convertToTimeStamp(endDate));
			ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));

			if (isCheckSkin){
				ps.setLong(paramIdx++, skinParam);
			}
			if (campaignId > 0){
				ps.setLong(paramIdx++, campaignId);
			}
			if (combId > 0){
				ps.setLong(paramIdx++, combId);
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				RegLeadsDetails vo = new RegLeadsDetails();
				vo.setCampaignId(rs.getLong("campaign_id"));
				vo.setCampaignName(rs.getString("camp_name"));
				vo.setCombId(rs.getLong("comb_id"));
				vo.setEntryType(rs.getInt("entry_type_id"));
				vo.setEntryTypeActionType(rs.getString("entry_type_action_key"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setContactId(rs.getLong("contact_id"));
				vo.setDepositior(rs.getInt("is_depositor")==1?true:false);
				vo.setSalesDep(rs.getInt("is_sales_deposit")==1?true:false);
				vo.setSalesDepRepName(rs.getString("sales_rep_name"));

				if (isForReport){
					vo.setTimeCreated(rs.getString("time_created"));
					vo.setActionTime(rs.getString("action_time"));
					vo.setOriginalPop(rs.getString("original_pop"));
					vo.setCurrentPop(rs.getString("curr_pop"));
					vo.setChannel(rs.getString("channel_name"));
					vo.setDepartment(rs.getString("rep_dep"));
					vo.setRepName(rs.getString("rep_name"));
					vo.setReachedStatus(rs.getString("reached_status_key"));
					vo.setActionType(rs.getString("action_type_key"));
					vo.setComments(rs.getString("comments"));
					vo.setIssueActionId(rs.getLong("action_id"));

					long subjectId = rs.getLong("subject_id");

					if (subjectId == IssuesManagerBase.ISSUE_SUBJ_POPULATION){
						vo.setSubject(rs.getString("issue_pop_name"));
					}else if(subjectId == 0){
						vo.setSubject("");
					}else{
						vo.setSubject(CommonUtil.getMessage(rs.getString("subject_name"),null));
					}
				}

				list.add(vo);


			}

		  } finally {
				closeResultSet(rs);
				closeStatement(ps);
		  }

		  return list;
	 }
	  
	  
	  public static ArrayList<RemarketingReport> getRemarketingReportDetails(Connection con,Date startDate, Date endDate, long skinId, long skinBusinessCaseId,long sourceId, long writerIdForSkin, long marketingCampaignManager) throws SQLException{
		  
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<RemarketingReport> list = new ArrayList<RemarketingReport>();
		  String skinStr = null;
		  Long skinParam = null;
		  boolean isBusinessSkin = skinBusinessCaseId > 0;
		  String skinStrWrtierId = "";
		  
		  if (isBusinessSkin){
			  skinStr = "AND mcom.skin_id in (select s.id from skins s where s.business_case_id = ?) ";
			  skinParam = skinBusinessCaseId;
		  }else{
			  skinStr = "AND decode(?,0,0,mcom.skin_id) = ? ";
			  skinParam = skinId;
		  }
		  
		  if (writerIdForSkin > 0){
			  skinStrWrtierId = " and mcom.skin_id in  ( select skin_id from writers_skin where writer_id= "+ writerIdForSkin +" ) ";
		  }
		  
		String sql = "SELECT "
				+ " ff.u_com as First_comb "
				+ ", ff.u_dp as First_DP "
				+ ", ff.u_source as First_source "
				+ ", ff.combination_id as Remarketing_comb "
				+ ", ff.dynamic_parameter as Remarketing_DP "
				+ ", ff.name as Remarketing_source "
				+ ", count(ff.id) as Num_of_FTD "
				+ "	FROM  "
				+ "(SELECT  DISTINCT " 
				+ " (u.combination_id) as u_com, u.dynamic_param as u_dp, ms.name as u_source, re_log.combination_id, re_log.dynamic_parameter, re_log.name, u.id "
				+ "FROM "
				+ " users u, "
				+ "transactions t, "
				+ "marketing_sources ms, "
				+ "marketing_campaigns mca, "
				+ "marketing_combinations mcom, "
				+ "(SELECT " 
				+  "rl.id, rl.user_id, rl.combination_id, rl.dynamic_parameter, ms.name "
				+ "FROM marketing_combinations mcomb, marketing_campaigns mc, marketing_sources ms, remarketing_logins rl, "
				+ "(SELECT " 
				+"rl.user_id, " 
				+ "max(rl.id) as max "
				+"FROM  "
				+ "   remarketing_logins rl " 
				+"WHERE  "
				+ "   rl.user_id is not null " 
				+"group by rl.user_id) aa "
				+ "WHERE aa.max=rl.id " 
				+"and rl.combination_id=mcomb.id "
				+ "AND mcomb.campaign_id=mc.id "
				+ "AND mc.source_id=ms.id) re_log "
				+ "WHERE re_log.user_id=u.id "
				+ "AND u.combination_id=mcom.id "
				+ "AND mcom.campaign_id=mca.id " 
				+ "AND mca.source_id=ms.id "
				+ "AND u.first_deposit_id=t.id "
				+ "AND u.first_deposit_id is not null " 
				+ "AND u.class_id<>0  ";
                       if (sourceId != 0 ){
       					sql += "AND ms.id = " + sourceId + " ";
       				}
                        sql += skinStr +
                         skinStrWrtierId ;
                         if(marketingCampaignManager != 0) {
                        	 sql += " AND mca.campaign_manager = ? ";
                         }
                         sql += "AND trunc(t.time_created) between ? and ?)ff "
                +" GROUP BY ff.u_com , ff.u_dp,ff.u_source , ff.combination_id , ff.dynamic_parameter,ff.name  ";
    
		  
		  ps = con.prepareStatement(sql);
		  
		  int paramIdx = 1;
		  ps.setLong(paramIdx++, skinParam);
		  if(!isBusinessSkin) {
		  ps.setLong(paramIdx++, skinParam);}
		  if(marketingCampaignManager != 0) {
		  ps.setLong(paramIdx++, marketingCampaignManager);
		  }
		  ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(startDate));
		  ps.setTimestamp(paramIdx++, CommonUtil.convertToTimeStamp(endDate));
          
		  rs = ps.executeQuery();
		  
		  while (rs.next()) {
			  
			  RemarketingReport vo = new RemarketingReport();
			  vo.setFirstComb(rs.getLong("first_comb"));
			  vo.setFirstDP(rs.getString("first_dp"));
			  vo.setFirstSource(rs.getString("first_source"));
			  vo.setRemarketingComb(rs.getLong("remarketing_comb"));
			  vo.setRemarketingDP(rs.getString("remarketing_dp"));
			  vo.setRemarketingSource(rs.getString("remarketing_source"));
			  vo.setNumOfFTD(rs.getLong("num_of_ftd"));
			  list.add(vo);
			    
		  }
		   
		   for (RemarketingReport row : list) {
			   System.out.println(row.getFirstComb());
		   }
				closeResultSet(rs);
				closeStatement(ps);
		  
		  return list;
		  
	  }
	  
}

