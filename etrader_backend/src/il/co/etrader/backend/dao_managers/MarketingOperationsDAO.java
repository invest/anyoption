package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.anyoption.common.daos.DAOBase;

public class MarketingOperationsDAO extends DAOBase {

	
	public static boolean createMarketingOperations( Connection con, String name, Double duration, Date created ) throws SQLException{

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_operations m (m.id, m.name, m.time_created, m.duration_days) " +
						"values ( SEQ_MAR_OPERATIONS.NEXTVAL, ?, ?, ?)";
				ps = con.prepareStatement(sql);
				
				ps.setString(1, name);
				ps.setTimestamp(2, new Timestamp(created.getTime()));
				ps.setDouble(3, duration);
				
				int k = ps.executeUpdate();

		  }  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
		  return true;
	}
}
