package il.co.etrader.backend.service.requests;

import java.util.List;

import com.anyoption.common.service.requests.MethodRequest;

import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;

/**
 * @author kiril.mutafchiev
 */
public class UpdateLoginProductsMethodRequest extends MethodRequest {

	private List<Long> skinIds;
	private LoginProductEnum loginProduct;
	private boolean enabled;

	public List<Long> getSkinIds() {
		return skinIds;
	}

	public void setSkinIds(List<Long> skinIds) {
		this.skinIds = skinIds;
	}

	public LoginProductEnum getLoginProduct() {
		return loginProduct;
	}

	public void setLoginProduct(LoginProductEnum loginProduct) {
		this.loginProduct = loginProduct;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "UpdateLoginProductsMethodRequest: " + ls
				+ super.toString()
				+ "skinIds: " + skinIds + ls
				+ "loginProduct: " + loginProduct + ls
				+ "enabled: " + enabled + ls;
	}
}