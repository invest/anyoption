package il.co.etrader.backend.service.access;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.service.CommonJSONService;

import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.SalesDepositsManager;
import il.co.etrader.backend.bl_vos.Representative;
import il.co.etrader.backend.bl_vos.SalesDepositsInfo;
import il.co.etrader.backend.bl_vos.SalesDepositsSummary;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.SalesDepositsConversionSender;
import il.co.etrader.backend.helper.SalesDepositsRetentionSender;
import il.co.etrader.backend.service.requests.SalesDepositsMethodRequest;
import il.co.etrader.backend.service.requests.WriterMethodRequest;
import il.co.etrader.backend.service.results.SalesDepositsMethodResult;
import il.co.etrader.backend.service.results.WriterMethodResult;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * 
 * @author Ivan Petkov
 *
 */

public class SalesDeposits {
	
	private static final Logger log = Logger.getLogger(SalesDeposits.class);
	
	public static SalesDepositsMethodResult getDepositsConversion(SalesDepositsMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getDepositsConversion" + request);
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long beginTime = System.currentTimeMillis();
		SalesDepositsMethodResult result = new SalesDepositsMethodResult();
		
		if (!httpRequest.isUserInRole("retentionM") && !httpRequest.isUserInRole("retention")) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_PERMISSIONS);
			return result;
		}
		long writerId = request.getWriterId();
		long skinId = request.getSkinId();
		String from = request.getStartDate();
		String to = request.getEndDate();
		Date startDate = new Date();
		Date endDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm z");
			try {
				startDate = formatter.parse(from);
			} catch (ParseException e) {
				log.error("Unable to parse date =" + from, e);
			}
			try {
				endDate = formatter.parse(to);
			} catch (ParseException e) {
				log.error("Unable to parse date =" + to, e);
			}
		if (startDate.getTime() > endDate.getTime()) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_END_DATE_EARLIER_START_DATE);
			return result;
		}
		if ((endDate.getTime() - startDate.getTime())/(1000 * 60 * 60 * 24) > 32) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return result;
		}
		
		try {
			if (request.isExportToExcel()) {
				Writer writer = writerWrapper.getWriter();
				SalesDepositsInfo sdi = new SalesDepositsInfo();
				sdi.setWriterId(writerId);
				sdi.setStartDate(startDate);
				sdi.setEndDate(endDate);
				sdi.setUserId(request.getUserId());
				sdi.setWriter(writer);
				sdi.setSkinId(skinId);
				SalesDepositsConversionSender sender = new SalesDepositsConversionSender(sdi);
				sender.start();
			} else {
				ArrayList<SalesDepositsSummary> summary = new ArrayList<SalesDepositsSummary>();
				summary = SalesDepositsManager.getDepositsConversionPage(request.getUserId(), startDate, endDate, writerId, request.getPageNo(), request.getResultsPerPage(), skinId);
				long pageCount = getPageCount(summary, request.getResultsPerPage());
				result.setDepositSummary(summary);
				result.setPageCount(pageCount);
			}
			
		} catch (Exception e) {
			log.error("Cannot get conversion deposits " + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getDepositsConversion " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static SalesDepositsMethodResult getDepositsRetention(SalesDepositsMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getDepositsRetention" + request);
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long beginTime = System.currentTimeMillis();
		SalesDepositsMethodResult result = new SalesDepositsMethodResult();
		
		if (!httpRequest.isUserInRole("retentionM") && !httpRequest.isUserInRole("retention")) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_PERMISSIONS);
			return result;
		}
		long writerId = request.getWriterId();
		long skinId = request.getSkinId();
		String from = request.getStartDate();
		String to = request.getEndDate();
		Date startDate = new Date();
		Date endDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm z");
			try {
				startDate = formatter.parse(from);
			} catch (ParseException e) {
				log.error("Unable to parse date =" + from, e);
			}
			try {
				endDate = formatter.parse(to);
			} catch (ParseException e) {
				log.error("Unable to parse date =" + to, e);
			}
		if (startDate.getTime() > endDate.getTime()) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_END_DATE_EARLIER_START_DATE);
			return result;
		}
		if ((endDate.getTime() - startDate.getTime())/(1000 * 60 * 60 * 24) > 32) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return result;
		}
		
		try {
			if (request.isExportToExcel()) {
				Writer writer = writerWrapper.getWriter();
				SalesDepositsInfo sdi = new SalesDepositsInfo();
				sdi.setWriterId(writerId);
				sdi.setStartDate(startDate);
				sdi.setEndDate(endDate);
				sdi.setUserId(request.getUserId());
				sdi.setWriter(writer);
				sdi.setSkinId(skinId);
				SalesDepositsRetentionSender sender = new SalesDepositsRetentionSender(sdi);
				sender.start();
			} else {
				ArrayList<SalesDepositsSummary> summary = new ArrayList<SalesDepositsSummary>();
				summary = SalesDepositsManager.getDepositsRetentionPage(request.getUserId(), startDate, endDate, writerId, request.getPageNo(), request.getResultsPerPage(), skinId);
				for(SalesDepositsSummary sum : summary) {
					sum.setUserRank(CommonUtil.getMessage(sum.getUserRank(), null));
					sum.setUserStatus(CommonUtil.getMessage(sum.getUserStatus(), null));
				}
				long pageCount = getPageCount(summary, request.getResultsPerPage());
				result.setDepositSummary(summary);
				result.setPageCount(pageCount);
			}
		} catch (Exception e) {
			log.error("Cannot get retention deposits " + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getDepositsRetention " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static long getPageCount(ArrayList<SalesDepositsSummary> summary, long resultsPerPage) {
		long pageCount = 0L;
		if (!summary.isEmpty()) {
			pageCount = summary.get(0).getTotalCount();
			if (pageCount > resultsPerPage) {
				pageCount = (pageCount/resultsPerPage) + ((pageCount%resultsPerPage) == 0 ? 0 : 1);
			} else {
				pageCount = 1L;
			}
		}
		return pageCount;
	}

	public static WriterMethodResult getWriter(WriterMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getWriter" + request);
		long beginTime = System.currentTimeMillis();
		WriterMethodResult result = new WriterMethodResult();
		List<String> permissions = new ArrayList<String>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		long salesType = writer.getWriter().getSales_type();
		if (httpRequest.isUserInRole("retentionM")) {
			permissions.add("sales_deposit_rep_filter");
			permissions.add("sales_deposit_deposits_conversion");
			permissions.add("sales_deposit_deposits_retention");
		}
		if (httpRequest.isUserInRole("retention") && (salesType == ConstantsBase.SALES_TYPE_CONVERSION || salesType == ConstantsBase.SALES_TYPE_BOTH)) {
			permissions.add("sales_deposit_deposits_conversion");
		}
		if (httpRequest.isUserInRole("retention") && (salesType == ConstantsBase.SALES_TYPE_RETENTION || salesType == ConstantsBase.SALES_TYPE_BOTH)) {
			permissions.add("sales_deposit_deposits_retention");
		}
		String offset = ((WriterWrapper)httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER)).getUtcOffset();
		result.setPermissions(permissions);
		result.setOffset(offset);
		result.setWriterId(writerId);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getWriter " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static WriterMethodResult getWriterConversionRepList(WriterMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getWriterConversionRepList" + request);
		long beginTime = System.currentTimeMillis();
		WriterMethodResult result = new WriterMethodResult();
		HashMap<Long, Writer> writerReps = new HashMap<Long, Writer>();
		List<Representative> list = new ArrayList<Representative>();
		try {
			writerReps = PopulationsManager.getSalesWritersConversionSalesType();
			list = getRepList(list, writerReps);
		} catch (SQLException e) {
			log.error("Cannot get conversion reps" + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		result.setConversionReps(list);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getWriterConversionRepList " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static WriterMethodResult getWriterRetentionRepList(WriterMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getWriterRetentionRepList" + request);
		long beginTime = System.currentTimeMillis();
		WriterMethodResult result = new WriterMethodResult();
		HashMap<Long, Writer> writerReps = new HashMap<Long, Writer>();
		List<Representative> list = new ArrayList<Representative>();
		try {
			writerReps = PopulationsManager.getSalesWritersRetentionSalesType();
			list = getRepList(list, writerReps);
		} catch (SQLException e) {
			log.error("Cannot get retention reps" + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		result.setRetentionReps(list);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getWriterRetentionRepList " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static List<Representative> getRepList(List<Representative> list, HashMap<Long, Writer> writerReps) {
		Iterator it = writerReps.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        Representative rep = new Representative();
	        rep.setRepId((long)pair.getKey());
	        rep.setRepName((String)writerReps.get(pair.getKey()).getUserName());
	        list.add(rep);
	    }
	    Collections.sort(list, (p1, p2) -> p1.getRepName().compareTo(p2.getRepName()));
	    return list;
	}
}
