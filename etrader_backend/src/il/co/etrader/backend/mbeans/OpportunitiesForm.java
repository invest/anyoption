package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.OpportunitiesManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;

public class OpportunitiesForm implements Serializable {

	/**
	 *
	 */
    private static final long serialVersionUID = 8212134360946284955L;

    private static final Logger log = Logger.getLogger(OpportunitiesForm.class);

    private Date from;
    private ArrayList list;
    // private ArrayList investmentsList;
    private String publishedSettled; // 0 - published , 1 - settled, 2 - not published
    private String marketGroup;
    private Opportunity opportunity;
    
    private String sortColumn;
    private String prevSortColumn;
    private boolean sortAscending = false;
    private String navigation;
    private String password;
    private long marketTypeId;
    private long classId;
    private long statusId;
    private long opportunityId;
    private long scheduled;
    private int suspended; // 0 - not suspended, 1 - suspended

    private boolean isPublished;
    private String upDown ="";
    private List<SelectItem> oddsGroupList;
    private List<SelectItem> oneTouchOddsTypesSI;

	public OpportunitiesForm() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		from = gc.getTime();
		opportunityId = 0;
		publishedSettled = "0";
		marketGroup="0,0";
		sortColumn="";
		prevSortColumn="";
		marketTypeId = 0;  // take all market types
		scheduled = 0;
		suspended = 3;	// all, suspended and not opportunities
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE; //take opps with investments by company or private
		statusId= ConstantsBase.INVESTMENT_CLASS_STATUS_ACTIVE;	//take opps with non cancelled investments
		updateList();
		createOddsGroupList();
		createOneTouchOddsTypesSI();
	}

	public void updateChange(ValueChangeEvent event) throws SQLException{
    	publishedSettled=(String)event.getNewValue();
    	updateList();
    }
    public void updateMarketChange(ValueChangeEvent event) throws SQLException{
    	marketGroup = (String)event.getNewValue();
    	updateList();
    }

    public void updateScheduledIdChange(ValueChangeEvent event) throws SQLException{
    	scheduled = (Long)event.getNewValue();
    	updateList();
    }

	public int getInvListSize() {
		FacesContext context = FacesContext.getCurrentInstance();
		ArrayList invList= (ArrayList)context.getApplication().createValueBinding(Constants.BIND_CANCELED_INVESTMENTS_LIST).getValue(context);

		if (invList!=null) {
			return invList.size();
		}
		return 0;
	}

	public String executeCancel() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		String remoteUser = context.getExternalContext().getRemoteUser();

		if (!UsersManager.getWriterPassword(remoteUser).equalsIgnoreCase(password)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("opportunities.cancel.wrong.password", null, Utils.getWriterLocale(context)),null);
			context.addMessage("cancelInvForm:password", fm);
			return null;
		}

		ArrayList invList= (ArrayList)context.getApplication().createValueBinding(Constants.BIND_CANCELED_INVESTMENTS_LIST).getValue(context);

		InvestmentsManager.cancelAllInvestments(invList);

		return Constants.NAV_EMPTY;
	}

	public void cancelInvestments(ActionEvent event) throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();

		ArrayList investmentsList=InvestmentsManager.getAllInvestmentsByOppId(opportunity.getId());
		for (int i=0;i<investmentsList.size();i++) {
			Investment inv=(Investment)investmentsList.get(i);
			inv.setMarketId(opportunity.getMarketId());
		}
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_CANCELED_INVESTMENTS_LIST, ArrayList.class);
		ve.getValue(context.getELContext());
		ve.setValue(context.getELContext(), investmentsList);

		final String viewId = Constants.NAV_CANCEL_INVESTMENTS;

		// This is the proper way to get the view's url
		ViewHandler viewHandler = context.getApplication().getViewHandler();
		String actionUrl = viewHandler.getActionURL(context, viewId);

		String javaScriptText = "window.open('" + actionUrl + "', 'popupWindow', 'menubar=no,resizable=no,location=no,toolbar=no,width=500,height=500');";

		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(context);
		addResource.addInlineScriptAtPosition(context, AddResource.HEADER_BEGIN, javaScriptText);
	}

	public boolean getHasInvestments() throws SQLException{

		if (opportunity.getId()>0) {
			return InvestmentsManager.hasNonCanceledInvestmentsByOppId(opportunity.getId());
		}
		return false;
	}

    public String sortList() {
	if (sortColumn.equals(prevSortColumn)) {
	    sortAscending = !sortAscending;
	} else {
	    sortAscending = true;
	}
	prevSortColumn = sortColumn;
	if (sortColumn.equals("id"))
	    Collections.sort(list, new IdComparator(sortAscending));
	if (sortColumn.equals("groupName"))
	    Collections.sort(list, new GroupNameComparator(sortAscending));
	if (sortColumn.equals("marketName"))
	    Collections.sort(list, new MarketNameComparator(sortAscending));
	if (sortColumn.equals("oppTypeDesc"))
	    Collections.sort(list, new OppTypeDescComparator(sortAscending));
	if (sortColumn.equals("oddsTypeName"))
	    Collections.sort(list, new OddsTypeNameComparator(sortAscending));
	if (sortColumn.equals("timeFirstInvest"))
	    Collections.sort(list, new TimeFirstInvestComparator(sortAscending));
	if (sortColumn.equals("estClosingTime"))
	    Collections.sort(list, new TimeEstClosingComparator(sortAscending));
	if (sortColumn.equals("timeLastInvest"))
	    Collections.sort(list, new TimeLastInvestComparator(sortAscending));
	if (sortColumn.equals("timeActClosing"))
	    Collections.sort(list, new TimeActClosingComparator(sortAscending));
	if (sortColumn.equals("status"))
	    Collections.sort(list, new StatusComparator(sortAscending));
	if (sortColumn.equals("disabled"))
	    Collections.sort(list, new DisabledComparator(sortAscending));
	if (sortColumn.equals("shiftparam"))
	    Collections.sort(list, new ShiftParamComparator(sortAscending, SkinGroup.ETRADER));
	if (sortColumn.equals("shiftparamAO"))
	    Collections.sort(list, new ShiftParamComparator(sortAscending, SkinGroup.ANYOPTION));
	if (sortColumn.equals("scheduled"))
	    Collections.sort(list, new ScheduledComparator(sortAscending));
	if (sortColumn.equals("writer"))
	    Collections.sort(list, new WriterComparator(sortAscending));
	if (sortColumn.equals("winlose"))
	    Collections.sort(list, new WinLoseComparator(sortAscending));

	CommonUtil.setTablesToFirstPage();

	return null;
    }

	private class WinLoseComparator implements Comparator {
		private boolean ascending;
		public WinLoseComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Long(op1.getWinLose()).compareTo(new Long(op2.getWinLose()));
   		 		}
   		 		return -new Long(op1.getWinLose()).compareTo(new Long(op2.getWinLose()));
		}
  	 }

	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Long(op1.getId()).compareTo(new Long(op2.getId()));
   		 		}
   		 		return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
  	 }

	private class MarketNameComparator implements Comparator {
		private boolean ascending;
		public MarketNameComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 		    return CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId()));
   		 		}
   		 		return -CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId()));      
		}
  	 }


	private class OppTypeDescComparator implements Comparator {
		private boolean ascending;
		public OppTypeDescComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return op1.getOpportunityTypeDesc().compareTo(op2.getOpportunityTypeDesc());
   		 		}
   		 		return -op1.getOpportunityTypeDesc().compareTo(op2.getOpportunityTypeDesc());
		}
  	 }
	private class OddsTypeNameComparator implements Comparator {
		private boolean ascending;
		public OddsTypeNameComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return op1.getOpportunityOddsTypeName().compareTo(op2.getOpportunityOddsTypeName());
   		 		}
   		 		return -op1.getOpportunityOddsTypeName().compareTo(op2.getOpportunityOddsTypeName());
		}
  	 }

	private class TimeFirstInvestComparator implements Comparator {
		private boolean ascending;
		public TimeFirstInvestComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (op1.getTimeFirstInvest()==null || op2.getTimeFirstInvest()==null)
   		 		    return CommonUtil.getOppTimeTxt(op1.getTimeFirstInvest()).compareTo(CommonUtil.getOppTimeTxt(op2.getTimeFirstInvest()));

   		 		if (ascending){
   		 			return op1.getTimeFirstInvest().compareTo(op2.getTimeFirstInvest());
   		 		}
   		 		return -op1.getTimeFirstInvest().compareTo(op2.getTimeFirstInvest());
		}
  	 }
	private class TimeEstClosingComparator implements Comparator {
		private boolean ascending;
		public TimeEstClosingComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
   		 		}
   		 		return -op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
		}
  	 }
	private class TimeLastInvestComparator implements Comparator {
		private boolean ascending;
		public TimeLastInvestComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (op1.getTimeLastInvest()==null || op2.getTimeLastInvest()==null)
   		 		    return CommonUtil.getOppTimeTxt(op1.getTimeLastInvest()).compareTo(CommonUtil.getOppTimeTxt(op2.getTimeLastInvest()));

   		 		if (ascending){
   		 			return op1.getTimeLastInvest().compareTo(op2.getTimeLastInvest());
   		 		}
   		 		return -op1.getTimeLastInvest().compareTo(op2.getTimeLastInvest());
		}
  	 }
	private class TimeActClosingComparator implements Comparator {
		private boolean ascending;
		public TimeActClosingComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (op1.getTimeActClosing()==null || op2.getTimeActClosing()==null)
   		 		    return CommonUtil.getOppTimeTxt(op1.getTimeActClosing()).compareTo(CommonUtil.getOppTimeTxt(op2.getTimeActClosing()));
   		 		if (ascending){
   		 			return op1.getTimeActClosing().compareTo(op2.getTimeActClosing());
   		 		}
   		 		return -op1.getTimeActClosing().compareTo(op2.getTimeActClosing());
		}
  	 }

	private class StatusComparator implements Comparator {
		private boolean ascending;
		public StatusComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return OpportunitiesManagerBase.getStatusTxt(op1.getIsSettled(), op1.getIsPublished()).
   		 			        compareTo(OpportunitiesManagerBase.getStatusTxt(op2.getIsSettled(), op2.getIsPublished()));
   		 		}
   		 		return -OpportunitiesManagerBase.getStatusTxt(op1.getIsSettled(), op1.getIsPublished()).
   		 		        compareTo(OpportunitiesManagerBase.getStatusTxt(op2.getIsSettled(), op2.getIsPublished()));

		}
  	 }

	private class DisabledComparator implements Comparator {
		private boolean ascending;
		public DisabledComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Long((op1.isDisabled()?0:1)).compareTo(new Long((op2.isDisabled()?0:1)));
   		 		}
   		 		return -new Long((op1.isDisabled()?0:1)).compareTo(new Long((op2.isDisabled()?0:1)));
		}
  	 }
	private class ShiftParamComparator implements Comparator<Opportunity> {
		private boolean ascending;
		private SkinGroup skinGroup;
		public ShiftParamComparator(boolean asascending, SkinGroup skinGroup) {
			this.ascending = asascending;
			this.skinGroup = skinGroup;
		}
   	    @Override
	    public int compare(Opportunity o1, Opportunity o2) {
   		 		if (ascending) {
   		 			return new Double(o1.getSkinGroupMappings().get(skinGroup).getShiftParameter())
	 						.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getShiftParameter()));
   		 		}
   		 		return -new Double(o1.getSkinGroupMappings().get(skinGroup).getShiftParameter())
	 						.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getShiftParameter()));
		}
  	}
	private class ScheduledComparator implements Comparator {
		private boolean ascending;
		public ScheduledComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Integer(op1.getScheduled()).compareTo(new Integer(op2.getScheduled()));
   		 		}
   		 	return -new Integer(op1.getScheduled()).compareTo(new Integer(op2.getScheduled()));
		}
  	 }
	private class WriterComparator implements Comparator {
		private boolean ascending;
		public WriterComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2)  {
   	    	try {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 		    return CommonUtil.getWriterName(op1.getWriterId()).compareTo(CommonUtil.getWriterName(op1.getWriterId()));
   		 		}
   		 	 return -CommonUtil.getWriterName(op1.getWriterId()).compareTo(CommonUtil.getWriterName(op1.getWriterId()));
   	    	} catch (SQLException e) {
   	    		return 0;
   	    	}
		}
  	 }

	/**
	 * Get list of opportunities
	 * @return
	 * @throws SQLException
	 */
	public String updateList() throws SQLException {

		long s = System.currentTimeMillis();
		list = InvestmentsManager.getOpportunities(from,publishedSettled,Long.valueOf(marketGroup.split(",")[0]), Long.valueOf(marketGroup.split(",")[1]),
												   marketTypeId, statusId, classId, opportunityId, scheduled, suspended);
		long e = System.currentTimeMillis();
		log.debug("getting opportunities: "+(e-s)+" milis");
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update opportunity
	 * @return
	 * @throws SQLException
	 */
	public String update() throws SQLException {
		// Traders no longer update opportunities via this form. per /* DEV-1821 */
		// They do per CORE-2057 and CORE-2176 ....
	    
		FacesContext context = FacesContext.getCurrentInstance();
		User sessionuser = (User) context.getApplication().getExpressionFactory().createValueExpression
	   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());

		boolean resUpDown = true;
		boolean res = false;
		boolean resCurLevel = true;
		FacesMessage fm = null;
		
		if (opportunity.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH){

			if (!isCurrentValueValid()){
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("opportunities.current.value.doesnt.match.opposite", null),null);
				context.addMessage("opportunitiesForm:currentLevel", fm);
				return null;
			}else {
			    if(opportunity.getCurrentLevel() != InvestmentsManager.getOpportunityById(opportunity.getId()).getCurrentLevel()) {			
				resCurLevel = InvestmentsManager.updateCurrentLevelOneTouchOpportunity(opportunity.getId(), opportunity.getCurrentLevel());
				if(resCurLevel) {
				    fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("opportunities.current.level.saved", null), null);
            			    context.addMessage(null, fm);
				}
			    }
			}

			resUpDown = InvestmentsManager.updateOneTouchOpportunity(opportunity.getId(), opportunity.getOneTouchUpDown() );

		}

		res = InvestmentsManager.updateOpportunity(opportunity);

		if (res == false || resUpDown == false || resCurLevel == false) {   // if some update failed
			return null;
		}

		updateList();
		if (navigation != null) {
			return navigation;
		}

		return Constants.NAV_OPPORTUNITIES;
	}

	/**
	 * Checks whether the current value of the current 1 touch opportunity is matching to the value
	 * of the opposite direction opportunity.
	 * @return boolean
	 * @throws SQLException
	 */
	private boolean isCurrentValueValid()  throws SQLException{
		//	Get the opposite direction 1 touch opportunnity.
		Opportunity oppositeOpertunity = InvestmentsManager.getOppositeDirectionOpportunity(opportunity);

		// If the current value of the opposite direction opportunity is 0, it means it hasn't been set yet.
		if (null == oppositeOpertunity || oppositeOpertunity.getCurrentLevel() == 0){
			return true;
		} else {
			// else - check the current value match the direction between both opportunities.
			if (opportunity.getIsOneTouchUp()){
				if (opportunity.getCurrentLevel() > oppositeOpertunity.getCurrentLevel()){
					return true;
				}
			} else{
				if (opportunity.getCurrentLevel() < oppositeOpertunity.getCurrentLevel()){
					return true;
				}
			}
		}
		return false;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Opportunity getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(Opportunity opportunity) {
		this.opportunity = opportunity;
	}

	private class GroupNameComparator implements Comparator {
		private boolean ascending;
		public GroupNameComparator(boolean asc) {
			ascending=asc;
		}
   	    @Override
	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 		    return ApplicationDataBase.getGroupName(op1.getId()).compareTo(ApplicationDataBase.getGroupName(op2.getId()));
   		 		
   		 		}
   		 		return -ApplicationDataBase.getGroupName(op1.getId()).compareTo(ApplicationDataBase.getGroupName(op2.getId()));
		}
  	 }

	public String getPublishedSettled() {
		return publishedSettled;
	}

	public void setPublishedSettled(String publishedSettled) {
		this.publishedSettled = publishedSettled;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public String getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

	public int getListSize() {
		return list.size();
	}

	public ArrayList getList() {
		return list;
	}

	public String getSortColumn()
    {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn)
    {
        this.sortColumn = sortColumn;
    }

    public boolean isSortAscending()
    {
        return sortAscending;
    }

    public void setSortAscending(boolean sortAscending)
    {
        this.sortAscending = sortAscending;
    }

	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}


	public String bNavOpportunity() throws SQLException{
		this.opportunity = InvestmentsManager.getOpportunityById(opportunity.getId());
		setNavigation(Constants.NAV_SHIFTINGS);
		return Constants.NAV_OPPORTUNITY;
	}

	public String getNavigation() {
		return navigation;
	}

	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}

	/**
	 * @return the marketTypeId
	 */
	public long getMarketTypeId() {
		return marketTypeId;
	}

	/**
	 * @param marketTypeId the marketTypeId to set
	 */
	public void setMarketTypeId(long marketTypeId) {
		this.marketTypeId = marketTypeId;
	}

	/**
	 * @return the isPublished
	 */
	public boolean isPublished() {

		if ( null != opportunity ) {

			if ( opportunity.getIsPublished() == Opportunity.PUBLISHED_YES) {
				isPublished = true;
			}
			else {
				isPublished = false;
			}
		}

		return isPublished;
	}

	/**
	 * @param isPublished the isPublished to set
	 */
	public void setPublished(boolean isPublished) {
		this.isPublished = isPublished;
		if ( isPublished ) {
			opportunity.setIsPublished(Opportunity.PUBLISHED_YES);
		}
		else {
			opportunity.setIsPublished(Opportunity.PUBLISHED_NO);
		}
	}

	/**
	 *  return Up or Down for one touch opp
	 */
	public String getUpDownValue() throws SQLException {
		String message;
		FacesContext context = FacesContext.getCurrentInstance();
		int upDown = InvestmentsManager.getOneTouchUpDown(opportunity.getId());
		if ( upDown == 1 ) {
			message = CommonUtil.getMessage("opportunities.one.touch.up", null, Utils.getWriterLocale(context));
		}
		else {
			message = CommonUtil.getMessage("opportunities.one.touch.down", null, Utils.getWriterLocale(context));
		}

		return message;
	}

	/**
	 * @return the upDown
	 */
	public String getUpDown() throws SQLException {
		int upDownOpp = InvestmentsManager.getOneTouchUpDown(opportunity.getId());
		upDown = String.valueOf(upDownOpp);
		return upDown;
	}

	/**
	 * @param upDown the upDown to set
	 */
	public void setUpDown(String upDown) {
		this.upDown = upDown;
		// set the opportunity
		this.opportunity.setOneTouchUpDown(Long.valueOf(upDown));
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "OpportunitiesForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "list = " + this.list + TAB
	        + "publishedSettled = " + this.publishedSettled + TAB
	        + "marketGroup = " + this.marketGroup + TAB
	        + "opportunity = " + this.opportunity + TAB
	        + "sortColumn = " + this.sortColumn + TAB
	        + "prevSortColumn = " + this.prevSortColumn + TAB
	        + "sortAscending = " + this.sortAscending + TAB
	        + "navigation = " + this.navigation + TAB
	        + " )";

	    return retValue;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

    /**
     * @return the opportunityId
     */
    public long getOpportunityId() {
        return opportunityId;
    }

    /**
     * @param opportunityId the opportunityId to set
     */
    public void setOpportunityId(long opportunityId) {
        this.opportunityId = opportunityId;
    }

	public void updateMarketTypeId(ValueChangeEvent event) {
		marketTypeId = ((Long) event.getNewValue());
		if (marketTypeId == 2) {
			from = null;
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			from = gc.getTime();
		}
	}
    
    public String getStatusTxt(){
        return OpportunitiesManagerBase.getStatusTxt(opportunity.getIsSettled(), opportunity.getIsPublished());
    }
    
    public String getScheduledTxt(long scheduled) {
        return InvestmentsManagerBase.getScheduledTxt(scheduled);
    }
    
    public String getSuspendedTxt(int suspended) {
        return InvestmentsManagerBase.getSuspendedTxt(suspended);
    }
    
    public void validateDecimalPoint(FacesContext context, UIComponent comp, Object value) throws Exception {
        FacesMessage msg = new FacesMessage(CommonUtil.getMessage("opportunity.validate.error.decimalPoint", null));
        String currentLevelTxt = String.valueOf(value);
        String[] currentLevelSplit = currentLevelTxt.split("\\.");
        if (currentLevelSplit[1].toString().equals("0")
                && opportunity.getDecimalPointOneTouch() == 0) {
            return;
        } else {
            if (opportunity.getDecimalPointOneTouch() >= currentLevelSplit[1].length()) {
                return;
            }
        }
        throw new ValidatorException(msg);
    }
    
    public double getETShiftParameter(Opportunity o) {
    	return o.getSkinGroupMappings().get(SkinGroup.ETRADER).getShiftParameter();
    }
    
    public int getETMaxExposure() {
    	return opportunity.getSkinGroupMappings().get(SkinGroup.ETRADER).getMaxExposure();
    }
    
    public void setETMaxExposure(int i) {
    	
    }

    public long getScheduled() {
	return scheduled;
    }

    public void setScheduled(long scheduled) {
	this.scheduled = scheduled;
    }

    public int getSuspended() {
	return suspended;
    }

    public void setSuspended(int suspended) {
	this.suspended = suspended;
    }

    public String getAmountWithoutFeesTxt(Investment i) {
    	if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId());
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public List<SelectItem> getOddsGroupList() {
		return oddsGroupList;
	}

	public void setOddsGroupList(List<SelectItem> oddsGroupList) {
		this.oddsGroupList = oddsGroupList;
	}

	public void setOneTouchOddsTypesSI(List<SelectItem> oneTouchOddsTypesSI) {
		this.oneTouchOddsTypesSI = oneTouchOddsTypesSI;
	}

	public List<SelectItem> getOneTouchOddsTypesSI() {
		return oneTouchOddsTypesSI;
	}

	private void createOddsGroupList() {
		oddsGroupList = new ArrayList<>();
		try {
			ArrayList<OpportunityOddsGroup> oddsGroups = InvestmentsManager.getOpportunityOddsGroup();
			for (OpportunityOddsGroup group : oddsGroups) {
				oddsGroupList.add(new SelectItem(group.getOddsGroupDefault(), group.getOddsGroupValue() + " " + group.getDefaultPair()));
			}
		} catch (SQLException e) {
			log.debug("Unable to load odds groups", e);
		}
	}
	
	private void createOneTouchOddsTypesSI() {
		oneTouchOddsTypesSI = new ArrayList<>();
		 FacesContext context = FacesContext.getCurrentInstance();
		 WriterWrapper writer = context.getApplication().evaluateExpressionGet(context, Constants.BIND_WRITER_WRAPPER, WriterWrapper.class);
	        for (SelectItem s : writer.getOddsTypesSI()) {
	            int end = s.getLabel().indexOf("VS");
	            if (end == -1) {
	                end = s.getLabel().indexOf("vs");
	            }
	            String win = s.getLabel().substring(0, end);
	            if (Integer.parseInt(win)>=100) {
	            	oneTouchOddsTypesSI.add(new SelectItem((Long)s.getValue(), s.getLabel()));
	            }
	        }
	        Collections.sort(oneTouchOddsTypesSI, new CommonUtil.selectItemComparator());
	}
}