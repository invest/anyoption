package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.IssueStatus;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.PendingUserWithdrawalsDetailsBase;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.managers.BaroPayManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Cheque;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class PendingWithdrawForm implements Serializable {

	private static final long serialVersionUID = -476749282072133362L;
	private static final Logger logger = Logger.getLogger(PendingWithdrawForm.class);

	private Date from;
	private Date to;
	private ArrayList<Transaction> transactionsList;

	private Transaction transaction;
	private User user;
	private Cheque cheque;
	private long fee;
	private long amountAfterFee;
	private String userId;
	private String userIdNum;
	private String userName;
	private long classId;
	private int skinId;
	private ArrayList<Integer> skinList;
	private long currencyId;
	private WireBase wire;
	private String chequeAddress;
	private String chequeCityAndZipCode;
	private String paypalTransactionId;
	private long skinBusinessCaseId;
	private boolean isCcwithdrawalTran;   // to know transaction withdrawal type
	private boolean isWirewithdrawalTran;
    private long chosenBankId;
    private boolean disableBankOption;
    private Boolean pendingDeposit;
    private long withdrawTypeId;

	private boolean isChangeExpectedProvider;
	private int previousProviderId;
	
   public PendingWithdrawForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		//gc.set(Calendar.HOUR_OF_DAY, 0);
		//gc.set(Calendar.MINUTE, 0);
		//gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		gc.add(GregorianCalendar.YEAR, -2);
		from=gc.getTime();
		userId=null;
		userIdNum=null;
		userName="";
		withdrawTypeId = 0;
		isCcwithdrawalTran = false;
		isChangeExpectedProvider = false;
		
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account

//		set currency filter to all currencies
		if (Utils.getWriter().isEtrader()) {
			currencyId=ConstantsBase.CURRENCY_ILS_ID;
			skinId = Skin.SKIN_ETRADER_INT;
		} else {
			currencyId=ConstantsBase.CURRENCY_ALL_ID;
			skinId = Skin.SKINS_ALL_VALUE;
		}

		updateTransactionsList();
	}

	public int getListSize() {
		return transactionsList.size();
	}

	public ArrayList<Transaction> getTransactionsList() {
		return transactionsList;
	}

	public void setTransactionsList(ArrayList<Transaction> transactionsList) {
		this.transactionsList = transactionsList;
	}

	public String updateTransactionsList() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		userName = userName.toUpperCase();
		transactionsList = TransactionsManager.getPendingWithdraws(from, to,userId,userIdNum,user.isAdminSelected() || user.isSupportSelected(),
				userName,classId, getSkinFilter(), currencyId, skinId, skinBusinessCaseId, withdrawTypeId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String updateTransaction(ValueChangeEvent event) throws SQLException {
		Boolean isFeeCanceled = (Boolean) event.getNewValue();
		cheque.setFeeCancel(isFeeCanceled.booleanValue());
		TransactionsManager.updateCheque(cheque);
		transaction.setFeeCancelByAdmin(transaction.isFeeCancel());
		TransactionsManager.updateFeeCancelByAdmin(cheque.getFeeCancel(), transaction.getId());
		updateTransactionFee();

		return Constants.NAV_ADMIN_PENDING_WITHDRAW;
	}

	/**
	 * Update feeCancel field in selected transaction
	 * this update done in transactions table, cc or wire withdrawal transaction type
	 * @param event
	 * @return
	 * @throws SQLException
	 */
	public String updateTransactionFee(ValueChangeEvent event) throws SQLException {
		Boolean isFeeCanceled = (Boolean) event.getNewValue();
		transaction.setFeeCancel(isFeeCanceled.booleanValue());
		TransactionsManager.updateFeeInTransaction(transaction, true);
		transaction.setFeeCancelByAdmin(transaction.isFeeCancel());
		TransactionsManager.updateFeeCancelByAdmin(transaction.isFeeCancel(), transaction.getId());
		updateTransactionFee();

		return Constants.NAV_ADMIN_PENDING_WITHDRAW;
	}

	public String updateTransactionCredit(ValueChangeEvent event) throws SQLException {
		Boolean isCreditTrx = (Boolean) event.getNewValue();
		transaction.setCreditWithdrawal(isCreditTrx.booleanValue());
		TransactionsManager.updateIsCreditWithdrawal(isCreditTrx, transaction.getId());
		return Constants.NAV_ADMIN_PENDING_WITHDRAW;
	}

	public String editTransaction() throws SQLException {
		user = UsersManager.getUserDetails(transaction.getUserId());
		// check transaction type
		if (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ) {
			isCcwithdrawalTran = true;
			isWirewithdrawalTran = false;
			previousProviderId = (int)transaction.getClearingProviderId();
			isChangeExpectedProvider = false;
		} else if (isChequeWithdrawl()) {   // set cheque
			cheque = TransactionsManager.getChequeDetails(transaction.getChequeId().longValue());
			chequeAddress = cheque.getStreet() + " " + cheque.getStreetNo();
			chequeCityAndZipCode = getChequeCityName() + " " + cheque.getZipCode();
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;
		} else if (isPayPal() || isEnvoy() || isMoneyBookers() || isCUPWithdraw()){
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;
		} else if (isWebMoney()) {
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;
			transaction.setWebMoneyId(TransactionsManager.getWebMoneyDetailsByPurseNumber(transaction.getWebMoneyPurse()));
		} else if (isBaroPay()) {
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;						
			transaction.setBaroPayRequest(BaroPayManagerBase.getBaroPayRequest(transaction.getId()));			
		} else { // wire withdrawal
			isWirewithdrawalTran = true;
			isCcwithdrawalTran = false;
			wire = TransactionsManagerBase.getWire(transaction.getWireId());
		}
		
		// update fee_cancle field
		if(!transaction.isFeeCancel()){		
			boolean needToTakeFee = TransactionsManagerBase.hasMonthlyWithdrawals(user.getId(), transaction.getTimeCreated(), transaction.getUtcOffsetCreated());
			boolean feeCancel = (!needToTakeFee);
	
			// when we creating withdrawal transaction, the ff_cancle if field  is false,
			// it will be true in case admin marked it as feeCancel true (from checkBox)
			if (transaction.isUpdateByAdmin() == false ) {
				if (isCcwithdrawalTran || isWirewithdrawalTran || isPayPal() || isEnvoy() || isMoneyBookers() || isWebMoney() || isDeltaPAyChinaWithdraw() || isBaroPay()) {
					transaction.setFeeCancel(feeCancel);
				} else {
					cheque.setFeeCancel(feeCancel);
				}
			}
			
			//Check if need Fee on first shouldChargeForLowWithdraw
			Limit limit = UsersManagerBase.getLimitById(user.getLimitIdLongValue());
			if(TransactionsManagerBase.shouldChargeForLowWithdraw(user.getId(), limit.getAmountForLowWithdraw(), transaction.getAmount(), transaction.getTimeCreated(), transaction.getUtcOffsetCreated())){
				transaction.setFeeCancel(false);
			}
		}
		return updateAndNavigate();
	}


	public String updateTransactionFee() throws SQLException {
		// check transaction type
		if (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ) {
			isCcwithdrawalTran = true;
			isWirewithdrawalTran = false;
		} else if (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW ) {   // set cheque
			cheque = TransactionsManager.getChequeDetails(transaction.getChequeId().longValue());
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;
		} else if (isPayPal() || isEnvoy() || isMoneyBookers() || isWebMoney() || isCUPWithdraw() || isBaroPay()) {   // PayPal
			isCcwithdrawalTran = false;
			isWirewithdrawalTran = false;
		} else { // wire withdrawal
			isWirewithdrawalTran = true;
			isCcwithdrawalTran = false;
		}
		return updateAndNavigate();
	}


	/**
	 * Update fee field and return navigation page
	 * @return
	 * @throws SQLException
	 */
	private String updateAndNavigate() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		User sessionUser = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		// update fee field
		fee = TransactionManager.getTransactionHomoFee(transaction.getAmount(), sessionUser.getCurrencyId(), transaction.getTypeId());
//		long feeValue = 0;
//		Limit limit = UsersManagerBase.getLimitById(user.getLimitIdLongValue());
//		chargeForLowWithdraw = TransactionsManagerBase.shouldChargeForLowWithdraw(user.getId(), limit.getAmountForLowWithdraw(), transaction.getAmount(), transaction.getTimeCreated(), transaction.getUtcOffsetCreated());
//		if (chargeForLowWithdraw) {
//			feeValue = TransactionsManager.getFeeByTranTypeId(limit, TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE);
//		} else {
//			if (isCcwithdrawalTran) {
//				feeValue = TransactionsManager.getFeeByTranTypeId(user.getLimitIdLongValue(),
//						TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
//			} else if (isWirewithdrawalTran) {
//				feeValue = TransactionsManager.getFeeByTranTypeId(user.getLimitIdLongValue(),
//						TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
//				disableBankOption = false;
//				if (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) {
//					chosenBankId = ConstantsBase.BANK_TYPE_ID_MONDIAL;
//					disableBankOption = true;
//				}
//			} else if (isPayPal() || isEnvoy() || isMoneyBookers() || isWebMoney() || isCUPWithdraw() || isBaroPay()) {
//				feeValue = TransactionsManager.getFeeByTranTypeId(user.getLimitIdLongValue(),
//						(int)transaction.getTypeId());
//			} else { // cheqeue withdrawal transaction type
//				feeValue = TransactionsManager.getFeeByTranTypeId(user.getLimitIdLongValue(),
//						TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW);
//			}
//		}
//
//		if ((!isCcwithdrawalTran && !isWirewithdrawalTran && !isPayPal() && !isMoneyBookers() && !isBaroPay() && !isWebMoney()
//				&& !isEnvoy() && !cheque.getFeeCancel() && !isDeltaPAyChinaWithdraw())
//				|| ((isCcwithdrawalTran || isWirewithdrawalTran || isPayPal() || isMoneyBookers() || isBaroPay() || isWebMoney()
//						|| isEnvoy() || isDeltaPAyChinaWithdraw()) && !transaction.isFeeCancel())) {
//			fee = feeValue;
//		} else {
//			fee = 0;
//		}

		if (transaction.isFeeCancel()) {
			fee = 0;
		}
		amountAfterFee = transaction.getAmount() - fee;

		if (sessionUser.isAdminSelected()) {
			return Constants.NAV_ADMIN_PENDING_WITHDRAW;
		} else if (sessionUser.isSupportSelected()) {
			return Constants.NAV_SUPPORT_PENDING_WITHDRAW;
		} else {
			return Constants.NAV_ACCOUNT_PENDING_WITHDRAW;
		}
	}

	public String approve() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		StringBuffer changedProviderComment = new StringBuffer();
		boolean res;
		synchronized (transaction) {
			long sId = TransactionsManagerBase.getTransaction(transaction.getId()).getStatusId();
			if (sId == TransactionsManager.TRANS_STATUS_SECOND_APPROVAL 
					|| sId == TransactionsManager.TRANS_STATUS_SUCCEED) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.trx.already.approved", null), null);
				context.addMessage(null, fm);
				return null;
			} else {
				if (isCcwithdrawalTran || isCUPWithdraw() || isBaroPay()) {
					// Disable negative transaction amount (after fee)
					if (amountAfterFee < 0) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
								CommonUtil.getMessage("error.negative.trx.amount", null), null);
						context.addMessage(null, fm);
						return null;
					}
					if (isChangeExpectedProvider && previousProviderId != transaction.getClearingProviderId()) {
						String providerName = "";
						if (ApplicationData.getCCClearingProviders() != null) {
							providerName = ApplicationData.getClearingProvidersStatic().get(previousProviderId);
						}
						changedProviderComment.append("provider changed from " + providerName);
						isChangeExpectedProvider = false;
					}
					res = TransactionsManager.approveCcWithdraw(transaction, fee,
							user.isAdminSelected() || user.isSupportSelected(),
							CommonUtil.getWriterName(Utils.getWriter().getWriter().getId()), changedProviderComment);
				} else if (isWirewithdrawalTran) {
					res = TransactionsManager.approveBankWireWithdraw(transaction, wire, fee,
							user.isAdminSelected() || user.isSupportSelected(), chosenBankId);
					if (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
						// sending mail to the client
						String ls = System.getProperty("line.separator");
						logger.info("Going to send withdrawal email: " + ls + "userId: " + transaction.getUserId() + ls
								+ "trxId: " + transaction.getId() + ls);
						Template template = new Template();
						template.setSubject("withdrawal.user.email");
						template.setFileName(Constants.TEMPLATE_WITHDRAW_BW_SUCCEED);

						// MailBox
						// if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
						Template t = AdminManager.getTemplateById(Template.WIRE_WITHDRAWAL_SUCCESS_MAIL_ID);
						template.setMailBoxEmailType(t.getMailBoxEmailType());
						// }

						// send email to user and to us for tracking
						try {
							User u = new User();
							u.setUserName(transaction.getUserName());
							String[] splitTo = CommonUtil.getProperty("succeed.email.to").split(";");
							UsersManagerBase.sendMailTemplateFile(template, Utils.getWriter().getWriter().getId(), u,
									false, transaction.getCc4digit(), null, transaction.getWireAmountAfterFee(),
									transaction.getWire().getBankNameTxt(), null, splitTo, transaction.getId(), null, 0,
									null, null, null, null, false, null, null, null, null, null, null);
						} catch (Exception e) {
							logger.error("Error, Can't send withdrawal email!", e);
						}

					}
				} else if (isPayPal()) {
					res = TransactionsManager.approvePayPalWithdraw(transaction, fee,
							user.isAdminSelected() || user.isSupportSelected(), paypalTransactionId);
				} else if (isEnvoy()) {
					res = TransactionsManager.approveEnvoyWithdraw(transaction, fee,
							user.isAdminSelected() || user.isSupportSelected());
				} else if (isMoneyBookers() || isWebMoney()) {
					int feeType = TransactionsManagerBase.TRANS_TYPE_HOMO_FEE;
					if (fee > 0 && !transaction.isFeeCancel() && !transaction.isFeeCancelByAdmin()
							&& user.isAccountingSelected()) {
						transaction.setAmount(transaction.getAmount() - fee);
						res = TransactionsManager.approveGeneralWithdraw(transaction, fee,
								user.isAdminSelected() || user.isSupportSelected());
						com.anyoption.common.beans.Transaction feeTransaction = createFeeTransaction(transaction,
								Utils.getWriter(), fee, feeType);
						TransactionsManagerBase.insertFeeTransaction(feeTransaction, transaction.getId());
					} else { // no fee
						res = TransactionsManager.approveGeneralWithdraw(transaction, fee,
								user.isAdminSelected() || user.isSupportSelected());
					}
				} else { // cheqeue withdrawal transaction type
					res = TransactionsManager.approveWithdraw(transaction, cheque, fee,
							user.isAdminSelected() || user.isSupportSelected());
				}

				if (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
					WithdrawEntryResult finalWithdrawEntryResult = WithdrawUtil
							.finalWithdrawEntry(new WithdrawEntryInfo(user, transaction.getId()));
				}

				if (res == false) {
					return null;
				}
				if (!(user.isAdminSelected() || user.isSupportSelected())) {
					PopulationsManager.removeFromOpenWithdrawPop(transaction.getUserId());
				}
			}
		}
		
		return navPendingWithdraws();
	}
	

	com.anyoption.common.beans.Transaction createFeeTransaction(com.anyoption.common.beans.Transaction mainTransaction, WriterWrapper  w, long feeAmount, int feeType) {
		com.anyoption.common.beans.Transaction feeTransaction = new com.anyoption.common.beans.Transaction();
		feeTransaction.setUserId(mainTransaction.getUserId());
		feeTransaction.setTypeId(feeType);
		feeTransaction.setTimeCreated(new Date());
		feeTransaction.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		feeTransaction.setWriterId(w.getWriter().getId());
		feeTransaction.setIp(mainTransaction.getIp());
		feeTransaction.setTimeSettled(new Date());
		feeTransaction.setProcessedWriterId(w.getWriter().getId());
		feeTransaction.setReferenceId(new BigDecimal(mainTransaction.getId()));
		feeTransaction.setAuthNumber(mainTransaction.getAuthNumber());
       	feeTransaction.setAmount(feeAmount);
    	feeTransaction.setUtcOffsetCreated(w.getUtcOffset());
    	feeTransaction.setUtcOffsetSettled(w.getUtcOffset());
    	return feeTransaction;
	}

	public String getShortUserName(String userName, int lenght){
		String res = userName;
		if(userName.length() > lenght){
			res = res.substring(0, lenght);
		}
		return res;
	}

	public String reject() throws SQLException {

		TransactionsManager.rejectWithdraw(transaction);

		return navPendingWithdraws();
	}

	public String getFeeTxt() {
		return CommonUtil.displayAmount(fee, transaction.getCurrencyId());
	}

	public IssueStatus getIdDocumentStatus() throws SQLException{
		return UsersManager.getIdDocumentStatusByUserId(user.getId());

	}

	public String getAmountAfterFeeTxt() {
		return CommonUtil.displayAmount(amountAfterFee, transaction.getCurrencyId());
	}

	public String navPendingWithdraws() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		updateTransactionsList();

		if (user.isAdminSelected()) {
			return Constants.NAV_ADMIN_PENDING_WITHDRAWS;
		} else if (user.isSupportSelected()){
			return Constants.NAV_SUPPORT_PENDING_WITHDRAWS;
		} else {
			return Constants.NAV_ACCOUNT_PENDING_WITHDRAWS;
		}
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Transaction getTransaction() throws SQLException {
		return transaction;
	}

	/**
	 * set selected transaction
	 * @param transaction
	 * @throws SQLException
	 */
	public void setTransaction(Transaction transaction) throws SQLException {
		this.transaction = transaction;
	}

	public String getChequeCityName() {
    	FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData a= (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
    	return (String)a.getCities().get(String.valueOf(cheque.getCityId()));
    }

	public long getAmountAfterFee() {
		return amountAfterFee;
	}

	public void setAmountAfterFee(long amountAfterFee) {
		this.amountAfterFee = amountAfterFee;
	}

	public Cheque getCheque() throws SQLException {
		return cheque;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserIdNum() {
		return userIdNum;
	}

	public void setUserIdNum(String userIdNum) {
		this.userIdNum = userIdNum;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return this.userName;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the isCcwithdrawalTran
	 */
	public boolean isCcwithdrawalTran() {
		return isCcwithdrawalTran;
	}

	/**
	 * @param isCcwithdrawalTran the isCcwithdrawalTran to set
	 */
	public void setCcwithdrawalTran(boolean isCcwithdrawalTran) {
		this.isCcwithdrawalTran = isCcwithdrawalTran;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "PendingWithdrawForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "transactionsList = " + this.transactionsList + TAB
	        + "transaction = " + this.transaction + TAB
	        + "user = " + this.user + TAB
	        + "cheque = " + this.cheque + TAB
	        + "fee = " + this.fee + TAB
	        + "amountAfterFee = " + this.amountAfterFee + TAB
	        + "userId = " + this.userId + TAB
	        + "userIdNum = " + this.userIdNum + TAB
	        + " )";

	    return retValue;
	}


	/**
	 * this method return a String that represent all of the writers skins seperated by a comma
	 * @return
	 */
	public String getSkinFilter() {
		if((skinList == null || skinList.isEmpty()) ||(skinList!=null && skinList.size() == 1 && Integer.parseInt(skinList.get(0).toString()) == 0)) {
			return Utils.getWriter().getSkinsToString();
		} else {
			return CommonUtil.convertToCSV(skinList);
		}
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the skinId
	 
	public int getSkinId() {
		return skinId;
	}
*/
	/**
	 * @param skinId the skinId to set
	 
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
*/
	/**
	 * @return the isWirewithdrawalTran
	 */
	public boolean isWirewithdrawalTran() {
		return isWirewithdrawalTran;
	}

	/**
	 * @param isWirewithdrawalTran the isWirewithdrawalTran to set
	 */
	public void setWirewithdrawalTran(boolean isWirewithdrawalTran) {
		this.isWirewithdrawalTran = isWirewithdrawalTran;
	}

	/**
	 * @return the wire
	 */
	public WireBase getWire() {
		return wire;
	}

	/**
	 * @param wire the wire to set
	 */
	public void setWire(WireBase wire) {
		this.wire = wire;
	}

	public boolean isPayPal() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
	}

	public boolean isMoneyBookers() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
	}

	public boolean isDeltaPAyChinaWithdraw() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
	}
	public boolean isCUPWithdraw() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
	}

	public boolean isWebMoney() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW);
	}

	public boolean isEnvoy() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW) ;
	}
	
	public boolean isBaroPay() {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) ;
	}

	public boolean isChequeWithdrawl(){
		return (transaction.isChequeWithdraw()) ;
	}

	/**
	 * Get clearing provider name
	 * @return
	 * @throws ClearingException
	 * @throws SQLException
	 */
	public String getClearingProviderName() throws ClearingException, SQLException {
		String providerName = "";
		if (transaction.getClearingProviderId() == 0) {  // for old transactions that created before inatec CFT code
			logger.info("clearingProviderId not set on creation!");
			ClearingInfo info = new ClearingInfo();
			CreditCard card = transaction.getCreditCard();
			info.setSkinId(user.getSkinId());
			info.setCcn(String.valueOf(card.getCcNumber()));
			info.setCurrencyId(user.getCurrencyId().intValue());
			info.setCcCountryId(card.getCountryId());
			info.setCcTypeId((int)card.getTypeIdByBin());
			info.setPlatformId(user.getPlatformId());
			transaction.setClearingProviderId(ClearingManager.findRoute(info).getDepositClearingProviderId());
			TransactionsManager.updateClearingProviderId(transaction.getId(), transaction.getClearingProviderId());
		}
		providerName = ClearingManager.getProviderName(transaction.getClearingProviderId());
		return providerName;
	}

	public boolean isInatecCreditAvailable() throws ClearingException, SQLException {
		ClearingInfo info = new ClearingInfo();
		CreditCard card = transaction.getCreditCard();
		info.setSkinId(user.getSkinId());
		info.setCcn(String.valueOf(card.getCcNumber()));
		info.setCurrencyId(user.getCurrencyId().intValue());
		info.setCcCountryId(card.getCountryId());
		info.setCcTypeId((int)card.getTypeIdByBin());
		info.setPlatformId(user.getPlatformId());
		Long providerId = ClearingManager.findRoute(info).getDepositClearingProviderId();  // take deposit provider
		String inatecFormer = ClearingManager.INATEC_PROVIDER_ID_EUR + "," +
				  ClearingManager.INATEC_PROVIDER_ID_USD + "," +
				  ClearingManager.INATEC_PROVIDER_ID_GBP + "," +
				  ClearingManager.INATEC_PROVIDER_ID_TRY ;
		if (null == providerId ||
				(null != providerId &&
						(providerId != ClearingManager.INATEC_PROVIDER_ID_USD &&
						 providerId != ClearingManager.INATEC_PROVIDER_ID_EUR &&
						 providerId != ClearingManager.INATEC_PROVIDER_ID_GBP &&
						 providerId != ClearingManager.INATEC_PROVIDER_ID_TRY && 
						 providerId != ClearingManager.TBI_AO_PROVIDER_ID))) {
			return false;
		}
		long creditAmountAllow = 0;
		if (transaction.getClearingProviderId() == 0) { // for old transactions that created before Inatec CFT code
			logger.info("clearingProviderId not set on creation!");
			transaction.setClearingProviderId(ClearingManager.findRoute(info).getWithdrawClearingProviderId());
			TransactionsManager.updateClearingProviderId(transaction.getId(), transaction.getClearingProviderId());
		}
		if (transaction.getClearingProviderId() == ClearingManager.INATEC_PROVIDER_ID_CFT || transaction.getClearingProviderId() == ClearingManager.TBI_AO_PROVIDER_ID) {
			
			ArrayList<Transaction> depositsList = TransactionsManager.getDepositTrxForCredit(user.getId(), transaction.getCreditCardId().longValue(), inatecFormer, false);
			for (Transaction t : depositsList) {
				creditAmountAllow += t.getCreditAmount();
			}
			if (transaction.getAmount() <= creditAmountAllow) {
				return true;
			}

		}
		return false;
	}

    public String saveToTxtFile() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        if ((user.getFirstName() + " " + user.getLastName()).length() > 40) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Faild to save cheque name: " + user.getFirstName() + " " + user.getLastName() + " is to long" , null);
            fc.addMessage(null, fm);
            return null;
        }

        //Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".txt";
        fmt.applyPattern("dd/MM/yy");
        String newline = "\r\n"; //System.getProperty("line.separator");
        String checkInfo=
            "Pay_date:" + fmt.format(dt) + newline +
            "Supplier_name:" + user.getFirstName() + " " + user.getLastName() + newline +
            "Supplier_number:" + user.getId() + newline +
            "Supplier_address:" + chequeAddress + newline +
            "Supplier_city:" + chequeCityAndZipCode + newline +
            "Amount:" + CommonUtil.displayAmount(amountAfterFee, false, user.getCurrencyId());

        logger.debug("saveing cheque to txt file " + checkInfo);
        //Setup the output
        String contentType = "text/plain";
        fc = FacesContext.getCurrentInstance();
        filename = user.getIdNum()+ "_" + filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("cp1255");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(checkInfo);
        out.close();
        fc.responseComplete();

        return null;
    }

	/**
	 * @return the chequeAddress
	 */
	public String getChequeAddress() {
		return chequeAddress;
	}

	/**
	 * @param chequeAddress the chequeAddress to set
	 */
	public void setChequeAddress(String chequeAddress) {
		this.chequeAddress = chequeAddress;
	}

	/**
	 * @return the chequeCityAndZipCode
	 */
	public String getChequeCityAndZipCode() {
		return chequeCityAndZipCode;
	}

	/**
	 * @param chequeCityAndZipCode the chequeCityAndZipCode to set
	 */
	public void setChequeCityAndZipCode(String chequeCityAndZipCode) {
		this.chequeCityAndZipCode = chequeCityAndZipCode;
	}

	/**
	 * @return the paypalTransactionId
	 */
	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}

	/**
	 * @param paypalTransactionId the paypalTransactionId to set
	 */
	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}

    /**
     * @return the chosenBank
     */
    public long getChosenBank() {
        return chosenBankId;
    }

    /**
     * @param chosenBank the chosenBank to set
     */
    public void setChosenBank(long chosenBank) {
        this.chosenBankId = chosenBank;
    }

	/**
	 * @return the disableBankOption
	 */
	public boolean isDisableBankOption() {
		return disableBankOption;
	}

	/**
	 * @param disableBankOption the disableBankOption to set
	 */
	public void setDisableBankOption(boolean disableBankOption) {
		this.disableBankOption = disableBankOption;
	}
	
	/**
	 * Check if user have wire deposit or cash deposit in status pending
	 * @return true - in case the user have wire deposit or cash deposit in status pending
	 */
	public boolean isHavePendingDeposit() {
		if (pendingDeposit == null) {
			try {
				pendingDeposit =  TransactionsManagerBase.IsHavePendingDeposit(transaction);
			} catch (SQLException e) {
				pendingDeposit = false;
				logger.error("Error, Can't check pending deposit!" , e);
			}
		}
		return pendingDeposit;
	}

	public long getSkinBusinessCaseId() {
		return skinBusinessCaseId;
	}

	public void setSkinBusinessCaseId(long skinBusinessCaseId) {
		this.skinBusinessCaseId = skinBusinessCaseId;
	}

	public ArrayList<Integer> getSkinList() {
		return skinList;
	}

	public void setSkinList(ArrayList<Integer> skinList) {
		this.skinList = skinList;
	}

	public int getSkinId() {
		return skinId;
	}

	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	
	public void updateSkinList(ValueChangeEvent event) {
		skinList = new ArrayList<Integer>();
		skinList.add((Integer)event.getNewValue());
	}
	
	public Long getWithdrawTypeId() {
		return withdrawTypeId;
	}

	public void setWithdrawTypeId(Long withdrawTypeId) {
		this.withdrawTypeId = withdrawTypeId;
	}

	public String test(long userId){
		PendingUserWithdrawalsDetailsBase pw = new PendingUserWithdrawalsDetailsBase();
		try {
			pw = UsersManager.pendingUserWithdrawalsDetails(userId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("ERRRR", e);
		}
		logger.debug(pw);
		return "";
	}

	/**
	 * @return the isChangeExpectedProvider
	 */
	public boolean isChangeExpectedProvider() {
		return isChangeExpectedProvider;
	}

	/**
	 * @param isChangeExpectedProvider the isChangeExpectedProvider to set
	 */
	public void setChangeExpectedProvider(boolean isChangeExpectedProvider) {
		this.isChangeExpectedProvider = isChangeExpectedProvider;
	}

	/**
	 * @return the previousProviderId
	 */
	public int getPreviousProviderId() {
		return previousProviderId;
	}

	/**
	 * @param previousProviderId the previousProviderId to set
	 */
	public void setPreviousProviderId(int previousProviderId) {
		this.previousProviderId = previousProviderId;
	}

}
