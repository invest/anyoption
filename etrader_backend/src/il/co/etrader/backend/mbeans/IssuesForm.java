package il.co.etrader.backend.mbeans;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;
import org.jfree.util.Log;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.IssueSubject;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfilesOnlineManager;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.PendingDocumentsManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.UserFiles;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.IssuesReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.FilesRiskStatus;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class IssuesForm extends FormBase implements Serializable {

	private static final long serialVersionUID = -474689997688354959L;

	private static final Logger logger = Logger.getLogger(IssuesForm.class);
	
	private static final int ISSUE_ACTION_TYPE_SUSPEND_REGULATION = 48;
	private static final int ISSUE_ACTION_TYPE_REMOVE_SUSPEND_REGULATION = 47;
	private static final int ISSUE_ACTION_TYPE_ACTIVATE_ACOUNT = 12;
	private static final int ISSUE_ACTION_TYPE_DEACTIVATE_ACOUNT = 13;
	
	private ArrayList<Issue> issuesList;
	private Issue issue;
	private Issue tempIssue;
	private ArrayList<IssueAction> issueActionsList;
	private IssueAction issueAction;
	private ArrayList<SelectItem> issueSubjectsSI;
	private ArrayList<Integer> skinList;
	private ArrayList<SelectItem> riskReasonsList;

	private HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> issueActionTypes;

	private Date from;
	private Date to;
	private String statusId;
	private String channelId;
	private String subjectId;
	private String priorityId;
	private String reachedStatusId;
	private String actionTypeId;
	private long callDirectionId;
	private String userName;
	private long classId;
	//private int skinId;
	private boolean byWriter;
	private boolean bySignificantNotes;
	private boolean isNewAction;
	private long writerId;
	private String issueActionName;
	private boolean cbRelevant;
	private long writerFilter;
	private int screenId;
	private long populationType;
	private List<? extends com.anyoption.common.beans.File> filesList;
	private List<? extends com.anyoption.common.beans.File> filesListPerUser;
	private Long cardNum;
	
	boolean canMoveToControl = false;
	boolean canMoveToReg = false;
	String comment;

	private PopulationEntry populationEntry;
	private long populationCurrentEntryId;

	//risk
	private boolean isDocumentAvailable;
	private long riskUserId;
	private ArrayList<CreditCard> cardsList;
	private UserFiles userFiles;
	private boolean riskIdRequest;
	private boolean riskIdSelected;
	private boolean isIdDocumentSent;
	private File idFile;
	private String userId;
	private HashMap<Long, ArrayList<Issue>> issuesByUserId;
	private String showRelevantTo;
	private Issue riskIssueToUserFiles;
	private boolean showAllCards;
	private long issueType;
	private ArrayList<CreditCard> tmpCardsList;
	private long riskIssueType;
	private String riskReasonId;
	//many list box
    private List<String> statuses;
    private List<String> insertRelevantTo;
	//sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private boolean activateAscending;
    private String searchBy ;
    private ArrayList<SelectItem> ccSI;
    private boolean showChangeSkinDropdown = false;
    private long componentId;
    private boolean copyopBlockedSelected;
    private boolean copyopRemovedSelected;
    private boolean copyopBlockRemoveFields;
    private boolean disabledBlockRemoveReason;
    
	public IssuesForm() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		if (null != user ){ // for risk issues
			riskUserId = user.getId();
		}
		searchBy = "0";
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, 1);
		to = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, -1);
		if (request.getRequestURL().indexOf("riskIssues") > -1 ){ // risk issues
			gc.add(GregorianCalendar.MONTH, -1);
		} else {
			//issues screen
			if(user.isSupportSelected()){
				gc.add(GregorianCalendar.YEAR, -1);
			} else if (user.isAdminSelected() || user.isSupportMoreOptionsSelected()) {
				gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
			} else {
				gc.add(GregorianCalendar.WEEK_OF_MONTH, -1); // sales and salesM
			}
		}
		from = gc.getTime();
		userName = "";
		issueActionName="";
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		if (user.isSupportSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
		}

		writerId = 0;
		writerFilter = 0;
		if (request.getRequestURL().indexOf("riskIssues") > -1){ // risk issues
			issuesList = new ArrayList<Issue>();
			riskIssueType = ConstantsBase.ALL_FILTER_ID;
			searchRisk();
		} else {
			search();
		}
		issueActionTypes = Utils.getWriter().getIssueActionType();
		screenId = user.getScreenId();
		//risk fields
		isIdDocumentSent = false;
		riskIdRequest = false;
		isNewAction = false;
		statuses = new ArrayList<String>();
		statuses.add("4");
		statuses.add("5");
		statuses.add("6");
		skinList = new ArrayList<Integer>();
		skinList.add(0);
		insertRelevantTo = new ArrayList<String>();
		isDocumentAvailable = true;
		showAllCards = false;
		issueType = ConstantsBase.ALL_FILTER_ID;
		
		PopulationEntryBase popEntry = (PopulationEntryBase) context.getApplication().evaluateExpressionGet(context, ConstantsBase.BIND_POPULATION_ENTRY, PopulationEntryBase.class);
		populationCurrentEntryId = popEntry.getCurrEntryId();
	}

	public void actionTypeChanged(ValueChangeEvent ev){
		User user = Utils.getUser();
		if(user.isRetentionMSelected()) {
			if("26".equals(ev.getNewValue())) {// "different language" (issue action type=26)
				if(user.getDeposits()>0) {
					showChangeSkinDropdown = true;
					return;
				}
			}
		}
		showChangeSkinDropdown = false;
	}
	// retention manager choose to change skin
	// after selecting "different language"
	public void retManChangeSkin(ValueChangeEvent ev){
		try {
			UsersManagerBase.updateUserSkin(Utils.getUser().getId(), Long.parseLong(ev.getNewValue().toString()));
		} catch (SQLException e) {
			logger.debug("Can't update user's skin:"+e.getMessage());
		}
	}
	public int getListSize() {
		if (issuesList == null){
			return 0;
		}
		return issuesList.size();
	}

	public int getIssueActionsListSize() {
		return issue.getIssueActionsList().size();
	}

	public String getNavigation() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.isSupportSelected()) {
			return Constants.NAV_ISSUES;
		} else if (user.isSupportMoreOptionsSelected()) {
			return Constants.NAV_ISSUES_MORE_OPTIONS;
		} else if (user.isAdminSelected()) {
			return Constants.NAV_ISSUES_ADMIN;
		} else if (user.isRetentionSelected()) {
			return Constants.NAV_ISSUES_RETENTION;
		} else if (user.isPartnerSelected()) {
			return Constants.NAV_ISSUES_PARTNER;
		} else if (user.isRetentionMSelected()) {
			return Constants.NAV_ISSUES_RETENTIONM;
		}

		return null;
	}

	public String addNewIssue() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		issue = new Issue();
		issueAction = new IssueAction();
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_CALL));
		tempIssue = null;

		if (user.isRetentionSelected() || user.isRetentionMSelected() ||
				(user.isSupportSelected() && user.isSupportMOLocking()) ){

			PopulationEntry lockedEntry;
			try {
				 lockedEntry = PopulationsManager.getLockedEntryByLockHisId(populationEntry.getLockHistoryId());
			} catch (PopulationHandlersException e) {
				return null;
			}

			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRY, PopulationEntryBase.class);
			PopulationEntryBase popEntrySession = (PopulationEntry) ve.getValue(context.getELContext());

			if (null != lockedEntry){
				long popEntryId = lockedEntry.getOldPopEntryId();
				long issueId = IssuesManagerBase.searchIssueIdByPopEntryID(popEntryId);

				issue.setUserId(lockedEntry.getUserId());
				issue.setContactId(lockedEntry.getContactId());
				//if user have no population and we want to change his population to Special CB deposit
				if (lockedEntry.getEntryTypeId()== ConstantsBase.POP_ENTRY_TYPE_CALLBACK &&  lockedEntry.getCurrEntryId() == 0){
					PopulationEntryBase p = null;
					long UserIdForTranfer;
					long ContactIdForTransfer;

					// insert user into Special entry
					UserIdForTranfer = lockedEntry.getUserId();
					ContactIdForTransfer =  lockedEntry.getContactId();
					
					long popTypeSpecial = PopulationsManagerBase.POP_TYPE_SPECIAL_CONVERSION;
					if (user.getFirstDepositId() > 0) {
						popTypeSpecial = PopulationsManagerBase.POP_TYPE_SPECIAL_RETENTION;
					}

					long specialPopulationId = PopulationsManager.getPopulationList(popTypeSpecial,
							lockedEntry.getSkinId(), true, PopulationsManagerBase.POP_TYPE_SPECIAL_CB_DEPOSIT, 0).get(0).getId();

					try {
						p = PopulationsManager.getPopulationUserByUserAndContactId(UserIdForTranfer,ContactIdForTransfer);

						if (null == p){
							// population handle
							p = new PopulationEntryBase();
							p.setUserId(UserIdForTranfer);
							p.setContactId(ContactIdForTransfer);
							p.setSkinId(user.getSkinId());
						}

						p.setPopulationId(specialPopulationId);
						p.setCurrPopualtionTypeId(popTypeSpecial);

						PopulationsManager.insertIntoPopulationEntries(p,0);
					} catch (SQLException e) {
						logger.error("Error, problem to insert populationEntry ", e);
					}

					popEntrySession.setCurrPopualtionTypeId(p.getCurrPopualtionTypeId());
					popEntrySession.setPopulationId(p.getPopulationId());
					popEntrySession.setCurrEntryId(p.getCurrEntryId());
					popEntrySession.setQualificationTime(p.getQualificationTime());
					popEntryId = p.getCurrEntryId();

					ve.setValue(context.getELContext(), popEntrySession);


					IssueSubject ib=new IssueSubject();
					ib.setName(PopulationsManagerBase.getNameByPopEntryID(p.getCurrEntryId()) +
							ConstantsBase.POPULATION_SIGN);
					issue.setSubject(ib);



				} else if (0 != issueId) { // if there are existing issues for this population entry send error message.
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
							CommonUtil.getMessage("issues.insert.failed.unique.issue.per.population.entry", null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);

					return null;
				}

				issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_POPULATION));
				issue.setPopulationEntryId(popEntryId);
			} else {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("issues.insert.failed.population.entry.not.found", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

				return null;
			}

		}

		return Constants.NAV_ISSUE;
	}

	public String search() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		userName = userName.toUpperCase();
		if (writerFilter > 0) {
			byWriter = false;
		}
		writerId = writerFilter;

		if (user.isAdminSelected() || user.isPartnerSelected() || user.isSupportMoreOptionsSelected()) {
			issuesList=IssuesManager.searchIssues(this,getSkinFilter(), byWriter, bySignificantNotes,writerId, app,
					wr.getUtcOffset(), wr.getLocalInfo().getLocale());
		} else if (user.isSupportSelected() && !user.isSupportMOLocking()) {
			issuesList=IssuesManager.searchIssuesByUserID(this,user.getId(),getSkinFilter(), byWriter, bySignificantNotes,writerId, issueType);
		} else if (user.isRetentionSelected() || user.isRetentionMSelected() ||
				(user.isSupportSelected() && user.isSupportMOLocking()) ) {
			populationEntry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);

			if (populationEntry != null){
				long popUserId = populationEntry.getPopulationUserId();

				if (popUserId > 0){
					long userId = populationEntry.getUserId();

					if (userId > 0){
						issuesList=IssuesManager.searchIssuesByUserID(this,userId,getSkinFilter(), byWriter, bySignificantNotes, writerId, issueType);
					} else{
						issuesList=IssuesManager.searchIssuesByPopUserID(this,popUserId ,getSkinFilter(), byWriter, bySignificantNotes,writerId, issueType);
					}
				}
			}
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	/**
	 * @param issueAction the {@link com.anyoption.common.beans.IssueAction} object
	 * @return the relevantToCards
	 * @throws CryptoException
	 * @throws SQLException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public String getRelevantToCards(IssueAction issueAction)	throws SQLException, CryptoException, InvalidKeyException,
																IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
																NoSuchPaddingException, InvalidAlgorithmParameterException {
		String rel = issueAction.getRelevantToCards();
		if (rel == null || rel.isEmpty()) {
			rel = IssuesManagerBase.getRelevantCardsToIssueAction(issueAction.getActionId());
			issueAction.setRelevantToCards(rel);
		}
		
		return rel;
	}
	
	/**
	 * @return the population type key of the issue population entry
	 */
	public String getPopulationTypeKey() throws SQLException{
		return PopulationsManagerBase.getNameByPopEntryID(getIssue().getPopulationEntryId());
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}
	
	public long getPopulationType() {
		return populationType;
	}
	
	public void setPopulationType(long populationType) {
		this.populationType = populationType;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public ArrayList getList() {
		return issuesList;
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue c) {

		this.issue = c;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public String getReachedStatusId() {
		return reachedStatusId;
	}

	public void setReachedStatusId(String reachedStatusId) {
		this.reachedStatusId = reachedStatusId;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "IssuesForm ( "
	        + super.toString() + TAB
	        + "issue = " + this.issue + TAB
	        + "issuesList = " + this.issuesList + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "statusId = " + this.statusId + TAB
	        + "channelId = " + this.channelId + TAB
	        + "subjectId = " + this.subjectId + TAB
	        + "priorityId = " + this.priorityId + TAB
	        + "reachedStatusId = " + this.reachedStatusId + TAB
	        + "actionTypeId = " + this.actionTypeId + TAB
	        + "searchBy = " + this.searchBy + TAB
	        + "populationType = " + this.populationType + TAB
	        + " )";

	    return retValue;
	}

	public String getSkinFilter() {
		if((skinList == null) ||(skinList!=null && skinList.size() == 1 && Integer.parseInt(skinList.get(0).toString()) == 0) || skinList.isEmpty()) { // ALL
			return Utils.getWriter().getSkinsToString();
		} else {
			return CommonUtil.convertToCSV(skinList);
		}
	}

	/**
	 * @return the byWriter
	 */
	public boolean isByWriter() {
		return byWriter;
	}

	/**
	 * @param byWriter the byWriter to set
	 */
	public void setByWriter(boolean byWriter) {
		this.byWriter = byWriter;
	}

	public void updateChannelId(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			String channelId = String.valueOf(ev.getNewValue());
			issueAction.setChannelId(channelId);
			switch (Integer.valueOf(channelId)) {
			case 2:
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
				break;
			case 3:
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
				break;
			case 4:
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
				break;
			case 8:
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_CHAT));
				break;
			default:
				break;
			}
		}
		context.renderResponse();
	}
	
	public void updateCopyopBlockRemoveReason(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			String reason = String.valueOf(ev.getNewValue());
			issueAction.setRiskReasonId(reason);
			if(reason.equals(String.valueOf(Issue.ISSUE_REASON_TYPE_BLOCK_COPYOP_USER)) || reason.equals(String.valueOf(Issue.ISSUE_REASON_TYPE_REMOVE_COPYOP_USER))){
				setCopyopBlockRemoveFields(true);
				if(user.getUserActiveData().getCopyopUserStatus() == UserStateEnum.STATE_REMOVED){
					setDisabledBlockRemoveReason(true);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.risk.issues.remove.copyop.user", null),null);
					context.addMessage(null, fm);
				}else if(user.getUserActiveData().getCopyopUserStatus() == UserStateEnum.STATE_BLOCKED){
					setDisabledBlockRemoveReason(true);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.risk.issues.block.copyop.user", null),null);
					context.addMessage(null, fm);
				}
			}else{
				setCopyopBlockRemoveFields(false);
				setDisabledBlockRemoveReason(false);
			}
		}
		context.renderResponse();
	}

	public void updatePhoneCallDirection(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			issueAction.setCallDirectionId(((Long)(ev.getNewValue())).longValue());
			if (issueAction.getCallDirectionId() == IssueAction.ISSUE_DIRECTION_IN_BOUND_PHONE_CALL) {  // for inBound calls set to reached status
				issueAction.setReachedStatusId(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED));
			}
		} else{
			issueAction.setCallDirectionId(IssueAction.ISSUE_NO_DIRECTION);
		}
		context.renderResponse();
	}

	public void updateReachedStatusId(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!CommonUtil.isParameterEmptyOrNull(String.valueOf(ev.getNewValue()))){
			issueAction.setReachedStatusId(String.valueOf(ev.getNewValue()));
		} else {
			issueAction.setReachedStatusId(null);
		}
		context.renderResponse();
	}

	public void updateCallBack() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		setCallBack(!isCallBack());
		context.renderResponse();
	}

	public void updateActionTypeId(ValueChangeEvent ev) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			issueAction.setActionTypeId(String.valueOf(ev.getNewValue()));
			if(ev.getNewValue().equals(ConstantsBase.ISSUE_ACTIVITY_PENDING_DOC)) {
				fillCreditCardList();
			}
		}
		context.renderResponse();
	}

	public boolean isPhoneCall() {
		if (issueAction != null){
			if (issueAction.getChannelId() != null && !issueAction.getChannelId().equals(ConstantsBase.EMPTY_STRING))
			{
				if (Long.parseLong(issueAction.getChannelId()) == IssuesManagerBase.ISSUE_CHANNEL_CALL) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isOutBoundCall() {
		if (isPhoneCall()){
			if ((issueAction.getCallDirectionId() == IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL) ||
				(issueAction.getCallDirectionId() == IssueAction.ISSUE_DIRECTION_MEMBER_REQUESTED_PHONE_CALL))
			 {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the issueAction
	 */
	public IssueAction getIssueAction() {
		return issueAction;
	}

	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueAction(IssueAction issueAction) {
		this.issueAction = issueAction;
	}

	/**
	 * @return the issueActionsList
	 */
	public ArrayList<IssueAction> getIssueActionsList() {
		return issueActionsList;
	}

	/**
	 * @param issueActionsList the issueActionsList to set
	 */
	public void setIssueActionsList(ArrayList<IssueAction> issueActionsList) {
		this.issueActionsList = issueActionsList;
	}

	/**
	 * @return the issuesList
	 */
	public ArrayList<Issue> getIssuesList() {
		return issuesList;
	}

	/**
	 * @param issuesList the issuesList to set
	 */
	public void setIssuesList(ArrayList<Issue> issuesList) {
		this.issuesList = issuesList;
	}

	public String exportIssuesResultToMail() throws IOException, SQLException, ParseException {
		String reportName = "issues_report_";
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		logger.info("Process " + reportName);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(from) + "_" + fmt.format(to) + ".csv";

		long currentWriterId = Utils.getWriter().getWriter().getId();

		// set defualt date for arabic partner
		if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
			String enumDate = CommonUtil.getEnum(
					Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
					Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			;
			if (from.before(startDateAr)) {
				from = startDateAr;
			}
		}

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter()
				.getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.issues.report.invalid.mail", null),null);
			context.addMessage(null, fm);
			return null;
		}

		IssuesReportSender sender = new IssuesReportSender(this, filename, ap ,wr, getSkinFilter());
		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				filename + " " + CommonUtil.getMessage("issues.report.processing.mail", null),null);
		context.addMessage(null, fm);

		return null;
	}

	public void getIssueActions(int issueType) throws SQLException{
		issue.setIssueActionsList(IssuesManager.getActionsByIssueId(issue.getId(), 0, false, issueType));
		updateCbRelevant(issue.getIssueActionsList());
	}
	
	private boolean allFilesSupportApproved(List<? extends com.anyoption.common.beans.File> list) {
		for(com.anyoption.common.beans.File f : list) {
			if(!f.isApproved() && !f.isNotNeeded()) {
					return false;
			}
		}
		return true;
	}
	
	private boolean allFilesControlApproved(List<? extends com.anyoption.common.beans.File> list) {
		for(com.anyoption.common.beans.File f : list) {
			if(!f.isControlApproved() && !f.isNotNeeded()) {
					return false;
			}
		}
		return true;
	}

	public String moveToControl() {

		// Check pending docs
		UserRegulationBase urb = new UserRegulationBase();
		urb.setUserId(issue.getUserId());
		try {
			long penDocs = PendingDocumentsManager.getPendingDocsNum(issue.getUserId());
			if (penDocs > 0) {
				logger.debug("Could not support move to Control user: " + userId + ", the user have pending docs: "
						+ penDocs);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support move to Control, the user have pending docs!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			}

			UserRegulationManager.getUserRegulation(urb);
			if (urb.getApprovedRegulationStep() == UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL) {
				logger.debug("Could not support move to Control user: " + userId
						+ ", the user is in STEP 6/REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL : " + urb);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support move to Control, the user is in Control!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			} else if (urb.getApprovedRegulationStep() == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER) {
				logger.debug("Could not support move to Control user: " + userId
						+ ", the user is in STEP 7/REGULATION_CONTROL_APPROVED_USER : " + urb);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support move to Control, the user is Approved by Control!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			} else if (urb.getApprovedRegulationStep() == UserRegulationBase.REGULATION_FIRST_DEPOSIT) {
				logger.debug("Could not support move to Control user: " + userId
						+ ", the user is in STEP 1/REGULATION_FIRST_DEPOSIT : " + urb);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support move to Control, the user must be fill questionnaire!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			} else if (urb.getApprovedRegulationStep() == UserRegulationBase.REGULATION_WC_APPROVAL) {
				logger.debug("Could not support move to Control user: " + userId
						+ ", the user is in STEP 1/REGULATION_FIRST_DEPOSIT : " + urb);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support move to Control, the user must be make FD!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			}

			urb.setApprovedRegulationStep(UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL);
			urb.setWriterId(Utils.getWriter().getWriter().getId());
			ArrayList<Issue> issuesList = new ArrayList<Issue>();
			issuesList = UserRegulationManager.getOpenedPendingDocsIssuesByUserId(urb.getUserId());
			if (issuesList != null) {
				try {
					UserRegulationManager.updateRegulationStep(urb);
					UsersManager.unlockUser(issue.getUserId(), false, Utils.getWriter().getWriter().getId());
					pendingDocumentsIssueActions(issuesList, urb);
					search();
				} catch (SQLException e) {
					logger.debug("Can't support approve issue: " + userId, e);
					FacesContext context = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Could not support approve issue!",
							null);
					context.addMessage(null, fm);
					return Constants.NAV_ISSUE_ACTIONS;
				}

				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Issue successfully moved to control",
						null);
				context.addMessage(null, fm);
				canMoveToControl = false;
				return Constants.NAV_ISSUE_ACTIONS;
			} else {
				logger.debug(
						"Can't support approve issue because there are no opened pending documents issues, userID: "
								+ userId);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Could not support approve issue, because there are no opened pending documents issues!", null);
				context.addMessage(null, fm);
				return Constants.NAV_ISSUE_ACTIONS;
			}
		} catch (Exception pe) {
			Log.error("When moveToControl ", pe);
			return Constants.NAV_ISSUE_ACTIONS;
		}
	}
	
	private void pendingDocumentsIssueActions(ArrayList<Issue> issuesList, UserRegulationBase urb) {
		try {
			String issueActionComments = "Moved to control by: " + CommonUtil.getWriterName(urb.getWriterId());
			for (Issue issue : issuesList) {
				issue.setStatusId(IssuesManagerBase.ISSUE_STATUS_FINISHED + "");
				IssuesManagerBase.insertUserRegulationIssueAction(urb.getUserId(),
						IssuesManagerBase.ISSUE_ACTION_TYPE_PENDING_DOCS + "", issueActionComments,
						IssuesManagerBase.ISSUE_PRIORITY_LOW + "", false, issue, false);
			}
		} catch (SQLException e) {
			logger.debug("Cannot get writer name: ", e);
		}
	}
	
	public String moveToReg() {
		if(getComment() == null || getComment().length() < 1) {
			FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Comment is mandatory field!", null);
            context.addMessage(null, fm);
            return Constants.NAV_ISSUE_ACTIONS;
		}
		
		UserRegulationBase urb = new UserRegulationBase();
		urb.setApprovedRegulationStep(UserRegulationBase.REGULATION_CONTROL_APPROVED_USER);
		urb.setUserId(issue.getUserId());
		urb.setWriterId(Utils.getWriter().getWriter().getId());
		urb.setComments(comment);
		try {
			UserRegulationManager.updateRegulationStep(urb);
		} catch (SQLException e) {
			logger.debug("Can't control approve issue: " + userId, e);
			FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Could not control approve issue!", null);
            context.addMessage(null, fm);
            return Constants.NAV_ISSUE_ACTIONS;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Issue successfully approved by control", null);
        context.addMessage(null, fm);
        canMoveToReg = false;
		return Constants.NAV_ISSUE_ACTIONS;
	}
	
	public String navigateToIssueActions() throws SQLException {
		getIssueActions(issue.getType());
		tempIssue = new Issue(issue);
		WriterWrapper ww = Utils.getWriter();
		issue.setComponentName(ww.getIssueComponentsHM().get(new Long(issue.getComponentId())).getName());
		long writerId = ww.getWriter().getId();
		
		UserRegulationBase	userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(issue.getUserId());
	   	UserRegulationManager.getUserRegulation(userRegulation);
	   	if(String.valueOf(ConstantsBase.ISSUE_SUBJECT_PENDING_DOC).equals(issue.getSubjectId())) {
		   	if(userRegulation.getApprovedRegulationStep() == UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE) {
		   		if(filesList.size() > 2) {
		   			if(allFilesSupportApproved(filesList)) {
		   				canMoveToControl = true;
		   			}
		   		}
		   	} else if (userRegulation.getApprovedRegulationStep() == UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL) {
		   		if(filesList.size() > 2) {
		   			if(allFilesControlApproved(filesList)) {
		   				canMoveToReg = true;
		   			}
		   		}
		   	} else {
		   		canMoveToControl = false;
		   		canMoveToReg = false;
		   	}
	   	}
	   	
		//regular issue action
		if (tempIssue.getStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED)) ||
			tempIssue.getStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED)) ||
			tempIssue.getStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS)) || String.valueOf(ConstantsBase.ISSUE_SUBJECT_PENDING_DOC).equals(issue.getSubjectId())){
				return Constants.NAV_ISSUE_ACTIONS;
		}
		//risk issue action
		//check for user id file
		if (null != issue.getIssueActionsList()){
			userFiles = UsersManager.getUserFiles(issue.getId(), 0);
			if (null != userFiles){
				if (userFiles.getUserIdFile().isApproved() && userFiles.getUserIdFile().getFileStatusId() == File.STATUS_DONE){
					isIdDocumentSent = true;
				}
				if(userFiles.getUserIdFile().getIssueActionsId() != 0){
					riskIdRequest = true;
				}
			} else {
				isIdDocumentSent = false;
				riskIdRequest = false;
			}
		}
		showAllCards = false;
		cardsList = TransactionsManager.getCCByUserId(issue.getUserId(), issue.getId(), 0);
		CommonUtil.setTablesToFirstPage();
		//create reason list on issue action
		String reasons = ConstantsBase.EMPTY_STRING;
		for (IssueAction i:issue.getIssueActionsList()){
			if (!CommonUtil.isParameterEmptyOrNull(i.getRiskReasonName()) && !reasons.contains(i.getRiskReasonName())){
				reasons += i.getRiskReasonName() +"<br/>";
			}
		}
		if (reasons.length() > 5){
			issue.setRiskReasons(reasons.substring(0,reasons.length()-5));
		}

		//Create cards list for additional requests
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isHasAdminRole() && user.isSupportSelected() || writerId == Constants.GADI_USER_ID || writerId == Constants.MARCELO_USER_ID || writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.DANYD_USER_ID || writerId == Constants.MARYVS_USER_ID || writerId == Constants.OANAD_USER_ID){
			tmpCardsList = TransactionsManager.getCCByUserId(riskUserId, 0, 0);
		}

		if (issue.getSubjectId().equals(Issue.ISSUE_SUBJECTS_COPYOP_BLOCK) || issue.getSubjectId().equals(Issue.ISSUE_SUBJECTS_COPYOP_REMOVE)) {
			copyopBlockRemoveFields = true;
		}

		return Constants.NAV_RISK_ISSUE_ACTIONS;
	}

	public String showNewActionFields()
	{
		isNewAction = true;
		issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_CALL));
		issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
		return null;
	}
	
	
	
	public String unblockUnremoveCopyopUser()throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String issueActionType = "";
		String lastIssueActionType = issue.getIssueActionsList().get(0).getActionTypeId();
		if(copyopBlockRemoveFields){
			if(lastIssueActionType.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK)){
				issueActionType = String.valueOf(IssueAction.ISSUE_ACTION_USER_COPYOP_UNBLOCK);
				IssuesManagerBase.updateCopyopUserStatus(user.getUserName(), user.getId(), issue, Utils.getWriter().getWriter().getId(), issueActionType);
			}else if(lastIssueActionType.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE)){
				issueActionType = String.valueOf(IssueAction.ISSUE_ACTION_USER_COPYOP_UNREMOVED);
				IssuesManagerBase.updateCopyopUserStatus(user.getUserName(), user.getId(), issue, Utils.getWriter().getWriter().getId(), issueActionType);
			}
		}
		UsersManager.loadUserStripFields(user, true, null);
		search();
		return Constants.NAV_ISSUES;
	}
	
	public String getIssueActionButtonName() {
		if (issue.getIssueActionsList().get(0).getActionTypeId().equals(IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK)) {
			return CommonUtil.getMessage("button.copyop.user.unblock", null);
		} else if (issue.getIssueActionsList().get(0).getActionTypeId().equals(IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE)) {
			return CommonUtil.getMessage("button.copyop.user.unremove", null);
		}
		return "";
	}
	
	public ArrayList<SelectItem> riskReasonList() throws SQLException{// TODO efrewsfrwe
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ArrayList<SelectItem> riskReasonList = null;
		WriterWrapper ww = new WriterWrapper();
		riskReasonList = ww.getRiskReasonSI();
		
		if(ProfileManager.getUserState(user.getId()) == null){
			Iterator it = riskReasonList.iterator();
			while(it.hasNext()){
				SelectItem item = (SelectItem) it.next();
				if(((String)item.getValue()).equals(String.valueOf(Issue.ISSUE_REASON_TYPE_REMOVE_COPYOP_USER))){
					it.remove();
				}
				if(((String)item.getValue()).equals(String.valueOf(Issue.ISSUE_REASON_TYPE_BLOCK_COPYOP_USER))){
					it.remove();
				}
			}
		}else if( ProfileManager.getUserState(user.getId()).getId() == UserStateEnum.STATE_REGULAR.getId() && ProfilesOnlineManager.isUserOnline(user.getId())){
			Iterator it = riskReasonList.iterator();
			while(it.hasNext()){
				SelectItem item = (SelectItem) it.next();
				if(((String)item.getValue()).equals(String.valueOf(Issue.ISSUE_REASON_TYPE_REMOVE_COPYOP_USER))){
					it.remove();
				}
			}
		}
		return riskReasonList;
	}

	
	
	public String hideNewActionFields()
	{
		isNewAction = false;
		issueAction = null;
		//reset cards list after hiding new action fields
		if (showAllCards){
			issue.setFilesRiskStatusId("");
			ArrayList<CreditCard> tmp = cardsList;
			cardsList = tmpCardsList;
			tmpCardsList = tmp;
			CommonUtil.setTablesToFirstPage();
			showAllCards = false;
		}
		return null;
	}

	/**
	 * @return the isNewAction
	 */
	public boolean isNewAction() {
		return isNewAction;
	}

	/**
	 * @param isNewAction the isNewAction to set
	 */
	public void setNewAction(boolean isNewAction) {
		this.isNewAction = isNewAction;
	}

	/**
	 * updates issue fields and/or insert new action according to fields.
	 */
	public String updateIssue() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		boolean res = false;
		boolean issueChanged = isIssueChanged();
		User tempUser = null;

		if (user.isRetentionSelected() || user.isRetentionMSelected() ||
				(user.isSupportSelected() && user.isSupportMOLocking()) ){
			long userId = populationEntry.getUserId();
//			long populationEntityType  = populationEntry.getEntityType();
//
			if (userId > 0){
				tempUser = UsersManager.getUserDetails(userId);
			} else {
				tempUser = new User();
				Contact contact = ContactsManager.getContactByID(populationEntry.getContactId());
				tempUser.setUtcOffset(contact.getUtcOffset());
			}

		} else {
			tempUser = user;
		}

		if (isNewAction){
			res = validateNewActionFields();
			if (res){
				issueAction.setWriterId(AdminManager.getWriterId());
				res = IssuesManager.updateIssue(issue, issueAction, issueChanged, tempUser, screenId);
			}
		} else {
			if(issueChanged){
				res = IssuesManager.updateIssue(issue, null, true, tempUser, screenId);
			}
		}

		if (res){
			isNewAction = false;
			getIssueActions(ConstantsBase.ISSUE_TYPE_REGULAR);
		}
		return null;
	}


	/**
	 * updates risk issue fields and/or insert new action according to fields.
	 */
	public String updateRiskIssue() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		WriterWrapper writer = context.getApplication().evaluateExpressionGet(context, Constants.BIND_WRITER_WRAPPER, WriterWrapper.class);
		boolean issueChanged = isIssueChanged();
		User tempUser = null;
		tempUser = user;
		boolean isRiskWriter = (writer.getWriter().getId() == Constants.ANNAS_USER_ID || writer.getWriter().getId() == Constants.AYAN_USER_ID || writer.getWriter().getId() == Constants.PANAGIOTAC_USER_ID || writer.getWriter().getId() == Constants.YAELJ_USER_ID || writer.getWriter().getId() == Constants.RONIN_USER_ID || writer.getWriter().getId() == Constants.WRITER_ANGELAD); //Anna & Aya & Panagiota & YaelJ

		if (!validateRiskActionFields()){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("risk.reason.mandatory", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

			return null;
		}
		riskIdRequest = UsersManager.verifyIsRiskIssue(riskUserId);
		if (isNewAction){
			int listSize = issue.getIssueActionsList().size() - 1;
			// if issue action type is contact user and there was reached call  - update status to done support
			if (null != issue.getIssueActionsList() && issue.getIssueActionsList().get(listSize).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))
					 && null != issueAction.getReachedStatusId() && issueAction.getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))) {
				issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_DONE_SUPPORT));
				issueChanged = true;
			}
			
			if (null != issue.getIssueActionsList() && !issue.getIssueActionsList().get(listSize).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){
				 updateFilesStatus(isRiskWriter);
				//check if we need to change issue status by files
				if (calculateIssueStatus()){
					issueChanged = true;
				}
			//contact user request
			} else if (null != issue.getIssueActionsList() && issue.getIssueActionsList().get(listSize).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){
				if (!CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId())){
					issue.setStatusId(issue.getFilesRiskStatusId());
					issueChanged = true;
				} else if (!isRiskWriter && (Integer.valueOf(issue.getStatusId()) == IssuesManagerBase.RISK_STATUS_NEW_ISSUE || Integer.valueOf(issue.getStatusId()) == IssuesManagerBase.RISK_STATUS_ADDITIONAL_REQUEST)){
					issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_IN_PROGRESS));
					issueChanged = true;
				}
			}
			
			if (issueStatusAfterNewAction()){
				issueChanged = true;
			}
			issueAction.setWriterId(AdminManager.getWriterId());
			IssuesManager.updateIssue(issue, issueAction, issueChanged, tempUser, screenId);
			//insert doc request and status
			if (null != issue.getIssueActionsList() && !issue.getIssueActionsList().get(listSize).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){
				insertDocRequestAndStatus(isRiskWriter);
			}
			if (null != issue.getIssueActionsList() && issue.getIssueActionsList().get(listSize).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))) {
				IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_CONTACT_USER);//create docs
			}
			
			insertFilesRequest(user.getId(), false);
			insertRelevantTo.clear();
		}

		isNewAction = false;
		//getIssueActions();
		search();

		return Constants.NAV_ISSUES;
	}


	private boolean validateNewActionFields(){
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm;
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		boolean res = true;

		// Check issue status change.
		if (null != tempIssue && Long.parseLong(issue.getStatusId()) == IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED) {
			if (Long.parseLong(tempIssue.getStatusId()) != IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.illegalstatus", null),null);
				context.addMessage("issuesForm:statusId", fm);
				res = false;
			}
		}
		if (null != tempIssue && Long.parseLong(issue.getStatusId()) == IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS) {
			if (Long.parseLong(tempIssue.getStatusId()) == IssuesManagerBase.ISSUE_STATUS_FINISHED) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.illegalstatus", null),null);
				context.addMessage("issuesForm:statusId", fm);
				res = false;
			}
		}

		if (issueAction != null){

			// check that the action from retention mangers belongs to the relevant Issue
			if (user.isRetentionSelected() || user.isRetentionMSelected() ||
					(user.isSupportSelected() && user.isSupportMOLocking()) ){
				PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
				long currEntryId = entry.getCurrEntryId();
				long issueEntryId = issue.getPopulationEntryId();

				if (entry.getPopulationUserId() != 0) {
					if (issueEntryId != 0){

						// check that user is in a population and the issue pop entry id = current entry id
						// or user isn't in a popualtion and his "old" entry id = issue pop entry id or his entry type is not general
						if ((0 != currEntryId && currEntryId != issueEntryId) ||
								(0 == currEntryId && (entry.getOldPopEntryId() != issueEntryId || entry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_GENERAL))) {

							fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.old.entry.update", null),null);
							context.addMessage(null, fm);

							res = false;
						}
					} else {
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.not.from.entry", null),null);
						context.addMessage(null, fm);

						res = false;
					}
				}
			}

			// Check comment change.
			if (issueAction.getComments()!= null){
				if (issueAction.getComments().length() > 2000){
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.commments.long", null),null);
					context.addMessage("issuesForm:newComments", fm);
					res = false;
				}
				if (issueAction.getComments().equals(ConstantsBase.EMPTY_STRING)){
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
					context.addMessage("issuesForm:newComments", fm);
					res = false;
				}
			}

			//Check if user is not active and issue action is wrong number
			if (user.getId() > 0 && !user.isActive() &&
					issueAction.getActionTypeId().equals(ConstantsBase.ISSUE_ACTION_TYPE_WRONG_NUMBER)) {
				if (!(user.getFailedCount() > ConstantsBase.FAILED_LOGIN_COUNT_LIMIT)) {
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("user.close.risk", null),null);
					context.addMessage(null, fm);
					res = false;
				}
			}

			// Check channel id, reached status and action type change.
			if (issueAction.getChannelId() != null && !issueAction.getChannelId().equals(ConstantsBase.EMPTY_STRING)) {
				PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
				if (Long.parseLong(issueAction.getChannelId()) == IssuesManagerBase.ISSUE_CHANNEL_CALL) {
					if (issueAction.getCallDirectionId() == 0){
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
						context.addMessage("issuesForm:callDirectionId", fm);
						res = false;

					//if user have have IssueActionTypeId with deposit on callback
					} if(!(CommonUtil.isParameterEmptyOrNull(issueAction.getActionTypeId()))
							  && (user.isRetentionSelected() || user.isRetentionMSelected() ||
										(user.isSupportSelected() && user.isSupportMOLocking()))
							  && (null != entry && entry.getPopulationUserId() != 0)
							  && entry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK){
							  if ((entry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_SPECIAL_CONVERSION ||
									  entry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_SPECIAL_RETENTION ||
									  entry.getCurrPopualtionTypeId() == 0)
								   && (null != issue && null != issue.getSubject())
								   && !(CommonUtil.isParameterEmptyOrNull(issue.getSubject().getName()))
								   && !(issue.getSubject().getName().contains(PopulationsManagerBase.POP_TYPE_SPECIAL_CB_DEPOSIT))
								   && isDepositActionType(Long.parseLong(issueAction.getActionTypeId()))) {
								String[] params = new String[1];
								params[0] = issueActionName;
								fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.reaction.deposit", params),null);
								context.addMessage("issuesForm:actionTypeId", fm);
								res = false;
							}

					}if (issueAction.getCallDirectionId() == IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL) {
						if (issueAction.getReachedStatusId().equals(ConstantsBase.EMPTY_STRING)) {
							fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
							context.addMessage("issuesForm:reachedStatusId", fm);
							res = false;
						}
						if(issueAction.getActionTypeId().equals(ConstantsBase.EMPTY_STRING)){
							fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
							context.addMessage("issuesForm:actionTypeId", fm);
							res = false;
						} else if (issueAction.isCallBack() && (user.isRetentionSelected() || user.isRetentionMSelected())){

							res = validateCallBack(context, user, res);

							if (res){
								//Unassigned CB with deposit during call reaction
								if(cbMoreThanWeek(issueAction, CommonUtil.getDateTimeFormaByTz(new Date(), issueAction.getCallBackTimeOffset()), user)){
									cancelAssign(issueAction, populationEntry);
								}
							}
						}
					}
				}

			} else {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
				context.addMessage("issuesForm:channelId", fm);
				res = false;
			}

		}
		return res;
	}


	/**
	 * Validate risk fields when inserting new risk issue
	 * @return
	 */
	public boolean validateRiskFields(){
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		FacesMessage fm;
		if (CommonUtil.isParameterEmptyOrNull(issueAction.getComments())){
			if (Integer.valueOf(issueAction.getActionTypeId()) == IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER){
				issueAction.setComments(Constants.IA_CONTACT_USER_COMMENT);
			} else {
				issueAction.setComments(Constants.IA_NEED_DUCUMENTS_COMMENT);
			}
		}

		if (issueAction.getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){
			return true;
		}
		for (CreditCard c: cardsList){
			if (c.getReqRiskIssueScreen().isPowerOfAttorney() || c.getReqRiskIssueScreen().isSentCcCopy() ||
					c.getReqRiskIssueScreen().isSentHolderIdCopy() || c.getReqRiskIssueScreen().isVerificationCall()){
				//Preventing from opening new issue for card that already have an open issue.
				if(user.getSkin().isRegulated()){
			 		if ((c.getRequest().isSentCcCopy()) && (c.getRequest().isPowerOfAttorney()||(c.getRequest().isSentHolderIdCopy()||c.getRequest().isVerificationCall()))){
						String[] params = new String[1];
						params[0] = String.valueOf(c.getCcNumber());
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("risk.error.already.exist", params),null);
						context.addMessage(null, fm);
						return false;
					} else {
						return true;
					}
				}else{
					if (c.getRequest().isPowerOfAttorney() || c.getRequest().isSentCcCopy() ||
							 (c.getRequest().isSentHolderIdCopy() || c.getRequest().isVerificationCall())){
						String[] params = new String[1];
						params[0] = String.valueOf(c.getCcNumber());
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("risk.error.already.exist", params),null);
						context.addMessage(null, fm);
						return false;	
					}else{
						return true;
					}
				}
			}
		}
		if (riskIdSelected){
			return true;
		}

		fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("risk.error.files", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);

		return false;
	}


	public String insertIssue() throws SQLException{
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		FacesMessage fm;
		boolean res = false;

		res = validateNewActionFields();
		issue.setContactId(user.getContactId());
		if (res){
			if (!user.isRetentionSelected() && !user.isRetentionMSelected() &&
					!(user.isSupportSelected() && user.isSupportMOLocking())) {
				issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
				issueAction.setWriterId(Utils.getWriter().getWriter().getId());
				res = IssuesManager.insertIssue(issue, issueAction, user, screenId);

				if (res == true && user.isSupportSelected()){
					UsersManager.loadUserStripFields(user, true, null);
				}
			}
			else {

				User retentionUser = null;

				if (populationEntry.getUserId() != 0){
					retentionUser = UsersManager.getUserDetails(populationEntry.getUserId());
				} else {
					retentionUser = new User();
					Contact contact = ContactsManager.getContactByID(populationEntry.getContactId());
					retentionUser.setUtcOffset(contact.getUtcOffset());

					long userId = contact.getUserId();

					// If userId in this contact is 0, look for userId by phone number
					if (0 == userId){
						userId = UsersManagerBase.getUserIdByPhone(contact.getPhone());
					}

					retentionUser.setId(userId);
				}

				try{
					issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
					issueAction.setWriterId(Utils.getWriter().getWriter().getId());
					res=IssuesManager.insertIssue(issue,issueAction,retentionUser, screenId);

				} catch (SQLException ex){
					// ORA-00001 - Unique constraint violated.
					// we can enter only one issue per entry trough Retention.
					if (ex.getErrorCode() == 1){
						fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.insert.failed.unique.issue.per.population.entry", null, Utils.getWriterLocale(context)),null);
						context.addMessage(null, fm);

						return null;
					}
					logger.error(ex);
				}

			}
			if (res){
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}else {
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.insert.failed", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

				return null;
			}
		} else {
			// If new action field are in valid show messages.
			return null;
		}

		search();
		return getNavigation();
	}

	public String insertRiskIssue() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		FacesMessage fm;
		
			if (copyopBlockRemoveFields) {
				String issueActionReason = "";
				if(issueAction.getRiskReasonId().equals(String.valueOf(Issue.ISSUE_REASON_TYPE_BLOCK_COPYOP_USER))){
					issueActionReason = IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK;
					IssuesManagerBase.updateCopyopUserStatus(user.getUserName(), user.getId(), issue, Utils.getWriter().getWriter().getId(), issueActionReason);
				} else if (issueAction.getRiskReasonId().equals(String.valueOf(Issue.ISSUE_REASON_TYPE_REMOVE_COPYOP_USER))){
					issueActionReason = IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE;
					IssuesManagerBase.updateCopyopUserStatus(user.getUserName(), user.getId(), issue, Utils.getWriter().getWriter().getId(), issueActionReason);
				}
				UsersManager.loadUserStripFields(user, true, null);
				search();
				return Constants.NAV_ISSUES;
			}
			
		boolean res = false;
		res = validateRiskFields();
		if (res){
			if (!user.isRetentionSelected() && !user.isRetentionMSelected() &&	
					!(user.isSupportSelected() && user.isSupportMOLocking())) {
					issue.setSubjectId(IssuesManagerBase.ISSUE_SUBJECT_RISK_ISSUE);
					issue.setType(ConstantsBase.ISSUE_TYPE_RISK);
					issue.setContactId(user.getContactId());
					issueAction.setWriterId(Utils.getWriter().getWriter().getId());
					if (issueAction.getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){
						res = IssuesManager.insertIssue(issue, issueAction, user, screenId);
						IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_CONTACT_USER);
					} else {
						calculateRiskIssueStatus();
						IssuesManager.insertIssue(issue, issueAction, user, screenId);
						insertFilesRequest(user.getId(), true);
					}
				}

			if (res){
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("issues.insert.failed", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

				return null;
			}
		} else {
			// If new action field are in valid show messages.
			return null;
		}

		search();
		return getNavigation();
	}


	private boolean isIssueChanged(){
		if(!issue.getStatusId().equals(tempIssue.getStatusId()) ||
				!issue.getPriorityId().equals(tempIssue.getPriorityId()) ||
				!issue.getSubjectId().equals(tempIssue.getSubjectId())){
			return true;
		}
		return false;
	}

	public String updateSignificant() throws SQLException{
		IssuesManager.updateSignificantAction(issueAction);
		getIssueActions(ConstantsBase.ISSUE_TYPE_REGULAR);
		return null;
	}

	/**
	 * @return the bySignificantNotes
	 */
	public boolean isBySignificantNotes() {
		return bySignificantNotes;
	}

	/**
	 * @param bySignificantNotes the bySignificantNotes to set
	 */
	public void setBySignificantNotes(boolean bySignificantNotes) {
		this.bySignificantNotes = bySignificantNotes;
	}

	/**
	 * @return the callDirectionId
	 */
	public long getCallDirectionId() {
		return callDirectionId;
	}

	/**
	 * @param callDirectionId the callDirectionId to set
	 */
	public void setCallDirectionId(long callDirectionId) {
		this.callDirectionId = callDirectionId;
	}

	/**
	 * @return the tempIssue
	 */
	public Issue getTempIssue() {
		return tempIssue;
	}

	/**
	 * @param tempIssue the tempIssue to set
	 */
	public void setTempIssue(Issue tempIssue) {
		this.tempIssue = tempIssue;
	}

	/**
	 * @return the issueSubjectsSI
	 */
	public ArrayList<SelectItem> getIssueSubjectsSI() {
		return issueSubjectsSI;
	}

	/**
	 * @param issueSubjectsSI the issueSubjectsSI to set
	 */
	public void setIssueSubjectsSI(ArrayList<SelectItem> issueSubjectsSI) {
		this.issueSubjectsSI = issueSubjectsSI;
	}


	public boolean isCallBack(){
		if (null == issueAction){
			return false;
		}
		return issueAction.isCallBack();
	}

	/**
	 * @param isCallBack the isCallBack to set
	 * @throws SQLException
	 */
	public void setCallBack(boolean isCallBack) throws SQLException {
		if (isCallBack) {
			if (issueAction.getCallBackTimeOffset() == null){

				PopulationEntry lockedEntry = null;
				try {
					 lockedEntry = PopulationsManager.getLockedEntryByLockHisId(populationEntry.getLockHistoryId());
				} catch (PopulationHandlersException e) {
					logger.error("setCallBack: Can't get locked entry for pop user" + populationEntry.getPopulationUserId());
				}

				long userId = issue.getUserId();
				if (lockedEntry != null){
					issueAction.setCallBackTimeOffset(lockedEntry.getCountryOffset());
				}else if (userId != 0){
					issueAction.setCallBackTimeOffset(Utils.getUser(userId).getUtcOffset());
				}else{
					issueAction.setCallBackTimeOffset(Utils.getWriter().getUtcOffset());
				}
			}
		}
		issueAction.setCallBack(isCallBack);
	}

	// Display call back option only after reached status was chosen and a reaction has been selected
	public boolean isDisplayCallBack(){
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.isRetentionSelected() || user.isRetentionMSelected()){
			if (isPhoneCall()){
				String reachedStatus = issueAction.getReachedStatusId() ;

				if (reachedStatus != null &&
						reachedStatus.equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))){
						String reaction = issueAction.getActionTypeId();
						if (null != reaction &&
								!reaction.equals(ConstantsBase.EMPTY_STRING)){
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public ArrayList<SelectItem> getRetentionWriters() {
		if (null != populationEntry){
			return WriterWrapper.getRetentionWritersSortedAll(populationEntry.getSkinId());
		}
		return null;
	}

	/**
	 * Check if ActionType is a deposit ActionType
	 * @param issueActionTypeId
	 * @return true if ActionType is deposit ActionType
	 */
	private boolean isDepositActionType(long issueActionTypeId){
		IssueActionType iat = ApplicationData.getActionTypesHm().get(issueActionTypeId);
         if (iat != null && iat.getDepositSearchType() > 0){
        	 issueActionName = CommonUtil.getMessage(iat.getName(),null);
        	 return true;
          }
		return false;
	}

	/**
	 * Update relevant CB for issues action
	 * @param ia
	 */
	public void updateCbRelevant(ArrayList<IssueAction> ia){
		boolean tmp;

		for (IssueAction list: ia){
			tmp = list.updateCallBackRelevant();
			if (tmp == true){
				cbRelevant = true;
			}
		}
	}

	/**
	 * @return the cbRelevant
	 */
	public boolean isCbRelevant() {
		return cbRelevant;
	}

	/**
	 * @param cbRelevant the cbRelevant to set
	 */
	public void setCbRelevant(boolean cbRelevant) {
		this.cbRelevant = cbRelevant;
	}

	/**
	 * Check if there are more than 2 consecutive call backs
	 * @param iaList
	 * @return true if there is more than 2 consecutive cb and false otherwise.
	 */
	public boolean isConsecutiveCB(ArrayList<IssueAction> iaList){
		int iaListSize = iaList.size();
		if (iaListSize > 1){ // checking the last 2 issue actions
			Date dateOne = iaList.get(0).getCallBackDate();
			Date dateTwo = iaList.get(1).getCallBackDate();
			 if (dateOne != null
					 && iaList.get(0).getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))
					 && dateTwo != null
					 && iaList.get(1).getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))){
				 return true;
			 }
		}
	 return false;
	}

	/**
	 * Unassign callback that scheduled more than a week
	 * @param issueAction
	 * @param currentTimeWithCallBackOffset
	 * @param populationEntry
	 */
	public boolean cbMoreThanWeek(IssueAction issueAction, Date currentTimeWithCallBackOffset, UserBase user){

		Date callBackTimeAfterOffset = CommonUtil.getDateTimeFormaByTz(issueAction.getCallBackDateAndTime(),issueAction.getCallBackTimeOffset());
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(currentTimeWithCallBackOffset);
		//Set 2 weeks for partner writers
		if (user.getSkinId() == Skin.SKIN_GERMAN){
			gc.add(GregorianCalendar.DAY_OF_MONTH,14);
		} else {
			gc.add(GregorianCalendar.DAY_OF_MONTH,7);
		}
		if (callBackTimeAfterOffset.after(gc.getTime()) && populationEntry.isAssignToWriter()){
			return true;
		}
		return false;
   }

	/**
	 * Cancel assign
	 * @param issueAction
	 * @param populationEntry
	 */
	public void cancelAssign(IssueAction issueAction,PopulationEntry populationEntry){
		try {
			FacesContext context=FacesContext.getCurrentInstance();
			PopulationEntriesManager.unLockEntry(populationEntry);
			PopulationEntriesManager.cancelAssign(populationEntry, true, false);
			logger.info("Cancel assign for population user id " + populationEntry.getPopulationUserId());
			//for message after cancel assign - user move to tab callback
			PopulationEntry popEntrySession = new PopulationEntry();
			popEntrySession.setEntryTypeId(Constants.POP_ENTRY_TYPE_CALLBACK);
			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRY, PopulationEntry.class);
			ve.getValue(context.getELContext());
			ve.setValue(context.getELContext(), popEntrySession);
		} catch (PopulationHandlersException e) {
			logger.error("Can't cancel Assign"+issueAction.getActionId()+e.getMessage());
		} catch (SQLException e) {
			logger.error("Can't cancel Assign"+issueAction.getActionId()+e.getMessage());
		}
	}

	/**
	 * @param iaList
	 * @param issueAction
	 * @return
	 */
	public IssueAction isCallBackInterval(IssueAction issueAction){
		Date currnetIaCbTime = CommonUtil.getDateTimeFormat(issueAction.getCallBackDateAndTime(), issueAction.getCallBackTimeOffset());

		try{
			return IssuesManager.searchForAssignCallBackByWriter(Utils.getWriter().getWriter().getId(), currnetIaCbTime);
		}
		catch (Exception e) {
			logger.error("Can't get Call Back TIme");
		}
		return null;
   }

	/**
	 * @param issueAction
	 * @return
	 */
	public boolean isCallBackReasonableHour(IssueAction issueAction){

		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		int hoursListSkinId = new Long(user.getSkinId()).intValue();
		ArrayList<String> weekHoursList = null;
		Date callBackTimeGmt = CommonUtil.getDateTimeFormat(issueAction.getCallBackDateAndTime(), issueAction.getCallBackTimeOffset());
		Date callBackOnIsraelTime = CommonUtil.getDateTimeFormaByTz(callBackTimeGmt,CommonUtil.getDefualtUtcOffset());
		GregorianCalendar gcIsraelDayOfCb = new GregorianCalendar();
		gcIsraelDayOfCb.setTime(callBackOnIsraelTime);

		weekHoursList = ApplicationData.getCallCenterHours().get(hoursListSkinId);

		if (null == weekHoursList){
			weekHoursList = ApplicationData.getCallCenterHours().get(Skin.SKINS_DEFAULT);
		}

		if (null != weekHoursList){

			int dayOfWeek = gcIsraelDayOfCb.get(GregorianCalendar.DAY_OF_WEEK);
			String hoursStr = weekHoursList.get(dayOfWeek-1);

			if (isCbBetweenHours(callBackOnIsraelTime, hoursStr, gcIsraelDayOfCb)){
				return true;
			}else{
				gcIsraelDayOfCb.add(GregorianCalendar.DAY_OF_MONTH, -1);
				dayOfWeek = gcIsraelDayOfCb.get(GregorianCalendar.DAY_OF_WEEK);
				hoursStr = weekHoursList.get(dayOfWeek-1);

				return isCbBetweenHours(callBackOnIsraelTime, hoursStr, gcIsraelDayOfCb);
			}
		}else{
			logger.error("weekHoursList wasn't found");
		}

		return false;
	}

	private boolean isCbBetweenHours(Date callBackOnIsraelTime, String hoursStr, GregorianCalendar gcIsraelDayOfCb){

		if (hoursStr != null){
		  	String[] hours = hoursStr.split("-");

			GregorianCalendar gcStartTime = (GregorianCalendar)gcIsraelDayOfCb.clone();
			GregorianCalendar gcEndTime = (GregorianCalendar)gcIsraelDayOfCb.clone();
			Date startTime = null;
			Date endTime = null;

			gcStartTime.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(hours[0].substring(0, 2)));
			gcStartTime.set(GregorianCalendar.MINUTE, Integer.parseInt(hours[0].substring(3, 5)));
			gcStartTime.set(GregorianCalendar.SECOND,0);
			startTime = gcStartTime.getTime();

			gcEndTime.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(hours[1].substring(0, 2)));
			gcEndTime.set(GregorianCalendar.MINUTE, Integer.parseInt(hours[1].substring(3, 5)));
			gcEndTime.set(GregorianCalendar.SECOND,0);

			if (gcEndTime.getTime().before(startTime)){
				gcEndTime.add(GregorianCalendar.DAY_OF_MONTH, 1);
			}
			endTime = gcEndTime.getTime();

			return (callBackOnIsraelTime.after(startTime) && callBackOnIsraelTime.before(endTime)
					|| callBackOnIsraelTime.equals(startTime));
		}

		return false;
	}


	public long getWriterFilter() {
		return writerFilter;
	}

	public void setWriterFilter(long writerFilter) {
		this.writerFilter = writerFilter;
	}

	public String embedChat() {
		if (null != issue && issue.getChat() != null) {
			FacesContext context = FacesContext.getCurrentInstance();
			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{issueChat}", Issue.class);
			ve.getValue(context.getELContext());
			ve.setValue(context.getELContext(), issue);
		} else {
			logger.info("Chat content was not found!");
		}
		return null;
	}


	public ArrayList<SelectItem> getActriviListHM() {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		Writer writer = Utils.getWriter().getWriter();
		long writerId = writer.getId();
		if (notShowTreatmanIssAction()) {
			// remove ISSUE_ACTION_REMOVE_FROM_TREATMENT when user is not suspended
			ArrayList<SelectItem> actriviListHMFilter = new ArrayList<SelectItem>();
			for (SelectItem st : getIssueActionTypesHM(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY)) {
				if (Integer.parseInt(st.getValue().toString()) != IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_TREATMENT &&
						Integer.parseInt(st.getValue().toString()) != IssuesManagerBase.ISSUE_ACTION_DEACTIVATE_ACCOUNT) {
					actriviListHMFilter.add(st);
				} else if (Integer.parseInt(st.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_DEACTIVATE_ACCOUNT &&
						(writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.OFIRS_USER_ID ||
						writerId == Constants.WRITER_FORTUNT || writerId == Constants.WRITER_SHLOMITM ||
						writerId == Constants.WRITER_ROMANL || writerId == Constants.MARCELO_USER_ID ||
						writerId == Constants.REUT_USER_ID || writerId == Constants.RONIN_USER_ID ||
						writerId == Constants.WRITER_ANGELAD || writerId == Constants.MARYVS_USER_ID ||
						writerId == Constants.OANAD_USER_ID || writerId == Constants.AYAN_USER_ID ||
						writerId == Constants.GADI_USER_ID || writerId == Constants.WRITER_ANNAMU1 ||
						writerId == Constants.WRITER_PETARK || writerId == Constants.WRITER_EVGENIAD)) {
					actriviListHMFilter.add(st);
				}
			}
			return filterIssueActionTypes(actriviListHMFilter,user, writer);
		} else {
			ArrayList<SelectItem> actriviListHMFilter = new ArrayList<SelectItem>();
			for (SelectItem st : getIssueActionTypesHM(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY)) {
				if (Integer.parseInt(st.getValue().toString()) != IssuesManagerBase.ISSUE_ACTION_DEACTIVATE_ACCOUNT) {
					actriviListHMFilter.add(st);
				} else if (Integer.parseInt(st.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_DEACTIVATE_ACCOUNT &&
						(writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.OFIRS_USER_ID ||
						writerId == Constants.WRITER_FORTUNT || writerId == Constants.WRITER_SHLOMITM ||
						writerId == Constants.WRITER_ROMANL || writerId == Constants.MARCELO_USER_ID ||
						writerId == Constants.REUT_USER_ID || writerId == Constants.RONIN_USER_ID ||
						writerId == Constants.WRITER_ANGELAD || writerId == Constants.MARYVS_USER_ID ||
						writerId == Constants.OANAD_USER_ID || writerId == Constants.AYAN_USER_ID ||
						writerId == Constants.GADI_USER_ID || writerId == Constants.WRITER_ANNAMU1 ||
						writerId == Constants.WRITER_PETARK || writerId == Constants.WRITER_EVGENIAD ||
						writerId == 635)) {
					actriviListHMFilter.add(st);
				}
			}
			return  filterIssueActionTypes(actriviListHMFilter,user, writer);
		}
	}
    /**
     * @return the false if user is Suspended
     */
    private boolean notShowTreatmanIssAction() {
        boolean res = true;
        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication()
                .createValueBinding(Constants.BIND_USER).getValue(context);
        if (user.getSkin().isRegulated()) {
            try {
                UserRegulationBase userRegulation = new UserRegulationBase();
                userRegulation.setUserId(user.getId());
                UserRegulationManager.getUserRegulation(userRegulation);
                if(userRegulation.isSuspended()){
                    res = false;
                }
            } catch (SQLException e) {
                logger.error("Can't get userRegulation:", e);
            }
        }
        return res;
    }

	public ArrayList<SelectItem> getReactionListHM(){
		return getIssueActionTypesHM(IssuesManagerBase.ISSUE_CHANNEL_CALL);
	}


	/**
	 * Get reaction and activities lists by channel id
	 * @param channelId
	 * @return
	 */
	public ArrayList<SelectItem> getIssueActionTypesHM(long channelId){
		ArrayList<IssueActionType> list = issueActionTypes.get(screenId).get(channelId);
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		for (IssueActionType iat: list){
			if (channelId == IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY) { // Activities
				si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
			} else { // Reactions
				if (!CommonUtil.isParameterEmptyOrNull(issueAction.getReachedStatusId()) && !issueAction.getReachedStatusId().equalsIgnoreCase("null") && // after JSF2 we have null as String
						Long.valueOf(issueAction.getReachedStatusId()) == IssuesManagerBase.ISSUE_REACHED_STATUS_NOT_REACHED){
					if (iat.getReachedStatusId() == IssuesManagerBase.ISSUE_REACHED_STATUS_NOT_REACHED){
						si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
					}
				} else {
					if (!CommonUtil.isParameterEmptyOrNull(issueAction.getReachedStatusId()) && !issueAction.getReachedStatusId().equalsIgnoreCase("null") && //// after JSF2 we have null as String
							iat.getReachedStatusId() == IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED){
						si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
					}
				}
			}
		}
		Collections.sort(si, new CommonUtil.selectItemComparator());
		return si;
	}

	/**
	 * @return the actionTypeId
	 */
	public String getActionTypeId() {
		return actionTypeId;
	}

	/**
	 * @param actionTypeId the actionTypeId to set
	 */
	public void setActionTypeId(String actionTypeId) {
		this.actionTypeId = actionTypeId;
	}

	/**
	 * @return the localCallBackTimeTxt
	 */
	public String getLocalCallBackTimeTxt() {
		Date gmtCallBackTime = CommonUtil.getDateTimeFormat(issueAction.getCallBackDateAndTime(), issueAction.getCallBackTimeOffset());
		Date localCallBackTime = CommonUtil.getDateTimeFormaByTz(gmtCallBackTime,Utils.getWriter().getUtcOffset());
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String localTime = fmt.format(localCallBackTime);
		return localTime;
	}

//	public void updateCallBackFields(ValueChangeEvent ev){
//		FacesContext context=FacesContext.getCurrentInstance();
//		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//
//		validateCallBack(context, user, true);
//	}

	public String updateCallBackFields(){
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		validateCallBack(context, user, true);
		return null;
	}

	private boolean validateCallBack(FacesContext context, User user,boolean res){
		FacesMessage fm;

		if (issueAction.isCallBack() && (user.isRetentionSelected() || user.isRetentionMSelected())){
			//Call back time should be checked only if we are on retenetion (and the reaction time is Call Back)
			if (CommonUtil.getDateTimeFormaByTz(new Date(), issueAction.getCallBackTimeOffset()).after(issueAction.getCallBackDateAndTime())) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.date.in.the.past", null),null);
				context.addMessage("issuesForm:callBackDate", fm);
				res = false;
			}

				//Check if there is more than 2 consecutive call backs for representive
			if (!user.isRetentionMSelected() && isConsecutiveCB(issue.getIssueActionsList())) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.consecutiv.cb", null),null);
				context.addMessage("issuesForm:callBackDate", fm);
				res = false;
			}

			//Check 30 minutes interval between cb for rep
			else if (!user.isRetentionMSelected()){
				IssueAction tmpIa = isCallBackInterval(issueAction);
				if (null != tmpIa){ // if found cb with less than 30 minutes interval
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.interval", null),null);
					context.addMessage("issuesForm:callBackDate", fm);
					res = false;
				}
			}

			  //Callback hour is not reasonable hour
			if (res && !isCallBackReasonableHour(issueAction)) {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.issue.reasonableHour", null),null);
				context.addMessage("issuesForm:callBackDate", fm);
				res = false;
			}
		}
		return res;
	}

	/**
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateRequestChange (ValueChangeEvent event) throws SQLException, ParseException{
		String requestId = (String)event.getNewValue();
		if (requestId != null && requestId.equalsIgnoreCase(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT))){
			isDocumentAvailable = true;
		} else {
			isDocumentAvailable = false;
		}
	}

	/**
	 * Collapse/Expand risk issues list by user Id
	 * @return
	 * @throws SQLException
	 */
	public String updateCollapseExpand() throws SQLException{
		if (issue.getExpandCollapseRI() == Constants.RISK_ISSUES_COLLAPSE){
			ArrayList<Issue> tmpList = new ArrayList<Issue>();
			tmpList.addAll(issuesList);
			issue.setExpandCollapseRI(Constants.RISK_ISSUES_EXPAND);
			issuesList.clear();
			//Build Risk issues list with all users risk issues
			for (Issue i:tmpList){
				issuesList.add(i);
				if (i.getUserId() == issue.getUserId()){
					issuesList.addAll(issuesByUserId.get(issue.getUserId()));
				}
			}
		} else {
			//Remove all user risk issues, only the first risk issue will display
			issue.setExpandCollapseRI(Constants.RISK_ISSUES_COLLAPSE);
			issuesList.removeAll(issuesByUserId.get(issue.getUserId()));
		}
		return null;
	}

	public String searchRisk() throws SQLException {
		ArrayList<Issue> tmpList;
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		tmpList = IssuesManager.getRiskIssues(getSkinFilter(), priorityId, statusId, subjectId, userName, from, to, user.isAdminSelected(), getStatusToString(), classId, userId, riskIssueType , searchBy);
		//All risk issues per user for expand/collapse
		issuesByUserId = new HashMap<Long, ArrayList<Issue>>();
		HashMap <Long,Integer> riskList = new HashMap<Long, Integer>();
		issuesList.clear();
		int index = 0;
		for (Issue i:tmpList){
			//For new user adding his first risk issue
			if (issuesByUserId.get(i.getUserId()) == null){
				issuesByUserId.put(i.getUserId(), new ArrayList<Issue>());
				issuesList.add(i);
				riskList.put(i.getUserId(),index);
				index++;
			} else {
				//Collet users risk issues for expanding
				issuesList.get(riskList.get(i.getUserId())).setExpandCollapseRI(Constants.RISK_ISSUES_COLLAPSE);
				issuesByUserId.get(i.getUserId()).add(i);
			}
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	public String addNewRiskIssue() throws SQLException{
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();
		issue = new Issue();
		issueAction = new IssueAction();
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		tempIssue = null;
		issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_NEW_ISSUE));
		issue.setFilesRiskStatusId(String.valueOf(IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE));
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT));
		issueAction.setCallDirectionId(0);
		cardsList = TransactionsManager.getCCByUserId(riskUserId, 0, 0);
		if (null == idFile){
			idFile = UsersManager.getSpecificFile(riskUserId, FileType.USER_ID_COPY, 0, 0);
			if (null != idFile){
				if (idFile.isApproved() && idFile.getFileStatusId() == File.STATUS_DONE){
					isIdDocumentSent = true;
				}
				//if(idFile.getIssueActionsId() != 0){
					//riskIdRequest = true;
				//}
				
			}
		}
		riskIdRequest = UsersManager.verifyIsRiskIssue(riskUserId);
		ArrayList<SelectItem> riskReasonList = riskReasonList();
		setRiskReasonsList(riskReasonList);
		CommonUtil.setTablesToFirstPage();
		return Constants.NAV_RISK_ISSUE;
	}

	public ArrayList<SelectItem> getRiskStatus() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		WriterWrapper w = new WriterWrapper();
		return null;
	}

	public ArrayList<SelectItem> getRequestList(){
		screenId = ConstantsBase.SCREEN_ADMIN;
		ArrayList<IssueActionType> list = issueActionTypes.get(screenId).get(Long.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		for (IssueActionType iat: list){
			si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
		}
		return si;
	}


	/**
	 * @return the isDocumentAvailable
	 */
	public boolean isDocumentAvailable() {
		return isDocumentAvailable;
	}

	/**
	 * @param isDocumentAvailable the isDocumentAvailable to set
	 */
	public void setDocumentAvailable(boolean isDocumentAvailable) {
		this.isDocumentAvailable = isDocumentAvailable;
	}

	/**
	 * @return the riskUserId
	 */
	public long getRiskUserId() {
		return riskUserId;
	}

	/**
	 * @param riskUserId the riskUserId to set
	 */
	public void setRiskUserId(long riskUserId) {
		this.riskUserId = riskUserId;
	}

	/**
	 * @return the isIdDocumentSent
	 */
	public boolean isIdDocumentSent() {
		return isIdDocumentSent;
	}

	public int getCardsListSize() {
		return cardsList.size();
	}

	/**
	 * @return the cardsList
	 */
	public ArrayList getCardsList() {
		return cardsList;
	}

	/**
	 * @param cardsList the cardsList to set
	 */
	public void setCardsList(ArrayList cardsList) {
		this.cardsList = cardsList;
	}

	/**
	 * @return the riskIdRequest
	 */
	public boolean isRiskIdRequest() {
		return riskIdRequest;
	}

	/**
	 * @param riskIdRequest the riskIdRequest to set
	 */
	public void setRiskIdRequest(boolean riskIdRequest) {
		this.riskIdRequest = riskIdRequest;
	}

	/**
	 * @return the riskIdSelected
	 */
	public boolean isRiskIdSelected() {
		return riskIdSelected;
	}

	/**
	 * @param riskIdSelected the riskIdSelected to set
	 */
	public void setRiskIdSelected(boolean riskIdSelected) {
		this.riskIdSelected = riskIdSelected;
	}

	public boolean getIsSupportRepresentative(){
		return Utils.getWriter().getWriter().isSupportEnable();
	}

	/**
	 * @return the statuses
	 */
	public List<String> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userFiles
	 */
	public UserFiles getUserFiles() {
		return userFiles;
	}

	/**
	 * @param userFiles the userFiles to set
	 */
	public void setUserFiles(UserFiles userFiles) {
		this.userFiles = userFiles;
	}

	/**
	 * @return the insertRelevantTo
	 */
	public List<String> getInsertRelevantTo() {
		return insertRelevantTo;
	}

	/**
	 * @param insertRelevantTo the insertRelevantTo to set
	 */
	public void setInsertRelevantTo(List<String> insertRelevantTo) {
		this.insertRelevantTo = insertRelevantTo;
	}

	/**
	 * @return the showRelevantTo
	 */
	public String getShowRelevantTo() {
		return showRelevantTo;
	}

	/**
	 * @param showRelevantTo the showRelevantTo to set
	 */
	public void setShowRelevantTo(String showRelevantTo) {
		this.showRelevantTo = showRelevantTo;
	}

	/**
	 * @return the riskIssueToUserFiles
	 */
	public Issue getRiskIssueToUserFiles() {
		return riskIssueToUserFiles;
	}

	/**
	 * @param riskIssueToUserFiles the riskIssueToUserFiles to set
	 */
	public void setRiskIssueToUserFiles(Issue riskIssueToUserFiles) {
		this.riskIssueToUserFiles = riskIssueToUserFiles;
	}

	/**
	 * @return the showAllCards
	 */
	public boolean isShowAllCards() {
		return showAllCards;
	}

	/**
	 * @param showAllCards the showAllCards to set
	 */
	public void setShowAllCards(boolean showAllCards) {
		this.showAllCards = showAllCards;
	}

	/**
	 * @return the issueType
	 */
	public long getIssueType() {
		return issueType;
	}

	/**
	 * @param issueType the issueType to set
	 */
	public void setIssueType(long issueType) {
		this.issueType = issueType;
	}

	/**
	 * @return the riskIssueType
	 */
	public long getRiskIssueType() {
		return riskIssueType;
	}

	/**
	 * @param riskIssueType the riskIssueType to set
	 */
	public void setRiskIssueType(long riskIssueType) {
		this.riskIssueType = riskIssueType;
	}

	/**
	 * @return the riskReasonId
	 */
	public String getRiskReasonId() {
		return riskReasonId;
	}

	/**
	 * @param riskReasonId the riskReasonId to set
	 */
	public void setRiskReasonId(String riskReasonId) {
		this.riskReasonId = riskReasonId;
	}

	/**
	 * Get statuses list to string
	 * @param list
	 * @return
	 */
	public String getStatusToString(){
		if (statuses == null || statuses.size() == 0) {
			if (null != statuses){
				statuses.add("0");
			}
			return "0";
		}

		String tmp = ConstantsBase.EMPTY_STRING;
		for (int i=0; i<statuses.size(); i++) {
			tmp += statuses.get(i)+",";
		}
		return tmp.substring(0,tmp.length()-1);
	}

	/**
	 * Prepare the requested file in order to insert it to DB
	 * @param card
	 * @param fileTypeId
	 * @return
	 * @throws SQLException
	 */
	public File prepareFile(CreditCard card, String fileTypeId) throws SQLException{
		File file = new File();
		file.setFileStatusId(File.STATUS_REQUESTED);
		file.setUserId(issue.getUserId());
		file.setTimeCreated(new Date());
		file.setUtcOffsetCreated(Utils.getWriter().getUtcOffset());
		file.setWriterId(Utils.getWriter().getWriter().getId());
		file.setCcId(card.getId());
		file.setIssueActionsId(issueAction.getActionId());
		file.setFileTypeId(Long.valueOf(fileTypeId));
		return file;
	}

	public ArrayList<SelectItem> getRelevantToList(){
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		if (CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId()) || !issue.getFilesRiskStatusId().equals(String.valueOf(IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST))){
			si.add(new SelectItem("0", "All"));
		}
		if (riskIdRequest || showAllCards){
			si.add(new SelectItem("1", "ID"));
		}
		int index = 0;
		for (CreditCard c: cardsList){ // ccId and file status Id
			si.add(new SelectItem(String.valueOf(c.getId()+ ":" + c.getFilesRiskStatusId()) + ":" + index++ ,   c.getCcNumberLast4() + " " + c.getType().getName()));
		}
		return si;
	}

	public String navigateToRiskUsersFiles() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String path;
		if (user.isAdminSelected()){
			path = "admin";
		} else {
			path = "support_more_options";
		}
		response.sendRedirect(request.getContextPath() + "/jsp/" + path + "/riskUsersFiles.jsf?riskUserId=" + String.valueOf(riskIssueToUserFiles.getUserId()) +
				"&riskUserName=" + riskIssueToUserFiles.getUsername());
		return null;
	}

	public void calculateRiskIssueStatus(){
		switch (Integer.valueOf(issue.getFilesRiskStatusId())) {
		case IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_NEW_ISSUE));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_IN_PROGRESS));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_ADDITIONAL_REQUEST));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_DONE_SUPPORT_UNREACHED));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_DONE_SUPPORT));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_NEED_APPROVE:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_NEED_APPROVE));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_CLOSED:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_CLOSED));
			break;
		case IssuesManagerBase.FILES_RISK_STATUS_DONE_UNCOOPERATIVE:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_DONE_UNCOOPERATIVE));
			break;
		default:
			issue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_NEW_ISSUE));
			break;
		}
	}


	/**
	 * Return IA risk statuses
	 * @return
	 */
	public ArrayList<SelectItem> getIaRiskStatuses() {
		issue.getFilesRiskStatusId();
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		WriterWrapper ww = Utils.getWriter();
		long writerId = ww.getWriter().getId();
		if (writerId == Constants.MARCELO_USER_ID || writerId == Constants.ANNAS_USER_ID || writerId == Constants.REUT_USER_ID || writerId == Constants.AYAN_USER_ID || writerId == Constants.PANAGIOTAC_USER_ID || writerId == Constants.YAELJ_USER_ID || user.isHasSAdminRole() || writerId == Constants.RONIN_USER_ID || writerId == Constants.MARYVS_USER_ID || writerId == Constants.OANAD_USER_ID || writerId == Constants.WRITER_ANGELAD){  // marcelo/reut/anna/panagiota/yaelj/SAdmin
			if(copyopBlockRemoveFields){
				for(SelectItem select: ww.getRiskStatusesSI()){
					if(Integer.parseInt(select.getValue().toString()) == IssuesManagerBase.RISK_STATUS_CLOSED || Integer.parseInt(select.getValue().toString()) == IssuesManagerBase.RISK_STATUS_NEW_ISSUE){
						list.add(select);
					}
				}
				return list;
			} 
			return ww.getRiskStatusesSI();
		}
		if(writerId == Constants.GADI_USER_ID){//gadiel	        
			for(SelectItem select: ww.getRiskStatusesSI()){
				if(Integer.parseInt(select.getValue().toString()) != IssuesManagerBase.RISK_STATUS_CLOSED){
					list.add(select);
				}
			}			
			return list;			
		}
		if(writerId == Constants.ADI_KOREN_USER_ID || writerId == Constants.DANYD_USER_ID || writerId == Constants.YUSUFH_USER_ID || writerId == Constants.FANIS_USER_ID || writerId == Constants.IDOA_USER_ID || writerId == Constants.VANESSAF_USER_ID || writerId == Constants.OFIRS_USER_ID) { //ADIK/DANYD/YUSUFH/FANIS/IDOA/VANESSAF
			for(SelectItem select: ww.getRiskStatusesSI()){
				if(Integer.parseInt(select.getValue().toString()) != IssuesManagerBase.RISK_STATUS_CLOSED &&
						Integer.parseInt(select.getValue().toString()) != IssuesManagerBase.RISK_STATUS_NEED_APPROVE){
					list.add(select);
				}
			}			
			return list;	
		}
		return list;
	}
	
	public boolean calculateIssueStatus(){
		HashMap<Long, FilesRiskStatus> filesRiskStatusHM = Utils.getWriter().getFilesRiskStatusHM();
		int issueStatus = IssuesManagerBase.RISK_STATUS_NEW_ISSUE;
		int priority = 99;
		int tmpPriorty = 99;
		if (!CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId())){
			priority = filesRiskStatusHM.get(Long.valueOf(issue.getFilesRiskStatusId())).getPriority();
			issueStatus = Integer.valueOf(issue.getFilesRiskStatusId());
		}
		for (CreditCard c: cardsList){
			if (c.getFilesRiskStatusId() != 0){
				tmpPriorty = filesRiskStatusHM.get(Long.valueOf(c.getFilesRiskStatusId())).getPriority();
				if (tmpPriorty < priority){
					priority = tmpPriorty;
					issueStatus = c.getFilesRiskStatusId();
				}
			}
		}
		if (null != userFiles){
			if (userFiles.getFilesRiskStatusId() != 0){
				tmpPriorty = filesRiskStatusHM.get(Long.valueOf(userFiles.getFilesRiskStatusId())).getPriority();
				if (tmpPriorty < priority){
					priority = tmpPriorty;
					issueStatus = userFiles.getFilesRiskStatusId();
				}
			}
		}
		issue.setStatusId(String.valueOf(issueStatus));
		if (tempIssue.getStatusId().equals(String.valueOf(issueStatus))){
			return false;
		}
		return true;
	}
	
	private boolean issueStatusAfterNewAction(){
		int issueStatus = Integer.valueOf(issue.getStatusId());
		if (!CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId()) && 
				Integer.valueOf(issue.getSubjectId()) == com.anyoption.common.managers.IssuesManagerBase.ISSUE_SUBJECT_FIVE_NO_DOC){			
			issueStatus = Integer.valueOf(issue.getFilesRiskStatusId());
			issue.setStatusId(String.valueOf(issueStatus));
		}		
		if (tempIssue.getStatusId().equals(String.valueOf(issueStatus))){
			return false;
		}
		return true;
	}

	public void showReasonList(ValueChangeEvent ev) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		String value = (String)ev.getNewValue();
		int firstAction = issue.getIssueActionsList().size()-1;
    	if (null != value && value.equals(String.valueOf(IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST))
    			&& !issue.getIssueActionsList().get(firstAction).getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER))){

    		issue.setFilesRiskStatusId(String.valueOf(IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST));
			showAllCards = true;
			tmpCardsList = cardsList;
			cardsList = TransactionsManager.getCCByUserId(riskUserId, 0, 0);
			
			if (null == idFile){
				idFile = UsersManager.getSpecificFile(riskUserId, FileType.USER_ID_COPY, 0, 0);
				if (null != idFile){
					if (idFile.isApproved() && idFile.getFileStatusId() == File.STATUS_DONE){
						isIdDocumentSent = true;
					}
					riskIdRequest = UsersManager.verifyIsRiskIssue(riskUserId);
					//if(idFile.getIssueActionsId() != 0){
						//iskIdRequest = true;
					//}
				}
			}
		} else {
			if (null != issueAction){
				issueAction.setRiskReasonId(null);
			}
			if (showAllCards){
				tmpCardsList = cardsList;
				cardsList = TransactionsManager.getCCByUserId(riskUserId, 0, 0);
				
				CommonUtil.setTablesToFirstPage();
			}
			if (null != value){
				issue.setFilesRiskStatusId(value);
			}
			showAllCards = false;
		}
		context.renderResponse();
	}

	/**
	 * Show relevant issue actions
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public String showRelevantIA(ValueChangeEvent event) throws SQLException, ParseException{
    	String value = (String)event.getNewValue();
    	if (!CommonUtil.isParameterEmptyOrNull(value)){
    		if (value.equals(Constants.RISK_DOC_REQ_ID)){//id was selected
    			issue.setIssueActionsList(IssuesManager.getActionsByIssueId(issue.getId(), 0, true, Constants.ISSUE_TYPE_RISK));
    		} else if (value.equals(Constants.RISK_DOC_REQ_ALL)){//all was selected
    			issue.setIssueActionsList(IssuesManager.getActionsByIssueId(issue.getId(), 0, false, Constants.ISSUE_TYPE_RISK));
    		} else {//cc was selected
    			String[] cc = value.split(":"); //
    			issue.setIssueActionsList(IssuesManager.getActionsByIssueId(issue.getId(), Long.valueOf(cc[0]), false, Constants.ISSUE_TYPE_RISK));
    		}
    	}
    	String reasons = ConstantsBase.EMPTY_STRING;
		for (IssueAction i:issue.getIssueActionsList()){
			if (!CommonUtil.isParameterEmptyOrNull(i.getRiskReasonName()) && !reasons.contains(i.getRiskReasonName())){
				reasons += i.getRiskReasonName() +"<br/>";
			}
		}
		if (reasons.length() > 5){
			issue.setRiskReasons(reasons.substring(0,reasons.length()-5));
		}
		CommonUtil.setTablesToFirstPage();
    	return null;
    }

    /**
     * Insert to doc request status  and update cc/user files status
     * @param statusChanged
     * @throws SQLException
     */
    public void insertDocRequestAndStatus(boolean isAccounting) throws SQLException {
    	try {
    		logger.debug("Going to insert doc request, issueId:" + issue.getId());
	    	FacesContext context=FacesContext.getCurrentInstance();
			User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
			if (CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId())){ // no need to change status (except new issue)
				for (String s: insertRelevantTo){
					if (s.equals(Constants.RISK_DOC_REQ_ALL)){ // Relevant to ALL selected
						if (riskIdRequest){
							IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_ID);
							if (userFiles.isNeedUpdateStatus() && !isAccounting){ //update 'new' status to 'in prodress'
								IssuesManager.updateUserDocStatus(user.getId(), IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
								userFiles.setNeedUpdateStatus(false);
							}
						}
						for (CreditCard c: cardsList){
							IssuesManager.insertDocRequest(issueAction.getActionId(),  c.getId(), Constants.RISK_DOC_REQ_CC);
							if (c.isNeedUpdateStatus() && !isAccounting){//update 'new' status to 'in prodress'
								IssuesManager.updateCCDocStatus(c.getId(), IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
								c.setNeedUpdateStatus(false);
							}
						}
						return;
					} else if (s.equals(Constants.RISK_DOC_REQ_ID)){ // Relevant to ID selected
						IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_ID);
						if (userFiles.isNeedUpdateStatus() && !isAccounting){//update 'new' status to 'in prodress'
							IssuesManager.updateUserDocStatus(user.getId(), IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
							userFiles.setNeedUpdateStatus(false);
						}
					} else { // Relevant to CC selected
						String[] cc = s.split(":");
						IssuesManager.insertDocRequest(issueAction.getActionId(),  Long.valueOf(cc[0]), Constants.RISK_DOC_REQ_CC);
						if (cardsList.get(Integer.valueOf(cc[2])).isNeedUpdateStatus() && !isAccounting){//update 'new' status to 'in prodress'
							IssuesManager.updateCCDocStatus(Long.valueOf(cc[0]), IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
							cardsList.get(Integer.valueOf(cc[2])).setNeedUpdateStatus(false);
						}
					}
				}
			} else { //need to change files status
				for (String s: insertRelevantTo){
					if (s.equals(Constants.RISK_DOC_REQ_ALL)){ // Relevant to ALL selected
						if (riskIdRequest){
							IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_ID);
							IssuesManager.updateUserDocStatus(user.getId(), Long.valueOf(issue.getFilesRiskStatusId()));
						}
						for (CreditCard c: cardsList){
							IssuesManager.insertDocRequest(issueAction.getActionId(),  c.getId(), Constants.RISK_DOC_REQ_CC);
							IssuesManager.updateCCDocStatus(c.getId(), Long.valueOf(issue.getFilesRiskStatusId()));
						}
						return;
					} else if (s.equals(Constants.RISK_DOC_REQ_ID)){ // Relevant to ID selected
						if (null != userFiles){//in case of inserting id copy request prevent from update status
							IssuesManager.insertDocRequest(issueAction.getActionId(),  0, Constants.RISK_DOC_REQ_ID);
							IssuesManager.updateUserDocStatus(user.getId(), Long.valueOf(issue.getFilesRiskStatusId()));
						}
					} else {// Relevant to CC selected
						String[] cc = s.split(":");
						IssuesManager.insertDocRequest(issueAction.getActionId(),  Long.valueOf(cc[0]), Constants.RISK_DOC_REQ_CC);
						IssuesManager.updateCCDocStatus(Long.valueOf(cc[0]), Long.valueOf(issue.getFilesRiskStatusId()));
					}
			  }
			}
    	}
		catch (Exception e) {
			logger.error("Can't change risk file status ");
		}
    }

    /**
     * Update files status according 'relevant to' selection
     * Change status automatically to in progress in case new IA inserted is status 'new', 'Additional request', exept accounting Issue Actions
     */
    public void updateFilesStatus(boolean isAccounting){
    		logger.debug("Going to update files status, issueId:" + issue.getId());
    		//Change status automatically to in progress in case new IA inserted is status 'new', 'In progress', exept Anna Issue Actions
    		if (CommonUtil.isParameterEmptyOrNull(issue.getFilesRiskStatusId())){
				for (String s: insertRelevantTo){
					if (s.equals(Constants.RISK_DOC_REQ_ALL)){ // Relevant to ALL selected
						if (null != userFiles && riskIdRequest){
							if (!isAccounting && (userFiles.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE)
									|| (userFiles.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST)){ //update new status to in prodress
								userFiles.setFilesRiskStatusId(IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
								userFiles.setNeedUpdateStatus(true);
							}
						}
						for (CreditCard c: cardsList){
							if (!isAccounting && (c.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE)
									|| (c.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST)){ //update new status to in prodress
								c.setFilesRiskStatusId(IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
								c.setNeedUpdateStatus(true);
							}
						}
						break;
					} else if (s.equals(Constants.RISK_DOC_REQ_ID)){ // Relevant to ID selected
						if (!isAccounting && null!= userFiles && ((userFiles.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE)
								|| (userFiles.getFilesRiskStatusId() == IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST))){ //update new status to in prodress
							userFiles.setFilesRiskStatusId(IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
							userFiles.setNeedUpdateStatus(true);
						}
					} else { // Relevant to CC selected
						String[] cc = s.split(":");
						if (!isAccounting && (Long.valueOf(cc[1]) == IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE)
								|| (Long.valueOf(cc[1]) == IssuesManagerBase.FILES_RISK_STATUS_ADDITIONAL_REQUEST)){ //update new status to in prodress
							cardsList.get(Integer.valueOf(cc[2])).setFilesRiskStatusId(IssuesManagerBase.FILES_RISK_STATUS_IN_PROGRESS);
							cardsList.get(Integer.valueOf(cc[2])).setNeedUpdateStatus(true);
						}
					}
				}
			} else { //need to change files status
				for (String s: insertRelevantTo){
					if (s.equals(Constants.RISK_DOC_REQ_ALL)){ // Relevant to ALL selected
						if (null != userFiles && riskIdRequest){
							userFiles.setFilesRiskStatusId(Integer.valueOf(issue.getFilesRiskStatusId()));
						}
						for (CreditCard c: cardsList){
							c.setFilesRiskStatusId(Integer.valueOf(issue.getFilesRiskStatusId()));
						}
						break;
					} else if (s.equals(Constants.RISK_DOC_REQ_ID)){ // Relevant to ID selected
						if (null != userFiles){//in case inserting id copy prevent from update status
							userFiles.setFilesRiskStatusId(Integer.valueOf(issue.getFilesRiskStatusId()));
						}
					} else {// Relevant to CC selected
						String[] cc = s.split(":");
						cardsList.get(Integer.valueOf(cc[2])).setFilesRiskStatusId(Integer.valueOf(issue.getFilesRiskStatusId()));
					}
			  }
			}
    }

    /**
     * Reason is mandatory when insering new card from issue action
     * @return
     */
    public boolean validateRiskActionFields(){
		for (CreditCard c: cardsList){
				//reason is mandatory with new card
				if (c.getFilesRiskStatusId() == 0 &&  CommonUtil.isParameterEmptyOrNull(issueAction.getRiskReasonId()) && (c.getReqRiskIssueScreen().isPowerOfAttorney() || c.getReqRiskIssueScreen().isSentCcCopy() ||
						c.getReqRiskIssueScreen().isSentHolderIdCopy() || c.getReqRiskIssueScreen().isVerificationCall())){
					return false;
				}
		}
		if (riskIdSelected && CommonUtil.isParameterEmptyOrNull(issueAction.getRiskReasonId())){
			return false;
		}
    	return true;
    }

    public void insertFilesRequest(long userId, boolean isNewRI) throws SQLException{
    	logger.debug("Going to insert files request, issueId:" + issue.getId());
		if (null != cardsList && cardsList.size() > 0){
			for(CreditCard card:cardsList){
				boolean insertReqFiles = false;
				if (card.getReqRiskIssueScreen().isPowerOfAttorney() || card.getReqRiskIssueScreen().isSentCcCopy() ||
						card.getReqRiskIssueScreen().isSentHolderIdCopy() || card.getReqRiskIssueScreen().isVerificationCall()){
						insertReqFiles = true;
				}
				if (insertReqFiles){
					File f = new File();
					if (card.getReqRiskIssueScreen().isPowerOfAttorney()) {
						f = prepareFile(card, String.valueOf(FileType.POWER_OF_ATTORNEY));
						IssuesManager.insertRequestFiles(f);
					}
					if (card.getReqRiskIssueScreen().isSentCcCopy()) {
						f = prepareFile(card, String.valueOf(FileType.CC_COPY));
						IssuesManager.insertRequestFiles(f);
					}
					if (card.getReqRiskIssueScreen().isSentHolderIdCopy()) {
						f = prepareFile(card, String.valueOf(FileType.CC_HOLDER_ID));
						IssuesManager.insertRequestFiles(f);
					}
					if (card.getReqRiskIssueScreen().isVerificationCall()) {
						IssuesManager.insertVerificationRequest(card.getId());
					}
					if (isNewRI){
						IssuesManager.insertDocRequest(issueAction.getActionId(), card.getId(), Constants.RISK_DOC_REQ_CC);
						IssuesManager.updateCCDocStatus(card.getId(), Long.valueOf(issue.getFilesRiskStatusId()));
					}
				}
			}
		}
		//ID COPY request
		if (riskIdSelected){
			File file = new File();
			file.setFileStatusId(File.STATUS_REQUESTED);
			file.setUserId(issue.getUserId());
			file.setTimeCreated(new Date());
			file.setUtcOffsetCreated(Utils.getWriter().getUtcOffset());
			file.setWriterId(Utils.getWriter().getWriter().getId());
			file.setIssueActionsId(issueAction.getActionId());
			file.setFileTypeId(FileType.USER_ID_COPY);
			IssuesManager.insertRequestFiles(file);
			IssuesManager.insertDocRequest(issueAction.getActionId(), 0, Constants.RISK_DOC_REQ_ID);
			IssuesManager.updateUserDocStatus(userId, Long.valueOf(issue.getFilesRiskStatusId()));
			riskIdSelected = false;
		}
    }

    /**
     * Return all reasons that related to current issue
     * @return
     */
    public ArrayList<SelectItem> getIssueReasonsSI(){
    	ArrayList<SelectItem> reasonsSI = new ArrayList<SelectItem>();
    	String reasons = ConstantsBase.EMPTY_STRING;
    	reasonsSI.add(new SelectItem(String.valueOf(ConstantsBase.ALL_FILTER_ID) ,CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null)));
    	for(IssueAction ia: issue.getIssueActionsList()){
    		if (null != ia.getRiskReasonId() && !reasons.contains(ia.getRiskReasonName())){
    			reasons += ia.getRiskReasonName(); // avoid duplicate reason to be display
    			reasonsSI.add(new SelectItem(ia.getRiskReasonId(),ia.getRiskReasonName()));
    		}
    	}
    	return reasonsSI;
    }

    /**
     * Update cards/id copy when selecting reason
     * @param event
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public String updateCardsByReason(ValueChangeEvent event) throws SQLException, ParseException{
    	String value = (String)event.getNewValue();
		if (!CommonUtil.isParameterEmptyOrNull(value) && ((cardsList.size() > 0) || (null != userFiles))){
			cardsList = TransactionsManager.getCCByUserId(issue.getUserId(), issue.getId(),  Long.valueOf(value));
			userFiles = UsersManager.getUserFiles(issue.getId(), Long.valueOf(value));
			if (null != userFiles){
				if (userFiles.getUserIdFile().isApproved() && userFiles.getUserIdFile().getFileStatusId() == File.STATUS_DONE){
					isIdDocumentSent = true;
				}
				if(userFiles.getUserIdFile().getIssueActionsId() != 0){
					riskIdRequest = true;
				}
			} else { //reset id document indicator
				isIdDocumentSent = false;
				riskIdRequest = false;
			}
		}
		return null;
    }


	/************************************************* SORTING ************************************************************/


	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	/**
	 * Sort list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String sortList() {
		if (activateAscending){
			if (sortColumn.equals(prevSortColumn)) {
				sortAscending = !sortAscending;
			} else {
				sortAscending = true;
			}
		}

		activateAscending = true;
		prevSortColumn = sortColumn;

		if (sortColumn.equals("daysTillUpdate")) {
			Collections.sort(issuesList,new DaysTillUpdateComparator(sortAscending));
		}

		if (sortColumn.equals("timeCreated")) {
			Collections.sort(issuesList,new TimeCreatedComparator(sortAscending));
		}

		if (sortColumn.equals("timeUpdated")) {
			Collections.sort(issuesList,new TimeUpdatedComparator(sortAscending));
		}

		if (sortColumn.equals("priority")) {
			Collections.sort(issuesList,new PriorityComparator(sortAscending));
		}

		if (sortColumn.equals("daysWithoutTreatment")) {
			Collections.sort(issuesList,new DaysWithoutTreatmentComparator(sortAscending));
		}

		if (sortColumn.equals("country")) {
			Collections.sort(issuesList,new countryComparator(sortAscending));
		}

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	private class countryComparator implements Comparator {
		private boolean ascending;
		public countryComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Issue i1=(Issue)o1;
   	    	Issue i2=(Issue)o2;
   		 		if (ascending){
   		 			return i1.getCountry().compareTo(i2.getCountry());
   		 		}
   		 	return -i1.getCountry().compareTo(i2.getCountry());
		}
  	 }

	private class DaysTillUpdateComparator implements Comparator {
		private boolean ascending;
		public DaysTillUpdateComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Issue i1=(Issue)o1;
   		 		Issue i2=(Issue)o2;
   		 		if (ascending){
   		 			return new Integer(i1.getDaysInProgress()).compareTo(new Integer(i2.getDaysInProgress()));
   		 		}
   		 		return -new Integer(i1.getDaysInProgress()).compareTo(new Integer(i2.getDaysInProgress()));
		}
  	 }

	private class TimeCreatedComparator implements Comparator {
		private boolean ascending;
		public TimeCreatedComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
		 		Issue i1=(Issue)o1;
   		 		Issue i2=(Issue)o2;
   		 		if (ascending){
   		 			return i1.getTimeCreated().compareTo(i2.getTimeCreated());
   		 		}
   		 		return -i1.getTimeCreated().compareTo(i2.getTimeCreated());
		}
  	 }


	private class TimeUpdatedComparator implements Comparator {
		private boolean ascending;
		public TimeUpdatedComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
		 		Issue i1=(Issue)o1;
			 	Issue i2=(Issue)o2;
   		 		if (ascending){
   		 			return i1.getLastActionTime().compareTo(i2.getLastActionTime());
   		 		}
   		 	return -i1.getLastActionTime().compareTo(i2.getLastActionTime());
		}
  	 }

	private class PriorityComparator implements Comparator {
		private boolean ascending;
		public PriorityComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
		 		Issue i1=(Issue)o1;
		 		Issue i2=(Issue)o2;
		 		if (ascending){
		 			return Integer.valueOf(i1.getPriorityId()).compareTo(Integer.valueOf(i2.getPriorityId()));
		 		}
		 		return -Integer.valueOf(i1.getPriorityId()).compareTo(Integer.valueOf(i2.getPriorityId()));
   	    }
  	 }

	private class DaysWithoutTreatmentComparator implements Comparator {
		private boolean ascending;
		public DaysWithoutTreatmentComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Issue i1=(Issue)o1;
   		 		Issue i2=(Issue)o2;
   		 		if (ascending){
   		 			return new Integer(i1.getDaysWithoutTreatment()).compareTo(new Integer(i2.getDaysWithoutTreatment()));
   		 		}
   		 		return -new Integer(i1.getDaysWithoutTreatment()).compareTo(new Integer(i2.getDaysWithoutTreatment()));
		}
  	 }

	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}

	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public List<? extends com.anyoption.common.beans.File> getFilesList() {
		return filesList;
	}

	public void setFilesList(List<? extends com.anyoption.common.beans.File> filesList) {
		this.filesList = filesList;
	}
	
	public List<? extends com.anyoption.common.beans.File> getFilesListPerUser() {
		return filesListPerUser;
	}

	public void setFilesListPerUser(List<? extends com.anyoption.common.beans.File> filesListPerUser) {
		this.filesListPerUser = filesListPerUser;
	}

	public void beforeFileList(ActionEvent event) {
		
		try {
			
		List<UIComponent> children = (List<UIComponent>) event.getComponent().getChildren();
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		 long issueId = 0;
		
		 for(UIComponent child : children) {
			 
			 HtmlOutputText text = (HtmlOutputText) child;
			 issueId = (Long) text.getValue();
		}
		setFilesList(IssuesManagerBase.getFileListByActionId(issueId,user.getId(), Utils.getWriterLocale(context)));
		setFilesListPerUser(IssuesManagerBase.getFileListByUserId(user.getId(), Utils.getWriterLocale(context)));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	/************************************************* END SORTING ********************************************************/
	
	private void fillCreditCardList() {
		try {
			ccSI = TransactionsManager.getDisplayCreditCardsByUser(riskUserId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Long getCardNum() {
		return cardNum;
	}

	public void setCardNum(Long cardNum) {
		FacesContext.getCurrentInstance().getAttributes().put(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE, cardNum.toString());
		this.cardNum = cardNum;
	}

	public ArrayList<SelectItem> getCcSI() {
		return ccSI;
	}

	public void setCcSI(ArrayList<SelectItem> ccSI) {
		this.ccSI = ccSI;
	}

	public boolean isCanMoveToControl() {
		return canMoveToControl;
	}

	public void setCanMoveToControl(boolean canMoveToControl) {
		this.canMoveToControl = canMoveToControl;
	}

	public boolean isCanMoveToReg() {
		return canMoveToReg;
	}

	public void setCanMoveToReg(boolean canMoveToReg) {
		this.canMoveToReg = canMoveToReg;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getPopulationCurrentEntryId() {
		return populationCurrentEntryId;
	}

	public void setPopulationCurrentEntryId(long populationCurrentEntryId) {
		this.populationCurrentEntryId = populationCurrentEntryId;
	}
	
	// dev-2192
	private ArrayList<SelectItem> filterIssueActionTypes(ArrayList<SelectItem> list, User user, Writer writer) {
		     
		Iterator<SelectItem> it = list.iterator();
		while (it.hasNext()) {
			SelectItem item = it.next();
			if (Integer.parseInt(item.getValue().toString()) == ISSUE_ACTION_TYPE_SUSPEND_REGULATION || Integer.parseInt(item.getValue().toString()) == ISSUE_ACTION_TYPE_REMOVE_SUSPEND_REGULATION
					 || Integer.parseInt(item.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION || Integer.parseInt(item.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION) {
				if (writer.getId() != ConstantsBase.YAELJ_USER_ID) {
					if (user.isHasAdminRole() || user.isHasControlRole() || user.isHasSAdminRole()) {
						//Check if user is Regulated remove the issue action for non reg
						Skin s = SkinsManagerBase.getSkin(user.getSkinId());
						if(s.isRegulated() && (Integer.parseInt(item.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION || Integer.parseInt(item.getValue().toString()) == IssuesManagerBase.ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION)){
							it.remove();
						}
						if(!s.isRegulated() && (Integer.parseInt(item.getValue().toString()) == ISSUE_ACTION_TYPE_SUSPEND_REGULATION || Integer.parseInt(item.getValue().toString()) == ISSUE_ACTION_TYPE_REMOVE_SUSPEND_REGULATION)){
							it.remove();
						}
					} else {
						it.remove();
					}
				}
			}
			if(Integer.parseInt(item.getValue().toString()) == ISSUE_ACTION_TYPE_ACTIVATE_ACOUNT && !havePermissionToActivateAcount()){
				it.remove();	
			}
		}
		return list;
	}

	public boolean isShowChangeSkinDropdown() {
		return showChangeSkinDropdown;
	}

	public void setShowChangeSkinDropdown(boolean showChangeSkinDropdown) {
		this.showChangeSkinDropdown = showChangeSkinDropdown;
	}

	public ArrayList<Integer> getSkinList() {
		return skinList;
	}

	public void setSkinList(ArrayList<Integer> skinList) {
		this.skinList = skinList;
	}
	
	public boolean havePermissionToActivateAcount(){
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		boolean havePermissions = false;
//		boolean canActivateAcount = IssuesManagerBase.getActionForDeactivation (user.getId());
		long actionType = IssuesManagerBase.getActionForDeactivation(user.getId());
//		if((canActivateAcount && user.isHasAdminRole()) || !canActivateAcount){
//				return true;	
//	    }
		if (actionType != ConstantsBase.ACTION_TYPE_FAILED_LOGIN && user.isHasAdminRole()) {
			havePermissions = true;
		}
		if(actionType == ConstantsBase.ACTION_TYPE_FAILED_LOGIN) {
			havePermissions = true;
		}
	    
	    return havePermissions;
	 }

	/**
	 * @return the componentId
	 */
	public long getComponentId() {
		return componentId;
	}

	/**
	 * @param componentId the componentId to set
	 */
	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	public boolean isCopyopBlockedSelected() {
		return copyopBlockedSelected;
	}

	public boolean isCopyopRemovedSelected() {
		return copyopRemovedSelected;
	}

	public void setCopyopRemovedSelected(boolean copyopRemovedSelected) {
		this.copyopRemovedSelected = copyopRemovedSelected;
	}

	public void setCopyopBlockedSelected(boolean copyopBlockedSelected) {
		this.copyopBlockedSelected = copyopBlockedSelected;
	}

	public boolean isCopyopBlockRemoveFields() {
		return copyopBlockRemoveFields;
	}

	public void setCopyopBlockRemoveFields(boolean copyopBlockRemoveFields) {
		this.copyopBlockRemoveFields = copyopBlockRemoveFields;
	}

	public ArrayList<SelectItem> getRiskReasonsList() {
		return riskReasonsList;
	}

	public void setRiskReasonsList(ArrayList<SelectItem> riskReasonsList) throws SQLException {
		this.riskReasonsList = riskReasonsList;
	}

	public boolean isDisabledBlockRemoveReason() {
		return disabledBlockRemoveReason;
	}

	public void setDisabledBlockRemoveReason(boolean disabledBlockRemoveReason) {
		this.disabledBlockRemoveReason = disabledBlockRemoveReason;
	}
}

