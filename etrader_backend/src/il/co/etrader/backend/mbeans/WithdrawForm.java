package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.beans.base.BaroPayRequest;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase.transactionUpdateStatus;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawEntryResult.WithdrawResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum;
import com.anyoption.common.util.AnyOptionCreditCardValidator;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.WireBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class WithdrawForm extends FormBase implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7064952537061037029L;

	private static final Logger logger = Logger.getLogger(WithdrawForm.class);

	private static final int WITHDRAW_TYPE_REGULAR = 1;
	private static final int WITHDRAW_TYPE_WEBMONEY = 2;
	private static final int WITHDRAW_TYPE_DELTAPAY_CHINA = 3;
	private static final int WITHDRAW_TYPE_BAROPAY = 4;
 
	private static final String WITHDRAW_AMOUNT_SESSION_PARAMETER = "withdrawAmountParameter";

	private String name;
	private String amount;
	private String street;
	private String streetNo;
	private long cityId;
	private String zipCode;
	private String comments;
	private String cityName;
	private boolean feeCancel;
	private WireBase wire;
	private String payPalEmail;
	private String moneybookersEmail;

	private HashMap<Long, CreditCard> ccHash;
	private String cardId;
	private boolean isCcWithdrawal;

	//for credit card withdraw
	private String typeId;
	private String ccNum;
	private String ccPass;
	private String expMonth;
	private String expYear;
	private String holderName;
	private String holderIdNum;
	private String deposit;
	private boolean sendReceipt;
	private boolean isCcError;  //for cc validation
	private ArrayList ccTypes;

	//	 Envoy Withdraw
	private String envoyAccountNum;
	private ArrayList<SelectItem> envoyAccountNumList;
	private String paymentMethod;
	private long envoyWithdrawMethodId;
	private boolean isEnvoyWithdraw;

	//	WebMoney Withdraw
	private ArrayList<SelectItem> webMoneyPurseList;
	private String webMoneyPurse;

	private int withdrawType;

	private Long transactionId;
	private String captureCode;
	private long statusId;

    private boolean eftWithdraw;
    
    private BaroPayRequest baroPayRequest;
    
    private long bankWireWithdrawalId;
	private String bankOtherNameTxt;
	// Rewards plan
	private boolean isHavingRewardExemptFee;

	public WithdrawForm() {

		FacesContext context = FacesContext.getCurrentInstance();

		paymentMethod = "";
		comments = "";
		envoyAccountNum="";
		feeCancel = false;
		ccHash = new HashMap<Long, CreditCard>();
		wire = new WireBase();
		isEnvoyWithdraw = false;

		//for credit card withdraw
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (CommonUtil.isHebrewSkin(ap.getSkinId())){
			ccTypes = ap.getCreditCardTypes();
		}
		else{
			ccTypes = ap.getCreditCardTypesAo();
		}
		sendReceipt = true;

        eftWithdraw = false;

		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		
        moneybookersEmail = TransactionsManagerBase.getLastMoneybookersEmail(user.getId());

        // for webmoney withdraw
        webMoneyPurse = "";
        withdrawType = WITHDRAW_TYPE_REGULAR;
        
        baroPayRequest = new BaroPayRequest();
        isHavingRewardExemptFee = false;
	}

	public String getAmountTxt() {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());

		if (amount!=null && !amount.equals("")) {
			return CommonUtil.displayAmount((long)((float)Float.parseFloat(amount)*(float)100), user.getCurrencyId().intValue());
		} else
			return "";
	}

	/**
	 * Get Credit Card list for Inatec CFT withdraw
	 * @return
	 * @throws Exception
	 */
	public ArrayList getInatecCcCftList() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		ccHash = TransactionsManager.getInatecCftCcWithrawalByUserId(user.getId());
		return TransactionsManagerBase.getDisplayWithrawalCreditCardsByUser(ccHash, user.getCurrency(),ConstantsBase.BACKEND_CONST);
	}



	/**
	 * Cheque withdrawal
	 * @return
	 * @throws SQLException
	 */
	public String insertWithdraw() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (Utils.getWriter().isEtrader()) {
			if (!TransactionsManager.validateCityWithdrawForm(cityName,cityId)) {
				return null;
			}
		}

		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, null, null, null, AdminManager.getWriterId());
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}
		
		if (error != null) {
		    TransactionsManager.insertWithdraw(amount,cityId, cityName, name,street,streetNo,zipCode,error,user,AdminManager.getWriterId(),feeCancel, 0L, null);
            return null;
		}

		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_WITHDRAW_APPROVE;		
	}

	private boolean checkLimits(User user){
		return !(user.isHasAccountingRole() || user.isHasAdminRole() || user.isHasSAdminRole());
	}
	public String insertAdminWithdraw() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());

		//implement if needed.
		if (false) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			}
		}
		
		int transTypeId = TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW;

		boolean res = TransactionsManager.insertAdminWithdraw(null,this,user.getId(), transTypeId);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		if (res)
			return Utils.navAfterUserOperation();
		else
			return null;
	}

	public String insertBonusWithdraw() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());

		//Implement if needed
		if (false) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			}
		}		
		
		int transTypeId = TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW;

		boolean res = TransactionsManager.insertBonusWithdraw(null,this,user.getId(), transTypeId);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		if (res)
			return Utils.navAfterUserOperation();
		else
			return null;
	}

	public String approveWithdraw() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		int tranTypeId = 0;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		String error = null;
		if(hasBonuses()){// cancel all bonuses that can be canceled or return error if something goes wrong
			List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
			for (BonusUsers bu : bonusUsers) {
				if (bu.isCanCanceled()) {
					error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
				}
				if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
					logger.error("Cannot cancel bonus with id: " + bu.getId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.creditcard.unapproved",null),null);
			     	context.addMessage(null, fm);
			     	//clearWithdrawForm();
					return Utils.navAfterUserOperation();
				}
			}
		}

		if (isCcWithdrawal) { //cc withdrawal
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW;
			error = TransactionsManager.insertCreditCardWithdraw(amount, ccHash.get(Long.valueOf(cardId)), name, null, user, AdminManager.getWriterId(), feeCancel, null, 0L, null);
			if ( error != null ) {  // cc error 
				return Utils.navAfterUserOperation();
			}
		} else if(isEnvoyWithdraw){
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW;
			TransactionsManager.insertWithdrawEnvoyService(amount, envoyWithdrawMethodId, user, Utils.getWriter().getWriter().getId(), feeCancel, null, envoyAccountNum, 0L, null);
		} else if (withdrawType == WITHDRAW_TYPE_WEBMONEY){
			TransactionsManager.insertWebMoneyWithdraw(amount, null, user, Utils.getWriter().getWriter().getId(), feeCancel, webMoneyPurse, 0L, null);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW;
		} else if (withdrawType == WITHDRAW_TYPE_DELTAPAY_CHINA) {
			TransactionsManager.insertDeltaPayChinaWithdraw(amount, null, user, Utils.getWriter().getWriter().getId(), feeCancel, 0L);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW;
		} else if (withdrawType == WITHDRAW_TYPE_BAROPAY) {
			TransactionsManager.insertBaroPayWithdraw(amount, null, user, Utils.getWriter().getWriter().getId(), feeCancel, baroPayRequest.getBankName(), baroPayRequest.getAccountOwner(), baroPayRequest.getAccountNumber(), 0L, null); 
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW;
		} else {// cheque withdrawal
			TransactionsManager.insertWithdraw(amount, cityId, cityName, name, street, streetNo, zipCode, null, user,AdminManager.getWriterId(), feeCancel, 0L, null);
			tranTypeId = TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW;
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		long amountLong = CommonUtil.calcAmount(amount);
		long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), tranTypeId);
		String fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());

		String[] params = new String[2];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());
		params[1] = fee;

		String msg ="";
	    if ( isCcWithdrawal) {
	      	msg = CommonUtil.getMessage("withdraw.success.cc",params, Utils.getWriterLocale(context));
	    } else if (isEnvoyWithdraw) {
	    	msg = CommonUtil.getMessage("withdraw.envoy.success",params, Utils.getWriterLocale(context));
	    } else if (withdrawType == WITHDRAW_TYPE_WEBMONEY){
	    	msg = CommonUtil.getMessage("withdraw.webmoney.success",params, Utils.getWriterLocale(context));
	    } else if (withdrawType == WITHDRAW_TYPE_DELTAPAY_CHINA) {
	    	msg = CommonUtil.getMessage("withdraw.deltapay.success",params, Utils.getWriterLocale(context));
	    } else if (withdrawType == WITHDRAW_TYPE_BAROPAY) {
	    	msg = CommonUtil.getMessage("withdraw.baropay.success",params, Utils.getWriterLocale(context));	    	
		} else {
	    	msg = CommonUtil.getMessage("withdraw.cc.success",params, Utils.getWriterLocale(context));
	    	if (isHavingRewardExemptFee) {
				msg = CommonUtil.getMessage("rewards.withdraw.cc.success",params, Utils.getWriterLocale(context));
			}
	    }

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		isCcWithdrawal = false;
		isEnvoyWithdraw= false;

		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();
	}

	public String approveWithdrawNoFee() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long amountLong = CommonUtil.calcAmount(amount);

		if ( isCcWithdrawal == false ) {
			TransactionsManager.insertWithdraw(amount,cityId, cityName,name,street,streetNo,zipCode,null,user,AdminManager.getWriterId(),true, 0L, null);
		}
		else {  // cc withdrawal
			String error = TransactionsManager.insertCreditCardWithdraw(amount, ccHash.get(Long.valueOf(cardId)), name, null, user, AdminManager.getWriterId(), true, null, 0L, null);
			if ( error != null ) {
				return Utils.navAfterUserOperation();
			}
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		String[] params = new String[1];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());

		String msg ="";
		if ( isCcWithdrawal == false ) {
			msg = CommonUtil.getMessage("withdraw.success",params, Utils.getWriterLocale(context));
		}
		else {  // cc withdrawal
			msg = CommonUtil.getMessage("withdraw.cc.success",params, Utils.getWriterLocale(context));
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		isCcWithdrawal = false;
		
		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();

	}

	public String notApproveWithdraw() throws SQLException {
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		long transactionTypeId;
		if (isCcWithdrawal) {
			transactionTypeId = TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW;
		} else if (isEnvoyWithdraw) {
			transactionTypeId = TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW;
		} else {
			switch (withdrawType) {
			case WITHDRAW_TYPE_WEBMONEY:
				transactionTypeId = WITHDRAW_TYPE_WEBMONEY;
				break;
			case WITHDRAW_TYPE_DELTAPAY_CHINA:
				transactionTypeId = WITHDRAW_TYPE_DELTAPAY_CHINA;
				break;
			case WITHDRAW_TYPE_BAROPAY:
				transactionTypeId = WITHDRAW_TYPE_BAROPAY;
				break;
			default:
				transactionTypeId = TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW;
				break;
			}
			transactionTypeId = withdrawType;
		}
		WithdrawBonusStateEnum state = WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES;
		if (!SkinsManagerBase.getSkin(user.getSkinId()).isRegulated()) {
			state = WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES;
		}
		TransactionsManagerBase.insertWithdrawCancel(	user.getId(), transactionTypeId, Long.valueOf(cardId), Utils.getWriter().getWriter().getId(),
														CommonUtil.getUtcOffset(), state, CommonUtil.getIPAddress());

		String msg = CommonUtil.getMessage("withdraw.cancel",null, Utils.getWriterLocale(context));
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);
		return Utils.navAfterUserOperation();
	}

	/**
	 * @return the creditCards list
	 */
	public ArrayList getCcList() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long minWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
		ccHash = TransactionsManager.getDisplayWithrawalCreditCardsByUser(user.getId(), minWithdraw,user.getSkinId(), true);
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list = TransactionsManagerBase.getDisplayWithrawalCreditCardsByUser(ccHash, user.getCurrency(),ConstantsBase.BACKEND_CONST);
	    for (Iterator<SelectItem> i = list.iterator();i.hasNext();){
	    	
	    	SelectItem si = i.next();

			if(si.getLabel().contains("Diners") || (si.getLabel().contains("American Express") && !user.isEtrader())){
	    		i.remove(); 		
	    	}
	    }
		
		return list;
		
	}

	/**
	 * Credit Card withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertCreditCardWithdraw() throws Exception {


		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		isCcWithdrawal = true;  // to save withdrawal type

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		CreditCard card = ccHash.get(Long.valueOf(cardId));
		String error = null;

		error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), card, null, null, null, AdminManager.getWriterId());
				
		if (null == error) {  // check card expiration
			error = TransactionsManagerBase.validateCardExpiration(card);
		}
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}

		if (error != null) {
			TransactionsManager.insertCreditCardWithdraw(amount,card,name,error,user,AdminManager.getWriterId(),feeCancel, null, 0L, null);
			return null;
		}

		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_CC_WITHDRAW_APPROVE;
	}

	/**
	 * Bank wire withdrawal
	 * @return
	 * @throws SQLException
	 */
	public String insertBankWire() throws SQLException {


		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

        if (ap.getSkinId() != Skin.SKIN_ETRADER) {
        	if (Utils.isParameterEmptyOrNull(wire.getBankNameTxt())) {
    			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.mandatory",null),null);
                context.addMessage("wireForm:bankNameTxt", fm);
                return null;
    		}
        }
        
        if (ap.getSkinId() == Skin.SKIN_ETRADER) {
            boolean isGoodbranchCode = false;
            for (BankBranches bankBranch : ApplicationData.getBankBranches()) {
                if (wire.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
                        wire.getBranch().equals(bankBranch.getBranchCode().toString())) {
                    isGoodbranchCode = true;
                    break;
                }
            }
            if (!isGoodbranchCode) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.branch.code",null),null);
                context.addMessage("wireForm:branchNumber", fm);
                return null;
            }

            for (SelectItem bank : ap.getBanksSI()) {
                if (wire.getBankId().equals(bank.getValue().toString())) {
                    wire.setBankNameTxt(bank.getLabel().toString());
                    break;
                }
            }

        }


		//last parameter  : true - check minimum limits,false - no check.
		String error = TransactionsManager.validateWireForm(wire.getAmount(), user, !feeCancel, checkLimits(user), AdminManager.getWriterId());		
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					for (WithdrawResult result : preWithdrawEntryResult.getWithdrawResult()) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,result.getDescription(),null);
						context.addMessage(null, fm);
					}
					return null;
				}
			}
		}
	
		if (error != null) {
			TransactionsManager.insertBankWireWithdraw(wire, error, user, AdminManager.getWriterId(), feeCancel, eftWithdraw, 0L, null);
			return null;
		}

		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, wire.getAmount());
		return Constants.NAV_BANK_WIRE_APPROVE;


	}


	/**
	 * Bank wire approve request
	 * @return
	 * @throws SQLException
	 */
	public String bankWireApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		String error = null;
		if(hasBonuses()){// cancel all bonuses that can be canceled or return error if something goes wrong
			List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
			for (BonusUsers bu : bonusUsers) {
				if (bu.isCanCanceled()) {
					error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
				}
				if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
					logger.error("Cannot cancel bonus with id: " + bu.getId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.creditcard.unapproved",null),null);
			     	context.addMessage(null, fm);
			     	//clearWithdrawForm();
					return Utils.navAfterUserOperation();
				}
			}
		}

		TransactionsManager.insertBankWireWithdraw(wire, null, user, AdminManager.getWriterId(), feeCancel, eftWithdraw, 0L, null);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		long amountLong = CommonUtil.calcAmount(wire.getAmount());

		String[] params = new String[2];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());
		long feeVal = 0;
		if(!feeCancel){
			TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
		}		
		params[1] = CommonUtil.displayAmount(feeVal, user.getCurrencyId());

		String msg = CommonUtil.getMessage("bank.wire.success",params, Utils.getWriter().getLocalInfo().getLocale());
		// Rewards plan
		if (isHavingRewardExemptFee) {
			msg = CommonUtil.getMessage("rewards.free.commission.bank.wire.success",params, Utils.getWriter().getLocalInfo().getLocale());
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);


		isCcWithdrawal = false;

		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();
	}


	/**
	 * Bank wire approve request without fee
	 * @return
	 * @throws SQLException
	 */
	public String bankWireApproveNoFee() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long amountLong = CommonUtil.calcAmount(wire.getAmount());

		TransactionsManager.insertBankWireWithdraw(wire, null, user, AdminManager.getWriterId(), true, eftWithdraw, 0L, null);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		String[] params = new String[1];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());

		String msg = CommonUtil.getMessage("bank.wire.success",params, Utils.getWriter().getLocalInfo().getLocale());

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);


		isCcWithdrawal = false;

		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();

	}

	/**
	 * Cancle bank wire request
	 * @return
	 * @throws SQLException
	 */
	public String bankWireNotApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		String msg = CommonUtil.getMessage("bank.wire.cancel",null, Utils.getWriter().getLocalInfo().getLocale());
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		return Utils.navAfterUserOperation();

	}

	/**
	 * Return dinamic message for bank wire approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getBankWireApproveMessage() throws Exception {
		return composeApproveMessage("bank.wire.approve.message", TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
	}

	/**
	 * Return formated amount for bank wire request
	 */
	public String getBankWireAmount() {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		return amount = CommonUtil.displayAmount( Long.valueOf(wire.getAmount()), user.getCurrencyId());
	}

	/**
	 * Return dinamic message for cc withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getCcApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.cc.approve.message", TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
	}

	private String composeApproveMessage(String approveMsg, int transactionType) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), Constants.BIND_USER, User.class);
//		// Rewards plan
//		if (RewardManager.isHasBenefit(String.valueOf(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE), user.getId())) {
//			isHavingRewardExemptFee = true;
//			return CommonUtil.getMessage("rewards.free.commission.approve.message", null);
//		}
        long feeVal;
        String[] params = new String[2];
        //Limit limit = UsersManagerBase.getLimitById(user.getLimitIdLongValue());
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        String amount = (String) session.getAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER);
        session.removeAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER); 
        long amounInCents = CommonUtil.calcAmount(amount);

        if(feeCancel){
        	approveMsg = "withdraw.cc.approve.message.fee.exempt";
        	return CommonUtil.getMessage(approveMsg, null);
        }
        
//		if (TransactionsManagerBase.shouldChargeForLowWithdraw(	user.getId(), limit.getAmountForLowWithdraw(),
//																CommonUtil.calcAmount(amount), new Date(), user.getUtcOffset())) {
//			feeVal = TransactionsManager.getFeeByTranTypeId(limit, TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE);
//			params[1] = String.valueOf(CommonUtil.displayAmount(limit.getAmountForLowWithdraw(), user.getCurrencyId()));
//			approveMsg += ".low.amount";
//		} else {
//			feeVal = TransactionsManager.getFeeByTranTypeId(user.getLimitIdLongValue(), transactionType);
//		}
        feeVal = TransactionsManagerBase.getTransactionHomoFee(amounInCents, user.getCurrencyId(), transactionType);
		String fee = null;

		if (user.getSkinId() != Skin.SKIN_ARABIC) {
        	fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		} else {
			fee = CommonUtil.displayAmountForInput(feeVal, false, user.getCurrencyId().intValue()) + " " +
					CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
		}

        params[0] = String.valueOf(fee);
		return CommonUtil.getMessage(approveMsg, params);
	}

	/**
	 * @return the ccHash
	 */
	public HashMap<Long, CreditCard> getCcHash() {
		return ccHash;
	}

	/**
	 * @param ccHash the ccHash to set
	 */
	public void setCcHash(HashMap<Long, CreditCard> ccHash) {
		this.ccHash = ccHash;
	}

	/**
	 * Fix balance withdrawal
	 * @return
	 * @throws SQLException
	 */
	public String insertFixBalanceWithdrawal() throws SQLException{
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		//implement if needed.
		if (false) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
			}
		}		
		
		boolean res = TransactionsManager.insertTransactionByType(amount, comments, user.getId(), user.getUserName(),
				TransactionsManagerBase.TRANS_TYPE_FIX_BALANCE_WITHDRAW, ConstantsBase.LOG_BALANCE_FIX_BALANCE_WITHDRAW, false);
		
		
		if (res) {
			UsersManager.loadUserStripFields(user, true, null);
			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}

	/**
	 * Return dinamic message for cheque withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getApproveMessage() throws Exception {
		return  composeApproveMessage("withdraw.approve.message", TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW);
	}

	/**
	 * Insert a new paypal withdrawal request
	 * @return
	 * @throws SQLException
	 */
	public String insertPayPal() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

        String error = TransactionsManagerBase.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, payPalEmail, null, null, AdminManager.getWriterId());

        if (error == null) {
        	if (!user.isHasAdminRole()) {
	        	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
	        	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
	        		error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	        		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
	        		context.addMessage(null, fm);
	        	}
        	}
        }

        
		if (error != null) {
			TransactionsManagerBase.insertPayPalWithdrawal(amount, payPalEmail, error, user, Utils.getWriter().getWriter().getId(), feeCancel, 0L);
			return null;
		}

		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);		
		return Constants.NAV_PAYPAL_WITHDRAW_APPROVE;
	}

	/**
	 * payPal approve request
	 * @return
	 * @throws SQLException
	 */
	public String payPalApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		String error = null;
		if(hasBonuses()){// cancel all bonuses that can be canceled or return error if something goes wrong
			List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
			for (BonusUsers bu : bonusUsers) {
				if (bu.isCanCanceled()) {
					error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
				}
				if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
					logger.error("Cannot cancel bonus with id: " + bu.getId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.creditcard.unapproved",null),null);
			     	context.addMessage(null, fm);
			     	//clearWithdrawForm();
					return Utils.navAfterUserOperation();
				}
			}
		}

		TransactionsManager.insertPayPalWithdrawal(amount, payPalEmail, null, user, Utils.getWriter().getWriter().getId(), feeCancel, 0L);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		long feeVal = 0;
		if(feeCancel){
			TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
		}		
		String[] params = new String[2];
		params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(amount), user.getCurrencyId().intValue());
		params[1] = CommonUtil.displayAmount(feeVal, user.getCurrencyId());

		String msg = CommonUtil.getMessage("paypal.withdrawal.success",params, Utils.getWriter().getLocalInfo().getLocale());
		if (isHavingRewardExemptFee) {
			msg = CommonUtil.getMessage("rewards.paypal.withdrawal.success",params, Utils.getWriterLocale(context));
		}
		
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);


		isCcWithdrawal = false;

		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();
	}

	/**
	 * Cancle payPal request
	 * @return
	 * @throws SQLException
	 */
	public String payPalNotApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		String msg = CommonUtil.getMessage("paypal.withdrawal.cancel",null, Utils.getWriter().getLocalInfo().getLocale());
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		return Utils.navAfterUserOperation();
	}

	/**
	 * Return dinamic message for payPal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getPayPalApproveMessage() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//		// Rewards plan
//		if (RewardManager.isHasBenefit(String.valueOf(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE), user.getId())) {
//			isHavingRewardExemptFee = true;
//			return CommonUtil.getMessage("rewards.free.commission.approve.message", null, Utils.getWriter().getLocalInfo().getLocale());
//		}
		//String maxAmount = CommonUtil.displayAmount(TransactionsManager.getMaxAmountForBankWireFee(user.getSkinId(), user.getCurrencyId()), user.getCurrency());
		long feeVal = 0;
		if(!feeCancel){
			feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
		}		
		String fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		String[] params = new String[1];
        //params[0] = String.valueOf(maxAmount);
        params[0] = String.valueOf(fee);
		return CommonUtil.getMessage("paypal.withdrawal.approve.message", params, Utils.getWriter().getLocalInfo().getLocale());
	}

	/**
	 * Return TL message for paypal
	 * @return
	 * @throws Exception
	 */
	public String getTRYConvertionMsg() throws Exception {
		double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(1, Constants.CURRENCY_TRY_ID, new Date());
		return CommonUtil.getMessage("paypal.withdrawal.TRY.message.convertion",
				new String[] {CommonUtil.displayAmount(CommonUtil.calcAmount(amountUSD), false, Constants.CURRENCY_USD_ID)});
	}

	/**
	 * Inatec CFT Credit Card withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertInatecCftCreditCardWithdraw() throws Exception {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		isCcWithdrawal = true;  // to save withdrawal type

		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());

		CreditCard card = ccHash.get(Long.valueOf(cardId));
		
		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), card, null, null, null, AdminManager.getWriterId());
		
		if (null == error) {  // check card expiration
			error = TransactionsManagerBase.validateCardExpiration(card);
			if (error != null){
				return null;
			}
		}

		if (null == error){ // validate inatec constrains
			error = TransactionsManager.validateInatecWithdraw(amount, card, Utils.getWriter().getWriter().getId() ,feeCancel, user);
			if (error != null){
				return null;
			}
		}
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}

		if (null != error) {
			TransactionsManager.insertCreditCardWithdraw(amount, card, null, error, user, Utils.getWriter().getWriter().getId(), feeCancel, null, 0L, null);
			return null;
		}
		
		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_CC_WITHDRAW_APPROVE;		
	}

	/**
	 * InsertCreditCardAndWithdraw
	 * @return
	 * @throws Exception
	 */
	public String insertCreditCardAndWithdraw() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		FacesMessage fm = new FacesMessage();
		boolean feeCancel = false;
		// cc validation
		if (!AnyOptionCreditCardValidator.validateCreditCardNumber(ccNum, user.getSkinId())) {
			setCcError(true);
			return null;
		}

		isCcWithdrawal = true;  // to save withdrawal type

	  //prevent identical requests from being processed
      if (this.saveTokenIsInvalid() ) {
          return this.logInvalidSaveAndReturn(logger, null);
      }
      this.generateSaveToken();
      
		UserBase u = TransactionsManager.getById(user.getId());

		//just insert the card
		CreditCard cc = TransactionsManager.insertCreditCard(typeId, ccNum, ccPass, expMonth, expYear, holderName, holderIdNum, amount, Utils.getWriter().getWriter().getId(), u, Constants.BACKEND_CONST, fm, "withdrawForm");

		//validate withdraw form
		String error = fm.getDetail();
		if (error == null){
			long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
			if(CommonUtil.calcAmount(amount) <= ccMinWithdraw) {
				FacesMessage fm1 = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw", null),null);
				context.addMessage("withdrawForm:amount", fm1);
				error = "error.transaction.withdraw.minamounnt";
			}
			if(error == null) {
				error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), cc, null, null, null, AdminManager.getWriterId());
			}
		}

		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, facesMessage);
				}
			}
		}
		
		//if card inserted succeffully
		if (null != cc ){
			cc.setClearingProviderId(ClearingManager.INATEC_PROVIDER_ID_CFT);
			cc.setCftAvailable(true);
			cc.setCreditEnabled(false);
			ccHash = TransactionsManager.getInatecCftCcWithrawalByUserId(user.getId());
			cardId = String.valueOf(cc.getId());
			if (null == error) {// check card expiration
				error = TransactionsManagerBase.validateCardExpiration(cc);
				if (error != null){
					return null;
				}
			}
			if (null == error) {// validate inatec constrains
				error = TransactionsManager.validateInatecWithdraw(amount, cc, Utils.getWriter().getWriter().getId(), false, user);
				if (error != null){
					return null;
				}
			}
			if (error != null) {// insert trx with validation error
				TransactionsManager.insertCreditCardWithdraw(amount, cc, null, error, user, Utils.getWriter().getWriter().getId(), feeCancel, null, 0L, null);
				return null;
			}

			if (!feeCancel) { // need to check fee logic
				((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
				return Constants.NAV_CC_WITHDRAW_APPROVE;
			}

			if (user.isSupportSelected()) {
				UsersManager.loadUserStripFields(user, true, null);
			}

			return Constants.NAV_WITHDRAW_APPROVE_NO_FEE;
		}
		return null;
	}

	/**
	 * This method is deprecated, because it was used only for updating status of transactions.
	 * Since 27.05.2013 we have TransactionUpdateForm mbean where the method was moved 
	 * @throws SQLException
	 */
	@Deprecated
	public String updateTransactionCases() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		FacesMessage fm2 = null;
		transactionUpdateStatus updateStatus = TransactionsManager.updateTransaction(transactionId, statusId, 0, 0, Utils.getWriter().getWriter().getId(), captureCode, 2);
		switch (updateStatus) {
		case TRANS_BALANCE_UPDATED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			break;
		case TRANS_UPDATED_BALANCE_FAILED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			fm2 = new FacesMessage(FacesMessage.SEVERITY_WARN, CommonUtil.getMessage("transaction.update.balance.failed", null), null);
			break;
		case TRANS_UPDATED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			break;
		case TRANS_FAILED:
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("transaction.update.problem", null), null);
			break;
		default:
			break;
		}
		
		context.addMessage(null,fm);
		if (fm2 != null) {
			context.addMessage(null,fm2);
		}
		return null;
	}

	/**
	 * @return the payPalEmail
	 */
	public String getPayPalEmail() {
		return payPalEmail;
	}

	/**
	 * @param payPalEmail the payPalEmail to set
	 */
	public void setPayPalEmail(String payPalEmail) {
		this.payPalEmail = payPalEmail;
	}


	/**
	 * @return the isCcWithdrawal
	 */
	public boolean isCcWithdrawal() {
		return isCcWithdrawal;
	}

	/**
	 * @param isCcWithdrawal the isCcWithdrawal to set
	 */
	public void setCcWithdrawal(boolean isCcWithdrawal) {
		this.isCcWithdrawal = isCcWithdrawal;
	}


	/**
	 * @return the wire
	 */
	public WireBase getWire() {
		return wire;
	}

	/**
	 * @param wire the wire to set
	 */
	public void setWire(WireBase wire) {
		this.wire = wire;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(long id) {
		if (Utils.getUser().isEtrader()) {
			this.cityId = id;
		} else {
			this.cityId = ConstantsBase.ANYOPTION_CITY_ID;
		}
	}

	public String getCityName() {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String n = (String) a.getCities().get(String.valueOf(cityId));
		if (n == null || cityId==ConstantsBase.ANYOPTION_CITY_ID)
			return cityName;
		return n;
	}

	public void setCityName(String cityName) throws SQLException {
		cityId = AdminManagerBase.getCityIdByName(cityName);
		this.cityName = cityName;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public boolean isFeeCancel() {
		return feeCancel;
	}

	public void setFeeCancel(boolean feeCancel) {
		this.feeCancel = feeCancel;
	}

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the ccNum
	 */
	public String getCcNum() {
		return ccNum;
	}

	/**
	 * @param ccNum the ccNum to set
	 */
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}

	/**
	 * @return the ccPass
	 */
	public String getCcPass() {
		return ccPass;
	}

	/**
	 * @param ccPass the ccPass to set
	 */
	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}

	/**
	 * @return the ccTypes
	 */
	public ArrayList getCcTypes() {
		return ccTypes;
	}

	/**
	 * @param ccTypes the ccTypes to set
	 */
	public void setCcTypes(ArrayList ccTypes) {
		this.ccTypes = ccTypes;
	}

	/**
	 * @return the deposit
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the expMonth
	 */
	public String getExpMonth() {
		return expMonth;
	}

	/**
	 * @param expMonth the expMonth to set
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * @return the expYear
	 */
	public String getExpYear() {
		return expYear;
	}

	/**
	 * @param expYear the expYear to set
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	/**
	 * @return the holderIdNum
	 */
	public String getHolderIdNum() {
		return holderIdNum;
	}

	/**
	 * @param holderIdNum the holderIdNum to set
	 */
	public void setHolderIdNum(String holderIdNum) {
		this.holderIdNum = holderIdNum;
	}

	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the isCcError
	 */
	public boolean isCcError() {
		return isCcError;
	}

	/**
	 * @param isCcError the isCcError to set
	 */
	public void setCcError(boolean isCcError) {
		this.isCcError = isCcError;
	}

	/**
	 * @return the sendReceipt
	 */
	public boolean isSendReceipt() {
		return sendReceipt;
	}

	/**
	 * @param sendReceipt the sendReceipt to set
	 */
	public void setSendReceipt(boolean sendReceipt) {
		this.sendReceipt = sendReceipt;
	}

	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the captureCode
	 */
	public String getCaptureCode() {
		return captureCode;
	}

	/**
	 * @param captureCode the captureCode to set
	 */
	public void setCaptureCode(String captureCode) {
		this.captureCode = captureCode;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "WithdrawForm ( "
	        + super.toString() + TAB
	        + "name = " + this.name + TAB
	        + "amount = " + this.amount + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "comments = " + this.comments + TAB
	        + "cityName = " + this.cityName + TAB
	        + "feeCancel = " + this.feeCancel + TAB
	        + "credit card = " +this.cardId + TAB
	        + " )";

	    return retValue;
	}

	public String getEnvoyAccountNum() {
		return envoyAccountNum;
	}

	public void setEnvoyAccountNum(String envoyAccountNum) {
		this.envoyAccountNum = envoyAccountNum;
	}

	public ArrayList<SelectItem> getEnvoyAccountNumList() {
		return envoyAccountNumList;
	}

	public void setEnvoyAccountNumList(ArrayList<SelectItem> envoyAccountNumList) {
		this.envoyAccountNumList = envoyAccountNumList;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * insertEnvoyWithdraw
	 * @return
	 * @throws Exception
	 */
	public String insertEnvoyWithdraw() throws Exception {

		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
       }
        this.generateSaveToken();
        FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, null, null, null, AdminManager.getWriterId());

		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}
		
		if (error != null) {
			TransactionsManager.insertWithdrawEnvoyService(amount, envoyWithdrawMethodId, user, Utils.getWriter().getWriter().getId(),
					feeCancel, error, envoyAccountNum, 0L, null);

 			return null;
		}
		isEnvoyWithdraw = true;
		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_WITHDRAW_APPROVE;    // allways display general fee message
	}

	private void initEnvoyAccountNumList(long paymentTypeId) throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		envoyAccountNumList =
			TransactionsManagerBase.getEnvoyAccountNumsByUserIdList(user.getId(),paymentTypeId);
		envoyAccountNum = "";
	}

	/**
	 * Moneybookers withdrawal
	 * @return
	 * @throws SQLException
	 */
	public String insertMoneybookersWithdraw() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

 		//last parameter  : true - check minimum limits,false - no check.
        String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, null, moneybookersEmail, null, AdminManager.getWriterId());
        
        if (error == null) {
        	if (!user.isHasAdminRole()) {
	        	WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
	        	if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
	        		error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
	        		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
	        		context.addMessage(null, fm);
	        	}
        	}
        }
        
		if (error != null) {
			TransactionsManager.insertMoneybookersWithdraw(amount, moneybookersEmail, error, user, AdminManager.getWriterId(), feeCancel);
			return null;
		}

		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_MONEYBOOKERS_WITHDRAW_APPROVE;
	}

	/**
	 * Insert WebMoney Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertDeltaPayChinaWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_DELTAPAY_CHINA;
        FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, null, null, null, AdminManager.getWriterId());
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}
		
		if (error != null) {
			TransactionsManager.insertDeltaPayChinaWithdraw(amount, error, user, CommonUtil.getWebWriterId(), feeCancel, 0L);
			return null;
		}
		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_WITHDRAW_APPROVE;
	}

	/**
	 * Insert DeltaPay China Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertWebMoneyWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_WEBMONEY;
        FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel, false, checkLimits(user), null, null, null, null, AdminManager.getWriterId());
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}

		if (error != null) {
			TransactionsManager.insertWebMoneyWithdraw(amount, error, user, Utils.getWriter().getWriter().getId(), feeCancel, webMoneyPurse, 0L, null);
			return null;
		}
		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_WITHDRAW_APPROVE_WEBMONEY;
	}
	
	/**
	 * Insert BaroPay Withdraw
	 * @return
	 * @throws Exception
	 */
	public String insertBaroPayWithdraw() throws Exception {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid()) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        withdrawType = WITHDRAW_TYPE_BAROPAY;
        FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		String error = TransactionsManager.validateWithdrawForm(amount, user, !feeCancel,  false, checkLimits(user), null, null, null, null, AdminManager.getWriterId());
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}
		
		if (error != null) {
			TransactionsManager.insertBaroPayWithdraw(amount, error, user, Utils.getWriter().getWriter().getId(), feeCancel, baroPayRequest.getBankName(), baroPayRequest.getAccountOwner(), baroPayRequest.getAccountNumber(), 0L, null); 
			return null;
		}
		((HttpSession) context.getExternalContext().getSession(false)).setAttribute(WITHDRAW_AMOUNT_SESSION_PARAMETER, amount);
		return Constants.NAV_WITHDRAW_APPROVE;
	}	

	public String getInitWebmoneyWithdraw() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		webMoneyPurseList = TransactionsManagerBase.getWebMoneyPursesByUserId(user.getId());
		if (null != webMoneyPurseList && webMoneyPurseList.size() > 0 ) {
			webMoneyPurse = webMoneyPurseList.get(0).getLabel().toString();
		}
		return null;
	}

	/**
	 * Return dinamic message for bank wire approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getMoneybookersMessage() throws Exception {
		return composeApproveMessage("withdraw.fee.approve.question", TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
	}

	/**
	 * Change Value DropDownBox
	 * @param event
	 * @throws SQLException
	 */
    public void updateChange(ValueChangeEvent event) throws Exception {
    	String envoyType = (String)event.getNewValue();
    	if(envoyType != null && ! envoyType.equals("")) {
    		if (envoyType.equals("Webmoney")) {
    			envoyWithdrawMethodId = TransactionsManagerBase.PAYMENT_TYPE_WEB_MONEY;
    			initEnvoyAccountNumList(envoyWithdrawMethodId);
        	} else if (envoyType.equals("Moneta")) {
        		envoyWithdrawMethodId = TransactionsManagerBase.PAYMENT_TYPE_MONETA;
        		initEnvoyAccountNumList(envoyWithdrawMethodId);
        	}
    	}
    }

    /**
     * @return the eftWithdraw
     */
    public boolean isEftWithdraw() {
        return eftWithdraw;
    }

    /**
     * @param eftWithdraw the eftWithdraw to set
     */
    public void setEftWithdraw(boolean eftWithdraw) {
        this.eftWithdraw = eftWithdraw;
    }

	/**
	 * @return the moneybookersEmail
	 */
	public String getMoneybookersEmail() {
		return moneybookersEmail;
	}

	/**
	 * @param moneybookersEmail the moneybookersEmail to set
	 */
	public void setMoneybookersEmail(String moneybookersEmail) {
		this.moneybookersEmail = moneybookersEmail;
	}

	/**
	 * Moneybookers approve request
	 * @return
	 * @throws SQLException
	 */
	public String moneybookersApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		long loginId = 0L;
		try {
			if (session.getAttribute(Constants.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(Constants.LOGIN_ID);
			}
		} catch (Exception e) {
			logger.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		String error = null;
		if(hasBonuses()){// cancel all bonuses that can be canceled or return error if something goes wrong
			List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
			for (BonusUsers bu : bonusUsers) {
				if (bu.isCanCanceled()) {
					error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
				}
				if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
					logger.error("Cannot cancel bonus with id: " + bu.getId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.creditcard.unapproved",null),null);
			     	context.addMessage(null, fm);
			     	//clearWithdrawForm();
					return Utils.navAfterUserOperation();
				}
			}
		}

		TransactionsManager.insertMoneybookersWithdraw(amount, moneybookersEmail, null, user, AdminManager.getWriterId(), feeCancel);

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		long amountLong = CommonUtil.calcAmount(amount);

		String[] params = new String[2];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());
		long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
		params[1] = CommonUtil.displayAmount(feeVal, user.getCurrencyId());

		String msg = CommonUtil.getMessage("moneybookers.withdraw.success",params, Utils.getWriter().getLocalInfo().getLocale());

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);


		isCcWithdrawal = false;

		WithdrawEntryResult postWithdrawEntryResult = WithdrawUtil.postWithdrawEntry(new WithdrawEntryInfo(user));
		
		return Utils.navAfterUserOperation();
	}

	/**
	 * Cancle moneybookers request
	 * @return
	 * @throws SQLException
	 */
	public String moneybookersNotApprove() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		String msg = CommonUtil.getMessage("moneybookers.withdraw.cancel",null, Utils.getWriter().getLocalInfo().getLocale());
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		return Utils.navAfterUserOperation();

	}

	/**
	 * Return dinamic message for WebMoney withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getWebMoneyApproveMessage() throws Exception {
		return composeApproveMessage("withdraw.fee.approve.question", TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW);
	}

	/**
	 * @return the webMoneyPurseList
	 */
	public ArrayList<SelectItem> getWebMoneyPurseList() {
		return webMoneyPurseList;
	}

	/**
	 * @param webMoneyPurseList the webMoneyPurseList to set
	 */
	public void setWebMoneyPurseList(ArrayList<SelectItem> webMoneyPurseList) {
		this.webMoneyPurseList = webMoneyPurseList;
	}

	/**
	 * @return the webMoneyPurse
	 */
	public String getWebMoneyPurse() {
		return webMoneyPurse;
	}

	/**
	 * @param webMoneyPurse the webMoneyPurse to set
	 */
	public void setWebMoneyPurse(String webMoneyPurse) {
		this.webMoneyPurse = webMoneyPurse;
	}

	/**
	 * @return the baroPayRequest
	 */
	public BaroPayRequest getBaroPayRequest() {
		return baroPayRequest;
	}

	/**
	 * @param baroPayRequest the baroPayRequest to set
	 */
	public void setBaroPayRequest(BaroPayRequest baroPayRequest) {
		this.baroPayRequest = baroPayRequest;
	}
	
	/**
     * get bank wire withdrawal.
     * @return bankWireWithdrawal as SelectItem
     * @throws Exception
     */
    public ArrayList<SelectItem> getBankWireWithdrawal() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		ArrayList<SelectItem> bankWireWithdrawal = new ArrayList<SelectItem>();
		bankWireWithdrawal.addAll(CommonUtil.translateSI(ApplicationData.getSkinById(user.getSkinId()).getBankWireWithdraw()));
    	return bankWireWithdrawal;
	}


	/**
	 * @return the bankWireWithdrawalId
	 */
	public long getBankWireWithdrawalId() {
		return bankWireWithdrawalId;
	}


	/**
	 * @param bankWireWithdrawalId the bankWireWithdrawalId to set
	 */
	public void setBankWireWithdrawalId(long bankWireWithdrawalId) {
		this.bankWireWithdrawalId = bankWireWithdrawalId;
		wire.setBankId(String.valueOf(bankWireWithdrawalId));
		try {
			ArrayList<SelectItem> bankWireWithdraw = getBankWireWithdrawal();
			for (SelectItem si : bankWireWithdraw) {
				if (bankWireWithdrawalId == (Long)si.getValue()) {
					wire.setBankNameTxt(si.getLabel());
					break;
				}
			}
		} catch (Exception e) {
			logger.error("error, can't get label from bank wire withdrawal", e);
		}
	}


	/**
	 * @return the bankOtherNameTxt
	 */
	public String getBankOtherNameTxt() {
		return bankOtherNameTxt;
	}


	/**
	 * @param bankOtherNameTxt the bankOtherNameTxt to set
	 */
	public void setBankOtherNameTxt(String bankOtherNameTxt) {
		this.bankOtherNameTxt = bankOtherNameTxt;
		if (getBankWireWithdrawalId() == ConstantsBase.BANK_WIRE_OTHER_BANK) {
			wire.setBankNameTxt(bankOtherNameTxt);
		}
	}

	/**
	 * @return the isHavingRewardExemptFee
	 */
	public boolean isHavingRewardExemptFee() {
		return isHavingRewardExemptFee;
	}

	/**
	 * @param isHavingRewardExemptFee the isHavingRewardExemptFee to set
	 */
	public void setHavingRewardExemptFee(boolean isHavingRewardExemptFee) {
		this.isHavingRewardExemptFee = isHavingRewardExemptFee;
	}
	
	public String getApproveMessageForBonuses() throws Exception {
		return  CommonUtil.getMessage("withdraw.approve.message.bonuses", null);
	}
	
	
	public boolean hasBonuses() {
		boolean hasBonuses = false;
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_USER, User.class);
		User user = (User) ve.getValue(context.getELContext());
		WithdrawBonusRegulationStateMethodResult wbrsmr = BonusManagerBase.getWithdrawBonusRegulationState(user.getId(), user.getSkinId());
		if (wbrsmr.getState().toString().equals("REGULATED_OPEN_BONUSES") || wbrsmr.getState().toString().equals("NOT_REGULATED_OPEN_BONUSES")) {
			hasBonuses = true;
		}
		return hasBonuses;
	}
}
