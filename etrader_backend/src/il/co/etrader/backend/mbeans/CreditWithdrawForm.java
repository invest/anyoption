package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

public class CreditWithdrawForm extends FormBase implements Serializable {

	private static final long serialVersionUID = -3135411625778677600L;

	private static final Logger logger = Logger.getLogger(CreditWithdrawForm.class);

	private String amount;
	private CreditCard creditCard;
	private String cardId;

	private long depositTrxId;
	private long creditAmount;
	// Rewards plan
	private boolean isHavingRewardExemptFee;


	public CreditWithdrawForm() throws NumberFormatException, SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		// get data from queryString
		String depositTrxIdParam = request.getParameter("depoTrx");
		if (!CommonUtil.isParameterEmptyOrNull(depositTrxIdParam)) {
			depositTrxId = Long.parseLong(depositTrxIdParam);
		}
		String creditAmountParam = request.getParameter("creditAmount");
		if (!CommonUtil.isParameterEmptyOrNull(creditAmountParam)) {
			creditAmount = Long.parseLong(creditAmountParam);
		}

		String cardId = request.getParameter("ccId");
		creditCard = UsersManager.getById(Long.valueOf(cardId));
		creditCard.setCreditEnabled(true);
		creditCard.setCreditAmount(creditAmount);
		creditCard.setId(Long.valueOf(cardId));
		isHavingRewardExemptFee = false;
	}


	/**
	 * Credit withdrawal
	 * @return
	 * @throws Exception
	 */
	public String insertCreditWithdraw() throws Exception {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
		if( CommonUtil.calcAmount(amount) <= ccMinWithdraw) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw", null),null);
			context.addMessage("withdrawForm:amount", fm);
			return "error.transaction.withdraw.minamounnt";
		}
		
		String error = TransactionsManager.validateWithdrawForm(amount, user, true, false,false, creditCard, null, null, null, AdminManager.getWriterId());

		if (null == error) {
			Transaction t = TransactionsManager.getTransaction(depositTrxId);
			if (t.getClearingProviderId() > 0) {
				creditCard.setClearingProviderId(t.getClearingProviderId());
			}
			if (null != t && t.getClearingProviderId() == ClearingManager.XOR_AO_PROVIDER_ID /* &&  cc in AMEX list */ ) {
				CreditCard cc = TransactionsManagerBase.getCreditCard(t.getCreditCardId());
				if (TransactionsManagerBase.isCcInBinsList(cc.getCcNumber())){
					error = CommonUtil.getMessage("error.transaction.credit.xor.mc", null);
				}
			}
		}
		
		if (error == null) {
			if (!user.isHasAdminRole()) {
				WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil.preWithdrawEntry(new WithdrawEntryInfo(user));
				if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
					error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(error, null),null);
					context.addMessage(null, fm);
				}
			}
		}


		if (error != null) {
			TransactionsManager.insertCreditCardWithdraw(amount,creditCard,null,error,user,AdminManager.getWriterId(),false, depositTrxId, 0L, null);
			return null;
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		return Constants.NAV_CREDIT_WITHDRAW_APPROVE;
	}


	/**
	 * approve the credit action
	 * @return
	 * @throws SQLException
	 */
	public String approveCreditWithdraw() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long amountLong = CommonUtil.calcAmount(amount);

		String error = TransactionsManager.insertCreditCardWithdraw(amount,creditCard,null,null,user,AdminManager.getWriterId(),true,depositTrxId, 0L, null);
		if ( error != null ) {
			return Constants.NAV_EMPTY_SUBTABS;
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		String[] params = new String[1];
		params[0] = CommonUtil.displayAmount(amountLong, user.getCurrencyId().intValue());

		String msg ="";
		msg = CommonUtil.getMessage("withdraw.credit.success",params, Utils.getWriterLocale(context));
		if (isHavingRewardExemptFee) {
			msg = CommonUtil.getMessage("rewards.withdraw.credit.success",params, Utils.getWriterLocale(context));
		}
		
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);


		return Constants.NAV_PARENT_REFRESH;

	}

	/**
	 * Cancel credit action
	 * @return
	 * @throws SQLException
	 */
	public String notApproveWithdraw() throws SQLException {

		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}

		this.generateSaveToken();

		FacesContext context = FacesContext.getCurrentInstance();

		String msg = CommonUtil.getMessage("withdraw.cancel",null, Utils.getWriterLocale(context));
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null);
		context.addMessage(null,fm);

		return Constants.NAV_PARENT_REFRESH;

	}

	public String getAmountTxt() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (amount!=null && !amount.equals("")) {
			return CommonUtil.displayAmount((long)((float)Float.parseFloat(amount)*(float)100), user.getCurrencyId().intValue());
		} else
			return "";
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CreditWithdrawForm ( "
	        + super.toString() + TAB
	        + "amount = " + this.amount + TAB
	        + "credit card = " +this.cardId + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}


	/**
	 * Return dinamic message for credit withdrawal approve
	 * @return
	 * 		formated message
	 * @throws Exception
	 */
	public String getCcApproveMessage() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//		// Rewards plan
//		if (RewardManager.isHasBenefit(String.valueOf(RewardBenefit.REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE), user.getId())) {
//			isHavingRewardExemptFee = true;
//			return CommonUtil.getMessage("rewards.free.commission.approve.message", null, Utils.getWriter().getLocalInfo().getLocale());
//		}
		long feeVal = TransactionsManagerBase.getFeeByTranTypeId(user.getLimitIdLongValue(), TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
		String fee = CommonUtil.displayAmount(feeVal, user.getCurrencyId());
		String[] params = new String[1];
        params[0] = String.valueOf(fee);
		return CommonUtil.getMessage("withdraw.cc.approve.message", params);
	}

	/**
	 * get creditAmount Txt
	 * @return
	 */
	public String getCreditAmountTxt() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return CommonUtil.displayAmount(creditAmount, user.getCurrencyId());
	}

	/**
	 * @return the depositTrxId
	 */
	public long getDepositTrxId() {
		return depositTrxId;
	}

	/**
	 * @param depositTrxId the depositTrxId to set
	 */
	public void setDepositTrxId(long depositTrxId) {
		this.depositTrxId = depositTrxId;
	}


	/**
	 * @return the creditAmount
	 */
	public long getCreditAmount() {
		return creditAmount;
	}


	/**
	 * @param creditAmount the creditAmount to set
	 */
	public void setCreditAmount(long creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * @return the creditCard
	 */
	public CreditCard getCreditCard() {
		return creditCard;
	}


	/**
	 * @param creditCard the creditCard to set
	 */
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}


	/**
	 * @return the isHavingRewardExemptFee
	 */
	public boolean isHavingRewardExemptFee() {
		return isHavingRewardExemptFee;
	}


	/**
	 * @param isHavingRewardExemptFee the isHavingRewardExemptFee to set
	 */
	public void setHavingRewardExemptFee(boolean isHavingRewardExemptFee) {
		this.isHavingRewardExemptFee = isHavingRewardExemptFee;
	}

}
