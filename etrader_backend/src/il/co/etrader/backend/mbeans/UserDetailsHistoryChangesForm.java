package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.UserDetailsHistoryChanges;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;


public class UserDetailsHistoryChangesForm implements Serializable {

	private static final long serialVersionUID = -5011753706723094515L;
	private static final Logger logger = Logger.getLogger(UserDetailsHistoryChangesForm.class);
	
	private ArrayList<UserDetailsHistoryChanges> historyList;
	
	//Filters
	private Date from;
	private Date to;
	private long fieldId;
	private long writerId;
	
	public UserDetailsHistoryChangesForm() {	
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		
		gc.add(Calendar.MONTH, -6);
		from = gc.getTime();
		fieldId = 0;
		writerId = 0;
		search();
	}
	
	public void search() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		try {
			historyList = UsersManager.getUserDetailsHistoryChangesList(user.getId(), from, to, fieldId, writerId);
			CommonUtil.setTablesToFirstPage();
		} catch (SQLException e) {
			logger.debug("Can't getUserDetailsHistoryChangesList", e);
		} 		
	}

	public ArrayList<UserDetailsHistoryChanges> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(ArrayList<UserDetailsHistoryChanges> historyList) {
		this.historyList = historyList;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public long getFieldId() {
		return fieldId;
	}

	public void setFieldId(long fieldId) {
		this.fieldId = fieldId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public int getListSize() {		
		return historyList.size();
	}
}
