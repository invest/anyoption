package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ReutersQuotesManager;
import il.co.etrader.backend.bl_vos.ReutersQuotesBE;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class ReutersQuotesForm implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ReutersQuotesForm.class);
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private long marketId;
    private long scheduledId;
    private Date from;
	private Date to;
	private ArrayList<ReutersQuotesBE> reutersQuotesList;

	public ReutersQuotesForm() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(Utils.getWriter().getUtcOffset()));
		reutersQuotesList = new ArrayList<ReutersQuotesBE>();
		gc.set(Calendar.HOUR_OF_DAY, 0 + gc.getTimeZone().getRawOffset()/(1000*60*60));
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		to = gc.getTime();
		from = gc.getTime();
		sortColumn = "";
		prevSortColumn = "";
		marketId = 0;
		scheduledId = 0;
		serchFilters();
	}

	/**
	 *
	 *
	 */
	public void serchFilters() {
		Calendar calendarFrom = Calendar.getInstance();
		calendarFrom.setTime(from);
		calendarFrom.set(Calendar.HOUR_OF_DAY, 0);
		calendarFrom.set(Calendar.MINUTE, 0);
		calendarFrom.set(Calendar.SECOND, 0);
		calendarFrom.set(Calendar.MILLISECOND, 0);
		Calendar calendarTo = Calendar.getInstance();
		calendarTo.setTime(to);
		calendarTo.set(Calendar.HOUR_OF_DAY, 23);
		calendarTo.set(Calendar.MINUTE, 59);
		calendarTo.set(Calendar.SECOND, 59);
		calendarTo.set(Calendar.MILLISECOND, 999);

		try {
			reutersQuotesList = ReutersQuotesManager.getAllData(marketId, scheduledId, calendarFrom.getTime(), calendarTo.getTime());
		} catch (SQLException e) {
			logger.error("ERROR GET REUTERS QUOTES DATA", e);
		}
		CommonUtil.setTablesToFirstPage();
	}

	/**
	 *
	 * @return
	 */
	public String sortList() {
		//Same column was clicked.
		if (sortColumn.equals(prevSortColumn))
			sortAscending =! sortAscending;
		else
			sortAscending = true;

		prevSortColumn=sortColumn;
		if (sortColumn.equals("time"))
			Collections.sort(reutersQuotesList, new TimeComparator(sortAscending));

		/* Add sort columns*/

		CommonUtil.setTablesToFirstPage();

		return null;
	}

	private class TimeComparator implements Comparator {
		private boolean ascending;

		public TimeComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
			ReutersQuotesBE op1 = (ReutersQuotesBE)o1;
			ReutersQuotesBE op2 = (ReutersQuotesBE)o2;
			if (ascending) {
				return op1.getTime().compareTo(op2.getTime());
			}
			return -op1.getTime().compareTo(op2.getTime());
		}
  	 }

	/**
	 * @return the reutersQuotesList
	 */
	public ArrayList<ReutersQuotesBE> getReutersQuotesList() {
		return reutersQuotesList;
	}
	/**
	 * @param reutersQuotesList the reutersQuotesList to set
	 */
	public void setReutersQuotesList(
			ArrayList<ReutersQuotesBE> reutersQuotesList) {
		this.reutersQuotesList = reutersQuotesList;
	}
	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}
	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}
	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}
	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}
	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	/**
	 *
	 * @return
	 */
	public int getListSize() {
		return reutersQuotesList.size();
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the scheduledId
	 */
	public long getScheduledId() {
		return scheduledId;
	}

	/**
	 * @param scheduledId the scheduledId to set
	 */
	public void setScheduledId(long scheduledId) {
		this.scheduledId = scheduledId;
	}

	/**
	 * @return the from
	 */

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}
}
