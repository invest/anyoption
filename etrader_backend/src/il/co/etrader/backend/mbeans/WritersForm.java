package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;

public class WritersForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3550928305745270759L;
	private Writer writer;
	private ArrayList list;


	public WritersForm() throws SQLException{

		search();

	}

	public String updateInsert() throws SQLException{
		//boolean res=AdminManager.updateInsertWriter(writer);
		//if (res==false)
		//	return null;
		search();
		return Constants.NAV_WRITERS;
	}

	public String navEdit() {
		return Constants.NAV_WRITER;
	}

	public String navList() throws SQLException{
		search();
		return Constants.NAV_WRITERS;
	}

	public String navNew() throws SQLException{
		writer=new Writer();

		return Constants.NAV_WRITER;
	}

	public String search() throws SQLException{
		list=AdminManager.searchWriters();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}
	public Writer getWriter() {
		return writer;
	}
	public void setWriter(Writer writer) {
		this.writer = writer;
	}



}
