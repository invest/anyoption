package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.util.CommonUtil;


public class CampaignsForm implements Serializable {

	private static final long serialVersionUID = 5730605078390266798L;

	private Date from;
	private Date to;
	private long paymentType;
	private long paymentRecipient;
	private String campaignName;
	private String campaignId;
	private ArrayList<MarketingCampaign> list;
	private MarketingCampaign campaign;
	private boolean disToDate;
	private long marketingChannel;
	private long marketingCampaignManager;

	public CampaignsForm() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		//gc.set(Calendar.HOUR_OF_DAY, 0);
		//gc.set(Calendar.MINUTE, 0);
		//gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		// set the from date to 01/01/2007 to show all campaigns
		gc.set(2007, 0, 1);
		from = gc.getTime();
		disToDate = true;

		updateList();
	}


	public String updateList() throws SQLException{

	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;

        if (u.isMarketingSelected() || u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingCampaigns(from, to, paymentType, campaignName, campaignId, paymentRecipient, writerIdForSkin, marketingChannel, marketingCampaignManager);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert campaign
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		campaign.setLastUpdatedBy(Utils.getWriter().getWriter().getId());

		boolean res = MarketingManager.insertUpdateCampaign(campaign);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_CAMPAIGNS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit campaign
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {

		// set date to writer offset
		Date startWithOffset = CommonUtil.getDateTimeFormaByTz(campaign.getStartDate(), Utils.getWriter().getUtcOffset());
		campaign.setStartDate(startWithOffset);
        long currentWriterId = Utils.getWriter().getWriter().getId();
        //campaign.setCampaignManagerId(currentWriterId);
		if ( null != campaign.getEndDate() ) {
			Date endWithOffset = CommonUtil.getDateTimeFormaByTz(campaign.getEndDate(), Utils.getWriter().getUtcOffset());
			campaign.setEndDate(endWithOffset);
		} else {
			campaign.setDisToDate(true);
		}

		return Constants.NAV_MARKETING_CAMPAIGN;
	}

	/**
	 * Navigate to insert new campaign
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
        long currentWriterId = Utils.getWriter().getWriter().getId();
		campaign = new MarketingCampaign();
		// set current date to writer offset
		Date startWithOffset = CommonUtil.getDateTimeFormaByTz(new Date(), Utils.getWriter().getUtcOffset());
		campaign.setStartDate(startWithOffset);
		campaign.setEndDate(startWithOffset);
		campaign.setWriterId(currentWriterId);
		//Only in NavNew we get the current writer from DB (in NavEdit we get it from sql)
        campaign.setCreatedByWriter(CommonUtil.getWriterName(currentWriterId));
        campaign.setCampaignManagerId(MarketingCampaign.MARKETING_MANAGER_WRITER_ID);
		return Constants.NAV_MARKETING_CAMPAIGN;
	}

	/**
	 * On change event (enable toDate)
	 * @param event
	 * @throws SQLException
	 */
    public void updateChange(ValueChangeEvent event) throws SQLException{
    	disToDate = (Boolean)event.getNewValue();
    	if (disToDate) {
    		to = null;
    	} else {
    		to = new Date();
    	}

    	updateList();

    }

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the paymentType
	 */
	public long getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(long paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the campaign
	 */
	public MarketingCampaign getCampaign() {
		return campaign;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(MarketingCampaign campaign) {
		this.campaign = campaign;
	}

	/**
	 * @return the disToDate
	 */
	public boolean isdisToDate() {
		return disToDate;
	}


	/**
	 * @param disToDate the disToDate to set
	 */
	public void setdisToDate(boolean disToDate) {
		this.disToDate = disToDate;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the paymentRecipient
	 */
	public long getPaymentRecipient() {
		return paymentRecipient;
	}

	/**
	 * @param paymentRecipient the paymentRecipient to set
	 */
	public void setPaymentRecipient(long paymentRecipient) {
		this.paymentRecipient = paymentRecipient;
	}


	public long getMarketingChannel() {
		return marketingChannel;
	}


	public void setMarketingChannel(long marketingChannel) {
		this.marketingChannel = marketingChannel;
	}
	
	public void validateUnitPrice (FacesContext context, UIComponent comp, Object value){
		 String unitPrice  = (String)value;
//       unitPrice = unitPrice.replace("," , ".");
//		 int start = unitPrice.indexOf(".");
//		 
//		 if(unitPrice!=null && !unitPrice.isEmpty()){
//			try {
//				   if (Double.parseDouble(unitPrice)<0){
//					   FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("compaigns.unit.price.error", null), null);
//					   throw new ValidatorException(msg);
//				   }
//				} catch (NumberFormatException e) {
//				 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("compaigns.unit.price.error", null), null);
//				 throw new ValidatorException(msg);
//				}
//		 }
//		  if ((start !=-1) && (unitPrice.substring(start+1, unitPrice.length()).length())>5)
//	    {
//			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("compaigns.unit.price.error", null), null);
//			throw new ValidatorException(msg);
//		}
		 
		 Pattern pattern =Pattern.compile("^[+]?\\d{1,10}+(\\.{0,1}\\,{0,1}(\\d{0,5}))?$");
		 Matcher matcher = pattern.matcher(unitPrice);
		 if(!matcher.matches()){
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("compaigns.unit.price.error", null), null);
				throw new ValidatorException(msg);
			 
		 }
		 
	}


	public long getMarketingCampaignManager() {
		return marketingCampaignManager;
	}


	public void setMarketingCampaignManager(long marketingCampaignManager) {
		this.marketingCampaignManager = marketingCampaignManager;
	}

}
