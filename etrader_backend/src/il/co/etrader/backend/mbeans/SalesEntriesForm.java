package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.AssignWriter;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.SkinAssigns;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.AutoDialerReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class SalesEntriesForm implements Serializable {

	private static final long serialVersionUID = -6673507499819988039L;
	private static final Logger logger = Logger.getLogger(SalesEntriesForm.class);
	
	/* Entries's data */
	private ArrayList<PopulationEntry> entriesList;
	private PopulationEntry populationEntry;
	
	/* Filters - parameters */
	private Date fromDate;
	private Date toDate;
	private long campaignId;
	private int retentionStatusColor;
	private long writerId;
	private int issueActionTypeId;
	private String username;
	private boolean hideNoAnswer;
	private long countriesGroupId;
	private long lastCallerWriterId;
	private int campaignPriority;
	private Date fromLogin;
	private Date toLogin;
	private boolean isLastLogin;
	private boolean assignOption;
	private int minCallsCountCurrPop;
	private int maxCallsCountCurrPop;
	private int minCallsCountGeneral;
	private int maxCallsCountGeneral;
	private long languageId;
	
	/* Filters - lists */
	private ArrayList<String> timeZoneList;
	private ArrayList<SelectItem> issueActionTypeIds;
	private ArrayList<SelectItem> countriesList;
	private ArrayList<String> selectedCountriesList;
	private ArrayList<String> populationTypesList;
	private ArrayList<String> affiliatesKeysList;
	
	/* Group assign */
	private ArrayList<SkinAssigns> skinsAssigns;
	private boolean assignAll;
	private long notAssignedNum;
	
	/**
	 * constructor for SalesEntriesForm
	 */
	public SalesEntriesForm() {
		logger.info("Start SalesEntriesForm constructor.");
		entriesList = new ArrayList<PopulationEntry>();
		initParams();
		search();
		logger.info("End SalesEntriesForm constructor.");
	}
	
	/**
	 * Initial Parameters.
	 */
	public void initParams() {
		/* Initial parameters */
		GregorianCalendar gc = new GregorianCalendar();
		toDate = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, -3);
		fromDate = gc.getTime();
        retentionStatusColor = 0;
        username = null;
        hideNoAnswer = false;
        fromLogin = null;
        toLogin = null;
        isLastLogin = false;
        assignOption = false;
		campaignId = ConstantsBase.ALL_FILTER_ID;
		countriesGroupId = ConstantsBase.ALL_FILTER_ID;
        lastCallerWriterId = ConstantsBase.ALL_FILTER_ID;
        languageId = ConstantsBase.ALL_FILTER_ID;
        issueActionTypeId = ConstantsBase.ALL_FILTER_ID_INT;
        maxCallsCountCurrPop = PopulationEntriesManager.MAX_CALLS_COUNT_CURR_POP;
        maxCallsCountGeneral = PopulationEntriesManager.MAX_CALLS_COUNT_GENERAL;
        writerId = ConstantsBase.NONE_FILTER_ID;
        User user = Utils.getUser();
		if (user.isRetentionSelected()) {
			writerId = Utils.getWriter().getWriter().getId();
		}
        
        /* Initial lists */
		timeZoneList = new ArrayList<String>();
		timeZoneList.add(ConstantsBase.ALL_TIME_ZONE_POPULATION);
        selectedCountriesList = new ArrayList<String>();
        selectedCountriesList.add(String.valueOf(Constants.ALL_FILTER_ID));
        populationTypesList = new ArrayList<String>();
        populationTypesList.add(String.valueOf(Constants.ALL_FILTER_ID));
        affiliatesKeysList = new ArrayList<String>();
        affiliatesKeysList.add(String.valueOf(Constants.ALL_FILTER_ID));
        countriesList = new ArrayList<SelectItem>();
		issueActionTypeIds = new ArrayList<SelectItem>();
		issueActionTypeIds.add(new SelectItem (String.valueOf(Constants.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null)));
		for(IssueActionType iat: ApplicationData.getActionTypesList()) {
			if(iat.getChannelId()==1) {
				issueActionTypeIds.add(new SelectItem( iat.getId(), CommonUtil.getMessage(iat.getName(), null)));
			}
		}
        updateCountries(timeZoneList);
        initGroupAssign(user);
        
        /* Only for sales deposit type conversion */
        Writer w = Utils.getWriter().getWriter();
    	w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION);
	}
	
	/**
	 * Initial parameters for group assign.
	 * @param user
	 */
	public void initGroupAssign(User user) {
		WriterWrapper w = Utils.getWriter();
		skinsAssigns = new ArrayList<SkinAssigns>();
		assignAll = false;
		ArrayList<Skins> skinsSorted = (ArrayList<Skins>) ApplicationData.getSkinsList().clone();
		Collections.sort(skinsSorted);
		if (user.isPartnerWriter()) { // have only 1 skin
			skinsSorted = new ArrayList<Skins>();
			skinsSorted.add(ApplicationData.getSkinById(w.getSkins().get(0)));
		}
		for (Skins s : skinsSorted) {
			SkinAssigns sa = new SkinAssigns();
			sa.setSkinId(Long.valueOf(s.getId()));
			sa.setSkinDescription(s.getDisplayName());
			ArrayList<AssignWriter> writers = new ArrayList<AssignWriter>();
			ArrayList<SelectItem> reWriters = w.getSalesWriters(sa.getSkinId());
			for (SelectItem i : reWriters) {
				AssignWriter aw = new AssignWriter();
				aw.setWriterId((Long) i.getValue());
				aw.setWriterName(i.getLabel());
				writers.add(aw);
			}
			sa.setWriters(writers);
			sa.setAssignAll(false);
			if (sa.getWriters().size() > 0) {
				skinsAssigns.add(sa);
			}
		}
		assignOption = false;
		notAssignedNum = 0;
	}
	
	/**
	 * Initial parameters before search.
	 */
	public void initParamsBeforeSearch() {
		// User name is after trim so "only spaces"
		// case will be ignored.
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)) {
			username = null;
		} 
		if (username != null) {
			username = username.toUpperCase();
		}
	}
	
	/**
	 * Search function.
	 * Get all relevant data according to filters.
	 */
	public void search() {
		logger.info("Start search entries for conversion.");
		try {
			initParamsBeforeSearch();
			entriesList = PopulationEntriesManager.getEntriesConversion(this);
		} catch (Exception e) {
			logger.error("Can't load Entries list for conversion :", e);
		}
		logger.info("End search entries for conversion.");
	}
	
	/**
	 * Lock Entry.
	 * @return navigate tab as String
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public String lockEntry() throws SQLException, PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, false);
		if (res) {
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;
	}

	/**
	 * Unlock Entry.
	 * @return null
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public String unLockEntry() throws SQLException,
			PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		populationEntry.setLockHistoryId(0);
		return null;
	}
	
	/**
	 * Update assigned leads.
	 * @return navigate tab as String
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
	public String updateAssignedLeads() throws PopulationHandlersException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		long writerId = wr.getWriter().getId();
		int countUpdatedLeads = 0;
		int successfullyUpdatedLeads = 0;
		for (PopulationEntry entry : entriesList) {
			long lastAssignWriterId = entry.getAssignWriterId();
			if (lastAssignWriterId != entry.getTempAssignWriterId()) {
				++countUpdatedLeads;
				if (entry.getTempAssignWriterId() == 0) {
					// cancel assing
					if (PopulationEntriesManager.cancelAssign(entry, false, false)) {
						successfullyUpdatedLeads++;
					}
				} else {
					// assign or reassign
					/* Only for sales deposit type conversion */
					entry.updateAssignedWriter();
					if (PopulationEntriesManager.assign(entry, false, lastAssignWriterId, false, false, writerId)) {
						successfullyUpdatedLeads++;
					}
				}
			}
		}
		if (successfullyUpdatedLeads > 0) {
			search();
			String msg = countUpdatedLeads + " " + CommonUtil.getMessage("retention.entry.multi.assign.success", null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
			context.addMessage(null, fm);
		}
		/* Only for sales deposit type conversion */
		return null;
	}
	
	/**
	 * Update Countries.
	 * @param GMT as List<String>
	 */
	public void updateCountries(List<String> GMT) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		countriesList.clear();
		countriesList.add(new SelectItem (String.valueOf(Constants.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null)));
		WriterWrapper w = Utils.getWriter();
		if (!GMT.contains("0")) {
			for (Country c : ApplicationData.getCountries().values()) {
				if (GMT.contains(c.getGmtOffset())) {
					list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
							CommonUtil.getMessage(c.getDisplayName(), null)
									+ ", "
									+ Utils.getUtcDifference(w.getUtcOffset(),
											c.getGmtOffset())));
				}
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		} else {
			for (Country c : ApplicationData.getCountries().values()) {
				list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
						CommonUtil.getMessage(c.getDisplayName(), null)
								+ ", "
								+ Utils.getUtcDifference(w.getUtcOffset(), c
										.getGmtOffset())));
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		}
		countriesList.addAll(list);
	}
	
	/**
	 * Update countries.
	 */
	public void updateCountries() {
		updateCountries(timeZoneList);
	}
	
	/**
	 * Assign Pressed
	 * @return null
	 */
	public String assignPressed() {
		assignOption = !assignOption;
		return null;
	}
	
	/**
	 * Get Rows Number
	 * @return Number Of Rows as long
	 */
	public long getRowsNumber() {
		return entriesList.size();
	}
	
	/**
	 * Get from login.
	 * When the check box 'Made login in the last X hours' clicked, 
	 * than the function will return current date minus X hours.
	 * else the function will return the member 'fromLogin'.
	 * @return tmpFromLogin as Date
	 */
	public Date getTempFromLogin() {
		Date tmpFromLogin = fromLogin;
		if (fromLogin != null) {
			tmpFromLogin = CommonUtil.getDateTimeFormat(fromLogin,  Utils.getWriter().getUtcOffset());
		}
		if (isLastLogin) {
			try {
				Calendar cal = Calendar.getInstance();
				int lastLoginMinutes = Integer.valueOf(CommonUtil.getEnum(Constants.ENUM_LAST_LOGIN_ENUMERATOR, Constants.ENUM_LAST_LOGIN_LEVEL_ONE_CODE));
				cal.add(Calendar.MINUTE, -(int)lastLoginMinutes);
				tmpFromLogin = cal.getTime();
			} catch (Exception e) {
				logger.error("Can't get enum for last login.", e);
			}
		}
		return tmpFromLogin;
	}
	
	/**
	 * Get to login.
	 * When the check box 'Made login in the last X hours' clicked, 
	 * than the function will return current date.
	 * else the function will return the member 'toLogin'.
	 * @return tmpToLogin as Date
	 */
	public Date getTempToLogin() {
		Date tmpToLogin = toLogin;
		if (toLogin != null) {
			tmpToLogin = CommonUtil.getDateTimeFormat(CommonUtil.addDay(toLogin),  Utils.getWriter().getUtcOffset());
		}
		if (isLastLogin) {
			Calendar cal = Calendar.getInstance();
			tmpToLogin = cal.getTime();
		}
		return tmpToLogin;
	}
	
	/**
	 * Get Constant Max Calls Count Current Population 
	 * @return MAX_CALLS_COUNT_CURR_POP as int
	 */
	public int getMaxCallsCountCurrPopConst() {
		return PopulationEntriesManager.MAX_CALLS_COUNT_CURR_POP;
	}
	
	/**
	 * Get Constant Max Calls Count General
	 * @return MAX_CALLS_COUNT_GENERAL as int
	 */
	public int getMaxCallsCountGeneralConst() {
		return PopulationEntriesManager.MAX_CALLS_COUNT_GENERAL;
	}
	
	/**
	 * check if is UnAssign filter.
	 * @return true when the filter UnAssign
	 */
	public boolean isUnassignFilter() {
		if (writerId == ConstantsBase.NONE_FILTER_ID) {
			return true;
		}
		return false;
	}
	
	/**
	 * Set assign to all skins.
	 * @param assignAll
	 *            the assignAll to set
	 */
	public void setAssignToAllSkins(boolean assignAll) {
		this.assignAll = assignAll;
		for (SkinAssigns sa : skinsAssigns) {
			sa.setAssignAllWriters(assignAll);
		}
	}

	/**
	 * Set assign for skin.
	 * @param assign
	 * @param skinId
	 */
	public void setAssignForSkin(boolean assign, long skinId) {
		if (!assign) {
			assignAll = assign;
		}
		for (SkinAssigns sa : skinsAssigns) {
			if (skinId == sa.getSkinId()) {
				sa.setAssignAllWriters(assign);
				break;
			}
		}
	}

	/**
	 * Set assign for skin and writer.
	 * @param assign
	 * @param skinId
	 * @param writerId
	 */
	public void setAssignForSkinAndWriter(boolean assign, long skinId,
			long writerId) {
		if (!assign) {
			assignAll = assign;
		}
		for (SkinAssigns sa : skinsAssigns) {
			if (skinId == sa.getSkinId()) {
				if (!assign) {
					sa.setAssignAll(false);
				}
				for (AssignWriter w : sa.getWriters()) {
					if (w.getWriterId() == writerId) {
						w.setAssign(assign);
						break;
					}
				}
			}
		}
	}

	/**
	 * Divide equal tasks for each assigned writer.
	 * @return null
	 */
	public String equally() {
		long entriesNum = getRowsNumber();
		long selectedAssign = getSelectedAssign();
		long popPerWriter = entriesNum / selectedAssign;
		notAssignedNum = entriesNum - (popPerWriter * selectedAssign);
		// set tasks for each writer
		ArrayList<AssignWriter> assignedWriters = new ArrayList<AssignWriter>();
		for (SkinAssigns sa : skinsAssigns) {
			for (AssignWriter w : sa.getWriters()) {
				if (w.isAssign()) {
					w.setTasks(popPerWriter);
					assignedWriters.add(w);
				}
			}
		}
		// give not assigned tasks randomly
		long notAssignedNumCP = notAssignedNum;
		if (notAssignedNumCP > 0) {
			Collections.shuffle(assignedWriters);
			for (int i = 0; i < assignedWriters.size() && notAssignedNumCP > 0; i++, notAssignedNumCP--) {
				AssignWriter w = assignedWriters.get(i);
				w.setTasks(w.getTasks() + 1);
			}
		}
		return null;
	}
	
	/**
	 * Get number of selected assign writers.
	 * @return number assign as long.
	 */
	public long getSelectedAssign() {
		long numAssign = 0;
		if (assignAll) {
			for (SkinAssigns sa : skinsAssigns) {
				numAssign += sa.getWriters().size();
			}
		} else {
			for (SkinAssigns sa : skinsAssigns) {
				if (sa.isAssignAll()) {
					numAssign += sa.getWriters().size();
				} else {
					for (AssignWriter w : sa.getWriters()) {
						if (w.isAssign()) {
							numAssign++;
						}
					}
				}
			}
		}
		return numAssign;
	}
	
	/**
	 * Is equally available.
	 * @return true if equally available.
	 */
	public boolean isEquallyAvailable() {
		for (SkinAssigns sa : skinsAssigns) {
			if (sa.isHasAssignWriters()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Reload page.
	 * @return null
	 */
	public String reload() {
		return null;
	}
	
	/**
	 * give tasks to assigned writers by writer tasks.
	 */
	public String giveTasks() throws PopulationHandlersException, SQLException {
		// shuffle population entries list
		Collections.shuffle(entriesList);
		// give tasks
		long assignedTasks = PopulationEntriesManager.giveTasks(entriesList, skinsAssigns);
		assignOption = false; // collect the tree nodes
		setAssignToAllSkins(false);
		String[] params = new String[2];
		params[0] = String.valueOf(assignedTasks);
		params[1] = String.valueOf(getRowsNumber());
		Utils.popupMessage("retention.submit.assign.done", params);
		search();
		return null;
	}
	
	/**
	 * 
	 * @return Automatic Dialer - Export result to mail.
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */
	public String autoDialerExportResultToMail() throws IOException, SQLException, ParseException {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = Utils.getWriter();
		String reportName = "voice_spin_report_";
		logger.info("Process " + reportName);
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(fromDate) + "_" + fmt.format(toDate) + ".csv";
		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter().getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.enteries.report.invalid.mail", null), null);
			context.addMessage(null, fm);
			return null;
		}
		AutoDialerReportSender sender = new AutoDialerReportSender(this, filename, wr);
		sender.start();
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, 
				filename + " " + CommonUtil.getMessage("enteries.report.processing.mail", null), null);
		context.addMessage(null, fm);
		return null;
	}
	
	/**
	 * get List Style.
	 * @param list
	 * @return String for style
	 */
	public String getListStyle(ArrayList<String> list) {
		if (list.contains(String.valueOf(Constants.ALL_FILTER_ID))) {
			return CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null);
		}
		return ConstantsBase.EMPTY_STRING;
	}

	/**
	 * @return the entriesList
	 */
	public ArrayList<PopulationEntry> getEntriesList() {
		return entriesList;
	}

	/**
	 * @param entriesList the entriesList to set
	 */
	public void setEntriesList(ArrayList<PopulationEntry> entriesList) {
		this.entriesList = entriesList;
	}

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the timeZoneList
	 */
	public ArrayList<String> getTimeZoneList() {
		return timeZoneList;
	}

	/**
	 * @param timeZoneList the timeZoneList to set
	 */
	public void setTimeZoneList(ArrayList<String> timeZoneList) {
		this.timeZoneList = timeZoneList;
	}

	/**
	 * @return the countriesList
	 */
	public ArrayList<SelectItem> getCountriesList() {
		return countriesList;
	}

	/**
	 * @param countriesList the countriesList to set
	 */
	public void setCountriesList(ArrayList<SelectItem> countriesList) {
		this.countriesList = countriesList;
	}

	/**
	 * @return the retentionStatusColor
	 */
	public int getRetentionStatusColor() {
		return retentionStatusColor;
	}

	/**
	 * @param retentionStatusColor the retentionStatusColor to set
	 */
	public void setRetentionStatusColor(int retentionStatusColor) {
		this.retentionStatusColor = retentionStatusColor;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the issueActionTypeIds
	 */
	public ArrayList<SelectItem> getIssueActionTypeIds() {
		return issueActionTypeIds;
	}

	/**
	 * @param issueActionTypeIds the issueActionTypeIds to set
	 */
	public void setIssueActionTypeIds(ArrayList<SelectItem> issueActionTypeIds) {
		this.issueActionTypeIds = issueActionTypeIds;
	}

	/**
	 * @return the issueActionTypeId
	 */
	public int getIssueActionTypeId() {
		return issueActionTypeId;
	}

	/**
	 * @param issueActionTypeId the issueActionTypeId to set
	 */
	public void setIssueActionTypeId(int issueActionTypeId) {
		this.issueActionTypeId = issueActionTypeId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username.trim();
	}

	/**
	 * @return the hideNoAnswer
	 */
	public boolean isHideNoAnswer() {
		return hideNoAnswer;
	}

	/**
	 * @param hideNoAnswer the hideNoAnswer to set
	 */
	public void setHideNoAnswer(boolean hideNoAnswer) {
		this.hideNoAnswer = hideNoAnswer;
	}

	/**
	 * @return the lastCallerWriterId
	 */
	public long getLastCallerWriterId() {
		return lastCallerWriterId;
	}

	/**
	 * @param lastCallerWriterId the lastCallerWriterId to set
	 */
	public void setLastCallerWriterId(long lastCallerWriterId) {
		this.lastCallerWriterId = lastCallerWriterId;
	}

	/**
	 * @return the campaignPriority
	 */
	public int getCampaignPriority() {
		return campaignPriority;
	}

	/**
	 * @param campaignPriority the campaignPriority to set
	 */
	public void setCampaignPriority(int campaignPriority) {
		this.campaignPriority = campaignPriority;
	}

	/**
	 * @return the fromLogin
	 */
	public Date getFromLogin() {
		return fromLogin;
	}

	/**
	 * @param fromLogin the fromLogin to set
	 */
	public void setFromLogin(Date fromLogin) {
		this.fromLogin = fromLogin;
	}

	/**
	 * @return the toLogin
	 */
	public Date getToLogin() {
		return toLogin;
	}

	/**
	 * @param toLogin the toLogin to set
	 */
	public void setToLogin(Date toLogin) {
		this.toLogin = toLogin;
	}

	/**
	 * @return the selectedCountriesList
	 */
	public ArrayList<String> getSelectedCountriesList() {
		return selectedCountriesList;
	}

	/**
	 * @param selectedCountriesList the selectedCountriesList to set
	 */
	public void setSelectedCountriesList(ArrayList<String> selectedCountriesList) {
		this.selectedCountriesList = selectedCountriesList;
	}

	/**
	 * @return the populationTypesList
	 */
	public ArrayList<String> getPopulationTypesList() {
		return populationTypesList;
	}

	/**
	 * @param populationTypesList the populationTypesList to set
	 */
	public void setPopulationTypesList(ArrayList<String> populationTypesList) {
		this.populationTypesList = populationTypesList;
	}

	/**
	 * @return the affiliatesKeysList
	 */
	public ArrayList<String> getAffiliatesKeysList() {
		return affiliatesKeysList;
	}

	/**
	 * @param affiliatesKeysList the affiliatesKeysList to set
	 */
	public void setAffiliatesKeysList(ArrayList<String> affiliatesKeysList) {
		this.affiliatesKeysList = affiliatesKeysList;
	}

	/**
	 * @return the isLastLogin
	 */
	public boolean isLastLogin() {
		return isLastLogin;
	}

	/**
	 * @param isLastLogin the isLastLogin to set
	 */
	public void setLastLogin(boolean isLastLogin) {
		this.isLastLogin = isLastLogin;
	}

	/**
	 * @return the assignOption
	 */
	public boolean isAssignOption() {
		return assignOption;
	}

	/**
	 * @param assignOption the assignOption to set
	 */
	public void setAssignOption(boolean assignOption) {
		this.assignOption = assignOption;
	}

	/**
	 * @return the countriesGroupId
	 */
	public long getCountriesGroupId() {
		return countriesGroupId;
	}

	/**
	 * @param countriesGroupId the countriesGroupId to set
	 */
	public void setCountriesGroupId(long countriesGroupId) {
		this.countriesGroupId = countriesGroupId;
	}

	/**
	 * @return the minCallsCountCurrPop
	 */
	public int getMinCallsCountCurrPop() {
		return minCallsCountCurrPop;
	}

	/**
	 * @param minCallsCountCurrPop the minCallsCountCurrPop to set
	 */
	public void setMinCallsCountCurrPop(int minCallsCountCurrPop) {
		this.minCallsCountCurrPop = minCallsCountCurrPop;
	}

	/**
	 * @return the maxCallsCountCurrPop
	 */
	public int getMaxCallsCountCurrPop() {
		return maxCallsCountCurrPop;
	}

	/**
	 * @param maxCallsCountCurrPop the maxCallsCountCurrPop to set
	 */
	public void setMaxCallsCountCurrPop(int maxCallsCountCurrPop) {
		this.maxCallsCountCurrPop = maxCallsCountCurrPop;
	}

	/**
	 * @return the minCallsCountGeneral
	 */
	public int getMinCallsCountGeneral() {
		return minCallsCountGeneral;
	}

	/**
	 * @param minCallsCountGeneral the minCallsCountGeneral to set
	 */
	public void setMinCallsCountGeneral(int minCallsCountGeneral) {
		this.minCallsCountGeneral = minCallsCountGeneral;
	}

	/**
	 * @return the maxCallsCountGeneral
	 */
	public int getMaxCallsCountGeneral() {
		return maxCallsCountGeneral;
	}

	/**
	 * @param maxCallsCountGeneral the maxCallsCountGeneral to set
	 */
	public void setMaxCallsCountGeneral(int maxCallsCountGeneral) {
		this.maxCallsCountGeneral = maxCallsCountGeneral;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the skinsAssigns
	 */
	public ArrayList<SkinAssigns> getSkinsAssigns() {
		return skinsAssigns;
	}

	/**
	 * @param skinsAssigns the skinsAssigns to set
	 */
	public void setSkinsAssigns(ArrayList<SkinAssigns> skinsAssigns) {
		this.skinsAssigns = skinsAssigns;
	}

	/**
	 * @return the assignAll
	 */
	public boolean isAssignAll() {
		return assignAll;
	}

	/**
	 * @param assignAll the assignAll to set
	 */
	public void setAssignAll(boolean assignAll) {
		this.assignAll = assignAll;
	}

	/**
	 * @return the notAssignedNum
	 */
	public long getNotAssignedNum() {
		return notAssignedNum;
	}

	/**
	 * @param notAssignedNum the notAssignedNum to set
	 */
	public void setNotAssignedNum(long notAssignedNum) {
		this.notAssignedNum = notAssignedNum;
	}
}
