package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignmentInfo;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;



public class MasterSearchForm implements Serializable {

	private static final long serialVersionUID = 2559808032752561408L;

	private static final Logger logger = Logger.getLogger(MasterSearchForm.class);

	private PopulationEntry populationEntry;
	private ArrayList<PopulationEntry> populationEntriesList;
	private PopulationEntry populationEntryForImport;

	// Filters
	private Long userId;
	private String username;
	private Long contactId;
	private boolean isLockedEntriesSearch;
	private boolean isUserInNoPopulation;


	// Import fields
	private boolean isImportUser;
	private ArrayList<SelectItem> specialPopulationIdList;
	private long specialPopulationId;
	private long importUserSkin;
	private String comments;
	private boolean isPasswordOk;
	private String password;
	User userSearched;

	public MasterSearchForm()  {
		initFormFilters();
	}

	private void initFormFilters() {
		userId = null;
		username = null;
		contactId = null;
		isLockedEntriesSearch = false;

		initImportFields();

	}


	private void initImportFields(){
		specialPopulationIdList = null;
		specialPopulationId = 0;
		isUserInNoPopulation = false;
		isImportUser = false;
		isPasswordOk = false;
		password = null;
		populationEntryForImport = null;
		userSearched = null;
		comments = null;
	}

	public String search() throws SQLException{
    	FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;

		long userIdValue;
		long contactIdValue;

		initImportFields();

		// User name and User Id number are after trim so "only spaces" case will be ignored.
		if (null != username && username.equals(ConstantsBase.EMPTY_STRING)){
			username = null;
		}

		if (null == userId && null == contactId && null == username && isLockedEntriesSearch == false){
			populationEntriesList = new ArrayList<PopulationEntry>();

			return null;
		}

		if (null != userId){
			userIdValue = userId.longValue();
		}else {
			userIdValue = 0;
		}

		if (null != contactId){
			contactIdValue = contactId.longValue();
		}else {
			contactIdValue = -1;
		}

		populationEntriesList = PopulationEntriesManager.getMasterSearchUsers(userIdValue, username, contactIdValue, isLockedEntriesSearch);

		if (populationEntriesList.size() == 0){

			if (null != userId ||  null != username){
				if (null != userId){
					userSearched = UsersManager.getUserDetails(userId);
				}
				if (null != username){
					userSearched = UsersManager.getByUserName(username);
				}

				if (null == userSearched ||
						(null != userSearched && (userSearched.isFalseAccount() || !userSearched.isActive() || userSearched.isTestUser()))){
					// User not found
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.notfound", null),null);
	    			context.addMessage(null, fm);

				} else{
					isUserInNoPopulation = true;
					importUserSkin = userSearched.getSkinId();
					fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.user.not.in.sales", null),null);
	    			context.addMessage(null, fm);

				}
			}

			if (null != contactId){
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.contact.not.found", null),null);
    			context.addMessage(null, fm);

			}

			if (isLockedEntriesSearch == true){
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.no.locked.entries", null),null);
    			context.addMessage(null, fm);

			}

		} else { // if getMasterSearchUsers is not empty
			if (null != userId ||  null != username || null != contactId){

				// it means we found a specific user
				if (populationEntriesList.size() == 1){
					populationEntryForImport = populationEntriesList.get(0);

					// if user doesn't belong to no population enable import
					if (populationEntryForImport.getCurrEntryId() == 0){
						isUserInNoPopulation = true;
						importUserSkin = populationEntryForImport.getSkinId();
					}
				}
			}
		}

		if(isUserInNoPopulation){
			if (userIdValue > 0) {
				UserBase user = UsersManager.getUserById(userIdValue);
				ArrayList<Population> populationsList = null;
				if (user.getFirstDepositId() > 0) {
					populationsList = PopulationsManager.getPopulationList(PopulationsManagerBase.POP_TYPE_SPECIAL_RETENTION, importUserSkin, true, null, 0);
				} else {
					populationsList = PopulationsManager.getPopulationList(PopulationsManagerBase.POP_TYPE_SPECIAL_CONVERSION, importUserSkin, true, null, 0);
				}
				
				specialPopulationIdList = new ArrayList<SelectItem>();

				for (Population p : populationsList){
	               // if (!(p.getName().equals("Special - CB deposit")))
					specialPopulationIdList.add(new SelectItem(p.getId(),p.getName()));
				}
			}
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public String importUserUpdate() throws SQLException{

		isImportUser = !isImportUser;
		return null;
	}

	public String submitImport() throws SQLException{
    	FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;

		// Check comment
		if (null != comments){
			if (comments.length() > 2000){
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.commments.long", null),null);
				context.addMessage("masterSearchForm:comments", fm);
				return null;
			}
			if (comments.equals(ConstantsBase.EMPTY_STRING)){
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
				context.addMessage("masterSearchForm:comments", fm);
				return null;
			}
		}

		// population handle
		PopulationEntryBase p = null;
		long UserIdForImport;
		long ContactIdForImport;
		User user;

		// insert user into Special entry
		if (null != populationEntryForImport){
			UserIdForImport = populationEntryForImport.getUserId();
			ContactIdForImport =  populationEntryForImport.getContactId();
			user = UsersManager.getUserDetails(UserIdForImport);
		}else{
			UserIdForImport = userSearched.getId();
			ContactIdForImport = 0;
			user = userSearched;
		}

		p = PopulationsManager.getPopulationUserByUserAndContactId(UserIdForImport,ContactIdForImport);

		if (null == p){
			p = new PopulationEntryBase();
			p.setUserId(UserIdForImport);
			p.setContactId(ContactIdForImport);
			if (null != user){
				p.setSkinId(user.getSkinId());
			}
			else{ //for contacts
				p.setSkinId(populationEntryForImport.getSkinId());
			}
		}else {
			// Check that user isn't in another population when importing
			if (p.getCurrEntryId() != 0){
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.import.user.has.curr.entry", null),null);
				context.addMessage(null, fm);

				return search();
			}
		}

        if (specialPopulationId == -1) {
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
            context.addMessage("masterSearchForm:specialPopulationId", fm);
            return null;
        }
        p.setPopulationId(specialPopulationId);
       
        
		try {
			if (populationEntry.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
				PopulationsManager.insertIntoPopulationEntries(p,0);
	
				insertIssueForImport(p,comments,user);
			} else {
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.import.user.remove.permanently", null),null);
				context.addMessage(null, fm);
				return null;
			}
		} catch (SQLException e) {
			p = PopulationsManager.getPopulationUserByUserAndContactId(UserIdForImport,ContactIdForImport);

			if (null != p){
				if (p.getPopulationUserId() != 0 && p.getCurrEntryId() != 0){
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.import.user.has.curr.entry", null),null);
					context.addMessage(null, fm);

					return search();
				}
			}

			String errMsg = CommonUtil.getMessage("error.import.failed", null) + " for userId: " + UserIdForImport + " contactId: " + ContactIdForImport;

			logger.error(errMsg, e);
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,errMsg,null);
			context.addMessage(null, fm);

		}


		return search();
	}


	private void insertIssueForImport(PopulationEntryBase p, String comments, User user){
		Writer w = Utils.getWriter().getWriter();
	    Issue issue = new Issue();
	   	IssueAction issueAction = new IssueAction();

		// Filling issues fields
	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_POPULATION));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setPopulationEntryId(p.getCurrEntryId());
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		issue.setContactId(user.getContactId());
		// Filling action fields
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT)); //EMAIL
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
		issueAction.setWriterId(w.getId());
		issueAction.setSignificant(false);
		issueAction.setActionTime(cal.getTime());
		issueAction.setComments(comments);

		try{
			// Insert Issue & action
			IssuesManager.insertIssue(issue, issueAction, user, ConstantsBase.SCREEN_SALEM);
		} catch(SQLException e){
			logger.fatal("Exception performing action on user :"+user);

		}
	}


	public String lockEntry() throws SQLException,PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, false);
		if (res){
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;

	}

	public String unLockEntry() throws SQLException,PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		search();
		return null;
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 * @return
	 * @throws SQLException
	 */
	public String loadEntry() throws SQLException {
		return PopulationEntriesManager.loadEntry(populationEntry);
	}

	/**
	 * Assign action
	 * in case the entry locked, unlock done before the assign
	 * @return
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
    public String assign() throws PopulationHandlersException, SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		long writerId = Utils.getWriter().getWriter().getId();
		long lastAssignWriterId = populationEntry.getAssignWriterId();
    	if (populationEntry.getPopulationSalesTypeId() == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT ||
    			populationEntry.getPopulationSalesTypeId() == PopulationsManagerBase.SALES_TYPE_RETENTION) { //assign limit for retention writers
    			//boolean reachedTotalLimit = PopulationEntriesManager.reachedTotalEntries(populationEntry.getAssignWriterId(), populationEntry.getSkinId());
    			long statusId = populationEntry.getUserStatusId();
    			// check if reached total limit ok.
    			long tempAssignWriterId = populationEntry.getTempAssignWriterId();
    			PopulationUsersAssignmentInfo puai = PopulationEntriesManager.isAssignmentOk(tempAssignWriterId, populationEntry.getSkinId(), populationEntry.getUserRankId(), statusId);
    			// check if count success deposit ok.
    			boolean countSuccessDepositOk = PopulationEntriesManager.isCountSuccessDepositOk(populationEntry.getUserId(), statusId);
    			if ((puai.isAssignmentOk() && countSuccessDepositOk) || user.isHasSAdminRole() || user.isHasSalesMExt()) {// Omri Keinan,Eldan Jerbi and Eliran Noy writer id, they are the only one who can assign without limit
    				if (populationEntry.getTimeJoinControlGroup() != null && !user.isHasSAdminRole() && !user.isHasSalesMExt()) {
        				String msg = CommonUtil.getMessage("retention.entry.control.group.assign.not.success", null);
        				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg,	null);
        				context.addMessage(null, fm);
        				return null;
    				} else {
    					populationEntry.updateAssignedWriter();
    					PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, false, false, writerId);
    				}
    			} else {
    				String msg = CommonUtil.getMessage("retention.entry.multi.assign.not.success", null);
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg,	null);
    				context.addMessage(null, fm);
        			if ( tempAssignWriterId > 0 && puai.isReachedTotalLimit()) {
        				String[] params = new String[1];
        				WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        				Writer w = wr.getAllWritersMap().get(tempAssignWriterId);
        				params[0] = w.getUserName();
        				msg = CommonUtil.getMessage("retention.entry.assign.not.success.limit", params);
            			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
            			context.addMessage(null, fm);
        			}
    			}
    	} else {
    		populationEntry.updateAssignedWriter();
    		PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, false, false, writerId);
    	}
    	return null;
    }

    /**
     * Like assign action
     */
    public String reAssign() throws PopulationHandlersException, SQLException {
    	assign();
    	return null;
    }

    /**
	 * Cancel assign action
	 * in case the entry locked, unlock done before the cancel
     * @return
     * @throws PopulationHandlersException
     * @throws SQLException
     */
    public String cancelAssign() throws PopulationHandlersException, SQLException {
    	PopulationEntriesManager.cancelAssign(populationEntry, true, false);
    	return null;
    }

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}


	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username.trim();
	}

	/**
	 * @return the populationEntriesList
	 */
	public ArrayList<PopulationEntry> getPopulationEntriesList() {
		return populationEntriesList;
	}

	/**
	 * @param populationEntriesList the populationEntriesList to set
	 */
	public void setPopulationEntriesList(
			ArrayList<PopulationEntry> populationEntriesList) {
		this.populationEntriesList = populationEntriesList;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the isLockedEntriesSearch
	 */
	public boolean isLockedEntriesSearch() {
		return isLockedEntriesSearch;
	}

	/**
	 * @param isLockedEntriesSearch the isLockedEntriesSearch to set
	 */
	public void setLockedEntriesSearch(boolean isLockedEntriesSearch) {
		this.isLockedEntriesSearch = isLockedEntriesSearch;
	}

	public int getListSize() {
		return populationEntriesList.size();
	}


	/**
	 * @return the isUserInNoPopulation
	 */
	public boolean isUserInNoPopulation() {
		return isUserInNoPopulation;
	}

	/**
	 * @param isUserInNoPopulation the isUserInNoPopulation to set
	 */
	public void setUserInNoPopulation(boolean isUserInNoPopulation) {
		this.isUserInNoPopulation = isUserInNoPopulation;
	}

	/**
	 * @return the specialPopulationId
	 */
	public long getSpecialPopulationId() {
		return specialPopulationId;
	}

	/**
	 * @param specialPopulationId the specialPopulationId to set
	 */
	public void setSpecialPopulationId(long specialPopulationId) {
		this.specialPopulationId = specialPopulationId;
	}

	/**
	 * @return the specialPopulationIdList
	 */
	public ArrayList<SelectItem> getSpecialPopulationIdList() {
		return specialPopulationIdList;
	}

	/**
	 * @param specialPopulationIdList the specialPopulationIdList to set
	 */
	public void setSpecialPopulationIdList(
			ArrayList<SelectItem> specialPopulationIdList) {
		this.specialPopulationIdList = specialPopulationIdList;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments.trim();
	}

	/**
	 * @return the importUserSkin
	 */
	public long getImportUserSkin() {
		return importUserSkin;
	}

	/**
	 * @param importUserSkin the importUserSkin to set
	 */
	public void setImportUserSkin(long importUserSkin) {
		this.importUserSkin = importUserSkin;
	}

	/**
	 * @return the isImportUser
	 */
	public boolean isImportUser() {
		return isImportUser;
	}

	/**
	 * @param isImportUser the isImportUser to set
	 */
	public void setImportUser(boolean isImportUser) {
		this.isImportUser = isImportUser;
	}

	public void initMasterSearchForm() throws SQLException{
		initFormFilters();
		search();
	}

	/**
	 * @return the isPasswordOk
	 */
	public boolean isPasswordOk() {
		return isPasswordOk;
	}

	/**
	 * @param isPasswordOk the isPasswordOk to set
	 */
	public void setPasswordOk(boolean isPasswordOk) {
		this.isPasswordOk = isPasswordOk;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public String checkPassword() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm;
		String enumerator = null;
		enumerator = CommonUtil.getEnum(Constants.ENUM_MASTER_SEARCH_ENUMERATOR,Constants.ENUM_MASTER_SEARCH_CODE);
		if (!CommonUtil.isParameterEmptyOrNull(password) &&
				enumerator.equalsIgnoreCase(password)){
 	    	isPasswordOk = true;
	    	isImportUser = true;
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("opportunities.cancel.wrong.password", null),null);
			context.addMessage(null, fm);

       }
		return null;
	}

	public String importFromRetentionToConversion(){
		if (populationEntry.getPopulationSalesTypeId() != PopulationsManagerBase.SALES_TYPE_CONVERSION && populationEntry.getUserId() > 0) {
			try {
				if (populationEntry.isAssignToWriter()) {
					PopulationEntriesManager.cancelAssign(populationEntry, true, false);
					populationEntry.setAssignWriterId(0);
				}
				
				PopulationsManagerBase.insertIntoPopulation(populationEntry.getContactId(), populationEntry.getSkinId(), populationEntry.getUserId(), "" ,
						0, PopulationsManagerBase.POP_TYPE_MANUALLY_IMPORTED_CONVERSION, populationEntry);
				FacesContext context = FacesContext.getCurrentInstance();
				String msg = CommonUtil.getMessage("retention.entry.user.imported.to.conversion", null);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg,	null);
				context.addMessage(null, fm);
			} catch (Exception e) {
				logger.error("Failed to import to conversion", e);
				FacesContext context = FacesContext.getCurrentInstance();
				String msg = CommonUtil.getMessage("retention.entry.user.imported.to.conversion.fail", null);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg,	null);
				context.addMessage(null, fm);
			}
		}
		return null;
	}

}
