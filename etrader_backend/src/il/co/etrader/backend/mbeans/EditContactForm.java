package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.util.CommonUtil;

public class EditContactForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(EditContactForm.class);

	private Contact contact;
	private long skinId;

	public EditContactForm() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		PopulationEntry  populationEntry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
		contact = ContactsManager.getContactByID(populationEntry.getContactId());
		skinId = contact.getSkinId();
	}

	public String updateContact() {
		FacesContext context = FacesContext.getCurrentInstance();
		contact.setSkinId(skinId);
		try {
			ContactsManager.updateContact(contact);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("user.update.success",
					null), null);
			logger.info(CommonUtil.getMessage("user.update.success", null));
			context.addMessage(null, fm);
		} catch (SQLException e) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.faild",
					null), null);
			logger.info(CommonUtil.getMessage("general.update.faild", null));
			context.addMessage(null, fm);
			e.printStackTrace();
		}
		return Constants.NAV_EMPTY_SUBTABS;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
}
