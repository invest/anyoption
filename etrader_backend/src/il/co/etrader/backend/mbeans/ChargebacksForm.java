package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.ChargeBack;

import il.co.etrader.backend.bl_managers.ChargeBacksManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class ChargebacksForm extends FormBase implements Serializable {

	private static final long serialVersionUID = 3807210811962657358L;

	private static final Logger logger = Logger.getLogger(ChargebacksForm.class);

	private ArrayList list;
	private long statusId;
	private long classId;
	private String htmlBuffer;
	private String userName;
	private String tranId;
	private String arn;
	private int state;
	private long currencyId;
	private long providerId;

	/**
	 * @return the providerId
	 */
	public long getProviderId() {
		return providerId;
	}


	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}


	public ChargebacksForm() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		tranId = "";
		arn = "";
		state = 0;

		if (user.isSupportSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
			userName = user.getUserName();
		}else {
			userName ="";
		}
		updateList();
	}


	public int getListSize() {
		return list.size();
	}
	public String updateList() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		userName = userName.toUpperCase();

		if (!user.isSupportSelected())
			list=TransactionsManager.getChargeBacks(0,statusId,userName, classId, tranId, arn,state, currencyId, providerId);
		else
			list=TransactionsManager.getChargeBacks(user.getId(),statusId,userName, classId, tranId, arn,state, currencyId, providerId);

		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}


	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setClassId(long classId ) {
		this.classId = classId;
	}

	public long getClassId() {
		return this.classId;
	}

	/**
	 * Export data to excel
	 * @throws IOException
	 */
	public void exportHtmlTableToExcel() throws IOException {

		//Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>" + htmlBuffer + "</html>";

        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "chargebacks_report_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();
    }


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ChargebacksForm ( "
	        + super.toString() + TAB
	        + "list = " + this.list + TAB
	        + "statusId = " + this.statusId + TAB
	        + " )";

	    return retValue;
	}


	public String getHtmlBuffer() {
		return htmlBuffer;
	}


	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}

	public String insertChargeBack() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		ChargeBack cb = (ChargeBack)context.getApplication().createValueBinding(Constants.BIND_CHARGE_BACK).getValue(context);

		try {
			cb.setWriterId(Utils.getWriter().getWriter().getId());
			ChargeBacksManager.insertChargeBack(cb);
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("chargebacks.insert.success", null, Utils.getWriterLocale(context)),null);
		}catch (Exception e) {
			logger.error("Problem insert ChargeBack for user: " + cb.getUserId() + " tran: " + cb.getTransactionId(),e);
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("chargebacks.insert.failed", null, Utils.getWriterLocale(context)),null);
		}
		context.addMessage(null, fm);
        


		return cb.doNavigation();
	}

	public String updateChargeBack() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		ChargeBack cb = (ChargeBack)context.getApplication().createValueBinding(Constants.BIND_CHARGE_BACK).getValue(context);

		try {
			ChargeBacksManager.updateChargeBack(cb);
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("chargebacks.update.success", null, Utils.getWriterLocale(context)),null);
		}catch (Exception e) {
			logger.error("Problem update ChargeBack id: " + cb.getId(),e);
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("chargebacks.update.failed", null, Utils.getWriterLocale(context)),null);
		}
		context.addMessage(null, fm);
        

		return cb.doNavigation();
	}

	/**
	 * Validator Method
	 * (validate the name property)
	 * @param context
	 * @param input
	 */
	public void validateDigits(FacesContext context, UIComponent component,
			Object input) throws ValidatorException {

		String var = (String)input;

		try {
			Pattern regex = Pattern.compile("\\D+",
					Pattern.CANON_EQ);
			Matcher matcher = regex.matcher(var);
			if (matcher.find()) {
				throw new ValidatorException(
						new FacesMessage("Must contains digits only", null));
			}
		} catch (PatternSyntaxException ex) {
		}
	}

	/**
	 * @return the arn
	 */
	public String getArn() {
		return arn;
	}


	/**
	 * @param arn the arn to set
	 */
	public void setArn(String arn) {
		this.arn = arn;
	}


	/**
	 * @return the tranId
	 */
	public String getTranId() {
		return tranId;
	}


	/**
	 * @param tranId the tranId to set
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}


	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}


	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}


	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
}