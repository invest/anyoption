package il.co.etrader.backend.mbeans;

import il.co.etrader.bl_managers.PopulationsManagerBase;

/**
 * DeclineAfterApprovedForm
 * @author eyalo
 */
public class DeclineAfterApprovedForm extends PopulationsEntriesForm {
	private static final long serialVersionUID = -2153808498991598537L;

	public DeclineAfterApprovedForm() {
		super();
	}
	
	public void init() {
		super.init();
		setSupportMoreOptionsType(PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS);
	}
}
