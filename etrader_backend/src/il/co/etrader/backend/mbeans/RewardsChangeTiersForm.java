package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.beans.RewardTier;

import il.co.etrader.backend.bl_managers.RewardsManager;
import il.co.etrader.backend.bl_vos.RewardUserTierDistribution;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.util.CommonUtil;


/**
 * @author eranl
 * In this screen we will have:
 * 1. Display to writer the minimum of experience points for current tiers
 * 2. Writer can change the experience points for each tier
 * 3. Validation for new minimum of experience points
 * 4. Confirmation message and relevant data
 * 5. Update tiers in the DataBase later on we will schedule a job to update all relevant users       
 *
 */
public class RewardsChangeTiersForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(RewardsChangeTiersForm.class);
	
	private long writerId;
	private ArrayList<RewardTier> currentTiers;
	private ArrayList<RewardTier> newTiers;
	private ArrayList<RewardUserTierDistribution> currentRewardUserTierDistributionList;
	private ArrayList<RewardUserTierDistribution> newRewardUserTierDistributionList;
	private int affectedUsers;				    
    //steps 
	private int step;
    private static final int STEP_EDIT_TIER			= 1;
    private static final int STEP_CONFIRMATION_DATA	= 2;    
    private static final int STEP_DB_UPDATE			= 3;    
    //navigate between steps
    private static final int NEXT	= 1;
    private static final int BACK	= 2;
    private static final int CANCEL	= 3;
	
	public RewardsChangeTiersForm() throws SQLException, CloneNotSupportedException {
		writerId = Utils.getWriter().getWriter().getId();
		init();
	}
	
	/**
	 * Initiate screen 
	 * @throws SQLException
	 * @throws CloneNotSupportedException
	 */
	private void init()  throws SQLException, CloneNotSupportedException {
		currentTiers = RewardsManager.getRewardTiers();		
		newTiers = new ArrayList<RewardTier>(currentTiers.size());						
		Iterator<RewardTier> iterator = currentTiers.iterator();
		while(iterator.hasNext()){
			newTiers.add((RewardTier)iterator.next().clone()); 
		}
		step = STEP_EDIT_TIER;
	}
	
	/**
	 * Update tiers table, in addition activate change tier job for users
	 * @return
	 */
	public boolean updateTiers() {
		try {
			//update tiers 
			RewardsManager.updateTiers(newTiers, writerId);
			logger.info("Reward update rewards_tier table was successful");
			//activate job
			ConfigManager.updateParametersByKey(ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_NAME, ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_ACTIVE_VALUE);
			logger.info("Reward activate job was successful");
		} catch (Exception e) {
			logger.error("Error while trying to update tier", e);
			return false;
		}
		return true;
	}
	
	/**
	 * Build confirmation data 
	 * @throws SQLException
	 */
	public void buildConfirmationData() throws SQLException {
		currentRewardUserTierDistributionList = RewardsManager.getRewardsUsersDistributionByTier();
		newRewardUserTierDistributionList = RewardsManager.getEstimateRewardsUsersDistributionByTier(newTiers);
		affectedUsers =  RewardsManager.countEstimatedRewardsUsersChanged(newTiers);
		StringBuffer tiersStr = new StringBuffer();
		// tiers change 
		for (int i = 0 ; i < currentTiers.size() ; i++) {
			tiersStr.append("TierId:" + currentTiers.get(i).getId() + " current minimum experience points:" + currentTiers.get(i).getMinExperiencePoints()); 
			tiersStr.append(" TierId:" + newTiers.get(i).getId() + " new minimum experience points:" + newTiers.get(i).getMinExperiencePoints() + "\n");
		}		
		// number of users that will be affected 
		StringBuffer usersAffectedStr = new StringBuffer();  
		usersAffectedStr.append(" Users affected:" + affectedUsers + "\n");
		logger.info("Rewards confirmation message. WriterId:" + Utils.getWriter().getWriter().getId() + " is trying to change rewards tiers:\n" + tiersStr.toString() + "\n" + usersAffectedStr.toString());
	}
	
	/**
	 * Validate tiers
	 * @return
	 * @throws SQLException 
	 */
	public boolean validateTiers() throws SQLException {		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;			
		// validate experience point is not negative
		if (newTiers.get(0).getMinExperiencePoints() < 0) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("rewards.tier.err.negative", null, Utils.getWriterLocale(context)),null);
			 context.addMessage(null, fm);
			 return false;
		}
		// validate each tier is lower from the next tier
		for (int i = newTiers.size() - 1 ; i > 0; i--) {
			if (newTiers.get(i).getMinExperiencePoints() <= newTiers.get(i - 1).getMinExperiencePoints() ) { 
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("rewards.tier.err.wrong.tier", null, Utils.getWriterLocale(context)),null);
				 context.addMessage(null, fm);
				 return false;
			}
		}		
		// check if tier job didn't run 
		// The job is running in 2 process:
		// 1. Update rewards tiers table 
		// 2. Update rewards users table
		// only after those tables will be updated we can change the tiers again. 				 
		if (ConfigManager.getParameter(ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_NAME).equals(String.valueOf(ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_ACTIVE_VALUE))) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("rewards.tier.err.job", null, Utils.getWriterLocale(context)),null);
			 context.addMessage(null, fm);
			 return false;
		}			
		// validate tier changed
		boolean tierChanged = false;
		for (int i = 0 ; i < currentTiers.size() ; i++) {
			if (currentTiers.get(i).getMinExperiencePoints() != newTiers.get(i).getMinExperiencePoints()) {
				tierChanged = true;		
				break;
			}
		}		
		if (!tierChanged) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("rewards.tier.err.not.changed", null, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);		
		}
		return tierChanged;
	}
	
    /**
     * Navigate the user to the previous/ next page. In addition he can choose to cancel the process at any time. 
     * @param choise
     * @return
     * @throws SQLException
     * @throws IOException
     * @throws CloneNotSupportedException 
     */
    public String navigate(long choise) throws SQLException, IOException, CloneNotSupportedException {
    	int userChoise = Integer.valueOf(String.valueOf(choise));
    	switch (userChoise) {
			case NEXT:
				if (step == STEP_EDIT_TIER) {
					boolean res = validateTiers();
					if (res) {
						step = STEP_CONFIRMATION_DATA;
						buildConfirmationData();
					}
				} else if (step == STEP_CONFIRMATION_DATA) {
					boolean res = updateTiers();
					FacesContext context = FacesContext.getCurrentInstance();
					String msg = "rewards.tier.update";					
					if (!res) {						
						msg = "rewards.tier.err.update";													
					} else { //success
						step = STEP_DB_UPDATE; 			
						Log.debug("Reward DB stage was successfully updated");
					}
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage(msg, null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);
				}
				break;	
			case BACK:
				if (step == STEP_CONFIRMATION_DATA) {
					step = STEP_EDIT_TIER;
					init();
				}
				break;
			case CANCEL:
				init();
				break;
		default:
			break;
		}
    	return null;
    }


	/**
	 * @return the currentTiers
	 */
	public ArrayList<RewardTier> getCurrentTiers() {
		return currentTiers;
	}

	/**
	 * @param currentTiers the currentTiers to set
	 */
	public void setCurrentTiers(ArrayList<RewardTier> currentTiers) {
		this.currentTiers = currentTiers;
	}

	/**
	 * @return the newTiers
	 */
	public ArrayList<RewardTier> getNewTiers() {
		return newTiers;
	}

	/**
	 * @param newTiers the newTiers to set
	 */
	public void setNewTiers(ArrayList<RewardTier> newTiers) {
		this.newTiers = newTiers;
	}

	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}

	/**
	 * @param step the step to set
	 */
	public void setStep(int step) {
		this.step = step;
	}

	/**
	 * @return the currentRewardUserTierDistributionList
	 */
	public ArrayList<RewardUserTierDistribution> getCurrentRewardUserTierDistributionList() {
		return currentRewardUserTierDistributionList;
	}

	/**
	 * @param currentRewardUserTierDistributionList the currentRewardUserTierDistributionList to set
	 */
	public void setCurrentRewardUserTierDistributionList(
			ArrayList<RewardUserTierDistribution> currentRewardUserTierDistributionList) {
		this.currentRewardUserTierDistributionList = currentRewardUserTierDistributionList;
	}

	/**
	 * @return the newRewardUserTierDistributionList
	 */
	public ArrayList<RewardUserTierDistribution> getnewRewardUserTierDistributionList() {
		return newRewardUserTierDistributionList;
	}

	/**
	 * @param newRewardUserTierDistributionList the newRewardUserTierDistributionList to set
	 */
	public void setnewRewardUserTierDistributionList(
			ArrayList<RewardUserTierDistribution> newRewardUserTierDistributionList) {
		this.newRewardUserTierDistributionList = newRewardUserTierDistributionList;
	}

	/**
	 * @return the affectedUsers
	 */
	public int getAffectedUsers() {
		return affectedUsers;
	}

	/**
	 * @param affectedUsers the affectedUsers to set
	 */
	public void setAffectedUsers(int affectedUsers) {
		this.affectedUsers = affectedUsers;
	}
	
}