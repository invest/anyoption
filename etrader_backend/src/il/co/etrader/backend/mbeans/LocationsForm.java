package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingLocation;
import il.co.etrader.util.CommonUtil;



public class LocationsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String locationName;
	private ArrayList<MarketingLocation> list;
	private MarketingLocation location;

	public LocationsForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingLocations(locationName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert location
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		location.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateLocation(location);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_LOCATIONS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit location
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_LOCATION;
	}

	/**
	 * Navigate to insert new location
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		location = new MarketingLocation();
		location.setWriterId(AdminManager.getWriterId());
		location.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_LOCATION;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the location
	 */
	public MarketingLocation getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(MarketingLocation location) {
		this.location = location;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}






}
