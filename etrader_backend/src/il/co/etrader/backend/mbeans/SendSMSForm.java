package il.co.etrader.backend.mbeans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.GetContactsForSmsSending;
import il.co.etrader.backend.helper.GetUsersForSmsSending;
import il.co.etrader.backend.helper.SendSmsOrEmailToUsers;
import il.co.etrader.backend.helper.SendSmsToContacts;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PromotionsManager;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendConfirmEmailToWriter;

public class SendSMSForm implements Serializable {
    private static final long serialVersionUID = 3887597842124630588L;

    private static final Logger logger = Logger.getLogger(SendSMSForm.class);

    private List<String> depositors;
    private List<String> skinId;
    private List<String> campaignIds;
    //agree to send sms. 2 - all / 1 - only user that agree / 0 - user that dont agree
    private List<String> customerValue;
    private List<String> timeZone;
    private ArrayList<SelectItem> customerValueSI;
    private int hasSalesDeposit;
    private List<String> populationType;
    private String lastLogin;
    private ArrayList<SelectItem> lastLoginSI;
    private String message;
    private String excludeCountries;
    private String includeCountries;
    private ArrayList<SelectItem> countriesList = new ArrayList<SelectItem>();
    private String[] selectedCountriesList;
    private long userClassId;
    //private ArrayList<User> usersList;
    private long mailBoxTemplateId;
    private File file;
    private int activeUsersFilter;
    private Integer lastloginStart;
    private Integer lastloginEnd;
    //private GetUsersForSmsSending smsUsers;
    private String promotionName;
    private int sendType;

	private HttpSession session;
	private int issueSubjectId;
	private boolean sendToContacts;
	
    public SendSMSForm() throws SQLException{

        customerValueSI = new ArrayList<SelectItem>();
        customerValueSI.add(new SelectItem(new Integer(0), CommonUtil.getMessage("general.all", null)));
        customerValueSI.add(new SelectItem(new Integer(1), "Negative"));
        customerValueSI.add(new SelectItem(new Integer(2), "+0-500"));
        customerValueSI.add(new SelectItem(new Integer(3), "501-1000"));
        customerValueSI.add(new SelectItem(new Integer(4), ">1000"));

        excludeCountries = "0";
        includeCountries = "0";

        populationType = new ArrayList<String>();
        populationType.add("0");
        customerValue = new ArrayList<String>();
        customerValue.add(String.valueOf(ConstantsBase.ALL_TIME_ZONE));
        timeZone = new ArrayList<String>();
        timeZone.add(String.valueOf(ConstantsBase.ALL_TIME_ZONE));
        depositors = new ArrayList<String>();
        depositors.add(String.valueOf(ConstantsBase.ALL_DEPOSIT));
        skinId = new ArrayList<String>();
        skinId.add(String.valueOf(Skin.SKINS_ALL));
        campaignIds = new ArrayList<String>();
        campaignIds.add("0");
        userClassId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;
        file = new File();
        updateCountries(timeZone);
        activeUsersFilter = Constants.USER_ACTIVE_INT;
        lastLogin = ConstantsBase.ALL_LAST_LOGIN_CHOISE;
        lastloginStart = null;
        lastloginEnd = null;
    }

    public String sendSMS() throws Exception {
    	if (CommonUtil.isParameterEmptyOrNull(message) || CommonUtil.isParameterEmptyOrNull(promotionName)) {
    		FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null);
            if(CommonUtil.isParameterEmptyOrNull(message)){
            	context.addMessage("sendSMSForm:msg", fm);
            }
            if(CommonUtil.isParameterEmptyOrNull(promotionName)){
            	context.addMessage("sendSMSForm:prmName", fm);
            }
            return null;
    	}
//    	if(issueSubjectId == -1 || issueSubjectId == 33){
//    		FacesContext context = FacesContext.getCurrentInstance();
//            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null);
//           	context.addMessage("sendSMSForm:issueSubjectId", fm);
//            return null;
//    	}
    	this.sendType = Constants.SEND_SMS;
    	return sendLogic(Constants.SEND_SMS);
    }
    
    public String sendSmsToContacts() throws Exception {
    	sendToContacts = true;
    	
    	if (CommonUtil.isParameterEmptyOrNull(message) || CommonUtil.isParameterEmptyOrNull(promotionName)) {
    		FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null);
            if(CommonUtil.isParameterEmptyOrNull(message)){
            	context.addMessage("sendSMSForm:msg", fm);
            }
            if(CommonUtil.isParameterEmptyOrNull(promotionName)){
            	context.addMessage("sendSMSForm:prmName", fm);
            }
            return null;
    	}
//    	if(issueSubjectId == -1 || issueSubjectId == 33){
//    		FacesContext context = FacesContext.getCurrentInstance();
//            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null);
//           	context.addMessage("sendSMSForm:issueSubjectId", fm);
//            return null;
//    	}
    	this.sendType = Constants.SEND_SMS;
    	
    	Writer w = Utils.getWriter().getWriter();
    	FacesContext context = FacesContext.getCurrentInstance();
    	session = (HttpSession) context.getExternalContext().getSession(true);
        String contactsFromFile = "";

        if (null != file.getFile()) {
			BufferedReader fr = null;
			try {
				fr = new BufferedReader(new InputStreamReader(file.getFile().getInputStream()));
	
				String s = new String();
				while ((s = fr.readLine()) != null)	{
					if (!s.equals(ConstantsBase.EMPTY_STRING )&& s.matches("\\d+")){
						contactsFromFile += s + ", ";
					}
				}
			} catch (IOException e) {
				logger.debug("Exception while geting users from file:"+e.getMessage());
			}
 		    contactsFromFile = contactsFromFile.substring(0, contactsFromFile.length() - 2);

        } else {

        	if (CommonUtil.isParameterEmptyOrNull(includeCountries)) {
                includeCountries = "0";
            }

            if (skinId.isEmpty() && includeCountries.equals("0") && null == file.getFile()) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose skin or country!", null);
                context.addMessage(null, fm);

                logger.debug("no skin or country was choosen");
                return null;
            }

            if (!populationType.contains("0")) {
            	depositors.add(String.valueOf(ConstantsBase.ALL_DEPOSIT));
            }
        }
        session.setAttribute(Constants.SELECTED_ISSUE_SUBJECT_ID, issueSubjectId);
        new GetContactsForSmsSending(session, contactsFromFile, w.getId(), getArrayToStringSkins(skinId), message, promotionName, includeCountries, getArrayToString(campaignIds), getArrayToString(populationType)).start();

        String email = w.getEmail();
        String subject = "Send SMS before confirmation.";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(SendConfirmEmailToWriter.PARAM_EMAIL, email);
        params.put(SendConfirmEmailToWriter.MAIL_FROM, email);
        params.put(SendConfirmEmailToWriter.MAIL_TO ,email );
        params.put(SendConfirmEmailToWriter.MAIL_SUBJECT, subject);
        setPageFiltersTohash(params);
        new SendConfirmEmailToWriter(params, session).start();        

        return Constants.NAV_SEND_CONFIRM;
    }

    public String sendEmail() throws Exception {
    	if (mailBoxTemplateId == 0 || CommonUtil.isParameterEmptyOrNull(promotionName)) {
    		FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null);
    		if(mailBoxTemplateId == 0){
    			context.addMessage("sendSMSForm:templateId", fm);
    		}
    		if(CommonUtil.isParameterEmptyOrNull(promotionName)){
    			context.addMessage("sendSMSForm:prmName", fm);
    		}
            return null;
    	}
    	issueSubjectId = 33;
    	this.sendType = Constants.SEND_EMAIL;
    	return sendLogic(Constants.SEND_EMAIL);
    }

    public String sendLogic(int sendType) throws Exception{
    	sendToContacts = false;
    	Writer w = Utils.getWriter().getWriter();
    	FacesContext context = FacesContext.getCurrentInstance();
    	session = (HttpSession) context.getExternalContext().getSession(true);
        String usersFromFile = "";

        if (null != file.getFile()) {

			BufferedReader fr = null;
			try {
				fr = new BufferedReader(new InputStreamReader(file.getFile().getInputStream()));
	
				String users = new String();
				while ((users = fr.readLine()) != null)	{
					if (!users.equals(ConstantsBase.EMPTY_STRING )&& users.matches("\\d+")){
						usersFromFile += users + ", ";
					}
				}
			} catch (IOException e) {
				logger.debug("Exception while geting users from file:"+e.getMessage());
			}
 		    usersFromFile = usersFromFile.substring(0, usersFromFile.length() - 2);

        } else {

        	if (CommonUtil.isParameterEmptyOrNull(includeCountries)) {
                includeCountries = "0";
            }

            if (skinId.isEmpty() && includeCountries.equals("0") && null == file.getFile()) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose skin or country!", null);
                context.addMessage(null, fm);

                logger.debug("no skin or country was choosen");
                return null;
            }

            if (!populationType.contains("0")) {
            	depositors.add(String.valueOf(ConstantsBase.ALL_DEPOSIT));
            }
        }
        session.setAttribute(Constants.SELECTED_ISSUE_SUBJECT_ID, issueSubjectId);
        new GetUsersForSmsSending(lastloginStart, lastloginEnd, includeCountries, getArrayToStringSkins(skinId),
			       	 getArrayToString(campaignIds), customerValue, getArrayToString(populationType), hasSalesDeposit,
			       	 depositors, userClassId, sendType, activeUsersFilter, mailBoxTemplateId, message, w,
			       	 promotionName, session, usersFromFile).start();

//        smsUsers.start();

        String email = w.getEmail();
        String subject = "Send SMS/EMAIL before confirmation.";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(SendConfirmEmailToWriter.PARAM_EMAIL, email);
        params.put(SendConfirmEmailToWriter.MAIL_FROM, email);
        params.put(SendConfirmEmailToWriter.MAIL_TO ,email );
        params.put(SendConfirmEmailToWriter.MAIL_SUBJECT, subject);
        setPageFiltersTohash(params);
        new SendConfirmEmailToWriter(params, session).start();        
    //    session.removeAttribute(Constants.PROMOTION_TOTAL_INSERTED_TO_TABLE);

        return Constants.NAV_SEND_CONFIRM;
    }

    public void setPageFiltersTohash(HashMap<String, String> params) throws SQLException{
    	String timeZones = "\r\n";
    	String Countries = "\r\n";
    	String campaignNames = "\r\n";
    	String winLose = "\r\n";
    	String populationsType = "\r\n";
    	Integer index = null;
    	long longIndex = -1;
    	boolean foundAllPop = false;
    	String depositStatus = "\r\n";

    	if(depositors.contains(0)){
    		depositStatus += "All" + "\r\n";
    	}
    	if(depositors.contains(2)){
    		depositStatus += Constants.FAILED_DEPOSIT_TXT + "\r\n";
    	}
		if(depositors.contains(1)){
			depositStatus += Constants.NO_DEPOSIT_TXT + "\r\n";
		}
		if(depositors.contains(3)){
			depositStatus += Constants.SUCCESS_DEPOSIT_TXT +"\r\n";
		}
    	params.put(SendConfirmEmailToWriter.PARAM_DEPOSITORS,depositStatus);

    	params.put(SendConfirmEmailToWriter.PARAM_LAST_LOGIN, lastLogin);

    	if(lastLogin.contains(ConstantsBase.ALL_LAST_LOGIN_CHOISE)){
    		params.put(SendConfirmEmailToWriter.PARAM_LAST_LOGIN, ConstantsBase.ALL_LAST_LOGIN);
    	}
    	else if(lastLogin.contains(ConstantsBase.LAST_LOGIN_BETWEEN)){
    		params.put(SendConfirmEmailToWriter.PARAM_LAST_LOGIN, lastloginStart + "-" + lastloginEnd);
    	}
    	else{
    		params.put(SendConfirmEmailToWriter.PARAM_LAST_LOGIN, ">" + lastloginEnd);
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_SKINS, getSkinsToStringNames() );

    	for(String str : timeZone){
    		if(str.equalsIgnoreCase("0")){
    			timeZones += "ALL" + "\r\n";
    		} else {
    			timeZones += str + "\r\n";
			}

    	}
    	params.put(SendConfirmEmailToWriter.PARAM_TIME_ZONE	,timeZones);

    	if (null != file.getFile()) {
    		params.put(SendConfirmEmailToWriter.PARAM_FILE_NAME, file.getFile().getName());
    	}else{
	    	String[] strArr = includeCountries.split(",");
	    	if(strArr[0].equals("0")){
	    		Countries = "All" + "\r\n";
	    	}
	    	for(String str : strArr){
	    		index = Integer.parseInt(str);
	    		if(index > 0){
	    			Countries += ApplicationData.getCountry(index).getName() + "\r\n";
	    		}
	    	}
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_COUNTRY, Countries);

    	ArrayList<SelectItem> arr = getAllMarketingCampaignsLongSI();
    	for(String obj : campaignIds){
    		index = Integer.parseInt(obj);
    		for (SelectItem item : arr){
    			if(Integer.parseInt(item.getValue().toString()) == index){
    				campaignNames += item.getLabel() + ",\r\n";
    			}
    		}
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_CAMPAIGNS, campaignNames);
    	for(int i=0 ; customerValue.size() > i; i++){
    		Object obj = customerValue.get(i);
    		index = Integer.parseInt(obj.toString());
    		winLose += customerValueSI.get(index).getLabel() + "\r\n";
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_WIN_LOSE, winLose);

    	if(hasSalesDeposit == ConstantsBase.ALL_DEPOSIT){
    		params.put(SendConfirmEmailToWriter.PARAM_SALES_DEPOSIT, "All");
    	}else if(hasSalesDeposit == ConstantsBase.NO_DEPOSIT){
    		params.put(SendConfirmEmailToWriter.PARAM_SALES_DEPOSIT, "No");
    	}else{
    		params.put(SendConfirmEmailToWriter.PARAM_SALES_DEPOSIT, "Yes");
    	}

    	if(activeUsersFilter == Constants.ALL_FILTER_ID_INT){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_STATUS,"All" );
    	}else if(activeUsersFilter == Constants.USER_ACTIVE_INT){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_STATUS,"Active Users" );
    	}else{
    		params.put(SendConfirmEmailToWriter.PARAM_USER_STATUS,"Inactive Users" );
    	}

    	if(userClassId == Constants.USER_CLASS_ALL){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_CLASS, "All");
    	}else if(userClassId == Constants.USER_CLASS_COMPANY){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_CLASS, "Company");
    	}else if(userClassId == Constants.USER_CLASS_COMPANY_OR_PRIVATE){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_CLASS, "Private/Company");
    	}else if(userClassId == Constants.USER_CLASS_PRIVATE){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_CLASS, "Private");
    	}else if(userClassId == Constants.USER_CLASS_TEST){
    		params.put(SendConfirmEmailToWriter.PARAM_USER_CLASS, "Tester");
    	}

    	HashMap<Long, PopulationType> hm = ApplicationData.getPopulationsTypes();
    	for(String str: populationType){
    		longIndex = Long.parseLong(str);
    		if(longIndex == Constants.POPULATION_TYPE_ALL){
    			populationsType = "All \r\n";
    			foundAllPop = true;
    		}
    		if(!foundAllPop){
    			populationsType += CommonUtil.getMessage(hm.get(longIndex).getName(), null) + "\r\n";
    		}
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_POPULATION_NAME,populationsType );

    	if(sendType == Constants.SEND_SMS){
    		params.put(SendConfirmEmailToWriter.PARAM_SEND_TYPE, "SMS");
    		params.put(SendConfirmEmailToWriter.PARAM_MESSAGE, message);
    	}
    	else{
    		params.put(SendConfirmEmailToWriter.PARAM_SEND_TYPE, "EMAIL");
    		params.put(SendConfirmEmailToWriter.PARAM_TEMPLATE_NAME, getTemplateName());
    	}
    	params.put(SendConfirmEmailToWriter.PARAM_PROMOTION_NAME, promotionName);
    }

	/**
     * insert issue to each user we sent sms
     * @param comments sms text
     * @param user the user we sent the sms
     * @throws SQLException
     */
    public String continueToSendSmsConfirm() throws SQLException{
    	FacesContext context = FacesContext.getCurrentInstance();
    	session = (HttpSession) context.getExternalContext().getSession(true);
    	session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, "0_0");
    	Writer w = Utils.getWriter().getWriter();
    	SendSmsOrEmailToUsers sendMessage = new SendSmsOrEmailToUsers(session, w);
		sendMessage.start();
		return Constants.NAV_SEND_SUCCESS;
	}
    

    public String returnToSendScreen() throws SQLException{
    	long promotionId = PromotionsManager.getLastPromotion(Utils.getWriter().getWriter()).getId();
    	PromotionsManager.updateStatus(Constants.PROMOTION_STATUS_ID_CANCELED, promotionId);
    	PromotionsManager.updatePromotionEntriesStatus(promotionId, Constants.PROMOTION_STATUS_ID_CANCELED);
    	return "sendSMS";
    }
    
    public String returnToSendToContactScreen() throws SQLException{
    	long promotionId = PromotionsManager.getLastPromotion(Utils.getWriter().getWriter()).getId();
    	PromotionsManager.updateStatus(Constants.PROMOTION_STATUS_ID_CANCELED, promotionId);
    	PromotionsManager.updatePromotionEntriesStatus(promotionId, Constants.PROMOTION_STATUS_ID_CANCELED);
    	return "sendSMScontacts";
    }

    public String continueToSendSmsToContactConfirm() throws SQLException{
    	FacesContext context = FacesContext.getCurrentInstance();
    	session = (HttpSession) context.getExternalContext().getSession(true);
    	session.setAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS, "0_0");
    	Writer w = Utils.getWriter().getWriter();
    	SendSmsToContacts sendMessage = new SendSmsToContacts(session, w);
		sendMessage.start();
		return Constants.NAV_SEND_SUCCESS;
	}
    
    public boolean isPopulationTypeZero() {
    	if(populationType.contains("0")) {
    		return true;
    	}
    	return false;
    }

    public String getTemplateName(){
    	String templateName= null;
    	try {
			templateName = MailBoxTemplatesManager.getTemplateById(mailBoxTemplateId).getName();
		} catch (SQLException e) {
			logger.debug("Can't get template name"+e.getMessage());
		}
		return templateName;
    }

    public String getSkinsToStringNames(){
    	String skinStr = "";
    	for (String string : skinId){
    		if(string.equals(Skin.SKINS_ALL)){
    			return "All";
    		}
    		skinStr += ApplicationData.getSkinById(Integer.parseInt(string)).getName() + ", ";
    	}
    	return skinStr;
    }
    /**
     * @return the campaignIds
     */
    public List<String> getCampaignIds() {
        return campaignIds;
    }

    /**
     * @param campaignIds the campaignIds to set
     */
    public void setCampaignIds(List<String> campaignIds) {
        this.campaignIds = campaignIds;
    }

    /**
     * @return the countriesList
     */
    public ArrayList<SelectItem> getCountriesList() {
        return countriesList;
    }

    /**
     * @param countriesList the countriesList to set
     */
    public void setCountriesList(ArrayList<SelectItem> countriesList) {
        this.countriesList = countriesList;
    }

    /**
     * @return the customerValueSI
     */
    public ArrayList<SelectItem> getCustomerValueSI() {
        return customerValueSI;
    }

    /**
     * @param customerValueSI the customerValueSI to set
     */
    public void setCustomerValueSI(ArrayList<SelectItem> customerValueSI) {
        this.customerValueSI = customerValueSI;
    }

    /**
     * @return the depositors
     */
    public List<String> getDepositors() {
        return depositors;
    }

    /**
     * @param depositors the depositors to set
     */
    public void setDepositors(List<String> depositors) {
        this.depositors = depositors;
    }

    /**
     * @return the selectedCountriesList
     */
    public String[] getSelectedCountriesList() {
        return selectedCountriesList;
    }

    /**
     * @param selectedCountriesList the selectedCountriesList to set
     */
    public void setSelectedCountriesList(String[] selectedCountriesList) {
        this.selectedCountriesList = selectedCountriesList;
    }

    /**
     * @return the customerValue
     */
    public List<String> getCustomerValue() {
        return customerValue;
    }
    /**
     * @param customerValue the customerValue to set
     */
    public void setCustomerValue(List<String> customerValue) {
        this.customerValue = customerValue;
    }
    /**
     * @return the hasSalesDeposit
     */
    public int getHasSalesDeposit() {
        return hasSalesDeposit;
    }
    /**
     * @param hasSalesDeposit the hasSalesDeposit to set
     */
    public void setHasSalesDeposit(int hasSalesDeposit) {
        this.hasSalesDeposit = hasSalesDeposit;
    }

    /**
     * @return the lastLoginSI
     */
    public ArrayList<SelectItem> getLastLoginSI() {
        return lastLoginSI;
    }
    /**
     * @param lastLoginSI the lastLoginSI to set
     */
    public void setLastLoginSI(ArrayList<SelectItem> lastLoginSI) {
        this.lastLoginSI = lastLoginSI;
    }
    /**
     * @return the skinId
     */
    public List<String> getSkinId() {
        return skinId;
    }
    /**
     * @param skinId the skinId to set
     */
    public void setSkinId(List<String> skinId) {
        this.skinId = skinId;
    }

    public ArrayList<SelectItem> getAllMarketingCampaignsLongSI() throws SQLException {
        WriterWrapper ww = Utils.getWriter();
        ArrayList<SelectItem> campaignNamesSI = new ArrayList<SelectItem>();
        for ( SelectItem m : ww.getAllMarketingCampaignsLongSI() ) {
            campaignNamesSI.add(new SelectItem(m.getValue().toString(), m.getLabel()));
        }
        return campaignNamesSI;
    }

    /**
     * @return the populationType
     */
    public List<String> getPopulationType() {
        return populationType;
    }

    /**
     * @param populationType the populationType to set
     */
    public void setPopulationType(List<String> populationType) {
        this.populationType = populationType;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public void updateCountries(List<String> GMT){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		countriesList.clear();
    	WriterWrapper w = Utils.getWriter();
		if (!GMT.contains("0")){
			for (Country c : ApplicationData.getCountries().values())  {
				if (GMT.contains(c.getGmtOffset())){
					list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
							CommonUtil.getMessage(c.getDisplayName(), null) + ", " +
							Utils.getUtcDifference(w.getUtcOffset(), c.getGmtOffset())));
				}
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		}
		else {
		  	for (Country c : ApplicationData.getCountries().values())  {
				list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
						CommonUtil.getMessage(c.getDisplayName(), null) + ", " +
						Utils.getUtcDifference(w.getUtcOffset(), c.getGmtOffset())));
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		}
		countriesList.addAll(list);
	}

    public void updateCountries(ValueChangeEvent event) {
		updateCountries((List<String>)event.getNewValue());
	}

    public void updatePopulationType(ValueChangeEvent event) {
    	Long eventLong = (Long)event.getNewValue();
    	this.populationType.add(String.valueOf(eventLong));
//        this.populationType = (Long)event.getNewValue();
    }
    /**
     * @return the excludeCountries
     */
    public String getExcludeCountries() {
        return excludeCountries;
    }
    /**
     * @param excludeCountries the excludeCountries to set
     */
    public void setExcludeCountries(String excludeCountries) {
        this.excludeCountries = excludeCountries;
    }
    /**
     * @return the includeCountries
     */
    public String getIncludeCountries() {
        return includeCountries;
    }
    /**
     * @param includeCountries the includeCountries to set
     */
    public void setIncludeCountries(String includeCountries) {
        this.includeCountries = includeCountries;
    }

    public String getArrayToString(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
            list += temp.get(i) + ",";
        }
        return list.substring(0,list.length()-1);
    }
    /**
     * return 0 if all selected else return string of skins
     * @param temp
     * @return
     */
    public String getArrayToStringSkins(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
        	if(temp.get(i).equals(Skin.SKINS_ALL)){
        		return Skin.SKINS_ALL;
        	}
            list += temp.get(i) + ",";
        }
        return list.substring(0,list.length()-1);
    }
    /**
     * The function return the number that appear after the character '>'
     *
     * @param lastLogin
     * @return minLastLogin as Integer
     */
    public int getMinLastLogin(String lastLogin) {
    	int index = 0;
    	for (int i = 0; i < lastLogin.length(); i++) {
    		index = lastLogin.indexOf(">");
    	}
    	String minLastLogin = lastLogin.substring(index + 1, lastLogin.length());
    	return (Integer.valueOf(minLastLogin));
    }

    /**
     * The function return the number that appear before the character '-'
     *
     * @param lastLogin
     * @return lowLastLogin as Integer
     */
    public int getLowLastLogin(String lastLogin) {
    	int index = 0;
    	for (int i = 0; i < lastLogin.length(); i++) {
    		index = lastLogin.indexOf("-");
    	}
    	String lowLastLogin = lastLogin.substring(0, index);
    	return (Integer.valueOf(lowLastLogin));
    }

    /**
     * The function return the number that appear after the character '-'
     *
     * @param lastLogin
     * @return highLastLogin as Integer
     */
    public int getHighLastLogin(String lastLogin) {
    	int index = 0;
    	for (int i = 0; i < lastLogin.length(); i++) {
    		index = lastLogin.indexOf("-");
    	}
    	String highLastLogin = lastLogin.substring(index + 1, lastLogin.length());
    	return (Integer.valueOf(highLastLogin));
    }

    public void updateChange(ValueChangeEvent event) throws SQLException{
    	lastLogin=(String)event.getNewValue();
    }

	public long getUserClassId() {
		return userClassId;
	}
	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}

	public long getMailBoxTemplateId() {
		return mailBoxTemplateId;
	}

	public void setMailBoxTemplateId(long mailBoxTemplateId) {
		this.mailBoxTemplateId = mailBoxTemplateId;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the activeUsersFilter
	 */
	public int getActiveUsersFilter() {
		return activeUsersFilter;
	}
	/**
	 * @param activeUsersFilter the activeUsersFilter to set
	 */
	public void setActiveUsersFilter(int activeUsersFilter) {
		this.activeUsersFilter = activeUsersFilter;
	}
	/**
	 * @return the lastLogin
	 */
	public String getLastLogin() {
		return lastLogin;
	}
	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	/**
	 * @return the timeZone
	 */
	public List<String> getTimeZone() {
		return timeZone;
	}
	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(List<String> timeZone) {
		this.timeZone = timeZone;
	}
	/**
	 * @return the lastloginEnd
	 */
	public Integer getLastloginEnd() {
		return lastloginEnd;
	}
	/**
	 * @param lastloginEnd the lastloginEnd to set
	 */
	public void setLastloginEnd(Integer lastloginEnd) {
		this.lastloginEnd = lastloginEnd;
	}
	/**
	 * @return the lastloginStart
	 */
	public Integer getLastloginStart() {
		return lastloginStart;
	}
	/**
	 * @param lastloginStart the lastloginStart to set
	 */
	public void setLastloginStart(Integer lastloginStart) {
		this.lastloginStart = lastloginStart;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public int getSendType() {
		return sendType;
	}
	public void setSendType(int sendType) {
		this.sendType = sendType;
	}
//	public ArrayList<User> getUsersList() {
//		return usersList;
//	}
//	public void setUsersList(ArrayList<User> usersList) {
//		this.usersList = usersList;
//	}
	public int getIssueSubjectId() {
		return issueSubjectId;
	}
	public void setIssueSubjectId(int issueSubjectId) {
		this.issueSubjectId = issueSubjectId;
	}

	public boolean isSendToContacts() {
		return sendToContacts;
	}

	public void setSendToContacts(boolean sendToContacts) {
		this.sendToContacts = sendToContacts;
	}


	
}
