package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.managers.RewardUsersManager;
import com.anyoption.common.rewards.RewardUtil;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;

/**
 * @author liors
 *
 */
public class EditRewardUserForm implements Serializable {	
	private static final long serialVersionUID = -7069357167442507898L;
	private static final Logger logger = Logger.getLogger(EditRewardUserForm.class);
	private User user;
	private String userName;
	private String password;
	private double experiencePoints; 
	private RewardUser rewardUser;
	
	public EditRewardUserForm() {
		getInit();
	}
	
	public String submit() {
		try {
			//same exp pnts
			if (experiencePoints == rewardUser.getExperiencePoints()) {
				returnMsg("rewards.exp.pnts.equal", null);
				return null;
			}
			
			//Password / username not valid
			if (!checkPassword(Utils.getWriter().getWriter())) {
				returnMsg("wrong.pass.user", null);
				return null;
			}
			
			//experience points not in the valid range.
			int firstTierMinExperiencePoints = RewardManager.getTiers().get(RewardTier.TIER_TRADER_1).getMinExperiencePoints();
			if (experiencePoints < firstTierMinExperiencePoints || experiencePoints > ConstantsBase.REWARD_MAXIMUM_VALID_EXP_PNTS) {
				String[] parmas = new String[2];
				parmas[0] = String.valueOf(firstTierMinExperiencePoints);
				parmas[1] = String.valueOf(ConstantsBase.REWARD_MAXIMUM_VALID_EXP_PNTS);
				returnMsg("rewards.exp.pnts.valid.range", parmas);
				return null;
			}
			
			RewardUsersManager.updateUserExperiencePoints(rewardUser, experiencePoints, 
					RewardUtil.safeLongToInt(Utils.getWriter().getWriter().getId()), BonusManagerBase.class, CommonUtil.getMessage("rewards.exp.pnts.history.update", null), RewardUsersHistoryManager.FIX_EXPERIENCE_POINTS);			
		} catch (Exception e) {
			logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "Problem to update user experience points!");
			returnMsg("technical.problem", null);
			return null;
		}		
		return Constants.NAV_REWARD_USER_UPDATE_SUCCESS;
	}
	
	public String getInit() {
		try {
			user = Utils.getUser();
			rewardUser = RewardUsersManager.getRewardUser(user.getId());
			experiencePoints = rewardUser.getExperiencePoints();
		} catch (SQLException e) {
			logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "Problem in get user Experience Points ", e);
		}
		return Constants.EMPTY_STRING;
	}
	
	private boolean checkPassword(Writer writer) {
		boolean res = false;
		if (this.userName.equalsIgnoreCase(writer.getUserName()) && this.password.equalsIgnoreCase(writer.getPassword())) {
			res = true;
		}	
		return res;
	}
	
	public void returnMsg(String msg, String[] params) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, Utils.getMessage(msg, params),null);
		context.addMessage(null, fm);
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the experiencePoints
	 */
	public double getExperiencePoints() {
		return experiencePoints;
	}

	/**
	 * @param experiencePoints the experiencePoints to set
	 */
	public void setExperiencePoints(double experiencePoints) {
		this.experiencePoints = experiencePoints;
	}
}