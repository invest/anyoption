package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ApiExternalUsersManager;
import il.co.etrader.backend.bl_vos.ApiExternalUser;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;

public class ApiExternalUserStripForm implements Serializable {

	private static final Logger log = Logger.getLogger(ApiExternalUserStripForm.class);

	private static final long serialVersionUID = -8281038051490295232L;
	private ApiExternalUser apiExternalUser;
	private long id;
	private String reference;
	private long apiUserId;

	public ApiExternalUserStripForm() throws SQLException{

	}

	public String search() {
		if (id == 0 && (Utils.isParameterEmptyOrNull(reference) || apiUserId == Constants.NONE_FILTER_ID)) {
			apiExternalUser = null;
			return Constants.NAV_EXTERNAL_SUPPORT;
		}

		//look for the user in the DB
		try {
			apiExternalUser = ApiExternalUsersManager.getApiExternalUser(id, reference, apiUserId);
		} catch (SQLException e) {
			log.error("cant load external user from db id = " + id + " reference = " + reference + " apiUserId = " + apiUserId, e);
		}

		if (null == apiExternalUser) {
//			TOOD: error msg use not found
			return Constants.NAV_EXTERNAL_SUPPORT;
		}
		this.id = apiExternalUser.getId();
		this.reference = apiExternalUser.getReference();
		this.apiUserId = apiExternalUser.getApiUserId();
		//load all the extra fildes
		apiExternalUser.loadStrip();
		return Constants.NAV_EXTERNAL_SUPPORT;
	}

	public String loadNewUserFromStrip() {
		return search();
		//return Constants.NAV_EXTERNAL_SUPPORT;
	}

	public String refreshStrip() throws SQLException{

		apiExternalUser.loadStrip();
		this.id = apiExternalUser.getId();
		this.reference = apiExternalUser.getReference();
		this.apiUserId = apiExternalUser.getApiUserId();
    	return Constants.NAV_EXTERNAL_SUPPORT;
	}

	public String loadNewUserFromInv() throws SQLException{
		search();
    	return Constants.NAV_EXTERNAL_SUPPORT;
	}



	/**
	 * @return the apiExternalUser
	 */
	public ApiExternalUser getApiExternalUser() {
		return apiExternalUser;
	}

	/**
	 * @param apiExternalUser the apiExternalUser to set
	 */
	public void setApiExternalUser(ApiExternalUser apiExternalUser) {
		this.apiExternalUser = apiExternalUser;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the apiUserId
	 */
	public long getApiUserId() {
		return apiUserId;
	}

	/**
	 * @param apiUserId the apiUserId to set
	 */
	public void setApiUserId(long apiUserId) {
		this.apiUserId = apiUserId;
	}

}
