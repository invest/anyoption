package il.co.etrader.backend.mbeans;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.IssueActionTaskBase;
import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.Task.Status;
import com.anyoption.common.beans.TaskGroup;
import com.anyoption.common.managers.TaskManager;
import com.google.gson.Gson;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class AutoIssuesForm implements Serializable {

	private static final long serialVersionUID = -474689997688354959L;

	private static final Logger logger = Logger.getLogger(AutoIssuesForm.class);

	private Issue issue;
	private IssueAction issueAction;
	private File file;
	private String actionTimeHour;
	private String actionTimeMin;
	private Date actionTime;

	/**
	 * @return the actionTime
	 */
	public Date getActionTime() {
		return actionTime;
	}

	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	/**
	 * @return the issue
	 */
	public Issue getIssue() {
		return issue;
	}

	/**
	 * @param issue the issue to set
	 */
	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	/**
	 * @return the issueAction
	 */
	public IssueAction getIssueAction() {
		return issueAction;
	}

	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueAction(IssueAction issueAction) {
		this.issueAction = issueAction;
	}


	public AutoIssuesForm() throws SQLException{

		issue = new Issue();
	   	issueAction = new IssueAction();

	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		// Filling action fields
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);

		issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);


		TimeZone writerTm = TimeZone.getTimeZone(Utils.getWriter().getUtcOffset());
		GregorianCalendar gc = new GregorianCalendar(writerTm);
		actionTime = gc.getTime();
		actionTimeHour = String.valueOf(gc.get(GregorianCalendar.HOUR_OF_DAY));
		actionTimeMin = String.valueOf(gc.get(GregorianCalendar.MINUTE));

		file = new File();
	}

	public String insertAutoIssues() {
		final ArrayList<String> usersList = new ArrayList<String>();
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;	
		Locale l = new Locale(ConstantsBase.LOCALE_DEFAULT);
		String message = "";
		String msgLocation = null;
		Severity severity = FacesMessage.SEVERITY_INFO;
			
		if (valitadeForm()){
			// Set the date including time (hours + minutes)
			Calendar cal = Calendar.getInstance();

			cal.setTime(actionTime);
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(actionTimeHour));
			cal.set(Calendar.MINUTE, Integer.parseInt(actionTimeMin));
			actionTime = CommonUtil.getDateTimeFormat(cal.getTime(),Utils.getWriter().getUtcOffset());
			issueAction.setActionTime(actionTime);
			issueAction.setWriterId(Utils.getWriter().getWriter().getId());
			
			boolean error = false;
			try {

				BufferedReader fr = new BufferedReader(new InputStreamReader(file.getFile().getInputStream()));
				String users = new String();
				String[] params = new String[1];

	 		    // repeat until all lines is read
	 		    while ((users = fr.readLine()) != null) {
	 		    	if (!users.equals(ConstantsBase.EMPTY_STRING)){
	 		    		usersList.add(users);
	 		    	}
	 		    }
	 		    
				if (usersList.size() > ConstantsBase.ISSUE_USERS_FILE_LIMIT) {
					params[0] = String.valueOf(ConstantsBase.ISSUE_USERS_FILE_LIMIT);
					message = CommonUtil.getMessage("issues.file.error.size", params, l);
					error = true;
					msgLocation = "autoIssuesForm:file";
					severity = FacesMessage.SEVERITY_ERROR;
				}
				
				if (!error) {
					issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
					
					IssueActionTaskBase issueActionTaskBase = new IssueActionTaskBase();
					issueActionTaskBase.setIssue(issue);
					issueActionTaskBase.setIssueAction(issueAction);
					
					Gson gson = new Gson();
					String parameters = gson.toJson(issueActionTaskBase);

					TaskGroup tg = new TaskGroup();
					tg.setTaskSubjectsTypesId(2);
					tg.setParameters(parameters);
					TaskManager.insertTaskGroup(tg);
					
					for (String user : usersList) {						
						Task task = new Task(Long.valueOf(user), Status.PENDING, (int)Utils.getWriter().getWriter().getId(), tg.getId());						
						TaskManager.insertTask(task);
					}
					
					params[0] = String.valueOf(usersList.size());
					message = CommonUtil.getMessage("issues.file.message.success.update", params, l);
				}
			} catch (Exception e) {
				message = CommonUtil.getMessage("issues.file.error.problem", null, l);
				error = true;
				severity = FacesMessage.SEVERITY_ERROR;
			}	
		}

		fm = new FacesMessage(severity , message, null);
		context.addMessage(msgLocation, fm);
		
		return null;	
	}
	
	private void sendEmail(boolean error, String errorMessage, String emailTo, String comment, String fileName, int totalUsers, ArrayList<Integer> resultCodes, long timeSeconds) {
	       // send email
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "false");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

	     Hashtable<String, String> email = new Hashtable<String, String>();
	     email.put("subject", "Auto issue completed work");

	     email.put("to", emailTo);
	
	     String htmlBuffer = 
	     		"<html>" +
	     					"<ul>" ;
	     						if(error) {
	     							htmlBuffer+="<li style=\"background-color:red;\"> ERROR : </li>";
		     						htmlBuffer+="<li>"+errorMessage+"</li>" ;
	     						} else {
	     							htmlBuffer+= "<div> Summary : </div>" ;
	     						};

	     							htmlBuffer+=	"<li> File name : "+fileName+"</li>" ;
	     							htmlBuffer+=	"<li> Issue comment : "+comment+"</li>" ;
	     							htmlBuffer+=	"<li> Total users : "+totalUsers+"</li>" ;
	     							if(resultCodes != null) {
		     							htmlBuffer+=	"<li> Users inserted : "+resultCodes.get(0)+"</li>" ;
		     							htmlBuffer+=	"<li> Users inserted in marketing : "+resultCodes.get(1)+"</li>" ;
		     							htmlBuffer+=	"<li> Users in marketing or higher population : "+resultCodes.get(2)+"</li>" ;
		     							htmlBuffer+=	"<li> Errors : "+resultCodes.get(3)+"</li>" ;
	     							}
	     							htmlBuffer+=	"<li> Time for processing : "+timeSeconds+" seconds </li>" +
							"</ul>"+				    			
	     		"</html>";
	
	     String senderEmailAddress = "asaf@etrader.co.il";
	     email.put("from", senderEmailAddress);
	     email.put("body", htmlBuffer);

		CommonUtil.sendEmail(serverProperties, email, null);
		
	}

	public void updateChannelId(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			String newChannelId = String.valueOf(ev.getNewValue());

			if (newChannelId.equals(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL)) ||
				newChannelId.equals(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_SMS)) ||
				newChannelId.equals(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT)) ||
				newChannelId.equals(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY))){
				issueAction.setChannelId(newChannelId);
				switch(Integer.valueOf(newChannelId)){
				case IssuesManagerBase.ISSUE_CHANNEL_SMS:
					issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
					break;
				case IssuesManagerBase.ISSUE_CHANNEL_EMAIL:
					issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
					break;
				case IssuesManagerBase.ISSUE_CHANNEL_COMMENT:
					issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
					break;
				default:
					break;
				}
			}else {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("issues.file.error.channel", null),null);
				context.addMessage("autoIssuesForm:channelId", fm);
				issueAction.setChannelId(ConstantsBase.EMPTY_STRING);
			}

		}
		context.renderResponse();
	}

	public void updateActivityId(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			issueAction.setActionTypeId(String.valueOf(ev.getNewValue()));
		}
		context.renderResponse();
	}

	private boolean valitadeForm(){
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		boolean res = true;

		if (issueAction.getChannelId().equals(ConstantsBase.EMPTY_STRING)) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("issues.file.error.channel", null),null);
			context.addMessage("autoIssuesForm:channelId", fm);

			res = false;
		}

		if (file.getFile() == null) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("issues.file.error.no.file", null),null);
			context.addMessage("autoIssuesForm:file", fm);

			res = false;
		}

		// check marketing operation id value
		long moId = issueAction.getMarketingOperationId();
		if (moId > 0){
			boolean isExistMoId = false;

			try {
				isExistMoId = IssuesManager.isExistsMarketingOperationId(moId);
			} catch (SQLException e) {
				logger.error("Problem finding MarketingOperationId " + moId,e);
			}

			if(!isExistMoId){
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("issues.file.error.no.marketing.operation.id", null),null);
				context.addMessage("autoIssuesForm:marketingOperationId", fm);

				res = false;
			}
		}
		
		if(file.getFile() != null && file.getFile().getSize()==0) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("issues.file.error.empty", null),null);
			context.addMessage("autoIssuesForm:file", fm);
			res = false;
		}

		return res;

	}

	public String getFilesPath() {
		return CommonUtil.getProperty(Constants.FILES_SERVER_PATH);
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the actionTimeHour
	 */
	public String getActionTimeHour() {
		return actionTimeHour;
	}

	/**
	 * @param actionTimeHour the actionTimeHour to set
	 */
	public void setActionTimeHour(String actionTimeHour) {
		this.actionTimeHour = actionTimeHour;
	}

	/**
	 * @return the actionTimeMin
	 */
	public String getActionTimeMin() {
		return actionTimeMin;
	}

	/**
	 * @param actionTimeMin the actionTimeMin to set
	 */
	public void setActionTimeMin(String actionTimeMin) {
		this.actionTimeMin = actionTimeMin;
	}
	
	public ArrayList<SelectItem> getIssueChannelsSI() {
		ArrayList<SelectItem> list = null;
		if (Utils.getUser().isMarketingSelected()) {
			list = new ArrayList<SelectItem>();
			for (int i = 0; i < Utils.getWriter().getIssueChannelsSI().size(); i++) {
				SelectItem selectItem = (SelectItem) Utils.getWriter().getIssueChannelsSI().get(i);
				int id = Integer.valueOf(String.valueOf(selectItem.getValue()));
				if (id == IssuesManagerBase.ISSUE_CHANNEL_COMMENT || 
						id == IssuesManagerBase.ISSUE_CHANNEL_EMAIL ||
								id == IssuesManagerBase.ISSUE_CHANNEL_PUSH_NOTIFICATION ||
										id == IssuesManagerBase.ISSUE_CHANNEL_SMS) {
					list.add(selectItem);
				}
			}
		} else {
			list = Utils.getWriter().getIssueChannelsSI();
		}
		return list;
	}
	
}
