package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class CardsForm extends FormBase implements Serializable {

	private static final long serialVersionUID = 4459703805085360345L;

	private static final Logger logger = Logger.getLogger(CardsForm.class);

	private CreditCard card;
	private ArrayList cardsList;
	private String newCardOwner;
	private ArrayList ccTypes;
	private UserBase user;
	private String blockedForDepositsCardInfo;
	private String blockedCardInfo;

	private int documentStatusFilter;
	private int depositsStatusFilter;
	private int documentsRequiredFilter;

	public CardsForm() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (CommonUtil.isHebrewSkin(ap.getSkinId())){
			ccTypes = ap.getCreditCardTypes();
		}
		else{
			ccTypes = ap.getCreditCardTypesAo();
		}
		user=(UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		documentStatusFilter = 0;
		depositsStatusFilter = 0;
		setDocumentsRequiredFilter(0);

		search();
	}

	public int getListSize() {
		return cardsList.size();
	}

	//@SuppressWarnings("unchecked")
	public void search() throws Exception{
		cardsList = TransactionsManager.getCreditCardsByUserIdAndFilters(user.getId(), documentStatusFilter, depositsStatusFilter, documentsRequiredFilter);
		//cardsList = removeAmexCC(cardsList);
		cardsList = removeCardsByRole(cardsList);
		cardsList = TransactionsManager.getAttachedFilesForCC(user.getId(), cardsList);
		cardsList = TransactionsManager.setCreditCardBinInfo(user.getId(), cardsList);
		cardsList = TransactionsManager.getCcHolderNameHistory(cardsList);
		CommonUtil.setTablesToFirstPage();
	}
	
	private static ArrayList<CreditCard> removeAmexCC(ArrayList<CreditCard> list) {
		if (list.isEmpty()) {
			return list;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isEtrader()) {
			return list;
		} else {
			ArrayList<CreditCard> res = new ArrayList<CreditCard>();
			Iterator<CreditCard> i = list.iterator();
			while (i.hasNext()) {
				CreditCard currentCC = i.next();
				if (currentCC.getTypeId() != Constants.AMEX_CARD_TYPE) {
					res.add(currentCC);
				}
			}
			return res;
		}
	}

	@SuppressWarnings("unchecked")
	private static ArrayList removeCardsByRole(ArrayList list) {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isHasAdminRole() || user.isHasSAdminRole() || user.isHasSupportRole()) {
			return list;
		}
		ArrayList res = new ArrayList();
		for (int i=0; i<list.size(); ++i) {
			CreditCard current = (CreditCard)list.get(i);
			if (current.isVisible()) {
				res.add(current);
			}
		}

		return res;
	}

	public String copyCard()	throws SQLException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
								IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
								InvalidAlgorithmParameterException {
		FacesContext context=FacesContext.getCurrentInstance();
		// check if user exist
		boolean isNewOwnerExist = UsersManager.isUserNameInUse(newCardOwner);
		if (isNewOwnerExist) {
			User originalOwner = UsersManager.getUserDetails(card.getUserId());
			// get new CC owner
			User newOwner = UsersManager.getByUserName(newCardOwner);
			try {
				String ccType = String.valueOf(card.getTypeId());
				String ccNumber = String.valueOf(card.getCcNumber());
				String ccPass = card.getCcPass();
				boolean isVisible = card.isVisible();
				boolean isAllowed = card.isAllowed();
				boolean isExistCC = false;

				ArrayList<CreditCard> newUserCCList = TransactionsManager.getAllCreditCardsByUser(newOwner.getId());
				for (int i=0; i<newUserCCList.size(); i++) {
					CreditCard tempCC = newUserCCList.get(i);
					if (card.getCcNumber() == tempCC.getCcNumber()) {
						isExistCC = true;
					}
				}
				if (! isExistCC) {
					TransactionsManager.copyCreditCardToUser(
							ccType, ccNumber ,ccPass, card.getExpMonth(), card.getExpYear(),
							card.getHolderName(),  originalOwner.getIdNum(), isVisible, isAllowed,
							AdminManager.getWriterId(), (UserBase)originalOwner, (UserBase)newOwner, Constants.BACKEND_CONST, card.isDocsAreNotRequired()
							);

					FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("card.copy.success",null, Utils.getWriterLocale(context)),null);
		    		context.addMessage(null, fm);


					return Constants.NAV_CARDS;
				} else { // new user already have this CC
					FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("card.copy.exist",null, Utils.getWriterLocale(context)),null);
		    		context.addMessage("cardsForm:accountName", fm);
				}
			} catch (NumberFormatException e) {
	    		FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.notfound",null, Utils.getWriterLocale(context)),null);
	    		context.addMessage("cardsForm:accountName", fm);
			}
		} else {
    		FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.notfound",null, Utils.getWriterLocale(context)),null);
    		context.addMessage("cardsForm:accountName", fm);
		}
		return null;
	}


	public String updateCard() throws Exception{
		//not allowing identical requetes
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(logger,null);
		}
		this.generateSaveToken();

		FacesContext context=FacesContext.getCurrentInstance();

	    if (card.getExpMonth()==null || card.getExpMonth().equals("") ||
			    card.getExpYear()==null || card.getExpYear().equals("")) {
		       		FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory",null, Utils.getWriterLocale(context)),null);
		       		context.addMessage("cardsForm:expMonth", fm);
		       		return null;
        }

	    if (card.isChanged() &&
	    		(card.getComment() == null || card.getComment().equals(""))) {
	    	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory",null, Utils.getWriterLocale(context)),null);
       		context.addMessage("cardsForm:comment", fm);
       		return null;
	    }

   		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);

		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(card.getExpYear())+2000, Integer.parseInt(card.getExpMonth()), 1);

    	if (cal.after(expCal)) {

    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.expdate",null, Utils.getWriterLocale(context)),null);
    		context.addMessage("cardsForm:expMonth", fm);
    		return null;
    	}
    	UserBase user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

    	card.setUtcOffsetModified(Utils.getUser(card.getUserId()).getUtcOffset());
		TransactionsManager.updateCreditCard(card);
		if(card.isChanged()) {
			switch(card.getPermission()) {
				case CreditCard.ALLOWED :
					String unblocked = "Unblocked " ;
					if(card.getPreviousPermission() == CreditCard.NOT_ALLOWED_DEPOSIT) {
						unblocked = unblocked + "for Deposits "; 
					}
					insertIssueActionForCreditCardAllow(unblocked + card.getCcNumberLast4() + " " + card.getComment(),card.getId() , user);
				break;
				case CreditCard.NOT_ALLOWED : 
					insertIssueActionForCreditCardAllow("Blocked " + card.getCcNumberLast4() + " "  + card.getComment(), card.getId(), user);
				break;
				case CreditCard.NOT_ALLOWED_DEPOSIT:
					insertIssueActionForCreditCardAllow("Blocked Deposits " + card.getCcNumberLast4() + " "  + card.getComment(), card.getId(), user);
				break;
				default:
					logger.info("Unknown permission state:"+card.getPermission());
				break;
			}
		}
		cardsList = TransactionsManager.getAllCreditCardsByUser(user.getId());
		cardsList = removeCardsByRole(cardsList);
		card = new CreditCard();

		search();
		return Constants.NAV_CARDS;
	}

	public String delete() throws Exception{
		TransactionsManager.deleteCards(cardsList);
		search();
		return Constants.NAV_CARDS;
	}


	public String deleteCard() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		boolean res = TransactionsManager.deleteCard(card);
		FacesMessage fm = null;

		if (res){
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("creditcards.delete.success",null),null);
		}else{
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("general.update.faild",null),null);
		}
		context.addMessage(null, fm);

		return null;
	}

	public String updateDocumentsSent() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();

		try {
			logger.info("Set document sent of card id " + card.getId() + " to: " + !card.isDocumentsSent());
			card.setDocumentsSent(!card.isDocumentsSent());
			TransactionsManager.updateCreditCard(card);
		} catch (SQLException e) {
			FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.update.failed",null),null);
    		context.addMessage(null, fm);

		}
		search();
		return null;
	}
	
	public String getBlockedCardInfo() {
		blockedCardInfo = getCardInfo();
		if (blockedCardInfo.contains("Blocked Deposits") || blockedCardInfo.contains("Unblocked for Deposits")) {
			blockedCardInfo = "";
		}
		return blockedCardInfo;
	}

	public String getBlockedForDepositsCardInfo() {
		blockedForDepositsCardInfo = getCardInfo();
		if (!(blockedForDepositsCardInfo.contains("Blocked Deposits")
				|| blockedForDepositsCardInfo.contains("Unblocked for Deposits"))
				|| blockedForDepositsCardInfo.contains("Blocked CC")) {
			blockedForDepositsCardInfo = "";
		}
		return blockedForDepositsCardInfo;
	}
	
	/**
	 * Gets the issue and the last action for it which affects
	 * CC status changes and returns the comment to be displayed
	 * @return String
	 */
	public String getCardInfo() {
		String comment = "";
		Issue issue = null;
		try {
			issue = IssuesManagerBase.getIssueByCreditCardIdAndSubjectId(card.getId(),
					IssuesManagerBase.ISSUE_SUBJ_CC_CHANGED);
			if (issue != null) {
				IssueAction lastAction = IssuesManagerBase.getLastCcChangeIssueAction(issue.getUserId(), card.getId(),
						IssuesManagerBase.ISSUE_SUBJ_CC_CHANGED, IssuesManagerBase.ISSUE_ACTION_ALLOWED_CREDIT_CARD);
				if (lastAction != null) {
					comment = lastAction.getComments() + " (" + lastAction.getWriterName() + ")";
				}
			}
		} catch (SQLException e) {
			logger.error("Issue action for issueId: " + issue.getId() + " could not be retrieved", e);
		}
		return comment;
	}
	
	public String bNavEditCC() {
		return Constants.NAV_CARD;
	}

	public ArrayList getCardsList() {
		return cardsList;
	}

	public CreditCard getCard() {
		return card;
	}

	public void setCard(CreditCard c) {

		this.card = c;
	}

	public void setCardsList(ArrayList cardsList) {
		this.cardsList = cardsList;
	}

	public void setNewCardOwner(String newCardOwner) {
		this.newCardOwner = newCardOwner;
	}

	public String getNewCardOwner() {
		return this.newCardOwner;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CardsForm ( "
	        + super.toString() + TAB
	        + "card = " + this.card + TAB
	        + "cardsList = " + this.cardsList + TAB
	        + " )";

	    return retValue;
	}

	public ArrayList getCcTypes() {
		return ccTypes;
	}

	public void setCcTypes(ArrayList ccTypes) {
		this.ccTypes = ccTypes;
	}

	public int getDepositsStatusFilter() {
		return depositsStatusFilter;
	}

	public void setDepositsStatusFilter(int depositsStatusFilter) {
		this.depositsStatusFilter = depositsStatusFilter;
	}

	public int getDocumentStatusFilter() {
		return documentStatusFilter;
	}

	public void setDocumentStatusFilter(int documentStatusFilter) {
		this.documentStatusFilter = documentStatusFilter;
	}

	public void updateBelongsToAccountHolder(ValueChangeEvent ev) {
		ev.getNewValue();
	}
	
	public String getFilesPath() {
		return CommonUtil.getProperty(Constants.FILES_SERVER_PATH);
	}

	private void insertIssueActionForCreditCardAllow(String comments, long creditCardId, UserBase userBase){
        long writerId = Utils.getWriter().getWriter().getId();
        Issue issue = new Issue();
        long id = 0;
        try {
        	id = IssuesManagerBase.getIssueIdByCreditCardId(creditCardId);
        } catch (Exception e) {
        	logger.fatal("Exception performing action on user : " + user.getId() + " when trying to get issue by cardit card");
		}
        IssueAction issueAction = new IssueAction();
        // Filling action fields
        issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
        issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_ALLOWED_CREDIT_CARD));
        issueAction.setSignificant(false);
        issueAction.setComments(comments);

        if(0 == id){ // Filling issues fields in case of new issue
        	issue = new Issue();
	        issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CC_CHANGED));
	        issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
	        issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
	        issue.setUserId(user.getId());
	        issue.setContactId(user.getContactId());
	        issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
	        issue.setCreditCardId(creditCardId);
	        try{
	            // Insert Issue & action
	        	IssuesManagerBase.insertIssue(issue, issueAction, Utils.getWriter().getUtcOffset(), writerId, userBase, 0);
	        } catch(Exception e) {
	            logger.fatal("Exception performing action on user : " + user.getId() + " when trying to insert issue");
	        }

        } else { // Insert an issue action to a exicst issue
        	FacesContext context = FacesContext.getCurrentInstance();
    		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
    		if (0 != id) {
    			issue.setId(id);
    		}
        	try{
	            // Insert Issue & action
        		issue = IssuesManagerBase.getIssueByCreditCardIdAndSubjectId(creditCardId, IssuesManagerBase.ISSUE_SUBJ_CC_CHANGED);
        		issueAction.setWriterId(AdminManager.getWriterId());
        		IssuesManager.updateIssue(issue, issueAction, false, user, 0);
	        } catch(Exception e) {
	            logger.fatal("Exception performing action on user : " + user.getId() + " when trying to  update issue");
	        }
        }
    }

	public int getDocumentsRequiredFilter() {
		return documentsRequiredFilter;
	}

	public void setDocumentsRequiredFilter(int documentsRequiredFilter) {
		this.documentsRequiredFilter = documentsRequiredFilter;
	}
}
