package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.clearing.ClearingException;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.bl_managers.ClearingManager;

/**
 * @author liors
 *
 */
public class FindRouteForm implements Serializable {

	private static final long serialVersionUID = -5011753706723094515L;
	private static final Logger logger = Logger.getLogger(FindRouteForm.class);
	private ArrayList<ClearingRoute> clearingRoutes;
	
	//Filters
	private int skinId;
	private String startBinId;
	private int countryId;
	private int currencyId;
	private int creditCardTypeId;
	private int platformId;
	
	public FindRouteForm() {
		skinId = 0;
		startBinId = "";
		countryId = 0;
		currencyId = 0;
		creditCardTypeId = 0;
		platformId = 0;		
		init();
	}
	
	public void init() {
		
	}
	
	public void search() {
		try {
			clearingRoutes = ClearingManager.findAllRoutes(startBinId, skinId, countryId, currencyId, creditCardTypeId, platformId);
		} catch (ClearingException ce) {
			logger.error("ERROR! try to find route with: " + toString(), ce);
		}
	}

	@Override
	public String toString() {
		return "FindRouteForm [clearingRoutes=" + clearingRoutes + ", skinId="
				+ skinId + ", startBinId=" + startBinId + ", countryId="
				+ countryId + ", currencyId=" + currencyId
				+ ", creditCardTypeId=" + creditCardTypeId + ", platformId="
				+ platformId + "]";
	}

	public String getProviderName(int providerId) {
		String providerName = "";
		Map<Integer, String> clearingProviders = ApplicationData.getClearingProvidersStatic(); 
		if (clearingProviders != null) {
			providerName = clearingProviders.get(providerId);
		}
		
		return providerName;
	}
	
	/**
	 * @return the clearingRoutes
	 */
	public ArrayList<ClearingRoute> getClearingRoutes() {
		return clearingRoutes;
	}

	/**
	 * @param clearingRoutes the clearingRoutes to set
	 */
	public void setClearingRoutes(ArrayList<ClearingRoute> clearingRoutes) {
		this.clearingRoutes = clearingRoutes;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the countryId
	 */
	public int getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the creditCardTypeId
	 */
	public int getCreditCardTypeId() {
		return creditCardTypeId;
	}

	/**
	 * @param creditCardTypeId the creditCardTypeId to set
	 */
	public void setCreditCardTypeId(int creditCardTypeId) {
		this.creditCardTypeId = creditCardTypeId;
	}

	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	/**
	 * @return the startBinId
	 */
	public String getStartBinId() {
		return startBinId;
	}

	/**
	 * @param startBinId the startBinId to set
	 */
	public void setStartBinId(String startBinId) {
		this.startBinId = startBinId;
	}
}
