package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Taxpayment;
import il.co.etrader.util.CommonUtil;

public class TaxpaymentsForm implements Serializable {

	private ArrayList<Taxpayment> list;

	public TaxpaymentsForm() throws SQLException{
		updateList();
	}

	public void updateList() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		list=InvestmentsManager.getTaxpayments((int)user.getId());
		CommonUtil.setTablesToFirstPage();
	}

	public int getListSize() {
		return list.size();
	}

	public ArrayList getList() {
		return list;
	}

}
