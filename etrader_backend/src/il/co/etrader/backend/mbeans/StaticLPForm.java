package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_managers.StaticLPManager;
import il.co.etrader.backend.bl_vos.StaticLandingPageLanguage;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

/**
 * @author EranL
 *
 */

public class StaticLPForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(StaticLPForm.class);

	private ArrayList<StaticLandingPageLanguage> landingPageList;
	//filters
	private long type;
	private long languageId;
	private long staticLandingPageId;
	private long staticLandingPagePathId;
	private ArrayList<SelectItem> listLandingPageNames;

	public StaticLPForm() throws SQLException {
		languageId = 0;
		landingPageList = new ArrayList<StaticLandingPageLanguage>();
		listLandingPageNames = new ArrayList<SelectItem>();
		search();
	}


	public String search() throws SQLException {
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;
        
        if (u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }
        
		landingPageList = StaticLPManager.getStaticLandigPages(type, languageId, staticLandingPageId, writerIdForSkin, null, staticLandingPagePathId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public int getListSize() {
		return landingPageList.size();
	}

	public ArrayList<SelectItem> getStaticLP() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),"All"));
		list.addAll(StaticLPManager.getStaticLandingPages());
		return list;
	}
	
	public ArrayList<SelectItem> getStaticLPByWriterSkinIdSI() throws SQLException {
		

	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Utils.getWriter().getWriter().getId();
        
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),"All"));
		list.addAll(StaticLPManager.getStaticLandingPagesByWriterSkinIdSI(writerIdForSkin));
		return list;
	}


	/**
	 * @return the landingPageList
	 */
	public ArrayList<StaticLandingPageLanguage> getLandingPageList() {
		return landingPageList;
	}

	/**
	 * @param landingPageList the landingPageList to set
	 */
	public void setLandingPageList(ArrayList<StaticLandingPageLanguage> landingPageList) {
		this.landingPageList = landingPageList;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}


	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the staticLandingPageId
	 */
	public long getStaticLandingPageId() {
		return staticLandingPageId;
	}

	/**
	 * @param staticLandingPageId the staticLandingPageId to set
	 */
	public void setStaticLandingPageId(long staticLandingPageId) {
		this.staticLandingPageId = staticLandingPageId;
	}

	/**
	 * @return the type
	 */
	public long getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(long type) {
		this.type = type;
	}


	/**
	 * @return the staticLandingPagePathId
	 */
	public long getStaticLandingPagePathId() {
		return staticLandingPagePathId;
	}


	/**
	 * @param staticLandingPagePathId the staticLandingPagePathId to set
	 */
	public void setStaticLandingPagePathId(long staticLandingPagePathId) {
		this.staticLandingPagePathId = staticLandingPagePathId;
	}
	
	/**
	 * Get Static Lp Names SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getStaticLpNamesSI() {
		listLandingPageNames = new ArrayList<SelectItem>();
		try {
			logger.log(Level.DEBUG,"About to get list of static landing pages.");
			listLandingPageNames.addAll(StaticLPManager.getStaticLpNamesSI(ConstantsBase.MARKETING_LANDING_PAGE_STATIC, staticLandingPagePathId));
		} catch (SQLException e) {
			logger.error("Problem to get list of static landing pages.", e);
		}
		return listLandingPageNames;
	}
}
