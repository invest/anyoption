package il.co.etrader.backend.mbeans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.StaticLPManager;
import il.co.etrader.backend.bl_vos.StaticLandingPage;
import il.co.etrader.backend.bl_vos.StaticLandingPageLanguage;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingLandingPage;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Upload static landing pages to database and to backend path. in the end it will upload to all WEB servers.
 * @author EranL
 *
 */
public class UploadLPForm implements Serializable {

	private static final long serialVersionUID = 1L;	
	private static final Logger log = Logger.getLogger(UploadLPForm.class);
	//many list box     
    private List<String> languages;
    private List<String> existingLanguages;
    private HashSet<String> existingLanguagesHS;
    private List<String> newLanguages;
    //path to folder
    private UploadedFile file;
    //landing pages list
    private ArrayList<StaticLandingPageLanguage> landingPageList;
    private long writerId;    
    //landing page name
    private String landingPageName;
    private long typeId;
    private long staticLandingPageId;
    //step
    private int step;
    //confirmation msg
    private String confirmationMsg;    
    // etrader or anyoption
    private String brand;
    
    private boolean onlyUploadZIP;
        
    //steps
    private static final int STEP_INSERT_LP_NAME	= 1;
    private static final int STEP_LP_EXIST 			= 2;
    private static final int STEP_CHOOSE_LANGUAGES 	= 3;    
    private static final int STEP_CONFIRMATION	 	= 4;
    private static final int STEP_CHOOSE_ZIP_FILE 	= 5;
    
    //navigate between steps
    private static final int NEXT	= 1;
    private static final int BACK	= 2;
    private static final int CANCEL	= 3;
    // static landing page path
    private long staticLandingPagePathId;
    
    public UploadLPForm() {
    	init();
    	writerId = Utils.getWriter().getWriter().getId();
    }
    
    /**
     * Reset all relevant attributes
     */
    private void init() {
    	landingPageList = new ArrayList<StaticLandingPageLanguage>();
    	languages = new ArrayList<String>();
    	existingLanguages = new ArrayList<String>();
    	existingLanguagesHS = new HashSet<String>();
    	newLanguages = new ArrayList<String>();    	
    	landingPageName = ConstantsBase.EMPTY_STRING;
    	step = STEP_INSERT_LP_NAME;
    	file = null;
    	typeId = 0;
    	onlyUploadZIP = false;
    }
    
    /**
     * Navigate the user to the previous/ next page. In addition he can choose to cancel the process at any time. 
     * @param choise
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public String navigate(long choise) throws SQLException, IOException {
    	int userChoise = Integer.valueOf(String.valueOf(choise));
    	switch (userChoise) {
			case NEXT:
				if (step == STEP_INSERT_LP_NAME) {
					// in case of Master option we want to skip all steps and redirect the user to the final step to upload the ZIP file
					if (staticLandingPagePathId == StaticLPManager.STATIC_LANDING_PAGE_PATH_MO) {
						onlyUploadZIP = true;
						step = STEP_CHOOSE_ZIP_FILE;
					} else {
						searchForLandingPage();
					}
				} else if (step == STEP_LP_EXIST) {
					step = STEP_CHOOSE_LANGUAGES;
					languages.clear();
					newLanguages.clear();
					languages.addAll(existingLanguages);
				} else if (step == STEP_CHOOSE_LANGUAGES) {					
					step = STEP_CONFIRMATION;
					buildConfirmationMsg();
				} else if (step == STEP_CONFIRMATION) {
					step = STEP_CHOOSE_ZIP_FILE;
				} else if (step == STEP_CHOOSE_ZIP_FILE) {
					handleFiles();
				}
				break;	
			case BACK:
				// in case of Master option we want to init the selections
				if (staticLandingPagePathId == StaticLPManager.STATIC_LANDING_PAGE_PATH_MO) {
					init();
				} else if (step == STEP_CHOOSE_LANGUAGES) {
					step = STEP_INSERT_LP_NAME;
				} else if (step == STEP_CONFIRMATION) {
					step = STEP_CHOOSE_LANGUAGES;
				} else if (step == STEP_CHOOSE_ZIP_FILE) {
					step = STEP_CONFIRMATION;
				}
				break;
			case CANCEL:
				init();
				break;
		default:
			break;
		}
    	return null;
    }
    
	/**
	 * Return languages list per brand - staticLandingPagePathId
	 * @return
	 */
	public ArrayList<SelectItem> getLanguagesPerBrand() {		
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		boolean isEtrader = true;		
		if(staticLandingPagePathId != StaticLPManager.STATIC_LANDING_PAGE_PATH_ET) {
			isEtrader = false;
		}
		for (Object select : Utils.getWriter().getLanguagesSI()) {
			SelectItem si = (SelectItem)select;
			if (isEtrader && (si.getValue().toString().equals(String.valueOf(Constants.LANGUAGES_HEBREW)))) {				
				list.add(si);
				break;
			}
			if (!isEtrader && (!si.getValue().toString().equals(String.valueOf(Constants.LANGUAGES_HEBREW)))) {
				list.add(si);
			}
		}								
		return list;
	}
	
	
    /**
     * Build confirmation message  
     */
    private void buildConfirmationMsg() {
    	String typeName = ConstantsBase.EMPTY_STRING;
    	String existingLanguagesStr = Constants.EMPTY_STRING;
    	String newLanguagesStr = Constants.EMPTY_STRING;
    	newLanguages.clear();
		for (int i = 0; i < existingLanguages.size(); i++) {    			
			long lang = Long.valueOf(String.valueOf(existingLanguages.get(i)));
			String displayName = ApplicationData.getLanguage(lang).getDisplayName();									
			existingLanguagesStr += CommonUtil.getMessage(displayName, null) + ",";
		}
		for (int i = 0; i < languages.size(); i++) {
			String lang = String.valueOf(languages.get(i));
			if (!existingLanguagesHS.contains(lang)) {
				newLanguages.add(lang);
				long langId = Long.valueOf(lang);
				String displayName = ApplicationData.getLanguage(langId).getDisplayName();											
				newLanguagesStr += CommonUtil.getMessage(displayName, null)  + ",";
			}
		}	
		// get type name of the landing page
		for (Object select : Utils.getWriter().getStaticLPTypesSI()) {
			SelectItem si = (SelectItem) select;
			if (si.getValue().toString().equals(String.valueOf(typeId))) {
				typeName = si.getLabel(); 
				break;
			}
		}
		
		confirmationMsg = "Brand:" + Utils.getWriter().getStaticLandingPagePathHM().get(staticLandingPagePathId).getName()  + "<br/>" + 
						  "Landing page name: " + landingPageName  + "<br/>" +
						  "Type: " + typeName  + "<br/>" +
						  "Existing lanuages: " + existingLanguagesStr + "<br/>" +
						  "New languages: " + newLanguagesStr + "<br/>";
		log.info("\nConfirmation for uploading landing pages: \n " +
				  "WriterId:" + writerId + "\n" +
				   confirmationMsg.replace("<br/>", "\n"));
    }
    
    /**
     * Handle files in the end of the process.
     * 1. Upload files to the relevant destination
     * 2. Insert pages to the database
     * 3. Navigate and add appropriate message in case of success or fail 
     * @return
     */
    private String handleFiles() {
    	boolean success = false;
    	FacesContext context = FacesContext.getCurrentInstance();
    	FacesMessage fm = null;
    	try {
	    	uploadFile(staticLandingPagePathId);
	    	if (!onlyUploadZIP) {
		    	insertToDB(staticLandingPagePathId);
		    	if (staticLandingPagePathId == StaticLPManager.STATIC_LANDING_PAGE_PATH_AO_REGULATED) {
		    		uploadFile(StaticLPManager.STATIC_LANDING_PAGE_PATH_AO_NONE_REGULATED);
			    	insertToDB(StaticLPManager.STATIC_LANDING_PAGE_PATH_AO_NONE_REGULATED);
		    	}
	    	}
	    	success = true;
    	} catch (IOException ioe) {
    		log.error("Error while trying to upload file ", ioe);
		} catch (SQLException sqle) {
			log.error("Error while trying to upload file ", sqle);
		} catch (Exception e) {
			log.error("Error while trying to upload file ", e);
		}
    	if (success) {
    		init();
    		fm = new FacesMessage(FacesMessage.SEVERITY_INFO, 
    				CommonUtil.getMessage("lp.upload.success", null, Utils.getWriterLocale(context)),null);    		
    	} else {
    		fm = new FacesMessage(FacesMessage.SEVERITY_INFO, 
    				CommonUtil.getMessage("lp.upload.fail", null, Utils.getWriterLocale(context)),null);    		    				
    	}
    	context.addMessage(null, fm);
    	return null;
    }
        
    /**
     * Search for existing landing page
     * 1. Build existing landing page list.
     * 2. In case of mismatch between the selected landing page brand and the landing page that we found from the database we will throw an error.
     * 3. Navigate the user to the relevant page. 
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public String searchForLandingPage() throws SQLException, IOException {    	
    	FacesContext context = FacesContext.getCurrentInstance();
		boolean isEtrader = true;		
		if(staticLandingPagePathId != StaticLPManager.STATIC_LANDING_PAGE_PATH_ET) {
			isEtrader = false;
		}
		ArrayList<StaticLandingPageLanguage> listStaticLP = StaticLPManager.getStaticLandigPages(0, 0, 0, 0, landingPageName, 0);
    	existingLanguages.clear();
    	String[] params = new String[2];
    	boolean error = false;
    	for (StaticLandingPageLanguage lp: listStaticLP) {
    		// validation for AO & ET.
    		long lpPathId = lp.getStaticLandingPagePath().getId();
    		if (isEtrader && (lpPathId != StaticLPManager.STATIC_LANDING_PAGE_PATH_ET)) {				
				params[0] = "anyoption";
				params[1] = "etrader";
				error = true;
		    	break;
    		} else if (!isEtrader && (lpPathId == StaticLPManager.STATIC_LANDING_PAGE_PATH_ET)) {
				params[0] = "etrader";
				params[1] = "anyoption";
				error = true;
    			break;
    		}
    		// Getting only relevant data.
    		if (staticLandingPagePathId == lpPathId) {
    			String languageId = String.valueOf(lp.getLanguageId());
        		existingLanguages.add(languageId);
        		existingLanguagesHS.add(languageId);
        		typeId = lp.getTypeId();
        		staticLandingPageId = lp.getStaticLandingPageId();
        		landingPageList.add(lp);
    		}
    	}
    	if (error) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("lp.upload.error.brand.mismatch", params), null);
	    	context.addMessage(null, msg);
	    	return null;
    	}
    	step = STEP_CHOOSE_LANGUAGES;
    	if (landingPageList.size() > 0) {
    		step = STEP_LP_EXIST;
    	} else {
    		staticLandingPageId = 0;
    	}
    	return null;
    }
        
    /**
     * Insert into DataBase
     * 1. Insert static landing page
     * 2. Insert static landing page language
     * For Italian language we we also upload to anyoption.it skin (skin id = 19) 
     * @return
     * @throws SQLException
     */
    private void insertToDB(long pathId) throws SQLException {
    	StaticLandingPage slp = new StaticLandingPage();
    	slp.setId(staticLandingPageId);     
    	if (slp.getId() == 0) {
        	//insert static landing page        	
        	slp.setName(landingPageName);
        	slp.setTypeId(typeId);
        	slp.setWriterId(writerId);
        	slp.setStaticLandingPagePathId(pathId);
        	log.debug("Going to insert landing page:" + landingPageName + " typeId:" + typeId + " writerId:" + writerId + " pathId:" + pathId + " to DB.");
        	StaticLPManager.insertStaticLandingPage(slp);
        	MarketingLandingPage mlp = new MarketingLandingPage();
        	mlp.setName(landingPageName);
        	mlp.setUrl(landingPageName);
        	mlp.setType(ConstantsBase.MARKETING_LANDING_PAGE_STATIC);
        	mlp.setWriterId(writerId);
        	mlp.setStaticLandingPageId(slp.getId());
        	MarketingManager.insertLandingPage(mlp);
        	log.debug("Success inserting landing page:" + landingPageName + " typeId:" + typeId + " writerId:" + writerId + " staticLandingPageId:" + slp.getId() + " to DB.");
    	}    	    	
    	//insert static landing page language
    	for (String lang : newLanguages) {
    		StaticLandingPageLanguage slpl = new StaticLandingPageLanguage();
    		slpl.setWriterId(writerId);
    		slpl.setTypeId(typeId);    		
    		slpl.setLanguageId(Long.valueOf(lang));
    		slpl.setStaticLandingPageId(slp.getId());
    		String langCode = ApplicationData.getLanguage(Long.valueOf(lang)).getCode();
    		long skinId; 
    		if (langCode.equalsIgnoreCase("iw")) {
    			skinId = Skin.SKIN_ETRADER;
    		} else {
    			Long skinIdLong = ApplicationData.getSkinByLanguageCode(ApplicationData.getLanguage(Long.valueOf(lang)).getCode(), false);
    			if (skinIdLong == null) {
    				skinIdLong = ApplicationData.getSkinByLanguageCode(ApplicationData.getLanguage(Long.valueOf(lang)).getCode(), true);
    			}
    			skinId = skinIdLong.longValue();     			
    		}   
    		
    		
    		slpl.setSkinId(skinId);
    		log.debug("Going to insert landing page language:" + landingPageName + " typeId:" + typeId + " languageId:" + lang + " writerId:" + writerId + " to DB.");
    		StaticLPManager.insertStaticLandingPageLanguage(slpl);
    		log.debug("Success inserting landing page language:" + landingPageName + " typeId:" + typeId + " languageId:" + lang + " writerId:" + writerId + " to DB.");
    		if (skinId == Skin.SKIN_ITALIAN) {    			
    			slpl.setSkinId(Skin.SKIN_REG_IT);
    			log.debug("Going to insert landing page language:" + landingPageName + " typeId:" + typeId + " languageId:" + lang + " writerId:" + writerId + " to DB.");
    			StaticLPManager.insertStaticLandingPageLanguage(slpl);
    			log.debug("Success inserting landing page language:" + landingPageName + " typeId:" + typeId + " languageId:" + lang + " writerId:" + writerId + " to DB.");
    		}    		
    	}    	
    }
    
	/**
	 * Validate ZIP file
	 * 1. File in the right format (ZIP file)
	 * 2. File name is the same as the name of the landing page 
	 * @param context
	 * @param comp
	 * @param value
	 */
	public void validateFile(FacesContext context, UIComponent comp, Object value) {
		UploadedFile file = (UploadedFile) value;
		if (file != null) {
			String fileContentType = file.getContentType();
			if (CommonUtil.isParameterEmptyOrNull(fileContentType))  {
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("lp.upload.error.file.content.type", null));
				throw new ValidatorException(msg);
			}
			String tmpLandingPageName = file.getName().substring(0, file.getName().lastIndexOf('.')); 
			if (!tmpLandingPageName.equals(landingPageName)) {
				String[] params = new String[2];
				params[0] = landingPageName;
				params[1] = tmpLandingPageName;
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("lp.upload.error.file.not.match", params), null);
				throw new ValidatorException(msg);
			}
			String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1, file.getName().length());
			if (!extension.equalsIgnoreCase("zip")) {			
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("lp.upload.error.file.content.type", null));
				throw new ValidatorException(msg);				
			}
		}
	}
	
	/**
	 * Upload ZIP file to specific location for anyoption and etrader  
	 * @throws IOException
	 */
	private void uploadFile(long pathId) throws IOException {		
		if (file != null) {			
			String name = file.getName();
			name = modifyFileName(name);				
			BufferedInputStream bis = new BufferedInputStream(file.getInputStream(), 4096);
			
			String path = CommonUtil.getProperty(Constants.LP_UPLOAD_PATH) + "/" + Utils.getWriter().getStaticLandingPagePathHM().get(pathId).getFolderName() + "/" + name;
			log.debug("Going to upload landing page " + landingPageName + " to:" + path);
			java.io.File targetFile = new java.io.File(path); // destination
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile),4096);
			int theChar;
			while((theChar = bis.read()) != -1) {
				bos.write(theChar);
			}
			bos.close();
			bis.close();
			log.debug("Success uploading landing page " + landingPageName + " to:" + path);
		} 
	}
	
	/**
	 * Modify file name for safely uploading 
	 * @param name
	 * @return
	 */
	private static String modifyFileName(String name) {
		int lastChar = name.lastIndexOf('\\');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		lastChar = name.lastIndexOf('/');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		name = name.replaceAll("\\?", "");
		name = name.replaceAll(":", "");
		name = name.replaceAll("\\*", "");
		name = name.replaceAll("<", "");
		name = name.replaceAll(">", "");
		name = name.replaceAll("\"", "");
		name = name.replaceAll("|", "");
		return name;
	}
	
	public boolean isPathRegulated() {
		if(Utils.getWriter().getStaticLandingPagePathHM().get(staticLandingPagePathId).isRegulated()) {
			return true;
		}
		return false;
	}
        
	public int getListSize() {
		return landingPageList.size();
	}
	
	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}

	/**
	 * @param step the step to set
	 */
	public void setStep(int step) {
		this.step = step;
	}

	/**
	 * @return the landingPageList
	 */
	public ArrayList<StaticLandingPageLanguage> getLandingPageList() {
		return landingPageList;
	}

	/**
	 * @param landingPageList the landingPageList to set
	 */
	public void setLandingPageList(
			ArrayList<StaticLandingPageLanguage> landingPageList) {
		this.landingPageList = landingPageList;
	}
	
	/**
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * @return the languages
	 */
	public List<String> getLanguages() {
		return languages;
	}

	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the confirmationMsg
	 */
	public String getConfirmationMsg() {
		return confirmationMsg;
	}

	/**
	 * @param confirmationMsg the confirmationMsg to set
	 */
	public void setConfirmationMsg(String confirmationMsg) {
		this.confirmationMsg = confirmationMsg;
	}

	/**
	 * @return the landingPageName
	 */
	public String getLandingPageName() {
		return landingPageName;
	}

	/**
	 * @param landingPageName the landingPageName to set
	 */
	public void setLandingPageName(String landingPageName) {
		this.landingPageName = landingPageName;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the staticLandingPagePathId
	 */
	public long getStaticLandingPagePathId() {
		return staticLandingPagePathId;
	}

	/**
	 * @param staticLandingPagePathId the staticLandingPagePathId to set
	 */
	public void setStaticLandingPagePathId(long staticLandingPagePathId) {
		this.staticLandingPagePathId = staticLandingPagePathId;
	}

	/**
	 * @return the onlyUploadZIP
	 */
	public boolean isOnlyUploadZIP() {
		return onlyUploadZIP;
	}

	/**
	 * @param onlyUploadZIP the onlyUploadZIP to set
	 */
	public void setOnlyUploadZIP(boolean onlyUploadZIP) {
		this.onlyUploadZIP = onlyUploadZIP;
	}
	
}
