package il.co.etrader.backend.mbeans;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.MarketGroup;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;


public class GroupsForm implements Serializable {

	private MarketGroup group;
	private ArrayList list;

	public GroupsForm() throws SQLException{
		search();
	}

	public int getListSize() {
		return list.size();
	}


	public String updateInsertGroup() throws SQLException{

		boolean res=InvestmentsManager.updateInsertGroup(group);
		if (res==false)
			return null;
		search();
		return Constants.NAV_GROUPS;
	}

	public String bNavAddGroup() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		group=new MarketGroup();
		group.setWriterId(AdminManager.getWriterId());
		group.setTimeCreated(new Date());

		return Constants.NAV_GROUP;
	}
	public String bNavEditGroup() {

		return Constants.NAV_GROUP;
	}

	public String bNavGroups() throws SQLException{

		search();
		return Constants.NAV_GROUPS;
	}

	public String search() throws SQLException{

		list=InvestmentsManager.searchGroups();
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public ArrayList getList() {
		return list;
	}

	public MarketGroup getGroup() {
		return group;
	}

	public void setGroup(MarketGroup group) {
		this.group = group;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "GroupsForm ( "
	        + super.toString() + TAB
	        + "group = " + this.group + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}


}
