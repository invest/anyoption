package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.datascroller.HtmlDataScroller;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.InvestmentsGroup;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class AnalForm implements Serializable {

	private static final long serialVersionUID = 2457707205427118344L;

	//private static final Logger logger = Logger.getLogger(AnalForm.class);

	private List<SelectItem> groupByList;
	private Integer groupBy;
	private Date date;
	private String marketGroup;
	private long classId;
	private long statusId;
	private long currencyId;
	private String htmlBuffer;
	private int skinId;
	private long fromHour;
	private long fromMin;
	private long toHour;
	private long toMin;
	private Date fromDate;
	private Date toDate;
	private long opportunityTypeId;
	private ArrayList<SelectItem> countriesSI;

	// filters
	long skinBusinessCaseId;
	long scheduledId;
	long groupId;
	long market;
	long countryId;	
	private List<String> insertedFrom;
	private int copyopInvTypeId;
	private long fromUserId;
	
	private String searchBy = "0";


	// Page Constants
	public final static int GROUP_BY_USER = 1;
	public final static int GROUP_BY_ASSET = 2;
	public final static int GROUP_BY_USER_AND_EXTERNAL = 3;
	public final static int GROUP_BY_EXTERNAL = 4;
	public final static int GROUP_BY_SKIN = 5;
	private final static int DATA_ROWS = 20;
	private final static int SUM_ROWS = 4;
	private HtmlDataScroller scroll = new HtmlDataScroller();

	private List<InvestmentsGroup> investmentsGroups;
	private List<InvestmentsGroup> investmentsGroupsSum;
	private String sortColumn;
	private String prevSortColumn;
	private boolean sortAscending = false;
	InvestmentsGroup igTotal;

	private String closingHour;
	private long showAdvanced;


	public AnalForm() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(Utils.getWriter().getUtcOffset()));
		gc.set(Calendar.HOUR_OF_DAY, 0 + gc.getTimeZone().getRawOffset()/(1000*60*60));
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		date = new Date(gc.getTimeInMillis());


		marketGroup = "0,0";
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		statusId = ConstantsBase.INVESTMENT_CLASS_STATUS_ACTIVE; // default to active investment
		currencyId = ConstantsBase.CURRENCY_ALL_ID;
		setSkinId(Skin.SKINS_ALL_VALUE);

		//if the writer is an etrader writer default currency id dropdown to ILS
		if (Utils.getWriter().isEtrader()) {
			currencyId = ConstantsBase.CURRENCY_ILS_ID;
		}

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.isSupportSelected() || user.isRetentionSelected() ||
				user.isRetentionMSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
			currencyId = user.getCurrencyId();
		}
		closingHour = new String("");
		showAdvanced = 0;
		groupByList = new ArrayList<SelectItem>();
		groupByList.add(new SelectItem(Integer.valueOf(GROUP_BY_ASSET),CommonUtil.getMessage("anal.filter.group.by.asset",null, Utils.getWriterLocale(context))) );
		groupByList.add(new SelectItem(Integer.valueOf(GROUP_BY_USER),CommonUtil.getMessage("anal.filter.group.by.user",null, Utils.getWriterLocale(context))) );
		groupByList.add(new SelectItem(Integer.valueOf(GROUP_BY_USER_AND_EXTERNAL),CommonUtil.getMessage("anal.filter.group.by.user.and.external",null, Utils.getWriterLocale(context))) );
		groupByList.add(new SelectItem(Integer.valueOf(GROUP_BY_EXTERNAL),CommonUtil.getMessage("anal.filter.group.by.external",null, Utils.getWriterLocale(context))) );
		groupByList.add(new SelectItem(Integer.valueOf(GROUP_BY_SKIN),CommonUtil.getMessage("anal.filter.group.by.skin",null, Utils.getWriterLocale(context))) );

		groupBy = Integer.valueOf(GROUP_BY_USER);
		skinId = 0;
		fromHour = 99;
		fromMin = 99;
		toHour = 99;
		toMin = 99;
		opportunityTypeId = Opportunity.TYPE_REGULAR;
		insertedFrom = new ArrayList<String>();
		insertedFrom.add("-1");
		fromUserId = 0;
		updateList(true);
	}


	public boolean getIsGroupByUser() {
		return groupBy == GROUP_BY_USER;
	}

	public boolean getIsGroupByAsset() {
		return groupBy == GROUP_BY_ASSET;
	}

	public boolean getIsGroupByWithExternal() {
		return groupBy == GROUP_BY_EXTERNAL || groupBy == GROUP_BY_USER_AND_EXTERNAL;
	}
	
	public boolean getIsGroupBySkin() {
		return groupBy == GROUP_BY_SKIN;
	}

	public Integer getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(Integer groupBy) {
		this.groupBy = groupBy;
	}

	public List<SelectItem> getGroupByList() {
		return groupByList;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	/**
	 * @return the from
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param from the from to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the classId
	 */
	public long getClassId() {
		return classId;
	}

	/**
	 * @param classId the classId to set
	 */
	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}


	/**
	 * @return the htmlBuffer
	 */
	public String getHtmlBuffer() {
		return htmlBuffer;
	}

	/**
	 * @param htmlBuffer the htmlBuffer to set
	 */
	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}

	/**
	 * @return the marketGroup
	 */
	public String getMarketGroup() {
		return marketGroup;
	}

	/**
	 * @param marketGroup the marketGroup to set
	 */
	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @return true if no specific currency selected
	 */
	public boolean isCurrencyAllSelected() {
		return currencyId == ConstantsBase.CURRENCY_ALL_ID;
	}

	/**
	 * @return true if specific currency selected
	 */
	public boolean isStatusAllSelected() {
		return statusId == ConstantsBase.INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the scroll
	 */
	public HtmlDataScroller getScroll() {
		return scroll;
	}

	/**
	 * @param scroll the scroll to set
	 */
	public void setScroll(HtmlDataScroller scroll) {
		this.scroll = scroll;
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {

		String ls = System.getProperty("line.separator");
		String retValue =
				"InvestmentsForm ( " + super.toString() + ls +
				"date = " + this.date + ls +
				"marketGroup = " + this.marketGroup + ls +
				"classId = " + this.classId + ls +
				"statusId = " + this.statusId + ls +
				"currencyId = " + this.currencyId + ls +
				"closingHour = " +this.closingHour+ ls +
				"opportunityTypeId = " +this.opportunityTypeId+ ls +
				" )";

		return retValue;
	}


	/**
	 * Get investmentsGroups list size
	 * @return
	 * 		size of all the model and not page size
	 */
	public int getListSize() {
		int size = 0;
		if ( null != investmentsGroups ) {
			size = investmentsGroups.size();
		}

		return size;
	}

	public List<InvestmentsGroup> getInvestmentsGroups() {
		return investmentsGroupsSum;
	}

	public void setInvestmentsGroups(List<InvestmentsGroup> list) {

	}

	public void setDates() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		if (99 == fromHour || 99 == fromMin) {
			//set time as 00:00:00
			gc.set(Calendar.HOUR_OF_DAY, 0);
			gc.set(Calendar.MINUTE, 5);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			fromDate = CommonUtil.getDateTimeFormat(gc.getTime(), Utils.getWriter().getUtcOffset());
		} else {
			gc.set(Calendar.HOUR_OF_DAY, (int)fromHour);
			gc.set(Calendar.MINUTE, (int)fromMin);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			fromDate = CommonUtil.getDateTimeFormat(gc.getTime(), Utils.getWriter().getUtcOffset());
		}
		if (99 == toHour || 99 == toMin) {
			//set time as 00:00:00
			gc.set(Calendar.HOUR_OF_DAY, 24);
			gc.set(Calendar.MINUTE, 5);
			gc.set(Calendar.SECOND, 59);
			gc.set(Calendar.MILLISECOND, 999);
			toDate = CommonUtil.getDateTimeFormat(gc.getTime(), Utils.getWriter().getUtcOffset());
		} else {
			gc.set(Calendar.HOUR_OF_DAY, (int)toHour);
			gc.set(Calendar.MINUTE, (int)toMin);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			toDate = CommonUtil.getDateTimeFormat(gc.getTime(), Utils.getWriter().getUtcOffset());
		}
	}

	/**
	 * Update investments list function.
	 * Create new DataModel instance if needed
	 * and symbol that search filter preesed for new DataModel creation
	 * @return
	 */
	public String updateList() throws SQLException {
		return updateList(true);
	}

	public String updateList(boolean setList2) throws SQLException {
		setDates();
		HashMap<Long, String> h = new HashMap<Long, String>();
		
		investmentsGroups = InvestmentsManager.getInvestmentsGroups(groupBy, fromDate, toDate, skinId, opportunityTypeId ,searchBy, 
	    		 skinBusinessCaseId,
	    		 scheduledId,
	    		 groupId,
	    		 market,
	    		 countryId, getWritersListToString(insertedFrom), copyopInvTypeId, fromUserId, statusId);
		igTotal = new InvestmentsGroup(InvestmentsGroup.SUM_IDX_TOTAL);
		HashMap<Long, Country> allCountries = ApplicationData.getCountries();
		for (int i=0; i< investmentsGroups.size(); ++i) {
			InvestmentsGroup investmentsGroup = investmentsGroups.get(i);
			igTotal.setTotalInv(igTotal.getTotalInv() + investmentsGroup.getTotalInv());
			igTotal.setTotalReturn(igTotal.getTotalReturn() + investmentsGroup.getTotalReturn());
			igTotal.setHits(igTotal.getHits() + investmentsGroup.getHits());
			igTotal.setCount(igTotal.getCount() + investmentsGroup.getCount());
			Skins skin = ApplicationData.getSkinById(investmentsGroup.getSkinId());
			if (null != skin) {
				investmentsGroup.setSkin(skin.getDisplayName());
			}
			
			if(countryId == 0) {
				List<String> countries = Arrays.asList(investmentsGroup.getCountryIds().split(","));
				HashSet<String> set = new HashSet<String>(countries);
				
				for(String countryId : set) {
					Long key = Long.parseLong(countryId);
					h.put(key, allCountries.get(key).getA3());
				}
			} else {
				h.put(new Long(investmentsGroup.getCountryId()), allCountries.get(investmentsGroup.getCountryId()).getA3());
			}
		}
		if (setList2) {
			setList2();
			try {
				sortColumn = "totalProf";
				sortList();
				prevSortColumn = "";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		countriesSI = CommonUtil.hashMap2SortedArrayList(h);
		
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public void setList2() {

		investmentsGroupsSum = new ArrayList<InvestmentsGroup>((int)(investmentsGroups.size() * 1.5));
		//InvestmentsGroup igSum
		int pageNum = (int)Math.ceil((double)investmentsGroups.size() / (double)DATA_ROWS);
		int nextIndex = 0;
		for (int i=0; i<pageNum; ++i) {
			InvestmentsGroup igSum = new InvestmentsGroup(InvestmentsGroup.SUM_IDX_SUM);
			int rowsInPage;
			for (rowsInPage=0; rowsInPage<DATA_ROWS; ++rowsInPage) {
				if (nextIndex >= investmentsGroups.size()) {
					break;
				}
				InvestmentsGroup igCurr = investmentsGroups.get(nextIndex++);
				igSum.setTotalInv(igSum.getTotalInv() + igCurr.getTotalInv());
				igSum.setTotalReturn(igSum.getTotalReturn() + igCurr.getTotalReturn());
				igSum.setHits(igSum.getHits() + igCurr.getHits());
				igSum.setCount(igSum.getCount() + igCurr.getCount());
				investmentsGroupsSum.add(igCurr);
			}
			if (pageNum > 1 && rowsInPage>1) {
				investmentsGroupsSum.add(new InvestmentsGroup(InvestmentsGroup.SUM_IDX_LINE));
				investmentsGroupsSum.add(igSum);
			}
			if (investmentsGroups.size() > 1) {
				investmentsGroupsSum.add(new InvestmentsGroup(InvestmentsGroup.SUM_IDX_LINE));
				investmentsGroupsSum.add(igTotal);
			}
		}
	}


	public String sortList() throws Exception {
		updateList(false);

		if (sortColumn.equals(prevSortColumn))
			sortAscending=!sortAscending;
		else
			sortAscending=false;

		prevSortColumn=sortColumn;

		if (sortColumn.equals("invCount"))
			Collections.sort(investmentsGroups, new InvCountComparator(sortAscending));

		if (sortColumn.equals("totalInv"))
			Collections.sort(investmentsGroups, new TotalInvComparator(sortAscending));

		if (sortColumn.equals("totalRtn"))
			Collections.sort(investmentsGroups, new TotalRtnComparator(sortAscending));

		if (sortColumn.equals("precRtn"))
			Collections.sort(investmentsGroups, new PrecRtnComparator(sortAscending));

		if (sortColumn.equals("totalProf"))
			Collections.sort(investmentsGroups, new TotalProfitComparator(!sortAscending));

		if (sortColumn.equals("precHit"))
			Collections.sort(investmentsGroups, new PrecHitComparator(sortAscending));

		if(sortColumn.equals("usersBalance"))
			Collections.sort(investmentsGroups, new usersBalanceComparator(sortAscending));

		if(sortColumn.equals("usersBalanceBase"))
			Collections.sort(investmentsGroups, new usersBalanceBaseComparator(sortAscending));

		if(sortColumn.equals("asset"))
			Collections.sort(investmentsGroups, new assetComparator(!sortAscending));

		setList2();

		return null;

	}

	private class InvCountComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public InvCountComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getCount()).compareTo(new Long(ig2.getCount()));
   		 		}
   		 	return -new Long(ig1.getCount()).compareTo(new Long(ig2.getCount()));
		}
  	 }
	private class assetComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public assetComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new String(ig1.getAsset()).compareTo(new String(ig2.getAsset()));
   		 		}
   		 	return -new String(ig1.getAsset()).compareTo(new String(ig2.getAsset()));
		}
  	 }

	private class usersBalanceComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public usersBalanceComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getUsersBalance()).compareTo(new Long(ig2.getUsersBalance()));
   		 		}
   		 	return -new Long(ig1.getUsersBalance()).compareTo(new Long(ig2.getUsersBalance()));
		}
  	 }

	private class usersBalanceBaseComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public usersBalanceBaseComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getUsersBalanceBase()).compareTo(new Long(ig2.getUsersBalanceBase()));
   		 		}
   		 	return -new Long(ig1.getUsersBalanceBase()).compareTo(new Long(ig2.getUsersBalanceBase()));
		}
  	 }

	private class TotalInvComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public TotalInvComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getTotalInv()).compareTo(new Long(ig2.getTotalInv()));
   		 		}
   		 	return -new Long(ig1.getTotalInv()).compareTo(new Long(ig2.getTotalInv()));
		}
  	 }

	private class TotalRtnComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public TotalRtnComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getTotalReturn()).compareTo(new Long(ig2.getTotalReturn()));
   		 		}
   		 	return -new Long(ig1.getTotalReturn()).compareTo(new Long(ig2.getTotalReturn()));
		}
  	 }

	private class PrecRtnComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public PrecRtnComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Double(ig1.getReturnRatio()).compareTo(new Double(ig2.getReturnRatio()));
   		 		}
   		 	return -new Double(ig1.getReturnRatio()).compareTo(new Double(ig2.getReturnRatio()));
		}
  	 }

	private class TotalProfitComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public TotalProfitComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Long(ig1.getTotalProfit()).compareTo(new Long(ig2.getTotalProfit()));
   		 		}
   		 	return -new Long(ig1.getTotalProfit()).compareTo(new Long(ig2.getTotalProfit()));
		}
  	 }

	private class PrecHitComparator implements Comparator<InvestmentsGroup> {
		private boolean ascending;
		public PrecHitComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(InvestmentsGroup ig1, InvestmentsGroup ig2) {
   		 		if (ascending){
   		 			return new Double(ig1.getHitsRatio()).compareTo(new Double(ig2.getHitsRatio()));
   		 		}
   		 	return -new Double(ig1.getHitsRatio()).compareTo(new Double(ig2.getHitsRatio()));
		}
  	 }
	
	/**
	 * Get writers list to string
	 * @param list
	 * @return
	 */
	public String getWritersListToString(List list){
		if (list == null || list.size() == 0) {
			if (null != list){
				list.add("-1");
			}
			return "-1";
		}

		String tmp = ConstantsBase.EMPTY_STRING;
		for (int i=0; i<list.size(); i++) {
			tmp += list.get(i)+",";
		}
		return tmp.substring(0,tmp.length()-1);
	}

    /**
     * Check if need to show scroller in the
     * current page.
     * @return
     * 		true - if scroller needed
     */
    public boolean isScrollerNeeded() {
		if (getListSize() > DATA_ROWS) {
			return true;
		}
		return false;
    }

    public int getRowNum() {
		return DATA_ROWS + SUM_ROWS;
	}

	public int getSkinId() {
		return skinId;
	}


	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public String getClosingHour() {
		return closingHour;
	}

	public void setClosingHour(String closingHour) {
		this.closingHour = closingHour;
	}

	public long getShowAdvanced() {
		return showAdvanced;
	}

	public void setShowAdvanced(long showAdvanced) {
		this.showAdvanced = showAdvanced;
	}

	public long getFromHour() {
		return fromHour;
	}

	public void setFromHour(long fromHour) {
		this.fromHour = fromHour;
	}

	public long getFromMin() {
		return fromMin;
	}

	public void setFromMin(long fromMin) {
		this.fromMin = fromMin;
	}

	public long getToHour() {
		return toHour;
	}

	public void setToHour(long toHour) {
		this.toHour = toHour;
	}

	public long getToMin() {
		return toMin;
	}

	public void setToMin(long toMin) {
		this.toMin = toMin;
	}

	/**
	 * @return the opportunityTypeId
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId the opportunityTypeId to set
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}


	public String getSearchBy() {
		return searchBy;
	}


	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}




	public long getSkinBusinessCaseId() {
		return skinBusinessCaseId;
	}


	public void setSkinBusinessCaseId(long skinBusinessCaseId) {
		this.skinBusinessCaseId = skinBusinessCaseId;
	}


	public long getGroupId() {
		return groupId;
	}


	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}


	public long getMarket() {
		return market;
	}


	public void setMarket(long market) {
		this.market = market;
	}


	public long getCountryId() {
		return countryId;
	}


	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}


	public ArrayList<SelectItem> getCountriesSI() {
		return countriesSI;
	}


	public void setCountriesSI(ArrayList<SelectItem> countriesSI) {
		this.countriesSI = countriesSI;
	}

	/**
	 * @return the insertedFrom
	 */
	public List<String> getInsertedFrom() {
		return insertedFrom;
	}

	/**
	 * @param insertedFrom the insertedFrom to set
	 */
	public void setInsertedFrom(List<String> insertedFrom) {
		this.insertedFrom = insertedFrom;
	}

	/**
	 * @return the copyopInvTypeId
	 */
	public int getCopyopInvTypeId() {
		return copyopInvTypeId;
	}


	/**
	 * @param copyopInvTypeId the copyopInvTypeId to set
	 */
	public void setCopyopInvTypeId(int copyopInvTypeId) {
		this.copyopInvTypeId = copyopInvTypeId;
	}


	/**
	 * @return the fromUserId
	 */
	public long getFromUserId() {
		return fromUserId;
	}


	/**
	 * @param fromUserId the fromUserId to set
	 */
	public void setFromUserId(long fromUserId) {
		this.fromUserId = fromUserId;
	}


	public long getScheduledId() {
		return scheduledId;
	}


	public void setScheduledId(long scheduledId) {
		this.scheduledId = scheduledId;
	}
	
}
