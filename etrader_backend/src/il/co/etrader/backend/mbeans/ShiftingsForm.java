package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;


public class ShiftingsForm implements Serializable {

	private static final long serialVersionUID = 6006831103298729688L;

	private ArrayList list;
	private Date from;
	private String marketGroup;
	private String shiftId;

	public ShiftingsForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		/*gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);*/
		from = gc.getTime();

		marketGroup = "0,0";
		shiftId = "";
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		long shiftid;
		try {
			shiftid = Long.valueOf(shiftId);

		} catch (NumberFormatException e) {
			shiftid = 0;
		}
		list=InvestmentsManager.searchShiftings(from,
				Long.valueOf(marketGroup.split(",")[0]),
				Long.valueOf(marketGroup.split(",")[1]),
				shiftid);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setlist(ArrayList list) {
		this.list = list;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public String bNavShiftings() throws SQLException{
		search();
		return Constants.NAV_SHIFTINGS;
	}

	public String getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

	public String getShiftId() {
		return shiftId;
	}

	public void setShiftId(String shiftId) {
		this.shiftId = shiftId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ShiftingsForm ( "
	        + super.toString() + TAB
	        + "list = " + this.list + TAB
	        + "from = " + this.from + TAB
	        + "marketGroup = " + this.marketGroup + TAB
	        + "shiftId = " + this.shiftId + TAB
	        + " )";

	    return retValue;
	}




}
