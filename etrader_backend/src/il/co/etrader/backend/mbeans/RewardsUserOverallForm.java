package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserPlatform;

import il.co.etrader.backend.bl_managers.RewardsManager;
import il.co.etrader.backend.bl_vos.RewardUserTaskExpPoint;
import il.co.etrader.backend.bl_vos.RewardsUserOverall;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;

/**
 * @author EranL
 *
 */
public class RewardsUserOverallForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private RewardsUserOverall rewardsUserOverall;
	private double invTotalTurnover;
	private double invTotalExpPnts;
	private double taskTotalExpPnts;	
	private User user;
	
	public RewardsUserOverallForm() throws SQLException {	 
		//init
		rewardsUserOverall = new RewardsUserOverall();
		invTotalExpPnts = 0;
		invTotalTurnover = 0;
		taskTotalExpPnts = 0;
		user = Utils.getUser();
		//overall table
		RewardUser rewardUser = RewardsManager.getRewardsUser(user.getId());
		rewardsUserOverall.setTierId(rewardUser.getTierId());
		rewardsUserOverall.setExpPntsBalance(rewardUser.getExperiencePoints());
		//Task experience points distribution
		rewardsUserOverall.setRewardUserTaskExpPoints(RewardsManager.getRewardUserTaskExpPoints(user.getId()));
		for (RewardUserTaskExpPoint obj : rewardsUserOverall.getRewardUserTaskExpPoints()) {
			taskTotalExpPnts += obj.getExperiencePointsGained();			
		}	
		//total
		rewardsUserOverall.getRewardUserTaskExpPoints().add(new RewardUserTaskExpPoint(RewardUserPlatform.ROW_TYPE_TOTAL));
		//Investments experience points distribution table:
		rewardsUserOverall.setRewardUserPlatformList(RewardsManager.getRewardUserPlatforms(user.getId()));
		for (RewardUserPlatform obj : rewardsUserOverall.getRewardUserPlatformList()) {
			invTotalTurnover +=  obj.getTurnover(); 
			invTotalExpPnts  +=  obj.getExperiencePoints();
		}
		//total
		rewardsUserOverall.getRewardUserPlatformList().add(new RewardUserPlatform(RewardUserPlatform.ROW_TYPE_TOTAL));
		//Rewards Granted table
		rewardsUserOverall.setRewardUserBenefitList(RewardsManager.getRewardsUserBenefits(user.getId()));
	}
		 
	/**
	 * @return the rewardsUserOverall
	 */
	public RewardsUserOverall getRewardsUserOverall() {
		return rewardsUserOverall;
	}

	/**
	 * @param rewardsUserOverall the rewardsUserOverall to set
	 */
	public void setRewardsUserOverall(RewardsUserOverall rewardsUserOverall) {
		this.rewardsUserOverall = rewardsUserOverall;
	}

	/**
	 * @return the invTotalTurnover
	 */
	public double getInvTotalTurnover() {
		return invTotalTurnover;
	}

	/**
	 * @param invTotalTurnover the invTotalTurnover to set
	 */
	public void setInvTotalTurnover(double invTotalTurnover) {
		this.invTotalTurnover = invTotalTurnover;
	}

	/**
	 * @return the invTotalExpPnts
	 */
	public double getInvTotalExpPnts() {
		return invTotalExpPnts;
	}

	/**
	 * @param invTotalExpPnts the invTotalExpPnts to set
	 */
	public void setInvTotalExpPnts(double invTotalExpPnts) {
		this.invTotalExpPnts = invTotalExpPnts;
	}

	/**
	 * @return the taskTotalExpPnts
	 */
	public double getTaskTotalExpPnts() {
		return taskTotalExpPnts;
	}

	/**
	 * @param taskTotalExpPnts the taskTotalExpPnts to set
	 */
	public void setTaskTotalExpPnts(double taskTotalExpPnts) {
		this.taskTotalExpPnts = taskTotalExpPnts;
	}
	
}
