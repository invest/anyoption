package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.StaticLPManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.util.CommonUtil;

public class StaticCombinationForm implements Serializable{

	private static final long serialVersionUID = 4180765569917492106L;
	private static final Logger logger = Logger.getLogger(StaticCombinationForm.class);

	private ArrayList<MarketingCombination> list;
	private MarketingCombination combination;

	private long skinId;
	private long landingPage;
	private long combId;
	private long fetchCount = 1000;
	private ArrayList<SelectItem> listLandingPageNames;
	private long pathId;

	public StaticCombinationForm() throws SQLException{
//		showUrl = false;
		combId = 0;
		listLandingPageNames = new ArrayList<SelectItem>();
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = StaticLPManager.getStaticCombination(combId, skinId, landingPage, writerIdForSkin, fetchCount, pathId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList<SelectItem> getStaticLP() throws SQLException {
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }
		
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),"All"));		
		
		if(writerIdForSkin>0){
			list.addAll(StaticLPManager.getStaticLandingPagesByWriterSkinIdSI(writerIdForSkin));
		}else {
			list.addAll(StaticLPManager.getStaticLandingPages());
		}
		
		return list;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to insert new combination
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		combination = new MarketingCombination();
		combination.setWriterId(AdminManager.getWriterId());
		combination.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_COMBINATION;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the landingPage
	 */
	public long getLandingPage() {
		return landingPage;
	}

	/**
	 * @param landingPage the landingPage to set
	 */
	public void setLandingPage(long landingPage) {
		this.landingPage = landingPage;
	}

	public String getCombId() {
		if (0 == combId) {
			return "";
		}
		return String.valueOf(combId);
	}

	public void setCombId(String combId) {
		long l = 0;
		try {
			l = Long.valueOf(combId);
		} catch (Exception e) {
			// not a number
		}finally {
			this.combId = l;
		}

	}

	/**
	 * @return the list
	 */
	public ArrayList<MarketingCombination> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<MarketingCombination> list) {
		this.list = list;
	}

    /**
     * @return the fetchCount
     */
    public long getFetchCount() {
        return fetchCount;
    }

    /**
     * @param fetchCount the fetchCount to set
     */
    public void setFetchCount(long fetchCount) {
        this.fetchCount = fetchCount;
    }
    
    /**
	 * Get Static Lp Names SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getStaticLpNamesSI() {
		listLandingPageNames = new ArrayList<SelectItem>();
		try {
			logger.log(Level.DEBUG,"About to get list of static landing pages.");
			listLandingPageNames.addAll(StaticLPManager.getStaticLpNamesSI(ConstantsBase.MARKETING_LANDING_PAGE_STATIC, pathId));
		} catch (SQLException e) {
			logger.error("Problem to get list of static landing pages.", e);
		}
		return listLandingPageNames;
	}

	/**
	 * @return the listLandingPageNames
	 */
	public ArrayList<SelectItem> getListLandingPageNames() {
		return listLandingPageNames;
	}

	/**
	 * @param listLandingPageNames the listLandingPageNames to set
	 */
	public void setListLandingPageNames(ArrayList<SelectItem> listLandingPageNames) {
		this.listLandingPageNames = listLandingPageNames;
	}

	/**
	 * @return the pathId
	 */
	public long getPathId() {
		return pathId;
	}

	/**
	 * @param pathId the pathId to set
	 */
	public void setPathId(long pathId) {
		this.pathId = pathId;
	}
}
