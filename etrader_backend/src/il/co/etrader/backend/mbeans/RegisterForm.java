package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.component.UIColumn;
import javax.faces.component.UICommand;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class RegisterForm implements Serializable {

	/**
	 *  Backend Advanced user search
	 */
	private static final long serialVersionUID = -6640282100813692929L;

	private static final Logger logger = Logger.getLogger(RegisterForm.class);

	private String userName;
	private String oldPassword;
	private String password;
	private String password2;
	private Long currencyId;
	private String firstName;
	private String lastName;
	private String street;
	private String streetNo;
	private long cityId;
	private String zipCode;
	private String email;
	private String birthYear;
	private String birthMonth;
	private String birthDay;
	private boolean contactByEmail;
	private String mobilePhoneSuffix;
	private String mobilePhonePrefix;
	private String mobilePhone;
	private String landLinePhoneSuffix;
	private String landLinePhonePrefix;
	private String landLinePhone;
	private String gender;
	private String idNum;
	private boolean terms;
	protected int isContactByEmail;
	private Long contactId;
	protected Long combId;
	protected String classId;
	protected String comments;
	protected String isActive;
	protected String limitId;
	private String cityName;
	private ArrayList<User> usersList;
	private boolean sendPassword;
	private Long userId;
	private boolean showTable;
	private String phoneSearch;
	private HtmlDataTable datatable;
	private ArrayList<User> userUpdates ;
	private ArrayList<String> skinId;
	private ArrayList<String> countries;
    
	// Paging.
	private int totalRows;
    private int firstRow;
    private int rowsPerPage;
    private int totalPages;
    private int pageRange;
    private Integer[] pages;
    private int currentPage;


	public RegisterForm() throws SQLException {
		rowsPerPage = 10;
	    pageRange = 10;
		userUpdates = new ArrayList<User>();
		usersList =  new ArrayList<User>();
		searchUsers();
	}

	public String changePass() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();

		UserBase user = (UserBase) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long writerId = Utils.getWriter().getWriter().getId();
		
		String password = user.getPassword();
		String newPassword = this.getPassword();
		if(user.getSkinId() == Skin.SKIN_ETRADER){
			password = password.toUpperCase();
			newPassword = newPassword.toUpperCase();
		}
		try {
			UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_PASSWORD, 
					AESUtil.encrypt(password), AESUtil.encrypt(newPassword));
		} catch (CryptoException ce) {
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
		
		UsersManager.changePass(this, user);

		return Constants.NAV_EMPTY_SUBTABS;
	}
	
	


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @param password2
	 *            the password2 to set
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public ArrayList<String> getCountries() {
		return countries;
	}

	public void setCountries(ArrayList<String> countries) {
		this.countries = countries;
	}

	public ArrayList<String> getSkinId() {
		return skinId;
	}

	public void setSkinId(ArrayList<String> skinId) {
		this.skinId = skinId;
	}
	
	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId
	 *            the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityId
	 */

	public long getCityId() {

		return cityId;
	}

	public void setCityId(long id) {

		this.cityId = id;

	}

	public String getCityName() {

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String n = (String) a.getCities().get(String.valueOf(cityId));
		if (n == null)
			return cityName;
		return n;
	}

	public void setCityName(String cityName) throws SQLException {
		cityId = AdminManagerBase.getCityIdByName(cityName);
		this.cityName = cityName;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}

	/**
	 * @param birthYear
	 *            the birthYear to set
	 */
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	/**
	 * @return the birthMonth
	 */
	public String getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @param birthMonth
	 *            the birthMonth to set
	 */
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	/**
	 * @return the birthDay
	 */
	public String getBirthDay() {
		return birthDay;
	}

	/**
	 * @param birthDay
	 *            the birthDay to set
	 */
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	/**
	 * @return the contactByEmail
	 */
	public boolean isContactByEmail() {
		return contactByEmail;
	}

	/**
	 * @return the isContactByEmail
	 */
	public int getIsContactByEmail() {
		return isContactByEmail;
	}

	/**
	 * @param isContactByEmail
	 *            the isContactByEmail to set
	 */
	public void setIsContactByEmail(int isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}

	/**
	 * @return the mobilePhonePrefix
	 */
	public String getMobilePhonePrefix() {
		return mobilePhonePrefix;
	}

	/**
	 * @param mobilePhonePrefix
	 *            the mobilePhonePrefix to set
	 */
	public void setMobilePhonePrefix(String mobilePhonePrefix) {
		this.mobilePhonePrefix = mobilePhonePrefix;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *            the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the landLinePhonePrefix
	 */
	public String getLandLinePhonePrefix() {
		return landLinePhonePrefix;
	}

	/**
	 * @param landLinePhonePrefix
	 *            the landLinePhonePrefix to set
	 */
	public void setLandLinePhonePrefix(String landLinePhonePrefix) {
		this.landLinePhonePrefix = landLinePhonePrefix;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone
	 *            the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the idNum
	 */
	public String getIdNum() {
		return idNum;
	}

	/**
	 * @param idNum
	 *            the idNum to set
	 */
	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}

	/**
	 * @return the terms
	 */
	public boolean isTerms() {
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(boolean terms) {
		this.terms = terms;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public ArrayList getUsersList() throws SQLException {

		// if (usersList==null)
		// usersList=UsersManager.searchUser(this);

		return usersList;
	}

	public void setUsersList(ArrayList usersList) {
		this.usersList = usersList;
	}

	public int getListSize() throws SQLException {

		// if (usersList==null)
		// getUsersList();

		return usersList.size();
	}

	public void setShowTable(boolean f) {
		showTable = f;
	}

	public boolean getShowTable() {
		return showTable;
	}


	public void UpdateChanges() throws SQLException{
		
		for(User u:userUpdates){
			
			if(u.getId()!=0){
				
				UsersManager.updateUserContactByEmail(u);
			}
			
			if(u.getContactId()!=0){
				Contact contact = new Contact();
				contact.setId(u.getContactId());
				contact.setContactByEmail(u.getContactByEmail());
				ContactsManager.updateIsContactByEmail(contact);
			}
		}
		
	}
	
	public void changed(ValueChangeEvent e){
			
		HtmlSelectBooleanCheckbox cb = (HtmlSelectBooleanCheckbox) e.getSource(); 
		UIColumn c = (UIColumn) cb.getParent();
		HtmlDataTable table = (HtmlDataTable) c.getParent();
		User u =  (User) table.getRowData();
	    u.setContactByEmail((Boolean)e.getNewValue());
	    userUpdates.add(u);
	}
	
	
	public String searchUsers() throws SQLException {
		 // Load list and totalCount.
		 initList();
	     return null;
	}
	
    public String initList() {
        // Load list and totalCount.
        loadData();

        // Set currentPage, totalPages and pages.
        totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        if (totalPages == 0) {
        	currentPage = 0;
        } else {
        	currentPage = (totalRows / rowsPerPage) - ((totalRows - firstRow) / rowsPerPage) + 1;
        }
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
        
        return null;
    }
	
	public void loadData(){
        
        try {
        	if(CommonUtil.isParameterEmptyOrNull(userName) && CommonUtil.isParameterEmptyOrNull(firstName)
        			&& CommonUtil.isParameterEmptyOrNull(lastName) && CommonUtil.isParameterEmptyOrNull(idNum)
        			&& CommonUtil.isParameterEmptyOrNull(email) && (userId == null)
        			&& CommonUtil.isParameterEmptyOrNull(phoneSearch) && CommonUtil.isParameterEmptyOrNull(mobilePhone)  && (contactId == null))  {
        		setShowTable(false);
        		usersList.clear();
        		totalPages=0;
        		currentPage=0;
        		totalRows=0;
        		pages = new Integer[pageRange];
        		return;
        	}
        	 setShowTable(true);	
        	 usersList = UsersManager.searchUserAndContact(this, Utils.getWriter().getWriter().getId());
             totalRows = UsersManager.searchUserAndContactCount(this, Utils.getWriter().getWriter().getId());
        } catch (SQLException e) {
            logger.error("Can't load Advanced Search User list  :" + e.getMessage());
        }
    }       

   public void pageFirst() {
	   this.firstRow = 0;
       initList();
    }

    public void pageNext() {
        page(firstRow + rowsPerPage);
    }

    public void pagePrevious() {
        page(firstRow - rowsPerPage);
    }

    public void pageLast() {
        page(totalRows - ((totalRows % rowsPerPage != 0) ? totalRows % rowsPerPage : rowsPerPage));
    }

    public void page(ActionEvent event) {
        page((((Integer) ((UICommand) event.getComponent()).getValue()) - 1) * rowsPerPage);
    }

    private void page(int firstRow) {
    	this.firstRow = firstRow;
        initList(); // Load requested page.
    }
    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageRange() {
        return pageRange;
    }

    public void setPageRange(int pageRange) {
        this.pageRange = pageRange;
    }

    public Integer[] getPages() {
        return pages;
    }

    public void setPages(Integer[] pages) {
        this.pages = pages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
    
	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public boolean isSendPassword() {
		return sendPassword;
	}

	public void setSendPassword(boolean sendPassword) {
		this.sendPassword = sendPassword;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = " \n ";

		String retValue = "";

		retValue = "RegisterForm ( " + super.toString() + TAB + "userName = "
				+ this.userName + TAB + "oldPassword = " + "*****"
				+ TAB + "password = " + "*****" + TAB + "password2 = "
				+ "*****" + TAB + "currencyId = " + this.currencyId
				+ TAB + "firstName = " + this.firstName + TAB + "lastName = "
				+ this.lastName + TAB + "street = " + this.street + TAB
				+ "streetNo = " + this.streetNo + TAB + "cityId = "
				+ this.cityId + TAB + "zipCode = " + this.zipCode + TAB
				+ "email = " + this.email + TAB + "birthYear = "
				+ this.birthYear + TAB + "birthMonth = " + this.birthMonth
				+ TAB + "birthDay = " + this.birthDay + TAB
				+ "contactByEmail = " + this.contactByEmail + TAB
				+ "mobilePhonePrefix = " + this.mobilePhonePrefix + TAB
				+ "mobilePhone = " + this.mobilePhone + TAB
				+ "landLinePhonePrefix = " + this.landLinePhonePrefix + TAB
				+ "landLinePhone = " + this.landLinePhone + TAB + "gender = "
				+ this.gender + TAB + "idNum = " + this.idNum + TAB
				+ "terms = " + this.terms + TAB + "cityName = " + this.cityName
				+ TAB + "usersList = " + this.usersList + TAB
				+ "sendPassword = " + this.sendPassword + TAB + " )";

		return retValue;
	}

	public String getIsActive() {
		return isActive;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public ArrayList getLimits() throws SQLException {
		return AdminManager.getLimits();
	}

	public long getCombId() {
		return combId;
	}

	public void setCombId(long combId) {
		this.combId = combId;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getLandLinePhoneSuffix() {
		return landLinePhoneSuffix;
	}

	public void setLandLinePhoneSuffix(String landLinePhoneSuffix) {
		this.landLinePhoneSuffix = landLinePhoneSuffix;
	}

	public String getMobilePhoneSuffix() {
		return mobilePhoneSuffix;
	}

	/**
	 * @return the isContactByEmail
	 */
	public boolean getContactByEmail() {
		return (isContactByEmail == 1);
	}

	public void setContactByEmail(boolean c) {
		if (c == false)
			isContactByEmail = 0;
		else
			isContactByEmail = 1;
	}

	public String getLimitId() {
		return limitId;
	}

	public void setLimitId(String limitId) {
		this.limitId = limitId;
	}

	public void setMobilePhoneSuffix(String mobilePhoneSuffix) {
		this.mobilePhoneSuffix = mobilePhoneSuffix;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	
	public HtmlDataTable getDatatable() {
        return datatable;
    }

    public void setDatatable(HtmlDataTable datatable) {
        this.datatable = datatable;
    }
	public String getPhoneSearch() {
		return phoneSearch;
	}

	public void setPhoneSearch(String phoneSearch) {
		this.phoneSearch = phoneSearch;
	}
	
	
}