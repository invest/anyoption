package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;


public class ReverseWithdrawForm extends FormBase implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 473395962592117259L;

	private static final Logger logger = Logger.getLogger(ReverseWithdrawForm.class);

	private Date from ;
	private Date to;
	private ArrayList transactionsList;


	public ReverseWithdrawForm() throws SQLException{
		updateTransactionsList();
	}

	public ArrayList getTransactionsList() {
		return transactionsList;
	}

	public int getListSize() {
		return transactionsList.size();
	}

	public void setTransactionsList(ArrayList transactionsList) {
		this.transactionsList = transactionsList;
	}

	public void updateTransactionsList() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user= (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		transactionsList=TransactionsManager.getPendingWithdraws(user.getId());
		CommonUtil.setTablesToFirstPage();
	}



	public String reverseWithdraw() throws SQLException{
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		TransactionsManager.reverseWithdraw(transactionsList,user.getId());

		if (user.isSupportSelected() || user.isRetentionMSelected() || user.isRetentionSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		updateTransactionsList();
		return Utils.navAfterUserOperation();
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ReverseWithdrawForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "transactionsList = " + this.transactionsList + TAB
	        + " )";

	    return retValue;
	}





}
