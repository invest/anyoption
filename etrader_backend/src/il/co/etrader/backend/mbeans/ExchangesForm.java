package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Exchange;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

public class ExchangesForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6983508679519007229L;
	private ArrayList list;
    private Exchange exchange;

	public ExchangesForm() throws SQLException{
		search();
	}


	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		list=InvestmentsManager.searchExchanges();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public String update() throws SQLException{
		boolean res=InvestmentsManager.updateExchange(exchange);
		if (res==false)
			return null;
		search();
		return Constants.NAV_EXCHANGES;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ExchangesForm ( "
	        + super.toString() + TAB
	        + "list = " + this.list + TAB
	        + "exchange = " + this.exchange + TAB
	        + " )";

	    return retValue;
	}
}
