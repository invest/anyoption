package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import com.anyoption.common.managers.MarketingAffiliatesManagerBase;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class AffilatesForm implements Serializable {

	private static final long serialVersionUID = 8847526067050595968L;

	private String affName;
	private String affKey;
	private ArrayList<MarketingAffilate> list;
	private MarketingAffilate affilate;
	private SelectItem pixel;
	private long affManagerId;

	/**
	 * @throws SQLException
	 */
	public AffilatesForm() throws SQLException{
		affName = null;
		affKey = null;
		affilate = null;
		list = null;
		pixel = null;
		affManagerId = MarketingAffiliatesManagerBase.AFFILIATE_MANAGER_ALL;
		updateList();
	}

	/**
	 * Update affiliates list
	 * @return
	 * @throws SQLException
	 */
	public String updateList() throws SQLException{
		list = MarketingManager.getMarketingAffilates(affName, affKey, affManagerId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert location
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{
		boolean isUpdate;
		if (affilate.getId() == 0){
			isUpdate = false;
		}
		else{
			isUpdate = true;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		affilate.setWriterId(Utils.getWriter().getWriter().getId());
		boolean res = MarketingManager.insertUpdateAffiliate(affilate);
		if (res == false) {
			return null;
		}
		if (isUpdate) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success",null),null);
			context.addMessage(null, fm);
            
		}
		else {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success",null),null);
			context.addMessage(null, fm);
            
		}
		updateList();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		session.setAttribute(Constants.UPDATE_AFFILIATE_LIST, Constants.ADDED_AFFILIATE);
		session.setAttribute(Constants.UPDATE_AFFILIATE_NAME_KEY_LIST, Constants.ADDED_AFFILIATE);
		return Constants.NAV_MARKETING_AFFILATES;
	}

	/**
	 * Delete pixel
	 * @return
	 * @throws SQLException
	 */
	public String deletePixel() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		MarketingManager.deletePixelByAffiliateId(affilate.getId(), 0, ((Long)pixel.getValue()).longValue());
		ArrayList<SelectItem> pixelsList = new ArrayList<SelectItem>();
		for (SelectItem si : affilate.getPixelsSi()) {
			if (((Long)si.getValue()) != ((Long)pixel.getValue())) {
				pixelsList.add(si);
			}
		}
		affilate.setPixelsSi(pixelsList);
		context.renderResponse();
		return null;
	}

	/**
	 * @return
	 */
	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit location
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_AFFILATE;
	}

	/**
	 * Navigate to insert new location
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		affilate = new MarketingAffilate();
		affilate.setWriterId(Utils.getWriter().getWriter().getId());
		affilate.setWriterName(Utils.getWriter().getWriter().getUserName());
		affilate.setPriorityId(String.valueOf(ConstantsBase.PRIORITY_LOW));
		return Constants.NAV_MARKETING_AFFILATE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the affName
	 */
	public String getAffName() {
		return affName;
	}

	/**
	 * @param affName the affName to set
	 */
	public void setAffName(String affName) {
		this.affName = affName;
	}
	
	public String getAffKey() {
		return affKey;
	}

	
	public void setAffKey(String affKey) {
		this.affKey = affKey;
	}

	/**
	 * @return the aff
	 */
	public MarketingAffilate getAffilate() {
		return affilate;
	}

	/**
	 * @param affilate the affilate to set
	 */
	public void setAffilate(MarketingAffilate affilate) {
		this.affilate = affilate;
	}

	/**
	 * @return the pixel
	 */
	public SelectItem getPixel() {
		return pixel;
	}

	/**
	 * @param pixel the pixel to set
	 */
	public void setPixel(SelectItem pixel) {
		this.pixel = pixel;
	}

	/**
	 * @return the affManagerId
	 */
	public long getAffManagerId() {
		return affManagerId;
	}

	/**
	 * @param affManagerId the affManagerId to set
	 */
	public void setAffManagerId(long affManagerId) {
		this.affManagerId = affManagerId;
	}

}
