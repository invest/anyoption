package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Transaction;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class BackgroundForm implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1712193480023655664L;
	private User user;
	private String background;
	private String tip;
	private String converstionDate;
	private String convertionAmount;
	private String convertionBy;
	private String marketingCampaign;
	private long tag;
	private ArrayList<SelectItem> tagList;

	public BackgroundForm() throws Exception {
		user = Utils.getUser();
		tag = user.getTag();
		tagList  = new ArrayList<SelectItem>();
		tagList.add(new SelectItem(new Long(0), ConstantsBase.EMPTY_STRING));
		tagList.add(new SelectItem(new Long(1), CommonUtil.getMessage("menu.retention.user.type.level.one", null)));
		tagList.add(new SelectItem(new Long(2), CommonUtil.getMessage("menu.retention.user.type.level.two", null)));
		tagList.add(new SelectItem(new Long(3), CommonUtil.getMessage("menu.retention.user.type.level.three", null)));
		tagList.add(new SelectItem(new Long(4), CommonUtil.getMessage("menu.retention.user.type.level.four", null)));
		tagList.add(new SelectItem(new Long(5), CommonUtil.getMessage("menu.retention.user.type.level.five", null)));

		if (user.getFirstDepositId() > 0) {
			Transaction t = TransactionsManager.getTransaction(user.getFirstDepositId());
			if (null != t) {
				converstionDate = t.getTimeCreated().toString();
				convertionAmount =  CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());;
				convertionBy = ApplicationData.getWriterName(t.getWriterId());
				MarketingCampaign mc = MarketingManager.getMarketingCampaignByUserId(user.getId());
				marketingCampaign = mc.getName();
			}
		}

		if (CommonUtil.isParameterEmptyOrNull(user.getBackground())) {
			background = ConstantsBase.EMPTY_STRING;
		} else {
			background = user.getBackground();
		}

		if (CommonUtil.isParameterEmptyOrNull(user.getTip())) {
			tip = ConstantsBase.EMPTY_STRING;
		} else {
			tip = user.getTip();
		}
	}

	public String update() throws Exception {
		FacesContext context=FacesContext.getCurrentInstance();
		UsersManager.updateUserBackgroundAndTip(user.getId(), background, tip, tag);
		Utils.getUser().setBackground(background);
		Utils.getUser().setTip(tip);
		Utils.getUser().setTag(tag);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);
		return null;
	}

	/**
	 * @return the background
	 */
	public String getBackground() {
		return background;
	}
	/**
	 * @param background the background to set
	 */
	public void setBackground(String background) {
		this.background = background;
	}
	/**
	 * @return the converstionDate
	 */
	public String getConverstionDate() {
		return converstionDate;
	}
	/**
	 * @param converstionDate the converstionDate to set
	 */
	public void setConverstionDate(String converstionDate) {
		this.converstionDate = converstionDate;
	}
	/**
	 * @return the convertionAmount
	 */
	public String getConvertionAmount() {
		return convertionAmount;
	}
	/**
	 * @param convertionAmount the convertionAmount to set
	 */
	public void setConvertionAmount(String convertionAmount) {
		this.convertionAmount = convertionAmount;
	}
	/**
	 * @return the convertionBy
	 */
	public String getConvertionBy() {
		return convertionBy;
	}
	/**
	 * @param convertionBy the convertionBy to set
	 */
	public void setConvertionBy(String convertionBy) {
		this.convertionBy = convertionBy;
	}
	/**
	 * @return the marketingCampaign
	 */
	public String getMarketingCampaign() {
		return marketingCampaign;
	}
	/**
	 * @param marketingCampaign the marketingCampaign to set
	 */
	public void setMarketingCampaign(String marketingCampaign) {
		this.marketingCampaign = marketingCampaign;
	}
	/**
	 * @return the tip
	 */
	public String getTip() {
		return tip;
	}
	/**
	 * @param tip the tip to set
	 */
	public void setTip(String tip) {
		this.tip = tip;
	}

	/**
	 * @return the tag
	 */
	public long getTag() {
		return tag;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setTag(long tag) {
		this.tag = tag;
	}

	/**
	 * @return the tagList
	 */
	public ArrayList<SelectItem> getTagList() {
		return tagList;
	}

	/**
	 * @param tagList the tagList to set
	 */
	public void setTagList(ArrayList<SelectItem> tagList) {
		this.tagList = tagList;
	}
}
