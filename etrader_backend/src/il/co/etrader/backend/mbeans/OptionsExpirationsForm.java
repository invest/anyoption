package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.BackendLevelsCache;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.OptionExpiration;
import il.co.etrader.util.CommonUtil;

public class OptionsExpirationsForm implements Serializable {

    private static final long serialVersionUID = 4696778705954943952L;

    private static final Logger log = Logger.getLogger(OptionExpiration.class);

    private ArrayList list;
	private OptionExpiration optionExpiration;
	private Long marketId;
	private String year;
	private String month;


	public OptionsExpirationsForm() throws SQLException{
		marketId = new Long(0);
		GregorianCalendar gc = new GregorianCalendar();
		year = String.valueOf(gc.get(GregorianCalendar.YEAR));
		month = String.valueOf(gc.get(GregorianCalendar.MONTH)+1);
		if (month.length() == 1) {
			month="0"+month;
		}
		search();
	}

	public ArrayList getYears() {
		ArrayList<SelectItem> years = new ArrayList<SelectItem>();
		GregorianCalendar gc = new GregorianCalendar();
		for (int i = gc.get(GregorianCalendar.YEAR) + 20; i >= gc.get(GregorianCalendar.YEAR) - 20; i--) {
			years.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
		}
		return years;
	}


	public String navNew() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		optionExpiration = new OptionExpiration();
		optionExpiration.setExpiryDate(new Date());
		optionExpiration.setTimeCreated(new Date());
		optionExpiration.setWriterId(AdminManager.getWriterId());
		optionExpiration.setWriterName(context.getExternalContext().getRemoteUser());
		optionExpiration.setActive(true);
        optionExpiration.setExpiryDateEnd(new Date());
		return Constants.NAV_OPTION_EXPIRATION;
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		list=InvestmentsManager.searchOptionsExpiration(marketId,Integer.valueOf(month),Integer.valueOf(year));
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public String update() throws SQLException{

		optionExpiration.setWriterId(AdminManager.getWriterId());
		boolean res = InvestmentsManager.updateInsertOptionExpiration(optionExpiration);
		if (res == false) { //no update/insert
			return null;
        }
        log.debug("checking if need to send msg to service");
        if (optionExpiration.getExpiryDate().before(new Date())) {
            //send to service
            log.debug("sending msg to service");
            FacesContext context = FacesContext.getCurrentInstance();
            BackendLevelsCache levelsCache = getLevelsCache(context);
            if (!levelsCache.notifyForOptionExpirationChange(optionExpiration.getMarketId())) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("expration.service.notify.failed", null, Utils.getWriterLocale(context)),null);
                context.addMessage(null, fm);
                
                log.debug("got error from service");
                return null;
            }
            log.debug("msg to service was good");
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("expration.service.notify.success", null, Utils.getWriterLocale(context)),null);
            context.addMessage(null, fm);
            
        }
		search();
		return Constants.NAV_OPTIONS_EXPIRATIONS;
	}

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public OptionExpiration getOptionExpiration() {
		return optionExpiration;
	}

	public void setOptionExpiration(OptionExpiration optionExpiration) {
		this.optionExpiration = optionExpiration;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

    public ArrayList getLettersSI() {
        ArrayList<SelectItem> si = new ArrayList<SelectItem>();
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        for (char c: alphabet) {
            String value = String.valueOf(c);
            si.add(new SelectItem(value, value));
        }
        return si;
    }

    /**
     * get the levelcache from the context
     * @param context
     * @return levelcache
     */
    private BackendLevelsCache getLevelsCache(FacesContext context) {
        return (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
    }
}
