package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author EranL
 *
 */
public class RiskUsersFilesForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date from;
	private Date to;
	private int skinId;
	private String userName;
	private String userId;
	private long classId;
	private String reasonId;
	private ArrayList<RiskUsersFiles> riskUsersFilesList;
	private boolean disableSelection;
	//many list box
    private List<String> statuses;

	public RiskUsersFilesForm()	throws SQLException, CryptoException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
								NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		disableSelection = false;
		statuses = new ArrayList<String>();

		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		userId = request.getParameter("riskUserId");
		userName = request.getParameter("riskUserName");
		statuses.add(String.valueOf(IssuesManagerBase.FILES_RISK_STATUS_NEED_APPROVE));
		//selecting user from risk issues screen
		if (!CommonUtil.isParameterEmptyOrNull(userId)){
			statuses.clear();
			statuses.add("0");
			disableSelection = true;
		}
		search();
	}

	public String search()	throws SQLException, CryptoException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
							NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		riskUsersFilesList = IssuesManager.getRiskUsersFiles(getSkinFilter(), userName, userId, classId, reasonId,  getStatusToString());
		return null;
	}

	/**
	 * Get statuses list to string
	 * @param list
	 * @return
	 */
	public String getStatusToString(){
		if (statuses == null || statuses.size() == 0) {
			if (null != statuses){
				statuses.add("0");
			}
			return "0";
		}

		String tmp = ConstantsBase.EMPTY_STRING;
		for (int i=0; i<statuses.size(); i++) {
			tmp += statuses.get(i)+",";
		}
		return tmp.substring(0,tmp.length()-1);
	}

	/**
	 * @return
	 */
	public String getSkinFilter() {
		if (skinId == Skin.SKINS_ALL_VALUE) {
			return Utils.getWriter().getSkinsToString();
		}
		else {
			return String.valueOf(skinId);
		}
	}

	/**
	 * @return the classId
	 */
	public long getClassId() {
		return classId;
	}

	/**
	 * @param classId the classId to set
	 */
	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the reasonId
	 */
	public String getReasonId() {
		return reasonId;
	}

	/**
	 * @param reasonId the reasonId to set
	 */
	public void setReasonId(String reasonId) {
		this.reasonId = reasonId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the statuses
	 */
	public List<String> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the riskUsersFilesList
	 */
	public ArrayList<RiskUsersFiles> getRiskUsersFilesList() {
		return riskUsersFilesList;
	}

	/**
	 * @param riskUsersFilesList the riskUsersFilesList to set
	 */
	public void setRiskUsersFilesList(ArrayList<RiskUsersFiles> riskUsersFilesList) {
		this.riskUsersFilesList = riskUsersFilesList;
	}

	public int getListSize(){
		if (riskUsersFilesList == null){
			return 0;
		}
		return riskUsersFilesList.size();
	}

	/**
	 * @return the disableSelection
	 */
	public boolean isDisableSelection() {
		return disableSelection;
	}

	/**
	 * @param disableSelection the disableSelection to set
	 */
	public void setDisableSelection(boolean disableSelection) {
		this.disableSelection = disableSelection;
	}

}
