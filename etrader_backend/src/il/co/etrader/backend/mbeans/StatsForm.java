package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.StatsResult;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

public class StatsForm implements Serializable {

	private static final long serialVersionUID = -1419631029518567910L;

	private static final Logger logger = Logger.getLogger(StatsForm.class);

	private List<StatsResult> assetProfitList;
	private List<StatsResult> assetSuccessList;
	private List<StatsResult> assetGroupProfitList;
	private List<StatsResult> assetGroupSuccessList;
	private List<StatsResult> timeProfitList;
	private List<StatsResult> timeSuccessList;
	private List<StatsResult> oneTouchProfitList;
	private List<StatsResult> oneTouchSuccessList;
	private List<StatsResult> profitPeakList;
	private List<StatsResult> peaksAssetProfitList;

	private Date peakRangeFrom = new Date(0);
	private Date peakRangeTo = new Date();


	public String getUpdateAssetProfitList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		assetProfitList = InvestmentsManager.getStatsAssetProfit(user.getId(), null, null, false);
		return null;
	}

	public String getUpdateAssetSuccessList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		assetSuccessList = InvestmentsManager.getStatsAssetSuccess(user.getId());
		return null;
	}

	public String getUpdateAssetGroupProfitList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		assetGroupProfitList = InvestmentsManager.getStatsAssetGroupProfit(user.getId());
		return null;
	}

	public String getUpdateAssetGroupSuccessList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		assetGroupSuccessList = InvestmentsManager.getStatsAssetGroupSuccess(user.getId());
		return null;
	}

	public String getUpdateTimeProfitList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		timeProfitList = InvestmentsManager.getStatsTimeProfit(user.getId());
		return null;
	}

	public String getUpdateTimeSuccessList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		timeSuccessList = InvestmentsManager.getStatsTimeSuccess(user.getId());
		return null;
	}

	public String getUpdateOneTouchProfitList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		oneTouchProfitList = InvestmentsManager.getStats1TouchProfit(user.getId());
		return null;
	}

	public String getUpdateOneTouchSuccessList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		oneTouchSuccessList = InvestmentsManager.getStats1TouchSuccess(user.getId());
		return null;
	}

	public String getUpdateProfitPeakList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		profitPeakList = findPeaks(user.getId());
		return null;
	}

	public String getUpdatePeaksAssetProfitList() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		String fromParam = request.getParameter("from");
		String toParam = request.getParameter("to");
		if (null == fromParam || null == toParam) {
			return null;
		}
		peakRangeFrom = CommonUtil.getStartOfDay(dateStringToDate(fromParam));
		peakRangeTo = CommonUtil.getEndOfDay(dateStringToDate(toParam));
		peaksAssetProfitList = InvestmentsManager.getStatsAssetProfit(user.getId(), peakRangeFrom, peakRangeTo, true);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 *  dd.MM.yyyy -> Date
	 */
	private static Date dateStringToDate(String dateFormat) {
		if (null == dateFormat) {
			return null;
		}
		Date res = null;
		try {
			Pattern regex = Pattern.compile("(\\d{1,2})\\.(\\d{1,2})\\.(\\d{2,4})",
				Pattern.CANON_EQ);
			Matcher matcher = regex.matcher(dateFormat);
			if (matcher.find()) {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.set(Integer.valueOf(matcher.group(3)),
						Integer.valueOf(matcher.group(2))-1, Integer.valueOf(matcher.group(1)));
				res = calendar.getTime();
			}
		} catch (PatternSyntaxException e) {
		}
		return res;
	}

	public static Logger getLogger() {
		return logger;
	}

	/**
	 * returns the 10 first peaks for the user
	 */
	private List<StatsResult> findPeaks(long userId) throws SQLException {
		// get list of day and its profit for each day with any setteled_time on it
		List<StatsResult> wlList = InvestmentsManager.getStatsDayAndProfit(userId);
		if (null == wlList) {
			return null;
		}
		long wl = 0;
		long invNum = 0;
		// calculate and set for each day the profit and the investments number from registration till this day
		for (int i=0; i<wlList.size(); i++) {
			StatsResult current = wlList.get(i);
			wl += current.getCustomerProfit();
			invNum += current.getInvestmentsNumber();
			current.setTotalCustomerProfit(wl);
			current.setTotalInvestmentsNumber(invNum);
		}
		// find for each per of days the profit and investments number in the range
		List<StatsResult> wl2Days = new ArrayList<StatsResult>((int)Math.pow(wlList.size(), 2));
		for (int i=0; i<wlList.size(); i++) {
			// for each day, checking all the days from itself (for one day range) and after it
			for (int j=i; j<wlList.size(); j++) {
				StatsResult from = wlList.get(i);
				StatsResult to = wlList.get(j);
				// Calculate the w/l in the range. need to reduce the "from" value, to calculate
				// the value from the begining of the day
				long wlRange = to.getTotalCustomerProfit() - (from.getTotalCustomerProfit() - from.getCustomerProfit());
				// Calculate the investments number in the range. need to reduce the "from" value, to calculate
				// the value from the begining of the day
				long invNumRange = to.getTotalInvestmentsNumber() - (from.getTotalInvestmentsNumber() - from.getInvestmentsNumber());
				// If the w/l > 0 (there is a profit for the user) and at least 3 investments,
				// save the match in the list
				if (wlRange > 0 && invNumRange > 3) {
					StatsResult match = new StatsResult();
					match.setDate1(from.getDate1());
					match.setDate2(to.getDate1());
					match.setTotalCustomerProfit(wlRange);
					match.setTotalInvestmentsNumber(invNumRange);
					wl2Days.add(match);
				}
			}
		}
		// Sort the matches list by profit - bigger is first
		Collections.sort(wl2Days, new Comparator<StatsResult>() {
			public int compare(StatsResult s1, StatsResult s2) {
				return Long.valueOf(s2.getTotalCustomerProfit()).compareTo(s1.getTotalCustomerProfit());
			}
		});

		List<StatsResult> res = new ArrayList<StatsResult>();
		if (null == wl2Days || 0 == wl2Days.size()) {
			return null;
		}
		// take the first - the bigger
		res.add(wl2Days.get(0));
		/*
		 * for each pair, check if part of its range is part of range of peak with largest profit.
		 * If so, delete that range.
		 * o/w add it to peaks list
		 */
		for (int i=1; i<wl2Days.size(); i++) {
			Date currentDateFrom = wl2Days.get(i).getDate1();
			Date currentDateTo = wl2Days.get(i).getDate2();
			int j;
			for (j=res.size()-1; j>=0; j--) {
				Date peakDateFrom = res.get(j).getDate1();
				Date peakDateTo = res.get(j).getDate2();
				if (!currentDateFrom.after(peakDateTo) && !currentDateTo.before(peakDateFrom)) {
					break;
				}
			}
			if (j < 0) {
				res.add(wl2Days.get(i));
			}
		}
		// in the peaks we need the firt 10 results
		int maxIndex = (res.size()<10 ? res.size() : 10);
		return res.subList(0, maxIndex);
	}

	public List<StatsResult> getAssetGroupProfitList() {
		return assetGroupProfitList;
	}

	public String getAssetGroupProfitListSize() {
		if (null == assetGroupProfitList) {
			return String.valueOf(0);
		}
		return String.valueOf(assetGroupProfitList.size());
	}

	public List<StatsResult> getAssetGroupSuccessList() {
		return assetGroupSuccessList;
	}

	public String getAssetGroupSuccessListSize() {
		if (null == assetGroupSuccessList) {
			return String.valueOf(0);
		}
		return String.valueOf(assetGroupSuccessList.size());
	}

	public List<StatsResult> getAssetProfitList() {
		return assetProfitList;
	}

	public String getAssetProfitListSize() {
		if (null == assetProfitList) {
			return String.valueOf(0);
		}
		return String.valueOf(assetProfitList.size());
	}

	public List<StatsResult> getAssetSuccessList() {
		return assetSuccessList;
	}

	public String getAssetSuccessListSize() {
		if (null == assetSuccessList) {
			return String.valueOf(0);
		}
		return String.valueOf(assetSuccessList.size());
	}

	public List<StatsResult> getOneTouchProfitList() {
		return oneTouchProfitList;
	}

	public String getOneTouchProfitListSize() {
		if (null == oneTouchProfitList) {
			return String.valueOf(0);
		}
		return String.valueOf(oneTouchProfitList.size());
	}

	public List<StatsResult> getOneTouchSuccessList() {
		return oneTouchSuccessList;
	}

	public String getOneTouchSuccessListSize() {
		if (null == oneTouchSuccessList) {
			return String.valueOf(0);
		}
		return String.valueOf(oneTouchSuccessList.size());
	}

	public List<StatsResult> getProfitPeakList() {
		return profitPeakList;
	}

	public String getProfitPeakListSize() {
		if (null == profitPeakList) {
			return String.valueOf(0);
		}
		return String.valueOf(profitPeakList.size());
	}

	public List<StatsResult> getTimeProfitList() {
		return timeProfitList;
	}

	public String getTimeProfitListSize() {
		if (null == timeProfitList) {
			return String.valueOf(0);
		}
		return String.valueOf(timeProfitList.size());
	}

	public List<StatsResult> getTimeSuccessList() {
		return timeSuccessList;
	}

	public String getTimeSuccessListSize() {
		if (null == timeSuccessList) {
			return String.valueOf(0);
		}
		return String.valueOf(timeSuccessList.size());
	}

	public String getPeakRangeFrom() {
		if (null == peakRangeFrom) {
			return null;
		}
		return new SimpleDateFormat("dd.MM.yyyy").format(peakRangeFrom);
	}

	public String getPeakRangeTo() {
		if (null == peakRangeTo) {
			return null;
		}
		return new SimpleDateFormat("dd.MM.yyyy").format(peakRangeTo);
	}

	public List<StatsResult> getPeaksAssetProfitList() {
		return peaksAssetProfitList;
	}

	public String getPeaksAssetProfitListSize() {
		if (null == peaksAssetProfitList) {
			return String.valueOf(0);
		}
		return String.valueOf(peaksAssetProfitList.size());
	}
}