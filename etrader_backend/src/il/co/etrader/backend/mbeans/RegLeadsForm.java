package il.co.etrader.backend.mbeans;


import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import il.co.etrader.backend.bl_managers.MarketingReportManager;
import il.co.etrader.backend.bl_vos.RegLeadsDetails;
import il.co.etrader.backend.bl_vos.RegLeadsTotals;
import il.co.etrader.backend.helper.MarketingReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;


public class RegLeadsForm implements Serializable {

	private static final long serialVersionUID = 1172585477608144244L;

	final private int DISPLAY_TYPE_TOTALS = 1;
	final private int DISPLAY_TYPE_DETAILS = 2;

	public int displayType;

	// Totals filters
	private Date from;
	private Date to;
	private long campaignId;
	private long combId;


	// Totals fields
	private ArrayList<RegLeadsTotals> totalsList;
	private ArrayList<RegLeadsTotals> totalsSumList;
	private int totalsListSize;
	RegLeadsTotals selectedTotalsRow;

	// Details fields
	private ArrayList<RegLeadsDetails> detailsList;
	private ArrayList<RegLeadsDetails> detailsListToExport;
	private int regLeadsType;
    

	public RegLeadsForm() throws SQLException{
		
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		to = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		from = gc.getTime();

		detailsList = new ArrayList<RegLeadsDetails>();	
		setDisplayType(DISPLAY_TYPE_TOTALS);
	}

	public int getDisplayType() {
		return displayType;
	}

	public void setDisplayType(int displayType) {
		this.displayType = displayType;
	}

	public int getTotalsListSize() {
		return totalsListSize;
	}

	public String search() throws SQLException{
		if (campaignId == 0 && combId == 0){
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.reg.leads.must.choose.campaign.or.comb", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
			return null;
		}


		totalsList = MarketingReportManager.getRegLeadsTotalReport(0, 0, from, to, campaignId, combId, 0);
		totalsSumList = new ArrayList<RegLeadsTotals>();

		if (totalsList.size() > 0){
			RegLeadsTotals sumRow = totalsList.remove(0);
			totalsSumList.add(sumRow);

			if (campaignId == 0){
				campaignId = sumRow.getCampaignId();
			}
		}else{
			
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("reg.leads.no.data.found", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
		}
		return null;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the detailsListSize
	 */
	public int getDetailsListSize() {
		return detailsList.size();
	}

	/**
	 * @param totalsListSize the totalsListSize to set
	 */
	public void setTotalsListSize(int totalsListSize) {
		this.totalsListSize = totalsListSize;
	}

	public String campaignEntriesLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_CAMPAIGN_ENTRIES;
		setDetailsList();
		return null;
	}

	public String callMeNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_CALL_ME;
		setDetailsList();
		return null;
	}
	public String regCallMeNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_REG_CALL_ME;
		setDetailsList();
		return null;
	}
	public String shortRegNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_SHORT_REG;
		setDetailsList();
		return null;
	}
	public String regShortRegNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_REG_SHORT_REG;
		setDetailsList();
		return null;
	}
	public String directRegNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_DIRECT_REG;
		setDetailsList();
		return null;
	}
	public String usersNumLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_USERS_NUM;
		setDetailsList();
		return null;
	}
	public String firstDepRegLink() throws SQLException{
		setDisplayType(DISPLAY_TYPE_DETAILS);
		regLeadsType = Constants.REG_LEADS_TYPE_FIRST_REG_DEP;
		setDetailsList();
		return null;
	}


	private void refresh(){
		
		// push refresh on regLeadsTotals.xhtml
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();		
					response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Pragma", "no-cache");
					response.setDateHeader("Expires", 0);
					String viewId = context.getViewRoot().getViewId();
					ViewHandler handler = context.getApplication()
							.getViewHandler();
					UIViewRoot root = handler.createView(context,
							viewId);
					root.setViewId(viewId);
					context.setViewRoot(root);
		
	}
	
	public String backToTotals() throws SQLException {
		
		setDisplayType(DISPLAY_TYPE_TOTALS);
		refresh();
		
		return null;
	}

	public String exportToExcel() throws SQLException{
		this.detailsListToExport = MarketingReportManager.getRegLeadsDetails(0, 0, from, to, selectedTotalsRow.getCampaignId(),
				selectedTotalsRow.getCombId(), regLeadsType, true);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = "Reg_leads_details_" + fmt.format(from) + "_" + fmt.format(to) + ".csv";
		MarketingReportSender sender = new MarketingReportSender(this,Constants.MARKETING_REPORT_TYPE_REG_LEADS_DETAILS,filename,Utils.getWriter());

		sender.start();

		return null;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}


	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}


	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}


	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}


	/**
	 * @return the selectedTotalsRow
	 */
	public RegLeadsTotals getSelectedTotalsRow() {
		return selectedTotalsRow;
	}


	/**
	 * @param selectedTotalsRow the selectedTotalsRow to set
	 */
	public void setSelectedTotalsRow(RegLeadsTotals selectedTotalsRow) {
		this.selectedTotalsRow = selectedTotalsRow;
	}


	/**
	 * @return the totalsList
	 */
	public ArrayList<RegLeadsTotals> getTotalsList() {
		return totalsList;
	}


	/**
	 * @param totalsList the totalsList to set
	 */
	public void setTotalsList(ArrayList<RegLeadsTotals> totalsList) {
		this.totalsList = totalsList;
	}


	/**
	 * @return the totalsSumList
	 */
	public ArrayList<RegLeadsTotals> getTotalsSumList() {
		return totalsSumList;
	}


	/**
	 * @param totalsSumList the totalsSumList to set
	 */
	public void setTotalsSumList(ArrayList<RegLeadsTotals> totalsSumList) {
		this.totalsSumList = totalsSumList;
	}

	public boolean isDisplayTotals() {
		return (displayType == DISPLAY_TYPE_TOTALS);
	}

	public boolean isDisplayDetails() {
		return (displayType == DISPLAY_TYPE_DETAILS);
	}


	/**
	 * @return the detailsList
	 */
	public ArrayList<RegLeadsDetails> getDetailsList() {
		return detailsList;
	}


	/**
	 * @param detailsList the detailsList to set
	 * @throws SQLException
	 */
	public void setDetailsList() throws SQLException {

		long tempCombId = combId;

		if (combId == 0){
			tempCombId = selectedTotalsRow.getCombId();
		}

		this.detailsList = MarketingReportManager.getRegLeadsDetails(0, 0, from, to, selectedTotalsRow.getCampaignId(),
				tempCombId, regLeadsType, false);
		        CommonUtil.setTablesToFirstPage();
		
	}


	/**
	 * @return the detailsListToExport
	 */
	public ArrayList<RegLeadsDetails> getDetailsListToExport() {
		return detailsListToExport;
	}


}
