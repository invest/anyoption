package il.co.etrader.backend.mbeans;

import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;

public class EncryptionForm extends FormBase {

	private String originalText;
	private String resultText;

	public String encrypt() {
		try {
			if (null != originalText && !originalText.equals("")) {
				resultText = AESUtil.encrypt(originalText);
			}
		} catch (Exception e) {
			resultText = "";
		}

		return null;
	}

	public String decrypt() {
		try {
			if (null != originalText && !originalText.equals("")) {
				resultText = AESUtil.decrypt(originalText);
			}
		} catch (Exception e) {
			resultText = "";
		}
		return null;
	}

	public String getOriginalText() {
		return originalText;
	}

	public void setOriginalText(String text1) {
		this.originalText = text1;
	}

	public String getResultText() {
		return resultText;
	}

	public void setResultText(String resultText) {
		this.resultText = resultText;
	}

	public String getIsCanSee() {
		long writerId = Utils.getWriter().getWriter().getId();
		if (Utils.getAppData().getIsLive()) {
			if 	(	writerId == 213 || // chen
					writerId == 38 || //  asaf
					writerId == 485 || // elhanan
					writerId == 26 || // vadim
					writerId == 215 || // eran
					writerId == 184 || // eliran
					writerId == 25 || // eyal
					writerId == 24 || // kobi
					writerId == 481 || // aviad
					writerId == 475) {  // idanz
				return "true";
			}
		} else { // test
			if 	(	writerId == 213 || // chen
					writerId == 38 || // asaf
					writerId == 476 || // elhanan
					writerId == 26 || // vadim
					writerId == 215 || // eran
					writerId == 184 || // eliran
					writerId == 25 || // eyal
					writerId == 24 || // kobi
					writerId == 475 || // aviad
					writerId == 474) {  // idanz
					return "true";
				}
		}
		return "false";
	}
}
