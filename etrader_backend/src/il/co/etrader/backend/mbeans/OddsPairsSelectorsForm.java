package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.beans.base.OpportunityOddsPair;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

public class OddsPairsSelectorsForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8983322841769398514L;
	private ArrayList<OpportunityOddsPair> list = new ArrayList<OpportunityOddsPair>();
	private OpportunityOddsPair oddsPair;

	public OddsPairsSelectorsForm() throws SQLException {
		Search();
	}

	public String Search() throws SQLException {
		list = InvestmentsManager.getOpportunityOddsPair();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String initValues() {
		oddsPair = new OpportunityOddsPair();
		oddsPair.setRefundSelector(0);
		oddsPair.setReturnSelector(0);
		return Constants.NAV_ODDS_PAIRS;
	}

	public String insert() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();

		if (oddsPair.getRefundSelector() == 0 || oddsPair.getRefundSelector() > oddsPair.getReturnSelector() || oddsPair.getRefundSelector() == oddsPair.getReturnSelector()) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage( "error.sadmin.selectors.pairs.return.refund", null), null);
			context.addMessage(null, fm);
			return null;
		}

		if (oddsPair.getRefundSelector() + oddsPair.getReturnSelector() > 100) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.sadmin.selectors.sum", null),
					null);
			context.addMessage(null, fm);

			return null;
		}

		if (InvestmentsManager.isExistOpportunityOddsPair(oddsPair)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.sadmin.selectors.pairs.exist",
							null), null);
			context.addMessage(null, fm);

			return null;
		}

		boolean res = InvestmentsManager.insertOpportunityOddsPair(oddsPair);
		list = InvestmentsManager.getOpportunityOddsPair();
		return null;
	}

	public ArrayList<OpportunityOddsPair> getList() {
		return list;
	}

	public void setList(ArrayList<OpportunityOddsPair> list) {
		this.list = list;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * @return the oddsPair
	 */
	public OpportunityOddsPair getOddsPair() {
		return oddsPair;
	}

	/**
	 * @param oddsPair
	 *            the oddsPair to set
	 */
	public void setOddsPair(OpportunityOddsPair oddsPair) {
		this.oddsPair = oddsPair;
	}

}
