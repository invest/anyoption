package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.ClearingLimitaion;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.ReroutingLimit;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

public class ReroutingLimitsForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ReroutingLimitsForm.class);
	private long limitType;
	private List<String> providers;
	private long currencyId;
	private ArrayList ccTypes;
	private long ccTypeId;
	private long binId;
	private long countryId;
	private ArrayList<SelectItem> clearingProviders;	
	private ArrayList<ReroutingLimit> reroutingLimitList;
	private ReroutingLimit reroutingLimit;
	private boolean insertMsgShow;

	public ReroutingLimitsForm() {
		FacesContext context = FacesContext.getCurrentInstance();
		limitType = 1;
		providers = new ArrayList<String>();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		ccTypes = ap.getAllCreditCardTypes();
		initClearingProviders();
		insertMsgShow = false;
		search();
	}

	public String search() {
		try {
			reroutingLimitList = TransactionsManager.getReroutingLimit();
		} catch (SQLException e) {
			reroutingLimitList = new ArrayList<ReroutingLimit>();
			log.error("Failed to get limits", e);
		}
		
		return null;		
	}
	
	public String insert() {
		FacesContext context = FacesContext.getCurrentInstance();
		boolean res = true;
		if (limitType == 0) {
			res = false;
		}
		
		for (String item: providers) {
			if (res) {
				ClearingLimitaion cl = new ClearingLimitaion();
				Long clearingProviderId = Long.valueOf(item);
				if (null != clearingProviderId) {
					cl.setClearingProviderIdGroup(clearingProviderId.longValue());
					
					if (limitType == 1) {
						cl.setCurrencyId(currencyId); 
					} else if (limitType == 2) {
						cl.setCreditCardTypeId(ccTypeId);
					} else if (limitType == 3) {
						cl.setBinId(binId);
					} else if (limitType == 4) {
						cl.setCountryId(countryId);
					}
					try {
						res = TransactionsManagerBase.insertLimit(cl);
					} catch (SQLException e) {
						log.error("Failed to insert limit", e);
					}
				}
			}
		}
		FacesMessage fm = null;
		if (res) {
			insertMsgShow = true;	
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.faild", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
		}		
		search();
		return null;
	}
	
	public String save(){
		FacesContext context = FacesContext.getCurrentInstance();
		boolean res = true;
		insertMsgShow = false;	
		try {
			res = TransactionsManager.setReroutingLimit(reroutingLimit);
		} catch (SQLException e) {
			log.error("Failed to update  reroutingLimit", e);
		}
		FacesMessage fm = null;
		if (res) {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);	
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.faild", null, Utils.getWriterLocale(context)),null);
		}
		context.addMessage(null, fm);
		search();
		return Constants.NAV_LIMITS_CONTENT;
	}
	
	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getLimitType() {
		return limitType;
	}

	public void setLimitType(long limitType) {
		this.limitType = limitType;
	}

	public List<String> getProviders() {
		return providers;
	}

	public void setProviders(List<String> providers) {
		this.providers = providers;
	}

	public ArrayList getCcTypes() {
		return ccTypes;
	}

	public void setCcTypes(ArrayList ccTypes) {
		this.ccTypes = ccTypes;
	}

	public long getCcTypeId() {
		return ccTypeId;
	}

	public void setCcTypeId(long ccTypeId) {
		this.ccTypeId = ccTypeId;
	}

	public long getBinId() {
		return binId;
	}

	public void setBinId(long binId) {
		this.binId = binId;
	}
	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
	private void initClearingProviders() {
		insertMsgShow = false;	
		clearingProviders = new ArrayList<SelectItem>();
		clearingProviders.add(new SelectItem("1", CommonUtil.getMessage("clearing.provider.inatec", null)));
		clearingProviders.add(new SelectItem("2", CommonUtil.getMessage("clearing.provider.wire.card", null)));
		clearingProviders.add(new SelectItem("3", CommonUtil.getMessage("clearing.provider.tbi", null)));
		clearingProviders.add(new SelectItem("4", CommonUtil.getMessage("clearing.provider.wire.card.3d", null)));
		clearingProviders.add(new SelectItem("5", CommonUtil.getMessage("clearing.provider.walpay", null)));
	}

	public ArrayList<SelectItem> getClearingProviders() {
		return clearingProviders;
	}

	public void setClearingProviders(ArrayList<SelectItem> clearingProviders) {
		this.clearingProviders = clearingProviders;
	}

	public long getListSize() {
		return reroutingLimitList.size();
	}

	/**
	 * @return the reroutingLimitList
	 */
	public ArrayList<ReroutingLimit> getReroutingLimitList() {
		return reroutingLimitList;
	}

	/**
	 * @param reroutingLimitList the reroutingLimitList to set
	 */
	public void setReroutingLimitList(ArrayList<ReroutingLimit> reroutingLimitList) {
		this.reroutingLimitList = reroutingLimitList;
	}

	/**
	 * @return the reroutingLimit
	 */
	public ReroutingLimit getReroutingLimit() {
		return reroutingLimit;
	}

	/**
	 * @param reroutingLimit the reroutingLimit to set
	 */
	public void setReroutingLimit(ReroutingLimit reroutingLimit) {
		this.reroutingLimit = reroutingLimit;
	}

	/**
	 * @return the insertMsgShow
	 */
	public boolean isInsertMsgShow() {
		return insertMsgShow;
	}

	/**
	 * @param insertMsgShow the insertMsgShow to set
	 */
	public void setInsertMsgShow(boolean insertMsgShow) {
		this.insertMsgShow = insertMsgShow;
	}
}
