package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.FeeCurrencyMap;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;

public class FeeConfigurationForm implements Serializable{

	private static final long serialVersionUID = 7735246676116390438L;
	private static final Logger logger = Logger.getLogger(FeeConfigurationForm.class);
	private ArrayList<SelectItem> currencyList;
	private ArrayList<SelectItem> feeList;
	private ArrayList<FeeCurrencyMap> mapList;
	private int listSize;
	private long writerId;
	private FeeCurrencyMap targetMap;

	public FeeConfigurationForm() {
		try {
			listSize = 0;
			setMapList(TransactionsManager.getFeeCurrencyList());
			listSize = mapList.size();
			writerId = Utils.getWriter().getWriter().getId();
		} catch (SQLException e) {
			logger.debug("FeeConfigurationForm Init: " + e.getMessage());
		}
	}
	
	public String enterEditPage() {
		return Constants.NAV_FEE_CONFIGURATION_EDIT;
	}
	public String updateInsert() {
		try {
			targetMap.setWriterId(writerId);
			TransactionsManager.updateFeeCurrencyMap(targetMap);
			setMapList(TransactionsManager.getFeeCurrencyList());
		} catch (SQLException e) {
			logger.debug("Can't update feeConfiguration: " + e.getMessage());
		}
		return Constants.NAV_FEE_CONFIGURATION;
	}
	public ArrayList<SelectItem> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(ArrayList<SelectItem> currencyList) {
		this.currencyList = currencyList;
	}

	public ArrayList<SelectItem> getFeeList() {
		return feeList;
	}

	public void setFeeList(ArrayList<SelectItem> feeList) {
		this.feeList = feeList;
	}

	public ArrayList<FeeCurrencyMap> getMapList() {
		return mapList;
	}

	public void setMapList(ArrayList<FeeCurrencyMap> mapList) {
		this.mapList = mapList;
	}

	public FeeCurrencyMap getTargetMap() {
		return targetMap;
	}

	public void setTargetMap(FeeCurrencyMap targetMap) {
		this.targetMap = targetMap;
	}

	public int getListSize() {
		return listSize;
	}

	public void setListSize(int listSize) {
		this.listSize = listSize;
	}
}
