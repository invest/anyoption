package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.bl_vos.ClearingPriority;

public class ReroutingPrioritiesForm implements Serializable{

	/**
	 * @author oshikl
	 */
	private static final long serialVersionUID = -8231783675577296543L;
	private ArrayList<ClearingPriority> list;
	
	public String navEdit() {
		return Constants.NAV_REROUTING_PRIORITY_CHANGE;
	}
	
	public ReroutingPrioritiesForm() throws SQLException {
		list = ConfigManager.getPriorityToChange();
	}
	
	public ArrayList<ClearingPriority> getList() {
		return list;
	}
	
	public void setList(ArrayList<ClearingPriority> list) {
		this.list = list;
	}

	public int getListSize() {
		return list.size();
	}
	
	public String update() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		boolean canUpdate = true;
		int sumOfPriority = list.size();
		for(ClearingPriority cp1 : list ){			
			for(ClearingPriority cp2 :list){				
				if(cp1.getId() != cp2.getId()){					
					if(cp2.getPriority() == cp1.getPriority() || cp2.getPriority() > sumOfPriority){
						
						canUpdate = false;
						FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO, "2 fields Are Equal",null);
			        	context.addMessage("reroutingPrioritiesForm:saveButton",fm);
						return null;		
						
					}					
				}				
			}			
		}
		if(canUpdate){	
			for(ClearingPriority cp1 : list ){
				Long priorityOld1 = Long.parseLong(cp1.getPriorityText().substring(Constants.REROUTING_PRIORITY_CONFIG_KEY.length()));
				if(priorityOld1 != cp1.getPriority()){
					for(ClearingPriority cp2 : list){
						Long priorityOld2 = Long.parseLong(cp2.getPriorityText().substring(Constants.REROUTING_PRIORITY_CONFIG_KEY.length()));
						if(cp1.getPriority() == priorityOld2 && cp1.getId() != cp2.getId() && !cp2.isChanged()){
							cp1.setChanged(true);
							long groupid = cp1.getGroupId();
							String groupName = cp1.getGroupName();
							cp1.setGroupId(cp2.getGroupId());
							cp2.setGroupId(groupid);
							cp1.setGroupName(cp2.getGroupName());
							cp2.setGroupName(groupName);
						}
					}
				}				
			}

			ConfigManager.updateReroutingPriorities(list);
		}		
		return Constants.NAV_REROUTING_PRIORITY_CHANGE_BACK;
	}
}
