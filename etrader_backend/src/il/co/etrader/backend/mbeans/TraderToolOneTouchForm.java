package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.enums.SkinGroup;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.OpportunityTotal;
import il.co.etrader.backend.dao_managers.MarketsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;



public class TraderToolOneTouchForm implements Serializable {

	private static final long serialVersionUID = -4365573988415113079L;

	private static final Logger log = Logger.getLogger(TraderToolOneTouchForm.class);

	private ArrayList<OpportunityTotal> list;
	private HashMap<Long, ArrayList<Market>> allMarketsByGroups;
	private LinkedHashMap<Long, String> marketsByGroups;

	private Date from;
	private String settled;
	private long groupId;
	private long market;
	private long upDown;

	//sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;

	public TraderToolOneTouchForm() throws SQLException, ParseException{

		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.MONTH, -1);
		from = gc.getTime();

		settled = Constants.NOT_SETTLED;
		market = 0;
		groupId = 0;
		upDown = -1;  // all

		sortColumn = "";
		prevSortColumn = "";
		allMarketsByGroups = new HashMap<Long, ArrayList<Market>>();
		marketsByGroups = new LinkedHashMap<Long, String>();
		marketsByGroups.put(new Long("0"), "All");
		for (Map.Entry<Long, MarketGroup> entry : MarketsManager.getMarketsByGroups().entrySet()) {
			allMarketsByGroups.put(entry.getKey(), entry.getValue().getMarkets());
			ArrayList<Market> markets = entry.getValue().getMarkets();
			for (Market m : markets) {
				marketsByGroups.put(m.getId(), m.getDisplayName());
			}
		}
		updateList();
	}

	/**
	 * Get oppurtunities list size
	 * @return
	 * 		size of all the model and not page size
	 */
	public int getListSize() {
		int size = 0;
		if ( null != list ) {
			size = list.size();
		}

		return size;
	}

	/**
	 * Get list of opportunities
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public String updateList() throws SQLException, ParseException {

		list = InvestmentsManager.getTtOneTouchOpportunities(from, settled, market, groupId, upDown);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

    public void updateMarketChange(ValueChangeEvent event) throws SQLException, ParseException{
    	market = (Long)event.getNewValue();
    	updateList();
    }

    public void updateGroupChange(final AjaxBehaviorEvent event) throws SQLException, ParseException {
    	ArrayList <Market> markets = new ArrayList <Market>();
    	if (groupId == 0) {
    		for(ArrayList<Market> list : allMarketsByGroups.values()) {
    			markets.addAll(list);
    		}
    		marketsByGroups.clear();
    		marketsByGroups.put(new Long("0"), "All");
    		for(Market m : markets) {
    			marketsByGroups.put(m.getId(), m.getDisplayName());
    		}
    	} else {
	    	markets = allMarketsByGroups.get(groupId); 
	    	marketsByGroups.clear();
    		marketsByGroups.put(new Long("0"), "All");
	    	for (Market m : markets) {
				marketsByGroups.put(m.getId(), m.getDisplayName());
	    	}
    	}
		market = 0;
    	updateList();
    }

    public void updateUpDown(ValueChangeEvent event) throws SQLException, ParseException{
    	upDown = (Long)event.getNewValue();
    	updateList();
    }

	/**
	 * Change settled radio button
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateChange(ValueChangeEvent event) throws SQLException, ParseException{
    	settled =(String)event.getNewValue();
    	updateList();
    }

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the settled
	 */
	public String getSettled() {
		return settled;
	}

	/**
	 * @param settled the settled to set
	 */
	public void setSettled(String settled) {
		this.settled = settled;
	}

	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the market
	 */
	public long getMarket() {
		return market;
	}

	/**
	 * @param market the market to set
	 */
	public void setMarket(long market) {
		this.market = market;
	}

	/**
	 * @return the list
	 */
	public ArrayList<OpportunityTotal> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<OpportunityTotal> list) {
		this.list = list;
	}

	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	/**
	 * Sort list
	 * @return
	 */
	public String sortList() {
		if (sortColumn.equals(prevSortColumn)) {
			sortAscending = !sortAscending;
		} else {
			sortAscending = true;
		}

		prevSortColumn = sortColumn;

		if (sortColumn.equals("id")) {
			Collections.sort(list,new IdComparator(sortAscending));
		}

		if (sortColumn.equals("marketName")) {
			Collections.sort(list,new MarketNameComparator(sortAscending));
		}

		if (sortColumn.equals("scheduled")) {
			Collections.sort(list,new ScheduledComparator(sortAscending));
		}

		if (sortColumn.equals("estClosingTime")) {
			Collections.sort(list,new TimeEstClosingComparator(sortAscending));
		}

		if (sortColumn.equals("volume")) {
			Collections.sort(list, new VolumeComparator(!sortAscending));
		}

		if (sortColumn.equals("oppExposure")) {
			Collections.sort(list, new ExposureParamComparator(sortAscending, SkinGroup.ETRADER));
		}

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Long(op1.getId()).compareTo(new Long(op2.getId()));
   		 		}
   		 		return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
  	 }

	private class MarketNameComparator implements Comparator {
		private boolean ascending;
		public MarketNameComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 		    return CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId()));
   		 		}
   		 		return -CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId())); 
		}
  	 }

	private class ScheduledComparator implements Comparator {
		private boolean ascending;
		public ScheduledComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Integer(op1.getScheduled()).compareTo(new Integer(op2.getScheduled()));
   		 		}
   		 	return -new Integer(op1.getScheduled()).compareTo(new Integer(op2.getScheduled()));
		}
  	 }

	private class TimeEstClosingComparator implements Comparator {
		private boolean ascending;
		public TimeEstClosingComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
   		 		}
   		 		return -op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
		}
  	 }

	private class VolumeComparator implements Comparator {
		private boolean ascending;
		public VolumeComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Double(op1.getCallsAmountAddPutsAmount()).compareTo(new Double(op2.getCallsAmountAddPutsAmount()));
   		 		}
   		 		return -new Double(op1.getCallsAmountAddPutsAmount()).compareTo(new Double(op2.getCallsAmountAddPutsAmount()));
		}
  	 }

	private class ExposureParamComparator implements Comparator<Opportunity> {
		private boolean ascending;
		private SkinGroup skinGroup;

		public ExposureParamComparator(boolean ascending, SkinGroup skinGroup) {
			this.ascending = ascending;
			this.skinGroup = skinGroup;
		}

		public int compare(Opportunity o1, Opportunity o2) {
			if (ascending) {
				return new Double(o1.getSkinGroupMappings().get(skinGroup).getMaxExposure())
							.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getMaxExposure()));
			}
			return -new Double(o1.getSkinGroupMappings().get(skinGroup).getMaxExposure())
							.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getMaxExposure()));
		}
	}

	/**
	 * @return the upDown
	 */
	public long getUpDown() {
		return upDown;
	}

	/**
	 * @param upDown the upDown to set
	 */
	public void setUpDown(long upDown) {
		this.upDown = upDown;
	}
	
    public String getStatusTxt(int isSettled, int isPublished ){
        return il.co.etrader.bl_managers.OpportunitiesManagerBase.getStatusTxt(isSettled, isSettled);
    }
    
    public HashMap<Long, String> getMarketsByGroups() {
		return marketsByGroups;
	}

	public void setMarketsByGroups(LinkedHashMap<Long, String> marketsByGroups) {
		this.marketsByGroups = marketsByGroups;
	}
}
