package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ApiIssuesManager;
import il.co.etrader.backend.bl_vos.ApiIssue;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

/**
 * @author EranL
 *
 */
@ManagedBean(name="apiIssueForm")
@RequestScoped
public class ApiIssuesForm extends FormBase implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ApiIssuesForm.class);
	@ManagedProperty(value="#{apiIssue}")
	private ApiIssue apiIssue;
	private ArrayList<ApiIssue> list;
	private static final String NAV_ISSUE = "apiIssue";
	private static final String NAV_ISSUES = "apiIssues";
	
	//filters
	private Date from;
	private Date to;
	private long statusId;
	private long channelId;
	private long subjectId;
	
	@PostConstruct
	public void init() {
		apiIssue = new ApiIssue();
		list = new ArrayList<ApiIssue>();
		searchApiIssues();
	}
	
	/**
	 * 
	 */
	public ApiIssuesForm() {
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		gc.add(GregorianCalendar.YEAR, -1);
		from = gc.getTime();				
	}
	
	/**
	 * Add new API issue 
	 * @return
	 */
	public String addNewIssue() {
		apiIssue = new ApiIssue();
		return NAV_ISSUE;
	}
	
	/**
	 * Navigate to API issue screen
	 * @return
	 */
	public String navigateToApiIssue() {
		return NAV_ISSUE;
	}
	
	/**
	 * Insert or update API issue 
	 * @return
	 */
	public String insertUpdateApiIssue() {		
		// prevent identical requests from being processed
		if (this.saveTokenIsInvalid()) {
			return this.logInvalidSaveAndReturn(log,null);
		}
		this.generateSaveToken();
		FacesContext context = FacesContext.getCurrentInstance();		
		FacesMessage fm;
		String nav = null;
		String facesMessage = null;
		Severity severity = null;
		
//      FacesContext fc = FacesContext.getCurrentInstance();
//      try {
//          ELContext elContext = fc.getELContext();
//          Object bean = elContext.getELResolver().getValue(elContext, null, "apiExternalUserStripForm");
//          apiExternalUserSession = ((ApiExternalUserStripForm)bean).getApiExternalUser().getId();
//      } catch (RuntimeException e) {
//        log.fatal("cant load bean apiExternalUserStripForm" , e);
//      }		
		try {
			if (apiIssue.getId() == 0) { //insert				
				apiIssue.setApiExternalUserId(6);
				apiIssue.setUtcOffset("GMT+00:00");
				apiIssue.setWriter_id(Utils.getWriter().getWriter().getId());
				ApiIssuesManager.insertApiIssue(apiIssue);
				facesMessage = "issues.insert.success";
			} else { //update
				apiIssue.setUtcOffset("GMT+00:00");
				apiIssue.setWriter_id(Utils.getWriter().getWriter().getId());				
				ApiIssuesManager.updateApiIssue(apiIssue); 
				facesMessage = "issues.update.success";				
			}	
			nav = NAV_ISSUES;
			severity = FacesMessage.SEVERITY_INFO;
		} catch (Exception e) {
			log.error("Error while trying to insert/update API issue ", e);
			facesMessage = "issues.insert.failed";
			severity = FacesMessage.SEVERITY_ERROR;			
		}
		// add messages 
		fm = new FacesMessage(severity,CommonUtil.getMessage(facesMessage, null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);
		if (nav != null) { // inserted/updated API issue successfully
			searchApiIssues();
		}
		return nav;
	}
	
	/**
	 * Search for API issues for external user
	 * @return
	 * @throws SQLException
	 */
	public String searchApiIssues() {
		long apiExternalUserId = 6;
		try {		
			to		= CommonUtil.addDay(CommonUtil.getDateTimeFormat(to, Utils.getWriter().getUtcOffset()));  	 
			from	= CommonUtil.getDateTimeFormat(from, Utils.getWriter().getUtcOffset()); 
			list	= ApiIssuesManager.searchApiIssues(apiExternalUserId, from, to, statusId, channelId, subjectId);			
		} catch (Exception e) {
			log.error("Error while trying to search for API issues for external user ", e);
			// add messages 
			FacesContext fc = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("files.error", null, Utils.getWriterLocale(fc)),null);
			fc.addMessage(null, fm);
		}		
		return null;
	}
		
	/**
	 * Return issues list size
	 * @return
	 */
	public int getListSize() {
		return list.size();
	}

	/**
	 * @return the apiIssue
	 */
	public ApiIssue getApiIssue() {
		return apiIssue;
	}

	/**
	 * @param apiIssue the apiIssue to set
	 */
	public void setApiIssue(ApiIssue apiIssue) {
		this.apiIssue = apiIssue;
	}
	/**
	 * @return the list
	 */
	public ArrayList<ApiIssue> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<ApiIssue> list) {
		this.list = list;
	}
	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}
	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return the channelId
	 */
	public long getChannelId() {
		return channelId;
	}
	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}
	/**
	 * @return the subjectId
	 */
	public long getSubjectId() {
		return subjectId;
	}
	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

}