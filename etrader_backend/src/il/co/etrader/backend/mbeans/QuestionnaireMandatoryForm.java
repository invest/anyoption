/**
 * 
 */
package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class QuestionnaireMandatoryForm implements Serializable {
	private static final int ANSWER_YES_IDX = 0;
	private static final int ANSWER_NO_IDX = 1;
	private static final int QUESTION_FUNDS_ORIGIN_IDX = 4;
	private static final int QUESTION_PEP_IDX = 5;
	private static final int QUESTION_TRADE_WISH_IDX = 6;
	
	private static final Logger log = Logger.getLogger(QuestionnaireMandatoryForm.class);
	private boolean submitted;
	private boolean PEPanswer;
	private static final long serialVersionUID = 1L;
	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private UserBase user;	
	private UserRegulationBase userRegulation;
	private Writer writer;
	
	public QuestionnaireMandatoryForm() throws Exception {
		WriterWrapper ww = Utils.getWriter();
		writer = ww.getWriter();
		
		questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME);
		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), writer.getId());	
		
	   	userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	UserRegulationManager.getUserRegulation(userRegulation);	   	
	}
	
	public String submitQuestionnaire () throws SQLException{
		
		boolean notAllowToSubmit = isEdit();
		if(!notAllowToSubmit){
			for (QuestionnaireUserAnswer answer : userAnswers){
				answer.setWriterId(writer.getId());
			}
			
			List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
			
			// TODO Remove this line when question 5 QUESTION_FUNDS_ORIGIN is viewable again.
			userAnswers.get(QUESTION_FUNDS_ORIGIN_IDX)
							.setAnswerId(questions.get(QUESTION_FUNDS_ORIGIN_IDX).getAnswers().get(ANSWER_YES_IDX).getId());
			
			QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);

			if (userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_FIRST_DEPOSIT) {
					log.warn("User [" + user.getId() + "] answered correct");
				if (userRegulation.getSuspendedReasonId() == UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT
						|| userRegulation.getSuspendedReasonId() == UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_NOT_FILLED){
					userRegulation.setSuspendedReasonId(UserRegulationBase.NOT_SUSPENDED);
					userRegulation.setComments("Unsespend afetr mandatory questionnaire answered correct");
					userRegulation.setWriterId(writer.getId());
					UserRegulationManager.updateSuspended(userRegulation);
					log.warn("User [" + user.getId() + "] create new issue for regulation documents...");
				}
				
				if (userRegulation.getApprovedRegulationStep() > UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
					//add in history table step 3 so user will not get suspended
					Integer tmpRegulationStep = userRegulation.getApprovedRegulationStep();
					String tmpRegulationComments = userRegulation.getComments();
					userRegulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
					userRegulation.setComments("add step 3 from BE overwrite");
					try {
						UserRegulationManager.updateRegulationStep(userRegulation);
					} catch (SQLException e) {
						log.error("User [" + user.getId() + "] problem in questions hist log");
						e.printStackTrace();
					}
					userRegulation.setApprovedRegulationStep(tmpRegulationStep);
					userRegulation.setComments(tmpRegulationComments);
					try {
						log.debug("Revert to a previous state " + tmpRegulationStep);
						UserRegulationManager.updateRegulationStep(userRegulation);
					} catch (SQLException e) {
						log.error("User [" + user.getId() + "] problem in questions hist log");
						e.printStackTrace();
					}
				}
				
				if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
					userRegulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
					userRegulation.setWriterId(writer.getId());
					UserRegulationManager.updateRegulationStep(userRegulation);
					log.warn("User [" + user.getId() + "] create new issue for regulation documents...");
					
					FacesContext context=FacesContext.getCurrentInstance();
					context.getAttributes().put(ConstantsBase.CREATE_ISSUE_FOR_REGULATION, "true");
					IssuesManagerBase.updatePendingRegulationIssuesForUser(user.getId());
					RiskAlertsManagerBase.createIssueForRegulation(user, true);
				}				
			}
		
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("questionnaire.mandatory.submit", null),null);
			context.addMessage(null, fm);					
			return Utils.navAfterUserOperation();
		} else {
			return null;
		}				
	}	
	
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}
	

	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public String insertQuestionnaire() throws Exception {
		return null;
	}

	public boolean isEdit() {
		if (userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE
				|| userRegulation.getSuspendedReasonId() ==  UserRegulationBase.SUSPENDED_MANDATORY_QUESTIONNAIRE_FILLED_INCORRECT) { 
			if(writer.getId() == ConstantsBase.REUT_USER_ID ||  writer.getId() == ConstantsBase.NAAMA_USER_ID || writer.getId() == ConstantsBase.QABG_USER_ID) {
				return false;
			} else {
				return true;
			}			
		} else {
			return false;
		}		
	}

	public boolean isPEPanswer() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long PEAnswer = userAnswers.get(QUESTION_TRADE_WISH_IDX).getAnswerId();
		if (PEAnswer == null || PEAnswer==questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_YES_IDX).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setPEPanswer(boolean pEPanswer) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (pEPanswer) {
			userAnswers.get(QUESTION_TRADE_WISH_IDX).setAnswerId(questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_YES_IDX).getId());
		} else {
			userAnswers.get(QUESTION_TRADE_WISH_IDX).setAnswerId(questions.get(QUESTION_TRADE_WISH_IDX).getAnswers().get(ANSWER_NO_IDX).getId());
		}
	}
	
}
