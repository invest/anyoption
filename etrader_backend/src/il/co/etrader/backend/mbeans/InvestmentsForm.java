package il.co.etrader.backend.mbeans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.datascroller.HtmlDataScroller;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.CurrenciesManagerBase;

import il.co.etrader.backend.bl_managers.ApiExternalUsersManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.ApiExternalUser;
import il.co.etrader.backend.bl_vos.InvestmentTotal;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.CopyopInvestmentDetails;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.DataPage;
import il.co.etrader.util.InvestmentFormatter;
import il.co.etrader.util.PagedListDataModel;

public class InvestmentsForm implements Serializable {

	private static final long serialVersionUID = -849821985533583425L;

	private static final Logger logger = Logger.getLogger(InvestmentsForm.class);
	
	private static final String DELEMITER = ",";

	private long id;
	private Date from;
	private Date to;
	private String isSettled;
	private String marketGroup;
	private long scheduled;
	private Investment investment;
	private String timeType;
	private ArrayList<SelectItem> timeTypeList;
	private String userName;
	private long classId;
	private long statusId;
	private long currencyId;
	private String htmlBuffer;
	private int skinId;
	private long skinBusinessCaseId;

	// Page Constants
	private final int INVESTMENT_SEPERATOR_LINE = 1;   //seperator line '----------'
	private final int INVESTMENT_PAGE_SUMMARY_LINE = 2; //sum of all investments on page
	private final int INVESTMENT_ALL_PAGES_SUMMARY_LINE = 3; //sum of all investments
	private final int TOTAL_ROWS = 4;
	private final int ROWS_TO_XSL = 120;
	private HtmlDataScroller scroll = new HtmlDataScroller();

	// paging variables
	private DataModel investments;
	private DataPage<Investment> dataPage;
	private boolean needUpdate;
	private long lastInvestmentId;

	private ArrayList<SelectItem> closingHourSI;
	private ArrayList<SelectItem> investmentTypeSI;
	private long invType;
	private long invAmount;
	private long invAmountBelow;
	private long invCloseHour;
	private long invCloseMin;

	private String closingHour;
	private long showAdvanced;
    private List<String> opportunitiesList;
    private Date arabicStartDate;
	//TODO: change this value to be associated with platform table (currently it's by writer)    
    private List<String> insertedFrom;
    //copyop
    private int copyopInvTypeId; 
    private long fromUserId;
    private HashMap<Long, CopyopInvestmentDetails> copyopInvestmentDetailsList;

    private long fromHour;
	private long fromMin;
	private long toHour;
	private long toMin;
	private int houseResult;

//	for api external user
    private long apiExternalUserId;
    private String apiExternalReference;
    private long apiUserId;

//	many lisy box
    private List<String> skins;
    private List<String> groups;
    private List<String> markets;
    private ArrayList<SelectItem> marketsList;
    private final String TIME_CREATED = "i.time_created";
	private final String TIME_SETTLED = "i.time_settled";
	private final int NON_ACTIVE_FIELD = 0;
	private final int NUM_OF_ROWS_DAILY = 23;

    private boolean isDaily;

    private int lastVisitPage;
	private final int INVESTMENTS = 1;
	private final int DAILY = 2;

//	sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private boolean activateAscending;

	public InvestmentsForm() throws SQLException, ParseException {
		lastVisitPage = 0;
	}

	public String getInitInvestments() throws SQLException, ParseException {
		if (lastVisitPage == INVESTMENTS) {
			return "";
		}
		lastVisitPage = INVESTMENTS;
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(Utils.getWriter().getUtcOffset()));
		SelectItem tmp;
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0 + gc.getTimeZone().getRawOffset()/(1000*60*60));
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, 1);
		to = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, -1);
		if(user.isSupportSelected()){
			gc.add(Calendar.YEAR, -1 );
		}
		from = gc.getTime();

		isSettled = "0";
		marketGroup = "0,0";
		userName="";
		invType = 0;
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		statusId = ConstantsBase.INVESTMENT_CLASS_STATUS_ACTIVE; // default to active investment
		currencyId = ConstantsBase.CURRENCY_ALL_ID;
//		opportunityTypeId = ConstantsBase.OPPORTUNITIES_TYPE_DEFAULT ;  // take all opp types
		opportunitiesList = new ArrayList<String>();
		opportunitiesList.add("1");
		opportunitiesList.add("2");
		setSkinId(Skin.SKINS_ALL_VALUE);

		//if the writer is an etrader writer default currency id dropdown to ILS
		if (Utils.getWriter().isEtrader()) {
			currencyId = ConstantsBase.CURRENCY_ILS_ID;
		}

		if (user.isSupportSelected() || user.isRetentionSelected() ||
				user.isRetentionMSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
			currencyId = user.getCurrencyId();
			opportunitiesList.clear();
			opportunitiesList.add("0");
//			opportunityTypeId = 0;
		}

		// set options of timeTypes
		timeTypeList = new ArrayList<SelectItem>();
		timeTypeList.add(new SelectItem("i.time_created",CommonUtil.getMessage("investments.timecreated",null, Utils.getWriterLocale(context))) );
		timeTypeList.add(new SelectItem("o.time_est_closing",CommonUtil.getMessage("investments.timeestclosing",null, Utils.getWriterLocale(context))) );
		timeType="i.time_created";

		invType = 0;
		invAmount=0;
		invAmountBelow=0;
		invCloseHour=99;
		invCloseMin=99;
		closingHour = new String("");
		showAdvanced = 0;

		//Prevent from partner arabic writers to see data before there starting time
	    long currentWriterId = Utils.getWriter().getWriter().getId();
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			arabicStartDate = sdf.parse(enumDate);
	    }
	   
	    
	    copyopInvTypeId = -1;
	    houseResult = 0;
	    isDaily = false;

	    sortColumn="";
		prevSortColumn="";
		activateAscending = false;

		apiUserId = -2;
		apiExternalReference = "";
		copyopInvestmentDetailsList = new HashMap<Long, CopyopInvestmentDetails>();
		fromUserId = 0;
		insertedFrom = new ArrayList<String>();        
		insertedFrom.add("-1");
	    updateList();
	    return "";
	}

	public String getInitDaily() throws SQLException, ParseException {
		if (lastVisitPage == DAILY) {
			return "";
		}
		lastVisitPage = DAILY;
		userName = "";
		fromHour = 99;
		fromMin = 99;
		toHour = 99;
		toMin = 99;
		invAmount = 0;
		invAmountBelow = 0;
		invCloseHour = 99;
		invCloseMin = 99;
		invType = 0;
		lastInvestmentId = 0;
		houseResult = 0;
		sortColumn="";
		prevSortColumn="";
		activateAscending = false;
		skins = new ArrayList<String>();
		markets = new ArrayList<String>();
		groups = new ArrayList<String>();
		insertedFrom = new ArrayList<String>();
        marketsList = CommonUtil.map2SelectList(Utils.getWriter().getActiveMarketsAndGroupsWithOneTouch());
		skins.add("0");
		markets.add("0");
		groups.add("0");
		insertedFrom.add("-1");
		opportunitiesList = new ArrayList<String>();
		opportunitiesList.add("0");
		isSettled = "0";
//		totalAmount = 0;
		marketGroup = "0,0";		
		copyopInvTypeId = -1;
		fromUserId = 0;
		isDaily = true;

		updateList();
		return "";
	}


	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the isSettled
	 */
	public String getIsSettled() {
		return isSettled;
	}

	/**
	 * @param isSettled the isSettled to set
	 */
	public void setIsSettled(String isSettled) {
		this.isSettled = isSettled;
	}

	/**
	 * @return the investment
	 */
	public Investment getInvestment() {
		return investment;
	}

	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * @return the timeType
	 */
	public String getTimeType() {
		return timeType;
	}

	/**
	 * @param timeType the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * @return Time type list
	 */
	public ArrayList getTimeTypeList() {
		return timeTypeList;
	}

	/**
	 * @return the classId
	 */
	public long getClassId() {
		return classId;
	}

	/**
	 * @param classId the classId to set
	 */
	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the htmlBuffer
	 */
	public String getHtmlBuffer() {
		return htmlBuffer;
	}

	/**
	 * @param htmlBuffer the htmlBuffer to set
	 */
	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}

	/**
	 * @return the marketGroup
	 */
	public String getMarketGroup() {
		return marketGroup;
	}

	/**
	 * @param marketGroup the marketGroup to set
	 */
	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @return true if no specific currency selected
	 */
	public boolean isCurrencyAllSelected() {
		return currencyId == ConstantsBase.CURRENCY_ALL_ID;
	}

	/**
	 * @return true if specific currency selected
	 */
	public boolean isStatusAllSelected() {
		return statusId == ConstantsBase.INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the scroll
	 */
	public HtmlDataScroller getScroll() {
		return scroll;
	}

	/**
	 * @param scroll the scroll to set
	 */
	public void setScroll(HtmlDataScroller scroll) {
		this.scroll = scroll;
	}

	/**
	 * @return the lastInvestmentId
	 */
	public long getLastInvestmentId() {
		return lastInvestmentId;
	}

	/**
	 * @param lastInvestmentId the lastInvestmentId to set
	 */
	public void setLastInvestmentId(long lastInvestmentId) {
		this.lastInvestmentId = lastInvestmentId;
	}


	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	@Override
	public String toString() {

		String ls = System.getProperty("line.separator");
		String retValue =
				"InvestmentsForm ( " + super.toString() + ls +
				"from = " + this.from + ls +
				"to = " + this.to + ls +
				"isSettled = " + this.isSettled + ls +
				"investment = " + this.investment + ls +
				"marketGroup = " + this.marketGroup + ls +
				"timeType = " + this.timeType + ls +
				"userName = " + this.userName + ls +
				"classId = " + this.classId + ls +
				"statusId = " + this.statusId + ls +
				"currencyId = " + this.currencyId + ls +
				"invType = " + this.invType + ls +
				"invAmount = " +this.invAmount + ls +
				"invAmountBelow = " +this.invAmountBelow + ls +
				"invCloseHour = " +this.invCloseHour+ ls +
				"invCloseMin = " +this.invCloseMin+ ls +
				"closingHour = " +this.closingHour+ ls +
				" )";

		return retValue;
	}

	public boolean getIsSettledBoolean() {
		if (isSettled == null) {
			return false;
		}

		return isSettled.equals("1");
	}

	/**
	 * Cancel investment
	 * After that dataModel will rebuild
	 * @throws SQLException
	 */
	public String cancel() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ArrayList<Long> investmentsIds = new ArrayList<Long>();

		if (investment.getTypeId() == InvestmentsManager.INVESTMENT_TYPE_BUY || investment.getTypeId() == InvestmentsManager.INVESTMENT_TYPE_SELL) {
			try {
				investmentsIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(investment.getId());
			} catch (Exception e) {
				logger.error("Can't load investments linked for the specific investment id.", e);
			}
			for (Long invId : investmentsIds) {
				InvestmentsManager.cancelInvestment(invId);
			}
		} else {
			InvestmentsManager.cancelInvestment(investment.getId());
		}

		updateList();

		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		return null;
	}


	/**
	 * Get investments list size
	 * @return
	 * 		size of all the model and not page size
	 */
	public int getListSize() {
		int size = 0;
		if ( null != investments ) {
			size = investments.getRowCount();
		}

		return size;
	}


	/**
	 * Update investments list function.
	 * Create new DataModel instance if needed
	 * and symbol that search filter preesed for new DataModel creation
	 * @return
	 */
	public String updateList()  {

		if ((apiUserId != Constants.NONE_FILTER_ID && Utils.isParameterEmptyOrNull(apiExternalReference)) ||
				(apiUserId == Constants.NONE_FILTER_ID && !Utils.isParameterEmptyOrNull(apiExternalReference))) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("external.user.strip.error.mandatory.short", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
			return null;
		} else if (apiUserId != Constants.NONE_FILTER_ID && !Utils.isParameterEmptyOrNull(apiExternalReference)) {
			apiExternalUserId = -1; //if no such user no data will be return
			try {
				ApiExternalUser apiExternalUser = ApiExternalUsersManager.getApiExternalUser(0, apiExternalReference, apiUserId);
				if (null != apiExternalUser) {
					apiExternalUserId = apiExternalUser.getId();
				}
			} catch (SQLException e) {
				logger.error("cant load external user from db reference = " + apiExternalReference + " apiUserId = " + apiUserId, e);
			}
		}

		if (null == investments) {
    		FacesContext context = FacesContext.getCurrentInstance();
    		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        	investments = new LocalDataModel((isDaily ? NUM_OF_ROWS_DAILY : user.getRowsByMenuInt()) + TOTAL_ROWS);
        }
		needUpdate = true;   // for taking data from db

        return null;
	}

	/**
     * Resets the Scroller index to the given <code>index</code>
     * @param index
     */
    public void resetScrollerIndex(int index) {
        if(getScroll() != null && getScroll().isPaginator()) {
            getScroll().getUIData().setFirst(index);
        }
    }

	/**
	 * Get investments list without summary
	 * This is for xsl export, we take max 120 rows for this
	 * @return
	 * 		ArrayList of investments
	 * @throws SQLException
	 */
	public ArrayList getListWithoutSummary() throws SQLException {

    	ArrayList<Investment> list = new ArrayList();
  		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		userName = userName.toUpperCase();

		if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
			list = InvestmentsManager.getInvestmentsList(0, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
					statusId, currencyId, getListToString(opportunitiesList), 0, ROWS_TO_XSL,getSkinFilter(),invType,CommonUtil.calcAmount(invAmount),
					CommonUtil.calcAmount(invAmountBelow) ,invCloseHour,invCloseMin, skinBusinessCaseId, null, null, houseResult, false, "asc", true, null, arabicStartDate, getWritersListToString(insertedFrom), 0, apiExternalUserId, copyopInvTypeId, fromUserId, id);
		} else {
			list = InvestmentsManager.getInvestmentsList(user.getId(), from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
					statusId, currencyId, getListToString(opportunitiesList), 0, ROWS_TO_XSL,getSkinFilter(),invType,CommonUtil.calcAmount(invAmount),
					CommonUtil.calcAmount(invAmountBelow),invCloseHour,invCloseMin, 0, null, null, houseResult, false, "asc", true, null, arabicStartDate, getWritersListToString(insertedFrom), 0, apiExternalUserId, copyopInvTypeId, fromUserId, id);
		}

		return list;
	}

	/**
	 * Export data to excel
	 * @throws IOException
	 */
	public String exportHtmlTableToExcel() throws IOException {

        //Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>"+htmlBuffer+"</html>";

        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "investments_report_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();

        return null;
    }


	/**
	 * Get page size
	 * @return
	 * 		page size for dataPage
	 */
	public int getPageSize() {
		int size = 0;     // default
		if ( null != dataPage ) {
			size = dataPage.getData().size();
		}
		return size;
	}

	/**
	 * Get data Page.
	 * Return DataPage object that represent page in the model.
	 * @param startRow
	 * 		the start row of this page relative to number of all rows
	 *      in the model.
	 * @param pageSize
	 * 		number of rows in the page.
	 * @return
	 * 		DataPage<Investment> instance.
	 */
    private DataPage<Investment> getDataPage(int startRow, int pageSize) {
        if (logger.isDebugEnabled()) {
        	logger.debug("getDataPage - startRow: " + startRow + " pageSize: " + pageSize);
        }
        if (null == dataPage || dataPage.getStartRow() != startRow
        		|| needUpdate == true ) {

            if (logger.isDebugEnabled()) {
            	logger.debug("loading page from db...");
            }

            try {

        		FacesContext context = FacesContext.getCurrentInstance();
        		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        		ArrayList baseTotals = null;
        		int modelSize = 0;
        		userName = userName.toUpperCase();

            	long userId = 0;
            	if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {   // for support
            		userId = user.getId();
            	}
            	if (!isDaily) {
            		baseTotals = InvestmentsManager.getBaseTotals(userId, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
            				statusId, currencyId, getListToString(opportunitiesList),getSkinFilter(),invType,CommonUtil.calcAmount(invAmount)
        									,CommonUtil.calcAmount(invAmountBelow),invCloseHour,invCloseMin, skinBusinessCaseId, null, null, 0, "asc", arabicStartDate, getWritersListToString(insertedFrom), apiExternalUserId, copyopInvTypeId, fromUserId, id);
            	} else {
            		setDates();
                    String useTime = null;
                    if (isSettled.equals("0")) {
                    	useTime = TIME_CREATED;
                    } else {
                    	useTime = TIME_SETTLED;
                    }
            		baseTotals = InvestmentsManager.getBaseTotals(userId, from, to, isSettled, "0,0", scheduled, useTime, userName.toUpperCase(), ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE,
            				statusId, NON_ACTIVE_FIELD, getListToString(opportunitiesList), getListToString(skins), invType, CommonUtil.calcAmount(invAmount),
							CommonUtil.calcAmount(invAmountBelow), invCloseHour, invCloseMin, NON_ACTIVE_FIELD, getListToString(groups), getListToString(markets), houseResult, "asc", null, getWritersListToString(insertedFrom), apiExternalUserId, copyopInvTypeId, fromUserId, id);
            	}
    			modelSize = ((Integer)baseTotals.get(4)).intValue(); // all rows
    			if ( modelSize > 0 ) {
    				int numPages = modelSize / (isDaily ? NUM_OF_ROWS_DAILY :user.getRowsByMenuInt());  // num pages for add totals rows untill now
    				if ( (modelSize % ((isDaily ? NUM_OF_ROWS_DAILY :user.getRowsByMenuInt()))) == 0 ) {
    					numPages--;
    				}
    				modelSize += numPages * TOTAL_ROWS;
    			}

        		long startRowSql = startRow;
            	if ( startRow > 0 ) {  // need to reduce total rows in all the pages befor, to get correct sql rowNum
            		int numPagesReduce = startRow / ((isDaily ? NUM_OF_ROWS_DAILY :user.getRowsByMenuInt()) + TOTAL_ROWS);
            		startRowSql -= (numPagesReduce * TOTAL_ROWS) ;
                    if (logger.isDebugEnabled()) {
                    	logger.debug("getDataPage - startRow: " + startRow + " startRowSql: " + startRowSql);
                    }
            	}

            	if (!isDaily) {
            		dataPage = InvestmentsManager.getInvestments(userId, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
            			statusId, currencyId, getListToString(opportunitiesList), startRow, startRowSql, pageSize - TOTAL_ROWS, modelSize, getSkinFilter(),
    					invType,CommonUtil.calcAmount(invAmount),CommonUtil.calcAmount(invAmountBelow), invCloseHour,invCloseMin, skinBusinessCaseId,
    					null, null, 0, false, "desc", arabicStartDate, getWritersListToString(insertedFrom), apiExternalUserId, copyopInvTypeId, fromUserId, id);

            	} else {
            		setDates();
                    String useTime = null;
                    if (isSettled.equals("0")) {
                    	useTime = TIME_CREATED;
                    } else {
                    	useTime = TIME_SETTLED;
                    }
            		dataPage = InvestmentsManager.getInvestments(0 , from, to, isSettled, "0,0", scheduled, useTime, userName.toUpperCase(), ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE,
            			statusId, NON_ACTIVE_FIELD, getListToString(opportunitiesList), startRow, startRowSql, pageSize - TOTAL_ROWS, modelSize,
    					getListToString(skins), invType,CommonUtil.calcAmount(invAmount),CommonUtil.calcAmount(invAmountBelow),	invCloseHour, invCloseMin, NON_ACTIVE_FIELD,
    					getListToString(groups), getListToString(markets), houseResult, true, "desc", null, getWritersListToString(insertedFrom), apiExternalUserId, copyopInvTypeId, fromUserId, id);
            	}

        		if ( dataPage.getData().size() > 0 ) {   // calculate totals if needed
        			calculateTotals(baseTotals);
        			sortList();

        		} else if ( startRow > 0 ) {
        			logger.debug("List Null - startRow: " + startRow + " startRowSql: " + startRowSql + " modelSize: " + modelSize);
        		}

        		needUpdate = false;

            } catch (Throwable t) {
            	logger.error("Can't load the page.", t);
            }
        } else {
            if (logger.isDebugEnabled()) {
            	logger.debug("Cached dataPage.");
            }
        }
        return dataPage;
    }

    public void csvExport(File fileToSave) throws Exception {
   	headersForCSVExport(fileToSave);

   	int curStartRow = 0;
   	int curEndRow = 100;

   	FacesContext context = FacesContext.getCurrentInstance();
   	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());
   	InvestmentsForm investForm = (InvestmentsForm) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_INVESTMENTS_FORM, InvestmentsForm.class).getValue(context.getELContext());

   	ArrayList<Investment> il = InvestmentsManager.getInvestmentsForDataPage(0, from, to, isSettled, marketGroup,
   		scheduled, timeType, userName, classId, statusId, currencyId, getListToString(opportunitiesList),
   		curStartRow, curEndRow, getSkinFilter(), invType, CommonUtil.calcAmount(invAmount),
   		CommonUtil.calcAmount(invAmountBelow), invCloseHour, invCloseMin, skinBusinessCaseId, null, null, 0,
   		false, "desc", arabicStartDate, getWritersListToString(insertedFrom), apiExternalUserId,
   		copyopInvTypeId, fromUserId, id);

   	// while il is not empty append info to a file, then display pop up to save file and would be deleted afterwards..
   	Writer output = null;
   	while (il != null && il.size() > 0) {
   	    try {
   		output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave, true), "UTF-8"));
   		//investmentsContentColumns.xhtml
   		for (Investment inv : il) {
   		    // investments.id.column -> ID
   		    if (InvestmentsManager.isBinary0100(inv)) {
   		    	ArrayList<Long> invIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(inv.getId());
	   			output.write(String.valueOf(invIds.get(invIds.size() - 1)));
	   			output.write(DELEMITER);
   		    } else {
   			output.write(String.valueOf(inv.getId()));
   			output.write(DELEMITER);
   		    }
   		    //investments.username -> User Name
   		    if ( (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() )
   			    || ( user.isSupportSelected() && user.isApiUser() )) {
   			if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() 
   				&& inv.getApiExternalUserId() == 0) {
   			    output.write(inv.getUserName() + " " + inv.getUserCountryCode());
   			    output.write(DELEMITER);
   			} else if ( (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() 
   				&& inv.getApiExternalUserId() > 0)
   				|| (user.isSupportSelected() && user.isApiUser() && (inv.getApiExternalUserId() > 0)) ) {
   			    output.write(inv.getApiExternalUserReference() + " " + inv.getUserName());
   			    output.write(DELEMITER);
   			}
   		    }
   		    // investments.marketname -> Market Name
   		    output.write(CommonUtil.appendDQ(inv.getMarketName()));
   		    output.write(DELEMITER);
		    if (!(InvestmentsManager.isBinary0100(inv))) {
			// investments.level -> Level
			output.write(CommonUtil.appendDQ(InvestmentFormatter.getCurrentLevelTxt(inv)));
			output.write(DELEMITER);
			// Type - e.g. PUT/Call and etc..+ copyOp values
			output.write(inv.getTypeName() + " " + (inv.getCopyOpTypeId() == 1 ? CopyOpInvTypeEnum.COPY
				: (inv.getCopyOpTypeId() == 2 ? CopyOpInvTypeEnum.FOLLOW : Constants.EMPTY_STRING)));
			output.write(DELEMITER);
		    } else if (InvestmentsManager.isBinary0100(inv)) {
			// investments.level -> Level
			output.write(CommonUtil.appendDQ(getOppCurrentLevelTxt(inv)));
			output.write(DELEMITER);
			// Type - e.g. PUT/Call and etc..+ copyOp values
			output.write(InvestmentsManager.getOpportunityTypeName(inv) + " " + (inv.getCopyOpTypeId() == 1
				? CopyOpInvTypeEnum.COPY
				: (inv.getCopyOpTypeId() == 2 ? CopyOpInvTypeEnum.FOLLOW : Constants.EMPTY_STRING)));
			output.write(DELEMITER);
		    }
   		    // investments.buy.sell -> Buy/Sell
   		    output.write(InvestmentsManager.isBinary0100(inv) ? inv.getTypeName() : Constants.EMPTY_STRING);
   		    output.write(DELEMITER);
   		    //investments.no.of.contract -> No. of contracts
   		    if (!InvestmentsManager.isBinary0100(inv)) {
   			output.write(Constants.EMPTY_STRING);
   			output.write(DELEMITER);
   		    } else if (InvestmentsManager.isBinary0100(inv) && user.isSupportSelected()) {
   			output.write(String.valueOf(inv.getInvestmentsCount()));
   			output.write(DELEMITER);
   		    } else if (InvestmentsManager.isBinary0100(inv) && !user.isSupportSelected()) {
   			output.write(String.valueOf(getBaseInvestmentsCount(inv)));
   			output.write(DELEMITER);
   		    }
   		    //investments.price -> Price
   		    if (!InvestmentsManager.isBinary0100(inv)) {
   			output.write(Constants.EMPTY_STRING);
   			output.write(DELEMITER);
   		    } else if (InvestmentsManager.isBinary0100(inv)) {
   			output.write(CommonUtil.appendDQ(InvestmentFormatter.getCurrentLevel0100BE(inv)));
   			output.write(DELEMITER);
   		    }
   		    // investments.reallevel -> Reuters
   		    if(user.isHasAdminRole() || user.isHasTraderRole()){
   			    output.write(CommonUtil.appendDQ(CommonUtil.formatRealLevelByMarket(inv.getRealLevel(), inv.getMarketId())));
   			    output.write(DELEMITER);
   		    }
		    // investments.wwwlevel -> Check
		    if (user.isHasAdminRole() || user.isHasTraderRole()) {
			output.write(
				InvestmentsManager.isBinary0100(inv) ? ConstantsBase.EMPTY_STRING : CommonUtil.appendDQ(InvestmentFormatter.getWwwLevelTxt(inv)));
			output.write(DELEMITER);
		    }
		    // investments.closinglevel -> Closing level
		    if (investForm.getIsSettledBoolean()) {
			output.write(CommonUtil.appendDQ(getClosingLevel0100BE(inv)));
			output.write(DELEMITER);
		    }
		    // investments.closing.price -> Closing Price
		    if (investForm.getIsSettledBoolean()) {
			output.write(
				InvestmentsManager.isBinary0100(inv) ? CommonUtil.appendDQ(InvestmentFormatter.getClosingPrice(inv)) : ConstantsBase.EMPTY_STRING);
			output.write(DELEMITER);
		    }
		    // investments.amount -> Amount
		    if (inv.getTotalLine() == 0 && !user.isSupportSelected() && !user.isRetentionSelected()
			    && !user.isRetentionMSelected()) {
			output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount(inv.getBaseAmount(),ConstantsBase.CURRENCY_BASE_ID)) : 
			    CommonUtil.appendDQ(CommonUtil.displayAmount(inv.getAmount(), inv.getCurrencyId())));
			output.write(DELEMITER);
		    }
		    // investments.if.above.customer -> if above (amount added to customer's balance)
		    if (!investForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected()
			    || user.isRetentionMSelected())) {
			if (inv.getTotalLine() == 0) {
			    if (InvestmentsManager.isBinary0100(inv)) {
				if (isTypeBinary0100Above(inv)) {
				    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpCustomerTxt(inv)));
				    output.write(DELEMITER);
				} else if (isTypeBinary0100Below(inv)) {
				    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseLoseDownTxt(inv)));
				    output.write(DELEMITER);
				}
			    } else if (!InvestmentsManager.isBinary0100(inv)) {
				output.write(CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(inv)), inv.getCurrencyId())));
				output.write(DELEMITER);
			    }
			}
		    }		    
		    // investments.if.above -> if above (how much house win)
		    if(!investForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected()
			    || user.isRetentionMSelected())) {
			if (inv.getTotalLine() == 0) {
			    if (InvestmentsManager.isBinary0100(inv)) {
				if (isTypeBinary0100Above(inv)) {
				    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseWinUpTxt(inv)) : 
					    CommonUtil.appendDQ((InvestmentFormatter.getBinary0100WinUpTxt(inv))));
					output.write(DELEMITER);
				    }
				} else if (isTypeBinary0100Below(inv)) {
				    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseRefundDownTxt(inv))
						: CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownTxt(inv)));
					output.write(DELEMITER);
				    }
				}
			    } else if (!InvestmentsManager.isBinary0100(inv)) {
				if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
				    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundUp(inv) - inv.getBaseAmount()) *(-1), ConstantsBase.CURRENCY_BASE_ID)) : 
					CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(inv) - inv.getAmount()) * (-1), inv.getCurrencyId())));
				    output.write(DELEMITER);
				}
			    }
			}
		    }
		    //investments.if.below.customer -> if below (amount added to customer's balance)
		    if(!investForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
			if (inv.getTotalLine() == 0) {
			    if (InvestmentsManager.isBinary0100(inv)) {
				if (isTypeBinary0100Above(inv)) {
				    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownCustomerTxt(inv)));
				    output.write(DELEMITER);
				} else if (isTypeBinary0100Below(inv)) {
				    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpCustomerTxt(inv)));
				    output.write(DELEMITER);
				}
			    } else if (!InvestmentsManager.isBinary0100(inv)) {
				output.write(CommonUtil.appendDQ(CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(inv), inv.getCurrencyId())));
				output.write(DELEMITER);
			    }
			}
		    }
		    //investments.if.below -> if below (how much house win)
		    if(!investForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
			if(inv.getTotalLine() == 0) {
			    if(InvestmentsManager.isBinary0100(inv) && (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected())) {
				if(isTypeBinary0100Above(inv)) {
				    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseLoseDownTxt(inv))
					    : CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownTxt(inv)));
				    output.write(DELEMITER);
				}else if(isTypeBinary0100Below(inv)) {
				    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseWinUpTxt(inv))
					    : CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpTxt(inv)));
				    output.write(DELEMITER);
				}
				
			    }else if(!InvestmentsManager.isBinary0100(inv)) {
				if(!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
				    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundDown(inv) - inv.getBaseAmount()) * (-1), ConstantsBase.CURRENCY_BASE_ID))
					    : CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundDown(inv) - inv.getAmount()) * (-1), inv.getCurrencyId())));
				    output.write(DELEMITER);
				}
			    }
			}
		    }
		    //investments.winlose.customer -> customer's return to balance
		    if(investForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
			if (inv.getTotalLine() == 0 && !InvestmentsManager.isBinary0100(inv)) {
				if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
				    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getCustomerBaseWinLoseTxt(inv))
					    : CommonUtil.appendDQ(InvestmentFormatter.getCustomerUserWinLoseTxt(inv)));
				    output.write(DELEMITER);
				}
			} else if (inv.getTotalLine() == 0 && InvestmentsManager.isBinary0100(inv)) {
			    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
				output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getCustomerBaseWinLoseTxt0100(inv))
					: CommonUtil.appendDQ(InvestmentFormatter.getCustomerUserWinLoseTxt0100(inv)));
				output.write(DELEMITER);
			    }
			}	
		    }
		    //investments.winLose -> Win\\Lose
		    if(investForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
			if (inv.getTotalLine() == 0 && !InvestmentsManager.isBinary0100(inv)) {
			    if(!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
				output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getBaseWinLoseTxt(inv))
					: CommonUtil.appendDQ(InvestmentFormatter.getUserWinLoseTxt(inv)));
				output.write(DELEMITER);
			    }
			}
			else if(inv.getTotalLine() == 0 && InvestmentsManager.isBinary0100(inv)) {
			    output.write(investForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getBaseWinLoseTxt0100(inv))
				    : CommonUtil.appendDQ(InvestmentFormatter.getUserWinLoseTxt0100(inv)));
			    output.write(DELEMITER);
			}
		    }   
		    // investments.timecreated -> Time created
		    output.write(CommonUtil.getDateAndTimeFormat(inv.getTimeCreated()));
		    output.write(DELEMITER);
		    //investments.est.closing -> Est. Time Closing
		    if (!investForm.getIsSettledBoolean()) {
			if (inv.getTotalLine() == 0 && !(InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus())) {
			    output.write((InvestmentFormatter.getTimeEstClosingTxt(inv)).replaceAll("&nbsp;", " "));
			    output.write(DELEMITER);
			} else if (inv.getTotalLine() == 0 && (InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus())) {
			    output.write((InvestmentFormatter.getTimeSettledTxt(inv)).replaceAll("&nbsp;", " "));
			    output.write(DELEMITER);
			}
		    } else if (investForm.getIsSettledBoolean()) {
			if (inv.getTotalLine() == 0
				&& !(InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus() || InvestmentsManager.isBinary0100(inv))) {
			    output.write((InvestmentFormatter.getTimeEstClosingTxt(inv)).replaceAll("&nbsp;", " "));
			    output.write(DELEMITER);
			} else if (inv.getTotalLine() == 0
				&& (InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus() || InvestmentsManager.isBinary0100(inv))) {
			    output.write((InvestmentFormatter.getTimeSettledTxt(inv)).replaceAll("&nbsp;", " "));
			    output.write(DELEMITER);
			}
		    }	   
		    // investments.ip -> IP address
		    output.write(inv.getIp());
		    output.write(DELEMITER);
		    // investments.insertedFrom -> Inserted from
		    output.write(inv.getWriterUserName());
		    output.write(DELEMITER);
		    // investments.balance -> balance
		    if (inv.getTotalLine() == 0 && isDaily) {
			output.write(
				inv.getApiExternalUserId() == 0 ? getUserBalanceTxt(inv) : Constants.EMPTY_STRING);
			output.write(DELEMITER);
		    }
		    output.write("\n");
		}
		curStartRow = curEndRow;
		curEndRow += curEndRow;
		il = InvestmentsManager.getInvestmentsForDataPage(0, from, to, isSettled, marketGroup, scheduled,
			timeType, userName, classId, statusId, currencyId, getListToString(opportunitiesList),
			curStartRow, curEndRow, getSkinFilter(), invType, CommonUtil.calcAmount(invAmount),
			CommonUtil.calcAmount(invAmountBelow), invCloseHour, invCloseMin, skinBusinessCaseId, null,
			null, 0, false, "desc", arabicStartDate, getWritersListToString(insertedFrom),
			apiExternalUserId, copyopInvTypeId, fromUserId, id);		
   	    } catch (Exception e) {
   		logger.error("Error when exporting CSV Data file:", e);
   	    } finally {
   		try {
   		    output.flush();
   		    output.close();
   		} catch (Exception e) {
   		    logger.error("Investment Data Form Error, Can't close file:", e);
   		}
   	    }
   	}
    }

    public void headersForCSVExport(File fileToSave) throws IOException {
	Writer output = null;
	
   	FacesContext context = FacesContext.getCurrentInstance();
   	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());
   	InvestmentsForm investForm = (InvestmentsForm) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_INVESTMENTS_FORM, InvestmentsForm.class).getValue(context.getELContext());
   	
	logger.debug("Trying to write in CSV File...");
	try {
	    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), "UTF-8"));
	    output.write("Investment_ID");	// 1 -> investments.id.column
	    output.write(DELEMITER);
	    if ((!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected())
		    || (user.isSupportSelected() && user.isApiUser())) {
		output.write("User_Name");	// 2 -> investments.username
		output.write(DELEMITER);
	    }	   
	    output.write("Market");		// 3 -> investments.marketname
	    output.write(DELEMITER);
	    output.write("Level");		// 4 -> investments.level
	    output.write(DELEMITER);
	    output.write("Type");		// 5 -> type
	    output.write(DELEMITER);
	    output.write("Buy/Sell");		// 6 -> investments.buy.sell
	    output.write(DELEMITER);
	    output.write("No. of Contracts");	// 7 -> investments.no.of.contract
	    output.write(DELEMITER);
	    output.write("Price");		// 8 -> investments.price
	    output.write(DELEMITER);
	    if (user.isHasAdminRole() || user.isHasTraderRole()) {
		output.write("Reuters"); 	// 9 -> investments.reallevel
		output.write(DELEMITER);
	    }
	    if (user.isHasSAdminRole() || user.isHasTraderRole()) {
		output.write("Check"); 		// 10 -> investments.wwwlevel
		output.write(DELEMITER);
	    }
	    if(investForm.getIsSettledBoolean()) { 	// 11 -> investments.closinglevel
		output.write("Closing Level");
		output.write(DELEMITER);
		output.write("Closing Price");		// 12 -> investments.closing.price
		output.write(DELEMITER);
	    }
	    output.write("Amount");			// 13 -> investments.amount
	    output.write(DELEMITER);
	    if (!investForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If above (amount added to customer's balance)");	// 14 -> investments.if.above.customer
		output.write(DELEMITER);
	    }
	    if (!investForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If above (how much house win)");			// 15 -> //investments.if.above
		output.write(DELEMITER);
	    }    
	    if (!investForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If below (amount added to customer's balance)");	// 16 -> investments.if.below.customer
		output.write(DELEMITER);
	    }
	    if (!investForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If below (how much house win)");	// 17 -> investments.if.below
		output.write(DELEMITER);
	    }
	    if (investForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("customer's return to Balance"); // 18 -> investments.winlose.customer
		output.write(DELEMITER);
	    }
	    if (investForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("Win/Lose"); // 19 -> investments.winLose
		output.write(DELEMITER);
	    }
	    output.write("Time Created");	// 20 -> investments.timecreated
	    output.write(DELEMITER);
	    output.write("Est. Time Closing");	// 21 -> investments.est.closing
	    output.write(DELEMITER);
	    output.write("IP address");		// 22 -> investments.ip
	    output.write(DELEMITER);
	    output.write("Inserted From");	// 23 -> investments.insertedFrom
	    output.write(DELEMITER);
	    if (isDaily) {
		output.write("balance"); 	// 24 -> investments.balance
		output.write(DELEMITER);
	    }
	    output.write("\n");
	} catch (Exception e) {
	    logger.error("Error when exporting CSV file:", e);
	} finally {
	    try {
		output.flush();
		output.close();
	    } catch (Exception e) {
		logger.error("Investment header Data Form Error, Can't close file:", e);
	    }
	}
    }
    
    public String exportTableToCSV() throws IOException, NumberFormatException, SQLException {

	// Set the filename
	Date dt = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	String filename = "investments_report_" + fmt.format(dt) + ".csv";

	// Setup the output
	String contentType = "text/csv";

	File tCSVfile = new File(CommonUtil.getProperty(Constants.FILES_PATH) + filename);
	FacesContext fc = FacesContext.getCurrentInstance();
	ExternalContext ex = fc.getExternalContext();
	HttpServletResponse response = (HttpServletResponse) ex.getResponse();
	try {
	    try {
		csvExport(tCSVfile);
	    } catch (Exception e) {
		logger.error("Problem creating CSV file due to: " + e);
		e.printStackTrace();
		return null;
	    }

	    int contentLength = (int) tCSVfile.length();
	    
	    // just making sure nothing non current is retrieved
	    response.reset();
	    
	    response.setContentType(contentType);
	    response.setContentLength(contentLength);
	    response.setCharacterEncoding("UTF-8");
	    response.setHeader("Content-disposition", "attachment; filename=" + filename);	   
	   
	    // Write the file back out...
	    OutputStream outputStream = response.getOutputStream();
	    FileInputStream fis = new FileInputStream(tCSVfile);

	    try {
		int n = 0;
		byte[] buffer = new byte[4 * 1024];
		while ((n = fis.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, n);
		}
		outputStream.flush();
	    } finally {
		fis.close();
		outputStream.close();
	    }
	} finally {
	    tCSVfile.delete();
	}

	fc.responseComplete();

	return null;
    }

       /**
        * Calculate all totals for page and model
        * 
        * @param baseTotals
        *            ArrayList with all totals of the model(not page) Note: we set
        *            totals and Base totals (e.g amount and baseAmount), the amout
        *            in use when currency of all investments is equal the
        *            baseAmount in use when currency of all investments is not
        *            equal
        */
       public void calculateTotals(ArrayList baseTotals) {

   	ArrayList<Investment> currentList = (ArrayList<Investment>) dataPage.getData();
   	ArrayList<Investment> newList = new ArrayList(currentList);

   	// The page total line (page investments)
   	InvestmentTotal pageTotalInv = new InvestmentTotal(INVESTMENT_PAGE_SUMMARY_LINE, new Date(),
   		Utils.getUser().getUtcOffset(), 0, currencyId, 0);

   	// the total line (model investments)
   	InvestmentTotal totalInv = new InvestmentTotal(INVESTMENT_ALL_PAGES_SUMMARY_LINE, new Date(),
   		Utils.getUser().getUtcOffset(), 0, currencyId, 0);

   	// set totals of the model
   	totalInv.setTotalAmount((Long) baseTotals.get(0));
   	totalInv.setTotalRefund((Long) baseTotals.get(1));
   	totalInv.setBaseTotalAmount((Long) baseTotals.get(2));
   	totalInv.setBaseTotalRefund((Long) baseTotals.get(3));
   	totalInv.setTotalPrice((Double) baseTotals.get(5));
   	totalInv.setTotalNumOfContracts((Long) baseTotals.get(6));
   	totalInv.setTotalWinLose((Long) baseTotals.get(7));
   	totalInv.setBaseTotalWinLose((Long) baseTotals.get(8));
   	/*
   	 * totalInv.setTotalAbove((Double)baseTotals.get(7));
   	 * totalInv.setTotalBelow((Double)baseTotals.get(8));
   	 * totalInv.setBaseTotalAbove((Double)baseTotals.get(9));
   	 * totalInv.setBaseTotalBelow((Double)baseTotals.get(10));
   	 */

   	// calculate page totals
   	for (Investment inv : currentList) {
   	    pageTotalInv.setTotalAmount(pageTotalInv.getTotalAmount() + inv.getAmount());
   	    pageTotalInv.setTotalRefund(pageTotalInv.getTotalRefund() + InvestmentsManagerBase.getRefund(inv));
   	    pageTotalInv.setBaseTotalAmount(pageTotalInv.getBaseTotalAmount() + inv.getBaseAmount());
   	    pageTotalInv.setBaseTotalRefund(pageTotalInv.getBaseTotalRefund() + InvestmentsManagerBase.getBaseRefund(inv));
   	    pageTotalInv.setTotalPrice(pageTotalInv.getTotalPrice() + InvestmentsManagerBase.getCurrentLevel0100Total(inv));
   	    if (isBinary0100(inv)) {
   		pageTotalInv.setTotalNumOfContracts(pageTotalInv.getTotalNumOfContracts() + inv.getInvestmentsCount());
   	    }
   	    pageTotalInv.setTotalWinLose(pageTotalInv.getTotalWinLose() + InvestmentsManagerBase.getUserWinLose(inv) * (-1));
   	    pageTotalInv.setBaseTotalWinLose(pageTotalInv.getBaseTotalWinLose() + InvestmentsManagerBase.getBaseUserWinLose(inv) * (-1));
   	    /*
   	     * pageTotalInv.setTotalAbove(pageTotalInv.getTotalAbove());
   	     * pageTotalInv.setTotalBelow(pageTotalInv.getTotalBelow());
   	     * pageTotalInv.setBaseTotalAbove(pageTotalInv.getBaseTotalAbove());
   	     * pageTotalInv.setBaseTotalBelow(pageTotalInv.getBaseTotalBelow());
   	     */

   	}

   	// create 4 totals lines
   	Investment invSep1 = new Investment(INVESTMENT_SEPERATOR_LINE);
   	invSep1.setId(1);
   	pageTotalInv.setId(2);
   	Investment invSep2 = new Investment(INVESTMENT_SEPERATOR_LINE);
   	invSep2.setId(3);
   	totalInv.setId(4);
   	newList.add(invSep1);
   	newList.add(pageTotalInv);
   	newList.add(invSep2);
   	newList.add(totalInv);

   	int newDataSetSize = dataPage.getDatasetSize() + TOTAL_ROWS;
   	int newStartRow = dataPage.getStartRow();
   	logger.debug("Add total rows - model size: " + newDataSetSize);
   	dataPage = new DataPage<Investment>(newDataSetSize, newStartRow, newList);

       }

       /**
        * Check if need to show scroller in the current page.
        * 
        * @return true - if scroller needed
        */
       public boolean isScrollerNeeded() {
   	FacesContext context = FacesContext.getCurrentInstance();
   	User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
   	int pageRows = dataPage.getData().size();
   	if (getListSize() > ((isDaily ? NUM_OF_ROWS_DAILY : user.getRowsByMenuInt()) + TOTAL_ROWS)) {
   	    return true;
   	}
   	return false;
       }

       /**
        * Get DataModel instance
        * 
        * @return investments
        */
       public DataModel getDataModel() {

   	if (logger.isDebugEnabled()) {
   	    logger.debug("getDataModel");
   	}

   	if (null == investments || needUpdate == true) { // on creation and
   							 // filtering

   	    // if ( null != investments && investments.getRowCount() > 0 &&
   	    // null != dataPage) { // save last investments id
   	    //
   	    // ArrayList<Investment> currentList =
   	    // (ArrayList<Investment>)dataPage.getData();
   	    // int idx = currentList.size() - 1 - TOTAL_ROWS;
   	    // lastInvestmentId = ((Investment)currentList.get(idx)).getId() ;
   	    // }

   	    FacesContext context = FacesContext.getCurrentInstance();
   	    User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
   	    investments = new LocalDataModel((isDaily ? NUM_OF_ROWS_DAILY : user.getRowsByMenuInt()) + TOTAL_ROWS);

   	    if (needUpdate == true) { // set to first page
   		CommonUtil.setTablesToFirstPage();
   	    }
   	    // else { // more then one page: set to the last page
   	    // int modelSize = getListSize();
   	    // int numPages = modelSize / (user.getRowsByMenuInt() +
   	    // TOTAL_ROWS);
   	    // if ( (modelSize % (user.getRowsByMenuInt() + TOTAL_ROWS )) == 0 )
   	    // {
   	    // numPages--;
   	    // }
   	    // int lastPageIndex = numPages * (user.getRowsByMenuInt() +
   	    // TOTAL_ROWS);
   	    // resetScrollerIndex(lastPageIndex);
   	    // }
   	}

   	return investments;
       }

       /**
        * Get LocalDataModel class. This inner class extends from
        * PagedListDataModel and implements the fetchPage function for getting
        * page. This is done for paging issues, each page get his data from Db.
        * 
        * @author Kobi
        */
       private class LocalDataModel extends PagedListDataModel<Investment> {
   	public LocalDataModel(int pageSize) {
   	    super(pageSize);
   	}

   	@Override
   	public DataPage<Investment> fetchPage(int startRow, int pageSize) {
   	    // call enclosing managed bean method to fetch the data
   	    return getDataPage(startRow, pageSize);
   	}
       }

       public int getSkinId() {
   	return skinId;
       }

       public void setSkinId(int skinId) {
   	this.skinId = skinId;
       }

       public String getSkinFilter() {
   	if (skinId == Skin.SKINS_ALL_VALUE) {
   	    return Utils.getWriter().getSkinsToString();
   	} else {
   	    return String.valueOf(skinId);
   	}
       }

       public ArrayList<SelectItem> getClosingHourSI() {
   	return closingHourSI;
       }

       public void setClosingHourSI(ArrayList<SelectItem> closingHourSI) {
   	this.closingHourSI = closingHourSI;
       }

       public String getClosingHour() {
   	return closingHour;
       }

       public void setClosingHour(String closingHour) {
   	this.closingHour = closingHour;
       }

       public ArrayList<SelectItem> getInvestmentTypeSI() {
   	return investmentTypeSI;
       }

       public void setInvestmentTypeSI(ArrayList<SelectItem> investmentTypeSI) {
   	this.investmentTypeSI = investmentTypeSI;
       }

       public long getInvType() {
   	return invType;
       }

       public void setInvType(long invType) {
   	this.invType = invType;
       }

       public long getInvAmount() {
   	return invAmount;
       }

       public void setInvAmount(long invAmount) {
   	this.invAmount = invAmount;
       }

       public long getInvAmountBelow() {
   	return invAmountBelow;
       }

       public void setInvAmountBelow(long invAmountBelow) {
   	this.invAmountBelow = invAmountBelow;
       }

       public long getInvCloseHour() {
   	return invCloseHour;
       }

       public void setInvCloseHour(long invCloseHour) {
   	this.invCloseHour = invCloseHour;
       }

       public long getInvCloseMin() {
   	return invCloseMin;
       }

       public void setInvCloseMin(long invCloseMin) {
   	this.invCloseMin = invCloseMin;
       }

       public long getShowAdvanced() {
   	return showAdvanced;
       }

       public void setShowAdvanced(long showAdvanced) {
   	this.showAdvanced = showAdvanced;
       }

       /**
        * @return the skinBusinessCaseId
        */
       public long getSkinBusinessCaseId() {
   	return skinBusinessCaseId;
       }

       /**
        * @param skinBusinessCaseId
        *            the skinBusinessCaseId to set
        */
       public void setSkinBusinessCaseId(long skinBusinessCaseId) {
   	this.skinBusinessCaseId = skinBusinessCaseId;
       }

       /**
        * Get writers list to string
        * 
        * @param list
        * @return
        */
       public String getWritersListToString(List list) {
   	if (list == null || list.size() == 0) {
   	    if (null != list) {
   		list.add("-1");
   	    }
   	    return "-1";
   	}

   	String tmp = ConstantsBase.EMPTY_STRING;
   	for (int i = 0; i < list.size(); i++) {
   	    tmp += list.get(i) + ",";
   	}
   	return tmp.substring(0, tmp.length() - 1);
       }

       /**
        * Get list (groups, skins and markets) to string
        * 
        * @param list
        * @return
        */
       public String getListToString(List list) {
   	if ((list == skins) && (list == null || list.size() == 0 || list.get(0).equals("0"))) {
   	    return Utils.getWriter().getSkinsToString();
   	} else if (list == null || list.size() == 0) {
   	    if (null != list) {
   		list.add("0");
   	    }
   	    return "0";
   	}

   	String tmp = ConstantsBase.EMPTY_STRING;
   	for (int i = 0; i < list.size(); i++) {
   	    tmp += list.get(i) + ",";
   	}
   	return tmp.substring(0, tmp.length() - 1);
       }

       public List<String> getOpportunitiesList() {
   	return opportunitiesList;
       }

       public void setOpportunitiesList(List<String> opportunitiesList) {
   	this.opportunitiesList = opportunitiesList;
       }

       /**
        * @return the insertedFrom
        */
       public List<String> getInsertedFrom() {
   	return insertedFrom;
       }

       /**
        * @param insertedFrom
        *            the insertedFrom to set
        */
       public void setInsertedFrom(List<String> insertedFrom) {
   	this.insertedFrom = insertedFrom;
       }

       public long getFromHour() {
   	return fromHour;
       }

       public void setFromHour(long fromHour) {
   	this.fromHour = fromHour;
       }

       public long getFromMin() {
   	return fromMin;
       }

       public void setFromMin(long fromMin) {
   	this.fromMin = fromMin;
       }

       public List<String> getGroups() {
   	return groups;
       }

       public void setGroups(List<String> groups) {
   	this.groups = groups;
       }

       public int getHouseResult() {
   	return houseResult;
       }

       public void setHouseResult(int houseResult) {
   	this.houseResult = houseResult;
       }

       public ArrayList<SelectItem> getMarketsList() {
   	return marketsList;
       }

       public void setMarketsList(ArrayList<SelectItem> marketsList) {
   	this.marketsList = marketsList;
       }

       public List<String> getSkins() {
   	return skins;
       }

       public void setSkins(List<String> skins) {
   	this.skins = skins;
       }

       public long getToHour() {
   	return toHour;
       }

       public void setToHour(long toHour) {
   	this.toHour = toHour;
       }

       public long getToMin() {
   	return toMin;
       }

       public void setToMin(long toMin) {
   	this.toMin = toMin;
       }

       public List<String> getMarkets() {
   	return markets;
       }

       public void setMarkets(List<String> markets) {
   	this.markets = markets;
       }

       public void setDates() {
   	GregorianCalendar gc = new GregorianCalendar();
   	gc.setTime(CommonUtil.getDateTimeFormaByTz(gc.getTime(), Utils.getWriter().getUtcOffset()));
   	if (99 == fromHour || 99 == fromMin) {
   	    // set time as 00:00:00
   	    gc.set(Calendar.HOUR_OF_DAY, 0);
   	    gc.set(Calendar.MINUTE, 0);
   	    gc.set(Calendar.SECOND, 0);
   	    gc.set(Calendar.MILLISECOND, 0);
   	    from = gc.getTime();
   	} else {
   	    gc.set(Calendar.HOUR_OF_DAY, (int) fromHour);
   	    gc.set(Calendar.MINUTE, (int) fromMin);
   	    gc.set(Calendar.SECOND, 0);
   	    gc.set(Calendar.MILLISECOND, 0);
   	    from = gc.getTime();
   	}
   	if (99 == toHour || 99 == toMin) {
   	    // set time as 02:00:00 to next day
   	    gc.set(Calendar.HOUR_OF_DAY, 23);
   	    gc.set(Calendar.MINUTE, 59);
   	    gc.set(Calendar.SECOND, 59);
   	    gc.set(Calendar.MILLISECOND, 999);
   	    to = gc.getTime();
   	} else {
   	    gc.set(Calendar.HOUR_OF_DAY, (int) toHour);
   	    gc.set(Calendar.MINUTE, (int) toMin);
   	    gc.set(Calendar.SECOND, 0);
   	    gc.set(Calendar.MILLISECOND, 0);
   	    to = gc.getTime();
   	}
       }

       /**
        * Change
        * 
        * @param event
        * @throws SQLException
        * @throws ParseException
        */
       public void updateMarkets(ValueChangeEvent event) throws SQLException, ParseException {
   	marketsList.clear();
   	markets.clear();
   	markets.add("0");
   	groups = (List<String>) event.getNewValue();
   	ArrayList<SelectItem> si;
   	if (groups.contains("0")) {
   	    si = CommonUtil.map2SelectList(Utils.getWriter().getActiveMarketsAndGroups());
   	} else {
   	    si = new ArrayList<SelectItem>();
   	    si = TraderManager.getMarketsListById(getListToString(groups));
   	    si = WriterWrapper.translateSI(si);
   	    Collections.sort(si, new CommonUtil.selectItemComparator());
   	}
   	marketsList = si;
       }

       /**
        * Change settled radio button
        * 
        * @param event
        * @throws SQLException
        * @throws ParseException
        */
       public void updateChange(ValueChangeEvent event) throws SQLException, ParseException {
   	String newIsSettled = (String) event.getNewValue();
   	if (!isSettled.equals(newIsSettled)) {
   	    isSettled = newIsSettled;
   	    if (isSettled.equals("0")) {
   		houseResult = 0;
   	    }
   	    updateList();
   	}

       }

       public int getNUM_OF_ROWS_DAILY() {
   	return NUM_OF_ROWS_DAILY;
       }

       /************************************************* SORTING ************************************************************/

       /**
        * @return the prevSortColumn
        */
       public String getPrevSortColumn() {
   	return prevSortColumn;
       }

       /**
        * @param prevSortColumn
        *            the prevSortColumn to set
        */
       public void setPrevSortColumn(String prevSortColumn) {
   	this.prevSortColumn = prevSortColumn;
       }

       /**
        * @return the sortAscending
        */
       public boolean isSortAscending() {
   	return sortAscending;
       }

       /**
        * @param sortAscending
        *            the sortAscending to set
        */
       public void setSortAscending(boolean sortAscending) {
   	this.sortAscending = sortAscending;
       }

       /**
        * @return the sortColumn
        */
       public String getSortColumn() {
   	return sortColumn;
       }

       /**
        * @param sortColumn
        *            the sortColumn to set
        */
       public void setSortColumn(String sortColumn) {
   	this.sortColumn = sortColumn;
       }

       /**
        * Sort list
        * 
        * @return
        */
       public String sortList() {
   	if (activateAscending) {
   	    if (sortColumn.equals(prevSortColumn)) {
   		sortAscending = !sortAscending;
   	    } else {
   		sortAscending = true;
   	    }
   	}

   	activateAscending = true;
   	prevSortColumn = sortColumn;

   	if (sortColumn.equals("id")) {
   	    Collections.sort(dataPage.getData(), new IdComparator(sortAscending));
   	}

   	if (sortColumn.equals("marketName")) {
   	    Collections.sort(dataPage.getData(), new MarketNameComparator(sortAscending));
   	}

   	if (sortColumn.equals("level")) {
   	    Collections.sort(dataPage.getData(), new LevelComparator(sortAscending));
   	}

   	if (sortColumn.equals("wwwLevel")) {
   	    Collections.sort(dataPage.getData(), new WwwLevelComparator(sortAscending));
   	}

   	if (sortColumn.equals("estClosingTime")) {
   	    Collections.sort(dataPage.getData(), new TimeEstClosingComparator(sortAscending));
   	}

   	if (sortColumn.equals("realLevel")) {
   	    Collections.sort(dataPage.getData(), new RealLevelComparator(sortAscending));
   	}

   	if (sortColumn.equals("amount")) {
   	    Collections.sort(dataPage.getData(), new AmountComparator(sortAscending));
   	}

   	if (sortColumn.equals("timeCreated")) {
   	    Collections.sort(dataPage.getData(), new TimeCreatedComparator(sortAscending));
   	}

   	if (sortColumn.equals("userName")) {
   	    Collections.sort(dataPage.getData(), new UserNameComparator(sortAscending));
   	}

   	if (sortColumn.equals("return")) {
   	    Collections.sort(dataPage.getData(), new ReturnComparator(sortAscending));
   	}

   	if (sortColumn.equals("winlose")) {
   	    Collections.sort(dataPage.getData(), new WinLoseComparator(sortAscending));
   	}

   	if (sortColumn.equals("oppLevelBinary0100")) {
   	    Collections.sort(dataPage.getData(), new oppLevelBinary0100Comparator(sortAscending));
   	}

   	if (sortColumn.equals("levelBinary0100")) {
   	    Collections.sort(dataPage.getData(), new levelBinary0100Comparator(sortAscending));
   	}

   	// CommonUtil.setTablesToFirstPage();
   	return null;
       }

       private class IdComparator implements Comparator {
   	private boolean ascending;

   	public IdComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (ascending) {
   		return new Long(inv1.getId()).compareTo(new Long(inv2.getId()));
   	    }
   	    return -new Long(inv1.getId()).compareTo(new Long(inv2.getId()));
   	}
       }

       private class MarketNameComparator implements Comparator {
   	private boolean ascending;

   	public MarketNameComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return inv1.getMarketName().compareTo(inv2.getMarketName());
   	    }
   	    return -inv1.getMarketName().compareTo(inv2.getMarketName());
   	}
       }

       private class LevelComparator implements Comparator {
   	private boolean ascending;

   	public LevelComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
   	    }
   	    return -InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
   	}
       }

       private class WwwLevelComparator implements Comparator {
   	private boolean ascending;

   	public WwwLevelComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return InvestmentFormatter.getWwwLevelTxt(inv1).compareTo(InvestmentFormatter.getWwwLevelTxt(inv2));
   	    }
   	    return -InvestmentFormatter.getWwwLevelTxt(inv1).compareTo(InvestmentFormatter.getWwwLevelTxt(inv2));
   	}
       }

       private class TimeEstClosingComparator implements Comparator {
   	private boolean ascending;

   	public TimeEstClosingComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return inv1.getTimeEstClosing().compareTo(inv2.getTimeEstClosing());
   	    }
   	    return -inv1.getTimeEstClosing().compareTo(inv2.getTimeEstClosing());
   	}
       }

       private class RealLevelComparator implements Comparator {
   	private boolean ascending;

   	public RealLevelComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return new Double(inv1.getRealLevel()).compareTo(new Double(inv2.getRealLevel()));
   	    }
   	    return -new Double(inv1.getRealLevel()).compareTo(new Double(inv2.getRealLevel()));
   	}
       }

       private class AmountComparator implements Comparator {
   	private boolean ascending;

   	public AmountComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return new Long(inv1.getBaseAmount()).compareTo(new Long(inv2.getBaseAmount()));
   	    }
   	    return -new Long(inv1.getBaseAmount()).compareTo(new Long(inv2.getBaseAmount()));
   	}
       }

       private class ReturnComparator implements Comparator {
   	private boolean ascending;

   	public ReturnComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return new Long(InvestmentsManagerBase.getBaseRefund(inv1)).compareTo(new Long(InvestmentsManagerBase.getBaseRefund(inv2)));
   	    }
   	    return -new Long(InvestmentsManagerBase.getBaseRefund(inv1)).compareTo(new Long(InvestmentsManagerBase.getBaseRefund(inv2)));
   	}
       }

       private class WinLoseComparator implements Comparator {
   	private boolean ascending;

   	public WinLoseComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return new Long(InvestmentsManager.getBaseWinLose(inv1)).compareTo(new Long(InvestmentsManager.getBaseWinLose(inv2)));
   	    }
   	    return -new Long(InvestmentsManager.getBaseWinLose(inv1)).compareTo(new Long(InvestmentsManager.getBaseWinLose(inv2)));
   	}
       }

       private class TimeCreatedComparator implements Comparator {
   	private boolean ascending;

   	public TimeCreatedComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return new Date(inv1.getTimeCreated().getTime()).compareTo(new Date(inv2.getTimeCreated().getTime()));
   	    }
   	    return -new Date(inv1.getTimeCreated().getTime()).compareTo(new Date(inv2.getTimeCreated().getTime()));
   	}
       }

       private class UserNameComparator implements Comparator {
   	private boolean ascending;

   	public UserNameComparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return inv1.getUserName().compareTo(inv2.getUserName());
   	    }
   	    return -inv1.getUserName().compareTo(inv2.getUserName());
   	}
       }

       /**
        * @author lior Sort by opportunity current level.
        */
       private class oppLevelBinary0100Comparator implements Comparator {
   	private boolean ascending;

   	public oppLevelBinary0100Comparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return getOppCurrentLevelTxt(inv1).compareTo(getOppCurrentLevelTxt(inv2));
   	    }
   	    return -getOppCurrentLevelTxt(inv1).compareTo(getOppCurrentLevelTxt(inv2));
   	}
       }

       /**
        * @author lior Sort by current level.
        */
       private class levelBinary0100Comparator implements Comparator {
   	private boolean ascending;

   	public levelBinary0100Comparator(boolean asc) {
   	    ascending = asc;
   	}

   	@Override
   	public int compare(Object o1, Object o2) {
   	    Investment inv1 = (Investment) o1;
   	    Investment inv2 = (Investment) o2;
   	    if (null == inv1.getMarketName() || null == inv2.getMarketName()) {
   		if (null == inv1.getMarketName() && null != inv2.getMarketName()) {
   		    return 1;
   		}
   		if (null == inv2.getMarketName() && null != inv1.getMarketName()) {
   		    return -1;
   		}
   		if (inv1.getId() > inv2.getId()) {
   		    return 1;
   		} else if (inv1.getId() < inv2.getId()) {
   		    return -1;
   		} else {
   		    return 0;
   		}
   	    }
   	    if (ascending) {
   		return InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
   	    }
   	    return -InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
   	}
       }

       /*************************************************
        * END SORTING
        ********************************************************/

       /**
        *
        * @return true for containing binary0100 investment type. false for not
        *         containing binary0100 investment type.
        */
       public boolean isBinary0100Selected() {
   	boolean binary0100 = false;
   	if (opportunitiesList.contains(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_BUY))
   		|| opportunitiesList.contains(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_SELL))) {
   	    binary0100 = true;
   	}
   	return binary0100;
       }

       /**
        * If its not binary0100 buy AND its not binary0100 sell then its other
        * investment type.
        * 
        * @return true for not containing binary0100 investment type. false for
        *         containing binary0100 investment type.
        */
       public boolean isNotBinary0100Selected() {
   	boolean binary0100 = false;
   	for (String oppType : opportunitiesList) {
   	    if (!oppType.equals(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_BUY))
   		    && !oppType.equals(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_SELL))) {
   		binary0100 = true;
   		break;
   	    }
   	}
   	return binary0100;
       }

       /**
        *
        * @return true if `opportunitiesList` contains several types of
        *         investments. false otherwise.
        */
       public boolean isNoSortOptions() {
   	boolean noSort = false;
   	if (opportunitiesList.contains("0") || (isBinary0100Selected() && isNotBinary0100Selected())) {
   	    noSort = true;
   	}
   	return noSort;
       }

       /**
        *
        * @return
        *
        */
       public boolean isJustBinary0100Selected() {
   	boolean just0100 = false;
   	if (isBinary0100Selected()) {
   	    just0100 = true;
   	}
   	if (isNotBinary0100Selected()) {
   	    just0100 = false;
   	}
   	return just0100;
       }

	public boolean isUpInvestment(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isUpInvestment(i);
	}

	public boolean isOneTouchInv(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.getIsOneTouchInv(i);
	}

	public long getRefundUp(Investment i) {
		if (i == null) {
			return 0l;
		}
		return InvestmentsManagerBase.getRefundUp(i);
	}

	public long getRefundDown(Investment i) {
		if (i == null) {
			return 0l;
		}
		return InvestmentsManagerBase.getRefundDown(i);
	}

	public boolean isRolledUp(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRolledUp(i);
	}

	public boolean isRollUpBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRollUpBought(i);
	}

	public boolean isGmBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isGmBought(i);
	}

	public String getAmountWithoutFeesTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId());
	}

    public String getInsuranceAmountRUTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountRU(), i.getCurrencyId());
    }

    public String getInsuranceAmountGMTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountGM(), i.getCurrencyId());
    }

    public String getRefundWithBonusTxt(Investment i) {
    	if (i == null) {
			return "";
		}
    	return InvestmentFormatter.getRefundWithBonusTxt(i);
    }

	public String getWinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(i)), i.getCurrencyId());
	}

	public String getBaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundUp(i) - i.getBaseAmount()) *(-1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBinary0100WinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100WinUpTxt(i);
	}
	
	public String getDynamicsWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100WinUpTxt(i);
	}

	public String getBinary0100WinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100WinUpCustomerTxt(i);
	}
	
	public String getDynamicsWinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsWinUpCustomerTxt(i);
	}

	public String getBinary0100LoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100LoseDownTxt(i);
	}

	public String getBinary0100LoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100LoseDownCustomerTxt(i);
	}
	
	public String getDynamicsLoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsLoseDownCustomerTxt(i);
	}

	public String getBinary0100BaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseWinUpTxt(i);
	}
	
	public String getDynamicsBaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseWinUpTxt(i);
	}

	public String getBinary0100BaseRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseRefundDownTxt(i);
	}

	public String getBinary0100BaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseLoseDownTxt(i);
	}
	
	public String getDynamicsBaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsBaseLoseDownTxt(i);
	}

	public String getLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundDown(i) - i.getAmount()) * (-1), i.getCurrencyId());
	}

	public String getLoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getBaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundDown(i) - i.getBaseAmount())* (-1),
										ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getUserWinLoseTxt(i);
	}

	public String getCustomerUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCustomerUserWinLoseTxt(i);
	}

	public String getUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getUserWinLoseTxt0100(i);
	}

	public String getCustomerUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCustomerUserWinLoseTxt0100(i);
	}

	public String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevelTxt(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getBubblesHighLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByMarket(i.getBubbleHighLevel(), i.getMarketId());
	}

	public String getBubblesLowLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByMarket(i.getBubbleLowLevel(), i.getMarketId());
	}

	public String getRealLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatRealLevelByMarket(i.getRealLevel(), i.getMarketId());
	}

	public String getWwwLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getWwwLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getTimeSettledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeSettledTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getClosingLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100(i);
	}

	public String getCurrentLevel0100BE(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevel0100BE(i);
	}

	public String getClosingPrice(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingPrice(i);
	}

	public String getClosingLevel0100BE(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100BE(i);
	}

	public String getBaseAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getBaseAmount(), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCanceledWriterName(i);
	}

	public String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getIsCanceledTxt(i);
	}

	public String getScheduledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentsManagerBase.getScheduledTxt(i.getScheduledId());
	}

	public boolean getCheckIfCancel(Investment i) {
		if (i == null) {
			return false;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		UserBase user = context.getApplication().evaluateExpressionGet(context, ConstantsBase.BIND_USER, UserBase.class);
		if (user.getRoles().contains(ConstantsBase.ROLE_SADMIN)) {
			return (i.getIsCanceled() == 0);
		}
		return false;
	}

	public String getTransactionAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getTransactionAmount(), i.getCurrencyId());
	}

	public String getBonusName(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getBonusOddsChangeTypeId() > 0) {
			return CommonUtil.getMessage(i.getBonusDisplayName(), null);
		}
		return null;
	}

	public boolean isBinary0100(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isBinary0100(i);
	}
	
	public boolean isDynamics(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isDynamics(i);
	}
	
	public boolean isDynamicsPartiallyClosed(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isDynamicsPartiallyClosed(i);
	}

	public boolean isBubbles(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isBubbles(i);
	}

	public ArrayList<Long> getInvestmentsConnectedIDByInvId(Investment i) throws Exception {
		if (i == null) {
			return new ArrayList<>();
		}
		return InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(i.getId());
	}

	public Long getLastInvestmentsConnectedIDByInvId(Investment i) throws Exception {
		if (i == null) {
			return 0L;
		}
		ArrayList<Long> invIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(i.getId());
		return invIds.get(invIds.size() - 1);
	}

	public String getOppCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByDecimalPoint(i.getOppCurrentLevel(), i.getDecimalPoint());
	}

	public String getOpportunityTypeName(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentsManager.getOpportunityTypeName(i);
	}

	public String getBaseWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getBaseWinLose(i), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCustomerBaseWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getBaseWinLose(i) * (-1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((-1)* (i.getBaseWin() - i.getBaseLose()) * i.getInvestmentsCount()
										/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCustomerBaseWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((i.getBaseWin() - i.getBaseLose())* i.getInvestmentsCount()
										/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseInvestmentsCount(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByDecimalPoint(i.getInvestmentsCount() * i.getRate(), 2);
	}

	public boolean isTypeBinary0100Above(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Above(i);
	}

	public boolean isTypeBinary0100Below(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Below(i);
	}

	public String getUserBalanceTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getUserBalance(), i.getCurrencyId());
	}

	public long getCopyopRelevantInvBE(Investment i) {
		if (i == null) {
			return 0l;
		}
		return i.getCopyopInvId() != 0 ? i.getCopyOpInvId() : i.getId();
	}

	public String returnBubblesWinTxt(Investment i) {
		if (i == null) {
			return "";
		}
		Currency currency = CurrenciesManagerBase.getCurrency(i.getCurrencyId());
		return CommonUtil.formatCurrencyAmount((i.getAmount() * (1 + i.getOddsWin())/100), true, currency);
	}

	public String returnBubblesLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		Currency currency = CurrenciesManagerBase.getCurrency(i.getCurrencyId());
		return CommonUtil.formatCurrencyAmount((i.getAmount() * (1 - i.getOddsLose())/100), true, currency);
	}

       /**
        * @return the apiExternalUserId
        */
       public long getApiExternalUserId() {
   	return apiExternalUserId;
       }

       /**
        * @param apiExternalUserId
        *            the apiExternalUserId to set
        */
       public void setApiExternalUserId(long apiExternalUserId) {
   	this.apiExternalUserId = apiExternalUserId;
       }

       /**
        * @return the apiUserId
        */
       public long getApiUserId() {
   	return apiUserId;
       }

       /**
        * @param apiUserId
        *            the apiUserId to set
        */
       public void setApiUserId(long apiUserId) {
   	this.apiUserId = apiUserId;
       }

       /**
        * @return the apiExternalReference
        */
       public String getApiExternalReference() {
   	return apiExternalReference;
       }

       /**
        * @param apiExternalReference
        *            the apiExternalReference to set
        */
       public void setApiExternalReference(String apiExternalReference) {
   	this.apiExternalReference = apiExternalReference;
       }

       /**
        * @return the copyopInvTypeId
        */
       public int getCopyopInvTypeId() {

   	return copyopInvTypeId;
       }

       /**
        * @param copyopInvTypeId
        *            the copyopInvTypeId to set
        */
       public void setCopyopInvTypeId(int copyopInvTypeId) {
   	this.copyopInvTypeId = copyopInvTypeId;
       }

       /**
        * @return the copyopInvestmentDetailsList
        */
       public HashMap<Long, CopyopInvestmentDetails> getCopyopInvestmentDetailsList() {
   	return copyopInvestmentDetailsList;
       }

       /**
        * @return the fromUserId
        */
       public long getFromUserId() {
   	return fromUserId;
       }

       /**
        * @param fromUserId
        *            the fromUserId to set
        */
       public void setFromUserId(long fromUserId) {
   	this.fromUserId = fromUserId;
       }

       public Long getId() {
   	return id;
       }

       public void setId(Long id) {
   	if (id == null) {
   	    this.id = 0;
   	} else {
   	    this.id = id;
   	}
       }

       /**
        * Setting copyop investment details by investment id
        * 
        * @param invId
        * @throws SQLException
        */
       public void setCopyopInvestmentDetailsByInvId(long invId) throws SQLException {
   	CopyopInvestmentDetails copyopInvestmentDetails = InvestmentsManagerBase.getCopyopInvestmentDetails(invId);
   	copyopInvestmentDetails.setOriginalInvestmentId(invId);
   	copyopInvestmentDetails.setCurrencyId(Currency.CURRENCY_USD_ID);
   	copyopInvestmentDetailsList.put(invId, copyopInvestmentDetails);
       }

       public long getScheduled() {
   	return scheduled;
       }

       public void setScheduled(long scheduled) {
   	this.scheduled = scheduled;
       }
       
	public String getDynamicsPartiallyClosedOriginalInvAmount(Investment i)throws SQLException {
		if (i == null) {
			return "";
		} else if (InvestmentsManagerBase.getDynamicsPartiallyClosedOriginalInvAmount(i) != null) {
			return getAmountTxt(InvestmentsManagerBase.getDynamicsPartiallyClosedOriginalInvAmount(i));
		} else {
			return "";
		}
	}

}