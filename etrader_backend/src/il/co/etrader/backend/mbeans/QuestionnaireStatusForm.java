package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.QuestionnaireStatusManager;
import il.co.etrader.backend.bl_vos.QuestionnaireStatus;

public class QuestionnaireStatusForm implements Serializable{

    private static final long serialVersionUID = -4149726927661782997L;
    
    private static final Logger logger = Logger.getLogger(QuestionnaireStatusForm.class);
    boolean notFilled = true;
    // Filters
    Long userId = 0l;           // any
    String userName = "";       // any
    int interested = 0;         // all 
    int daysConditionDeposit = 0;      // not reached 30 days
    int daysConditionQualified = 1;      // not reached 30 days
    int reached = 0;
	int qualified = 1; 			// 2-all, 1-yes, 0-no
	int skinId = 0;
    // Paging.
    private int totalRows;
    private int firstRow;
    private int rowsPerPage;
    private int totalPages;
    private int pageRange;
    private Integer[] pages;
    private int currentPage;

    
    ArrayList<QuestionnaireStatus> list;
    
    public QuestionnaireStatusForm(){
        rowsPerPage = 10;
        pageRange = 10;
        daysConditionDeposit = 0;
        list = new ArrayList<QuestionnaireStatus>();
    }
    
    public int getListSize() {
        return list.size();
    }
    
    public String search() {
        initList();
        return null;
    }
    
    public String initList() {
        // Load list and totalCount.
        loadData();

        // Set currentPage, totalPages and pages.
        currentPage = (totalRows / rowsPerPage) - ((totalRows - firstRow) / rowsPerPage) + 1;
        totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
        
        return null;
    }
    
    public void filterChanged(){
        firstRow = 0;
    }
    
    public void loadData(){
        
        try {
                if(notFilled){
                    list = QuestionnaireStatusManager.getQuestionnaireNotFilled(userId, userName.trim(), interested, daysConditionDeposit, reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);
                    totalRows = QuestionnaireStatusManager.getQuestionnaireNotFilledCount(userId, userName.trim(), interested, daysConditionDeposit, reached, qualified, daysConditionQualified, skinId);
                } else {
                    list = QuestionnaireStatusManager.getQuestionnaireFilledIncorrectly(userId, userName.trim(), interested, daysConditionDeposit, reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);
                    totalRows = QuestionnaireStatusManager.getQuestionnaireFilledIncorrectlyCount(userId, userName.trim(), interested, daysConditionDeposit, reached, qualified, daysConditionQualified, skinId);
                }
        } catch (SQLException e) {
            logger.error("Can't load Questionnaire Not Filled list :" + e.getMessage());
        }
    }        
    public void reset() {
        userId = 0l;
        userName = "";
        interested = 0;
        daysConditionDeposit = 0;
        reached = 0;
    }
    
   public void pageFirst() {
        page(0);
    }

    public void pageNext() {
        page(firstRow + rowsPerPage);
    }

    public void pagePrevious() {
        page(firstRow - rowsPerPage);
    }

    public void pageLast() {
        page(totalRows - ((totalRows % rowsPerPage != 0) ? totalRows % rowsPerPage : rowsPerPage));
    }

    public void page(ActionEvent event) {
        page((((Integer) ((UICommand) event.getComponent()).getValue()) - 1) * rowsPerPage);
    }

    private void page(int firstRow) {
        this.firstRow = firstRow;
        loadData(); // Load requested page.
    }

    public ArrayList<QuestionnaireStatus> getList() {
        return list;
    }

    public void setList(ArrayList<QuestionnaireStatus> list) {
        this.list = list;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getInterested() {
        return interested;
    }

    public void setInterested(int interested) {
        this.interested = interested;
    }
    
    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageRange() {
        return pageRange;
    }

    public void setPageRange(int pageRange) {
        this.pageRange = pageRange;
    }

    public Integer[] getPages() {
        return pages;
    }

    public void setPages(Integer[] pages) {
        this.pages = pages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getDaysCondition() {
        return daysConditionDeposit;
    }

    public void setDaysCondition(int daysCondition) {
        this.daysConditionDeposit = daysCondition;
    }

    /**
     * @return the reached
     */
    public int getReached() {
        return reached;
    }

    /**
     * @param reached the reached to set
     */
    public void setReached(int reached) {
        this.reached = reached;
    }

    /**
     * @return the notFilled
     */
    public boolean isNotFilled() {
        return notFilled;
    }

    /**
     * @param notFilled the notFilled to set
     */
    public void setNotFilled(boolean notFilled) {
        this.notFilled = notFilled;
    }

	public int getQualified() {
		return qualified;
	}

	public void setQualified(int qualified) {
		this.qualified = qualified;
	}

	public int getDaysConditionDeposit() {
		return daysConditionDeposit;
	}

	public void setDaysConditionDeposit(int daysConditionDeposit) {
		this.daysConditionDeposit = daysConditionDeposit;
	}

	public int getDaysConditionQualified() {
		return daysConditionQualified;
	}

	public void setDaysConditionQualified(int daysConditionQualified) {
		this.daysConditionQualified = daysConditionQualified;
	}

	public int getSkinId() {
		return skinId;
	}

	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	
}
