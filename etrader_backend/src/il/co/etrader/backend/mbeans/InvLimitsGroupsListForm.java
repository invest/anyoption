package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.InvestmentLimitsGroup;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

public class InvLimitsGroupsListForm implements Serializable {

	private static final long serialVersionUID = 3515915001529485508L;

	private InvestmentLimitsGroup group;
	private ArrayList list;

	public InvLimitsGroupsListForm() throws SQLException{
		search();
	}


	public String updateInsert() throws SQLException{
//		boolean res=InvestmentsManager.updateInsertInvLimGroup(group);
//		if (res==false)
//			return null;
		search();
		return Constants.NAV_INV_LIM_GROUPS;
	}

	public String navNew() throws SQLException{
		group=new InvestmentLimitsGroup();
		return Constants.NAV_INV_LIM_GROUP;
	}

	public String search() throws SQLException{
//		list=InvestmentsManager.searchInvLimGroups();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}

	public InvestmentLimitsGroup getGroup() {
		return group;
	}

	public void setGroup(InvestmentLimitsGroup group) {
		this.group = group;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvLimitsGroupsListForm ( "
	        + super.toString() + TAB
	        + "group = " + this.group + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}



}
