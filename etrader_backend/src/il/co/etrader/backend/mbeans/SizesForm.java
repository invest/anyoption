package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingSize;
import il.co.etrader.util.CommonUtil;



public class SizesForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String verticalSize;
	private String horizontalSize;
	private ArrayList<MarketingSize> list;
	private MarketingSize size;

	public SizesForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingSizes(verticalSize, horizontalSize, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert size
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		size.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateSize(size);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_SIZES;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit size
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_SIZE;
	}

	/**
	 * Navigate to insert new size
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		size = new MarketingSize();
		size.setWriterId(AdminManager.getWriterId());
		size.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_SIZE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the horizontalSize
	 */
	public String getHorizontalSize() {
		return horizontalSize;
	}

	/**
	 * @param horizontalSize the horizontalSize to set
	 */
	public void setHorizontalSize(String horizontalSize) {
		this.horizontalSize = horizontalSize;
	}

	/**
	 * @return the size
	 */
	public MarketingSize getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(MarketingSize size) {
		this.size = size;
	}

	/**
	 * @return the verticalSize
	 */
	public String getVerticalSize() {
		return verticalSize;
	}

	/**
	 * @param verticalSize the verticalSize to set
	 */
	public void setVerticalSize(String verticalSize) {
		this.verticalSize = verticalSize;
	}


}
