//package il.co.etrader.backend.mbeans;
//
//import il.co.etrader.backend.bl_vos.WriterWrapper;
//import il.co.etrader.backend.util.Utils;
//import il.co.etrader.bl_vos.File;
//import il.co.etrader.util.CommonUtil;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.Serializable;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.Hashtable;
//import java.util.StringTokenizer;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.context.FacesContext;
//
//public class EncodeHeaderForm implements Serializable {
//
//	private static final long serialVersionUID = -2399285495491758445L;
//	private ArrayList<String> listToEncode;
//	private ArrayList<String> listAfterEncode;
//	private File file;
//	private String generalMessage;
//	private String falseInsertMessage;
//	ArrayList<String> errorLines;
//	public static final long NUMBER_OF_COLUMN_IN_THE_CSV_FILE = 10;
//	
//	public EncodeHeaderForm() {
//		file = new File();
//		errorLines = new ArrayList<String>();
//	}
//	
//	public String convertToEncode() {
//		
//		generalMessage = "";
//		falseInsertMessage = "";
//		errorLines.clear();
//		FacesContext context = FacesContext.getCurrentInstance();
//		FacesMessage fm = null;
//		int falseInsertCounter = 0;
//		listToEncode = new ArrayList<String>();
//		listAfterEncode = new ArrayList<String>();
//		boolean errorFound = false;
//
//		try {
//			if (file.getFile() == null) {
//				generalMessage = CommonUtil.getMessage("issues.file.error.empty", null);
//				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
//				context.addMessage(null, fm);
//				return null;
//			}
//			BufferedReader fr = new BufferedReader(new InputStreamReader(file.getFile().getInputStream(), "UTF8"));
//			String encode = new String();
//			StringTokenizer st = null;
//			String encodeHeader = null;
//			int stringTokenizerCounter = 0;
//			int errorLineNum = 0;
//
//			while (null != (encode = fr.readLine())) {
//				encodeHeader = new String();
//				errorLineNum++;
//				st = new StringTokenizer(encode, ",");
//				stringTokenizerCounter = st.countTokens();
//				if(stringTokenizerCounter >= 1) {
//					while (st.hasMoreTokens()) {
//						encodeHeader = st.nextToken();
//					}
//					listToEncode.add(encodeHeader);
//				} else {
//					errorFound = true;
//					String error = "Empty field";
//					falseInsertCounter++;
//					errorLines.add(String.valueOf(errorLineNum) + " " + error);
//				}
//			}
//		} catch (IOException e) {
//			generalMessage = CommonUtil.getMessage("issues.file.error.problem", null);
//			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
//			context.addMessage(null, fm);
//			return null;
//		}
//
//		if (!errorFound) { //success
//			String report = encodeList();
//			sendEmail(report);
//		} else { // failed
//			generalMessage = CommonUtil.getMessage("import.leads.file.failed.upload", null);
//			String[] params =  new String[1];
//			params[0] = String.valueOf(falseInsertCounter);
//			falseInsertMessage =  CommonUtil.getMessage("import.leads.error.summary", params);
//			for(int i = 0; i < errorLines.size() ; i++) {
//				falseInsertMessage += "Line #" + errorLines.get(i) + " <br/>";
//			}
//		}
//
//		fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
//		context.addMessage(null, fm);
//		return null;
//	}
//	
//	/**
//	 * encode the list and create html. 
//	 * 
//	 * @return report as a String
//	 */
//	public String encodeList() {
//		String report = "<html><body><table border=\"1\"><tr><td><b>List to encode</b></td><td><b>List after encode</b></td></tr>";
//		generalMessage = CommonUtil.getMessage("import.leads.file.success.upload", null);
//		for (String s: listToEncode) {
//			try {
//				String encodedurl = URLEncoder.encode(s, "UTF-8");
//				// match encode for space in js. 
//				encodedurl = encodedurl.replace("+", "%20");
//				listAfterEncode.add(encodedurl);
//				report += "<tr><td>" +
//			              	s +
//			              "</td><td>" +
//			              	encodedurl +
//		                  "</td></tr>";
//			} catch (UnsupportedEncodingException e) {
//				e.printStackTrace();
//			}
//		}
//		report += "<table><body><html>";
//		return report;
//	}
//	
//	/**
//	 * Send email with report of encode text. 
//	 * 
//	 * @param report
//	 */
//	public void sendEmail(String report) {
//		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
//		serverProperties.put("url", CommonUtil.getProperty("email.server"));
//		serverProperties.put("auth", "true");
//		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
//		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
//		serverProperties.put("contenttype", "text/html; charset=UTF-8");
//		
//		WriterWrapper ww = Utils.getWriter();
//		String writerEmail = ww.getWriter().getEmail();
//        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
//        emailProperties.put("subject", CommonUtil.getMessage("convert.to.encode", null));
//        emailProperties.put("to", writerEmail);
//        emailProperties.put("from", CommonUtil.getProperty("email.from.anyoption"));
//        emailProperties.put("body", report);
//		CommonUtil.sendEmail(serverProperties, emailProperties, null);
//	}
//
//	/**
//	 * @return the file
//	 */
//	public File getFile() {
//		return file;
//	}
//
//	/**
//	 * @param file the file to set
//	 */
//	public void setFile(File file) {
//		this.file = file;
//	}
//
//	/**
//	 * @return the falseInsertMessage
//	 */
//	public String getFalseInsertMessage() {
//		return falseInsertMessage;
//	}
//
//	/**
//	 * @param falseInsertMessage the falseInsertMessage to set
//	 */
//	public void setFalseInsertMessage(String falseInsertMessage) {
//		this.falseInsertMessage = falseInsertMessage;
//	}
//
//	/**
//	 * @return the generalMessage
//	 */
//	public String getGeneralMessage() {
//		return generalMessage;
//	}
//
//	/**
//	 * @param generalMessage the generalMessage to set
//	 */
//	public void setGeneralMessage(String generalMessage) {
//		this.generalMessage = generalMessage;
//	}
//
//}
