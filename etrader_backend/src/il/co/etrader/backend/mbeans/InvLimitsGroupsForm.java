package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.InvestmentLimitAndGroup;
import il.co.etrader.util.CommonUtil;

public class InvLimitsGroupsForm implements Serializable {

	private static final long serialVersionUID = -6298745722310951889L;

	private InvestmentLimitAndGroup limitGroup;
	private InvestmentLimitAndGroup limitGroupPrev;
	private ArrayList list;
	private Long groupId;

	public InvLimitsGroupsForm() throws SQLException{
		limitGroupPrev=new InvestmentLimitAndGroup();
		//search();
	}


	public String updateInsert() throws SQLException{
//		boolean res=InvestmentsManager.updateInsertInvLimitGroup(limitGroup, limitGroupPrev);

//		if (res==false)
//			return null;
		search();
		return Constants.NAV_INV_LIMIT_GROUPS;
	}

	public String newLimitGroup() throws SQLException{
		limitGroup=new InvestmentLimitAndGroup();
		limitGroup.setInvestmentLimitGroupId(groupId);
		limitGroupPrev=null;
		return Constants.NAV_INV_LIMIT_GROUP;
	}

	public String search() throws SQLException{
//		list=InvestmentsManager.searchInvLimitsAndGroups(groupId);
		CommonUtil.setTablesToFirstPage();
		return Constants.NAV_INV_LIMIT_GROUPS;
	}

	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}

	public InvestmentLimitAndGroup getLimitGroup() {
		return limitGroup;
	}

	public void setLimitGroup(InvestmentLimitAndGroup limitGroup) {
		this.limitGroup = limitGroup;
	}

	public InvestmentLimitAndGroup getLimitGroupPrev() {
		return limitGroupPrev;
	}

	public void setLimitGroupPrev(InvestmentLimitAndGroup limitGroupPrev) {
		this.limitGroupPrev = limitGroupPrev;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvLimitsGroupsForm ( "
	        + super.toString() + TAB
	        + "limitGroup = " + this.limitGroup + TAB
	        + "limitGroupPrev = " + this.limitGroupPrev + TAB
	        + "list = " + this.list + TAB
	        + "groupId = " + this.groupId + TAB
	        + " )";

	    return retValue;
	}


}
