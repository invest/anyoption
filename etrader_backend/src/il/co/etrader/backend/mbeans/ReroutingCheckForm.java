package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.bl_vos.ClearingLimitaion;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

public class ReroutingCheckForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long currParameterX;
	private long currParameterY;
	private Long newParameterX;
	private Long newParameterY;
	private ArrayList<ClearingLimitaion> clearingLimitaionList;
	private long limitType;
	
	public ReroutingCheckForm() throws Exception {
		currParameterX = ConfigManager.getDaysNumForFailures();
		currParameterY = ConfigManager.getFailuresNumConsecutive(); 	
		newParameterX = new Long(0);
		newParameterY = new Long(0);
	}

	public String updateParameterX() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		boolean res = false;
		if (null != newParameterX) {
			if (newParameterX.longValue() != currParameterX) {
				res = ConfigManager.updateParameter(TransactionsManagerBase.CONFIG_ID_PARAMETER_X, newParameterX.longValue());
				currParameterX = newParameterX.longValue();
			}
		}

		FacesMessage fm = null;
		if (res) {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);			
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.faild", null, Utils.getWriterLocale(context)),null);
		}
		context.addMessage(null, fm);
		return null;
	}
	
	public String updateParameterY() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		boolean res = false;
		if (null != newParameterY) {
			if (newParameterY.longValue() != currParameterY) {
				res = ConfigManager.updateParameter(TransactionsManagerBase.CONFIG_ID_PARAMETER_Y, newParameterY.longValue());
				currParameterY = newParameterY.longValue();
			}
		}
		FacesMessage fm = null;
		if (res) {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);			
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.faild", null, Utils.getWriterLocale(context)),null);
		}
		context.addMessage(null, fm);
		return null;
	}
	
	public long getCurrParameterX() {
		return currParameterX;
	}

	public void setCurrParameterX(long currParameterX) {
		this.currParameterX = currParameterX;
	}

	public long getCurrParameterY() {
		return currParameterY;
	}

	public void setCurrParameterY(long currParameterY) {
		this.currParameterY = currParameterY;
	}

	public Long getNewParameterX() {
		return newParameterX;
	}

	public void setNewParameterX(Long newParameterX) {
		this.newParameterX = newParameterX;
	}

	public Long getNewParameterY() {
		return newParameterY;
	}

	public void setNewParameterY(Long newParameterY) {
		this.newParameterY = newParameterY;
	}

	public long getLimitType() {
		return limitType;
	}

	public void setLimitType(long limitType) {
		this.limitType = limitType;
	}
	
}
