/**
 * 
 */
package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;

import il.co.etrader.backend.bl_managers.DeeplinkParamsToUrlManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

/**
 * @author Eyal Goren
 *
 */
public class DeeplinkParamsToUrlForm implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(DeeplinkParamsToUrlForm.class);	
	
	private DeeplinkParamsToUrl deeplinkParamsToUrl;
	private ArrayList<DeeplinkParamsToUrl> deeplinkParamsToUrlList;
	
	
	public DeeplinkParamsToUrlForm() throws SQLException {	
//		deeplinkParamsToUrl = new DeeplinkParamsToUrl();
		search();
	}
	
	public String search() throws SQLException {
		deeplinkParamsToUrlList = DeeplinkParamsToUrlManager.getAllArrayList();
//		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	public int getListSize() {
		return deeplinkParamsToUrlList.size();
	}

	public ArrayList<DeeplinkParamsToUrl> getDeeplinkParamsToUrlList() {
		return deeplinkParamsToUrlList;
	}

	public void setDeeplinkParamsToUrlList(
			ArrayList<DeeplinkParamsToUrl> deeplinkParamsToUrlList) {
		this.deeplinkParamsToUrlList = deeplinkParamsToUrlList;
	}

	public DeeplinkParamsToUrl getDeeplinkParamsToUrl() {
		return deeplinkParamsToUrl;
	}

	public void setDeeplinkParamsToUrl(DeeplinkParamsToUrl deeplinkParamsToUrl) {
		this.deeplinkParamsToUrl = deeplinkParamsToUrl;
	}
	
	//not the best way to do it!!
	public String getWriterName(long writerId) throws SQLException{
		return ApplicationDataBase.getWriterName(writerId);
	}

	public String updateInsert() {
		try {
			deeplinkParamsToUrl.setWriterId(Utils.getWriter().getWriter().getId());
			DeeplinkParamsToUrlManager.insertUpdate(deeplinkParamsToUrl);
			search();
		} catch (SQLException e) {
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.update.insert.fail", null);
			logger.error("cant insert or update deep link", e);
			return null;
		}
		
		return Constants.NAV_DEEPLINKK_PARAMS_TO_URL_SEARCH;
	}
	
	public String navNew() {
		deeplinkParamsToUrl = new DeeplinkParamsToUrl();
		return Constants.NAV_DEEPLINKK_PARAMS_TO_URL_NEW;
	}
	
	
}
