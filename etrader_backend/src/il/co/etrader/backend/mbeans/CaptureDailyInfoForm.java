package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.CaptureDailyInfo;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;


/**
 * Capture daily inforamtion Bean
 * @author Kobi
 *
 */
public class CaptureDailyInfoForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;
	private ArrayList<CaptureDailyInfo> list;
	private long investStatus;

	public CaptureDailyInfoForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		list = TransactionsManager.getCaptureDailyInfo(investStatus);
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public int getListSize() {
		return list.size();
	}


	/**
	 * @return the list
	 */
	public ArrayList<CaptureDailyInfo> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<CaptureDailyInfo> list) {
		this.list = list;
	}

	/**
	 * @return the investStatus
	 */
	public long getInvestStatus() {
		return investStatus;
	}

	/**
	 * @param investStatus the investStatus to set
	 */
	public void setInvestStatus(long investStatus) {
		this.investStatus = investStatus;
	}

	public ArrayList<SelectItem> getInvestStatusesSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0), CommonUtil.getMessage("capture.daily.have.investments.all", null)));
		list.add(new SelectItem(new Long(Constants.CAPTURE_INFO_NOT_INVEST), CommonUtil.getMessage("capture.daily.not.have.investments", null)));
		list.add(new SelectItem(new Long(Constants.CAPTURE_INFO_INVEST), CommonUtil.getMessage("capture.daily.have.investments", null)));
		return list;
	}

}
