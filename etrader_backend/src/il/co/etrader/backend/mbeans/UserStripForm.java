package il.co.etrader.backend.mbeans;

import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;

public class UserStripForm implements java.io.Serializable {


	private static final long serialVersionUID = -2006645983408957329L;
	
	protected static final Logger logger = Logger.getLogger(UserStripForm.class);
	
	private static final String SCREEN_SUPPORT 		= "support";
	private static final String SCREEN_SALES 		= "sales";
	private static final String SCREEN_SUPPORT_URL 	= "support/support.jsf";
	private static final String SCREEN_SALES_URL 	= "retention/retention.jsf";
	private static final String PARAMETER_USER_ID = "userId";
	private static final String PARAMETER_SCREEN = "screen";
	
	public UserStripForm() {
		
	}

	public String loadUser() throws Exception {
		String screen = getScreenFromRequest();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		if (null != screen && screen.equals(SCREEN_SUPPORT)) {
			try {
				User user = new User();
				HttpSession session = request.getSession();
				synchronized (session) {
					session.setAttribute(ConstantsBase.BIND_SESSION_USER, user);
					long userId = getUserIdFromRequest();
					user.setTmpUserId(userId);
					try {
						String result = user.loadNewUserFromStrip();
						if (result.equals(SCREEN_SUPPORT)) {
							return SCREEN_SUPPORT_URL;
						}
					} catch (ParseException pe) {
						logger.error("Error loading new user from strip! " + pe);
					}
				}
			} catch (SQLException sqle) {
				logger.error("Error getting user! " + sqle);
			}
		} else if (null != screen && screen.equals(SCREEN_SALES)) {
		    WriterWrapper writerWrapper = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);        
	        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	        String entryId = request.getParameter("entryId");
	        logger.info("CPATTERN, entryId = " + entryId);
	        if (!Utils.isParameterEmptyOrNull(entryId)) {
	        	User u = Utils.getUser();	        	
	        	long writerId = writerWrapper.getWriter().getId();
	            long populationEntryId = Long.valueOf(entryId);
	            boolean result = true;
	            PopulationEntryBase populationEntryBase = PopulationsManagerBase.getPopulationEntryByEntryId(populationEntryId);
	            PopulationEntry populationEntry = new PopulationEntry();
	            populationEntry.convertBaseToPopulationEntry(populationEntryBase);
	            boolean isEntrylocked = populationEntryBase.isEntryLocked();	             
	            logger.info("CPATTERN, isEntrylocked " + isEntrylocked + " writerId in session: " + writerId + " current assign writerId for entry:" + populationEntryBase.getAssignWriterId());
	            // check that this entry belongs to this writer
	            if (populationEntryBase.getAssignWriterId() != 0 && (populationEntryBase.getAssignWriterId() != writerId) && u.isRetentionSelected()) {	            		            
	            	result = false;
	            }	            	            	            
	            if (result && !isEntrylocked) {
	            	// lock and load entry.
	            	result = PopulationEntriesManager.lockEntry(populationEntry, false);
	            }
	            // send redirect to retention.
	            try {
	                if (result) {
	                    logger.info("redirect to user strip in retention with populationEntryId = " + entryId + ".");
	                    return SCREEN_SALES_URL;
	                } else {
	                    logger.info("redirect to error page in retention with populationEntryId = " + entryId + ".");
	                    response.sendRedirect(CommonUtil.getProperty(Constants.HOST_URL_HTTPS) + "/jsp/retention/retention.jsf?retentionError=true&entryId=" + entryId);
	                }
	            } catch (IOException e) {
	                logger.error("Error! Problem to redirect. entryId = " + entryId + ".", e);
	            }
	        }			
		}			    		
		return null;
	}
	
	private long getUserIdFromRequest() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		long userId = Long.parseLong(request.getParameter(PARAMETER_USER_ID));
		return userId;
	}
	
	private String getScreenFromRequest() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String screen = request.getParameter(PARAMETER_SCREEN);
		return screen;
	}
	
}