package il.co.etrader.backend.mbeans;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.managers.ProfileManager;

/**
 * @author liors
 *
 */
public class CopyopUserCopiersForm extends CopyopUserCopyForm implements Serializable {
	
	private static final long serialVersionUID = -5051411137579982353L;
	private static final Logger logger = Logger.getLogger(CopyopUserCopiersForm.class);
	
	public CopyopUserCopiersForm() {
		super();
	}
	
	public void search() {
		//assumption: method return size can't return more than ConstantsBase.MAXIMUM_ORACLE_IN_CLAUSE elements.
		this.userCopyList = ProfileManager.getUsersCopy(ProfileLinkTypeEnum.COPIED_BY, user.getId(), from, userId, skinId, isStillCopy, isFrozen);	
		//TODO Refactoring. Filters layer - before insert to the list => efficiency. 
		this.userCopyList = ProfileManager.doUserCopyFilters(this.userCopyList, from, userId, skinId, isStillCopy, isFrozen);						
		CheckIfEmptyMessage();	
	}
}
