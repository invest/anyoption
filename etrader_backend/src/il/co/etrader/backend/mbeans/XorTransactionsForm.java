package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class XorTransactionsForm implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3389374998197707837L;

	private Date from;
	private String fromMin;
	private String fromHours;
	private Date to;
	private String toMin;
	private String toHours;
	private ArrayList transactionsList;
	private long classId;
	private long typeId;
	private int skinId;
	private String htmlBuffer;
	private long skinBusinessCaseId;
	private long currencyId;
	private long providerId;
	private boolean byPhone;

	private String searchBy;

	public XorTransactionsForm() throws SQLException{

/*
		GregorianCalendar togc=new GregorianCalendar();
		togc.add(GregorianCalendar.DAY_OF_MONTH, -1);
		to=togc.getTime();

		GregorianCalendar gc=new GregorianCalendar();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
		from=gc.getTime();
*/
		searchBy="1";
		fromMin="00";
		fromHours="00";
		toMin="59";
		toHours="23";
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		typeId = TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT;
		FacesContext context = FacesContext.getCurrentInstance();
		User sessionuser = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (sessionuser.isSupportSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
		}

		GregorianCalendar f=new GregorianCalendar();
		//f.setTime(from);
		f.add(GregorianCalendar.DAY_OF_MONTH, -1);
		f.set(GregorianCalendar.SECOND, 0);
		f.set(GregorianCalendar.MINUTE, 0);
		f.set(GregorianCalendar.HOUR_OF_DAY,0);
		from=f.getTime();

		GregorianCalendar t=new GregorianCalendar();
		//t.setTime(to);
		t.add(GregorianCalendar.DAY_OF_MONTH, -1);
		t.set(GregorianCalendar.SECOND, 0);
		t.set(GregorianCalendar.MINUTE, 59);
		t.set(GregorianCalendar.HOUR_OF_DAY,23);
		to=t.getTime();

		updateTransactionsList();
	}

	public String updateTransactionsList() throws SQLException{

		transactionsList=TransactionsManager.getTransactions(0,from,to,typeId,
				TransactionsManager.TRANS_STATUS_SUCCEED,searchBy,null,null,true,null,classId,getSkinFilter(),new Long(0), skinBusinessCaseId, new Long(0), new Long(99), new Long(99),
				currencyId, new Long(0), new Long(0),providerId,byPhone, 
				null, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, "0", -1);
			CommonUtil.setTablesToFirstPage();
		return null;
	}

	public void exportHtmlTableToExcel() throws IOException {

        //Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>"+htmlBuffer+"</html>";


        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "deposits_report_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();
    }


	public int getListSize() {
		return transactionsList.size();
	}

	public ArrayList getTransactionsList() {
		return transactionsList;
	}

	public void setTransactionsList(ArrayList transactionsList) {
		this.transactionsList = transactionsList;
	}




	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getFromHours() {
		return fromHours;
	}

	public void setFromHours(String fromHours) {
		this.fromHours = fromHours;
	}

	public String getFromMin() {
		return fromMin;
	}

	public void setFromMin(String fromMin) {
		this.fromMin = fromMin;
	}

	public String getToHours() {
		return toHours;
	}

	public void setToHours(String toHours) {
		this.toHours = toHours;
	}

	public String getToMin() {
		return toMin;
	}

	public void setToMin(String toMin) {
		this.toMin = toMin;
	}

	public String getHtmlBuffer() {
		return htmlBuffer;
	}

	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getFrom() {
		return from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	public int getSkinId() {
		return skinId;
	}


	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public String getSkinFilter() {
		if (skinId == Skin.SKINS_ALL_VALUE) {
			return Utils.getWriter().getSkinsToString();
		}
		else {
			return String.valueOf(skinId);
		}
	}

	/**
	 * @return the skinBusinessCaseId
	 */
	public long getSkinBusinessCaseId() {
		return skinBusinessCaseId;
	}

	/**
	 * @param skinBusinessCaseId the skinBusinessCaseId to set
	 */
	public void setSkinBusinessCaseId(long skinBusinessCaseId) {
		this.skinBusinessCaseId = skinBusinessCaseId;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the providerId
	 */
	public long getProviderId() {
		return providerId;
	}

	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	/**
	 * @return the byPhone
	 */
	public boolean isByPhone() {
		return byPhone;
	}

	/**
	 * @param byPhone the byPhone to set
	 */
	public void setByPhone(boolean byPhone) {
		this.byPhone = byPhone;
	}


}
