package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.helper.GeneralReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class GeneralReportForm implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(GeneralReportForm.class);
	private Date startDate;
	private Date endDate;
	private long currentWriterId;

	public GeneralReportForm() throws SQLException, ParseException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		endDate = gc.getTime();
		gc.add(GregorianCalendar.MONTH, -1);
		startDate = gc.getTime();
		currentWriterId = Utils.getWriter().getWriter().getId();
		//set defualt date for arabic partner
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);;
			if (startDate.before(startDateAr)){
				startDate = startDateAr;
			}
	    }
	}


	public String exportIssueActionReportToExcel() throws IOException, SQLException, ParseException {
		return exportHtmlTableToExcel(Constants.SALES_SUPPORT_REPORT_TYPE_ISSUE_ACTION,"issue_action_report_", false);
	}
	
	public String exportHtmlTableToExcel(int reportType, String reportName, boolean isGoogleReport) throws IOException, SQLException, ParseException {
		FacesContext context=FacesContext.getCurrentInstance();		
		logger.info("Process " + reportName);
		boolean isMarketingDept=false;
		if(Utils.getWriter().getWriter().getDeptId()==8){
			isMarketingDept=true;
		}else{
			isMarketingDept=false;
		}

		// Add 1 to the end date, to include the end date
		Date endDatePlusDay = CommonUtil.addDay(endDate);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String filename = reportName + fmt.format(startDate) + "_" + fmt.format(endDate);

		//set defualt date for arabic partner
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);;
			if (startDate.before(startDateAr)){
				startDate = startDateAr;
			}
	    }

	    //Send csv file.
	    filename += ".csv";

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter().getEmail())){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.marketing.report.invalid.mail", null),null);
			context.addMessage(null, fm);
			return null;
		}

		GeneralReportSender sender = new GeneralReportSender(this,reportType,filename,Utils.getWriter());

		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				filename + " " + CommonUtil.getMessage("marketing.report.processing.mail", null),null);
		context.addMessage(null, fm);
        
		return null;
	}



	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "GeneralForm ( "
	        + super.toString() + TAB
	        + "startDate = " + this.startDate + TAB
	        + "endDate = " + this.endDate + TAB
	        + " )";

	    return retValue;
	}


	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}


	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}


	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
