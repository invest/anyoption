package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.BonusManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bonus.BonusMessageHandler;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * bonus users form
 * @author Kobi
 *
 */
public class BonusUsersForm implements Serializable {
	private static final long serialVersionUID = 641448643239231821L;

	private static final Logger log = Logger.getLogger(BonusUsersForm.class);

	private Date from;
	private Date to;
	private String userName;
	private long classId;
	private int skinId;
	private long bonusTypeId;
	private long userActionId;
	private long bonusStateId;
	private long wageringParam;
	private long dynamicSumDeposits;
	private long writerId;
	private long sumInvestOrAmount;
	private String isBonusAmount;
	private boolean displaySumInvestAndAmount;
	private String descriptions;
	private boolean displayDynamicSumDeposits;
	private long dynamicWagering;
	private boolean displayDinamicWagering;

	private ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
	private BonusUsers bonusUser = null;
	private ArrayList<SelectItem> writerFilterList = new ArrayList<SelectItem>();

	public BonusUsersForm() throws SQLException{
		dynamicSumDeposits =0;
		wageringParam = 0;
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, 1);
		to = gc.getTime();
		gc.add(Calendar.DATE, -3);
		from = gc.getTime();
		writerId = 0;
		sumInvestOrAmount = 0;
		isBonusAmount = "0";
		dynamicWagering = 0;
		displayDinamicWagering = true;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
		}
		updateList();
	}


	public String updateList() throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		long userId = 0;
		long populationTypeId = 0;
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			userId = user.getId();
		}
		if (user.isRetentionSelected() || user.isRetentionMSelected()) {
			PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
			populationTypeId = entry.getCurrPopualtionTypeId();
		}

		list = BonusManager.getBounsUsers(userId, from, to, userName, getSkinFilter(), classId, bonusTypeId,
					bonusStateId, userActionId, w.getWriter().getGroupId(), populationTypeId, writerId, Utils.getAppData().getUtcOffset());

		createWriterFilter();

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Navigate to insert new campaign
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		bonusUser = new BonusUsers();
		bonusUser.setUserId(user.getId());
		bonusUser.setWriterId(AdminManager.getWriterId());

		return Constants.NAV_BONUS_USER;
	}

	/**
	 * Update / insert campaign
	 * @return
	 * @throws SQLException
	 */
	public String insert() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		boolean isSadmin = ((HttpServletRequest)context.getExternalContext().getRequest()).isUserInRole("sadmin");
		
		if(user.isBonusAbuser()&&!isSadmin){
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "User is bonus abuser ! Only Super admin can grant bonus to this user.",null);
			context.addMessage(null, fm);
			return null;
		}
		long popTypeId = 0;
		long popEntryId = 0;
		int bonusPopLimitTypeId = 0;

		// handle bonus population limits
		if (user.isRetentionSelected() || user.isRetentionMSelected() || user.isSupportSelectedWithMOLocking()) {
			Bonus b = BonusManager.getBonusById(bonusUser.getBonusId());

			if (b.isCheckLimit()) {
				PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
				if (null != entry){
					popTypeId = entry.getCurrPopualtionTypeId();

					if (popTypeId > 0){
						popEntryId = entry.getCurrEntryId();
						WriterWrapper wr = Utils.getWriter();
				    	bonusPopLimitTypeId = wr.getBonusPopLimitType(popTypeId);
				    	if (bonusPopLimitTypeId <= 0){
				        	log.warn("Problem getting bonusPopLimitId. userId: " + user.getId() + ", popTypeId: " + popTypeId);
				    	}
					}else{
						log.warn("Problem getting popTypeId. userId: " +  user.getId());
					}
				}
			}
		}
		// set admin sum deposits if not 0
		if (dynamicSumDeposits > 0){
			bonusUser.setSumDeposits(dynamicSumDeposits*100);
		}

		boolean res = BonusManager.insertBonusUser(bonusUser, user,AdminManager.getWriterId(), popEntryId, bonusPopLimitTypeId, true, dynamicWagering);
		if (res == false) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.new.bonus", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
            
			return null;
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("new.bonus.succeed", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
            
			UsersManager.loadUserStripFields(user, true, null);
		}
		updateList();
		return getNavigation();
	}

	/**
	 * Cancel bonus for user
	 * @return 
	 * @throws SQLException
	 */
	public String cancel() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String msg = "";
		try {
			bonusUser.setWriterIdCancel(Utils.getWriter().getWriter().getId());
			msg = BonusManagerBase.cancelBonus(bonusUser,user.getUtcOffset(), Utils.getWriter().getWriter().getId(), user.getSkinId(), user, 0L, CommonUtil.getIPAddress());
		} catch (Exception e) {
			log.error("BMS error!!! In cancel BonusUsersForm.", e);
			msg = "BMS.error.exception";
		}
		updateList();
		if (!CommonUtil.isParameterEmptyOrNull(msg)) {
			msg = CommonUtil.getMessage(msg, null, Utils.getWriterLocale(context));
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
			context.addMessage(null, fm);
		}
		
		return null;
	}

	/**
	 * update Wagering for user
	 * @return
	 * @throws SQLException
	 */
	public String updateWagering() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		try{
			BonusManager.updateBonusWageringParam(bonusUser.getId(), wageringParam);
			updateList();
		} catch (Exception e) {
			String errorMsg = CommonUtil.getMessage("error.bonus.update.wagering", null, Utils.getWriterLocale(context));
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,errorMsg,null);
			context.addMessage(null, fm);
            
			log.warn(errorMsg,e);
		}

		return null;
	}

	public int getListSize() {
		return list.size();
	}


	public String getNavigation() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.isSupportSelected()) {
			return Constants.NAV_BONUS_USERS_SUPPORT;
		} else if (user.isRetentionSelected() || user.isRetentionMSelected()) {
			return Constants.NAV_BONUS_USERS_RETENTION;
		}
		return null;
	}

	/**
	 * Create writer filter SI from bonus users list
	 */
	private void createWriterFilter(){
		HashMap<Long, String> h = new HashMap<Long, String>();
		for (BonusUsers bu :list){
			h.put(new Long(bu.getWriterId()),  bu.getWriterName());
		}
		writerFilterList = CommonUtil.hashMap2SortedArrayList(h);
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the classId
	 */
	public long getClassId() {
		return classId;
	}

	/**
	 * @param classId the classId to set
	 */
	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSkinFilter() {
		if (skinId == Skin.SKINS_ALL_VALUE) {
			return Utils.getWriter().getSkinsToString();
		}
		else {
			return String.valueOf(skinId);
		}
	}


	/**
	 * @return the bonusTypeId
	 */
	public long getBonusTypeId() {
		return bonusTypeId;
	}


	/**
	 * @param bonusTypeId the bonusTypeId to set
	 */
	public void setBonusTypeId(long bonusTypeId) {
		this.bonusTypeId = bonusTypeId;
	}

	/**
	 * @return the bonusStateId
	 */
	public long getBonusStateId() {
		return bonusStateId;
	}

	/**
	 * @param bonusStateId the bonusStateId to set
	 */
	public void setBonusStateId(long bonusStateId) {
		this.bonusStateId = bonusStateId;
	}

	/**
	 * @return the userActionId
	 */
	public long getUserActionId() {
		return userActionId;
	}

	/**
	 * @param userActionId the userActionId to set
	 */
	public void setUserActionId(long userActionId) {
		this.userActionId = userActionId;
	}

	/**
	 * @return the bonusUser
	 */
	public BonusUsers getBonusUser() {
		return bonusUser;
	}

	/**
	 * @param bonusUser the bonusUser to set
	 */
	public void setBonusUser(BonusUsers bonusUser) {
		this.bonusUser = bonusUser;
	}

	public void changeDescByType(ValueChangeEvent event) throws SQLException, UnsupportedEncodingException {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest)context.getExternalContext().getRequest();
		bonusTypeId = Long.parseLong(req.getParameter("bonusUsersForm:bonusId"));
		//(String) event.getNewValue();
	}


	/**
	 * @return the wageringParam
	 */
	public long getWageringParam() {
		return wageringParam;
	}


	/**
	 * @param wageringParam the wageringParam to set
	 */
	public void setWageringParam(long wageringParam) {
		this.wageringParam = wageringParam;
	}


	/**
	 * @return the dynamicSumDeposits
	 */
	public long getDynamicSumDeposits() {
		return dynamicSumDeposits;
	}


	/**
	 * @param dynamicSumDeposits the dynamicSumDeposits to set
	 */
	public void setDynamicSumDeposits(long dynamicSumDeposits) {
		bonusUser.setSumDeposits(dynamicSumDeposits * 100);
		this.dynamicSumDeposits = dynamicSumDeposits;
	}


	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}


	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the writerFilterList
	 */
	public ArrayList<SelectItem> getWriterFilterList() {
		return writerFilterList;
	}

	/**
	 * @param writerFilterList the writerFilterList to set
	 */
	public void setWriterFilterList(ArrayList<SelectItem> writerFilterList) {
		this.writerFilterList = writerFilterList;
	}

	/**
	 * 
	 * @return isBonusAmount
	 */
	public String getIsBonusAmount() {
		return isBonusAmount;
	}

	/**
	 * 
	 * @param isBonusAmount
	 */
	public void setIsBonusAmount(String isBonusAmount) {
		this.isBonusAmount = isBonusAmount;
	}
	
	/**
	 * @return
	 */
	public boolean getIsBonusAmountBoolean() {
		if (isBonusAmount == null) {
			return false;
		}
		return isBonusAmount.equals("1");
	}

	
	/**
	 * Change bonusAmount radio button
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateChange(ValueChangeEvent event) throws SQLException, ParseException{
    	String newIsBonusAmount = (String)event.getNewValue();
    	if (!isBonusAmount.equals(newIsBonusAmount)){
    		isBonusAmount = newIsBonusAmount;
    	}
    }


	public long getSumInvestOrAmount() {
		return sumInvestOrAmount;
	}


	public void setSumInvestOrAmount(long sumInvestOrAmount) {
		long bonusId = bonusUser.getBonusId();
		try {
			if (bonusId == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID){
				if (getIsBonusAmountBoolean()){
					Bonus b = BonusManager.getBonusById(bonusId);
					long bonusAmount = sumInvestOrAmount*100;
					bonusUser.setBonusAmount(bonusAmount);
					bonusUser.setSumInvQualify(bonusAmount * b.getTurnoverParam());
				} else {
					Bonus b = BonusManager.getBonusById(bonusId);
					long sumInvest = sumInvestOrAmount*100;
					bonusUser.setSumInvQualify(sumInvest);
					if (sumInvest != ConstantsBase.SUM_INVEST_OR_AMOUNT_ZERO){
						bonusUser.setBonusAmount(sumInvest / b.getTurnoverParam());
					} else {
						bonusUser.setBonusAmount(ConstantsBase.SUM_INVEST_OR_AMOUNT_ZERO);
					}
				}
			}
		} catch (SQLException e) {
			log.error("Exception Insert get bonus by id", e);
		}
		this.sumInvestOrAmount = sumInvestOrAmount;
	}
	
	public void updateChangeBonus(ValueChangeEvent event) throws SQLException{
		String bonusId = (String.valueOf(event.getNewValue()));
		if (null != bonusId) {
			if (Long.parseLong(bonusId) == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
	    		bonusUser.setBonusId(ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID);
	    		displaySumInvestAndAmount = true;
	    		displayDynamicSumDeposits = false;
	    		displayDinamicWagering = false;
	    	} else if (Long.parseLong(bonusId) == ConstantsBase.DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID) {
	    		bonusUser.setBonusId(ConstantsBase.DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID);
	    		displaySumInvestAndAmount = false;
	    		displayDynamicSumDeposits =true;
	    		displayDinamicWagering = false;
	    	} else {
	    		bonusUser.setBonusId(Long.parseLong(bonusId));
	    		displaySumInvestAndAmount = false;
	    		displayDynamicSumDeposits = false;
	    		displayDinamicWagering = true;
	    	}
		}	
	}


	public boolean isDisplaySumInvestAndAmount() {
		return displaySumInvestAndAmount;
	}


	public void setDisplaySumInvestAndAmount(boolean displaySumInvestAndAmount) {
		this.displaySumInvestAndAmount = displaySumInvestAndAmount;
	}


	/**
	 * @return the descriptions
	 */
	public String getDescriptions() {
		String out = "";
		long bonusId = bonusUser.getBonusId();
		if (bonusId != 0) {
			try {
				long currencyId = Utils.getUser().getCurrencyId();
				Bonus b = BonusManager.getBonusById(bonusId);
				BonusCurrency bc = BonusManager.getBonusCurrency(bonusId, currencyId);
				BonusHandlerBase bh = BonusHandlerFactory.getInstance(b.getTypeId());
				long popTypeId = 0;
				long popEnteyId = 0;
				int bonusPopLimitTypeId = 0;
				long userId = 0;

				out = "Bonus will be granted for default period of "+b.getDefaultPeriod()+" days <br/>";

				// Population limits
				if (b.isCheckLimit()){
					FacesContext context = FacesContext.getCurrentInstance();
					User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
					if (user.isRetentionSelected() || user.isRetentionMSelected() || user.isSupportSelectedWithMOLocking()) {
						userId = user.getId();
						PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
						if (null != entry){
							popTypeId = entry.getCurrPopualtionTypeId();

							if (popTypeId != 0){
								popEnteyId = entry.getCurrEntryId();
						    	WriterWrapper wr = Utils.getWriter();
						    	bonusPopLimitTypeId = wr.getBonusPopLimitType(popTypeId);
							}else{
								log.warn("Problem getting popTypeId. userId: " + userId);
							}
						}else{
							log.warn("Problem getting PopulationEntry for check bonus pop limits ");
						}
					}
				}
				
				if (dynamicWagering > 0) {
					b.setWageringParameter(dynamicWagering);
				}

				if (null != bh){
					if (bonusUser.getBonusId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID ||
							bonusUser.getBonusId() == ConstantsBase.DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID){
						out+= BonusMessageHandler.getBonusMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusUser.getBonusAmount(), bonusUser.getSumInvQualify(), dynamicSumDeposits*100);
					} else {
						out+= BonusMessageHandler.getBonusMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, 0, 0, 0);
					}
				}
				dynamicSumDeposits = bc.getSumDeposits();
			} catch (Exception e) {
				log.log(Level.ERROR,
						"Can't write reply for bonus description request. bonusId="+bonusUser.getBonusId(), e);
			}
		}
		return out;
	}


	/**
	 * @param descriptions the descriptions to set
	 */
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}


	/**
	 * @return the displayDynamicSumDeposits
	 */
	public boolean isDisplayDynamicSumDeposits() {
		return displayDynamicSumDeposits;
	}


	/**
	 * @param displayDynamicSumDeposits the displayDynamicSumDeposits to set
	 */
	public void setDisplayDynamicSumDeposits(boolean displayDynamicSumDeposits) {
		this.displayDynamicSumDeposits = displayDynamicSumDeposits;
	}


	/**
	 * @return the dynamicWagering
	 */
	public long getDynamicWagering() {
		return dynamicWagering;
	}


	/**
	 * @param dynamicWagering the dynamicWagering to set
	 */
	public void setDynamicWagering(long dynamicWagering) {
		this.dynamicWagering = dynamicWagering;
	}


	/**
	 * @return the displayDinamicWagering
	 */
	public boolean isDisplayDinamicWagering() {
		return displayDinamicWagering;
	}


	/**
	 * @param displayDinamicWagering the displayDinamicWagering to set
	 */
	public void setDisplayDinamicWagering(boolean displayDinamicWagering) {
		this.displayDinamicWagering = displayDinamicWagering;
	}
	
}
