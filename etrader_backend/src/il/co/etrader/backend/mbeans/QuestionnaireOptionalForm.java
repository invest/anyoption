package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class QuestionnaireOptionalForm implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3904024132964360116L;

	private static final Logger log = Logger.getLogger(QuestionnaireOptionalForm.class);

	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private List<QuestionnaireUserAnswer> multipleUserAnswers;
	private List<QuestionnaireQuestion> questions;
	private Long[] checkedAnsweredIds;
	private UserBase user;
	private UserRegulationBase userRegulation;
	private Writer writer;
	
	public QuestionnaireOptionalForm() throws SQLException{
		WriterWrapper ww = Utils.getWriter();
		writer = ww.getWriter();
		
		questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME);
		
		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		questions = findAndAddMultipleAnswerQuestion();
		
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questions, writer.getId());
		multipleUserAnswers = userAnswers.subList(userAnswers.size() - 4, userAnswers.size());		
		checkedAnsweredIds = addCheckedAnsweredIds();
	   	userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	UserRegulationManager.getUserRegulation(userRegulation);			
	}
	
	public String submitQuestionnaire () throws SQLException{
		
		boolean notAllowToSubmit = isEdit();
		if(!notAllowToSubmit){
			List<QuestionnaireAnswer > checkboxQuestionAnswers = questions.get(questions.size() - 1).getAnswers();
			for (int idx = 0; idx < checkboxQuestionAnswers.size(); idx++) {
				boolean found = false;
				for (Long checkedAnswer : checkedAnsweredIds) {
					if (checkedAnswer != null && checkedAnswer == checkboxQuestionAnswers.get(idx).getId()) {
						multipleUserAnswers.get(idx).setAnswerId(checkedAnswer);
						found = true;
					}
				}
				if (!found) {
					multipleUserAnswers.get(idx).setAnswerId(null);
				}				
			}
			
			for (QuestionnaireUserAnswer answer : userAnswers){
				answer.setWriterId(writer.getId());
			}
			QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);
			userRegulation.setOptionalQuestionnaireDone(true);
			userRegulation.setWriterId(writer.getId());
			UserRegulationManager.updateOptionalQuestionnaireStatus(userRegulation);
	    	FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("questionnaire.optional.submit", null),null);
			context.addMessage(null, fm);					
			return Utils.navAfterUserOperation();
		} else {
			return null;
		}		
	}	

	public List<QuestionnaireQuestion> getQuestions() {
		return questions;
	}
	
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	public List<QuestionnaireUserAnswer> getMultipleUserAnswers() {
		return multipleUserAnswers;
	}

	public Long[] getCheckedAnsweredIds() {
		return checkedAnsweredIds;
	}
	
	public void setCheckedAnsweredIds(Long[] checkedAnsweredIds) {
		this.checkedAnsweredIds = checkedAnsweredIds;
	}
	
	public boolean isEdit() {
		if (userRegulation.isOptionalQuestionnaireDone()) { 
			if(writer.getId() == ConstantsBase.REUT_USER_ID ||  writer.getId() == ConstantsBase.NAAMA_USER_ID || writer.getId() == ConstantsBase.QABG_USER_ID) {
				return false;
			} else {
				return true;
			}			
		} else {
			return false;
		}		
	}

	private List<QuestionnaireQuestion> findAndAddMultipleAnswerQuestion() {
		QuestionnaireQuestion multipleAnswerQuestion = null;
		multipleAnswerQuestion = questionnaire.getQuestions().get(5);
		ArrayList<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion> (questionnaire.getQuestions());
		questions.remove(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		questions.add(multipleAnswerQuestion);
		
		return questions;
	}	
	
	private Long[] addCheckedAnsweredIds() {
		Long[] ids = new Long[multipleUserAnswers.size()];
		int index = 0;
		for (QuestionnaireUserAnswer userAnswer : multipleUserAnswers) {
			if (userAnswer.getAnswerId() != null) {
				ids[index++] = userAnswer.getAnswerId().longValue();
			}
		}		
		return ids;
	}	
}
