package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.DepositsSummary;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;


public class DepositsSummaryForm implements Serializable {

	private static final long serialVersionUID = -5011753706723094515L;
	private static final Logger logger = Logger.getLogger(DepositsSummaryForm.class);
	
	private ArrayList<DepositsSummary> depositsSummaryList;
	
	public DepositsSummaryForm() {	
		search();
	}
	
	public void search() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		try {
			depositsSummaryList = TransactionsManager.getDepositsSummaryList(user.getId());
			CommonUtil.setTablesToFirstPage();
		} catch (Exception e) {
			logger.debug("Can't getDepositsSummaryList for userId " + user.getId(), e);
		} 		
	}

	public ArrayList<DepositsSummary> getDepositsSummaryList() {
		return depositsSummaryList;
	}

	public void setDepositsSummaryList(ArrayList<DepositsSummary> depositsSummaryList) {
		this.depositsSummaryList = depositsSummaryList;
	}

	public int getListSize() {
		return depositsSummaryList.size();
	}
	
}
