package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.managers.InvestmentsManagerBase;

import il.co.etrader.backend.bl_managers.TiersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.InvestmentFormatter;

public class LoyaltyHistoryForm implements Serializable {
	private static final long serialVersionUID = 6474436628906721777L;

	private ArrayList<TierUserHistory> list;;
	private TierUserHistory userHis;

	public LoyaltyHistoryForm() throws SQLException{
		updateList();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateList() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		list = TiersManager.getUserPointsHistory(user.getId());
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String getRefundUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundUp(i), i.getCurrencyId());
	}

	public String getRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevelTxt(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCanceledWriterName(i);
	}

	public String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getIsCanceledTxt(i);
	}

	public String getScheduledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return il.co.etrader.bl_managers.InvestmentsManagerBase.getScheduledTxt(i.getScheduledId());
	}

	public ArrayList<TierUserHistory> getList() {
		return list;
	}

	public void setList(ArrayList<TierUserHistory> list) {
		this.list = list;
	}

	/**
	 * @return the userHis
	 */
	public TierUserHistory getUserHis() {
		return userHis;
	}

	/**
	 * @param userHis the userHis to set
	 */
	public void setUserHis(TierUserHistory userHis) {
		this.userHis = userHis;
	}
}
