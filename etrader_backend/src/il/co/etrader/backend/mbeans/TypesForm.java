package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingType;
import il.co.etrader.util.CommonUtil;



public class TypesForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String typeName;
	private ArrayList<MarketingType> list;
	private MarketingType type;

	public TypesForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingTypes(typeName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert type
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		type.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateType(type);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_TYPES;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit type
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_TYPE;
	}

	/**
	 * Navigate to insert new type
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		type = new MarketingType();
		type.setWriterId(AdminManager.getWriterId());
		type.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_TYPE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the type
	 */
	public MarketingType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(MarketingType type) {
		this.type = type;
	}

}
