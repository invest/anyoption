package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.UserRegulationHistory;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;

public class UserRegulationHistoryForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5002660298538389018L;
	private  ArrayList<UserRegulationHistory> list;
	private UserBase user;
	
	// Filter params
	private Date from;
	private Date to;
	
	public UserRegulationHistoryForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		FacesContext context = FacesContext.getCurrentInstance();
		UserBase user = (UserBase) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		setUser(user);
		from = user.getTimeCreated();
		to = gc.getTime();
		search();
	}
	
	public String search() throws SQLException {
		if(to.before(from) || to == null || from == null) {
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("regulation.history.invalid.date.range", null, Utils.getWriterLocale(context)),null);
			context.addMessage("userRegulationHistoryForm:from", fm);
			return null;
		}
		list = UsersManager.getUserRegulationHistory(user.getId(), from, to);
		return null;
	}
	
	public String getDateTimeByWriterOffset(Date timeCreated){
		return CommonUtil.getDateAndTimeFormat(timeCreated, Utils.getWriter().getUtcOffset());
	}
		 

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public ArrayList<UserRegulationHistory> getList() {
		return list;
	}

	public void setList(ArrayList<UserRegulationHistory> list) {
		this.list = list;
	}
	
	public int getListSize() {
		return list.size();
	}

	public UserBase getUser() {
		return user;
	}

	public void setUser(UserBase user) {
		this.user = user;
	}

}
