package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingTerritory;
import il.co.etrader.util.CommonUtil;



public class TerritoriesForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String territoryName;
	private ArrayList<MarketingTerritory> list;
	private MarketingTerritory territory;

	public TerritoriesForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingTerritories(territoryName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert territory
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		territory.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateTerriroty(territory);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_TERRITORIES;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit territory
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_TERRITORY;
	}

	/**
	 * Navigate to insert new territory
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		territory = new MarketingTerritory();
		territory.setWriterId(AdminManager.getWriterId());
		territory.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_TERRITORY;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the territory
	 */
	public MarketingTerritory getTerritory() {
		return territory;
	}

	/**
	 * @param territory the territory to set
	 */
	public void setTerritory(MarketingTerritory territory) {
		this.territory = territory;
	}

	/**
	 * @return the territoryName
	 */
	public String getTerritoryName() {
		return territoryName;
	}

	/**
	 * @param territoryName the territoryName to set
	 */
	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}




}
