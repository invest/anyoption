package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.AssetIndexMarketManager;
import il.co.etrader.backend.bl_vos.AssetIndexMarket;
import il.co.etrader.backend.cms.CmsGuiBindingsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class AssetIndexMarketForm implements Serializable {


	private static final long serialVersionUID = 32128806730796327L;

	public static final Logger log = Logger.getLogger(AssetIndexMarketForm.class);
	
	private AssetIndexMarket assetIndexMarket;
	private ArrayList list;
	private String marketName;
	private Long groupId;
	private Long skinId;
	CmsGuiBindingsManager cmsManager;

	public AssetIndexMarketForm() throws SQLException{
		groupId = new Long(0);
		skinId =  new Long(0);
		list = new ArrayList<AssetIndexMarket>();
		cmsManager = new CmsGuiBindingsManager("#{Jmx}");
	}

	public String navEdit() {
		return Constants.NAV_ASSET_INDEX_MARKET;
	}

	public String search() throws SQLException{

		list = AssetIndexMarketManager.getAssetIndexMarketsList(marketName, skinId, groupId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	

	public String update() throws SQLException{
		AssetIndexMarketManager.update(assetIndexMarket);
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null), null);	
		context.addMessage(null, fm);
		return Constants.NAV_ASSET_INDEXES_MARKET;
	}
	
	public String initCaches(String application){	
		
		log.debug("WriterId:" + Utils.getWriter().getWriter().getId() + " run asset index refresh");
		boolean res = true;
		String msg = "asset.index.market.refresh.servers.ok";
		String resulTx = " ";
		ArrayList<String> serverResult = new ArrayList<String>();
		try {
			res = cmsManager.initAssetIndexMarket(application, serverResult);
		} catch (Exception e) {
			log.error("When initCahcne on:" + application, e);
			res = false;
		}
		
		//Show msg
		if(!res){
			msg = "asset.index.market.refresh.servers.error";
		}		

		for (String txt : serverResult) {
			resulTx += " " + txt;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null), null);	
		FacesMessage fm2 = new FacesMessage(FacesMessage.SEVERITY_INFO,application + resulTx, null);
		context.addMessage(null, fm);
		context.addMessage(null, fm2);
		
	
		return null;
	}

	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public Long getSkinId() {
		return skinId;
	}

	public void setSkinId(Long skinId) {
		this.skinId = skinId;
	}

	public AssetIndexMarket getAssetIndexMarket() {
		return assetIndexMarket;
	}

	public void setAssetIndexMarket(AssetIndexMarket assetIndexMarket) {
		this.assetIndexMarket = assetIndexMarket;
	}
}
