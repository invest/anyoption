package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TransactionsIssuesManager;
import il.co.etrader.backend.bl_vos.SalesDepositsTotals;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


/**
 * Sales depositors TV Bean
 * @author Kobi
 *
 */
public class SalesDepositsTVForm implements Serializable {

	private static final long serialVersionUID = 3833405861644662005L;

	private ArrayList<SalesDepositsTotals> totalsList;
	private ArrayList<SalesDepositsTotals> totalsMixedList;   // etrader & anyoption writers
	private ArrayList<SalesDepositsTotals> conTotalsRow;

	private int tvFirstScreenView;
	private int tvSecondScreenView;
	private String viewName;
	private boolean isEtraderView;
	// members after online deposit via Backend
	private int viewAfterOnlineDeposit;    // page view
	private String repName;                // writer name
	private String repDepositAmount;	   // deposit amount
	private int repDepositType;
	private int tvDailyScreenView;
	private boolean etraderOnly;			   //if only Etrader rep's will be in the table
	private long skinId;

	public SalesDepositsTVForm() throws SQLException {
		initSalesDepositsTVForm();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		etraderOnly = Boolean.parseBoolean(request.getParameter("etraderOnly"));
		String writerUserName = Utils.getWriter().getWriter().getUserName();
		String skinFromWriterUserName = writerUserName.replace("SALESTV", "");
		skinId = 0;
		if (!CommonUtil.isParameterEmptyOrNull(skinFromWriterUserName)) {			
			skinId = ApplicationData.getSkinByLanguageCode(skinFromWriterUserName.toLowerCase(), false);
		}
//		if (context.getViewRoot().getViewId().indexOf("salesDepositsFirstTV") > -1) {
//			updateViewNameFirstPage();
//			searchTV1();
//		} else
		if (context.getViewRoot().getViewId().indexOf("salesDepositsSecondTV") > -1) {
			updateViewNameSecondPage();
			searchTV2();
		} else {  // daily all skins
			updateViewNameDAilyPage();
			searchDailyTV();
		}
	}

	public void initSalesDepositsTVForm() throws SQLException {
//		tvFirstScreenView = Constants.DAILY_DEPOSITS_ET;
		tvSecondScreenView = Constants.DAILY_DEPOSITS;
		tvDailyScreenView = Constants.DAILY_DEPOSITS_CONVERSION;
		viewAfterOnlineDeposit = -1;
		totalsList = new ArrayList<SalesDepositsTotals>();
		isEtraderView = true;
		totalsList = new ArrayList<SalesDepositsTotals>();
	}

	public int getTotalsListSize() {
		return totalsList.size();
	}

//	public String searchTV1() throws SQLException {
//		totalsList = TransactionsIssuesManager.getSalesWriterDepositsTv(getPeriodOfPage(true), isEtraderView, false);
//		return null;
//	}

//	public String searchTV1NextView() throws SQLException {
//		calcNextTVFirstView();
//		updateViewNameFirstPage();
//		totalsList = TransactionsIssuesManager.getSalesWriterDepositsTv(getPeriodOfPage(true), isEtraderView, false);
//		return null;
//	}

	public String searchTV2() throws SQLException {
		totalsList = TransactionsIssuesManager.getSalesWriterDepositsTv(getPeriodOfPage(false), isEtraderView, false, etraderOnly);
		conTotalsRow = new ArrayList<SalesDepositsTotals>();
		conTotalsRow.add(totalsList.remove(totalsList.size()-1));
		return null;
	}

	public String searchTV2NextView() throws SQLException {
		calcNextTVSecondView();
		updateViewNameSecondPage();
		totalsList = TransactionsIssuesManager.getSalesWriterDepositsTv(getPeriodOfPage(false), isEtraderView, false, etraderOnly);
		conTotalsRow = new ArrayList<SalesDepositsTotals>();
		conTotalsRow.add(totalsList.remove(totalsList.size()-1));
		return null;
	}

	public String searchDailyTV() throws SQLException {
		totalsMixedList = TransactionsIssuesManager.getConversionWriterDepositsTv(false, true, true, false, skinId);
		conTotalsRow = new ArrayList<SalesDepositsTotals>();
		conTotalsRow.add(totalsMixedList.remove(totalsMixedList.size()-2));
		conTotalsRow.add(totalsMixedList.remove(totalsMixedList.size()-1));
		return null;
	}

	public String searchDailyNextView() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		tvDailyScreenView++;
		tvDailyScreenView = (tvDailyScreenView % 2);
		updateViewNameDAilyPage();
		boolean isDaily = true;
		if (tvDailyScreenView == Constants.MONTHLY_DEPOSITS_CONVERSION) {
			isDaily = false;
		}
		if (context.getViewRoot().getViewId().indexOf("salesDepositsSecondTV") > -1) {
			totalsMixedList = TransactionsIssuesManager.getConversionWriterDepositsTv(false, true, isDaily, false, skinId);
		} else {  // daily all skins
			totalsMixedList = TransactionsIssuesManager.getConversionWriterDepositsTv(false, true, isDaily, false, skinId);
		}

		conTotalsRow = new ArrayList<SalesDepositsTotals>();
		conTotalsRow.add(totalsMixedList.remove(totalsMixedList.size()-2));
		conTotalsRow.add(totalsMixedList.remove(totalsMixedList.size()-1));
		return null;
	}

	public ArrayList<SalesDepositsTotals> getTotalsList() {
		return totalsList;
	}

	public void setTotalsList(ArrayList<SalesDepositsTotals>  list) {
		this.totalsList = list;
	}

//	/**
//	 * get the next page view for tv1
//	 * @return
//	 */
//	public void calcNextTVFirstView() {
//		if (viewAfterOnlineDeposit != -1) {
//			tvFirstScreenView = viewAfterOnlineDeposit;
//			viewAfterOnlineDeposit = -1;
//		} else {
//			tvFirstScreenView++;
//			tvFirstScreenView = (tvFirstScreenView % Constants.DEPOSIT_TV_FIRST_VIEWS);
//		}
//	}

	/**
	 * get the next page view for tv2
	 * @return
	 */
	public void calcNextTVSecondView() {
		if (viewAfterOnlineDeposit != -1) {
			tvSecondScreenView = viewAfterOnlineDeposit;
			viewAfterOnlineDeposit = -1;
		} else {
			tvSecondScreenView++;
			tvSecondScreenView = (tvSecondScreenView % Constants.DEPOSIT_TV_SECOND_VIEWS);
		}
	}

	/**
	 * @return the tvFirstScreenView
	 */
	public int getTvFirstScreenView() {
		return tvFirstScreenView;
	}

	/**
	 * @param tvFirstScreenView the tvFirstScreenView to set
	 */
	public void setTvFirstScreenView(int tvFirstScreenView) {
		this.tvFirstScreenView = tvFirstScreenView;
	}

	/**
	 * @return the tvSeconfScreenView
	 */
	public int getTvSecondScreenView() {
		return tvSecondScreenView;
	}

	/**
	 * @param tvSeconfScreenView the tvSeconfScreenView to set
	 */
	public void setTvSecondScreenView(int tvSecondScreenView) {
		this.tvSecondScreenView = tvSecondScreenView;
	}

	/**
	 * @return the viewAfterOnlineDeposit
	 */
	public int getViewAfterOnlineDeposit() {
		return viewAfterOnlineDeposit;
	}

	/**
	 * @param viewAfterOnlineDeposit the viewAfterOnlineDeposit to set
	 */
	public void setViewAfterOnlineDeposit(int viewAfterOnlineDeposit) {
		this.viewAfterOnlineDeposit = viewAfterOnlineDeposit;
	}

	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}

	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

//	public void updateViewNameFirstPage() {
//		String key = "";
//		if (tvFirstScreenView == Constants.DAILY_DEPOSITS_ET) {
//			key = "sales.deposit.tv.view.et.daily";
//			isEtraderView = true;
//		} else if (tvFirstScreenView == Constants.WEEKLY_DEPOSITS_ET) {
//			key = "sales.deposit.tv.view.et.weekly";
//			isEtraderView = true;
//		} else if (tvFirstScreenView == Constants.DAILY_DEPOSITS_AO) {
//			key = "sales.deposit.tv.view.ao.daily";
//			isEtraderView = false;
//		} else if (tvFirstScreenView == Constants.WEEKLY_DEPOSITS_AO) {
//			key = "sales.deposit.tv.view.ao.weekly";
//			isEtraderView = false;
//		} else {
//			this.viewName = "";
//		}
//		this.viewName = CommonUtil.getMessage(key, null);
//	}

	public void updateViewNameSecondPage() {
		String key = "";
		if (tvSecondScreenView == Constants.DAILY_DEPOSITS) {
			key = "sales.deposit.tv.view.et.daily";
		} else if (tvSecondScreenView == Constants.MONTHLY_DEPOSITS) {
			key = "sales.deposit.tv.view.et.monthly";
		} else {
			this.viewName = "";
		}
		this.viewName = CommonUtil.getMessage(key, null);
	}

	public void updateViewNameDAilyPage() {
		String key = "";
		if (tvDailyScreenView == Constants.DAILY_DEPOSITS_CONVERSION) {
			key = "sales.deposit.tv.view.et.daily";
		} else if (tvDailyScreenView == Constants.MONTHLY_DEPOSITS_CONVERSION) {
			key = "sales.deposit.tv.view.et.monthly";
		} else {
			this.viewName = "";
		}
		this.viewName = CommonUtil.getMessage(key, null);
	}

	public boolean getIsTvDailyScreenView() {
		if (tvDailyScreenView == Constants.MONTHLY_DEPOSITS_CONVERSION) {
			return false;
		}
		return true;
	}

	/**
	 * Get page period for SQL
	 * @param isFirstTV
	 * @return
	 */
	public int getPeriodOfPage(boolean isFirstTV) {
		int period = 0;
//		if (isFirstTV) {
//			if (tvFirstScreenView == Constants.DAILY_DEPOSITS_ET ||
//					tvFirstScreenView == Constants.DAILY_DEPOSITS_AO) {
//				period = Constants.SALES_DEPOSIT_DAILY_VIEW;
//			} else if (tvFirstScreenView == Constants.WEEKLY_DEPOSITS_ET ||
//					tvFirstScreenView == Constants.WEEKLY_DEPOSITS_AO) {
//				period = Constants.SALES_DEPOSIT_WEEKLY_VIEW;
//			}
//		} else {
		if (tvSecondScreenView == Constants.DAILY_DEPOSITS) {
			period = Constants.SALES_DEPOSIT_DAILY_VIEW;
		} else if (tvSecondScreenView == Constants.MONTHLY_DEPOSITS) {
			period = Constants.SALES_DEPOSIT_MONTHLY_VIEW;
		}
//		else if (tvSecondScreenView == Constants.WEEKLY_DEPOSITS_ET ||
//				tvSecondScreenView == Constants.WEEKLY_DEPOSITS_AO) {
//			period = Constants.SALES_DEPOSIT_WEEKLY_VIEW;
//		}
//		}
		return period;
	}

	/**
	 * @return the repDepositAmount
	 */
	public String getRepDepositAmount() {
		return repDepositAmount;
	}

	/**
	 * @param repDepositAmount the repDepositAmount to set
	 */
	public void setRepDepositAmount(String repDepositAmount) {
		this.repDepositAmount = repDepositAmount;
	}

	/**
	 * @return the repName
	 */
	public String getRepName() {
		return repName;
	}

	/**
	 * @param repName the repName to set
	 */
	public void setRepName(String repName) {
		this.repName = repName;
	}

	/**
	 * @return the totalsMixedList
	 */
	public ArrayList<SalesDepositsTotals> getTotalsMixedList() {
		return totalsMixedList;
	}

	/**
	 * @param totalsMixedList the totalsMixedList to set
	 */
	public void setTotalsMixedList(ArrayList<SalesDepositsTotals> totalsMixedList) {
		this.totalsMixedList = totalsMixedList;
	}

	/**
	 * @return the conTotalsRow
	 */
	public ArrayList<SalesDepositsTotals> getConTotalsRow() {
		return conTotalsRow;
	}

	/**
	 * @param conTotalsRow the conTotalsRow to set
	 */
	public void setConTotalsRow(ArrayList<SalesDepositsTotals> conTotalsRow) {
		this.conTotalsRow = conTotalsRow;
	}

	/**
	 * @return the repDepositType
	 */
	public int getRepDepositType() {
		return repDepositType;
	}

	/**
	 * @param repDepositType the repDepositType to set
	 */
	public void setRepDepositType(int repDepositType) {
		this.repDepositType = repDepositType;
	}

	public boolean isConversionDeposit(){
		return repDepositType == ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
	}

	/**
	 * @return the etraderOnly
	 */
	public boolean isEtraderOnly() {
		return etraderOnly;
	}

	/**
	 * @param etraderOnly the etraderOnly to set
	 */
	public void setEtraderOnly(boolean etraderOnly) {
		this.etraderOnly = etraderOnly;
	}
}