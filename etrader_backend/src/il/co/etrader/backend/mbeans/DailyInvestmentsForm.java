package il.co.etrader.backend.mbeans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.CurrenciesManagerBase;

import il.co.etrader.backend.bl_managers.ApiExternalUsersManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_vos.ApiExternalUser;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.CopyopInvestmentDetails;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;

public class DailyInvestmentsForm implements Serializable {

    private static final long serialVersionUID = -476321898003866278L;

    private static final Logger logger = Logger.getLogger(DailyInvestmentsForm.class);

    private static final String DELEMITER = ",";

    private long id;
    private String userName;
    private long fromHour;
    private long fromMin;
    private long toHour;
    private long toMin;
    private Date fromDate;
    private Date toDate;
    private long invAmount;
    private long invAmountBelow;
    private long invCloseHour;
    private long invCloseMin;
    private long invType;
    private long totalAmount;
    private ArrayList<Investment> investmentsList;
    private final String TIME_CREATED = "i.time_created";
    private final String TIME_SETTLED = "i.time_settled";
    private final int NON_ACTIVE_FIELD = 0;
    private final int NUM_OF_ROWS = 27;
    private int houseResult;
    private String isSettled;
    private ArrayList<SelectItem> marketsList;
    // paging variables
    private long lastInvestmentId;
    private long scheduled;

    private ArrayList<SelectItem> countriesSI;
    long skinBusinessCaseId;
    long countryId;

    // copyop
    private int copyopInvTypeId;
    private long fromUserId;
    private HashMap<Long, CopyopInvestmentDetails> copyopInvestmentDetailsList;

    // for api external user
    private long apiExternalUserId;
    private String apiExternalReference;
    private long apiUserId;

    // many lisy box
    private List<String> skins;
    private List<String> groups;
    private List<String> markets;
    private List<String> opportunitiesList;
    // TODO: change this value to be associated with platform table (currently
    // it's by writer)
    private List<String> insertedFrom;

    // sorting
    private String sortColumn;
    private String prevSortColumn;
    private boolean sortAscending = false;
    private boolean activateAscending;
    private Investment baseSumInvestments;
    private long statusId;
    
    public Investment getBaseSumInvestments() {
	return baseSumInvestments;
    }

    public void setBaseSumInvestments(Investment sumInvestments) {
	this.baseSumInvestments = sumInvestments;
    }

    public DailyInvestmentsForm() throws NumberFormatException, SQLException {
	initComponents();
	updateList();
    }

    /**
     * Initialize Components
     */
    public void initComponents() {

	investmentsList = new ArrayList<Investment>();
	userName = "";
	fromHour = 99;
	fromMin = 99;
	toHour = 99;
	toMin = 99;
	invAmount = 0;
	invAmountBelow = 0;
	invCloseHour = 99;
	invCloseMin = 99;
	invType = 0;
	lastInvestmentId = 0;
	houseResult = 0;
	sortColumn = "";
	prevSortColumn = "";
	skins = new ArrayList<String>();
	markets = new ArrayList<String>();
	groups = new ArrayList<String>();
	insertedFrom = new ArrayList<String>();
	marketsList = CommonUtil.map2SelectList(Utils.getWriter().getActiveMarketsAndGroupsWithOneTouch());
	skins.add("0");
	markets.add("0");
	groups.add("0");
	insertedFrom.add("-1");
	opportunitiesList = new ArrayList<String>();
	opportunitiesList.add("0");
	isSettled = "0";
	totalAmount = 0;
	activateAscending = false;
	countriesSI = new ArrayList<SelectItem>();
	apiUserId = -2;
	apiExternalReference = "";
	copyopInvTypeId = -1;
	fromUserId = 0;
	copyopInvestmentDetailsList = new HashMap<Long, CopyopInvestmentDetails>();
	statusId = ConstantsBase.INVESTMENT_CLASS_STATUS_ACTIVE; // default to active investment
    }

    /**
     * @return
     * @throws SQLException
     * @throws NumberFormatException
     *             TIME_CREATED
     */
    public String updateList() throws NumberFormatException, SQLException {

	if ((apiUserId != Constants.NONE_FILTER_ID && Utils.isParameterEmptyOrNull(apiExternalReference))
		|| (apiUserId == Constants.NONE_FILTER_ID && !Utils.isParameterEmptyOrNull(apiExternalReference))) {
	    FacesContext context = FacesContext.getCurrentInstance();
	    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(
		    "external.user.strip.error.mandatory.short", null, Utils.getWriterLocale(context)), null);
	    context.addMessage(null, fm);
	    return null;
	} else if (apiUserId != Constants.NONE_FILTER_ID && !Utils.isParameterEmptyOrNull(apiExternalReference)) {
	    apiExternalUserId = -1; // if no such user no data will be return
	    try {
		ApiExternalUser apiExternalUser = ApiExternalUsersManager.getApiExternalUser(0, apiExternalReference,
			apiUserId);
		if (null != apiExternalUser) {
		    apiExternalUserId = apiExternalUser.getId();
		}
	    } catch (SQLException e) {
		logger.error("cant load external user from db reference = " + apiExternalReference + " apiUserId = "
			+ apiUserId, e);
	    }
	}

	setDates();
	totalAmount = 0;
	baseSumInvestments = new Investment();
	baseSumInvestments.setCurrencyId(ConstantsBase.CURRENCY_BASE_ID);
	String useTime = null;
	if (isSettled.equals("0")) {
	    useTime = TIME_CREATED;
	} else {
	    useTime = TIME_SETTLED;
	}
	investmentsList = InvestmentsManager.getInvestmentsList(NON_ACTIVE_FIELD, fromDate, toDate, isSettled, "0,0",
		scheduled, useTime, userName.toUpperCase(), ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE,
		statusId, NON_ACTIVE_FIELD, getListToString(opportunitiesList), 0,
		10000, getListToString(skins), invType, CommonUtil.calcAmount(invAmount),
		CommonUtil.calcAmount(invAmountBelow), invCloseHour, invCloseMin, skinBusinessCaseId,
		getListToString(groups), getListToString(markets), houseResult, true, "desc", false, baseSumInvestments,
		null, getWritersListToString(insertedFrom), countryId, apiExternalUserId, copyopInvTypeId, fromUserId,
		id);
	activateAscending = false;
	sortList();
	if (investmentsList.size() > 0) {
	    lastInvestmentId = investmentsList.get(investmentsList.size() - 1).getId();
	    totalAmount = baseSumInvestments.getAmount();
	}

	HashMap<Long, String> h = new HashMap<Long, String>();
	for (Investment i : investmentsList) {
	    h.put(new Long(i.getCountryId()), i.getCountryName());
	}
	countriesSI = CommonUtil.hashMap2SortedArrayList(h);

	CommonUtil.setTablesToFirstPage();
	return null;
    }

    public void setDates() {
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(CommonUtil.getDateTimeFormaByTz(gc.getTime(), Utils.getWriter().getUtcOffset()));
	if (99 == fromHour || 99 == fromMin) {
	    // set time as 00:00:00
	    gc.set(Calendar.HOUR_OF_DAY, 0);
	    gc.set(Calendar.MINUTE, 0);
	    gc.set(Calendar.SECOND, 0);
	    gc.set(Calendar.MILLISECOND, 0);
	    fromDate = gc.getTime();
	} else {
	    gc.set(Calendar.HOUR_OF_DAY, (int) fromHour);
	    gc.set(Calendar.MINUTE, (int) fromMin);
	    gc.set(Calendar.SECOND, 0);
	    gc.set(Calendar.MILLISECOND, 0);
	    fromDate = gc.getTime();
	}
	if (99 == toHour || 99 == toMin) {
	    // set time as 02:00:00 to next day
	    gc.set(Calendar.HOUR_OF_DAY, 23);
	    gc.set(Calendar.MINUTE, 59);
	    gc.set(Calendar.SECOND, 59);
	    gc.set(Calendar.MILLISECOND, 999);
	    toDate = gc.getTime();
	} else {
	    gc.set(Calendar.HOUR_OF_DAY, (int) toHour);
	    gc.set(Calendar.MINUTE, (int) toMin);
	    gc.set(Calendar.SECOND, 0);
	    gc.set(Calendar.MILLISECOND, 0);
	    toDate = gc.getTime();
	}
    }

    /**
     * Get writers list to string
     * 
     * @param list
     * @return
     */
    public String getWritersListToString(List list) {
	if (list == null || list.size() == 0) {
	    if (null != list) {
		list.add("-1");
	    }
	    return "-1";
	}

	String tmp = ConstantsBase.EMPTY_STRING;
	for (int i = 0; i < list.size(); i++) {
	    tmp += list.get(i) + ",";
	}
	return tmp.substring(0, tmp.length() - 1);
    }

    /**
     * Get list (groups, skins and markets) to string
     * 
     * @param list
     * @return
     */
    public String getListToString(List list) {
	if ((list == skins) && (list == null || list.size() == 0 || list.get(0).equals("0"))) {
	    return Utils.getWriter().getSkinsToString();
	} else if (list == null || list.size() == 0) {
	    if (null != list) {
		list.add("0");
	    }
	    return "0";
	}

	String tmp = ConstantsBase.EMPTY_STRING;
	for (int i = 0; i < list.size(); i++) {
	    tmp += list.get(i) + ",";
	}
	return tmp.substring(0, tmp.length() - 1);
    }

    /**
     * Change settled radio button
     * 
     * @param event
     * @throws SQLException
     * @throws ParseException
     */
    public void updateChange(ValueChangeEvent event) throws SQLException, ParseException {
	String newIsSettled = (String) event.getNewValue();
	if (!isSettled.equals(newIsSettled)) {
	    isSettled = newIsSettled;
	    if (isSettled.equals("0")) {
		houseResult = 0;
	    }
	    updateList();
	}

    }

    /**
     * Change
     * 
     * @param event
     * @throws SQLException
     * @throws ParseException
     */
    public void updateMarkets(ValueChangeEvent event) throws SQLException, ParseException {
	marketsList.clear();
	markets.clear();
	markets.add("0");
	groups = (List<String>) event.getNewValue();
	ArrayList<SelectItem> si;
	if (groups.contains("0")) {
	    si = CommonUtil.map2SelectList(Utils.getWriter().getActiveMarketsAndGroups());
	} else {
	    si = new ArrayList<SelectItem>();
	    si = TraderManager.getMarketsListById(getListToString(groups));
	    //si = WriterWrapper.translateSI(si);
	    Collections.sort(si, new CommonUtil.selectItemComparator());
	}
	marketsList = si;
    }

    /**
     * @return number of rows of the rable
     */
    public int getRowsNum() {
	return NUM_OF_ROWS;
    }

    /**
     * @return the totalAmount
     */
    public String getTotalAmount() {
	return CommonUtil.displayAmount(totalAmount, ConstantsBase.CURRENCY_BASE_ID);
    }

    /**
     * @return the isSettled
     */
    public String getIsSettled() {
	return isSettled;
    }

    /**
     * @param isSettled
     *            the isSettled to set
     */
    public void setIsSettled(String isSettled) {
	this.isSettled = isSettled;
    }

    /**
     * @return the fromHour
     */
    public long getFromHour() {
	return fromHour;
    }

    /**
     * @param fromHour
     *            the fromHour to set
     */
    public void setFromHour(long fromHour) {
	this.fromHour = fromHour;
    }

    /**
     * @return the fromMin
     */
    public long getFromMin() {
	return fromMin;
    }

    /**
     * @param fromMin
     *            the fromMin to set
     */
    public void setFromMin(long fromMin) {
	this.fromMin = fromMin;
    }

    /**
     * @return the toHour
     */
    public long getToHour() {
	return toHour;
    }

    /**
     * @param toHour
     *            the toHour to set
     */
    public void setToHour(long toHour) {
	this.toHour = toHour;
    }

    /**
     * @return the toMin
     */
    public long getToMin() {
	return toMin;
    }

    /**
     * @param toMin
     *            the toMin to set
     */
    public void setToMin(long toMin) {
	this.toMin = toMin;
    }

    /**
     * @return
     */
    public int getInvestmentsListSize() {
	return investmentsList.size();
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * @return the invAmount
     */
    public long getInvAmount() {
	return invAmount;
    }

    /**
     * @param invAmount
     *            the invAmount to set
     */
    public void setInvAmount(long invAmount) {
	this.invAmount = invAmount;
    }

    /**
     * @return the invAmountBelow
     */
    public long getInvAmountBelow() {
	return invAmountBelow;
    }

    /**
     * @param invAmountBelow
     *            the invAmountBelow to set
     */
    public void setInvAmountBelow(long invAmountBelow) {
	this.invAmountBelow = invAmountBelow;
    }

    /**
     * @return the invCloseHour
     */
    public long getInvCloseHour() {
	return invCloseHour;
    }

    /**
     * @param invCloseHour
     *            the invCloseHour to set
     */
    public void setInvCloseHour(long invCloseHour) {
	this.invCloseHour = invCloseHour;
    }

    /**
     * @return the invCloseMin
     */
    public long getInvCloseMin() {
	return invCloseMin;
    }

    /**
     * @param invCloseMin
     *            the invCloseMin to set
     */
    public void setInvCloseMin(long invCloseMin) {
	this.invCloseMin = invCloseMin;
    }

    /**
     * @return the invType
     */
    public long getInvType() {
	return invType;
    }

    /**
     * @param invType
     *            the invType to set
     */
    public void setInvType(long invType) {
	this.invType = invType;
    }

    /**
     * @return the investmentsList
     */
    public ArrayList<Investment> getInvestmentsList() {
	return investmentsList;
    }

    /**
     * @param investmentsList
     *            the investmentsList to set
     */
    public void setInvestmentsList(ArrayList<Investment> investmentsList) {
	this.investmentsList = investmentsList;
    }

    /**
     * @return the lastInvestmentId
     */
    public long getLastInvestmentId() {
	return lastInvestmentId;
    }

    /**
     * @param lastInvestmentId
     *            the lastInvestmentId to set
     */
    public void setLastInvestmentId(long lastInvestmentId) {
	this.lastInvestmentId = lastInvestmentId;
    }

    /**
     * @return the markets
     */
    public List<String> getMarkets() {
	return markets;
    }

    /**
     * @param markets
     *            the markets to set
     */
    public void setMarkets(List<String> markets) {
	this.markets = markets;
    }

    /**
     * @return the skins
     */
    public List<String> getSkins() {
	return skins;
    }

    /**
     * @param skins
     *            the skins to set
     */
    public void setSkins(List<String> skins) {
	this.skins = skins;
    }

    /**
     * @return the group
     */
    public List<String> getGroups() {
	return groups;
    }

    /**
     * @param group
     *            the group to set
     */
    public void setGroups(List<String> groups) {
	this.groups = groups;
    }

    /**
     * @return the houseResult
     */
    public int getHouseResult() {
	return houseResult;
    }

    /**
     * @param houseResult
     *            the houseResult to set
     */
    public void setHouseResult(int houseResult) {
	this.houseResult = houseResult;
    }

    /**
     * @return the marketsList
     */
    public ArrayList<SelectItem> getMarketsList() {
	return marketsList;
    }

    /**
     * @param marketsList
     *            the marketsList to set
     */
    public void setMarketsList(ArrayList<SelectItem> marketsList) {
	this.marketsList = marketsList;
    }

    /**
     * Return writer offset
     * 
     * @return
     */
    public String getWriteOffset() {
	return Utils.getWriter().getUtcOffset();
    }

    /**
     * @return
     */
    public boolean getIsSettledBoolean() {
	if (isSettled == null) {
	    return false;
	}
	return isSettled.equals("1");
    }

    /************************************************* SORTING ************************************************************/

    /**
     * @return the prevSortColumn
     */
    public String getPrevSortColumn() {
	return prevSortColumn;
    }

    /**
     * @param prevSortColumn
     *            the prevSortColumn to set
     */
    public void setPrevSortColumn(String prevSortColumn) {
	this.prevSortColumn = prevSortColumn;
    }

    /**
     * @return the sortAscending
     */
    public boolean isSortAscending() {
	return sortAscending;
    }

    /**
     * @param sortAscending
     *            the sortAscending to set
     */
    public void setSortAscending(boolean sortAscending) {
	this.sortAscending = sortAscending;
    }

    /**
     * @return the sortColumn
     */
    public String getSortColumn() {
	return sortColumn;
    }

    /**
     * @param sortColumn
     *            the sortColumn to set
     */
    public void setSortColumn(String sortColumn) {
	this.sortColumn = sortColumn;
    }

    /**
     * Sort list
     * 
     * @return
     */
    public String sortList() {
	if (activateAscending) {
	    if (sortColumn.equals(prevSortColumn)) {
		sortAscending = !sortAscending;
	    } else {
		sortAscending = true;
	    }
	}

	activateAscending = true;
	prevSortColumn = sortColumn;

	if (sortColumn.equals("id")) {
	    Collections.sort(investmentsList, new IdComparator(sortAscending));
	}

	if (sortColumn.equals("marketName")) {
	    Collections.sort(investmentsList, new MarketNameComparator(sortAscending));
	}

	if (sortColumn.equals("level")) {
	    Collections.sort(investmentsList, new LevelComparator(sortAscending));
	}

	if (sortColumn.equals("wwwLevel")) {
	    Collections.sort(investmentsList, new WwwLevelComparator(sortAscending));
	}

	if (sortColumn.equals("estClosingTime")) {
	    Collections.sort(investmentsList, new TimeEstClosingComparator(sortAscending));
	}

	if (sortColumn.equals("realLevel")) {
	    Collections.sort(investmentsList, new RealLevelComparator(sortAscending));
	}

	if (sortColumn.equals("amount")) {
	    Collections.sort(investmentsList, new AmountComparator(!sortAscending));
	}

	if (sortColumn.equals("timeCreated")) {
	    Collections.sort(investmentsList, new TimeCreatedComparator(sortAscending));
	}

	if (sortColumn.equals("userName")) {
	    Collections.sort(investmentsList, new UserNameComparator(sortAscending));
	}

	if (sortColumn.equals("return")) {
	    Collections.sort(investmentsList, new ReturnComparator(sortAscending));
	}

	if (sortColumn.equals("winlose")) {
	    Collections.sort(investmentsList, new WinLoseComparator(sortAscending));
	}

	if (sortColumn.equals("oppLevelBinary0100")) {
	    Collections.sort(investmentsList, new oppLevelBinary0100Comparator(sortAscending));
	}

	if (sortColumn.equals("levelBinary0100")) {
	    Collections.sort(investmentsList, new levelBinary0100Comparator(sortAscending));
	}

	CommonUtil.setTablesToFirstPage();
	return null;
    }

    private class IdComparator implements Comparator {
	private boolean ascending;

	public IdComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Long(inv1.getId()).compareTo(new Long(inv2.getId()));
	    }
	    return -new Long(inv1.getId()).compareTo(new Long(inv2.getId()));
	}
    }

    private class MarketNameComparator implements Comparator {
	private boolean ascending;

	public MarketNameComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return inv1.getMarketName().compareTo(inv2.getMarketName());
	    }
	    return -inv1.getMarketName().compareTo(inv2.getMarketName());
	}
    }

    private class LevelComparator implements Comparator {
	private boolean ascending;

	public LevelComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
	    }
	    return -InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
	}
    }

    private class WwwLevelComparator implements Comparator {
	private boolean ascending;

	public WwwLevelComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return InvestmentFormatter.getWwwLevelTxt(inv1).compareTo(InvestmentFormatter.getWwwLevelTxt(inv2));
	    }
	    return -InvestmentFormatter.getWwwLevelTxt(inv1).compareTo(InvestmentFormatter.getWwwLevelTxt(inv2));
	}
    }

    private class TimeEstClosingComparator implements Comparator {
	private boolean ascending;

	public TimeEstClosingComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return inv1.getTimeEstClosing().compareTo(inv2.getTimeEstClosing());
	    }
	    return -inv1.getTimeEstClosing().compareTo(inv2.getTimeEstClosing());
	}
    }

    private class RealLevelComparator implements Comparator {
	private boolean ascending;

	public RealLevelComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Double(inv1.getRealLevel()).compareTo(new Double(inv2.getRealLevel()));
	    }
	    return -new Double(inv1.getRealLevel()).compareTo(new Double(inv2.getRealLevel()));
	}
    }

    private class AmountComparator implements Comparator {
	private boolean ascending;

	public AmountComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Long(inv1.getBaseAmount()).compareTo(new Long(inv2.getBaseAmount()));
	    }
	    return -new Long(inv1.getBaseAmount()).compareTo(new Long(inv2.getBaseAmount()));
	}
    }

    private class ReturnComparator implements Comparator {
	private boolean ascending;

	public ReturnComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Long(InvestmentsManagerBase.getBaseRefund(inv1)).compareTo(new Long(InvestmentsManagerBase.getBaseRefund(inv2)));
	    }
	    return -new Long(InvestmentsManagerBase.getBaseRefund(inv1)).compareTo(new Long(InvestmentsManagerBase.getBaseRefund(inv2)));
	}
    }

    private class WinLoseComparator implements Comparator {
	private boolean ascending;

	public WinLoseComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Long(InvestmentsManager.getBaseWinLose(inv1)).compareTo(new Long(InvestmentsManager.getBaseWinLose(inv2)));
	    }
	    return -new Long(InvestmentsManager.getBaseWinLose(inv1)).compareTo(new Long(InvestmentsManager.getBaseWinLose(inv2)));
	}
    }

    private class TimeCreatedComparator implements Comparator {
	private boolean ascending;

	public TimeCreatedComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return new Date(inv1.getTimeCreated().getTime()).compareTo(new Date(inv2.getTimeCreated().getTime()));
	    }
	    return -new Date(inv1.getTimeCreated().getTime()).compareTo(new Date(inv2.getTimeCreated().getTime()));
	}
    }

    private class UserNameComparator implements Comparator {
	private boolean ascending;

	public UserNameComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return inv1.getUserName().compareTo(inv2.getUserName());
	    }
	    return -inv1.getUserName().compareTo(inv2.getUserName());
	}
    }

    /**
     * @author lior Sort by opportunity current level.
     */
    private class oppLevelBinary0100Comparator implements Comparator {
	private boolean ascending;

	public oppLevelBinary0100Comparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return getOppCurrentLevelTxt(inv1).compareTo(getOppCurrentLevelTxt(inv2));
	    }
	    return -getOppCurrentLevelTxt(inv1).compareTo(getOppCurrentLevelTxt(inv2));
	}
    }

    /**
     * @author lior Sort by current level.
     */
    private class levelBinary0100Comparator implements Comparator {
	private boolean ascending;

	public levelBinary0100Comparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    Investment inv1 = (Investment) o1;
	    Investment inv2 = (Investment) o2;
	    if (ascending) {
		return InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
	    }
	    return -InvestmentFormatter.getCurrentLevelTxt(inv1).compareTo(InvestmentFormatter.getCurrentLevelTxt(inv2));
	}
    }

    public List<String> getOpportunitiesList() {
	return opportunitiesList;
    }

    public void setOpportunitiesList(List<String> opportunitiesList) {
	this.opportunitiesList = opportunitiesList;
    }

    /*************************************************
     * END SORTING
     ********************************************************/

    /**
     *
     * @return true for containing binary0100 investment type. false for not
     *         containing binary0100 investment type.
     */
    public boolean isBinary0100Selected() {
	boolean binary0100 = false;
	if (opportunitiesList.contains(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_BUY))
		|| opportunitiesList.contains(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_SELL))) {
	    binary0100 = true;
	}
	return binary0100;
    }

    /**
     * If its not binary0100 buy AND its not binary0100 sell then its other
     * investment type.
     * 
     * @return true for not containing binary0100 investment type. false for
     *         containing binary0100 investment type.
     */
    public boolean isNotBinary0100Selected() {
	boolean binary0100 = false;
	for (String oppType : opportunitiesList) {
	    if (!oppType.equals(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_BUY))
		    && !oppType.equals(String.valueOf(InvestmentsManager.INVESTMENT_TYPE_SELL))) {
		binary0100 = true;
		break;
	    }
	}
	return binary0100;
    }

    /**
     *
     * @return true if `opportunitiesList` contains several types of
     *         investments. false otherwise.
     */
    public boolean isNoSortOptions() {
	boolean noSort = false;
	if (opportunitiesList.contains("0") || (isBinary0100Selected() && isNotBinary0100Selected())) {
	    noSort = true;
	}
	return noSort;
    }

    /**
     *
     * @return
     *
     */
    public boolean isJustBinary0100Selected() {
	boolean just0100 = false;
	if (isBinary0100Selected()) {
	    just0100 = true;
	}
	if (isNotBinary0100Selected()) {
	    just0100 = false;
	}
	return just0100;
    }

	public boolean isUpInvestment(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isUpInvestment(i);
	}

	public boolean isOneTouchInv(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.getIsOneTouchInv(i);
	}

	public boolean isRolledUp(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRolledUp(i);
	}

	public boolean isRollUpBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRollUpBought(i);
	}

	public boolean isGmBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isGmBought(i);
	}

	public String getAmountWithoutFeesTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId());
	}

    public String getInsuranceAmountRUTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountRU(), i.getCurrencyId());
    }

    public String getInsuranceAmountGMTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(i.getInsuranceAmountGM(), i.getCurrencyId());
    }

    public String getRefundWithBonusTxt(Investment i) {
    	if (i == null) {
			return "";
		}
    	return InvestmentFormatter.getRefundWithBonusTxt(i);
    }

	public String getBonusName(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getBonusOddsChangeTypeId() > 0) {
			return CommonUtil.getMessage(i.getBonusDisplayName(), null);
		}
		return null;
	}

    /**
     * Setting copyop investment details by investment id
     * 
     * @param invId
     * @throws SQLException
     */
    public void setCopyopInvestmentDetailsByInvId(long invId) throws SQLException {
	CopyopInvestmentDetails copyopInvestmentDetails = InvestmentsManagerBase.getCopyopInvestmentDetails(invId);
	copyopInvestmentDetails.setOriginalInvestmentId(invId);
	copyopInvestmentDetails.setCurrencyId(Currency.CURRENCY_USD_ID);
	copyopInvestmentDetailsList.put(invId, copyopInvestmentDetails);
    }

    /**
     * @return true if no specific currency selected
     */
    public boolean isCurrencyAllSelected() {
	return true;
    }

    /**
     * @return true if specific currency selected
     */
    public boolean isStatusAllSelected() {
	return true;
    }

    public ArrayList<SelectItem> getCountriesSI() {
	return countriesSI;
    }

    public void setCountriesSI(ArrayList<SelectItem> countriesSI) {
	this.countriesSI = countriesSI;
    }

    public long getSkinBusinessCaseId() {
	return skinBusinessCaseId;
    }

    public void setSkinBusinessCaseId(long skinBusinessCaseId) {
	this.skinBusinessCaseId = skinBusinessCaseId;
    }

    public long getCountryId() {
	return countryId;
    }

    public void setCountryId(long countryId) {
	this.countryId = countryId;
    }

    /**
     * @return the apiExternalReference
     */
    public String getApiExternalReference() {
	return apiExternalReference;
    }

    /**
     * @param apiExternalReference
     *            the apiExternalReference to set
     */
    public void setApiExternalReference(String apiExternalReference) {
	this.apiExternalReference = apiExternalReference;
    }

    /**
     * @return the apiUserId
     */
    public long getApiUserId() {
	return apiUserId;
    }

    /**
     * @param apiUserId
     *            the apiUserId to set
     */
    public void setApiUserId(long apiUserId) {
	this.apiUserId = apiUserId;
    }

    /**
     * @return the insertedFrom
     */
    public List<String> getInsertedFrom() {
	return insertedFrom;
    }

    /**
     * @param insertedFrom
     *            the insertedFrom to set
     */
    public void setInsertedFrom(List<String> insertedFrom) {
	this.insertedFrom = insertedFrom;
    }

    /**
     * @return the copyopInvTypeId
     */
    public int getCopyopInvTypeId() {
	return copyopInvTypeId;
    }

    /**
     * @param copyopInvTypeId
     *            the copyopInvTypeId to set
     */
    public void setCopyopInvTypeId(int copyopInvTypeId) {
	this.copyopInvTypeId = copyopInvTypeId;
    }

    /**
     * @return the fromUserId
     */
    public long getFromUserId() {
	return fromUserId;
    }

    /**
     * @param fromUserId
     *            the fromUserId to set
     */
    public void setFromUserId(long fromUserId) {
	this.fromUserId = fromUserId;
    }

    /**
     * @return the copyopInvestmentDetailsList
     */
    public HashMap<Long, CopyopInvestmentDetails> getCopyopInvestmentDetailsList() {
	return copyopInvestmentDetailsList;
    }

    /**
     * @param copyopInvestmentDetailsList
     *            the copyopInvestmentDetailsList to set
     */
    public void setCopyopInvestmentDetailsList(HashMap<Long, CopyopInvestmentDetails> copyopInvestmentDetailsList) {
	this.copyopInvestmentDetailsList = copyopInvestmentDetailsList;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public long getScheduled() {
	return scheduled;
    }

    public void setScheduled(long scheduled) {
	this.scheduled = scheduled;
    }

    public void csvExport(File fileToSave) throws Exception {
	   	headersForCSVExport(fileToSave);

	   	FacesContext context = FacesContext.getCurrentInstance();
	   	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
	   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());
	   	DailyInvestmentsForm dailyInvForm = (DailyInvestmentsForm) context.getApplication().getExpressionFactory().createValueExpression
	   		(context.getELContext(), Constants.BIND_DAILY_INVESTMENTS_FORM, DailyInvestmentsForm.class).getValue(context.getELContext());

		Writer output = null;

		try {
		    ArrayList<Investment> listOfInvests = getInvestmentsList();
		    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave, true), "UTF-8"));
		    for (Investment inv :  listOfInvests) {
	   		//investmentsContentColumns.xhtml
	   		    // investments.id.column -> ID
	   		    if (InvestmentsManager.isBinary0100(inv)) {
	   		    	ArrayList<Long> invIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(inv.getId());
		   			output.write(String.valueOf(invIds.get(invIds.size() - 1)));
		   			output.write(DELEMITER);
	   		    } else {
	   			output.write(String.valueOf(inv.getId()));
	   			output.write(DELEMITER);
	   		    }
	   		    //investments.username -> User Name
	   		    if ( (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() )
	   			    || ( user.isSupportSelected() && user.isApiUser() )) {
	   			if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() 
	   				&& inv.getApiExternalUserId() == 0) {
	   			    output.write(inv.getUserName() + " " + inv.getUserCountryCode());
	   			    output.write(DELEMITER);
	   			} else if ( (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() 
	   				&& inv.getApiExternalUserId() > 0)
	   				|| (user.isSupportSelected() && user.isApiUser() && (inv.getApiExternalUserId() > 0)) ) {
	   			    output.write(inv.getApiExternalUserReference() + " " + inv.getUserName());
	   			    output.write(DELEMITER);
	   			}
	   		    }
	   		    // investments.marketname -> Market Name
	   		    output.write(CommonUtil.appendDQ(inv.getMarketName()));
	   		    output.write(DELEMITER);
			    if (!(InvestmentsManager.isBinary0100(inv))) {
				// investments.level -> Level
				output.write(CommonUtil.appendDQ(InvestmentFormatter.getCurrentLevelTxt(inv)));
				output.write(DELEMITER);
				// Type - e.g. PUT/Call and etc..+ copyOp values
				output.write(inv.getTypeName() + " " + (inv.getCopyOpTypeId() == 1 ? CopyOpInvTypeEnum.COPY
					: (inv.getCopyOpTypeId() == 2 ? CopyOpInvTypeEnum.FOLLOW : Constants.EMPTY_STRING)));
				output.write(DELEMITER);
			    } else if (InvestmentsManager.isBinary0100(inv)) {
				// investments.level -> Level
				output.write(CommonUtil.appendDQ(getOppCurrentLevelTxt(inv)));
				output.write(DELEMITER);
				// Type - e.g. PUT/Call and etc..+ copyOp values
				output.write(InvestmentsManager.getOpportunityTypeName(inv) + " " + (inv.getCopyOpTypeId() == 1
					? CopyOpInvTypeEnum.COPY
					: (inv.getCopyOpTypeId() == 2 ? CopyOpInvTypeEnum.FOLLOW : Constants.EMPTY_STRING)));
				output.write(DELEMITER);
			    }
	   		    // investments.buy.sell -> Buy/Sell
	   		    output.write(InvestmentsManager.isBinary0100(inv) ? inv.getTypeName() : Constants.EMPTY_STRING);
	   		    output.write(DELEMITER);
	   		    //investments.no.of.contract -> No. of contracts
	   		    if (!InvestmentsManager.isBinary0100(inv)) {
	   			output.write(Constants.EMPTY_STRING);
	   			output.write(DELEMITER);
	   		    } else if (InvestmentsManager.isBinary0100(inv) && user.isSupportSelected()) {
	   			output.write(String.valueOf(inv.getInvestmentsCount()));
	   			output.write(DELEMITER);
	   		    } else if (InvestmentsManager.isBinary0100(inv) && !user.isSupportSelected()) {
	   			output.write(String.valueOf(getBaseInvestmentsCount(inv)));
	   			output.write(DELEMITER);
	   		    }
	   		    //investments.price -> Price
	   		    if (!InvestmentsManager.isBinary0100(inv)) {
	   			output.write(Constants.EMPTY_STRING);
	   			output.write(DELEMITER);
	   		    } else if (InvestmentsManager.isBinary0100(inv)) {
	   			output.write(CommonUtil.appendDQ(InvestmentFormatter.getCurrentLevel0100BE(inv)));
	   			output.write(DELEMITER);
	   		    }
	   		    // investments.reallevel -> Reuters
	   		    if(user.isHasAdminRole() || user.isHasTraderRole()){
	   			    output.write(CommonUtil.appendDQ(CommonUtil.formatRealLevelByMarket(inv.getRealLevel(), inv.getMarketId())));
	   			    output.write(DELEMITER);
	   		    }
			    // investments.wwwlevel -> Check
			    if (user.isHasAdminRole() || user.isHasTraderRole()) {
				output.write(
					InvestmentsManager.isBinary0100(inv) ? ConstantsBase.EMPTY_STRING : CommonUtil.appendDQ(InvestmentFormatter.getWwwLevelTxt(inv)));
				output.write(DELEMITER);
			    }
			    // investments.closinglevel -> Closing level
			    if (dailyInvForm.getIsSettledBoolean()) {
				output.write(CommonUtil.appendDQ(getClosingLevel0100BE(inv)));
				output.write(DELEMITER);
			    }
			    // investments.closing.price -> Closing Price
			    if (dailyInvForm.getIsSettledBoolean()) {
				output.write(
					InvestmentsManager.isBinary0100(inv) ?  CommonUtil.appendDQ(InvestmentFormatter.getClosingPrice(inv)) : ConstantsBase.EMPTY_STRING);
				output.write(DELEMITER);
			    }
			    // investments.amount -> Amount
			    if (inv.getTotalLine() == 0 && !user.isSupportSelected() && !user.isRetentionSelected()
				    && !user.isRetentionMSelected()) {
				output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount(inv.getBaseAmount(),ConstantsBase.CURRENCY_BASE_ID)) : 
				    CommonUtil.appendDQ(CommonUtil.displayAmount(inv.getAmount(), inv.getCurrencyId())));
				output.write(DELEMITER);
			    }
			    // investments.if.above.customer -> if above (amount added to customer's balance)
			    if (!dailyInvForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected()
				    || user.isRetentionMSelected())) {
				if (inv.getTotalLine() == 0) {
				    if (InvestmentsManager.isBinary0100(inv)) {
					if (isTypeBinary0100Above(inv)) {
					    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpCustomerTxt(inv)));
					    output.write(DELEMITER);
					} else if (isTypeBinary0100Below(inv)) {
					    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseLoseDownTxt(inv)));
					    output.write(DELEMITER);
					}
				    } else if (!InvestmentsManager.isBinary0100(inv)) {
					output.write(CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(inv)), inv.getCurrencyId())));
					output.write(DELEMITER);
				    }
				}
			    }		    
			    // investments.if.above -> if above (how much house win)
			    if(!dailyInvForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected()
				    || user.isRetentionMSelected())) {
				if (inv.getTotalLine() == 0) {
				    if (InvestmentsManager.isBinary0100(inv)) {
					if (isTypeBinary0100Above(inv)) {
					    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
						output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseWinUpTxt(inv)) : 
						    CommonUtil.appendDQ((InvestmentFormatter.getBinary0100WinUpTxt(inv))));
						output.write(DELEMITER);
					    }
					} else if (isTypeBinary0100Below(inv)) {
					    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
						output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseRefundDownTxt(inv))
							: CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownTxt(inv)));
						output.write(DELEMITER);
					    }
					}
				    } else if (!InvestmentsManager.isBinary0100(inv)) {
					if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundUp(inv) - inv.getBaseAmount()) *(-1), ConstantsBase.CURRENCY_BASE_ID)) : 
						CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(inv) - inv.getAmount()) * (-1), inv.getCurrencyId())));
					    output.write(DELEMITER);
					}
				    }
				}
			    }
			    //investments.if.below.customer -> if below (amount added to customer's balance)
			    if(!dailyInvForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
				if (inv.getTotalLine() == 0) {
				    if (InvestmentsManager.isBinary0100(inv)) {
					if (isTypeBinary0100Above(inv)) {
					    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownCustomerTxt(inv)));
					    output.write(DELEMITER);
					} else if (isTypeBinary0100Below(inv)) {
					    output.write(CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpCustomerTxt(inv)));
					    output.write(DELEMITER);
					}
				    } else if (!InvestmentsManager.isBinary0100(inv)) {
					output.write(CommonUtil.appendDQ(CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(inv), inv.getCurrencyId())));
					output.write(DELEMITER);
				    }
				}
			    }
			    //investments.if.below -> if below (how much house win)
			    if(!dailyInvForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
				if(inv.getTotalLine() == 0) {
				    if(InvestmentsManager.isBinary0100(inv) && (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected())) {
					if(isTypeBinary0100Above(inv)) {
					    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseLoseDownTxt(inv))
						    : CommonUtil.appendDQ(InvestmentFormatter.getBinary0100LoseDownTxt(inv)));
					    output.write(DELEMITER);
					}else if(isTypeBinary0100Below(inv)) {
					    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(InvestmentFormatter.getBinary0100BaseWinUpTxt(inv))
						    : CommonUtil.appendDQ(InvestmentFormatter.getBinary0100WinUpTxt(inv)));
					    output.write(DELEMITER);
					}
					
				    }else if(!InvestmentsManager.isBinary0100(inv)) {
					if(!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundDown(inv) - inv.getBaseAmount()) * (-1), ConstantsBase.CURRENCY_BASE_ID))
						    : CommonUtil.appendDQ(CommonUtil.displayAmount((InvestmentsManagerBase.getRefundDown(inv) - inv.getAmount()) * (-1), inv.getCurrencyId())));
					    output.write(DELEMITER);
					}
				    }
				}
			    }
			    //investments.winlose.customer -> customer's return to balance
			    if(dailyInvForm.getIsSettledBoolean() && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
				if (inv.getTotalLine() == 0 && !InvestmentsManager.isBinary0100(inv)) {
					if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getCustomerBaseWinLoseTxt(inv))
						    : CommonUtil.appendDQ(InvestmentFormatter.getCustomerUserWinLoseTxt(inv)));
					    output.write(DELEMITER);
					}
				} else if (inv.getTotalLine() == 0 && InvestmentsManager.isBinary0100(inv)) {
				    if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getCustomerBaseWinLoseTxt0100(inv))
						: CommonUtil.appendDQ(InvestmentFormatter.getCustomerUserWinLoseTxt0100(inv)));
					output.write(DELEMITER);
				    }
				}	
			    }
			    //investments.winLose -> Win\\Lose
			    if(dailyInvForm.getIsSettledBoolean() && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
				if (inv.getTotalLine() == 0 && !InvestmentsManager.isBinary0100(inv)) {
				    if(!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
					output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getBaseWinLoseTxt(inv))
						: CommonUtil.appendDQ(InvestmentFormatter.getUserWinLoseTxt(inv)));
					output.write(DELEMITER);
				    }
				}
				else if(inv.getTotalLine() == 0 && InvestmentsManager.isBinary0100(inv)) {
				    output.write(dailyInvForm.isCurrencyAllSelected() ? CommonUtil.appendDQ(getBaseWinLoseTxt0100(inv))
					    : CommonUtil.appendDQ(InvestmentFormatter.getUserWinLoseTxt0100(inv)));
				    output.write(DELEMITER);
				}
			    }   
			    // investments.timecreated -> Time created
			    output.write((CommonUtil.getDateTimeFormatDisplay(inv.getTimeCreated(), inv.getUtcOffsetCreated())).replaceAll("&nbsp;", " "));
			    output.write(DELEMITER);
			    //investments.est.closing -> Est. Time Closing
			    if (!dailyInvForm.getIsSettledBoolean()) {
				if (inv.getTotalLine() == 0 && !(InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus())) {
				    output.write((InvestmentFormatter.getTimeEstClosingTxt(inv)).replaceAll("&nbsp;", " "));
				    output.write(DELEMITER);
				} else if (inv.getTotalLine() == 0 && (InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus())) {
				    output.write((InvestmentFormatter.getTimeSettledTxt(inv)).replaceAll("&nbsp;", " "));
				    output.write(DELEMITER);
				}
			    } else if (dailyInvForm.getIsSettledBoolean()) {
				if (inv.getTotalLine() == 0
					&& !(InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus() || InvestmentsManager.isBinary0100(inv))) {
				    output.write((InvestmentFormatter.getTimeEstClosingTxt(inv)).replaceAll("&nbsp;", " "));
				    output.write(DELEMITER);
				} else if (inv.getTotalLine() == 0
					&& (InvestmentsManagerBase.isGmBought(inv) || inv.isOptionPlus() || InvestmentsManager.isBinary0100(inv))) {
				    output.write((InvestmentFormatter.getTimeSettledTxt(inv)).replaceAll("&nbsp;", " "));
				    output.write(DELEMITER);
				}
			    }	   
			    // investments.ip -> IP address
			    output.write(inv.getIp());
			    output.write(DELEMITER);
			    // investments.insertedFrom -> Inserted from
			    output.write(inv.getWriterUserName());
			    output.write(DELEMITER);
			    // investments.balance -> balance
			    if (inv.getTotalLine() == 0) {
				output.write(inv.getApiExternalUserId() == 0 ? CommonUtil.appendDQ(getUserBalanceTxt(inv)) : Constants.EMPTY_STRING);
				output.write(DELEMITER);
			    }
			    output.write("\n");
		    }
	   		
	   	    } catch (Exception e) {
	   		logger.error("Error when exporting CSV Data file:", e);
	   	    } finally {
	   		try {
	   		    output.flush();
	   		    output.close();
	   		} catch (Exception e) {
	   		    logger.error("Daily Investment Data Form Error, Can't close file:", e);
	   		}
	   	    }
    }

    public void headersForCSVExport(File fileToSave) throws IOException {
	Writer output = null;
	
   	FacesContext context = FacesContext.getCurrentInstance();
   	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());
   	DailyInvestmentsForm dailyInvForm = (DailyInvestmentsForm) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_DAILY_INVESTMENTS_FORM, DailyInvestmentsForm.class).getValue(context.getELContext());
   	
	logger.debug("Trying to write in CSV File...");
	try {
	    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), "UTF-8"));
	    output.write("Investment_ID"); // 1 -> investments.id.column
	    output.write(DELEMITER);
	    if ((!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected())
		    || (user.isSupportSelected() && user.isApiUser())) {
		output.write("User_Name"); // 2 -> investments.username
		output.write(DELEMITER);
	    }
	    output.write("Market"); // 3 -> investments.marketname
	    output.write(DELEMITER);
	    output.write("Level"); // 4 -> investments.level
	    output.write(DELEMITER);
	    output.write("Type"); // 5 -> type
	    output.write(DELEMITER);
	    output.write("Buy/Sell"); // 6 -> investments.buy.sell
	    output.write(DELEMITER);
	    output.write("No. of Contracts"); // 7 -> investments.no.of.contract
	    output.write(DELEMITER);
	    output.write("Price"); // 8 -> investments.price
	    output.write(DELEMITER);
	    if (user.isHasAdminRole() || user.isHasTraderRole()) {
		output.write("Reuters"); // 9 -> investments.reallevel
		output.write(DELEMITER);
	    }
	    if (user.isHasSAdminRole() || user.isHasTraderRole()) {
		output.write("Check"); // 10 -> investments.wwwlevel
		output.write(DELEMITER);
	    }
	    if (dailyInvForm.getIsSettledBoolean()) { // 11 -> investments.closinglevel
		output.write("Closing Level");
		output.write(DELEMITER);
		output.write("Closing Price"); // 12 -> investments.closing.price
		output.write(DELEMITER);
	    }
	    output.write("Amount"); // 13 -> investments.amount
	    output.write(DELEMITER);
	    if (!dailyInvForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If above (amount added to customer's balance)"); // 14 investments.if.above.customer
		output.write(DELEMITER);
	    }
	    if (!dailyInvForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If above (how much house win)"); // 15 -> investments.if.above
		output.write(DELEMITER);
	    }
	    if (!dailyInvForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If below (amount added to customer's balance)"); // 16 -> investments.if.below.customer
		output.write(DELEMITER);
	    }
	    if (!dailyInvForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("If below (how much house win)"); // 17 -> investments.if.below
		output.write(DELEMITER);
	    }
	    if (dailyInvForm.getIsSettledBoolean()
		    && (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("customer's return to Balance"); // 18 -> investments.winlose.customer
		output.write(DELEMITER);
	    }
	    if (dailyInvForm.getIsSettledBoolean()
		    && !(user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected())) {
		output.write("Win/Lose"); 	// 19 -> investments.winLose
		output.write(DELEMITER);
	    }
	    output.write("Time Created"); 	// 20 -> investments.timecreated
	    output.write(DELEMITER);
	    output.write("Est. Time Closing"); 	// 21 -> investments.est.closing
	    output.write(DELEMITER);
	    output.write("IP address"); 	// 22 -> investments.ip
	    output.write(DELEMITER);
	    output.write("Inserted From"); 	// 23 -> investments.insertedFrom
	    output.write(DELEMITER);
	    output.write("balance"); 		// 24 -> investments.balance
	    output.write(DELEMITER);
	    output.write("\n");
	} catch (Exception e) {
	    logger.error("Error when exporting CSV file:", e);
	} finally {
	    try {
		output.flush();
		output.close();
	    } catch (Exception e) {
		logger.error("Daily Investment header Data Form Error, Can't close file:", e);
	    }
	}
    }
    public String exportTableToCSV() throws IOException, NumberFormatException, SQLException {

	// Set the filename
	Date dt = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	String filename = "daily_investments_report_" + fmt.format(dt) + ".csv";

	// Setup the output
	String contentType = "text/csv";

	File tCSVfile = new File(CommonUtil.getProperty(Constants.FILES_PATH) + filename);
	FacesContext fc = FacesContext.getCurrentInstance();
	ExternalContext ex = fc.getExternalContext();
	HttpServletResponse response = (HttpServletResponse) ex.getResponse();
	try {
	    try {
		csvExport(tCSVfile);
	    } catch (Exception e) {
		logger.error("Problem creating CSV file due to: " + e);
		e.printStackTrace();
		return null;
	    }

	    int contentLength = (int) tCSVfile.length();
	    
	    // just making sure nothing non current is retrieved
	    response.reset();
	    
	    response.setContentType(contentType);
	    response.setContentLength(contentLength);
	    response.setCharacterEncoding("UTF-8");
	    response.setHeader("Content-disposition", "attachment; filename=" + filename);	   
	   
	    // Write the file back out...
	    OutputStream outputStream = response.getOutputStream();
	    FileInputStream fis = new FileInputStream(tCSVfile);

	    try {
		int n = 0;
		byte[] buffer = new byte[4 * 1024];
		while ((n = fis.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, n);
		}
		outputStream.flush();
	    } finally {
		fis.close();
		outputStream.close();
	    }
	} finally {
	    tCSVfile.delete();
	}

	fc.responseComplete();

	return null;
    }

	public String getBaseRefundTxt() {
		if (baseSumInvestments.getIsCanceled() == 1) {
			if (baseSumInvestments.getBonusOddsChangeTypeId() == 1) {
				return CommonUtil.displayAmount(baseSumInvestments.getBaseAmount(), baseSumInvestments.getCurrencyId());
			} else {
				return CommonUtil.getMessage("investments.iscanceled", null);
			}
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getBaseRefund(baseSumInvestments), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getWinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(i)), i.getCurrencyId());
	}

	public String getBaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundUp(i) - i.getBaseAmount())* (-1),
										ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBinary0100WinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100WinUpTxt(i);
	}
	
	public String getDynamicsWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsWinUpTxt(i);
	}

	public String getDynamicsBaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsBaseWinUpTxt(i);
	}
	
	public String getBinary0100WinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100WinUpCustomerTxt(i);
	}
	
	public String getDynamicsWinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsWinUpCustomerTxt(i);
	}

	public String getBinary0100LoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100LoseDownTxt(i);
	}

	public String getBinary0100LoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100LoseDownCustomerTxt(i);
	}
	
	public String getDynamicsLoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsLoseDownCustomerTxt(i);
	}

	public String getBinary0100BaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseWinUpTxt(i);
	}

	public String getBinary0100BaseRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseRefundDownTxt(i);
	}

	public String getBinary0100BaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getBinary0100BaseLoseDownTxt(i);
	}
	
	public String getDynamicsBaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getDynamicsBaseLoseDownTxt(i);
	}

	public String getLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundDown(i) - i.getAmount()) * (-1), i.getCurrencyId());
	}

	public String getLoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getBaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundDown(i) - i.getBaseAmount())* (-1),
										ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getUserWinLoseTxt(i);
	}

	public String getCustomerUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCustomerUserWinLoseTxt(i);
	}

	public String getUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getUserWinLoseTxt0100(i);
	}

	public String getCustomerUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCustomerUserWinLoseTxt0100(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getBubblesHighLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByMarket(i.getBubbleHighLevel(), i.getMarketId());
	}
	
	public String getBubblesLowLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByMarket(i.getBubbleLowLevel(), i.getMarketId());
	}

	public String getRealLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatRealLevelByMarket(i.getRealLevel(), i.getMarketId());
	}

	public String getWwwLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getWwwLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getTimeSettledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeSettledTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getClosingLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100(i);
	}

	public String getCurrentLevel0100BE(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevel0100BE(i);
	}

	public String getClosingPrice(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingPrice(i);
	}

	public String getClosingLevel0100BE(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevel0100BE(i);
	}

	public String getBaseAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getBaseAmount(), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCanceledWriterName(i);
	}

	public String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getIsCanceledTxt(i);
	}

	public String getScheduledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentsManagerBase.getScheduledTxt(i.getScheduledId());
	}

	public boolean getCheckIfCancel(Investment i) {
		if (i == null) {
			return false;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		UserBase user = context.getApplication().evaluateExpressionGet(context, ConstantsBase.BIND_USER, UserBase.class);
		if (user.getRoles().contains(ConstantsBase.ROLE_SADMIN)) {
			return (i.getIsCanceled() == 0);
		}
		return false;
	}

	public String getTransactionAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getTransactionAmount(), i.getCurrencyId());
	}

	public boolean isBinary0100(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isBinary0100(i);
	}
	
	public boolean isDynamics(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isDynamics(i);
	}
	
	public boolean isDynamicsPartiallyClosed(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isDynamicsPartiallyClosed(i);
	}

	public boolean isBubbles(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isBubbles(i);
	}

	public ArrayList<Long> getInvestmentsConnectedIDByInvId(Investment i) throws Exception {
		if (i == null) {
			return new ArrayList<>();
		}
		return InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(i.getId());
	}

	public Long getLastInvestmentsConnectedIDByInvId(Investment i) throws Exception {
		if (i == null) {
			return 0L;
		}
		ArrayList<Long> invIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(i.getId());
		return invIds.get(invIds.size() - 1);
	}

	public String getOppCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByDecimalPoint(i.getOppCurrentLevel(), i.getDecimalPoint());
	}

	public String getOpportunityTypeName(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentsManager.getOpportunityTypeName(i);
	}

	public String getBaseProfitTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getBaseProfit(i), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getBaseWinLose(i), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCustomerBaseWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManager.getBaseWinLose(i) * (-1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((-1)* (i.getBaseWin() - i.getBaseLose()) * i.getInvestmentsCount()
										/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getCustomerBaseWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((i.getBaseWin() - i.getBaseLose())* i.getInvestmentsCount()
										/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1), ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getProfitStyleColor(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getStyleColor(InvestmentsManager.getBaseProfit(i));
	}

	public String getBaseInvestmentsCount(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByDecimalPoint(i.getInvestmentsCount() * i.getRate(), 2);
	}

	public boolean isTypeBinary0100Above(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Above(i);
	}

	public boolean isTypeBinary0100Below(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManager.isTypeBinary0100Below(i);
	}

	public String getUserBalanceTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getUserBalance(), i.getCurrencyId());
	}

	public long getCopyopRelevantInvBE(Investment i) {
		if (i == null) {
			return 0l;
		}
		return i.getCopyopInvId() != 0 ? i.getCopyOpInvId() : i.getId();
	}

	public String returnBubblesWinTxt(Investment i) {
		if (i == null) {
			return "";
		}
		Currency currency = CurrenciesManagerBase.getCurrency(i.getCurrencyId());
		return CommonUtil.formatCurrencyAmount((i.getAmount() * (1 + i.getOddsWin())/100), true, currency);
	}

	public String returnBubblesLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		Currency currency = CurrenciesManagerBase.getCurrency(i.getCurrencyId());
		return CommonUtil.formatCurrencyAmount((i.getAmount() * (1 - i.getOddsLose())/100), true, currency);
	}
	
	public String getDynamicsPartiallyClosedOriginalInvAmount(Investment i)throws SQLException {
		if (i == null) {
			return "";
		} else if (InvestmentsManagerBase.getDynamicsPartiallyClosedOriginalInvAmount(i) != null) {
			return getAmountTxt(InvestmentsManagerBase.getDynamicsPartiallyClosedOriginalInvAmount(i));
		} else {
			return "";
		}
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	
}