package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.AssignWriter;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignmentInfo;
import il.co.etrader.backend.bl_vos.SkinAssigns;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.EnteriesReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.DataPage;
import il.co.etrader.util.PagedListDataModel;

public class PopulationsEntriesForm implements Serializable {

	private static final long serialVersionUID = 2559808032752561408L;

	private static final Logger logger = Logger
			.getLogger(PopulationsEntriesForm.class);

	private PopulationEntry populationEntry;

	private int sort;

	private HashMap<Integer, ArrayList<String>> typeIdToValue;

	// Filters
	private long skinId;

	private long languageId;

	private long writerId;

	private long writerFilter;

	private Long userId;

	private long countryId;

	private String username;

	private boolean assignFilter;

	private int verifiedUsersFilter;

	// New Filters
	private Date from;

	private Date to;

	private Date fromLogin = null;

	private Date toLogin = null;

	private long campaignId;

	private List<String> timeZone;

	private long businessCaseId;

	private int retentionStatus;

	private long populationType;

	private String excludeCountries;

	private String includeCountries;

	private boolean hideNoAnswer;

	private String callsCount;

	private String populationTypes;

	private String populationTypesNames;

	private ArrayList<String> populationTypesList;

	private String countriesGroup;

	private boolean isNotCalledPops;

	private String[] affiliatesKeys;

	private String affiliatesNames;

	private String excludeAffiliates;

	private String includeAffiliates;

	private long lastSalesDepRepId;

	private int campaignPriority;

	private boolean isSpecialCare;

	private boolean isRetention = false;

	private boolean isConvresion = false;
	
	private List<String> userPlatform;

	// List enteries
	//private List<PopulationEntry> report;

	// Paging variables
	private DataModel<PopulationEntry> populationEntries;

	private DataPage<PopulationEntry> dataPage;

	private boolean needUpdate;

	// After action lock , unclock , assign no need to back to first page...
	private boolean backToFirstPage;

	private ArrayList<SkinAssigns> skinsAssigns;

	private boolean assignAll;

	private boolean assignOption;

	private long notAssignedNum;

	private ArrayList<SelectItem> countriesList = new ArrayList<SelectItem>();
	private ArrayList<Long> skinList = new ArrayList<Long>();
	private String[] selectedCountriesList;

	private boolean isUpdateAssigningNeeded;

	private boolean isHighPriority = false;

	private boolean isMediumPriority = false;

	private boolean isLowPriority = false;

	private HashMap<Integer, ArrayList<String>> typeIdToPriority;

	private Long contactId;

	private long lastLoginInXMinutes;

	private String timeZoneNames;

	private ArrayList<SelectItem> affiliatesNamesAndKeysSI;

	private long userStatusId;

	private long userRank;

	private boolean balanceBelowMinInvestAmount;

	private boolean madeDepositButdidntMakeInv24HAfter;

	private long populationDept = 0;

	private long supportMoreOptionsType;

	private long lastCallerWriterId;
	
	private Date lastCallFrom;
	
	private Date lastCallTo;
	
	private boolean timeType;
	
	private int issueActionTypeId = ConstantsBase.DO_NOT_SELECT;
	
	private ArrayList<SelectItem> issueActionTypeIds;
	private ArrayList<SelectItem> chatWriters;
	private long lastChatWriterId;
	
	private ArrayList<SelectItem> reachCustomersSI;
	private long reachCustomersFlag = 0;
	private long retentionTeamId;
	private String screenName;
	private long everCalled;
	
//	sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private boolean activateAscending;
    
    private String sortAmountColumn = "";
    private boolean sortAmountColumnDesc = true;
    private long countryGroupTierId;
    private boolean isReachCustomerFlag;
    
	public PopulationsEntriesForm() {
		init();
		initTypeIdOfValues();
		initkeyAndPriority();
		initPopulationsEntriesForm();
		initAffiliateForSelect();
		initConverstionOrRetention();
	}

	public void initData(){
		dataPage = null;
		populationEntries = null;
	}
	
	public String getGoConvertion() {
		this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		if (isRetention) {
			initPopulationTypes();
			search();
		}
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION);
		this.isConvresion = true;
		this.isRetention = false;
		return "";
	}

	public String getGoRetention() {
		this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
		if (isConvresion) {
			initPopulationTypes();
			User u = Utils.getUser();
			search();
		}
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION);
		this.isConvresion = false;
		this.isRetention = true;
		return "";
	}

	public String getGoDAA() {
		this.populationDept = 0;
		supportMoreOptionsType = PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS;
		return "";
	}

	public String getGoCallMe() {
		this.populationDept = 0;
		supportMoreOptionsType = PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_CALL_ME;
		return "";
	}

	public void initConverstionOrRetention() {
		Writer w = Utils.getWriter().getWriter();
		User u = Utils.getUser();
		if (w.getSales_type() == Constants.SALES_TYPE_RETENTION || (u.isRetentionMSelected() && isRetention)) {
			this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
		} else if (w.getSales_type() == Constants.SALES_TYPE_CONVERSION || (u.isRetentionMSelected() && isConvresion)) {
			this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		}
	}

	public void initTypeIdOfValues() {
		try {
			typeIdToValue = PopulationsManager.getTypesIdAndValueArray();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void initkeyAndPriority() {
		try {
			typeIdToPriority = PopulationsManager.getKeyAndPriorityArray();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initFormFilters() {
		User user = Utils.getUser();

		sort = ConstantsBase.POPULATION_ENTRY_SORT_BY_TIME_CREATED;
		//skinId = 0;
		languageId = 0;
		writerId = 0;
		writerFilter = 0;
		userId = null;
		assignFilter = false;
		username = null;
		populationType = 0;
		//timeZone = "0";
		hideNoAnswer = false;
		callsCount = "";
		countriesGroup = "0";
		verifiedUsersFilter = 0;
		isSpecialCare = false;
		isRetention = false;
		isConvresion = false;
		contactId = null;
		isHighPriority = false;
		isMediumPriority = false;
		reachCustomersFlag = 0;
		isLowPriority = false;
		lastLoginInXMinutes = ConstantsBase.ALL_FILTER_ID;
		balanceBelowMinInvestAmount = false;
		madeDepositButdidntMakeInv24HAfter = false;
		userStatusId = 0;
		userRank = 0;
		issueActionTypeId = ConstantsBase.DO_NOT_SELECT;
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		if(user.isRetentionSelected()) {
			gc.add(Calendar.DAY_OF_MONTH, -3);
		} else {
			gc.add(Calendar.DAY_OF_MONTH, -3);
		}
		from = gc.getTime();
		lastCallFrom = gc.getTime();
		lastCallTo =  gc.getTime();
		campaignId = 0;
		retentionStatus = 0;
		skinId = 0;
		skinList = new ArrayList<Long>();
		
		// assign tree set
		WriterWrapper w = Utils.getWriter();
		skinsAssigns = new ArrayList<SkinAssigns>();
		assignAll = false;

		ArrayList<Skins> skinsSorted = (ArrayList<Skins>) ApplicationData
				.getSkinsList().clone();
		Collections.sort(skinsSorted);

		if (user.isPartnerWriter()) { // have only 1 skin
			skinsSorted = new ArrayList<Skins>();
			skinsSorted.add(ApplicationData.getSkinById(w.getSkins().get(0)));
			//skinId = w.getSkins().get(0);
		}

		for (Skins s : skinsSorted) {
			SkinAssigns sa = new SkinAssigns();
			sa.setSkinId(Long.valueOf(s.getId()));
			sa.setSkinDescription(s.getDisplayName());
			ArrayList<AssignWriter> writers = new ArrayList<AssignWriter>();
			ArrayList<SelectItem> reWriters = w.getSalesWriters(sa.getSkinId());
			for (SelectItem i : reWriters) {
				AssignWriter aw = new AssignWriter();
				aw.setWriterId((Long) i.getValue());
				aw.setWriterName(i.getLabel());
				writers.add(aw);
			}
			sa.setWriters(writers);
			sa.setAssignAll(false);
			if (sa.getWriters().size() > 0) {
				skinsAssigns.add(sa);
			}
		}

		assignOption = false;
		notAssignedNum = 0;
		isUpdateAssigningNeeded = false;
		lastCallerWriterId = ConstantsBase.ALL_FILTER_ID;
		timeType=true;
		
		issueActionTypeIds = new ArrayList<SelectItem>();
		issueActionTypeIds.add(new SelectItem( -1, " "));
		for(IssueActionType iat: ApplicationData.getActionTypesList()) {
			if(iat.getChannelId()==1) {
				issueActionTypeIds.add(new SelectItem( iat.getId(), CommonUtil.getMessage(iat.getName(), null)));
			}
		}	
		setLastChatWriterId(Constants.ALL_CHAT_FILTER_ID);
		chatWriters = getChatWritersList();
		reachCustomersSI = reachCustomers();
		retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		everCalled = ConstantsBase.ALL_FILTER_ID;
		/* PopulationTypes */
		initPopulationTypes();
		countryGroupTierId = ConstantsBase.ALL_FILTER_ID;
	}
	
	public ArrayList<SelectItem> getChatWritersList() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		HashMap<Long, Writer> chatWriter;
		
		try {
			chatWriter = WritersManager.getChatWriters();
			Iterator<Long> iter = chatWriter.keySet().iterator();
			Long key;
			Writer writer;
			while(iter.hasNext()) {
				key = iter.next();
				writer = chatWriter.get(key);
				list.add(new SelectItem(new Long(writer.getId()), writer.getUserName()));
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
			list.add(0, new SelectItem(new Long(ConstantsBase.ALL_CHAT_FILTER_ID), CommonUtil.getMessage("general.all", null)));
		} catch (SQLException e) {
			logger.error("ERROR! getChatWriters - entries content ", e);
			list = null;
		} 
		return list;
	}
	
    private ArrayList<SelectItem> reachCustomers() {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(0, new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID_INT), CommonUtil.getMessage("general.all", null)));
        list.add(1, new SelectItem(1L, CommonUtil.getMessage("population.entry.reach.customers.8.days.filter", null)));
        list.add(2, new SelectItem(2L, CommonUtil.getMessage("population.entry.reach.customers.12.days.filter", null)));
        list.add(3, new SelectItem(3L, CommonUtil.getMessage("population.entry.reach.customers.never", null)));
        return list;
    }	

	public void initPopulationsEntriesForm() {
		initFormFilters();
		setPagingUpdate(true, true);
	}

	public void search() {
		sortAmountColumn = "";
		setPagingUpdate(true, true);
	}

	public void searchInitFilter() {
		sortAmountColumn = "";
		initFormFilters();
		setPagingUpdate(true, true);
	}

	public String lockEntry() throws SQLException, PopulationHandlersException {
	    return lockEntry(false);
	}
	
	public String lockEntry(boolean isVoiceSpin) throws SQLException, PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, isVoiceSpin);
		if (res) {
			setPagingUpdate(true, false);
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;
	}

	public String unLockEntry() throws SQLException,
			PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		setPagingUpdate(true, false);
		return null;
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 *
	 * @return
	 * @throws SQLException
	 */
	public String loadEntry() throws SQLException {
		return PopulationEntriesManager.loadEntry(populationEntry);
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId
	 *            the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the skinId
	 
	public Long getSkinId() {
		return skinId;
	}*/

	/**
	 * @param skinId
	 *            the skinId to set
	 
	public void setSkinId(Long skinId) {
		this.skinId = skinId;
	}*/

	/**
	 * @return the sort
	 */
	public int getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            the sort to set
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}

	public String exportEnteriesResultToMail() throws IOException,
        SQLException, ParseException {
	    return exportEnteriesResultToMail(Constants.IS_NOT_VOICE_SPIN);
	}
	
	public String exportEnteriesResultToMail(String isVoiceSpinSystem) throws IOException,
			SQLException, ParseException {
		// return exportHtmlTableToExcel("enteries_", true);
	    boolean isVoiceSpin = false;
	    if (isVoiceSpinSystem.equals(Constants.IS_VOICE_SPIN)) {
	        isVoiceSpin = true;
	    }
		String reportName = "enteries_report_";
		if (isVoiceSpin) {
		    reportName = "voice_spin_report_";
		}
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = Utils.getWriter();
		ApplicationData ap = Utils.getAppData();
		User user = Utils.getUser();
		logger.info("Process " + reportName);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(from) + "_" + fmt.format(to)
				+ ".csv";

		long currentWriterId = Utils.getWriter().getWriter().getId();

		// set defualt date for arabic partner
		if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
			String enumDate = CommonUtil.getEnum(
					Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
					Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			;
			if (from.before(startDateAr)) {
				from = startDateAr;
			}
		}

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter()
				.getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.enteries.report.invalid.mail",
							null), null);
			context.addMessage(null, fm);
			return null;
		}

		EnteriesReportSender sender = new EnteriesReportSender(this, filename,
				wr, user.isRetentionSelected(), isVoiceSpin, ap);
		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, filename
				+ " "
				+ CommonUtil
						.getMessage("enteries.report.processing.mail", null),
				null);
		context.addMessage(null, fm);

		return null;
	}

	public DataPage<PopulationEntry> getDataPage(int startRow, int pageSize) {
		if (logger.isDebugEnabled()) {
			logger.debug("getDataPage - startRow: " + startRow + " pageSize: "
					+ pageSize);
		}
		if (dataPage == null || dataPage.getStartRow() != startRow
				|| needUpdate == true) {
			if (logger.isDebugEnabled()) {
				logger.debug("loading page from db...");
			}
			try {
				User user = Utils.getUser();

				if (!assignFilter) {
					writerFilter = 0;
				}

				if (user.isRetentionSelected()) {
					writerId = Utils.getWriter().getWriter().getId();
				} else {
					writerId = writerFilter;
				}

				long userIdVal = 0;
				if (null != userId) {
					userIdVal = userId.longValue();
				}

				long contactIdVal = 0;
				if (null != contactId) {
					contactIdVal = contactId.longValue();
				}

				updateCountriesToString();
				updateAffiliateToString();

				// User name and User Id number are after trim so "only spaces"
				// case will be ignored.
				if (username != null
						&& username.equals(ConstantsBase.EMPTY_STRING)) {
					username = null;
				}
				
				//check date filter Qualification time or last call time
				if(!timeType) {
					from = lastCallFrom;
					to = lastCallTo;
				}

				dataPage = PopulationEntriesManager.getPopulationEntries(
						populationTypes, getSkins(), businessCaseId, languageId,
						writerId, campaignId, includeCountries, from, to,
						retentionStatus, assignFilter, userIdVal, username, contactIdVal,
						startRow, pageSize, supportMoreOptionsType, getArrayToString(timeZone),
						excludeCountries, hideNoAnswer, callsCount,
						countriesGroup, isNotCalledPops,
						includeAffiliates, excludeAffiliates, verifiedUsersFilter,
						lastSalesDepRepId, campaignPriority, isSpecialCare,
						fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount,
						madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept, userRank, lastCallerWriterId, timeType, issueActionTypeId, lastChatWriterId, reachCustomersFlag,
						retentionTeamId, everCalled, getSortAmountColumn(), getWritersListToString(userPlatform), countryGroupTierId);

				
				    setPagingUpdate(false, false);
				
				

			} catch (Throwable t) {
				logger.error("Can't load the page.", t);
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Cached dataPage.");
			}
		}
		return dataPage;
	}

	public DataModel getDataModel() {
		if (logger.isDebugEnabled()) {
			logger.debug("getDataModel");
		}
		if (populationEntries == null || needUpdate == true) {
			try {
				populationEntries = new LocalDataModel(Constants.RETENTION_PAGE_SIZE);
				if (backToFirstPage) {
					CommonUtil.setTablesToFirstPage();
				}
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage(
								"retention.cant.get.data.from.db", null), null);
				context.addMessage(null, fm);

				logger.error("Retention Data fetch failed.", e);
			}

		}
		return populationEntries;
	}

	private class LocalDataModel extends PagedListDataModel<PopulationEntry> {
		
		public LocalDataModel(int pageSize) {
			super(pageSize);
		}

		public DataPage<PopulationEntry> fetchPage(int startRow, int pageSize) {
			// call enclosing managed bean method to fetch the data
			return getDataPage(startRow, pageSize);
		}
	}

	/**
	 * @return the populationEntries
	 */
	public DataModel getPopulationEntries() {
		return populationEntries;
	}

	/**
	 * @param populationEntries
	 *            the populationEntries to set
	 */
	public void setPopulationEntries(DataModel populationEntries) {
		this.populationEntries = populationEntries;
	}

	public long getRowsNumber() {
		return dataPage.getDatasetSize();
	}

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry
	 *            the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	/**
	 * @return the writerFilter
	 */
	public long getWriterFilter() {
		return writerFilter;
	}

	/**
	 * @param writerFilter
	 *            the writerFilter to set
	 */
	public void setWriterFilter(long writerFilter) {
		this.writerFilter = writerFilter;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId
	 *            the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username.trim();
	}

	/**
	 * Set paging variables
	 *
	 * @param needUpdate
	 * @param backToFirstPage
	 */
	private void setPagingUpdate(boolean needUpdate, boolean backToFirstPage) {
		this.needUpdate = needUpdate;
		this.backToFirstPage = backToFirstPage;
		isUpdateAssigningNeeded = false;
		//maximum filter in Retention
		GregorianCalendar gc = new GregorianCalendar();
		User user = Utils.getUser();
		gc.setTime(this.to);
		if (user.isHasRetentionRole() || user.isHasRetentionMRole()) {
			gc.add(Calendar.YEAR, -1);
		} else {
			gc.add(Calendar.MONTH, -2);
		}
		if (this.from.before(gc.getTime())) {
			this.from = gc.getTime();
			FacesContext context = FacesContext.getCurrentInstance();
			String[] params = new String[1];
			params[0] = gc.getTime().toString();
			String msg = CommonUtil.getMessage("retention.submit.from.filter.check", params);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
			context.addMessage(null, fm);
		}
	}

	/**
	 * @return the skinsAssigns
	 */
	public ArrayList<SkinAssigns> getSkinsAssigns() {
		return skinsAssigns;
	}

	/**
	 * @param skinsAssigns
	 *            the skinsAssigns to set
	 */
	public void setSkinsAssigns(ArrayList<SkinAssigns> skinsAssigns) {
		this.skinsAssigns = skinsAssigns;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the assignAll
	 */
	public boolean isAssignAll() {
		return assignAll;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId
	 *            the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the timeZone
	 */
	public List<String> getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone
	 *            the timeZone to set
	 */
	public void setTimeZone(List<String> timeZone) {
		this.timeZone = timeZone;

		int listSize = timeZone.size();
		boolean allTimeZone = false;
		timeZoneNames = "";
		if (listSize > 0) {
			for (int i = 0; i < listSize; i++) {
				if (timeZone.get(i).equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)) {
					allTimeZone = true;
				}
				timeZoneNames += timeZone.get(i);
				// add comma if not last value
				if (listSize - 1 != i) {
					timeZoneNames += ",";
				}
			}
			// check if choose all time zone
			if (allTimeZone == true) {
				timeZoneNames = ConstantsBase.ALL_TIME_ZONE_NAME;
			}
		}
	}

//	public ArrayList<SelectItem> getSkins() throws SQLException {
//		FacesContext context = FacesContext.getCurrentInstance();
//		WriterWrapper wr = (WriterWrapper) context.getApplication()
//				.createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(
//						context);
//		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
//		logger.info(wr.getSkinBusinessCasesSI().get(2).getLabel());
//		// wr.getSkins()
//		return list;
//	}

	/**
	 * @return the BusinessCaseId
	 */
	public Long getBusinessCaseId() {
		return businessCaseId;
	}

	/**
	 * @param BusinessCaseId
	 *            the BusinessCaseId to set
	 */
	public void setBusinessCaseId(Long businessCaseId) {
		this.businessCaseId = businessCaseId;
	}

	/**
	 * @return the retentionStatus
	 */
	public int getRetentionStatus() {
		return retentionStatus;
	}

	/**
	 * @param retentionStatus
	 *            the retentionStatus to set
	 */
	public void setRetentionStatus(int retentionStatus) {
		this.retentionStatus = retentionStatus;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId
	 *            the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the populationType
	 */
	public long getPopulationType() {
		return populationType;
	}

	/**
	 * @param populationType
	 *            the populationType to set
	 */
	public void setPopulationType(long populationType) {
		this.populationType = populationType;
	}

	/**
	 * @param assignAll
	 *            the assignAll to set
	 */
	public void setAssignAll(boolean assignAll) {
		this.assignAll = assignAll;
	}

	/**
	 * @param assignAll
	 *            the assignAll to set
	 */
	public void setAssignToAllSkins(boolean assignAll) {
		this.assignAll = assignAll;
		for (SkinAssigns sa : skinsAssigns) {
			sa.setAssignAllWriters(assignAll);
		}
	}

	public void setAssignForSkin(boolean assign, long skinId) {
		if (!assign) {
			assignAll = assign;
		}
		for (SkinAssigns sa : skinsAssigns) {
			if (skinId == sa.getSkinId()) {
				sa.setAssignAllWriters(assign);
				break;
			}
		}
	}

	public void setAssignForSkinAndWriter(boolean assign, long skinId,
			long writerId) {
		if (!assign) {
			assignAll = assign;
		}
		for (SkinAssigns sa : skinsAssigns) {
			if (skinId == sa.getSkinId()) {
				if (!assign) {
					sa.setAssignAll(false);
				}
				for (AssignWriter w : sa.getWriters()) {
					if (w.getWriterId() == writerId) {
						w.setAssign(assign);
						break;
					}
				}
			}
		}
	}

	/**
	 * divide equall tasks for each assigned writer
	 */
	public String equally() {
		long entriesNum = getRowsNumber();
		long selectedAssign = getSelectedAssign();
		long popPerWriter = entriesNum / selectedAssign;
		notAssignedNum = entriesNum - (popPerWriter * selectedAssign);
		// set tasks for each writer
		ArrayList<AssignWriter> assignedWriters = new ArrayList<AssignWriter>();
		for (SkinAssigns sa : skinsAssigns) {
			for (AssignWriter w : sa.getWriters()) {
				if (w.isAssign()) {
					w.setTasks(popPerWriter);
					assignedWriters.add(w);
				}
			}
		}

		// give not assigned tasks randomally
		long notAssignedNumCP = notAssignedNum;
		if (notAssignedNumCP > 0) {
			Collections.shuffle(assignedWriters);
			for (int i = 0; i < assignedWriters.size() && notAssignedNumCP > 0; i++, notAssignedNumCP--) {
				AssignWriter w = assignedWriters.get(i);
				w.setTasks(w.getTasks() + 1);
			}
		}

		return null;
	}

	/**
	 * give tasks to assigned writers by writer tasks
	 */
	public String giveTasks() throws PopulationHandlersException, SQLException {
		//WriterWrapper wr = Utils.getWriter();

		long userIdVal = 0;
		if (null != userId) {
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)) {
			username = null;
		}

		updateCountriesToString();
		updateAffiliateToString();

		// get all population entries
		DataPage<PopulationEntry> e = PopulationEntriesManager.getPopulationEntries(
				populationTypes, getSkins(), businessCaseId, languageId, writerId,
				campaignId, includeCountries, from, to,	retentionStatus,
				assignFilter, userIdVal, username, 0, 0,
				0, supportMoreOptionsType, getArrayToString(timeZone), excludeCountries, hideNoAnswer,
				callsCount,	countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates,
				verifiedUsersFilter, lastSalesDepRepId, campaignPriority, isSpecialCare, fromLogin,
				toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount,	madeDepositButdidntMakeInv24HAfter, userStatusId,
				populationDept, userRank, lastCallerWriterId, timeType, issueActionTypeId, lastChatWriterId, reachCustomersFlag,
				retentionTeamId, everCalled, getSortAmountColumn(), getWritersListToString(userPlatform), countryGroupTierId);
		ArrayList<PopulationEntry> entries = (ArrayList<PopulationEntry>) e.getData();
				
				
				/*PopulationEntriesManager
				.getPopulationEntriesAll(populationTypes, CommonUtil.convertToCSV(skinList),
						businessCaseId, languageId, 0, campaignId,
						includeCountries, from, to, retentionStatus, false,
						userIdVal, username, getArrayToString(timeZone), null, excludeCountries,
						hideNoAnswer, callsCount, countriesGroup,
						includeAffiliates, excludeAffiliates, wr, fromLogin , toLogin, populationDept);*/

		// shuffle population entries list
		Collections.shuffle(entries);

		// give tasks
		long assignedTasks = PopulationEntriesManager.giveTasks(entries,
				skinsAssigns);

		assignOption = false; // collect the tree nodes
		setAssignToAllSkins(false);
		setPagingUpdate(true, true);
		String[] params = new String[2];
		params[0] = String.valueOf(assignedTasks);
		params[1] = String.valueOf(getRowsNumber());
		Utils.popupMessage("retention.submit.assign.done", params);
		return null;
	}

	/**
	 * Return number of selected assign writers
	 *
	 * @return
	 */
	public long getSelectedAssign() {
		long numAssign = 0;
		if (assignAll) {
			for (SkinAssigns sa : skinsAssigns) {
				numAssign += sa.getWriters().size();
			}
		} else {
			for (SkinAssigns sa : skinsAssigns) {
				if (sa.isAssignAll()) {
					numAssign += sa.getWriters().size();
				} else {
					for (AssignWriter w : sa.getWriters()) {
						if (w.isAssign()) {
							numAssign++;
						}
					}
				}
			}
		}
		return numAssign;
	}

	public boolean isEquallyAvailable() {
		for (SkinAssigns sa : skinsAssigns) {
			if (sa.isHasAssignWriters()) {
				return true;
			}
		}
		return false;
	}

	public String reload() {
		return null;
	}

	/**
	 * @return the assignOption
	 */
	public boolean isAssignOption() {
		return assignOption;
	}

	/**
	 * @param assignOption
	 *            the assignOption to set
	 */
	public void setAssignOption(boolean assignOption) {
		this.assignOption = assignOption;
	}

	public String assignPressed() {
		assignOption = !assignOption;
		return null;
	}

	/**
	 * @return the notAssignedNum
	 */
	public long getNotAssignedNum() {
		return notAssignedNum;
	}

	/**
	 * @param notAssignedNum
	 *            the notAssignedNum to set
	 */
	public void setNotAssignedNum(long notAssignedNum) {
		this.notAssignedNum = notAssignedNum;
	}

	public long getSalesPageSize() {
		return Constants.RETENTION_PAGE_SIZE;
	}

	/**
	 * @return the assignFilter
	 */
	public boolean isAssignFilter() {
		return assignFilter;
	}

	/**
	 * @param assignFilter
	 *            the assignFilter to set
	 */
	public void setAssignFilter(boolean assignFilter) {
		this.assignFilter = assignFilter;
	}

	public void updatePopulationType(ValueChangeEvent event) {
		this.populationType = (Long) event.getNewValue();
	}

	/**
	 * @return the excludeCountryId
	 */
	public String getExcludeCountries() {
		return excludeCountries;
	}

	/**
	 * @param excludeCountryId
	 *            the excludeCountryId to set
	 */
	public void setExcludeCountries(String excludeCountries) {
		this.excludeCountries = excludeCountries;
	}

	/**
	 * @return the countriesList
	 */
	public ArrayList<SelectItem> getCountriesList() {
		return countriesList;
	}

	/**
	 * @param countriesList
	 *            the countriesList to set
	 */
	public void setCountriesList(ArrayList<SelectItem> countriesList) {
		this.countriesList = countriesList;
	}

	public void updateCountries(List<String> GMT) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		countriesList.clear();
		WriterWrapper w = Utils.getWriter();
		if (!GMT.contains("0")) {
			for (Country c : ApplicationData.getCountries().values()) {
				if (GMT.contains(c.getGmtOffset())) {
					list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
							CommonUtil.getMessage(c.getDisplayName(), null)
									+ ", "
									+ Utils.getUtcDifference(w.getUtcOffset(),
											c.getGmtOffset())));
				}
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		} else {
			for (Country c : ApplicationData.getCountries().values()) {
				list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
						CommonUtil.getMessage(c.getDisplayName(), null)
								+ ", "
								+ Utils.getUtcDifference(w.getUtcOffset(), c
										.getGmtOffset())));
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		}
		countriesList.addAll(list);
	}

	public void updateCountries(ValueChangeEvent event) {
		updateCountries((List<String>) event.getNewValue());
	}

	public void updateAssignedWriter(ValueChangeEvent event) {
		isUpdateAssigningNeeded = true;
	}

	public String updateAssignedLeads() throws PopulationHandlersException,
			SQLException {
		List<PopulationEntry> popEntries = dataPage.getData();
		int countUpdatedLeads = 0;
		int successfullyUpdatedLeads = 0;
		int notAssign = 0;
		//String userIds = "";
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		long writerId = wr.getWriter().getId();
		HashMap<Long, Writer> WritersMap = wr.getAllWritersMap();
		List<String> writerNameList = new ArrayList<String>();

		for (PopulationEntry entry : popEntries) {
			long lastAssignWriterId = entry.getAssignWriterId();
			long tempAssignWriterId = entry.getTempAssignWriterId();
			if (lastAssignWriterId != tempAssignWriterId) {
				++countUpdatedLeads;

				if (tempAssignWriterId == 0) {
					// cancel assing
					if (PopulationEntriesManager.cancelAssign(entry, false, false)) {
						successfullyUpdatedLeads++;
					}
				} else {
					// assign or reassign
					if (isRetention) {
						PopulationUsersAssignmentInfo puai = PopulationEntriesManager.isAssignmentOk(entry.getTempAssignWriterId(), entry.getSkinId(), entry.getUserRankId(), entry.getUserStatusId());
						if (puai.isAssignmentOk()) {
							entry.updateAssignedWriter();
							if (PopulationEntriesManager.assign(entry, false, lastAssignWriterId, false, false, writerId)) {
								successfullyUpdatedLeads++;
							}
						} else {
							notAssign++;
			    			if (puai.isReachedTotalLimit()) {
			    				Writer w = WritersMap.get(tempAssignWriterId);
			    				if (!writerNameList.contains(w.getUserName())) {
			    					writerNameList.add(w.getUserName());
			    				}
			    			}
						}
					} else {
						entry.updateAssignedWriter();
						if (PopulationEntriesManager.assign(entry, false, lastAssignWriterId, false, false, writerId)) {
							successfullyUpdatedLeads++;
						}
					}

				}
			}
		}
		isUpdateAssigningNeeded = false;

		if (successfullyUpdatedLeads > 0) {
			setPagingUpdate(true, false);

			String msg = countUpdatedLeads
					+ " "
					+ CommonUtil.getMessage(
							"retention.entry.multi.assign.success", null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg,
					null);
			context.addMessage(null, fm);
		}

		if (notAssign > 0) {
			setPagingUpdate(true, false);

			String msg = notAssign + " "
					+ CommonUtil.getMessage(
							"retention.entry.multi.assign.not.success", null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg,
					null);
			context.addMessage(null, fm);
			if (writerNameList.size() > 0) {
				String[] params = new String[1];
				params[0] = CommonUtil.getAppendStringByArray(writerNameList);
				msg = CommonUtil.getMessage("retention.entry.assign.not.success.limit", params);
    			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
    			context.addMessage(null, fm);
			}
		}

		if (isConvresion) {
			return Constants.NAV_RETENTION_MANAGER_ENTRIES_TAB_CONVERTION;
		} else {
			return Constants.NAV_RETENTION_MANAGER_ENTRIES_TAB_RETENTION;
		}
	}

	/**
	 * @return the hideNoAnswer
	 */
	public boolean isHideNoAnswer() {
		return hideNoAnswer;
	}

	/**
	 * @param hideNoAnswer
	 *            the hideNoAnswer to set
	 */
	public void setHideNoAnswer(boolean hideNoAnswer) {
		this.hideNoAnswer = hideNoAnswer;
	}

	/**
	 * @return the SelectedCountriesList
	 */
	public String[] getSelectedCountriesList() {
		return selectedCountriesList;
	}

	/**
	 * @param SelectedCountriesList
	 *            the SelectedCountriesList to set
	 */
	public void setSelectedCountriesList(String[] selectedCountriesList) {
		this.selectedCountriesList = selectedCountriesList;
	}

	/**
	 * Format countries for SQL
	 *
	 * @return list of countries with , between them
	 */

	public void updateCountriesToString() {
		String tmp = ConstantsBase.EMPTY_STRING;
		excludeCountries = "0";
		if (includeCountries.equals("")) { // if selection all exclude
											// countries
			includeCountries = ConstantsBase.COUNTRIES_NONE;
		}

		if (null != selectedCountriesList && selectedCountriesList.length == 0) {// if
																					// selection
																					// all
																					// include
																					// countries
			includeCountries = "0";
		}

		if (!includeCountries.equals(ConstantsBase.COUNTRIES_NONE)) {
			if (null != selectedCountriesList
					&& selectedCountriesList.length > 0
					&& selectedCountriesList.length < 120) {
				includeCountries = "0";
				for (int i = 0; i < selectedCountriesList.length; i++) {
					tmp += selectedCountriesList[i] + ",";
				}
				excludeCountries = tmp.substring(0, tmp.length() - 1);
			}
		}
	}

	/**
	 * @return the includeCountries
	 */
	public String getIncludeCountries() {
		return includeCountries;
	}

	/**
	 * @param includeCountries
	 *            the includeCountries to set
	 */
	public void setIncludeCountries(String includeCountries) {
		this.includeCountries = includeCountries;
	}

	/**
	 * @return the callsCount
	 */
	public String getCallsCount() {
		return callsCount;
	}

	/**
	 * @param callsCount
	 *            the callsCount to set
	 */
	public void setCallsCount(String callsCount) {
		this.callsCount = callsCount;
	}

	/**
	 * @return the populationTypes
	 */
	public String getPopulationTypes() {
		return populationTypes;
	}

	/**
	 * @param populationTypes
	 *            the populationTypes to set
	 */
	public void setPopulationTypes(String populationTypes) {
		this.populationTypes = populationTypes;
	}

	/**
	 * @return the populationTypesList
	 */
	public ArrayList<String> getPopulationTypesList() {
		return populationTypesList;
	}

	/**
	 * @return the countriesGroup
	 */
	public String getCountriesGroup() {
		return countriesGroup;
	}

	/**
	 * @param countriesGroup
	 *            the countriesGroup to set
	 */
	public void setCountriesGroup(String countriesGroup) {
		this.countriesGroup = countriesGroup;
	}

	/**
	 * @param populationTypesList
	 *            the populationTypesList to set
	 */
	public void setPopulationTypesList(ArrayList<String> populationTypesList) {
		this.populationTypesList = populationTypesList;

		int listSize = populationTypesList.size();
		populationTypes = "";
		populationTypesNames = "";

		if (listSize > 0) {
			String popType = null;
			String popTypeKey = null;
			String popTypeName = null;
			HashMap<Long, PopulationType> populationsTypes = ApplicationData
					.getPopulationsTypes();

			for (int i = 0; i < listSize; i++) {
				popType = populationTypesList.get(i);
				populationTypes += popType;
				popTypeKey = populationsTypes.get(Long.parseLong(popType))
						.getName();
				popTypeName = CommonUtil.getMessage(popTypeKey, null);
				populationTypesNames += popTypeName;

				// add comma if not last value
				if (listSize - 1 != i) {
					populationTypes += ",";
					populationTypesNames += ", ";
				}
			}
		}
	}

	/**
	 * @return the populationTypesNames
	 */
	public String getPopulationTypesNames() {
		return populationTypesNames;
	}

	/**
	 * @return the isNotCalledPops
	 */
	public boolean isNotCalledPops() {
		return isNotCalledPops;
	}

	/**
	 * @param isNotCalledPops
	 *            the isNotCalledPops to set
	 */
	public void setNotCalledPops(boolean isNotCalledPops) {
		this.isNotCalledPops = isNotCalledPops;
	}

	/**
	 * @return the affiliatesKeys
	 */
	public String[] getAffiliatesKeys() {
		return affiliatesKeys;
	}

	/**
	 * @param affiliatesKeys
	 *            the affiliatesKeys to set
	 */
	public void setAffiliatesKeys(String[] affiliatesKeys) {
		this.affiliatesKeys = affiliatesKeys;
	}

	/**
	 * @return the affiliatesNames
	 */
	public String getAffiliatesNames() {
		return affiliatesNames;
	}

	/**
	 * @param affiliatesNames
	 *            the affiliatesNames to set
	 */
	public void setAffiliatesNames(String affiliatesNames) {
		this.affiliatesNames = affiliatesNames;
	}

	/**
	 * Format Affiliate for SQL
	 *
	 * @return list of Affiliate with , between them
	 */

	public void updateAffiliateToString() {
		String tmp = ConstantsBase.EMPTY_STRING;
		excludeAffiliates = "0";
		if (includeAffiliates.equals("")) { // if selection all exclude affiliates
			includeAffiliates = ConstantsBase.AFFILIATE_NONE;
		}

		if (null != affiliatesKeys && affiliatesKeys.length == 0) {// if selection all include affiliates
			includeAffiliates = "0";
		}

		if (!includeAffiliates.equals(ConstantsBase.AFFILIATE_NONE)) {
			if (null != affiliatesKeys
					&& affiliatesKeys.length > 0
					&& affiliatesKeys.length < 120) {
				includeAffiliates = "0";
				for (int i = 0; i < affiliatesKeys.length; i++) {
					tmp += affiliatesKeys[i] + ",";
				}
				excludeAffiliates = tmp.substring(0, tmp.length() - 1);
			}
		}
	}

	/**
	 * Return true if affiliate list is empty
	 *
	 * @return
	 */
	public boolean isAffiliatesEmpty() {
		if (null == affiliatesKeys || affiliatesKeys.equals("0")
				|| affiliatesKeys.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Update chosen affiliates names for filter
	 *
	 * @param event
	 */
	public void updateAffiliatesName(ValueChangeEvent event) {
		List<String> affiliates = (List<String>) event.getNewValue();
		HashMap<Long, MarketingAffilate> hm = Utils.getWriter()
				.getAffiliatesHM();
		affiliatesNames = ConstantsBase.EMPTY_STRING;
		for (String key : affiliates) {
			affiliatesNames += (hm.get(Long.valueOf(key)).getName()) + ", ";
		}
		if (!affiliatesNames.equals(Constants.EMPTY_STRING)) {
			affiliatesNames = affiliatesNames.substring(0, affiliatesNames
					.length() - 2);
		}
	}

	/**
	 * Return true affiliates list is empty
	 *
	 * @return
	 */
	public boolean isAffiliatesNamesEmpty() {
		if (affiliatesNames.equals(ConstantsBase.EMPTY_STRING)) {
			return true;
		}
		return false;
	}

	/**
	 * Return true countries list is empty
	 *
	 * @return
	 */
	public boolean isCountriesEmpty() {
		if (null == selectedCountriesList || selectedCountriesList.equals("0")
				|| selectedCountriesList.length == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Return true population list is empty
	 *
	 * @return
	 */
	public boolean isPopulationTypesEmpty() {
		if (populationTypesNames.equals(ConstantsBase.EMPTY_STRING)) {
			return true;
		}
		return false;
	}

	/**
	 * Return true population list is empty
	 *
	 * @return
	 */
	public boolean isTimeZonesEmpty() {
		if (timeZoneNames.equals(ConstantsBase.EMPTY_STRING)) {
			return true;
		}
		return false;
	}
	/**
	 * @return the verifiedUsersFilter
	 */
	public int getVerifiedUsersFilter() {
		return verifiedUsersFilter;
	}

	/**
	 * @param verifiedUsersFilter
	 *            the verifiedUsersFilter to set
	 */
	public void setVerifiedUsersFilter(int verifiedUsersFilter) {
		this.verifiedUsersFilter = verifiedUsersFilter;
	}

	/**
	 * @return the lastSalesDepRepId
	 */
	public long getLastSalesDepRepId() {
		return lastSalesDepRepId;
	}

	/**
	 * @param lastSalesDepRepId
	 *            the lastSalesDepRepId to set
	 */
	public void setLastSalesDepRepId(long lastSalesDepRepId) {
		this.lastSalesDepRepId = lastSalesDepRepId;
	}

	/**
	 * @return the isUpdateAssigningNeeded
	 */
	public boolean isUpdateAssigningNeeded() {
		return isUpdateAssigningNeeded;
	}

	/**
	 * @param isUpdateAssigningNeeded
	 *            the isUpdateAssigningNeeded to set
	 */
	public void setUpdateAssigningNeeded(boolean isUpdateAssigningNeeded) {
		this.isUpdateAssigningNeeded = isUpdateAssigningNeeded;
	}

	/**
	 * @return the campaignPriority
	 */
	public int getCampaignPriority() {
		return campaignPriority;
	}

	/**
	 * @param campaignPriority
	 *            the campaignPriority to set
	 */
	public void setCampaignPriority(int campaignPriority) {
		this.campaignPriority = campaignPriority;
	}

	/**
	 * @return the isSpecialCare
	 */
	public boolean isSpecialCare() {
		return isSpecialCare;
	}

	/**
	 * @param isSpecialCare
	 *            the isSpecialCare to set
	 */
	public void setSpecialCare(boolean isSpecialCare) {
		this.isSpecialCare = isSpecialCare;
	}

	public HashMap<Integer, ArrayList<String>> getTypeIdToValue() {
		return typeIdToValue;
	}

	public void setTypeIdToValue(HashMap<Integer, ArrayList<String>> typeIdToValue) {
		this.typeIdToValue = typeIdToValue;
	}

	public boolean isConvresion() {
		return isConvresion;
	}

	public void setConvresion(boolean isConvresion) {
		this.isConvresion = isConvresion;
	}

	public boolean isRetention() {
		return isRetention;
	}

	public void setRetention(boolean isRetention) {
		this.isRetention = isRetention;
	}


	public ArrayList<String> getArrayOfRetention(){
		return typeIdToValue.get(Constants.POPULATION_TYPE_ID_RETENTION);
	}

	public ArrayList<String> getArrayOfConversion(){
		return typeIdToValue.get(Constants.POPULATION_TYPE_ID_CONVERSION);
	}

	/**
	 * @return the fromLogin
	 */
	public Date getFromLogin() {
		return fromLogin;
	}

	/**
	 * @param fromLogin the fromLogin to set
	 */
	public void setFromLogin(Date fromLogin) {
		this.fromLogin = fromLogin;
	}

	/**
	 * @return the toLogin
	 */
	public Date getToLogin() {
		return toLogin;
	}

	/**
	 * @param toLogin the toLogin to set
	 */
	public void setToLogin(Date toLogin) {
		this.toLogin = toLogin;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the excludeAffiliates
	 */
	public String getExcludeAffiliates() {
		return excludeAffiliates;
	}

	/**
	 * @param excludeAffiliates the excludeAffiliates to set
	 */
	public void setExcludeAffiliates(String excludeAffiliates) {
		this.excludeAffiliates = excludeAffiliates;
	}

	/**
	 * @return the includeAffiliates
	 */
	public String getIncludeAffiliates() {
		return includeAffiliates;
	}

	/**
	 * @param includeAffiliates the includeAffiliates to set
	 */
	public void setIncludeAffiliates(String includeAffiliates) {
		this.includeAffiliates = includeAffiliates;
	}

	/**
	 * @return the isHighPriority
	 */
	public boolean isHighPriority() {
		return isHighPriority;
	}

	/**
	 * @param isHighPriority the isHighPriority to set
	 */
	public void setHighPriority(boolean isHighPriority) {
		this.isHighPriority = isHighPriority;
	}

	/**
	 * @return the isLowPriority
	 */
	public boolean isLowPriority() {
		return isLowPriority;
	}

	/**
	 * @param isLowPriority the isLowPriority to set
	 */
	public void setLowPriority(boolean isLowPriority) {
		this.isLowPriority = isLowPriority;
	}

	/**
	 * @return the isMediumPriority
	 */
	public boolean isMediumPriority() {
		return isMediumPriority;
	}

	/**
	 * @param isMediumPriority the isMediumPriority to set
	 */
	public void setMediumPriority(boolean isMediumPriority) {
		this.isMediumPriority = isMediumPriority;
	}

	public ArrayList<String> getArrayOfHighPriority(){
		return typeIdToPriority.get(Constants.AFFILIATE_PRIORITY_HIGH);
	}

	public ArrayList<String> getArrayOfMediumPriority(){
		return typeIdToPriority.get(Constants.AFFILIATE_PRIORITY_MEDIUM);
	}

	public ArrayList<String> getArrayOfLowPriority(){
		return typeIdToPriority.get(Constants.AFFILIATE_PRIORITY_LOW);
	}

	/**
	 * @return the typeIdToPriority
	 */
	public HashMap<Integer, ArrayList<String>> getTypeIdToPriority() {
		return typeIdToPriority;
	}

	/**
	 * @param typeIdToPriority the typeIdToPriority to set
	 */
	public void setTypeIdToPriority(
			HashMap<Integer, ArrayList<String>> typeIdToPriority) {
		this.typeIdToPriority = typeIdToPriority;
	}

	/**
	 * Get Affiliates Array list SI
	 * @return
	 */
	public ArrayList<SelectItem> getAffiliateNameAndKeySI(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String updateAffiliate = (String)session.getAttribute(Constants.UPDATE_AFFILIATE_LIST);
		if (updateAffiliate == Constants.ADDED_AFFILIATE) {
			try {
				affiliatesNamesAndKeysSI = MarketingManager.getAffiliatesNamesAndKeys();
				session.setAttribute(Constants.UPDATE_AFFILIATE_LIST, Constants.NOT_ADDED_AFFILIATE);
			} catch (SQLException e) {
				logger.debug("MarketingManager.getAffiliatesNamesAndKeys failed");
			}
		}
		return WriterWrapper.splitTranslateSI(affiliatesNamesAndKeysSI);
	}

	public void initAffiliateForSelect() {
		try {
			affiliatesNamesAndKeysSI = MarketingManager.getAffiliatesNamesAndKeys();
		} catch (SQLException e) {
			logger.debug("initAffiliateForSelect MarketingManager.getAffiliatesNamesAndKeys failed");
		}
	}

	/**
     * return 0 if all selected else return string of GMT
     * @param temp
     * @return
     */
    public String getArrayToString(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
        	if(temp.get(i).equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)){
        		return ConstantsBase.ALL_TIME_ZONE_POPULATION;
        	}
        	list += "'" + temp.get(i) + "'" + ",";
        }
        return list.substring(0,list.length()-1);
    }

	/**
	 * @return the balanceBelowMinInvestAmount
	 */
	public boolean isBalanceBelowMinInvestAmount() {
		return balanceBelowMinInvestAmount;
	}

	/**
	 * @param balanceBelowMinInvestAmount the balanceBelowMinInvestAmount to set
	 */
	public void setBalanceBelowMinInvestAmount(boolean balanceBelowMinInvestAmount) {
		this.balanceBelowMinInvestAmount = balanceBelowMinInvestAmount;
	}

	/**
	 * @return the madeDepositButdidntMakeInv24HAfter
	 */
	public boolean isMadeDepositButdidntMakeInv24HAfter() {
		return madeDepositButdidntMakeInv24HAfter;
	}

	/**
	 * @param madeDepositButdidntMakeInv24HAfter the madeDepositButdidntMakeInv24HAfter to set
	 */
	public void setMadeDepositButdidntMakeInv24HAfter(
			boolean madeDepositButdidntMakeInv24HAfter) {
		this.madeDepositButdidntMakeInv24HAfter = madeDepositButdidntMakeInv24HAfter;
	}
	
	/**
	 * @return the timeZoneNames
	 */
	public String getTimeZoneNames() {
		return timeZoneNames;
	}

	/**
	 * @param timeZoneNames the timeZoneNames to set
	 */
	public void setTimeZoneNames(String timeZoneNames) {
		this.timeZoneNames = timeZoneNames;
	}

	/**
	 * @return the userStatusId
	 */
	public long getUserStatusId() {
		return userStatusId;
	}

	/**
	 * @param userStatusId the userStatusId to set
	 */
	public void setUserStatusId(long userStatusId) {
		this.userStatusId = userStatusId;
	}

	/**
	 * @return the userRank
	 */
	public long getUserRank() {
		return userRank;
	}

	/**
	 * @param userRank the userRank to set
	 */
	public void setUserRank(long userRank) {
		this.userRank = userRank;
	}

	public long getLastCallerWriterId() {
		return lastCallerWriterId;
	}

	public void setLastCallerWriterId(long lastCallerWriterId) {
		this.lastCallerWriterId = lastCallerWriterId;
	}


	public Date getLastCallFrom() {
		return lastCallFrom;
	}

	public void setLastCallFrom(Date lastCallFrom) {
		this.lastCallFrom = lastCallFrom;
	}

	public Date getLastCallTo() {
		return lastCallTo;
	}

	public void setLastCallTo(Date lastCallTo) {
		this.lastCallTo = lastCallTo;
	}

	public boolean getTimeType() {
		return timeType;
	}

	public void setTimeType(boolean timeType) {
		this.timeType = timeType;
	}

	public int getIssueActionTypeId() {
		return issueActionTypeId;
	}

	public void setIssueActionTypeId(int issueActionTypeId) {
		this.issueActionTypeId = issueActionTypeId;
	}

	public ArrayList<SelectItem> getIssueActionTypeIds() {
		return issueActionTypeIds;
	}

	public void setIssueActionTypeIds(ArrayList<SelectItem> issueActionTypeIds) {
		this.issueActionTypeIds = issueActionTypeIds;
	}

	public ArrayList<Long> getSkinList() {
		return skinList;
	}
	
	public int getSkinListSize() {
		return skinList.size();
	}
	

	public String getSkins(){ 
		return CommonUtil.convertToCSV(skinList);
	}
	
	public void setSkinList(ArrayList<Long> skinList) {
		this.skinList = skinList;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	public void updateSkinList(ValueChangeEvent event) {
		skinList = new ArrayList<Long>();
		skinList.add((Long)event.getNewValue());
	}
	
	/**
	 * @return the chatWriters
	 */
	public ArrayList<SelectItem> getChatWriters() {
		return chatWriters;
	}

	/**
	 * @param chatWriters the chatWriters to set
	 */
	public void setChatWriters(ArrayList<SelectItem> chatWriters) {
		this.chatWriters = chatWriters;
	}

	/**
	 * @return the lastChatWriterId
	 */
	public long getLastChatWriterId() {
		return lastChatWriterId;
	}

	/**
	 * @param lastChatWriterId the lastChatWriterId to set
	 */
	public void setLastChatWriterId(long lastChatWriterId) {
		this.lastChatWriterId = lastChatWriterId;
	}

	/**
	 * get voice spin call.
	 * Assign, lock and load entryId to writer that found in session.
	 * Insert issue for each call.
	 * Redirect to user strip in retention.
	 * @return String
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public String getVoiceSpinCall() throws SQLException, PopulationHandlersException {
	    FacesContext context = FacesContext.getCurrentInstance();
	    WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        String entryId = request.getParameter("entryId");
        String writerName = request.getParameter("writerName");
        logger.info("Get voice spin call with parameters: entryId = " + entryId + ", writerName = " + writerName + ".");
        if (!Utils.isParameterEmptyOrNull(entryId)) {
            long populationEntryId = Long.valueOf(entryId);
            PopulationEntryBase populationEntryBase = PopulationsManagerBase.getPopulationEntryByEntryId(populationEntryId);
            populationEntry = new PopulationEntry();
            populationEntry.convertBaseToPopulationEntry(populationEntryBase);
            // assign.
            long lastAssignWriterId = populationEntry.getAssignWriterId();
            long writerId = w.getWriter().getId();
            logger.info("writerId in session: " + writerId + ".");
            if (writerId != lastAssignWriterId) {
                if (lastAssignWriterId != 0) {
                    PopulationEntriesManager.cancelAssign(populationEntry, false, true);
                }
                populationEntry.setAssignWriterId(writerId);
                PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, true, false, writerId);
            }
            // lock and load entry.
            String nav = lockEntry(true);
            User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
            // insert issue.
            Issue issue = new Issue();
            issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
            issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
            issue.setUserId(populationEntry.getUserId());
            issue.setContactId(populationEntry.getContactId());
            issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
            issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
            issue.setPopulationEntryId(populationEntryId);
            IssueAction issueAction = new IssueAction();
            issueAction.setCallDirectionId(IssueAction.ISSUE_NO_DIRECTION);
            issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_AUTOMATIC_DIALER));
            issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_AUTOMATIC_DIALER));
            issueAction.setWriterId(writerId);
            boolean res = false;
            int screenId = user.getScreenId();
            long issueId = IssuesManagerBase.searchIssueIdByPopEntryID(populationEntryId);
            if (0 == issueId) {
                res = IssuesManager.insertIssue(issue, issueAction, user, screenId);
            } else {
                issue.setId(issueId);
                res = IssuesManager.updateIssue(issue, issueAction, false, user, screenId);
            }
            // send redirect to retention.
            try {
                if (null != nav && res) {
                    logger.info("redirect to user strip in retention with populationEntryId = " + entryId + ".");
                    response.sendRedirect(CommonUtil.getProperty(Constants.HOST_URL_HTTPS) + "/jsp/retention/retention.jsf");
                } else {
                    logger.info("redirect to error page in retention with populationEntryId = " + entryId + ".");
                    response.sendRedirect(CommonUtil.getProperty(Constants.HOST_URL_HTTPS) + "/jsp/retention/retention.jsf?retentionError=true&entryId=" + entryId);
                }
            } catch (IOException e) {
                logger.error("Error! Problem to redirect. entryId = " + entryId + ".", e);
            }
        }
        return null;
	}
	
    /**
     * Add "No answer" issue action to the population issue
     * @throws SQLException 
     */
	public void insertNoAnswerIssue() throws SQLException{
		User user = null;
		Issue issue = null;
		Writer w = Utils.getWriter().getWriter();
		
		try {
			user = Utils.getUser(populationEntry.getUserId());
		} catch (SQLException e1) {
			logger.error("Can't get user.", e1);
		}
		
        IssueAction issueAction = new IssueAction();
        issueAction.setSignificant(false);
        issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
        issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_CALL));
        issueAction.setReachedStatusId(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_NOT_REACHED));
        issueAction.setComments("Added by quick no answer action button");
        issueAction.setWriterId(w.getId());
        issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_NO_ANSWER));
        int screenId = user.getScreenId();
		       
        long issueId = IssuesManagerBase.searchIssueIdByPopEntryID(populationEntry.getCurrEntryId());
		issue = IssuesManager.getIssueById(issueId);
		if (issue != null) {
	        try {
	        	IssuesManagerBase.insertAction(issue, issueAction, user.getUtcOffset(), w.getId(), user, screenId);
			} catch (SQLException e) {
				logger.error("Can't insert issue action for No answer.", e);
				e.printStackTrace();
			}
		} else {
			Issue newIssue = new Issue();
			newIssue.setTimeCreated(new Date());
			newIssue.setPopulationEntryId(populationEntry.getCurrEntryId());
			newIssue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
			newIssue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_POPULATION));
			newIssue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
			try {
				IssuesManager.insertIssue(newIssue, issueAction, user, screenId);
			 } catch (SQLException e) {
				logger.error("Can't insert issue and issue action for No answer.", e);
			 }
		}
        
	}
    
    public String getShowTab() throws SQLException, PopulationHandlersException {
        FacesContext context = FacesContext.getCurrentInstance();
        WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String voiceSpinError = request.getParameter("retentionError");
        if (!CommonUtil.isParameterEmptyOrNull(voiceSpinError)) {
            return Constants.RETENTION_ERROR_TAB;
        }
        if (w.isWriterHasLockedEntry()) {
            return Constants.RETENTION_ENTRY_STRIP_TAB;
        }
        return Constants.RETENTION_ASSIGNMENTS_TAB;
    }

    /**
     * @return the populationDept
     */
    public long getPopulationDept() {
        return populationDept;
    }

    /**
     * @param populationDept the populationDept to set
     */
    public void setPopulationDept(long populationDept) {
        this.populationDept = populationDept;
    }

    /**
     * @return the reachCustomersSI
     */
    public ArrayList<SelectItem> getReachCustomersSI() {
        return reachCustomersSI;
    }

    /**
     * @param reachCustomersSI the reachCustomersSI to set
     */
    public void setReachCustomersSI(ArrayList<SelectItem> reachCustomersSI) {
        this.reachCustomersSI = reachCustomersSI;
    }

    /**
     * @return the reachCustomersFlag
     */
    public long getReachCustomersFlag() {
        return reachCustomersFlag;
    }

    /**
     * @param reachCustomersFlag the reachCustomersFlag to set
     */
    public void setReachCustomersFlag(long reachCustomersFlag) {
        this.reachCustomersFlag = reachCustomersFlag;
    }
    
	/**
	 * @return the retentionTeamId
	 */
	public long getRetentionTeamId() {
		return retentionTeamId;
	}

	/**
	 * @param retentionTeamId the retentionTeamId to set
	 */
	public void setRetentionTeamId(long retentionTeamId) {
		this.retentionTeamId = retentionTeamId;
	}

	/**
	 * Initial parameters by constructor.
	 */
	public void init() {
		userPlatform = new ArrayList<String>();
		userPlatform.add("-1");
		/* Time Zone */
		timeZone = new ArrayList<String>();
        timeZone.add(ConstantsBase.ALL_TIME_ZONE_POPULATION);
        timeZoneNames = "";
        /* Countries */
        excludeCountries = "0";
		includeCountries = "0";
		updateCountries(timeZone);
		selectedCountriesList = null;
		/* Affiliate */
		affiliatesKeys = null;
		affiliatesNames = "";
		excludeAffiliates ="0";
		includeAffiliates ="0";
		/* Screen Name */
		screenName = Constants.EMPTY_STRING;
	}
	
	/**
	 * Initial Parameters Of Population Types.
	 */
	public void initPopulationTypes() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!screenName.equals(context.getViewRoot().getViewId())) {
			/* Population Types */
			populationTypes = "";
			populationTypesNames = "";
			populationTypesList = new ArrayList<String>();
			/* Screen Name */
			screenName = context.getViewRoot().getViewId();
		}
	}
	
    /************************************************* SORTING ************************************************************/

    /**
     * @return the prevSortColumn
     */
    public String getPrevSortColumn() {
	return prevSortColumn;
    }

    /**
     * @param prevSortColumn
     *            the prevSortColumn to set
     */
    public void setPrevSortColumn(String prevSortColumn) {
	this.prevSortColumn = prevSortColumn;
    }

    /**
     * @return the sortAscending
     */
    public boolean isSortAscending() {
	return sortAscending;
    }

    /**
     * @param sortAscending
     *            the sortAscending to set
     */
    public void setSortAscending(boolean sortAscending) {
	this.sortAscending = sortAscending;
    }

    /**
     * @return the sortColumn
     */
    public String getSortColumn() {
	return sortColumn;
    }

    /**
     * @param sortColumn
     *            the sortColumn to set
     */
    public void setSortColumn(String sortColumn) {
	this.sortColumn = sortColumn;
    }

    /**
     * Sort list
     * 
     * @return
     */
    public String sortList() {
	if (activateAscending) {
	    if (sortColumn.equals(prevSortColumn)) {
		sortAscending = !sortAscending;
	    } else {
		sortAscending = true;
	    }
	}

	activateAscending = true;
	prevSortColumn = sortColumn;

	if (sortColumn.equals("amount")) {
	    Collections.sort(dataPage.getData(), new AmountComparator(sortAscending));
	}
	
	return null;
    }
    
    public void sortListByAmount() {    	
    	if (sortAmountColumnDesc) {
    		sortAmountColumn = "1";
    		sortAmountColumnDesc = false;
    	} else {
    		sortAmountColumn = "0";
    		sortAmountColumnDesc = true;
    	} 
    	
    	setPagingUpdate(true, false);
    }
    private class AmountComparator implements Comparator {
	private boolean ascending;

	public AmountComparator(boolean asc) {
	    ascending = asc;
	}

	@Override
	public int compare(Object o1, Object o2) {
	    PopulationEntry pe1 = (PopulationEntry) o1;
	    PopulationEntry pe2 = (PopulationEntry) o2;
	    if (ascending) {
		return new Double(pe1.getFirstDeclineAmountUsd()).compareTo(new Double(pe2.getFirstDeclineAmountUsd()));
	    }
	    return -new Double(pe1.getFirstDeclineAmountUsd()).compareTo(new Double(pe2.getFirstDeclineAmountUsd()));
	}
    }

	/**
	 * @return the supportMoreOptionsType
	 */
	public long getSupportMoreOptionsType() {
		return supportMoreOptionsType;
	}

	/**
	 * @param supportMoreOptionsType the supportMoreOptionsType to set
	 */
	public void setSupportMoreOptionsType(long supportMoreOptionsType) {
		this.supportMoreOptionsType = supportMoreOptionsType;
	}

	/**
	 * @return the everCalled
	 */
	public long getEverCalled() {
		return everCalled;
	}

	/**
	 * @param everCalled the everCalled to set
	 */
	public void setEverCalled(long everCalled) {
		this.everCalled = everCalled;
	}

	/**
	 * @return the lastLoginInXMinutes
	 */
	public long getLastLoginInXMinutes() {
		return lastLoginInXMinutes;
	}

	/**
	 * @param lastLoginInXMinutes the lastLoginInXMinutes to set
	 */
	public void setLastLoginInXMinutes(long lastLoginInXMinutes) {
		this.lastLoginInXMinutes = lastLoginInXMinutes;
	}

	/**
	 * @return the sortAmountColumn
	 */
	public String getSortAmountColumn() {
		return sortAmountColumn;
	}

	/**
	 * @param sortAmountColumn the sortAmountColumn to set
	 */
	public void setSortAmountColumn(String sortAmountColumn) {
		this.sortAmountColumn = sortAmountColumn;
	}
	
	/**
	 * @return the userPlatform
	 */
	public List<String> getUserPlatform() {
		return userPlatform;
	}

	/**
	 * @param userPlatform the userPlatform to set
	 */
	public void setUserPlatform(List<String> userPlatform) {
		this.userPlatform = userPlatform;
	}

    /**
     * Get writers list to string
     * 
     * @param list
     * @return
     */
    public String getWritersListToString(List list) {
		if (list == null || list.size() == 0) {
		    if (null != list) {
		    	list.add("-1");
		    }
		    return "-1";
		}
	
		String tmp = ConstantsBase.EMPTY_STRING;
		for (int i = 0; i < list.size(); i++) {
		    tmp += list.get(i) + ",";
		}
		return tmp.substring(0, tmp.length() - 1);
    }

	/**
	 * @return the countryGroupTierId
	 */
	public long getCountryGroupTierId() {
		return countryGroupTierId;
	}

	/**
	 * @param countryGroupTierId the countryGroupTierId to set
	 */
	public void setCountryGroupTierId(long countryGroupTierId) {
		this.countryGroupTierId = countryGroupTierId;
	}

	public boolean isReachCustomerFlag() {
		return isReachCustomerFlag;
	}

	public void setReachCustomerFlag(boolean isReachCustomerFlag) {
		if(isReachCustomerFlag){
			reachCustomersFlag = 3;
		}else{
			reachCustomersFlag = 0;
		}
		this.isReachCustomerFlag = isReachCustomerFlag;
	}
}
