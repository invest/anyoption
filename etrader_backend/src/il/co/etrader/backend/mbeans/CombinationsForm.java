package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.MarketingCombinationSeris;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.MarketingDomainsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.CommonUtil.selectItemComparator;



public class CombinationsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;
	private static final Logger logger = Logger.getLogger(CombinationsForm.class);
	private static final long STATIC_LANDING_PAGE_TYPE = 1;

	private ArrayList<MarketingCombination> list;
	private MarketingCombination combination;
	private ArrayList<MarketingCombinationSeris> seriesList;
	private ArrayList<SelectItem> listLandingPageNames = new ArrayList<SelectItem>();
	private ArrayList<SelectItem> urlSkins;
	
	private long skinId;
	private String campaign;
	private String source;
	private long medium;
	private long content;
	private String verticalSize;
	private String horizontalSize;
	private String type;
	private String location;
	private long landingPage;
	private boolean showUrl;
	private long existsPixelId;
	private long combId;
	private SelectItem pixel;
	private long fetchCount = 1000;
	private ArrayList<Long> pixelsList;
	private long numSeries = 1;
	private String htmlBuffer;

	public CombinationsForm() throws SQLException{
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Init CombinationForm");
  		 }
		showUrl = false;
		combId = 0;
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;

        if (u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingCombinations(skinId, campaign, source, medium, content,
				verticalSize, horizontalSize, type, location, landingPage, combId, writerIdForSkin, fetchCount);
		
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"CombinationForm.updateList: " + ls + list);
  		 }
		
		if(pixelsList != null ){
		    pixelsList.clear();
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert combination
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		combination.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateCombination(combination, pixelsList);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_COMBINATIONS;
	}

	/**
	 * Update / insert combination and create url
	 * @return
	 * @throws SQLException
	 */
	public String createUrl() throws SQLException{

		combination.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateCombination(combination, pixelsList);
		if (res == false) {
			return null;
		}

		updateList();
		combination = getById(combination.getId());
		showUrl = true;
		return Constants.NAV_MARKETING_COMBINATION;
	}
	
	/**
	 *Series combinations
	 * @return
	 * @throws SQLException
	 */
	public String createSeries() throws SQLException{
		
		if(seriesList != null){
			seriesList.clear();
		} else {
			seriesList = new ArrayList<MarketingCombinationSeris>();
		}
		
		if(numSeries > 0){
			combination.setWriterId(AdminManager.getWriterId());
			for (int x = 1; x < numSeries + 1; x ++) {
				MarketingCombinationSeris mcs = new MarketingCombinationSeris(combination, x, pixelsList);
				//mcs.setId(x);
				seriesList.add(mcs);
			}
			CommonUtil.setTablesToFirstPage();
			return Constants.NAV_MARKETING_COMBINATIONS_SERIES;
		} else {
			return null;
		}
	}
	
	public String insertSeries() throws SQLException{
		
		if(seriesList != null){
			List<Long> mcIds = MarketingManager.insertMarketingCombination(seriesList);
			seriesList = MarketingManager.getAllSerries(mcIds);
			return Constants.NAV_MARKETING_COMBINATIONS_SERIES_LIST;
		} else {
			return null;
		}
	}
	
	public String deleteSeriesRow() {
		seriesList.remove(combination);
		return Constants.NAV_MARKETING_COMBINATIONS_SERIES;
	}
	
	/**
	 * Export data to excel
	 * @throws IOException
	 */
	public void exportHtmlTableToExcel() throws IOException {

		//Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>" + htmlBuffer + "</html>";

        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "marketing_url_series_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();
    }

	/**
	 * Get combination from list
	 * @param id
	 * @return
	 */
	private MarketingCombination getById(long id) {
		MarketingCombination newCom = new MarketingCombination();
		for ( MarketingCombination m : list ) {
			if ( m.getId() == id ) {
				newCom = m;
				break;
			}
		}
		return newCom;
	}

	public int getListSize() {
		return list.size();
	}
	
	public int getSeriesListSize() {
		return seriesList.size();
	}

	/**
	 * Navigate to edit combination
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_COMBINATION;
	}

	/**
	 * Navigate to insert new combination
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		combination = new MarketingCombination();
		combination.setWriterId(AdminManager.getWriterId());
		combination.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_COMBINATION;
	}

	/**
	 * Navigate to insert duplicated combination
	 * @return
	 * @throws SQLException
	 */
	public String navDup() throws SQLException {
		combination.setId(0);
		combination.setWriterId(AdminManager.getWriterId());
		combination.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_COMBINATION;
	}

	/**
	 * Back to combinations page
	 * @return
	 * @throws SQLException
	 */
	public String navBack() throws SQLException {
		return Constants.NAV_MARKETING_COMBINATIONS;
	}

	/**
	 * Delete pixel
	 * @return
	 * @throws SQLException
	 */
	public String deletePixel() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		MarketingManager.deletePixelByCombId(combination.getId(), ((Long)pixel.getValue()).longValue());
		ArrayList<SelectItem> pixelsList = new ArrayList<SelectItem>();
		for (SelectItem si : combination.getPixelsSi()) {
			if (((Long)si.getValue()) != ((Long)pixel.getValue())) {
				pixelsList.add(si);
			}
		}
		combination.setPixelsSi(pixelsList);
		context.renderResponse();
		return null;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the campaign
	 */
	public String getCampaign() {
		return campaign;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	/**
	 * @return the combination
	 */
	public MarketingCombination getCombination() {
		return combination;
	}

	/**
	 * @param combination the combination to set
	 */
	public void setCombination(MarketingCombination combination) {
		this.combination = combination;
	}

	/**
	 * @return the content
	 */
	public long getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(long content) {
		this.content = content;
	}

	/**
	 * @return the horizontalSize
	 */
	public String getHorizontalSize() {
		return horizontalSize;
	}

	/**
	 * @param horizontalSize the horizontalSize to set
	 */
	public void setHorizontalSize(String horizontalSize) {
		this.horizontalSize = horizontalSize;
	}

	/**
	 * @return the landingPage
	 */
	public long getLandingPage() {
		return landingPage;
	}

	/**
	 * @param landingPage the landingPage to set
	 */
	public void setLandingPage(long landingPage) {
		this.landingPage = landingPage;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the medium
	 */
	public long getMedium() {
		return medium;
	}

	/**
	 * @param medium the medium to set
	 */
	public void setMedium(long medium) {
		this.medium = medium;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the verticalSize
	 */
	public String getVerticalSize() {
		return verticalSize;
	}

	/**
	 * @param verticalSize the verticalSize to set
	 */
	public void setVerticalSize(String verticalSize) {
		this.verticalSize = verticalSize;
	}

	/**
	 * @return the showUrl
	 */
	public boolean isShowUrl() {
		return showUrl;
	}

	/**
	 * @param showUrl the showUrl to set
	 */
	public void setShowUrl(boolean showUrl) {
		this.showUrl = showUrl;
	}

	/**
	 * @return the existsPixelId
	 */
	public long getExistsPixelId() {
		return existsPixelId;
	}

	/**
	 * @param existsPixelId the existsPixelId to set
	 */
	public void setExistsPixelId(long existsPixelId) {
		this.existsPixelId = existsPixelId;
	}

	/**
	 * @return the pixel
	 */
	public SelectItem getPixel() {
		return pixel;
	}

	/**
	 * @param pixel the pixel to set
	 */
	public void setPixel(SelectItem pixel) {
		this.pixel = pixel;
	}

	public String getCombId() {
		if (0 == combId) {
			return "";
		}
		return String.valueOf(combId);
	}

	public void setCombId(String combId) {
		long l = 0;
		try {
			l = Long.valueOf(combId);
		} catch (Exception e) {
			// not a number
		}finally {
			this.combId = l;
		}

	}

    /**
     * @return the fetchCount
     */
    public long getFetchCount() {
        return fetchCount;
    }

    /**
     * @param fetchCount the fetchCount to set
     */
    public void setFetchCount(long fetchCount) {
        this.fetchCount = fetchCount;
    }

    /**
     * @return the skinList
     */
    public ArrayList<Long> getPixelsList() {
        return pixelsList;
    }

    /**
     * @param skinList the skinList to set
     */
    public void setPixelsList(ArrayList<Long> pixelsList) {
        this.pixelsList = pixelsList;
    }

	public long getNumSeries() {
		return numSeries;
	}

	public void setNumSeries(long numSeries) {
		this.numSeries = numSeries;
	}

	public ArrayList<MarketingCombinationSeris> getSeriesList() {
		return seriesList;
	}

	public void setSeriesList(ArrayList<MarketingCombinationSeris> seriesList) {
		this.seriesList = seriesList;
	}

	public String getHtmlBuffer() {
		return htmlBuffer;
	}

	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}
	
	/**
	 * Get marketing domains SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getMarketingDomainsSI() {
		return getMarketingDomainsByfiltersSI(combination.getSkinId(), combination.getUrlSourceTypeId(), combination.getLandingPageId());
	}
	
	/**
	 * Get marketing domains by filters SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getMarketingDomainsByfiltersSI(long skinId, long urlSourceTypeId, long marketingLandingpageId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		if (skinId > 0 && urlSourceTypeId > 0 && marketingLandingpageId > 0) {
			try {
				logger.log(Level.DEBUG,"About to get list of marketing domains.");
				list.addAll(MarketingDomainsManager.getMarketingDomainsByfiltersSI(skinId, urlSourceTypeId, marketingLandingpageId));
			} catch (SQLException e) {
				logger.error("Problem to get list of marketing domains", e);
			}
		}
		return list;
	}

	/**
	 * marketingDomainsSeriesSI
	 * @param mcs
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> marketingDomainsSeriesSI(MarketingCombinationSeris mcs) {
		return getMarketingDomainsByfiltersSI(mcs.getSkinId(), mcs.getUrlSourceTypeId(), mcs.getLandingPageId());
	}
	
	/**
	 * Get marketing domains SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getMarketingLpNamesSI() {
		return getMarketingLpNamesByfiltersSI(combination.getLandingPageType(), combination.getPathId());
	}
	
	/**
	 * Get marketing domains by filters SI
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getMarketingLpNamesByfiltersSI(long landingPageType, long pathId) {
		listLandingPageNames = new ArrayList<SelectItem>();
		try {
			logger.log(Level.DEBUG,"About to get list of marketing landing pages.");
			listLandingPageNames.addAll(MarketingManager.getMarketingLpNamesSI(landingPageType, pathId));
		} catch (SQLException e) {
			logger.error("Problem to get list of marketing landing pages.", e);
		}
		return listLandingPageNames;
	}
	
	/**
	 * isLandingPageTypeStatic
	 * @return true if static
	 */
	public boolean isLandingPageTypeDynamic() {
		if (combination.getLandingPageType() == Constants.MARKETING_LANDING_PAGE_DYNAMIC) {
			return true;
		}
		return false;
	}

	/**
	 * @return the listLandingPageNames
	 */
	public ArrayList<SelectItem> getListLandingPageNames() {
		return listLandingPageNames;
	}

	/**
	 * @param listLandingPageNames the listLandingPageNames to set
	 */
	public void setListLandingPageNames(ArrayList<SelectItem> listLandingPageNames) {
		this.listLandingPageNames = listLandingPageNames;
	}
	
	public ArrayList<SelectItem> getUrlSkins() {
		if (urlSkins == null) {
			urlSkins = WritersManager.getUrlSkins();
	        urlSkins = CommonUtil.translateSI(urlSkins);
	        Collections.sort(urlSkins, new selectItemComparator());
		}
		return urlSkins;
	}

}
