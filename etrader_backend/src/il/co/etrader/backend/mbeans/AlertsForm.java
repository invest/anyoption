package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AlertsDAOManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.bl_vos.AlertsBase;
import il.co.etrader.util.CommonUtil;

public class AlertsForm  extends FormBase implements Serializable {
    
    private static final long serialVersionUID = 7843589610895821316L;
    
    private boolean deposit=false;
    private boolean failedDeposit=false;
    private boolean withdrawRequest=false;
    private boolean investment=false;
    private boolean bonusDone=false;
    private boolean login=false;
    private Long userId;
    private String userName;
    private Integer fromAmount;
    private Integer toAmount;
    private final String bonusMailSubject = "Bonus done";
    private final String loginMailSubject = "Login";
    private final String depositMailSubject="Deposit (succeed)";
    private final String depositFailSubject="Failed deposit";
    private final String withdrawMailSubject = "Withdraw request";
    private final String investmentMailSubject="Investment";
    private final String  successMsg = " is successful.";
    private final String failMsg = " has failed.";
    private String userSkin;
    private String userBalance;
    private Integer resLogin;
    
    private ArrayList<AlertsBase> alertsList;
    
    AlertsDAOManager adm = new AlertsDAOManager();  
    
    public AlertsForm() throws SQLException{
        
        init();
        
    }
    
    private ArrayList<AlertsBase> fillEmptyStructure(ArrayList<AlertsBase> insertAlerts){
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        
        if(bonusDone){
            AlertsBase bonusBase = new AlertsBase();
            bonusBase.setUserId(userId);
            bonusBase.setAlertType(1);
            bonusBase.setAlertStatusId(null);
            bonusBase.setAlertEmailSubject(bonusMailSubject);
            bonusBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(bonusBase);
        } 
         
        if(login){
            AlertsBase loginBase = new AlertsBase();
            loginBase.setUserId(userId);
            loginBase.setAlertType(2);
            loginBase.setAlertStatusId(null);
            loginBase.setAlertEmailSubject(loginMailSubject);
            loginBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(loginBase);
        } 
        
        if(deposit){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(3);
            depositBase.setAlertStatusId("2");
            depositBase.setAlertClassType(1);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(depositBase);
        
            AlertsBase tdepositBase = new AlertsBase();
            tdepositBase.setUserId(userId);
            tdepositBase.setAlertType(4);
            tdepositBase.setAlertStatusId("7");
            tdepositBase.setAlertClassType(1);
            tdepositBase.setAlertEmailSubject(depositMailSubject);
            tdepositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(tdepositBase);
        
            AlertsBase thdepositBase = new AlertsBase();
            thdepositBase.setUserId(userId);
            thdepositBase.setAlertType(5);
            thdepositBase.setAlertStatusId("8");
            thdepositBase.setAlertClassType(1);
            thdepositBase.setAlertEmailSubject(depositMailSubject);
            thdepositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(thdepositBase);
            
        }
         
        if(failedDeposit){
            AlertsBase fdepositBase = new AlertsBase();
            fdepositBase.setUserId(userId);
            fdepositBase.setAlertType(6);
            fdepositBase.setAlertStatusId("3");
            fdepositBase.setAlertClassType(1);
            fdepositBase.setAlertEmailSubject(depositFailSubject);
            fdepositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(fdepositBase);
        }
        
        if(withdrawRequest){
            AlertsBase withdrawBase = new AlertsBase();
            withdrawBase.setUserId(userId);
            withdrawBase.setAlertType(7);
            withdrawBase.setAlertStatusId("%");
            withdrawBase.setAlertClassType(2);
            withdrawBase.setAlertEmailSubject(withdrawMailSubject);
            withdrawBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(withdrawBase);
        }
        
        if(investment){
            AlertsBase iBase = new AlertsBase();
            iBase.setUserId(userId);
            iBase.setAlertType(8);
            iBase.setAlertStatusId(null);
            iBase.setAlertClassType(null);
            iBase.setAlertFromAmount(fromAmount);
            iBase.setAlertToAmount(toAmount);
            iBase.setAlertEmailSubject(investmentMailSubject);
            iBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(iBase);
         }
    
        
        return insertAlerts;
        
    }
    
    private ArrayList<AlertsBase> fillDelete(ArrayList<AlertsBase> deleteAlerts,FacesContext ctx){
        
        if((!bonusDone)&&(ifExists(alertsList,1))){
            AlertsBase bonusBase = new AlertsBase();
            bonusBase.setUserId(userId);
            bonusBase.setAlertType(1);
            bonusBase.setAlertStatusId(null);
            bonusBase.setAlertClassType(0);
            bonusBase.setAlertFromAmount(0);
            bonusBase.setAlertToAmount(0);
            bonusBase.setAlertEmailSubject(bonusMailSubject);
            bonusBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(bonusBase);
        }if((!login)&&(ifExists(alertsList,2))){
            AlertsBase loginBase = new AlertsBase();
            loginBase.setUserId(userId);
            loginBase.setAlertType(2);
            loginBase.setAlertStatusId(null);
            loginBase.setAlertClassType(0);
            loginBase.setAlertFromAmount(0);
            loginBase.setAlertToAmount(0);
            loginBase.setAlertEmailSubject(loginMailSubject);
            loginBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(loginBase);
        }if((!deposit)&&(ifExists(alertsList,3))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(3);
            depositBase.setAlertStatusId("2");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(0);
            depositBase.setAlertToAmount(0);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(depositBase);
        }if((!deposit)&&(ifExists(alertsList,4))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(4);
            depositBase.setAlertStatusId("7");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(0);
            depositBase.setAlertToAmount(0);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(depositBase);
        }if((!deposit)&&(ifExists(alertsList,5))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(5);
            depositBase.setAlertStatusId("8");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(0);
            depositBase.setAlertToAmount(0);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(depositBase);
        }if((!failedDeposit)&&(ifExists(alertsList,6))){
            AlertsBase fdepositBase = new AlertsBase();
            fdepositBase.setUserId(userId);
            fdepositBase.setAlertType(6);
            fdepositBase.setAlertStatusId("3");
            fdepositBase.setAlertClassType(1);
            fdepositBase.setAlertFromAmount(0);
            fdepositBase.setAlertToAmount(0);
            fdepositBase.setAlertEmailSubject(depositFailSubject);
            fdepositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(fdepositBase);
        }if((!withdrawRequest)&&(ifExists(alertsList,7))){
            AlertsBase withdrawBase = new AlertsBase();
            withdrawBase.setUserId(userId);
            withdrawBase.setAlertType(7);
            withdrawBase.setAlertStatusId("%");
            withdrawBase.setAlertClassType(1);
            withdrawBase.setAlertFromAmount(0);
            withdrawBase.setAlertToAmount(0);
            withdrawBase.setAlertEmailSubject(withdrawMailSubject);
            withdrawBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(withdrawBase);
        }if((!investment)&&(ifExists(alertsList,8))){
            AlertsBase iBase = new AlertsBase();
            iBase.setUserId(userId);
            iBase.setAlertType(8);
            iBase.setAlertStatusId(null);
            iBase.setAlertClassType(0);
            iBase.setAlertFromAmount(fromAmount);
            iBase.setAlertToAmount(toAmount);
            iBase.setAlertEmailSubject(investmentMailSubject);
            iBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            deleteAlerts.add(iBase);
          }
    
        
        return deleteAlerts;
    }
    
    
    private ArrayList<AlertsBase> fillInsert(ArrayList<AlertsBase> insertAlerts,FacesContext ctx){
        
        if((bonusDone)&&(!ifExists(alertsList,1))){
            AlertsBase bonusBase = new AlertsBase();
            bonusBase.setUserId(userId);
            bonusBase.setAlertType(1);
            bonusBase.setAlertStatusId(null);
            bonusBase.setAlertFromAmount(null);
            bonusBase.setAlertToAmount(null);
            bonusBase.setAlertEmailSubject(bonusMailSubject);
            bonusBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(bonusBase);
        }if((login)&&(!ifExists(alertsList,2))){
            AlertsBase loginBase = new AlertsBase();
            loginBase.setUserId(userId);
            loginBase.setAlertType(2);
            loginBase.setAlertStatusId(null);
            loginBase.setAlertFromAmount(null);
            loginBase.setAlertToAmount(null);
            loginBase.setAlertEmailSubject(loginMailSubject);
            loginBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(loginBase);
        }if((deposit)&&(!ifExists(alertsList,3))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(3);
            depositBase.setAlertStatusId("2");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(null);
            depositBase.setAlertToAmount(null);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(depositBase);
        }if((deposit)&&(!ifExists(alertsList,4))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(4);
            depositBase.setAlertStatusId("7");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(null);
            depositBase.setAlertToAmount(null);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(depositBase);
        }if((deposit)&&(!ifExists(alertsList,5))){
            AlertsBase depositBase = new AlertsBase();
            depositBase.setUserId(userId);
            depositBase.setAlertType(5);
            depositBase.setAlertStatusId("8");
            depositBase.setAlertClassType(1);
            depositBase.setAlertFromAmount(null);
            depositBase.setAlertToAmount(null);
            depositBase.setAlertEmailSubject(depositMailSubject);
            depositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(depositBase);
        }if((failedDeposit)&&(!ifExists(alertsList,6))){
            AlertsBase fdepositBase = new AlertsBase();
            fdepositBase.setUserId(userId);
            fdepositBase.setAlertType(6);
            fdepositBase.setAlertStatusId("3");
            fdepositBase.setAlertClassType(1);
            fdepositBase.setAlertFromAmount(null);
            fdepositBase.setAlertToAmount(null);
            fdepositBase.setAlertEmailSubject(depositFailSubject);
            fdepositBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(fdepositBase);
        }if((withdrawRequest)&&(!ifExists(alertsList,7))){
            AlertsBase withdrawBase = new AlertsBase();
            withdrawBase.setUserId(userId);
            withdrawBase.setAlertType(7);
            withdrawBase.setAlertStatusId("%");
            withdrawBase.setAlertClassType(2);
            withdrawBase.setAlertFromAmount(null);
            withdrawBase.setAlertToAmount(null);
            withdrawBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(withdrawBase);
        }if((investment)&&(!ifExists(alertsList,8))){
            AlertsBase iBase = new AlertsBase();
            iBase.setUserId(userId);
            iBase.setAlertType(8);
            iBase.setAlertStatusId(null);
            iBase.setAlertClassType(null);
            iBase.setAlertFromAmount(fromAmount);
            iBase.setAlertToAmount(toAmount);
            iBase.setAlertEmailSubject(investmentMailSubject);
            iBase.setAlertEmailRecepients(ctx.getExternalContext().getInitParameter("mailRecipients"));
            insertAlerts.add(iBase);
          }
    
        
        
        
        return insertAlerts;
    }
    
    private boolean ifExists(ArrayList<AlertsBase> bases,int alertType) {
        
        int listSize = bases.size();
        for(int c=0;c<listSize;c++){
            
            if(bases.get(c).getAlertType() ==1){
                return true;
               }
            if(bases.get(c).getAlertType() ==2){
                return true;
            }
            if(bases.get(c).getAlertType() ==3){
                return true;
            }
            if(bases.get(c).getAlertType() ==4){
                return true;
            }
            if(bases.get(c).getAlertType() ==5){
                return true;
            }
            if(bases.get(c).getAlertType() ==6){
                return true;
            }
            if(bases.get(c).getAlertType() ==7){
                return true;
            }
            if(bases.get(c).getAlertType() ==8){
                return true;
            }
        
           }
        
        return false;
        
    }
    
    
    public void commit() throws SQLException {
        
        resLogin=0;
        
        ArrayList<AlertsBase> insertAlerts = new ArrayList<AlertsBase>();
        boolean resSQL = true;

          int listSize = alertsList.size();
          
          if (listSize !=0){
              resSQL = adm.deleteAlertsBaseAll(userId);
          }
          insertAlerts = fillEmptyStructure(insertAlerts);

           if (insertAlerts.size()!=0){ 
               
               if(adm.insertAlertsBase(insertAlerts) && resSQL){     
                   resLogin = 5;
              }
               else{
                   resLogin = 6;
           }
               
         }
            resLogin = 5;
            init();

    }
    
    
    public void load() throws SQLException, ParseException{
        resLogin=0;
        setUserSkin(null);
        setUserBalance(null);
        
        if (userId!=null && !userName.isEmpty()) {
            resLogin=1;
            return;
        }
        if (userId==null && userName.isEmpty()) {
            resLogin=2;
            return;
        }
        
        if (!adm.getExistsUser(userName, userId)) {
            resLogin=0;
            return;
        }        
        User user= new User();
        if (userId!=null) {
            user.setId(userId);
            user.setUserName(null);
            user.selectUserNoPopUpById();
                
        } else {
            user.setUserName(userName.toUpperCase());
            user.setId(Long.parseLong("0"));
            user.refreshStrip();
        }
        
        setUserSkin(user.getSkinName());
        setUserBalance(CommonUtil.displayAmount(user.getBalance(), true, user.getCurrencyId())); 
        setUserId(user.getId());
        setUserName(user.getUserName());
        
        init();
        
        resLogin=3;
    }
    
    private void init() throws SQLException{
        
        if(userId!=null || userName!=null){
            
            deposit=false;
            failedDeposit=false;
            withdrawRequest=false;
            investment=false;
            bonusDone=false;
            login=false;
            fromAmount =0;
            toAmount=0;
            
        int listSize = getAlertsList().size();
        for(int c=0;c<listSize;c++){
            
            if(alertsList.get(c).getAlertType() ==1){
                setBonusDone(true);
            }
            if(alertsList.get(c).getAlertType() ==2){
                setLogin(true);
            }
            if(alertsList.get(c).getAlertType() ==3){
                setDeposit(true);
            }
            if(alertsList.get(c).getAlertType() ==4){
                setDeposit(true);
            }
            if(alertsList.get(c).getAlertType() ==5){
                setDeposit(true);
            }
            if(alertsList.get(c).getAlertType() ==6){
                setFailedDeposit(true);
            }
            if(alertsList.get(c).getAlertType() ==7){
                setWithdrawRequest(true);
            }
            if(alertsList.get(c).getAlertType() ==8){
                    
                    setInvestment(true);
                    setFromAmount(alertsList.get(c).getAlertFromAmount());
                    setToAmount(alertsList.get(c).getAlertToAmount());
            
               }
        
            }
          
         }else{
             
            deposit=false;
            failedDeposit=false;
            withdrawRequest=false;
            investment=false;
            bonusDone=false;
            login=false;
            fromAmount =0;
            toAmount=0;
             
         }
        
    }
    
    public Integer getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(Integer fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Integer getToAmount() {
        return toAmount;
    }

    public void setToAmount(Integer toAmount) {
        this.toAmount = toAmount;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    public ArrayList<AlertsBase> getAlertsList() throws SQLException {
        
        alertsList = adm.getAlertsByUser(userId); 
        
        return alertsList;
    }
    public void setAlertsList(ArrayList<AlertsBase> alertsList) {
        this.alertsList = alertsList;
    }
    public boolean isDeposit() {
        return deposit;
    }
    public void setDeposit(boolean deposit) {
        this.deposit = deposit;
    }
    public boolean isFailedDeposit() {
        return failedDeposit;
    }
    public void setFailedDeposit(boolean failedDeposit) {
        this.failedDeposit = failedDeposit;
    }
    public boolean isWithdrawRequest() {
        return withdrawRequest;
    }
    public void setWithdrawRequest(boolean withdrawRequest) {
        this.withdrawRequest = withdrawRequest;
    }
    public boolean isInvestment() {
        return investment;
    }
    public void setInvestment(boolean investment) {
        this.investment = investment;
    }
    public boolean isBonusDone() {
        return bonusDone;
    }
    public void setBonusDone(boolean bonusDone) {
        this.bonusDone = bonusDone;
    }
    public boolean isLogin() {
        return login;
    }
    public void setLogin(boolean login) {
        this.login = login;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSkin() {
        return userSkin;
    }

    public void setUserSkin(String userSkin) {
        this.userSkin = userSkin;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public Integer getResLogin() {
        return resLogin;
    }

    public void setResLogin(Integer resLogin) {
        this.resLogin = resLogin;
    }
    

}
