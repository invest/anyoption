package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingLandingPage;
import il.co.etrader.util.CommonUtil;



public class LandingPagesForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String landingPageName;
	private ArrayList<MarketingLandingPage> list;
	private MarketingLandingPage landingPage;

	public LandingPagesForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingLandingPages(landingPageName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert landing page
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		landingPage.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateLandingPage(landingPage);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_LANDING_PAGES;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit medium
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_LANDING_PAGE;
	}

	/**
	 * Navigate to insert new medium
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		landingPage = new MarketingLandingPage();
		landingPage.setWriterId(AdminManager.getWriterId());
		landingPage.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_LANDING_PAGE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the landingPage
	 */
	public MarketingLandingPage getLandingPage() {
		return landingPage;
	}

	/**
	 * @param landingPage the landingPage to set
	 */
	public void setLandingPage(MarketingLandingPage landingPage) {
		this.landingPage = landingPage;
	}

	/**
	 * @return the landingPageName
	 */
	public String getLandingPageName() {
		return landingPageName;
	}

	/**
	 * @param landingPageName the landingPageName to set
	 */
	public void setLandingPageName(String landingPageName) {
		this.landingPageName = landingPageName;
	}


}
