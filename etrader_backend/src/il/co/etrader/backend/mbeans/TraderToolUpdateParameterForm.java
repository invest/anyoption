package il.co.etrader.backend.mbeans;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.ServiceConfig;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class TraderToolUpdateParameterForm implements Serializable {

	private static final long serialVersionUID = -1020544479058468097L;

	private static final Logger log = Logger.getLogger(TraderToolUpdateParameterForm.class);

	private double t25Parameter;
	private double sp500Parameter;
	private double ftseParameter;
	private double cacParameter;
	private double klseParameter;
	private double d4BanksParameter;
	private double banksBursaParameter;
	private double d4SpreadParameter;

	private ServiceConfig serviceConfig;
	private BackendLevelsCache levelsCache;
	FacesContext context = null;
	private String errorMsg;
	private String successMsg;
	private String noChangeMsg;

	public TraderToolUpdateParameterForm() {
		try {
			context = FacesContext.getCurrentInstance();
			levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
			serviceConfig = TraderManager.getServiceConfig();
			t25Parameter = serviceConfig.getTa25Parameter();
			sp500Parameter = serviceConfig.getSp500Parameter().doubleValue();
			ftseParameter = serviceConfig.getFtseParameter().doubleValue();
			cacParameter = serviceConfig.getCacParameter().doubleValue();
			klseParameter = serviceConfig.getKlseParameter().doubleValue();
			d4BanksParameter = serviceConfig.getD4BanksParameter().doubleValue();
			banksBursaParameter = serviceConfig.getBanksBursaParameter().doubleValue();
			d4SpreadParameter = serviceConfig.getD4SpreadParameter().doubleValue();
			} catch (SQLException e) {
			log.info("cant load service config!",e);
		}
		errorMsg = "tradertool.faild.parameter";
		successMsg = "tradertool.success.enable";
		noChangeMsg = "tradertool.same.parameter";
	}

	/**
	 * @return the cacParameter
	 */
	public double getCacParameter() {
		return cacParameter;
	}

	/**
	 * @param cacParameter the cacParameter to set
	 */
	public void setCacParameter(double cacParameter) {
		this.cacParameter = cacParameter;
	}

	/**
	 * @return the ftseParameter
	 */
	public double getFtseParameter() {
		return ftseParameter;
	}

	/**
	 * @param ftseParameter the ftseParameter to set
	 */
	public void setFtseParameter(double ftseParameter) {
		this.ftseParameter = ftseParameter;
	}

	/**
	 * @return the sp500Parameter
	 */
	public double getSp500Parameter() {
		return sp500Parameter;
	}

	/**
	 * @param sp500Parameter the sp500Parameter to set
	 */
	public void setSp500Parameter(double sp500Parameter) {
		this.sp500Parameter = sp500Parameter;
	}

	/**
	 * @return the t25Parameter
	 */
	public double getT25Parameter() {
		return t25Parameter;
	}

	/**
	 * @param parameter the t25Parameter to set
	 */
	public void setT25Parameter(double parameter) {
		t25Parameter = parameter;
	}

	/**
	 * @return the klseParameter
	 */
	public double getKlseParameter() {
		return klseParameter;
	}

	/**
	 * @param klseParameter the klseParameter to set
	 */
	public void setKlseParameter(double klseParameter) {
		this.klseParameter = klseParameter;
	}

	/**
	 * @return the banksBursaParameter
	 */
	public double getBanksBursaParameter() {
		return banksBursaParameter;
	}

	/**
	 * @param banksBursaParameter the banksBursaParameter to set
	 */
	public void setBanksBursaParameter(double banksBursaParameter) {
		this.banksBursaParameter = banksBursaParameter;
	}

	/**
	 * @return the d4BanksParameter
	 */
	public double getD4BanksParameter() {
		return d4BanksParameter;
	}

	/**
	 * @param banksParameter the d4BanksParameter to set
	 */
	public void setD4BanksParameter(double banksParameter) {
		d4BanksParameter = banksParameter;
	}

	/**
	 * @return the d4SpreadParameter
	 */
	public double getD4SpreadParameter() {
		return d4SpreadParameter;
	}

	/**
	 * @param spreadParameter the d4SpreadParameter to set
	 */
	public void setD4SpreadParameter(double spreadParameter) {
		d4SpreadParameter = spreadParameter;
	}

	public String updateT25() {
		String market = "TA25_PARAMETER";
		if (this.t25Parameter != serviceConfig.getTa25Parameter()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + t25Parameter);
			if (levelsCache.notifyForTA25ParameterChange(new BigDecimal(t25Parameter))) {
				log.debug("notify ok.");
				serviceConfig.setTa25Parameter(t25Parameter);
				try {
					TraderManager.updateParameter(market, t25Parameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_TA25, String.valueOf(t25Parameter));
					log.info("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateFtse() {
		String market = "ftse_parameter";
		if (this.ftseParameter != serviceConfig.getFtseParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + ftseParameter);
			if (levelsCache.notifyForFTSEParameterChange(new BigDecimal(ftseParameter))) {
				log.debug("notify ok.");
				serviceConfig.setFtseParameter(new BigDecimal(ftseParameter));
				try {
					TraderManager.updateParameter(market, ftseParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_FTSE, String.valueOf(ftseParameter));
					log.info("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateCac() {
		String market = "cac_parameter";
		if (this.cacParameter != serviceConfig.getCacParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + cacParameter);
			if (levelsCache.notifyForCACParameterChange(new BigDecimal(cacParameter))) {
				log.debug("notify ok.");
				serviceConfig.setCacParameter(new BigDecimal(cacParameter));
				try {
					TraderManager.updateParameter(market, cacParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_CAC, String.valueOf(cacParameter));
					log.info("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateSp500() {
		String market = "SP500_PARAMETER";
		if (this.sp500Parameter != serviceConfig.getSp500Parameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + sp500Parameter);
			if (levelsCache.notifyForSP500ParameterChange(new BigDecimal(sp500Parameter))) {
				log.debug("notify ok.");
				serviceConfig.setSp500Parameter(new BigDecimal(sp500Parameter));
				try {
					TraderManager.updateParameter(market, sp500Parameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_SP500, String.valueOf(sp500Parameter));
					log.debug("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateKlse() {
		String market = "KLSE_PARAMETER";
		if (this.klseParameter != serviceConfig.getKlseParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + klseParameter);
			if (levelsCache.notifyForKLSEParameterChange(new BigDecimal(klseParameter))) {
				log.debug("notify ok.");
				serviceConfig.setKlseParameter(new BigDecimal(klseParameter));
				try {
					TraderManager.updateParameter(market, klseParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_KLSE, String.valueOf(klseParameter));
					log.debug("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateD4Banks() {
		String market = "D4Banks_PARAMETER";
		if (this.d4BanksParameter != serviceConfig.getD4BanksParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + d4BanksParameter);
			if (levelsCache.notifyForD4BanksParameterChange(new BigDecimal(d4BanksParameter))) {
				log.debug("notify ok.");
				serviceConfig.setD4BanksParameter(new BigDecimal(d4BanksParameter));
				try {
					TraderManager.updateParameter(market, d4BanksParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_D4BANKS, String.valueOf(d4BanksParameter));
					log.debug("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateBanksBursa() {
		String market = "BanksBursa_PARAMETER";
		if (this.banksBursaParameter != serviceConfig.getBanksBursaParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + banksBursaParameter);
			if (levelsCache.notifyForBanksBursaParameterChange(new BigDecimal(banksBursaParameter))) {
				log.debug("notify ok.");
				serviceConfig.setBanksBursaParameter(new BigDecimal(banksBursaParameter));
				try {
					TraderManager.updateParameter(market, banksBursaParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_BANKSBURSA, String.valueOf(banksBursaParameter));
					log.debug("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	public String updateD4Spread() {
		String market = "D4Spread_PARAMETER";
		if (this.d4SpreadParameter != serviceConfig.getD4SpreadParameter().doubleValue()) {
			String msgKey = errorMsg;
			long writerId = Utils.getWriter().getWriter().getId();
			log.debug("sending notify for " + market + ":" + d4SpreadParameter);
			if (levelsCache.notifyForD4SpreadParameterChange(new BigDecimal(d4SpreadParameter))) {
				log.debug("notify ok.");
				serviceConfig.setD4SpreadParameter(new BigDecimal(d4SpreadParameter));
				try {
					TraderManager.updateParameter(market, d4SpreadParameter);
					TraderManager.addToLog(writerId, "service_config", 0, ConstantsBase.LOG_COMMANDS_UPDATE_D4SPREAD, String.valueOf(d4SpreadParameter));
					log.debug("success to change parameter for market: " + market);
					msgKey = successMsg;
				} catch (SQLException e2) {
					log.error("can't perform changing parameter for " + market, e2);
				}
			}
			returnMsg(msgKey, market);
		} else {
			log.info("same parameter for " + market + " no change");
			returnMsg(noChangeMsg, market);
		}
		return Constants.NAV_PARENT_REFRESH;
	}


	public void returnMsg(String msg, String market) {
		String[] params = new String[1];
		params[0] = market;
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(msg, params),null);
		context.addMessage(null, fm);
        
	}
	
	public String gett25ParameterNameTxt(){
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, Market.MARKET_TEL_AVIV_25_ID);
	}
	
	public String getsp500ParameterNameTxt(){
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, Market.MARKET_SP_500);
	}
	
	public String getftseParameterNameTxt(){
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, Market.MARKET_FTSE_ID);
	}
	
	public String getcacParameterNameTxt(){
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, Market.MARKET_CAC_ID);
	}
	
}
