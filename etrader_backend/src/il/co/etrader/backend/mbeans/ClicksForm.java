package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class ClicksForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -661691360639201555L;
	private Date from;
	private Date to;
	private ArrayList list;
	private long campaignId;
	private long currentWriterId;



	public ClicksForm() throws SQLException, ParseException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		from = gc.getTime();
		campaignId = 0;
		currentWriterId = Utils.getWriter().getWriter().getId();
		search();
	}

	public String search() throws SQLException, ParseException {
		//set defualt date for arabic partner
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDate = sdf.parse(enumDate);
			if (from.before(startDate)){
			   from = startDate;
			}
	    }
		list = AdminManager.searchClicks(from, to, campaignId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public int getListSize() {
		return list.size();
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ClicksForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "list = " + this.list + TAB
	        + "campaignId = " + this.campaignId + TAB
	        + " )";

	    return retValue;
	}
}
