package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class LimitsForm implements Serializable {

	private Limit limit;
	private ArrayList list;

	private int skinId;
	private long currencyId;

	public LimitsForm() throws SQLException{

		//	set currency filter to all currencies
		if (Utils.getWriter().isEtrader()) {
			currencyId = ConstantsBase.CURRENCY_ILS_ID;
			skinId = Skin.SKIN_ETRADER_INT;
		} else {
			currencyId = ConstantsBase.CURRENCY_ALL_ID;
			skinId = Skin.SKINS_ALL_VALUE;
		}

		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateInsertLimit() throws SQLException{

		boolean res = AdminManager.updateInsertLimit(limit);
		if (res == false) {
			return null;
		}
		search();
		return Constants.NAV_LIMITS;
	}

	/**
	 * add limit
	 * @return
	 * @throws SQLException
	 */
	public String bNavAddLimit() throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();
		limit = new Limit();
		limit.setWriterId(AdminManager.getWriterId());
		limit.setWriterName(context.getExternalContext().getRemoteUser());
		limit.setTimeCreated(new Date());
		limit.setTimeLastUpdate(new Date());

		if (Utils.getWriter().isEtrader()) {
			limit.setCurrencyId(ConstantsBase.CURRENCY_ILS_ID);
			limit.setSkinId(Skin.SKIN_ETRADER_INT);
		} else {
			limit.setCurrencyId(ConstantsBase.CURRENCY_USD_ID);
			limit.setSkinId(Skin.SKINS_DEFAULT);
		}

		return Constants.NAV_LIMIT;
	}

	public String bNavEditLimit() {
		limit.setTimeLastUpdate(new Date());
		return Constants.NAV_LIMIT;
	}

	public String bNavLimits() throws SQLException{
		search();
		return Constants.NAV_LIMITS;
	}

	public String search() throws SQLException{
		list = AdminManager.searchLimits(skinId, currencyId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public ArrayList getList() {
		return list;
	}

	public Limit getLimit() {
		return limit;
	}

	public void setLimit(Limit limit) {
		this.limit = limit;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "LimitsForm ( "
	        + super.toString() + TAB
	        + "limit = " + this.limit + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}



}
