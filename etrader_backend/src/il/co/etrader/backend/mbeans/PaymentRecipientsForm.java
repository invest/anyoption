package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingPaymentRecipient;
import il.co.etrader.util.CommonUtil;



public class PaymentRecipientsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String agentName;
	private ArrayList<MarketingPaymentRecipient> list;
	private MarketingPaymentRecipient paymentRecipient;

	public PaymentRecipientsForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingPaymentRecipients(agentName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert payment recipient
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		paymentRecipient.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdatePaymentRecipient(paymentRecipient);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_PAYMENT_RECIPIENTS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit payment recipient
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_PAYMENT_RECIPIENT;
	}

	/**
	 * Navigate to insert new payment recipient
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		paymentRecipient = new MarketingPaymentRecipient();
		paymentRecipient.setWriterId(AdminManager.getWriterId());
		paymentRecipient.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_PAYMENT_RECIPIENT;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the paymentRecipient
	 */
	public MarketingPaymentRecipient getPaymentRecipient() {
		return paymentRecipient;
	}

	/**
	 * @param paymentRecipient the paymentRecipient to set
	 */
	public void setPaymentRecipient(MarketingPaymentRecipient paymentRecipient) {
		this.paymentRecipient = paymentRecipient;
	}


}
