package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.model.DataModel;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUserHistory;

import il.co.etrader.backend.bl_managers.RewardsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.DataPage;
import il.co.etrader.util.PagedListDataModel;

/**
 * @author EranL
 * 
 */
public class RewardsUserHistoryForm implements Serializable {

	private static final long serialVersionUID = -778382955091247733L;

	private static final Logger logger = Logger.getLogger(RewardsUserHistoryForm.class);

	// paging variables
	private DataModel rewardUserHistory;
	private DataPage<RewardUserHistory> dataPage;
	private boolean needUpdate;
	private User user;
	//filters
	private Date from;
	private Date to;
	private long tierId;
	private long typeId;
	private String actionId; 
	private long userId;
	
	public RewardsUserHistoryForm() {
		getInit();
	}
	
	public String updateList() {
		needUpdate = true;
		return null;
	}
	
	public String getResetUser() {
		if (userId == 0 || userId != Utils.getUser().getId()) {
			userId = Utils.getUser().getId();
			updateList();
		}
		return Constants.EMPTY_STRING; 
	}
	
	public String getInit() {
		user = Utils.getUser();		
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();				
		gc.add(Calendar.YEAR, -1);		
		from = gc.getTime();
		needUpdate = true;  // for taking data from db
		return Constants.EMPTY_STRING;
	}

	/**
	 * Get data Page. Return DataPage object that represent page in the model.
	 * 
	 * @param startRow
	 *            the start row of this page relative to number of all rows in the model.
	 * @param pageSize
	 *            number of rows in the page.
	 * @return DataPage<RewardUserHistory> instance.
	 */
	private DataPage<RewardUserHistory> getDataPage(int startRow, int pageSize) {
		logger.debug("getDataPage - startRow: " + startRow + " pageSize: " + pageSize);
		if (null == dataPage || dataPage.getStartRow() != startRow || needUpdate == true) {
			logger.debug("loading reward user history page from db...");
			try {
				dataPage = RewardsManager.getRewardUserHistory(user.getId(), startRow, pageSize, from, to, tierId, typeId, actionId);
				needUpdate = false;
			} catch (Throwable t) {
				logger.error("Can't load the page.", t);
			}
		} else {
			logger.debug("Cached dataPage.");
		}
		return dataPage;
	}

	/**
	 * Get page size
	 * 
	 * @return page size for dataPage
	 */
	public int getPageSize() {
		int size = 0; // default
		if (null != dataPage && dataPage.getDatasetSize() > 0) {
			size = dataPage.getDatasetSize();
		}
		return size;
	}

	/**
	 * Get DataModel instance
	 * 
	 */
	public DataModel getDataModel() {
		logger.debug("getDataModel");

		if (null == rewardUserHistory || needUpdate == true) { // on creation
															   // and filtering
			rewardUserHistory = new LocalDataModel(10);
			if (needUpdate == true) { // set to first page
				CommonUtil.setTablesToFirstPage();
			}
		}

		return rewardUserHistory;
	}

	/**
	 * Get LocalDataModel class. This inner class extends from
	 * PagedListDataModel and implements the fetchPage function for getting
	 * page. This is done for paging issues, each page get his data from Db.
	 * 
	 */
	private class LocalDataModel extends PagedListDataModel<RewardUserHistory> {
		
		public LocalDataModel(int pageSize) {
			super(pageSize);
		}

		public DataPage<RewardUserHistory> fetchPage(int startRow, int pageSize) {
			// call enclosing managed bean method to fetch the data
			return getDataPage(startRow, pageSize);
		}
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the tierId
	 */
	public long getTierId() {
		return tierId;
	}

	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(long tierId) {
		this.tierId = tierId;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the actionId
	 */
	public String getActionId() {
		return actionId;
	}

	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
}
