/*
 * This class represents the form the xhtml page is bounded to.
 */
package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_vos.NewAccounts;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;

public class NewAccountsForm implements Serializable {
	private static final long serialVersionUID = 5664970920054075296L;

	private Date from;
	private Date to;
	private long campaignId;
	private ArrayList<NewAccounts> newAccountsList; //array for all vo's
	private int skinId;
	private ArrayList<SelectItem> depositsStatusSI;
	private int depositsStatus;
	private ArrayList<SelectItem> countriesSI;
	private long countryId;
	private long skinBusinessCaseId;
	private long writerId;
	private ArrayList<SelectItem> writerFilterList = new ArrayList<SelectItem>();
	private Date arabicStartDate;
	private int verifiedUsersFilter;
    private long currencyId;
    private String amountAbove;
    private String amountBelow;

	public NewAccountsForm() throws SQLException, ParseException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		//gc.set(Calendar.HOUR_OF_DAY, 0);
		//gc.set(Calendar.MINUTE, 0);
		//gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		gc.add(GregorianCalendar.DATE, -7); //set default start date to 1 week past from now
		from = gc.getTime();
		campaignId=0;

		//set skins filter to all writer skins
		setSkinId(Skin.SKINS_ALL_VALUE);
   		newAccountsList = null;
   		depositsStatus = 0;
   		verifiedUsersFilter = 0;

   		//For arabic partner writer we want users that created after the arabic partner starting date
	    long currentWriterId = Utils.getWriter().getWriter().getId();
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumerator = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			arabicStartDate = sdf.parse(enumerator);
	    }
   		search();
	}

	public void search() throws SQLException{
		newAccountsList = AdminManager.searchNewAccounts(from, to, campaignId, getSkinFilter(),depositsStatus, countryId, skinBusinessCaseId, writerId,
                arabicStartDate, verifiedUsersFilter, currencyId, amountAbove, amountBelow);
		HashMap<Long, String> h = new HashMap<Long, String>();
		for (NewAccounts n : newAccountsList) {
			h.put(new Long(n.getCountryId()), n.getCountry());
		}
		countriesSI = CommonUtil.hashMap2SortedArrayList(h);
		createWriterFilter();
		CommonUtil.setTablesToFirstPage();
	}

	public Date getFrom(){
		return from;
	}

	public Date getTo(){
		return to;
	}

	public ArrayList<NewAccounts> getNewAccountsList(){
		return newAccountsList;
	}

	public int getNewAccountsListSize(){
		return newAccountsList.size();
	}

	public void setNewAccounts(ArrayList<NewAccounts> newAccountsList){
		this.newAccountsList = newAccountsList;
	}

	public void setFrom(Date from){
		this.from = from;
	}

	public void setTo(Date to){
		this.to = to;
	}

	public long getCampaignId(){
		return campaignId;
	}

	public void setCampaignId(long campaignId){
		this.campaignId = campaignId;
	}

	/**
	 * this method return a String that represent all of the writers skins seperated by a comma
	 * @return
	 */
	public String getSkinFilter() {
		if (skinId == Skin.SKINS_ALL_VALUE) {
			return Utils.getWriter().getSkinsToString();
		}
		else {
			return String.valueOf(skinId);
		}
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public ArrayList<SelectItem> getDepositsStatusSI() {
		return depositsStatusSI;
	}

	public void setDepositsStatusSI(ArrayList<SelectItem> depositsStatusSI) {
		this.depositsStatusSI = depositsStatusSI;
	}

	public int getDepositsStatus() {
		return depositsStatus;
	}

	public void setDepositsStatus(int depositsStatus) {
		this.depositsStatus = depositsStatus;
	}

	/**
	 * @param newAccountsList the newAccountsList to set
	 */
	public void setNewAccountsList(ArrayList<NewAccounts> newAccountsList) {
		this.newAccountsList = newAccountsList;
	}

	/**
	 * @return the countriesSI
	 */
	public ArrayList<SelectItem> getCountriesSI() {
		return countriesSI;
	}

	/**
	 * @param countriesSI the countriesSI to set
	 */
	public void setCountriesSI(ArrayList<SelectItem> countriesSI) {
		this.countriesSI = countriesSI;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the skinBusinessCaseId
	 */
	public long getSkinBusinessCaseId() {
		return skinBusinessCaseId;
	}

	/**
	 * @param skinBusinessCaseId the skinBusinessCaseId to set
	 */
	public void setSkinBusinessCaseId(long skinBusinessCaseId) {
		this.skinBusinessCaseId = skinBusinessCaseId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the writerFilterList
	 */
	public ArrayList<SelectItem> getWriterFilterList() {
		return writerFilterList;
	}

	/**
	 * @param writerFilterList the writerFilterList to set
	 */
	public void setWriterFilterList(ArrayList<SelectItem> writerFilterList) {
		this.writerFilterList = writerFilterList;
	}

	/**
	 * Create writer filter SI from new accounts list
	 */
	private void createWriterFilter(){
		HashMap<Long, String> h = new HashMap<Long, String>();
		HashMap<Long, Writer> writersMap;
		writersMap = Utils.getWriter().getAllWritersMap();
		for (NewAccounts na :newAccountsList){
			h.put(new Long(na.getWriterId()), writersMap.get(Long.valueOf(na.getWriterId())).getUserName());
		}
		writerFilterList = CommonUtil.hashMap2SortedArrayList(h);
	}

	/**
	 * @return the isVerifiedUsers
	 */
	public int getVerifiedUsersFilter() {
		return verifiedUsersFilter;
	}

	/**
	 * @param isVerifiedUsers the isVerifiedUsers to set
	 */
	public void setVerifiedUsersFilter(int isVerifiedUsers) {
		this.verifiedUsersFilter = isVerifiedUsers;
	}

    /**
     * @return the currencyId
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the amountAbove
     */
    public String getAmountAbove() {
        return amountAbove;
    }

    /**
     * @param amountAbove the amountAbove to set
     */
    public void setAmountAbove(String amountAbove) {
        this.amountAbove = amountAbove;
    }

    /**
     * @return the amountBelow
     */
    public String getAmountBelow() {
        return amountBelow;
    }

    /**
     * @param amountBelow the amountBelow to set
     */
    public void setAmountBelow(String amountBelow) {
        this.amountBelow = amountBelow;
    }

}
