package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.util.CommonUtil;

public class CalcalistInvestorsForm implements Serializable {
	private static final long serialVersionUID = 73486659540567352L;

	private static final Logger logger = Logger.getLogger(CalcalistInvestorsForm.class);

	private ArrayList list;
	private Date from = null;
	private Date to = null;
	private String userId;
	private String userIdNum;

	public CalcalistInvestorsForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.DAY_OF_MONTH,1);
		gc.set(GregorianCalendar.MONTH,11);
		gc.set(GregorianCalendar.HOUR,0);
		gc.set(GregorianCalendar.MINUTE,0);
		gc.set(GregorianCalendar.SECOND,0);
		to = gc.getTime();
		gc.set(GregorianCalendar.DAY_OF_MONTH,17);
		gc.set(GregorianCalendar.MONTH,9);
		from = gc.getTime();
		updateList();
		gc.set(GregorianCalendar.DAY_OF_MONTH,18);
		from = gc.getTime(); // to set the calander
	}

	public int getListSize() {
		return list.size();
	}

	public String updateList() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(to);
		gc.add(GregorianCalendar.DAY_OF_MONTH,1);
		to = gc.getTime();
		list = UsersManager.getCalcalistUsers(userId,userIdNum,from,to);
		gc.add(GregorianCalendar.DAY_OF_MONTH,-1);
		to = gc.getTime();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userIdNum
	 */
	public String getUserIdNum() {
		return userIdNum;
	}

	/**
	 * @param userIdNum the userIdNum to set
	 */
	public void setUserIdNum(String userIdNum) {
		this.userIdNum = userIdNum;
	}
}
