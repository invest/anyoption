package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import il.co.etrader.backend.bl_managers.OptionPlusManager;
import il.co.etrader.backend.bl_vos.OptionPlusPriceMatrixItem;
import il.co.etrader.util.CommonUtil;


public class OptionPlusForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<OptionPlusPriceMatrixItem> list;
	private OptionPlusPriceMatrixItem item;
	private double newPrice;
	private long marketId;

	/**
	 * @throws SQLException
	 */
	public OptionPlusForm() throws SQLException{
		marketId = 552;
		updateList();
	}

	/**
	 * Update affiliates list
	 * @return
	 * @throws SQLException
	 */
	public String updateList() throws SQLException{
		list = OptionPlusManager.buildOptionPlusPriceMatrix(marketId);
		return null;
	}

	public String updatePrice() throws SQLException {
		OptionPlusManager.updatePrice(item.getId(), newPrice/100);
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("optionplus.updated", null),null);
		context.addMessage(null, fm);
        
		updateList();
		return null;
	}

	/**
	 * @return
	 */
	public int getListSize() {
		return list.size();
	}


	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the item
	 */
	public OptionPlusPriceMatrixItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(OptionPlusPriceMatrixItem item) {
		this.item = item;
	}

	/**
	 * @return the newPrice
	 */
	public double getNewPrice() {
		return newPrice;
	}

	/**
	 * @param newPrice the newPrice to set
	 */
	public void setNewPrice(double newPrice) {
		this.newPrice = newPrice;
	}

	public void updateMarketChange(ValueChangeEvent event) throws SQLException{
    	marketId = (Long)event.getNewValue();
    	updateList();
    }

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

}
