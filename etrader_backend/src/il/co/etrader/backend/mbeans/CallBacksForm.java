package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignmentInfo;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.CallBackReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class CallBacksForm implements Serializable {


    /**
	 *
	 */
	private static final long serialVersionUID = -7790426385911796212L;

	private static final Logger logger = Logger.getLogger(CallBacksForm.class);

	private ArrayList list;
	private Date from = null;
	private Date to = null;
	private Date fromLogin = null;
	private Date toLogin = null;
	private long skinId;
	private PopulationEntry populationEntry;
	private long writerId;
	private String username;
	private Long userId;
	private Long contactId;
	private String priorityId;
	private boolean assignFilter;
	private long writerFilter;
	private long salesType;
	private long userStatusId;
	private long userRankId;
	private long retentionTeamId;


	public CallBacksForm() throws SQLException{
		skinId = 0 ;
		username = null ;
		userId = null ;
		contactId = null;
		priorityId = null ;
		writerId = 0 ;
		assignFilter = true;
		writerFilter = 0 ;
		userStatusId = 0;
		userRankId = 0;
		retentionTeamId = ConstantsBase.ALL_FILTER_ID;

		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		gc.add(GregorianCalendar.YEAR,-2);
		from = gc.getTime();
		salesType = 1;
		updateList();
//		gc.set(GregorianCalendar.DAY_OF_MONTH,18);
//		from = gc.getTime(); // to set the calander

	}

	public String lockEntry() throws SQLException,PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, false);
		if (res){
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;
	}

	public String unLockEntry() throws SQLException,PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		updateList();
		return null;
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 * @return
	 * @throws SQLException
	 */
	public String loadEntry() throws SQLException {
		return PopulationEntriesManager.loadEntry(populationEntry);
	}

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	public int getListSize() {
		return list.size();
	}

	public String updateList() throws SQLException{

//		GregorianCalendar gc = new GregorianCalendar();
//		gc.setTime(to);
//		gc.add(GregorianCalendar.DAY_OF_MONTH,1);
//		to = gc.getTime();

		long userIdVal =0;
		if (null != userId){
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)){
			username = null;
		}

		long contactIdVal =0;
		if (null != contactId){
			contactIdVal = contactId.longValue();
		}

		FacesContext context=FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.isRetentionSelected()) {
			writerId = Utils.getWriter().getWriter().getId();
		}
		else {
			writerId= writerFilter;
		}

		Writer w = Utils.getWriter().getWriter();
		if (w.getSales_type() == Constants.SALES_TYPE_CONVERSION) {
			salesType = Constants.POPULATION_TYPE_ID_CONVERSION;
		} else if (w.getSales_type() == Constants.SALES_TYPE_RETENTION) {
			salesType = Constants.POPULATION_TYPE_ID_RETENTION;
		}

		list = PopulationEntriesManager.getPopulationEntriesByEntryType(null,skinId, writerId,
				ConstantsBase.POP_ENTRY_TYPE_CALLBACK,from,to,fromLogin,toLogin,
				0,assignFilter, userIdVal, username, contactIdVal, priorityId, wr, 0, 0, null, userStatusId, "0", false, false, 0, salesType, userRankId, retentionTeamId, null, null);

//		gc.add(GregorianCalendar.DAY_OF_MONTH,-1);
//		to = gc.getTime();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the priorityId
	 */
	public String getPriorityId() {
		return priorityId;
	}

	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the assignFilter
	 */
	public boolean isAssignFilter() {
		return assignFilter;
	}

	/**
	 * @param assignFilter the assignFilter to set
	 */
	public void setAssignFilter(boolean assignFilter) {
		this.assignFilter = assignFilter;
	}

	/**
	 * @return the writerFilter
	 */
	public long getWriterFilter() {
		return writerFilter;
	}

	/**
	 * @param writerFilter the writerFilter to set
	 */
	public void setWriterFilter(long writerFilter) {
		this.writerFilter = writerFilter;
	}

	/**
	 * Assign action
	 * in case the entry locked, unlock done before the assign
	 * @return
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
    public String assign() throws PopulationHandlersException, SQLException { 
    	long lastAssignWriterId = populationEntry.getAssignWriterId();
    	FacesContext context = FacesContext.getCurrentInstance();
    	WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
    	long writerId = wr.getWriter().getId();
    	if (populationEntry.getPopulationSalesTypeId() == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT ||
    			populationEntry.getPopulationSalesTypeId() == PopulationsManagerBase.SALES_TYPE_RETENTION) { //assign limit for retention writers    		
    		long tempAssignWriterId = populationEntry.getTempAssignWriterId();
    		PopulationUsersAssignmentInfo puai = PopulationEntriesManager.isAssignmentOk(tempAssignWriterId, populationEntry.getSkinId(), populationEntry.getUserRankId(), populationEntry.getUserStatusId()); 
    		if (puai.isAssignmentOk()) {
    			populationEntry.updateAssignedWriter();
    			PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, false, false, writerId);	
    		} else {
    			String msg = CommonUtil.getMessage("retention.entry.multi.assign.not.success", null);
    			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
    			context.addMessage(null, fm);
    			if ( tempAssignWriterId > 0 && puai.isReachedTotalLimit()) {
    				String[] params = new String[1];
    				Writer w = wr.getAllWritersMap().get(tempAssignWriterId);
    				params[0] = w.getUserName();
    				msg = CommonUtil.getMessage("retention.entry.assign.not.success.limit", params);
        			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
        			context.addMessage(null, fm);
    			}
    		}
    	} else {
    		populationEntry.updateAssignedWriter();
    		PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, false, false, writerId);
    	}
    	updateList();
    	return Constants.NAV_RETENTION_CALLBACKS;
    }

    /**
     * Like assign action
     */
    public String reAssign() throws PopulationHandlersException, SQLException {
    	assign();
    	return null;
    }

    /**
	 * Cancel assign action
	 * in case the entry locked, unlock done before the cancel
     * @return
     * @throws PopulationHandlersException
     * @throws SQLException
     */
    public String cancelAssign() throws PopulationHandlersException, SQLException {
    	PopulationEntriesManager.cancelAssign(populationEntry, true, false);
    	updateList();
    	return Constants.NAV_RETENTION_CALLBACKS;
    }

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String exportCallBacksResultToMail() throws IOException, SQLException, ParseException {
		String reportName = "callBacks_report_";
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		logger.info("Process " + reportName);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(from) + "_" + fmt.format(to) + ".csv";

		long currentWriterId = Utils.getWriter().getWriter().getId();

		// set defualt date for arabic partner
		if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
			String enumDate = CommonUtil.getEnum(
					Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
					Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			;
			if (from.before(startDateAr)) {
				from = startDateAr;
			}
		}

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter()
				.getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.enteries.report.invalid.mail", null),null);
			context.addMessage(null, fm);
			return null;
		}

		CallBackReportSender sender = new CallBackReportSender(this, filename, wr, user.isRetentionSelected());
		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				filename + " " + CommonUtil.getMessage("enteries.report.processing.mail", null),null);
		context.addMessage(null, fm);

		return null;
	}

	/**
	 * @return the salesType
	 */
	public long getSalesType() {
		return salesType;
	}
	/**
	 * @param salesType the salesType to set
	 */
	public void setSalesType(long salesType) {
		this.salesType = salesType;
	}

	/**
	 * @return the userStatusId
	 */
	public long getUserStatusId() {
		return userStatusId;
	}

	/**
	 * @param userStatusId the userStatusId to set
	 */
	public void setUserStatusId(long userStatusId) {
		this.userStatusId = userStatusId;
	}

	/**
	 * @return the userRankId
	 */
	public long getUserRankId() {
		return userRankId;
	}

	/**
	 * @param userRankId the userRankId to set
	 */
	public void setUserRankId(long userRankId) {
		this.userRankId = userRankId;
	}

	public Date getFromLogin() {
		return fromLogin;
	}

	public void setFromLogin(Date fromLogin) {
		this.fromLogin = fromLogin;
	}

	public Date getToLogin() {
		return toLogin;
	}

	public void setToLogin(Date toLogin) {
		this.toLogin = toLogin;
	}

	/**
	 * @return the retentionTeamId
	 */
	public long getRetentionTeamId() {
		return retentionTeamId;
	}

	/**
	 * @param retentionTeamId the retentionTeamId to set
	 */
	public void setRetentionTeamId(long retentionTeamId) {
		this.retentionTeamId = retentionTeamId;
	}
}
