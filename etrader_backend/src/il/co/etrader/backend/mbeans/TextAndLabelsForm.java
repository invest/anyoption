package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.configuration.ConfigurationException;

import il.co.etrader.backend.cms.CmsDataObjects;
import il.co.etrader.backend.cms.CmsGuiBindingsManager;
import il.co.etrader.backend.cms.common.PropertyTypeEnum;
import il.co.etrader.util.CommonUtil;

@ManagedBean(name="textAndLabels")
@SessionScoped
public class TextAndLabelsForm  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5232293545141918912L;
	private String page;
	private String  key;
	private String  lang;
	private String image;
	private int skinId;
	private ArrayList<SelectItem> pageList;
	private ArrayList<SelectItem> typeList;
	private ArrayList<SelectItem> keyList;
	private ArrayList<SelectItem> langList;
	private String rowsByMenu ="30";
	private CmsDataObjects cmsData;
	private ArrayList<CmsDataObjects> objectsList;	
	private ArrayList<CmsDataObjects> objectsListEdit;
	CmsGuiBindingsManager manager;
	private String cvsMsg;
	private boolean showSearch;
	private boolean showEdit;
	private boolean showCommit;
	private boolean showEditTable;
	private PropertyTypeEnum type;
	private String newValue;
	private boolean afterPreview;
	private boolean showPreview;
	private boolean showTableForEdit;
	private String popUp;
	private int rows;
	
	
	public TextAndLabelsForm() throws SQLException{
	  manager = new CmsGuiBindingsManager();
	  setShowSearch(false);
	}
	
	
	public void Edit(){
		
		 boolean edit  = manager.edit(cmsData);
		 if (!edit){
			 setCvsMsg(manager.getCvsMessage());
		 }else{
		 setCvsMsg(manager.getCvsMessage());
		 setShowSearch(true);
		 setShowEdit(false);
		 setShowCommit(true);
		 setAfterPreview(false);
		 setShowPreview(true);
		 setShowEditTable(false);
		 setShowTableForEdit(true);
		 }
   		 refresh();
   		 objectsListEdit = new ArrayList<CmsDataObjects>();
		 objectsListEdit.add(cmsData);
   		 setNewValue(cmsData.getValue());
	}
	
	
	public String Search(){
		
		setObjectsList(manager.search(skinId, page,getType(),key));
		rows = objectsList.size();
		setCvsMsg("");
		setShowEdit(true);
		setShowEditTable(true);
		setShowTableForEdit(false);
		CommonUtil.setTablesToFirstPage();
		
		return null;		
	}
	
	public String Commit(){
		
		setShowSearch(false);
		setShowCommit(false);
		manager.commit(cmsData);
		setCvsMsg(manager.getCvsMessage());
		setShowEdit(true);
		setShowEditTable(true);
		setShowTableForEdit(false);
		
		return null;
		
  	}
	
   public String Rollback(){
	   
	    setShowSearch(false);
		setShowCommit(false);
		manager.rollback(cmsData);
		setCvsMsg(manager.getCvsMessage());
		setObjectsList(manager.search(skinId, page,getType(),key));
		setShowEdit(true);
		setShowEditTable(true);
		setShowTableForEdit(false);
		
		return null;
	}

   public String Preview() throws ConfigurationException, IOException{
	    setShowSearch(true);
		setShowEdit(false);
		setShowEditTable(false);
		setShowCommit(true);
		setAfterPreview(true);
		setShowPreview(false);
		cmsData.setValue(newValue);
		manager.preview(cmsData);
		setCvsMsg(manager.getCvsMessage());
		return null;
    }
   
	private void refresh(){
		setObjectsList(manager.search(skinId, page,getType(),key));
		ArrayList<CmsDataObjects> tmpList=getObjectsList();
		for(CmsDataObjects listItem : tmpList ){
			if(listItem.getKey().equals(cmsData.getKey()) && listItem.getPage().equals(cmsData.getPage())
					&& (listItem.getSkin()==cmsData.getSkin()) && (listItem.getType()==cmsData.getType())){
				cmsData=listItem;
			}
			
		}
		
	}

	public ArrayList<CmsDataObjects> getObjectsList() {
		return objectsList;
	}


	public void setObjectsList(ArrayList<CmsDataObjects> objectsList) {
		this.objectsList = objectsList;
	}


	public String getRowsByMenu() {
		return rowsByMenu;
	}


	public void setRowsByMenu(String rowsByMenu) {
		this.rowsByMenu = rowsByMenu;
	}


	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public PropertyTypeEnum getType() {
		return type;
	}

	public void setType(PropertyTypeEnum type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}



	public ArrayList<SelectItem> getPageList() {
		
		if(  pageList!=null){
		     pageList.clear();
		}
		     pageList = manager.getPageList();
		return pageList;
	}



	public void setPageList(ArrayList<SelectItem> pageList) {
		        this.pageList = pageList;
	}


	@SuppressWarnings("static-access")
	public ArrayList<SelectItem> getTypeList() {
		typeList = new ArrayList<SelectItem>(); 
		for (PropertyTypeEnum type : PropertyTypeEnum.values()){
			
			if( (!type.getLabel().equals(type.ALT_TEXT.getLabel())) && (!type.getLabel().equals(type.IMAGE.getLabel()))){
			
				typeList.add(new SelectItem(type, type.getLabel()));
			}
		}
		return typeList;
	}


	public void setTypeList(ArrayList<SelectItem> typeList) {
		        this.typeList = typeList;
	}


	public ArrayList<SelectItem> getKeyList() {
		if( keyList !=null){
		    keyList.clear();
		}
		    keyList = manager.getKeyList(false);
		return  keyList;
	}



	public void setKeyList(ArrayList<SelectItem> keyList) {
		        this.keyList = keyList;
	}



	public ArrayList<SelectItem> getLangList() {
		/*if( langList !=null){
		    langList.clear();
		}*/
		    langList = manager.getSkinList();
		return 
				langList;
	}



	public void setLangList(ArrayList<SelectItem> langList) {
		        this.langList = langList;
	}


	public CmsDataObjects getCmsData() {
		return cmsData;
	}


	public void setCmsData(CmsDataObjects cmsData) {
		this.cmsData = cmsData;
	}


	public String getCvsMsg() {
		return cvsMsg;
	}


	public void setCvsMsg(String cvsMsg) {
		this.cvsMsg = cvsMsg;
	}


	public boolean isShowSearch() {
		return showSearch;
	}


	public void setShowSearch(boolean showSearch) {
		this.showSearch = showSearch;
	}


	public boolean isShowEdit() {
		return showEdit;
	}


	public void setShowEdit(boolean showEdit) {
		this.showEdit = showEdit;
	}


	public boolean isShowCommit() {
		return showCommit;
	}


	public void setShowCommit(boolean showCommit) {
		this.showCommit = showCommit;
	}


	public boolean isShowEditTable() {
		return showEditTable;
	}


	public void setShowEditTable(boolean showEditTable) {
		this.showEditTable = showEditTable;
	}


	public String getNewValue() {
		return newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}


	public boolean isAfterPreview() {
		return afterPreview;
	}


	public void setAfterPreview(boolean afterPreview) {
		this.afterPreview = afterPreview;
	}


	public int getSkinId() {
		return skinId;
	}


	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}


	public boolean isShowPreview() {
		return showPreview;
	}

	public void setShowPreview(boolean showPreview) {
		this.showPreview = showPreview;
	}

	public String getPopUp() {
		return popUp;
	}

	public void setPopUp(String popUp) {
		this.popUp = popUp;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}


	public ArrayList<CmsDataObjects> getObjectsListEdit() {
		return objectsListEdit;
	}


	public void setObjectsListEdit(ArrayList<CmsDataObjects> objectsListEdit) {
		this.objectsListEdit = objectsListEdit;
	}


	public boolean isShowTableForEdit() {
		return showTableForEdit;
	}


	public void setShowTableForEdit(boolean showTableForEdit) {
		this.showTableForEdit = showTableForEdit;
	}
	
	
	
}
