package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.OpportunityTemplate;

public class OpportunityOneTouchForm implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4619819855316206578L;

	private OpportunityTemplate template;
	private Opportunity opportunity;

	private Date firstInvDate;
	private Date estClosingDate;
	private Date lastInvDate;

	//private String oneTouchMsg;

	private boolean isPublished;
	private String upDown;


	public OpportunityOneTouchForm() throws SQLException{

		upDown = "";

		firstInvDate = new Date();
		estClosingDate = new Date();
		lastInvDate = new Date();

		initValues();
	}

	/**
	 * insert one touch oppurtunity to Db.
	 * @throws SQLException
	 */
	public String insert()  {
		// This method is no longer used.
		
		// seting dates
		Calendar firstInvCal = Calendar.getInstance();
		firstInvCal.setTime(firstInvDate);
		Calendar estClosingCal = Calendar.getInstance();
		estClosingCal.setTime(estClosingDate);
		Calendar lastInvalCal = Calendar.getInstance();
		lastInvalCal.setTime(lastInvDate);

		try {
		formatMessageOpp();
		InvestmentsManager.insertOneTouchOpportunity(opportunity, firstInvCal, estClosingCal,
				lastInvalCal,template.getTimeZone(),template.getTimeFirstInvest(),
				template.getTimeEstClosing(), template.getTimeLastInvest(), Skin.SKIN_ETRADER);

		} catch (Exception e) {
			return Constants.NAV_INSERT_ONE_TOUCH_FAILED;
		}

		return Constants.NAV_INSERT_ONE_TOUCH_SUCCEED;
	}

	/**
	 * initizlize fields
	 * @throws SQLException
	 */
	public void initValues() throws SQLException{

		// set opportunity fields
		opportunity = new Opportunity();
		opportunity.setScheduled(Opportunity.SCHEDULED_WEEKLY);
		opportunity.setWriterId(AdminManager.getWriterId());
		opportunity.setOpportunityTypeId(Opportunity.TYPE_ONE_TOUCH);

		//	set opportunity template fields
		template = new OpportunityTemplate();
		template.setTimeFirstInvest("00:00");
		template.setTimeEstClosing("00:00");
		template.setTimeLastInvest("00:00");

	}


	/**
	 * set the oneTouchMsg field.
	 * message format : onetouch.market display name.up/down
	 * @throws SQLException
	 */
	public void formatMessageOpp() throws SQLException {

		String displayName = AdminManager.getDisplayNameById(opportunity.getMarketId());
		String msg = "onetouch." + displayName + ".";
		if ( opportunity.getIsOneTouchUp() ) {
			msg += "up";
		}
		else {  // down
			msg += "down";
		}
		opportunity.setOneTouchMsg(msg);

	}

	public OpportunityTemplate getTemplate() {
		return template;
	}

	public void setTemplate(OpportunityTemplate template) {
		this.template = template;
	}

	/**
	 * @return the firstInv
	 */
	public Date getFirstInvDate() {
		return firstInvDate;
	}

	/**
	 * @param firstInv the firstInv to set
	 */
	public void setFirstInvDate(Date firstInvDate) {
		this.firstInvDate = firstInvDate;
	}

	/**
	 * @return the estClosingDate
	 */
	public Date getEstClosingDate() {
		return estClosingDate;
	}

	/**
	 * @param estClosingDate the estClosingDate to set
	 */
	public void setEstClosingDate(Date estClosingDate) {
		this.estClosingDate = estClosingDate;
	}

	/**
	 * @return the lastInvDate
	 */
	public Date getLastInvDate() {
		return lastInvDate;
	}

	/**
	 * @param lastInvDate the lastInvDate to set
	 */
	public void setLastInvDate(Date lastInvDate) {
		this.lastInvDate = lastInvDate;
	}

	/**
	 * @return the oppurtunity
	 */
	public Opportunity getOpportunity() {
		return opportunity;
	}

	/**
	 * @param oppurtunity the oppurtunity to set
	 */
	public void setOppurtunity(Opportunity opportunity) {
		this.opportunity = opportunity;
	}

	/**
	 * @return the isPublished
	 */
	public boolean getIsPublished() {
		return isPublished;
	}

	/**
	 * @param isPublished the isPublished to set
	 */
	public void setIsPublished(boolean isPublished) {
		this.isPublished = isPublished;
		if ( isPublished ) {
			opportunity.setIsPublished(Opportunity.PUBLISHED_YES);
		}
		else {
			opportunity.setIsPublished(Opportunity.PUBLISHED_NO);
		}
	}

	/**
	 * @return the upDown
	 */
	public String getUpDown() {
		return upDown;
	}

	/**
	 * @param upDown the upDown to set
	 */
	public void setUpDown(String upDown) {
		this.upDown = upDown;
		// set the opportunity
		this.opportunity.setOneTouchUpDown(Long.valueOf(upDown));
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OpportunityOneTouchForm ( "
	        + super.toString() + TAB
	        + "opportunity = " + opportunity.toString() + TAB
	        + " )";

	    return retValue;
	}



}
