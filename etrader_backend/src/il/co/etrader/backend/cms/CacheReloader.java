package il.co.etrader.backend.cms;

import com.anyoption.common.beans.JmxServerConfig;

public interface CacheReloader {
	public void clearProdEnvCache(String application);
	public void clearTestEnvCache(String application);
	public boolean initAssetIndexMarketsCache(String application, JmxServerConfig jmxServerConfig);
}
