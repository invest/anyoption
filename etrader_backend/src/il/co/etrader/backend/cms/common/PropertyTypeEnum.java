/**
 * 
 */
package il.co.etrader.backend.cms.common;

import java.util.HashMap;
import java.util.Map;


/**
 * This enumeration holds all possible CMS property types.
 * 
 * @author pavelhe
 *
 */
public enum PropertyTypeEnum {
    ALL("all", "All"),
    TEXT("text", "Text"),
    LABEL("label", "Label"),
    METADATA("metadata", "Metadata"),
    ALT_TEXT("alt", "Alt Text"),
    IMAGE("image", "Image");
    
    private static Map<String, PropertyTypeEnum> idToEnum;
    
    static {
        idToEnum = new HashMap<String, PropertyTypeEnum>();
        for (PropertyTypeEnum value : PropertyTypeEnum.values()) {
            idToEnum.put(value.getId(), value);
        }
    }
    
    private String id;
    private String label;

    PropertyTypeEnum(String id, String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }
    
   /**
     * @param id
     * @return the enumeration member that corresponds to the given ID or <code>null</code> if there is no such member.
     */
    public static PropertyTypeEnum getEnumById(String id) {
        return idToEnum.get(id);
    }
    
}
