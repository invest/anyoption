/**
 * 
 */
package il.co.etrader.backend.cms.common;


/**
 * This bean should hold data that can be used by all parts of the CVS module.
 * 
 * @author pavelhe
 *
 */
public class CommonCMSBean {
	
    /**
     * represents the possible stages during the edit of a resource in the cms module
     */
    public static enum CmsStage {
    	CHECKOUT, EDITORS, EDIT, UNEDIT, PREVIEW, END
    }
	
    private String cmsCheckoutDir;
    
    private String cvsUserName;
    private String cvsPassword;
    
    private CmsStage	currentCmsImageStage;
    private String		currentCvsImagePath;
    
    private CmsStage	currentCmsPropertyStage;
    private String		currentCvsPropertyPath;
    

    /**
     * @return the cmsCheckoutDir
     */
    public String getCmsCheckoutDir() {
        return cmsCheckoutDir;
    }

    /**
     * @return a {@link String} that represents the absolute checkout path
     *         for the current session. 
     */
    public void setCmsCheckoutDir(String cmsCheckoutDir) {
        this.cmsCheckoutDir = cmsCheckoutDir;
    }

    /**
     * @return the cvsUserName
     */
    public String getCvsUserName() {
        return cvsUserName;
    }

    /**
     * @param cvsUserName the cvsUserName to set
     */
    public void setCvsUserName(String cvsUserName) {
        this.cvsUserName = cvsUserName;
    }

    /**
     * @return the cvsPassword
     */
    public String getCvsPassword() {
        return cvsPassword;
    }

    /**
     * @param cvsPassword the cvsPassword to set
     */
    public void setCvsPassword(String cvsPassword) {
        this.cvsPassword = cvsPassword;
    }

	public String getCurrentCvsImagePath() {
		return currentCvsImagePath;
	}

	public void setCurrentCvsImagePath(String currentCvsImagePath) {
		this.currentCvsImagePath = currentCvsImagePath;
	}

	public String getCurrentCvsPropertyPath() {
		return currentCvsPropertyPath;
	}

	public void setCurrentCvsPropertyPath(String currentCvsPropertyPath) {
		this.currentCvsPropertyPath = currentCvsPropertyPath;
	}

	public CmsStage getCurrentCmsImageStage() {
		return currentCmsImageStage;
	}

	public void setCurrentCmsImageStage(CmsStage currentCmsImageStage) {
		this.currentCmsImageStage = currentCmsImageStage;
	}

	public CmsStage getCurrentCmsPropertyStage() {
		return currentCmsPropertyStage;
	}

	public void setCurrentCmsPropertyStage(CmsStage currentCmsPropertyStage) {
		this.currentCmsPropertyStage = currentCmsPropertyStage;
	}

}
