package il.co.etrader.backend.cms.common;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * This class should have only static utility messages specific for the CMS module.
 * 
 * @author pavelhe
 *
 */
public class CMSUtil {

    
    /**
     * @return the session ID of the current session, taken from {@link FacesContext}.
     */
    public static String getSessionId() {
        return ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getId();
    }
    
    /**
     * @param elExpression Expression Language expression for accessing the managed bean
     * @param clazz the {@link Class} to cast the managed bean.
     * @return the managed bean instance taken from {@link FacesContext}
     */
    public static <T> T getManagedBean(String elExpression, Class<T> clazz) {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getApplication().evaluateExpressionGet(context, elExpression, clazz);
    }
    
    /**
     * @param objectName the {@link String} key of the object in session map.
     * @return the object taken form the session map
     */
    public static Object getSessionObject(String objectName) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(objectName);
    }
    
    /**
     * Puts an object in the session map.
     * 
     * @param objectName the {@link String} that will be used as key in session map.
     * @param obj
     */
    public static void putSessionObject(String objectName, Object obj) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(objectName, obj);
    }
    
}
