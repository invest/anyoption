package il.co.etrader.backend.cms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.akamai.purge.api.PurgeException;
import com.akamai.purge.api.PurgeOptions;
import com.akamai.purge.api.PurgeOptions.Action;
import com.akamai.purge.api.PurgeOptions.Domain;
import com.akamai.purge.api.PurgeOptions.PurgeType;
import com.akamai.purge.api.PurgeRequest;
import com.akamai.purge.api.PurgeResult;
import com.akamai.purge.api.PurgeResult.ResultType;

public class AkamaiImpl implements Akamai, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 277556560180049869L;
	private static Logger log = Logger.getLogger(AkamaiImpl.class);
    FacesContext context = FacesContext.getCurrentInstance();
    private int retryLimit = Integer.parseInt(context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.AkamaiImpl.retryLimit"));
    private int MinutesBtwnRetries = Integer.parseInt(context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.AkamaiImpl.Minutes"));
    private int retries = 0;
    private List<String> email = new ArrayList<String>();
    private List<String> file = new ArrayList<String>();
    private String userName = null;
    private String passWord = null;
    private String get_message = null;
    private int get_code = 0;

    public String getGet_message() {
        return get_message;
    }
    
    @Override
    public int getGet_code() {
        return get_code;
    }

    @Override
    public void refresh(String baseDir, String destination) {
        
        email.add(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.AkamaiImpl.email"));
        userName = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.AkamaiImpl.username");
        passWord = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.AkamaiImpl.password");
        file.add(baseDir + destination);
        PurgeOptions purgeOptions = new PurgeOptions(email, PurgeType.URI, Action.REMOVE, Domain.DEFAULT);
        PurgeRequest request = new PurgeRequest(purgeOptions);
        PurgeResult result = null;
        log.debug("Starting Akamai");
        do {
            try {
                result = request.send(userName, passWord, file);

                log.debug("Starting Akamai try " + retries);
                if (result.getResult() == ResultType.QUEUE_LIMIT_REACHED) {
                    Thread.sleep(MinutesBtwnRetries * 60 * 1000);
                    retries++;
                    continue;
                } else {
                    retries = retryLimit;
                }
            } catch (PurgeException e) {
                log.error("error open connection: " + e.toString());
                this.get_message = "error check log";
            } catch (InterruptedException e1) {
                log.error("error open connection: " + e1.toString());
                this.get_message = "error check log";
            }
        } while (retries < retryLimit);
        log.debug("End Akamai");
        this.get_message = this.get_message + "\nResult Code: " + result.getResultCode();
        this.get_code = result.getResultCode();
        this.get_message = this.get_message + "\nResult Msg: " + result.getResultMessage();
        try {
            if (result.getResult() == ResultType.INVALID_URI_FORMAT || result.getResult() == ResultType.NOT_AUTHORIZED_FOR_THIS_URI) {
                this.get_message = this.get_message + "\nerror check log";
            }
        } catch (PurgeException e) {
            log.error("error open connection: " + e.toString());
            this.get_message = this.get_message + "\nerror check log";
        }
        this.get_message = this.get_message + "\nSession ID: " + result.getSessionID();
        this.get_message = this.get_message + "\nestTime: " + result.getEstimatedTimeToCompletionInSeconds();
    }
}
