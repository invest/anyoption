package il.co.etrader.backend.cms;

import java.util.ArrayList;

public interface Cvs {

	public void checkoutDir(String baseDir, String source, String userName, String password);
	
	public void checkoutFile(String baseDir, String source, String userName, String password);
	
	public String editors(String source, String userName, String password);
	
	public void edit(String source, String userName, String password);
	
	public void commit(String baseDir, String source, String userName, String password);
	
	public void unedit(String source, String userName, String password);

	public String getGet_message();

    public void add(String baseDir, ArrayList<String> fileNames, String userName, String password, boolean isBinary);	
}
