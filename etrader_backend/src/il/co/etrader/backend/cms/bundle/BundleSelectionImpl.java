/**
 * 
 */
package il.co.etrader.backend.cms.bundle;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.cms.BundleSelection;
import il.co.etrader.backend.cms.CMSConstants;
import il.co.etrader.backend.cms.CmsDataObjects;
import il.co.etrader.backend.cms.Cvs;
import il.co.etrader.backend.cms.common.CMSUtil;
import il.co.etrader.backend.cms.common.CommonCMSBean;
import il.co.etrader.backend.cms.common.CommonCMSBean.CmsStage;
import il.co.etrader.backend.cms.common.PropertyTypeEnum;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

/**
 * The implementation of {@link BundleSelection} that uses Apache Commons {@link PropertiesConfiguration} to load
 * all properties files for the active skins.
 * 
 * @author pavelhe
 *
 */
public class BundleSelectionImpl implements BundleSelection, Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2212110710277354633L;

	private static final Logger logger = Logger.getLogger(BundleSelectionImpl.class);
    
    private static final String CVS_CHECKOUT_DIR = "etrader_web/res_CMS/";
    private static final String FILE_ENCODING = "UTF-8";
    private static final String CMS_PREFIX = "CMS";
    private static final String PAGE_AND_KEY_ID_ALL = "-1";
    private static final int SKIN_ID_ALL = -1;
    
    private boolean loaded = false;
    private Map<Integer, SkinPropertiesWrapper> skinIdToWrapper;
    private Map<String, SkinPropertiesWrapper> filePathToWrapper;
    private Map<Integer, String> skinIdToImagesFolder;
    Set<String> pageSet;
    private ArrayList<SelectItem> skinList;
    private ArrayList<SelectItem> pageList;
    private ArrayList<SelectItem> altTextKeyList;
    private ArrayList<SelectItem> nonAltTextKeyList;
    private CommonCMSBean commonBean;
    
    private boolean CmsUserMoreThanOne;
    private String CmsUsernameCountReset;
    private String CmsUsernameCountResetStatus;
    
    @Override
    public ArrayList<CmsDataObjects> getList(int skinId,
                                        String page,
                                        PropertyTypeEnum type,
                                        String key) {
        /*
         * Adapting search criteria for search algorithm.
         */
        return getList((skinId == SKIN_ID_ALL ? skinIdToWrapper.values() : Arrays.asList(skinIdToWrapper.get(skinId))),
                            (page == null || page.equals(PAGE_AND_KEY_ID_ALL) ? pageSet : Arrays.asList(page)),
                            (type == null || type == PropertyTypeEnum.ALL
                                ? EnumSet.of(PropertyTypeEnum.TEXT, PropertyTypeEnum.LABEL, PropertyTypeEnum.METADATA)
                                : EnumSet.of(type)),
                            key);
    }
    
    /**
     * Searches for all properties that comply with the given criteria.
     * 
     * @param wrappers collection of skin wrappers. The search should go through all skins in the collection. 
     * @param pages collection of pages that the search should go through
     * @param types collection of property types that the search should go through
     * @param key the key that should be looked for or "-1" if all keys should be searched
     * @return list of CmsDataObjects. Each member represents a single property of a skin.
     */
    private ArrayList<CmsDataObjects> getList(Collection<SkinPropertiesWrapper> wrappers,
                                                Collection<String> pages,
                                                Collection<PropertyTypeEnum> types,
                                                String key) {
        ArrayList<CmsDataObjects> list = new ArrayList<CmsDataObjects>();
        Configuration subPageConf;
        for (SkinPropertiesWrapper wrapper : wrappers) {
            for (String page : pages) {
                subPageConf = wrapper.getCmsDecoratedConfiguration().subset(page);
                Configuration subTypeConf;
                for (PropertyTypeEnum type : types) {
                    subTypeConf = subPageConf.subset(type.getId());
                    if (key == null || key.equals(PAGE_AND_KEY_ID_ALL)) {
                        Iterator<String> allKeys = subTypeConf.getKeys();
                        String keyItem;
                        while (allKeys.hasNext()) {
                            keyItem = allKeys.next();
                            list.add(new CmsDataObjects(page,
                                                        type,
                                                        wrapper.getId(),
                                                        wrapper.getName(),
                                                        keyItem,
                                                        subTypeConf.getString(keyItem)));
                        }
                    } else if (subTypeConf.containsKey(key)) {
                        list.add(new CmsDataObjects(page,
                                                    type,
                                                    wrapper.getId(),
                                                    wrapper.getName(),
                                                    key,
                                                    subTypeConf.getString(key)));
                    }
                }
            }
        }
        
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ArrayList<SelectItem> getSkinList() {
        return (ArrayList<SelectItem>) skinList.clone();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ArrayList<SelectItem> getPageList() {
        return (ArrayList<SelectItem>) pageList.clone();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ArrayList<SelectItem> getKeyList(boolean onlyAltKeys) {
        return (ArrayList<SelectItem>) (onlyAltKeys ? altTextKeyList.clone() : nonAltTextKeyList.clone());
    }

    @Override
    public void setValue(int skinId,
                            String page,
                            PropertyTypeEnum type,
                            String key,
                            String value) throws ConfigurationException, IOException {
        SkinPropertiesWrapper wrapper = skinIdToWrapper.get(skinId);
        StringBuilder builder = new StringBuilder();
        builder.append(CMS_PREFIX)
                .append(".")
                .append(page)
                .append(".")
                .append(type.getId())
                .append(".")
                .append(key);
        /*
         * It is very important to set the property with the PropertiesConfiguration. If using the decorated subset
         * configuration, the edited property will be put at the end of the file.
         */
        wrapper.getPropertiesConfiguration().setProperty(builder.toString(), value);
        
        OutputStream out = new FileOutputStream(wrapper.getConfigurationFilePath());
        wrapper.getPropertiesConfiguration().save(out, FILE_ENCODING);
        out.close();
    }

    @Override
    public synchronized void load() throws ConfigurationException, IOException, SQLException {
        logger.debug("Loading bundle selection manager");
        if (loaded) {
            logger.debug("Bundle selection manager already loaded, no need to load it again");
            return;
        }
        
        commonBean = buildCommonBean();
        
        if (WritersManager.getWritersCMS(Utils.getWriter().getWriter().getId()) != 0) {
            this.CmsUserMoreThanOne = true;
            return;
        }      
        
        this.CmsUserMoreThanOne = false;
        WritersManager.setWritersCMS(1, Utils.getWriter().getWriter().getId());
        
        skinIdToWrapper = new LinkedHashMap<Integer, SkinPropertiesWrapper>();
        filePathToWrapper = new HashMap<String, SkinPropertiesWrapper>();
        
        loadSkins();
        
        checkOut();
        
        loadFiles();
        
        loadPagesAndKeys();

        commonBean.setCurrentCmsPropertyStage(CmsStage.CHECKOUT);
        CMSUtil.putSessionObject(CMSConstants.COMMON_CMS_BEAN_NAME, commonBean);
        loaded = true;
        logger.info("Bundle selection manager successfully loaded");
    }
    
    /**
     * Generates sets and lists of all pages and keys from the default skin properties file.
     */
    private void loadPagesAndKeys() {
        SkinPropertiesWrapper wrapper = skinIdToWrapper.get((int) Skin.SKIN_ENGLISH);
        Configuration config = wrapper.getCmsDecoratedConfiguration();
        Iterator<String> keysIterator = config.getKeys();
        String fullKey;
        
        pageSet = new TreeSet<String>();
        Set<String> altTextKeySet = new TreeSet<String>();
        Set<String> nonAltTextKeySet = new TreeSet<String>();
        String keyType;
        String key;
        while (keysIterator.hasNext()) {
            fullKey = keysIterator.next();
            int firstIndexOfDot = fullKey.indexOf(".");
            int secondIndexOfDot = fullKey.indexOf(".", firstIndexOfDot + 1);
            pageSet.add(fullKey.substring(0, firstIndexOfDot));
            keyType = fullKey.substring(firstIndexOfDot + 1, secondIndexOfDot);
            key = fullKey.substring(secondIndexOfDot + 1);
            if (PropertyTypeEnum.getEnumById(keyType) == PropertyTypeEnum.ALT_TEXT) {
                altTextKeySet.add(key);
            } else {
                nonAltTextKeySet.add(key);
            }
        }
        
        pageList = buildSelectItemList(pageSet);
        altTextKeyList = buildSelectItemList(altTextKeySet);
        nonAltTextKeyList = buildSelectItemList(nonAltTextKeySet);
    }
    
    /**
     * Helper method to generate a list of {@link SelectItem} from a set of {@link String}.
     * @param set
     * @return the generated list of {@link SelectItem}
     */
    private ArrayList<SelectItem> buildSelectItemList(Set<String> set) {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>(set.size());
        list.add(new SelectItem(PAGE_AND_KEY_ID_ALL, "All"));
        for (String item : set) {
            list.add(new SelectItem(item));
        }
        return list;
    }
    
    /**
     * Loads all skin properties files that has been checked out from CVS
     * 
     * @throws ConfigurationException
     * @throws IOException
     */
    private void loadFiles() throws ConfigurationException, IOException {
        for (SkinPropertiesWrapper wrapper : skinIdToWrapper.values()) {
           loadFile(wrapper);
        }
    }
    
    /**
     * Checks out all skins properties files from CVS.
     * 
     * @throws FileNotFoundException
     */
    private void checkOut() throws FileNotFoundException {
        logger.debug("Checking out properties files");
        Cvs cvsAccessBean = CMSUtil.getManagedBean(CMSConstants.BIND_CVS, Cvs.class);
        
        cvsAccessBean.checkoutDir(commonBean.getCmsCheckoutDir(),
                                    CVS_CHECKOUT_DIR,
                                    commonBean.getCvsUserName(),
                                    commonBean.getCvsPassword());
        logger.debug("Validating checked out files");
        for (SkinPropertiesWrapper entry : skinIdToWrapper.values()) {
            File fileCheck = new File(entry.getConfigurationFilePath());
            if (!fileCheck.exists()) {
                throw new FileNotFoundException(entry.getConfigurationFilePath());
            }
        }
        logger.debug("Checkout passed successfully");
    }
    
    /**
     * Initiates (sets the checkout directory and the CVS user name and password) the {@link CommonCMSBean} and returns it.
     * 
     * @return the initiated {@link CommonCMSBean} 
     */
    private CommonCMSBean buildCommonBean() {
        CommonCMSBean bean = new CommonCMSBean();
        
        bean.setCmsCheckoutDir(buildCMSCheckoutDir());
        
        Writer writer = Utils.getWriter().getWriter();
        bean.setCvsUserName(writer.getUserName());
        bean.setCvsPassword(writer.getPassword());
        
        return bean;
    }
    
    /**
     * @return the directory where CMS should operate during the current session. 
     */
    private String buildCMSCheckoutDir() {
        return FacesContext.getCurrentInstance().getExternalContext()
                                                        .getInitParameter(CMSConstants.CONTENT_PATH_PROPERTY_NAME)
                    + CMSUtil.getSessionId()
                    + "/";
    }
    
    
    /**
     * Loads all skin information that will be used by CMS module.
     */
    private void loadSkins() {
        logger.debug("Loading skin info");
        skinIdToImagesFolder = new HashMap<Integer, String>();
        skinList = new ArrayList<SelectItem>();
        skinList.add(new SelectItem(SKIN_ID_ALL, "All"));
        SkinPropertiesWrapper skinWrapper;
        for (Skins skin : ApplicationData.getSkinsList()) {
            if (skin.getId() == Skin.SKIN_API
                    || skin.getId() == Skin.SKIN_SPAINMI
                    || skin.getId() ==  Skin.SKIN_ETRADER_INT) {
                /* Skin "api skin", "Spanish MI" and "ETrader" should be skipped */
                continue;
            }
            
            skinList.add(new SelectItem(skin.getId(), skin.getName()));
            skinWrapper = new SkinPropertiesWrapper();
            
            skinWrapper.setId(skin.getId());
            skinWrapper.setName(skin.getName());
            
            String localeId = ApplicationData.getLanguage(skin.getDefaultLanguageId()).getCode().toLowerCase();
            
            // WARNING Order is critical!!!
            skinIdToImagesFolder.put(skin.getId(), localeId + "_" + skin.getId());
            
            if (skin.isCountryInLocaleUse()) {
                localeId = localeId
                                + "_"
                                + ApplicationData.getCountry(skin.getDefaultCountryId()).getA2().toUpperCase();
            }
            
            skinWrapper.setLoacale(localeId);
            
            String filePath = commonBean.getCmsCheckoutDir()
                                + CVS_CHECKOUT_DIR
                                + "MessageResources_" + localeId + ".properties";
            skinWrapper.setConfigurationFilePath(filePath);
            
            filePathToWrapper.put(filePath, skinWrapper);
            skinIdToWrapper.put(skin.getId(), skinWrapper);
        }
        
        logger.debug("Skin info loaded successfully");
    }
    
    @Override
    public void load(String filePath) throws ConfigurationException, IOException {
        loadFile(filePathToWrapper.get(filePath));
    }

    @Override
    public void load(int skinId) throws ConfigurationException, IOException {
        loadFile(skinIdToWrapper.get(skinId));
    }
    
    /**
     * Loads a properties file for the given skin wrapper.
     * 
     * @param wrapper
     * @throws ConfigurationException
     * @throws IOException
     */
    private void loadFile(SkinPropertiesWrapper wrapper)  throws ConfigurationException, IOException {
        logger.debug("Loading skin [" + wrapper.getId() + ":" + wrapper.getName()
                            + "] file [" + wrapper.getConfigurationFilePath() + "]");
        PropertiesConfiguration config;
        BufferedReader fis = new BufferedReader(
                                    new InputStreamReader(
                                            new FileInputStream(wrapper.getConfigurationFilePath()), FILE_ENCODING));
        config = new PropertiesConfiguration();
        config.setIOFactory(new EncodingSupportIOFactory(FILE_ENCODING));
        config.setDelimiterParsingDisabled(true);
        config.load(fis);
        fis.close();
        
        wrapper.setPropertiesConfiguration(config);
        wrapper.setCmsDecoratedConfiguration(config.subset(CMS_PREFIX));
    }

    @Override
    public String getSkinFilePath(int skinId) {
        return skinIdToWrapper.get(skinId).getConfigurationFilePath();
    }

    @Override
    public String getSkinImagesFolder(int skinId) {
        return skinIdToImagesFolder.get(skinId);
    }
    
    public boolean isCmsUserMoreThanOne() {
        return CmsUserMoreThanOne;
    }

    public void setCmsUserMoreThanOne(boolean cmsUserMoreThanOne) {
        CmsUserMoreThanOne = cmsUserMoreThanOne;
    }

    public String getCmsUsernameCountReset() {
        return CmsUsernameCountReset;
    }

    public void setCmsUsernameCountReset(String cmsUsernameCountReset) {
        CmsUsernameCountReset = cmsUsernameCountReset;
    }
    
    public void resetUsername () {
        this.CmsUsernameCountResetStatus = "OK";
        try {
            if (!WritersManager.resetWritersCMS(this.CmsUsernameCountReset)) {
                this.CmsUsernameCountResetStatus = "ERR";
            }
        } catch (SQLException e) {
            this.CmsUsernameCountResetStatus = e.toString();
        }
    }

    public String getCmsUsernameCountResetStatus() {
        return CmsUsernameCountResetStatus;
    }

    public void setCmsUsernameCountResetStatus(String cmsUsernameCountResetStatus) {
        CmsUsernameCountResetStatus = cmsUsernameCountResetStatus;
    }
    
}
