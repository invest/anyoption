/**
 * 
 */
package il.co.etrader.backend.cms.bundle;


import java.io.Writer;

import org.apache.commons.configuration.PropertiesConfiguration.DefaultIOFactory;
import org.apache.commons.configuration.PropertiesConfiguration.PropertiesWriter;

/**
 * This factory provides encoding specific writer implementations. If the
 * encoding is the default one (ISO-8859-1) or <code>null</code>, the default
 * {@link PropertiesWriter} is provided. Otherwise, a 
 * {@link UnescapedProepertiesWriter} is instantiated. 
 * 
 * @author pavelhe
 *
 */
public class EncodingSupportIOFactory extends DefaultIOFactory {

	/**
     * The default encoding (ISO-8859-1 as specified by
     * http://java.sun.com/j2se/1.5.0/docs/api/java/util/Properties.html)
     */
    private static final String DEFAULT_ENCODING = "ISO-8859-1";
	
	private String encoding;

	/**
	 * Constructor
	 * 
	 * @param encoding the encoding that the writer should support.
	 */
	public EncodingSupportIOFactory(String encoding) {
		this.encoding = encoding;
	}
	
	@Override
	public PropertiesWriter createPropertiesWriter(Writer out, char delimiter) {
		if (encoding != null && !encoding.equals(DEFAULT_ENCODING)) {
			return new UnescapedProepertiesWriter(out, delimiter);
		}
		return super.createPropertiesWriter(out, delimiter);
	}

}
