package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.common.PropertyTypeEnum;

import java.io.Serializable;

import javax.faces.context.FacesContext;

public class CmsDataObjects implements Serializable {
	
    private String page;
    private PropertyTypeEnum type;
    private String key;
    private String value;
    private String imagePath;
    private int skin;
    private String skinName;
    private String akamaiDestination;
    private BundleSelection inetrfaceBundleSelection;
    
    
    public CmsDataObjects(String page,PropertyTypeEnum type,int skin, String skinName, String key, String value)
    {
    	this.page=page;
    	this.type=type;
    	this.key=key;
    	this.value=value;
    	this.skin=skin;
    	this.skinName=skinName;
    	
    	//this.imagePath="https://www.testenv.anyoption.com/images/en_2/"+key;
    	FacesContext facesContext = FacesContext.getCurrentInstance();
    	inetrfaceBundleSelection=(BundleSelection)facesContext.getApplication()
				  .createValueBinding("#{cmsBundleSelection}").getValue(facesContext);
    	
    	this.imagePath=FacesContext.getCurrentInstance().getExternalContext()
                .getInitParameter("il.co.etrader.backend.cms.image_preview_path")
                + inetrfaceBundleSelection.getSkinImagesFolder(skin)+"/" + getParseKey(key);
    	
    	this.akamaiDestination=inetrfaceBundleSelection.getSkinImagesFolder(skin)+"/" + getParseKey(key);
    }
    
    public String getPage()
    {
        return this.page;
    }

    public void setPage(String page)
    {
        this.page = page;
    }
    
    public PropertyTypeEnum getType()
    {
        return this.type;
    }

    public void setType(PropertyTypeEnum type)
    {
        this.type = type;
    }
    
    public String getKey()
    {
        return this.key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }
    
    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String getImagePath()
    {
        return this.imagePath;
    }

    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }
    
    public int getSkin()
    {
        return this.skin;
    }

    public void setSkinh(int skin)
    {
        this.skin = skin;
    }
    
    public String getSkinName()
    {
        return this.skinName;
    }

    public void setSkinName(String skinName)
    {
        this.skinName = skinName;
    }
    
    public String getAkamaiDestination()
    {
        return this.akamaiDestination;
    }

    public void setAkamaiDestination(String akamaiDestination)
    {
        this.akamaiDestination = akamaiDestination;
    } 

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CmsDataObjects [page=" + page + ", type=" + type + ", key="
				+ key + ", value=" + value + ", imagePath=" + imagePath
				+ ", skin=" + skin + ", skinName=" + skinName
				+ ", inetrfaceBundleSelection=" + inetrfaceBundleSelection
				+ "]";
	}
	
	private String getParseKey(String key){
		String parsedKey="";
		String[] temp;
		String delimiter = "\\.";
		  temp = key.split(delimiter);
		  for(int i =0; i < temp.length ; i++){
			 if(i==temp.length-2){
				 parsedKey=parsedKey+(temp[i])+'.';
			 }else if(i==temp.length-1){
				 parsedKey=parsedKey+(temp[i]);
			 }else{
				 parsedKey=parsedKey+(temp[i])+'/'; 
			 }
			  
		  }
		return parsedKey;
	}

}
