package il.co.etrader.backend.cms;

import il.co.etrader.i18n.Assert;
import il.co.etrader.i18n.ReloadableResourceBundleMessageSource;
import il.co.etrader.i18n.ReloadableResourceBundleMessageSourceMBean;
import il.co.etrader.jmx.JMXCacheCleanParts;
import il.co.etrader.jmx.JMXCacheCleanPartsMBean;
import il.co.etrader.jmx.JmxClient;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.JmxServerConfig;
import com.anyoption.common.jmx.JMXCacheCleanPartsJson;
import com.anyoption.common.jmx.JMXCacheCleanPartsJsonMBean;

public class JmxCacheReloader implements CacheReloader, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2263342374661122734L;
	/*
	 * applicationName is the suffix used to register the 
	 * ReloadableResourceBundleMessageSource MBean in order to 
	 * distinguish different applications MBeans 
	 * example :
	 * "anyoption", "etrader" or "backend"
	 * the same suffix is added to the following when registering the
	 * jmx urls in web.xml 
	 */
	private final static String productionJmxServiceURLPrefix	= "il.co.etrader.backend.cms.JMX.prodEnvJmxServiceURL.";
	private final static String testJmxServiceURLPrefix			= "il.co.etrader.backend.cms.JMX.testEnvJmxServiceURL.";

	
	/*
	 * collection of all production or test environment for a given 
	 * application name
	 * 
	 * JMXServiceURL must begin with the string "service:jmx:"
	 * see javax.management.remote.JMXServiceURL for details
	 * example :
	 * service:jmx:rmi:///jndi/rmi://localhost:8181/jmxrmi
	 * 
	 * production key format in web.xml : il.co.etrader.backend.cms.JMX.prodEnvJmxServiceURL.[applicationName]
	 * test key format in web.xml: il.co.etrader.backend.cms.JMX.testEnvJmxServiceURL.[applicationName]
	 */
	private HashMap<String, ArrayList<String>> testEnvJmxServiceURL = null;
	private HashMap<String, ArrayList<String>> prodEnvJmxServiceURL = null;

	private static final Logger logger = Logger.getLogger(JmxCacheReloader.class);
	
	public JmxCacheReloader() {
		Map parameters = FacesContext.getCurrentInstance().getExternalContext().getInitParameterMap();
		testEnvJmxServiceURL = new HashMap<String, ArrayList<String>>();
		prodEnvJmxServiceURL = new HashMap<String, ArrayList<String>>();
		
		// for each application load test and prod environment addresses
		ArrayList<String> testEnvKeys = loadKeysSubset(parameters, testJmxServiceURLPrefix);
		for(String testEnvKey:testEnvKeys){
			String applicationName = testEnvKey.substring(testJmxServiceURLPrefix.length());
			String value = (String) parameters.get(testEnvKey);
			String[] urls = value.split(",");
			testEnvJmxServiceURL.put(applicationName, new ArrayList<String>(Arrays.asList(urls)));
		}
		
		ArrayList<String> prodEnvKeys = loadKeysSubset(parameters, productionJmxServiceURLPrefix);
		for(String prodEnvKey:prodEnvKeys){
			String applicationName = prodEnvKey.substring(productionJmxServiceURLPrefix.length());
			String value = (String) parameters.get(prodEnvKey);
			String[] urls = value.split(",");
			prodEnvJmxServiceURL.put(applicationName, new ArrayList<String>(Arrays.asList(urls)));
		}
	}
	
	@Override
	public void clearProdEnvCache(String applicationName) {
		ArrayList<String> jmxServiceUrls = prodEnvJmxServiceURL.get(applicationName);
		if(jmxServiceUrls!=null) {
			for(String jmxServiceUrl:jmxServiceUrls) {
				clearCache(applicationName, jmxServiceUrl);
			}
		}
		
	}

	@Override
	public void clearTestEnvCache(String applicationName) {
		ArrayList<String> jmxServiceUrls = testEnvJmxServiceURL.get(applicationName);
		if(jmxServiceUrls!=null) {
			for(String jmxServiceUrl:jmxServiceUrls) {
				clearCache(applicationName, jmxServiceUrl);
			}
		}
	}
	
	private void clearCache(String applicationName, String jmxServiceURL) {
		Assert.notNull(applicationName, "JmxCacheReloader.applicationName must be set");
		Assert.notNull(jmxServiceURL, "JmxCacheReloader.url must be set");
		JmxClient jmxClient = null;
		try {
			jmxClient = new JmxClient(jmxServiceURL);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Assert.notNull(jmxClient, "Connection to JMX failed! JMXServiceURL:"+jmxServiceURL+" applicationName:"+applicationName);
		
		ReloadableResourceBundleMessageSourceMBean reloadableMessageSource = jmxClient.getMBeanProxy(ReloadableResourceBundleMessageSource.RELOADABLE_MESSAGE_SOURCE_JMX_OBJECT_NAME+applicationName, ReloadableResourceBundleMessageSourceMBean.class);
		reloadableMessageSource.clearCache();
	}

	/*
	 * from @parameters load the keys startign with @prefix
	 */
	private ArrayList<String> loadKeysSubset(Map<String, String> parameters, String prefix){
		ArrayList<String> appNames = new ArrayList<String>();
		Iterator<String> i = parameters.keySet().iterator();
		while(i.hasNext()) {
			String key = i.next();
			if(key.startsWith(prefix)) {
				appNames.add(key);
			}
		}
		if(appNames.size()==0) {
			System.out.println("loadSubset from prefix:"+prefix+" is empty !");
		}
		return appNames;
	}
	
	public boolean initAssetIndexMarketsCache(String applicationName, JmxServerConfig jmxServerConfig) {
		String jmxServiceURL = "";
		String mbeanName = "";
		String mbeanNameJson = "";

		JmxClient jmxClient = null;
		try {
			
			jmxServiceURL = "service:jmx:rmi:///jndi/rmi://" + jmxServerConfig.getAddress() + ":" + jmxServerConfig.getPort() +"/jmxrmi";
			logger.debug("Try to jmx call:" + applicationName + " and address:" + jmxServiceURL);
			
			if(jmxServerConfig.getPassword() != null && jmxServerConfig.getUserName() != null){
				jmxClient = new JmxClient(jmxServiceURL, jmxServerConfig.getUserName(), jmxServerConfig.getPassword());
			} else {
				jmxClient = new JmxClient(jmxServiceURL);
			}			

			if(applicationName.contains("AO")){
				mbeanName = JMXCacheCleanParts.WEB_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_AO;
				mbeanNameJson = JMXCacheCleanPartsJson.JSON_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_AO;
			} else if(applicationName.contains("ET")){
				mbeanName = JMXCacheCleanParts.WEB_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_ET;
				mbeanNameJson = JMXCacheCleanPartsJson.JSON_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_ET;
			}
			
			//For web
			initWebAssets(applicationName, jmxServiceURL, mbeanName, jmxClient);
			//For Json
			initJson(applicationName, jmxServiceURL, mbeanNameJson, jmxClient);
			
		} catch (IOException e) {
			logger.error("When jmxClient create", e);
			return false;
		} finally{
			try {
				jmxClient.close();
			} catch (IOException e) {
				logger.error("When close connection ", e);
			}
		}
		return true;
	}

	private void initWebAssets(String applicationName, String jmxServiceURL, String mbeanName, JmxClient jmxClient) {
		try {
			JMXCacheCleanPartsMBean cacheCleanParts = jmxClient.getMBeanProxy(mbeanName, JMXCacheCleanPartsMBean.class);
			cacheCleanParts.resetSkinAssetIndexMarkets();
			logger.debug("resetSkinAssetIndexMarkets on:" + applicationName + " and address:" + jmxServiceURL);
			cacheCleanParts.initSkinAssetIndexMarkets();
			logger.debug("initSkinAssetIndexMarkets on:" + applicationName + " and address:" + jmxServiceURL);
			
		} catch (Exception e) {
			logger.error("Can't init WEB asst index info!!!!");
		}
	}
		
	private void initJson(String applicationName, String jmxServiceURL,
			String mbeanNameJson, JmxClient jmxClient) {
		try {
			JMXCacheCleanPartsJsonMBean cacheCleanPartsJson = jmxClient.getMBeanProxy(mbeanNameJson, JMXCacheCleanPartsJsonMBean.class);
			cacheCleanPartsJson.initSkinAssetIndexMarketsJson();
			logger.debug("initSkinAssetIndexMarketsJson on:" + applicationName + " and address:" + jmxServiceURL);	
		} catch (Exception e) {
			logger.error("Can't init Json asst index info!!!!");
		}
	}
}
