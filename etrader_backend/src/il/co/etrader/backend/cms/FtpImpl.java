package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.CMSConstants.fileType;
import il.co.etrader.backend.cms.FtpImpl.ThreadResult;
import il.co.etrader.backend.cms.common.CMSUtil;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.listener.Log4jListener;


public class FtpImpl implements Ftp, FtpWorkerListener, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7496015764466231507L;

	private static final Logger log = Logger.getLogger(FtpImpl.class);
	
	private final String path = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_build_path");
	private final String cvsPath = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.cvs_path");
	private String message;
	private File buildFile;
	private String liveServers;
	private String previewServer;
	private String port;
	private String userId;
	private String password;
	private Cvs cvs;
	private List<String> upldOnServer;
	private Map<String, Integer> failedCountMap;
	private int numOfServers;
	private int failsPermitted = 3;
	enum ThreadResult {
		success, fail;
	}
	
	public FtpImpl(){
		// TODO This method is not supported during startup
		cvs = CMSUtil.getManagedBean(CMSConstants.BIND_CVS, Cvs.class);
		buildFile = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(path));
		liveServers = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_server.live");
		previewServer = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_server.preview");
		port = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_port");
		userId = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_userId");
		password = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("il.co.etrader.backend.cms.FtpImpl.ftp_password");		
	}
	
	
	@Override
	public void preview(String baseDir, String destination, fileType type, String cvsUser, String cvsPass) {
		message = null;
		
		threadExecutor(baseDir, destination, type, cvsUser, cvsPass, previewServer);
	}

	@Override
	public void publish(String baseDir, String destination, fileType type, String cvsUser, String cvsPass) {
		message = null;
		
		threadExecutor(baseDir, destination, type, cvsUser, cvsPass, liveServers);
	}

	private void threadExecutor(String baseDir, String destination, fileType type, String cvsUser, String cvsPass, String servers) {
		upldOnServer = new LinkedList<String>();
		failedCountMap = new HashMap<String, Integer>();
		
		String[] srvrs = servers.split(",");
		numOfServers = srvrs.length;
		for (String server : srvrs) {
			if (server.contains(":")) { // This means that we have multiple servers and/or the server and the port are added in one parameter.
				String[] s = server.split(":");
				new Thread(new FtpWorker(s[0].trim(), this, buildFile, baseDir, destination, type, cvsPath, s[1].trim(), userId, password, cvsUser, cvsPass, cvs)).start();
			} else {
				new Thread(new FtpWorker(server.trim(), this, buildFile, baseDir, destination, type, cvsPath, port, userId, password, cvsUser, cvsPass, cvs)).start();				
			}
		}
		while (true) {
			for (String key : failedCountMap.keySet()) {
				if (failedCountMap.get(key) < failsPermitted) {
					String[] s = key.split(":");
					new Thread(new FtpWorker(s[0], this, buildFile, baseDir, destination, type, cvsPath, s[1], userId, password, cvsUser, cvsPass, cvs)).start();
				} else {
					upldOnServer.add(key.split(":")[0]);
				}
			}
			if (upldOnServer.size() == numOfServers) {
				log.debug("All done");
				break;
			}
			synchronized (this) {
				try {
					log.debug("wait..");
					wait();
					log.debug("wait done");
				} catch (InterruptedException ie) {
					if (this.message != null) {
						this.message = this.message + "\n" + "thread error: " + "\n" + ie.getMessage();
					} else {
						this.message = "thread error: " + "\n" + ie.getMessage();
					}
				}
			}
		}
	}
	
	@Override
	public void workerResult(String serverName, String port, String msg, ThreadResult threadResult) {
		synchronized (this) {
			log.debug("Processing result");
			this.message = msg;
			switch (threadResult) {
			case success:
				upldOnServer.add(serverName);
				break;

			case fail:
				if (failedCountMap.containsKey(serverName + ":" + port)) {
					Integer count = failedCountMap.get(serverName + ":" + port);
					failedCountMap.put(serverName + ":" + port, ++count);
				} else {
					failedCountMap.put(serverName + ":" + port, new Integer(1));
				}					
				break;
				
			default:
				break;
			}
			log.debug("notify");
			notify();
		}
	}
	
	@Override
	public String getMessage() {
		return message;
	}	
}

class FtpWorker implements Runnable {
	private static final Logger log = Logger.getLogger(FtpWorker.class);

	private String serverName;
	private FtpWorkerListener listener;
	private File buildFile;
	private String baseDir;
	private String destination;
	private fileType type;
	private String cvsPath;
	private String port;
	private String userId;
	private String password;
	private String cvsUser;
	private String cvsPassword;
	private Cvs cvs;
	private String message;
	
	public FtpWorker(String serverName, FtpWorkerListener listener, File buildFile, String baseDir, String destination, fileType type, String cvsPath, String port, String user, String password, String cvsUser, String cvsPassword, Cvs cvs) {
		this.serverName = serverName.trim();
		this.listener = listener;
		this.buildFile = buildFile;
		this.baseDir = baseDir;
		this.destination = destination;
		this.type = type;
		this.cvsPath = cvsPath;
		this.port = port;
		this.userId = user;
		this.password = password;
		this.cvsUser = cvsUser;
		this.cvsPassword = cvsPassword;
		this.cvs = cvs;
	}
	
	public void run() {
		Thread.currentThread().setName("FTP worker - " + serverName);
		message = null;
		Project p = new Project();
		p.setUserProperty("ant.file", buildFile.getPath());
		p.setProperty("srvDir", serverName);
		p.setProperty("msgBaseDir", baseDir);
		p.setProperty("cvsPath", cvsPath);
		int lastChar = destination.lastIndexOf('/');
		String destPath = destination.substring(0, lastChar);
		p.setProperty("destPath", destPath);
		String ftpDestPath = FilePathConvertor.convertCvsToFtpPath(destPath, type);
		p.setProperty("ftpDestPath", ftpDestPath);
		String fileName = destination.substring(lastChar + 1);
		p.setProperty("fileName", fileName);
		
		
		p.setProperty("server", serverName);
		p.setProperty("port", port);
		p.setProperty("userId", userId);
		p.setProperty("password", password);
		
		Log4jListener loggerListener = new Log4jListener();
		p.addBuildListener(loggerListener);
		try {
			p.fireBuildStarted();
			p.init();
			ProjectHelper helper = ProjectHelper.getProjectHelper();
			p.addReference("ant.projectHelper", helper);
			helper.parse(p, buildFile);
			switch (type) {
			case property:
				p.executeTarget("mk_tmp_dirs");
				if ( !fileName.matches(".*_.*_.*") ) {
					chekoutCommonFile(baseDir, destPath, fileName, cvsUser, cvsPassword, serverName);
					p.executeTarget("upload_property_with_markets");
				} else {
					p.executeTarget("upload_property_without_markets");
				}
				p.executeTarget("del_tmp_dirs");
				break;
				
			case image:
				p.executeTarget("upload_image");
				break;

			default:
				break;
			}
			log.debug("Sending result");
			listener.workerResult(serverName, port, message, ThreadResult.success);
			p.fireBuildFinished(null);
		} catch (BuildException e) {
			if (this.message != null) {
				this.message = this.message + "\n" + "build error: " + "\n" + e.getMessage();
			} else {
				this.message = "build error: " + "\n" + e.getMessage();
			}
			listener.workerResult(serverName, port, message, ThreadResult.fail);
			p.executeTarget("del_tmp_dirs");
			p.fireBuildFinished(e);
		}		
	}
	
	private void chekoutCommonFile(String baseDir, String destPath, String fileName, String cvsUser, String cvsPass, String serverName) {
		cvs.checkoutFile(baseDir + destPath + "/" + serverName + "/tmp/common/", cvsPath + fileName, cvsUser, cvsPass);
		if (this.message != null && cvs.getGet_message() != null) { 
			this.message = this.message + "\n" + cvs.getGet_message();
		} else {
			this.message = cvs.getGet_message();
		} 
	}
}

interface FtpWorkerListener {
	public void workerResult(String serverName, String port, String msg, ThreadResult threadResult);
}
