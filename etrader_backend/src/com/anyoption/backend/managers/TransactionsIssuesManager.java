package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.TransactionsIssuesDAO;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.helper.UserForFirstDepositEmail;

public class TransactionsIssuesManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(TransactionsIssuesManager.class);
	
	public static void transactionsIssuesFill() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            TransactionsIssuesDAO.transactionsIssuesFill(conn);
        } finally {
            closeConnection(conn);
        }
	}

	public static void transactionsIssuesCycleFill() throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            TransactionsIssuesDAO.transactionsIssuesCycleFill(conn);
        } finally {
            closeConnection(conn);
        }
	}

    public static ArrayList<UserForFirstDepositEmail> getPastFiveMinutesTransactionsUsers() throws SQLException, ParseException {
    	Connection con = null;
    	ArrayList<UserForFirstDepositEmail> users = null;
    	try {
    		con = getConnection();
    		users = TransactionsIssuesDAO.getPastFiveMinutesTransactionsUsers(con);
    		TransactionsIssuesDAO.updateLastRun(con);
    	} finally {
    		closeConnection(con);
    	}
    	return users;
    }

}
