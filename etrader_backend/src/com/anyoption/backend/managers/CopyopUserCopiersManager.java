package com.anyoption.backend.managers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.anyoption.backend.service.copyopusercopiers.CopyopUserCopiersMethodRequest;
import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.beans.UserUniqueDetails;
import com.copyop.common.dao.CopiedInvestmentDAO;
import com.copyop.common.dao.ProfileDAO;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.ProfileLinkHistory;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.managers.ManagerBase;
import com.datastax.driver.core.Session;

import il.co.etrader.backend.dao_managers.CopyopUserCopiersDAO;
import il.co.etrader.util.CommonUtil;

public class CopyopUserCopiersManager extends ManagerBase {

	private static final Logger logger = Logger.getLogger(CopyopUserCopiersManager.class);

	public static ArrayList<CopyopUserCopy> getUsersCopy(CopyopUserCopiersMethodRequest request,
			ProfileLinkTypeEnum profileLinkType, long userOffsetId, Date timeCreatedOffset) {
		ArrayList<CopyopUserCopy> copyopUserCopyList = new ArrayList<CopyopUserCopy>();
		HashMap<Long, UserUniqueDetails> usersUniqueDetails = new HashMap<Long, UserUniqueDetails>();
		ArrayList<Long> destinationUsersIds = new ArrayList<Long>();

		Session session = ManagerBase.getSession();

		boolean remove = false;

		if (request.isStillCopying() == ConstantsBase.FILTER_YES) {
			List<ProfileLink> profilesLink = getProfileLinksByType(request.getUserId(), profileLinkType, userOffsetId);

			while (copyopUserCopyList.size() < request.getRowsPerPage()) {
				for (ProfileLink profileLink : profilesLink) {
					destinationUsersIds.add(profileLink.getDestUserId());
				}
				if (profilesLink.size() > 0) {

					if (userOffsetId == profilesLink.get(profilesLink.size() - 1).getDestUserId()
							|| copyopUserCopyList.size() == request.getRowsPerPage()) {
						request.setUserOffsetId(userOffsetId);
						break;
					}

					usersUniqueDetails = getUserUniqueDetails(session, request.getUserId(), destinationUsersIds);

					for (ProfileLink profileLink : profilesLink) {
						CopyopUserCopy copyopUserCopy = getCopyopUserCopy(profileLink, usersUniqueDetails);
						remove = false;

						if (copyopUserCopyList.size() < request.getRowsPerPage()) {
							if (copyopUserCopy != null) {
								Date toDate = new Date(request.getToDate());
								toDate = DateUtils.addHours(toDate, 23);
								toDate = DateUtils.addMinutes(toDate, 59);
								toDate = DateUtils.addSeconds(toDate, 59);
								if (copyopUserCopy.getStartDate().before(new Date(request.getFromDate()))
										|| copyopUserCopy.getStartDate().after(toDate) ) {
									remove = true;
								}
								if (request.getUserLeaderId() != ConstantsBase.FILTER_ALL) {
									if (request.getUserLeaderId() != copyopUserCopy.getUserId()) {
										remove = true;
									}
								}
								if (request.isStillCopying() == ConstantsBase.FILTER_YES) {
									if (copyopUserCopy.getEndDate() != null) {
										remove = true;
									}
								} 
								if (request.isFrozen() == ConstantsBase.FILTER_NO) {
									if (copyopUserCopy.isFrozen()) {
										remove = true;
									}
								}
								if (!remove) {
									copyopUserCopyList.add(copyopUserCopy);
								}
							}
							userOffsetId = profileLink.getDestUserId();
							request.setUserOffsetId(userOffsetId);
						} else {
							break;
						}
					}
					profilesLink = getProfileLinksByType(request.getUserId(), profileLinkType, userOffsetId);
				} else {
					break;
				}
			}
		}
		if (request.isStillCopying() == ConstantsBase.FILTER_NO) {
			if (timeCreatedOffset == null) {
				timeCreatedOffset = new Date(request.getToDate());
			}
			List<ProfileLinkHistory> profilesLinkHistory = CopyopUserCopiersDAO.getProfileLinksHistory(session,
					request.getUserId(), profileLinkType, timeCreatedOffset, new Date(request.getFromDate()),
					new Date(request.getToDate()));
			while (copyopUserCopyList.size() < request.getRowsPerPage()) {
				for (ProfileLinkHistory profileLinkHistory : profilesLinkHistory) {
					destinationUsersIds.add(profileLinkHistory.getDestUserId());
				}

				if (profilesLinkHistory.size() > 0) {

					if (timeCreatedOffset
							.equals(profilesLinkHistory.get(profilesLinkHistory.size() - 1).getTimeCreated())
							|| copyopUserCopyList.size() == request.getRowsPerPage()) {
						request.setTimeCreatedOffset(timeCreatedOffset);
						break;
					}

					usersUniqueDetails = getUserUniqueDetails(session, request.getUserId(), destinationUsersIds);

					for (ProfileLinkHistory profileLinkHistory : profilesLinkHistory) {
						CopyopUserCopy copyopUserCopy = getCopyopUserCopyHistory(profileLinkHistory,
								usersUniqueDetails);
						remove = false;
						if (copyopUserCopyList.size() < request.getRowsPerPage()) {
							if (copyopUserCopy != null) {
								if (request.getUserLeaderId() != ConstantsBase.FILTER_ALL) {
									if (request.getUserLeaderId() != copyopUserCopy.getUserId()) {
										remove = true;
									}
								}
								if (request.isStillCopying() == ConstantsBase.FILTER_NO) {
									if (copyopUserCopy.getEndDate() == null) {
										remove = true;
									}
								}
								if (request.isFrozen() == ConstantsBase.FILTER_NO) {
									if (copyopUserCopy.isFrozen()) {
										remove = true;
									}
								}
								if (!remove) {
									copyopUserCopyList.add(copyopUserCopy);
								}
							}
							timeCreatedOffset = profileLinkHistory.getTimeCreated();
							request.setTimeCreatedOffset(timeCreatedOffset);
						} else {
							break;
						}
					}
					profilesLinkHistory = CopyopUserCopiersDAO.getProfileLinksHistory(session, request.getUserId(),
							profileLinkType, timeCreatedOffset, new Date(request.getFromDate()),
							new Date(request.getToDate()));
				} else {
					break;
				}
			}
		}
		return copyopUserCopyList;

	}

	private static CopyopUserCopy getCopyopUserCopy(ProfileLink profileLink,
			HashMap<Long, UserUniqueDetails> usersUniqueDetails) {
		CopyopUserCopy copyopUserCopy = null;
		UserUniqueDetails user = usersUniqueDetails.get(profileLink.getDestUserId());
		if (user != null) {
			copyopUserCopy = new CopyopUserCopy();
			copyopUserCopy.setNickName(usersUniqueDetails.get(profileLink.getDestUserId()).getNickname());
			copyopUserCopy
					.setCopyTradingProfit(usersUniqueDetails.get(profileLink.getDestUserId()).getCopyTradingProfit());
			copyopUserCopy.setTurnover(usersUniqueDetails.get(profileLink.getDestUserId()).getTurnOver());
			copyopUserCopy.setBalance(usersUniqueDetails.get(profileLink.getDestUserId()).getBalance());
			copyopUserCopy.setFrozen(usersUniqueDetails.get(profileLink.getDestUserId()).isFrozen());
			copyopUserCopy.setUserId(profileLink.getDestUserId());
			copyopUserCopy.setStartDate(profileLink.getTimeCreated());
			copyopUserCopy.setInvCount(profileLink.getCount());
			copyopUserCopy.setInvCountReached(profileLink.getCountReached());
			copyopUserCopy.setAmount(profileLink.getAmount());
			copyopUserCopy
					.setSpecific((profileLink.getAssets().isEmpty() || profileLink.getAssets() == null) ? false : true); // empty
																															// list
																															// means
																															// "all".
			for (Long item : profileLink.getAssets()) {
				copyopUserCopy.getSpecificAssets().add(CommonUtil.getMarketName(item));
			}
			copyopUserCopy.setCoinsGenerated(usersUniqueDetails.get(profileLink.getDestUserId()).getCoinsGenerated());
			copyopUserCopy.setCurrencyId(usersUniqueDetails.get(profileLink.getDestUserId()).getCurrencyId());
			copyopUserCopy.setAssets(profileLink.getAssets());
			copyopUserCopy.setSkinId(usersUniqueDetails.get(profileLink.getDestUserId()).getSkinId());
		}

		return copyopUserCopy;
	}

	private static CopyopUserCopy getCopyopUserCopyHistory(ProfileLinkHistory profileLinkHistory,
			HashMap<Long, UserUniqueDetails> usersUniqueDetails) {
		CopyopUserCopy copyopUserCopy = getCopyopUserCopy(profileLinkHistory, usersUniqueDetails);

		if (copyopUserCopy != null) {
			copyopUserCopy.setEndDate(profileLinkHistory.getTimeDone());
			copyopUserCopy.setEndReason(profileLinkHistory.getResonDone().getDisplayName());
		}
		return copyopUserCopy;
	}

	private static HashMap<Long, UserUniqueDetails> getUserUniqueDetails(Session session, long userId,
			List<Long> destinationUsersIds) {
		ArrayList<Long> arrDestUsersIds = new ArrayList<Long>();
		StringBuilder usersIdsParse = new StringBuilder();
		String delim = "";

		arrDestUsersIds.addAll(destinationUsersIds);
		HashMap<Long, UserUniqueDetails> userUniqueDetailsC = ProfileDAO.getNicknamesAndFrozen(session,
				arrDestUsersIds);
		for (Long destUserId : arrDestUsersIds) {
			usersIdsParse.append(delim).append(destUserId);
			delim = ",";
		}

		HashMap<Long, UserUniqueDetails> userUniqueDetailsO = UsersManagerBase.getUserUniqueDetails(usersIdsParse);
		// user & his sum.
		HashMap<Long, CopiedInvestment> usersCopiedInvestmentSum = CopiedInvestmentDAO.getCopiedInvestmentSum(session,
				userId, arrDestUsersIds);

		for (Map.Entry<Long, UserUniqueDetails> userDetailsEntry : userUniqueDetailsO.entrySet()) {
			logger.info("Key : " + userDetailsEntry.getKey() + " Value : " + userDetailsEntry.getValue().toString());
			UserUniqueDetails userUniqueDetails = new UserUniqueDetails();

			if (userUniqueDetailsO.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails.setBalance(userUniqueDetailsO.get(userDetailsEntry.getKey()).getBalance());
				userUniqueDetails.setTurnOver((userUniqueDetailsO.get(userDetailsEntry.getKey()).getTurnOver()));
				userUniqueDetails.setCurrencyId(userUniqueDetailsO.get(userDetailsEntry.getKey()).getCurrencyId());
				userUniqueDetails.setSkinId(userUniqueDetailsO.get(userDetailsEntry.getKey()).getSkinId());
			}

			if (userUniqueDetailsC.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails.setFrozen(userUniqueDetailsC.get(userDetailsEntry.getKey()).isFrozen());
				userUniqueDetails.setNickname(userUniqueDetailsC.get(userDetailsEntry.getKey()).getNickname());
			}

			if (usersCopiedInvestmentSum.containsKey(userDetailsEntry.getKey())) {
				userUniqueDetails
						.setCopyTradingProfit(usersCopiedInvestmentSum.get(userDetailsEntry.getKey()).getProfit());
				userUniqueDetails.setCoinsGenerated(
						2 * usersCopiedInvestmentSum.get(userDetailsEntry.getKey()).getCopiedCountSuccess());
			}

			userUniqueDetailsC.put(userDetailsEntry.getKey(), userUniqueDetails);
		}
		return userUniqueDetailsC;
	}

	public static List<ProfileLink> getProfileLinksByType(long userId, ProfileLinkTypeEnum type, long userOffsetId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (logger.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<ProfileLink> pl = CopyopUserCopiersDAO.getProfileLinksByType(session, userId, type, userOffsetId);
		if (logger.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			logger.trace("TIME: get profile links by type " + (endTime - beginTime) + "ms");
		}
		return pl;
	}
}
