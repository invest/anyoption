package com.anyoption.backend.managers;

import il.co.etrader.bl_vos.CountryGroup;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.CountryDAO;
import com.anyoption.backend.service.country.CountryGroupMethodRequest;
import com.anyoption.backend.service.country.CountryGroupMethodResponse;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author eyal.ohana
 *
 */
public class CountryManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(CountryManager.class);
	
	public static final String FILTER_COUNTRIES_GROUP_TYPES = "countries_group_types";
	public static final String FILTER_COUNTRIES_GROUP_TIERS = "countries_group_tiers";
	public static final String FILTER_COUNTRIES = "countries";
	
	public static final String[] filterConstants = {"", FILTER_COUNTRIES_GROUP_TYPES, FILTER_COUNTRIES_GROUP_TIERS, FILTER_COUNTRIES};
	
	public static HashMap<Long, String> countriesGroupTypes;
	public static HashMap<Long, String> countriesGroupTiers;
	public static HashMap<Long, String> countries;
	
	/**
	 * Get countries group types
	 * @return countriesGroupTypes as HashMap<Long, String>
	 */
	public static HashMap<Long, String> getCountriesGroupTypes() {
		if (countriesGroupTypes == null) {
			Connection con = null;
			try {
				con = getConnection();
				countriesGroupTypes = CountryDAO.getFilters(con).get(FILTER_COUNTRIES_GROUP_TYPES);
			} catch (SQLException e) {
				logger.error("Can't lazzy load countries group types. ", e);
			} finally {
				closeConnection(con);
			}
		}
		return countriesGroupTypes;
	}
	
	/**
	 * Get countries group tiers
	 * @return countriesGroupTiers as HashMap<Long, String>
	 */
	public static HashMap<Long, String> getCountriesGroupTiers() {
		if (countriesGroupTiers == null) {
			Connection con = null;
			try {
				con = getConnection();
				countriesGroupTiers = CountryDAO.getFilters(con).get(FILTER_COUNTRIES_GROUP_TIERS);
			} catch (SQLException e) {
				logger.error("Can't lazzy load countries group tiers. ", e);
			} finally {
				closeConnection(con);
			}
		}
		return countriesGroupTiers;
	}
	
	/**
	 * Get countries
	 * @return countries as HashMap<Long, String
	 */
	public static HashMap<Long, String> getCountries() {
		if (countries == null) {
			Connection con = null;
			try {
				con = getConnection();
				countries = CountryDAO.getFilters(con).get(FILTER_COUNTRIES);
			} catch (SQLException e) {
				logger.error("Can't lazzy load countries. ", e);
			} finally {
				closeConnection(con);
			}
		}
		return countries;
	}
	
	/**
	 * Get country groups
	 * @return countryGroups as ArrayList<CountryGroup>
	 */
	public static ArrayList<CountryGroup> getCountryGroups(CountryGroupMethodResponse response) {
		Connection con = null;
		try {
			con = getConnection();
			return CountryDAO.getCountryGroups(con);
		} catch (SQLException e) {
			logger.info("Failed to get all country groups due to: ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_GET_COUNTRY_GROUPS); 
			return null;
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * update country group
	 * @param request
	 * @param response
	 */
	public static void updateCountryGroup(CountryGroupMethodRequest request, MethodResult response) {
		Connection con = null;
		try {
			con = getConnection();
			CountryDAO.updateCountryGroup(con, request);
		} catch (SQLException e) {
			logger.info("Fail to update country group due to: ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE_COUNTRY_GROUPS);
		} finally {
			closeConnection(con);
		}
	}
}
