package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.CallStatusDAO;
import com.anyoption.backend.service.salescallstatusmanager.SalesCallStatusManagerMethodRequest;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.bl_vos.CallsRepresentative;

public class CallStatusManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(CallStatusManager.class);

	public static final String FILTER_USER_RANK = "userRanks";
	public static final String FILTER_REPRESENTATIVES = "representatives";
	public static final String FILTER_SKINS = "skins";

	public static final String[] filterConstants = {"", FILTER_USER_RANK, FILTER_REPRESENTATIVES, FILTER_SKINS};

	public static HashMap<Long, String> userRanks;
	public static HashMap<Long, String> representatives;
	public static HashMap<Long, String> skins;

	public static HashMap<Long, String> getUserRanks() {
		if (userRanks == null) {
			Connection con = null;
			try {
				con = getConnection();
				userRanks = CallStatusDAO.getFilters(con).get(FILTER_USER_RANK);
			} catch (SQLException e) {
				logger.error("Can't lazy load User Ranks: ", e);
			} finally {
				closeConnection(con);
			}
		}
		return userRanks;
	}

	public static HashMap<Long, String> getRepresentatives() {
		if (representatives == null) {
			Connection con = null;
			try {
				con = getConnection();
				representatives = CallStatusDAO.getFilters(con).get(FILTER_REPRESENTATIVES);
			} catch (SQLException e) {
				logger.error("Can't lazy load Representatives: ", e);
			} finally {
				closeConnection(con);
			}
		}
		return representatives;
	}

	public static HashMap<Long, String> getSkins() {
		if (skins == null) {
			Connection con = null;
			try {
				con = getConnection();
				skins = CallStatusDAO.getFilters(con).get(FILTER_SKINS);
			} catch (SQLException e) {
				logger.error("Can't lazy load Skins: ", e);
			} finally {
				closeConnection(con);
			}
		}
		return skins;
	}

	public static ArrayList<CallsRepresentative> getSalesCallStatusRepresentatives(SalesCallStatusManagerMethodRequest request)
			throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return CallStatusDAO.getSalesCallStatusRepresentatives(conn, request);
		} finally {
			closeConnection(conn);
		}
	}
}
