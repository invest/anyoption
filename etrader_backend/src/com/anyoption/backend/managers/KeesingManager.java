/**
 * 
 */
package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.KeesingDAO;
import com.anyoption.backend.service.keesing.KeesingRequest;
import com.anyoption.backend.service.keesing.KeesingResponse;
import com.anyoption.common.beans.File;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.service.CommonJSONService;

import il.co.etrader.backend.bl_vos.IdsFiles;

/**
 * @author pavel.tabakov
 *
 */
public class KeesingManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(KeesingManager.class);

	public static void insertResponse(KeesingResponse response, KeesingRequest request) {
		Connection conn = null;
		try {
			conn = getConnection();
			KeesingDAO.insertResponse(response, request, conn);
		} catch (SQLException ex) {
			logger.info("Fail to insert keesing row due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_INSERT);
		} finally {
			closeConnection(conn);
		}
	}	
	
	public static File loadKeesingFileForSend(long fileId) {
		Connection con = null;
		File file = null;
		try {
			con = getConnection();
			file = KeesingDAO.loadKeesingFileForSend(con, fileId);
		} catch (SQLException e) {
			logger.error("When loadKeesingFileForSend ", e);
		} finally {
			closeConnection(con);
		}

		return file;
	}
	
	public static File markKeesingFileFailed(long fileId) throws SQLException {
	    Connection con = getConnection();
		try {
			return KeesingDAO.markKeesingFileFailed(con, fileId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<IdsFiles> loadKeesingFileForApprove(long pageNumber, long rowsPerPage, 
			  ArrayList<Integer> uploadedBy, int uploadedByBackend, Long businessCase, ArrayList<Integer> skins, ArrayList<Integer> countries,
			  Long ksStatus, Long userId, String userName, Long userClassType) throws SQLException {
	    Connection con = getConnection();
		try {
			return KeesingDAO.loadKeesingFileForApprove(con, pageNumber, rowsPerPage,
					uploadedBy, uploadedByBackend, businessCase, skins, countries,
					ksStatus, userId, userName, userClassType);
		} finally {
			closeConnection(con);
		}
	}
}
