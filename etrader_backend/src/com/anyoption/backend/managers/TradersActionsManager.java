package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.TradersActionsDAO;
import com.anyoption.backend.service.tradersactions.TradersActionsMethodRequest;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.bl_vos.TradersActions;
import il.co.etrader.backend.dao_managers.MarketsManager;

public class TradersActionsManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(TradersActionsManager.class);

	public static final String FILTER_TRADERS = "traders";
	public static final String FILTER_ACTIONS = "actions";
	public static final String FILTER_ASSETS = "assets";
	
	private static LocalDate dateMarketGroups;

	public static final String[] filterConstants = {"", FILTER_TRADERS, FILTER_ACTIONS};

	private static HashMap<Long, String> traders;
	private static HashMap<Long, String> actions;
	private static HashMap<Long, String> assets;
	
	private static HashMap<String, HashMap<Long, String>> filters;
	
	public static HashMap<String, HashMap<Long, String>> getFilters() {
		if (filters == null) {
			Connection con = null;
			try {
				con = getConnection();
				filters = TradersActionsDAO.getFilters(con);
			} catch (SQLException e) {
				logger.error("Can't lazy load all filters: ", e);
			} finally {
				closeConnection(con);
			}
		}
		return filters;
	}
	
	public static HashMap<Long, String> getTraders() {
		if (traders == null) {
			traders = getFilters().get(FILTER_TRADERS);
		}
		return traders;
	}

	public static HashMap<Long, String> getActions() {
		if (actions == null) {
			HashMap<Long, String> tmp = getFilters().get(FILTER_ACTIONS);
			actions = new HashMap<Long, String>();
			for (Map.Entry<Long, String> entry : tmp.entrySet()) {
				String s = entry.getValue();
				s = s.split(":")[1];
				s = s.substring(1, 2).toUpperCase() + s.substring(2);
				actions.put(entry.getKey(), s);
			}
		}
		return actions;
	}
	
	public static HashMap<Long, String> getAssets() {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == dateMarketGroups || currentDate.isAfter(dateMarketGroups) || null == assets) {
			try {
				con = getConnection();
				if (null == assets) {
					assets = new HashMap<Long, String>();
				}
				for (Map.Entry<Long, MarketGroup> entry : MarketsManager.getMarketsByGroups().entrySet()) {
					ArrayList<Market> markets = entry.getValue().getMarkets();
					for (Market m : markets) {
						assets.put(m.getId(), m.getDisplayName());
					}
				}
				dateMarketGroups = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load assets", sqle);
			} finally {
				closeConnection(con);
			}
		}
		return assets;
	}

	public static ArrayList<TradersActions> getTradersActions(TradersActionsMethodRequest request, ArrayList<Integer> commands)
			throws SQLException {
		Connection conn = null;
		if (null == commands) {
			return null;
		}
		try {
			conn = getConnection();
			return TradersActionsDAO.getTradersActions(conn, commands, request);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void refreshUsersCache(long writerId) {
		if (traders != null && !traders.containsKey(writerId)) {
			traders = null;
		}
	}
}
