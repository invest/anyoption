package com.anyoption.backend.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.daos.MailBoxUsersDAOBase;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.MailBoxTemplatesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 */
public class CapInternalMailerJob extends BaseBLManager implements ScheduledJob {
    private static Logger log = Logger.getLogger(CapInternalMailerJob.class);
    HashMap<String, String> jobProperties;
    private boolean isTestRun = false;
    
    @Override
    public void setJobInfo(Job job) {
    	jobProperties = new HashMap<String, String>();
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
            	String[] p = ps[i].split("=");
            	jobProperties.put(p[0], p[1]);
            }
		}
		String test = jobProperties.get("test");
		isTestRun = test != null && test.equalsIgnoreCase("true");
    }
    
	@Override
	public void stop() {
		
	}

    /**
     * Capture Withdrawal Job.
     * @param args
     */
	@Override
	public boolean run() {
        log.info("Starting CapInternalMailer job.");
        
        sendCapMails();
        
        log.info("CapInternalMailer Job completed.");
        return true;
    }
  
	private void sendCapMails() {
		Connection conn1 = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn1 = getConnection();
            ClearingManager.loadClearingProviders(conn1);
			String sql = 
					"SELECT * FROM " +
					" ( " +
						" SELECT " + 
					          "ur.user_id usr, " +
					          "TRUNC(SYSDATE) - TRUNC(questionnaire_done_date) AS days_since_quest " +
					  "FROM  " +
					          "users_regulation ur " +
					  "WHERE  " +
					          "ur.approved_regulation_step = 3 " +
					          "AND ur.questionnaire_done_date IS NOT NULL " +
					          "AND NOT EXISTS(  " +
					        		  			"SELECT ID FROM files f " + 
					        		  			"WHERE  " +
					        		  			"f.user_id = ur.user_id " + 
					        		  			"and f.is_current = 1 " +
					        		  			"and (f.file_type_id = 1 OR f.file_type_id = 2) " + // ID and Passport 
					        		  		") " +
					") " +
					"WHERE days_since_quest IN (3,6,10,13,14) ORDER BY usr ";


			pstmt = conn1.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				long userId = rs.getLong("usr");
				int days = rs.getInt("days_since_quest");
				
				 UserBase uBase = new UserBase();
				 UsersDAOBase.getByUserId(conn1, userId, uBase);
				 int templateId = 0;
				 switch(days) {
				 	case 3:
				 		templateId = Template.CAP_INTERNAL_MAIL_D3;
				 	break;
				 	case 6:
				 		templateId = Template.CAP_INTERNAL_MAIL_D6;
				 	break;
				 	case 10:
				 		templateId = Template.CAP_INTERNAL_MAIL_D10;
				 	break;
				 	case 13:
				 		templateId = Template.CAP_INTERNAL_MAIL_D13;
				 	break;
				 	case 14:
				 		templateId = Template.CAP_INTERNAL_MAIL_D14;
				 	break;
				 	default:
				 		log.error("Unexpected days since:"+days);
				 		templateId = Template.CAP_INTERNAL_MAIL_D3;
				 	break;
				 }
				 Template t = TemplatesDAO.get(conn1, templateId);
				 if(t != null) {
					MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(conn1, t.getMailBoxEmailType() , uBase.getSkinId(), uBase.getLanguageId());
					ArrayList<MailBoxUser> sentMail = MailBoxUsersDAOBase.getUserMailByTemplate(conn1, userId, template.getId());
					if(sentMail.size() == 0 ) {
						UsersManagerBase.sendToMailBox(conn1, uBase, template, 0l/*Auto*/, null, 0);
					} else {
						log.debug(" Already send mail to: " + userId + " templateId :" + template.getId());
					}
				 } else {
					 log.error("Can't load template :" + templateId);
				 }
             }
        } catch (Exception e) {
            log.error("Error sending CAP mail", e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn1.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
    }


}