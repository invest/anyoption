package com.anyoption.backend.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.FibonatixClearingProvider;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.UsersManagerBase;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;

public class FibonatixFinishJob  extends BaseBLManager implements ScheduledJob {
    private static Logger log = Logger.getLogger(FibonatixFinishJob.class);
    Long transactionId = null;
    boolean isTestRun = false;
    
	@Override
	public void setJobInfo(Job job) {
		HashMap<String, String> jobProperties = new HashMap<String, String>();
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
            	String[] p = ps[i].split("=");
            	jobProperties.put(p[0], p[1]);
            }
		}
		String test = jobProperties.get("test");
		try {
			transactionId = Long.parseLong(jobProperties.get("transactionId"));
		} catch (Exception e) {
			transactionId = null;
		}
		

		isTestRun = test != null && test.equalsIgnoreCase("true");
		
	}

	@Override
	public boolean run() {
		return finishTransactions();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}

	
	private boolean finishTransactions() {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//			conn = getConnection();
//            ClearingManager.loadClearingProviders(conn);
//			String sql = 
//					"SELECT * " +
//							"FROM transactions " + 
//							"WHERE " +
//							 "clearing_provider_id in (63, 64) " + // fibonatix ids
//							 "AND status_id = 10 " + // auth
//							 "AND type_id = 1 " + // credit card deposit
//							 "AND time_created < (sysdate-1/24) " + // more than an hour - not polling
//							 " order by time_created "; 
//			
//			pstmt = conn.prepareStatement(sql);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {

//				try {
//					long clearingProviderId  = rs.getLong("clearing_provider_id");
//					
//					FibonatixClearingProvider fibonatix =  (FibonatixClearingProvider) ClearingManager.getClearingProviders().get(clearingProviderId);
//					ClearingInfo info = new ClearingInfo();
//					info.setTransactionId(rs.getLong("id"));
//					info.setAcquirerResponseId(rs.getString("acquirer_response_id"));
//					info.setAuthNumber(rs.getString("auth_number"));
//					fibonatix.getStatus(info);
//					
//					Transaction t = TransactionsManagerBase.getTransaction(info.getTransactionId());
//	        		String utcOffset = UsersManagerBase.getUserUtcOffset(t.getUserId()); 
//					String message = info.isSuccessful() ? info.getAdditionalRequestParams() : info.getMessage();
//					TransactionsManagerBase.finishFibonatixTransaction(t, Writer.WRITER_ID_AUTO, utcOffset, info.getAcquirerResponseId(), message, info.isSuccessful());
//				} catch(Throwable t) {
//					log.error("Failed to get fibonatix status", t);
//				}
//			}
			return true;
//		} catch (Throwable t) {
//			log.error("Failed to finish fibonatix trx", t);
//			return false;
//		} finally {
//			closeConnection(conn);
//		}
	}
}
