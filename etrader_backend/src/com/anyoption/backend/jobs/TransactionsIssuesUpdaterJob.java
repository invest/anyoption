package com.anyoption.backend.jobs;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionsIssuesManager;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.Utils;

import il.co.etrader.backend.helper.UserForFirstDepositEmail;
import il.co.etrader.util.CommonUtil;

public class TransactionsIssuesUpdaterJob implements ScheduledJob {

    private static final Logger log = Logger.getLogger(TransactionsIssuesUpdaterJob.class); 

    private String emailFrom;
    private String emailSubject;
    private String emailToEtrader;
    private String emailToAnyoption;
    private String emailToAnyoptionZh;

    public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("email_from")) {
                	emailFrom = p[1].toString();                	
                } else if (p[0].equals("email_subject")) {
                	emailSubject = p[1].toString();
                } else if (p[0].equals("email_to_anyoption")) {
                	emailToAnyoption = p[1].toString();
                } else if (p[0].equals("email_to_anyoptionZh")) {
                	emailToAnyoptionZh = p[1].toString();
                }
            }
		}
    }

	public boolean run() {
		long beginTime = System.currentTimeMillis();    		
    	log.debug("============ BEGIN TransactionsIssuesUpdaterJob ====================="); 

    	try {
           	TransactionsIssuesManager.transactionsIssuesFill();
           	TransactionsIssuesManager.transactionsIssuesCycleFill();
           	sendEmail();
        } catch (Exception e) {
            log.error("Failed to update Transactions Issues.", e);
            return false;
        }
	        
       	long endTime = System.currentTimeMillis();   	
       	log.debug("============ FINISHED TransactionsIssuesUpdaterJob" + " in:" + (endTime - beginTime) + "ms =====================");
        return true;	
	}

    public void stop() {
        log.info("TransactionsIssuesUpdaterJob stop");
    }
    
    private void sendEmail() throws SQLException, ParseException {
    	ArrayList<UserForFirstDepositEmail> users = TransactionsIssuesManager.getPastFiveMinutesTransactionsUsers();
    	log.info("Got all the users from DB And found " + users.size() + " users");
        
    	log.info("Start inserting server properties data into hashtable");
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", Utils.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", Utils.getProperty("email.uname"));
		serverProperties.put("pass", Utils.getProperty("email.pass"));
        log.info("Finished to insert server properties data to the hashtable");
        
    	for(UserForFirstDepositEmail u : users) {
	        log.info("Start inserting email properties data into hashtable");
	        // Set the email properties
	        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
	        emailProperties.put("subject", emailSubject + " - " + u.displaySkin(u.getSkinId()));
	        emailProperties.put("from", emailFrom);
	        if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
	        	if (u.getSkinId() == Skin.SKIN_CHINESE) {
	        		log.info("Send to Anyoption ZH reps");
	        		emailProperties.put("to", emailToAnyoptionZh);
	        	} else {
	        		log.info("Send to Anyoption reps");
	        		emailProperties.put("to", emailToAnyoption);
	        	}
	        } else {
	        	log.info("Send to Etrader reps");
	        	emailProperties.put("to", emailToEtrader);
	        }
	        emailProperties.put("body", u.toString());
	        log.info("Finished to insert email properties data to the hashtable");
	        
	        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    	}
    	
    }
    
}
