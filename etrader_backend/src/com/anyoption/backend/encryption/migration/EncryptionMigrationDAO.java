package com.anyoption.backend.encryption.migration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.anyoption.backend.encryption.migration.EncryptionMigrationManager.EncriptionMigrationCreditCard;
import com.anyoption.backend.encryption.migration.EncryptionMigrationManager.EncryptionMigrationUser;
import com.anyoption.common.daos.DAOBase;

/**
 * @author kiril.mutafchiev
 */
class EncryptionMigrationDAO extends DAOBase {

	static List<EncryptionMigrationUser> getUsersForMigration(Connection con, int limit, EncryptionMigrationManager manager) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<EncryptionMigrationUser> result = new ArrayList<>();
    	try {
    		String sql = "select u.id, u.password, u.id_num "
    					+ "from users u "
    					+ "where not exists (select id from encryption_migration_users emu where emu.user_id = u.id) and rownum <= ?";
    		ps = con.prepareStatement(sql);
    		ps.setInt(1, limit);
    		rs = ps.executeQuery();
    		while (rs.next()) {
    			EncryptionMigrationUser user = manager. new EncryptionMigrationUser();
    			result.add(user);
    			user.setId(rs.getLong("id"));
    			user.setOldPassword(rs.getString("password"));
    			user.setOldIdNum(rs.getString("id_num"));
    		}
    	} finally {
			closeResultSet(rs);
        	closeStatement(ps);
		}
    	return result;
	}

	static List<EncriptionMigrationCreditCard> getCreditCardsForMigration(	Connection con, int limit,
																					EncryptionMigrationManager manager) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<EncriptionMigrationCreditCard> result = new ArrayList<>();
    	try {
    		String sql = "select cc.id, cc.cc_number "
    					+ "from credit_cards cc "
    					+ "where not exists (select id from encryption_migration_c_cards emcc where emcc.cc_id = cc.id) and rownum <= ?";
    		ps = con.prepareStatement(sql);
    		ps.setInt(1, limit);
    		rs = ps.executeQuery();
    		while (rs.next()) {
    			EncriptionMigrationCreditCard card = manager. new EncriptionMigrationCreditCard();
    			result.add(card);
    			card.setId(rs.getLong("id"));
    			card.setOldCcNum(rs.getString("cc_number"));
    		}
    	} finally {
			closeResultSet(rs);
        	closeStatement(ps);
		}
    	return result;
	}

	static void migrate(Connection con, EncryptionMigrationUser user) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "insert /*+ APPEND */ into encryption_migration_users (id, user_id, old_password, new_password, old_id_num, new_id_num) "
																+ "values (?, ?, ?, ?, ?, ?)";
			long id = getSequenceNextVal(con, "SEQ_ENCRYPTION_MIGRATION_USERS");
			int index = 1;
			ps = con.prepareStatement(sql);
			ps.setLong(index++, id);
			ps.setLong(index++, user.getId());
			ps.setString(index++, user.getOldPassword());
			ps.setString(index++, user.getNewPassword());
			ps.setString(index++, user.getOldIdNum());
			ps.setString(index++, user.getNewIdNum());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	static void migrate(Connection con, EncriptionMigrationCreditCard card) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "insert /*+ APPEND */ into encryption_migration_c_cards (id, cc_id, old_cc_number, new_cc_number) values (?, ?, ?, ?)";
			long id = getSequenceNextVal(con, "SEQ_ENCRYPTION_MIGRATION_C_CRD");
			int index = 1;
			ps = con.prepareStatement(sql);
			ps.setLong(index++, id);
			ps.setLong(index++, card.getId());
			ps.setString(index++, card.getOldCcNum());
			ps.setString(index++, card.getNewCcNum());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}