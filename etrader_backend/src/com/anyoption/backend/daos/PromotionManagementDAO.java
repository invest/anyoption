package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.service.promotionmanagement.PromotionBannerSliderInsertUpdateRequest;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSlidersGetAllMethodRequest;
import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.bl_vos.PromotionBannerSliderImage;
import com.anyoption.common.daos.PromotionManagementDAOBase;

import il.co.etrader.backend.bl_vos.PromotionBanner;
import oracle.jdbc.OracleTypes;

public class PromotionManagementDAO extends PromotionManagementDAOBase{
	
	public static HashMap<Long, String> getBannersList(Connection conn) throws SQLException {
		HashMap<Long, String> bannersList = new HashMap<Long, String>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_promotion_management.get_all_banners(?)}"; 
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				bannersList.put(rs.getLong("id"), rs.getString("name"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return bannersList;
	}
	
	public static HashMap<Long, PromotionBanner> getBannerDimensions(Connection conn) throws SQLException {
		HashMap<Long, PromotionBanner> banners = new HashMap<Long, PromotionBanner>();
		PromotionBanner banner = new PromotionBanner();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_promotion_management.get_all_banners(?)}"; 
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				loadBannerVO(rs, banner);
				banners.put(rs.getLong("id"), banner);
			}
		} finally {
			closeStatement(cstmt);
		}
		return banners;
	}
	
	private static void loadBannerVO(ResultSet rs, PromotionBanner banner) throws SQLException {
		banner.setId(rs.getLong("id"));
		banner.setName(rs.getString("name"));
		banner.setWidth(rs.getInt("width_pixels"));
		banner.setHeight(rs.getInt("height_pixels"));
	}
	
	public static HashMap<Long, String> getBannerSliderTypeList(Connection conn) throws SQLException {
		HashMap<Long, String> bannerSliderTypes = new HashMap<Long, String>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_promotion_management.get_all_slider_link_types(?)}"; 
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				bannerSliderTypes.put(rs.getLong("id"), rs.getString("name"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return bannerSliderTypes;
	}
	
	public static ArrayList<PromotionBannerSlider> getAllSliders(Connection conn, PromotionBannerSlidersGetAllMethodRequest request) throws SQLException {
		ArrayList<PromotionBannerSlider> bannerSliders = new ArrayList<PromotionBannerSlider>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.get_banner_sliders(o_data => ? " + 
																   ",i_banner_id => ? " + 
																   ",i_is_active => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			cstmt.setLong(index++, request.getBannerId());
			setStatementValue(request.isActive(), index++, cstmt);
			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);

			while (rs.next()) {
				PromotionBannerSlider vo = new PromotionBannerSlider();
				vo.setBannerId(request.getBannerId());
				loadBannerSliderVO(rs, vo);
				bannerSliders.add(vo);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return bannerSliders;
	}	
	
	protected static void loadBannerSliderVO(ResultSet rs, PromotionBannerSlider bannerSlider) throws SQLException {
		bannerSlider.setId(rs.getLong("slider_id"));
		bannerSlider.setPosition(rs.getInt("position"));
		bannerSlider.setActive(rs.getLong("is_active") == 1 ? true : false);
		bannerSlider.setDefaultImageName(rs.getString("image_name"));	
		bannerSlider.setLinkTypeId(rs.getLong("link_type_id"));
	}
	
	public static PromotionBannerSlider insertSlider(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		PromotionBannerSlider bannerSlider = new PromotionBannerSlider();
		CallableStatement cstmt = null;
		Integer rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.create_banner_slider(o_slider_id => ? " +
																	  ",i_banner_id => ? " +
																	  ",i_writer_id => ? " +
																	  ",i_link_type_id => ? )}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			
			setStatementValue(request.getBannerId(), index++, cstmt);
			setStatementValue(request.getWriterId(), index++, cstmt);
			cstmt.setInt(index++, request.getLinkTypeId());

			cstmt.execute();
			
			rs =  (Integer) cstmt.getObject(1);
			bannerSlider.setId(rs.longValue());
		} finally {
			closeStatement(cstmt);
		}
		return bannerSlider;
	}
	
	public static void insertSliderImage(PromotionBannerSliderImage sliderImage, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.create_banner_slider_image(" + 
																	  " i_banner_slider_id => ? " +
																	  ",i_language_id => ? " +
																	  ",i_link_url => ? " +
																	  ",i_image_name => ? " +
																	  ",i_writer_id => ? )}");

			setStatementValue(sliderImage.getBannerSliderId(), index++, cstmt);
			setStatementValue(sliderImage.getLanguageId(), index++, cstmt);
			setStatementValue(sliderImage.getImageUrl(), index++, cstmt);
			setStatementValue(sliderImage.getImageName(), index++, cstmt);
			setStatementValue(sliderImage.getWriterId(), index++, cstmt);
	
			cstmt.execute();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void disableBannerSlider(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.disable_banner_slider(" + 
																	  " i_slider_id => ? )}");

			setStatementValue(request.getBannerSliderId(), index++, cstmt);
	
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void enableBannerSlider(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.enable_banner_slider(" + 
																	  " i_slider_id => ? )}");

			setStatementValue(request.getBannerSliderId(), index++, cstmt);
	
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void deleteBannerSlider(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.delete_banner_slider(" + 
																	  " i_slider_id => ? )}");

			setStatementValue(request.getBannerSliderId(), index++, cstmt);
	
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateSliderImage(PromotionBannerSliderImage sliderImage, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.update_slider_image(" + 
																	  " i_slider_image_id => ? " +
																	  ",i_link_url => ? " +
																	  ",i_image_name => ? " +
																	  ",i_writer_id => ? )}");

			setStatementValue(sliderImage.getId(), index++, cstmt);
			setStatementValue(sliderImage.getImageUrl(), index++, cstmt);
			setStatementValue(sliderImage.getImageName(), index++, cstmt);
			setStatementValue(sliderImage.getWriterId(), index++, cstmt);
	
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>> getAllSliderImages(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		ArrayList<PromotionBannerSliderImage> sliderImages = new ArrayList<PromotionBannerSliderImage>();
		PromotionBannerSlider slider = new PromotionBannerSlider();
		HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>> hm = new HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.get_slider_images(o_data => ? " + 
																					  ",i_slider_id => ? " +
																					  ",i_language_id => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			setStatementValue(request.getBannerSliderId(), index++, cstmt);
			cstmt.setNull(index++, OracleTypes.NULL);
			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);

			while (rs.next()) {
				PromotionBannerSliderImage vo = new PromotionBannerSliderImage();
				loadBannerSliderImageVO(rs, vo);
				sliderImages.add(vo);
				slider.setLinkTypeId(rs.getInt("link_type_id"));
				slider.setBannerId(rs.getInt("banner_id"));
				
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		hm.put(slider, sliderImages);
		return hm;
	}
	
	public static void updateBannerSliderPosition(Long id, Integer newPosition, Long writerId, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.change_slider_pos(" + 
																	  " i_slider_id => ? " +
																	  ",i_new_position => ?" +
																	  ",i_writer_id => ?)}");

			setStatementValue(id, index++, cstmt);
			cstmt.setInt(index++, newPosition);
			setStatementValue(writerId, index++, cstmt);
			
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateBannerSliderType(PromotionBannerSliderInsertUpdateRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_promotion_management.update_slider_type(" + 
																	  " i_banner_slider_id => ? " +
																	  ",i_link_type_id => ? )}");

			setStatementValue(request.getBannerSliderId(), index++, cstmt);
			cstmt.setInt(index++, request.getLinkTypeId()); 
						
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}

}
