package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.backend.helper.UserForFirstDepositEmail;
import oracle.jdbc.OracleTypes;

public class TransactionsIssuesDAO extends DAOBase {

	public static final long HALF_AN_HOUR_TO_ADD_TO_CREATION_TIME = 1/48;
	public static final long LAST_FIRST_DEPOSIT_EMAIL_RUN = 11;
	public static final long CLASS_ID_NOT_ACTIVE = 0;
	private static final Logger logger = Logger.getLogger(TransactionsIssuesDAO.class);

	public static void transactionsIssuesFill(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN TRANSACTIONS_ISSUES_FILL(1); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static void transactionsIssuesCycleFill(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN TRANSACTIONS_ISSUES_CYCLE_FILL(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static ArrayList<UserForFirstDepositEmail> getPastFiveMinutesTransactionsUsers(Connection con) throws SQLException, ParseException {
    	ArrayList<UserForFirstDepositEmail> users = new ArrayList<UserForFirstDepositEmail>();
		CallableStatement cs = null;
        ResultSet rs = null;
        
        logger.info("Start query");
        try {
        	String proc = "{call pkg_transactions.get_last_trnsusers(o_data => ?)}";

        	cs = con.prepareCall(proc);
        	cs.registerOutParameter(1, OracleTypes.CURSOR);
        	cs.execute();
        	
        	rs = (ResultSet) cs.getObject(1);
        			
        	logger.info("Execute query");
        	while(rs.next()) {
        		UserForFirstDepositEmail vo = new UserForFirstDepositEmail();
        		vo.setUserId(rs.getLong("id"));
        		vo.setUserName(rs.getString("user_name"));
        		vo.setSkinId(rs.getInt("skin_id"));
        		vo.setMobilePhone(rs.getString("mobile_phone"));
        		vo.setLandLinePhone(rs.getString("land_line_phone"));
        		vo.setFirstDepAmount(rs.getLong("first_deposit_amount"));
        		vo.setCountryCode(rs.getLong("country_phone_code"));
        		vo.setCountryName(rs.getString("country_name"));
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        		vo.setFirstDepositTimeCreated(localDateFormat.parse(rs.getString("first_deposit_time_created")));
        		vo.setCurrencyId(rs.getLong("currency_id"));
        		users.add(vo);
        	}
        	logger.info("Found " + users.size() + " users");
        } finally {
			closeResultSet(rs);
        	closeStatement(cs);
        }
		return users;
	}

	public static void updateLastRun(Connection con) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE db_parameters " +
						 " SET date_value = sysdate " +
						 " WHERE id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, LAST_FIRST_DEPOSIT_EMAIL_RUN);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

}
