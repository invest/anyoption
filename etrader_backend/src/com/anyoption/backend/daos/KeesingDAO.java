/**
 * 
 */
package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.keesing.KeesingRequest;
import com.anyoption.backend.service.keesing.KeesingResponse;
import com.anyoption.common.beans.File;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.backend.bl_vos.IdsFiles;
import oracle.jdbc.OracleTypes;

/**
 * @author pavel.tabakov
 *
 */
public class KeesingDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(KeesingDAO.class);

	public static void insertResponse(KeesingResponse response, KeesingRequest request, Connection conn) throws SQLException {
		
		logger.info("Start inserting keesing row....................");
		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_userfiles.create_ks_record(i_file_id => ? "+
																		  ",i_first_name  => ? "+
																		  ",i_last_name => ? "+
																		  ",i_nationality => ? "+
																		  ",i_issuing_country => ? "+
																		  ",i_doc_number => ? "+
																		  ",i_expiry_date => ? "+
																		  ",i_personal_id => ? "+
																		  ",i_date_of_birth => ? "+
																		  ",i_gender => ? "+
																		  ",i_doc_id => ? "+
																		  ",i_check_status => ? "+
																		  ",i_reason_nok => ? "+
																		  ",i_response => ? )}");
			
					
					cstmt.setLong(index++, request.getFileId());//must
					
					cstmt.setString(index++, response.getFirstnames());
					cstmt.setString(index++, response.getLastname());
					cstmt.setString(index++, response.getNationality());
					cstmt.setString(index++, response.getIssuingCountry());
					cstmt.setString(index++, response.getDocumentNr());
					
					setDateVal(response.getExpireDate(), index++, cstmt);
					
					setIntegerVal(response.getPersID(), index++, cstmt);
					
					setDateVal(response.getDateOfBirth(), index++, cstmt);
					
					cstmt.setString(index++, response.getGender());
					
					setIntegerVal(response.getDocID(), index++, cstmt);
					cstmt.setLong(index++, response.getCheckStatusValue());//must
					setIntegerVal(response.getReasonNotOkValue(), index++, cstmt);
					
					cstmt.setString(index++, response.getXml());
					
					cstmt.execute();

			
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void setIntegerVal(Long value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.INTEGER);
		} else {
			cstmt.setLong(index, value);
		}
	}

	public static void setDateVal(Date value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.DATE);
		} else {
			cstmt.setTimestamp(index, convertToTimestamp(value));
		}
	}
	
	  public static File loadKeesingFileForSend(Connection con, long fileId) throws SQLException {
		  	File file = null;
			CallableStatement cstmt = null;
			try {
				String sql = "{call pkg_userfiles.load_ks_files_for_send(?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(1, Types.INTEGER);
				cstmt.registerOutParameter(2, Types.VARCHAR);
				if(fileId > 0){
					cstmt.setLong(1, fileId);
				}				
				else {
					cstmt.setNull(1, OracleTypes.INTEGER);
				}
				cstmt.execute();
				
				String fileName = cstmt.getString(2);
				if (fileName != null) {
					file = new File();
					file.setId(cstmt.getLong(1));
					file.setFileName(fileName);						
				}
			} finally {
				closeStatement(cstmt);			
			}
			return file;
	  }
	  
	  public static File markKeesingFileFailed(Connection con, long fileId) throws SQLException {
		  	File file = null;
			CallableStatement cstmt = null;
			int index = 1;
			try {
				String sql = "{call pkg_userfiles.mark_file_ks_failed(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.setLong(index++, fileId);
				cstmt.executeQuery();							
			} finally {
				closeStatement(cstmt);			
			}
			return file;
	  }
	  
	  public static ArrayList<IdsFiles> loadKeesingFileForApprove(Connection con, long pageNumber, long rowsPerPage, 
			  ArrayList<Integer> uploadedBy, int uploadedByBackend, Long businessCase, ArrayList<Integer> skins, ArrayList<Integer> countries,
			  Long ksStatus, Long userId, String userName, Long userClassType) throws SQLException {		  	
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			ArrayList<IdsFiles> idsFilesList = new ArrayList<>();
			try {
				String sql = "{call pkg_userfiles.load_ks_files_for_approve(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				
				cstmt.setLong(index++, pageNumber);
				cstmt.setLong(index++, rowsPerPage);
				//Upload By
				if(uploadedBy != null){
					cstmt.setArray(index++, getPreparedSqlArray(con, uploadedBy));
				} else {
					cstmt.setNull(index++, Types.ARRAY, "NUMBER_TABLE");					
				}				
				//BE Uplode
				if(uploadedBy == null && uploadedByBackend == 0){
					cstmt.setNull(index++, OracleTypes.INTEGER);
				} else {
					cstmt.setInt(index++, uploadedByBackend);
				}
				//businessCase
				if(businessCase != null){
					cstmt.setLong(index++, businessCase);
				} else{
					cstmt.setNull(index++, OracleTypes.INTEGER);
				}
				//skins
				if(skins != null){
					cstmt.setArray(index++, getPreparedSqlArray(con, skins));
				} else {
					cstmt.setNull(index++, Types.ARRAY, "NUMBER_TABLE");
				}
				//countries
				if(countries != null){
					cstmt.setArray(index++, getPreparedSqlArray(con, countries));
				} else {
					cstmt.setNull(index++, Types.ARRAY, "NUMBER_TABLE");
				}
				//ksStatus
				if(ksStatus != null){
					cstmt.setLong(index++, ksStatus);
				} else{
					cstmt.setNull(index++, OracleTypes.INTEGER);
				}
				if(userId != null && userId != 0){
					cstmt.setLong(index++, userId);
				} else{
					cstmt.setNull(index++, OracleTypes.INTEGER);
				}
				if(userName != null){
					cstmt.setString(index++, userName);
				} else {
					cstmt.setNull(index++, OracleTypes.VARCHAR);
				}
				if(userClassType != null){
					cstmt.setLong(index++, userClassType);
				} else{
					cstmt.setNull(index++, OracleTypes.INTEGER);
				}
				cstmt.executeQuery();
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					IdsFiles idF = new IdsFiles(rs.getLong("file_id"), rs.getLong("user_id"), rs.getString("user_name"), 
												rs.getString("first_name"), rs.getString("last_name"), rs.getString("country_name"), 
												rs.getString("skin_name"), convertToDate(rs.getTimestamp("time_uploaded")), 
												rs.getLong("check_status"), rs.getLong("regulated"), rs.getLong("total_count"), rs.getLong("skin_id"));
					idsFilesList.add(idF);
				}
			} finally {
				closeStatement(cstmt);			
			}
			return idsFilesList;
	  }
}
