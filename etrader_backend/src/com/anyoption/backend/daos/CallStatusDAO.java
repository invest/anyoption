package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.CallStatusManager;
import com.anyoption.backend.service.salescallstatusmanager.SalesCallStatusManagerMethodRequest;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.backend.bl_vos.CallsRepresentative;
import oracle.jdbc.OracleTypes;

public class CallStatusDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(CallStatusDAO.class);
	
	public static HashMap<String, HashMap<Long, String>> getFilters(Connection conn) throws SQLException {
		logger.info("Call Status DAO getFilters");
		HashMap<String, HashMap<Long, String>> filtersList = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_accret.load_filters(o_user_ranks => ? " +
																   ",o_representatives => ? " +
																   ",o_skins => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			cstmt.executeQuery();
			
			TransactionDAO.loadFilters(rs, cstmt, filtersList, CallStatusManager.filterConstants);
			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return filtersList;
	}
	
	public static ArrayList<CallsRepresentative> getSalesCallStatusRepresentatives(Connection conn,
			SalesCallStatusManagerMethodRequest request) throws SQLException {
		logger.info("Call Status DAO getSalesCallStatusManager");
		ArrayList<CallsRepresentative> representativesList = new ArrayList<CallsRepresentative>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			   
			cstmt = conn.prepareCall("{call pkg_accret.get_call_status(o_call_statuses => ? " +
																   ",i_fromdate => ? " +
																   ",i_todate => ? " +
																   ",i_userrank => ? " +
																   ",i_writer_id => ? " +  
																   ",i_skin_id => ? " + 
																   ",i_page_number => ? " + 
																   ",i_rows_per_page => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setTimestamp(index++, new Timestamp(request.getFromDate()));
			cstmt.setTimestamp(index++, new Timestamp(request.getToDate()));
			
			if (request.getUserRank() == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, request.getUserRank());
			}
			
			if (request.getWriterId() == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, request.getWriterId());
			}
			
			if (request.getSkinId() == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, request.getSkinId());
			}
			
			cstmt.setLong(index++, request.getPageNumber());
			cstmt.setLong(index++, request.getRowsPerPage());
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				long reached = rs.getLong("reached");
				long notReached = rs.getLong("not_reached");
				long notCalled = rs.getLong("not_called");
				CallsRepresentative representative = new CallsRepresentative();
				representative.setRepName(rs.getString("user_name"));
				representative.setCallsReached(reached);
				representative.setCallsNotReached(notReached);
				representative.setNotCalled(notCalled > 0 ? notCalled : 0);
				representative.setDailyAverage(rs.getLong("daily_avg"));
				representative.setTotalCount(rs.getLong("total_count"));
				representativesList.add(representative);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		
		return representativesList;
	}
}
