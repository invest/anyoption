package com.anyoption.backend.daos;

import il.co.etrader.bl_vos.CountryGroup;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.CountryManager;
import com.anyoption.backend.service.country.CountryGroupMethodRequest;
import com.anyoption.common.daos.CountryDAOBase;

/**
 * 
 * @author eyal.ohana
 *
 */
public class CountryDAO extends CountryDAOBase {
	private static final Logger logger = Logger.getLogger(CountryDAO.class);
	
	/**
	 * Get filters
	 * @param conn
	 * @return HashMap<String, HashMap<Long, String>>
	 * @throws SQLException
	 */
	public static HashMap<String, HashMap<Long, String>> getFilters(Connection conn) throws SQLException {
		HashMap<String, HashMap<Long, String>> filtersList = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_countries.load_filters(o_countries_group_types => ? "+
	   				  													",o_countries_group_tiers => ? " +
	   				  													",o_countries => ? )}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			TransactionDAO.loadFilters(rs, cstmt, filtersList, CountryManager.filterConstants);
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return filtersList;
	}
	
	/**
	 * Get country groups
	 * @param con
	 * @return ArrayList<CountryGroup>
	 * @throws SQLException
	 */
	public static ArrayList<CountryGroup> getCountryGroups(Connection con) throws SQLException {
		ArrayList<CountryGroup> countryGroups = new ArrayList<CountryGroup>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_countries.get_country_groups(o_countries_group => ? )}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				CountryGroup cg = new CountryGroup();
				loadCountryGroupVO(rs, cg);
				countryGroups.add(cg);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return countryGroups;
	}
	
	/**
	 * Load country group VO
	 * @param rs
	 * @param cg
	 * @throws SQLException
	 */
	protected static void loadCountryGroupVO(ResultSet rs, CountryGroup cg) throws SQLException {
		cg.setCountryId(rs.getLong("country_id"));
		cg.setCountriesGroupTypeId(rs.getLong("countries_group_type_id"));
		cg.setCountriesGroupTierId(rs.getLong("countries_group_tier_id"));
	}
	
	/**
	 * Update country group
	 * @param con
	 * @param request
	 * @throws SQLException
	 */
	public static void updateCountryGroup(Connection con, CountryGroupMethodRequest request) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_countries.update_country_group(" +
																				" i_cg_type_id => ? " +
																				",i_cg_tier_id => ? " +
																				",i_writer_id => ? " +
																				",i_country_id => ? )}");
			cstmt.setLong(index++, request.getCountryGroup().getCountriesGroupTypeId());
			cstmt.setLong(index++, request.getCountryGroup().getCountriesGroupTierId());
			cstmt.setLong(index++, request.getWriterId());
			cstmt.setLong(index++, request.getCountryGroup().getCountryId());
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
}
