package com.anyoption.backend.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.phoneconversioncallshistory.PhoneConversionCallsHistoryMethodRequest;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.IssuesManagerBase;

public class PhoneConversionCallsHistoryDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(PhoneConversionCallsHistoryDAO.class);

	public static HashMap<String, Object> getCallsHistory(Connection conn, PhoneConversionCallsHistoryMethodRequest request) throws SQLException {
		logger.info("Phone Conversion Calls History DAO : getCallsHistory");
		
		HashMap<String, Object> callsHistory = new HashMap<String, Object>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			String sql = " SELECT " +
								" w.user_name last_caller, " + 
								" last_issue_action_call.count_calls " +
						" FROM " + 
								" issue_actions ia " +
								" JOIN " +
										" (SELECT " +    
												" count(*) count_calls, " +
												" max(ia.id) last_ia " +
										" FROM " +
												" issues i " +
												" JOIN   issue_actions ia  ON ia.issue_id = i.id " +              
												" JOIN   issue_action_types iat ON iat.id = ia.ISSUE_ACTION_TYPE_ID " +
										" WHERE " + 
												" iat.channel_id = ? " + 
												" AND (i.contact_id = ? or i.user_id = ?) " +
										" )    last_issue_action_call  on last_issue_action_call.last_ia = ia.id, " + 
								" writers w " +
						" WHERE " +  
								" w.id = ia.writer_id ";
			
			ps = conn.prepareStatement(sql);

			ps.setLong(index++, IssuesManagerBase.ISSUE_CHANNEL_CALL);
			if(request.getContactId() == 0) { 
				ps.setLong(index++ , -1);
			} else {
				ps.setLong(index++ , request.getContactId());
			} 
			if(request.getUserId() == 0) {
				ps.setLong(index++, -1);
			} else {
				ps.setLong(index++ , request.getUserId());
			}
			rs = ps.executeQuery();
			
			if(rs.next()) {
				callsHistory.put("lastCaller", rs.getString("last_caller"));
				callsHistory.put("callsCount", rs.getLong("count_calls"));
			} else {
				callsHistory.put("lastCaller", "None");
				callsHistory.put("callsCount", new Long(0));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return callsHistory;
	}
}
