/**
 * 
 */
package com.anyoption.backend.service.keesing;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class KeesingRequest extends UserMethodRequest {

	public KeesingRequest(long fileId, String filePath, String fileName) {
		super();
		this.fileId = fileId;
		this.filePath = filePath;
		this.fileName = fileName;
	}

	private long fileId;
	private String filePath;
	private String fileName;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
		
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "KeesingRequest: " + ls + super.toString() + ls + "filePath: " + filePath + ls + "fileName: "
				+ fileName + ls + "fileId: " + fileId + ls;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

}
