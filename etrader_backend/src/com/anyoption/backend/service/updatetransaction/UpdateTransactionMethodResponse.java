package com.anyoption.backend.service.updatetransaction;

import java.util.Map;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.service.results.MethodResult;

public class UpdateTransactionMethodResponse extends MethodResult {
	
	private Transaction transaction;
	private Map<Long, String> statuses;
	private Map<Long, String> types;
	
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Map<Long, String> getStatuses() {
		return statuses;
	}

	public void setStatuses(Map<Long, String> statuses) {
		this.statuses = statuses;
	}
	
	public Map<Long, String> getTypes() {
		return types;
	}

	public void setTypes(Map<Long, String> types) {
		this.types = types;
	}
}
