package com.anyoption.backend.service.updatetransaction;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.transaction.TransactionGetAllMethodRequest;
import com.anyoption.backend.service.transaction.TransactionServices;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.bl_managers.ApplicationDataBase;

public class UpdateTransactionServices {

	private static final Logger log = Logger.getLogger(UpdateTransactionServices.class);
	private static final String SCREEN_NAME_UPDATE_TRANSACTION = "updateTransaction";
	
	public static ScreenSettingsResponse getUpdateTransactionScreenSettings(UserMethodRequest request,
			HttpServletRequest httpRequest) {
		log.info("Method getUpdateTransactionScreenSettings");
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_UPDATE_TRANSACTION, response,
				httpRequest);
		return response;
	}
	
	@BackendPermission (id = "updateTransaction_view")
	public static UpdateTransactionMethodResponse getTransaction(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		UpdateTransactionMethodResponse response = new UpdateTransactionMethodResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		ArrayList<Transaction> tmp = TransactionServices.getAll(request, httpRequest).getTransactions();
		if (tmp.isEmpty()) {
			return response;
		} else {
			Transaction t = tmp.get(0);
			if (TransactionManager.getExcludedTypes().get(t.getTypeId()) != null) {
				return response;
			} else if (TransactionManager.getExcludedStatuses().get(t.getStatusId()) != null) {
				return response;
			} else {
				if (t.getTypeId() == 17) {
					if (t.getPaymentTypeId() == 2) {
						t.setTypeName("transactions.direct24.withdraw");
					}
					if (t.getPaymentTypeId() == 3) {
						t.setTypeName("transactions.giropay.withdraw");
					}
					if (t.getPaymentTypeId() == 4) {
						t.setTypeName("transactions.eps.withdraw");
					}
				}
				t.setCountryName(ApplicationDataBase.getCountry(t.getCountryId()).getDisplayName());
				response.setTransaction(t);
				TransactionManager.getTransactionChanges(request, response);
				t.setStatusName(TransactionManager.getAllTransactionStatuses().get(t.getStatusId()));
				if (t.getTypeId() != 17) {
					t.setTypeName(TransactionManager.getAllTransactionTypes().get(t.getTypeId()));
				}
			}
		}
		return response;
	}
	
	@BackendPermission (id = "updateTransaction_view")
	public static UpdateTransactionMethodResponse updateTransaction(UpdateTransactionMethodRequest request, HttpServletRequest httpRequest) {
		UpdateTransactionMethodResponse response = new UpdateTransactionMethodResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		try {
			Transaction t = TransactionsManager.getTransaction(request.getTransactionId());
			if (null != request.getTypeId() && t.getTypeId() != request.getTypeId().longValue()) {
					TransactionManager.updateTransactionChange(request, t);					
			} else if (null != request.getStatusId() && t.getStatusId() != request.getStatusId().longValue()) {
				TransactionManager.updateTransactionChange(request, t);
				response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
			}			
		} catch (SQLException e) {
			log.error("Error while updating transaction changes", e);
		}
		return response;
	}
}
