package com.anyoption.backend.service.chargebacks;

import java.io.Serializable;

public class Chargeback implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long chargebackId;
	private long userId;
	private long statusId;
	private String statusName;
	private long amount;
	private long currencyId;
	private long chargebackDate;
	private long caseId;
	private String rc;
	private long writerId;
	private String writerName;
	private long deadline;
	private String provider;
	private long totalCount;
	private long transactionId;
	private boolean is3d;
	
	public long getChargebackId() {
		return chargebackId;
	}
	
	public void setChargebackId(long chargebackId) {
		this.chargebackId = chargebackId;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getStatusId() {
		return statusId;
	}
	
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}
	
	public String getStatusName() {
		return statusName;
	}
	
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public long getCurrencyId() {
		return currencyId;
	}
	
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	
	public long getChargebackDate() {
		return chargebackDate;
	}
	
	public void setChargebackDate(long chargebackDate) {
		this.chargebackDate = chargebackDate;
	}
	
	public long getCaseId() {
		return caseId;
	}
	
	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}
	
	public String getRc() {
		return rc;
	}
	
	public void setRc(String rc) {
		this.rc = rc;
	}
	
	public long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public String getWriterName() {
		return writerName;
	}
	
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	
	public long getDeadline() {
		return deadline;
	}
	
	public void setDeadline(long deadline) {
		this.deadline = deadline;
	}
	
	public String getProvider() {
		return provider;
	}
	
	public void setProvider(String provider) {
		this.provider = provider;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isIs3d() {
		return is3d;
	}

	public void setIs3d(boolean is3d) {
		this.is3d = is3d;
	}
}