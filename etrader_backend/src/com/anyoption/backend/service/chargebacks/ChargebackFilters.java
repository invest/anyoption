package com.anyoption.backend.service.chargebacks;

import java.util.ArrayList;

public class ChargebackFilters {

	private Long fromDate;
	private Long toDate;
	private Long transactionId;
	private Long userId;
	private String arn;
	private String last4Digits;
	private Long userClasses;
	private ArrayList<Integer> businessCases;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private ArrayList<Integer> currencies;
	private ArrayList<Integer> statuses;
	private ArrayList<Integer> providers;
	private ArrayList<Integer> rc;
	private Long page;
	private long rowsPerPage;
	private boolean otherRC;
	
	public Long getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(Long fromDate) {
		this.fromDate = fromDate;
	}
	
	public Long getToDate() {
		return toDate;
	}
	
	public void setToDate(Long toDate) {
		this.toDate = toDate;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getArn() {
		return arn;
	}
	
	public void setArn(String arn) {
		this.arn = arn;
	}
	
	public String getLast4Digits() {
		return last4Digits;
	}
	
	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}
	
	public Long getUserClasses() {
		return userClasses;
	}
	
	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}
	
	public ArrayList<Integer> getBusinessCases() {
		return businessCases;
	}
	
	public void setBusinessCases(ArrayList<Integer> businessCases) {
		this.businessCases = businessCases;
	}
	
	public ArrayList<Integer> getSkins() {
		return skins;
	}
	
	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	
	public ArrayList<Integer> getCurrencies() {
		return currencies;
	}
	
	public void setCurrencies(ArrayList<Integer> currencies) {
		this.currencies = currencies;
	}
	
	public ArrayList<Integer> getStatuses() {
		return statuses;
	}
	
	public void setStatuses(ArrayList<Integer> statuses) {
		this.statuses = statuses;
	}
	
	public ArrayList<Integer> getProviders() {
		return providers;
	}
	
	public void setProviders(ArrayList<Integer> providers) {
		this.providers = providers;
	}
	
	public ArrayList<Integer> getRc() {
		return rc;
	}
	
	public void setRc(ArrayList<Integer> rc) {
		this.rc = rc;
	}
	
	public Long getPage() {
		return page;
	}
	
	public void setPage(Long page) {
		this.page = page;
	}
	
	public long getRowsPerPage() {
		return rowsPerPage;
	}
	
	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	
	public boolean isOtherRC() {
		return otherRC;
	}

	public void setOtherRC(boolean otherRC) {
		this.otherRC = otherRC;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "ChargebackFilters: "
            + super.toString()
            + "fromDate: " + fromDate + ls
            + "toDate: " + toDate + ls
            + "transactionId: " + transactionId + ls
            + "userId: " + userId + ls
            + "arn: " + arn + ls
            + "last4Digits: " + last4Digits + ls
            + "userClasses: " + userClasses + ls
            + "businessCases: " + businessCases + ls
            + "skins: " + skins + ls
            + "countries: " + countries + ls
            + "currencies: " + currencies + ls
            + "statuses: " + statuses + ls
            + "providers: " + providers + ls
            + "rc: " + rc + ls
            + "page: " + page + ls
            + "rowsPerPage: " + rowsPerPage + ls
        	+ "otherRC: " + otherRC + ls;
	}
}