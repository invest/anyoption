package com.anyoption.backend.service.chargebacks;

import com.anyoption.common.service.requests.UserMethodRequest;

public class ChargebackFiltersRequest extends UserMethodRequest {

	private ChargebackFilters chargebackFilters;

	public ChargebackFilters getChargebackFilters() {
		return chargebackFilters;
	}

	public void setChargebackFilters(ChargebackFilters chargebackFilters) {
		this.chargebackFilters = chargebackFilters;
	}
}