package com.anyoption.backend.service.chargebacks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.UsersManagerBase;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class ChargeBacksManager extends BaseBLManager {
	
	public static final String FILTER_STATUSES = "statuses";
	public static final String FILTER_PROVIDERS = "providers";
	public static final String FILTER_REASON_CODES = "reasonCodes";
	
	private static Logger log = Logger.getLogger(ChargeBacksManager.class);
	
	public static HashMap<Long, String> getChargebackProviders() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ChargebacksDAO.getChargebackProviders(con);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<ChargebackReasonCode> getAllReasonCodes() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ChargebacksDAO.getAllReasonCodes(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Chargeback> getChargebacks(ChargebackFilters chargebackFilters) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ChargebacksDAO.getChargebacks(con, chargebackFilters);
		} finally {
			closeConnection(con);
		}
	}

	public static ChargebackDetails getChargebackDetails(long transactionId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ChargebacksDAO.getChargebackDetails(con, transactionId);
		} finally {
			closeConnection(con);
		}
	}

	public static void insertChargeback(ChargebackDetails chargebackDetails, WriterWrapper wrapper) throws SQLException {
		Connection con = getConnection();
		con.setAutoCommit(false);
		try {
			// first insert cb issue.
			insertChargeBackIssue(con, chargebackDetails, wrapper);

			// only if cb issue was successfully inserted, insert cb
			ChargebacksDAO.insertChargeback(con, chargebackDetails);

			TransactionsDAOBase.updateTransactionChargeBack(con, chargebackDetails.getTransactionId(),
															chargebackDetails.getId(), true);

			Transaction trans = TransactionsDAOBase.getById(con, chargebackDetails.getTransactionId());
			String offset = UsersManagerBase.getUserUtcOffset(trans.getUserId());
			long negFixTransactionId = UsersManager.addToBalanceNonNeg(con, trans.getUserId(), -trans.getAmount(),
					trans.getRate(), offset);
			GeneralDAO.insertBalanceLog(con, wrapper.getWriter().getId(), trans.getUserId(),
					Constants.TABLE_TRANSACTIONS, trans.getId(), Constants.LOG_BALANCE_CHARGE_BACK,
					offset);
			if (negFixTransactionId > 0) {
				GeneralDAO.insertBalanceLog(con, wrapper.getWriter().getId(), trans.getUserId(),
						Constants.TABLE_TRANSACTIONS, trans.getId(), Constants.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT,
						offset);
			}

			con.commit();
		} catch (SQLException e) {
			log.error("Exception inserting chargback. ", e);
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw e;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
			closeConnection(con);
		}
	}
	
	public static void insertChargeBackIssue(Connection con, ChargebackDetails chargebackDetails, WriterWrapper wrapper) throws SQLException {
		Issue issue = new Issue();
		long userId = chargebackDetails.getUserId();

		issue.setUserId(userId);
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CHARGEBACK));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Insert Issue
		IssuesDAOBase.insertIssue(con, issue);
		chargebackDetails.setIssueId(issue.getId());

		// Insert Issue Action
		insertChargeBackIssueAction(con, chargebackDetails, wrapper);
	}
	
	public static void insertChargeBackIssueAction(Connection con, ChargebackDetails chargebackDetails, WriterWrapper wrapper)	throws SQLException {
		IssueAction action = new IssueAction();
		long issueId = chargebackDetails.getIssueId();

		action.setIssueId(issueId);
		String comments = "";
		String amount = CommonUtil.displayAmount(chargebackDetails.getAmount(), chargebackDetails.getCurrencyId());
		comments = "Amount: " + amount + " Trx ID: " + chargebackDetails.getTransactionId() + " Case ID: "
				+ chargebackDetails.getCaseId() + chargebackDetails.getProvider();
		action.setComments(comments);
		action.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
		action.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
		action.setWriterId(wrapper.getWriter().getId());
		action.setActionTime(new Date());
		action.setActionTimeOffset(wrapper.getUtcOffset());
		action.setSignificant(false);

		IssuesManagerBase.insertAction(con, action);
	}
	
	public static void updateChargeback(ChargebackDetails chargebackDetails, WriterWrapper wrapper) throws SQLException {
		Connection con = getConnection();

		con.setAutoCommit(false);
		try {
			// first insert cb issue action.
			insertChargeBackIssueAction(con, chargebackDetails, wrapper);

			// only if cb issue action was successfully inserted, update cb
			ChargebacksDAO.updateChargeback(con, chargebackDetails);
			con.commit();
		} catch (SQLException e) {
			log.error("Exception updating chargback. ", e);
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw e;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
			closeConnection(con);
		}
	}
}
