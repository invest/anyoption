package com.anyoption.backend.service.salescallstatusmanager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.CallStatusManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.createmarket.CreateMarketServices;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_vos.CallsRepresentative;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

public class SalesCallStatusManagerServices {
	
	private static final Logger logger = Logger.getLogger(SalesCallStatusManagerServices.class);
	private static final String SCREEN_NAME_PERMISSIONS_SALES_CALL_STATUS_MANAGER = "callStatus";

	@BackendPermission(id = "callStatus_view")
	public static ScreenSettingsResponse getSalesCallStatusScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getSalesCallStatusScreenSettings: " + request);
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_SALES_CALL_STATUS_MANAGER, response, httpRequest);
		
		//FILTERS
		//skins
		HashMap<Long, String> skins = CallStatusManager.getSkins();
		filters.put(CallStatusManager.FILTER_SKINS, skins);
		
		//userRanks
		HashMap<Long, String> userRanks = CallStatusManager.getUserRanks();
		filters.put(CallStatusManager.FILTER_USER_RANK, userRanks);
		
		//representatives
		HashMap<Long, String> representatives = CallStatusManager.getRepresentatives();
		filters.put(CallStatusManager.FILTER_REPRESENTATIVES, representatives);
		response.setScreenFilters(filters);
		
		return response;
	}

	public static SalesCallStatusManagerMethodResponse getSalesCallStatusRepresentatives(SalesCallStatusManagerMethodRequest request) {
		SalesCallStatusManagerMethodResponse response = new SalesCallStatusManagerMethodResponse();

		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}
		if (request.getToDate() == 0 || request.getFromDate() == 0) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		Calendar cFrom = Calendar.getInstance();
		cFrom.setTimeInMillis(request.getFromDate());
		
		Calendar cTo = Calendar.getInstance();
		cTo.setTimeInMillis(request.getToDate());
				
		if (cFrom.after(cTo)) { //to date must be after from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		cTo.add(Calendar.DAY_OF_MONTH, -31);
		if (cTo.after(cFrom)) { //to date can't be more then 31 days from from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		ArrayList<CallsRepresentative> representatives = null;
		try {
			representatives = CallStatusManager.getSalesCallStatusRepresentatives(request);
		} catch (SQLException e) {
			logger.error("Unable to get Sales Call Status Manager", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if (representatives == null){
			return response;
		}		
		response.setRepresentatives(representatives);
		
		return response;
	}
}
