package com.anyoption.backend.service.editmarketfields;

import java.util.ArrayList;
import java.util.Map;

import com.anyoption.common.beans.EmirReport;
import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.MarketBean;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.bl_vos.AssetIndex;

/**
 * 
 * @author ivan.petkov
 *
 */
public class MarketResponse extends MethodResult {

	private MarketBean market;
	private Map<String, ArrayList<String>> marketFieldTypes;
	private Map<Long, MarketGroup> marketGroups;
	private Map<Long, Exchange> exchanges;
	private Map<Long, InvestmentLimitsGroup> investmentLimitGroups;
	private Map<Long, String> typeOfShifting;
	private Map<Long, String> bubblesPricingLevels;
	private AssetIndex assetIndex;
	private EmirReport emirReport;

	public MarketBean getMarket() {
		return market;
	}

	public void setMarket(MarketBean market) {
		this.market = market;
	}

	public Map<String, ArrayList<String>> getMarketFieldTypes() {
		return marketFieldTypes;
	}

	public void setMarketFieldTypes(Map<String, ArrayList<String>> marketFieldTypes) {
		this.marketFieldTypes = marketFieldTypes;
	}

	public Map<Long, MarketGroup> getMarketGroups() {
		return marketGroups;
	}

	public void setMarketGroups(Map<Long, MarketGroup> marketGroups) {
		this.marketGroups = marketGroups;
	}

	public Map<Long, Exchange> getExchanges() {
		return exchanges;
	}

	public void setExchanges(Map<Long, Exchange> exchanges) {
		this.exchanges = exchanges;
	}

	public Map<Long, InvestmentLimitsGroup> getInvestmentLimitGroups() {
		return investmentLimitGroups;
	}

	public void setInvestmentLimitGroups(Map<Long, InvestmentLimitsGroup> investmentLimitGroups) {
		this.investmentLimitGroups = investmentLimitGroups;
	}

	public Map<Long, String> getTypeOfShifting() {
		return typeOfShifting;
	}

	public void setTypeOfShifting(Map<Long, String> typeOfShifting) {
		this.typeOfShifting = typeOfShifting;
	}

	public Map<Long, String> getBubblesPricingLevels() {
		return bubblesPricingLevels;
	}

	public void setBubblesPricingLevels(Map<Long, String> bubblesPricingLevels) {
		this.bubblesPricingLevels = bubblesPricingLevels;
	}

	public AssetIndex getAssetIndex() {
		return assetIndex;
	}

	public void setAssetIndex(AssetIndex assetIndex) {
		this.assetIndex = assetIndex;
	}

	public EmirReport getEmirReport() {
		return emirReport;
	}

	public void setEmirReport(EmirReport emirReport) {
		this.emirReport = emirReport;
	}
}