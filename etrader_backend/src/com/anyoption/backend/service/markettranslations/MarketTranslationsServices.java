package com.anyoption.backend.service.markettranslations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.createmarket.CopyMarketResponse;
import com.anyoption.backend.service.editmarketconfig.EditMarketConfigServices;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.MarketTranslations;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketAssetIndexManager;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;

public class MarketTranslationsServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_MARKET_TRANSLATIONS = "marketTranslations";
	private static final Logger log = Logger.getLogger(EditMarketConfigServices.class);
	
	@BackendPermission(id = "marketTranslations_view")
	public static TranslationFiltersResponse getMarketTranslationsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketTranslationsScreenSettings:" + request);
		TranslationFiltersResponse response = new TranslationFiltersResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_MARKET_TRANSLATIONS, response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			response.setLanguages(LanguagesManagerBase.getLanguages());
			response.setFilters(mapFilters);
		} catch (SQLException e) {
			log.error("Unable to select markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketTranslations_view")
	public static MarketTranslationsResponse getMarketTranslations(MarketRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketTranslations:" + request);
		MarketTranslationsResponse response = new MarketTranslationsResponse();
		try {
			MarketTranslations translations = MarketAssetIndexManager.getMarketTranslationsByMarketId(request.getMarketId());
			response.setMarketTranslations(translations);
		} catch (SQLException sqle) {
			log.error("Unable to select markets translations! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketTranslations_view")
	public static CopyMarketResponse updateMarketTranslations(MarketTranslationsRequest request, HttpServletRequest httpRequest) {
		log.debug("updateMarketTranslations:" + request);
		CopyMarketResponse response = new CopyMarketResponse();
		try {
			MarketAssetIndexManager.updateMarketTranslations(request.getMarketTranslations(), request.getMarketId());
		} catch (SQLException sqle) {
			log.error("Unable to update markets translations! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}
