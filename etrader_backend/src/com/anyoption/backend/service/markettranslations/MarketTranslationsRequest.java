package com.anyoption.backend.service.markettranslations;

import com.anyoption.common.beans.MarketTranslations;

public class MarketTranslationsRequest {
	
	MarketTranslations marketTranslations;
	long marketId;

	public MarketTranslations getMarketTranslations() {
		return marketTranslations;
	}

	public void setMarketTranslations(MarketTranslations marketTranslations) {
		this.marketTranslations = marketTranslations;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
}
