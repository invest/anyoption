package com.anyoption.backend.service.markettranslations;

import java.util.Hashtable;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;
import com.anyoption.common.beans.base.Language;

public class TranslationFiltersResponse extends CopyMarketResponse {
	
	private Hashtable<Long, Language> languages;

	public Hashtable<Long, Language> getLanguages() {
		return languages;
	}

	public void setLanguages(Hashtable<Long, Language> languages) {
		this.languages = languages;
	}
}
