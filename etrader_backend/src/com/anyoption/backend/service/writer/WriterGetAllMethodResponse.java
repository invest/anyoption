/**
 * 
 */
package com.anyoption.backend.service.writer;

import java.util.ArrayList;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class WriterGetAllMethodResponse extends MethodResult {

	private ArrayList<Writer> writers;
	private long totalCount;

	public ArrayList<Writer> getWriters() {
		return writers;
	}

	public void setWriters(ArrayList<Writer> writers) {
		this.writers = writers;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
