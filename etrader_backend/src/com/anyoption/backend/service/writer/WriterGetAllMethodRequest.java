/**
 * 
 */
package com.anyoption.backend.service.writer;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class WriterGetAllMethodRequest extends UserMethodRequest {
	// filters
	private Long isActive;
	private ArrayList<Integer> roles;
	private ArrayList<Integer> departments;
	private ArrayList<Integer> site;
	private ArrayList<Integer> skins;
	private String freeTextFilter;
	private Long page;


	public ArrayList<Integer> getDepartments() {
		return departments;
	}

	public void setDepartments(ArrayList<Integer> departments) {
		this.departments = departments;
	}

	public String getFreeTextFilter() {
		return freeTextFilter;
	}

	public void setFreeTextFilter(String freeTextFilter) {
		this.freeTextFilter = freeTextFilter;
	}

	public ArrayList<Integer> getSkins() {
		return skins;
	}

	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}

	public ArrayList<Integer> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<Integer> roles) {
		this.roles = roles;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public ArrayList<Integer> getSite() {
		return site;
	}

	public void setSite(ArrayList<Integer> site) {
		this.site = site;
	}
}
