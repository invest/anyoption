package com.anyoption.backend.service.marketpriorities;

import java.util.Map;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;

public class MarketPrioritiesResponse extends PermissionsGetResponse {
	
	private Map<String, Map<Long, String>> filters;
	private Map<Long, Map<Long, String>> markets;

	public Map<String, Map<Long, String>> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, Map<Long, String>> filters) {
		this.filters = filters;
	}

	public Map<Long, Map<Long, String>> getMarkets() {
		return markets;
	}

	public void setMarkets(Map<Long, Map<Long, String>> markets) {
		this.markets = markets;
	}
}
