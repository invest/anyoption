package com.anyoption.backend.service.dynamicsquoteparams;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Market;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.DynamicsUtil;

import il.co.etrader.backend.bl_managers.OpportunityQuotes0100Manager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
import il.co.etrader.backend.dao_managers.MarketsManager;
import il.co.etrader.backend.util.Constants;

public class DynamicsQuoteParamsServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_DYNAMICS_QUOTE_PARAMS = "dynamicsQuoteParams";
	private static final Logger log = Logger.getLogger(DynamicsQuoteParamsServices.class);
	
	@BackendPermission(id = "dynamicsQuoteParams_view")
	public static DynamicsQuotesFilterResponse getDynamicsQuotesScreenSettings(UserMethodRequest request,
			HttpServletRequest httpRequest) {
		log.debug("getDynamicsQuotesScreenSettings:" + request);
		DynamicsQuotesFilterResponse response = new DynamicsQuotesFilterResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_DYNAMICS_QUOTE_PARAMS,
																		response, httpRequest);
		try {
			Map<Long, String> map = MarketsManager.getDynamicsMarkets();
			response.setDynamicsMarkets(map);
		} catch (Exception e) {
			log.error("Unable to get dynamics markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "dynamicsQuoteParams_view")
	public static DynamicsMarketResponse getDynamicsMarketQuotes(MarketRequest request) {
		log.debug("getDynamicsMarketQuotes:" + request);
		DynamicsMarketResponse response = new DynamicsMarketResponse();
		try {
			Market market = TraderManager.getDynamicsMarketQuoteParams(request.getMarketId());
			response.setMarket(market);
		} catch (Exception e) {
			log.error("Unable to get dynamics market quote params! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "dynamicsQuoteParams_view")
	public static MethodResult saveQuoteChanges(DynamicsMarketRequest request, HttpServletRequest httpRequest) {
		log.debug("saveQuoteChanges:" + request);
		MethodResult response = new MethodResult();
		try {
			Market marketOld = TraderManager.getDynamicsMarketQuoteParams(request.getMarketId());
			BackendLevelsCache levelsCache = (BackendLevelsCache) httpRequest.getServletContext().getAttribute("levelsCache");
			if (levelsCache.updateDynamicsMarketQuoteParams(marketOld.getFeedName(), DynamicsUtil.quoteParamsToString(request.getQuoteParams()))) {
				il.co.etrader.bl_managers.MarketsManager.setMarketAndOppQuote(request.getMarketId(), DynamicsUtil.quoteParamsToString(request.getQuoteParams()));
				OpportunityQuotes0100 vo = new OpportunityQuotes0100(null, request.getMarketId(),
						Constants.SCREEN_TYPE_DATIKA, request.getWriterId(), request.getQuoteParams(),
						DynamicsUtil.quoteParamsToString(request.getQuoteParams()) + marketOld.getQuoteParams());
				OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
			}
		} catch (Exception e) {
			log.error("Unable to save dynamics market quote params changes! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

}
