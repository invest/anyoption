package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class PromotionBannerSlidersBankResponse extends MethodResult {
	private ArrayList<String> imagesFromBank;

	public ArrayList<String> getImagesFromBank() {
		return imagesFromBank;
	}

	public void setImagesFromBank(ArrayList<String> imagesFromBank) {
		this.imagesFromBank = imagesFromBank;
	}
	
}
