package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;

import com.anyoption.common.bl_vos.PromotionBannerSliderImage;
import com.anyoption.common.service.requests.UserMethodRequest;

public class PromotionBannerSliderInsertUpdateRequest extends UserMethodRequest {
	
	private Long bannerId;
	private Long bannerSliderId;
	private Integer position;
	private Integer isActive;
	private Integer linkTypeId;
	private ArrayList<PromotionBannerSliderImage> bannerSliderImages;
	
	public Long getBannerId() {
		return bannerId;
	}
	public void setBannerId(Long bannerId) {
		this.bannerId = bannerId;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getLinkTypeId() {
		return linkTypeId;
	}
	public void setLinkTypeId(Integer linkTypeId) {
		this.linkTypeId = linkTypeId;
	}
	public ArrayList<PromotionBannerSliderImage> getBannerSliderImages() {
		return bannerSliderImages;
	}
	public void setBannerSliderImages(ArrayList<PromotionBannerSliderImage> bannerSliderImages) {
		this.bannerSliderImages = bannerSliderImages;
	}
	public Long getBannerSliderId() {
		return bannerSliderId;
	}
	public void setBannerSliderId(Long bannerSliderId) {
		this.bannerSliderId = bannerSliderId;
	}
	
	

}
