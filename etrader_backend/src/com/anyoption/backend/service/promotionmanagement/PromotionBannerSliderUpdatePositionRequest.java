package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;


public class PromotionBannerSliderUpdatePositionRequest extends UserMethodRequest{
	private ArrayList<Long> slideIds;

	public ArrayList<Long> getSlideIds() {
		return slideIds;
	}

	public void setSlideIds(ArrayList<Long> sliderOrderedIds) {
		this.slideIds = sliderOrderedIds;
	}



}
