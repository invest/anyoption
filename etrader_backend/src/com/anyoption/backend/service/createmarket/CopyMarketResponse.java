package com.anyoption.backend.service.createmarket;

import java.util.Map;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;

public class CopyMarketResponse extends PermissionsGetResponse {
	
	private Map<String, Map<Long, Map<Long, String>>> filters;
	
	private long marketId;

	public Map<String, Map<Long, Map<Long, String>>> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, Map<Long, Map<Long, String>>> filters) {
		this.filters = filters;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
}
