package com.anyoption.backend.service.createmarket;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class CreateMarketServices {
	
	private static final Logger log = Logger.getLogger(CreateMarketServices.class);
	private static final String SCREEN_NAME_PERMISSIONS_CREATE_MARKET = "createMarket";
	
	@BackendPermission(id = "createMarket_view")
	public static CopyMarketResponse getCreateMarketScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCreateMarketScreenSettings:" + request);
		CopyMarketResponse response = new CopyMarketResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_CREATE_MARKET, response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			response.setFilters(mapFilters);
		} catch (SQLException e) {
			log.error("Unable to select markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "createMarket_view")
	public static CopyMarketResponse copyMarket(MarketRequest request, HttpServletRequest httpRequest) {
		log.debug("copyMarket:" + request);
		CopyMarketResponse response = new CopyMarketResponse();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			Writer writer = writerWrapper.getWriter();
			long writerId = writer.getId();
			long marketId = MarketsManagerBase.copyMarket(request.getMarketId(), writerId);
			response.setMarketId(marketId);
		} catch (SQLException e) {
			log.error("Unable to copy market! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}