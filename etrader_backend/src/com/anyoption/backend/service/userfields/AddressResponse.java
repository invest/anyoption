package com.anyoption.backend.service.userfields;

import com.anyoption.common.service.results.MethodResult;

public class AddressResponse  extends MethodResult {

	private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
