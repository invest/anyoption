/**
 * 
 */
package com.anyoption.backend.service.idsfiles;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.IdsFiles;

public class IdsFilesGetResponse extends MethodResult {
	
	private ArrayList<IdsFiles> idsFiles;

	public ArrayList<IdsFiles> getIdsFiles() {
		return idsFiles;
	}

	public void setIdsFiles(ArrayList<IdsFiles> idsFiles) {
		this.idsFiles = idsFiles;
	}	
}
