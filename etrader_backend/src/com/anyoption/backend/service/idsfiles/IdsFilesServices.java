package com.anyoption.backend.service.idsfiles;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.KeesingManager;
import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.files.FileRequest;
import com.anyoption.backend.service.files.FilesServices;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;

public class IdsFilesServices {	         
	private static final Logger log = Logger.getLogger(IdsFilesServices.class);
	
	private static final String SCREEN_NAME_FILE = "idsFile";	
	public static final String FILTER_KS_STATUS = "ksStatus";
	
	@BackendPermission(id = "idsFile_view")
	public static IdsFilesScreenSettingsResponse getIdsFileScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getIdsFileScreenSettings:" + request);		
		IdsFilesScreenSettingsResponse response = new IdsFilesScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> screenFilters = new HashMap<String, HashMap<Long, String>>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_FILE, response, httpRequest);
		try {
			// writer
			HashMap<Long, String> systemWriters = new HashMap<>();
			systemWriters.put((long)Writer.WRITER_ID_WEB, "WEB");
			systemWriters.put((long) Writer.WRITER_ID_MOBILE, "MOBILE");
			systemWriters.put((long) Writer.WRITER_ID_COPYOP_WEB, "COPYOP_WEB");
			systemWriters.put((long) Writer.WRITER_ID_COPYOP_MOBILE, "COPYOP_MOBILE");
			screenFilters.put(WritersManager.FILTER_SYSTEM_WRITERS, systemWriters);
			
			//platform
			 HashMap<Long, String> platformF = new HashMap<Long, String>();
			 platformF.put((long) Platform.OUROBORS, "business.case.ouroboros");
			 platformF.put((long) Platform.ANYOPTION, "business.case.ao");
			 screenFilters.put(TransactionManager.FILTER_PLATFORMS, platformF);
			 
			 //skins
			 HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(SkinsManagerBase.getAllSkinsFilter(), writer, log);
			 screenFilters.put(SkinsManagerBase.FILTER_SKINS, skins);
			 
			 //countries
			 HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
			 screenFilters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
			 
			 //ksStatus
			 HashMap<Long, String> ksStatus = new HashMap<Long, String>();
			 ksStatus.put(0l, "OK");
			 ksStatus.put(1l, "Not OK");			 
			 screenFilters.put(FILTER_KS_STATUS, ksStatus);
			 
			//Users ClassType
			 HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
			 screenFilters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
			 //Remove API classType
			 screenFilters.get(TransactionManager.FILTER_CLASS_USERS).remove(3l);
			 
		} catch (Exception e) {
			log.error("Unable to getIdsFileScreenSettings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setScreenFilters(screenFilters);		
		return response;
	}
	
	@BackendPermission(id = "idsFile_view")
	public static IdsFilesGetResponse getFiles(IdsFileRequest request, HttpServletRequest httpRequest) {
		log.debug("getFiles:" + request);
		IdsFilesGetResponse response = new IdsFilesGetResponse();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(
								ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			ArrayList<Integer> skinsList = request.getSkins();
			skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writerWrapper);
			request.setSkins(skinsList);
			response.setIdsFiles(KeesingManager.loadKeesingFileForApprove(request.getPage(), request.getRowPerPage(),
					request.getUploadedBy(), request.getUploadedByBackend(), request.getBusinessCase(), request.getSkins(), 
					request.getCountries(), request.getKsStatus(), request.getUserId(), request.getUserName(), request.getUserClasses()));
		} catch (Exception e) {
			log.error("Unable to getFiles", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "idsFile_save")
	public static MethodResult saveIdsFile(FileRequest request, HttpServletRequest httpRequest) {							
		log.debug("saveIdsFile:" + request);
		request.setLoadedFile(false);
		request.setIDsScreen(true);
		MethodResult response = FilesServices.updateInsertFile(request, httpRequest);		
		return response;
	}	
}