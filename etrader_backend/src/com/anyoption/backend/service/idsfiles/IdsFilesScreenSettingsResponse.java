/**
 * 
 */
package com.anyoption.backend.service.idsfiles;

import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FilesAdditionalInfo;

public class IdsFilesScreenSettingsResponse extends ScreenSettingsResponse {
	private HashMap<Long, FileType> fileTypes;
	private HashMap<Long, ArrayList<HashMap<Long, String>>> fileRejectReasons;
	private HashMap<Long, FilesAdditionalInfo> filesAdditionalInfo;

	public HashMap<Long, FileType> getFileTypes() {
		return fileTypes;
	}
	
	public void setFileTypes(HashMap<Long, FileType> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public HashMap<Long, ArrayList<HashMap<Long, String>>> getFileRejectReasons() {
		return fileRejectReasons;
	}

	public void setFileRejectReasons(HashMap<Long, ArrayList<HashMap<Long, String>>> fileRejectReasons) {
		this.fileRejectReasons = fileRejectReasons;
	}

	public HashMap<Long, FilesAdditionalInfo> getFilesAdditionalInfo() {
		return filesAdditionalInfo;
	}

	public void setFilesAdditionalInfo(HashMap<Long, FilesAdditionalInfo> filesAdditionalInfo) {
		this.filesAdditionalInfo = filesAdditionalInfo;
	}

}
