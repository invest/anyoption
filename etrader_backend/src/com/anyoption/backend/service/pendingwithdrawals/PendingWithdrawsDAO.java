package com.anyoption.backend.service.pendingwithdrawals;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.TransactionDAO;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.managers.TransactionsManagerBase;

import oracle.jdbc.OracleTypes;

/**
 * 
 * @author ivan.petkov
 *
 */
public class PendingWithdrawsDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(PendingWithdrawsDAO.class);

	public static ArrayList<PendingWithdraw> getPendingWithdraws(Connection conn,
			PendingWithdrawFilters pendingWithdrawFilters, boolean isSecondApproval) throws SQLException {
		ArrayList<PendingWithdraw> pendingWithdrawsList = new ArrayList<PendingWithdraw>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_pending_withdrawals(o_pending_withdraws => ? " +
																								" , i_from_date => ? " +
																								" , i_to_date => ? " +
																								" , i_transaction_id => ? " +
																								" , i_user_id => ? " +
																								" , i_amount_from => ? " +
																								" , i_amount_to => ? " +
																								" , i_business_cases => ? " +
																								" , i_skins => ? " +
																								" , i_countries => ? " +
																								" , i_user_classes => ? " +
																								" , i_currencies => ? " +
																								" , i_withdraw_types => ? " +
																								" , i_invested => ? " +
																								" , i_past_withdraws => ? " +
																								" , i_flag_subject => ? " +
																								" , i_status => ? " +
																								" , i_page_number => ? " +
																								" , i_rows_per_page => ? " +
																								" , i_screen => ? " +
																								" , i_flag_accounting_ok => ? )}");		
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(pendingWithdrawFilters.getFrom(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getTo(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getTransactionId(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getUserId(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getAmountUSDFrom(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getAmountUSDTo(), index++, cstmt);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getBusinessCases(), index++, cstmt, conn);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getSkins(), index++, cstmt, conn);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getCountries(), index++, cstmt, conn);
			setStatementValue(pendingWithdrawFilters.getUserClasses(), index++, cstmt);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getCurrencies(), index++, cstmt, conn);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getTransactionTypes(), index++, cstmt, conn);
			setStatementValue(pendingWithdrawFilters.getInvested(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getPastWithdraws(), index++, cstmt);
			TransactionDAO.setArrayVal(pendingWithdrawFilters.getFlagSubject(), index++, cstmt, conn);
			setStatementValue(pendingWithdrawFilters.getStatus(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getPage(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getRowsPerPage(), index++, cstmt);
			setStatementValue(pendingWithdrawFilters.getScreen(), index++, cstmt);
			if (isSecondApproval) {
				if (pendingWithdrawFilters.getFlagAccountingOk() == -1) {
					cstmt.setNull(index++, Types.NULL);
				} else {
					setStatementValue(pendingWithdrawFilters.getFlagAccountingOk() == 0 ? new Long(1) : new Long(0), index++, cstmt);
				}
			} else {
				cstmt.setNull(index++, Types.NULL);
			}
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			
			while (rs.next()) {
				PendingWithdraw pendingWithdraw = new PendingWithdraw();
				pendingWithdraw.setTransactionId(rs.getLong("transactionid"));
				pendingWithdraw.setTransactionFlagged(rs.getLong("transaction_flagged") == 0 ? false : true);
				pendingWithdraw.setFlagSubject(rs.getString("flag_subject_name"));
				pendingWithdraw.setUserId(rs.getLong("userid"));
				pendingWithdraw.setUserName(rs.getString("user_name"));
				pendingWithdraw.setSkinName(rs.getString("skinname"));
				pendingWithdraw.setCountryName(rs.getString("country_name"));
				pendingWithdraw.setClearingProviderId(rs.getLong("clearing_provider"));
				if (rs.getTimestamp("time_created") != null) {
					pendingWithdraw.setTimeCreated(convertToDate(rs.getTimestamp("time_created")).getTime());
				}
				pendingWithdraw.setTypeName(rs.getString("transactiontype"));
				pendingWithdraw.setTypeId(rs.getLong("type_id"));
				pendingWithdraw.setAmount(rs.getLong("amount"));
				pendingWithdraw.setAmountInUSD(rs.getLong("base_amount"));
				pendingWithdraw.setInvested(rs.getLong("invested") == 1 ? true : false);
				pendingWithdraw.setPastWithdraws(rs.getLong("past_withdraws") == 1 ? true : false);
				pendingWithdraw.setTotalCount(rs.getLong("total_count"));
				pendingWithdraw.setCurrencyId(rs.getLong("currency_id"));
				pendingWithdraw.setStatus(rs.getLong("status_id"));
				pendingWithdraw.setAccountingOkFlag(rs.getLong("flag_accounting_ok") == 1 ? true : false);
				pendingWithdrawsList.add(pendingWithdraw);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return pendingWithdrawsList;
	}
	
	public static UserDetails getUserDetails(Connection conn, long transactionId) throws SQLException {
		UserDetails userDetails = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_details(o_data => ? " +
																				" ,i_transation_id => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, transactionId);
			
			cstmt.execute();
			userDetails = new UserDetails();
			rs = (ResultSet) cstmt.getObject(1);
			
			if (rs.next()) {
				userDetails.setUserName(rs.getString("user_name"));
				userDetails.setCurrencyId(rs.getLong("currency_id"));
				userDetails.setActiveUser(rs.getLong("is_active") == 1 ? true : false);
				userDetails.setUserFirstName(rs.getString("first_name"));
				userDetails.setUserLastName(rs.getString("last_name"));
				String street = rs.getString("street") == null ? "" : rs.getString("street") + " ";
				String streetNum = rs.getString("street_no") == null ? "" : rs.getString("street_no") + " ";
				String city = rs.getString("city_name") == null ? "" : rs.getString("city_name") + " ";
				String zip = rs.getString("zip_code") == null ? "" : rs.getString("zip_code") + " ";
				String address = street + streetNum + city + zip;
				userDetails.setUserAddress(address);
				userDetails.setCountryName(rs.getString("country_name"));
				userDetails.setCountryId(rs.getLong("country_id"));
				userDetails.setMobilePhone(rs.getString("mobile_phone"));
				userDetails.setLandlinePhone(rs.getString("land_line_phone"));
				userDetails.setLastFourDigitsCCnumber(rs.getString("cc_number_last_4_digits"));
				userDetails.setBin(rs.getString("bin"));
				userDetails.setBeneficiaryName(rs.getString("beneficiary_name"));
				userDetails.setSwift(rs.getString("swift"));
				userDetails.setIban(rs.getString("iban"));
				userDetails.setAccountNumber(rs.getLong("account_num"));
				userDetails.setBankName(rs.getString("bank_name"));
				userDetails.setBranchAddress(rs.getString("branch_address"));
				userDetails.setBranchNumber(rs.getString("branch_number"));
				userDetails.setSkrillEmail(rs.getString("skrill_account"));
				userDetails.setPaypalEmail(rs.getString("paypal_email"));
				userDetails.setSource(rs.getString("src"));
				userDetails.setFirstApproveWriter(rs.getString("first_approve_writer"));
				if (rs.getTimestamp("first_approve_time") != null) {
					userDetails.setFirstApproveDate(convertToDate(rs.getTimestamp("first_approve_time")).getTime());
				}
				userDetails.setAmount(rs.getLong("amount"));
				userDetails.setAmountUSD(rs.getLong("amount_in_usd"));
				userDetails.setFeeAmount(TransactionsManagerBase.getTransactionHomoFee(rs.getLong("amount"),
																		rs.getLong("currency_id"), rs.getLong("type_id")));
				userDetails.setClearingProviderId(rs.getLong("clearing_provider_id"));
				if (rs.getTimestamp("flag_date") != null) {
					userDetails.setFlagDate(convertToDate(rs.getTimestamp("flag_date")).getTime());
				}
				userDetails.setFlagWriter(rs.getString("flag_writer"));
				userDetails.setFlagSubject(rs.getLong("FLAG_SUBJECT_ID"));
				userDetails.setFlag(rs.getLong("flag") == 0 ? false : true);
				userDetails.setAccountingOkFlag(rs.getLong("flag_accounting_ok") == 0 ? false : true);
				userDetails.setAccountingOkWriter(rs.getString("accounting_ok_writer"));
				userDetails.setAccountingOkWriterId(rs.getLong("accounting_ok_writer_id"));
				if (rs.getTimestamp("accounting_ok_date") != null) {
					userDetails.setAccountingOkDate(convertToDate(rs.getTimestamp("accounting_ok_date")).getTime());
				}
				userDetails.setComments(rs.getString("comments") == null ? "" : rs.getString("comments"));
				//For Cheque, we check fee_cancel of the cheque
				if (rs.getLong("cheque_fee_cancel") == 2) {
					if (rs.getLong("fee_cancel") == 2) {
						userDetails.setFeeCancel(null);
					} else {
						userDetails.setFeeCancel(rs.getLong("fee_cancel") == 1 ? true : false);
					}
				} else {
					userDetails.setFeeCancel(rs.getLong("cheque_fee_cancel") == 1 ? true : false);
				}
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return userDetails;
	}
	
	public static TransactionDetails getTransactionDetails(Connection conn, long transactionId) throws SQLException {
		TransactionDetails transactionDetails = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_details_user(o_data_user => ? " +
																				" ,i_transation_id => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, transactionId);
			
			cstmt.execute();
			transactionDetails = new TransactionDetails();
			
			rs = (ResultSet) cstmt.getObject(1);
			
			if (rs.next()) {
				transactionDetails.setBalance(rs.getLong("balance"));
				transactionDetails.setWinLose(rs.getLong("winlose_balance"));
				transactionDetails.setUserRegulated(rs.getLong("user_regulated") == 0 ? false : true);
				transactionDetails.setControlApproved(rs.getLong("control_approved") == 1 ? true : false);
				if (rs.getTimestamp("control_approved_date") != null) {
					transactionDetails.setControlApprovedDate(convertToDate(rs.getTimestamp("control_approved_date")).getTime());
				}
				transactionDetails.setInvestmentsCount(rs.getLong("investments_count"));
				transactionDetails.setBonusUsed(rs.getLong("used_bonuses"));
				transactionDetails.setBonusReceived(rs.getLong("received_bonuses"));
				transactionDetails.setActiveBonuses(rs.getLong("b_state"));
				transactionDetails.setWageringWaved(rs.getLong("bonuses_waived") == 1 ? true : false);
				transactionDetails.setTradersReviewed(rs.getLong("reviewed_by_traders") == 1 ? true : false);
				if (rs.getTimestamp("traders_review_date") != null) {
					transactionDetails.setTradersReviewedDate(convertToDate(rs.getTimestamp("traders_review_date")).getTime());
				}
				transactionDetails.setTradersReviewedWriterId(rs.getLong("traders_review_writer_id"));
				transactionDetails.setTradersReviewedWriter(rs.getString("traders_review_writer"));
				transactionDetails.setSuccessfulWithdrawsCount(rs.getLong("suc_with_count"));
				transactionDetails.setSumWithdraws(rs.getLong("withdraw_amount"));
				transactionDetails.setApprovedId(rs.getLong("id_approve") == 1 ? true : false);
				transactionDetails.setApprovedUtilityBill(rs.getLong("ub_approve") == 1 ? true : false);
				transactionDetails.setApprovedCreditCard(rs.getLong("cc_approve") == 1 ? true : false);
				transactionDetails.setDisplayCC(rs.getLong("display_cc") == 1 ? true : false);
				transactionDetails.setSignedFormName(rs.getString("file_name"));
				if (rs.getTimestamp("last_file") != null) {
					transactionDetails.setSignedFormDate(convertToDate(rs.getTimestamp("last_file")).getTime());
				}
				transactionDetails.setTotalDeposits(rs.getLong("deposit_amount"));
				transactionDetails.setTotalDepositsUSD(rs.getLong("deposit_amount_usd"));
				Transaction t = new Transaction();
				t.setUserId(rs.getLong("user_regulated"));
				transactionDetails.setHavePendingDeposit(TransactionsDAOBase.IsHavePendingDeposit(conn, t));
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return transactionDetails;
	}
	
	public static ArrayList<DepositSummary> getDepositSummaryDetails(Connection conn, long transactionId, long userId) throws SQLException {
		ArrayList<DepositSummary> depSumList = new ArrayList<DepositSummary>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_details_deposit(o_data_deposit => ? " +
																				",i_transation_id => ? " +
																				",i_user_id => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, transactionId);
			cstmt.setLong(index++, userId);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			
			while (rs.next()) {
				DepositSummary depSum = new DepositSummary();
				depSum.setTypeId(rs.getLong("type_id"));
				depSum.setDescription(rs.getString("description"));
				depSum.setCardDetails(rs.getString("card_details"));
				depSum.setLastFourDigitsCC(rs.getString("cc_number_last_4_digits"));
				depSum.setProviderName(rs.getString("name"));
				depSum.setDepositCount(rs.getLong("count"));
				depSum.setAmount(rs.getLong("amount"));
				depSum.setAmountToCredit(rs.getLong("amount_to_credit"));
				depSum.setCurrencyId(rs.getLong("currency_id"));
				if (rs.getTimestamp("max_date") != null) {
					depSum.setMaxDate(convertToDate(rs.getTimestamp("max_date")).getTime());
				}
				String expMonth = rs.getString("exp_month") == null ? "" : rs.getString("exp_month");
				String expYear = rs.getString("exp_year") == null ? "" : rs.getString("exp_year");
				String expDate = expMonth;
				if (rs.getString("exp_year") != null && rs.getString("exp_year") != null) {
					expDate += "/";
				}
				expDate += expYear;
				depSum.setExpDate(expDate);
				depSumList.add(depSum);
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return depSumList;
	}

	public static FlagApproveChange getFlagApproveChange(Connection conn, long withdrawChangeId) throws SQLException {
		FlagApproveChange flagApproveChange = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_flag_approve_change(o_data => ? " +
																						 " ,i_withdrawal_changes_id => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, withdrawChangeId);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				flagApproveChange = new FlagApproveChange();
				flagApproveChange.setId(rs.getLong("id"));
				flagApproveChange.setTransactionId(rs.getLong("transaction_id"));
				flagApproveChange.setFlagChangeDate(convertToDate(rs.getTimestamp("flag_change")));
				flagApproveChange.setFlagWriterId(rs.getLong("flag_writer_id"));
				flagApproveChange.setApproveChangeDate(convertToDate(rs.getTimestamp("approve_change")));
				flagApproveChange.setApproveWriterId(rs.getLong("approve_writer_id"));
				flagApproveChange.setFlag(rs.getLong("withdraw_support_flag") == 1 ? true : false);
				flagApproveChange.setFlagSubjectId(rs.getLong("flag_subject_id"));
				flagApproveChange.setFlagAccountingOk(rs.getLong("flag_accounting_ok") == 1 ? true : false);
				flagApproveChange.setAccountingOkWriterId(rs.getLong("accounting_ok_writer_id"));
				flagApproveChange.setAccountingOkDate(convertToDate(rs.getTimestamp("accounting_ok_date")));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return flagApproveChange;
	}

	public static void updateFlagApproveChange(Connection conn, FlagApproveChange flagApproveChange) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.update_flag_approve_change(i_transaction_id => ? " +
																							" ,i_flag_change_date => ? " +
																							" ,i_flag_writer_id => ? " +
																							" ,i_approve_change_date => ? " +
																							" ,i_approve_writer_id => ? " +
																							" ,i_flag => ? " +
																							" ,i_flag_subject_id => ? " +
																							" ,i_flag_accounting_ok => ? " +
																							" ,i_accounting_ok_writer_id => ? " +
																							" ,i_accounting_ok_date => ? )}");

			cstmt.setLong(index++, flagApproveChange.getTransactionId());
			setStatementValue(new Timestamp(
					flagApproveChange.getFlagChangeDate() == null ? 0 : flagApproveChange.getFlagChangeDate().getTime()), index++, cstmt);
			cstmt.setLong(index++, flagApproveChange.getFlagWriterId());
			setStatementValue(new Timestamp(
					flagApproveChange.getApproveChangeDate() == null ? 0 : flagApproveChange.getApproveChangeDate().getTime()), index++, cstmt);
			cstmt.setLong(index++, flagApproveChange.getApproveWriterId());
			cstmt.setLong(index++, flagApproveChange.isFlag() ? 1 : 0);
			setStatementValue(flagApproveChange.getFlagSubjectId(), index++, cstmt);
			cstmt.setLong(index++, flagApproveChange.isFlagAccountingOk() ? 1 : 0);
			cstmt.setLong(index++, flagApproveChange.getAccountingOkWriterId());
			setStatementValue(new Timestamp(
					flagApproveChange.getAccountingOkDate() == null ? 0 : flagApproveChange.getAccountingOkDate().getTime()), index++, cstmt);
			
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateTradersReviewedFlag(Connection con, boolean tradersReviewed, long writerId, long userId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_pending_withdrawals.update_traders_reviewed(i_traders_reviewed_flag => ? " +
																						   " ,i_writer_id => ? " +
																						   " ,i_user_id => ? )}");
			cstmt.setLong(index++, tradersReviewed ? 1 : 0);
			cstmt.setLong(index++, writerId);
			cstmt.setLong(index++, userId);
			
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}

	public static HashMap<Long, String> getFlagSubjects(Connection conn) throws SQLException {
		HashMap<Long, String> flagSubjects = new HashMap<Long, String>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = conn.prepareCall("{call pkg_pending_withdrawals.get_flag_subjects(o_flag_subjects => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				flagSubjects.put(rs.getLong("id"), rs.getString("name"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return flagSubjects;
	}
}