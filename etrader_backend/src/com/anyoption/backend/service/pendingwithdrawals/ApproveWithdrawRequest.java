package com.anyoption.backend.service.pendingwithdrawals;

import com.anyoption.common.service.requests.UserMethodRequest;

public class ApproveWithdrawRequest extends UserMethodRequest {
	
	private long transactionId;
	private boolean expectedProviderChange;
	private long clearingProviderId;
	private boolean exemptFee;
	private String comments;
	private boolean flagChecked;
	private long flagSubjectId;
	private long bankId;
	private String paypalTransactionId;
	private boolean tradersReviewed;
	private boolean accountingOkFlag;
	private long accountingOkWriter;
	private String accountingOkWriterName;
	private long accountingOkDate;
	private boolean accountingChecked;
	
	public long getTransactionId() {
		return transactionId;
	}
	 
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	public boolean isExpectedProviderChange() {
		return expectedProviderChange;
	}
	
	public void setExpectedProviderChange(boolean expectedProviderChange) {
		this.expectedProviderChange = expectedProviderChange;
	}
	
	public long getClearingProviderId() {
		return clearingProviderId;
	}
	
	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
	
	public boolean isExemptFee() {
		return exemptFee;
	}
	
	public void setExemptFee(boolean exemptFee) {
		this.exemptFee = exemptFee;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isFlagChecked() {
		return flagChecked;
	}

	public void setFlagChecked(boolean flagChecked) {
		this.flagChecked = flagChecked;
	}

	public long getFlagSubjectId() {
		return flagSubjectId;
	}

	public void setFlagSubjectId(long flagSubjectId) {
		this.flagSubjectId = flagSubjectId;
	}

	public long getBankId() {
		return bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}

	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}

	public boolean isTradersReviewed() {
		return tradersReviewed;
	}

	public void setTradersReviewed(boolean tradersReviewed) {
		this.tradersReviewed = tradersReviewed;
	}

	public boolean isFlagAccountingOk() {
		return accountingOkFlag;
	}

	public void setFlagAccountingOk(boolean flagAccountingOk) {
		this.accountingOkFlag = flagAccountingOk;
	}

	public long getAccountingOkWriter() {
		return accountingOkWriter;
	}

	public void setAccountingOkWriter(long accountingOkWriter) {
		this.accountingOkWriter = accountingOkWriter;
	}

	public String getAccountingOkWriterName() {
		return accountingOkWriterName;
	}

	public void setAccountingOkWriterName(String accountingOkWriterName) {
		this.accountingOkWriterName = accountingOkWriterName;
	}

	public long getAccountingOkDate() {
		return accountingOkDate;
	}

	public void setAccountingOkDate(long accountingOkDate) {
		this.accountingOkDate = accountingOkDate;
	}

	public boolean isAccountingChecked() {
		return accountingChecked;
	}

	public void setAccountingChecked(boolean accountingChecked) {
		this.accountingChecked = accountingChecked;
	}
}
