package com.anyoption.backend.service.pendingwithdrawals;

import java.util.Date;

public class FlagApproveChange {
	
	private long id;
	private long transactionId;
	private Date flagChangeDate;
	private long flagWriterId;
	private Date approveChangeDate;
	private long approveWriterId;
	private boolean flag;
	private Long flagSubjectId;
	private boolean flagAccountingOk;
	private long accountingOkWriterId;
	private Date accountingOkDate;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	public Date getFlagChangeDate() {
		return flagChangeDate;
	}
	
	public void setFlagChangeDate(Date flagChangeDate) {
		this.flagChangeDate = flagChangeDate;
	}
	
	public long getFlagWriterId() {
		return flagWriterId;
	}
	
	public void setFlagWriterId(long flagWriterId) {
		this.flagWriterId = flagWriterId;
	}
	
	public Date getApproveChangeDate() {
		return approveChangeDate;
	}
	
	public void setApproveChangeDate(Date approveChangeDate) {
		this.approveChangeDate = approveChangeDate;
	}
	
	public long getApproveWriterId() {
		return approveWriterId;
	}
	
	public void setApproveWriterId(long approveWriterId) {
		this.approveWriterId = approveWriterId;
	}
	
	public boolean isFlag() {
		return flag;
	}
	
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public Long getFlagSubjectId() {
		return flagSubjectId;
	}
	
	public void setFlagSubjectId(Long flagSubjectId) {
		this.flagSubjectId = flagSubjectId;
	}

	public boolean isFlagAccountingOk() {
		return flagAccountingOk;
	}

	public void setFlagAccountingOk(boolean flagAccountingOk) {
		this.flagAccountingOk = flagAccountingOk;
	}

	public long getAccountingOkWriterId() {
		return accountingOkWriterId;
	}

	public void setAccountingOkWriterId(long accountingOkWriterId) {
		this.accountingOkWriterId = accountingOkWriterId;
	}

	public Date getAccountingOkDate() {
		return accountingOkDate;
	}

	public void setAccountingOkDate(Date accountingOkDate) {
		this.accountingOkDate = accountingOkDate;
	}
}
