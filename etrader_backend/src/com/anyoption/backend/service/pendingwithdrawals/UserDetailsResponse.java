package com.anyoption.backend.service.pendingwithdrawals;

import com.anyoption.common.service.results.MethodResult;

public class UserDetailsResponse extends MethodResult {
	
	private UserDetails userDetails;

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
}
