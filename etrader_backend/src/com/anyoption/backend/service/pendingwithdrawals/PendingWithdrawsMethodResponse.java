package com.anyoption.backend.service.pendingwithdrawals;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author ivan.petkov
 *
 */
public class PendingWithdrawsMethodResponse extends MethodResult {
	
	private ArrayList<PendingWithdraw> pendingWithdraw;

	public ArrayList<PendingWithdraw> getPendingWithdraw() {
		return pendingWithdraw;
	}

	public void setPendingWithdraw(ArrayList<PendingWithdraw> pendingWithdraw) {
		this.pendingWithdraw = pendingWithdraw;
	}
}