/**
 * 
 */
package com.anyoption.backend.service.withdraw;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.tabakov
 *
 */
public class WithdrawServices {
	
	private static final String SCREEN_NAME_WITHDRAW = "withdraw";
	private static final String FILTER_TRANSACTION_TYPES_GERMAN_WITHDRAW = "transactionTypesGermanWithdraw";
	private static final Logger log = Logger.getLogger(WithdrawServices.class);
	private static final int WITHDRAW_AMOUNT_HIGHER_THAN_USER_BALANCE = 787;
	
	public static ScreenSettingsResponse getWithdrawScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_WITHDRAW, response, httpRequest);
		
		HashMap<Long, String> transactionTypes = new HashMap<>(); 
		
		for (Map.Entry<Long, String> entry : TransactionManager.getAllTransactionTypes().entrySet()) {
			Long key = entry.getKey();
			if (key == TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW || key == TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW || key == TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) {
				transactionTypes.put(key, entry.getValue());
			}
		}
		
		filters.put(FILTER_TRANSACTION_TYPES_GERMAN_WITHDRAW, transactionTypes);
		response.setScreenFilters(filters);
		return response;
	}
	
	
	public static MethodResult withdrawInsertRecord(WithdrawInsertMethodRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		
		if (isValid(request)) {
			WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			request.setWriterId(writer.getWriter().getId());
			try {
				UserBase user = UsersManager.getUserByUserIdService(request.getUserId());
				request.setRate(CurrencyRatesManagerBase.getInvestmentsRate(user.getCurrencyId()));
				if (request.getAmount() != null) {
					long amount = CommonUtil.calcAmount(request.getAmount());
					if (amount > user.getBalance()) {
						response.setErrorCode(WITHDRAW_AMOUNT_HIGHER_THAN_USER_BALANCE);
						return response;
					}
					request.setConvertedAmount(amount);
				}
				
				return TransactionManager.insertWithdraw(request);
			} catch (SQLException ex) {
				log.debug("Can not get user with id: " + request.getUserId() + ex);
			}
			
		} else {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		
		return response;
	}
	
	private static boolean isValid(WithdrawInsertMethodRequest request) {
		if (request == null) {
			return false;
		}

		if (request.getComment() == null || request.getComment().equals("")) {
			return false;
		}

		if (request.getAmount() == null || request.getAmount() <= 0) {
			return false;
		}

		if (request.getUserId() == null || request.getUserId() <= 0) {
			return false;
		}

		if (request.getWithdrawTypeId() == null) {
			return false;
		}
		return true;
	}
	
}
