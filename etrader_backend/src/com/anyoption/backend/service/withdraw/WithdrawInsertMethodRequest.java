/**
 * 
 */
package com.anyoption.backend.service.withdraw;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class WithdrawInsertMethodRequest extends UserMethodRequest{

	private Long withdrawTypeId;
	private Double amount;
	private String comment;
	private Long userId;
	private double rate;
	private long convertedAmount;


	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "WithdrawInsertMethodRequest: " + ls + super.toString() + ls + "withdrawTypeId: " + withdrawTypeId
				+ ls + "amount: " + amount + ls + "comment: " + comment + ls + "userId: " + userId + ls + "rate: " + rate + ls + "convertedAmount: " + convertedAmount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getWithdrawTypeId() {
		return withdrawTypeId;
	}

	public void setWithdrawTypeId(Long withdrawTypeId) {
		this.withdrawTypeId = withdrawTypeId;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public long getConvertedAmount() {
		return convertedAmount;
	}

	public void setConvertedAmount(long convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

}
