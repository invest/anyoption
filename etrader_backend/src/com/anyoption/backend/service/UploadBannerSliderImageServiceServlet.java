package com.anyoption.backend.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.PromotionManagementManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.backend.service.AnyoptionService;
import il.co.etrader.util.CommonUtil;

public class UploadBannerSliderImageServiceServlet extends HttpServlet {

	private static final long serialVersionUID = 7256494852272342630L;
	private static final Logger log = Logger.getLogger(UploadBannerSliderImageServiceServlet.class);
	private static final String FILE_PART_NAME = "myFile";
	private static final String FILE_NAME_PARAMETER = "fileName";
	private static final String BANNER_ID_PARAMETER = "bannerId";


	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		log.debug("UploadBannerSliderImageServiceServlet.doPost");

		MethodResult result = new MethodResult();

		String partName = null;

		String fileName = request.getParameter(FILE_NAME_PARAMETER);
		Long bannerId = Long.valueOf(request.getParameter(BANNER_ID_PARAMETER));
		
		log.debug("Try upload file wiht parameters are: fileName: " + fileName);

		try {
			for (Part part : request.getParts()) {
				if (part.getName().equals(FILE_PART_NAME) && part.getContentType() != null) {
					partName = part.getName();
				}
			}
		} catch (IllegalStateException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (IOException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (ServletException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}

		if (!PromotionManagementManager.validateImageExtension(fileName)) { 
			log.debug("File extension not allowed: " + fileName);
			PromotionManagementManager.deleteImageByName(fileName);
			result.setErrorCode(CommonJSONService.ERROR_CODE_NOT_ALLOWED_EXTENSION_SLIDER_IMAGE); 
		} else {	
			try {
				if (result.getErrorCode() == AnyoptionService.ERROR_CODE_SUCCESS) {
					BufferedInputStream bis = new BufferedInputStream(request.getPart(partName).getInputStream());
					boolean isUploaded = PromotionManagementManager.saveFile(fileName, bis, bannerId);
					if(isUploaded == true){
						result.addUserMessage("fileName", fileName);
						log.debug("Uploaded fileName:" + fileName);
					} else {
						ArrayList<ErrorMessage> errorMessages = new ArrayList<ErrorMessage>();
						ErrorMessage message = new ErrorMessage();
						String[] params = new String[2];
						params[0] = String.valueOf(PromotionManagementManager.getBannerDimensions().get(bannerId).getWidth());
						params[1] = String.valueOf(PromotionManagementManager.getBannerDimensions().get(bannerId).getHeight());
						message.setMessage(CommonUtil.getMessage("error.wrong_dimensions_slider_image", params, Locale.ENGLISH));
						errorMessages.add(message);
						result.setUserMessages(errorMessages);
						result.setErrorCode(CommonJSONService.ERROR_CODE_WRONG_DIMENSIONS_SLIDER_IMAGE); 
						log.error("Can't upload file!" + fileName);
					}
				}
			} catch (IllegalStateException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			} catch (IOException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			} catch (ServletException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
			}
		}

		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			String jsonResponse = gson.toJson(result);
			if (null != jsonResponse && jsonResponse.length() > 0) {
				log.debug(jsonResponse);
				byte[] data = jsonResponse.getBytes("UTF-8");
				OutputStream os = response.getOutputStream();
				response.setHeader("Content-Type", "application/json");
				response.setHeader("Content-Length", String.valueOf(data.length));
				os.write(data, 0, data.length);
				os.flush();
				os.close();
			}
		} catch (IOException e) {
			log.error("Unable to serialize response", e);
		}

	}
}