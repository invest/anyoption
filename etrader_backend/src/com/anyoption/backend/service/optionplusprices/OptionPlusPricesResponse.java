package com.anyoption.backend.service.optionplusprices;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.OptionPlusPriceMatrixItem;

public class OptionPlusPricesResponse extends MethodResult {
	
	private ArrayList<OptionPlusPriceMatrixItem> optionPlusMatrix;

	public ArrayList<OptionPlusPriceMatrixItem> getOptionPlusMatrix() {
		return optionPlusMatrix;
	}

	public void setOptionPlusMatrix(ArrayList<OptionPlusPriceMatrixItem> optionPlusMatrix) {
		this.optionPlusMatrix = optionPlusMatrix;
	}
}
