package com.anyoption.backend.service.optionplusprices;

import java.util.Map;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;

public class OptionPlusPricesFilterResponse extends PermissionsGetResponse {
	
	private Map<Long, String> optionPlusMarkets;

	public Map<Long, String> getOptionPlusMarkets() {
		return optionPlusMarkets;
	}

	public void setOptionPlusMarkets(Map<Long, String> optionPlusMarkets) {
		this.optionPlusMarkets = optionPlusMarkets;
	}
}
