package com.anyoption.backend.service.copyopusercopiers;

import java.util.List;

import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.service.results.MethodResult;

public class CopyopUserCopiersMethodResponse extends MethodResult {
	
	private List<CopyopUserCopy> userCopyList;
	
	public List<CopyopUserCopy> getUserCopyList() {
		return userCopyList;
	}

	public void setUserCopyList(List<CopyopUserCopy> list) {
		this.userCopyList = list;
	}
}
