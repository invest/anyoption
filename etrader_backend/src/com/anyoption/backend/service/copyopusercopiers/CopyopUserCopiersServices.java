package com.anyoption.backend.service.copyopusercopiers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.CopyopUserCopiersManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.common.dto.base.CopyConfig;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

public class CopyopUserCopiersServices {
	private static final Logger log = Logger.getLogger(CopyopUserCopiersServices.class);
	//private static final String SCREEN_NAME_COPYOP_USER_COPY = "copyopUserCopy";
	private static final String USER_OFFSET_ID = "userOffsetId";
	private static final String TIMRE_CREATED_OFFSET = "timeCreatedOffset";
	private static final String FILTER_STILL_COPYING = "stillCopying";
	private static final String FILTER_IS_FROZEN = "isFrozen";
	private static final String FILTER_YES = "Yes";
	private static final String FILTER_NO = "No";
	
	//@BackendPermission(id = "copyopUserCopy_view")
	//TODO 
	//Insert screen permission in the database !!!
	public static ScreenSettingsResponse getCopyopUserCopiersScreenSettings(UserMethodRequest request,
			HttpServletRequest httpRequest) {
		log.debug("getCopyopUserCopiersScreenSettings: " + request);
		ScreenSettingsResponse response = new ScreenSettingsResponse();
//		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
//		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_COPYOP_USER_COPY, response,
//				httpRequest);
		
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();

		HashMap<Long, String> stillCopying = new HashMap<Long, String>();
		stillCopying.put(new Long (ConstantsBase.FILTER_YES), FILTER_YES);
		stillCopying.put(new Long (ConstantsBase.FILTER_NO), FILTER_NO);
		filters.put(FILTER_STILL_COPYING, stillCopying);
		
		HashMap<Long, String> isFrozen = new HashMap<Long, String>();
		isFrozen.put(new Long (ConstantsBase.FILTER_YES), FILTER_YES);
		isFrozen.put(new Long (ConstantsBase.FILTER_NO), FILTER_NO);
		filters.put(FILTER_IS_FROZEN, isFrozen);
		
		response.setScreenFilters(filters);
		return response;
	}

	public static CopyopUserCopiersMethodResponse getUserCopiers(CopyopUserCopiersMethodRequest request,
			HttpServletRequest httpRequest) {
		CopyopUserCopiersMethodResponse response = new CopyopUserCopiersMethodResponse();
		int userOffsetId = 0;
		Date timeCreatedOffset = null;

		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}

		if (request.isNext()) {
			if (httpRequest.getSession().getAttribute(USER_OFFSET_ID) != null) {
				userOffsetId = Integer.parseInt(httpRequest.getSession().getAttribute(USER_OFFSET_ID).toString());
			}

			if (httpRequest.getSession().getAttribute(TIMRE_CREATED_OFFSET) != null) {
				String date = (httpRequest.getSession().getAttribute(TIMRE_CREATED_OFFSET).toString());
				DateFormat formatter = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
				try {
					timeCreatedOffset = (Date) formatter.parse(date);
				} catch (ParseException e) {
					log.error("Cannot parse date stored in http session", e);
				}
			}
		}

		ArrayList<CopyopUserCopy> userCopyList = null;
		userCopyList = CopyopUserCopiersManager.getUsersCopy(request, ProfileLinkTypeEnum.COPIED_BY, userOffsetId,
				timeCreatedOffset);

		if (userCopyList == null) {
			return response;
		}

		httpRequest.getSession().setAttribute(USER_OFFSET_ID, request.getUserOffsetId());
		if (request.getTimeCreatedOffset() != null) {
			httpRequest.getSession().setAttribute(TIMRE_CREATED_OFFSET, request.getTimeCreatedOffset().toString());
		}

		response.setUserCopyList(userCopyList);
		return response;
	}

	public static CopyopUserCopiersMethodResponse getCopiedUsers(CopyopUserCopiersMethodRequest request,
			HttpServletRequest httpRequest) {
		CopyopUserCopiersMethodResponse response = new CopyopUserCopiersMethodResponse();

		int userOffsetId = 0;
		Date timeCreatedOffset = null;

		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}

		if (request.isNext()) {
			if (httpRequest.getSession().getAttribute(USER_OFFSET_ID) != null) {
				userOffsetId = Integer.parseInt(httpRequest.getSession().getAttribute(USER_OFFSET_ID).toString());
			}

			if (httpRequest.getSession().getAttribute(TIMRE_CREATED_OFFSET) != null) {
				String date = (httpRequest.getSession().getAttribute(TIMRE_CREATED_OFFSET).toString());
				DateFormat formatter = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
				try {
					timeCreatedOffset = (Date) formatter.parse(date);
				} catch (ParseException e) {
					log.error("Cannot parse date stored in http session", e);
				}
			}
		}

		ArrayList<CopyopUserCopy> userCopyList = null;
		userCopyList = CopyopUserCopiersManager.getUsersCopy(request, ProfileLinkTypeEnum.COPY, userOffsetId,
				timeCreatedOffset);

		if (userCopyList == null) {
			return response;
		}

		httpRequest.getSession().setAttribute(USER_OFFSET_ID, Long.valueOf(request.getUserOffsetId()));
		if (request.getTimeCreatedOffset() != null) {
			httpRequest.getSession().setAttribute(TIMRE_CREATED_OFFSET, request.getTimeCreatedOffset().toString());
		}
		response.setUserCopyList(userCopyList);
		return response;
	}
	
	public static CopyopUserCopiersMethodResponse stop(CopyopStopUserRequest request,
			HttpServletRequest httpRequest) {
		CopyopUserCopiersMethodResponse response = new CopyopUserCopiersMethodResponse();
		
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}

		if (request.getCopyopUser() == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			log.error("The request user for stopping is not received");
			return response;
		}
		
		ProfileLinksManager.deleteCopyProfileLink(request.getUserId(),
				new CopyConfig(request.getUserId(), request.getCopyopUser().getUserId(), request.getCopyopUser().getInvCount(),
						request.getCopyopUser().getAmount(), request.getCopyopUser().getAssets()),
				request.getUserClassId() == ConstantsBase.USER_CLASS_TEST,
				ProfileManager.isFrozenProfile(request.getCopyopUser().getUserId()));
		
		return response;
	}
}
