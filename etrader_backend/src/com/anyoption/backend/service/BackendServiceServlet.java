package com.anyoption.backend.service;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonServiceServlet;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

/**
 * @author Jamal
 */
public class BackendServiceServlet extends CommonServiceServlet {
	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(BackendServiceServlet.class);

	@Override
	public void init() {
		log.info("Backend Service init...");
		super.init();
	}
	
	protected String getPackage(String className){
		String pkg = "com.anyoption.backend.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
	
	protected boolean isHavePermission(Method method, HttpServletRequest request) {
		boolean res = false;
		BackendPermission annotation = method.getAnnotation(BackendPermission.class);
		if (annotation != null) {
			WriterWrapper writerWrapper = (WriterWrapper) request.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			long roleId = writerWrapper.getWriter().getRoleId();
			
			return WriterPermisionsManager.isExistPermission(roleId, annotation.id());
		} else {
			res = true;
		}
		return res;
	}
	
	protected boolean isWriterInSession(HttpServletRequest request) {
		boolean res = true;
		WriterWrapper writerWrapper = (WriterWrapper) request.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		if(writerWrapper == null){
			res = false;
		}		
		return res;
	}
}