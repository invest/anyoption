/**
 * 
 */
package com.anyoption.backend.service.transaction;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionChargebackRequest extends UserMethodRequest {

	private Long statusId;
	private Long timeCreated;
	private String description;
	private Long issueId;
	private String utcOffsetCreated;
	private String arn;
	private Long timePos;
	private Long timeChb;
	private Long timeRep;
	private Long timeSettled;
	private Long state;
	private Long transactionId;
	private Long userId;
	private String writerUtcOffset;

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "TransactionGetAllMethodRequest: " + super.toString() + "statusId: " + statusId + ls
				+ "timeCreated: " + timeCreated + ls + "description: " + description + ls + "issueId: " + issueId + ls
				+ "utcOffsetCreated: " + utcOffsetCreated + ls + "arn: " + arn + ls + "timePos: " + timePos + ls
				+ "timeChb: " + timeChb + ls + "timeRep: " + timeRep + ls + "timeSettled: " + timeSettled + ls
				+ "state: " + state + ls + "transactionId: " + transactionId + ls + "userId: " + userId + ls + "writerUtcOffset: " + writerUtcOffset + ls;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public String getArn() {
		return arn;
	}

	public void setArn(String arn) {
		this.arn = arn;
	}

	public Long getTimePos() {
		return timePos;
	}

	public void setTimePos(Long timePos) {
		this.timePos = timePos;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTimeChb() {
		return timeChb;
	}

	public void setTimeChb(Long timeChb) {
		this.timeChb = timeChb;
	}

	public Long getTimeRep() {
		return timeRep;
	}

	public void setTimeRep(Long timeRep) {
		this.timeRep = timeRep;
	}

	public Long getTimeSettled() {
		return timeSettled;
	}

	public void setTimeSettled(Long timeSettled) {
		this.timeSettled = timeSettled;
	}

	public String getWriterUtcOffset() {
		return writerUtcOffset;
	}

	public void setWriterUtcOffset(String writerUtcOffset) {
		this.writerUtcOffset = writerUtcOffset;
	}

}
