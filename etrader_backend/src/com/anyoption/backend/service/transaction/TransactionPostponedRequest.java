/**
 * 
 */
package com.anyoption.backend.service.transaction;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionPostponedRequest extends UserMethodRequest {

	private Long id;
	private long transactionId;
	private long numberOfDays;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(long numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TransactionPostponedRequest: "
            + super.toString()
            + "id: " + id + ls
            + "transactionId: " + transactionId + ls
            + "numberOfDays: " + numberOfDays + ls;
    }
}
