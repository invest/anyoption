/**
 * 
 */
package com.anyoption.backend.service.gbg;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.FilesDAOBase;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.GBGManagerBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.userdocuments.UserDocumentsDAO;

import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.ConstantsBase;

public class GBGManager extends GBGManagerBase {

	private static final Logger logger = Logger.getLogger(GBGManager.class);
	
	public static final long GBG_INSERTED_STATUS_ID = 0l;
	public static final long GBG_INSERTED_INPROCESS_ID = 1l;
	public static final long GBG_INSERTED_DONE_ID = 2l;
	
	public static final long GBG_PEP_SUSPEND = 1l;
	public static final long GBG_LOW_SCORE = 2l;
	public static final long GBG_APPROVE_FILE = 3l;
	
	public static final long REJECT_REASON_NEED_PEP_CONFIRAMTION = 47;
	public static final long REJECT_REASON_LOW_GBG_SCORE = 48;
	
	
	public static void insertResponse(long userId) {
		Connection conn = null;
		try {
			conn = getConnection();
			GBGDAO.insertResponse(userId, conn);
		} catch (SQLException ex) {
			logger.info("Fail to insert GBG row due to: ", ex);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<GBGRequest> loadGBGUsersForSend() {
			Connection conn = null;
			ArrayList<GBGRequest> res = new ArrayList<>();
			try {
				conn = getConnection();
				res = GBGDAO.loadGBGUsersForSend(conn);
			} catch (SQLException ex) {
				logger.info("Fail to loadGBGUsersForSend: ", ex);				
			} finally {
				closeConnection(conn);
			}			
			return res;
	}
	
	public static void updateGBGStatus(long id, long statusId) {
		Connection conn = null;
		try {
			conn = getConnection();
			GBGDAO.updateGBGStatus(id, statusId, conn);
		} catch (SQLException ex) {
			logger.info("Fail to updateGBGStatus: ", ex);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static long updateGBGResponse(GBGResponse response) {
		Connection conn = null;
		long res = 0;
		try {
			conn = getConnection();
			res = GBGDAO.updateGBGResponse(conn, response);
		} catch (SQLException ex) {
			logger.info("Fail to updateGBGResponse: ", ex);				
		} finally {
			closeConnection(conn);
		}		
		return res;
	}

	public static void uploadGBGFileId(GBGResponse response, long userId, long stateAfterGBG) throws Exception {
		Connection conn = null;
		try {
			
			boolean isApproved = false;
			Long approvedWriterId = null;
			boolean isReject = false;
			Long rejectReasonId = 0l;
			Long rejectWriterId = null;
			
			if(stateAfterGBG == GBG_PEP_SUSPEND){
				isReject = true;
				rejectReasonId = REJECT_REASON_NEED_PEP_CONFIRAMTION;
				rejectWriterId = (long) Writer.WRITER_ID_AUTO;
			}
			
			if(stateAfterGBG == GBG_LOW_SCORE){
				isReject = true;
				rejectReasonId = REJECT_REASON_LOW_GBG_SCORE;
				rejectWriterId = (long) Writer.WRITER_ID_AUTO;
			}
			
			if(stateAfterGBG == GBG_APPROVE_FILE){
				isApproved = true;
				approvedWriterId = (long) Writer.WRITER_ID_AUTO;
			}
			logger.debug("GBG Try to upload file for response[" + response + "]");
	    	InputStream stream = new ByteArrayInputStream(response.getXmlResponse().getBytes(StandardCharsets.UTF_8));
	    	BufferedInputStream bis = new BufferedInputStream(stream);
	    	String fileName = FilesManagerBase.saveFile("gbgResponse.xml", bis, userId, FileType.GBG_FILE, true, false);			
	    	conn = getConnection();
			long fileId = FilesDAOBase.insertUploadFileWithStatus(conn, userId, 
														  FileType.GBG_FILE, 
														  Writer.WRITER_ID_GBG, 
														  fileName, 
														  File.STATUS_DONE, 
														  null, isApproved, approvedWriterId, 
														  isReject, rejectReasonId, rejectWriterId);
			GBGDAO.updateGBGFileId(response.getId(), fileId, conn);
			logger.debug("GBG uploaded file with fileID[" + fileId + "]");
			UserDocumentsDAO.setCurrentFlagDocs(conn, userId, FileType.GBG_FILE, null);
			
			if(stateAfterGBG == GBG_PEP_SUSPEND){
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(userId);
				UserRegulationManager.getUserRegulation(ur);
				ur.setPepState(UserRegulationBase.PEP_AUTO_PROHIBITED);
				QuestionnaireManagerBase.updateUserPepAnswer(ur);
				IssuesManagerBase.insertUserRegulationPEPRestrictedIssue(userId, Writer.WRITER_ID_GBG, "", true);
				logger.debug("GBG inserted Pep issue for userId[" + userId + "]");
				try {
					logger.debug("Try to send support PEP Mail");
					UserBase user = UsersManagerBase.getUserById(userId);
					Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
					user.setLocale(locale);
					QuestionnaireManagerBase.sendEmailInCaseUserAnswearNoOnPEPQuestion(user, new Date(), true);
					UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, (long)Writer.WRITER_ID_WEB, user, null, new String(), new String(), null);
					logger.debug("GBG send Mail Pep to userId[" + userId + "]");
				} catch (Exception e) {
					logger.error("When send PEP mails ", e);
				}
			}
			
		} finally {
			closeConnection(conn);
		}
	}
	
	public static GBGResponse getGBGResult(long fileId) throws SQLException {
		Connection conn = null;		
		try {
			conn = getConnection();
			return  GBGDAO.getGBGResult(fileId, conn);				
		} finally {
			closeConnection(conn);
		}		
	}
}

