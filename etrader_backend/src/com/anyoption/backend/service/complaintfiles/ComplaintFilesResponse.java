package com.anyoption.backend.service.complaintfiles;

import java.util.List;

import com.anyoption.common.beans.File;
import com.anyoption.common.service.results.MethodResult;

public class ComplaintFilesResponse extends MethodResult {
	
	private List<File> allFiles;

	public List<File> getAllFiles() {
		return allFiles;
	}

	public void setAllFiles(List<File> allFiles) {
		this.allFiles = allFiles;
	}

}
