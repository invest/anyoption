package com.anyoption.backend.service.defaultfiletype;

public class AllDefaultFileType {
	
	private long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String country;
	private long skinId;
	private long defaultDocumentsCount;
	private long lastDefaultDocumentId;
	private long lastMatchDate;
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public long getSkinId() {
		return skinId;
	}
	
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	public long getDefaultDocumentsCount() {
		return defaultDocumentsCount;
	}
	
	public void setDefaultDocumentsCount(long defaultDocumentsCount) {
		this.defaultDocumentsCount = defaultDocumentsCount;
	}
	
	public long getLastDefaultDocumentId() {
		return lastDefaultDocumentId;
	}
	
	public void setLastDefaultDocumentId(long lastDefaultDocumentId) {
		this.lastDefaultDocumentId = lastDefaultDocumentId;
	}
	
	public long getLastMatchDate() {
		return lastMatchDate;
	}
	
	public void setLastMatchDate(long lastMatchDate) {
		this.lastMatchDate = lastMatchDate;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "AllDefaultFileType: "
            + super.toString()
            + "userId: " + userId + ls
        	+ "userName: " + userName + ls
        	+ "firstName: " + firstName + ls
        	+ "lastName: " + lastName + ls
        	+ "country: " + country + ls
        	+ "skinId: " + skinId + ls
        	+ "defaultDocumentsCount: " + defaultDocumentsCount + ls
        	+ "lastDefaultDocumentId: " + lastDefaultDocumentId + ls
        	+ "lastMatchDate: " + lastMatchDate + ls;
	}
}
