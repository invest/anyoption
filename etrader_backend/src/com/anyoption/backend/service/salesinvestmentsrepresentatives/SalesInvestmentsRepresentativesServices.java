package com.anyoption.backend.service.salesinvestmentsrepresentatives;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.SalesInvestmentsManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.beans.SalesInvestments;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author Eyal Goren
 *
 */
public class SalesInvestmentsRepresentativesServices {
	
	private static final String SCREEN_NAME_SALES_INVESTMENTS_REPRESENTATIVES = "salesInvestmentsRepresentatives";
	private static final Logger logger = Logger.getLogger(SalesInvestmentsRepresentativesServices.class);
	
	public static ScreenSettingsResponse getSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_SALES_INVESTMENTS_REPRESENTATIVES, response, httpRequest);
		return response;
	}
	
	public static SalesInvestmentsRepresentativesMethodResponse getSalesInvestmentsRep(SalesInvestmentsRepresentativesMethodRequest request,
			HttpServletRequest httpRequest) {
		SalesInvestmentsRepresentativesMethodResponse response = new SalesInvestmentsRepresentativesMethodResponse();
		
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		if (request.getToDate() == 0 || request.getFromDate() == 0) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		Calendar cFrom = Calendar.getInstance();
		cFrom.setTimeInMillis(request.getFromDate());
		
		Calendar cTo = Calendar.getInstance();
		cTo.setTimeInMillis(request.getToDate());
				
		if (cFrom.after(cTo)) { //to date must be after from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		cTo.add(Calendar.DAY_OF_MONTH, -35);
		if (cTo.after(cFrom)) { //to date cant be more then 31 days from from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		if (request.getWriterId() == 0) {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			request.setWriterId(writerWrapper.getWriter().getId());
		}
		 
		ArrayList<SalesInvestments> salesInvestments = null;
		try {
			salesInvestments = SalesInvestmentsManager.getSalesInvestmentsRep(request, response);
		} catch (SQLException e) {
			logger.error("Unable to get Sales Investments Rep", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if (salesInvestments == null){
			return response;
		}		
		response.setSalesInvestments(salesInvestments);
		
		return response;
	}
	
	

}
