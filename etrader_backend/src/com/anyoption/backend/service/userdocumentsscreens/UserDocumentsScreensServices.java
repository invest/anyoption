package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;

public class UserDocumentsScreensServices {
	private static final Logger log = Logger.getLogger(UserDocumentsScreensServices.class);	
	
	private static final String CALL_CENTER_SCREEN_NAME = "callCenter";
	private static final String BACK_OFFICE_SCREEN_NAME = "backOffice";
	public static final String FILTER_EXIST_PW = "pendingWithdrawlFilter";
	public static final String FILTER_DAYS_LEFT = "daysLeftFilter";
	public static final String FILTER_USER_SUSPENDED = "userSuspendedFilter";
	public static final String FILTER_USER_ACTIVE = "userActiveFilter";
	public static final String FILTER_COMPLIANCE_APPROVED = "complianceApprovedFilter";
	public static final String FILTER_RESOLUTION = "resolutionFilter";
	public static final String FILTER_DOC_UPLOADED = "docUploadedFilter";
	public static final String FILTER_DOC_TYPE = "docTypeFilter";
	public static final String FILTER_CALL_ATTEMPTS = "callAttempts";
	public static final String FILTER_SORTED_BY = "sortedBy";
	public static final String FILTER_EXPIRE_DOC = "expireDoc";
	public static final long SCREEN_CALLCENTER = 1;
	public static final long SCREEN_BACKOFFICE = 2;
	
	@BackendPermission(id = "callCenter_view")
	public static UserDocumentsScreenSettingsResponse getCallCenterScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getCallCenterScreenSettings:" + request);		
		UserDocumentsScreenSettingsResponse response = new UserDocumentsScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> screenFilters = new HashMap<String, HashMap<Long, String>>();
		HashMap<String, HashMap<String, ArrayList<Integer>>> daysLeftFilters = new HashMap<>();
		
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(CALL_CENTER_SCREEN_NAME, response, httpRequest);
		try {			
			setFilters(screenFilters, daysLeftFilters, writer);	
			 
			//Call Attempts
			 HashMap<Long, String> ca = new HashMap<Long, String>();
			 ca.put(0l, "0");			 
			 ca.put(1l, "1");
			 ca.put(2l, "2");
			 ca.put(3l, "3");
			 ca.put(4l, "4");
			 ca.put(5l, "5");
			 ca.put(6l, "6+");
			 screenFilters.put(FILTER_CALL_ATTEMPTS, ca);
			 
			 //Sorted By
			 HashMap<Long, String> ob = new HashMap<Long, String>();
			 ob.put(1l, "Days left");			 
			 ob.put(2l, "Call attempts");
			 screenFilters.put(FILTER_SORTED_BY, ob);
			 
			 //Expire Doc
			 HashMap<Long, String> exp = new HashMap<Long, String>();
			 exp.put(1l, "About to expire");			 
			 exp.put(2l, "Expired");
			 screenFilters.put(FILTER_EXPIRE_DOC, exp);
		} catch (Exception e) {
			log.error("Unable to getCallCenterScreenSettings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setScreenFilters(screenFilters);
		response.setDaysLeftFilters(daysLeftFilters);
		
		return response;
	}
	
	@BackendPermission(id = "backOffice_view")
	public static UserDocumentsScreenSettingsResponse getBackOfficeScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getBackOfficeScreenSettings:" + request);		
		UserDocumentsScreenSettingsResponse response = new UserDocumentsScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> screenFilters = new HashMap<String, HashMap<Long, String>>();
		HashMap<String, HashMap<String, ArrayList<Integer>>> daysLeftFilters = new HashMap<>();
		
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(BACK_OFFICE_SCREEN_NAME, response, httpRequest);
		try {			
			setFilters(screenFilters, daysLeftFilters, writer);			 
		} catch (Exception e) {
			log.error("Unable to getBackOfficeScreenSettings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setScreenFilters(screenFilters);
		response.setDaysLeftFilters(daysLeftFilters);
		
		return response;
	}

	@BackendPermission(id = "callCenter_view")
	public static UserDocumentsScreenGetResponse getCallCenterDocuments (UserDocumentsScreenGetRequest request, HttpServletRequest httpRequest) {		
		log.debug("getCallCenterDocuments:" + request);
		UserDocumentsScreenGetResponse result = new UserDocumentsScreenGetResponse();		
		try {
			setWriterSkins(request, httpRequest);
			result.setUserDocuments(UserDocumentsScreenManager.getCallCenterDocuments(request, getWriterUtcOffset(httpRequest)));
		} catch (Exception e) {
			log.error("When getCallCenterDocuments", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	
	@BackendPermission(id = "backOffice_view")
	public static UserDocumentsScreenGetResponse getBackOfficeDocuments (UserDocumentsScreenGetRequest request, HttpServletRequest httpRequest) {		
		log.debug("getBackOfficeDocuments:" + request);
		UserDocumentsScreenGetResponse result = new UserDocumentsScreenGetResponse();		
		try {
			setWriterSkins(request, httpRequest);
			result.setUserDocuments(UserDocumentsScreenManager.getBackOfficeDocuments(request, getWriterUtcOffset(httpRequest)));
		} catch (Exception e) {
			log.error("When getBackOfficeDocuments", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	@BackendPermission(id = "backOffice_view")
	public static UserDocumentsUserDataGetResponse getUserDocumentsUserDataBackOffice(UserDocumentsUserDataGetRequest request, HttpServletRequest httpRequest) {
		long screen = SCREEN_BACKOFFICE;
		return getUserDocumentsUserData(request, httpRequest, screen);
	}
	
	@BackendPermission(id = "callCenter_view")
	public static UserDocumentsUserDataGetResponse getUserDocumentsUserDataCallCenter(UserDocumentsUserDataGetRequest request, HttpServletRequest httpRequest) {
		long screen = SCREEN_CALLCENTER;
		return getUserDocumentsUserData(request, httpRequest, screen);
	}
	
	private static UserDocumentsUserDataGetResponse getUserDocumentsUserData (UserDocumentsUserDataGetRequest request, HttpServletRequest httpRequest, long screen) {				
		log.debug("getBackOfficeDocuments:" + request);
		UserDocumentsUserDataGetResponse result = new UserDocumentsUserDataGetResponse();		
		try {
			WriterWrapper w = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			long writerId = w.getWriter().getId();
			log.debug("Try to lock userId [" + request.getUserId() + "] from writerId[" + writerId + "]");
			String lockedfromWriter = UserDocumentsScreenManager.lockDocumentStatus(httpRequest.getSession().getId(), 
																					writerId, request.getUserId(), screen);
			if(!CommonUtil.isParameterEmptyOrNull(lockedfromWriter)){
				String msg = "Customer [" + request.getUserId() + "] is locked by writer[" + lockedfromWriter + "]";
				result.setErrorCode(CommonJSONService.ERROR_CODE_USER_DOC_IS_ALREADY_LOCKED);			
				result.addErrorMessage(null, msg);
				result.addUserMessage(null, msg);
				return result;
			}
			result = UserDocumentsScreenManager.getUserDocumentsUserData(request);
		} catch (Exception e) {
			log.error("When getUserDocumentsUserData", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	@BackendPermission(id = "backOffice_regulationApprove")
	public static MethodResult regulationApprove (UserDocumentsUserDataGetRequest request) {		
		log.debug("regulationApprove:" + request);
		MethodResult result = new MethodResult();		
		try {	
			
			if(CommonUtil.isParameterEmptyOrNull(request.getComments())){
				log.error("Comment is empty, UserId[" + request.getUserId() + "] can't regualtion Approve!");
				result.setErrorCode(CommonJSONService.ERROR_CODE_GENERAL_VALIDATION);
				return result;
			}
			
			if(UserDocumentsScreenManager.isCanRegulationApprove(request.getUserId())){
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(request.getUserId());
				ur.setWriterId(request.getWriterId());				
				ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_CONTROL_APPROVED_USER);
				ur.setComments(request.getComments());
				UserRegulationManager.updateRegulationStep(ur);		
				log.debug("UserId[" + request.getUserId() + "] was regualtion Approved!");
			} else {
				log.error("UserId[" + request.getUserId() + "] can't regualtion Approve!");
				result.setErrorCode(CommonJSONService.ERROR_CODE_GENERAL_VALIDATION);
			}
		} catch (Exception e) {
			log.error("When regulationApprove", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	@BackendPermission(id = "backOffice_regulationApproveManager")
	public static MethodResult regulationApproveManager (UserDocumentsUserDataGetRequest request) {

		log.debug("regulationApproveManager:" + request);
		MethodResult result = new MethodResult();		
		try {	
			if(CommonUtil.isParameterEmptyOrNull(request.getComments())){
				log.error("Comment is empty, UserId[" + request.getUserId() + "] can't regualtion Approve!");
				result.setErrorCode(CommonJSONService.ERROR_CODE_REGULATION_CAN_NOT_FINAL_APPROVE);
				return result;
			}			
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(request.getUserId());
			ur.setWriterId(request.getWriterId());			
			ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_CONTROL_APPROVED_USER);
			ur.setComments(request.getComments());
			UserRegulationManager.updateRegulationStep(ur);				
			log.debug("UserId[" + request.getUserId() + "] was regualtion Approved from Manager:[" + request.getWriterId() + "] ");
		} catch (Exception e) {
			log.error("When regulationApproveManager", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	@BackendPermission(id = "callCenter_view")
	public static MethodResult unlockCallcenter(MethodRequest request, HttpServletRequest httpRequest) {
		long screen = SCREEN_CALLCENTER;
		return unlock(request, httpRequest, screen);
	}
	
	@BackendPermission(id = "backOffice_view")
	public static MethodResult unlockBackoffice(MethodRequest request, HttpServletRequest httpRequest) {
		long screen = SCREEN_BACKOFFICE;
		return unlock(request, httpRequest, screen);
	}
	
	@BackendPermission(id = "backOffice_view")
	public static MethodResult saveComments(UserFileCommentsRequest request, HttpServletRequest httpRequest) {
		MethodResult result = new MethodResult();
		try {
			WriterWrapper w = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			long writerId = w.getWriter().getId();
			UserDocumentsScreenManager.saveComments(request.getUserId(), writerId, request.getComments(), getWriterUtcOffset(httpRequest));
		} catch (Exception e) {
			log.error("When saving comments", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	private static MethodResult unlock(MethodRequest request, HttpServletRequest httpRequest, long screenId) {
		log.debug("unlock:" + request);
		MethodResult result = new MethodResult();		
		try {					
			WriterWrapper w = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			long writerId = w.getWriter().getId();
			log.debug("Try to unlock users for writerId [" + writerId + "]");		
			UserDocumentsScreenManager.unlockDocumentStatusByWriter(writerId, screenId);
		} catch (Exception e) {
			log.error("When unlock", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return result;
	}
	
	private static void setFilters(HashMap<String, HashMap<Long, String>> screenFilters,
			HashMap<String, HashMap<String, ArrayList<Integer>>> daysLeftFilters, WriterWrapper writer) {
		//Users ClassType
		 HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
		 screenFilters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
		 //Remove API classType
		 screenFilters.get(TransactionManager.FILTER_CLASS_USERS).remove(3l);
		 
		//business case
		HashMap<Long, String> skinBusinessCases = TransactionManager.getSkinBusinessCases();
		screenFilters.put(TransactionManager.FILTER_SKIN_BUSINESS_CASES, skinBusinessCases);
		 
		 //skins
		 HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(SkinsManagerBase.getAllSkinsFilter(), writer, log);
		 screenFilters.put(SkinsManagerBase.FILTER_SKINS, skins);
		 
		 //countries
		 HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		 screenFilters.put(CountryManagerBase.FILTER_COUNTRIES, countries);	
		
		 //Pending Withdraw
		 HashMap<Long, String> pw = new HashMap<Long, String>();
		 pw.put(1l, "Yes");
		 pw.put(0l, "No");			 
		 screenFilters.put(FILTER_EXIST_PW, pw);
		
		 //Days Left Filter
		 daysLeftFilters.put(FILTER_DAYS_LEFT, getDaysLeftFilter());
		 
		 //User Suspended
		 HashMap<Long, String> suspended = new HashMap<Long, String>();
		 suspended.put(1l, "Yes");
		 suspended.put(0l, "No");			 
		 screenFilters.put(FILTER_USER_SUSPENDED, suspended);
		 
		 //User Active
		 HashMap<Long, String> userActive = new HashMap<Long, String>();
		 userActive.put(1l, "Yes");
		 userActive.put(0l, "No");			 
		 screenFilters.put(FILTER_USER_ACTIVE, userActive);
		 
		 //Compliance Approved
		 HashMap<Long, String> complianceApproved = new HashMap<Long, String>();
		 complianceApproved.put(1l, "Yes");
		 complianceApproved.put(0l, "No");			 
		 screenFilters.put(FILTER_COMPLIANCE_APPROVED, complianceApproved);
		 
		 HashMap<Long, String> resolution = new HashMap<Long, String>();
		 resolution.put(1l, "Missing");
		 resolution.put(2l, "Pending");
		 resolution.put(3l, "Approved");
		 resolution.put(4l, "Rejected");
		 screenFilters.put(FILTER_RESOLUTION, resolution);
		 
		 //Compliance Approved
		 HashMap<Long, String> docUploaded = new HashMap<Long, String>();
		 docUploaded.put(1l, "Yes");
		 docUploaded.put(0l, "No");			 
		 screenFilters.put(FILTER_DOC_UPLOADED, complianceApproved);
		 
		 HashMap<Long, String> docType = new HashMap<Long, String>();
		 docType.put(1l, "Proof of Identity(POI)");
		 docType.put(2l, "Proof of Residence(POR)");	
		 docType.put(3l, "Proof of Payment(POP)");			 
		 screenFilters.put(FILTER_DOC_TYPE, docType);
	}
	
	private static HashMap<String, ArrayList<Integer>> getDaysLeftFilter(){
		 HashMap<String, ArrayList<Integer>> df = new LinkedHashMap<>();		 
		 ArrayList<Integer> arr0 = new ArrayList<>();
		 arr0.add(0);
		 df.put("0", arr0);
		 ArrayList<Integer> arr1 = new ArrayList<>();
		 arr1.add(1);
		 arr1.add(2);
		 arr1.add(3);
		 df.put("1-3", arr1);
		 ArrayList<Integer> arr4 = new ArrayList<>();
		 arr4.add(4);
		 arr4.add(5);
		 arr4.add(6);
		 arr4.add(7);
		 df.put("4-7", arr4);
		 ArrayList<Integer> arr8 = new ArrayList<>();
		 arr8.add(8);
		 arr8.add(9);
		 arr8.add(10);
		 arr8.add(11);
		 df.put("8-11", arr8);
		 ArrayList<Integer> arr12 = new ArrayList<>();
		 arr12.add(12);
		 arr12.add(13);
		 arr12.add(14);
		 df.put("12-14", arr12);		 
		 return df;
	}
	
	private static void setWriterSkins(UserDocumentsScreenGetRequest request, HttpServletRequest httpRequest) {
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		ArrayList<Integer> skinsList = request.getSkins();
		skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writerWrapper);
		request.setSkins(skinsList);
	}
	
	private static String getWriterUtcOffset(HttpServletRequest httpRequest){
		WriterWrapper w = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		return w.getUtcOffset();
	}
}