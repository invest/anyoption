/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;

public class UserDocumentsScreenSettingsResponse extends ScreenSettingsResponse {
	
	private HashMap<String, HashMap<String, ArrayList<Integer>>> daysLeftFilters;

	public HashMap<String, HashMap<String, ArrayList<Integer>>> getDaysLeftFilters() {
		return daysLeftFilters;
	}

	public void setDaysLeftFilters(HashMap<String, HashMap<String, ArrayList<Integer>>> daysLeftFilters) {
		this.daysLeftFilters = daysLeftFilters;
	}


}
