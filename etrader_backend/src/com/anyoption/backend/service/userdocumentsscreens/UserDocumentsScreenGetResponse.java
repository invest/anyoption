/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.IdsFiles;

public class UserDocumentsScreenGetResponse extends MethodResult {
	
	private ArrayList<UserDocuments> UserDocuments;

	public ArrayList<UserDocuments> getUserDocuments() {
		return UserDocuments;
	}

	public void setUserDocuments(ArrayList<UserDocuments> userDocuments) {
		UserDocuments = userDocuments;
	}
}
