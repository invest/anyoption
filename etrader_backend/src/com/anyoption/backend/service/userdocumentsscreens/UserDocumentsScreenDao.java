package com.anyoption.backend.service.userdocumentsscreens;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.util.Utils;
import oracle.jdbc.OracleTypes;

public class UserDocumentsScreenDao extends DAOBase {
	
	private static final Logger log = Logger.getLogger(UserDocumentsScreenDao.class);	
	
	public static ArrayList<UserDocuments> getCallCenterDocuments(Connection conn, UserDocumentsScreenGetRequest request, String writerUtcOffset) throws SQLException {
		ArrayList<UserDocuments> res = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		double currEurRate = CurrencyRatesManagerBase.getEurRate();
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_CALL_CENTER_SCREENS(   O_DATA => ? " +
																					   " ,I_FTD_DATE_FROM => ? " +
																					   " ,I_FTD_DATE_TO => ? " +
																					   " ,I_FE_DATE_FROM => ? " +
																					   " ,I_FE_DATE_TO => ? " +
																					   " ,I_USER_ID => ? " +
																					   " ,I_USERN_NAME => ? " +
																					   " ,I_USER_CLASSES => ? " +
																					   " ,I_SKINS => ? " +
																					   " ,I_COUNTRIES => ? " +
																					   " ,I_IS_HAVE_PW => ? " +
																					   " ,I_DAYS_LEFT => ? " +
																					   " ,I_IS_SPENDED => ? " +
																					   " ,I_IS_USER_ACTIVE => ? " +
																					   " ,I_IS_COMPLIANCE_APPROVED => ? " +
																					   " ,I_DOCUMENTS_UPLOADED => ? " +
																					   " ,I_FROM_TOTAL_DEPOSIT => ? " +
																					   " ,I_TO_TOTAL_DEPOSIT => ? " +
																					   " ,I_CALL_ATTEMPS => ? " +
																					   " ,I_RATE => ? " +
																					   " ,I_ORDER_BY_ID => ? " +
																					   " ,I_ASC_DESC_PARAM => ? " +
																					   " ,I_PAGE_NUMBER => ? " +
																					   " ,I_ROWS_PER_PAGE => ? " +
																					   " ,I_DOC_TYPE_1 => ? " +
																					   " ,I_RESOLUTION_1 => ? " +
																					   " ,I_DOC_TYPE_2 => ? " +
																					   " ,I_RESOLUTION_2 => ? " +
																					   " ,I_EXPIRE_DOC => ? " +
																					   " ,I_BUSINESS_CASES => ?)}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(new Timestamp(request.getFtdDateFrom() == null ? 0 : request.getFtdDateFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getFtdDateTo() == null ? 0 : request.getFtdDateTo() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getFirstEngagementDateFrom() == null ? 0 : request.getFirstEngagementDateFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getFirstEngagementDateTo() == null ? 0 : request.getFirstEngagementDateTo() + 1), index++, cstmt);
			setStatementValue(request.getUserId(), index++, cstmt);
			setStatementValue(request.getUserName(), index++, cstmt);
			setStatementValue(request.getUserClasses(), index++, cstmt);
			setArrayVal(request.getSkins(), index++, cstmt, conn);
			setArrayVal(request.getCountries(), index++, cstmt, conn);
			setStatementValue(request.getPendingWithdrawal(), index++, cstmt);
			setArrayVal(request.getDaysLeft(), index++, cstmt, conn);
			setStatementValue(request.getSuspended(), index++, cstmt);
			setStatementValue(request.getActive(), index++, cstmt);
			setStatementValue(request.getComplianceApproved(), index++, cstmt);
			setStatementValue(request.getDocUploaded(), index++, cstmt);
			setStatementValue(request.getFromDeposits(), index++, cstmt);
			setStatementValue(request.getToDeposits(), index++, cstmt);
			setStatementValue(request.getCallAttempts(), index++, cstmt);
			setStatementValue(currEurRate, index++, cstmt);
			setStatementValue(request.getOrderById(), index++, cstmt);
			setStatementValue(request.getAscDesc(), index++, cstmt);
			setStatementValue(request.getPage(), index++, cstmt);
			setStatementValue(request.getRowsPerPage(), index++, cstmt);
			setStatementValue(request.getDocType1(), index++, cstmt);
			setArrayVal(request.getResolutionStatus1(), index++, cstmt, conn);
			setStatementValue(request.getDocType2(), index++, cstmt);
			setArrayVal(request.getResolutionStatus2(), index++, cstmt, conn);
			setStatementValue(request.getExpireDoc(), index++, cstmt);
			setArrayVal(request.getBusinessCases(), index++, cstmt, conn);
			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				UserDocuments ud = new UserDocuments(rs.getLong("user_id"), 
													 rs.getString("first_name"), 
													 rs.getString("last_name"), 
													 rs.getString("country_name"), 
													 rs.getString("skin_name"),
													 rs.getLong("skin_id"), 
													 rs.getBoolean("is_regulation_approved"), 
													 rs.getLong("compl_docs_upload"),
													 rs.getLong("sum_deposits"),
													 rs.getLong("days_left"), 
													 rs.getLong("total_count"),
													 rs.getLong("call_attemps"),
													 rs.getTimestamp("last_call_time") != null ? 
															 convertToDate(rs.getTimestamp("last_call_time")).getTime() : null,
												     rs.getTimestamp("last_reached_time") != null ? 
														     convertToDate(rs.getTimestamp("last_reached_time")).getTime() : null);
				res.add(ud);				
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}
	
	public static ArrayList<UserDocuments> getBackOfficeDocuments(Connection conn, UserDocumentsScreenGetRequest request, String writerUtcOffset) throws SQLException {
		ArrayList<UserDocuments> res = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		double currEurRate = CurrencyRatesManagerBase.getEurRate();
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_BACK_OFFICE_SCREENS(   O_DATA => ? " +
					   																   " ,I_FE_DATE_FROM => ? " +
					   																   " ,I_FE_DATE_TO => ? " +
																					   " ,I_USER_ID => ? " +
																					   " ,I_USERN_NAME => ? " +
																					   " ,I_USER_CLASSES => ? " +
																					   " ,I_SKINS => ? " +
																					   " ,I_COUNTRIES => ? " +
																					   " ,I_IS_HAVE_PW => ? " +
																					   " ,I_DAYS_LEFT => ? " +
																					   " ,I_IS_SPENDED => ? " +
																					   " ,I_IS_USER_ACTIVE => ? " +
																					   " ,I_IS_COMPLIANCE_APPROVED => ? " +
																					   " ,I_FROM_TOTAL_DEPOSIT => ? " +
																					   " ,I_TO_TOTAL_DEPOSIT => ? " +
																					   " ,I_RATE => ? " +
																					   " ,I_ORDER_BY_ID => ? " +
																					   " ,I_ASC_DESC_PARAM => ? " +
																					   " ,I_PAGE_NUMBER => ? " +
																					   " ,I_ROWS_PER_PAGE => ? " +
																					   " ,I_DOC_TYPE_1 => ? " +
																					   " ,I_RESOLUTION_1 => ? " +
																					   " ,I_DOC_TYPE_2 => ? " +
																					   " ,I_RESOLUTION_2 => ? " +
																					   " ,I_BUSINESS_CASES => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(new Timestamp(request.getFirstEngagementDateFrom() == null ? 0 : request.getFirstEngagementDateFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getFirstEngagementDateTo() == null ? 0 : request.getFirstEngagementDateTo() + 1), index++, cstmt);
			setStatementValue(request.getUserId(), index++, cstmt);
			setStatementValue(request.getUserName(), index++, cstmt);
			setStatementValue(request.getUserClasses(), index++, cstmt);
			setArrayVal(request.getSkins(), index++, cstmt, conn);
			setArrayVal(request.getCountries(), index++, cstmt, conn);
			setStatementValue(request.getPendingWithdrawal(), index++, cstmt);
			setArrayVal(request.getDaysLeft(), index++, cstmt, conn);
			setStatementValue(request.getSuspended(), index++, cstmt);
			setStatementValue(request.getActive(), index++, cstmt);
			setStatementValue(request.getComplianceApproved(), index++, cstmt);
			setStatementValue(request.getFromDeposits(), index++, cstmt);
			setStatementValue(request.getToDeposits(), index++, cstmt);
			setStatementValue(currEurRate, index++, cstmt);
			setStatementValue(request.getOrderById(), index++, cstmt);
			setStatementValue(request.getAscDesc(), index++, cstmt);
			setStatementValue(request.getPage(), index++, cstmt);
			setStatementValue(request.getRowsPerPage(), index++, cstmt);
			setStatementValue(request.getDocType1(), index++, cstmt);
			setArrayVal(request.getResolutionStatus1(), index++, cstmt, conn);
			setStatementValue(request.getDocType2(), index++, cstmt);
			setArrayVal(request.getResolutionStatus2(), index++, cstmt, conn);
			setArrayVal(request.getBusinessCases(), index++, cstmt, conn);
			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				UserDocuments ud = new UserDocuments(rs.getLong("user_id"), 
													 rs.getString("first_name"), 
													 rs.getString("last_name"), 
													 rs.getString("country_name"), 
													 rs.getString("skin_name"),
													 rs.getLong("skin_id"), 
													 rs.getBoolean("is_regulation_approved"), 
													 rs.getLong("compl_docs_upload"),
													 rs.getLong("sum_deposits"),
													 rs.getLong("days_left"), 
													 rs.getLong("total_count"),
													 null,
													 null,
													 null);
				res.add(ud);				
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}
	
	public static UserDocumentsUserDataGetResponse getUserDocumentsUserData(Connection conn, UserDocumentsUserDataGetRequest request) throws SQLException {
		UserDocumentsUserDataGetResponse ud = new UserDocumentsUserDataGetResponse();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ResultSet rsFiles = null;
		ResultSet rsComments = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_USER_DATA_BY_ID (O_DATA => ? " +
																				     " ,O_FILE_DATA => ? " +
																				     " ,O_USER_FILE_COMMENTS => ? " +
																				     " ,O_IS_ENABLE_REG_APPROVE => ? " +
																				     " ,O_IS_ENABLE_REG_APPROVE_MAN => ? " +
																				     " ,I_USER_ID => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, Types.INTEGER);
			cstmt.registerOutParameter(index++, Types.INTEGER);
			setStatementValue(request.getUserId(), index++, cstmt);
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				ud.setUserId(rs.getLong("user_id"));
				if (rs.getTimestamp("registration_date") != null) {
					ud.setRegistrionDate(convertToDate(rs.getTimestamp("registration_date")).getTime());
				}
				if (rs.getTimestamp("questionnaire_done_date") != null) {
					ud.setEngagementDate(convertToDate(rs.getTimestamp("questionnaire_done_date")).getTime());
				}
				ud.setAccountManagerName(rs.getString("account_manager"));
				ud.setBalance(rs.getLong("balance"));
				ud.setBalanceCurrencyId(rs.getLong("currency_id"));
				ud.setPhoneNumber(rs.getString("mobile_phone"));
				if (rs.getTimestamp("last_login_date") != null) {
					ud.setLastLoginDate(convertToDate(rs.getTimestamp("last_login_date")).getTime());
				}
				ud.setCallAttempts(rs.getLong("call_attemps"));
				if (rs.getTimestamp("last_call_time") != null) {
					ud.setLastCallTime(convertToDate(rs.getTimestamp("last_call_time")).getTime());
				}
				if (rs.getTimestamp("last_reached_time") != null) {
					ud.setLastReachedTime(convertToDate(rs.getTimestamp("last_reached_time")).getTime());
				}
			}
			 
			rsFiles = (ResultSet) cstmt.getObject(2);
			Locale locale = new Locale("en");
			ArrayList<UserDocumentsFile> files = new ArrayList<>();
			while (rsFiles.next()) {
				Long expDate = null;
				if (rsFiles.getTimestamp("exp_date") != null) {
					expDate = convertToDate(rsFiles.getTimestamp("exp_date")).getTime();
				}
				UserDocumentsFile f = new UserDocumentsFile(rsFiles.getLong("file_id"), 
															CommonUtil.getMessage(locale, rsFiles.getString("file_type_name"), null), 
															rsFiles.getBoolean("is_approved"), 
															rsFiles.getBoolean("is_support_rejected"), 
															rsFiles.getString("writer_approved"), 
															rsFiles.getString("writer_rejected"),
															expDate,
															CommonUtil.getMessage(locale, rsFiles.getString("reason_name"), null),
															rsFiles.getString("cc_exp"),
															rsFiles.getLong("file_type_id"));
				if(rsFiles.getTimestamp("time_uploaded") != null) {
					f.setReceivedAt(convertToDate(rsFiles.getTimestamp("time_uploaded")).getTime());
				} else {
					f.setReceivedAt(null);
				}
				
				String ccNumber = rsFiles.getString("cc_number");
				if (!CommonUtil.isParameterEmptyOrNull(ccNumber)){
					String ccName = CommonUtil.getMessage(locale, rsFiles.getString("cc_type_name"),null);
					try {
						ccNumber = AESUtil.decrypt(ccNumber);
				} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
							| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
					// FIXME very bad way of handling exceptions
					throw new SQLException(ce.getMessage());
				}

					int l = ccNumber.length();
					if (l > 4){
						f.setCcName(ccNumber.substring(l - 4, l) + "  " + ccName);
					}
				}
				
				files.add(f);
			}
			rsComments = (ResultSet) cstmt.getObject(3);
			if (rsComments.next()) {
				ud.setComments(rsComments.getString("comments"));
			}
			ud.setFiles(files);			
			ud.setEnableRegulationApprove(cstmt.getBoolean(4));
			ud.setEnableRegulationApproveManager(cstmt.getBoolean(5));
			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
			closeResultSet(rsFiles);
			closeResultSet(rsComments);
		}
		return ud;
	}
	
	public static boolean isCanRegulationApprove(Connection conn, long userId) throws SQLException {
		CallableStatement cstmt = null;
		Boolean res ;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.IS_CAN_REGULATION_APPROVE( O_IS_CAN_APPROVE => ? " +
																					   " ,I_USER_ID => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			setStatementValue(userId, index++, cstmt);			
			cstmt.execute();			
			res = cstmt.getBoolean(1);			
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static Long getDocumentStatus(Connection conn, long userId) throws SQLException {
		CallableStatement cstmt = null;
		Long res ;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.GET_DOCUMENT_STATUS( O_DOCUMENT_STATUS => ? " +
																			     " ,I_USER_ID => ? )}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			setStatementValue(userId, index++, cstmt);			
			cstmt.execute();			
			res = cstmt.getLong(1);			
		} finally {
			closeStatement(cstmt);
		}
		return res;
	}
	
	public static String lockDocumentStatus(Connection conn, String sessionId, long writerId, long userId, long screenId) throws SQLException {
		CallableStatement cstmt = null;
		String lockedWriter = null;
		int index = 1;
		try {
			String serverId = ServerConfiguration.getInstance().getServerName();
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.LOCK_USER_DOC(    O_LOCKED_WRITER_NAME => ? " +
																			  " ,I_SESSION_ID => ? " +
																			  " ,I_WRITER_ID => ? " +
																			  " ,I_USER_ID => ?" +
																			  " ,I_SERVER_ID => ?" +
																			  ",I_SCREEN_ID => ?)}"); 																					   
			cstmt.registerOutParameter(index++, OracleTypes.VARCHAR);
			setStatementValue(sessionId, index++, cstmt);
			setStatementValue(writerId, index++, cstmt);
			setStatementValue(userId, index++, cstmt);
			setStatementValue(serverId, index++, cstmt);
			setStatementValue(screenId, index++, cstmt);
			cstmt.execute();			
			
			lockedWriter = cstmt.getString(1);
			if(!CommonUtil.isParameterEmptyOrNull(lockedWriter)){
				log.debug("Can't lock userID[" + userId + "] he is locked from writer[" + lockedWriter + "]");
			} else{
				log.debug("userID[" + userId + "] was locked from writer[" + writerId + "]");
			}
		} finally {
			closeStatement(cstmt);
		}
		return lockedWriter;
	}
	
	public static void unlockDocumentStatus(Connection conn, String sessionId, Long writerId, String serverId, Long screenId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		log.debug("Try to unlock doc users for writer[" + writerId + "] and sessionId[" + sessionId + "] and serverId[" + serverId + "]");
		try {
			cstmt = conn.prepareCall("{call PKG_USER_DOCUMENTS.UNLOCK_USER_DOC(  I_SESSION_ID => ? " +
																			  " ,I_WRITER_ID => ? "  +
																			  " ,I_SERVER_ID => ? "  +
																			  " ,I_SCREEN_ID => ?)}"); 																					   
			setStatementValue(sessionId, index++, cstmt);
			setStatementValue(writerId, index++, cstmt);
			setStatementValue(serverId, index++, cstmt);
			setStatementValue(screenId, index++, cstmt);
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
		}
	}
}