package com.anyoption.backend.service.userdocumentsscreens;

import java.io.Serializable;

public class UserDocumentsFile implements Serializable {
	
	public UserDocumentsFile(long fileId, String fileTypeName, boolean isApproved, boolean isRejected,
			String writerApproved, String writerRejected, Long expDate, String rejectReason, String ccExp, long fileType) {
		super();
		this.fileId = fileId;
		this.fileTypeName = fileTypeName;
		this.isApproved = isApproved;
		this.isRejected = isRejected;
		this.writerApproved = writerApproved;
		this.writerRejected = writerRejected;
		this.expDate = expDate;
		this.rejectReason = rejectReason;
		this.ccExp = ccExp;
		this.fileType = fileType;
	}
	
	private static final long serialVersionUID = 1L;				
	
	private long fileId;
	private String fileTypeName;
	private boolean isApproved;
	private boolean isRejected;
	private String writerApproved;
	private String writerRejected;
	private Long expDate;
	private String rejectReason;
	private String ccExp;
	private long fileType;
	private Long receivedAt;
	private String ccName;
	
	
	public long getFileType() {
		return fileType;
	}
	public void setFileType(long fileType) {
		this.fileType = fileType;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public String getFileTypeName() {
		return fileTypeName;
	}
	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	public boolean isRejected() {
		return isRejected;
	}
	public void setRejected(boolean isRejected) {
		this.isRejected = isRejected;
	}
	public String getWriterApproved() {
		return writerApproved;
	}
	public void setWriterApproved(String writerApproved) {
		this.writerApproved = writerApproved;
	}
	public String getWriterRejected() {
		return writerRejected;
	}
	public void setWriterRejected(String writerRejected) {
		this.writerRejected = writerRejected;
	}
	public Long getExpDate() {
		return expDate;
	}
	public void setExpDate(Long expDate) {
		this.expDate = expDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getCcExp() {
		return ccExp;
	}
	public void setCcExp(String ccExp) {
		this.ccExp = ccExp;
	}
	public Long getReceivedAt() {
		return receivedAt;
	}
	public void setReceivedAt(Long receivedAt) {
		this.receivedAt = receivedAt;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}		
}
