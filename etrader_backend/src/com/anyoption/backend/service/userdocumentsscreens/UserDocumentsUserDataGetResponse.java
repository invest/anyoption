/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class UserDocumentsUserDataGetResponse extends MethodResult {
		
		private long userId;
		private Long registrionDate;
		private Long engagementDate;
		private String accountManagerName;
		private Long balance;
		private Long balanceCurrencyId;
		private String phoneNumber;
		private Long lastLoginDate;
		private boolean isEnableRegulationApprove;
		private boolean isEnableRegulationApproveManager;		
		private ArrayList<UserDocumentsFile> files;
		private Long callAttempts;
		private Long lastCallTime;
		private Long lastReachedTime;
		private String comments;
				
		public long getUserId() {
			return userId;
		}
		public void setUserId(long userId) {
			this.userId = userId;
		}
		public Long getRegistrionDate() {
			return registrionDate;
		}
		public void setRegistrionDate(Long registrionDate) {
			this.registrionDate = registrionDate;
		}
		public Long getEngagementDate() {
			return engagementDate;
		}
		public void setEngagementDate(Long engagementDate) {
			this.engagementDate = engagementDate;
		}
		public String getAccountManagerName() {
			return accountManagerName;
		}
		public void setAccountManagerName(String accountManagerName) {
			this.accountManagerName = accountManagerName;
		}
		public Long getBalance() {
			return balance;
		}
		public void setBalance(Long balance) {
			this.balance = balance;
		}
		public Long getBalanceCurrencyId() {
			return balanceCurrencyId;
		}
		public void setBalanceCurrencyId(Long balanceCurrencyId) {
			this.balanceCurrencyId = balanceCurrencyId;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public Long getLastLoginDate() {
			return lastLoginDate;
		}
		public void setLastLoginDate(Long lastLoginDate) {
			this.lastLoginDate = lastLoginDate;
		}
		public boolean isEnableRegulationApprove() {
			return isEnableRegulationApprove;
		}
		public void setEnableRegulationApprove(boolean isEnableRegulationApprove) {
			this.isEnableRegulationApprove = isEnableRegulationApprove;
		}
		public boolean isEnableRegulationApproveManager() {
			return isEnableRegulationApproveManager;
		}
		public void setEnableRegulationApproveManager(boolean isEnableRegulationApproveManager) {
			this.isEnableRegulationApproveManager = isEnableRegulationApproveManager;
		}
		
	    public String toString() {
	        String ls = System.getProperty("line.separator");
	        return ls + "UserDocumentsUserDataGetResponse: "
	            + super.toString()
	            + "userId" + userId + ls
	            + "registrionDate: " + registrionDate + ls
	            + "engagementDate: " + engagementDate + ls
	            + "accountManagerName: " + accountManagerName + ls
	            + "balance: " + balance + ls
	            + "balanceCurrencyId: " + balanceCurrencyId + ls
	            + "phoneNumber: " + phoneNumber + ls
	            + "lastLoginDate: " + lastLoginDate + ls
	            + "isEnableRegulationApprove: " + isEnableRegulationApprove + ls
	        	+ "isEnableRegulationApproveManager: " + isEnableRegulationApproveManager + ls;	        
	        }
		public ArrayList<UserDocumentsFile> getFiles() {
			return files;
		}
		public void setFiles(ArrayList<UserDocumentsFile> files) {
			this.files = files;
		}
		public Long getCallAttempts() {
			return callAttempts;
		}
		public void setCallAttempts(Long callAttempts) {
			this.callAttempts = callAttempts;
		}
		public Long getLastCallTime() {
			return lastCallTime;
		}
		public void setLastCallTime(Long lastCallTime) {
			this.lastCallTime = lastCallTime;
		}
		public Long getLastReachedTime() {
			return lastReachedTime;
		}
		public void setLastReachedTime(Long lastReachedTime) {
			this.lastReachedTime = lastReachedTime;
		}
		public String getComments() {
			return comments;
		}
		public void setComments(String comments) {
			this.comments = comments;
		}
}
