package com.anyoption.backend.service.userdocumentsscreens;

import java.io.Serializable;

public class UserDocuments implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public UserDocuments(long userId, String firstName, String lastName, String countryName, String skin, 
			long skinId, boolean isRegulationApproved, long complianceDocUploaded, Long totalDeposit,
			long daysLeft, long totalCount, Long callAttempts, Long lastCallTime, Long lastReachedTime) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.countryName = countryName;
		this.skin = skin;
		this.skinId = skinId;
		this.isRegulationApproved = isRegulationApproved;
		this.complianceDocUploaded = complianceDocUploaded;
		this.totalDeposit = totalDeposit;
		this.daysLeft = daysLeft;
		this.totalCount = totalCount;
		this.callAttempts = callAttempts;
		this.lastCallTime = lastCallTime;
		this.lastReachedTime = lastReachedTime;	
	}
	
	private long userId;
	private String firstName;
	private String lastName;
	private String countryName;
	private String skin;
	private long skinId;
	private boolean isRegulationApproved;
	private Long complianceDocUploaded;
	private Long totalDeposit;
	private long daysLeft;
	private long totalCount;
	private Long callAttempts;
	private Long lastCallTime;
	private Long lastReachedTime;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getSkin() {
		return skin;
	}
	public void setSkin(String skin) {
		this.skin = skin;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	public boolean isRegulationApproved() {
		return isRegulationApproved;
	}
	public void setRegulationApproved(boolean isRegulationApproved) {
		this.isRegulationApproved = isRegulationApproved;
	}
	public Long getTotalDeposit() {
		return totalDeposit;
	}
	public void setTotalDeposit(Long totalDeposit) {
		this.totalDeposit = totalDeposit;
	}
	public long getDaysLeft() {
		return daysLeft;
	}
	public void setDaysLeft(long daysLeft) {
		this.daysLeft = daysLeft;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public Long getComplianceDocUploaded() {
		return complianceDocUploaded;
	}
	public void setComplianceDocUploaded(Long complianceDocUploaded) {
		this.complianceDocUploaded = complianceDocUploaded;
	}
	public Long getCallAttempts() {
		return callAttempts;
	}
	public void setCallAttempts(Long callAttempts) {
		this.callAttempts = callAttempts;
	}
	public Long getLastCallTime() {
		return lastCallTime;
	}
	public void setLastCallTime(Long lastCallTime) {
		this.lastCallTime = lastCallTime;
	}
	public Long getLastReachedTime() {
		return lastReachedTime;
	}
	public void setLastReachedTime(Long lastReachedTime) {
		this.lastReachedTime = lastReachedTime;
	}
	
}
