package com.anyoption.backend.service.files;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.KeesingManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.gbg.GBGManager;
import com.anyoption.backend.service.gbg.GBGResponse;
import com.anyoption.backend.service.keesing.KeesingRequest;
import com.anyoption.backend.service.keesing.KeesingResponse;
import com.anyoption.backend.service.keesing.KeesingServices;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FilesAdditionalInfo;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CreditCardsManagerBase;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.UsersPendingDocsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.CommonServiceServlet;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.FilesManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class FilesServices {	         
	private static final Logger log = Logger.getLogger(FilesServices.class);
	
	private static final String SCREEN_NAME_FILE = "file";
	private static final String PERMISSION_FILE_CONTROL_APPROVE = "file_controlApprove";
	private static final String PERMISSION_FILE_CONTROL_REJECT = "file_controlReject";
	
	private static final String PERMISSION_IDS_CONTROL_APPROVE = "idsFile_controlApprove";
	private static final String PERMISSION_IDS_CONTROL_REJECT = "idsFile_controlReject";
	
	private static final String PERMISSION_FILE_KS_UPDATE_FIRSTNAME = "file_updateFirstName";
	private static final String PERMISSION_FILE_KS_UPDATE_LASTNAME = "file_updateLastName";
	
	private static final String PERMISSION_IDS_KS_UPDATE_FIRSTNAME = "idsFile_updateFirstName";
	private static final String PERMISSION_IDS_KS_UPDATE_LASTNAME = "idsFile_updateLastName";
	
	@BackendPermission(id = "file_view")
	public static FilesScreenSettingsResponse getFileScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getFileScreenSettings:" + request);		
		FilesScreenSettingsResponse response = new FilesScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> screenFilters = new HashMap<String, HashMap<Long, String>>();
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_FILE, response, httpRequest);
		try {
			screenFilters.put(CountryManagerBase.FILTER_COUNTRIES, CountryManagerBase.getCountriesFilter());
			screenFilters.put(FilesManager.FILTER_FOR_REJECT_REASONST, FilesManager.getRejectReasons());
			response.setFileTypes(FilesManager.getFileTypes());
			response.setFileRejectReasons(FilesManager.getFileRejectReasons());
			response.setFilesAdditionalInfo(FilesManager.getFilesAdditionalInfo());
			if(request.getSkinId() == Skin.SKIN_ENGLISH_NON_REG || request.getSkinId() == Skin.SKIN_SPAIN_NON_REG){
				response.setShowControl(false);
				//remove UB from list
				HashMap<Long, FileType> nonRegfileTypes = new HashMap<>();
				nonRegfileTypes.putAll(FilesManager.getFileTypes());
				nonRegfileTypes.remove(FileType.UTILITY_BILL);
				response.setFileTypes(nonRegfileTypes);
			} else {
				response.setShowControl(true);
			}
		} catch (SQLException e) {
			log.error("Unable to getFileScreenSettings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setScreenFilters(screenFilters);
		return response;
	}
	
	@BackendPermission(id = "file_view")
	public static FileGetResponse getFile(FileRequest request, HttpServletRequest httpRequest) {
		log.debug("getFile:" + request);
		FileGetResponse response = new FileGetResponse();
		try {
			WriterWrapper writerWapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			response.setFile(FilesManager.getFileById(request.getFile().getId(), writerWapper.getLocalInfo().getLocale(), request.getPreviousNext()));
			if(response.getFile() == null){
				log.debug("Not found FILE with request" + request);
				response.setErrorCode(CommonJSONService.ERROR_CODE_NO_FOUND_FILE);
				return response;
			}
			//set additional info
			FilesAdditionalInfo addInfo = FilesManager.getFilesAdditionalInfo().get(response.getFile().getFileTypeId()); 
			if( addInfo != null){
				response.getFile().setFilesAdditionalInfo(addInfo);
			}			
			response.setCreditCards(TransactionsManagerBase.getAllCreditCardsByUserHM(response.getFile().getUserId()));
		} catch (Exception e) {
			log.error("Unable to getFile", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
		
	@BackendPermission(id = "file_save")
	public static MethodResult updateInsertFile(FileRequest request, HttpServletRequest httpRequest) {
		log.debug("updateInsertFile:" + request);
		MethodResult response = new MethodResult();
		boolean isUpdate = false;
		try {			
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			FilesManagerBase.updateExpDate(request.getFile());
			if(request.getFile().getId() > 0){			
				isUpdate = true;
				File beforeUpdateFile = FilesManager.getFileById(request.getFile().getId(), writerWrapper.getLocalInfo().getLocale(), 0);
				log.debug("Try to Update, before Update File is: " + beforeUpdateFile);
				
				//Permission Checks
				String permissionContorlApprove = PERMISSION_FILE_CONTROL_APPROVE;
				String permissionContorlReject = PERMISSION_FILE_CONTROL_REJECT;
				String permKsUpdateFirstName = PERMISSION_FILE_KS_UPDATE_FIRSTNAME;
				String permKsUpdateLastName = PERMISSION_FILE_KS_UPDATE_LASTNAME;
				if(request.isIDsScreen()){
					permissionContorlApprove = PERMISSION_IDS_CONTROL_APPROVE;
					permissionContorlReject = PERMISSION_IDS_CONTROL_REJECT;
					permKsUpdateFirstName = PERMISSION_IDS_KS_UPDATE_FIRSTNAME;
					permKsUpdateLastName = PERMISSION_IDS_KS_UPDATE_LASTNAME;
				}
				
				
				if(request.getFile().isControlApproved() || request.getFile().isControlReject()){
					if(beforeUpdateFile.isControlApproved() != request.getFile().isControlApproved() && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permissionContorlApprove)){
						log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permissionContorlApprove);
						response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
						return response;
					}			
					if(beforeUpdateFile.isControlReject() != request.getFile().isControlReject() && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permissionContorlReject)){
						log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permissionContorlReject);
						response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
						return response;
					}					
				}
				
				if(!CommonUtil.isParameterEmptyOrNull(beforeUpdateFile.getFileName()) && request.isLoadedFile()){
					log.error("Can't re upload the file, must createa new: fileId" + request.getFile().getId());
					response.setErrorCode(CommonJSONService.ERROR_EXIST_UPLOAD_FILE);
					return response;					
				}
				
				if(request.getFile().getFilesKeesingMatch() != null){
					if(request.getFile().getFilesKeesingMatch().getFlagFirstName() != null){// has firstName flag
						if(beforeUpdateFile.getFilesKeesingMatch() != null){
							if(beforeUpdateFile.getFilesKeesingMatch().getFlagFirstName() != null){// has file with first name flag
								if(request.getFile().getFilesKeesingMatch().getFlagFirstName() != beforeUpdateFile.getFilesKeesingMatch().getFlagFirstName()){
									if(request.getFile().getFilesKeesingMatch().getFlagFirstName() == 2 && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permKsUpdateFirstName)){
										log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permKsUpdateFirstName);
										response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
										return response;
									}
								}
							}
						}else{
							if(request.getFile().getFilesKeesingMatch().getFlagFirstName() == 2 && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permKsUpdateFirstName)){
								log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permKsUpdateFirstName);
								response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
								return response;
							}
						}
					}

					if(request.getFile().getFilesKeesingMatch().getFlagLastName() != null){// has lastName flag
						if(beforeUpdateFile.getFilesKeesingMatch() != null){
							if(beforeUpdateFile.getFilesKeesingMatch().getFlagLastName() != null){// has file with lastName flag
								if(request.getFile().getFilesKeesingMatch().getFlagLastName() != beforeUpdateFile.getFilesKeesingMatch().getFlagLastName()){
									if(request.getFile().getFilesKeesingMatch().getFlagLastName() == 2 && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permKsUpdateLastName)){
										log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permKsUpdateLastName);
										response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
										return response;
									}
								}
							}
						}else{
							if(request.getFile().getFilesKeesingMatch().getFlagLastName() == 2 && !WriterPermisionsManager.isExistPermission(writerWrapper.getWriter().getRoleId(), permKsUpdateLastName)){
								log.error("Writer:" + writerWrapper.getWriter().getId() + " don't have permission: " + permKsUpdateLastName);
								response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
								return response;
							}
						}
					}
				}				
				
			} else {
				log.debug("Try to Insert");
				UserBase user = UsersManager.getUserByUserIdService(request.getFile().getUserId());
				request.getFile().setTimeCreated(new Date());
				request.getFile().setUtcOffsetCreated(user.getUtcOffset());
			}
			
			//Common File Checks and set fields
			if(!commonFileChecks(request.getFile(), response)){
				log.debug("File checks is faild!");
				return response;
			}			
			setSupportControlFields(request.getFile(), writerWrapper.getWriter());			
			request.getFile().setWriterId(writerWrapper.getWriter().getId());
			request.getFile().setTimeUpdated(new Date());
			
			if(request.isLoadedFile()){
				request.getFile().setUploaderId(writerWrapper.getWriter().getId());
				request.getFile().setUploadDate(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
			}
			
			if(!request.getFile().isSupportReject()){
				request.getFile().setRejectReason(0);
			}
			
			if(!request.getFile().isControlReject()){
				request.getFile().setControlRejectReason(0);
			}
			
			setFileStatus(request.getFile(), isUpdate, request.isLoadedFile());
			
			if(isUpdate){
				FilesManager.updateFileBE(request.getFile());
				if(request.getFile().isCurrent()){
					UserDocumentsManager.setCurrentFlagDocsManual(request.getFile().getId());
				}
				log.debug("The file updated:" + request.getFile());
			} else {
				FilesManager.insertFileBE(request.getFile());
				log.debug("The file inserted:" + request.getFile());			
			}
			
			if(request.getFile().isSupportReject() || request.getFile().isControlReject()) {				
				UsersManager.sendRejectMail(request.getFile());
				log.debug("Sended Reject Mail");
			}			

			
			log.debug("Calculate pending docs for file:" + request.getFile().getId());
			UsersPendingDocsManagerBase.calculatePendingDocs(request.getFile().getId(), UsersPendingDocsManagerBase.EMPTY_PARAM);
			
			if(request.isLoadedFile()){
				UsersPendingDocsManagerBase.updateLastUploadFileTime(request.getFile().getId());
			}	
		} catch (Exception e) {
			log.error("Unable to updateFile: " + request + " " , e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if(response.getErrorCode() == CommonJSONService.ERROR_CODE_SUCCESS){
			response = getFile(request, httpRequest);
		}
		
		long fileTypeId = request.getFile().getFileTypeId();
		if ((fileTypeId == FileType.CC_COPY_FRONT || fileTypeId == FileType.CC_COPY_BACK) && request.getFile().isApproved()) {
			try {
				int update = CreditCardsManagerBase.updateCCDocumentSent(fileTypeId, request.getFile().getUserId(), request.getFile().getCcId());
				if (response.getErrorCode() != CommonJSONService.ERROR_CODE_NO_FOUND_FILE && response.getErrorCode() != CommonJSONService.ERROR_CODE_UNKNOWN && update > 0) {
					response.setErrorCode(CommonJSONService.ERROR_CC_DOCUMENT_SENT_UPDATED);//This card was marked as "Documents sent" in the CC menu.
				}
			} catch (SQLException ex) {
				log.error("Fail to update credit card document sent column due to: " + ex);
			}
		}
		return response;
	}

	private static boolean commonFileChecks(File file, MethodResult response)
			throws SQLException {
		if(file.isSupportReject()){
			if(file.getRejectReason() == null ||  file.getRejectReason() == 0){
				log.error("Missing suppoert reject reason: " + file);
				response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_SUPPORT_REJETC_REASON_PERMISSIONS);
				return false;
			}			
			if(file.getRejectReason() == FilesManager.FILE_REJECT_REASON_OTHER  && (file.getComment() == null || file.getComment().length() < 1)){
				log.error("Missing suppoert comments: " + file);
				response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_SUPPORT_REJETC_COMMENT);
				return false;
			}
		}
		
		if(file.isControlReject()){
			if(file.getControlRejectReason() == null ||  file.getControlRejectReason() == 0){
				log.error("Missing control reject reason: " + file);
				response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_CONTROL_REJETC_REASON_PERMISSIONS);
				return false;
			}			
		}	
		
		if (file.getControlRejectReason() == FilesManager.FILE_REJECT_REASON_OTHER 
				&& (file.getControlRejectComment() == null || (file.getControlRejectComment() != null && file.getControlRejectComment().length() < 1))){
			log.error("Missing control comments: " + file);
			response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_CONTROL_REJETC_COMMENT);
			return false;
		}
		
		if(file.getFileName() == null && 
				(file.isApproved() || file.isSupportReject() || file.isControlApproved() || file.isControlReject())){
			log.error("File must be upload: " + file);
			response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_FILE);
			return false;
		}		
		
		if ((file.isApproved() || file.isControlApproved()) 
				&& file.getExpDate() == null
				&& !file.isNotNeeded() 
				&& (file.getFilesKeesingResponse() == null 
						|| (file.getFilesKeesingResponse() != null && file.getFilesKeesingResponse().getCheckStatus() != 0))
				&& (file.getFileTypeId() == FileType.USER_ID_COPY || file.getFileTypeId() == FileType.DRIVER_LICENSE
						|| file.getFileTypeId() == FileType.UTILITY_BILL || file.getFileTypeId() == FileType.PASSPORT
						|| file.getFileTypeId() == FileType.CC_HOLDER_ID || file.getFileTypeId() == FileType.GBG_FILE)) {
			log.error("Exp date must be filled: " + file);
			response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_EXP_DATE);
			return false;
		}
		
		if(file.isApproved() && file.isSupportReject()){
			log.error("The file isApproved and isSupportReject in some time: " + file);
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUPPORT_APPROVED_AND_REJETC);
			return false;
		}
		
		if(file.isControlApproved() && file.isControlReject()){
			log.error("The file isControlApproved and isControlReject in some time: " + file);
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUPPORT_APPROVED_AND_REJETC);
			return false;
		}
		
		if(FilesManager.getFileTypes().get(file.getFileTypeId()).isCreditCardFile()){
			if(file.getCcId() == 0){					
				log.error("Missing Credit CardId: " + file);
				response.setErrorCode(CommonJSONService.ERROR_CODE_MISS_CC_ID);
				return false;
			}
		}		
		return true;
	}

	private static void setSupportControlFields(File file, Writer writer) {
		log.debug("Begin set Support/Control Fields");
		//Support approved
		if (file.isApproved()) {
			file.setSupportReject(false);
			file.setRejectReason(File.REJECT_REASON_NOT_REJECTED);
			file.setTimeSupportApproved(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
			file.setSupportApprovedWriterId(writer.getId());
		} else {				
			file.setTimeSupportApproved(null);
			file.setSupportApprovedWriterId(0);
		}
		
		//Support Reject
		if (file.isSupportReject()) {
			file.setApproved(false);
			file.setFileStatusId(File.STATUS_INVALID);
			file.setRejectionDate(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
			file.setRejectionWriterId(writer.getId());
		} else {
			file.setRejectionDate(null);
			file.setRejectionWriterId(0);
		}
		
		//Control approved
		if (file.isControlApproved()) {
			file.setControlReject(false);
			file.setControlRejectReason(File.REJECT_REASON_NOT_REJECTED);
			file.setTimeControlApproved(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
			file.setControlApprovedWriterId(writer.getId());				
		} else {
			file.setTimeControlApproved(null);
			file.setControlApprovedWriterId(0);
		}
		
		//Control reject
		if (file.isControlReject()) {
			file.setControlApproved(false);
			file.setFileStatusId(File.STATUS_INVALID);
			file.setTimeControlRejected(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
			file.setControlRejectWriterId(writer.getId());
		} else {
			file.setTimeControlRejected(null);
			file.setControlRejectWriterId(0);
		}
	}
	
	private static void setFileStatus(File file, boolean isUpdate, boolean isUploaded) {		
		if(file.getFileStatusId() != File.STATUS_NOT_NEEDED){
			if(!isUpdate){
				file.setFileStatusId(File.STATUS_REQUESTED);
			}			
			if(isUploaded){
				file.setFileStatusId(File.STATUS_IN_PROGRESS);
			}
			if(isUploaded){
				file.setFileStatusId(File.STATUS_IN_PROGRESS);
			}
			if(file.isSupportReject() || file.isControlReject()){
				file.setFileStatusId(File.STATUS_INVALID);				
			}			
			if(file.isApproved() && !file.isControlReject()){
				file.setFileStatusId(File.STATUS_DONE);				
			}			
			if(file.isControlApproved()){
				file.setFileStatusId(File.STATUS_DONE);				
			}
		}	
	}
	
	@BackendPermission(id = "file_sendToKeesing")
	public static FileGetResponse sendKeesingFile(FileRequest request, HttpServletRequest httpRequest) {		
		log.debug("sendKeesingFile:" + request);
		FileGetResponse response = new FileGetResponse();
		try {
    		long fileId = request.getFile().getId();
    		File f = KeesingManager.loadKeesingFileForSend(fileId);
    		if(f != null){
        		String filePath = CommonUtil.getProperty(ConstantsBase.FILES_PATH);
        		String fileName = f.getFileName();
        		
        		log.debug("Try to Send KEESING FileID:" + fileId + " and file:" + filePath + fileName);
        		KeesingRequest requestKeesing = new KeesingRequest(fileId, filePath, fileName);
        		KeesingResponse responseKeesing =  KeesingServices.checkIDDocument(requestKeesing);
        		log.debug("After Sent KEESING Response: " + responseKeesing);
        		
        		if(responseKeesing.getErrorCode() != CommonJSONService.ERROR_CODE_SUCCESS){
        			log.error("Unsuccessful sent manual file to Keesing, mark file ks failed");
        			KeesingManager.markKeesingFileFailed(fileId);
        		}
    		} else {
    			log.error("Unable to sent Keesing file");
    			response.setErrorCode(CommonJSONService.ERROR_CODE_UNABLE_SENT_KEESING_FILE);
    		}    		    		
		} catch (Exception e) {
			log.error("Unable to getFile", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if(response.getErrorCode() == CommonJSONService.ERROR_CODE_SUCCESS){
			response = getFile(request, httpRequest);
		}
		
		return response;
	}
	
	@BackendPermission(id = "file_save")
	public static UserGetCCResponse getUserCC(UserIdRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserCC:" + request);		
		UserGetCCResponse response = new UserGetCCResponse();		
		try {
			response.setCreditCards(TransactionsManagerBase.getAllCreditCardsByUserHM(request.getUserId()));
		} catch (SQLException e) {
			log.error("Unable to getUserCC", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "file_view")
	public static GBGResponse getGBGFile(FileRequest request, HttpServletRequest httpRequest) {
		log.debug("getGBGFile:" + request);
		GBGResponse response = new GBGResponse();
		try {
			response = GBGManager.getGBGResult(request.getFile().getId());
		} catch (SQLException e) {
			log.error("Unable to getGBGFile", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN + "");
		}
		return response;
	}
}