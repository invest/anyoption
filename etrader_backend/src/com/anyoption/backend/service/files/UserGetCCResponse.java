/**
 * 
 */
package com.anyoption.backend.service.files;

import java.util.HashMap;

import com.anyoption.common.service.results.MethodResult;

public class UserGetCCResponse extends MethodResult {
		
	private HashMap<Long, HashMap<String, String>>  creditCards;

	public HashMap<Long, HashMap<String, String>> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(HashMap<Long, HashMap<String, String>> creditCards) {
		this.creditCards = creditCards;
	}

		
}
