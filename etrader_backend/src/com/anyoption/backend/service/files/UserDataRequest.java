package com.anyoption.backend.service.files;

import com.anyoption.common.service.requests.MethodRequest;

public class UserDataRequest extends MethodRequest {

	private long userId;
	private String firstName;
	private String lastName;
	private Long dateOfBirth;
		
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Long getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
