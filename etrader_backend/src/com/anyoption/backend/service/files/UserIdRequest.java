/**
 * 
 */
package com.anyoption.backend.service.files;

import com.anyoption.common.service.requests.UserMethodRequest;

public class UserIdRequest extends UserMethodRequest {
	private long userId;
	private Long dateOfBirth;

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Long getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Long dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
