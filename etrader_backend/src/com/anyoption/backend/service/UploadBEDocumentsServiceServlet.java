package com.anyoption.backend.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.backend.service.AnyoptionService;

public class UploadBEDocumentsServiceServlet extends HttpServlet {

	private static final long serialVersionUID = 7256494852272342630L;
	private static final Logger log = Logger.getLogger(UploadBEDocumentsServiceServlet.class);
	private static final String FILE_PART_NAME = "myFile";
	private static final String FILE_TYPE_PARAMETER = "fileType";
	private static final String FILE_NAME_PARAMETER = "fileName";
	private static final String FILE_ID_PARAMETER = "fileId";
	private static final String CREDIT_USER_ID_PARAMETER = "userId";
	private static final String IS_COMPLAINT_FILE = "isComplaint";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		log.debug("UploadBEDocumentsServiceServlet.doPost");

		MethodResult result = new MethodResult();

		String partName = null;

		String fileName = request.getParameter(FILE_NAME_PARAMETER);
		boolean isComplaint = false;
		if (request.getParameter(IS_COMPLAINT_FILE) != null) {
			isComplaint = Integer.valueOf(request.getParameter(IS_COMPLAINT_FILE)) == 1 ? true : false;
		}				
		long fileType = request.getParameter(FILE_TYPE_PARAMETER) != null ? Long.valueOf(request.getParameter(FILE_TYPE_PARAMETER)) : 0;
		long fileId = request.getParameter(FILE_ID_PARAMETER) != null ? Long.valueOf(request.getParameter(FILE_ID_PARAMETER)) : 0;
		long userId = request.getParameter(CREDIT_USER_ID_PARAMETER) != null ? Long.valueOf(request.getParameter(CREDIT_USER_ID_PARAMETER)) : 0;
		
		log.debug("Try upload file wiht parameters are: fileName: "+ fileName + ", fileType: " + fileType
				+ ", fileId: " + fileId + ", userId: " + userId);

		try {
			for (Part part : request.getParts()) {
				if (part.getName().equals(FILE_PART_NAME) && part.getContentType() != null) {
					partName = part.getName();
				}
			}
		} catch (IllegalStateException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (IOException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (ServletException e) {
			log.error("Unable to upload file", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}

		if (!com.anyoption.common.util.Utils.validateFileExtension(fileName)) {
			log.debug("File extension not allowed: " + fileName);
			FilesManagerBase.deleteFileByName(fileName);
			if (isComplaint) {
				result.setErrorCode(CommonJSONService.ERROR_CODE_NOT_ALLOWED_EXTENSION);
			} else {
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				return;
			}
		} 
		if (CommonUtil.isParameterEmptyOrNull(fileName) && isComplaint) {
			result.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_FILE_NAME);
		}
		try {
			if (result.getErrorCode() == AnyoptionService.ERROR_CODE_SUCCESS) {
				BufferedInputStream bis = new BufferedInputStream(request.getPart(partName).getInputStream());
				String uplFileName = FilesManagerBase.saveFile(fileName, bis, userId, fileType, true, isComplaint);
				if(uplFileName != null){
					result.addUserMessage("fileName", uplFileName);
					log.debug("Uploaded fileName:" + uplFileName);
				} else {
					log.error("Can't upload file!" + fileName);
				}
			}
		} catch (IllegalStateException e) {
			log.error("Unable to read file stream", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (IOException e) {
			log.error("Unable to read file stream", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		} catch (ServletException e) {
			log.error("Unable to read file stream", e);
			result.setErrorCode(AnyoptionService.ERROR_CODE_UNKNOWN);
		}
		
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			String jsonResponse = gson.toJson(result);
			if (null != jsonResponse && jsonResponse.length() > 0) {
				log.debug(jsonResponse);
				byte[] data = jsonResponse.getBytes("UTF-8");
				OutputStream os = response.getOutputStream();
				response.setHeader("Content-Type", "application/json");
				response.setHeader("Content-Length", String.valueOf(data.length));
				os.write(data, 0, data.length);
				os.flush();
				os.close();
			}
		} catch (IOException e) {
			log.error("Unable to serialize response", e);
		}

	}
}