package com.anyoption.backend.service.terms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.managers.TermsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.TermsPartFilesMethodResult;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class TermsServices {
	
	private static final Logger log = Logger.getLogger(TermsServices.class);	
	private static final String SCREEN_NAME_PERMISSIONS = "terms";	
	
	@BackendPermission(id = "terms_view")
	public static TermsScreenSettingsMethodResult getTermsPermisionsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getTermsPermisionsScreenSettings:" + request);		
		TermsScreenSettingsMethodResult response = new TermsScreenSettingsMethodResult();		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS, response, httpRequest);
		try {
			response.setFilters(TermsManagerBase.getTermsFilter());
		} catch (SQLException e) {
			log.error("Unable to getWriterPermisionsScreenSetings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "terms_view")
	public static TermsFileMethodResult getTermsFile(TermsFileMethodRequest request) {
		log.debug("getTermsFile:" + request);
		TermsFileMethodResult result = new TermsFileMethodResult();
		try {
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			if(file.getFileName() != null){
				file.setHtml(getTermsFileBase(file.getFileName()));	
				result.setTermsPartFile(file);
			} else {
				log.debug("Can't find fileName :" + request);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	private static String getTermsFileBase(String fileName) {
		String result = null;
		String dirFileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH) + fileName;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dirFileName), "UTF8"));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			log.error("Can't get termsFile ", e);
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				log.error("Can't close buffer ", e);
			}
		}
		return result;
	}
	
	@BackendPermission(id = "terms_view")
	public static MethodResult saveTermsFile(TermsFileMethodRequest request) {
		log.debug("saveTermsFile:" + request);
		MethodResult result = new MethodResult();
		try {
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			String fileName = file.getFileName();
			if(fileName != null){
				fileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH_PREVIEW) + fileName;
				Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
	            out.write(request.getHtml());
	            out.close();
			} else {
				log.error("Can't find fileName :" + request);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	@BackendPermission(id = "terms_view")
	public static MethodResult copyTermsFileLiveFolder(TermsFileMethodRequest request) {
		log.debug("saveTermsFileLive:" + request);
		MethodResult result = new MethodResult();
		try {
			
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			String fileName = file.getFileName();
			if(fileName != null){
				fileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH_PREVIEW) + fileName;
				String liveFile =  CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH) + file.getFileName();
				TermsManagerBase.updateFile(file.getId(), request);
				copyFile(fileName, liveFile);
			} else {
				log.error("Can't find fileName :" + request);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	private static void copyFile(String sourPath, String destPath) throws IOException {
		Path sourcePath = Paths.get(sourPath);
		Path toDirPath = Paths.get(destPath);
	    Files.copy(sourcePath, toDirPath, StandardCopyOption.REPLACE_EXISTING);
	    log.debug(sourPath + " was copied to " + destPath);
	    
	}
	
	@BackendPermission(id = "terms_view")
	public static TermsPartFilesMethodResult getTermsFiles(TermsFileMethodRequest request) {
		log.debug("getTermsFile:" + request);
		TermsPartFilesMethodResult result = CommonJSONService.getTermsFilesBase(request, true);
		return result;
	}
}