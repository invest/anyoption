package com.anyoption.backend.service.terms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;
import com.anyoption.common.beans.base.TermsParts;

public class TermsScreenSettingsMethodResult extends PermissionsGetResponse {
	
	private Map<Long, Map<Long, ArrayList<TermsParts>>> filters = new HashMap<Long, Map<Long,ArrayList<TermsParts>>>();

	public Map<Long, Map<Long, ArrayList<TermsParts>>> getFilters() {
		return filters;
	}

	public void setFilters(Map<Long, Map<Long, ArrayList<TermsParts>>> filters) {
		this.filters = filters;
	}
}
