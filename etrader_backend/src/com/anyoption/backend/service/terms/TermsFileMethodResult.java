package com.anyoption.backend.service.terms;

import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.service.results.MethodResult;

public class TermsFileMethodResult extends MethodResult {
	
	private TermsPartFile termsPartFile;

	public TermsPartFile getTermsPartFile() {
		return termsPartFile;
	}

	public void setTermsPartFile(TermsPartFile termsPartFile) {
		this.termsPartFile = termsPartFile;
	}

}
