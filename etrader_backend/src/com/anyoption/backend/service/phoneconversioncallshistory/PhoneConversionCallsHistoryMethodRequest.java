package com.anyoption.backend.service.phoneconversioncallshistory;

import com.anyoption.common.service.requests.MethodRequest;

public class PhoneConversionCallsHistoryMethodRequest extends MethodRequest {

	private long userId;
	private long contactId;
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getContactId() {
		return contactId;
	}
	
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
}
