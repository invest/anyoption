package com.anyoption.backend.service.phoneconversioncallshistory;

import com.anyoption.common.service.results.MethodResult;

public class PhoneConversionCallsHistoryMethodResponse extends MethodResult {

	private String lastCaller;
	private Long callsCount;
	private String callsCountTxt;
	
	public String getLastCaller() {
		return lastCaller;
	}
	
	public void setLastCaller(String lastCaller) {
		this.lastCaller = lastCaller;
	}
	
	public Long getCallsCount() {
		return callsCount;
	}
	
	public void setCallsCount(Long callsCount) {
		this.callsCount = callsCount;
		setCallsCountTxt(String.valueOf(callsCount));
	}

	public String getCallsCountTxt() {
		return callsCountTxt;
	}

	public void setCallsCountTxt(String callsCountTxt) {
		this.callsCountTxt = callsCountTxt;
	}
}
