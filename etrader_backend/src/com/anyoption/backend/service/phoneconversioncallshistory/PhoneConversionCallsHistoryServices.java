package com.anyoption.backend.service.phoneconversioncallshistory;

import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.PhoneConversionCallsHistoryManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

public class PhoneConversionCallsHistoryServices {
	private static final Logger logger = Logger.getLogger(PhoneConversionCallsHistoryServices.class);
	private static final String SCREEN_NAME_PERMISSIONS_PHONE_CONVERSION_CALLS_HISTORY = "phoneConversionCallsHistory";

	@BackendPermission(id = "phoneConversionCallsHistory_view")
	public static ScreenSettingsResponse getPhoneConversionCallsHistoryScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getPhoneConversionCallsHistoryScreenSettings: " + request);
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_PHONE_CONVERSION_CALLS_HISTORY, response, httpRequest);
		
		return response;
	}

	public static PhoneConversionCallsHistoryMethodResponse getCallsHistory(PhoneConversionCallsHistoryMethodRequest request) {
		PhoneConversionCallsHistoryMethodResponse response = new PhoneConversionCallsHistoryMethodResponse();

		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}
		HashMap<String, Object> callsHistory = new HashMap<String, Object>();
		try {
			callsHistory = PhoneConversionCallsHistoryManager.getCallsHistory(request);
		} catch (SQLException e) {
			logger.error("Unable to get Phone Conversion Calls History Manager", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if (callsHistory == null){
			return response;
		}		
		
		response.setLastCaller((String) callsHistory.get("lastCaller"));
		response.setCallsCount((Long) callsHistory.get("callsCount"));
		
		return response;
	}
}
