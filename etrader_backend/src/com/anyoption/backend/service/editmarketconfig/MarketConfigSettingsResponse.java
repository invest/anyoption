package com.anyoption.backend.service.editmarketconfig;

import java.util.List;
import java.util.Map;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;
import com.anyoption.common.beans.MarketFormula;

/**
 * @author kiril.mutafchiev
 */
public class MarketConfigSettingsResponse extends CopyMarketResponse {

	private Map<Integer, List<MarketFormula>> formulas;

	public Map<Integer, List<MarketFormula>> getFormulas() {
		return formulas;
	}

	public void setFormulas(Map<Integer, List<MarketFormula>> formulas) {
		this.formulas = formulas;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "MarketConfigSettingsResponse: " + ls
				+ super.toString()
				+ "formulas: " + formulas + ls;
	}
}
