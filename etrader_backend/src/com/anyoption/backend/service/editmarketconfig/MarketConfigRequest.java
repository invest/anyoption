package com.anyoption.backend.service.editmarketconfig;

import com.anyoption.common.beans.MarketConfig;

public class MarketConfigRequest {
	
	MarketConfig marketConfig;
	long marketId;

	public MarketConfig getMarketConfig() {
		return marketConfig;
	}

	public void setMarketConfig(MarketConfig marketConfig) {
		this.marketConfig = marketConfig;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "MarketConfigRequest: " + ls
				+ super.toString() + ls
				+ "marketConfig: " + marketConfig + ls
				+ "marketId: " + marketId + ls;
	}
}
