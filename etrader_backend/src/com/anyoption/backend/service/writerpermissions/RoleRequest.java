/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import com.anyoption.common.service.requests.UserMethodRequest;

public class RoleRequest extends UserMethodRequest {
	
	private Long roleId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "RoleRequest: "
            + super.toString()
            + "roleId: " + roleId + ls;
    }	
}
