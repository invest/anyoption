/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import java.util.HashMap;

public class ScreenSettingsResponse extends PermissionsGetResponse {
	
	private HashMap<String, HashMap<Long, String>> screenFilters;

	public HashMap<String, HashMap<Long, String>> getScreenFilters() {
		return screenFilters;
	}

	public void setScreenFilters(HashMap<String, HashMap<Long, String>> screenFilters) {
		this.screenFilters = screenFilters;
	}
	
}
