/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import com.anyoption.common.service.results.PermissionsGetResponseBase;

import il.co.etrader.bl_vos.WriterBase;

public class PermissionsGetResponse extends PermissionsGetResponseBase {
	
	private WriterBase writer;
	public WriterBase getWriter() {
		return writer;
	}

	public void setWriter(WriterBase writer) {
		this.writer = writer;
	}
	
}
