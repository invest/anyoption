package com.anyoption.backend.service.writerpermissions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_vos.ExistWriterRole;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WriterBase;
import il.co.etrader.util.ConstantsBase;

/**
 * @author Jamal
 */
public class WriterPermissionsServices {	         
	private static final Logger log = Logger.getLogger(WriterPermissionsServices.class);
	
	private static final String FILTER_FOR_ROLE = "roles";
	private static final String SCREEN_NAME_PERMISSIONS = "permissions";
	public static final int ERROR_CODE_ROLE_NAME_EXIST = 6000;
	
	public static PermissionsGetResponse getSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getSetings:" + request);
		PermissionsGetResponse response = new PermissionsGetResponse();
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		Writer writer = writerWrapper.getWriter();
		log.debug("Get setings for Writer:" + writer.getId());
		ArrayList<String> permissionsList = null;
		try {
			permissionsList = WriterPermisionsManager.getWriterPermissions(writer.getId());
		} catch (SQLException e) {
			log.error("Unable to getSetings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		WriterBase writerBase = new WriterBase(writer.getId(), writer.getUserName(), writer.getDeptId(), 
					writer.getRoleId(), writer.getDepartmentName(), writer.getUtcOffset(), writer.isMultitude());
		
		response.setPermissionsList(permissionsList);
		response.setWriter(writerBase);
		return response;
	}
	
	@BackendPermission(id = "permissions_view")
	public static ScreenSettingsResponse getWriterPermisionsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getWriterPermisionsScreenSetings:" + request);
		
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> screenFilters = new HashMap<String, HashMap<Long, String>>();
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS, response, httpRequest);
		try {
			screenFilters.put(FILTER_FOR_ROLE, WriterPermisionsManager.getRoles());
		} catch (SQLException e) {
			log.error("Unable to getWriterPermisionsScreenSetings", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setScreenFilters(screenFilters);
		return response;
	}
	
	@BackendPermission(id = "permissions_view")
	public static DepartmentWriterGetResponse getDepartmentWriterByRole(RoleRequest request) {
		log.debug("getDepartmentWriterByRole:" + request);
		DepartmentWriterGetResponse response = new DepartmentWriterGetResponse();
			try {
				ArrayList<WriterBase> writerList =  new ArrayList<>();
				writerList = WriterPermisionsManager.getDepartmentWriters(request.getRoleId());
				response.setWriters(writerList);
		} catch (SQLException e) {
			log.error("Unable to getDepartmentWriterByRole", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "permissions_view")
	public static PermissionsGetResponse getPermissionsByRole(RoleRequest request) {
		log.debug("getPermissionsByRole:" + request);
		PermissionsGetResponse response = new PermissionsGetResponse();
		ArrayList<String> permissionsList = null;
		try {
			permissionsList = WriterPermisionsManager.getRolePermissions(request.getRoleId());
		} catch (SQLException e) {
			log.error("Unable to getPermissionsByRole", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setPermissionsList(permissionsList);
		return response;
	}
	
	@BackendPermission(id = "permissions_editRole")
	public static RolePemissionsResponse editRolePermissions(RolePermissionsRequest request) {
		log.debug("editRolePermissions:" + request);
		RolePemissionsResponse response = new RolePemissionsResponse();
		ArrayList<ExistWriterRole> existWriterRoleList = null;
		try {
			existWriterRoleList = WriterPermisionsManager.editRolePermissions(request.getRoleId(), request.getRoleName(), 
					request.getPermissionList(), request.getWriterList(), request.getUnsetWriterList());
		} catch (SQLException e) {
			if(e.getErrorCode() == 20000){
				log.error("Exist role Name:" + request.getRoleName() + " unable Change");
				response.setErrorCode(ERROR_CODE_ROLE_NAME_EXIST);
			} else {
				log.error("Unable to editRolePermissions", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		}
		response.setExistWriterRoleList(existWriterRoleList);
		return response;
	}
	
	@BackendPermission(id = "permissions_addRole")
	public static RolePemissionsResponse addRolePermissions(RolePermissionsRequest request) {
		log.debug("addRolePermissions:" + request);
		RolePemissionsResponse response = new RolePemissionsResponse();
		ArrayList<ExistWriterRole> existWriterRoleList = null;
		try {
			if(CommonUtil.isParameterEmptyOrNull(request.getRoleName())){
				log.error(" RoleName is NULL, Unable to addRolePermissions");
				response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
				return response;
			}
			existWriterRoleList = WriterPermisionsManager.addRolePermissions(request.getRoleName(), request.getPermissionList(), request.getWriterList());
		} catch (SQLException e) {
			if(e.getErrorCode() == 20000){
				log.error("Exist role Name:" + request.getRoleName() + " unable Insert");
				response.setErrorCode(ERROR_CODE_ROLE_NAME_EXIST);
			} else {
				log.error("Unable to addRolePermissions", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		}
		response.setExistWriterRoleList(existWriterRoleList);
		return response;
	}	
}