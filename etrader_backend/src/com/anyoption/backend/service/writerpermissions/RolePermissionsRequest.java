/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

public class RolePermissionsRequest extends UserMethodRequest {
	
	private Long roleId;
	private String roleName;
	private ArrayList<String> permissionList;
	private ArrayList<Long> writerList;
	private ArrayList<Long> unsetWriterList;


	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}	

	public ArrayList<String> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(ArrayList<String> permissionList) {
		this.permissionList = permissionList;
	}		

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public ArrayList<Long> getWriterList() {
		return writerList;
	}

	public void setWriterList(ArrayList<Long> writerList) {
		this.writerList = writerList;
	}

	public ArrayList<Long> getUnsetWriterList() {
		return unsetWriterList;
	}

	public void setUnsetWriterList(ArrayList<Long> unsetWriterList) {
		this.unsetWriterList = unsetWriterList;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "RolePermissionsRequest: "
            + super.toString()
            + "roleId: " + roleId + ls
            + "roleName: " + roleName + ls
            + "writerList: " + writerList + ls
            + "unsetWriterList: " + unsetWriterList + ls
        	+ "permissionList: " + permissionList + ls;
    }
}
