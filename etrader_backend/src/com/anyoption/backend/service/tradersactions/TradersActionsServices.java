package com.anyoption.backend.service.tradersactions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TradersActionsManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_vos.TradersActions;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;


public class TradersActionsServices {

	private static final Logger logger = Logger.getLogger(TradersActionsServices.class);
	private static final String SCREEN_NAME_PERMISSIONS_TRADERS_ACTIONS = "tradersActions";
	
	
	@BackendPermission(id = "tradersActions_view")
	public static ScreenSettingsResponse getTradersActionsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getTradersActionsScreenSettings: " + request);
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<String, HashMap<Long, String>>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_TRADERS_ACTIONS, response, httpRequest);
		
		//FILTERS
		filters = TradersActionsManager.getFilters();

		response.setScreenFilters(filters);
		
		return response;
	}

	public static TradersActionsMethodResponse getTradersActions(TradersActionsMethodRequest request) {
		TradersActionsMethodResponse response = new TradersActionsMethodResponse();

		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}
		if (request.getToDate() == 0 || request.getFromDate() == 0) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		Calendar cFrom = Calendar.getInstance();
		cFrom.setTimeInMillis(request.getFromDate());
		
		Calendar cTo = Calendar.getInstance();
		cTo.setTimeInMillis(request.getToDate());
				
		if (cFrom.after(cTo)) { //to date must be after from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		cTo.add(Calendar.MONTH, -1);
		if (cTo.after(cFrom)) { //to date can't be more then 31 days from from date
			response.setErrorCode(CommonJSONService.ERROR_CODE_VALIDATE_DATE_RANGE);
			return response;
		}
		
		ArrayList<TradersActions> tradersActions = null;
		ArrayList<Integer> commands = null;
		try {
			if (null == commands) {
				commands = new ArrayList<Integer>();
			}
			commands.add(ConstantsBase.LOG_COMMANDS_ENABLE_OPP);
			commands.add(ConstantsBase.LOG_COMMANDS_DISABLE_OPP);
			commands.add(ConstantsBase.LOG_COMMANDS_SUSPEND_MARKET);
			commands.add(ConstantsBase.LOG_COMMANDS_UNSUSPEND_MARKET);
			commands.add(ConstantsBase.LOG_COMMANDS_CHANGE_MAX_MIN_INV);
			commands.add(ConstantsBase.LOG_COMMANDS_SUSPEND_OPP);
			commands.add(ConstantsBase.LOG_COMMANDS_CHANGE_EXPOSURE);
			
			tradersActions = TradersActionsManager.getTradersActions(request, commands);
		} catch (SQLException e) {
			logger.error("Unable to get traders actions", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		if (tradersActions == null) {
			return response; 
		} 
		
		response.setTradersActions(tradersActions);
		
		return response;
	}
}
