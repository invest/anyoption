package com.anyoption.backend.service.apmwithdrawals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.CommonServiceServlet;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayRequest;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayResult;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalServices;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.util.Utils;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class APMWithdrawalsServices {

	private static final String SCREEN_NAME = "directBanking";
	private static final String FEE_EXMPT_PERMISION = "directBanking_feeExempt";
	private static final String FILTER_TRANSACTION_TYPES_WITHDRAW = "transactionTypesWithdraw";
	
	private static final Logger logger = Logger.getLogger(APMWithdrawalsServices.class);
	
	@BackendPermission(id = "directBanking_view")
	public static APMWithdrawalsScreenSettings getAPMWithdrawalsScreenSettings(APMWithdrawalsMethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getAPMWithdrawalsScreenSettings " + request);
		APMWithdrawalsScreenSettings response = new APMWithdrawalsScreenSettings();
		HashMap<String, HashMap<String, Boolean>> filters = new HashMap<>();
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME, response, httpRequest);
		
		HashMap<String, Boolean> transactionTypes = new HashMap<>(); 
		
		GmWithdrawalPermissionToDisplayRequest gmRequest = new GmWithdrawalPermissionToDisplayRequest();
		gmRequest.setUserId(request.getUserId());
		GmWithdrawalPermissionToDisplayResult result = GmWithdrawalServices.getPermissionsToDisplay(gmRequest, httpRequest);
		
		transactionTypes.put(CommonUtil.getMessage("transactions.direct24.withdraw.clean", null), result.isDisplayDirect24());
		transactionTypes.put(CommonUtil.getMessage("transactions.eps.withdraw.clean", null), result.isDisplayEPS());
		transactionTypes.put(CommonUtil.getMessage("transactions.giropay.withdraw.clean", null), result.isDisplayGiropay());
		transactionTypes.put(CommonUtil.getMessage("transaction.ideal.withdraw.clean", null), result.isDisplayIdeal());
		try {
			transactionTypes.put(CommonUtil.getMessage("transactions.moneybookers.withdraw.clean", null),
					TransactionsManager.isDisplaySkrill(request.getUserId()));
		} catch (SQLException e) {
			logger.error("getAPMWithdrawalsScreenSettings: ", e);
		}

		filters.put(FILTER_TRANSACTION_TYPES_WITHDRAW, transactionTypes);
		response.setScreenFilter(filters);
		return response;
	}
	
	@BackendPermission(id = "directBanking_save")
	public static APMSkrillMethodResponse insertMoneybookersWithdraw(APMSkrillMethodRequest request, HttpServletRequest httpRequest) {
		APMSkrillMethodResponse response = new APMSkrillMethodResponse();
		User user = null;
		ArrayList<ErrorMessage> errorList = new ArrayList<>();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession()
					.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			user = UsersManager.getUserDetails(request.getUserId(), writerWrapper.getSkinsToString());

			if (Utils.isParameterEmptyOrNull(request.getAmount()) || 
				Utils.isParameterEmptyOrNull(request.getMoneybookersEmail()) ) {
				
				ErrorMessage errMsg = null;
				
				if (Utils.isParameterEmptyOrNull(request.getMoneybookersEmail())) {
					errMsg = new ErrorMessage("moneybookersEmail", CommonUtil.getMessage("error.mandatory", null));
					errorList.add(errMsg);
				}

				if (Utils.isParameterEmptyOrNull(request.getAmount())) {
					errMsg = new ErrorMessage("amount", CommonUtil.getMessage("error.mandatory", null));
					errorList.add(errMsg);
				}
				response.setUserMessages(errorList);
				
				response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
				return response;
			}
			
			String error = TransactionsManager.validateWithdrawForm(request.getAmount(), user, !request.isExemptFee(), false,
					checkLimits(user, request.getWriterId()), null, null, request.getMoneybookersEmail(), null, request.getWriterId());
	
			if (error == null) {
				if (!user.isHasAdminRole()) {
					WithdrawEntryResult preWithdrawEntryResult = WithdrawUtil
							.preWithdrawEntry(new WithdrawEntryInfo(user));
					if (!preWithdrawEntryResult.getWithdrawResult().isEmpty()) {
						error = preWithdrawEntryResult.getWithdrawResult().iterator().next().getDescription();
						response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
						logger.debug("Error in insertMoneybookersWithdraw " + error);
					}
				}
			}
			
			if (error != null) {
				if (request.isExemptFee()) {			
					if(WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()).contains(FEE_EXMPT_PERMISION)){
						logger.error("Writer:" + request.getWriterId() + " exempt the fee SkrillWithd for userId " + request.getUserId());
					} else {
						logger.error("Writer:" + request.getWriterId() + " don't have permission: " + FEE_EXMPT_PERMISION);
						response.setErrorCode(CommonServiceServlet.ERROR_CODE_USER_NOT_HAVE_PERMISSION);
						return response;
					}
				}
				TransactionsManager.insertMoneybookersWithdraw(request.getAmount(), request.getMoneybookersEmail(),
						error, user, request.getWriterId(), request.isExemptFee());
				return response;
			}
			long feeVal = 0;
			if (!request.isExemptFee()) {
				feeVal = TransactionsManagerBase.getTransactionHomoFee(CommonUtil.calcAmount(request.getAmount()),
						user.getCurrencyId(), TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
			}
			response.setFee(feeVal);
		} catch (SQLException e) {
			logger.error("Can't insert skrill withdraw in insertMoneybookersWithdraw", e);
		}

		return response;
	}
	
	@BackendPermission(id = "directBanking_save")
	public static MethodResult moneybookersApprove(APMSkrillMethodRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		User user;
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession()
					.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			user = UsersManager.getUserDetails(request.getUserId(), writerWrapper.getSkinsToString());
			long loginId = 0L;
			try {
				HttpSession session = httpRequest.getSession();
				if (session.getAttribute(Constants.LOGIN_ID) != null) {
					loginId = (long) session.getAttribute(Constants.LOGIN_ID);
				}
			} catch (Exception e) {
				logger.error("Cannot get login ID from session!", e);
				loginId = 0L;
			}			
			String error = null;
			if(hasBonuses(user)){// cancel all bonuses that can be canceled or return error if something goes wrong
				List<BonusUsers> bonusUsers = BonusManagerBase.getAllUserBonus(user.getId(), null, null, 0, CommonUtil.getUtcOffset(), new ArrayList<>(Arrays.asList(CurrenciesManagerBase.getCurrency(user.getCurrencyId()))));
				for (BonusUsers bu : bonusUsers) {
					if (bu.isCanCanceled()) {
						error = BonusManagerBase.cancelBonus(bu, CommonUtil.getUtcOffset(), Writer.WRITER_ID_WEB, user.getSkinId(), user, loginId, CommonUtil.getIPAddress());
					}
					if (error != null && error.equals(BonusManagerBase.ERROR_DUMMY)) {
						logger.error("Cannot cancel bonus with id: " + bu.getId());
						response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
						return response;
					}
				}
			}
			
			TransactionsManager.insertMoneybookersWithdraw(request.getAmount(), request.getMoneybookersEmail(), null, user, request.getWriterId(), request.isExemptFee());

		} catch (SQLException e) {
			logger.error("Can't approve skrill withdraw in moneybookersApprove", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}		
		return response;
	}

	private static boolean checkLimits(User user, long writerId) throws SQLException{
		String writer = WritersManager.getWriter(writerId).getUserName();
		user.setRoles(LoginManager.getWriterRoles(writer));
		return !(user.isHasAccountingRole() || user.isHasAdminRole() || user.isHasSAdminRole());
	}
		
	private static boolean hasBonuses(User user) {
		boolean hasBonuses = false;
		WithdrawBonusRegulationStateMethodResult wbrsmr = BonusManagerBase.getWithdrawBonusRegulationState(user.getId(), user.getSkinId());
		if (wbrsmr.getState().toString().equals("REGULATED_OPEN_BONUSES") || wbrsmr.getState().toString().equals("NOT_REGULATED_OPEN_BONUSES")) {
			hasBonuses = true;
		}
		return hasBonuses;
	}
}
