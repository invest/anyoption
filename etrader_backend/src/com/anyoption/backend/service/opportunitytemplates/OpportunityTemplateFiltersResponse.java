package com.anyoption.backend.service.opportunitytemplates;

import java.util.Map;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;

public class OpportunityTemplateFiltersResponse extends CopyMarketResponse {
	
	private Map<Long, String> active;
	private Map<Long, String> oddsType;
	private Map<Long, String> scheduled;
	private Map<Long, String> timezone;
	private Map<Long, String> fullDay;
	private Map<String, String> selectorsGroup;

	public Map<Long, String> getOddsType() {
		return oddsType;
	}

	public void setOddsType(Map<Long, String> oddsType) {
		this.oddsType = oddsType;
	}

	public Map<Long, String> getScheduled() {
		return scheduled;
	}

	public void setScheduled(Map<Long, String> scheduled) {
		this.scheduled = scheduled;
	}

	public Map<Long, String> getTimezone() {
		return timezone;
	}

	public void setTimezone(Map<Long, String> timezone) {
		this.timezone = timezone;
	}

	public Map<Long, String> getFullDay() {
		return fullDay;
	}

	public void setFullDay(Map<Long, String> fullDay) {
		this.fullDay = fullDay;
	}

	public Map<String, String> getSelectorsGroup() {
		return selectorsGroup;
	}

	public void setSelectorsGroup(Map<String, String> selectorsGroup) {
		this.selectorsGroup = selectorsGroup;
	}

	public Map<Long, String> getActive() {
		return active;
	}

	public void setActive(Map<Long, String> active) {
		this.active = active;
	}
}
