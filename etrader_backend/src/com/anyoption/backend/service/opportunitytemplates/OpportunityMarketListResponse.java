package com.anyoption.backend.service.opportunitytemplates;

import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;

import il.co.etrader.bl_vos.OpportunityTemplate;

public class OpportunityMarketListResponse extends CopyMarketResponse  {
	
	private ArrayList<OpportunityTemplate> opportunityTemplates;
	private HashMap<Long, String> writerNames;

	public ArrayList<OpportunityTemplate> getOpportunityTemplates() {
		return opportunityTemplates;
	}

	public void setOpportunityTemplates(ArrayList<OpportunityTemplate> opportunityTemplates) {
		this.opportunityTemplates = opportunityTemplates;
	}

	public HashMap<Long, String> getWriterNames() {
		return writerNames;
	}

	public void setWriterNames(HashMap<Long, String> writerNames) {
		this.writerNames = writerNames;
	}
}