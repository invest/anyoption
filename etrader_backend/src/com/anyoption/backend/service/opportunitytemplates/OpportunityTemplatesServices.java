package com.anyoption.backend.service.opportunitytemplates;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Enumerator;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.OpportunitiesManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.OpportunityTemplate;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class OpportunityTemplatesServices {

	private static final String SCREEN_NAME_PERMISSIONS_OPPORTUNITY_TEMPLATES = "marketSchedule";
	private static final int APPLY_TO_ONE_DAY_AHEAD = 1;
	private static final Logger log = Logger.getLogger(OpportunityTemplatesServices.class);

	@BackendPermission(id = "marketSchedule_view")
	public static OpportunityTemplateFiltersResponse getOpportunityTemplatesScreenSettings(UserMethodRequest request,
			HttpServletRequest httpRequest) {
		log.debug("getOpportunityTemplatesScreenSettings:" + request);
		OpportunityTemplateFiltersResponse response = new OpportunityTemplateFiltersResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_OPPORTUNITY_TEMPLATES,
				response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			Map<Long, String> active = new LinkedHashMap<Long, String>();
			active.put(0L, "All");
			active.put(1L, "Active");
			active.put(2L, "Inactive");
			response.setActive(active);
			response.setFilters(mapFilters);
			Map<Long, String> oddsTypes = new HashMap<Long, String>();
			Iterator it = OpportunitiesManagerBase.getOpportunityOddsTypes().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Entry) it.next();
				oddsTypes.put((Long) entry.getKey(), (String) ((OpportunityOddsType) entry.getValue()).getName());
			}
			LinkedHashMap sortedOddsTypesDropdown = oddsTypes.entrySet().stream().sorted(Map.Entry.comparingByValue())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
							LinkedHashMap::new));
			response.setOddsType(sortedOddsTypesDropdown);
			Map<Long, String> scheduledMap = new LinkedHashMap<Long, String>();
			// TODO switch constants to Opportunity ones
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_HOURLY, CommonUtil.getMessage("opportunities.templates.hourly", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_DAILY, CommonUtil.getMessage("opportunities.templates.daily", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_WEEKLY, CommonUtil.getMessage("opportunities.templates.weekly", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_MONTHLY, CommonUtil.getMessage("opportunities.templates.monthly", null));
			scheduledMap.put((long) Opportunity.SCHEDULED_QUARTERLY, CommonUtil.getMessage("opportunities.templates.quarterly", null));
			response.setScheduled(scheduledMap);
			ArrayList<Enumerator> list = CommonUtil.getEnum(Constants.ENUM_TIMEZONE);
			list.sort((p1, p2) -> p1.getDescription().compareToIgnoreCase(p2.getDescription()));
			Map<Long, String> timezoneMap = new LinkedHashMap<Long, String>();
			list.forEach(item -> timezoneMap.put((Long) item.getId(), (String) item.getDescription()));
			response.setTimezone(timezoneMap);
			Map<Long, String> fullDayMap = new LinkedHashMap<Long, String>();
			fullDayMap.put(0L, CommonUtil.getMessage("no", null));
			fullDayMap.put(1L, CommonUtil.getMessage("yes", null));
			fullDayMap.put(2L, CommonUtil.getMessage("opportunity.templates.full.day.sunday", null));
			response.setFullDay(fullDayMap);
			
			ArrayList<OpportunityOddsGroup> oddsGroups = InvestmentsManager.getOpportunityOddsGroup();
			Map<String, String> oddsGroupMap = new LinkedHashMap<String, String>();
			for (OpportunityOddsGroup oog : oddsGroups) {
				oddsGroupMap.put(oog.getOddsGroup(), oog.getOddsGroupValue() + "  " + oog.getDefaultPair());
			}
			response.setSelectorsGroup(oddsGroupMap);
		} catch (SQLException e) {
			log.error("Unable to select markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	@BackendPermission(id = "marketSchedule_view")
	public static OpportunityMarketListResponse getOpportunityTemplatesByMarket(
			OpportunityTemplateFiltersRequest request, HttpServletRequest httpRequest) {
		log.debug("getOpportunityTemplatesByMarket:" + request);
		OpportunityMarketListResponse response = new OpportunityMarketListResponse();
		ArrayList<OpportunityTemplate> opportunityTemplatesList = new ArrayList<OpportunityTemplate>();
		try {
			opportunityTemplatesList = InvestmentsManager.searchOppTemplates(0L, request.getMarketId(), true, 
																		request.getActive(), Skins.SKIN_REG_EN);
			Set<Long> writerIDs = new HashSet<Long>();
			for (OpportunityTemplate ot : opportunityTemplatesList) {
				writerIDs.add(ot.getWriterId());
			}
			ArrayList<Long> writers = new ArrayList<Long>();
			writers.addAll(writerIDs);
			HashMap<Long, String> writerNames = new HashMap<Long, String>();
			HashMap<Long, Writer> writerMap = ApplicationData.getWritersHM();
			for (Long writer : writers) {
				writerNames.put(writer, writerMap.get(writer).getUserName());
			}
			response.setWriterNames(writerNames);
			response.setOpportunityTemplates(opportunityTemplatesList);
		} catch (SQLException e) {
			log.error("Unable to select opportunity templates list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	@BackendPermission(id = "marketSchedule_view")
	public static MarketOpportunityResponse getOpportunityTemplateByMarket(OpportunityTemplateRequest request,
			HttpServletRequest httpRequest) {
		log.debug("getOpportunityTemplatesByMarket:" + request);
		MarketOpportunityResponse response = new MarketOpportunityResponse();
		ArrayList<OpportunityTemplate> opportunityTemplatesList = new ArrayList<OpportunityTemplate>();
		try {
			opportunityTemplatesList = InvestmentsManager.searchOppTemplates(0L, request.getMarketId(), true,
					request.getActive(), Skins.SKIN_REG_EN);
			for (OpportunityTemplate opportunity : opportunityTemplatesList) {
				if (opportunity.getId() == request.getOpportunityTemplateId()) {
					response.setOpportunityTemplate(opportunity);
					response.setWriterName(ApplicationData.getWritersHM().get(opportunity.getWriterId()).getUserName());
				}
			}
		} catch (SQLException e) {
			log.error("Unable to select opportunity templates list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

	@BackendPermission(id = "marketSchedule_view")
	public static MethodResult updateInsertOpportunityTemplate(OpportunityTemplateRequest request,
			HttpServletRequest httpRequest) {
		log.debug("updateInsertOpportunityTemplate:" + request);
		MethodResult response = new MethodResult();
		OpportunityTemplate opportunityTemplate = request.getOpportunityTemplate();
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		Writer writer = writerWrapper.getWriter();
		long writerId = writer.getId();
		opportunityTemplate.setWriterId(writerId);
		
		try {
			if (opportunityTemplate.getOpportunityTypeId() == Opportunity.TYPE_REGULAR
					&& opportunityTemplate.getOddsGroup() == null) {
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				return response;
			} else if (opportunityTemplate.getOpportunityTypeId() == Opportunity.TYPE_REGULAR
					&& !isValidOddsGoupsOddsType(opportunityTemplate.getOddsGroup(),
							opportunityTemplate.getOddsTypeId())) {
				response.setErrorCode(CommonJSONService.ERROR_CODE_ODDS_GROUP_ODDS_TYPES);
				return response;
			}
			opportunityTemplate.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
			boolean res = InvestmentsManager.updateInsertOppTemplate(opportunityTemplate, true);
			String failedToNotify = "";

			// update all opportunities of that opportunity template
			ArrayList<Opportunity> list = null;

			if (opportunityTemplate.isChangeAllScheduled()) {
				list = InvestmentsManager.getOpportunitiesByTemplate(opportunityTemplate,
						APPLY_TO_ONE_DAY_AHEAD, false, true);
			} else {
				if (opportunityTemplate.isChangeAll()) {
					list = InvestmentsManager.getOpportunitiesByTemplate(opportunityTemplate,
							APPLY_TO_ONE_DAY_AHEAD, opportunityTemplate.isChangeAll(), true);
				}
			}
			if (list != null && (opportunityTemplate.isChangeAll()
					|| opportunityTemplate.isChangeAllScheduled())) {
				BackendLevelsCache levelsCache = (BackendLevelsCache) httpRequest.getServletContext()
						.getAttribute("levelsCache");

				// update opportunity and notify service
				boolean resp = false;
				for (int i = 0; i < list.size(); i++) {
					long oddsTypeId = opportunityTemplate.getOddsTypeId();
					resp = InvestmentsManager.updateOpportunityOdds(list.get(i), oddsTypeId,
							opportunityTemplate.getOddsGroup());
					if (!resp) {
						if (!levelsCache.notifyForOpportunityOddsChange(list.get(i).getId())) {
							failedToNotify += String.valueOf(list.get(i).getId()) + " ";
						}
					}
				}
			}
		} catch (SQLException e) {
			log.error("Unable to insert/update opportunity template! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketSchedule_view")
	public static MethodResult createOpportunitiesForMarket(MarketRequest request) {
		log.debug("createOpportunitiesForMarket: " + request);
		MethodResult result = new MethodResult();
		try {
			OpportunitiesManagerBase.createOpportunitiesForMarket(request.getMarketId());
		} catch (SQLException e) {
			log.error("Unable to create opportunities for market! ", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}

	private static Boolean isValidOddsGoupsOddsType(String oddsGroup, long oddsTypeId) throws SQLException {
		return InvestmentsManager.isExistOddsGoupsInOddsType(oddsGroup, oddsTypeId);
	}
}