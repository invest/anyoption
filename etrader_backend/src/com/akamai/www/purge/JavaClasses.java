/**
 * JavaClasses.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.akamai.www.purge;

public interface JavaClasses extends javax.xml.rpc.Service {

/**
 * Provides programmatic purge access
 */
    public java.lang.String getPurgeApiAddress();

    public com.akamai.www.purge.PurgeApi getPurgeApi() throws javax.xml.rpc.ServiceException;

    public com.akamai.www.purge.PurgeApi getPurgeApi(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
