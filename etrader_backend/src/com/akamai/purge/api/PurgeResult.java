// Copyright (c) 2005-2009 Akamai Technologies, Inc.
// The information herein is proprietary and confidential to Akamai, and
// it may only be used under appropriate agreements with the Company.
// Access to this information does not imply or grant you any right to
// use the information, all such rights being expressly reserved.

package com.akamai.purge.api;

/**
 * This class provides a nicer interface to the results of the CCU
 * service call than the WSDL-generated
 * com.akamai.www.purge.PurgeResult class.  
 *
 * Note that you can still get the raw data coming back from the
 * CCUAPI service through this API, but the ResultType enum and the
 * getResult() method are probably more useful for real error
 * handling.
 */
public class PurgeResult {
    private com.akamai.www.purge.PurgeResult rawResult;
        
    public PurgeResult(com.akamai.www.purge.PurgeResult result)
    {
        rawResult = result;
    }
        
    /**
     * The set of possible results of submitting a purge.  To get more
     * details, see the Akamai CCUAPI docuemtation.
     */
    public enum ResultType {
        SUCCESS,
        WARNING,
        INVALID_USER_CREDENTIAL,
        INVALID_OPTION,
        BAD_OPTION_SYNTAX,
        BAD_OPTION_VALUE,
        DUPLICATE_OPTION,
        UNAUTHORIZED_USE_OF_OPTION,
        NO_URIS_SPECIFIED,
        INVALID_URI_FORMAT,
        NOT_AUTHORIZED_FOR_THIS_URI,
        ILLEGAL_URI,
        QUEUE_LIMIT_REACHED,
        NOT_AUTHORIZED_FOR_CPCODE_PURGE,
        REQUEST_FILE_TOO_LARGE,
        INTERNAL_ERROR;
    }

    /**
     * Get the result of the purge submission as an object which can
     * be used to drive further behavior in the system.
     */
    public ResultType getResult() throws PurgeException
    {
        ResultType toReturn = ResultType.SUCCESS;
        int resultCode = getResultCode();
        if (100 <= resultCode && resultCode < 200) {
            toReturn = ResultType.SUCCESS;
        }
        else if (200 <= resultCode && resultCode < 300) {
            toReturn = ResultType.WARNING;
        }
        else if (resultCode == 301) {
            toReturn = ResultType.INVALID_USER_CREDENTIAL;
        }
        else if (resultCode == 302) {
            toReturn = ResultType.INVALID_OPTION;
        }
        else if (resultCode == 303) {
            toReturn = ResultType.BAD_OPTION_VALUE;
        }
        else if (resultCode == 304) {
            toReturn = ResultType.DUPLICATE_OPTION;
        }
        else if (resultCode == 305) {
            toReturn = ResultType.BAD_OPTION_SYNTAX;
        }
        else if (resultCode == 306) {
            toReturn = ResultType.UNAUTHORIZED_USE_OF_OPTION;
        }
        else if (resultCode == 320) {
            toReturn = ResultType.NO_URIS_SPECIFIED;
        }       
        else if (resultCode == 321) {
            toReturn = ResultType.INVALID_URI_FORMAT;
        }       
        else if (resultCode == 322) {
            toReturn = ResultType.NOT_AUTHORIZED_FOR_THIS_URI;
        }       
        else if (resultCode == 323) {
            toReturn = ResultType.ILLEGAL_URI;
        }       
        else if (resultCode == 332) {
            toReturn = ResultType.QUEUE_LIMIT_REACHED;
        }               
        else if (resultCode == 340) {
            toReturn = ResultType.NOT_AUTHORIZED_FOR_CPCODE_PURGE;
        }
        else if (resultCode == 401 || resultCode == 402) {
            toReturn = ResultType.INTERNAL_ERROR;
        }               
        else if (resultCode == 413) {
            toReturn = ResultType.REQUEST_FILE_TOO_LARGE;
        }               
        else {
            throw new PurgeException("Unknown result code:  " + resultCode + 
                                     "\nPlease contact Akamai with details");
        }
                
        return toReturn;
    }
        
    /**
     * Get the result code of the purge submission.  The result code
     * can be looked up in the Akamai CCUAPI documentation, or you can
     * just call getResultMessage() to get a text description of what
     * it means.
     */
    public int getResultCode() {
        return rawResult.getResultCode();
    }

    /**
     * Get an English message which describes the result of the
     * submitted request.
     */
    public String getResultMessage() {
        return rawResult.getResultMsg();
    }
        
    /**
     * If there was an error due to a problem with one of the URIs
     * being submitted for purging (e.g., that it was malformed), this
     * will give you the zero-based index of the problematic URI in
     * question in your submission,
     */
    public int getUriIndex() {
        return rawResult.getUriIndex();
    }

    /**
     * Get the "session ID" of your submission.  This is useful if you
     * need to talk to Akamai Customer Care about a problem with a
     * purge request.
     */
    public String getSessionID() {
        return rawResult.getSessionID();
    }
        
    /**
     * Get a rough estimate of how long it should take this request to
     * complete.
     */
    public int getEstimatedTimeToCompletionInSeconds() {
        return rawResult.getEstTime();
    }
}
