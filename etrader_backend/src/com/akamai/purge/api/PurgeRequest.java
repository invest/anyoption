// Copyright (c) 2005-2009 Akamai Technologies, Inc.
// The information herein is proprietary and confidential to Akamai, and
// it may only be used under appropriate agreements with the Company.
// Access to this information does not imply or grant you any right to
// use the information, all such rights being expressly reserved.

package com.akamai.purge.api;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.xml.rpc.ServiceException;

import com.akamai.www.purge.JavaClasses;
import com.akamai.www.purge.JavaClassesLocator;
import com.akamai.www.purge.PurgeApi;
//The "com.akamai.www.purge.*" classes being imported here are
//generated from the ccuapi-axis WSDL at:
//  http://ccuapi.akamai.com/ccuapi-axis.wsdl
//via WSDL2Java from Axis 1.4.  See the build.xml file for more
//details.

/**
 * This class provides a nicer interface to the CCU service than is
 * available through the WSDL-generated interface classes.
 */
public class PurgeRequest {
    private PurgeOptions options;
    private static final String USELESS_LEGACY_ARG = "";
    
    public PurgeRequest(PurgeOptions options) {
        this.options = options;
    }

    /**
     * This method does the basic work of the client -- invoke the web service 
     * and return the result.
     * 
     * @param user The EdgeControl user-name
     * @param password The EdgeControl password
     * @param urls The list of URLs (or ARLs or CPCodes) to be purged.
     * @return The result of the purge call.  Note that this is our 
     *         PurgeResult wrapper class, not the raw PurgeResult generated
     *         from the WSDL. 
     * @throws PurgeException 
     */
    public PurgeResult send(String user, String password, Collection<String> urls) 
        throws PurgeException {
        try {
            // Find service from WSDL2Java generated classes and invoke it.  
            // 
            // N.B. The "network" argument of the PurgeApi.purgeRequest is a
            //      vestige of an earlier time in the life of this service and
            //      serves no purpose whatsoever today.  Hence the (rather
            //      embarrassing) USELESS_LEGACY_ARG thing here...
            JavaClasses service = new JavaClassesLocator();
            PurgeApi p = service.getPurgeApi();             
            return new PurgeResult(p.purgeRequest(user, 
                                                  password,
                                                  USELESS_LEGACY_ARG,
                                                  options.getOptionsArray(),
                                                  urls.toArray(new String[0])));
        }
        catch (RemoteException e) {
            throw new PurgeException("Problem with RMI call deep in SOAP.", e);
        }
        catch (ServiceException e) {
            throw new PurgeException("Problem with RPC call in SOAP.", e);
        }
    }
}
