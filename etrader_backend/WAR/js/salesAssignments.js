function checkChars(obj) {
	var validchars = "0123456789";
	var sc = ".";
	var scused = false;
	var newval = "";
	var currval;
	var allowed = 0;

	currval = obj.value;

	for (var x = 0; x < currval.length; x++) {
        var currchar = currval.charAt(x);
        for (var y = 0; y < validchars.length; y++) {
            var c = validchars.charAt(y);
            if (currchar == c) {
                if (scused) {
                    allowed++;
                    if (allowed > 2) {
                        break;
                    } else {
                        newval += currchar;
                        break;
                    }
                } else {
                    newval += currchar;
                    break;
                }
            } else if (!scused) {
                if (currchar == sc) {
                    scused = true;
                    newval += currchar;
                }
            }
        }
    }
    obj.value = newval;
}

function updateAssignments(tasksElm, event) {
	if (event.keyCode == 13) {
		return false;
	}
	if ((event.keyCode != 9) && (event.keyCode != 16) &&
			(event.keyCode != 37) && (event.keyCode != 38) &&
			(event.keyCode != 39) && (event.keyCode != 40)) {
		checkChars(tasksElm);
	}
	updateAllAssignments();
}

function updateAllAssignments() {
	var assignedElm = document.getElementById("populationsEntriesForm:pressedRecords");
	var tb = document.getElementById("assignTable");
	if (null != tb) {
		var tbInputsArr = tb.getElementsByTagName("INPUT");
		var num = 0;
		for (var i = 0 ; i < tbInputsArr.length ; i++ ) {
			num += new Number(tbInputsArr[i].value);
		}
		assignedElm.innerHTML = num;
	}
}
