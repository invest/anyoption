function showAssignOption() {
	var assignOption = document.getElementById(formId + ":assignOption");
	if (null != assignOption) {
		assignOption.style.display = "inline";
	}
}

function confirmit(msgType) {
    if (msgType == '3') {  // assign submit
		// check assignRows
		var allEntriesNum = new Number(document.getElementById(formId + ":allEntries").innerHTML);
		var assignedNum = new Number(document.getElementById(formId + ":pressedRecords").innerHTML);
		if (assignedNum > allEntriesNum) {
			alert("Assign error: selected tasks > number of entries ");
			return false;
		}
    }
	if (!confirm(gettext(msgType))) {
		return false;
	}
	return true;
}
