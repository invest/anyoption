var UTC_HINTS_CFG = {
	'smart'      : true, // don't go off screen, don't overlap the object in the document
	'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
	'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
	'align'      : 'bctl', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
	'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
	'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
	'hide_delay' : 1, // a delay between closing event (mouseout for example) and hint disappearing
	'follow'     : false, // hint follows the mouse as it moves
	'z-index'    : 100, // a z-index for all hint layers
	'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
	'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
	'opacity'    : 90 // opacity of the hint in %%
};

var ROLLUP_HINT = [
			'<div id="insuranceBought"></div>'
		];
var rollUpHint = new THints(ROLLUP_HINT, UTC_HINTS_CFG);

function showRollupBoughtHint(invAmount, insuranceAmount) {
	document.getElementById('insuranceBought').innerHTML = rollUpBoughtMsg1 + " " + invAmount + "<br/>" + rollUpBoughtMsg2 + " " + insuranceAmount;
	rollUpHint.show('0');
}

function showGMBoughtHint(invAmount, insuranceAmount) {
	document.getElementById('insuranceBought').innerHTML = GMBoughtMsg1 + " " + invAmount + "<br/>" + GMBoughtMsg2 + " " + insuranceAmount;
	rollUpHint.show('0');
}

function showOptionPlusHint(price, timeSettled) {
	document.getElementById('insuranceBought').innerHTML = optionPlusTip + " " + price + " settled at " + timeSettled;
	rollUpHint.show('0');
}

var BONUS_HINT = [
			'<div id="bonus"></div>'
		];

var bonusHint = new THints(BONUS_HINT, UTC_HINTS_CFG);

function showNotSettledBonusHint(bonusName) {
	document.getElementById('bonus').innerHTML = oddsChangeNotSettledMsg1 + " " + bonusName + " " + oddsChangeNotSettledMsg2;
	bonusHint.show('0');
}

function showSettledBonusHint(bonusName, bonusAmonut) {
	document.getElementById('bonus').innerHTML = oddsChangeSettledMsg1 + " " + bonusName + " " + oddsChangeSettledMsg2 + " " + bonusAmonut + " " + oddsChangeSettledMsg3;
	bonusHint.show('0');
}

var REG_LEADS_HINTS_CFG = {
	'smart'      : true, // don't go off screen, don't overlap the object in the document
	'margin'     : 10, // minimum allowed distance between the hint and the window edge (negative values accepted)
	'gap'        : 5, // minimum allowed distance between the hint and the origin (negative values accepted)
	'align'      : 'bltr', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
	'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
	'show_delay' : 200, // a delay between initiating event (mouseover for example) and hint appearing
	'hide_delay' : 500, // a delay between closing event (mouseout for example) and hint disappearing
	'follow'     : false, // hint follows the mouse as it moves
	'z-index'    : 100, // a z-index for all hint layers
	'IEfix'      : true, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
	'IEtrans'    : ['blendTrans(DURATION=.3)'], // [show transition, hide transition] - transition effects, only work in IE5+
	'opacity'    : 90 // opacity of the hint in %%
};

var REG_LEADS_HINTS_ITEMS= [
	'<div id="regLeadHint"></div>'
];

var regLeadsHint = new THints (REG_LEADS_HINTS_ITEMS, REG_LEADS_HINTS_CFG);

function regLeadsHintDisplay(text){
	if(text != ''){
		document.getElementById('regLeadHint').innerHTML = text;
		regLeadsHint.show('0');
	}
}