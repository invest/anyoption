backendApp.controller('MarketingCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {

}]);

backendApp.controller('PromotionsManagementCtr', ['$rootScope', '$scope', '$http', '$upload', '$timeout', '$uibModal', 'Multiselect', 'SingleSelect', 'Pagination', function($rootScope, $scope, $http, $upload, $timeout, $uibModal, Multiselect, SingleSelect, Pagination) {
	$scope.MAX_SLIDES_COUNT = 4;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	
	$scope.filter.languages = {};
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.banners = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: ''}});
		$scope.filter.active = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.linkType = new SingleSelect.InstanceClass({});
	}
	
	$scope.changePage = function(page){
		$scope.getFiles(page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.results = [];
	$scope.selectedBanner = {};
	$scope.selectedSlide = {};
	
	$scope.imagesFromBank = {};
	
	$scope.treeOptions = {
		beforeDrop : function (e) {
			var slideIds = [];
			for(var i = 0; i < $scope.results.length; i++){
				var j = i;
				if(j == e.dest.index){
					j = e.source.index;
				}else if(j == e.source.index){
					j = e.dest.index;
				}
				slideIds.push($scope.results[j].id);
			}
			return $scope.reorderSlides(slideIds);
		}
	}

	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getPromotionManagementScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getPromotionManagementScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.slideId){
			$scope.action = 'edit';
			$scope.selectedSlide = {};
			$scope.selectedSlide.slideId = $rootScope.$stateParams.slideId;
			$scope.getSlide();
		}else if($rootScope.$state.includes("ln.marketing.promotions_management.add_slide") && $rootScope.$stateParams.bannerId){
			$scope.action = 'add';
			$scope.selectedSlide = {};
			$scope.selectedSlide.bannerId = $rootScope.$stateParams.bannerId;
			$scope.initSlide();
		}else{
			$scope.action = '';
		}
	}
	
	$scope.getPromotionManagementScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/getPromotionManagementScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					$scope.filter.banners.fillOptions(data.screenFilters.banners);
					$scope.filter.active.fillOptions(data.screenFilters.isActive);
					$scope.filter.linkType.fillOptions(data.screenFilters.bannerSliderTypes);
				}
				$scope.filter.languages = data.languages;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPromotionManagementScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getPromotionManagementScreenSettings');
			})
	}
	
	$scope.getPromotionBannerSliders = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.bannerId = $scope.filter.banners.getId();
		$scope.filter.active.getId() != null && $scope.filter.active.getId() >= 0 ? methodRequest.isActive = $scope.filter.active.getId() : '';
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/getPromotionBannerSliders', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.selectedBanner = $scope.filter.banners.getId();
				$scope.results = data.bannerSliders;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPromotionBannerSliders');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.getSlideUrl = function(defaultImageName){
		if(defaultImageName){
			return settings.backendPromotionsImagesLink + defaultImageName;
		}
		return '';
	}
	
	$scope.enableSlide = function(slideKey){
		showLoading();
		var methodRequest = {};
		methodRequest.bannerSliderId = $scope.results[slideKey].id;
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/enablePromotionBannerSlider', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.results[slideKey].isActive = true;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'enableSlide');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.disableSlide = function(slideKey){
		showLoading();
		var methodRequest = {};
		methodRequest.bannerSliderId = $scope.results[slideKey].id;
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/disablePromotionBannerSlider', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.results[slideKey].isActive = false;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'disableSlide')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.deleteSlide = function(slideKey){
		$rootScope.confirm($rootScope.getMsgs('confirm.slide-delete'), function(){
			showLoading();
			var methodRequest = {};
			methodRequest.bannerSliderId = $scope.results[slideKey].id;
			$http.post(settings.backendJsonLink + 'PromotionManagementServices/deletePromotionBannerSlider', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.results.splice(slideKey, 1);
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'deleteSlide')
				})
				.finally(function() {
					hideLoading();
				})
		});
	}
	
	$scope.reorderSlides = function(slideIds){
		var methodRequest = {};
		methodRequest.bannerId = $scope.filter.banners.getId();
		methodRequest.slideIds = slideIds;
		return $http.post(settings.backendJsonLink + 'PromotionManagementServices/updatePromotionBannerSliderPosition', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				return true;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'reorderSlides')
				return false;
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	
	$scope.getSlide = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.bannerSliderId = $scope.selectedSlide.slideId;
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/getPromotionBannerSliderImages', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.initSlide(data);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPromotionBannerSliders')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.initSlide = function(data){
		$scope.selectedSlide.sliderImages = [];
		$scope.selectedSlide.linkType = new SingleSelect.InstanceClass({});
		$scope.selectedSlide.linkType.fillOptions($scope.filter.linkType.options, true);
		var bannerId = 0;
		if(data && typeof data.bannerId != "undefined"){
			bannerId = data.bannerId;
		}else if($scope.selectedSlide.bannerId){
			bannerId = $scope.selectedSlide.bannerId;
		}
		for(key in $scope.filter.languages){
			if($scope.filter.languages.hasOwnProperty(key)){
				if(data && (sliderImagesKey = searchJsonKeyInArray(data.sliderImages, 'languageId', $scope.filter.languages[key].id)) >= 0){
					$scope.selectedSlide.sliderImages.push(new Slide(bannerId, $scope.selectedSlide.slideId, data.sliderImages[sliderImagesKey]));
				}else{
					$scope.selectedSlide.sliderImages.push(new Slide(bannerId, $scope.selectedSlide.slideId, {languageId: $scope.filter.languages[key].id}));
				}
			}
		}
	}
	
	$scope.getImageFromBank = function(slideKey){
		showLoading();
		$scope.imagesFromBank = {};
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'PromotionManagementServices/getBannerSliderImageFromBank', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.imagesFromBank.slideKey = slideKey;
				$scope.imagesFromBank.images = data.imagesFromBank;
				var previewModal = $uibModal.open({
					templateUrl: folderPrefix + 'components/images-from-bank-popup.html',
					scope: $scope,
					windowClass: 'images-from-bank-popup'
				});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPromotionBannerSliders');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.selectImageFromBank = function(successCallback){
		successCallback();
		$scope.selectedSlide.sliderImages[$scope.imagesFromBank.slideKey].imageName = $scope.imagesFromBank.selected;
	}
	
	$scope.saveSlide = function(_form){
		$rootScope.resetGlobalErrorMsg();
		var service = 'insertPromotionBannerSlider';
		if($scope.action == 'edit'){
			service = 'updateBannerSliderImages';
		}
		if (!_form || _form.$valid) {
			showLoading();
			
			var methodRequest = {};
			if($scope.action == 'add'){
				methodRequest.bannerId = $scope.selectedSlide.bannerId;
			}
			if($scope.action == 'edit'){
				methodRequest.bannerSliderId = $scope.selectedSlide.slideId;
			}
			methodRequest.linkTypeId = $scope.selectedSlide.linkType.getId();
			methodRequest.bannerSliderImages = $scope.selectedSlide.sliderImages;
			for(var i = 0; i < methodRequest.bannerSliderImages.length; i++){
				methodRequest.bannerSliderImages[i].uploadedImage = null;
			}
			$http.post(settings.backendJsonLink + 'PromotionManagementServices/' + service, methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$rootScope.$state.go('ln.marketing.promotions_management', {ln: $scope.$state.params.ln});
					$scope.getPromotionBannerSliders();
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, service)
				})
				.finally(function() {
					hideLoading();
				})
		} else {
			var data = {
				errorCode: 200
			}
			$rootScope.handleErrors(data, service);
		}
	}
	
	//Handle file upload
	$scope.selectedFiles = [];
	$scope.selectedSlideKey = null;
	$scope.progress = [];
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, slideKey) {
		$rootScope.resetGlobalErrorMsg();
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.selectedSlideKey = slideKey;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.selectedSlide.sliderImages[$scope.selectedSlideKey].uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
			$scope.send();
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			params += '&bannerId=' + $scope.selectedSlide.sliderImages[$scope.selectedSlideKey].bannerId;
			params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: settings.backendPromotionsUploadLink + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileName: $scope.selectedFiles[index].name
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0){
					hideLoading();
					$scope.selectedSlide.sliderImages[$scope.selectedSlideKey].uploadedImage = null;
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			hideLoading();
			$scope.selectedSlide.sliderImages[$scope.selectedSlideKey].uploadedImage = null;
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		hideLoading();
		if (!$rootScope.handleErrors(data, 'UploadSlideService')) {
			if(data.userMessages){
				for(var i = 0; i < data.userMessages.length; i++){
					if(data.userMessages[i].field == 'fileName'){
						$scope.selectedSlide.sliderImages[$scope.selectedSlideKey].imageName = data.userMessages[i].message;
					}
				}
			}
		} else {
			$scope.selectedSlide.sliderImages[$scope.selectedSlideKey].uploadedImage = null;
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
	$scope.send = function(){
		showLoading();
		for ( var i = 0; i < $scope.selectedFiles.length; i++) {
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	}
	
	function Slide(bannerId, bannerSliderId, slide){
		if(!slide){
			slide = {};
		}
		this.id = slide.id;
		this.languageId = slide.languageId;
		this.imageName = slide.imageName;
		this.imageUrl = slide.imageUrl;
		this.timeCreated = slide.timeCreated;
		this.timeUpdated = slide.timeUpdated;
		this.writerId = slide.writerId;
		
		this.bannerId = bannerId;
		this.bannerSliderId = bannerSliderId;
	}
	
}]);