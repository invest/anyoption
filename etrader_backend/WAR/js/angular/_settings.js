var settings = {
	domain: window.location.host,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsEUsufix: '_EU',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	writerId: 10000,
	skinId: 2,
	skinIdTexts: 2,
	skinLanguage: 'en',
	documentSizeLimit: 15,//MB
	loggedIn: false,
	serverTime: 0,
	serverOffsetMillis: 0,
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 2,
	loginDetected: false,
	url_params: window.location.search,
	sessionTimeout: (1000*60*31)//31 min
}

var skinMap = {
	'1': {
		subdomain:'www',
		locale: 'iw',
		skinTranslation: 'skins.il',
		isRgulated: false,
		languageId: 1,
		skinSelector: false,
		isAllowed: false,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Zא-ת ]+$",
		platforms: [1]
	},
	'2': {
		subdomain:'www',
		locale: 'en',
		skinTranslation: 'skins.us',
		isRgulated: false,
		rgulatedSkinId: 16,
		languageId: 2,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	},
	'3': {
		subdomain:'tr',
		locale: 'tr',
		skinTranslation: 'skins.tr',
		isRgulated: false,
		languageId: 3,
		skinSelector: false,
		isAllowed: false,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$",
		platforms: [2]
	},
	'4': {
		subdomain:'ar',
		locale: 'ar',
		skinTranslation: 'skins.ar',
		isRgulated: false,
		languageId: 4,
		skinSelector: false,
		isAllowed: false,
		isActive: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2]
	},
	'5': {
		subdomain:'es',
		locale: 'es',
		skinTranslation: 'skins.spa',
		isRgulated: false,
		rgulatedSkinId: 18,
		languageId: 5,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 5,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		platforms: [2,3]
	},
	'8': {
		subdomain:'de',
		locale: 'de',
		skinTranslation: 'skins.de',
		isRgulated: false,
		rgulatedSkinId: 19,
		languageId: 8,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 8,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$",
		platforms: [2,3]
	},
	'9': {
		subdomain:'it',
		locale: 'it',
		skinTranslation: 'skins.ita',
		isRgulated: false,
		rgulatedSkinId: 20,
		languageId: 9,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 9,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$",
		platforms: [2,3]
	},
	'10': {
		subdomain:'ru',
		locale: 'ru',
		skinTranslation: 'skins.rus',
		isRgulated: false,
		languageId: 10,
		skinSelector: false,
		isAllowed: false,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZА-Яа-яЁё ]+$",
		platforms: [2]
	},
	'12': {
		subdomain:'fr',
		locale: 'fr',
		skinTranslation: 'skins.fr',
		isRgulated: false,
		rgulatedSkinId: 21,
		languageId: 12,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 12,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$",
		platforms: [2,3]
	},
	'13': {
		subdomain:'en-us',
		locale: 'en-us',
		skinTranslation: 'skins.enus',
		isRgulated: false,
		languageId: 2,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 13,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2]
	},
	'14': {
		subdomain:'es-us',
		locale: 'es-us',
		skinTranslation: 'skins.esus',
		isRgulated: false,
		languageId: 5,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 14,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		platforms: [2]
	},
	'15': {
		subdomain:'sg',
		locale: 'sg',
		skinTranslation: 'skins.zh',
		isRgulated: false,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$",
		platforms: [2]
	},
	'16': {
		subdomain:'www',
		locale: 'en',
		skinTranslation: 'skins.useu',
		isRgulated: true,
		languageId: 2,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 16,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	},
	'17': {
		subdomain:'kr',
		locale: 'kr',
		skinTranslation: 'skins.kr',
		isRgulated: false,
		languageId: 17,
		skinSelector: false,
		isAllowed: false,
		isActive: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$",
		platforms: [2]
	},
	'18': {
		subdomain:'es',
		locale: 'es',
		skinTranslation: 'skins.spaeu',
		isRgulated: true,
		languageId: 5,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 18,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		platforms: [2,3]
	},
	'19': {
		subdomain:'de',
		locale: 'de',
		skinTranslation: 'skins.deeu',
		isRgulated: true,
		languageId: 8,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 19,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$",
		platforms: [2,3]
	},
	'20': {
		subdomain:'www1',
		locale: 'it',
		skinTranslation: 'skins.itaeu',
		isRgulated: true,
		languageId: 9,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 20,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$",
		platforms: [2,3]
	},
	'21': {
		subdomain:'fr',
		locale: 'fr',
		skinTranslation: 'skins.freu',
		isRgulated: true,
		languageId: 12,
		isAllowed: true,
		isActive: true,
		skinSelector: false,
		skinIdTexts: 21,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$",
		platforms: [2,3]
	},
	'22': {
		subdomain:'hk',
		locale: 'hk',
		skinTranslation: 'skins.22',
		isRgulated: false,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		isActive: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$",
		platforms: [2]
	},
	'23': {
		subdomain:'nl',
		locale: 'nl',
		skinTranslation: 'skins.nl',
		isRgulated: true,
		languageId: 23,
		skinSelector: false,
		isAllowed: true,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Zéëï ]+$",
		platforms: [2,3]
	},
	'24': {
		subdomain:'se',
		locale: 'sv',
		skinTranslation: 'skins.se',
		isRgulated: true,
		languageId: 24,
		skinSelector: false,
		isAllowed: true,
		isActive: true,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZÅÄÖåäö ]+$",
		platforms: [2,3]
	},
	'25': {
		subdomain:'www',
		locale: 'en',
		skinTranslation: 'skins.goldbeam.en',
		isRgulated: false,
		isGoldbeam: true,
		rgulatedSkinId: 16,
		defaultSkinId: 16,
		languageId: 25,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	},
	'26': {
		subdomain:'es',
		locale: 'es',
		skinTranslation: 'skins.goldbeam.es',
		isRgulated: false,
		isGoldbeam: true,
		rgulatedSkinId: 18,
		defaultSkinId: 18,
		languageId: 26,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 5,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$",
		platforms: [2,3]
	},
	'27': {
		subdomain:'cs',
		locale: 'cs',
		skinTranslation: 'skins.cz',
		isRgulated: true,
		rgulatedSkinId: 27,
		defaultSkinId: 27,
		languageId: 27,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 27,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	},
	'28': {
		subdomain:'pl',
		locale: 'pl',
		skinTranslation: 'skins.pl',
		isRgulated: true,
		rgulatedSkinId: 28,
		defaultSkinId: 28,
		languageId: 28,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 28,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	},
	'29': {
		subdomain:'pt',
		locale: 'pt',
		skinTranslation: 'skins.pt',
		isRgulated: true,
		rgulatedSkinId: 29,
		defaultSkinId: 29,
		languageId: 29,
		isAllowed: true,
		isActive: true,
		skinSelector: true,
		skinIdTexts: 29,
		regEx_letters: "^['a-zA-Z ]+$",
		platforms: [2,3]
	}
}

var errorCodeMap = {
	success: 0,
	withdrawal_wrong_status: 112,
	withdrawal_negative_amount: 113,
	withdrawal_amount_higher_than_allowed: 114,
	withdrawal_unauthorized_transaction_type: 115,
	withdrawal_empty_cheque: 116,
	withdrawal_flagged: 117,
	withdrawal_no_flag_subject: 118,
	withdrawal_no_bank_id: 119,
	withdrawal_no_expected_provider: 121,
	missing_values: 200,
	login_failed: 205,
	invalid_email: 207,
	regulation_missing_questionnaire: 300,
	regulation_suspended: 301,
	regulation_suspended_documents: 302,
	user_not_regulated: 303,
	regulation_user_restricted: 304,
	reg_suspended_quest_incorrect: 305,
	regulation_user_pep_prohibited: 307,
	regulation_user_is_treshold_block: 308,
	failed_insert: 5000,
	failed_update: 5001,
	role_name_exists: 6000,
	fail_to_insert_banner_sliders: 6001,
	not_allowed_extension_slider_image: 6002,
	wrong_dimensions_slider_image: 6003,
	fail_to_delete_banner_slider: 6004,
	fail_to_disable_banner_slider: 6005,
	fail_to_enable_banner_slider: 6006,
	fail_to_update_banner_slider_image: 6007,
	missing_banner_slider_image_link: 6008,
	unable_to_get_slider_images: 6009,
	unable_to_update_slider_position: 6010,
	fail_to_load_banner_sliders: 6011,
	no_permission: 6998,
	session_expired: 6999,
	file_too_large: 600,
	miss_support_reject_reason: 4001,
	miss_control_reject_reason: 4002,
	miss_support_reject_comment: 4003,
	miss_file: 4004,
	miss_exp_date: 4005,
	miss_control_reject_comment: 4006,
	miss_support_approved_and_reject: 4007,
	miss_control_approved_and_reject: 4008,
	miss_cc_id: 4009,
	miss_file_id: 4010,
	invalid_date_range: 4011,
	unable_sent_keesing_file: 4012,
	no_found_file: 4013,
	cc_document_sent_updated:4014,
	odds_group_value_not_in_odds_type: 4700,
	withdraw_amount_higher_than_user_balance: 787,
	code_writer_username_or_email_incorrect: 5003,
	writer_is_not_active: 5004,
	unknown: 999
}

settings.currencies = {
	'2': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.usd',
		currencyLeftSymbol: true,
		currencyName: 'currencies.usd'
	},
	'1': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.ils',
		currencyLeftSymbol: true,
		currencyName: 'currencies.ils'
	},
	'3': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.eur',
		currencyLeftSymbol: true,
		currencyName: 'currencies.eur'
	},
	'4': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.gbp',
		currencyLeftSymbol: true,
		currencyName: 'currencies.gbp'
	},
	'5': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.try',
		currencyLeftSymbol: false,
		currencyName: 'currencies.try'
	},
	'6': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.rub',
		currencyLeftSymbol: false,
		currencyName: 'currencies.rub'
	},
	'8': {
		decimalPointDigits: 0,
		currencySymbol: 'currency.krw',
		currencyLeftSymbol: true,
		currencyName: 'currencies.krw'
	},
	'7': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.cny',
		currencyLeftSymbol: true,
		currencyName: 'currencies.cny'
	},
	'10': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.aud',
		currencyLeftSymbol: false,
		currencyName: 'currencies.aud'
	},
	'12': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.czk',
		currencyLeftSymbol: false,
		currencyName: 'currencies.czk'
	},
	'9': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.sek',
		currencyLeftSymbol: false,
		currencyName: 'currencies.sek'
	},
	'11': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.zar',
		currencyLeftSymbol: false,
		currencyName: 'currencies.zar'
	},
	'13': {
		decimalPointDigits: 2,
		currencySymbol: 'currency.pln',
		currencyLeftSymbol: false,
		currencyName: 'currencies.pln'
	}
}

var amountDivideBy100 = true;

settings.platforms = {
	ao: 2,
	co: 3
}

TOAST_TYPES = {
	info: 1,
	error: 2
}

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = "^['\\p{L} ]+$";