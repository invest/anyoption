/* controller::ApmWithdrawalsController */
(function() {
	'use strict';

	angular.module('backendApp').controller('ApmWithdrawalsController', ApmWithdrawalsController);

	ApmWithdrawalsController.$inject = ['screenSettingsPrepService', 'utilsService', 'apmWithdrawalsService'];
	function ApmWithdrawalsController(screenSettingsPrepService, utilsService, apmWithdrawalsService) {
		var _this = this;
		_this._u = utilsService;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.withdrawTypes = apmWithdrawalsService.withdrawTypes;
		
		_this.form = {}
		_this.ready = true;
		_this.state = 0;//0-form, 1-confirm fee, 2-success, 3-cancel, 4-confirm no fee, 
		
		_this.approveMsgs = '';
		
		
		//Set visible transaction types
		_this.count = 0;
		
		_this.displaySkrill = true;
		_this.displayDirect24 = screenSettings.screenFilter.transactionTypesWithdraw.Direct24;
		_this.displayGiropay = screenSettings.screenFilter.transactionTypesWithdraw.Giropay;
		_this.displayEPS = screenSettings.screenFilter.transactionTypesWithdraw.EPS;
		_this.displayIdeal = screenSettings.screenFilter.transactionTypesWithdraw['iDeal Withdraw'];

		_this.count = 0;
		if (screenSettings.screenFilter.transactionTypesWithdraw.Skrill) {_this.count++;}
		if (screenSettings.screenFilter.transactionTypesWithdraw.Direct24) {_this.count++;}
		if (screenSettings.screenFilter.transactionTypesWithdraw.Giropay) {_this.count++;}
		if (screenSettings.screenFilter.transactionTypesWithdraw.EPS) {_this.count++;}
		if (screenSettings.screenFilter.transactionTypesWithdraw['iDeal Withdraw']) {_this.count++;}

		if (_this.count == 1) {
			if (screenSettings.screenFilter.transactionTypesWithdraw.Skrill) {_this.form.withdrawType = _this.withdrawTypes.skrill;}
			if (screenSettings.screenFilter.transactionTypesWithdraw.Direct24) {_this.form.withdrawType = _this.withdrawTypes.direct24;}
			if (screenSettings.screenFilter.transactionTypesWithdraw.Giropay) {_this.form.withdrawType = _this.withdrawTypes.giropay;}
			if (screenSettings.screenFilter.transactionTypesWithdraw.EPS) {_this.form.withdrawType = _this.withdrawTypes.eps;}
			if (screenSettings.screenFilter.transactionTypesWithdraw['iDeal Withdraw']) {_this.form.withdrawType = _this.withdrawTypes.ideal;}
		}
		
		
		_this.submitWithdraw = submitWithdraw;
		_this.approveWithdraw = approveWithdraw;
		_this.cancelWithdraw = cancelWithdraw;
		_this.clearError = clearError;
		_this.clearErrors = clearErrors;
		
		
		//functions
		
		function submitWithdraw(_form) {
			if (_form.$valid) {
				if (isUndefined(_this.form.withdrawType)) {
					showToast(2, _this._u.getMsg('no-provider'));
					return;
				}
				_this.WithdrawalRequest = {
					beneficiaryName: _this.form.beneficiaryName,
					swiftBicCode: (_this.form.withdrawType == _this.withdrawTypes.eps || _this.form.withdrawType == _this.withdrawTypes.ideal) ? _this.form.swiftBicCode : null,// EPS only
					iban: (_this.form.withdrawType == _this.withdrawTypes.eps || _this.form.withdrawType == _this.withdrawTypes.ideal) ? _this.form.iban : null,//EPS only
					accountNumber: (_this.form.withdrawType != _this.withdrawTypes.ideal) ? parseInt(_this.form.accountNumber) : null,
					bankName: (_this.form.withdrawType != _this.withdrawTypes.ideal) ? _this.form.bankName : null,
					branchNumber: (_this.form.withdrawType != _this.withdrawTypes.ideal) ? parseInt(_this.form.branchNumber) : null,
					branchAddress: (_this.form.withdrawType != _this.withdrawTypes.ideal) ? _this.form.branchAddress : null,
					moneybookersEmail: (_this.form.withdrawType == _this.withdrawTypes.skrill) ? _this.form.moneybookersEmail : null,
					amount: parseInt(_this.form.amount),
					gmType: _this.form.withdrawType, // EPS value->61, Giropay value->60, Direct24 value->59, ideal value->63
					userId: userId,
					exemptFee: _this.form.exemptFee,
					writerId: writerId
				};
				
				if (_this.form.exemptFee) {
					_this.state = 4;
					return;
				}
				
				apmWithdrawalsService.submitWithdraw(_this.WithdrawalRequest, _this.form.withdrawType)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.state = 1;
						_this.approveMsgs = _this._u.getMsg('bank.wire.approve.message');
						_this.approveMsgs = _this.approveMsgs.replace('{0}', formatAmountSimple(data.fee/100, 0));
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'validateGMWithdraw');
					})
			} else {
				logIt({'type':3,'msg':_form.$error});
			}
		}
		
		function approveWithdraw(_form) {
			apmWithdrawalsService.approveWithdraw(_this.WithdrawalRequest, _this.form.withdrawType)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					_this.state = 2;
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'approveWithdraw');
				})
		}
		
		function cancelWithdraw() {
			_this.state = 3;
		}
		
		//TODO - consider a better approach (a directive and property in the controller probably)
		function clearError(id){
			$('#' + id).removeClass('error');
			$('#' + id + '-error').html('');
			$('#' + id + '-error').removeClass('error');
		}
		function clearErrors(){
			$('.apm-withdrawals-screen .error').each(function(index, el){
				clearError(el.id);
			});
		}
	}
})();