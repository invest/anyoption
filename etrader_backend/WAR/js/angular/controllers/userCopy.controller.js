/* controller::UserCopyController */
(function() {
	'use strict';

	angular.module('backendApp').controller('UserCopyController', UserCopyController);

	UserCopyController.$inject = ['utilsService', 'copyopUserCopiersServices', 'screenSettingsPrepService'];
	function UserCopyController(utilsService, copyopUserCopiersServices, screenSettingsPrepService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.formCopy;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.resultList = null;
		
		//filters
		this.filter = {
			fromDate: _this._u.setDateNoSec({setMonths: -1}),
			toDate: _this._u.setDateNoSec(),
			userLeaderId: '',
			userId: '',
			stillCopying: new _this._u.SingleSelect.InstanceClass({
				options: screenSettings.screenFilters.stillCopying
			}),
			isFrozen: new _this._u.SingleSelect.InstanceClass({
				options: screenSettings.screenFilters.isFrozen
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
		this.filter.stillCopying.setModel(this.filter.stillCopying.getOptionById(0));
		this.filter.isFrozen.setModel(this.filter.isFrozen.getOptionById(0));

		this.dateManager = new _this._u.Utils.DateManager();
		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});
		
		_this.writerCurrencyId = currencyId;
		if (_this._u.$state.is('ln.users.copyop.copying')) {
			_this.screen = 'copied';
		} else {
			_this.screen = 'copiers';
		}
		
		_this.getList = getList;
		_this.stop = stop;
		
		
		//functions
		function getList(_form, isNext) {
			if (_form) {
				_this.formCopy = _form;
			} else {
				_form = _this.formCopy
			}
			
			if (_form.$valid) {
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var CopyopUserCopiersMethodRequest = {
					fromDate: _this._u.setDateNoSec({date: _this.filter.fromDate, checkNull: true, startOfDay: true, timestamp: true}),
					toDate: _this._u.setDateNoSec({date: _this.filter.toDate, checkNull: true, endOfDay: true, timestamp: true}),
					userLeaderId: (_this.filter.userLeaderId == '') ? -1 : _this.filter.userLeaderId,
					userId: userId,
					isStillCopying: _this.filter.stillCopying.getId(),
					isFrozen: _this.filter.isFrozen.getId(),
					rowsPerPage: _this.filter.resultsPerPage.getId(),
					isNext: (isNext) ? isNext : false
				}
				
				copyopUserCopiersServices.getList(_this.screen, CopyopUserCopiersMethodRequest)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.resultList = data.userCopyList;
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getUserCopiers');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function stop(user) {
			_this._u.resetGlobalErrorMsg();
			_this._u.showLoading();
			
			var CopyopStopUserRequest = {
				userId: userId,
				userClassId: userClassId,
				copyopUser: user
			};
			
			var sure = confirm('Are you sure you want to delete?');
			if (sure) {
				copyopUserCopiersServices.stop(CopyopStopUserRequest)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'stop');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			}
		}
	}
})();