/* controller::ChargebacksAddEditController */
(function() {
	'use strict';

	angular.module('backendApp').controller('ChargebacksAddEditController', ChargebacksAddEditController);

	ChargebacksAddEditController.$inject = ['screenSettingsPrepService', 'utilsService', 'chargebackService', '$rootScope'];
	function ChargebacksAddEditController(screenSettingsPrepService, utilsService, chargebackService, $rootScope) {
		var _this = this;
		_this._u = utilsService;
		
		_this.isEdit = true;
		_this.isDisabledFlag = true;
		
		this.amountSettings = {centsPart: 2};
		
		var screenSettings = screenSettingsPrepService;

		var allReasonCodesFilter = [];
		var statuses = [];
		
		this.filterDetails = {
			statuses: new _this._u.SingleSelect.InstanceClass({
				mutateCallback: _this._u.getMsg, 
			}),
			rc: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'choose'}, 
				mutateCallback: _this._u.getMsg, 
				mutateOnlyAggregate: true
			}),
		}
		
		this.dateManager = new _this._u.Utils.DateManager();
		this.dateManagerDispute = new _this._u.Utils.DateManager();
		this.dateManagerDispute.onOpenCallback = function() {
			if (!_this.chargebackDetails.disputeDate || _this.chargebackDetails.disputeDate == 0) {
				_this.chargebackDetails.disputeDate = _this._u.setDateNoSec();
			}
		}
		

		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});
		
		_this.getDetails = getDetails;
		_this.saveChargeback = saveChargeback;

		_this.openCalendar = openCalendar;
		_this.changeMode = changeMode;
		_this.goBack = goBack;
		
		_this.getDetails();
		
		
		
		//functions
		function getDetails() {
			var ChargebackRequest = {
				transactionId: $rootScope.stateParams.transactionId
			}
			chargebackService.get('getChargebackDetails', ChargebackRequest)
				.then(function(data) {
					if (data.chargebackDetails.id == 0) {
						_this.isEdit = false;
						changeMode(true);
					}
					
					_this.chargebackDetails = data.chargebackDetails;
					_this.chargebackDetails.transactionId = $rootScope.stateParams.transactionId;
					
					if (screenSettings.allReasonCodes) {
						for (var i = 0; i < screenSettings.allReasonCodes.length; i++) {
							if (!_this.chargebackDetails.creditCard || screenSettings.allReasonCodes.type == _this.chargebackDetails.creditCard.typeId) {
								allReasonCodesFilter.push({
									id: screenSettings.allReasonCodes[i].rc,
									name: screenSettings.allReasonCodes[i].rc + ' ' + screenSettings.allReasonCodes[i].reasonCode
								})
							}
						}
						
						if (!_this.isEdit) {
							var allowKeys = [2, 5, 6];
							angular.forEach(screenSettings.screenFilters.statuses, function(value, key) {
								if ($.inArray(parseInt(key), allowKeys) > -1) {
									statuses.push({
										id: parseInt(key),
										name: value
									})
								}
							})
						} else {
							statuses = screenSettings.screenFilters.statuses;
						}
					}
					
					allReasonCodesFilter.push({
						id: null,
						name: 'Other'//will fix when we wait for trnaslations
					})
					
					_this.filterDetails.rc.fillOptions(allReasonCodesFilter);
					_this.filterDetails.statuses.fillOptions(statuses);
				
					if (_this.chargebackDetails.status && _this.chargebackDetails.status > 0) {
						_this.filterDetails.statuses.setModel(_this.filterDetails.statuses.getOptionById(_this.chargebackDetails.status));
					} else {
						_this.filterDetails.statuses.setModel(_this.filterDetails.statuses.getOptionById(2));
					}
					
					if (_this.chargebackDetails.rc && _this.chargebackDetails.rc > 0) {
						if (_this.filterDetails.rc.getOptionById(_this.chargebackDetails.rc)) {
							_this.filterDetails.rc.setModel(_this.filterDetails.rc.getOptionById(_this.chargebackDetails.rc));
						} else {
							_this.filterDetails.rc.setModel(_this.filterDetails.rc.getLastOption());
						}
					}
					if (!_this.chargebackDetails.chargebackDate || _this.chargebackDetails.chargebackDate === 0) {
						_this.chargebackDetails.chargebackDate = _this._u.setDateNoSec({setDays: -1});
					}
				})
		}
	
		function saveChargeback(_form) {
			_form.$setSubmitted();
			if (_form.$valid) {
				_this._u.showLoading();
				
				var serviceName = (_this.isEdit) ? 'updateChargeback' : 'insertChargeback';
				
				var ChargebackDetailsRequest = {
					chargebackDetails: {
						writerId: writerId,
						userId: _this.chargebackDetails.userId,
						transactionId: _this.chargebackDetails.transactionId,
						id: _this.chargebackDetails.id,
						chargebackDate: _this._u.checkNull(_this.chargebackDetails.chargebackDate, 'date'),
						status: _this._u.checkNull(_this.filterDetails.statuses.getId(), 'id'),
						caseId: _this._u.checkNull(_this.chargebackDetails.caseId),
						arn: _this._u.checkNull(_this.chargebackDetails.arn),
						rc: _this._u.checkNull(_this.filterDetails.rc.getId(), 'id'),
						rcNew: _this._u.checkNull(_this.chargebackDetails.rcNew),
						reasonCodeNew: _this._u.checkNull(_this.chargebackDetails.reasonCodeNew),
						disputeDate: _this._u.checkNull(_this.chargebackDetails.disputeDate, 'date'),
						comments: _this._u.checkNull(_this.chargebackDetails.comments),
						provider: _this.chargebackDetails.provider,
						amount: _this.chargebackDetails.amount,
						currencyId: _this.chargebackDetails.currencyId,
					}
				}
				
				chargebackService.set(serviceName, ChargebackDetailsRequest)
					.then(function(data) {
						var toastMsg = (_this.isEdit) ? 'chargeback-edited' : 'chargeback-added';
						showToast(1, $rootScope.getMsgs(toastMsg));
						setTimeout(function() {
							goBack();
						}, 2000)
					})
					.catch(function(data) {
						_this._u.handleErrors(data, serviceName);
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, $rootScope.getMsgs('error.mandatory'));
			}
		}

		function openCalendar(e, date) {
			_this.openDates[date] = true;
			if (e.target.tagName.toLowerCase() == 'input') {
				setTimeout(function() {
					e.target.focus();
				}, 100);
			}
		}
		
		function changeMode(mode) {
			if (mode) {
				_this.editAddMode = mode;
			} else {
				_this.editAddMode = !_this.editAddMode;
			}
			_this.isDisabledFlag = isDisabled();
		}
	
		function goBack() {
			window.history.back();
		}
		
		function isDisabled() {
			if (_this.isEdit) {
				if ($rootScope.permissions.get('chargebacks_edit') && _this.editAddMode) {
					return false;
				} else {
					return true;
				}
			} else {
				return !$rootScope.permissions.get('chargebacks_add');
			}
		}
	}
})();