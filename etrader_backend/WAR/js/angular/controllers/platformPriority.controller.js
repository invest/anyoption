/* controller::PlatformPriorityController */
(function() {
	'use strict';

	angular.module('backendApp').controller('PlatformPriorityController', PlatformPriorityController);

	PlatformPriorityController.$inject = ['platformPriorityPrepService', 'utilsService', 'platformPriorityService'];
	function PlatformPriorityController(platformPriorityPrepService, utilsService, platformPriorityService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.loginProductMap = platformPriorityPrepService.loginProductMap;
		_this.loginProductTypes = platformPriorityPrepService.loginProductTypes;
		_this.skinsFilter = platformPriorityPrepService.skinsFilter;
		
		_this.filter = {
			skinIds: [],
			loginProduct: new _this._u.SingleSelect.InstanceClass({
				options: _this.loginProductTypes
			}),
			enabled: true,
			checkAll: false
		}
		_this.filter.loginProduct.setModel(this.filter.loginProduct.getOptionById('BINARY'));
	
		_this.update = update;
		_this.checkAll = checkAll;
	
		function update() {
			if (_this.filter.skinIds.length != 0) {
				var request = {
					writerId: writerId,
					skinIds: _this.filter.skinIds,
					loginProduct: _this.filter.loginProduct.getId(),
					enabled: _this.filter.enabled
				}
				
				platformPriorityService.update(request)
					.then(function(data) {
						showToast(1, _this._u.getMsg('general.update.success'));
						
						_this.filter.skinIds.map(function(current) {
							_this.loginProductMap[current].enabled = _this.filter.enabled;
							_this.loginProductMap[current].timeUpdated = new Date();
							_this.loginProductMap[current].loginProduct = _this.filter.loginProduct.getId();
						});
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function checkAll() {
			_this.filter.skinIds.length = 0;
			
			if (_this.filter.checkAll) {
				angular.forEach(_this.skinsFilter, function(value, key) {
					_this.filter.skinIds.push(key);
				})
			}
		}

	}
})();