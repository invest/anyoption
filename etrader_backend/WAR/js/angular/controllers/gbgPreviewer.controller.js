/* controller::GbgPreviewerController */
(function() {
	'use strict';

	angular.module('backendApp').controller('GbgPreviewerController', GbgPreviewerController);

	GbgPreviewerController.$inject = ['screenSettingsPrepService', 'utilsService', 'gbgPreviewerService'];
	function GbgPreviewerController(screenSettingsPrepService, utilsService, gbgPreviewerService) {
		var _this = this;
		_this._u = utilsService;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.fileId = _this._u.getStateParams().fileId;
		_this.xmlRequest = null;
		_this.xmlResponse = null;
		_this.xmlResultCodes = null;
		
		_this.getGbgXml = getGbgXml;
		_this.getRequestVal = getRequestVal;
		_this.getResponseVal = getResponseVal;
		_this.getResultCodeVal = getResultCodeVal;
		_this.getNodeVal = getNodeVal;
		
		getGbgXml();
		
		//functions
		function getGbgXml() {
			_this._u.resetGlobalErrorMsg();
			_this._u.showLoading();
			
			var request = {
				file: {
					id: _this.fileId
				}
			};
			
			gbgPreviewerService.getGbgXml(request)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					if(!_this._u.handleErrors(data, 'getGbgXml')){
						_this.xmlRequest = $(data.xmlRequest);
						_this.xmlResponse = $(data.xmlResponse);
						_this.xmlResultCodes = getResultsCodes();
					}
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'getGbgXml');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
		function getResultsCodes(){
			if(!_this.xmlResponse){
				return;
			}
			var arr = [];
			var children = _this.xmlResponse.find('s\\:body AuthenticateSPResponse AuthenticateSPResult ResultCodes').children();
			for(var i = 0; i < children.length; i++){
				var globalResultCodesArr = [];
				var globalResultCodes = $(children[i]).find('GlobalItemCheckResultCode');
				for(var j = 0; j < globalResultCodes.length; j++){
					globalResultCodesArr.push({node: globalResultCodes[j]});
				}
				arr.push({node: children[i], globalResultCodes: globalResultCodesArr});
			}
			return arr;
		}
		
		function getRequestVal(property, parent){
			if(!_this.xmlRequest || !property){
				return;
			}
			if(!parent){
				parent = _this.xmlRequest;
			}
			property = property.replace(/\./gi, ' ').replace(/\:/gi, '\\:').replace(/body/gi, 'SOAP-ENV\\:Body');
			return parent.find(property).html();
		}
		
		function getResponseVal(property, parent){
			if(!_this.xmlResponse || !property){
				return;
			}
			if(!parent){
				parent = _this.xmlResponse;
			}
			property = property.replace(/\./gi, ' ').replace(/body/gi, 's\\:body');
			return parent.find(property).html();
		}
		
		function getResultCodeVal(property, resultCode){
			if(!_this.xmlResponse || !property){
				return;
			}
			property = property.replace(/\./gi, ' ');
			return $(resultCode.node).find(property).html();
		}
		
		function getNodeVal(property, nodeHolder){
			if(!_this.xmlResponse || !property){
				return;
			}
			property = property.replace(/\./gi, ' ');
			return $(nodeHolder.node).find(property).html();
		}
	}
})();