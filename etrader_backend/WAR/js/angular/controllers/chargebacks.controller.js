/* controller::ChargebacksController */
(function() {
	'use strict';

	angular.module('backendApp').controller('ChargebacksController', ChargebacksController);

	ChargebacksController.$inject = ['screenSettingsPrepService', 'utilsService', 'chargebackService'];
	function ChargebacksController(screenSettingsPrepService, utilsService, chargebackService) {
		var _this = this;
		_this._u = utilsService;
		
		this.amountSettings = {centsPart: 2};
		this.action = '';
		this.editAddMode = false;
		this.formDetails = {}
		
		var screenSettings = screenSettingsPrepService;

		var allReasonCodesFilter = [];
		if (screenSettings.allReasonCodes) {
			for (var i = 0; i < screenSettings.allReasonCodes.length; i++) {
				allReasonCodesFilter.push({
					id: screenSettings.allReasonCodes[i].rc,
					name: screenSettings.allReasonCodes[i].rc + ' ' + screenSettings.allReasonCodes[i].reasonCode
				});
			}
			
			allReasonCodesFilter.push({
				id: null,
				name: 'Other'//will fix when we wait for trnaslations
			})
		}
		
		_this.results = null;
		
		//filters
		this.filter = {
			fromDate: _this._u.setDateNoSec({month: -1}),
			toDate: _this._u.setDateNoSec(),
			transactionId: '',
			userId: '',
			arn: '',
			ccLast4Digits: '',
			classUsers: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				options: screenSettings.screenFilters.classUsers
			}),
			skinBusinessCases: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: false,
				options: screenSettings.screenFilters.skinBusinessCases
			}),
			skins: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true,
				options: screenSettings.screenFilters.skins
			}),
			countries: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true, 
				options: screenSettings.screenFilters.countries,
				iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}
			}),
			currencies: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				options: screenSettings.screenFilters.currencies,
				isSmart: true
			}),
			statuses: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				options: screenSettings.screenFilters.statuses,
				isSmart: true
			}),
			providers: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.providers,
				isSmart: true
			}),
			rc: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				mutateOnlyAggregate: true,
				options: allReasonCodesFilter,
				isSmart: true
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		//set default loaded
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
		this.filter.classUsers.setModel(this.filter.classUsers.getOptionById(12));
		this.filter.statuses.setSelectedOptions([2]);

		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
//		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});//BAC-4114
		
		_this.openDates = {
			fromDate: false,
			toDate: false,
			chargebackDate: false,
			disputeDate: false
		}
		
		_this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
		}
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		
		_this.getChargebacks = getChargebacks;
		// _this.getChargebacksSavedSearch = getChargebacksSavedSearch;
		// _this.getChargebacksSavedSearch();
		
		_this.openCalendar = openCalendar;
		_this.changePage = changePage;
		
		
		//functions
		function getChargebacks(_form, page) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if((page = _this.pagination.validatePage(page)) === false){
				return;
			}
			
			if (_form.$valid || paging) {
				_this.currentPage = page;
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var otherRC = false;
				var rc = _this._u.checkNull(_this.filter.rc.getCheckedIds(), 'ids');
				var index = $.inArray(null, _this.filter.rc.getCheckedIds());
				if (index  > -1) {
					rc.splice(index, 1);
					otherRC = true;
				}
				
				var ChargebackFiltersRequest = {
					// saveRequest: true,
					chargebackFilters: {
						fromDate: _this.filter.fromDate.getTime(),
						toDate: _this.filter.toDate.getTime(),
						transactionId: _this._u.checkNull(_this.filter.transactionId),
						userId: _this._u.checkNull(_this.filter.userId),
						arn: _this._u.checkNull(_this.filter.arn),
						last4Digits: _this._u.checkNull(_this.filter.ccLast4Digits),
						userClasses: _this._u.checkNull(_this.filter.classUsers.getId(), 'id'),
						businessCases: _this._u.checkNull(_this.filter.skinBusinessCases.getCheckedIds(), 'ids'),
						skins: _this._u.checkNull(_this.filter.skins.getCheckedIds(), 'ids'),
						countries: _this._u.checkNull(_this.filter.countries.getCheckedIds(), 'ids'),
						currencies: _this._u.checkNull(_this.filter.currencies.getCheckedIds(), 'ids'),
						statuses: _this._u.checkNull(_this.filter.statuses.getCheckedIds(), 'ids'),
						providers: _this._u.checkNull(_this.filter.providers.getCheckedIds(), 'ids'),
						rc: !otherRC ? rc : null,
						otherRC: otherRC,
						page: page,
						rowsPerPage: _this.filter.resultsPerPage.getId()
					}
				}
				
				chargebackService.get('getChargebacks', ChargebackFiltersRequest)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.results = data.chargebacksList;
						
						var totalCount = 0;
						if(_this.results.length > 0){
							totalCount = _this.results[0].totalCount;
						}
						_this.pagination.setPages(page, totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getChargebacks');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		// function getChargebacksSavedSearch() {
			// chargebackService.get('getChargebacks')
				// .then(function(data) {
					// afterGetChargebacks(data);
				// })
				// .finally(function(data) {
					// _this._u.hideLoading();
				// })
		// }
		
		function openCalendar(e, date) {
			_this.openDates[date] = true;
			if (e.target.tagName.toLowerCase() == 'input') {
				setTimeout(function() {
					e.target.focus();
				}, 100);
			}
		}
		
		function changePage(page) {
			getChargebacks(null, page);
		}
	}
})();