/* controller::TradersActionsController */
(function() {
	'use strict';

	angular.module('backendApp').controller('TradersActionsController', TradersActionsController);

	TradersActionsController.$inject = ['screenSettingsPrepService', 'utilsService', 'tradersActionsService'];
	function TradersActionsController(screenSettingsPrepService, utilsService, tradersActionsService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.resultsList = null;
		
		var screenSettings = screenSettingsPrepService;

		//filters
		this.filter = {
			fromDate: _this._u.setDateNoSec({setDays: -1}),
			toDate: _this._u.setDateNoSec(),
			actions: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg,
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.actions
			}),
			assets: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg,
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.assets
			}),
			traders: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg,
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.traders
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));

		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		_this.dateManager = new _this._u.Utils.DateManager();
		_this.orderBy = 1;
		_this.orderWay = -1;
		
		
		_this.getList = getList;
		_this.orderList = orderList;
		_this.changePage = changePage;
		
		
		//functions
		function getList(_form, page, orderBy, orderWay) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if ((page = _this.pagination.validatePage(page)) === false) {
				return;
			}
			
			if (_form.$valid || paging) {
				_this.currentPage = page;
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				if (!orderBy) {orderBy = _this.orderBy;}
				if (!orderWay) {orderWay = _this.orderWay;}
				
				var TradersActionsMethodRequest = {
					fromDate: _this._u.setDateNoSec({date: _this.filter.fromDate, checkNull: true, startOfDay: true, timestamp: true}), 
					toDate: _this._u.setDateNoSec({date: _this.filter.toDate, checkNull: true, endOfDay: true, timestamp: true}),
					pageNumber: page,
					rowsPerPage: _this.filter.resultsPerPage.getId(),
					actionId: _this._u.checkNull(_this.filter.actions.getId(), 'id'),
					assetId: _this._u.checkNull(_this.filter.assets.getId(), 'id'),
					traderId: _this._u.checkNull(_this.filter.traders.getId(), 'id'),
					column: orderBy,
					orderType: orderWay
				}
				
				tradersActionsService.getList(TradersActionsMethodRequest)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.resultList = data.tradersActions;
						
						var totalCount = 0;
						if(_this.resultList.length > 0){
							totalCount = _this.resultList[0].totalCount;
						}
						_this.pagination.setPages(page, totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getStuff');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function orderList(orderBy, page) {
			if (orderBy == _this.orderBy) {
				_this.orderWay = (_this.orderWay == 1) ? -1 : 1;
			}
			_this.orderBy = orderBy;
			
			getList(null, page, orderBy, _this.orderWay);
		}
		
		function changePage(page) {
			getList(null, page);
		}
	}
})();