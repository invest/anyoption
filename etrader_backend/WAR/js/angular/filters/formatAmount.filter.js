/* filter::formatAmount */
(function() {
	'use strict';

	angular.module('backendApp').filter('formatAmount', formatAmount);

	formatAmount.$inject = ['$rootScope'];
	function formatAmount($rootScope) {
		return function(amount, params_1, params_2) {//params_1 = usually it is int -  currencyId
			var paramsAmount = {
				amount: amount
			}
			if (typeof params_1 == 'object') {
				angular.merge(paramsAmount, params_1);					
			} else if (params_1){
				paramsAmount.currencyId = params_1;
			}
			
			if (params_2) {
				angular.merge(paramsAmount, params_2);
			}
			
			return $rootScope.formatAmount(paramsAmount);
		}
	}
})();