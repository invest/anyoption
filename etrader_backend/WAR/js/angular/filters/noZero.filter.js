/* filter::noZero */
(function() {
	'use strict';

	angular.module('backendApp').filter('noZero', noZero);

	noZero.$inject = [];
	function noZero() {
		return function(string) {
			return (string != 0) ? string : '';
		}
	}
})();