backendApp.controller('SalesDepositsCtr', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$rootScope.salesWriter = {};
	$rootScope.salesWriter.permissions = {};
	$rootScope.salesWriter.permissions.loaded = false;
	$rootScope.salesWriter.permissions.isManager = false;
	$rootScope.salesWriter.permissions.hasConversion = false;
	$rootScope.salesWriter.permissions.hasRetention = false;
	$rootScope.salesWriter.utcOffsetHours = 0;
	$rootScope.salesWriter.utcOffset = 'GMT+00:00';
	$rootScope.writerEmail = writerEmail;
	
	$scope.skins = [];
	
	//Conversion
	$scope.conversion = {};
	$scope.conversion.depositSummary = {};
	$scope.conversion.pages = [];
	$scope.conversion.currentPage = 1;
	$scope.conversion.maxPage = 1;
	$scope.conversion.totalCount = 0;
	
	$scope.conversion.search = {};
	$scope.conversion.search.resultsPerPage = 20;
	$scope.conversion.search.dateRangeDays = 32;
	$scope.conversion.search.userID = '';
	$scope.conversion.search.representative = '0';
	$scope.conversion.representatives = [];
	$scope.conversion.search.skin = {};
	
	$scope.conversion.search.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.conversion.openCalendar = function(e, date) {
        $scope.conversion.search.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	
	//Retention
	$scope.retention = {};
	$scope.retention.depositSummary = {};
	$scope.retention.pages = [];
	$scope.retention.currentPage = 1;
	$scope.retention.maxPage = 1;
	
	$scope.retention.search = {};
	$scope.retention.search.resultsPerPage = 20;
	$scope.retention.search.dateRangeDays = 32;
	$scope.retention.search.userID = '';
	$scope.retention.search.representative = '0';
	$scope.retention.representatives = [];
	$scope.retention.search.skin = {};
	
	$scope.retention.search.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.retention.openCalendar = function(e, date) {
        $scope.retention.search.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);
		
		$scope.startOfMonthDate = new Date();
		$scope.startOfMonthDate.setDate(1);
		
		//Conversion
		$scope.conversion.search.startDate = $scope.startOfMonthDate;
		$scope.conversion.search.endDate = $scope.currentDate;
		if($scope.conversion.search.endDate.getTime() - $scope.conversion.search.startDate.getTime() > ($scope.conversion.search.dateRangeDays - 1)*24*60*60*1000){
			$scope.conversion.search.startDate.setTime($scope.conversion.search.endDate.getTime() - ($scope.conversion.search.dateRangeDays - 1)*24*60*60*1000);
		}
		
		$scope.conversion.minDate = new Date();
		/*$scope.endDateWatch = $scope.$watch(function(){return $scope.conversion.search.endDate;}, function(){
			$scope.conversion.minDate.setTime($scope.conversion.search.endDate.getTime() - ($scope.conversion.search.dateRangeDays - 1)*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		});*/
		$scope.conversion.minDate.setTime($scope.conversion.search.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.conversion.maxDate = new Date();
		/*$scope.startDateWatch = $scope.$watch(function(){return $scope.conversion.search.startDate;}, function(){
			$scope.conversion.maxDate.setTime(Math.min($scope.conversion.search.startDate.getTime() + ($scope.conversion.search.dateRangeDays - 1)*24*60*60*1000 + $scope.hoursOffset*60*60*1000, new Date().getTime() + $scope.hoursOffset*60*60*1000));
		});*/
		$scope.conversion.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
		
		//Retention
		$scope.retention.search.startDate = $scope.startOfMonthDate;
		$scope.retention.search.endDate = $scope.currentDate;
		if($scope.retention.search.endDate.getTime() - $scope.retention.search.startDate.getTime() > ($scope.retention.search.dateRangeDays - 1)*24*60*60*1000){
			$scope.retention.search.startDate.setTime($scope.retention.search.endDate.getTime() - ($scope.retention.search.dateRangeDays - 1)*24*60*60*1000);
		}
		
		$scope.retention.minDate = new Date();
		/*$scope.endDateWatch = $scope.$watch(function(){return $scope.retention.search.endDate;}, function(){
			$scope.retention.minDate.setTime($scope.retention.search.endDate.getTime() - ($scope.retention.search.dateRangeDays - 1)*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		});*/
		$scope.retention.minDate.setTime($scope.retention.search.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.retention.maxDate = new Date();
		/*$scope.startDateWatch = $scope.$watch(function(){return $scope.retention.search.startDate;}, function(){
			$scope.retention.maxDate.setTime(Math.min($scope.retention.search.startDate.getTime() + ($scope.retention.search.dateRangeDays - 1)*24*60*60*1000 + $scope.hoursOffset*60*60*1000, new Date().getTime() + $scope.hoursOffset*60*60*1000));
		});*/
		$scope.retention.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	$scope.checkSubmitConversion = function(e){
		if(e.keyCode == 13){
			$scope.getDepositsConversion();
			e.stopPropagation();
			e.preventDefault();
			return false;
		}
	}
	
	$scope.checkSubmitRetention = function(e){
		if(e.keyCode == 13){
			$scope.getDepositsRetention();
			e.stopPropagation();
			e.preventDefault();
			return false;
		}
	}
	
	$scope.initWatch = $scope.$watch(function(){return $rootScope.ready;}, function(){
		if($rootScope.ready){
			$scope.initWatch();
			$scope.init();
		}
	});
	
	$rootScope.salesWriter.permissionsWatch = $scope.$watch(function(){
			return $rootScope.salesWriter.permissions.loaded;
		}, function(){
			if($rootScope.salesWriter.permissions.loaded){
				$rootScope.salesWriter.permissionsWatch();
				//Permissions loaded
				$scope.setTabs();
				if(!$scope.inTabs()){
					if($rootScope.salesWriter.permissions.hasConversion){
						$rootScope.$state.go('ln.sales_deposits.conversion', {ln: $scope.skinLanguage}, {location: 'replace'});
					}else if($rootScope.salesWriter.permissions.hasRetention){
						$rootScope.$state.go('ln.sales_deposits.retention', {ln: $scope.skinLanguage}, {location: 'replace'});
					}
				}
			}
		});
	
	
	$scope.init = function(){
		$scope.getWriter();
	}
	
	$scope.getWriter = function(){
		var MethodRequest = {};
		$http.post(settings.jsonLink + 'getWriter', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$rootScope.salesWriter.id = data.writerId;
				$rootScope.salesWriter.permissions.list = data.permissions;
				if($rootScope.salesWriter.permissions.list.indexOf('sales_deposit_rep_filter') > -1){
					$rootScope.salesWriter.permissions.isManager = true;
				}
				if($rootScope.salesWriter.permissions.list.indexOf('sales_deposit_deposits_conversion') > -1){
					$rootScope.salesWriter.permissions.hasConversion = true;
				}
				if($rootScope.salesWriter.permissions.list.indexOf('sales_deposit_deposits_retention') > -1){
					$rootScope.salesWriter.permissions.hasRetention = true;
				}
				
				$scope.skins.push({id: 0, name: $rootScope.getMsgs('all')});
				for(skin in skinMap){
					if(skinMap.hasOwnProperty(skin)){
						if(skinMap[skin].isActive){
							$scope.skins.push({id: skin, name: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
						}
					}
				}
				$scope.conversion.search.skin = $scope.skins[0];
				$scope.retention.search.skin = $scope.skins[0];
				
				$rootScope.salesWriter.permissions.loaded = true;
				if(data.offset != ''){
					try{
						$scope.writer.utcOffset = data.offset;
						var utcOffset = data.offset.split('+')[1].split(':');
						$scope.writer.utcOffsetHours = parseInt(utcOffset[0]);
					}catch(e){}
				}
				$scope.initDates();
				if($rootScope.salesWriter.permissions.hasConversion){
					$scope.getWriterConversionRepList();
					$scope.getDepositsConversion();
				}
				if($rootScope.salesWriter.permissions.hasRetention){
					$scope.getWriterRetentionRepList();
					$scope.getDepositsRetention();
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriter');
			})
			.finally(function() {
				$rootScope.salesWriter.permissions.loaded = true;
			})
	}
	
	$scope.getWriterConversionRepList = function(){
		var MethodRequest = {};
		$http.post(settings.jsonLink + 'getWriterConversionRepList', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.conversion.representatives.push({id: 0, name: $rootScope.getMsgs('All')});
				for(var i = 0; i < data.conversionReps.length; i++){
					$scope.conversion.representatives.push({id: data.conversionReps[i].repId, name: data.conversionReps[i].repName});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriterConversionRepList');
			})
	}
	
	$scope.getWriterRetentionRepList = function(){
		var MethodRequest = {};
		$http.post(settings.jsonLink + 'getWriterRetentionRepList', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.retention.representatives.push({id: 0, name: $rootScope.getMsgs('All')});
				for(var i = 0; i < data.retentionReps.length; i++){
					$scope.retention.representatives.push({id: data.retentionReps[i].repId, name: data.retentionReps[i].repName});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriterRetentionRepList');
			})
	}
	
	
	$scope.getDepositsConversion = function(page){
		//Prevent clicking on current page
		if(typeof page != 'undefined' && page == $scope.conversion.currentPage){
			return;
		}
		$scope.conversion.depositSummary = [];
		$scope.conversion.pages = [];
		//Call wasn't from page, set page to 1
		if(typeof page == 'undefined'){
			page = 1;
		}
		showLoading();
		var MethodRequest = {};
		MethodRequest.userId = ($scope.conversion.search.userID != '') ? $scope.conversion.search.userID : 0;
		MethodRequest.startDate = $scope.parseDateOnly({date: $scope.conversion.search.startDate, startOfDay: true});
		MethodRequest.endDate = $scope.parseDateOnly({date: $scope.conversion.search.endDate, endOfDay: true});
		if(!$rootScope.salesWriter.permissions.isManager){
			MethodRequest.writerId = $rootScope.salesWriter.id;
		}else{
			if($scope.conversion.search.representative != '0'){
				MethodRequest.writerId = $scope.conversion.search.representative;
			}
			if($scope.conversion.search.skin.id > 0){
				MethodRequest.skinId = $scope.conversion.search.skin.id;
			}
		}
		MethodRequest.pageNo = page;
		MethodRequest.resultsPerPage = $scope.conversion.search.resultsPerPage;
		$http.post(settings.jsonLink + 'getDepositsConversion', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.conversion.depositSummary = data.depositSummary;
				if ($scope.conversion.depositSummary.length == 0) {
					$scope.conversion.totalCount = 0;
				}
				for(var i = 0; i < $scope.conversion.depositSummary.length; i++){
					if (i == 0) {
						$scope.conversion.totalCount = $scope.conversion.depositSummary[i].totalCount;
					}
					$scope.conversion.depositSummary[i].transactionTime = $scope.formatDisplayDate(new Date($scope.conversion.depositSummary[i].transactionTime));
					$scope.conversion.depositSummary[i].depositAmountUSDFormatted = $rootScope.formatAmount({amount: $scope.conversion.depositSummary[i].depositAmountUSD, currencyId: 2, centsPart: 0});
					$scope.conversion.depositSummary[i].withdrawAmountUSDFormatted = $rootScope.formatAmount({amount: $scope.conversion.depositSummary[i].withdrawAmountUSD, currencyId: 2, centsPart: 0});
					$scope.conversion.depositSummary[i].depositSumAmountFormatted = $rootScope.formatAmount({amount: $scope.conversion.depositSummary[i].sumAllDeposits, currencyId: 2, centsPart: 0});
					$scope.conversion.depositSummary[i].withdrawAmountUSDFormatted = $rootScope.formatAmount({amount: $scope.conversion.depositSummary[i].sumAllWithdraws, currencyId: 2, centsPart: 0});
				}
				$scope.conversion.maxPage = data.pageCount;
				$scope.conversion.currentPage = page;
				$scope.conversion.pages = [];
				for(var i = Math.max(0, $scope.conversion.currentPage - 4); i < Math.min(data.pageCount, Math.max(0, $scope.conversion.currentPage - 4) + 10); i++){
					$scope.conversion.pages.push(i+1);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDepositsConversion');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.getDepositsRetention = function(page){
		//Prevent clicking on current page
		if(typeof page != 'undefined' && page == $scope.retention.currentPage){
			return;
		}
		$scope.retention.depositSummary = [];
		$scope.retention.pages = [];
		//Call wasn't from page, set page to 1
		if(typeof page == 'undefined'){
			page = 1;
		}
		showLoading();
		var MethodRequest = {};
		MethodRequest.userId = ($scope.retention.search.userID != '') ? $scope.retention.search.userID : 0;
		MethodRequest.startDate = $scope.parseDateOnly({date: $scope.retention.search.startDate, startOfDay: true});
		MethodRequest.endDate = $scope.parseDateOnly({date: $scope.retention.search.endDate, endOfDay: true});
		if(!$rootScope.salesWriter.permissions.isManager){
			MethodRequest.writerId = $rootScope.salesWriter.id;
		}else{
			if($scope.retention.search.representative != '0'){
				MethodRequest.writerId = $scope.retention.search.representative;
			}
			if($scope.retention.search.skin.id > 0){
				MethodRequest.skinId = $scope.retention.search.skin.id;
			}
		}
		MethodRequest.pageNo = page;
		MethodRequest.resultsPerPage = $scope.retention.search.resultsPerPage;
		$http.post(settings.jsonLink + 'getDepositsRetention', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.retention.depositSummary = data.depositSummary;
				for(var i = 0; i < $scope.retention.depositSummary.length; i++){
					$scope.retention.depositSummary[i].transactionTime = $scope.formatDisplayDate(new Date($scope.retention.depositSummary[i].transactionTime));
					$scope.retention.depositSummary[i].depositAmountUSDFormatted = $rootScope.formatAmount({amount: $scope.retention.depositSummary[i].depositAmountUSD, currencyId: 2, centsPart: 0});
					$scope.retention.depositSummary[i].withdrawAmountUSDFormatted = $rootScope.formatAmount({amount: $scope.retention.depositSummary[i].withdrawAmountUSD, currencyId: 2, centsPart: 0});
					$scope.retention.depositSummary[i].depositSumAmountFormatted = $rootScope.formatAmount({amount: $scope.retention.depositSummary[i].sumAllDeposits, currencyId: 2, centsPart: 0});
					$scope.retention.depositSummary[i].withdrawSumAmountFormatted = $rootScope.formatAmount({amount: $scope.retention.depositSummary[i].sumAllWithdraws, currencyId: 2, centsPart: 0});
				}
				$scope.retention.maxPage = data.pageCount;
				$scope.retention.currentPage = page;
				$scope.retention.pages = [];
				for(var i = Math.max(0, $scope.retention.currentPage - 4); i < Math.min(data.pageCount, Math.max(0, $scope.retention.currentPage - 4) + 10); i++){
					$scope.retention.pages.push(i+1);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDepositsRetention');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	
	$scope.exportConversion = function(){
		showLoading();
		var MethodRequest = {};
		MethodRequest.isExportToExcel = true;
		MethodRequest.userId = ($scope.conversion.search.userID != '') ? $scope.conversion.search.userID : 0;
		MethodRequest.startDate = $scope.parseDateOnly({date: $scope.conversion.search.startDate, startOfDay: true});
		MethodRequest.endDate = $scope.parseDateOnly({date: $scope.conversion.search.endDate, endOfDay: true});
		if(!$rootScope.salesWriter.permissions.isManager){
			MethodRequest.writerId = $rootScope.salesWriter.id;
		}else{
			if($scope.conversion.search.representative != '0'){
				MethodRequest.writerId = $scope.conversion.search.representative;
			}
		}
		MethodRequest.pageNo = $scope.conversion.currentPage;
		MethodRequest.resultsPerPage = $scope.conversion.search.resultsPerPage;
		$http.post(settings.jsonLink + 'getDepositsConversion', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.exportConversionSuccessful = true;
				$timeout(function(){$scope.exportConversionSuccessful = false;}, 3000);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'exportConversion')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.exportRetention = function(){
		showLoading();
		var MethodRequest = {};
		MethodRequest.isExportToExcel = true;
		MethodRequest.userId = ($scope.retention.search.userID != '') ? $scope.retention.search.userID : 0;
		MethodRequest.startDate = $scope.parseDateOnly({date: $scope.retention.search.startDate, startOfDay: true});
		MethodRequest.endDate = $scope.parseDateOnly({date: $scope.retention.search.endDate, endOfDay: true});
		if(!$rootScope.salesWriter.permissions.isManager){
			MethodRequest.writerId = $rootScope.salesWriter.id;
		}else{
			if($scope.retention.search.representative != '0'){
				MethodRequest.writerId = $scope.retention.search.representative;
			}
		}
		MethodRequest.pageNo = $scope.retention.currentPage;
		MethodRequest.resultsPerPage = $scope.retention.search.resultsPerPage;
		$http.post(settings.jsonLink + 'getDepositsRetention', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.exportRetentionSuccessful = true;
				$timeout(function(){$scope.exportRetentionSuccessful = false;}, 3000);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'exportRetention')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	
	$scope.previousPage = function(currentPage){
		return Math.max(currentPage - 1, 1);
	}
	$scope.nextPage = function(currentPage, maxPage){
		return Math.min(currentPage + 1, maxPage);
	}
	$scope.previousPageGroup = function(currentPage){
		return Math.max(currentPage - 10, 1);
	}
	$scope.nextPageGroup = function(currentPage, maxPage){
		return Math.min(currentPage + 10, maxPage);
	}
	
	
	
	$scope.formatDisplayDate = function(d){
		var date = new Date(d);
		date.setHours(date.getHours() + date.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours);
		return $scope.fillToTwoDigits(date.getHours()) + ':' + $scope.fillToTwoDigits(date.getMinutes()) + ' ' + $scope.fillToTwoDigits(date.getDate()) + '/' + $scope.fillToTwoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
	}
	
	$scope.parseDateOnly = function(params){
		var date = new Date(params.date);
		var hour = $scope.fillToTwoDigits(date.getHours());
		var minute = $scope.fillToTwoDigits(date.getMinutes());
		if(params.startOfDay){
			date.setHours(0/* + $rootScope.salesWriter.utcOffsetHours*/);
			hour = $scope.fillToTwoDigits(date.getHours());
			minute = '00';
		}else if(params.endOfDay){
			date.setHours(24/* + $rootScope.salesWriter.utcOffsetHours*/);
			hour = $scope.fillToTwoDigits(date.getHours());
			minute = '00';
		}
		return date.getFullYear() + '/' + $scope.fillToTwoDigits(date.getMonth() + 1) + '/' + $scope.fillToTwoDigits(date.getDate()) + ' ' + hour + ':' + minute + ' ' + $rootScope.salesWriter.utcOffset;
	}
	
	$scope.fillToTwoDigits = function(num){
		if(num >= 0 && num < 10){
			return '0' + num;
		}else{
			return num;
		}
	}
	
	$scope.inTabs = function(){
		var result = false;
		for(var i = 0; i < $scope.salesDepositsTabs.length; i++){
			if($rootScope.$state.includes($scope.salesDepositsTabs[i].link) && $scope.salesDepositsTabs[i].visible){
				result = true;
				break;
			}
		}
		return result;
	}
	
	$scope.setTabs = function(){
		$scope.salesDepositsTabs = [
    		{visible: $rootScope.salesWriter.permissions.hasConversion, link: 'ln.sales_deposits.conversion', name: $rootScope.getMsgs('sales.deposits.tabs.conversion')},
    		{visible: $rootScope.salesWriter.permissions.hasRetention, link: 'ln.sales_deposits.retention', name: $rootScope.getMsgs('sales.deposits.tabs.retention')}
    	];
	}
}]);