backendApp.controller('backendCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree) {
	if(window.currencySymbol){//because it might not be defined
		$rootScope.currencySymbol = currencySymbol;//why not :D
	}
	//Expose the static methods of the service to the template
	$scope.dateNow = new Date();
	$scope.MenuTree = {};
	$scope.MenuTree.toggleNode = MenuTree.InstanceClass.toggleNode;
	$rootScope.ready = false;
	$scope.loggedIn = false;
	$scope.folderPrefix = folderPrefix;
	
	$rootScope.readyStateCheck = $rootScope.$watch(function(){
			return !isUndefined($rootScope.$state.current.needLogin);
		}, function(){
			if(!isUndefined($rootScope.$state.current.needLogin)){
				$rootScope.readyStateCheck();
				$scope.initRequests();
			}
	});
	
	//get translation from key
	$rootScope.getMsgs = function(key, params) {
		if (isUndefined(key)) {
			return;
		}
		if (typeof key != 'object') {
			if ($scope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $scope.msgsParam($scope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
					// return $scope.msgsParam($scope.msgs[key], params);
				} else {
					return $scope.msgs[key];
				}
			} else if (params && params.returnKeyIfNoTranslation) {
				return key;
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key + "]?";
			}
		} else {
			if ($scope.msgs.hasOwnProperty(key[0])) {
				if ($scope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key[0]][key[1]], params));
					} else {
						return $scope.msgs[key[0]][key[1]];
					}
				} else {
					return params && params.hideMissing ? '' : "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return params && params.hideMissing ? '' : "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	$rootScope.isTranslated = function(str, checkEmpty){
		return str.indexOf('?msgs[') == -1 && (!checkEmpty ? true : str != '');
	}
	
	//get msg with param
	$scope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}
	
	$scope.msgs = {};
	var getMsgsJson_first = true;
	//get all translations (bundle)
	$scope.getMsgsJson = function() {
		$scope.initRequestStatus.getMsgsJson = false;
		//skinMap[skinMap[settings.skinId].skinIdTexts].locale seems legit :D
		var date = new Date();
		var timestamp = (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + '-' + date.getUTCFullYear();
		var msgsUrl = settings.jsonImagegLink + settings.msgsPath + settings.msgsFileName + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ((getMsgsJson_first)?"":settings.msgsEUsufix) + settings.msgsExtension + '?' + timestamp;
		$http.get(msgsUrl)
			.then(function(data) {
				if (typeof data == 'object') {
					if (getMsgsJson_first) {
						$scope.msgs = data;
					} else {
						$scope.msgs = jsonConcat($scope.msgs, data);
						if (!settings.loggedIn) {
							$scope.initRequestStatus.getMsgsJson = true;
						}
					}
					if (settings.isRegulated && getMsgsJson_first) {
						getMsgsJson_first = false;
						$scope.getMsgsJson();
					} else {
						$scope.initRequestStatus.getMsgsJson = true;
					}
					
					if (!isUndefined($rootScope.$state.current.metadataKey)) {
						$rootScope.metadataTitle = $rootScope.$state.current.metadataKey;
					} else {
						$rootScope.metadataTitle = 'index';
					}
				}
			})
			.catch(function( data, status ) {
				handleNetworkError(data);
			});
	}
	
	$scope.getSettings = function(){
		$scope.initRequestStatus.getSettings = false;
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'WriterPermissionsServices/getSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.initRequestStatus.getSettings = true;
				$scope.loggedIn = true;
				
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				$rootScope.writer = data.writer;
				try{
					var utcOffset = $rootScope.writer.utcOffset.split('+')[1].split(':');
					$rootScope.writer.utcOffsetHours = $scope.writer.utcOffsetHours = parseInt(utcOffset[0]);
				}catch(e){}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSettings');
			})
	}
	
	
	//Init writer
	$rootScope.writer = {};
	$rootScope.writer.utcOffsetHours = 3;
	$rootScope.writer.utcOffset = 'GMT+03:00';
	
	//Init user
	$scope.userId = 0;
	if(window.userId){
		$scope.userId = parseInt(userId);
	}
	if(window.userCurrency){
		$scope.userCurrency = userCurrency;
	}
	$scope.getUserCurrency = function(){
		return $scope.userCurrency;
	}
	
	
	$scope.checkInitStatus = function() {
		for (var key in $scope.initRequestStatus) {
			if (!$scope.initRequestStatus[key]) {
				$timeout(function() {
					$scope.checkInitStatus();
				}, 50);
				return;
			}
		}
		$rootScope.ready = $scope.ready = true;
		if(window.menu){
			$rootScope.menu = $rootScope.getMenu();
		}
		$rootScope.$apply();
	}
	
	$scope.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
	$scope.initRequestStatus = {}
	$scope.initRequests = function() {
		$rootScope.ready = false;
		$scope.getMsgsJson();
		if ($rootScope.$state.current.needLogin > 1) {
			$scope.getSettings();
		}
		$scope.checkInitStatus();
	}
	
	$scope.skinLanguage = 'en';
	$rootScope.permissions = new Permissions.InstanceClass(permissions);
	//$scope.initRequests();
	
	$rootScope.getSkinsTree = function(){
		if(!$rootScope.skinsTree){
			$rootScope.skinsTree = [{
				title: $rootScope.getMsgs('general.all'),
				nodes: [
					{
						title: 'AOPS',
						nodes: []
					},{
						title: 'Ouroboros',
						nodes: []
					},{
						title: 'Goldbeam',
						nodes: []
					},{
						title: 'Etrader',
						nodes: []
					}
				]
			}];
			for(skin in skinMap){
				if(skinMap.hasOwnProperty(skin)){
					if(!skinMap[skin].isActive){
						continue;
					}
					if(skin == 1){
						$rootScope.skinsTree[0].nodes[3].nodes.push({id: skin, title: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
					}else if(skinMap[skin].isGoldbeam){
						$rootScope.skinsTree[0].nodes[2].nodes.push({id: skin, title: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
					}else if(skinMap[skin].isRgulated){
						$rootScope.skinsTree[0].nodes[1].nodes.push({id: skin, title: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
					}else{
						$rootScope.skinsTree[0].nodes[0].nodes.push({id: skin, title: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
					}
				}
			}
		}
		return $rootScope.skinsTree;
	}
	
	$rootScope.getMenu = function(){
		if(!$rootScope.menu){
			$rootScope.menu = new MenuTree.InstanceClass(menu, false, $rootScope.permissions);
			$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
		}
		return $rootScope.menu;
	}
	
	//Dependencies functions
	$rootScope.addDependency = function(scope, dependency, callback){
		if(!checkOwnProperty(scope, 'dependencies')){
			scope.dependencies = {};
		}
		scope.dependencies[dependency.name] = dependency;
		scope.dependencies[dependency.name].callback = callback;
	}
	
	$rootScope.markDependencyDone = function(scope, dependencyName, dependencyItem){
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName] && scope.dependencies[dependencyName].dependencies){
			scope.dependencies[dependencyName].dependencies[dependencyItem] = true;
		}
		if(checkOwnProperty(scope, 'dependencies') && scope.dependencies[dependencyName]){
			var isReady = true;
			for(var dependencyItem in scope.dependencies[dependencyName].dependencies){
				if(scope.dependencies[dependencyName].dependencies.hasOwnProperty(dependencyItem)){
					if(!scope.dependencies[dependencyName].dependencies[dependencyItem]){
						isReady = false;
					}
				}
			}
			if(isReady){
				$timeout(function(){scope.dependencies[dependencyName].callback();}, 0);
			}
		}
	}
	
	//Init screen controllers
	$scope.initScreenCtr = function(scope, dependencies, initCallback){
		scope.ready = false;
		scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if(scope.ready){
				if(scope.checkAction){
					scope.checkAction();
				}
			}
		});
		scope.mainInitWatch = scope.$watch(function(){return $rootScope.ready;}, function(){
			if($rootScope.ready){
				scope.mainInitWatch();
				scope.init();
			}
		});
		scope.init = function(){
			showLoading();
			scope.localErrorMsgs = [];
			$rootScope.addDependency(scope, dependencies, function(){scope.initDone();});
			if(scope.initFiltersOnReady){
				scope.initFiltersOnReady();
			}
			initCallback();
		}
		scope.initDone = function(){
			scope.ready = true;
			hideLoading();
			if(scope.checkAction){
				scope.checkAction();
			}
		}
		scope.showLoading = Utils.showLoading;
		scope.hideLoading = Utils.hideLoading;
		
		scope.dateManager = new Utils.DateManager();
	}
	
	//Common screen controllers functions
	$rootScope.confirm = function(message, successCallback, cancelCallback, buttonTexts){
		$rootScope.confirmMessage = message;
		$rootScope.buttonTexts = buttonTexts;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/confirm-popup.html',
			scope: $scope,
			windowClass: 'confirm-popup'
		});
		previewModal.result.then(function () {
			if(successCallback){
				successCallback();
			}
		}, function () {
			if(cancelCallback){
				cancelCallback();
			}
		});
	}
	$rootScope.translateJsonKeyInArray = function(arr, key){
		if(arr && Array.isArray(arr) && key){
			for(var i = 0; i < arr.length; i++){
				if(arr[i][key]){
					arr[i][key] = $rootScope.getMsgs(arr[i][key]);
				}
			}
		}
		return arr;
	}
	$rootScope.fillFilter = function(params){
		if(params.dontPreserveFirstEl){
			params.arr = [];
		}else{
			params.arr.splice(1, params.arr.length);
		}
		var holder = null;
		if(params.data){
			holder = params.data.screenFilters;
		}
		if(params.holder){
			holder = params.holder;
		}
		if(holder && holder[params.filter]){
			for(var el in holder[params.filter]){
				if(holder[params.filter].hasOwnProperty(el)){
					var name = holder[params.filter][el];
					if(params.translate){
						name = $rootScope.getMsgs(name);
					}
					params.arr.push({id: el, name: name});
				}
			}
		}
	}
	$rootScope.getFilterElById = function(arr, value){
		return (typeof value != "undefined" && value != null) ? arr[searchJsonKeyInArray(arr, 'id', value)] : null;
	}
	
	$rootScope.sortFilter = function(filter){
		if(!Array.isArray(filter)){
			return filter.id == -1 ? '' : filter.name.trim();
		}else{
			return $filter('orderBy')(filter, $rootScope.sortFilter);
		}
	}
	
	
	$rootScope.globalErrorMsg = '';
	$scope.handleErrors = $rootScope.handleErrors = function(data, name, holder) {//returns true on error
		$scope.requestsStatus[data.serviceName] = 1;
		if (data.errorCode <= -10000 && data.errorCode != 0) {
			showToast(2, data.errorMessages[0].message);
			return true;
		} else if (data.errorCode > errorCodeMap.success) {//0
			var defMsg = $rootScope.getMsgs('error-' + data.errorCode);
			logIt({'type':3,'msg':name + " (" + defMsg + ")"});
			logIt({'type':3,'msg':data});
			
			$rootScope.resetGlobalErrorMsg(holder);
			if (data.errorCode == errorCodeMap.session_expired) { //session expired 6999
				if (isUndefined(data.loginPage)) {
					try {
						/*$rootScope.changeHeader(false);
						$rootScope.$state.go('ln.login', {ln: settings.skinLanguage});
						$rootScope.initSettings();*/
						window.location.hash = '';
						window.location.replace(window.location.pathname.replace(/index.html/, '').replace(/previewer.html/, ''));
					} catch(e) {}
				}
			} else {
				$rootScope.resetErrorMsgs(data);
				if (data.userMessages != null) {
					for (var i = 0; i < data.userMessages.length; i++) {
						if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
							$rootScope.addGlobalErrorMsg(data.userMessages[i].message, holder);
							showToast(2, data.userMessages[i].message);
						} else {
							var input = g(data.userMessages[i].field);
							if (input != null) {
								input.className += " error";
							}
							var error_el_id = data.userMessages[i].field + '-error';
							error_stack.push(data.userMessages[i].field);
							var err_place = g(error_el_id);
							if (err_place != null) {
								err_place.innerHTML = data.userMessages[i].message;
								$(err_place).addClass('error');
							} else {
								var field_place = g(data.userMessages[i].field + '-container');
								if (field_place != null) {
									var el = document.createElement('span');
									el.id = error_el_id;
									el.className = 'error-message icon';
									el.innerHTML = data.userMessages[i].message;
									field_place.appendChild(el);
								} else {
									$rootScope.addGlobalErrorMsg(data.userMessages[i].message, holder);
								}
							}
						}
					}
				}else{
					var autoTranslationFound = false;
					for(key in errorCodeMap){
						if(errorCodeMap.hasOwnProperty(key)){
							if(errorCodeMap[key] == data.errorCode){
								autoTranslationFound = true;
								if($rootScope.isTranslated($rootScope.getMsgs('error.' + key), true)){
									$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.' + key), holder);
									showToast(2, $rootScope.getMsgs('error.' + key));
								}
								break;
							}
						}
					}
					
					if(!autoTranslationFound) {
						logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
						$rootScope.addGlobalErrorMsg(defMsg, holder);
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	$rootScope.resetErrorMsgs = function(data) {
		if (typeof data.stopClear == 'undefined' || !data.stopClear) {
			for (var i = 0; i < error_stack.length; i++) {
				var error_holder = g(error_stack[i]);
				if (error_holder != null) {
					g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
				}
				var error_holder = g(error_stack[i] + '-error');
				if (error_holder != null) {
					$('#' + error_stack[i] + '-error').html('');
					$('#' + error_stack[i] + '-error').removeClass('error');
				}
			}
			error_stack = [];
		}
	}
	
	$rootScope.addGlobalErrorMsg = function(data, holder){
		if(holder){
			holder.push(data);
		}else{
			$rootScope.globalErrorMsg += '<span class="error">' + data + '</span>';
		}
	}
	
	$rootScope.resetGlobalErrorMsg = function(holder){
		if(holder){
			holder.length = 0;
		}else{
			$rootScope.globalErrorMsg = '';
		}
	}
	
	
	$rootScope.formatAmountForDb = function(params){
		var amount = params.amount;
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
					amount = amount*100;
				}
			}
		}catch(e){}
		return amount;
	}
	
	$rootScope.formatAmount = function(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
		var amount = params.amount;
		var currency = settings.currencies['2'];//USD
		if(params.currencyId && settings.currencies[params.currencyId]){
			currency = settings.currencies[params.currencyId];
		}
		var centsPart = params.centsPart;
		var withoutDecimalPoint = params.withoutDecimalPoint;
		var round = params.round;
		var skipAmountDivide = params.skipAmountDivide;
		
		if(params.amountOnly){
			try{
				if (!isNaN(amount)) {
					if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
						amount = amount/100;
					}
				}
			}catch(e){}
			return amount;
		}
		try{
			if (!isNaN(amount)) {
				if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100 && !skipAmountDivide){
					amount = amount/100;
				}
				var rtn = '';
				if (amount.toString().charAt(0) == '-') {
					rtn = '-';
					amount *= -1;
				}
				if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
					amount = amountToFloat(amount);
				}
				if (typeof round == 'undefined' || !round || amount < 1000 ) {
					amount = Math.round(amount*100)/100;
					var tmp = amount.toString().split('.');
					var decimal = parseInt(tmp[0]);
					var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
					if((cents === '') || (cents === 0)){cents = '00';}
					else if(cents.length == '1'){cents = cents + '0';}
					else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
					
					if(currency.currencyLeftSymbol){
						rtn += $rootScope.getMsgs(currency.currencySymbol);
					}
					rtn += numberWithCommas(decimal);
					if(currency.decimalPointDigits == 0){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
						rtn += "<span>"+cents+"</span>";
					}
					else if((centsPart == 3) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else if((centsPart == 2) && (parseInt(cents) == 0)){
						//Absolutly nothing. Just chill!
					}
					else{
						rtn += "."+cents;
					}
					if(!currency.currencyLeftSymbol){
						rtn += $rootScope.getMsgs(currency.currencySymbol);
					}
				} else {
					if(currency.currencyLeftSymbol){
						rtn += $rootScope.getMsgs(currency.currencySymbol);
					}
					rtn += (Math.round(amount / 100)) / 10 + "K";
					if(!currency.currencyLeftSymbol){
						rtn += $rootScope.getMsgs(currency.currencySymbol);
					}
				}
				return rtn;
			} else {
				return ' ';
			}
		} catch(e) {
			logIt({'type':3,'msg':e});
		}
	}
	
	
	
	
	//Date functions
	$rootScope.dateFormatDisplay = function(d){
		var date = new Date(d);
		date.setHours(date.getHours() + date.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes()) + ' ' + $rootScope.dateFillToTwoDigits(date.getDate()) + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
	}
	
	$rootScope.dateParse = function(params){
		var date = new Date(params.date);
		var hour = $rootScope.dateFillToTwoDigits(date.getHours());
		var minute = $rootScope.dateFillToTwoDigits(date.getMinutes());
		if(params.startOfDay){
			date.setHours(0/* + $rootScope.writer.utcOffsetHours*/);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			hour = $rootScope.dateFillToTwoDigits(date.getHours());
			minute = '00';
		}else if(params.endOfDay){
			date.setHours(24/* + $rootScope.writer.utcOffsetHours*/);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			hour = $rootScope.dateFillToTwoDigits(date.getHours());
			minute = '00';
		}
		if(params.timestamp){
			return date.getTime();
		}
		return date.getFullYear() + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + $rootScope.dateFillToTwoDigits(date.getDate()) + ' ' + hour + ':' + minute + ' ' + $rootScope.writer.utcOffset;
	}
	
	$rootScope.dateFillToTwoDigits = function(num){
		if(num >= 0 && num < 10){
			return '0' + num;
		}else{
			return num;
		}
	}
	
	$rootScope.dateGetSimple = function(d){
		if(!d){
			return '';
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getDate()) + '/' + $rootScope.dateFillToTwoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
	}
	
	$rootScope.timeGetSimple = function(d){
		if(!d){
			return '';
		}
		var date = new Date(d);
		return $rootScope.dateFillToTwoDigits(date.getHours()) + ':' + $rootScope.dateFillToTwoDigits(date.getMinutes());
	}
	
	
	$rootScope.getUserStripUrl = function(userId){
		return settings.jsonImagegLink + 'jsp/NavigationUserStrip.jsf?screen=support&userId=' + userId;
	}
	
	
	//File init
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
	
}]);