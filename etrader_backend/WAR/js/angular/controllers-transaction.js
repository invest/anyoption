backendApp.controller('TransactionCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	
}]);

backendApp.controller('TransactionsCtr', ['$rootScope', '$scope', '$http', '$state', 'SingleSelect', 'Multiselect', 'Pagination', '$timeout', 'utilsService', function($rootScope, $scope, $http, $state, SingleSelect, Multiselect, Pagination, $timeout, utilsService) {
	$scope.USERS_PRIVATE_COMPANY = 12;
	$scope.TRANSATION_CLASS_TYPES_REAL_DEPOSITS = 1;
	$scope.TRANSATION_CLASS_TYPES_REAL_WITHDRAWALS = 2;
	$scope.TRANSATION_STATUSES_SUCCEED = 2;
	$scope.TRANSATION_STATUSES_PENDING = 7;
	$scope.SYSTEM_WRITERS_BACKEND = 987;

	$scope.TRANSACTION_IS_DEPOSIT = 1;
	$scope.TRANSACTION_IS_WITHDRAW = 2;
	
	$scope.TRANS_STATUS_PENDING = 7;

	$scope.TRANSACTION_TYPE_CC_DEPOSIT = 1;
	$scope.TRANSACTION_TYPE_INTERNAL_CREDIT = 16;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	$scope.filter.startDate = 0;
	$scope.filter.endDate = 0;
	$scope.filter.searchBy = 1;
	$scope.filter.transactionId = '';
	$scope.filter.userId = '';
	$scope.filter.referenceId = '';
	$scope.filter.skinBusinessCases = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: false});
	$scope.filter.skins = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.countries = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true, iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}});
	$scope.filter.currencies = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.transactionTypes = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.transactionStatuses = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	
	$scope.filter.transactionClassTypes = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.systemWriters = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, isSmart: false, noSort: true});
	$scope.filter.clearingProviders = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, isSmart: false});
	$scope.filter.creditCardCountries = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true, iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}});
	
	$scope.filter.receiptNumber = '';
	$scope.filter.ccBin = '';
	$scope.filter.ccLast4Digits = '';
	
	$scope.transactions = null;
	$scope.summary = {};
	
	$scope.filter.amountUSDFrom = '';
	$scope.filter.amountUSDTo = '';

	$scope.isReferenceIdView = false;

	$scope.changePage = function(page){
		$scope.getAll(null, page);
	}
	
	$scope.filter.resultsPerPage = new SingleSelect.InstanceClass({sortById: true});
	$scope.filter.resultsPerPage.fillOptions(utilsService.resultsPerPage);
	$scope.filter.resultsPerPage.setModel($scope.filter.resultsPerPage.getOptionById(15));
	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.selectedFilter = null;
	
	$scope.filterCollapsed = false;
	
	
	$scope.filter.dateRangeDays = 32;
	$scope.filter.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.openCalendar = function(e, date) {
		$scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);
		
		$scope.defaultStartDate = new Date();
		$scope.defaultStartDate.setDate($scope.defaultStartDate.getDate() - 1);
		
		$scope.filter.startDate = $scope.defaultStartDate;
		$scope.filter.endDate = $scope.currentDate;
		if($scope.filter.endDate.getTime() - $scope.filter.startDate.getTime() > ($scope.filter.dateRangeDays - 1)*24*60*60*1000){
			$scope.filter.startDate.setTime($scope.filter.endDate.getTime() - ($scope.filter.dateRangeDays - 1)*24*60*60*1000);
		}
		
		$scope.filter.minDate = new Date();
		$scope.filter.minDate.setTime($scope.filter.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.filter.maxDate = new Date();
		$scope.filter.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	$scope.initDates();
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	$scope.filterToggled = function(toggleName, isOn){
		$scope.filterCollapsed = !isOn;
	}
	
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.classUsers = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.classUser = $scope.filter.classUsers[0];
		$scope.filter.campaigns = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.campaign = $scope.filter.campaigns[0];
		$scope.filter.hasBalanceFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.hasBalance = $scope.filter.hasBalanceFilter[0];
		$scope.filter.gatewayFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.gateway = $scope.filter.gatewayFilter[0];
		$scope.filter.j4postponedFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.j4postponed = $scope.filter.j4postponedFilter[0];
		$scope.filter.salesTypesFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.salesTypes = $scope.filter.salesTypesFilter[0];
		$scope.filter.depositSourceFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.depositSource = $scope.filter.depositSourceFilter[0];
		$scope.filter.wasReroutedFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.wasRerouted = $scope.filter.wasReroutedFilter[0];
		$scope.filter.isThreeDFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.isThreeD = $scope.filter.isThreeDFilter[0];
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getTransactionScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getTransactionScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getTransactionScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'TransactionServices/getTransactionScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					$rootScope.fillFilter({arr: $scope.filter.classUsers, data: data, filter: 'classUsers', translate: true});
					for(var i = 0; i < $scope.filter.classUsers.length; i++){
						$scope.filter.classUsers[i].name = $scope.filter.classUsers[i].name.replace(/\\\\/, '\\');
						if($scope.filter.classUsers[i].id == $scope.USERS_PRIVATE_COMPANY){
							$scope.filter.classUser = $scope.filter.classUsers[i];
						}
					}
					$scope.filter.skinBusinessCases.fillOptions(data.screenFilters.skinBusinessCases);
					$scope.filter.skins.fillOptions(data.screenFilters.skins);
					$scope.filter.countries.fillOptions(data.screenFilters.countries);
					$scope.filter.currencies.fillOptions(data.screenFilters.currencies);
					$rootScope.fillFilter({arr: $scope.filter.campaigns, data: data, filter: 'campaigns'});
					
					$scope.filter.transactionClassTypes.fillOptions(data.screenFilters.transactionClassTypes);
					$scope.filter.transactionClassTypes.setSelectedOptions([$scope.TRANSATION_CLASS_TYPES_REAL_DEPOSITS, $scope.TRANSATION_CLASS_TYPES_REAL_WITHDRAWALS]);
					$scope.filter.transactionTypes.fillOptions(data.screenFilters.transactionTypes);
					$scope.filter.transactionTypes.setFilterCallback(function(option){
						if($scope.filter.transactionClassTypes.getCheckedIds().length == 0 || $scope.filter.transactionClassTypes.getCheckedIds().indexOf(data.screenFilters.transactionsAndClassTypeTablesMapping[option.id]) !== -1){
							return true;
						}else{
							return false;
						}
					});
					$scope.filter.creditCardCountries.fillOptions(data.screenFilters.countries);
					
					$scope.filter.transactionStatuses.fillOptions(data.screenFilters.transactionStatuses);
					$scope.filter.transactionStatuses.setFilterCallback(function(option){
						if($scope.filter.transactionClassTypes.getCheckedIds().length == 0 || 
							$scope.filter.transactionClassTypes.getCheckedIds().indexOf(data.screenFilters.statusClassMapping[option.id]) !== -1 ||
							option.id == 2 ||//always show success
							(($scope.filter.transactionClassTypes.getCheckedIds().indexOf('1') !== -1 || 
								$scope.filter.transactionClassTypes.getCheckedIds().indexOf('2') !== -1 || 
								$scope.filter.transactionClassTypes.getCheckedIds().indexOf('3') !== -1
							) && option.id == 3)
						){
							return true;
						}else{
							return false;
						}
					});
					$scope.filter.transactionStatuses.setSelectedOptions([$scope.TRANSATION_STATUSES_SUCCEED, $scope.TRANSATION_STATUSES_PENDING]);
					$scope.filter.systemWriters.fillOptions(data.screenFilters.systemWriters);
					$rootScope.fillFilter({arr: $scope.filter.hasBalanceFilter, data: data, filter: 'hasBalance'});
					$rootScope.fillFilter({arr: $scope.filter.isThreeDFilter, data: data, filter: 'isThreeD'});
					
					$rootScope.fillFilter({arr: $scope.filter.gatewayFilter, data: data, filter: 'gateway'});
					$scope.filter.clearingProviders.fillOptions(data.screenFilters.clearingProviders);
					$rootScope.fillFilter({arr: $scope.filter.j4postponedFilter, data: data, filter: 'j4postponed'});
					$rootScope.fillFilter({arr: $scope.filter.salesTypesFilter, data: data, filter: 'salesTypes', translate: true});
					$rootScope.fillFilter({arr: $scope.filter.depositSourceFilter, data: data, filter: 'depositSource', translate: true});
					$rootScope.fillFilter({arr: $scope.filter.wasReroutedFilter, data: data, filter: 'wasRerouted'});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getTransactionScreenSettings');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getTransactionScreenSettings');
			})
	}
	
	var _formCopy;
	$scope.getAll = function(_form, page, trnIds){
		_formCopy = _form;
		$scope.isReferenceIdView = false;
		var paging = false;
		if (!isUndefined(_form)) {
			$scope._form = _form;
		} else {
			_form = $scope._form;
			paging = true;
		}
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		if (_form.$valid || paging) {
			$scope.currentPage = page;
			$rootScope.resetGlobalErrorMsg();
			showLoading();
			
			var methodRequest = {
				page: page,
				rowsPerPage: $scope.filter.resultsPerPage.getId(),
				from: $rootScope.dateParse({
					date: $scope.filter.startDate, 
					startOfDay: true, 
					timestamp: true
				}),
				to: $rootScope.dateParse({
					date: $scope.filter.endDate,
					endOfDay: true,
					timestamp: true
				}),
				searchBy: $scope.filter.searchBy
			};
			
			$scope.filter.transactionId != '' ? methodRequest.transactionId = $scope.filter.transactionId : '';
			$scope.filter.userId != '' ? methodRequest.userId = $scope.filter.userId : '';
			$scope.filter.referenceId != '' ? methodRequest.reference = $scope.filter.referenceId : '';
			
			if(!$scope.filterCollapsed){
				$scope.filter.classUser.id >= 0 ? methodRequest.userClasses = $scope.filter.classUser.id : '';
				$scope.filter.skinBusinessCases.getCheckedIds().length > 0 ? methodRequest.businessCases = $scope.filter.skinBusinessCases.getCheckedIds() : '';
				$scope.filter.skins.getCheckedIds().length > 0 ? methodRequest.skins = $scope.filter.skins.getCheckedIds() : '';
				$scope.filter.countries.getCheckedIds().length > 0 ? methodRequest.countries = $scope.filter.countries.getCheckedIds() : '';
				$scope.filter.currencies.getCheckedIds().length > 0 ? methodRequest.currencies = $scope.filter.currencies.getCheckedIds() : '';
				$scope.filter.campaign.id >= 0 ? methodRequest.campaign = $scope.filter.campaign.id : '';
				$scope.filter.transactionClassTypes.getCheckedIds().length > 0 ? methodRequest.transactionClassTypes = $scope.filter.transactionClassTypes.getCheckedIds() : '';
				$scope.filter.transactionTypes.getCheckedIds().length > 0 ? methodRequest.transactionTypes = $scope.filter.transactionTypes.getCheckedIds() : '';
				$scope.filter.transactionStatuses.getCheckedIds().length > 0 ? methodRequest.statuses = $scope.filter.transactionStatuses.getCheckedIds() : '';
				
				$scope.filter.systemWriters.getCheckedIds().length > 0 ? methodRequest.systemWriters = $scope.filter.systemWriters.getCheckedIds() : '';
				if(methodRequest.systemWriters){
					for(var i = 0; i < methodRequest.systemWriters.length; i++){
						if(methodRequest.systemWriters[i] == $scope.SYSTEM_WRITERS_BACKEND){
							methodRequest.isIndividualWritersNeedToBeInclude = 1;
							methodRequest.systemWriters.splice(i, 1);
						}
					}
					if(methodRequest.systemWriters.length == 0){
						methodRequest.systemWriters = null;
					}
				}
				
				$scope.filter.hasBalance.id >= 0 ? methodRequest.hasBalance = $scope.filter.hasBalance.id : '';
				$scope.filter.receiptNumber != '' ? methodRequest.receiptNumber = $scope.filter.receiptNumber : '';
				$scope.filter.gateway.id >= 0 ? methodRequest.gateway = $scope.filter.gateway.id : '';
				$scope.filter.clearingProviders.getCheckedIds().length > 0 ? methodRequest.ccProviders = $scope.filter.clearingProviders.getCheckedIds() : '';
				$scope.filter.ccBin != '' ? methodRequest.ccBin = $scope.filter.ccBin : '';
				$scope.filter.ccLast4Digits != '' ? methodRequest.ccLast4Digits = $scope.filter.ccLast4Digits : '';
				$scope.filter.j4postponed.id >= 0 ? methodRequest.J4postponed = $scope.filter.j4postponed.id : '';
				$scope.filter.salesTypes.id >= 0 ? methodRequest.salesTypes = $scope.filter.salesTypes.id : '';
				$scope.filter.depositSource.id >= 0 ? methodRequest.depositSource = $scope.filter.depositSource.id : '';
				$scope.filter.wasRerouted.id >= 0 ? methodRequest.wasRerouted = $scope.filter.wasRerouted.id : '';
				$scope.filter.amountUSDFrom != '' ? methodRequest.amountFrom = ($scope.filter.amountUSDFrom * 100) : '';
				$scope.filter.amountUSDTo != '' ? methodRequest.amountTo = ($scope.filter.amountUSDTo * 100) : '';
				$scope.filter.isThreeD.id >= 0 ? methodRequest.isThreeD = $scope.filter.isThreeD.id : '';
				$scope.filter.creditCardCountries.getCheckedIds().length > 0 ? methodRequest.creditCardCountries = $scope.filter.creditCardCountries.getCheckedIds() : '';
			}
			
			if (paging) {
				methodRequest = $scope.lastSearch;
				methodRequest.page = $scope.currentPage;
			} else {
				$scope.lastSearch = methodRequest;
			}
			methodRequest.trnIds = (trnIds) ? trnIds : null;
			
			$http.post(settings.backendJsonLink + 'TransactionServices/getAll', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.transactions = data.transactions;
					//Add j4 postpone dates
					for(var i = 0; i < $scope.transactions.length; i++){
						$scope.transactions[i].creditAmountDsp = $rootScope.formatAmount({amount: $scope.transactions[i].creditAmount, centsPart: 0, currencyId: $scope.transactions[i].currencyId});  
						$scope.calculateJ4Postpone($scope.transactions[i]);
					}
					
					$scope.summary.pageTotalDeposits = data.pageTotalDeposits;
					$scope.summary.pageTotalWithdraws = data.pageTotalWithdraws;
					$scope.summary.totalCountDeposits = data.totalCountDeposits;
					$scope.summary.totalCountWithdraws = data.totalCountWithdraws;
					$scope.summary.totalSumDeposits = data.totalSumDeposits;
					$scope.summary.totalSumWithdraws = data.totalSumWithdraws;
					$scope.pagination.setPages(page, data.totalCount, $scope.filter.resultsPerPage.getId());
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'getAll')
				})
				.finally(function() {
					hideLoading();
				})
		} else {
			showToast(2, $rootScope.getMsgs('error.general_invalid_form'));
		}
	}
	
	$scope.calculateJ4Postpone = function(transaction) {
		var postponeIndex = 0;
		var postponeDates = [];
		var j = 0;
		var currentDate = new Date();
		var daysAddition = 0;
		if(currentDate.getHours() == 23){
			daysAddition = 1;
		}
		for(j; j < 3; j++){
			var d = new Date(transaction.timeCreated);
			d.setDate(d.getDate() + j + daysAddition + 1);
			d.setHours(23);
			d.setMinutes(0);
			postponeDates.push({name: $rootScope.dateGetSimple(d), numberOfDays: j, value: d});
			if(transaction.transactionPostponed.numberOfDays == j){
				postponeIndex = j;
			}
		}
		transaction.postponeDates = postponeDates;
		transaction.postponeDate = transaction.postponeDates[postponeIndex];
		//it is ugly, i know, ask Tabakov :D
		//changes for direct24, EPS, Giropay
		if (transaction.paymentTypeId > 0) {
			transaction.typeIdView = '-' + transaction.typeId + transaction.paymentTypeId;  
		} else {
			transaction.typeIdView = transaction.typeId;
		}
		return transaction;
	}
	
	$scope.cancelDeposit = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		$rootScope.confirm($rootScope.getMsgs('confirm.transactions-cancel'), function(){
			showLoading();
			var methodRequest = {
				trnIds: [transaction.id],
				transaction: transaction
			}
			
			$http.post(settings.backendJsonLink + 'TransactionServices/cancelDeposit', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					var arrayIndex = searchJsonKeyInArray($scope.transactions, 'id', transaction.id);
					$scope.transactions[arrayIndex] = $scope.calculateJ4Postpone(data.transactions[0]);
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.cancelled-deposit'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'cancelDeposit')
				})
				.finally(function() {
					hideLoading();
				})
		}, null, {ok: $rootScope.getMsgs('yes'), cancel: $rootScope.getMsgs('no')});
	}
	$scope.cancelMaintenanceFee = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		$rootScope.confirm($rootScope.getMsgs('confirm.transactions-cancel-maintenance-fee'), function(){
			showLoading();
			var methodRequest = {
				trnIds: [transaction.id],
				transaction: transaction
			}
			$http.post(settings.backendJsonLink + 'TransactionServices/cancelMaintenanceFee', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					var arrayIndex = searchJsonKeyInArray($scope.transactions, 'id', transaction.id);
					$scope.transactions[arrayIndex] = $scope.calculateJ4Postpone(data.transactions[0]);
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.cancelled-maintenance-fee'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'cancelMaintenanceFee')
				})
				.finally(function() {
					hideLoading();
				})
		}, null, {ok: $rootScope.getMsgs('yes'), cancel: $rootScope.getMsgs('no')});
	}
	$scope.cancelDepositFraud = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		$rootScope.confirm($rootScope.getMsgs('confirm.transactions-fraud'), function(){
			showLoading();
			var methodRequest = {
				trnIds: [transaction.id],
				transaction: transaction
			}
			$http.post(settings.backendJsonLink + 'TransactionServices/cancelDepositFraud', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					var arrayIndex = searchJsonKeyInArray($scope.transactions, 'id', transaction.id);
					$scope.transactions[arrayIndex] = $scope.calculateJ4Postpone(data.transactions[0]);
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.cancelled-deposit-fraud'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'cancelDepositFraud')
				})
				.finally(function() {
					hideLoading();
				})
		}, null, {ok: $rootScope.getMsgs('yes'), cancel: $rootScope.getMsgs('no')});
	}
	$scope.approveDeposit = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {
			trnIds: [transaction.id],
			transaction: transaction
		}
		$http.post(settings.backendJsonLink + 'TransactionServices/approveDeposit', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				var arrayIndex = searchJsonKeyInArray($scope.transactions, 'id', transaction.id);
				$scope.transactions[arrayIndex] = $scope.calculateJ4Postpone(data.transactions[0]);
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.approved-wire-deposit'));
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'approveDeposit')
			})
			.finally(function() {
				hideLoading();
			})
	}
	$scope.cancelDepositMistake = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		$rootScope.confirm($rootScope.getMsgs('confirm.transactions-mistake'), function(){
			showLoading();
			var methodRequest = {
				trnIds: [transaction.id],
				transaction: transaction
			}
			
			$http.post(settings.backendJsonLink + 'TransactionServices/cancelDepositMistake', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					var arrayIndex = searchJsonKeyInArray($scope.transactions, 'id', transaction.id);
					$scope.transactions[arrayIndex] = $scope.calculateJ4Postpone(data.transactions[0]);
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.cancelled-deposit'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'cancelDepositMistake')
				})
				.finally(function() {
					hideLoading();
				})
		}, null, {ok: $rootScope.getMsgs('yes'), cancel: $rootScope.getMsgs('no')});
	}
	$scope.postponeJ4 = function(transaction){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		if(transaction.transactionPostponed.id){
			methodRequest.id = transaction.transactionPostponed.id;
		}
		methodRequest.transactionId = transaction.id;
		methodRequest.numberOfDays = transaction.postponeDate.numberOfDays;
		$http.post(settings.backendJsonLink + 'TransactionServices/manageTransactionPostponed', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('transaction-postponed', {transactionId: transaction.id}));
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'postponeJ4')
			})
			.finally(function() {
				hideLoading();
			})
	}
	$scope.isCcDepositAndPending = function(transaction) {
		return (transaction.typeId == $scope.TRANSACTION_TYPE_CC_DEPOSIT && transaction.chargeBackId == null && transaction.statusId == $scope.TRANS_STATUS_PENDING);
	}

	$scope.showChargeback = function(transaction) {
		$rootScope.$state.go('ln.risk.chargebacks.view', {ln: $scope.$state.params.ln, transactionId: transaction.id});
	}
	$scope.showForm = true;
	$scope.showDetails = false;

	$scope.getTransactionsByReferenceId = function(transaction) {
		var trnIds = [transaction.id, transaction.referenceId];
		$scope.getAll(_formCopy, null, trnIds);
	}

	$scope.showAdditionalInfoFn = function(transaction) {
		transaction.showAdditionalInfo = !transaction.showAdditionalInfo;
		if (transaction.showAdditionalInfo && transaction.isRerouted) {
			$scope.getReroutingTransactions(transaction);
		}
	}
	
	$scope.getReroutingTransactions = function(transaction) {
		var request = {
			referenceId: transaction.id,
			writerId: writerId
		}
		$http.post(settings.backendJsonLink + 'TransactionServices/getReroutingTransactions', request)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				transaction.reroutingTransactions = {
					typeId: data.typeId,
					reroutingTransactionId: data.reroutingTransactionId
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getReroutingTransactions')
			})
	}
	
	$scope.getAllPreset = function(transaction) {
		var trnIds = [transaction.id];
		for (var i = 0; i < transaction.reroutingTransactions.reroutingTransactionId.length; i++) {
			trnIds.push(transaction.reroutingTransactions.reroutingTransactionId[i]);
		}
		$scope.getAll(_formCopy, null, trnIds);
	}
}]);

backendApp.controller('PendingWithdrawalsController', ['$rootScope', '$scope', '$http', '$state', 'SingleSelect', 'Multiselect', 'Pagination', '$timeout', 'utilsService', function($rootScope, $scope, $http, $state, SingleSelect, Multiselect, Pagination, $timeout, utilsService) {
	switch($rootScope.$state.current.name) {
		case 'ln.transaction.pendingWithdrawals_firstApprove':
			$scope.screenName = 'FirstApprove';
			$scope.screenNameShort = 'first';
			break;
		case 'ln.transaction.pendingWithdrawals_secondApprove':
			$scope.screenName = 'SecondApprove';
			$scope.screenNameShort = 'second';
			break;
		case 'ln.transaction.pendingWithdrawals_support':
			$scope.screenName = 'Support';
			$scope.screenNameShort = 'support';
			break;
	}
	
	var services = {
		screenSettings: 'get' + $scope.screenName + 'ScreenSettings',
		search: 'search' + $scope.screenName + 'Withdraws',
		userDetails: 'get' + $scope.screenName + 'UserDetails',
		transactionDetails: 'get' + $scope.screenName + 'TransactionDetails',
		depositSummaryDetails: 'get' + $scope.screenName + 'DepositSummaryDetails',
		approve: $scope.screenNameShort + 'ApproveWithdraw',
		save: $scope.screenNameShort + 'SaveWithdraw'
	}
	
	$scope.pagePermissions = {
		flag: 'pendingWithdrawal' + $scope.screenName + '_searchWithdrawsFlag',
		status: 'pendingWithdrawal' + $scope.screenName + '_searchWithdrawsStatus',
		tradersReviewed: 'pendingWithdrawal' + $scope.screenName + '_tradersReviewed',
		save: 'pendingWithdrawal' + $scope.screenName + '_saveWithdraw',
		approve: 'pendingWithdrawal' + $scope.screenName + '_approveWithdraw',
		feeExamp: 'pendingWithdrawal' + $scope.screenName + '_feeExempt',
		clearingProvider: 'pendingWithdrawal' + $scope.screenName + '_clearingProvider',
		accountingOk: 'pendingWithdrawal' + $scope.screenName + '_accountingOk',
	}
	
	$scope.USERS_PRIVATE_COMPANY = 12;
	$scope.TRANSATION_CLASS_TYPES_REAL_DEPOSITS = 1;
	$scope.TRANSATION_CLASS_TYPES_REAL_WITHDRAWALS = 2;
	
	$scope.formDetails = {}
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {
		startDate: 0,
		endDate: 0,
		transactionId: '',
		userId: '',
		skinBusinessCases: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'all'}, 
			mutateCallback: $rootScope.getMsgs, 
			isSmart: false
		}),
		skins: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'all'}, 
			mutateCallback: $rootScope.getMsgs, 
			isSmart: true
		}),
		countries: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'all'}, 
			mutateCallback: $rootScope.getMsgs, 
			isSmart: true, 
			iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}
		}),
		currencies: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'all'}, 
			mutateCallback: $rootScope.getMsgs, 
			isSmart: true
		}),
		pendingWithdrawTypes: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'all'}, 
			mutateCallback: $rootScope.getMsgs, 
			isSmart: true
		}),
		userInvested: '',
		pastWithdraws: '',
		amountUSDFrom: '',
		amountUSDTo: '',
		flagSubjects: new Multiselect.InstanceClass({
			aggregateOption: {id: -1, name: 'All'}, 
			isSmart: true
		}),
		status: ''
	};
	
	
	$scope.pendingWithdrawals = null;
	
	$scope.changePage = function(page){
		$scope.getAll(null, page);
	}
	
	$scope.filter.resultsPerPage = new SingleSelect.InstanceClass({sortById: true});
	$scope.filter.resultsPerPage.fillOptions(utilsService.resultsPerPage);
	$scope.filter.resultsPerPage.setModel($scope.filter.resultsPerPage.getOptionById(15));
	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.selectedFilter = null;
	
	$scope.filterCollapsed = false;
	
	$scope.filter.dateRangeDays = 14;
	$scope.filter.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.openCalendar = function(e, date) {
		$scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);
		
		$scope.defaultStartDate = new Date();
		$scope.defaultStartDate.setDate($scope.defaultStartDate.getDate() - 30);
		
		$scope.filter.startDate = $scope.defaultStartDate;
		$scope.filter.endDate = $scope.currentDate;
		// if($scope.filter.endDate.getTime() - $scope.filter.startDate.getTime() > ($scope.filter.dateRangeDays - 1)*24*60*60*1000){
			// $scope.filter.startDate.setTime($scope.filter.endDate.getTime() - ($scope.filter.dateRangeDays - 1)*24*60*60*1000);
		// }
		
		$scope.filter.minDate = new Date();
		$scope.filter.minDate.setTime($scope.filter.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.filter.maxDate = new Date();
		$scope.filter.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	$scope.initDates();
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	$scope.filterToggled = function(toggleName, isOn){
		$scope.filterCollapsed = !isOn;
	}
	
	
	$scope.initFiltersOnReady = function() {
		$scope.filter.classUsers = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.classUser = $scope.filter.classUsers[0];
		$scope.filter.userInvestedFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.userInvested = $scope.filter.userInvestedFilter[0];
		$scope.filter.pastWithdraws = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.pastWithdraw = $scope.filter.pastWithdraws[0];
		$scope.filter.withdrawStatusFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.withdrawStatus = $scope.filter.withdrawStatusFilter[0];
		$scope.filter.flagSubjectsSingleFilter = [{id: -1, name: $rootScope.getMsgs('select')}];
		$scope.filter.clearingProvidersFilter = [{id: -1, name: $rootScope.getMsgs('select')}];
		$scope.filter.flagAccountingOkFilter = [{id: -1, name: $rootScope.getMsgs('all')}];
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getPendingWithdrawalsScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getPendingWithdrawalsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getPendingWithdrawalsScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.screenSettings, MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					$rootScope.fillFilter({arr: $scope.filter.classUsers, data: data, filter: 'classUsers', translate: true});
					for(var i = 0; i < $scope.filter.classUsers.length; i++){
						$scope.filter.classUsers[i].name = $scope.filter.classUsers[i].name.replace(/\\\\/, '\\');
						if($scope.filter.classUsers[i].id == $scope.USERS_PRIVATE_COMPANY){
							$scope.filter.classUser = $scope.filter.classUsers[i];
						}
					}
					
					$scope.filter.skinBusinessCases.fillOptions(data.screenFilters.skinBusinessCases);
					$scope.filter.skins.fillOptions(data.screenFilters.skins);
					$scope.filter.countries.fillOptions(data.screenFilters.countries);
					$scope.filter.currencies.fillOptions(data.screenFilters.currencies);
					$scope.filter.pendingWithdrawTypes.fillOptions(data.screenFilters.pendingWithdrawTypes);
					$rootScope.fillFilter({arr: $scope.filter.userInvestedFilter, data: data, filter: 'userInvested'});
					$rootScope.fillFilter({arr: $scope.filter.pastWithdraws, data: data, filter: 'pastWithdraws'});
					$scope.filter.flagSubjects.fillOptions(data.screenFilters.flagSubjects);
					$rootScope.fillFilter({arr: $scope.filter.withdrawStatusFilter, data: data, filter: 'withdrawStatus'});
					$rootScope.fillFilter({arr: $scope.filter.flagAccountingOkFilter, data: data, filter: 'accountingOk'});
					$scope.filter.flagAccountingOk = $scope.filter.flagAccountingOkFilter[2];
					//details screen
					$rootScope.fillFilter({arr: $scope.filter.flagSubjectsSingleFilter, data: data, filter: 'flagSubjects'});
					$rootScope.fillFilter({arr: $scope.filter.clearingProvidersFilter, data: data, filter: 'clearingProviders'});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, services.screenSettings);
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getPendingWithdrawalsScreenSettings');
			})
	}
	
	$scope.getAll = function(_form, page){
		var paging = false;
		if (!isUndefined(_form)) {
			$scope._form = _form;
		} else {
			_form = $scope._form;
			paging = true;
		}
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		if (_form.$valid || paging) {
			$scope.currentPage = page;
			$rootScope.resetGlobalErrorMsg();
			showLoading();
			
			var methodRequest = {
				pendingWithdraw: {
					page: page,
					rowsPerPage: $scope.filter.resultsPerPage.getId(),
					from: $rootScope.dateParse({
						date: $scope.filter.startDate, 
						startOfDay: true, 
						timestamp: true
					}),
					to: $rootScope.dateParse({
						date: $scope.filter.endDate,
						endOfDay: true,
						timestamp: true
					})
				}
			};
			
			$scope.filter.transactionId != '' ? methodRequest.pendingWithdraw.transactionId = $scope.filter.transactionId : '';
			$scope.filter.userId != '' ? methodRequest.pendingWithdraw.userId = $scope.filter.userId : '';
			
			if (!$scope.filterCollapsed) {
				$scope.filter.classUser.id >= 0 ? methodRequest.pendingWithdraw.userClasses = $scope.filter.classUser.id : '';
				$scope.filter.skinBusinessCases.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.businessCases = $scope.filter.skinBusinessCases.getCheckedIds() : '';
				$scope.filter.skins.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.skins = $scope.filter.skins.getCheckedIds() : '';
				$scope.filter.countries.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.countries = $scope.filter.countries.getCheckedIds() : '';
				$scope.filter.currencies.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.currencies = $scope.filter.currencies.getCheckedIds() : '';
				
				$scope.filter.pendingWithdrawTypes.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.transactionTypes = $scope.filter.pendingWithdrawTypes.getCheckedIds() : '';
				$scope.filter.userInvested.id >= 0 ? methodRequest.pendingWithdraw.invested = $scope.filter.userInvested.id : '';
				$scope.filter.pastWithdraw.id >= 0 ? methodRequest.pendingWithdraw.pastWithdraws = $scope.filter.pastWithdraw.id : '';
				$scope.filter.amountUSDFrom != '' ? methodRequest.pendingWithdraw.amountUSDFrom = ($scope.filter.amountUSDFrom * 100) : '';
				$scope.filter.amountUSDTo != '' ? methodRequest.pendingWithdraw.amountUSDTo = ($scope.filter.amountUSDTo * 100) : '';
				
				$scope.filter.flagSubjects.getCheckedIds().length > 0 ? methodRequest.pendingWithdraw.flagSubjects = $scope.filter.flagSubjects.getCheckedIds() : '';
				$scope.filter.withdrawStatus.id >= 0 ? methodRequest.pendingWithdraw.status = $scope.filter.withdrawStatus.id : '';
				
				methodRequest.pendingWithdraw.flagAccountingOk = $scope.filter.flagAccountingOk.id;
			}
			
			if (paging) {
				methodRequest = $scope.lastSearch;
				methodRequest.pendingWithdraw.page = $scope.currentPage;
			} else {
				$scope.lastSearch = methodRequest;
			}
			$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.search, methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.pendingWithdraws = data.pendingWithdraw.map(function(current) {
						if (current.timeCreated == 0) {
							current.timeCreated = null;
						}
						return current;
					});
				
					var totalCount = 0;
					if (data.pendingWithdraw.length > 0) {
						totalCount = data.pendingWithdraw[0].totalCount;
					}
					$scope.pagination.setPages(page, totalCount, $scope.filter.resultsPerPage.getId());
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'searchPendingWithdraws')
				})
				.finally(function() {
					hideLoading();
				})
		} else {
			showToast(2, $rootScope.getMsgs('error.general_invalid_form'));
		}
	}
	
	$scope.getUserDetails = function(transactionId) {
		$scope.transactionIdSelected = transactionId;
		showLoading();
		var WithdrawDetailsRequest = {
			transactionId: transactionId
		}
		$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.userDetails, WithdrawDetailsRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.pendingWithdrawSelected.statusDsp = $scope.filter.withdrawStatusFilter[searchJsonKeyInJson($scope.filter.withdrawStatusFilter, 'id', $scope.pendingWithdrawSelected.status)].name;
				
				$scope.accountingOkWriterChanged = false;
				
				$scope.formDetails.flagChecked = data.userDetails.flag;
				if (data.userDetails.firstApproveDate == 0) {data.userDetails.firstApproveDate = null;}
				if (data.userDetails.bin) {data.userDetails.binDsp = detectCCtype(data.userDetails.bin);}
				$scope.formDetails.comments = data.userDetails.comments;
				$scope.formDetails.exemptFee = data.userDetails.feeCancel;
				$scope.formDetails.accountingOkFlag = data.userDetails.accountingOkFlag;
				$scope.formDetails.accountingOkWriter = data.userDetails.accountingOkWriter;
				$scope.formDetails.accountingOkWriterId = data.userDetails.accountingOkWriterId;
				$scope.formDetails.accountingChecked = false;
				
				$scope.userDetailsSelected = data.userDetails;
				$scope.userDetailsSelected.countryFlag = settings.jsonImagegLink + 'images/flags/' + data.userDetails.countryId + '.png';
				
				if ($scope.userDetailsSelected.flagSubject == 0) {$scope.userDetailsSelected.flagSubject = -1}
				$scope.filter.flagSubjectsSingle = $scope.filter.flagSubjectsSingleFilter[searchJsonKeyInJson($scope.filter.flagSubjectsSingleFilter, 'id', $scope.userDetailsSelected.flagSubject)];

				if ($scope.userDetailsSelected.clearingProviderId == 0) {$scope.userDetailsSelected.clearingProviderId = -1}
				$scope.filter.clearingProvider = $scope.filter.clearingProvidersFilter[searchJsonKeyInJson($scope.filter.clearingProvidersFilter, 'id', $scope.userDetailsSelected.clearingProviderId)];
				
				$('._toggleLine').on('click', function() {
					$(this).parent().find('._toggleLineContent').fadeToggle().css("display","table");
				})
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, services.userDetails)
			})
			.finally(function() {
				hideLoading();
			})
	}

	$scope.getTransactionDetails = function() {
		if ($scope.transactionDetailsSelected == null) {
			showLoading();
			var WithdrawDetailsRequest = {
				transactionId: $scope.transactionIdSelected
			}
			$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.transactionDetails, WithdrawDetailsRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					if (data.transactionDetails.controlApproved == 0) {data.transactionDetails.controlApproved = null;}
					switch(data.transactionDetails.activeBonuses) {
						case 1: data.transactionDetails.activeBonusesDsp = $rootScope.getMsgs('yes');break;
						case 2: data.transactionDetails.activeBonusesDsp = $rootScope.getMsgs('no');break;
						case 3: data.transactionDetails.activeBonusesDsp = $rootScope.getMsgs('trans.status.pending');break;
					}
					
					$scope.transactionDetailsSelected = data.transactionDetails;
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, services.transactionDetails)
				})
				.finally(function() {
					hideLoading();
				})
		}
	}

	$scope.getDepositSummaryDetails = function() {
		if ($scope.depositSummaryListSelected == null) {
			showLoading();
			var WithdrawDetailsRequest = {
				transactionId: $scope.transactionIdSelected
			}
			$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.depositSummaryDetails, WithdrawDetailsRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.depositSummaryListSelected = data.depositSummaryList.map(function(current, index) {
						if (index > 0) {
							if (current.providerName == null) {
									current.rowTotal = true;
								} else {
									current.rowTotal = false;
								}
						} else {
							current.rowTotal = true;
						}
						return current;
					});
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, services.depositSummaryDetails)
				})
				.finally(function() {
					hideLoading();
				})
		}
	}

	$scope.changeView = function(view, withdrawDetails) {
		$rootScope.resetGlobalErrorMsg();
		$scope.action = view;
		if ($scope.action == 'details') {
			$scope.pendingWithdrawSelected = withdrawDetails;
			$scope.userDetailsSelected = null;
			$scope.transactionDetailsSelected = null;
			$scope.depositSummaryListSelected = null;
			$scope.getUserDetails(withdrawDetails.transactionId);
		} else {
			$scope.getAll(null, $scope.currentPage);
		}
	}

	$scope.approveWithdraw = function() {
		showLoading();
		var tradersReviewed = $scope.transactionDetailsSelected ? $scope.transactionDetailsSelected.tradersReviewed : false;
		var accountingOkWriter = $scope.accountingOkWriterChanged ? $rootScope.writer.id : $scope.formDetails.accountingOkWriterId;
		var accountingOkWriterName = $scope.accountingOkWriterChanged ? $rootScope.writer.userName : $scope.formDetails.accountingOkWriter;
		var ApproveWithdrawRequest = {
			transactionId: $scope.pendingWithdrawSelected.transactionId,
			expectedProviderChange: $scope.formDetails.expectedProviderChange,
			clearingProviderId: (!isUndefined($scope.filter.clearingProvider)) ? $scope.filter.clearingProvider.id : null,
			exemptFee: $scope.formDetails.exemptFee,
			comments: $scope.formDetails.comments,
			flagChecked: $scope.formDetails.flagChecked,
			flagSubjectId: (!isUndefined($scope.filter.flagSubjectsSingle) && $scope.filter.flagSubjectsSingle.id > 0) ? $scope.filter.flagSubjectsSingle.id : null,
			tradersReviewed: tradersReviewed,
			accountingOkFlag: $scope.formDetails.accountingOkFlag,
			accountingOkWriter: accountingOkWriter,
			accountingOkWriterName: accountingOkWriterName,
			accountingChecked: $scope.formDetails.accountingChecked
		}
		$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.approve, ApproveWithdrawRequest)
			.then(function(data) {
				$scope.accountingOkWriterChanged = false;
				$rootScope.resetGlobalErrorMsg();
				showToast(1, $rootScope.getMsgs('general.update.success'));
				$scope.changeView('');
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, services.approve)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.cySecondApproveWithdraw = function(transaction) {
		showLoading();
		var ApproveWithdrawRequest = {
			transactionId: transaction.transactionId,
			// expectedProviderChange: false,
			clearingProviderId: transaction.clearingProviderId,
			exemptFee: false,
			comments: '',
			flagChecked: false,
			flagSubjectId: null,
			tradersReviewed: false,
			accountingOkFlag: true,
			accountingOkWriter: $rootScope.writer.id,
			accountingOkWriterName: $rootScope.writer.userName,
			accountingChecked: true
		}
		$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/cySecondApproveWithdraw', ApproveWithdrawRequest)
			.then(function(data) {
				$scope.accountingOkWriterChanged = false;
				$rootScope.resetGlobalErrorMsg();
				showToast(1, $rootScope.getMsgs('general.update.success'));
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, services.approve)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.saveWithdraw = function() {
		showLoading();
		var tradersReviewed = $scope.transactionDetailsSelected ? $scope.transactionDetailsSelected.tradersReviewed : false;
		var accountingOkWriter = $scope.accountingOkWriterChanged ? $rootScope.writer.id : $scope.formDetails.accountingOkWriterId;
		var accountingOkWriterName = $scope.accountingOkWriterChanged ? $rootScope.writer.userName : $scope.formDetails.accountingOkWriter;
		var ApproveWithdrawRequest = {
			transactionId: $scope.pendingWithdrawSelected.transactionId,
			expectedProviderChange: $scope.formDetails.expectedProviderChange,
			clearingProviderId: (!isUndefined($scope.filter.clearingProvider)) ? $scope.filter.clearingProvider.id : null,
			exemptFee: $scope.formDetails.exemptFee,
			comments: $scope.formDetails.comments,
			flagChecked: $scope.formDetails.flagChecked,
			flagSubjectId: (!isUndefined($scope.filter.flagSubjectsSingle) && $scope.filter.flagSubjectsSingle.id > 0) ? $scope.filter.flagSubjectsSingle.id : null,
			tradersReviewed: tradersReviewed,
			accountingOkFlag: $scope.formDetails.accountingOkFlag,
			accountingOkWriter: accountingOkWriter,
			accountingOkWriterName: accountingOkWriterName,
			accountingChecked: $scope.formDetails.accountingChecked
		}
		$http.post(settings.backendJsonLink + 'PendingWithdrawalsServices/' + services.save, ApproveWithdrawRequest)
			.then(function(data) {
				$scope.accountingOkWriterChanged = false;
				$rootScope.resetGlobalErrorMsg();
				showToast(1, $rootScope.getMsgs('general.update.success'));
				$scope.changeView('');
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, services.save)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.getDocUrl = function(fileName){
		return settings.jsonImagegLink + 'docs/' + fileName + '?isNewBe=1';
	}

	$scope.setAccountingWriter = function() {
		$scope.formDetails.accountingOkWriter = writerId;
		$scope.formDetails.accountingChecked = true;
		$scope.accountingOkWriterChanged = true;
	}
}]);
