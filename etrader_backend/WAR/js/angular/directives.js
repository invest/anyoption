backendApp.directive('autoFocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				$element[0].focus();
			});
		}
	}
}]);

backendApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
});

backendApp.directive('validCcNum', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
				return value;
			});
		}
	};
});

backendApp.directive('validEmail', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validEmail', regEx_email.test(value));
				return value;
			});
		}
	};
});

backendApp.directive('restrictNickname', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				// ngModel.$setValidity('nickname', (regEx_nickname.test(value)));
				value = value.replace(regEx_nickname_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictDigits', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				value = value.replace(regEx_digits_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictFloat', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '.').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictLetters', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				//var regEx_letters_reverse = new RegExp((skinMap[settings.skinId].regEx_letters+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				var regEx_letters_reverse = new XRegExp((regEx_lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				value = value.replace(regEx_letters_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('device', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				var device = detectDevice();
				if (device != '') {
					$element.addClass('mobile-device');
				}
				$element.addClass(device);
			});
		}
	}
}]);

backendApp.directive('compile', ['$compile', function($compile) {
	// directive factory creates a link function
	return function(scope, elem, attrs) {
		scope.$watch(
			function(scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(attrs.compile);
			},
			function(value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				elem.html(value);

				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(elem.contents())(scope);
			}
		);
	}
}]);

backendApp.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				if(iAttrs.fallbackSrc != ""){
					angular.element(this).attr("src", iAttrs.fallbackSrc);
					angular.element(this).css("visibility", "visible");
				}else{
				//	angular.element(this).css("visibility", "hidden"); //Causes problems, sometimes, with some markets
				}
			});
		}
	}
	return fallbackSrc;
});

backendApp.directive('loading', ['$timeout','$compile', function($timeout, $compile) {
	return {
		restrict: "A",
		link: function(scope,element,attrs,ngCtrl) {
			scope.ready;
			var loader = angular.element('<i class="loading-section-el" ng-class="{dN: ready}" ng-hide="ready" ></i>');
			element.addClass('loading-section');
			element.append($compile(loader)(scope));
		}
	};
}]);

backendApp.directive('pagination', ['$rootScope', function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'components/pagination.html', // markup for template
		scope: {
			pagination: '=model'
		},
		transclude: true,
		link: function(scope, element, attrs) {
			scope.getMsgs = $rootScope.getMsgs;
		}
	};
}]);

backendApp.directive('multiselect', ['$document', '$timeout', function($document, $timeout) {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'components/multiselect.html', // markup for template
		scope: {
			multiselect: '=model',
			ngDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.toggleOpen = function(forceClose, e){
				if (e) {//stop opening dropdown if clicked on element for deletion 
					if ($(e.target).parents('._stopElement').length > 0) {
						return;
					}
				}
				ngModel ? ngModel.$setTouched() : '';
				if(!scope.multiselect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.multiselect.visible = true;
					$timeout(function() {
						$(element[0]).find('[data-filter]:eq(0)').focus();
					});
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.multiselect.visible = false;
					scope.multiselect.filterText = '';
					$document.unbind('mousedown', documentClickBind);
				}
			}
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.ngDisabled){
					return;
				}
				
				if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}
			});
			
			scope.$watch(function(){
				return scope.multiselect ? scope.multiselect.getCheckedIds() : null;
			}, function(){
				scope.updateScroll();
			}, true);
			
			scope.updateScroll = function(){
				$timeout(function(){
					if($(element[0]).find('.multiselect-selected-options:eq(0)').height() > $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()){
						scope.showScroll = true;
						scope.disableArrows();
					} else {
						scope.showScroll = false;
					}
				});
			}
			
			scope.selectedOptionsScrollUp = function() {
				$(element[0])
					.find('.multiselect-selected-options-holder:eq(0)')
					.scrollTop(
						$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() - $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()
					);
				scope.disableArrows();
			}
			
			scope.selectedOptionsScrollDown = function(){
				$(element[0])
					.find('.multiselect-selected-options-holder:eq(0)')
					.scrollTop(
						$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() + $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()
					);
				scope.disableArrows();
			}
			
			scope.disableArrows = function() {
				if ($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() == 0) {
					scope.noScrollUp = true;
				} else {
					scope.noScrollUp = false;
				}
				if ($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() >= $(element[0]).find('.multiselect-selected-options').height() - $(element[0]).find('.multiselect-selected-options-holder').height()) {
					scope.noScrollDown = true;
				} else {
					scope.noScrollDown = false;
				}
			}
		}
	};
}]);

backendApp.directive('singleSelect', ['$document', '$timeout', '$rootScope', function($document, $timeout, $rootScope) {
	return {
		restrict: 'E',
		require: '?ngModel',
		//templateUrl: $rootScope.isMobile ? folderPrefix + 'components/single-select-mobile.html' : folderPrefix + 'components/single-select.html', // markup for template
		template: '<ng-include src="template()"></ng-include>',
		scope: {
			ngDisabled: '=',
			
		},
		link: function(scope, element, attrs, ngModel) {
			scope.templateName = '';
			scope.template = function(){
				scope.templateName = attrs.template;
				return scope.templateName ? folderPrefix + 'components/single-select-' + scope.templateName + '.html' : folderPrefix + 'components/single-select.html';
			}
			
			scope.getMsgs = $rootScope.getMsgs;
			
			ngModel.$options = {
				allowInvalid: true
			};
			ngModel.$isEmpty = function(value){
				if(!value.model || !value.model.model || !value.model.model.name || value.model.model.id === -1){
					return true;
				}
				return false;
			}
			ngModel.$formatters.push(function(model){
				return {model: model};
			});
			ngModel.$parsers.push(function(viewValue){
				return viewValue.model;
			});
			ngModel.$render = function(){
				scope.singleSelect = ngModel.$viewValue.model;
			};
			scope.selectOption = function(option){
				scope.toggleOpen();
				scope.updateModel(option);
			}
			scope.updateModel = function(option){
				if(scope.ngDisabled){
					return;
				}
				ngModel.$modelValue.setModel(option);
				ngModel.$setViewValue({model: ngModel.$modelValue.clone()});
				ngModel.$commitViewValue();
				ngModel.$render();
			}
			
			scope.toggleOpen = function(forceClose){
				ngModel.$setTouched();
				if(!scope.singleSelect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.singleSelect.visible = true;
					element.on('transitionend', toggleOpenTransitionEndHandler);
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.singleSelect.visible = false;
					scope.singleSelect.filterText = '';
					element.off('transitionend', toggleOpenTransitionEndHandler);
					$document.unbind('mousedown', documentClickBind);
				}
			}
			
			function toggleOpenTransitionEndHandler() {
				element.find('[data-filter]:eq(0)').focus();
			}
			
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.singleSelect.isSmart){
					return;
				}
				if(scope.ngDisabled){
					return;
				}
				if(e.keyCode == 38){//Up key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getPreviousOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 40){//Down key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOption()
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}else if(e.key && !e.ctrlKey){
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOptionByKey(e.key);
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
							return false;
						}
					});
				}
			});
			
			scope.$watch(function(){
				return scope.singleSelect ? scope.singleSelect.options : null;
			}, function(){
				scope.updateSize();
			}, true);
			
			scope.updateSize = function(){
				$timeout(function(){
					var width;
					var maxWidth = null;
					if($(element[0]).css('max-width') != 'none'){
						maxWidth = $(element[0]).width();
					}
					var maxTextWidth = $(element[0]).find('.single-select-header-holder:eq(0)').width();
					if($(element[0]).find('[data-options-box]:eq(0)') && $(element[0]).find('[data-options-box]:eq(0)').length > 0){
						var rect = $(element[0]).find('[data-options-box]:eq(0)')[0].getBoundingClientRect();
						if(rect.width){
							width = rect.width;
						}else{
							width = rect.right - rect.left;
						}
						if(maxWidth && width > maxWidth){
							maxTextWidth -= (width - maxWidth);
							width = maxWidth;
						}
					}
					if(width > 0){
						$(element[0]).width(Math.ceil(width));
						
						var arrowWidth = 18;
						$(element[0]).find('.single-select-selected-option:eq(0)').css('max-width', Math.ceil(maxTextWidth - arrowWidth) + 'px');
					}
				});
			}
			
			scope.updateScroll = function(option){
				$timeout(function(){
					var optionEl = $(element[0]).find('[data-options-box] .single-select-options > span:eq(' + option.index + ')');
					if(optionEl.position().top < 0 || (optionEl.position().top + optionEl.outerHeight() > optionEl.offsetParent().height())){
						optionEl.offsetParent().scrollTop(optionEl.offsetParent().scrollTop() + optionEl.position().top);
					}
					optionEl = null;
				});
			}
		}
	};
}]);

backendApp.directive('checkLastCallback', ['$parse', function($parse) {
	return {
		restrict: 'A',
		scope: true,
		link: function(scope, element, attrs) {
			if(attrs.checkLastCallback != ''){
				if(scope.$last === true){
					var invoker = $parse(attrs.checkLastCallback);
					invoker(scope)();
				}
			}
		}
	};
}]);

backendApp.directive('convertToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(val) {
				return parseInt(val, 10);
			});
			ngModel.$formatters.push(function(val) {
				return '' + val;
			});
		}
	};
});

backendApp.directive('unselectable', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
		}
	};
}]);

backendApp.directive('ngSrcNoCache', function() {
	return {
		priority: 99,
		link: function(scope, element, attrs) {
			attrs.$observe('ngSrcNoCache', function(ngSrcNoCache) {
				var prefix = '?';
				if(ngSrcNoCache.indexOf('?') != -1){
					prefix = '&';
				}
				ngSrcNoCache += prefix + (new Date()).getTime();
				attrs.$set('src', ngSrcNoCache);
			});
		}
	}
});

/*
backendApp.directive('singleSelect', ['$compile', '$timeout', function($compile, $timeout) {
	return {
		restrict: 'E',
		compile: function(element, attrs, transclude) {
			var select = element.find('select');
			var span = document.createElement('span');
			var model = select.attr('ng-model');
			select.attr('ng-model', model + '.model');
			$(span).html('{{' + model + '.getTextValue()}}');
			element.append(span);
		}
	};
}]);
*/
/*
* Kept for possible future development
backendApp.directive('singleSelect', ['$compile', '$timeout', function($compile, $timeout) {
	return {
		restrict: 'E',
		template:
		'<select ng-model="ngModel"></select>' +
		'<span></span>',
		transclude: true,
		compile: function(tElement, tAttrs) {
			var options = tAttrs.options;
			var model = tAttrs.ngModel;
			var optionsRepeat = tAttrs.optionsRepeat;
			var optionsRepeatName = tAttrs.optionsRepeatName;
			var optionsRepeatFilter = tAttrs.optionsRepeatFilter;
			
			var select = tElement.find('select');
			var span = tElement.find('span');
			
			select.attr('ng-model', model);
			if(optionsRepeat && optionsRepeat != '' && optionsRepeatName && optionsRepeatName != ''){
				var optionsEl = document.createElement('option');
				$(optionsEl).attr('ng-repeat', optionsRepeat);
				$(optionsEl).attr('value', '{{' + optionsRepeatName + '.id}}');
				optionsEl.text = '{{' + optionsRepeatName + '.name}}';
				$(select).append(optionsEl);
				span.attr('ng-bind-html', 'getFilterElById(' + optionsRepeatFilter + ',' + model + ').name');
			}else{
				select.attr('ng-options', options);
				span.attr('ng-bind-html', model + '.name');
			}
		}
	};
}]);
*/


backendApp.directive('toggableOn', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-on', tAttrs.toggableOn);
		}
	}
}]);
backendApp.directive('toggableOff', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-off', tAttrs.toggableOff);
		}
	}
}]);
backendApp.directive('toggler', ['$timeout', function($timeout) {
	return {
		restrict: 'E',
		template: '<span></span>',
		scope: {
			onToggle: '='
		},
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element).on('click', function(){
				if($(element).data('isOn')){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
				}else{
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
				}
				toggleTogglerState();
			});
			
			function setDefaultState(){
				if(attrs.defaultState == 'on'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					toggleTogglerState({forceOn: true, preventOnToggle: true});
				}else if(attrs.defaultState == 'off'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					toggleTogglerState({forceOn: false, preventOnToggle: true});
				}
			};
			
			setDefaultState();
			
			function toggle(el, isOn){
				if(isOn){
					$(el).css('display', '');
				}else{
					$(el).css('display', 'none');
				}
			}
			function toggleTogglerState(params){
				if(!params){
					params = {};
				}
				var isOn = $(element).data('isOn');
				if(typeof params.forceOn == 'undefined' && isOn || typeof params.forceOn != 'undefined' && !params.forceOn){
					$(element).removeClass('block-toggler-on');
					$(element).data('isOn', false);
				}else{
					$(element).addClass('block-toggler-on');
					$(element).data('isOn', true);
				}
				if(scope.onToggle && !params.preventOnToggle){
					scope.$apply(function(){
						scope.onToggle(attrs.toggle, !isOn);
					});
				}
			}
		}
	};
}]);



backendApp.directive('fileWrapper', ['$parse', function($parse) {
	return {
		restrict: 'E',
		template: '<ng-include src="\'' + folderPrefix + 'users/files.html\'"></ng-include>',
		scope: true,
		link: function(scope, element, attrs) {
			scope.filesList = {
				fileId: scope.$eval(attrs.fileId),
				userId: scope.$eval(attrs.userId),
				skinId: scope.$eval(attrs.skinId),
				showPreviousNext: scope.$eval(attrs.showPreviousNext),
				saveCallback: function(){$parse(attrs.saveCallback)(scope)}
			};
			
		}
	};
}]);
/* filter::dateNoZero */
(function() {
	'use strict';
	
	angular.module('backendApp').directive('checkboxAll', checkboxAll);

	//checkboxAll.$inject = [];
	function checkboxAll() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('click',function() {
					var allCheckBoxes = $('.' + attrs.checkboxAll);
					if (element.is(":checked")) {
						allCheckBoxes.prop('checked', true);
					} else {
						allCheckBoxes.prop('checked', false);
					}
				})
			}
		};
	}
})();

/* directive::clickOutside */
(function() {
	'use strict';

	angular.module('backendApp').directive('clickOutside', clickOutside);

	clickOutside.$inject = ['$document', '$parse', 'utilsService'];
	function clickOutside($document, $parse, utilsService) {
		return {
		restrict: 'A',
		scope: false,
		link: function (scope, el, attrs) {
			$document[0].addEventListener('click', clickOutsideHandler, true);
			scope.$on('$destroy', function(){
				$document[0].removeEventListener('click', clickOutsideHandler, true);
			});

			function clickOutsideHandler(e) {
				if (el !== e.target && !el[0].contains(e.target)) {
					scope.$applyAsync(function () {
						scope.$eval(attrs.clickOutside);
					});
				}
			}
		}
	};
	}
})();

/*
//DO NOT UNCOMMENT, IT'S GOING TO BLAW
//Can be used instead of the watch of popUps in controllers.js

backendApp.directive('popups', ['$compile', '$timeout', function(compile, timeout){
	return {
		restrict: 'E',
		link: function(scope, element, attrs) {
			showPopups();
			scope.$watch('popUps', function(){
				showPopups();
			}, true);
			
			function showPopups(){
				for(var i = 0; i < scope.popUps.length; i++) {
					var template = '<div data-popup ng-include="\'' + scope.popUps[i].url + '\'" ng-controller="' + scope.popUps[i].ctr + '" ng-init="passParams(popUps[' + i + '].config)"></div>';
					var cTemplate = compile(template)(scope);
					element.append(cTemplate);
				}
				timeout(function(){
					console.log(element.children().length, scope.popUps.length);
					if(scope.popUps.length == 0 && element.children().length != 0){
						element.empty();
					//	element.remove();
					}else{
						var childCount = 0;
						element.children().each(function(index, el){
							childCount++;
							if(childCount > scope.popUps.length){
								el.remove();
							}
						});
					}
				}, 0);
			}
		}
	}
}]);
*/
backendApp.factory('handleErrors', ['$q', function($q) {  
    var handleErrors = {
//        request: function(config) {
//            return config;
//        },
//        requestError: function(rejectReason) {
//        	return rejectReason;
//        },
        response: function(response) {
        	if (response.config.method == 'POST') {
        		if (response.data == '') {
            		return $q.reject({
            			errorCode: -10000,
            			errorMessages: [{
							field: "",
							message: 'Response is empty string. Check eclipse console for exeptions.'
            			}]
            		});
        		} else if (response.data == null) {
            		return $q.reject({
            			errorCode: -10000,
            			errorMessages: [{
							field: "",
							message: 'Response is null. Check eclipse console for exeptions.'
            			}]
            		});
        		} else if (response.data.errorCode != 0) {
            		return $q.reject({
            			errorCode: response.data.errorCode,
            			errorMessages: response.data.errorMessages,
            			userMessages: response.data.userMessages,
            			params: response.data.params
            		});
            	} else {
	            	response.data._config = response.config;
            	}
        		
        		return $q.resolve(response.data);
        	} else {
				if (typeof response.data == 'object') {
					return response.data;
					
				} else {
					return response;
				}
        	}
        },
        responseError: function(rejectReason) {
        	rejectReason.errorCode = -10001;
    		return $q.reject(rejectReason); 
        }
    };
    return handleErrors;
}]);