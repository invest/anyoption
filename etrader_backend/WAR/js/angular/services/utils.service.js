/* service::utilsService */
(function() {
	'use strict';

	angular.module('backendApp').service('utilsService', utilsService);

	utilsService.$inject = ['$rootScope', 'SingleSelect', 'Multiselect', 'Pagination', 'Utils'];
	function utilsService($rootScope, SingleSelect, Multiselect, Pagination, Utils) {
		var _this = this;
		
		_this.resultsPerPage = [{id: 5, name: '5'}, {id: 10, name: '10'}, {id: 15, name: '15'}, {id: 20, name: '20'}, {id: 30, name: '30'}, {id: 50, name: '50'}, {id: 100, name: '100'}, {id: 200, name: '200'}];
		_this.writer = $rootScope.writer;
		//functions
		_this.getMsg = $rootScope.getMsgs;
		_this.SingleSelect = SingleSelect;
		_this.Multiselect = Multiselect;
		_this.Pagination = Pagination;
		_this.showLoading = Utils.showLoading
		_this.hideLoading = Utils.hideLoading
		_this.Utils = Utils;
		_this.permissions = $rootScope.permissions;
		_this.resetGlobalErrorMsg = $rootScope.resetGlobalErrorMsg;
		_this.handleErrors = $rootScope.handleErrors;
		_this.stateParams = $rootScope.stateParams;
		_this.$state = $rootScope.$state;
		_this.sortFilter = $rootScope.sortFilter;
		_this.dateGetSimple = $rootScope.dateGetSimple;
		_this.confirm = $rootScope.confirm;
		//local
		_this.getPermissions = getPermissions;
		_this.getStateParams = getStateParams;
		_this.goTo = goTo;
		_this.getStateAction = getStateAction;
		_this.setDateNoSec = setDateNoSec;
		_this.checkNull = checkNull;
		_this.openCustomerInformation = openCustomerInformation;
		_this.uiRouterEncodeSlashes = uiRouterEncodeSlashes;
		_this.uiRouterDecodeSlashes = uiRouterDecodeSlashes;
		_this.validateLanguages = validateLanguages;
		
		
		function getPermissions() {
			return $rootScope.permissions;
		}
		
		function getStateParams() {
			return $rootScope.stateParams;
		}
		
		
		function goTo(dest, params) {
			if(!params){
				params = {};
			}
			params.ln = $rootScope.$state.params.ln;
			$rootScope.$state.go(dest, params);
		}
		
		function getStateAction(){
			//TODO
		}
		
		function setDateNoSec(params) {
			var date = new Date();
			
			if (!params) {
				params = {}
			}
			
			if (params.checkNull && !params.date) {
				return null;
			}
			
			if (params.date) {
				date = new Date(params.date);
			}
			
			if (params.startOfDay) {
				date.setHours(0/* + $rootScope.writer.utcOffsetHours*/);
			} else if (params.endOfDay) {
				date.setHours(24/* + $rootScope.writer.utcOffsetHours*/);
			}
			
			if (params.startOfDay || params.endOfDay) {
				date.setMilliseconds(0);
				date.setSeconds(0);
				date.setMinutes(0);
			}
			
			if (params.offset) {
				var hoursOffset = date.getTimezoneOffset()/60 + params.offset;
				date.setHours(date.getHours() + hoursOffset);
			}
			if (params.day) {
				date.setDate(params.day);
			}
			if (params.month) {
				date.setMonth(date.getMonth() + params.month);
			}
			if (params.year) {
				date.setFullYear(params.year);
			}
			var aDay = 1000*60*60*24;
			if (params.setDays) {
				date.setTime(date.getTime() + aDay * params.setDays);
			}
			if (params.setMonths) {
				date.setMonth(date.getMonth() + params.setMonths);
			}
			if (params.setYears) {
				date.setTime(date.getTime() + aDay * 365 * params.setYears);
			}
			
			if (params.timestamp) {
				return date.getTime();
			}
			
			return date;
		}
	
		function checkNull(value, checkType) {
			if (!checkType) {
				return (value != '') ? value : null;
			} else if (checkType == 'id') {
				return (value > -1) ? value : null;
			} else if (checkType == 'ids') {
				return (value.length > 0) ? value : null;
			} else if (checkType == 'date') {
				if (!value || value == 0) {
					return null;
				} else if (typeof value == 'object') {
					return value.getTime();
				} else {
					return value;
				}
			}
		}
	
		function openCustomerInformation(userId){
			window.open($rootScope.getUserStripUrl(userId));
		}
		
		function uiRouterEncodeSlashes(str){
			return str.replace(/\//gi, '~2F').replace(/\?/gi, '~3F');
		}
		
		function uiRouterDecodeSlashes(str){
			return str.replace(/~2F/gi, '/').replace(/~3F/gi, '?');
		}
		
		function validateLanguages(languages){
			var arr = [];
			for(var key in languages){
				if(languages.hasOwnProperty(key)){
					var isAllowed = false;
					for(var skinKey in skinMap){
						if(skinMap.hasOwnProperty(skinKey)){
							if(skinMap[skinKey].locale == languages[key].code && skinMap[skinKey].isAllowed){
								isAllowed = true;
								break;
							}
						}
					}
					if(isAllowed){
						arr.push(languages[key]);
					}
				}
			}
			return arr;
		}
	}
})();