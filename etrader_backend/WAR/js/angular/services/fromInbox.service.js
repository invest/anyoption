/* service::fromInboxService */
(function() {
	'use strict';

	angular.module('backendApp').service('fromInboxService', fromInboxService);

	fromInboxService.$inject = ['$http', '$q'];
	function fromInboxService($http, $q) {
		var _this = this;
		
		_this.getList = getList;
		_this.getSkinName = getSkinName;
		
		function getList(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'DefaultFileTypeServices/getAllDefaultFiles', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function getSkinName(skinId){
			var skin = skinMap[skinId];
			if(skin){
				return skin.skinTranslation;
			}
		}
	}
})();
