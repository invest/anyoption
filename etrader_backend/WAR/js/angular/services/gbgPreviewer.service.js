/* service::gbgPreviewerService */
(function() {
	'use strict';

	angular.module('backendApp').service('gbgPreviewerService', gbgPreviewerService);

	gbgPreviewerService.$inject = ['$http', '$q'];
	function gbgPreviewerService($http, $q) {
		var _this = this;
		
		_this.getGbgXml = getGbgXml;
		
		function getGbgXml(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'FilesServices/getGBGFile', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
