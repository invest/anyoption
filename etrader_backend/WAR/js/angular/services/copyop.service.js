/* service::copyopUserCopiersServices */
(function() {
	'use strict';

	angular.module('backendApp').service('copyopUserCopiersServices', copyopUserCopiersServices);

	copyopUserCopiersServices.$inject = ['$http', '$q'];
	function copyopUserCopiersServices($http, $q) {
		var _this = this;
		
		_this.getList = getList;
		_this.stop = stop;
		
		
		function getList(service, request) {
			var deferred = $q.defer();
			
			var serviceName = 'getUserCopiers';
			if (service == 'copied') {
				serviceName = 'getCopiedUsers';
			}
			
			$http.post(settings.backendJsonLink + 'CopyopUserCopiersServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function stop(request) {
			var deferred = $q.defer();

			$http.post(settings.backendJsonLink + 'CopyopUserCopiersServices/stop', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
