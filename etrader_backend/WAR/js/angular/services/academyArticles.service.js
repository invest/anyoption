/* service::academyArticlesService */
(function() {
	'use strict';

	angular.module('backendApp').service('academyArticlesService', academyArticlesService);

	academyArticlesService.$inject = ['$http', '$q'];
	function academyArticlesService($http, $q) {
		var _this = this;
		
		_this.getList = getList;
		_this.getArticle = getArticle;
		_this.editArticle = editArticle;
		_this.addArticle = addArticle;
		_this.deleteArticle = deleteArticle;
		_this.getSuggestedTopics = getSuggestedTopics;
		
		function getList(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/getAll', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function getArticle(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/getArticleById', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function editArticle(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/editArticle', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function addArticle(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/addArticle', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function deleteArticle(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/deleteArticle', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function getSuggestedTopics(request){
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'AcademyArticlesServices/getTopicSuggestions', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
