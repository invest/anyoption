/* service::chargebackService */
(function() {
	'use strict';

	angular.module('backendApp').service('chargebackService', chargebackService);

	chargebackService.$inject = ['$http', '$q'];
	function chargebackService($http, $q) {
		var _this = this;
		
		_this.savedRequests = [];
		
		_this.get = get;
		_this.set = set;
		
		
		function get(serviceName, request) {
			var deferred = $q.defer();
			
			// if (!request && !_this.savedRequests[serviceName]) {
				// deferred.reject({});
			// } else if (!request) {
				// request = _this.savedRequests[serviceName];
			// }
			
			// if (request) {
				$http.post(settings.backendJsonLink + 'ChargebacksServices/' + serviceName, request)
					.then(function(data) {
						// if (request.saveRequest) {
							// _this.savedRequests[serviceName] = request;
						// }
						// data.request = request;
						deferred.resolve(data);
					})
					.catch(function(data) {
						deferred.reject(data);
					})
			// }
				
			return deferred.promise;
		}
		
		function set(serviceName, request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'ChargebacksServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
