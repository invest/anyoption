var permissions = [
	{
		type: 'space',
		id: 1,
		title: 'Transaction',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Transactions',
				nodes: [
					{
						type: 'permission',
						id: 'transactions_view',
						name: 'transactions_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'transactions_ccNum',
								name: 'transactions_ccNum',
								title: 'CC number+ BIN'
							},{ 
								type: 'permission',
								id: 'transactions_cancelPendingDeposit',
								name: 'transactions_cancelPendingDeposit',
								title: 'Cancel pending deposit'
							},{
								type: 'permission',
								id: 'transactions_fraudCancelPendingDeposit',
								name: 'transactions_fraudCancelPendingDeposit',
								title: 'Fraud cancel pending deposit'
							},{
								type: 'permission',
								id: 'transactions_approveWireDeposit',
								name: 'transactions_approveWireDeposit',
								title: 'Approve Wire deposit'
							},{
								type: 'permission',
								id: 'transactions_cancelBankWireMistake',
								name: 'transactions_cancelBankWireMistake',
								title: 'Cancel Bank wire (Mistake)'
							},{
								type: 'permission',
								id: 'transactions_exportExcel',
								name: 'transactions_exportExcel',
								title: 'Export to excel'
							},{
								type: 'permission',
								id: 'transactions_postponeJ4',
								name: 'transactions_postponeJ4',
								title: 'Postpone J4'
							},{
								type: 'permission',
								id: 'transactions_cancelMaintenanceFee',
								name: 'transactions_cancelMaintenanceFee',
								title: 'Cancel maintenance fee'
							}
						]
					}
				]
			},{
				type: 'screen',
				id: 3,
				title: 'Update transactions',
				nodes: [
					{
						type: 'permission',
						id: 'updateTransaction_view',
						name: 'updateTransaction_view',
						title: 'View'
					}
				]
			},{
				type: 'group',
				id: 2,
				title: 'Pending withdraws',
				nodes: [
					{
						type: 'screen',
						id: 1,
						title: 'First Approval',
						nodes: [
							{
								type: 'permission',
								id: 'pendingWithdrawalFirstApprove_view',
								name: 'pendingWithdrawalFirstApprove_view',
								title: 'View'
							},{
								type: '',
								id: 3,
								title: 'Actions',
								nodes: [
									/* {
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_searchWithdraws',
										name: 'pendingWithdrawalFirstApprove_searchWithdraws',
										title: 'Search'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_searchDetails',
										name: 'pendingWithdrawalFirstApprove_searchDetails',
										title: 'Details'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_searchWithdrawsFlag',
										name: 'pendingWithdrawalFirstApprove_searchWithdrawsFlag',
										title: 'Flag'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_searchWithdrawsStatus',
										name: 'pendingWithdrawalFirstApprove_searchWithdrawsStatus',
										title: 'Status'
									},*/{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_tradersReviewed',
										name: 'pendingWithdrawalFirstApprove_tradersReviewed',
										title: 'Traders reviewed?'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_saveWithdraw',
										name: 'pendingWithdrawalFirstApprove_saveWithdraw',
										title: 'Save'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_approveWithdraw',
										name: 'pendingWithdrawalFirstApprove_approveWithdraw',
										title: 'Approve'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_feeExempt',
										name: 'pendingWithdrawalFirstApprove_feeExempt',
										title: 'Fee exempt'
									},{
										type: 'permission',
										id: 'pendingWithdrawalFirstApprove_clearingProvider',
										name: 'pendingWithdrawalFirstApprove_clearingProvider',
										title: 'Clearing provider'
									}
								]
							}
						]
					},{
						type: 'screen',
						id: 1,
						title: 'Second Approval',
						nodes: [
							{
								type: 'permission',
								id: 'pendingWithdrawalSecondApprove_view',
								name: 'pendingWithdrawalSecondApprove_view',
								title: 'View'
							},{
								type: '',
								id: 3,
								title: 'Actions',
								nodes: [
									/*{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_searchWithdraws',
										name: 'pendingWithdrawalSecondApprove_searchWithdraws',
										title: 'Search'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_searchDetails',
										name: 'pendingWithdrawalSecondApprove_searchDetails',
										title: 'Details'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_searchWithdrawsFlag',
										name: 'pendingWithdrawalSecondApprove_searchWithdrawsFlag',
										title: 'Flag'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_searchWithdrawsStatus',
										name: 'pendingWithdrawalSecondApprove_searchWithdrawsStatus',
										title: 'Status'
									},*/{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_tradersReviewed',
										name: 'pendingWithdrawalSecondApprove_tradersReviewed',
										title: 'Traders reviewed?'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_saveWithdraw',
										name: 'pendingWithdrawalFirstApprove_saveWithdraw',
										title: 'Save'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_approveWithdraw',
										name: 'pendingWithdrawalSecondApprove_approveWithdraw',
										title: 'Approve'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_feeExempt',
										name: 'pendingWithdrawalSecondApprove_feeExempt',
										title: 'Fee exempt'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_clearingProvider',
										name: 'pendingWithdrawalSecondApprove_clearingProvider',
										title: 'Clearing provider'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_accountingOk',
										name: 'pendingWithdrawalSecondApprove_accountingOk',
										title: 'Accounting OK'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSecondApprove_cyApprove',
										name: 'pendingWithdrawalSecondApprove_cyApprove',
										title: 'CY Approve'
									}
								]
							}
						]
					},{
						type: 'screen',
						id: 1,
						title: 'Support',
						nodes: [
							{
								type: 'permission',
								id: 'pendingWithdrawalSupport_view',
								name: 'pendingWithdrawalSupport_view',
								title: 'View'
							},{
								type: '',
								id: 3,
								title: 'Actions',
								nodes: [
									/*{
										type: 'permission',
										id: 'pendingWithdrawalSupport_searchWithdraws',
										name: 'pendingWithdrawalSupport_searchWithdraws',
										title: 'Search'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSupport_searchDetails',
										name: 'pendingWithdrawalSupport_searchDetails',
										title: 'Details'
									},*/{
										type: 'permission',
										id: 'pendingWithdrawalSupport_searchWithdrawsFlag',
										name: 'pendingWithdrawalSupport_searchWithdrawsFlag',
										title: 'Option to search by flag'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSupport_searchWithdrawsStatus',
										name: 'pendingWithdrawalSupport_searchWithdrawsStatus',
										title: 'Option to search by status'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSupport_tradersReviewed',
										name: 'pendingWithdrawalSupport_tradersReviewed',
										title: 'Traders reviewed?'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSupport_saveWithdraw',
										name: 'pendingWithdrawalFirstApprove_saveWithdraw',
										title: 'Save'
									}/*,{
										type: 'permission',
										id: 'pendingWithdrawalSupport_approveWithdraw',
										name: 'pendingWithdrawalSupport_approveWithdraw',
										title: 'Approve'
									}*/,{
										type: 'permission',
										id: 'pendingWithdrawalSupport_feeExempt',
										name: 'pendingWithdrawalSupport_feeExempt',
										title: 'Fee exempt'
									},{
										type: 'permission',
										id: 'pendingWithdrawalSupport_clearingProvider',
										name: 'pendingWithdrawalSupport_clearingProvider',
										title: 'Clearing provider'
									}
								]
							}
						]
					}
				]
			}
		]
	},{
		type: 'space',
		id: 2,
		title: 'Content',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Terms',
				nodes: [
					{
						type: 'permission',
						id: 'terms_view',
						name: 'terms_view',
						title: 'View'
					}
				]
			},{
				type: 'screen',
				id: 2,
				title: 'Academy articles',
				nodes: [
					{
						type: 'permission',
						id: 'academyFiles_view',
						name: 'academyFiles_view',
						title: 'View'
					}
				]
			}
		]
	},{
		type: 'space',
		id: 3,
		title: 'Administrator',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'writers',
				nodes: [
					{
						type: 'permission',
						id: 'writers_view',
						name: 'writers_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'writers_addWriter',
								name: 'writers_addWriter',
								title: 'Add writer'
							},{
								type: 'permission',
								id: 'writers_editWriter',
								name: 'writers_editWriter',
								title: 'Edit writer'
							}
						]
					}
				]
			},{
				type: 'screen',
				id: 2,
				title: 'permissions',
				nodes: [
					{
						type: 'permission',
						id: 'permissions_view',
						name: 'permissions_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'permissions_addRole',
								name: 'permissions_addRole',
								title: 'Add group'
							},{
								type: 'permission',
								id: 'permissions_editRole',
								name: 'permissions_editRole',
								title: 'Edit group'
							}
						]
					}
				]
			},{
				type: 'group',
				id: 3,
				title: 'Markets',
				nodes: [
					{
						type: 'screen',
						id: 2,
						title: 'Create Market',
						nodes: [
							{
								type: 'permission',
								id: 'createMarket_view',
								name: 'createMarket_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Fields',
						nodes: [
							{
								type: 'permission',
								id: 'marketFields_view',
								name: 'marketFields_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Formulas',
						nodes: [
							{
								type: 'permission',
								id: 'marketFormulas_view',
								name: 'marketFormulas_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Translations',
						nodes: [
							{
								type: 'permission',
								id: 'marketTranslations_view',
								name: 'marketTranslations_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Schedule',
						nodes: [
							{
								type: 'permission',
								id: 'marketSchedule_view',
								name: 'marketSchedule_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Priorities',
						nodes: [
							{
								type: 'permission',
								id: 'marketPriorities_view',
								name: 'marketPriorities_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Pauses',
						nodes: [
							{
								type: 'permission',
								id: 'marketPauses_view',
								name: 'marketPauses_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Dynamics Quotes',
						nodes: [
							{
								type: 'permission',
								id: 'dynamicsQuoteParams_view',
								name: 'dynamicsQuoteParams_view',
								title: 'View'
							}
						]
					},{
						type: 'screen',
						id: 2,
						title: 'Market Option Plus Prices',
						nodes: [
							{
								type: 'permission',
								id: 'optionPlusPrices_view',
								name: 'optionPlusPrices_view',
								title: 'View'
							}
						]
					}
				]
			}
		]
	},{
		type: 'space',
		id: 4,
		title: 'Users',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'File',
				nodes: [
					{
						type: 'permission',
						id: 'file_view',
						name: 'file_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'file_save',
								name: 'file_save',
								title: 'Save'
							},{
								type: 'permission',
								id: 'file_controlApprove',
								name: 'file_controlApprove',
								title: 'Control Approve'
							},{
								type: 'permission',
								id: 'file_controlReject',
								name: 'file_controlReject',
								title: 'Control Reject'
							},{
								type: 'permission',
								id: 'file_sendToKeesing',
								name: 'file_sendToKeesing',
								title: 'Send to Keesing'
							},{
								type: 'permission',
								id: 'file_updateFirstName',
								name: 'file_updateFirstName',
								title: 'Update first name'
							},{
								type: 'permission',
								id: 'file_updateLastName',
								name: 'file_updateLastName',
								title: 'Update last name'
							}
						]
					}
				]
			},{
				type: 'screen',
				id: 1,
				title: 'Complaint files',
				nodes: [
					{
						type: 'permission',
						id: 'complaintFiles_view',
						name: 'complaintFiles_view',
						title: 'View'
					}
				]
			},{
				type: 'screen',
				id: 1,
				title: 'Questionnaire',
				nodes: [
					{
						type: 'permission',
						id: 'questionnaire_view',
						name: 'questionnaire_view',
						title: 'View'
					},{
						type: 'permission',
						id: 'questionnaire_save',
						name: 'questionnaire_save',
						title: 'Save'
					}
				]
			},{
				type: 'space',
				id: 4,
				title: 'Withdraws',
				nodes: [
					{
						type: 'screen',
						id: 1,
						title: 'Direct Banking',
						nodes: [
							{
								type: 'permission',
								id: 'directBanking_view',
								name: 'directBanking_view',
								title: 'View'
							},{
								type: 'permission',
								id: 'directBanking_save',
								name: 'directBanking_save',
								title: 'Save'
							},{
								type: 'permission',
								id: 'directBanking_feeExempt',
								name: 'directBanking_feeExempt',
								title: 'Fee exempt'
							}
						]
					}
				]
			},{
				type: 'space',
				id: 4,
				title: 'User details',
				nodes: [
					{
						type: 'screen',
						id: 1,
						title: 'Bank wire/Skill details',
						nodes: [
							{
								type: 'permission',
								id: 'withdrawUserDetails_view',
								name: 'withdrawUserDetails_view',
								title: 'View'
							},{
								type: 'permission',
								id: 'withdrawUserDetails_save',
								name: 'withdrawUserDetails_save',
								title: 'Save'
							}
						]
					}
				]
			},{
				type: 'space',
				id: 5,
				title: 'Account Transactions',
				nodes: [
					{
						type: 'screen',
						id: 1,
						title: 'Deposits Summary',
						nodes: [
							{
								type: 'permission',
								id: 'depositsSummary_view',
								name: 'depositsSummary_view',
								title: 'View'
							}
						]
					}
				]
			}
		]
	},{
		type: 'space',
		id: 5,
		title: 'Documents',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'idsFile',
				nodes: [
					{
						type: 'permission',
						id: 'idsFile_view',
						name: 'idsFile_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'idsFile_save',
								name: 'idsFile_save',
								title: 'Save'
							},{
								type: 'permission',
								id: 'idsFile_controlApprove',
								name: 'idsFile_controlApprove',
								title: 'Control Approve'
							},{
								type: 'permission',
								id: 'idsFile_controlReject',
								name: 'idsFile_controlReject',
								title: 'Control Reject'
							},{
								type: 'permission',
								id: 'idsFile_updateFirstName',
								name: 'idsFile_updateFirstName',
								title: 'Update first name'
							},{
								type: 'permission',
								id: 'idsFile_updateLastName',
								name: 'idsFile_updateLastName',
								title: 'Update last name'
							}
						]
					}
				]
			},{
				type: 'screen',
				id: 2,
				title: 'Call center',
				nodes: [
					{
						type: 'permission',
						id: 'callCenter_view',
						name: 'callCenter_view',
						title: 'View'
					}
				]
			},{
				type: 'screen',
				id: 3,
				title: 'Back office',
				nodes: [
					{
						type: 'permission',
						id: 'backOffice_view',
						name: 'backOffice_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'backOffice_regulationApprove',
								name: 'backOffice_regulationApprove',
								title: 'Regulation approve'
							},{
								type: 'permission',
								id: 'backOffice_regulationApproveManager',
								name: 'backOffice_regulationApproveManager',
								title: 'Regulation approve (Managers)'
							}
						]
					}
				]
			},{
				type: 'screen',
				id: 4,
				title: 'From inbox',
				nodes: [
					{
						type: 'permission',
						id: 'fromInbox_view',
						name: 'fromInbox_view',
						title: 'View'
					}
				]
			}
		]
	},{
		type: 'space',
		id: 6,
		title: 'Marketing',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Promotion management',
				nodes: [
					{
						type: 'permission',
						id: 'promotionManagement_view',
						name: 'promotionManagement_view',
						title: 'View'
					}
				]
			}
		]
	},{
		type: 'space',
		id: 7,
		title: 'Sales',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Country configuration',
				nodes: [
					{
						type: 'permission',
						id: 'countryGroup_view',
						name: 'countryGroup_view',
						title: 'View'
					}
				]
			},{
				type: 'screen',
				id: 2,
				title: 'Call Status',
				nodes: [
					{
						type: 'permission',
						id: 'callStatus_view',
						name: 'callStatus_view',
						title: 'View'
					},{
						type: '',
						id: 3,
						title: 'Actions',
						nodes: [
							{
								type: 'permission',
								id: 'callStatus_manager',
								name: 'callStatus_manager',
								title: 'Manager'
							}
						]
					}
				]
			}
		]
	},{
		type: 'space',
		id: 8,
		title: 'Risk',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Chargebacks',
				nodes: [
					{
						type: 'permission',
						id: 'chargebacks_view',
						name: 'chargebacks_view',
						title: 'View'
					},{
						type: '',
						id: 2,
						title: 'Actions',
						nodes: [{
							type: 'permission',
							id: 'chargebacks_add',
							name: 'chargebacks_add',
							title: 'Add'
						},{
							type: 'permission',
							id: 'chargebacks_edit',
							name: 'chargebacks_edit',
							title: 'Edit'
						}]
					}
				]
			}
		]
	},{
		type: 'space',
		id: 8,
		title: 'Trader',
		nodes: [
			{
				type: 'screen',
				id: 1,
				title: 'Traders-actions',
				nodes: [
					{
						type: 'permission',
						id: 'tradersActions_view',
						name: 'tradersActions_view',
						title: 'View'
					}
				]
			}
		]
	}
]