backendApp.controller('AdminCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	
}]);

backendApp.controller('PermissionsCtr', ['$rootScope', '$scope', '$http', '$state', 'CheckboxTree', function($rootScope, $scope, $http, $state, CheckboxTree) {
	//Expose the static methods of the service to the template
	$scope.CheckboxTree = {};
	$scope.CheckboxTree.toggleNode = CheckboxTree.InstanceClass.toggleNode;
	$scope.CheckboxTree.updateTree = CheckboxTree.InstanceClass.updateTree;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	$scope.filter.roles = [{id: 0, name: ''}];
	$scope.filter.role = $scope.filter.roles[0];
	$scope.filter.roleName = '';
	
	$scope.selectedRole = null;
	$scope.result = null;
	
	$scope.writersList = {};
	$scope.permissionsList = {};
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getWriterPermisionsScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getWriterPermisionsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.roleId){
			$scope.action = 'edit';
			$scope.result = null;
			for(var i = 0; i < $scope.filter.roles.length; i++){
				if($scope.filter.roles[i].id == $rootScope.$stateParams.roleId){
					$scope.filter.role = $scope.filter.roles[i];
					$scope.filter.roleName = $scope.filter.role.name;
					$scope.getEditPermissions();
					break;
				}
			}
		}else if($rootScope.$state.includes("ln.admin.permissions.add")){
			$scope.action = 'add';
			$scope.result = null;
			$scope.filter.roleName = '';
			$scope.getAddPermissions();
		}else{
			$scope.action = '';
			$scope.filter.roleName = '';
			if($scope.selectedRole){
				$scope.getViewPermissions();
			}else{
				$scope.writersList = {};
				$scope.permissionsList = {};
			}
		}
	}
	
	
	$scope.getWriterPermisionsScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'WriterPermissionsServices/getWriterPermisionsScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				$rootScope.fillFilter({arr: $scope.filter.roles, data: data, filter: 'roles'});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriterPermisionsScreenSettings');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getWriterPermisionsScreenSettings');
			})
	}
	
	
	$scope.getViewPermissions = function(){
		$rootScope.resetGlobalErrorMsg();
		if($scope.filter.role.id == 0){
			$scope.selectedRole = null;
			$scope.writersList = {};
			$scope.permissionsList = {};
			return;
		}
		showLoading();
		$scope.selectedRole = $scope.filter;
		var editDependencies = {name: 'editDependencies', dependencies: {getDepartmentWriterByRole: false, getPermissionsByRole: true}};
		$rootScope.addDependency($scope, editDependencies, function(){hideLoading();});
		
		$scope.getDepartmentWriterByRole($scope.selectedRole.role.id, false, editDependencies.name);
		$scope.getPermissionsByRole($scope.selectedRole.role.id, false, editDependencies.name);
	}
	
	$scope.getEditPermissions = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		$scope.selectedRole = $scope.filter;
		var editDependencies = {name: 'editDependencies', dependencies: {getDepartmentWriterByRole: false, getPermissionsByRole: true}};
		$rootScope.addDependency($scope, editDependencies, function(){hideLoading();});
		
		$scope.getDepartmentWriterByRole(false, true, editDependencies.name);
		$scope.getPermissionsByRole($scope.selectedRole.role.id, true, editDependencies.name);
	}
	
	$scope.getAddPermissions = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var addDependencies = {name: 'addDependencies', dependencies: {getDepartmentWriterByRole: false}};
		$rootScope.addDependency($scope, addDependencies, function(){hideLoading();});
		
		$scope.getDepartmentWriterByRole(false, true, addDependencies.name);
		$scope.getPermissionsByRole(false, true);
	}
	
	$scope.getDepartmentWriterByRole = function(roleId, active, dependencyName){
		var methodRequest = {};
		if(roleId){
			methodRequest.roleId = roleId;
		}
		$http.post(settings.backendJsonLink + 'WriterPermissionsServices/getDepartmentWriterByRole', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				var departments = [];
				for(var i = 0; i < data.writers.length; i++){
					var department = null;
					for(var j = 0; j < departments.length; j++){
						if(departments[j].title == data.writers[i].departmentName){
							department = departments[j];
						}
					}
					if(!department){
						department = {title: data.writers[i].departmentName};
						if($scope.action == 'add'){
							department.collapsed = true;
						}
						if(!active){
							if(data.writers[i].id > 0 && data.writers[i].isMultitude){
								department.checkedStatus = 1;
							}else if(data.writers[i].id > 0){
								department.checkedStatus = 2;
							}
						}
						departments.push(department);
					}
					if(!department.nodes){
						department.nodes = [];
					}
					if(data.writers[i].id > 0){
						var checked = true;
						if(active){
							if($scope.action == 'add' || !$scope.selectedRole || (data.writers[i].roleId != $scope.selectedRole.role.id)){
								checked = false;
							}
						}
						department.nodes.push({id: data.writers[i].id, title: data.writers[i].userName, checked: checked});
					}
				}
				$scope.writersList = new CheckboxTree.InstanceClass(departments, active);
				if(active){
					$scope.writersList.normalizeCheckedStatus();
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDepartmentWriterByRole')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getDepartmentWriterByRole');
			})
	}
	
	$scope.getPermissionsByRole = function(roleId, active, dependencyName){
		var methodRequest = {};
		if(roleId){
			methodRequest.roleId = roleId;
		}else{
			$scope.permissionsList = new CheckboxTree.InstanceClass(permissions, active);
			$scope.permissionsList.normalizeCheckedStatus();
			return;
		}
		$http.post(settings.backendJsonLink + 'WriterPermissionsServices/getPermissionsByRole', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.permissionsList = new CheckboxTree.InstanceClass(permissions, active);
				for(var i = 0; i < data.permissionsList.length; i++){
					$scope.permissionsList.checkNode($scope.permissionsList.root, data.permissionsList[i]);
				}
				
				$scope.permissionsList.normalizeCheckedStatus();
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPermissionsByRole')
			})
			.finally(function(){
				$rootScope.markDependencyDone($scope, dependencyName, 'getPermissionsByRole');
			})
	}
	
	$scope.savePermissions = function(){
		showLoading();
		var service = 'addRolePermissions';
		if($scope.action == 'edit'){
			service = 'editRolePermissions';
		}
		
		var methodRequest = {};
		if($scope.action == 'edit'){
			methodRequest.roleId = $scope.selectedRole.role.id;
			var unsetWriterList = $scope.writersList.getUncheckedNodes(true);
			if(unsetWriterList.length > 0){
				methodRequest.unsetWriterList = unsetWriterList;
			}
		}
		methodRequest.roleName = $scope.filter.roleName;
		var permissionList = $scope.permissionsList.getCheckedNodes(false);
		if(permissionList.length > 0){
			methodRequest.permissionList = permissionList;
		}else{
			hideLoading();
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('permissions.error.add.permission'));
			return;
		}
		var writerList = $scope.writersList.getCheckedNodes(true);
		if(writerList.length > 0){
			methodRequest.writerList = writerList;
		}
		
		$http.post(settings.backendJsonLink + 'WriterPermissionsServices/' + service, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.result = {roleName: $scope.filter.roleName};
				if(data.existWriterRoleList && data.existWriterRoleList.length > 0){
					$scope.result.existWriterRoleList = data.existWriterRoleList;
				}
				if($scope.action == 'edit'){
					if($scope.selectedRole.role.id == $rootScope.writer.roleId){
						$scope.result.forceRelogin = true;
					}
					$scope.filter.role.name = $scope.filter.roleName;
					$rootScope.$state.go('ln.admin.permissions', {ln: $state.params.ln});
				}else{
					showLoading();
					$scope.selectedRole = null;
					$scope.filter.role = $scope.filter.roles[0];
					var saveDependencies = {name: 'saveDependencies', dependencies: {getWriterPermisionsScreenSettings: false}};
					$rootScope.addDependency($scope, saveDependencies, function(){
						hideLoading();
						$rootScope.$state.go('ln.admin.permissions', {ln: $state.params.ln});
					});
					$scope.getWriterPermisionsScreenSettings(saveDependencies.name);
				}
				$scope.filter.roleName = '';
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, service);
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.closeResult = function(){
		$scope.result = null;
	}
	
}]);

backendApp.controller('WritersCtr', ['$rootScope', '$scope', '$http', 'CheckboxTree', 'SingleSelect', 'Multiselect', 'Pagination', '$upload', '$timeout', function($rootScope, $scope, $http, CheckboxTree, SingleSelect, Multiselect, Pagination, $upload, $timeout) {
	//Expose the static methods of the service to the template
	$scope.CheckboxTree = {};
	$scope.CheckboxTree.toggleNode = CheckboxTree.InstanceClass.toggleNode;
	$scope.CheckboxTree.updateTree = CheckboxTree.InstanceClass.updateTree;

	$scope.DEPARTMENT_RETENTION = 3;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};

	$scope.filter.department = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, isSmart: true});
	$scope.filter.skins = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.role = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, isSmart: true});
	$scope.filter.site = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});

	$scope.filter.freeTextFilter = '';

	$scope.writersList = null;
	$scope.selectedWriter = {};

	$scope.initFiltersOnReady = function(){
		$scope.filter.isActive = [{id: -2, name: $rootScope.getMsgs('all')}];
		$scope.filter.isActiveStatus = $scope.filter.isActive[0];
		$scope.filter.salesTypeFilter = [{id: 0, name: $rootScope.getMsgs('none')}];
		$scope.filter.salesType = $scope.filter.salesTypeFilter[0];
	}

	$scope.changePage = function(page){
		$scope.getAll(page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});

	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getWriterScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getWriterScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.writerId){
			$scope.action = 'edit';
			$scope.selectedWriter = {};
			$scope.selectedWriter.writerId = $rootScope.$stateParams.writerId;
			$scope.getWriter();
		}else if($rootScope.$state.includes("ln.admin.writers.add")){
			$scope.action = 'add';
			$scope.selectedWriter = {};
			$scope.initWriterFilters();
		}else{
			$scope.action = '';
		}
	}
	
	
	$scope.getWriterScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'WriterServices/getWriterScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				$rootScope.fillFilter({arr: $scope.filter.isActive, data: data, filter: 'isActive'});
				$scope.filter.department.fillOptions(data.screenFilters.departments);
				$scope.filter.role.fillOptions(data.screenFilters.roles);
				$scope.filter.skins.fillOptions(data.screenFilters.skins);
				$scope.filter.site.fillOptions(data.screenFilters.site);
				$scope.filter.isActiveStatus = $scope.filter.isActive[2];

				$rootScope.fillFilter({arr: $scope.filter.salesTypeFilter, data: data, filter: 'salesType'});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriterScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getWriterScreenSettings');
			})
	}

	$scope.initWriterFilters = function(){
		$scope.selectedWriter.filter = {};
		$scope.selectedWriter.filter.departments = cloneObj($scope.filter.department.options);
		$scope.selectedWriter.filter.departments.splice(0, 1);
		$scope.selectedWriter.filter.department = $rootScope.getFilterElById($scope.selectedWriter.filter.departments, $scope.selectedWriter.departmentId);
		
		$scope.selectedWriter.filter.sites = cloneObj($scope.filter.site.options);
		$scope.selectedWriter.filter.sites.splice(0, 1);
		$scope.selectedWriter.filter.site = $rootScope.getFilterElById($scope.selectedWriter.filter.sites, $scope.selectedWriter.site);
		
		$scope.selectedWriter.filter.roles = cloneObj($scope.filter.role.options);
		$scope.selectedWriter.filter.roles.splice(0, 1);
		$scope.selectedWriter.filter.role = $rootScope.getFilterElById($scope.selectedWriter.filter.roles, $scope.selectedWriter.roleId);

		$scope.selectedWriter.filter.salesTypeFilter = cloneObj($scope.filter.salesTypeFilter);
		if(!$scope.selectedWriter.salesType || $scope.selectedWriter.salesType == 0){
			$scope.selectedWriter.filter.salesType = $scope.selectedWriter.filter.salesTypeFilter[0];
		}else{
			$scope.selectedWriter.filter.salesType = $rootScope.getFilterElById($scope.selectedWriter.filter.salesTypeFilter, $scope.selectedWriter.salesType);
		}

		$scope.selectedWriter.filter.isActiveFilter = cloneObj($scope.filter.isActive);
		$scope.selectedWriter.filter.isActiveFilter.splice(0, 1);
		$scope.selectedWriter.filter.isActive = $rootScope.getFilterElById($scope.selectedWriter.filter.isActiveFilter, $scope.selectedWriter.isActive);
		
		var skins = cloneObj($rootScope.getSkinsTree());
		if($scope.selectedWriter.skins || $scope.action == 'add'){
			for(var i = 0; i < skins[0].nodes.length; i++){
				for(var j = 0; j < skins[0].nodes[i].nodes.length; j++){
					if($scope.action == 'add'){
						skins[0].nodes[i].nodes[j].checked = true;
					}else{
						if($scope.selectedWriter.skins.indexOf(parseInt(skins[0].nodes[i].nodes[j].id)) != -1){
							skins[0].nodes[i].nodes[j].checked = true;
						}
					}
				}
			}
		}
		$scope.selectedWriter.filter.skins = new CheckboxTree.InstanceClass(skins, true);
		$scope.selectedWriter.filter.skins.normalizeCheckedStatus();
	}

	$scope.getWriter = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.writerId = $scope.selectedWriter.writerId;
		$http.post(settings.backendJsonLink + 'WriterServices/getAll', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				if(data.writers.length > 0){
					$scope.selectedWriter = data.writers[0];
					$scope.initWriterFilters();
				}else{
					//error
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWriter')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	
	$scope.getAll = function(page){
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		
		showLoading();
		var methodRequest = {};
		methodRequest.page = page;

		$scope.filter.department.getCheckedIds().length > 0 ? methodRequest.departments = $scope.filter.department.getCheckedIds() : '';
		$scope.filter.role.getCheckedIds().length > 0 ? methodRequest.roles = $scope.filter.role.getCheckedIds() : '';

		if($scope.filter.isActiveStatus.id >= 0){
			methodRequest.isActive = $scope.filter.isActiveStatus.id;
		}
		$scope.filter.skins.getCheckedIds().length > 0 ? methodRequest.skins = $scope.filter.skins.getCheckedIds() : '';
		methodRequest.freeTextFilter = $scope.filter.freeTextFilter;
		$scope.filter.site.getCheckedIds().length > 0 ? methodRequest.site = $scope.filter.site.getCheckedIds() : '';
		$http.post(settings.backendJsonLink + 'WriterServices/getAll', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.writersList = data.writers;
				$scope.pagination.setPages(page, data.totalCount, $scope.resultsPerPage);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getAll');
			})
			.finally(function() {
				hideLoading();
			})
	}

	$scope.parseSkins = function (skins){
		var result = '';

		var hasAllRegulated = true;
		var hasAllGoldbeam = true;
		var hasAllNonRegulated = true;
		var hasEtrader = true;
		for(key in skinMap){
			if(skinMap.hasOwnProperty(key)){
				if(!skinMap[key].isActive){
					continue;
				}
				//regulated
				if(skinMap[key].isRgulated && !skinMap[key].isGoldbeam){
					if(skins.indexOf(parseInt(key)) == -1){
						hasAllRegulated = false;
					}
				}
				//goldbeam
				if(skinMap[key].isGoldbeam){
					if(skins.indexOf(parseInt(key)) == -1){
						hasAllGoldbeam = false;
					}
				}
				//nonregulated
				if(!skinMap[key].isRgulated && skinMap[key].platforms.indexOf(1) == -1 && !skinMap[key].isGoldbeam){
					if(skins.indexOf(parseInt(key)) == -1){
						hasAllNonRegulated = false;
					}
				}
				//etrader
				if(skinMap[key].platforms.indexOf(1) != -1){
					if(skins.indexOf(parseInt(key)) == -1){
						hasEtrader = false;
					}
				}
			}

		}

		if(hasAllRegulated){
			if(result){
				result += ', ';
			}
			result += $rootScope.getMsgs('ouroboros');
		}
		if(hasAllGoldbeam){
			if(result){
				result += ', ';
			}
			result += $rootScope.getMsgs('goldbeam');
		}
		if(hasAllNonRegulated){
			if(result){
				result += ', ';
			}
			result += $rootScope.getMsgs('aops');
		}
		if(hasEtrader){
			if(result){
				result += ', ';
			}
			result += $rootScope.getMsgs('etrader');
		}
		for(var i = 0; i < skins.length; i++){
			if(skinMap[skins[i]]){
				var addSkin = false;
				if(skinMap[skins[i]].isGoldbeam && !hasAllGoldbeam){
					addSkin = true;
				}else if(!skinMap[skins[i]].isGoldbeam && skinMap[skins[i]].isRgulated){
					if(!hasAllRegulated){
						addSkin = true;
					}
				}else if(!skinMap[skins[i]].isGoldbeam && !skinMap[skins[i]].isRgulated && skinMap[skins[i]].platforms.indexOf(1) == -1){
					if(!hasAllNonRegulated){
						addSkin = true;
					}
				}
				if(addSkin){
					if(result){
						result += ', ';
					}
					result += $rootScope.getMsgs(skinMap[skins[i]].skinTranslation);
				}
			}
		}
		return result;
	}

	$scope.saveWriter = function(_form){
		var service = 'insertWriter';
		if($scope.action == 'edit'){
			service = 'updateWriter';
		}
		if (_form.$valid) {
			showLoading();
			
			var methodRequest = {};
			if($scope.action == 'edit'){
				methodRequest.id = $scope.selectedWriter.id;
				methodRequest.isActive = $scope.selectedWriter.filter.isActive.id;
			}
			methodRequest.firstName = $scope.selectedWriter.firstName;
			methodRequest.lastName = $scope.selectedWriter.lastName;
			methodRequest.email = $scope.selectedWriter.email;
			methodRequest.mobilePhone = $scope.selectedWriter.mobilePhone;
			methodRequest.nickNameFirst = $scope.selectedWriter.nickNameFirst;
			methodRequest.nickNameLast = $scope.selectedWriter.nickNameLast;
			methodRequest.employeeId = $scope.selectedWriter.employeeId;
			if($scope.selectedWriter.filter.department && $scope.selectedWriter.filter.department.id){
				methodRequest.departmentId = $scope.selectedWriter.filter.department.id;
			}
			if($scope.selectedWriter.filter.role){
				methodRequest.roleId = $scope.selectedWriter.filter.role.id;
			}
			if($scope.selectedWriter.filter.site){
				methodRequest.site = $scope.selectedWriter.filter.site.id;
			}
			if($scope.selectedWriter.filter.salesType && $scope.selectedWriter.filter.salesType.id > 0){
				methodRequest.salesType = $scope.selectedWriter.filter.salesType.id;
			}else if(!($scope.selectedWriter.filter.department && $scope.selectedWriter.filter.department.id == $scope.DEPARTMENT_RETENTION) && $scope.selectedWriter.filter.salesType.id == 0){
				methodRequest.salesType = $scope.selectedWriter.salesType;
			}
			methodRequest.skins = $scope.selectedWriter.filter.skins.getCheckedNodes(false);
			
			$http.post(settings.backendJsonLink + 'WriterServices/' + service, methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$rootScope.$state.go('ln.admin.writers', {ln: $scope.$state.params.ln});
					$scope.getAll();
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, service)
				})
				.finally(function() {
					hideLoading();
				})
		} else {
			var data = {
				errorCode: 200
			}
			$rootScope.handleErrors(data, service);
		}
	}
}]);

backendApp.controller('CreateMarketCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getCreateMarketScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getCreateMarketScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getCreateMarketScreenSettings = function(dependencyName){
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'CreateMarketServices/getCreateMarketScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for (var i = 0; i < data.permissionsList.length; i++) {
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				$rootScope.fillFilter({arr: $scope.filter.markets, holder: data.filters.markets, filter: '1'});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getCreateMarketScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getCreateMarketScreenSettings');
			})
	}
	
	$scope.copyMarket = function(){
		if($scope.filter.market.id <= 0){
			return;
		}
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = $scope.filter.market.id;
		$http.post(settings.backendJsonLink + 'CreateMarketServices/copyMarket', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$rootScope.$state.go('ln.admin.markets.market_fields.edit', {ln: $state.params.ln, marketId: data.marketId, inWizard: true});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'copyMarket');
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketFieldsCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.marketsTypes = [];
	$scope.filter.marketType = null;
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	
	$scope.marketFields = null;
	$scope.marketFieldTypes = {};
	$scope.marketFieldsDropdowns = {};
	$scope.marketFieldsDropdowns.marketGroupId = [];
	$scope.marketFieldsDropdowns.exchangeId = [];
	$scope.marketFieldsDropdowns.investmentLimitGroupId = [];
	$scope.marketFieldsDropdowns.typeOfShifting = [];
	$scope.marketFieldsDropdowns.bubblesPricingLevel = [];
	
	$scope.assetIndex = null;
	$scope.assetIndexFields = {};
	$scope.assetIndexFields.hidden = ['id', 'startTime', 'endTime'];
	$scope.assetIndexFields.checkboxes = ['isFullDay', 'twentyFourSeven'];
	
	$scope.emirReport = null;
	$scope.emirReportFields = {};
	$scope.emirReportFields.hidden = ['assetId'];
	$scope.emirReportFields.checkboxes = [];
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getMarketFieldsScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getMarketFieldsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.getMarketFields($scope.marketId);
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.marketFields = null;
			$scope.assetIndex = null;
		}
	}
	
	$scope.getMarketFieldsScreenSettings = function(dependencyName){
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'EditMarketFieldsServices/getMarketFieldsScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(type in data.filters.markets){
					if(data.filters.markets.hasOwnProperty(type)){
						var markets = [];
						for(market in data.filters.markets[type]){
							if(data.filters.markets[type][market]){
								markets.push({id: market, name: data.filters.markets[type][market]});
							}
						}
						$scope.filter.marketsTypes.push({id: type, markets: markets});
					}
				}
				if($scope.filter.marketsTypes.length > 0){
					$scope.filter.marketType = $scope.filter.marketsTypes[0];
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketFieldsScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getMarketFieldsScreenSettings');
			})
	}
	
	$scope.editMarketFields = function(marketId){
		$rootScope.$state.go('ln.admin.markets.market_fields.edit', {ln: $state.params.ln, marketId: marketId});
	}
	
	$scope.getMarketFields = function(marketId){
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'EditMarketFieldsServices/getMarketFields', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketFields = {};
				$scope.assetIndex = {};
				for(field in data.market){
					if(data.market.hasOwnProperty(field)){
						if(data.marketFieldTypes.amounts.indexOf(field) != -1){
							$scope.marketFields[field] = $rootScope.formatAmount({amount: data.market[field], amountOnly: true});
						}else{
							$scope.marketFields[field] = data.market[field];
						}
					}
				}
				$scope.marketFieldTypes = data.marketFieldTypes;
				$scope.marketFieldsDropdowns.marketGroupId = [];
				$scope.marketFieldsDropdowns.exchangeId = [];
				$scope.marketFieldsDropdowns.investmentLimitGroupId = [];
				$scope.marketFieldsDropdowns.typeOfShifting = [];
				$scope.marketFieldsDropdowns.bubblesPricingLevel = [];
				for(key in data.marketGroups){
					if(data.marketGroups.hasOwnProperty(key)){
						$scope.marketFieldsDropdowns.marketGroupId.push({id: key, name: $rootScope.getMsgs(data.marketGroups[key].displayName)});
					}
				}
				for(key in data.exchanges){
					if(data.exchanges.hasOwnProperty(key)){
						$scope.marketFieldsDropdowns.exchangeId.push({id: data.exchanges[key].id, name: data.exchanges[key].name});
					}
				}
				for(key in data.investmentLimitGroups){
					if(data.investmentLimitGroups.hasOwnProperty(key)){
						$scope.marketFieldsDropdowns.investmentLimitGroupId.push({id: data.investmentLimitGroups[key].id, name: data.investmentLimitGroups[key].groupName + data.investmentLimitGroups[key].amountPerCurrencyDisplay});
					}
				}
				for(key in data.typeOfShifting){
					if(data.typeOfShifting.hasOwnProperty(key)){
						$scope.marketFieldsDropdowns.typeOfShifting.push({id: key, name: data.typeOfShifting[key]});
					}
				}
				for(key in data.bubblesPricingLevels){
					if(data.bubblesPricingLevels.hasOwnProperty(key)){
						$scope.marketFieldsDropdowns.bubblesPricingLevel.push({id: key, name: $rootScope.getMsgs(data.bubblesPricingLevels[key])});
					}
				}
				
				$scope.assetIndex = data.assetIndex;
				$scope.emirReport = data.emirReport;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketFields')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.fieldIsDisabled = function(field){
		if($scope.action != 'edit' || ($scope.marketFieldTypes.disabled && $scope.marketFieldTypes.disabled.indexOf(field) != -1)){
			return true;
		}
		return false;
	}
	$scope.fieldIsHidden = function(field){
		if($scope.marketFieldTypes.hidden && $scope.marketFieldTypes.hidden.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	$scope.fieldIsCheckbox = function(field){
		if($scope.marketFieldTypes.checkboxes && $scope.marketFieldTypes.checkboxes.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	$scope.fieldIsDropdown = function(field){
		if($scope.marketFieldTypes.dropdowns && $scope.marketFieldTypes.dropdowns.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	$scope.fieldIsInput = function(field){
		if($scope.fieldIsCheckbox(field) || $scope.fieldIsDropdown(field)){
			return false;
		}
		return true;
	}
	$scope.fieldIsTimestamp = function(field){
		if($scope.marketFieldTypes.timestamp && $scope.marketFieldTypes.timestamp.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	
	$scope.assetIndexFieldIsDisabled = function(field){
		if($scope.action != 'edit'){
			return true;
		}
		return false;
	}
	$scope.assetIndexFieldIsHidden = function(field){
		if($scope.assetIndexFields.hidden && $scope.assetIndexFields.hidden.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	$scope.assetIndexFieldIsCheckbox = function(field){
		if($scope.assetIndexFields.checkboxes && $scope.assetIndexFields.checkboxes.indexOf(field) != -1){
			return true;
		}
		return false;
	}

	$scope.emirReportFieldIsDisabled = function(field){
		if($scope.action != 'edit'){
			return true;
		}
		return false;
	}
	$scope.emirReportFieldIsHidden = function(field){
		if($scope.emirReportFields.hidden && $scope.emirReportFields.hidden.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	$scope.emirReportFieldIsCheckbox = function(field){
		if($scope.emirReportFields.checkboxes && $scope.emirReportFields.checkboxes.indexOf(field) != -1){
			return true;
		}
		return false;
	}
	
	$scope.updateMarketFields = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.market = {};
		methodRequest.marketId = $scope.marketId;
		for(field in $scope.marketFields){
			if($scope.marketFields.hasOwnProperty(field)){
				if($scope.marketFieldTypes.amounts.indexOf(field) != -1){
					methodRequest.market[field] = $rootScope.formatAmountForDb({amount: $scope.marketFields[field]});
				}else{
					methodRequest.market[field] = $scope.marketFields[field];
				}
			}
		}
		methodRequest.assetIndex = $scope.assetIndex;
		methodRequest.emirReport = $scope.emirReport;
		$http.post(settings.backendJsonLink + 'EditMarketFieldsServices/updateMarketFields', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				if($rootScope.$stateParams.inWizard){
					$rootScope.$state.go('ln.admin.markets.market_formulas.edit', {ln: $state.params.ln, marketId: $scope.marketId, inWizard: true});
				}else{
					$rootScope.$state.go('ln.admin.markets.market_fields', {ln: $state.params.ln});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateMarketFields')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketFormulasCtr', ['$rootScope', '$scope', '$http', '$state', 'SingleSelect', function($rootScope, $scope, $http, $state, SingleSelect) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.marketsTypes = [];
	$scope.filter.marketType = null;
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	
	$scope.marketFormulas = null;
	
	$scope.formulaTypesFilter = {};
	$scope.formulaTypesFilter.dayClosing = new SingleSelect.InstanceClass({});
	$scope.formulaTypesFilter.hourClosing = new SingleSelect.InstanceClass({});
	$scope.formulaTypesFilter.hourLevel = new SingleSelect.InstanceClass({});
	$scope.formulaTypesFilter.longTerm = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: ''}});
	$scope.formulaTypesFilter.realLevel = new SingleSelect.InstanceClass({});
	
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getMarketConfigFieldsScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getMarketConfigFieldsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.getMarketFormulas($scope.marketId);
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.marketFormulas = null;
		}
	}
	
	$scope.getMarketConfigFieldsScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'EditMarketConfigServices/getMarketConfigFieldsScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(type in data.filters.markets){
					if(data.filters.markets.hasOwnProperty(type)){
						var markets = [];
						for(market in data.filters.markets[type]){
							if(data.filters.markets[type][market]){
								markets.push({id: market, name: data.filters.markets[type][market]});
							}
						}
						$scope.filter.marketsTypes.push({id: type, markets: markets});
					}
				}
				if ($scope.filter.marketsTypes.length > 0) {
					$scope.filter.marketType = $scope.filter.marketsTypes[0];
				}
				if(data.formulas){
					var formulaTypes = {};
					formulaTypes['1'] = [];
					formulaTypes['2'] = [];
					for(var i = 0; i < data.formulas['1'].length; i++){
						formulaTypes['1'].push(data.formulas['1'][i]);
					}
					for(var i = 0; i < data.formulas['2'].length; i++){
						formulaTypes['2'].push(data.formulas['2'][i]);
					}
					
					$scope.formulaTypesFilter.dayClosing.fillOptions(formulaTypes['1']);
					$scope.formulaTypesFilter.hourClosing.fillOptions(formulaTypes['1']);
					$scope.formulaTypesFilter.hourLevel.fillOptions(formulaTypes['1']);
					$scope.formulaTypesFilter.longTerm.fillOptions(formulaTypes['2']);
					$scope.formulaTypesFilter.realLevel.fillOptions(formulaTypes['1']);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketConfigFieldsScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getMarketConfigFieldsScreenSettings');
			})
	}
	
	$scope.editMarketFormulas = function(marketId){
		$rootScope.$state.go('ln.admin.markets.market_formulas.edit', {ln: $state.params.ln, marketId: marketId});
	}
	
	$scope.getMarketFormulas = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'EditMarketConfigServices/getMarketConfigFields', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketFormulas = {};
				if(data.marketConfig){
					$scope.marketFormulas.dayClosingFormulaParams = $scope.objectToTree(JSON.parse(data.marketConfig.dayClosingFormulaParams));
					$scope.marketFormulas.hourClosingFormulaParams = $scope.objectToTree(JSON.parse(data.marketConfig.hourClosingFormulaParams));
					$scope.marketFormulas.hourLevelFormulaParams = $scope.objectToTree(JSON.parse(data.marketConfig.hourLevelFormulaParams));
					$scope.marketFormulas.longTermFormulaParams = $scope.objectToTree(JSON.parse(data.marketConfig.longTermFormulaParams));
					$scope.marketFormulas.marketParams = $scope.objectToTree(JSON.parse(data.marketConfig.marketParams));
					$scope.marketFormulas.realLevelFormulaParams = $scope.objectToTree(JSON.parse(data.marketConfig.realLevelFormulaParams));
					$scope.marketFormulas.hourLevelFormulaClass = data.marketConfig.hourLevelFormulaClass;
					$scope.marketFormulas.hourClosingFormulaClass = data.marketConfig.hourClosingFormulaClass;
					$scope.marketFormulas.dayClosingFormulaClass = data.marketConfig.dayClosingFormulaClass;
					$scope.marketFormulas.longTermFormulaClass = data.marketConfig.longTermFormulaClass;
					$scope.marketFormulas.realLevelFormulaClass = data.marketConfig.realLevelFormulaClass;
					$scope.marketFormulas.marketDisableConditions = data.marketConfig.marketDisableConditions;
					
					$scope.formulaTypesFilter.dayClosing.setModel($scope.formulaTypesFilter.dayClosing.getOptionById(data.marketConfig.dayClosingFormulaId));
					$scope.formulaTypesFilter.hourClosing.setModel($scope.formulaTypesFilter.hourClosing.getOptionById(data.marketConfig.hourClosingFormulaId));
					$scope.formulaTypesFilter.hourLevel.setModel($scope.formulaTypesFilter.hourLevel.getOptionById(data.marketConfig.hourLevelFormulaId));
					$scope.formulaTypesFilter.longTerm.setModel($scope.formulaTypesFilter.longTerm.getOptionById(data.marketConfig.longTermFormulaId));
					$scope.formulaTypesFilter.realLevel.setModel($scope.formulaTypesFilter.realLevel.getOptionById(data.marketConfig.realLevelFormulaId));
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketFormulas')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.normalizeFormula = function(node){
		if(node.key == 'identifier'){
			node.parent.key = node.value;
		}
	}
	
	$scope.isObject = function(el){
		if(el !== null && typeof el === 'object'){
			return true;
		}
		return false;
	}
	
	$scope.objectToTree = function(obj, parent){
		if(typeof parent == "undefined" || parent == null){
			parent = {};
		}
		parent.nodes = [];
		for(key in obj){
			if(obj.hasOwnProperty(key)){
				var el = {};
				el.key = key;
				if(!$scope.isObject(obj[key])){
					el.value = obj[key];
				}
				el.parent = parent;
				parent.nodes.push(el);
				if($scope.isObject(obj[key])){
					$scope.objectToTree(obj[key], el);
				}
			}
		}
		return parent;
	}
	
	$scope.treeToObject = function(tree, result){
		if(typeof tree == "undefined" || tree == null){
			return '';
		}
		if(typeof result == "undefined" || result == null){
			result = {};
		}
		for(var i = 0; i < tree.nodes.length; i++){
			if(!tree.nodes[i].nodes){
				result[tree.nodes[i].key] = tree.nodes[i].value;
			}else{
				result[tree.nodes[i].key] = {};
				$scope.treeToObject(tree.nodes[i], result[tree.nodes[i].key]);
			}
		}
		return result;
	}
	
	
	
	$scope.changeFormula = function(singleSelect, formulaName){
		var params = singleSelect.model.params;
		if(!params){
			params = "{}";
		}
		var temp = $scope.objectToTree(JSON.parse(params));
		if($scope.marketFormulas[formulaName]){
			$scope.cloneProperties($scope.marketFormulas[formulaName], temp);
		}
		$scope.marketFormulas[formulaName] = temp;
	}
	$scope.cloneProperties = function(src, dest, destIdentifiersFound){
		for(var i = 0; i < src.nodes.length; i++){
			var index = -1;
			
			var isIdentifierParent = false;
			if(src.nodes[i].nodes){
				for(var j = 0; j < src.nodes[i].nodes.length; j++){
					if(src.nodes[i].nodes[j].key == 'identifier'){
						isIdentifierParent = true;
						break;
					}
				}
			}
			if(isIdentifierParent){
				var suffix = destIdentifiersFound ? destIdentifiersFound : 0;
				for(var j = 0; j < dest.nodes.length; j++){
					if(dest.nodes[j].key == 'Identifier'){
						index = j;
					}
				}
				if(index == -1){
					for(var j = 0; j < dest.nodes.length; j++){
						if(dest.nodes[j].key == 'Identifier' + (suffix + 1)){
							index = j;
						}
					}
				}
			}else{
				for(var j = 0; j < dest.nodes.length; j++){
					if(dest.nodes[j].key == src.nodes[i].key){
						index = j;
					}
				}
			}
			
			if(dest.nodes[index]){
				if(!src.nodes[i].nodes){
					dest.nodes[index].value = src.nodes[i].value;
					$scope.normalizeFormula(dest.nodes[index]);
				}else{
					$scope.cloneProperties(src.nodes[i], dest.nodes[index], destIdentifiersFound);
				}
			}
		}
	}
	
	$scope.getMarketName = function(){
		var result = '';
		
		for(group in $scope.filter.marketsTypes){
			if($scope.filter.marketsTypes.hasOwnProperty(group)){
				for(var i = 0; i < $scope.filter.marketsTypes[group].markets.length; i++){
					if($scope.filter.marketsTypes[group].markets[i].id == $scope.marketId){
						result = $scope.filter.marketsTypes[group].markets[i].name;
						break;
					}
				}
			}
		}
		
		return result;
	}
	
	
	$scope.updateMarketFormulas = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = $scope.marketId;
		methodRequest.marketConfig = {};
		methodRequest.marketConfig.dayClosingFormulaParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.dayClosingFormulaParams));
		methodRequest.marketConfig.hourClosingFormulaParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.hourClosingFormulaParams));
		methodRequest.marketConfig.hourLevelFormulaParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.hourLevelFormulaParams));
		methodRequest.marketConfig.longTermFormulaParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.longTermFormulaParams));
		methodRequest.marketConfig.marketParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.marketParams));
		methodRequest.marketConfig.realLevelFormulaParams = JSON.stringify($scope.treeToObject($scope.marketFormulas.realLevelFormulaParams));
		methodRequest.marketConfig.hourLevelFormulaClass = $scope.marketFormulas.hourLevelFormulaClass;
		methodRequest.marketConfig.hourClosingFormulaClass = $scope.marketFormulas.hourClosingFormulaClass;
		methodRequest.marketConfig.dayClosingFormulaClass = $scope.marketFormulas.dayClosingFormulaClass;
		methodRequest.marketConfig.longTermFormulaClass = $scope.marketFormulas.longTermFormulaClass;
		methodRequest.marketConfig.realLevelFormulaClass = $scope.marketFormulas.realLevelFormulaClass;
		methodRequest.marketConfig.marketDisableConditions = $scope.marketFormulas.marketDisableConditions;
		$scope.formulaTypesFilter.dayClosing.getId() >= 0 ? methodRequest.marketConfig.dayClosingFormulaId = $scope.formulaTypesFilter.dayClosing.getId() : '';
		$scope.formulaTypesFilter.hourClosing.getId() >= 0 ? methodRequest.marketConfig.hourClosingFormulaId = $scope.formulaTypesFilter.hourClosing.getId() : '';
		$scope.formulaTypesFilter.hourLevel.getId() >= 0 ? methodRequest.marketConfig.hourLevelFormulaId = $scope.formulaTypesFilter.hourLevel.getId() : '';
		$scope.formulaTypesFilter.longTerm.getId() >= 0 ? methodRequest.marketConfig.longTermFormulaId = $scope.formulaTypesFilter.longTerm.getId() : '';
		$scope.formulaTypesFilter.realLevel.getId() >= 0 ? methodRequest.marketConfig.realLevelFormulaId = $scope.formulaTypesFilter.realLevel.getId() : '';
		
		$http.post(settings.backendJsonLink + 'EditMarketConfigServices/updateMarketConfigFields', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				if($rootScope.$stateParams.inWizard){
					$rootScope.$state.go('ln.admin.markets.market_translations.edit', {ln: $state.params.ln, marketId: $scope.marketId, inWizard: true});
				}else{
					$rootScope.$state.go('ln.admin.markets.market_formulas', {ln: $state.params.ln});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateMarketFormulas')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketTranslationsCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.marketsTypes = [];
	$scope.filter.marketType = null;
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	
	$scope.filter.languages = {};
	
	$scope.marketTranslations = null;
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getMarketTranslationsScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getMarketTranslationsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.getMarketTranslations($scope.marketId);
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.marketTranslations = null;
		}
	}
	
	$scope.getMarketTranslationsScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'MarketTranslationsServices/getMarketTranslationsScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(type in data.filters.markets){
					if(data.filters.markets.hasOwnProperty(type)){
						var markets = [];
						for(market in data.filters.markets[type]){
							if(data.filters.markets[type][market]){
								markets.push({id: market, name: data.filters.markets[type][market]});
							}
						}
						$scope.filter.marketsTypes.push({id: type, markets: markets});
						$scope.filter.languages = data.languages;
					}
				}
				if($scope.filter.marketsTypes.length > 0){
					$scope.filter.marketType = $scope.filter.marketsTypes[0];
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketTranslationsScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getMarketTranslationsScreenSettings');
			})
	}
	
	$scope.editMarketTranslations = function(marketId){
		$rootScope.$state.go('ln.admin.markets.market_translations.edit', {ln: $state.params.ln, marketId: marketId});
	}
	
	$scope.getMarketTranslations = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'MarketTranslationsServices/getMarketTranslations', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketTranslations = data.marketTranslations;
				for(marketKey in $scope.marketTranslations){
					if($scope.marketTranslations.hasOwnProperty(marketKey)){
						for(key in $scope.filter.languages){
							if($scope.filter.languages.hasOwnProperty(key)){
								if(!$scope.marketTranslations[marketKey][key]){
									$scope.marketTranslations[marketKey][key] = '';
								}
							}
						}
					}
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketTranslations')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateMarketTranslations = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = $scope.marketId;
		methodRequest.marketTranslations = $scope.marketTranslations;
		$http.post(settings.backendJsonLink + 'MarketTranslationsServices/updateMarketTranslations', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				if($rootScope.$stateParams.inWizard){
					$rootScope.$state.go('ln.admin.markets.market_opportunity_templates.edit', {ln: $state.params.ln, marketId: $scope.marketId, inWizard: true});
				}else{
					$rootScope.$state.go('ln.admin.markets.market_translations', {ln: $state.params.ln});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateMarketTranslations')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketOpportunityTemplatesCtr', ['$rootScope', '$scope', '$http', '$state', '$timeout', function($rootScope, $scope, $http, $state, $timeout) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.marketsTypes = [];
	$scope.filter.marketType = null;
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	$scope.filter.activeFilter = [];
	$scope.filter.active = null;
	$scope.filter.oddsTypeFilter = [];
	$scope.filter.scheduledFilter = [];
	$scope.filter.timeZoneFilter = [];
	$scope.filter.fullDayFilter = [];
	$scope.filter.selectorsGroupFilter = [];
	
	$scope.edit = {};
	$scope.edit.marketType = null;
	
	$scope.marketOpportunityTemplates = null;
	$scope.marketOpportunityTemplate = null;
	
	$scope.filter.timeFirstInvest = 0;
	$scope.filter.timeEstClosing = 0;
	$scope.filter.timeLastInvest = 0;
	$scope.filter.timeEstClosingHalfday = 0;
	$scope.filter.timeLastInvestHalfday = 0;
	$scope.filter.openDates = {
		timeFirstInvest: false,
		timeEstClosing: false,
		timeLastInvest: false,
		timeEstClosingHalfday: false,
		timeLastInvestHalfday: false
	};
	$scope.openCalendar = function(e, date) {
		$scope.initDates(date);
        $scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.defaultDate = new Date();
	$scope.defaultDate.setHours(0);
	$scope.defaultDate.setMinutes(0);
	$scope.initDates = function(date){
		if(!$scope.marketOpportunityTemplate[date]){
			$scope.marketOpportunityTemplate[date] = $scope.defaultDate;
		}
	}
	$scope.dateOptions = {
		readonlyInput: false,
        showMeridian: false
	};
	
	$scope.marketOpportunityWriterNames = {};
	$scope.marketOpportunityWriterName = '';
	
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getOpportunityTemplatesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getOpportunityTemplatesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId && $rootScope.$stateParams.oppId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.oppId = $rootScope.$stateParams.oppId;
			$scope.getMarketOpportunityTemplate($scope.marketId, $scope.oppId);
		}else if($rootScope.$state.includes("ln.admin.markets.market_opportunity_templates.add")){
			$scope.action = 'add';
			$scope.marketId = 0;
			$scope.marketOpportunityTemplate = {};
			$scope.marketOpportunityTemplate.fullDayWeek = [0, 0, 0, 0, 0, 0, 0];
			$scope.marketOpportunityTemplate.halfDayWeek = [0, 0, 0, 0, 0, 0, 0];
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.oppId = 0;
			$scope.marketOpportunityTemplates = null;
			$scope.marketOpportunityTemplate = null;
		}
	}
	
	$scope.getOpportunityTemplatesScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'OpportunityTemplatesServices/getOpportunityTemplatesScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(type in data.filters.markets){
					if(data.filters.markets.hasOwnProperty(type)){
						var markets = [];
						for(market in data.filters.markets[type]){
							if(data.filters.markets[type][market]){
								markets.push({id: market, name: data.filters.markets[type][market]});
							}
						}
						$scope.filter.marketsTypes.push({id: type, name: $rootScope.getMsgs('market-type-' + type), markets: markets});
						$scope.filter.languages = data.languages;
					}
				}
				if($scope.filter.marketsTypes.length > 0){
					$scope.filter.marketType = $scope.filter.marketsTypes[0];
					$scope.edit.marketType = $scope.filter.marketsTypes[0];
				}
				
				$rootScope.fillFilter({arr: $scope.filter.activeFilter, holder: data, filter: 'active'});
				if($scope.filter.activeFilter.length > 0){
					$scope.filter.active = $scope.filter.activeFilter[0];
				}
				
				$rootScope.fillFilter({arr: $scope.filter.oddsTypeFilter, holder: data, filter: 'oddsType'});
				$rootScope.fillFilter({arr: $scope.filter.scheduledFilter, holder: data, filter: 'scheduled'});
				$rootScope.fillFilter({arr: $scope.filter.timeZoneFilter, holder: data, filter: 'timezone'});
				$rootScope.fillFilter({arr: $scope.filter.fullDayFilter, holder: data, filter: 'fullDay'});
				$rootScope.fillFilter({arr: $scope.filter.selectorsGroupFilter, holder: data, filter: 'selectorsGroup'});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getOpportunityTemplatesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getOpportunityTemplatesScreenSettings');
			})
	}
	
	$scope.editMarketOpportunityTemplate = function(marketId, oppId){
		$rootScope.$state.go('ln.admin.markets.market_opportunity_templates.edit', {ln: $state.params.ln, marketId: marketId, oppId: oppId});
	}
	
	$scope.getMarketOpportunityTemplates = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		methodRequest.active = $scope.filter.active.id;
		$http.post(settings.backendJsonLink + 'OpportunityTemplatesServices/getOpportunityTemplatesByMarket', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketOpportunityTemplates = data.opportunityTemplates;
				$scope.marketOpportunityWriterNames = data.writerNames;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketOpportunityTemplates')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.getMarketOpportunityTemplate = function(marketId, oppId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		methodRequest.opportunityTemplateId = oppId;
		$http.post(settings.backendJsonLink + 'OpportunityTemplatesServices/getOpportunityTemplateByMarket', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketOpportunityTemplate = data.opportunityTemplate;
				$scope.marketOpportunityTemplate.timeFirstInvest = setUpTime($scope.marketOpportunityTemplate.timeFirstInvest);
				$scope.marketOpportunityTemplate.timeEstClosing = setUpTime($scope.marketOpportunityTemplate.timeEstClosing);
				$scope.marketOpportunityTemplate.timeLastInvest = setUpTime($scope.marketOpportunityTemplate.timeLastInvest);
				$scope.marketOpportunityTemplate.timeEstClosingHalfday = setUpTime($scope.marketOpportunityTemplate.timeEstClosingHalfday);
				$scope.marketOpportunityTemplate.timeLastInvestHalfday = setUpTime($scope.marketOpportunityTemplate.timeLastInvestHalfday);
				$scope.marketOpportunityWriterName = data.writerName;
				
				function setUpTime(str){
					if(!str){
						return null;
					}
					var arr = str.split(':');
					var d = new Date();
					d.setHours(arr[0]);
					d.setMinutes(arr[1]);
					return d;
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketOpportunityTemplate')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateMarketOpportunityTemplate = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		if($scope.action == 'edit'){
			methodRequest.opportunityTemplateId = $scope.marketOpportunityTemplate.id;
		}
		methodRequest.opportunityTemplate = cloneObj($scope.marketOpportunityTemplate);
		if($scope.marketOpportunityTemplate.timeFirstInvest){
			methodRequest.opportunityTemplate.timeFirstInvest = $rootScope.timeGetSimple($scope.marketOpportunityTemplate.timeFirstInvest);
		}
		if($scope.marketOpportunityTemplate.timeEstClosing){
			methodRequest.opportunityTemplate.timeEstClosing = $rootScope.timeGetSimple($scope.marketOpportunityTemplate.timeEstClosing);
		}
		if($scope.marketOpportunityTemplate.timeLastInvest){
			methodRequest.opportunityTemplate.timeLastInvest = $rootScope.timeGetSimple($scope.marketOpportunityTemplate.timeLastInvest);
		}
		if($scope.marketOpportunityTemplate.timeEstClosingHalfday){
			methodRequest.opportunityTemplate.timeEstClosingHalfday = $rootScope.timeGetSimple($scope.marketOpportunityTemplate.timeEstClosingHalfday);
		}
		if($scope.marketOpportunityTemplate.timeLastInvestHalfday){
			methodRequest.opportunityTemplate.timeLastInvestHalfday = $rootScope.timeGetSimple($scope.marketOpportunityTemplate.timeLastInvestHalfday);
		}
		$http.post(settings.backendJsonLink + 'OpportunityTemplatesServices/updateInsertOpportunityTemplate', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$rootScope.$state.go('ln.admin.markets.market_opportunity_templates', {ln: $state.params.ln});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateMarketOpportunityTemplate')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.createOpportunities = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'OpportunityTemplatesServices/createOpportunitiesForMarket', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'createOpportunities');
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketPrioritiesCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.skins = [];
	$scope.filter.skin = null;
	$scope.selectedSkin = '';
	$scope.markets = [];
	
	$scope.allMarkets = {};
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getMarketPrioritiesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getMarketPrioritiesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getMarketPrioritiesScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'MarketPrioritiesServices/getMarketPrioritiesScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				$rootScope.fillFilter({arr: $scope.filter.skins, holder: data.filters, filter: 'writerSkins'});
				for(var skin in skinMap){
					if(skinMap.hasOwnProperty(skin)){
						if(skin == 1 || !skinMap[skin].isActive){
							for(var i = 0; i < $scope.filter.skins.length; i++){
								if($scope.filter.skins[i].id == skin){
									$scope.filter.skins.splice(i, 1);
								}
							}
						}
					}
				}
				$scope.filter.skin = $scope.filter.skins[0];
				
				$scope.allMarkets = data.markets;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketPrioritiesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getMarketPrioritiesScreenSettings');
			})
	}
	
	$scope.getSkinMarketsPriorities = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.skinId = $scope.filter.skin.id;
		$http.post(settings.backendJsonLink + 'MarketPrioritiesServices/getSkinMarketsPriorities', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.markets = data.markets;
				$scope.selectedSkin = $scope.filter.skin;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSkinMarketsPriorities');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.marketInList = function(id){
		var result = false;
		for(var i = 0; i < $scope.markets.length; i++){
			if($scope.markets[i].marketId == id){
				result = true;
				break;
			}
		}
		return result;
	}
	
	$scope.addMarket = function(id, name){
		$scope.markets.push({marketId: id, marketName: name, isNew: true});
	}
	$scope.undoAddMarket = function(item){
		for(var i = 0; i < $scope.markets.length; i++){
			if($scope.markets[i] == item){
				$scope.markets.splice(i, 1);
				break;
			}
		}
	}
	
	$scope.updateMarketsPriorities = function(){
		showLoading();
		var methodRequest = {};
		var marketIds = [];
		var newMarketsIds = [];
		for(var i = 0; i < $scope.markets.length; i++){
			if($scope.markets[i].isNew){
				newMarketsIds.push($scope.markets[i].marketId);
			}else{
				marketIds.push($scope.markets[i].skinMarketGroupMarketId);
			}
		}
		methodRequest.skinMarketGroupMarketIds = marketIds;
		methodRequest.newMarketsIds = newMarketsIds;
		methodRequest.selectedSkinId = $scope.filter.skin.id;
		$http.post(settings.backendJsonLink + 'MarketPrioritiesServices/updateMarketsPriorities', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('changes.saved.successfully'));
				$scope.getSkinMarketsPriorities();
			})
			.catch(function(data) {
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('changes.saved.error'));
				$rootScope.handleErrors(data, 'updateMarketsPriorities');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
}]);

backendApp.controller('MarketPausesCtr', ['$rootScope', '$scope', '$http', '$state', '$timeout', function($rootScope, $scope, $http, $state, $timeout) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.marketsTypes = [];
	$scope.filter.marketType = null;
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	$scope.filter.scheduledFilter = [];
	
	$scope.edit = {};
	$scope.edit.marketType = null;
	
	$scope.marketPauses = null;
	$scope.marketPause = null;
	
	$scope.filter.pauseStart = 0;
	$scope.filter.pauseEnd = 0;
	$scope.filter.openDates = {
		pauseStart: false,
		pauseEnd: false
	};
	$scope.openCalendar = function(e, date) {
		$scope.initDates(date);
        $scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.defaultDate = new Date();
	$scope.defaultDate.setHours(0);
	$scope.defaultDate.setMinutes(0);
	$scope.initDates = function(date){
		if(!$scope.marketPause[date]){
			$scope.marketPause[date] = $scope.defaultDate;
		}
	}
	$scope.dateOptions = {
		readonlyInput: false,
        showMeridian: false
	};
	
	
	$scope.weekDays = [
		'sunday',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday'
	];
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getMarketPausesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getMarketPausesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId && $rootScope.$stateParams.marketPauseId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.marketPauseId = $rootScope.$stateParams.marketPauseId;
			$scope.getMarketPause($scope.marketId, $scope.marketPauseId);
		}else if($rootScope.$state.includes("ln.admin.markets.market_pauses.add")){
			$scope.action = 'add';
			$scope.marketId = 0;
			$scope.marketPause = {};
			$scope.marketPause.validDaysArr = [];
			for(var i = 0; i < 7; i++){
				$scope.marketPause.validDaysArr.push({id: i, value: false});
			}
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.marketPauseId = 0;
			$scope.marketPauses = null;
			$scope.marketPause = null;
		}
	}
	
	$scope.getMarketPausesScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'MarketPausesServices/getMarketPausesScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(type in data.filters.markets){
					if(data.filters.markets.hasOwnProperty(type)){
						var markets = [];
						for(market in data.filters.markets[type]){
							if(data.filters.markets[type][market]){
								markets.push({id: market, name: data.filters.markets[type][market]});
							}
						}
						$scope.filter.marketsTypes.push({id: type, markets: markets});
						$scope.filter.languages = data.languages;
					}
				}
				if($scope.filter.marketsTypes.length > 0){
					$scope.filter.marketType = $scope.filter.marketsTypes[0];
					$scope.edit.marketType = $scope.filter.marketsTypes[0];
				}
				
				$rootScope.fillFilter({arr: $scope.filter.scheduledFilter, holder: data, filter: 'scheduled'});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketPausesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getMarketPausesScreenSettings');
			})
	}
	
	$scope.editMarketPause = function(marketId, marketPauseId){
		$rootScope.$state.go('ln.admin.markets.market_pauses.edit', {ln: $state.params.ln, marketId: marketId, marketPauseId: marketPauseId});
	}
	
	$scope.getMarketPauses = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'MarketPausesServices/getMarketPausesByMarketId', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketPauses = data.marketPauses;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketPauses')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.parseValidDay = function(pause, index){
		var bits = (pause.validDays).toString(2);
		while(bits.length < 7){
			bits = '0' + bits;
		}
		return bits[index] == 1;
	}
	
	$scope.getMarketName = function(marketId){
		var name = '';
		for(var i = 0; i < $scope.filter.marketsTypes.length; i++){
			for(j = 0; j < $scope.filter.marketsTypes[i].markets.length; j++){
				if($scope.filter.marketsTypes[i].markets[j].id == marketId){
					name = $scope.filter.marketsTypes[i].markets[j].name;
					break;
				}
			}
		}
		return name;
	}
	
	$scope.getMarketPause = function(marketId, marketPauseId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketPauseDetails = {};
		methodRequest.marketPauseDetails.marketId = marketId;
		methodRequest.marketPauseDetails.marketPauseId = marketPauseId;
		$http.post(settings.backendJsonLink + 'MarketPausesServices/getMarketPausesByMarketPauseId', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.marketPause = data.marketPauses[0];
				
				$scope.marketPause.validDaysArr = [];
				for(var i = 0; i < 7; i++){
					$scope.marketPause.validDaysArr.push({id: i, value: $scope.parseValidDay($scope.marketPause, i)});
				}
				
				$scope.marketPause.pauseStart = setUpTime($scope.marketPause.pauseStart);
				$scope.marketPause.pauseEnd = setUpTime($scope.marketPause.pauseEnd);
				
				function setUpTime(str){
					var arr = str.split(':');
					var d = new Date();
					d.setHours(arr[0]);
					d.setMinutes(arr[1]);
					return d;
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getMarketPause')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateMarketPause = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketPauseDetails = cloneObj($scope.marketPause);
		
		var validDays = '';
		for(var i = 0; i < $scope.marketPause.validDaysArr.length; i++){
			validDays += '' + ($scope.marketPause.validDaysArr[i].value ? 1 : 0);
		}
		methodRequest.marketPauseDetails.validDays = parseInt(validDays, 2);
		methodRequest.marketPauseDetails.validDaysArr = null;
		
		if($scope.marketPause.pauseStart){
			methodRequest.marketPauseDetails.pauseStart = $rootScope.timeGetSimple($scope.marketPause.pauseStart);
		}
		if($scope.marketPause.pauseEnd){
			methodRequest.marketPauseDetails.pauseEnd = $rootScope.timeGetSimple($scope.marketPause.pauseEnd);
		}
		
		$http.post(settings.backendJsonLink + 'MarketPausesServices/editInsertMarketPause', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$rootScope.$state.go('ln.admin.markets.market_pauses', {ln: $state.params.ln});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateMarketPause')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('MarketDynamicsQuotesCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.markets = [{id: -1, name: ''}];
	$scope.filter.market = $scope.filter.markets[0];
	
	$scope.theMarket = null;
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getDynamicsQuotesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getDynamicsQuotesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.marketId){
			$scope.action = 'edit';
			$scope.marketId = $rootScope.$stateParams.marketId;
			$scope.getDynamicsMarketQuotes($scope.marketId);
		}else{
			$scope.action = '';
			$scope.marketId = 0;
			$scope.theMarket = null;
		}
	}
	
	$scope.getDynamicsQuotesScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'DynamicsQuoteParamsServices/getDynamicsQuotesScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(market in data.dynamicsMarkets){
					if(data.dynamicsMarkets.hasOwnProperty(market)){
						$scope.filter.markets.push({id: market, name: data.dynamicsMarkets[market]});
					}
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDynamicsQuotesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getDynamicsQuotesScreenSettings');
			})
	}
	
	$scope.editDynamicsMarketQuotes = function(marketId){
		$rootScope.$state.go('ln.admin.markets.market_dynamics_quotes.edit', {ln: $state.params.ln, marketId: marketId});
	}
	
	$scope.getDynamicsMarketQuotes = function(marketId){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = marketId;
		$http.post(settings.backendJsonLink + 'DynamicsQuoteParamsServices/getDynamicsMarketQuotes', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.theMarket = data.market;
				$scope.theMarket.quoteParams = JSON.parse($scope.theMarket.quoteParams);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDynamicsMarketQuotes')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.saveQuoteChanges = function(){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.marketId = $scope.marketId;
		methodRequest.quoteParams = $scope.theMarket.quoteParams;
		$http.post(settings.backendJsonLink + 'DynamicsQuoteParamsServices/saveQuoteChanges', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$rootScope.$state.go('ln.admin.markets.market_dynamics_quotes', {ln: $state.params.ln});
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'saveQuoteChanges')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);