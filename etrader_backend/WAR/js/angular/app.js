var currentTab = '';
var forceCurrentTab = false;
function setTab(tab, force, loadCss){
	currentTab = tab;
	forceCurrentTab = force;
	if(loadCss){
		$('head').append('<link rel="stylesheet" type="text/css" href="../../css/style.css">');
	}
}

if(typeof window.isNewBackend == 'undefined'){
	var isNewBackend = false;
}
if(typeof window.folderPrefix == 'undefined'){
	var folderPrefix = '../../html/templates/';
}
if(typeof window.indexTemplate == 'undefined'){
	var indexTemplate = 'blank.html';
}

var backendApp = angular.module('backendApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'vcRecaptcha',
	'ui.bootstrap',
	'ui.bootstrap.datetimepicker', 
	'ui.tinymce', 
	'ui.tree',
	'checklist-model',
])
.run(['$rootScope', '$state', '$stateParams', '$interval', '$timeout', '$location', 
	function ($rootScope, $state, $stateParams, $interval, $timeout, $location) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.ready = false;
		
		$rootScope.folderPrefix = folderPrefix;
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln}, {location: 'replace'});
			}
			if(currentTab && !$state.includes("ln." + currentTab)){
				var suffix = '.index';
				if(forceCurrentTab){
					suffix = '';
				}
				$rootScope.$state.go("ln." + currentTab + suffix, {ln: $rootScope.$state.params.ln}, {location: 'replace'});
			}
			if($rootScope.menu){
				$rootScope.menu.updateMenuNodes(function(link){return $rootScope.$state.includes(link)});
			}
			$rootScope.resetGlobalErrorMsg();
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				$rootScope.stateParams = toParams;
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				//$rootScope.closeCopyOpPopup({all:true});
			});
		
		$rootScope.$on('$stateChangeError', 
			function(event, toState, toParams, fromState, fromParams, error) {
				//TODO - Handle resolve errors
				//Handle session expired, duplicating the code from the handleErrors function in controllers.js for now - TODO - don't repeat this...
				if (error.errorCode == errorCodeMap.session_expired) { //session expired 6999
					try {
						/*$rootScope.changeHeader(false);
						$rootScope.$state.go('ln.login', {ln: settings.skinLanguage});
						$rootScope.initSettings();*/
						window.location.hash = '';
						window.location.replace(window.location.pathname.replace(/index.html/, ''));
					} catch(e) {}
				} else if (error.errorMessages) {
					showToast(TOAST_TYPES.error, error.errorMessages);
				} else {
					showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.unknown'));
					$state.go('ln.index');
				}
			});
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$locationProvider',
	function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
		$httpProvider.interceptors.push('handleErrors');
		
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		$urlRouterProvider
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
		.when('/', '/en/')
		.otherwise('/en/');
		
		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: folderPrefix + 'template.html',
				'abstract': true,
				needLogin: 1//0 - no, 1 - doesn't matter, 2-yes
			})
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: folderPrefix + indexTemplate
					}
				},
				needLogin: 2
			})
			//forgot-password
			.state('ln.forgot-password', {
				url: '/forgot-password',
				views: {
					'main': {
						templateUrl: folderPrefix + 'profile/forgot-password.html',
						controller: 'forgotPasswordController'
					}
				},
				needLogin: 0
			})
			//Bubbles tab
			.state('ln.bubbles', {
				url: '/bubbles',
				views: {
					'main': {
						templateUrl: 'bubbles-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.bubbles.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: 'bubbles-index.html'
					}
				},
				needLogin: 2
			})
			//Content space
			.state('ln.content', {
				url: '/content',
				views: {
					'main': {
						templateUrl: folderPrefix + 'content/content-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.content.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'content/content-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.content.terms', {
				url: '/terms',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'content/terms.html'
					}
				},
				needLogin: 2
			})
			.state('ln.content.academy-articles', {
				url: '/academy-articles',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'content/academy-articles.html',
						controller: 'AcademyArticlesController',
						controllerAs: 'aa',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('AcademyArticlesServices/getScreenSettings', {}, settings.commonJsonLink)
						}
					}
				},
				needLogin: 2
			})
			.state('ln.content.academy-articles.add', {
				url: '/add',
				views: {
					'main-content@ln.content': {
						templateUrl: folderPrefix + 'content/academy-articles-details.html',
						controller: 'AcademyArticlesController',
						controllerAs: 'aa',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('AcademyArticlesServices/getScreenSettings', {}, settings.commonJsonLink)
						}
					}
				},
				needLogin: 2
			})
			.state('ln.content.academy-articles.edit', {
				url: '/:articleId',
				views: {
					'main-content@ln.content': {
						templateUrl: folderPrefix + 'content/academy-articles-details.html',
						controller: 'AcademyArticlesController',
						controllerAs: 'aa',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('AcademyArticlesServices/getScreenSettings', {}, settings.commonJsonLink)
						}
					}
				},
				needLogin: 2
			})
			//Sales deposits tab
			.state('ln.sales_deposits', {
				url: '/sales-deposits',
				views: {
					'main': {
						templateUrl: '../retention/sales-deposits-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.sales_deposits.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: '../retention/sales-deposits-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.sales_deposits.conversion', {
				url: '/conversion',
				views: {
					'main-content': {
						templateUrl: '../retention/sales-deposits-conversion.html'
					}
				},
				needLogin: 2
			})
			.state('ln.sales_deposits.retention', {
				url: '/retention',
				views: {
					'main-content': {
						templateUrl: '../retention/sales-deposits-retention.html'
					}
				},
				needLogin: 2
			})
			//Sales space
			.state('ln.sales', {
				url: '/sales',
				views: {
					'main': {
						templateUrl: folderPrefix + 'sales/sales-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.sales.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'sales/sales-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.sales.sales_investments', {
				url: '/sales-investments',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'sales/sales-investments.html'
					}
				},
				needLogin: 2
			})
			.state('ln.sales.country_configuration', {
				url: '/country-configuration',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'sales/country-configuration.html'
					}
				},
				needLogin: 2
			})
			.state('ln.sales.calls_status', {
				url: '/calls-status',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'sales/calls-status.html'
					}
				},
				needLogin: 2
			})
			//Home page priority (markets)
			.state('ln.home_page_priority', {
				url: '/home-page-priority',
				views: {
					'main': {
						templateUrl: folderPrefix + 'traders/home-page-priority-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.home_page_priority.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'traders/home-page-priority-index.html'
					}
				},
				needLogin: 2
			})
			//Transaction space
			.state('ln.transaction', {
				url: '/transaction',
				views: {
					'main': {
						templateUrl: folderPrefix + 'transaction/transaction-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.transaction.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/transaction-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.transaction.transactions', {
				url: '/transactions',
				params: {preserveSearch: false},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/transactions.html'
					}
				},
				needLogin: 2
			})
			.state('ln.transaction.update-transactions', {
				url: '/update-transactions',
				params: {preserveSearch: false},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/update-transactions.html',
						controller: 'UpdateTransactionController',
						controllerAs: 'utCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('UpdateTransactionServices/getUpdateTransactionScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.transaction.pendingWithdrawals_firstApprove', {
				url: '/pendingWithdrawals-firstApprove',
				params: {preserveSearch: false},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/pendingWithdrawals.html',
					}
				},
				needLogin: 2
			})
			.state('ln.transaction.pendingWithdrawals_secondApprove', {
				url: '/pendingWithdrawals-secondApprove',
				params: {preserveSearch: false},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/pendingWithdrawals.html',
					}
				},
				needLogin: 2
			})
			.state('ln.transaction.pendingWithdrawals_support', {
				url: '/pendingWithdrawals-support',
				params: {preserveSearch: false},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'transaction/pendingWithdrawals.html',
					}
				},
				needLogin: 2
			})
			//Users space
			.state('ln.users', {
				url: '/users',
				views: {
					'main': {
						templateUrl: folderPrefix + 'users/users-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.users.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/users-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.files', {
				url: '/files',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.files.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.files.edit', {
				url: '/edit-:fileId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.complaintsFiles', {
				url: '/complaints-files',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/complaints-files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.complaintsFiles.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/complaints-files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.singleQuestionnaire', {
				url: '/single-questionnaire',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/questionnaireSingle.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.withdrawals', {
				url: '/withdrawals',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/withdrawals/withdrawals-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.users.withdrawals.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/withdrawals/withdrawals-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.withdrawals.insert_record', {
				url: '/insert-record',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/withdrawals/insert-record.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.withdrawals.directBanking', {
				url: '/direct-banking',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/withdrawals/direct-banking.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.withdrawals.apmWithdrawals', {
				url: '/apm-withdrawals',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/withdrawals/apm-withdrawals.html',
						controller: 'ApmWithdrawalsController',
						controllerAs: 'apm',
						resolve: {
							//screenSettingsPrepService: resolveScreenSettings('GmWithdrawalServices/getScreenSettings', {writerId: writerId}, settings.commonJsonLink)
							screenSettingsPrepService: resolveScreenSettings('APMWithdrawalsServices/getAPMWithdrawalsScreenSettings', {userId: userId})
						}
					}
				},
				needLogin: 2
			})
			.state('ln.users.userDetails', {
				url: '/userDetails',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/user-details/user-details-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.users.userDetails.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/user-details/user-details-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.userDetails.bankSkrillDetails', {
				url: '/bank-skrill-details',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/user-details/bank-skrill-details.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.account-transactions', {
				url: '/account-transactions',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/account-transactions/account-transactions-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.users.account-transactions.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/account-transactions/account-transactions-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.account-transactions.deposits-summary', {
				url: '/deposits-summary',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/account-transactions/deposits-summary.html'
					}
				},
				needLogin: 2
			})
			.state('ln.users.copyop', {
				url: '/copyop',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/copyop/copyop-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.users.copyop.copying', {
				url: '/copying',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/copyop/user-copy.html',
						controller: 'UserCopyController',
						controllerAs: 'ucpCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('CopyopUserCopiersServices/getCopyopUserCopiersScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.users.copyop.copied-by', {
				url: '/copied-by',
				views: {
					'sub-content': {
						templateUrl: folderPrefix + 'users/copyop/user-copy.html',
						controller: 'UserCopyController',
						controllerAs: 'ucpCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('CopyopUserCopiersServices/getCopyopUserCopiersScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			//Documents space
			.state('ln.documents', {
				url: '/documents',
				views: {
					'main': {
						templateUrl: folderPrefix + 'documents/documents-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.documents.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'documents/documents-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.documents.ids_files', {
				url: '/ids-files',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'documents/ids-files.html'
					}
				},
				needLogin: 2
			})
			.state('ln.documents.call_center', {
				url: '/call-center',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'documents/call-center.html'
					}
				},
				needLogin: 2
			})
			.state('ln.documents.back_office', {
				url: '/back-office',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'documents/back-office.html'
					}
				},
				needLogin: 2
			})
			.state('ln.documents.from-inbox', {
				url: '/from-inbox',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'documents/from-inbox.html',
						controller: 'FromInboxController',
						controllerAs: 'fromInboxCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('DefaultFileTypeServices/getDefaultFileTypeScreenSettings'),
							idsFilesScreenSettingsPrepService: resolveScreenSettings('IdsFilesServices/getIdsFileScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			//Marketing space
			.state('ln.marketing', {
				url: '/marketing',
				views: {
					'main': {
						templateUrl: folderPrefix + 'marketing/marketing-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.marketing.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'marketing/marketing-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.marketing.promotions_management', {
				url: '/promotions-management',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'marketing/promotions-management.html'
					}
				},
				needLogin: 2
			})
			.state('ln.marketing.promotions_management.edit', {
				url: '/edit-:slideId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'marketing/promotions-management.html'
					}
				},
				needLogin: 2
			})
			.state('ln.marketing.promotions_management.add_slide', {
				url: '/add-slide-:bannerId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'marketing/promotions-management.html'
					}
				},
				needLogin: 2
			})
			//Administrator space
			.state('ln.admin', {
				url: '/admin',
				views: {
					'main': {
						templateUrl: folderPrefix + 'admin/admin-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.admin.index', {
				url: '/',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/admin-index.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.permissions', {
				url: '/permissions',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/permissions.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.permissions.edit', {
				url: '/edit-:roleId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/permissions.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.permissions.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/permissions.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.writers', {
				url: '/writers',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.writers.edit', {
				url: '/edit-:writerId',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.writers.add', {
				url: '/add',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'admin/writers.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets', {
				url: '/markets',
				views: {
					'main-content': {
						template: '<ui-view name="market-view"/>'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.create_market', {
				url: '/create-market',
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/create-market.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_fields', {
				url: '/market-fields',
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-fields.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_fields.edit', {
				url: '/edit-:marketId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-fields.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_formulas', {
				url: '/market-formulas',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-formulas.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_formulas.edit', {
				url: '/edit-:marketId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-formulas.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_translations', {
				url: '/market-translations',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-translations.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_translations.edit', {
				url: '/edit-:marketId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-translations.html'
					}
				}
			})
			.state('ln.admin.markets.market_opportunity_templates', {
				url: '/market-opportunity-templates',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-opportunity-templates.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_opportunity_templates.add', {
				url: '/add',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-opportunity-templates.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_opportunity_templates.edit', {
				url: '/edit-:marketId/:oppId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-opportunity-templates.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_priorities', {
				url: '/market-priorities',
				params: {inWizard: false},
				views: {
					'market-view': {
					templateUrl: folderPrefix + 'admin/markets/market-priorities.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_pauses', {
				url: '/market-pauses',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-pauses.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_pauses.add', {
				url: '/add',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-pauses.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_pauses.edit', {
				url: '/edit-:marketId/:marketPauseId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-pauses.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_dynamics_quotes', {
				url: '/market-dynamics-quotes',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-dynamics-quotes.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market_dynamics_quotes.edit', {
				url: '/edit-:marketId',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-dynamics-quotes.html'
					}
				},
				needLogin: 2
			})
			.state('ln.admin.markets.market-option-plus-prices', {
				url: '/market-option-plus-prices',
				params: {inWizard: false},
				views: {
					'market-view': {
						templateUrl: folderPrefix + 'admin/markets/market-option-plus-prices.html',
						controller: 'MarketOptionPlusPricesController',
						controllerAs: 'oppCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('OptionPlusPricesServices/getOptionPlusPricesScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.risk', {
				url: '/risk',
				views: {
					'main': {
						templateUrl: folderPrefix + 'risk/chargebacks-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.risk.chargebacks', {
				url: '/chargebacks',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'risk/chargebacks.html',
						controller: 'ChargebacksController',
						controllerAs: 'chbCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('ChargebacksServices/getChargebacksScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.risk.chargebacks.view', {
				url: '/view/:transactionId',
				views: {
					'main-content@ln.risk': {
						templateUrl: folderPrefix + 'risk/chargebacks-details.html',
						controller: 'ChargebacksAddEditController',
						controllerAs: 'chbACtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('ChargebacksServices/getChargebacksScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.trader', {
				url: '/trader',
				views: {
					'main': {
						templateUrl: folderPrefix + 'traders/traders-template.html'
					}
				},
				'abstract': true,
				needLogin: 2
			})
			.state('ln.trader.traders-actions', {
				url: '/traders-actions',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'traders/traders-actions.html',
						controller: 'TradersActionsController',
						controllerAs: 'trdAcCtrl',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('TradersActionsServices/getTradersActionsScreenSettings')
						}
					}
				},
				needLogin: 2
			})
			.state('ln.trader.platform-priority', {
				url: '/platform-priority',
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'traders/platform-priority.html',
						controller: 'PlatformPriorityController',
						controllerAs: 'ppCtrl',
						resolve: {
							platformPriorityPrepService: platformPriorityPrepService
						}
					}
				},
				needLogin: 2
			})
			
			.state('ln.users.files-previewers-gbg', {
				url: '/files/previewers/gbg/:fileId',
				params: {},
				views: {
					'main-content': {
						templateUrl: folderPrefix + 'users/previewers/gbg-previewer.html',
						controller: 'GbgPreviewerController',
						controllerAs: 'gbg',
						resolve: {
							screenSettingsPrepService: resolveScreenSettings('FilesServices/getFileScreenSettings')
						}
					}
				},
				needLogin: 2
			})

			// enable html5Mode for pushstate ('#'-less URLs) //requires base tag in the head
			/*if (settings.isLive) {
				$locationProvider.html5Mode(true).hashPrefix('!');
			}*/
	}]);
	
backendApp.config(['$compileProvider', function ($compileProvider) {
	// $compileProvider.debugInfoEnabled(false);
}]);



