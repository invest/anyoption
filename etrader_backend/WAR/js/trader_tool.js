			// variable from lighstreamer var schema
			var SUBSCR_FLD_KEY							= 1;
			var SUBSCR_FLD_COMMAND						= 2;
			var SUBSCR_FLD_CALLS_AMOUNT					= 3;
			var SUBSCR_FLD_PUTS_AMOUNT					= 4;
			var SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT	= 5;
			var SUBSCR_FLD_CALLS_AMOUNT_ADD_PUTS_AMOUNT	= 6;
			var SUBSCR_FLD_CALLS						= 7;
			var SUBSCR_FLD_PUTS							= 8;
			var SUBSCR_FLD_TIME_LAST_INVEST				= 9;
			var SUBSCR_FLD_MARKET_ID					= 10;
			var SUBSCR_FLD_MARKET_GROUP_ID				= 11;
			var SUBSCR_FLD_SCHEDULED					= 12;
			var SUBSCR_FLD_OPP_TYPE						= 13;
			var SUBSCR_FLD_SHIFT_PARAMETER 				= 14;
			var	SUBSCR_FLD_AUTO_SHIFT_PARAMETER 		= 15;
			var SUBSCR_FLD_IS_DISABLED_TRADER 			= 16;
			var SUBSCR_FLD_FEED_NAME 					= 17;
			var SUBSCR_FLD_IS_SUSPENDED 				= 18;
			var SUBSCR_FLD_SUSPENDED_MESSAGE 			= 19;
			var SUBSCR_FLD_STATE 						= 20;
			var SUBSCR_FLD_TIME_EST_CLOSING				= 21;
			var SUBSCR_FLD_MAX_EXPOSURE 				= 22;
			var SUBSCR_FLD_DECIMAL_POINT				= 23;
			var SUBSCR_FLD_ET_LEVEL						= 24;
			var SUBSCR_FLD_AO_LEVEL						= 25;
			var SUBSCR_FLD_SHIFT_PARAMETER_AO			= 26;
			var SUBSCR_FLD_SHIFT_PARAMETER_ZH			= 27;
			var SUBSCR_FLD_SHIFT_PARAMETER_TR			= 28;
			var SUBSCR_FLD_ZH_LEVEL						= 29;
			var SUBSCR_FLD_TR_LEVEL						= 30;
			// variables created in function updateItem(item, updateInfo)
			var SUBSCR_FLD_MARKET_NAME					= 32;
			var SUBSCR_FLD_SCHEDULED_NAME				= 33;
			var SUBSCR_FLD_OPP_TYPE_NAME				= 34;
			var SUBSCR_FLD_STATE_CHANGED 				= 35;
			var FORMATED_TIME_EST_CLOSING				= 36;
			var FORMATED_CALLS_AMOUNT					= 37;
			var FORMATED_PUTS_AMOUNT					= 38;
			var FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT	= 39;
			var FORMATED_CALLS_AMOUNT_ADD_PUTS_AMOUNT   = 40;
			var FORMATED_MAX_EXPOSURE					= 41;
			var SUBSCR_FLD_ET_LEVEL_CHANGED				= 42;
			var SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT_CHANGED	= 43;
			var SUBSCR_FLD_MAX_EXPOSURE_CHANGED					= 44;
			var SUBSCR_FLD_IS_DISABLED_TRADER_CHANGED			= 45;
			var SUBSCR_FLD_IS_SUSPENDED_CHANGED 				= 46;

			var OPPORTUNITY_STATE_OPEN 					= 1;
			var OPPORTUNITY_WAITING_TO_EXPIRE 			= 2;
			var OPPORTUNITY_SUSPENDED					= 3;
			var OPPORTUNITY_DISABLE						= 4;

			var OPPORTUNITY_TYPE_DEFAULT 				= 1;

			//constants for the row for each opp every td will have constants
			var TD_OPP_ID 									= 1;
			var TD_MARKET_NAME  							= 2;
			var TD_ET_AO_LEVEL 								= 3;
			var TD_ZH_TR_LEVEL 								= 4;
			var TD_SUBSCR_FLD_SCHEDULED_NAME 				= 5;
			var TD_FORMATED_CALLS_AMOUNT 					= 6;
			var TD_FORMATED_PUTS_AMOUNT 					= 7;
			var TD_FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT 	= 8;
			var TD_FORMATED_CALLS_AMOUNT_ADD_PUTS_AMOUNT 	= 9;
			var TD_FORMATED_TIME_EST_CLOSING 				= 10;
			var TD_SUBSCR_FLD_SHIFT_PARAMETER_ET_AO			= 11;
			var TD_SUBSCR_FLD_SHIFT_PARAMETER_ZH_TR			= 12;
			var TD_SUBSCR_FLD_AUTO_SHIFT_PARAMETER 			= 13;
			var TD_FORMATED_MAX_EXPOSURE 					= 14;
			var TD_SPREAD_BUTTON							= 15;
			var TD_DISABLE_BUTTON 							= 16;
			var TD_SUSPEND_BUTTON 							= 17;
			var TD_MARKET_ID 								= 18;
			var TD_SUBSCR_FLD_MARKET_GROUP_ID 				= 19;
			var TD_SCHEDULED 								= 20;
			var TD_OPP_TYPE_ID 								= 21;
			var TD_SUBSCR_FLD_FEED_NAME 					= 22;
			var TD_SUBSCR_FLD_SUSPENDED_MESSAGE 			= 23;
			var TD_SUBSCR_FLD_DECIMAL_POINT 				= 24;
			var TD_SETTLE_BUTTON 							= 25;
			var TD_SUBSCR_FLD_MAX_EXPOSURE 					= 26;

			var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';

			// last field and direction (asc or desc)
			var last_sort_field = null;
			var last_sort_direction = null;

			//for highlight to remmber what was the last color when mouseout
			var last_highlight_row_bgColor = null;
			var last_highlight_row_oppId = null;

function formatValues(item, itemUpdate) {
	if (item == null) {
		return;
	}
	if (itemUpdate == null) {
		return;
	}

	if (itemUpdate.getServerValue(SUBSCR_FLD_STATE_CHANGED) == 1) {
		state = itemUpdate.getServerValue(SUBSCR_FLD_STATE);
		if (state == OPPORTUNITY_DISABLE) {
			item.setAttribute('bgColor', '#9D9D9D');
			changeRowStyle(item, "row_disable");
		} else if (state == OPPORTUNITY_SUSPENDED) {
			item.setAttribute('bgColor', '#FFD700');
			changeRowStyle(item, "row_suspend");
		} else if (state == OPPORTUNITY_WAITING_TO_EXPIRE) {
			item.setAttribute('bgColor', '#11b000');
			changeRowStyle(item, "row_waiting");
		} else {
			if (itemUpdate.getServerValue(SUBSCR_FLD_OPP_TYPE) == OPPORTUNITY_TYPE_DEFAULT) {
				item.setAttribute('bgColor', '#FFFFFF');
			} else {
				item.setAttribute('bgColor', '#B2E6D1');
			}
			changeRowStyle(item, "row_other");
		}

		//if this row is highlight now, then change the last bg color to the new one
		if (last_highlight_row_oppId == itemUpdate.getServerValue(SUBSCR_FLD_KEY)) {
			last_highlight_row_bgColor = item.getAttribute('bgColor');
		}
	}
	if (updateInfo.getChangedFieldValue("oppOnlySuspended") != null) {
		var suspended = updateInfo.getChangedFieldValue("oppOnlySuspended");
		if (suspended == 1) {
			domNode.setAttribute('bgColor', '#7EC0EE');
			domNode.setAttribute("style", "row_suspend");
		}
	}

	var td = null;
	var button = null;
	var delta = new Number(itemUpdate.getServerValue(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT));

	if (itemUpdate.getServerValue(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT_CHANGED) == 1 || itemUpdate.getServerValue(SUBSCR_FLD_MAX_EXPOSURE_CHANGED) == 1) {

		var expusre = new Number(itemUpdate.getServerValue(SUBSCR_FLD_MAX_EXPOSURE));

		if (Math.abs(expusre) * 0.75 < delta) {
			td = getChildOfType(item, "TD", TD_FORMATED_MAX_EXPOSURE);
			button = getChildOfType(td, "INPUT", 1);
			button.style.background='#FF0000';
		}
	}

//	if (itemUpdate.getServerValue(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT_CHANGED) == 1) {
		if (delta > 0) {
			td = getChildOfType(item, "TD", TD_FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT);
			td.setAttribute(classAtr, 'trader_td_blue');
		} else if (delta < 0){
			td = getChildOfType(item, "TD", TD_FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT);
			td.setAttribute(classAtr, 'trader_td_red');
		}
//	}

	if (itemUpdate.getServerValue(SUBSCR_FLD_COMMAND) == "ADD") {
		filters(item, itemUpdate.getServerValue(SUBSCR_FLD_MARKET_ID), itemUpdate.getServerValue(SUBSCR_FLD_MARKET_GROUP_ID), itemUpdate.getServerValue(SUBSCR_FLD_SCHEDULED), itemUpdate.getServerValue(SUBSCR_FLD_OPP_TYPE));
	}

	if (itemUpdate.getServerValue(SUBSCR_FLD_IS_DISABLED_TRADER_CHANGED) == 1) {
		td = getChildOfType(item, "TD", TD_DISABLE_BUTTON);
		button = getChildOfType(td, "INPUT", 1);
		//set the value of the disable button (enable or disable);
		button.setAttribute('value',itemUpdate.getServerValue(SUBSCR_FLD_IS_DISABLED_TRADER));
	}
	if (itemUpdate.getServerValue(SUBSCR_FLD_IS_SUSPENDED_CHANGED) == 1) {
		td = getChildOfType(item, "TD", TD_SUSPEND_BUTTON);
		button = getChildOfType(td, "INPUT", 1);
		//set the value of the suspend button (suspend or unSuspend);
		button.setAttribute('value',itemUpdate.getServerValue(SUBSCR_FLD_IS_SUSPENDED));
	}
	if (itemUpdate.getServerValue(SUBSCR_FLD_MAX_EXPOSURE_CHANGED) == 1) {
		td = getChildOfType(item, "TD", TD_FORMATED_MAX_EXPOSURE);
		button = getChildOfType(td, "INPUT", 1);
		//set the value of the max exposure button (the opp max exposure value);
		button.setAttribute('value',itemUpdate.getServerValue(FORMATED_MAX_EXPOSURE));
	}
}

function updateItem(item, updateInfo) {


	//add filed to change display name
	if (updateInfo.isValueChanged(SUBSCR_FLD_MARKET_ID)) {
		for (var i=0; i<marketsDiaplayName.length; i++) {
	//		alert("market name: " + marketsDiaplayName[i][1] + " marketid: " + marketsDiaplayName[i][0] + "  ls: " + updateInfo.getNewValue(SUBSCR_FLD_ET_GROUP) );
			if (marketsDiaplayName[i][0] == updateInfo.getNewValue(SUBSCR_FLD_MARKET_ID)) {
		//		alert( "id: " + marketsDiaplayName[i][0] + " name: " + marketsDiaplayName[i][1] + "  ****** ");
				updateInfo.addField(SUBSCR_FLD_MARKET_NAME, marketsDiaplayName[i][1])
				break;
			}
		}
	}

	//add filed to change SCHEDULED name
	if (updateInfo.isValueChanged(SUBSCR_FLD_SCHEDULED)) {
		updateInfo.addField(SUBSCR_FLD_SCHEDULED_NAME, scheduledTxt[new Number(updateInfo.getNewValue(SUBSCR_FLD_SCHEDULED)) - 1]);
	}

	//add filed to change opp type name
	if (updateInfo.isValueChanged(SUBSCR_FLD_OPP_TYPE)) {
		updateInfo.addField(SUBSCR_FLD_OPP_TYPE_NAME, oppTypeTxt[new Number(updateInfo.getNewValue(SUBSCR_FLD_OPP_TYPE)) - 1]);
	}

	//if state change need to change color of row do it at formatvalues
	if (updateInfo.isValueChanged(SUBSCR_FLD_STATE)) {
		updateInfo.addField(SUBSCR_FLD_STATE_CHANGED, 1);
	} else {
		updateInfo.addField(SUBSCR_FLD_STATE_CHANGED, 0);
	}

	//format the est_close_time according to client windows offset
	if (updateInfo.isValueChanged(SUBSCR_FLD_TIME_EST_CLOSING)) {
		var est_date = new Date(updateInfo.getNewValue(SUBSCR_FLD_TIME_EST_CLOSING));
		updateInfo.addField(FORMATED_TIME_EST_CLOSING, formatDate(est_date));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_CALLS_AMOUNT)) {
		updateInfo.addField(FORMATED_CALLS_AMOUNT, "$" + addCommas(updateInfo.getNewValue(SUBSCR_FLD_CALLS_AMOUNT)));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_PUTS_AMOUNT)) {
		updateInfo.addField(FORMATED_PUTS_AMOUNT, "$" + addCommas(updateInfo.getNewValue(SUBSCR_FLD_PUTS_AMOUNT)));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT)) {
		updateInfo.addField(FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT, "$" + addCommas(updateInfo.getNewValue(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT)));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_CALLS_AMOUNT_ADD_PUTS_AMOUNT)) {
		updateInfo.addField(FORMATED_CALLS_AMOUNT_ADD_PUTS_AMOUNT, "$" + addCommas(updateInfo.getNewValue(SUBSCR_FLD_CALLS_AMOUNT_ADD_PUTS_AMOUNT)));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_MAX_EXPOSURE)) {
		updateInfo.addField(FORMATED_MAX_EXPOSURE, "$" + addCommas(updateInfo.getNewValue(SUBSCR_FLD_MAX_EXPOSURE)));
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT)) {
		updateInfo.addField(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT_CHANGED, 1);
	} else {
		updateInfo.addField(SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT_CHANGED, 0);
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_MAX_EXPOSURE)) {
		updateInfo.addField(SUBSCR_FLD_MAX_EXPOSURE_CHANGED, 1);
	} else {
		updateInfo.addField(SUBSCR_FLD_MAX_EXPOSURE_CHANGED, 0);
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_IS_DISABLED_TRADER)) {
		updateInfo.addField(SUBSCR_FLD_IS_DISABLED_TRADER_CHANGED, 1);
	} else {
		updateInfo.addField(SUBSCR_FLD_IS_DISABLED_TRADER_CHANGED, 0);
	}

	if (updateInfo.isValueChanged(SUBSCR_FLD_IS_SUSPENDED)) {
		updateInfo.addField(SUBSCR_FLD_IS_SUSPENDED_CHANGED, 1);
	} else {
		updateInfo.addField(SUBSCR_FLD_IS_SUSPENDED_CHANGED, 0);
	}


}

/**
	when changeing filters update the table according to the filters (which row to show and which to hide)
**/
function updateMarketsSelection() {
	var tbl = document.getElementById("oppsTable");
	var tbody = getChildOfType(tbl, "TBODY", 1);

	var i = 3;
	do {
		var row = getChildOfType(tbody, "TR", i);
		if (null != row) {
			//console.log(row);
			var market_id = getOppValue(row, TD_MARKET_ID);
			var row_group_id = getOppValue(row, TD_SUBSCR_FLD_MARKET_GROUP_ID);
			var row_scheduled = getOppValue(row, TD_SCHEDULED);
			var row_oppType = getOppValue(row, TD_OPP_TYPE_ID);

			filters(row, market_id, row_group_id, row_scheduled, row_oppType);

		} else {
			break;
		}
		i = i + 1;
	} while (true);
}


//update table according to market group selection and then update market select list
function updateMarketGroupSelection(selectList) {
	updateMarketsSelection();
	var marketGroupIds = getSelectedItems(selectList);
	if (marketGroupIds.indexOf(',') > -1) {
		marketGroupIds = marketGroupIds.substring(0, marketGroupIds.length - 1);
	} else {
		marketGroupIds = "0";
	}
	marketGroupChangeAjaxCall(marketGroupIds);
}

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

function getOppValue(opp, field) {
	var td = getChildOfType(opp, "TD", field);
	var div = getChildOfType(td, "DIV", 1);
	return div.innerHTML;
}

function getOppValueShift(opp, field, divNum) {
	var td = getChildOfType(opp, "TD", field);
	var div = getChildOfType(td, "DIV", divNum);
	div = getChildOfType(div, "DIV", 1);
	return div.innerHTML;
}

function openActionWindow(button, link) {
	var row = button.parentNode.parentNode;
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_suspendMsg = getOppValue(row, TD_SUBSCR_FLD_SUSPENDED_MESSAGE);
	
	var row_spread_et = getOppValueShift(row, TD_SUBSCR_FLD_SHIFT_PARAMETER_ET_AO, 1);
	var row_spread_ao = getOppValueShift(row, TD_SUBSCR_FLD_SHIFT_PARAMETER_ET_AO, 2);
	var row_spread_zh = getOppValueShift(row, TD_SUBSCR_FLD_SHIFT_PARAMETER_ZH_TR, 1);
	var row_spread_tr = getOppValueShift(row, TD_SUBSCR_FLD_SHIFT_PARAMETER_ZH_TR, 2);
	
	var row_feedName = encodeURIComponent(getOppValue(row, TD_SUBSCR_FLD_FEED_NAME));
	var row_markeName = escape(getOppValue(row, TD_MARKET_NAME).replace('&amp;', '&'));
	var row_maxExposure = getOppValue(row, TD_SUBSCR_FLD_MAX_EXPOSURE);
	var actionName = button.getAttribute('value');
	if (link == 'MaxExposure') {
		actionName = 'change max exposure';
	}
	var popupWin = window.open(context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + actionName + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&feedName=" + row_feedName + "&marketName=" + row_markeName + "&maxExposure=" + row_maxExposure + "&spread1=" + row_spread_et+"&spread2=" + row_spread_ao+"&spread3=" + row_spread_zh+"&spread4=" + row_spread_tr,'actionWindow' + row_oppId, 'width=400,height=420,scrollbars=yes,left=' + document.body.clientWidth + ',top=0');
	//window.top.location.href = context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&spread=" + row_spread + "&feedName=" + row_feedName + "&marketName=" + row_markeName + "&maxExposure=" + row_maxExposure
   if (window.focus) {
   		popupWin.focus();
   	}
}

function openActionWindowSettle(button, link) {
	var row = button.parentNode.parentNode;
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_decimalPoint = getOppValue(row, TD_SUBSCR_FLD_DECIMAL_POINT);
	var row_markeName = escape(getOppValue(row, TD_MARKET_NAME).replace('&amp;', '&'));
	var popupWin = window.open(context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&decimalPoint=" + row_decimalPoint + "&marketName=" + row_markeName ,'actionWindow','width=400,height=240');
    if (window.focus) {
    	popupWin.focus();
    }
}

/**
* sort the table according to the culomn clicked
*@field - a field descriptor object for the field to be used as sort field, or null to disable sorting. A String field name or a Number representing a field position can also be used directly, instead of a field descriptor object.
*@direction - [Optional] true or false to perform descending or ascending sort. This parameter is optional; if missing or null, then ascending sort is performed.
*@numeric - [Optional] true or false to perform numeric or alphabetical sort. This parameter is optional; if missing or null, then alphabetical sort is performed.
**/
function sort(field, direction, numeric) {
	if (field == last_sort_field) {
		if (last_sort_direction == true) {
			last_sort_direction = false;
		} else {
			last_sort_direction = true;
		}
	} else {
		last_sort_field = field;
		last_sort_direction = direction;
	}
	newTable.setMetapushSort(field, last_sort_direction, numeric);
}

// check if the row should be visible according to filters
function filters(row_row, row_market_id, row_group_id, row_scheduled, row_oppType) {
	var mId = row_market_id;
	row_market_id = "0," + row_market_id;

	var mSel = document.getElementById("select_market");
	var market = mSel.options[mSel.selectedIndex].value;

	var sSel = document.getElementById("select_scheduled");
	var i;
	var scheduled = getSelectedItems(sSel);

	var gSel = document.getElementById("select_group");
	var group_id = getSelectedItems(gSel);

	var tSel = document.getElementById("select_oppType");
	var oppType_id = tSel.options[tSel.selectedIndex].value;

	var skinSel = document.getElementById("skinId");
	var skinId = skinSel.options[skinSel.selectedIndex].value;

	var skinSel = document.getElementById("skinId");
	var skinId = skinSel.options[skinSel.selectedIndex].value;

	//	document.getElementById("log").innerHTML += oppType_id + ", " + row_oppType + "<br/>";
	var row = row_row;
	if (((market == row_market_id) || (market == '0,0')) && ((scheduled.indexOf(row_scheduled) > -1) || (scheduled.indexOf('0') > -1)) && ((group_id.indexOf(row_group_id) > -1) || (group_id.indexOf('0') > -1)) && ((oppType_id == row_oppType) || (oppType_id == '0'))) {
		row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	} else {
		row.style.display = 'none';
	}

	if(skinId>0) {
		if(s[skinId].indexOf(parseInt(mId))>-1) {
			row.style.display = 'none';
		}
	}
}


function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}

function formatDate(est_date) {
	est_date = adjustFromUTCToLocal(est_date);

	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
	}
	return est_date;
}

function changeRowStyle(row, styleClass) {
	var td = null;
	for (var i = 1; i < 15; i++) {
		td = getChildOfType(row, "TD", i);
		td.setAttribute(classAtr, styleClass);
	}
}

// add the commas and round the number
//parm amount: the amount
function addCommas(amount) {
	amount += '';
	x = amount.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

/** highlight Row when mouseover and back to real color on mouse out
*@parm row the row to hightlight
*@parm direction over if mouse over out if mouse out
**/
function highlightRow(rowP, dirction) {
	//for IE Bug
	var row = rowP
	var colorStyle = "#EE82EE";
	if (dirction == "over") {
		last_highlight_row_bgColor = row.getAttribute('bgColor');
		last_highlight_row_oppId = getOppValue(row, 1);
	} else {
		colorStyle = last_highlight_row_bgColor;
	}
	row.setAttribute('bgColor', colorStyle);
}


//ajax requset
function AJAXInteraction(url, callback) {

   	var req = init();
    req.onreadystatechange = processRequest;

    function init() {
   		if (window.XMLHttpRequest) {
        	return new XMLHttpRequest();
        } else if (window.ActiveXObject) {
        	return new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
	// status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) {
          	callback(req.responseXML);
          }
        }
      }
    }

    this.doGet = function() {
      // make a HTTP GET request to the URL asynchronously
      req.open("GET", url, true);
      req.send(null);
   	}
}

//build new select list with values from ajax callback
function buildSelect(nodeName, selectId, responseXML) {
	var currArr = responseXML.documentElement; //<marketsSelect>
	var markets = currArr.getElementsByTagName("market");
	var len = markets.length;
	var currSel = document.getElementById(selectId);
	var marketName = null;

	//safty check
	if (!currSel) {
		return null;
	}
	currSel.options.length = 0;
	currSel.options.add(new Option("All","0,0"));
	for (var i=0; i<len; i++) {
    	marketName = markets[i].getElementsByTagName("name")[0].firstChild.data;
    	marketName = unescape(marketName.replace(/\+/g," "));
	    currSel.options.add(new Option(marketName, markets[i].getElementsByTagName("value")[0].firstChild.data));
	}
	currSel.selectedIndex = 0;

}

function marketGroupChangeAjaxCall(MarketGroupId) {
    var url = "ajax.jsf?marketGroupId=" + encodeURIComponent(MarketGroupId);
    var ajax = new AJAXInteraction(url, changeMarketsSelect);
    ajax.doGet();
}

function changeMarketsSelect(responseXML) {
	buildSelect("marketsSelect", "select_market", responseXML);
}

function getSelectedItems(list) {
	var temp = "";
	for (var i = 0; i < list.length; i++) {
		if (list.options[i].selected) {
			temp += list.options[i].value + ",";
		}
	}
	return temp;
}