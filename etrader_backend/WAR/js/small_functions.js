var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;

function getLangCodeBySkinId(skinId) {
	//set default
	var langCode = "en";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			langCode = "en";
			break;
		case SKIN_ID_TR:
			langCode = "tr";
			break;
		case SKIN_ID_AR:
			langCode = "en";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			langCode = "it";
			break;
		case SKIN_ID_RU:
			langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			langCode = "fr";
			break;
		case SKIN_ID_KR:
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			langCode = "zh";
			break;
		case SKIN_ID_NL:
			langCode = "nl";
			break;
		case SKIN_ID_SV:
			langCode = "sv";
			break;
	}
	return langCode;
}


//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}
try {
	var balancePopupId = 'balancePopup';
	$(document).ready(function(){
		$('#'+balancePopupId).mouseenter(function(event){
			$(this).attr('data-hovering', '1');
		});
	});
} catch (e) {}
function openCacheBonusPopup(params){
	var dataParams = {'userId' : params.userId, 'investmentId' : params.investmentId, 'balanceCallType' : params.callType}
	$.ajax({
		type: "POST",
		url: jsonServiceUrl + "getUserDepositBonusBalance",
		data: JSON.stringify(dataParams),
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:  function(result) {
			showCacheBonusPopup(params, result);
		}
	});
}

function showCacheBonusPopup(params, data){
	$('#depositCashBalance').html(formatAmountSimple(data.depositBonusBalanceBase.depositCashBalance/100, 0));
	$('#bonusBalance').html(formatAmountSimple(data.depositBonusBalanceBase.bonusBalance/100, 0));
	$('#bonusProfit').html(formatAmountSimple(data.depositBonusBalanceBase.bonusWinnings/100, 0));
	$('#totalBalance').html(formatAmountSimple(data.depositBonusBalanceBase.totalBalance/100, 0));
	
	var $totalTxtHolder = $('#totalTxtHolder');
	$totalTxtHolder.removeClass('total amount return');
	
	if (params.callType == jsonDepositBonusBalanceConstants.totalAmount) {
		$('#bonusProfit').parent().css('display', '');
		$('.balancePopUp_info').css('display', '');
		$totalTxtHolder.addClass('total');
		if (isUndefined(params.position) || params.position) {
			$('#' + params.popUpId).removeClass('small_popUp');
			positionToolTip(params);
		}
	} else {
		$('#bonusProfit').parent().css('display', 'none');
		$('.balancePopUp_info').css('display', 'none');
		if (params.callType == jsonDepositBonusBalanceConstants.openInvestmentsAmount || params.callType == jsonDepositBonusBalanceConstants.settledInvestmentsAmount) {
			$totalTxtHolder.addClass('amount');
		} else {
			$totalTxtHolder.addClass('return');
		}
		if (isUndefined(params.position) || params.position) {
			$('#' + params.popUpId).addClass('small_popUp');
			positionToolTip(params);
		}
	}
	
	positionToolTip(params);
}

function positionToolTip(params) {
    var hardOffset = 5;
    var e = params.e;
    var $popup = $('#' + params.popUpId);

    $(e.target).mouseleave(function() {
        setTimeout(function(){
			if($('#'+balancePopupId).attr('data-hovering') != 1){
				$popup.fadeOut(200);
			}else{
				$('#'+balancePopupId).mouseleave(function(event){
					$(this).attr('data-hovering', '0');
					$popup.fadeOut(200);
				});
			}
		}, 50);
    });

    var offset = {
        l: 0,
        t: 0
    };

    var $popupArrow = $popup.find('._arrow');

    var $screen = {
        w: $(window).width(),//width
        h: $(window).height(),//height
        t: $(document).scrollTop(),//scroll top
        l: $(document).scrollLeft()//scroll left
    }
    var $popUpSize = {
        w: $popup.width(),
        h: $popup.height()
    }

    if (!isUndefined(params.offsetEl)) {
    	params.offsetEl = $(params.offsetEl);
        offset = {
            l: params.offsetEl.offset().left,
            t: params.offsetEl.offset().top
        }
    }

    var left = offset.l + $(e.target).offset().left + (e.target.offsetWidth / 2) - ($popUpSize.w / 2);
    var top = offset.t + $(e.target).offset().top + e.target.offsetHeight;

    var arrowOffeset = {
        left: 0,
        right: 0
    };

    if ($screen.w < $popUpSize.w || left < 0) {
        left = hardOffset;
        arrowOffeset.left = left + $(e.target).offset().left + (e.target.offsetWidth / 2) - ($popupArrow.width() / 2) - (hardOffset * 2);
        if (arrowOffeset.left < hardOffset) {
            arrowOffeset.left = hardOffset;
        } else {
            arrowOffeset.right = 'auto';
        }
    } else {
        if ((left + $popUpSize.w + hardOffset) > ($screen.w - $screen.l)) {
            left = $screen.w - $popUpSize.w - hardOffset;
            arrowOffeset.left = $(e.target).offset().left - left + (e.target.offsetWidth / 2) - ($popupArrow.width() / 2);
            if ((arrowOffeset.left + $popupArrow.width() + hardOffset) > $popup.width()) {
                arrowOffeset.left = 'auto';
                arrowOffeset.right = hardOffset;
            } else{
                arrowOffeset.right = 'auto';
            }
        }
    }

    $popupArrow.css('left', arrowOffeset.left).css('right', arrowOffeset.right);

    $popupArrow.removeClass('bottom top');
    if ($screen.h < $popUpSize.h || top < 0) {
        top = 0;
    } else {
        if ((top + $popUpSize.h + hardOffset) > ($screen.t + $screen.h)) {
            top -= ($popUpSize.h + e.target.offsetHeight + ($popupArrow.height() / 2));
            $popupArrow.addClass('bottom');
        } else {
            top += ($popupArrow.height() / 2);
            $popupArrow.addClass('top');
        }
    }
    
    $popup.css('left', left).css('top', top);
    $popup.fadeIn(200);
}

function formatAmountSimple(amount,centsPart){//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	try{
		if(!isNaN(amount)){
			amount = Math.round(amount*100)/100;
			var tmp = amount.toString().split('.');
			var decimal = parseInt(tmp[0]);
			var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
			if((cents === '') || (cents === 0)){cents = '00';}
			else if(cents.length == '1'){cents = cents + '0';}
			else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
			
			var rtn = '';
			if(isLeftCurrencySymbol){
				rtn += currencySymbol;
			}
			rtn += numberWithCommas(decimal);
			if(currencyDecimalPoint == 0){
				//Absolutly nothing. Just chill!
			}
			else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
				rtn += "<span>"+cents+"</span>";
			}
			else if((centsPart == 3) && (parseInt(cents) == 0)){
				//Absolutly nothing. Just chill!
			}
			else if((centsPart == 2) && (parseInt(cents) == 0)){
				//Absolutly nothing. Just chill!
			}
			else{
				rtn += "."+cents;
			}
			if(!isLeftCurrencySymbol){
				rtn += currencySymbol;
			}
			return rtn;
		}else{
			return ' ';
		}
	}catch(e){
		
	}
}
function numberWithCommas(x) {
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
    var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}

function addOptionsToSelect(oddsGroupDefault, oddsGroupID) {
	var pairList = oddsGroupDefault.trim().split(' ');
	var pairListSelected = 0;
	var selectElm = document.getElementById('oddsPairsSelect_' + oddsGroupID)
	for (var i = 0; i < pairList.length; i++) {
		var option = document.createElement("option");
		if (pairList[i].search('d') > -1) {
			pairListSelected = i;
			pairList[i] = pairList[i].replace('d', '');
			option.selected = true;
		}
	    option.text = pairList[i];
	    selectElm.add(option);
	}
}
function selectDefaulPair(el, id) {
	document.getElementById('OddsPairsSelectorsGroupForm:oddsGroupId').value = id;
	document.getElementById('OddsPairsSelectorsGroupForm:oddsGroupDefaultValue').value = el.value;
	return myfaces.oam.submitForm('OddsPairsSelectorsGroupForm','OddsPairsSelectorsGroupForm:hiddenBtn');
}

function getParentByTag(el, tag){
	while (el && el.parentNode) {
		el = el.parentNode;
		if (el.tagName && el.tagName.toLowerCase() == tag) {
			return el;
		}
	}
	return null;
}

function showPendingWithdrawsData(params){
	var pendingUserWithdrawalsDetailsHolderId = "pendingUserWithdrawalsDetailsHolder";
	var pendingUserWithdrawalsDetailsId = "pendingUserWithdrawalsDetails";
	
	var isSame = false;
	var parentEl = getParentByTag(params.callFrom, "tr");
	if(parentEl != null){
		if(parentEl.nextSibling == document.getElementById(pendingUserWithdrawalsDetailsHolderId) && document.getElementById(pendingUserWithdrawalsDetailsHolderId) != null){
			isSame = true;
			if(document.getElementById(pendingUserWithdrawalsDetailsHolderId)){
				document.getElementById("pendingUserWithdrawalsDetailsPlaceholder").appendChild(document.getElementById(pendingUserWithdrawalsDetailsId));
				document.getElementById(pendingUserWithdrawalsDetailsHolderId).parentNode.removeChild(document.getElementById(pendingUserWithdrawalsDetailsHolderId));
			}
			parentEl = null;
		}
	}
	if(isSame){
		setHeight();
		return true;
	}
	
	var dataParams = {'userId' : params.userId}
	$.ajax({
		type: "POST",
		url: jsonServiceUrl + "getPendingUserWithdrawalsDetails",
		data: JSON.stringify(dataParams),
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:  function(result) {
			var details = document.getElementById(pendingUserWithdrawalsDetailsId);
			document.getElementById("details_invested").innerHTML = result.pendingUserWithdrawalsDetailsBase.isHaveInvestmentsTxt;
			document.getElementById("details_win_lose").innerHTML = result.pendingUserWithdrawalsDetailsBase.winloseBalanceTxt;
			if(result.pendingUserWithdrawalsDetailsBase.winLoseBalance >= 0){
				document.getElementById("details_win_lose").style.color = "#ff0000";
			}else{
				document.getElementById("details_win_lose").style.color = "#008000";
			}
			document.getElementById("details_bonus").innerHTML = result.pendingUserWithdrawalsDetailsBase.userBonusStateTxt;
			document.getElementById("details_total_success_withdrawals").innerHTML = result.pendingUserWithdrawalsDetailsBase.successfulWithdrawals != 0 ? result.pendingUserWithdrawalsDetailsBase.successfulWithdrawals : noneTxt;
			document.getElementById("details_deposit_amount").innerHTML = result.pendingUserWithdrawalsDetailsBase.depositAmountInUSDTxt;
			document.getElementById("details_total_deposit_amount").innerHTML = result.pendingUserWithdrawalsDetailsBase.depositAmountTxt;
			document.getElementById("details_total_deposit_amount_usd").innerHTML = "(" + result.pendingUserWithdrawalsDetailsBase.depositAmountInUSDTxt + ")";
			document.getElementById("details_withdrawal_amount_USD").innerHTML = params.amountUSD;
			
			var el = params.callFrom;
			while (el && el.parentNode) {
				el = el.parentNode;
				if (el.tagName && el.tagName.toLowerCase() == "tr") {
					break;
				}
			}
			if(el != params.callFrom){
				if(document.getElementById(pendingUserWithdrawalsDetailsHolderId)){
					document.getElementById("pendingUserWithdrawalsDetailsPlaceholder").appendChild(document.getElementById(pendingUserWithdrawalsDetailsId));
					document.getElementById(pendingUserWithdrawalsDetailsHolderId).parentNode.removeChild(document.getElementById(pendingUserWithdrawalsDetailsHolderId));
				}
				var tr = document.createElement("tr");
				tr.id = pendingUserWithdrawalsDetailsHolderId;
				var td = document.createElement("td");
				td.colSpan = "50";
				tr.appendChild(td);
				td.appendChild(details);
				el.parentNode.insertBefore(tr, el.nextSibling);
				tr = null;
				td = null;
			}
			el = null;
			details = null;
			setHeight();
		}
	});
}

function showPhoneRetentionConversionData(params){
	var phoneRetentionConversionDetailsHolderId = "phoneRetentionConversionDetailsHolder";
	var phoneRetentionConversionDetailsId = "phoneRetentionConversionDetails";
	
	var isSame = false;
	var parentEl = getParentByTag(params.callFrom, "tr");
	if(parentEl != null){
		if(parentEl.nextSibling == document.getElementById(phoneRetentionConversionDetailsHolderId) && document.getElementById(phoneRetentionConversionDetailsHolderId) != null){
			isSame = true;
			if(document.getElementById(phoneRetentionConversionDetailsHolderId)){
				document.getElementById("phoneRetentionConversionDetailsPlaceholder").appendChild(document.getElementById(phoneRetentionConversionDetailsId));
				document.getElementById(phoneRetentionConversionDetailsHolderId).parentNode.removeChild(document.getElementById(phoneRetentionConversionDetailsHolderId));
			}
			parentEl = null;
		}
	}
	if(isSame){
		setHeight();
		return true;
	}
	
	var dataParams = {'userId' : params.userId, 'contactId' : params.contactId};
	$.ajax({
		type: "POST",
		url: settings.backendJsonLink + "PhoneConversionCallsHistoryServices/getCallsHistory",
		data: JSON.stringify(dataParams),
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:  function(result) {
			var details = document.getElementById(phoneRetentionConversionDetailsId);
			
			document.getElementById("details_last_caller").innerHTML = result.lastCaller;
			document.getElementById("details_calls_count").innerHTML = result.callsCountTxt;

			var el = params.callFrom;
			while (el && el.parentNode) {
				el = el.parentNode;
				if (el.tagName && el.tagName.toLowerCase() == "tr") {
					break;
				}
			}
			if(el != params.callFrom){
				if(document.getElementById(phoneRetentionConversionDetailsHolderId)){
					document.getElementById("phoneRetentionConversionDetailsPlaceholder").appendChild(document.getElementById(phoneRetentionConversionDetailsId));
					document.getElementById(phoneRetentionConversionDetailsHolderId).parentNode.removeChild(document.getElementById(phoneRetentionConversionDetailsHolderId));
				}
				var tr = document.createElement("tr");
				tr.id = phoneRetentionConversionDetailsHolderId;
				var td = document.createElement("td");
				td.colSpan = "50";
				tr.appendChild(td);
				td.appendChild(details);
				el.parentNode.insertBefore(tr, el.nextSibling);
				tr = null;
				td = null;
			}
			el = null;
			details = null;
			setHeight();
		}
	});
}

function shiftStep(el, dir) {
	var oppId = $(el).closest('[source=lightstreamer]').attr('oppid');
	var marketId = $(el).closest('[source=lightstreamer]').attr('marketId');
	var shiftParameter_2 = $(el).closest('[source=lightstreamer]').attr('shiftParameter_2');

	var dataParams = {'oppID' : oppId, 'marketID' : marketId, 'sign' : dir, 'writerID': userId, 'currentSpreadAO': shiftParameter_2}
	$.ajax({
		type: "POST",
		//url: jsonServiceUrl.replace('www.','') + "getInstantSpreadChange",
		url: jsonServiceUrl + "getInstantSpreadChange",
		data: JSON.stringify(dataParams),
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:  function(result) {
			shiftStepResp(result, el,dataParams);
		}
	});
	
	function shiftStepResp(result, el, dataParams) {
		var msgs = '';
		if (result.errorCode != 0) {
			if (result.errorCode == 830) {
				if (dataParams.sign) {
					msgs = "You have reached the maximum spread limit.";
				} else {
					msgs = "You have reached the minimum spread limit.";
				}
			} else if (result.errorCode == 831) {
				msgs = "You have reached the night shift limit.";
			}
			alert(msgs);
		}
		//console.log(result);
	}
}

function loginShowError() {
	var $error = $('#login\\:errorloginMsg');
	var $errorJs = $('#login\\:errorlogin');
	var $welcome = $('#loginWelcome');

	if ($error.html() != '' || $errorJs.html() != '') {
		$welcome.hide();
	}
}

function setFocus(id) {
	$('#' + id).focus()
}

function loading(el) {
	$(el).html('Loading ...');
}

function resizePageCallback(data) {
	var status = data.status;
	
	switch (status) {
		case "begin": // Before the ajax request is sent.
	          // ...
	          break;
	
		case "complete": // After the ajax response is arrived.
	          // ...
	          break;
	
		case "success": // After update of HTML DOM based on ajax response..
	          setHeight();
	          break;
	  }
}

//makes google fonts looks great :)
window.WebFontConfig = {
    google: { families: [ 'PT Sans:400,700' ] },
    active: function(){ $(window).trigger('fontsloaded') }
};
