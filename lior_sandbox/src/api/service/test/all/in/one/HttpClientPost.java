package api.service.test.all.in.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * @author liors
 * 
 * RESTful Java HttpClient with Apache 
 * Create a RESTful Java client with Apache HttpClient, to perform a “POST” request
 * 
 * It's Just an Example!
 *
 */
public class HttpClientPost {
	final static String ANYOPTION_API_URL = "https://api.anyoption.com/api_gateway/services"; 
	final static String SERVICE = "/getUserDetails"; 
	
	public static void main(String[] args) {
		String jsonRequest = "{\"apiUser\":{\"userName\":\"anyoption_test\",\"password\":\"q1w2e3\"},\"userName\":\"chenen\",\"password\":\"123456\",\"locale\":\"en\"}";		
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(ANYOPTION_API_URL + SERVICE);
			StringEntity request = new StringEntity(jsonRequest);
			request.setContentType("application/json");
			postRequest.setEntity(request);
			HttpResponse response = httpClient.execute(postRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println("\nServer Output: \n" + output);
								
				
				//TODO something COOL! :)
				
				
			}
			httpClient.getConnectionManager().shutdown();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

