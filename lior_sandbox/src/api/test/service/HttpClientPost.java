package api.test.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * @author liors
 * 
 * RESTful Java HttpClient with Apache 
 * Create a RESTful Java client with Apache HttpClient, to perform a “POST” request
 * 
 * It's Just an Example!
 *
 */
public class HttpClientPost {
	
	public static String doPost(String URL, String jsonRequest) {
		String output = "";
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {			
			HttpPost postRequest = new HttpPost(URL);
			StringEntity request = new StringEntity(jsonRequest);
			request.setContentType("application/json");
			postRequest.setEntity(request);
			HttpResponse response = httpClient.execute(postRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			output = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return output;
	}
}

