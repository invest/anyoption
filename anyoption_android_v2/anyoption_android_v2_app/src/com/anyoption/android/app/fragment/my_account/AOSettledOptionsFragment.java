package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.widget.adapter.AOTradingOptionsListAdapter;

import android.os.Bundle;
import android.util.Log;

public class AOSettledOptionsFragment extends SettledOptionsFragment{

	public static AOSettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment AOSettledOptionsFragment");
		AOSettledOptionsFragment fragment = new AOSettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new AOTradingOptionsListAdapter(investments, false);
	}
	
}
