package com.anyoption.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AORegisterFragment extends RegisterFragment{

	public static AORegisterFragment newInstance(Bundle bundle){
		AORegisterFragment registerFragment = new AORegisterFragment();	
		registerFragment.setArguments(bundle);
		return registerFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		View rootView = super.onCreateView(inflater, container, savedInstanceState); 
		TextView loginLinkTextView = (TextView) rootView.findViewById(R.id.register_screen_login_link_textview);
		loginLinkTextView.setText(getString(R.string.login) + " »");
		loginLinkTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(Screen.LOGIN, NavigationType.INIT));
			}
		});
		
		return rootView;
	}
	
	@Override
	protected void underlineTryWithoutAccountText(){
		//Ignore
	}
	
}
