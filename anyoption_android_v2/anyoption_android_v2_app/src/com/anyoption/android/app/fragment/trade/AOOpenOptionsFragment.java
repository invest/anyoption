package com.anyoption.android.app.fragment.trade;

import com.anyoption.android.app.widget.adapter.AOTradingOptionsListAdapter;

import android.os.Bundle;

public class AOOpenOptionsFragment extends OpenOptionsFragment{

	public static AOOpenOptionsFragment newInstance(Bundle bundle) {
		AOOpenOptionsFragment fragment = new AOOpenOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new AOTradingOptionsListAdapter(investments);
	}
	
}
