package com.anyoption.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AOWireDepositFragment extends WireDepositFragment{

    public static AOWireDepositFragment newInstance(Bundle bundle) {
    	AOWireDepositFragment wireDepositFragment = new AOWireDepositFragment();
        wireDepositFragment.setArguments(bundle);
        return wireDepositFragment;
    }
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		TextView skipLinkTextView = (TextView) rootView.findViewById(R.id.skip_link);
		if(application.getUser().getFirstDepositId() == 0){
			skipLinkTextView.setVisibility(View.VISIBLE);
			skipLinkTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
				}
			});
		}else{
			skipLinkTextView.setVisibility(View.GONE);
		}
		
		return rootView;
    }
    
    
}
