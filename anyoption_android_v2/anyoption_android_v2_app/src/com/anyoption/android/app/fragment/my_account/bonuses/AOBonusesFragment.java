package com.anyoption.android.app.fragment.my_account.bonuses;

import java.util.List;

import com.anyoption.android.app.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class AOBonusesFragment extends BonusesFragment {

	public static AOBonusesFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BonusesFragment.");
		AOBonusesFragment bonusesFragment = new AOBonusesFragment();
		bonusesFragment.setArguments(bundle);
		return bonusesFragment;
	}

	@Override
	protected ListViewAdapter createAdapter() {
		return new AOListViewAdapter(application, bonusesList);
	}

	public class AOBonusItemHolder extends BonusItemHolder {
		public View claimButton;
	}

	public class AOListViewAdapter extends ListViewAdapter implements OnClickListener {

		public AOListViewAdapter(Context context, List<BonusUsers> items) {
			super(context, items);
		}

		@Override
		protected BonusItemHolder createItemHolder() {
			return new AOBonusItemHolder();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			BonusUsers bonus = getItem(position);
			AOBonusItemHolder bonusItemHolder = (AOBonusItemHolder) view.getTag();

			if (convertView == null) {
				bonusItemHolder.claimButton = view.findViewById(R.id.bonus_list_item_claim_button);
			}

			if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
				bonusItemHolder.claimButton.setVisibility(View.VISIBLE);
				bonusItemHolder.stateTextView.setVisibility(View.GONE);
				bonusItemHolder.claimButton.setTag(bonusItemHolder);
				bonusItemHolder.claimButton.setOnClickListener(this);
			} else {
				bonusItemHolder.claimButton.setVisibility(View.GONE);
				bonusItemHolder.stateTextView.setVisibility(View.VISIBLE);
			}

			return view;
		}

		@Override
		public void onClick(View v) {
			if (v.getTag() instanceof AOBonusItemHolder) {
				AOBonusItemHolder bonusItemHolder = (AOBonusItemHolder) v.getTag();
				int position = bonusItemHolder.position;
				listView.performItemClick(listView.getChildAt(position), position, listAdapter.getItemId(position));
			}
		}

	}

}
