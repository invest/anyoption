package com.anyoption.android.app.fragment;

import com.anyoption.android.app.AOApplication.AOScreen;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;

import android.os.Bundle;

public class AOActionFragment extends ActionFragment{

	public static AOActionFragment newInstance(Bundle bundle){
		AOActionFragment fragment = new AOActionFragment();	
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void setupSpecialCases(Screenable screen) {
		if(screen == AOScreen.FIRST_NEW_CARD){
			if(application.getUser() != null && application.getUser().getFirstDepositId() > 0){
				isBackbutton = true;
				setupBackButton();
			}else{
				isMenuButton = false;
				hideMenuButton();
			}
		}
	}
	
	@Override
	protected boolean shouldSetupTitle(Screenable screenable){
		return true;
	}
	
	@Override
	protected Screenable getScreenFromEvent(Screenable screenable){
		if(screenable == Screen.FIRST_NEW_CARD){
			screenable = AOScreen.FIRST_NEW_CARD;
		}
		return screenable;
	}
	
}
