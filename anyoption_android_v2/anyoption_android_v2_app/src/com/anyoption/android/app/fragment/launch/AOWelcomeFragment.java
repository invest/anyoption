package com.anyoption.android.app.fragment.launch;

import com.anyoption.android.app.R;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AOWelcomeFragment extends WelcomeFragment{

	public static AOWelcomeFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new AOWelcomeFragment.");
		AOWelcomeFragment fragment = new AOWelcomeFragment();
		fragment.setArguments(bundle);	
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.welcome_fragment_layout, container, false);
		setupLoginRegisterButtons();
		setupTryWithoutAccount();
		return rootView;
	}
	
}