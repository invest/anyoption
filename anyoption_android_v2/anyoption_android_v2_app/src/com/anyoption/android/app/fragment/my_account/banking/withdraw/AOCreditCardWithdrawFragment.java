package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.WithdrawBonusResetDialogFragment;
import com.anyoption.android.app.popup.WithdrawBonusResetDialogFragment.WithdrawBonusResetPopupListener;
import com.anyoption.android.app.util.AOConstants;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;

import android.os.Bundle;
import android.util.Log;

public class AOCreditCardWithdrawFragment extends CreditCardWithdrawFragment{

	
	public static AOCreditCardWithdrawFragment newInstance(Bundle bundle){
		AOCreditCardWithdrawFragment creditCardWithdrawFragment = new AOCreditCardWithdrawFragment();
		creditCardWithdrawFragment.setArguments(bundle);
		return creditCardWithdrawFragment;
	}
	
	private boolean isCheckingBalance;
	
	@Override
	protected void tryToWithdraw() {
		if(!isCheckingBalance){
			getSplitBalance();
		}
	}
	
	private void getSplitBalance(){
		isCheckingBalance = true;
		DepositBonusBalanceMethodRequest request = new DepositBonusBalanceMethodRequest();
		request.setUserId(application.getUser().getId());
		request.setBalanceCallType(1);
		application.getCommunicationManager().requestService(this, "getSplitBalanceCallback", AOConstants.SERVICE_GET_SPLIT_BALANCE, request, DepositBonusBalanceMethodResult.class, false, false);
	}
	
	public void getSplitBalanceCallback(Object resObj){
		DepositBonusBalanceMethodResult result = (DepositBonusBalanceMethodResult) resObj;
		if(result != null && result instanceof DepositBonusBalanceMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			if(result.getDepositBonusBalanceBase() != null){
				DepositBonusBalanceBase depositBonusBalanceBase = result.getDepositBonusBalanceBase();
				boolean shouldShowBonusResetPopup = (depositBonusBalanceBase.getBonusBalance() > 0l) ? true:false;
				if(shouldShowBonusResetPopup){
					final WithdrawBonusResetDialogFragment bonusResetDialogFragment = (WithdrawBonusResetDialogFragment) application.getFragmentManager().showDialogFragment(WithdrawBonusResetDialogFragment.class, null);
					bonusResetDialogFragment.setListener(new WithdrawBonusResetPopupListener() {
						@Override
						public void onYes() {
							AOCreditCardWithdrawFragment.super.tryToWithdraw();
							bonusResetDialogFragment.dismiss();
						}
						@Override
						public void onNo() {
							bonusResetDialogFragment.dismiss();
							application.postEvent(new NavigationEvent(Screen.TRADE, NavigationType.INIT));
						}
					});
				}else{
					super.tryToWithdraw();
				}
			}
		}else{
			Log.e(TAG, "Error in getSplitBalanceCallback");
			ErrorUtils.handleResultError(result);
		}
		
		isCheckingBalance = false;
	}
}
