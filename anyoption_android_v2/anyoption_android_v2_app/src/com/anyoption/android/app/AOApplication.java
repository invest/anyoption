package com.anyoption.android.app;
import java.util.HashMap;

import com.anyoption.android.app.manager.AOAppoxeeManager;
import com.anyoption.android.app.manager.AOGoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.anyoption.android.app.util.AOConstants;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class AOApplication extends Application {

	public static final String SUBCLASS_PREFIX = "AO";
	
	protected AOAppoxeeManager 	appoxeeManager;
	
	//#################################################################################################################
	//######################					ANYOPTION	SCREENs						###################################
	//#################################################################################################################
	
	public interface AOScreenable extends Screenable{
		
	}
	
	public enum AOScreen implements AOScreenable {
		FIRST_NEW_CARD("new card", get().getResources().getString(R.string.newCardScreenTitle), 0, Screenable.SCREEN_FLAG_ADD_HELP, Screenable.SCREEN_FLAG_ADD_BACK);
		
		/**
		 * Tag used to search.
		 */
		private String tag;

		/**
		 * A default title. "" - if no default title is needed.
		 */
		private String title;
		/**
		 * Id of an image used instead of title. 0 - if no image is needed.
		 */
		private int imageID;

		/**
		 * Additional details for the Screen
		 */
		private int[] flags;
		
		/**
		 * Map of all screens
		 */
		private static HashMap<String, Screenable> screensMap;
		
		AOScreen(String tag, String title, int imageID, int... flags){
			this.tag = tag;
			this.title = title;
			this.imageID = imageID;
			this.flags = flags;
		}

		public String getTag() {
			return tag;
		}
		public String getTitle() {
			return title;
		}
		public int getImageID() {
			return imageID;
		}
		public boolean isHasHelpInfo() {
			return false;
		}
		public int[] getFlags(){
			return this.flags;
		}

		public static Screenable getScreenByTag(String tag) {
			if (screensMap == null) {
				Screenable[] baseScreens = Screen.values();
				// Add base screens
				screensMap = new HashMap<String, Screenable>(baseScreens.length);
				for (Screenable screen : baseScreens) {
					screensMap.put(screen.getTag(), screen);
				}

				// Add CO screens
				Screenable[] coScreens = AOScreen.values();
				for (Screenable screen : coScreens) {
					screensMap.put(screen.getTag(), screen);
				}
			}

			return screensMap.get(tag);
		}

	}
	//#################################################################################################################
	//#################################################################################################################
	//#################################################################################################################
	
	@Override
	public String getServiceURL() {
		return AOConstants.SERVICE_URL;
	}

	@Override
	public String getLSURL() {
		return AOConstants.LS_URL;
	}

	@Override
	public String getMerchantID() {
		return AOConstants.MERCHANT_ID;
	}

	@Override
	public String getOneClickURL() {
		return AOConstants.ONE_CLICK_URL;
	}

	@Override
	public String getWebSiteLink() {
		return AOConstants.WEB_SITE_LINK;
	}

	@Override
	protected String getSubclassPrefix() {
		return SUBCLASS_PREFIX;
	}
	
	//#################################################################################################################
	//###########################					MANAGERS						###################################
	//#################################################################################################################
	
	@Override
	public synchronized AOAppoxeeManager getAppoxeeManager(){
		if(appoxeeManager == null){
			appoxeeManager = new AOAppoxeeManager(this);
		}
		return appoxeeManager;
	}

	@Override
	public GoogleAdWordsManager getGoogleAdWordsManager() {
		if (googleAdWordsManager == null) {
			googleAdWordsManager = new AOGoogleAdWordsManager(this);
		}
		return googleAdWordsManager;
	}

	@Override
	public synchronized GoogleAnalyticsManager getGoogleAnalyticsManager() {
		return new GoogleAnalyticsManager(R.xml.app_tracker);
	}
	//#################################################################################################################
	
	
}
