package com.anyoption.android.app.manager;

import com.anyoption.android.app.AOApplication;

public class AOAppoxeeManager extends AppoxeeManager{

	public static final String APP_KEY 					= "54aa6a6f087a48.12356917";
	public static final String SECRET_KEY 				= "54aa6a6f087b74.34470779";
	
	public AOAppoxeeManager(AOApplication application) {
		super(application);
	}
	
	protected String getApiKey(){
		return AOAppoxeeManager.APP_KEY;
	}
	
	protected String getSecretKey(){
		return AOAppoxeeManager.SECRET_KEY;
	}
	
}
