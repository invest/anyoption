package com.anyoption.android.app.manager;

import com.anyoption.android.app.AOApplication;
import com.anyoption.android.app.util.AOConstants;

public class AOCommunicationManager extends CommunicationManager {

	public static final String TAG = AOCommunicationManager.class.getSimpleName();

	public AOCommunicationManager(AOApplication application) {
		super(application);
	}
	
}