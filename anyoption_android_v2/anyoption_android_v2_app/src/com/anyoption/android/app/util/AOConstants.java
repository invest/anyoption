package com.anyoption.android.app.util;

import com.anyoption.android.app.util.constants.Constants;

public class AOConstants extends Constants{

	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (LIOR)		////////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://liors.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://lstest.anyoption.com:80";
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (TONI)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.antonba.bg.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://lstest.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.testenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (JIMI)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.dzhamaldv.bg.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://lstest.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.testenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (KIRO)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://192.168.100.64/jsonService/AnyoptionService/";
//	public static final String LS_URL = "https://ls.bgtestenv.anyoption.com:443";//TODO 443 for https
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "https://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "https://m.bgtestenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (STEFAN)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://192.168.100.147/jsonService/AnyoptionService/";
//	public static final String LS_URL = "https://ls.bgtestenv.anyoption.com:443";//TODO 443 for https
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "https://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "https://m.bgtestenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Eyal Goren)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://eyal.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://lstest.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.testenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	//////////////////			TEST ENVIRONMENT		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.testenv.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://lstest.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.testenv.anyoption.com/mobile/";

	//################################################################################################
	
	//################################################################################################
	////////////////////////		BG TEST ENVIRONMENT			/////////////////////////////////////
	//################################################################################################ 
	
	public static final String SERVICE_URL = "https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/";
	public static final String LS_URL = "http://ls.bgtestenv.anyoption.com:80";//TODO 443 for https, 80 for http
	public static final String MERCHANT_ID = "000203AYP";
	public static final String ONE_CLICK_URL = "https://test.envoytransfers.com";
	public static final String WEB_SITE_LINK = "https://m.bgtestenv.anyoption.com/mobile/";
	
	//################################################################################################
	
	//################################################################################################
    //////////////////         LIVE ENVIRONMENT		////////////////////////////////////////////////
	//################################################################################################
	
//	public static final String SERVICE_URL = "https://json.anyoptionservice.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://ls.anyoptionservice.com:80";//TODO 443 for https
//	public static final String MERCHANT_ID = "000266AYP";
//	public static final String ONE_CLICK_URL = "https://www.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.anyoption.com/mobile/";

	//################################################################################################
	
}
