package com.anyoption.android.app.widget.form;

import java.util.Arrays;
import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.json.results.CurrenciesResult;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

public class AOFormCurrencySelector extends FormCurrencySelector{

	public AOFormCurrencySelector(Context context) {
		super(context);
	}
	
	public AOFormCurrencySelector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public AOFormCurrencySelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void setupCurenciesDisplayNames(){
		Application application = Application.get();
		for(Currency currency : currencyList){
			if(currency.getCode().equals("USD")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_USD));
			}else if(currency.getCode().equals("EUR")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_EUR));
			}else if(currency.getCode().equals("GBP")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_GBP));
			}else if(currency.getCode().equals("TL") || currency.getCode().equals("TRY")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_TL));
			}else if(currency.getCode().equals("AUD")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_AUD));
			}else if(currency.getCode().equals("SEK")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_SEK));
			}else if(currency.getCode().equals("RUB")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_RUB));
			}else if(currency.getCode().equals("CNY")){
				currency.setDisplayName(application.getString(R.string.currencyPopupItem_CNY));
			}
		}
	}
	
}
