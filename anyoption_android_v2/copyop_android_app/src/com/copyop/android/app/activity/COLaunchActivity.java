package com.copyop.android.app.activity;

import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.LaunchActivity;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.manager.AppoxeeManager.InboxListener;
import com.anyoption.android.app.util.constants.Constants;
import com.appoxee.inbox.Message;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

public class COLaunchActivity extends LaunchActivity {
	
	private View promotionLayout;

	@Override
	@SuppressWarnings("unchecked")
	protected void handleRestart(Bundle intentBundle) {
		super.handleRestart(intentBundle);
		Object objectProfile = intentBundle.get(COConstants.EXTRA_PROFILE);
		if (objectProfile != null && objectProfile instanceof Profile) {
			Profile profile = (Profile) intentBundle.get(COConstants.EXTRA_PROFILE);
			List<Long> copyAmounts = (List<Long>) intentBundle.get(COConstants.EXTRA_COPY_AMOUNTS);
			double copyOddsWin = intentBundle.getDouble(COConstants.EXTRA_COPY_ODDS_WIN);
			double copyOddsLose = intentBundle.getDouble(COConstants.EXTRA_COPY_ODDS_LOSE);
			int maxCopiersPerAccount = intentBundle.getInt(COConstants.EXTRA_MAX_COPIERS_PER_ACCOUNT);
			
			Log.d(TAG, "#################");
			Log.d(TAG, "Getting Saved Profile");
			Log.d(TAG, "Profile = " + profile.getNickname());
			Log.d(TAG, "#################");

			COApplication.get().setProfile(profile);
			COApplication.get().setCopyAmounts(copyAmounts);
			COApplication.get().setCopyOddsWin(copyOddsWin);
			COApplication.get().setCopyOddsLose(copyOddsLose);
			COApplication.get().setMaxCopiersPerAccount(maxCopiersPerAccount);
		}
	}
	
	/**
	 * Checks whether a welcome screen should be shown.
	 */
	@Override
	protected void checkForWelcome() {
		checkForPromotion();
	}

	@Override
	protected void handleWelcome(){
		removeSplash();
		if(!application.getSharedPreferencesManager().getBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, false)){
			showWelcomeScreen();
		} else {
			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.EXTRA_SCREEN, COScreen.LSS_HOME);				
			openMainActivity(bundle);
		}
	}
	
	private void checkForPromotion() {
		application.getAppoxeeManager().hasInboxMessages(new InboxListener() {
			@Override
			public void gotInboxMessages(List<Message> inboxMessagesList) {
				promotionLayout = findViewById(R.id.launch_promotion_layout);
				if(inboxMessagesList != null && !inboxMessagesList.isEmpty()) {
					removeSplash();
					promotionLayout.setVisibility(View.VISIBLE);
					showPromotion(inboxMessagesList.get(inboxMessagesList.size() - 1));
				}else {
					promotionLayout.setVisibility(View.GONE);
					handleWelcome();
				}
			}
		});
				
	}

	private void showPromotion(Message message) {
		WebView webView = (WebView) findViewById(R.id.launch_promotion_web_view);
        webView.loadUrl(message.getLink());
		View skipButton = findViewById(R.id.launch_promotion_skip_button);
		skipButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				promotionLayout.setVisibility(View.GONE);
				handleWelcome();
			}
		});
	}
	
	@Override
	protected void setupSplashScreen() {
		// Do nothing
	}
	
	@Override
	protected Class<? extends BaseFragment> getFragmentClass(Bundle bundle, Screenable screen) {
		if (screen == COScreen.LSS_SIGNUP) {
			bundle.putSerializable(Constants.EXTRA_SCREEN, COScreen.LSS_SIGNUP);
			return null;
		} else if (screen == COScreen.LSS_LOGIN_SIGNUP) {
			bundle.putSerializable(Constants.EXTRA_SCREEN, COScreen.LSS_LOGIN_SIGNUP);
			return null;
		} else if (screen == COScreen.NEWS_HOT_EXPLORE) {
			bundle.putSerializable(Constants.EXTRA_SCREEN, COScreen.NEWS_HOT_EXPLORE);
			return null;
		} else if (screen == Screen.SETTINGS) {
			bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.SETTINGS);
			return null;
		}
		
		return super.getFragmentClass(bundle, screen);
	}
}