package com.copyop.android.app.fragment;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.COApplication.COScreenable;
import com.copyop.android.app.R;
import com.copyop.android.app.widget.DoneButton;
import com.copyop.android.app.widget.InboxButton;
import com.copyop.android.app.widget.LanguageButton;
import com.copyop.android.app.widget.PoweredByAnyoptionLayout;
import com.copyop.android.app.widget.ShareButton;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class COActionFragment extends ActionFragment {

	public final String TAG = COActionFragment.class.getSimpleName();
	
	public static COActionFragment newInstance(Bundle bundle){
		COActionFragment coActionFragment = new COActionFragment();	
		coActionFragment.setArguments(bundle);
		return coActionFragment;
	}
	
	private boolean isInboxButton;
	private boolean isShareButton;
	private boolean isDoneButton;
	private boolean isPoweredByAnyoption;
	private boolean isTitleOrange;
	private boolean isLanguage;
	
	private InboxButton inboxButton; 
	private ShareButton shareBtton;
	private DoneButton doneButton;
	private PoweredByAnyoptionLayout poweredByAnyoptionLayout;
	private LanguageButton languageButton;
	
	@Override
	public void onEventMainThread(NavigationEvent event) {
		//Set default values:
		isInboxButton = false;
		isShareButton = false;
		isDoneButton = false;
		isPoweredByAnyoption = false;
		isTitleOrange = false;
		isLanguage = false;
		/////////////////
		super.onEventMainThread(event);
	}
	
	@Override
	public void onEventMainThread(LoginLogoutEvent event){
		if(event.getType() == Type.LOGOUT){
			hideMenuButton();
		}
	}
	
	@Override
	protected void setupScreenPreferences(int... flags){
		super.setupScreenPreferences(flags);
		for(int flag : flags){
			if(flag == COScreenable.SCREEN_FLAG_ADD_INBOX){
				isInboxButton = true;
			}else if(flag == COScreenable.SCREEN_FLAG_REMOVE_INBOX){
				isInboxButton = false;
			}else if(flag == COScreenable.SCREEN_FLAG_ADD_SHARE){
				isShareButton = true;
			}else if(flag == COScreenable.SCREEN_FLAG_REMOVE_SHARE){
				isShareButton = false;
			}else if(flag == COScreenable.SCREEN_FLAG_ADD_DONE){
				isDoneButton = true;
			}else if(flag == COScreenable.SCREEN_FLAG_REMOVE_DONE){
				isDoneButton = false;
			}else if(flag == COScreenable.SCREEN_FLAG_ADD_POWERED_BY){
				isPoweredByAnyoption = true;
			}else if(flag == COScreenable.SCREEN_FLAG_REMOVE_POWERED_BY){
				isPoweredByAnyoption = false;
			}else if(flag == COScreenable.SCREEN_FLAG_TITLE_ORANGE){
				isTitleOrange = true;
			}else if(flag == COScreenable.SCREEN_FLAG_ADD_LANGUAGE) {
				// Language selector will be shown only when the user is not logged in.
				isLanguage = !application.isLoggedIn();
			}else if(flag == COScreenable.SCREEN_FLAG_REMOVE_LANGUAGE){
				isLanguage = false;
			}
		}
	}

	@Override
	protected void setupTitle() {
		if(isTitleOrange){
			titleTextView.setTextAppearance(application, R.style.action_text_light);
			titleTextView.setTextColor(getResources().getColor(R.color.action_bar_text_orange));
			titleTextView.setFont(Font.ROBOTO_LIGHT);
		}else{
			titleTextView.setTextAppearance(application, R.style.action_text);
			titleTextView.setTextColor(getResources().getColor(R.color.action_bar_text));
			titleTextView.setFont(Font.ROBOTO_REGULAR);
		}
		super.setupTitle();
	}
	
	@Override
	protected Screenable getScreenFromEvent(Screenable screen){
		COScreen coScreen = null;
		try {
			// We check if the Screen is a new CO Screen or a OVERRIDDEN Screen.
			coScreen = COScreen.valueOf(screen.toString());
		} catch (Exception e) {
			//There is no CO Screen with this name.
			Log.d(TAG, "Could NOT find CO Screen!");
		}
		if(coScreen != null){
			return coScreen;
		}
		return super.getScreenFromEvent(screen);
	}
	
	@Override
	protected void setupUI() {
		super.setupUI();
		if(!isResumed()){
			shouldRefresh = true;
			return;
		}
		shouldRefresh = false;
		// Set up the INBOX Button:
		if(isInboxButton) {
			setupInboxButton();
		}else{
			hideInboxButton();
		}
		/////////////////////
		// Set up the SHARE Button:
		if(isShareButton) {
			setupShareButton();
		}else{
			hideShareButton();
		}
		///////////////////////
		// Set up the DONE Button:
		if(isDoneButton) {
			setupDoneButton();
		}else{
			hideDoneButton();
		}
		///////////////////////
		// Set up the Powered By Layout:
		if(isPoweredByAnyoption) {
			setupPoweredBy();
		}else{
			hidePoweredBy();
		}
		///////////////////////
		// Set up the Language selector:
		if(isLanguage){
			setupLanguage();
		}else{
			hideLanguage();
		}
		/////////////////////
	}

	@Override
	protected void setupMenuButton() {
		if(application.isLoggedIn()){
			super.setupMenuButton();			
		}
	}
	
	protected void setupInboxButton(){
		if(inboxButton == null){
			inboxButton = new InboxButton(application);
			rightView.addView(inboxButton);
			rightView.invalidate();
		}
		if(inboxButton.getVisibility() != View.VISIBLE){
			inboxButton.setVisibility(View.VISIBLE);
			rightView.invalidate();
		}
	}

	protected void hideInboxButton(){
		if(inboxButton != null){
			inboxButton.setVisibility(View.GONE);
		}
	}
	
	protected void setupShareButton(){
		if(shareBtton == null){
			shareBtton = new ShareButton(application);
			rightView.addView(shareBtton);
			rightView.invalidate();
		}
		if(shareBtton.getVisibility() != View.VISIBLE){
			shareBtton.setVisibility(View.VISIBLE);
			rightView.invalidate();
		}
	}

	protected void hideShareButton(){
		if(shareBtton != null){
			shareBtton.setVisibility(View.GONE);
		}
	}
	
	protected void setupDoneButton(){
		if(doneButton == null){
			doneButton = new DoneButton(application);
			leftView.addView(doneButton);
			leftView.invalidate();
		}
		if(doneButton.getVisibility() != View.VISIBLE){
			doneButton.setVisibility(View.VISIBLE);
			leftView.invalidate();
		}
	}

	protected void hideDoneButton(){
		if(doneButton != null){
			doneButton.setVisibility(View.GONE);
		}
	}
	
	protected void setupPoweredBy(){
		if(poweredByAnyoptionLayout == null){
			poweredByAnyoptionLayout = new PoweredByAnyoptionLayout(application);
			rightView.addView(poweredByAnyoptionLayout);
			rightView.invalidate();
		}
		if(poweredByAnyoptionLayout.getVisibility() != View.VISIBLE){
			poweredByAnyoptionLayout.setVisibility(View.VISIBLE);
			rightView.invalidate();
		}
	}

	protected void hidePoweredBy(){
		if(poweredByAnyoptionLayout != null){
			poweredByAnyoptionLayout.setVisibility(View.GONE);
		}
	}
	
	protected void setupLanguage(){
		if(languageButton == null){
			languageButton = new LanguageButton(application);
			languageButton.setPadding(getResources().getDimensionPixelSize(R.dimen.default_page_padding), 0, 0, 0);
			leftView.addView(languageButton);
			leftView.invalidate();
		}
		if(languageButton.getVisibility() != View.VISIBLE){
			languageButton.setVisibility(View.VISIBLE);
			leftView.invalidate();
		}
	}

	protected void hideLanguage(){
		if(languageButton != null){
			languageButton.setVisibility(View.GONE);
		}
	}
}
