package com.copyop.android.app.fragment.news_feed;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.FragmentManager;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.manager.PopUpManager.PopupListener;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.fragment.walkthrough.HotWalkthroughDialog;
import com.copyop.android.app.fragment.walkthrough.NewsWalkthroughDialog;
import com.copyop.android.app.popup.AutoWatchDialogFragment;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class NewsHotExploreFragment extends NavigationalFragment implements PopupListener{

	public static final byte TAB_NEWS 		= 1;
	public static final byte TAB_HOT 		= 2;
	public static final byte TAB_EXPLORE 	= 3;

	private NewsFragment newsFragment;
	private HotFragment hotFragment;
	private ExploreFragment exploreFragment;
	
	public static NewsHotExploreFragment newInstance(Bundle bundle){
		NewsHotExploreFragment fragment = new NewsHotExploreFragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	private FragmentManager fragmentManager;

	private View newsTab;
	private View newsTabIndicator;
	private View hotTab;
	private View hotTabIndicator;
	private View exploreTab;
	private View exploreTabIndicator;
	private View currentTabLayout;

	private byte currentTab = TAB_NEWS;
	
	private boolean isFirstLogin;
	private boolean isQuestPopupShown;

	@Override
	protected Screenable setupScreen() {
		return COScreen.NEWS_HOT_EXPLORE;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "On Create");
		Bundle bundle = getArguments();
		if(bundle != null){
			//Get arguments:
			currentTab = bundle.getByte(COConstants.EXTRA_NEWS_HOT_EXPLORE_TAB, currentTab);
		}

		fragmentManager = application.getFragmentManager();

		isFirstLogin = application.getSharedPreferencesManager().getBoolean(
				Constants.PREFERENCES_IS_FIRST_LOGIN, false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		application.getPopUpManager().addPopupListener(this);
		
		Bundle bundle = getArguments();
		boolean isOpenFollowDialog = bundle.getBoolean(COConstants.EXTRA_OPEN_FOLLOW_DIALOG, false);
		if(isOpenFollowDialog){
			currentTab = TAB_NEWS;
			selectTab();
		}
	}

	@Override
	public void onPause() {
		application.getPopUpManager().removePopupListener(this);
		super.onPause();
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			// Make sure that only the selected tab is visible.
			openTab();
		}

		super.onHiddenChanged(hidden);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.news_hot_explore_layout, container, false);

		isQuestPopupShown = (application.getPopUpManager().isPopupShown(PopUpManager.POPUP_QUEST_BLOCKED) || application.getPopUpManager().isPopupShown(PopUpManager.POPUP_QUEST_RESTRICTED));
		
		newsTab = rootView.findViewById(R.id.news_hot_explore_news_tab);
		newsTabIndicator = rootView.findViewById(R.id.news_hot_explore_news_tab_select_indicator);
		hotTab = rootView.findViewById(R.id.news_hot_explore_hot_tab);
		hotTabIndicator = rootView.findViewById(R.id.news_hot_explore_hot_tab_select_indicator);
		exploreTab = rootView.findViewById(R.id.news_hot_explore_explore_tab);
		exploreTabIndicator = rootView.findViewById(R.id.news_hot_explore_explore_tab_select_indicator);
		
		currentTabLayout = rootView.findViewById(R.id.news_hot_explore_current_tab_layout);
		
		OnClickListener tabClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v == newsTab){
					if(currentTab == TAB_NEWS){ 
						return;
					}
					currentTab = TAB_NEWS;
				}else if(v == hotTab){
					if(currentTab == TAB_HOT){ 
						return;
					}
					currentTab = TAB_HOT;
				}else if(v == exploreTab){
					if(currentTab == TAB_EXPLORE){ 
						return;
					}
					currentTab = TAB_EXPLORE;
				}
				selectTab();
			}
		};
		
		newsTab.setOnClickListener(tabClickListener);
		hotTab.setOnClickListener(tabClickListener);
		exploreTab.setOnClickListener(tabClickListener);
		
		//Initial Tab:
		selectTab();
		
		return rootView;
	}

	private void selectTab(){
		if(currentTab == TAB_NEWS){
			newsTabIndicator.setVisibility(View.VISIBLE);
			hotTabIndicator.setVisibility(View.INVISIBLE);
			exploreTabIndicator.setVisibility(View.INVISIBLE);
		}else if(currentTab == TAB_HOT){
			newsTabIndicator.setVisibility(View.INVISIBLE);
			hotTabIndicator.setVisibility(View.VISIBLE);
			exploreTabIndicator.setVisibility(View.INVISIBLE);
		}else if(currentTab == TAB_EXPLORE){
			newsTabIndicator.setVisibility(View.INVISIBLE);
			hotTabIndicator.setVisibility(View.INVISIBLE);
			exploreTabIndicator.setVisibility(View.VISIBLE);
		}
		
		openTab();
	}

	private void openTab() {
		Bundle bundle = getArguments();
		android.support.v4.app.FragmentManager activityFragmentManager = getChildFragmentManager();

		if (currentTab == TAB_NEWS) {
			if (newsFragment == null) {
				newsFragment = NewsFragment.newInstance(bundle);
			}

			fragmentManager.replaceFragment(currentTabLayout.getId(), newsFragment,
					activityFragmentManager);
		} else if (currentTab == TAB_HOT) {
			if (hotFragment == null) {
				hotFragment = HotFragment.newInstance(bundle);
			}

			fragmentManager.replaceFragment(currentTabLayout.getId(), hotFragment,
					activityFragmentManager);
		} else if (currentTab == TAB_EXPLORE) {
			if (exploreFragment == null) {
				exploreFragment = ExploreFragment.newInstance(bundle);
			}

			fragmentManager.replaceFragment(currentTabLayout.getId(), exploreFragment,
					activityFragmentManager);
		}

		if (currentTab == TAB_NEWS) {
			setUpNewsDialogs();
		} else if (currentTab == TAB_HOT) {
			setUpHotWalkthrough();
		}
	}

	private void setUpNewsDialogs() {
		if(isQuestPopupShown){
			return;
		}
		
		boolean shouldShowWalkthrough = !application.getSharedPreferencesManager().getBoolean(
				COConstants.PREFERENCES_IS_NEWS_WALKTHROUGH_SHOWN, false);
		boolean shouldShowAutoWatched = isFirstLogin
				&& application.getUser().getFirstDepositId() != 0
				&& !application.getSharedPreferencesManager().getBoolean(
						COConstants.PREFERENCES_IS_AUTO_WATCHED_SHOWN, false);

		if (shouldShowWalkthrough) {
			BaseDialogFragment walkthroughDialog = application.getFragmentManager()
					.showDialogFragment(NewsWalkthroughDialog.class, new Bundle());
			if (shouldShowAutoWatched) {
				walkthroughDialog.setOnDismissListener(new OnDialogDismissListener() {

					@Override
					public void onDismiss() {
						showAutoWatchDialog();
					}
				});
			}
		} else if (shouldShowAutoWatched) {
			showAutoWatchDialog();
		}
	}

	private void showAutoWatchDialog() {
		application.getFragmentManager().showDialogFragment(AutoWatchDialogFragment.class,
				new Bundle());
	}

	private void setUpHotWalkthrough() {
		boolean shouldShowWalkthrough = !application.getSharedPreferencesManager().getBoolean(
				COConstants.PREFERENCES_IS_HOT_WALKTHROUGH_SHOWN, false);
		if (shouldShowWalkthrough) {
			application.getFragmentManager().showDialogFragment(HotWalkthroughDialog.class,
					new Bundle());
		}
	}

	@Override
	public void onPopupShown(int popupCode) {
		if(popupCode == PopUpManager.POPUP_QUEST_BLOCKED || popupCode == PopUpManager.POPUP_QUEST_RESTRICTED){
			isQuestPopupShown = true;
		}
	}

	@Override
	public void onPopupDismissed(int popupCode) {
		if(popupCode == PopUpManager.POPUP_QUEST_BLOCKED || popupCode == PopUpManager.POPUP_QUEST_RESTRICTED){
			isQuestPopupShown = false;
			if (currentTab == TAB_NEWS) {
				setUpNewsDialogs();
			}
		}
	}

}
