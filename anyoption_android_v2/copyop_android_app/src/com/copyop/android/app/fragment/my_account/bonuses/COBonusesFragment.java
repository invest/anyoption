package com.copyop.android.app.fragment.my_account.bonuses;

import java.util.List;

import com.anyoption.android.app.fragment.my_account.bonuses.BonusesFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.BonusUsers;
import com.copyop.android.app.R;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class COBonusesFragment extends BonusesFragment {

	public static COBonusesFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BonusesFragment.");
		COBonusesFragment bonusesFragment = new COBonusesFragment();
		bonusesFragment.setArguments(bundle);
		return bonusesFragment;
	}

	@Override
	protected ListViewAdapter createAdapter() {
		return new COListViewAdapter(application, bonusesList);
	}

	public class COBonusItemHolder extends BonusItemHolder {
		public View claimButton;
	}

	public class COListViewAdapter extends ListViewAdapter implements OnClickListener {

		public COListViewAdapter(Context context, List<BonusUsers> items) {
			super(context, items);
		}

		@Override
		protected BonusItemHolder createItemHolder() {
			return new COBonusItemHolder();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			BonusUsers bonus = getItem(position);
			COBonusItemHolder bonusItemHolder = (COBonusItemHolder) view.getTag();

			if (convertView == null) {
				bonusItemHolder.claimButton = view.findViewById(R.id.bonus_list_item_claim_button);
			}

			if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
				bonusItemHolder.claimButton.setVisibility(View.VISIBLE);
				bonusItemHolder.stateTextView.setVisibility(View.GONE);
				bonusItemHolder.claimButton.setTag(bonusItemHolder);
				bonusItemHolder.claimButton.setOnClickListener(this);
			} else {
				bonusItemHolder.claimButton.setVisibility(View.GONE);
				bonusItemHolder.stateTextView.setVisibility(View.VISIBLE);
			}

			return view;
		}

		@Override
		public void onClick(View v) {
			if (v.getTag() instanceof COBonusItemHolder) {
				COBonusItemHolder bonusItemHolder = (COBonusItemHolder) v.getTag();
				int position = bonusItemHolder.position;
				listView.performItemClick(listView.getChildAt(position), position, listAdapter.getItemId(position));
			}
		}

	}

}
