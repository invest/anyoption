package com.copyop.android.app.fragment.login_register;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COLoginManager;
import com.facebook.Session;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class LSSLoginFragment extends NavigationalFragment {
	public static final String TAG = LSSLoginFragment.class.getSimpleName();

//	private Form form;
	private COLoginManager loginManager;

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSLoginFragment newInstance() {	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSLoginFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LSSLoginFragment.");
		LSSLoginFragment loginFragment = new LSSLoginFragment();	
		loginFragment.setArguments(bundle);
		return loginFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		View rootView = inflater.inflate(R.layout.lss_login_fragment, container, false);
		TextView headerTextView = (TextView) rootView.findViewById(R.id.lss_anyoption_copyop_credentials);
		Spannable spannable = TextUtils.decorateInnerText(headerTextView, null, headerTextView.getText().toString(), "anyoption", Font.ROBOTO_MEDIUM, 0, 0);
		TextUtils.decorateInnerText(headerTextView, spannable, headerTextView.getText().toString(), "copyop", Font.ROBOTO_MEDIUM, 0, 0);
		TextUtils.decorateInnerText(headerTextView, spannable, headerTextView.getText().toString(), "any", Font.ROBOTO_BOLD, 0, 0);
		
		boolean isTablet = getResources().getBoolean(R.bool.isTablet);
		FormEditText emailField = (FormEditText) rootView.findViewById(R.id.lss_email_edittext);
		FormEditText passwordField = (FormEditText) rootView.findViewById(R.id.lss_pass_edittext);
		TextView forgotPassTextView = (TextView) rootView.findViewById(R.id.lss_forgot_password);
		RequestButton loginButton = (RequestButton) rootView.findViewById(R.id.lss_ao_login_button);
		FormCheckBox rememberPasswordCheckBox = (FormCheckBox) rootView.findViewById(R.id.lss_rememeber_password);
		
		loginManager = new COLoginManager();
		loginManager.onCreateView(this, isTablet, emailField, passwordField, forgotPassTextView, loginButton, rememberPasswordCheckBox);
		
		View signupButton = rootView.findViewById(R.id.lss_login_signup_button);
		signupButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.postEvent(new NavigationEvent(COScreen.LSS_SIGNUP, NavigationType.HORIZONTAL));
			}
		});
		
		View forgotPasswordButton = rootView.findViewById(R.id.lss_forgot_password);
		forgotPasswordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				COApplication.get().postEvent(new NavigationEvent(Screen.FORGOT_PASSWORD, NavigationType.DEEP));
			}
		});
		
		View supportButton = rootView.findViewById(R.id.lss_support);
		supportButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				COApplication.get().postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP));
			}
		});

		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.LSS_LOGIN_SIGNUP;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode + " data: " + data);
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
		if (resultCode != Activity.RESULT_OK) {
			loginManager.finishLoginCallback();
		}
	}
	
}