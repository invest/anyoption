package com.copyop.android.app.fragment.profile.list;

import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.CopyListMethodResult;

import android.os.Bundle;
import android.util.Log;

public class CopiersListFragment extends ProfilesListFragment {

	private static final String TAG = CopiersListFragment.class.getSimpleName();

	/**
	 * Creates new instance of this fragment.
	 */
	public static CopiersListFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Creating CopiersListFragment fragment");
		CopiersListFragment fragment = new CopiersListFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.COPIERS_LIST;
	}

	@Override
	protected Class<? extends MethodResult> getResultClass() {
		return CopyListMethodResult.class;
	}

	@Override
	protected String getRequestMethodName() {
		return COConstants.SERVICE_GET_COPIERS;
	}

	@Override
	protected ProfileMethodRequest getProfilesRequest() {
		return new ProfileMethodRequest();
	}

	@Override
	protected List<? extends Profile> getResultProfiles(MethodResult resultObj) {
		CopyListMethodResult result = (CopyListMethodResult) resultObj;
		return result.getCopyList();
	}

}
