package com.copyop.android.app.fragment.profile.list;

import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.requests.WatchListMethodRequest;
import com.copyop.json.results.WatchListMethodResult;

import android.os.Bundle;
import android.util.Log;

public class WatchersListFragment extends ProfilesListFragment {

	private static final String TAG = WatchersListFragment.class.getSimpleName();

	/**
	 * Creates new instance of this fragment.
	 */
	public static WatchersListFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Creating WatchersListFragment fragment");
		WatchersListFragment fragment = new WatchersListFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.WATCHERS_LIST;
	}

	@Override
	protected Class<? extends MethodResult> getResultClass() {
		return WatchListMethodResult.class;
	}

	@Override
	protected String getRequestMethodName() {
		return COConstants.SERVICE_GET_WATCHERS;
	}

	@Override
	protected ProfileMethodRequest getProfilesRequest() {
		return new WatchListMethodRequest();
	}

	@Override
	protected List<? extends Profile> getResultProfiles(MethodResult resultObj) {
		WatchListMethodResult result = (WatchListMethodResult) resultObj;
		return result.getWatchList();
	}

}
