package com.copyop.android.app.fragment.profile;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.copyop.android.app.R;
import com.copyop.json.results.ProfileMethodResult;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class UserSummaryPagerFragment extends BaseFragment implements OnClickListener {

	private static final int NUMBER_OF_PAGES = 2;

	private ViewPager pager;
	private FragmentStatePagerAdapter pagerAdapter;
	private LinearLayout dotsLayout;
	private View editButton;

	private UserSummaryPage1Fragment page1;
	private UserSummaryPage2Fragment page2;

	/**
	 * Creates new instance of this fragment.
	 */
	public static UserSummaryPagerFragment newInstance(Bundle bundle) {
		UserSummaryPagerFragment fragment = new UserSummaryPagerFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.user_summary_pager_fragment, container, false);

		pager = (ViewPager) rootView.findViewById(R.id.user_profile_pager);
		dotsLayout = (LinearLayout) rootView.findViewById(R.id.user_profile_summary_dots_layout);

		editButton = rootView.findViewById(R.id.user_profile_summary_edit);
		editButton.setOnClickListener(this);

		setUpPager();
		changeDots(0);

		return rootView;
	}

	private void setUpPager() {
		pagerAdapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {

			@Override
			public int getCount() {
				return NUMBER_OF_PAGES;
			}

			@Override
			public Fragment getItem(int index) {
				if (index == 0) {
					if (page1 == null) {
						page1 = UserSummaryPage1Fragment.newInstance(new Bundle());
					}

					return page1;
				}

				if (page2 == null) {
					page2 = UserSummaryPage2Fragment.newInstance(new Bundle());
				}

				return page2;
			}
		};

		pager.setAdapter(pagerAdapter);

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				changeDots(position);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				// Do nothing
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// Do nothing
			}
		});
	}

	/**
	 * Change the dot that corresponds to the current page.
	 */
	private void changeDots(int page) {
		for (int i = 0; i < dotsLayout.getChildCount(); i++) {
			ImageView dotImage = (ImageView) dotsLayout.getChildAt(i);
			if (i == page) {
				dotImage.setImageResource(R.drawable.dot_filled_black);
			} else {
				dotImage.setImageResource(R.drawable.dot_empty_black);
			}
		}

		dotsLayout.invalidate();
	}

	public void setProfileInfo(ProfileMethodResult profileInfo, boolean isMyProfile) {
		page1.setProfileInfo(profileInfo, isMyProfile);
		page2.setProfileInfo(profileInfo, isMyProfile);

		if (isMyProfile) {
			editButton.setVisibility(View.VISIBLE);
		} else {
			editButton.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		application
				.postEvent(new NavigationEvent(Screen.PERSONAL_DETAILS_MENU, NavigationType.DEEP));
	}

}
