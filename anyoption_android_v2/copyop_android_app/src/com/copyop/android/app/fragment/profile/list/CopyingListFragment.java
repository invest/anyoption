package com.copyop.android.app.fragment.profile.list;

import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.CopyListMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CopyingListFragment extends ProfilesListFragment {

	private static final String TAG = CopyingListFragment.class.getSimpleName();

	/**
	 * Creates new instance of this fragment.
	 */
	public static CopyingListFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Creating CopyingListFragment fragment");
		CopyingListFragment fragment = new CopyingListFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		if(destUserId == application.getUser().getId()){
			listAdapter.setProfitLossShown(true);
		}
		listAdapter.notifyDataSetChanged();
		return view;
	}
	
	@Override
	protected Screenable setupScreen() {
		return COScreen.COPYING_LIST;
	}

	@Override
	protected Class<? extends MethodResult> getResultClass() {
		return CopyListMethodResult.class;
	}

	@Override
	protected String getRequestMethodName() {
		return COConstants.SERVICE_GET_COPYING;
	}

	@Override
	protected ProfileMethodRequest getProfilesRequest() {
		return new ProfileMethodRequest();
	}

	@Override
	protected List<? extends Profile> getResultProfiles(MethodResult resultObj) {
		CopyListMethodResult result = (CopyListMethodResult) resultObj;
		return result.getCopyList();
	}

}
