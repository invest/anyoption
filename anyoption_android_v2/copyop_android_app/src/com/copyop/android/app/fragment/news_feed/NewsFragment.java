package com.copyop.android.app.fragment.news_feed;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.lightstreamer.LightstreamerListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.model.Update.Type;
import com.copyop.android.app.popup.FollowDialogFragment;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.android.app.widget.adapter.NewsAdapter;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.json.results.FeedMethodResult;
import com.lightstreamer.ls_client.UpdateInfo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Displays {@link FeedMessage} updates in a List.
 * @author Anastas Arnaudov
 *
 */
public class NewsFragment extends BaseFragment implements LightstreamerListener,
		OnItemClickListener, Callback {

	@SuppressWarnings("hiding")
	public final String TAG = NewsFragment.class.getSimpleName();
	
	public static final int MAX_UPDATES_COUNT = 50;

	private static final int MESSAGE_UPDATE_LIST = 1;

	public static NewsFragment newInstance(Bundle bundle){
		NewsFragment fragment = new NewsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	protected List<Update> updatesList;
	protected View rootView;
	protected ListView listView;
	protected NewsAdapter adapter;
	protected View loadingView;
	protected boolean isLoading;

	private AsyncTask<List<FeedMessage>, Void, List<Update>> parseNewsTask;
	
	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if(bundle != null){
			//Get arguments:
			Log.d(TAG, "onCreate -> " + bundle.toString());
		}
		updatesList = new ArrayList<Update>();
		handler = new Handler(Looper.getMainLooper(), this);
		refresh();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(isPaused){
			refresh();
			isPaused = false;
		}
		Bundle bundle = getArguments();
		boolean isOpenFollowDialog = bundle.getBoolean(COConstants.EXTRA_OPEN_FOLLOW_DIALOG, false);
		if(isOpenFollowDialog){
			long marketId 		= Long.valueOf(bundle.getString(FeedMessage.PROPERTY_KEY_MARKET_ID));
			long investmentId 	= Long.valueOf(bundle.getString(FeedMessage.PROPERTY_KEY_INV_ID));
			int direction 		= Integer.valueOf(bundle.getString(FeedMessage.PROPERTY_KEY_DIRECTION));
			
			if(marketId > 0 && investmentId > 0 && direction > -1){
				Bundle args = new Bundle();
				args.putLong(COConstants.EXTRA_MARKET_ID, marketId);
				args.putLong(COConstants.EXTRA_INVESTMENT_ID, investmentId);
				args.putInt(COConstants.EXTRA_INVESTMENT_TYPE, direction);
				application.getFragmentManager().showDialogFragment(FollowDialogFragment.class, args);
			}
			bundle.remove(COConstants.EXTRA_OPEN_FOLLOW_DIALOG);
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (hidden) {
			cancelAllTasks();
			clean();
		} else {
			refresh();
		}
		super.onHiddenChanged(hidden);
	}
	
	@Override
	public void onDestroy() {
		clean();
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.news_layout, container, false);
		loadingView = rootView.findViewById(R.id.loadingView);
		listView = (ListView) rootView.findViewById(R.id.news_list_view);
		adapter = new NewsAdapter(updatesList);
		listView.setAdapter(adapter);
		listView.setItemsCanFocus(false);
		listView.setOnItemClickListener(this);
		
		showHideLoading();
		
		return rootView;
	}
	
	protected void showHideLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	protected void addUpdate(Update update){
		if(updatesList.size() >= MAX_UPDATES_COUNT){
			updatesList.remove(updatesList.size() - 1);
		}
		updatesList.add(0, update);
	}
		
	private void setupEmptyList(){
		isLoading = false;
		showHideLoading();
	}
	
	private void refresh(){
		clean();
		getNews();
	}
	
	private void clean() {
		detachLightstreamer();
		updatesList.clear();
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	private void cancelAllTasks() {
		application.getCommunicationManager().cancelAllRequests(this);
		if (parseNewsTask != null && !parseNewsTask.isCancelled()) {
			parseNewsTask.cancel(true);
		}
	}

	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	
	protected void getNews(){
		isLoading = true;
		showHideLoading();
		if(!application.getCommunicationManager().requestService(this, "gotNews", getNewsRequestName(),  new UserMethodRequest(), FeedMethodResult.class, false, false)){
			setupEmptyList();
		}
	}
	
	protected String getNewsRequestName(){
		return COConstants.SERVICE_GET_COPYOP_NEWS;
	}
	
	@SuppressWarnings("unchecked")
	public void gotNews(Object result){
		if (isHidden()) {
			return;
		}

		if(result != null && result instanceof FeedMethodResult && ((FeedMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			FeedMethodResult feedMethodResult = (FeedMethodResult) result;			
			parseNewsTask = new AsyncTask<List<FeedMessage>, Void, List<Update>>() {
				@Override
				protected List<Update> doInBackground(List<FeedMessage>... params) {
					List<FeedMessage> feedMessagesList = params[0];
					List<Update> result = new ArrayList<Update>();
					if (feedMessagesList != null) {
						for (FeedMessage feedMessage : feedMessagesList) {
							Update update = ConvertUtils.convertFeedMessageToUpdate(feedMessage);
							result.add(update);
						}
					}
					return result;
				}
				@Override
				protected void onCancelled() {
					Log.e(TAG, getNewsRequestName() + " callback -> Task cancelled.");
					setupEmptyList();
					super.onCancelled();
				}
				@Override
				protected void onPostExecute(List<Update> result) {
					updatesList.addAll(result);
					if (adapter != null) {
						adapter.notifyDataSetChanged();
					}
					attachLightstreamer();
					isLoading = false;
					showHideLoading();
				}
			}.execute(feedMethodResult.getFeed());
		}else{
			Log.e(TAG, getNewsRequestName() + " callback -> Could NOT parse result.");
			setupEmptyList();
		}
	}
	
	//#####################################################################
	
	//#####################################################################
	//#########					LIGHSTREAMER					###########
	//#####################################################################
	
	protected void attachLightstreamer(){
		COApplication.get().getLightstreamerManager().subscribeNewsTable(NewsFragment.this);
	}
	
	@SuppressWarnings("static-method")
	protected void detachLightstreamer(){
		COApplication.get().getLightstreamerManager().unSubscribeNewsTable();
	}
	
	@Override
	public void updateItem(int itemPos, String itemName, UpdateInfo updateInfo) {
		Update update = ConvertUtils.convertLSUpdateToUpdate(updateInfo, getQueueType());
		addUpdate(update);
		if (!handler.hasMessages(MESSAGE_UPDATE_LIST)) {
			Message msg = handler.obtainMessage(MESSAGE_UPDATE_LIST);
			msg.sendToTarget();
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_UPDATE_LIST) {
			adapter.notifyDataSetChanged();
		}

		return true;
	}

	protected QueueTypeEnum getQueueType(){
		return QueueTypeEnum.NEWS;
	}
	
	//#####################################################################
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Update update = updatesList.get(position);
		if(update.getType() == Type.PROMOTION){
			if(update.getBonusTypeId() == 0){
				//Go to Share:
				if(!update.isShared()){
					update.setShared(true);
					goToShare();
				}
			}else{
				//Go to Bonuses:
				goToBonuses();
			}
		}
	}
	
	private void goToShare(){
		COApplication.get().getShareRateManager().homeScreenShare();
	}

	private void goToBonuses() {
		application.postEvent(new NavigationEvent(Screen.BONUSES, NavigationType.DEEP));
	}

}