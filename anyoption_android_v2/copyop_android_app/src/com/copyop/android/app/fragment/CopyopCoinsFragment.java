package com.copyop.android.app.fragment;

import java.util.Map;
import java.util.TreeMap;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CORequestButton;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.ProfileCoinsMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CopyopCoinsFragment extends NavigationalFragment implements OnClickListener {

	private static final String TAG = CopyopCoinsFragment.class.getSimpleName();

	private static final long DAY_HOURS = 24;

	private CommunicationManager communicationManager;
	private Currency currency;
	private ProfileCoinsMethodResult coinsInfo;

	private LayoutInflater layoutInflater;
	private View contentView;
	private View loadingView;

	private TextView coinsView;
	private CORequestButton convertButton;
	private TextView covertDaysView;

	private View whatIsButton;
	private ImageView whatIsButtonArrow;
	private View whatIsContent;
	private TextView whatIsContentPar1;

	private View ratesButton;
	private ImageView ratesButtonArrow;
	private LinearLayout ratesContent;

	private boolean isConvertEnabled;

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static CopyopCoinsFragment newInstance(Bundle bundle) {
		CopyopCoinsFragment fragment = new CopyopCoinsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		communicationManager = application.getCommunicationManager();
		currency = application.getCurrency();

		coinsInfo = null;
	}

	@Override
	public void onResume() {
		requestCoinsInfo();

		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.copyop_coins_layout, container, false);
		this.layoutInflater = inflater;

		contentView = rootView.findViewById(R.id.copyop_coins_content);
		loadingView = rootView.findViewById(R.id.copyop_coins_loading);

		convertButton = (CORequestButton) rootView.findViewById(R.id.copyop_coins_convert_button);
		convertButton.setOnClickListener(this);
		convertButton.setEnabled(false);
		isConvertEnabled = false;

		whatIsButton = rootView.findViewById(R.id.copyop_coins_what_is_co_coins_button);
		whatIsButton.setOnClickListener(this);
		whatIsButtonArrow = (ImageView) rootView.findViewById(R.id.copyop_coins_what_is_arrow);
		whatIsContent = rootView.findViewById(R.id.copyop_coins_what_is_co_coins_content_layout);
		whatIsContentPar1 = (TextView) rootView.findViewById(R.id.whatIsCoinsContentPar1);

		ratesButton = rootView.findViewById(R.id.copyop_coins_conv_rates_button);
		ratesButton.setOnClickListener(this);
		ratesButtonArrow = (ImageView) rootView.findViewById(R.id.copyop_coins_conv_rates_arrow);
		ratesContent = (LinearLayout) rootView
				.findViewById(R.id.copyop_coins_conv_rates_content_layout);

		coinsView = (TextView) rootView.findViewById(R.id.copyop_coins_coins);
		covertDaysView = (TextView) rootView.findViewById(R.id.copyop_coins_covert_days);

		setUpInfo();

		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.COPYOP_COINS;
	}

	private void setUpInfo() {
		if (coinsInfo == null) {
			return;
		}

		long coins = coinsInfo.getBalance();
		coinsView.setText(String.valueOf(coins));

		int convertDays = getConvertDays(coinsInfo.getConvertRemainingTime());
		covertDaysView.setText(getString(R.string.convertDays, convertDays));
		
		//If no Coins => hide the timer:
		if(coins > 0){
			covertDaysView.setVisibility(View.VISIBLE);
		}else{
			covertDaysView.setVisibility(View.INVISIBLE);
		}

		setUpRateItems();

		long minCoinsToConvert = coinsInfo.getRates().firstEntry().getValue();
		whatIsContentPar1.setText(getString(R.string.whatIsCoinsContentPar1, minCoinsToConvert));

		if (convertDays > 0 && coins >= minCoinsToConvert) {
			enableConvertButton(true);
		} else {
			enableConvertButton(false);
		}

		contentView.setVisibility(View.VISIBLE);
		loadingView.setVisibility(View.GONE);
	}

	private void setUpRateItems() {
		TreeMap<Long, Long> rates = coinsInfo.getRates();
		if (ratesContent.getChildCount() > 0) {
			return;
		}

		ratesContent.removeAllViews();

		for (Map.Entry<Long, Long> entry : rates.entrySet()) {
			View rateItemView = layoutInflater.inflate(R.layout.copyop_coins_conv_rate,
					ratesContent, false);
			TextView rateCoinsView = (TextView) rateItemView
					.findViewById(R.id.copyop_coins_rate_coins_view);
			rateCoinsView.setText(String.valueOf(entry.getValue()));
			TextView rateAmountView = (TextView) rateItemView
					.findViewById(R.id.copyop_coins_rate_amount_view);
			String formattedAmount = AmountUtil.getFormattedAmount(entry.getKey(),
					currency.getSymbol(), currency.getIsLeftSymbolBool(), 0, currency.getId());
			rateAmountView.setText(formattedAmount);
			ratesContent.addView(rateItemView);
		}

		// Add footer view
		layoutInflater.inflate(R.layout.copyop_coins_conv_rates_footer, ratesContent, true);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == convertButton.getId()) {
			if (isConvertEnabled) {
				convertCoins();
			} else {
				// Disables the button if it is not disabled here.
				// Fixes problem because of loading animation.
				enableConvertButton(false);
			}
		} else if (v.getId() == whatIsButton.getId()) {
			toggleWhatIs();
		} else if (v.getId() == ratesButton.getId()) {
			toggleRates();
		}
	}

	private void requestCoinsInfo() {
		loadingView.setVisibility(View.VISIBLE);
		contentView.setVisibility(View.GONE);

		if (convertButton != null) {
			convertButton.setEnabled(false);
		}

		ProfileMethodRequest request = new ProfileMethodRequest();
		request.setRequestedUserId(application.getUser().getId());
		communicationManager.requestService(this, "coinsInfoCallBack",
				COConstants.SERVICE_GET_PROFILE_COINS, request, ProfileCoinsMethodResult.class);
	}

	public void coinsInfoCallBack(Object resultObj) {
		ProfileCoinsMethodResult result = (ProfileCoinsMethodResult) resultObj;

		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			coinsInfo = result;

			setUpInfo();
		} else {
			if (result != null) {
				Log.e(TAG, "Could not get coins info. ErrorCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Could not get coins info. Result = null");
			}

			contentView.setVisibility(View.GONE);
			loadingView.setVisibility(View.GONE);
		}
	}

	private void convertCoins() {
		convertButton.startLoading();
		isConvertEnabled = false;

		UserMethodRequest request = new UserMethodRequest();
		communicationManager.requestService(this, "convertCallBack",
				COConstants.SERVICE_DEPOSIT_COINS, request, ProfileCoinsMethodResult.class);
	}

	public void convertCallBack(Object resultObj) {
		convertButton.stopLoading();

		ProfileCoinsMethodResult result = (ProfileCoinsMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			// Updates coins
			coinsInfo = result;
			setUpInfo();

			// Refresh the user to update his balance.
			refreshUser();
		} else {
			if (result != null) {
				Log.e(TAG, "Could not convert coins. ErrorCode: " + result.getErrorCode());
			} else {
				Log.e(TAG, "Could not convert coins. Result = null");
			}
		}
	}

	private void toggleWhatIs() {
		if (whatIsContent.getVisibility() == View.VISIBLE) {
			whatIsContent.setVisibility(View.GONE);
			whatIsButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_right);
		} else {
			ratesButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_right);
			ratesContent.setVisibility(View.GONE);
			whatIsButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_up);
			whatIsContent.setVisibility(View.VISIBLE);
		}
	}

	private void toggleRates() {
		if (ratesContent.getVisibility() == View.VISIBLE) {
			ratesContent.setVisibility(View.GONE);
			ratesButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_right);
		} else {
			whatIsButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_right);
			whatIsContent.setVisibility(View.GONE);
			ratesButtonArrow.setImageResource(R.drawable.copyop_coins_arrow_up);
			ratesContent.setVisibility(View.VISIBLE);
		}
	}

	private static int getConvertDays(long hours) {
		if (hours <= 0) {
			return 0;
		}

		int convertDays = 0;
		convertDays = (int) (hours / DAY_HOURS);
		if (convertDays == 0) {
			// TODO One day if hours are < 24
			convertDays = 1;
		}

		return convertDays;
	}

	private void enableConvertButton(boolean enabled) {
		isConvertEnabled = enabled;
		convertButton.enableButton(enabled);
	}

}
