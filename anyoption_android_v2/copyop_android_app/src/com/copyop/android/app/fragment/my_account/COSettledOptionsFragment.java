package com.copyop.android.app.fragment.my_account;

import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.copyop.android.app.widget.adapter.COTradingOptionsListAdapter;

import android.os.Bundle;
import android.util.Log;

public class COSettledOptionsFragment extends SettledOptionsFragment {

	public static COSettledOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment COSettledOptionsFragment");
		COSettledOptionsFragment fragment = new COSettledOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new COTradingOptionsListAdapter(investments, false);
	}

}
