package com.copyop.android.app.fragment.settings;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.settings.SettingsFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class COSettingsFragment extends SettingsFragment {

	private View resetGuidlinesButton;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static COSettingsFragment newInstance(Bundle bundle){
		COSettingsFragment coSettingsFragment = new COSettingsFragment();
		coSettingsFragment.setArguments(bundle);
		return coSettingsFragment;
	}
	
	@Override
	protected void setupUI() {
		super.setupUI();
		setupResetGuidilinesButton();
	}
	
	@Override
	protected void setupAppVersionText() {
		//Do nothing
	}
	
	@Override
	protected Screenable getLoginScreen(){
		return COScreen.LSS_LOGIN_SIGNUP;
	}
	
	@Override
	protected void setupResetIntrosButton(){
		resetIntrosButton = rootView.findViewById(R.id.settings_reset_intros_button);
		resetIntrosButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_INTROS_ON, false);
				resetIntros();
			}
		});
	}
	
	private void setupResetGuidilinesButton(){
		resetGuidlinesButton = rootView.findViewById(R.id.settings_reset_guidelines_button);
		resetGuidlinesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				application.getSharedPreferencesManager().putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, false);
				application.getSharedPreferencesManager().putBoolean(COConstants.PREFERENCES_IS_NEWS_WALKTHROUGH_SHOWN, false);
				application.getSharedPreferencesManager().putBoolean(COConstants.PREFERENCES_IS_HOT_WALKTHROUGH_SHOWN, false);
				application.getSharedPreferencesManager().putBoolean(COConstants.PREFERENCES_IS_PROFILE_WALKTHROUGH_SHOWN, false);
				application.getSharedPreferencesManager().putBoolean(COConstants.PREFERENCES_IS_COPY_WALKTHROUGH_SHOWN, false);
			}
		});
	}
	
	@Override
	protected void setupLanguageButton(){
		//Do nothing
	}
	
}
