package com.copyop.android.app.fragment.login_register;

import com.anyoption.android.app.fragment.login_register.TermsAndConditionsFragment;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;

public class COTermsAndConditionsFragment extends TermsAndConditionsFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static COTermsAndConditionsFragment newInstance(Bundle bundle) {
		COTermsAndConditionsFragment termsAndConditionsFragment = new COTermsAndConditionsFragment();
		termsAndConditionsFragment.setArguments(bundle);
		return termsAndConditionsFragment;
	}

	@Override
	protected String getTermsUrl(long skinId, long currCountryId) {
		return COConstants.TERMS_LINK + "?fromApp=true&s=" + skinId + "&cid=" + currCountryId
				+ "&version=2-mobile";
	}

}
