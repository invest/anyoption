package com.copyop.android.app.fragment.my_account.banking.deposit;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.my_account.banking.deposit.DepositMenuFragment;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class CODepositMenuFragment extends DepositMenuFragment {

	private TextView skipButton;

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static CODepositMenuFragment newInstance(Bundle bundle) {
		CODepositMenuFragment depositMenuFragment = new CODepositMenuFragment();
		depositMenuFragment.setArguments(bundle);
		return depositMenuFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = super.onCreateView(inflater, container, savedInstanceState);

		boolean showSkip = false;

		Bundle args = getArguments();
		if (args != null && args.containsKey(Constants.EXTRA_MENU_BUTTON)) {
			boolean showMenu = args.getBoolean(Constants.EXTRA_MENU_BUTTON, true);
			showSkip = !showMenu;
		}

		if (showSkip) {
			skipButton = (TextView) rootView.findViewById(R.id.depositSkip);
			TextUtils.underlineText(skipButton);
			skipButton.setVisibility(View.VISIBLE);
			skipButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					application.postEvent(new NavigationEvent(application.getHomeScreen(),
							NavigationType.INIT));
					skipButton.setClickable(false);
				}
			});
		}

		return rootView;
	}

	@Override
	protected void setUpOtherFundingMethods() {
		super.setUpOtherFundingMethods();
		rootView.findViewById(R.id.depositOtherFunding).setVisibility(View.VISIBLE);
	}

}
