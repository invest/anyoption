package com.copyop.android.app.fragment.login_register;

import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;
import com.copyop.json.results.UserMethodResult;
import com.facebook.model.GraphUser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LSSSignupFinishFragment extends CORegisterFragment {
	public static final String TAG = LSSSignupFinishFragment.class.getSimpleName();
	
	public static final String FB_ID = "fbId";
	public static final String FB_FNAME = "fbFirstName";
	public static final String FB_LNAME = "fbLastName";
	public static final String FB_EMAIL = "fbEmail";
	
	private String firstName;
	private String lastName;
	private String email;
	protected TextView nameTextView;
	private UserMethodResult result;

	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSSignupFinishFragment newInstance() {	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSSignupFinishFragment newInstance(Bundle bundle) {
		LSSSignupFinishFragment registerFragment = new LSSSignupFinishFragment();	
		registerFragment.setArguments(bundle);
		return registerFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (null != bundle) {
			fbId = bundle.getString(FB_ID);
			firstName = bundle.getString(FB_FNAME);
			lastName = bundle.getString(FB_LNAME);
			email = bundle.getString(FB_EMAIL);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = inflater.inflate(R.layout.lss_signup_finish_fragment, container, false); 

		init();
		avatarURL = COFacebookManager.getAvatarURL(fbId);
		 
		setupForm();
 
		setupTermsAndConditions();

		setupRegisterButton();
		
		firstNameField.setValue(firstName);
		lastNameField.setValue(lastName);
		nameTextView = (TextView) rootView.findViewById(R.id.lss_signup_finish_name_textview);
		nameTextView.setText(firstName + " " + lastName);
		
		if (null != email) {
			emailField.setValue(email);
		}
		
		setupSupportButton();
		
		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.LSS_SIGNUP_FINISH;
	}

	public void insertProfileCallBack(Object resultObj) {
		result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			COApplication.get().getFacebookManager().getFriends(this.getActivity(), this, new COFacebookManager.FriendsListener() {
				@Override
				public void onFriends(List<GraphUser> friends) {
					if (null != friends) {
						updateFacebookFriends(result.getCopyopProfile().getFbId(), friends);
					} else {
						finishInsertProfileCallBack();
					}
				}
			});
		} else {
			finishInsertProfileCallBack();
		}
	}

	private void finishInsertProfileCallBack() {
		super.insertProfileCallBack(result);
	}

	private void updateFacebookFriends(String fbId, List<GraphUser> friends) {
		COApplication.get().getFacebookManager().updateFriends(fbId, friends, this, "updateFacebookFriendsCallback");
	}

	public void updateFacebookFriendsCallback(Object resultObj) {
		finishInsertProfileCallBack();
	}
}