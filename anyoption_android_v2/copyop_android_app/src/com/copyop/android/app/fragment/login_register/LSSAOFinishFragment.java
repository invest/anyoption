package com.copyop.android.app.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.manager.LoginManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.User;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.AcceptedTermsAndConditions;
import com.copyop.json.results.UserMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LSSAOFinishFragment extends CORegisterFragment {
	
	private User user;
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSAOFinishFragment newInstance() {	
		return newInstance(new Bundle());
	}
	
	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static LSSAOFinishFragment newInstance(Bundle bundle) {
		LSSAOFinishFragment aoFinishFragment = new LSSAOFinishFragment();	
		aoFinishFragment.setArguments(bundle);
		if (null != bundle) {
			aoFinishFragment.setUser((User) bundle.getSerializable(Constants.EXTRA_USER));
		}
		return aoFinishFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = inflater.inflate(R.layout.lss_ao_finish_fragment, container, false); 
		isTermsAndConditionsButtonEnabled = true;
		initAvatar();

		form = new Form(new Form.OnSubmitListener() { 
			@Override 
			public void onSubmit() {
				insertRegister();
			} 
		}); 
		
		setupCopyopForm();
 
		setupTermsAndConditions();
		
		setupRegisterButton();

		setupSupportButton();
		
		if (null != user) {
			TextView nameTextView = (TextView) rootView.findViewById(R.id.lss_ao_finish_name_textview);
			nameTextView.setText(user.getFirstName() + " " + user.getLastName());
	
			TextView fromTextView = (TextView) rootView.findViewById(R.id.lss_signup_finish_from_text);
			Country country = application.getCountriesMap().get(user.getCountryId());
			String countryName = null;
			if (country != null) {
				countryName = country.getName();
			} else {
				countryName = "";
			}
			String city = "";
			if(user.getCityName() != null){
				city = user.getCityName() + ", ";
			}
			fromTextView.setText(application.getString(R.string.lssAOFinishFrom, city + countryName));
		}
		
		return rootView;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.LSS_AO_FINISH;
	}
	
	@Override
	public void insertRegister() {
		AcceptedTermsAndConditions request = new AcceptedTermsAndConditions();
		if (null != user) {
			request.setUserName(user.getUserName());
			request.setPassword(user.getPassword());
		}
		request.setAvatar(avatarURL);
		request.setNickname(nicknameField.getValue());
		request.setAcceptedTermsAndConditions(termsAndConditionsCheckBox.isChecked());
		
		if (!Application.get().getCommunicationManager().requestService(LSSAOFinishFragment.this, "acceptedTermsAndConditionsCallBack", COConstants.SERVICE_ACCEPT_TERMS_AND_CONDITIONS, request, UserMethodResult.class, true, false)) {
			registerButton.stopLoading();
		}
	}
	
	public void acceptedTermsAndConditionsCallBack(Object resultObj) {
		final UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			COApplication.get().setProfile(result.getCopyopProfile());
			if (result.getCopyAmounts() != null) {
				COApplication.get().setCopyAmounts(result.getCopyAmounts());
			}
			COApplication.get().setMaxCopiersPerAccount(result.getMaxCopiersPerAccount());

			final LoginManager loginManager = new LoginManager();
			if (bitmap != null) {
				COApplication.get().getCommunicationManager().uploadAvatarImage(bitmap, new Object(){
					
					@SuppressWarnings("unused")
					public void onAvatarUploaded(UserMethodResult userMethodResult){
						Log.d(TAG, "onAvatarUploaded");
						COApplication.get().getCommunicationManager().onAvatarUploaded(userMethodResult);
						loginManager.processLoginAttemptResult(result, registerButton, LSSAOFinishFragment.this, getResources().getBoolean(R.bool.isTablet), form);
					}
					
				}, "onAvatarUploaded");
			} else {
				loginManager.processLoginAttemptResult(result, registerButton, LSSAOFinishFragment.this, getResources().getBoolean(R.bool.isTablet), form);
			}
		} else {
			registerButton.stopLoading();	
		}
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}