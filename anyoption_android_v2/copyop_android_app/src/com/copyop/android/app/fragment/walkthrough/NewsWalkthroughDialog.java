package com.copyop.android.app.fragment.walkthrough;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class NewsWalkthroughDialog extends BaseDialogFragment implements OnClickListener {

	private static final String TAG = NewsWalkthroughDialog.class.getSimpleName();

	private View gotItPage1;
	private View gotItPage2;
	private View gotItPage3;
	private View gotItPage4;

	private View page1;
	private View page2;
	private View page3;
	private View page4;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static NewsWalkthroughDialog newInstance(Bundle bundle) {
		Log.d(TAG, "Created new NewsWalkthroughDialog.");
		NewsWalkthroughDialog fragment = new NewsWalkthroughDialog();
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, 0);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		page1 = contentView.findViewById(R.id.news_walkthrough_page1);
		page2 = contentView.findViewById(R.id.news_walkthrough_page2);
		page3 = contentView.findViewById(R.id.news_walkthrough_page3);
		page4 = contentView.findViewById(R.id.news_walkthrough_page4);

		gotItPage1 = contentView.findViewById(R.id.news_walkthrough_page1_got_it);
		gotItPage1.setOnClickListener(this);
		gotItPage2 = contentView.findViewById(R.id.news_walkthrough_page2_got_it);
		gotItPage2.setOnClickListener(this);
		gotItPage3 = contentView.findViewById(R.id.news_walkthrough_page3_got_it);
		gotItPage3.setOnClickListener(this);
		gotItPage4 = contentView.findViewById(R.id.news_walkthrough_page4_got_it);
		gotItPage4.setOnClickListener(this);

		setUpTitles();
		setUpPage3();
		setUpPage4();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.news_walkthrough_dialog;
	}

	private void setUpTitles() {
		TextView titlePage2 = (TextView) page2.findViewById(R.id.news_walkthrough_page2_title);
		TextUtils.underlineText(titlePage2);

		TextView titlePage3 = (TextView) page3.findViewById(R.id.news_walkthrough_page3_title);
		TextUtils.underlineText(titlePage3);

		TextView titlePage4 = (TextView) page4.findViewById(R.id.news_walkthrough_page4_title);
		TextUtils.underlineText(titlePage4);
	}

	private void setUpPage3() {
		View dividerTop = page3.findViewById(R.id.news_item_divider_top);
		dividerTop.setVisibility(View.GONE);
		View dividerBottom = page3.findViewById(R.id.news_item_divider_bottom);
		dividerBottom.setVisibility(View.VISIBLE);
		TextView titleTextView = (TextView) page3.findViewById(R.id.news_item_title_text_view);
		String nickname = "John Smith";
		titleTextView.setText(nickname);

		TextView copiersTextView = (TextView) page3.findViewById(R.id.news_item_copiers_text_view);
		copiersTextView.setText(getString(R.string.newsItemCopiers, 19));
		TextView watchersTextView = (TextView) page3
				.findViewById(R.id.news_item_watchers_text_view);
		watchersTextView.setText(getString(R.string.newsItemWatchers, 231));

		TextView bodyTextView = (TextView) page3.findViewById(R.id.news_item_body_text_view);
		String secondNickname = "Jimmy Brown";
		bodyTextView.setText(getString(R.string.newsItemStartedCopying, secondNickname));
		createProfileInnerLink(bodyTextView, new SpannableStringBuilder(), secondNickname);

		TextView timestampTextView = (TextView) page3
				.findViewById(R.id.news_item_timestamp_text_view);
		timestampTextView.setText(getString(R.string.hoursAgo, 2));

		View copyButton = page3.findViewById(R.id.news_item_copy_button);
		copyButton.setEnabled(false);
		View watchButton = page3.findViewById(R.id.news_item_watch_button);
		watchButton.setEnabled(false);
		View followButton = page3.findViewById(R.id.news_item_follow_button);
		followButton.setVisibility(View.GONE);
	}

	private void setUpPage4() {
		View dividerTop = page4.findViewById(R.id.news_item_divider_top);
		dividerTop.setVisibility(View.GONE);
		View dividerBottom = page4.findViewById(R.id.news_item_divider_bottom);
		dividerBottom.setVisibility(View.GONE);
		TextView titleTextView = (TextView) page4.findViewById(R.id.news_item_title_text_view);
		String nickname = "Rob Johnson";
		titleTextView.setText(nickname);

		TextView copiersTextView = (TextView) page4.findViewById(R.id.news_item_copiers_text_view);
		copiersTextView.setText(getString(R.string.newsItemCopiers, 34));
		TextView watchersTextView = (TextView) page4
				.findViewById(R.id.news_item_watchers_text_view);
		watchersTextView.setText(getString(R.string.newsItemWatchers, 173));

		TextView bodyTextView = (TextView) page4.findViewById(R.id.news_item_body_text_view);

		String marketName = "EUR/USD";
		String amountFormatted = "100$";
		String levelFormated = "(@1.244609)";
		String directionFormatted = getString(R.string.newsItemCall);

		bodyTextView.setText(getString(R.string.newsItemBoughtOnMarket, "", directionFormatted,
				marketName, amountFormatted, levelFormated).trim());
		decorateOrangeInnerText(bodyTextView, new SpannableStringBuilder(), directionFormatted,
				marketName, levelFormated);

		TextView timestampTextView = (TextView) page4
				.findViewById(R.id.news_item_timestamp_text_view);
		timestampTextView.setText(getString(R.string.minuteAgo));

		View copyButton = page4.findViewById(R.id.news_item_copy_button);
		copyButton.setVisibility(View.GONE);
		View watchButton = page4.findViewById(R.id.news_item_watch_button);
		watchButton.setVisibility(View.GONE);
		View followButton = page4.findViewById(R.id.news_item_follow_button);
		followButton.setVisibility(View.VISIBLE);
		followButton.setEnabled(false);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == gotItPage1.getId()) {
			showPage2();
		} else if (v.getId() == gotItPage2.getId()) {
			showPage3();
		} else if (v.getId() == gotItPage3.getId()) {
			showPage4();
		} else if (v.getId() == gotItPage4.getId()) {
			finishWalkthrough();
		}
	}

	private void showPage2() {
		page2.setVisibility(View.VISIBLE);
		page1.setVisibility(View.GONE);
	}

	private void showPage3() {
		page3.setVisibility(View.VISIBLE);
		page2.setVisibility(View.GONE);
	}

	private void showPage4() {
		page4.setVisibility(View.VISIBLE);
		page3.setVisibility(View.GONE);
	}

	private void finishWalkthrough() {
		application.getSharedPreferencesManager().putBoolean(
				COConstants.PREFERENCES_IS_NEWS_WALKTHROUGH_SHOWN, true);

		dismiss();
	}

	private static void createProfileInnerLink(final TextView textview, Spannable spannable,
			String name) {
		if (name == null || name.isEmpty()) {
			return;
		}

		TextUtils.makeClickableInnerText(textview, spannable, textview.getText().toString(), name,
				Font.ROBOTO_BOLD, 0, R.color.inbox_item_content_text, false, null);
	}

	private static void decorateOrangeInnerText(final TextView textview, Spannable spannable,
			String... innerTexts) {
		for (String innerText : innerTexts) {
			TextUtils.decorateInnerText(textview, spannable, textview.getText().toString(),
					innerText, Font.ROBOTO_MEDIUM, R.color.news_item_body_orange_link, 0);
		}
	}

}
