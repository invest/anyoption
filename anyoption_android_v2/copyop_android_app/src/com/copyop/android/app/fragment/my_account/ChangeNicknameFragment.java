package com.copyop.android.app.fragment.my_account;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.CORequestButton;
import com.copyop.json.requests.ChangeNicknameMethodRequest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class ChangeNicknameFragment extends NavigationalFragment{

	
	public static ChangeNicknameFragment newInstance(Bundle bundle){
		ChangeNicknameFragment fragment = new ChangeNicknameFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	private CORequestButton updateButton;
	private FormEditText nickNameFormEditText;
	private TextView currentNicknameTextView;
	private Form form;

	@Override
	protected Screenable setupScreen() {
		return COScreen.CHANGE_NICKNAME;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.change_nickname, container, false);
		currentNicknameTextView = (TextView) rootView.findViewById(R.id.change_nickname_current_nickname);
		nickNameFormEditText = (FormEditText) rootView.findViewById(R.id.change_nickname_nickname);
		nickNameFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.NICKNAME);
		nickNameFormEditText.setFieldName(COConstants.FIELD_NICKNAME);
		TextView warningTextView = (TextView) rootView.findViewById(R.id.change_nickname_warning_text);
		TextUtils.decorateInnerText(warningTextView, null, warningTextView.getText().toString(), getString(R.string.once), Font.ROBOTO_MEDIUM, 0, 0);
		updateButton = (CORequestButton) rootView.findViewById(R.id.change_nickname_update_button);
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(form.checkValidity()){
					updateButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});									
				}
			}
		});
		
		form = new Form(new Form.OnSubmitListener() { 
			@Override 
			public void onSubmit() {
				changeNickname();
			} 
		}); 
		form.addItem(nickNameFormEditText);
		
		currentNicknameTextView.setText(COApplication.get().getProfile().getNickname());

		return rootView;
	}

	private void changeNickname(){
		ChangeNicknameMethodRequest request = new ChangeNicknameMethodRequest();
		request.setNickname(nickNameFormEditText.getValue());
		if(!application.getCommunicationManager().requestService(this, "changeNicknameCallback", COConstants.SERVICE_CHANGE_COPYOP_NICKNAME, request, MethodResult.class)){
			updateButton.stopLoading();
		}
	}
	
	public void changeNicknameCallback(Object result){
		updateButton.stopLoading();
		if(result != null && result instanceof MethodResult && ((MethodResult)result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			COApplication.get().getProfile().setNickname(nickNameFormEditText.getValue());
			COApplication.get().getProfile().setNicknameChanged(true);
			application.getFragmentManager().onBack();
		}else{
			ErrorUtils.displayFieldError(form, (MethodResult) result);
		}
	}
}
