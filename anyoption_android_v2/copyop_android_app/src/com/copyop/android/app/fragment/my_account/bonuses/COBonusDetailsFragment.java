package com.copyop.android.app.fragment.my_account.bonuses;



import com.anyoption.android.app.fragment.my_account.bonuses.BonusDetailsFragment;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class COBonusDetailsFragment extends BonusDetailsFragment {

	private static final String GSON_DATE_FORMAT = "MMM d, yyyy hh:mm:ss aaa";
	
	public static COBonusDetailsFragment newInstance(Bundle bundle) {
		COBonusDetailsFragment cOBonusDetailsFragment = new COBonusDetailsFragment();
		cOBonusDetailsFragment.setArguments(bundle);
		return cOBonusDetailsFragment;
	}

	private TextView acceptButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);

		acceptButton = (TextView) rootView.findViewById(R.id.bonus_details_screen_accept_button);
		View waiveButton = rootView.findViewById(R.id.buttonWaiveBonus);
	    if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	    	acceptButton.setVisibility(View.VISIBLE);
	    	waiveButton.setVisibility(View.GONE);
            rootView.findViewById(R.id.buttonWaiveBonus).setOnClickListener(this);            
        }
		
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptButton.setEnabled(false);
				updateBonus();
			}
		});
		return rootView;
	}

	@Override
	protected void setupBonusState(BonusMethodRequest request) {
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
			request.setBonusStateId(Constants.BONUS_STATE_PANDING);
		} else {
			super.setupBonusState(request);
		}
	}

	@Override
	public void updateBonusCallBack(Object result) {
		acceptButton.setEnabled(true);
		MethodResult methodResult = (MethodResult) result;
		if (bonus.getBonusStateId() == Constants.BONUS_STATE_PANDING) {
	        if (methodResult != null && methodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
	        	acceptButton.setEnabled(false);
	            application.getCurrentActivity().onBackPressed();
	        }else{
	        	ErrorUtils.handleResultError(methodResult);
	        }
		}else{
			super.updateBonusCallBack(result);
		}
	}
	
    protected void getBonusDescription() {
    	Gson gson = new GsonBuilder().setDateFormat(GSON_DATE_FORMAT).create();
		String jsonBonus = gson.toJson(bonus);
		String jsonBonusFactor = gson.toJson(bonusFactor);
		String url = COConstants.BONUS_LINK + "?s=" + application.getSkinId() + "&data=" + jsonBonus + "&turnoverFactor=" + jsonBonusFactor;
    	webView.loadUrl(url);
    }
	
}
