package com.copyop.android.app.fragment.facebook;

import java.util.List;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.manager.COFacebookManager;
import com.facebook.Session;
import com.facebook.model.GraphUser;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class FacebookNotConnectedFragment extends NavigationalFragment{

	public static FacebookNotConnectedFragment newInstance(Bundle bundle){
		FacebookNotConnectedFragment fragment = new FacebookNotConnectedFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected Screenable setupScreen() {
		return COScreen.FACEBOOK_NOT_CONNECTED;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.facebook_not_connected, container, false);
		
		View connectButton = rootView.findViewById(R.id.facebook_not_connected_connect_button);
		connectButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				COFacebookManager.getUser(getActivity(), FacebookNotConnectedFragment.this, new COFacebookManager.UserListener() {
					@Override
					public void onUser(GraphUser user) {
						if (null != user) {
							COApplication.get().getProfile().setFbId(user.getId());
							getFacebookFriends(user);
						}
					}
				});
			}
		});
		
		return rootView;
	}
	
	private void getFacebookFriends(final GraphUser user) {
		COApplication.get().getFacebookManager().getFriends(getActivity(), this, new COFacebookManager.FriendsListener() {
			@Override
			public void onFriends(List<GraphUser> friends) {
				if (null != friends) {
					updateFacebookFriends(user.getId(), friends);
				}
			}
		});
	}
	
	private void updateFacebookFriends(String fbId, List<GraphUser> friends) {
		COApplication.get().getFacebookManager().updateFriends(fbId, friends, this, "updateFacebookFriendsCallback");
	}
	
	public void updateFacebookFriendsCallback(Object resultObj) {
		application.postEvent(new NavigationEvent(COScreen.FACEBOOK_CONNECTED, NavigationType.HORIZONTAL, new Bundle()));
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
	}
}