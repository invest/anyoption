package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.AssetPageFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class COAssetPageFragment extends AssetPageFragment {

	/**
	 * Creates new instance of this fragment.
	 */
	public static COAssetPageFragment newInstance(Bundle bundle) {
		COAssetPageFragment fragment = new COAssetPageFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = super.onCreateView(inflater, container, savedInstanceState);

		return root;
	}

}
