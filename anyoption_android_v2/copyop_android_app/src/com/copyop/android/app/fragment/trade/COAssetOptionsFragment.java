package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.AssetOptionsFragment;
import com.copyop.android.app.widget.adapter.COAssetOptionsListAdapter;

import android.os.Bundle;
import android.util.Log;

public class COAssetOptionsFragment extends AssetOptionsFragment {

	private static final String TAG = COAssetOptionsFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static AssetOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COAssetOptionsFragment.");
		COAssetOptionsFragment fragment = new COAssetOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new COAssetOptionsListAdapter(market);
	}

}
