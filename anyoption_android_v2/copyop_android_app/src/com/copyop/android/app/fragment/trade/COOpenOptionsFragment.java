package com.copyop.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.OpenOptionsFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.copyop.android.app.R;
import com.copyop.android.app.event.InvestmentCopiedEvent;
import com.copyop.android.app.widget.adapter.COTradingOptionsListAdapter;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class COOpenOptionsFragment extends OpenOptionsFragment {

	private final String TAG = COOpenOptionsFragment.class.getSimpleName();

	private static final int MESSAGE_REFRESH_INVESTMENTS = 2;

	private View takeProfitDisableView;
	private View rollForwardDisableView;

	public static COOpenOptionsFragment newInstance(Bundle bundle) {
		COOpenOptionsFragment fragment = new COOpenOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);

		takeProfitDisableView = rootView
				.findViewById(R.id.trading_open_options_take_profit_disable_view);
		rollForwardDisableView = rootView
				.findViewById(R.id.trading_open_options_roll_forward_disable_view);

		rollForwardButton.setClickable(true);
		takeProfitButton.setClickable(true);

		return rootView;
	}

	@Override
	protected void setUpListAdapter() {
		optionsListAdapter = new COTradingOptionsListAdapter(investments);
	}

	@Override
	protected void updateTakeProfit() {
		Log.d(TAG, "updateTakeProfit");
		if (!takeProfitGroups.isEmpty()) {
			takeProfitTimeView.setVisibility(View.VISIBLE);
			takeProfitDisableView.setVisibility(View.GONE);
		} else {
			takeProfitTimeView.setVisibility(View.GONE);
			takeProfitDisableView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void updateRollForward() {
		Log.d(TAG, "updateRollForward");
		if (!rollForwardGroups.isEmpty()) {
			rollForwardTimeView.setVisibility(View.VISIBLE);
			rollForwardDisableView.setVisibility(View.GONE);
		} else {
			rollForwardTimeView.setVisibility(View.GONE);
			rollForwardDisableView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void registerForEvents(){
		super.registerForEvents();
		application.registerForEvents(this, InvestmentCopiedEvent.class);
	}
	
	//#####################################################################
	//#########					EVENTS CALLBACKs				###########
	//#####################################################################
	
	@SuppressWarnings("unused")
	public void onEvent(InvestmentCopiedEvent event) {
		Message message = handler.obtainMessage(MESSAGE_REFRESH_INVESTMENTS);
		message.sendToTarget();
	}

	//#####################################################################

	@Override
	protected void updateAmountValue(TextView view, double amount) {
		view.setText(AmountUtil.getFormattedAmount(amount, currency));
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_REFRESH_INVESTMENTS) {
			requestInvestments();
			return true;
		}

		return super.handleMessage(msg);
	}

}
