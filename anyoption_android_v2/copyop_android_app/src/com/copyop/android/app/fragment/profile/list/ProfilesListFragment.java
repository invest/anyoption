package com.copyop.android.app.fragment.profile.list;

import java.util.List;

import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.adapter.ProfilesListAdapter;
import com.copyop.common.dto.base.Profile;
import com.copyop.json.requests.ProfileMethodRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public abstract class ProfilesListFragment extends NavigationalFragment {

	@SuppressWarnings("hiding")
	private static final String TAG = ProfilesListFragment.class.getSimpleName();

	private View rootView;
	private ListView list;
	protected ProfilesListAdapter listAdapter;

	protected long destUserId;
	private List<? extends Profile> profiles;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null && args.containsKey(COConstants.EXTRA_PROFILE_ID)) {
			destUserId = args.getLong(COConstants.EXTRA_PROFILE_ID);
		} else {
			destUserId = application.getUser().getId();
		}

		requestProfilesList();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.profiles_list_layout, container, false);
		list = (ListView) rootView.findViewById(R.id.profiles_list);
		listAdapter = new ProfilesListAdapter();
		list.setAdapter(listAdapter);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				listAdapter.showProfile(position);
			}
		});

		if (profiles != null) {
			listAdapter.setProfiles(profiles);
			listAdapter.notifyDataSetChanged();
		}

		return rootView;
	}

	protected abstract Class<? extends MethodResult> getResultClass();

	protected abstract String getRequestMethodName();

	protected abstract ProfileMethodRequest getProfilesRequest();

	protected abstract List<? extends Profile> getResultProfiles(MethodResult resultObj);

	private void requestProfilesList() {
		ProfileMethodRequest request = getProfilesRequest();
		request.setRequestedUserId(destUserId);

		application.getCommunicationManager().requestService(this, "profilesRequestCallback",
				getRequestMethodName(), request, getResultClass());
	}

	public void profilesRequestCallback(Object resultObj) {
		MethodResult result = (MethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			profiles = getResultProfiles(result);

			if (listAdapter != null) {
				listAdapter.setProfiles(profiles);
				listAdapter.notifyDataSetChanged();
			}
		} else {
			if (result != null) {
				Log.e(TAG, getRequestMethodName() + " error: " + result.getErrorCode());
			} else {
				Log.e(TAG, getRequestMethodName() + " error: result is null");
			}
		}
	}

}
