package com.copyop.android.app.util;

import java.util.HashMap;
import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.event.InvestmentCopiedEvent;
import com.copyop.android.app.model.HotCopier;
import com.copyop.android.app.model.HotTrade;
import com.copyop.android.app.model.HotTrader;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.model.Update.Type;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.lightstreamer.ls_client.UpdateInfo;

import android.content.Context;
import android.util.Log;

public class ConvertUtils {

	public static final String TAG = ConvertUtils.class.getSimpleName();

	/**
	 * Formats the update based on the type.
	 */
	@SuppressWarnings("incomplete-switch")
	public static void formatUpdate(Update update){
		Context context = Application.get();
		Type type = null;
		if(update.getQueueType() == QueueTypeEnum.NEWS || update.getQueueType() == QueueTypeEnum.EXPLORE || update.getQueueType() == QueueTypeEnum.SYSTEM){
			type = Type.NEWS;
		}else if(update.getQueueType() == QueueTypeEnum.INBOX){
			type = Type.INBOX;
		}
		String title = null;
		String body = null;
		int promotionIconresId = 0;
		boolean isMarketIcon = false;
		boolean isCopiersWatchers = false;
		boolean isCopyWatchButtons = false;
		boolean isFollowButton = false;
		boolean isCoinsButton = false;
		boolean isDepositButton = false;
		boolean isShowOfIcon = false;
		boolean hasNicknameInBody = false;
		
		UpdateTypeEnum updateType = update.getUpdateType();
		switch (updateType) {
			case AB_I_COPIED_INVESTMENT:
				body = context.getString(R.string.notificationCopiedInvestment);
				Application.get().postEvent(new InvestmentCopiedEvent());
				break;
			case AB_MY_EXPIRY:
				body = context.getResources().getString(R.string.notificationOptionExpire);
				if(update.getAmount() > 0){
					body += " " + context.getResources().getString(R.string.addedToYourBalance, update.getAmountFormatted());
				}
			break;
			case AB_BALANCE_LESS_MIN_COPIED:
				body = context.getString(R.string.inboxItemWarningInsufficientFundsCopied, update.getNickname(), update.getCopiers());
				hasNicknameInBody = true;
				break;
			case AB_BALANCE_LESS_MIN_COPYING:
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = context.getString(R.string.notificationBalanceMinCopying, update.getNickname());
					hasNicknameInBody = true;
				}else{
					title = context.getString(R.string.inboxItemWarningInsufficientFundsCopyingTitle);
					body = context.getString(R.string.inboxItemWarningInsufficientFundsCopyingBody);
				}
				break;
			case AB_BALANCE_LESS_MIN_WARN:
				body = context.getString(R.string.inboxItemWarningInsufficientFundsWarning);
				isDepositButton = true;
				break;
			case AB_IM_FULLY_COPIED:
				title = context.getString(R.string.newsItemYouMustBePopular);
				body = context.getString(R.string.newsItemYourCopiersListFull);
				break;
			case AB_IM_TOP_PROFITABLE:
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = context.getString(R.string.notificationTopProfitable, update.getTopProfitableTraders());
				}else{
					title = context.getString(R.string.newsItemYouAreRainmaker);
					body = context.getString(R.string.newsItemYouWereRankedTopTradersWithProfit, update.getTopProfitableTraders(), update.getProfitFormatted());
				}
				break;
			case AB_REACHED_COINS:
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = context.getString(R.string.inboxItemWarningCoinsTitle, update.getCoinsBalance());
				}else{
					title = context.getString(R.string.inboxItemWarningCoinsTitle, update.getCoinsBalance());
					body = context.getString(R.string.inboxItemWarningCoinsBody);
				}
				isCoinsButton = true;
				break;
			case CP_FROZE:
				title = context.getString(R.string.newsItemBoring);
				body = context.getString(R.string.newsItemNotTradingTryAnother, update.getNickname());
				hasNicknameInBody = true;
				isCopyWatchButtons = true;
				break;
			case CP_NEW_COPYING:
				title = update.getNickname();
				if(update.getSecondUserId() == Application.get().getUser().getId()){
					update.setSecondNickname(context.getString(R.string.me));
				}
				body = context.getString(R.string.newsItemStartedCopying, update.getSecondNickname());
				hasNicknameInBody = true;
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case CP_SHOW_OFF:
				title = context.getString(R.string.newsItemShowOff , update.getNickname());
				body = context.getString(R.string.newsItemIJustMade, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				isShowOfIcon = true;
				break;
			case FB_IN_MONEY_EXPIRY:
				title = context.getString(R.string.newsItemMustBeHappy , update.getNickname());
				body = context.getString(R.string.newsItemMadeOnMarket, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case FB_NEW_COPYING:
				title = update.getNickname();
				body = context.getString(R.string.newsItemStartedCopying, update.getSecondNickname());
				hasNicknameInBody = true;
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case FB_NEW_FRIEND:
				title = update.getFullName();
				body = context.getString(R.string.newsItemJoinedCopyop);
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case FB_NEW_INVESTMENT:
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = context.getString(R.string.newsItemBoughtOnMarket,update.getNickname(), update.getDirectionFormatted(), update.getMarketName(), update.getAmountFormatted(), update.getLevelFormated());
				}else{
					title = update.getNickname();
					body = context.getString(R.string.newsItemBoughtOnMarket,"", update.getDirectionFormatted(), update.getMarketName(), update.getAmountFormatted(), update.getLevelFormated()).trim();
					isCopiersWatchers = true;
					isFollowButton = true;
				}
				break;
			case FB_NEW_WATCHING:
				title = update.getNickname();
				body = context.getString(R.string.newsItemStartedWatching, update.getSecondNickname());
				hasNicknameInBody = true;
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case FB_SHOW_OFF:
				title = context.getString(R.string.newsItemShowOff , update.getNickname());
				body = context.getString(R.string.newsItemIJustMade, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				isShowOfIcon = true;
				break;
			case SP_TODAYS_OFFER:
				type = Type.PROMOTION;
				title = context.getString(R.string.newsItemPromotionTitle);
				body = update.getBonusDescription();
				promotionIconresId = CODrawableUtils.generateRandomPromotionResId();
				break;
			case SYS_1H_TRENDIEST:
				title = context.getString(R.string.newsItemStrongTrendAlert);
				body = context.getString(R.string.newsItemBoughtOptions, update.getTrendPercent(), update.getInvestmentTypeFormatted(), update.getMarketName(), update.getExpiryTimestampFormated());
				isMarketIcon = true;
				break;
			case SYS_24H_MOST_COPIED:
				title = context.getString(R.string.newsItemEverybodyLoves, update.getNickname());
				body = context.getString(R.string.newsItemNewCopiersInPastHours, update.getNewCopiers(), update.getLastHours());
				break;
			case SYS_8H_HIGHEST_SR:
				title = context.getString(R.string.newsItemHit, update.getHitPercent());
				body = context.getString(R.string.newsItemWonInLastHours, update.getNickname(), update.getHitPercent(), update.getLastHours(), update.getProfitFormatted());
				hasNicknameInBody = true;
				break;
			case SYS_8H_MOST_PROFITABLE:
				title = context.getString(R.string.newsItemRainmakerInAction);
				body = context.getString(R.string.newsItemMadeProfitInLastHours, update.getNickname(), update.getProfitFormatted(), update.getLastHours());
				hasNicknameInBody = true;
				break;
			case SYS_ANY_IN_MONEY_EXPIRY:
				title = context.getString(R.string.newsItemMustBeHappy , update.getNickname());
				body = context.getString(R.string.newsItemMadeOnMarket, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case SYS_ANY_NEW_INV:
				title = update.getNickname();
				body = context.getString(R.string.newsItemBoughtOnMarket,"", update.getDirectionFormatted(), update.getMarketName(), update.getAmountFormatted(), update.getLevelFormated()).trim();
				isCopiersWatchers = true;
				isFollowButton = true;
				break;
			case SYS_ANY_SHOW_OFF:
				title = context.getString(R.string.newsItemShowOff , update.getNickname());
				body = context.getString(R.string.newsItemIJustMade, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				isShowOfIcon = true;
				break;
			case SYS_NICKNAME_CHANGED:
				title = context.getString(R.string.newsItemChangedNicknameTo, update.getOldNickname(), update.getNickname());
				break;
			case W_100_EXPIRY:
				title = context.getString(R.string.newsItemMustBeHappy , update.getNickname());
				body = context.getString(R.string.newsItemMadeOnMarket, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				break;
			case W_NEARLY_FULL:
				title = context.getString(R.string.newsItemSeatsLeft , update.getSeatsLeft());
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = title + "\n" + context.getString(R.string.newsItemCopiersListAlmostFull, update.getNickname());
				}else{
					body = context.getString(R.string.newsItemCopiersListAlmostFull, update.getNickname());
				}
				hasNicknameInBody = true;
				isCopyWatchButtons = true;
				break;
			case W_NEW_INVESTMENT:
				if(update.getQueueType() == QueueTypeEnum.IN_APP_STRIP){
					body = context.getString(R.string.newsItemBoughtOnMarket, update.getNickname(), update.getDirectionFormatted(), update.getMarketName(), update.getAmountFormatted(), update.getLevelFormated());
					hasNicknameInBody = true;
				}else{
					title = update.getNickname();
					body = context.getString(R.string.newsItemBoughtOnMarket, "", update.getDirectionFormatted(), update.getMarketName(), update.getAmountFormatted(), update.getLevelFormated()).trim();
					isCopiersWatchers = true;
					isFollowButton = true;
				}
				break;
			case W_NOW_AVAILABLE:
				title = context.getString(R.string.newsItemSeatOpen);
				body = context.getString(R.string.newsItemVacancySeat, update.getNickname());
				hasNicknameInBody = true;
				isCopyWatchButtons = true;
				break;
			case W_SHOW_OFF:
				title = context.getString(R.string.newsItemShowOff , update.getNickname());
				body = context.getString(R.string.newsItemIJustMade, update.getAmountFormatted(), update.getMarketName(), update.getSettledTimeFormatted());
				isCopiersWatchers = true;
				isCopyWatchButtons = true;
				isShowOfIcon = true;
				break;
		}
		
		update.setType(type);
		//Check if the user is the current user:
		if( (update.getSecondUserId() == Application.get().getUser().getId() ) || (update.getUserId() == Application.get().getUser().getId())){
			isCopyWatchButtons = false;
			isFollowButton = false;
		}
		if(update.getUserId() == Application.get().getUser().getId()){
			//Set the AVATAR if it is null:
			if(update.getIconURL() == null || update.getIconURL().isEmpty()){
				if(COApplication.get().getProfile() != null){
					update.setIconURL(COApplication.get().getProfile().getAvatar());
				}
			}
		}
		///////////////////////////////////////
		update.setHasNicknameInBody(hasNicknameInBody);
		update.setPromotionIconResId(promotionIconresId);
		update.setCopiersWatchers(isCopiersWatchers);
		update.setCopyWatchButtons(isCopyWatchButtons);
		update.setFollowButton(isFollowButton);
		update.setDepositButton(isDepositButton);
		update.setCoinsButton(isCoinsButton);
		update.setShowOfIcon(isShowOfIcon);
		update.setMarketIcon(isMarketIcon);
		//Check the state of copying and watching:
		update.setCopying(COApplication.get().isCopying((update.getSecondUserId() > 0) ? update.getSecondUserId():update.getUserId()));
		if(update.isCopying()){
			update.setWatching(true);
		}else{
			update.setWatching(COApplication.get().isWatching((update.getSecondUserId() > 0) ? update.getSecondUserId():update.getUserId()));
		}
		/////////////////////////////////////////
		
		title = COTextUtils.makeInnerTextUnbreakable(title, update.getNickname(), update);
		title = COTextUtils.makeInnerTextUnbreakable(title, update.getSecondNickname(), update);
		title = COTextUtils.makeInnerTextUnbreakable(title, update.getOldNickname(), update);
		body = COTextUtils.makeInnerTextUnbreakable(body, update.getNickname(), update);
		body = COTextUtils.makeInnerTextUnbreakable(body, update.getSecondNickname(), update);
		body = COTextUtils.makeInnerTextUnbreakable(body, update.getMarketName(), update);
		
		update.setTitle(title);
		update.setBody(body);
	}
	
	
	/**
	 * Converts the {@link FeedMessage} into a {@link Update} object.
	 */
	public static Update convertFeedMessageToUpdate(FeedMessage feedMessage){
		Log.d(TAG, "Start converting FeedMessage To Update ...");
		
		Map<String, String> map = feedMessage.getProperties();
		Update update = convertMapToUpdate(map);
		update.setUpdateType(UpdateTypeEnum.getById(feedMessage.getMsgType()));
		update.setQueueType(QueueTypeEnum.getById(feedMessage.getQueueType()));
		update.setTimestamp(feedMessage.getTimeCreated().getTime(), true);
		formatUpdate(update);
		Log.d(TAG, "Complete converting FeedMessage To Update.");
		return update;
	}
	
	/**
	 * Converts the LISGHTREAMER {@link UpdateInfo} into a {@link Update} object.
	 */
	public static Update convertLSUpdateToUpdate(UpdateInfo update, QueueTypeEnum queueType){
		Log.d(TAG, "Start converting Update To Update ...");
		
		HashMap<String,	String> map = new HashMap<String, String>();
		
		map.put(FeedMessage.PROPERTY_KEY_UPDATE_TYPE, update.getNewValue(FeedMessage.PROPERTY_KEY_UPDATE_TYPE));
		map.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR + Application.get().getUser().getCurrencyId(), update.getNewValue(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR + Application.get().getUser().getCurrencyId()));
		map.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, update.getNewValue(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS));
		map.put(FeedMessage.PROPERTY_KEY_ASSET_TREND, update.getNewValue(FeedMessage.PROPERTY_KEY_ASSET_TREND));
		map.put(FeedMessage.PROPERTY_KEY_AVATAR, update.getNewValue(FeedMessage.PROPERTY_KEY_AVATAR));
		map.put(FeedMessage.PROPERTY_KEY_COPIED_COUNT, update.getNewValue(FeedMessage.PROPERTY_KEY_COPIED_COUNT));
		map.put(FeedMessage.PROPERTY_KEY_COPIERS, update.getNewValue(FeedMessage.PROPERTY_KEY_COPIERS));
		map.put(FeedMessage.PROPERTY_KEY_DEST_USER_ID, update.getNewValue(FeedMessage.PROPERTY_KEY_DEST_USER_ID));
		map.put(FeedMessage.PROPERTY_KEY_DEST_NICKNAME, update.getNewValue(FeedMessage.PROPERTY_KEY_DEST_NICKNAME));
		map.put(FeedMessage.PROPERTY_KEY_DIRECTION, update.getNewValue(FeedMessage.PROPERTY_KEY_DIRECTION));
		map.put(FeedMessage.PROPERTY_KEY_HIT_RATE, update.getNewValue(FeedMessage.PROPERTY_KEY_HIT_RATE));
		map.put(FeedMessage.PROPERTY_KEY_INV_ID, update.getNewValue(FeedMessage.PROPERTY_KEY_INV_ID));
		map.put(FeedMessage.PROPERTY_KEY_INVESTMENT_TYPE, update.getNewValue(FeedMessage.PROPERTY_KEY_INVESTMENT_TYPE));
		map.put(FeedMessage.PROPERTY_KEY_LEVEL, update.getNewValue(FeedMessage.PROPERTY_KEY_LEVEL));
		map.put(FeedMessage.PROPERTY_KEY_MARKET_ID, update.getNewValue(FeedMessage.PROPERTY_KEY_MARKET_ID));
		map.put(FeedMessage.PROPERTY_KEY_NICKNAME, update.getNewValue(FeedMessage.PROPERTY_KEY_NICKNAME));
		map.put(FeedMessage.PROPERTY_KEY_ODDS_LOSE, update.getNewValue(FeedMessage.PROPERTY_KEY_ODDS_LOSE));
		map.put(FeedMessage.PROPERTY_KEY_ODDS_WIN, update.getNewValue(FeedMessage.PROPERTY_KEY_ODDS_WIN));
		map.put(FeedMessage.PROPERTY_KEY_OPPORTUNITY_EXPIRE, update.getNewValue(FeedMessage.PROPERTY_KEY_OPPORTUNITY_EXPIRE));
		map.put(FeedMessage.PROPERTY_KEY_PROFIT + "_" +  + Application.get().getUser().getCurrencyId(), update.getNewValue(FeedMessage.PROPERTY_KEY_PROFIT + "_" +  + Application.get().getUser().getCurrencyId()));
		map.put(FeedMessage.PROPERTY_KEY_TIME_SETTLED, update.getNewValue(FeedMessage.PROPERTY_KEY_TIME_SETTLED));
		map.put(FeedMessage.PROPERTY_KEY_USER_ID, update.getNewValue(FeedMessage.PROPERTY_KEY_USER_ID));
		map.put(FeedMessage.PROPERTY_KEY_WATCHERS, update.getNewValue(FeedMessage.PROPERTY_KEY_WATCHERS));
		map.put(FeedMessage.PROPERTY_BONUS_TYPE_ID, update.getNewValue(FeedMessage.PROPERTY_BONUS_TYPE_ID));
		map.put(FeedMessage.PROPERTY_BONUS_DESCRIPTION, update.getNewValue(FeedMessage.PROPERTY_BONUS_DESCRIPTION));
		map.put(FeedMessage.PROPERTY_KEY_TOP_RANK, update.getNewValue(FeedMessage.PROPERTY_KEY_TOP_RANK));
		map.put(FeedMessage.PROPERTY_KEY_RANKED_IN_TOP, update.getNewValue(FeedMessage.PROPERTY_KEY_RANKED_IN_TOP));
		map.put(FeedMessage.PROPERTY_KEY_COINS_BALANCE, update.getNewValue(FeedMessage.PROPERTY_KEY_COINS_BALANCE));
		map.put(FeedMessage.PROPERTY_KEY_TIME_CREATED, update.getNewValue(FeedMessage.PROPERTY_KEY_TIME_CREATED));
		map.put(FeedMessage.PROPERTY_KEY_FIRST_NAME, update.getNewValue(FeedMessage.PROPERTY_KEY_FIRST_NAME));
		map.put(FeedMessage.PROPERTY_KEY_LAST_NAME, update.getNewValue(FeedMessage.PROPERTY_KEY_LAST_NAME));	
		map.put(FeedMessage.PROPERTY_KEY_OLD_NICKNAME, update.getNewValue(FeedMessage.PROPERTY_KEY_OLD_NICKNAME));		
		map.put(FeedMessage.PROPERTY_KEY_UUID, update.getNewValue(FeedMessage.PROPERTY_KEY_UUID));
		map.put(FeedMessage.PROPERTY_KEY_INHERITED_UUID, update.getNewValue(FeedMessage.PROPERTY_KEY_INHERITED_UUID));
		map.put(FeedMessage.PROPERTY_KEY_TIME_LAST_INVEST, update.getNewValue(FeedMessage.PROPERTY_KEY_TIME_LAST_INVEST));
		map.put(FeedMessage.PROPERTY_KEY_SEATS_LEFT, update.getNewValue(FeedMessage.PROPERTY_KEY_SEATS_LEFT));		
		map.put(FeedMessage.PROPERTY_KEY_USER_FROZEN, update.getNewValue(FeedMessage.PROPERTY_KEY_USER_FROZEN));		
		map.put(FeedMessage.PROPERTY_KEY_DEST_USER_FROZEN, update.getNewValue(FeedMessage.PROPERTY_KEY_DEST_USER_FROZEN));		
		
		Update result = convertMapToUpdate(map);
		result.setQueueType(queueType);
		formatUpdate(result);
		
		Log.d(TAG, "Complete converting Update To Update.");
		return result;
	}
	
	/**
	 * Converts the {@link Map} to a {@link Update} object.
	 * @param map
	 * @return
	 */
	public static Update convertMapToUpdate(Map<String, String> map){
		Log.d(TAG, "Start converting Map To Update ...");
		
		Update update = new Update();
		String type							= map.get(FeedMessage.PROPERTY_KEY_UPDATE_TYPE);
		String amount						= map.get(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR + Application.get().getUser().getCurrencyId());
		String lastHours					= map.get(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS); 
		String trend						= map.get(FeedMessage.PROPERTY_KEY_ASSET_TREND);
		String avatar						= map.get(FeedMessage.PROPERTY_KEY_AVATAR);
		String copiedCount					= map.get(FeedMessage.PROPERTY_KEY_COPIED_COUNT);		
		String copiers						= map.get(FeedMessage.PROPERTY_KEY_COPIERS);
		String secondUserId					= map.get(FeedMessage.PROPERTY_KEY_DEST_USER_ID);
		String secondNickname				= map.get(FeedMessage.PROPERTY_KEY_DEST_NICKNAME);
		String direction					= map.get(FeedMessage.PROPERTY_KEY_DIRECTION);	
		String hitRate						= map.get(FeedMessage.PROPERTY_KEY_HIT_RATE);
		String investmentId					= map.get(FeedMessage.PROPERTY_KEY_INV_ID);
		String investmentType				= map.get(FeedMessage.PROPERTY_KEY_INVESTMENT_TYPE);
		String level						= map.get(FeedMessage.PROPERTY_KEY_LEVEL);
		String marketId						= map.get(FeedMessage.PROPERTY_KEY_MARKET_ID);
		String nickname						= map.get(FeedMessage.PROPERTY_KEY_NICKNAME);
		String oddsLose						= map.get(FeedMessage.PROPERTY_KEY_ODDS_LOSE);
		String oddsWin						= map.get(FeedMessage.PROPERTY_KEY_ODDS_WIN);
		String opportunityExpire			= map.get(FeedMessage.PROPERTY_KEY_OPPORTUNITY_EXPIRE);
		String profit						= map.get(FeedMessage.PROPERTY_KEY_PROFIT + "_" + Application.get().getUser().getCurrencyId());
		String timeSettled					= map.get(FeedMessage.PROPERTY_KEY_TIME_SETTLED);
		String userId						= map.get(FeedMessage.PROPERTY_KEY_USER_ID);
		String watchers						= map.get(FeedMessage.PROPERTY_KEY_WATCHERS);
		String bonusTypeId 					= map.get(FeedMessage.PROPERTY_BONUS_TYPE_ID);
		String bonusDescription				= map.get(FeedMessage.PROPERTY_BONUS_DESCRIPTION);
		String topProfitable  				= map.get(FeedMessage.PROPERTY_KEY_RANKED_IN_TOP);
		String coinsBalance  				= map.get(FeedMessage.PROPERTY_KEY_COINS_BALANCE);
		String seatsLeft  					= map.get(FeedMessage.PROPERTY_KEY_SEATS_LEFT);
		String timestamp					= map.get(FeedMessage.PROPERTY_KEY_TIME_CREATED);
		String firstName 					= map.get(FeedMessage.PROPERTY_KEY_FIRST_NAME);
		String lastName 					= map.get(FeedMessage.PROPERTY_KEY_LAST_NAME);
		String oldNickname 					= map.get(FeedMessage.PROPERTY_KEY_OLD_NICKNAME);
		String lastInvestTime				= map.get(FeedMessage.PROPERTY_KEY_TIME_LAST_INVEST);
		String isUserFrozen 				= map.get(FeedMessage.PROPERTY_KEY_USER_FROZEN);
		String isSecondUserFrozen 			= map.get(FeedMessage.PROPERTY_KEY_DEST_USER_FROZEN);		
		
		if(type != null){
			try {
				UpdateTypeEnum updateType = UpdateTypeEnum.getById(Integer.valueOf(type));
				update.setUpdateType(updateType);
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse update type", e);
			}
		}
		if(amount != null){
			try {
				update.setAmount(Double.valueOf(amount));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse amount");
			}
		}
		if(lastHours != null){
			try {
				update.setLastHours(Integer.valueOf(lastHours));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse trading hours");
			}
		}
		if(trend != null){
			try {
				update.setTrendPercent(Double.valueOf(trend));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse trend percent");
			}
		}
		update.setIconURL(avatar);
		if(copiedCount != null){
			try {
				update.setNewCopiers(Long.valueOf(copiedCount));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse new copiers count");
			}
		}
		if(copiers != null){
			try {
				update.setCopiers(Long.valueOf(copiers));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse copiers count");
			}
		}
		if(secondUserId != null){
			try {
				update.setSecondUserId(Long.valueOf(secondUserId));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Second User Id");
			}
		}
		update.setSecondNickname(secondNickname);
		if(direction != null){
			try {
				update.setDirection(Integer.valueOf(direction));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse direction");
			}
		}
		if(hitRate != null){
			try {
				update.setHitPercent(Double.valueOf(hitRate));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse hit percent");
			}
		}
		if(investmentId != null){
			try {
				update.setInvestmentId(Long.valueOf(investmentId));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse investment id");
			}
		}
		if(investmentType != null){
			try {
				update.setInvestmentType(Long.valueOf(investmentType));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse investment type");
			}
		}
		if(marketId != null){
			try {
				Market market = Application.get().getMarketForID(Long.valueOf(marketId));
				update.setMarket(market);
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Market id");
			}
		}
		if(level != null){
			try {
				update.setLevel(Double.valueOf(level));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Level");
			}
		}
		update.setNickname(nickname);
		if(oddsLose != null){
			try {
				update.setOddsLose(Double.valueOf(oddsLose));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Odds Lose");
			}
		}
		if(oddsWin != null){
			try {
				update.setOddsWin(Double.valueOf(oddsWin));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Odds Win");
			}
		}
		if(opportunityExpire != null){
			try {
				update.setExpiryTimestamp(Long.valueOf(opportunityExpire));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Opportunity Expire Timestamp");
			}
		}
		if(profit != null){
			try {
				update.setProfit(Double.valueOf(profit));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse profit");
			}
		}
		if(timeSettled != null){
			try {
				update.setSettledTime(Long.valueOf(timeSettled));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse time settled");
			}
		}
		if(userId != null){
			try {
				update.setUserId(Long.valueOf(userId));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse time created");
			}
		}
		if(watchers != null){
			try {
				update.setWatchers(Long.valueOf(watchers));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse watchers count");
			}
		}
		if(bonusTypeId != null){
			try {
				update.setBonusTypeId(Long.valueOf(bonusTypeId));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse watchers count");
			}
		}
		update.setBonusDescription(bonusDescription);
		if(topProfitable != null){
			try {
				update.setTopProfitableTraders(Integer.valueOf(topProfitable));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse top profitable count");
			}
		}
		if(coinsBalance != null){
			try {
				update.setCoinsBalance(Long.valueOf(coinsBalance));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse coins balance");
			}
		}
		if(seatsLeft != null){
			try {
				update.setSeatsLeft(Integer.valueOf(seatsLeft));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse seats left");
			}
		}
		if(timestamp != null){
			try {
				update.setTimestamp(Long.valueOf(timestamp), false);
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse timestamp", e);
			}
		}
		update.setFirstName(firstName);
		update.setLastName(lastName);
		update.setOldNickname(oldNickname);
		if(lastInvestTime != null){
			try {
				update.setLastInvestTime(Long.valueOf(lastInvestTime));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Last Invest Time", e);
			}
		}
		if(isUserFrozen != null){
			try {
				update.setCopyingFrozen(Boolean.valueOf(isUserFrozen));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Is User Frozen", e);
			}
		}
		if(isSecondUserFrozen != null){
			try {
				update.setCopyingFrozen(Boolean.valueOf(isSecondUserFrozen));
			} catch (Exception e) {
				Log.e(TAG, "updateItem -> Could NOT parse Is Second User Frozen", e);
			}
		}
		
		Log.d(TAG, "Complete converting Map To Update.");
		return update;
	}

	/**
	 * Converts the {@link FeedMessage} to {@link HotTrader}.
	 * @param feedMessage
	 * @return
	 */
	public static HotTrader convertFeedMessageToHotTrader(FeedMessage feedMessage){
		Log.d(TAG, "Start converting FeedMessage To HotTrader...");
		if(QueueTypeEnum.getById(feedMessage.getQueueType()) != QueueTypeEnum.HOT){
			Log.e(TAG, "convertFeedMessageToHotTrader -> FeedMessage is NOT of type HOT!!!");
			return null;
		}
		HotTrader hotTrader = new HotTrader();
		Map<String, String> map = feedMessage.getProperties();
		
		hotTrader.setUpdateType(UpdateTypeEnum.getById(feedMessage.getMsgType()));
		
		String userId 			= map.get(FeedMessage.PROPERTY_KEY_USER_ID);
		String avatar 			= map.get(FeedMessage.PROPERTY_KEY_AVATAR);
		String nickname 		= map.get(FeedMessage.PROPERTY_KEY_NICKNAME);
		String copiersCount 	= map.get(FeedMessage.PROPERTY_KEY_COPIERS);
		String profit 			= map.get(FeedMessage.PROPERTY_KEY_PROFIT + "_" + Application.get().getUser().getCurrencyId());
		String hitPercent 		= map.get(FeedMessage.PROPERTY_KEY_HIT_RATE);
		String lastHours 		= map.get(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS); 
		
		if(userId != null){
			try {
				hotTrader.setUserId(Long.valueOf(userId));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse user id.");
			}
		}
		hotTrader.setIconURL(avatar);
		if(hotTrader.getUserId() == Application.get().getUser().getId()){
			hotTrader.setNickname(Application.get().getString(R.string.hotNicknameMe));
		}else{
			hotTrader.setNickname(nickname);
		}
		if(copiersCount != null){
			try {
				hotTrader.setCopiersCount(Long.valueOf(copiersCount));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse copiers count.");
			}
		}
		if(profit != null){
			try {
				hotTrader.setProfit(Double.valueOf(profit));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse profit.");
			}
		}
		if(hitPercent != null){
			try {
				hotTrader.setHitPercent(Integer.valueOf(hitPercent));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse HIt Percent.");
			}
		}
		if(lastHours != null){
			try {
				hotTrader.setLastHours(Integer.valueOf(lastHours));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse trading hours");
			}
		}
		
		Log.d(TAG, "Complete converting FeedMessage To HotTrader.");
		return hotTrader;
	}
	
	/**
	 * Converts the {@link FeedMessage} to {@link HotTrade}.
	 * @param feedMessage
	 * @return
	 */
	public static HotTrade convertFeedMessageToHotTrade(FeedMessage feedMessage){
		Log.d(TAG, "Start converting FeedMessage To HotTrade...");
		if(QueueTypeEnum.getById(feedMessage.getQueueType()) != QueueTypeEnum.HOT){
			Log.e(TAG, "convertFeedMessageToHotTrade -> FeedMessage is NOT of type HOT!!!");
			return null;
		}
		HotTrade hotTrade = new HotTrade();
		Map<String, String> map = feedMessage.getProperties();
		
		hotTrade.setUpdateType(UpdateTypeEnum.getById(feedMessage.getMsgType()));
		
		String userId 			= map.get(FeedMessage.PROPERTY_KEY_USER_ID);
		String avatar 			= map.get(FeedMessage.PROPERTY_KEY_AVATAR);
		String nickname 		= map.get(FeedMessage.PROPERTY_KEY_NICKNAME);
		String profit 			= map.get(FeedMessage.PROPERTY_KEY_PROFIT + "_" + Application.get().getUser().getCurrencyId());
		String lastHours 		= map.get(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS); 
		String marketId 		= map.get(FeedMessage.PROPERTY_KEY_MARKET_ID); 
		String time 			= map.get(FeedMessage.PROPERTY_KEY_ASSET_CREATE_DATE_TIME);
		
		if(userId != null){
			try {
				hotTrade.setUserId(Long.valueOf(userId));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse user id.");
			}
		}
		hotTrade.setIconURL(avatar);
		if(hotTrade.getUserId() == Application.get().getUser().getId()){
			hotTrade.setNickname(Application.get().getString(R.string.hotNicknameMe));
		}else{
			hotTrade.setNickname(nickname);
		}
		if(profit != null){
			try {
				hotTrade.setProfit(Double.valueOf(profit));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse profit.");
			}
		}
		if(marketId != null){
			try {
				Market market = Application.get().getMarketForID(Long.valueOf(marketId));
				hotTrade.setMarket(market);
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse market id.");
			}
		}
		if(time != null){
			try {
				hotTrade.setTime(COTimeUtils.getTimeFromHotDate(time));
			}catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse Create Time");
			}
		}
		if(lastHours != null){
			try {
				hotTrade.setLastHours(Integer.valueOf(lastHours));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse trading hours");
			}
		}
		
		Log.d(TAG, "Complete converting FeedMessage To HotTrade.");
		return hotTrade;
	}
	
	/**
	 * Converts the {@link FeedMessage} to {@link HotCopier}.
	 * @param feedMessage
	 * @return
	 */
	public static HotCopier convertFeedMessageToHotCopier(FeedMessage feedMessage){
		Log.d(TAG, "Start converting FeedMessage To HotCopier...");
		if(QueueTypeEnum.getById(feedMessage.getQueueType()) != QueueTypeEnum.HOT){
			Log.e(TAG, "convertFeedMessageToHotCopier -> FeedMessage is NOT of type HOT!!!");
			return null;
		}
		HotCopier hotCopier = new HotCopier();
		Map<String, String> map = feedMessage.getProperties();
		
		hotCopier.setUpdateType(UpdateTypeEnum.getById(feedMessage.getMsgType()));
		
		String userId 			= map.get(FeedMessage.PROPERTY_KEY_USER_ID);
		String avatar 			= map.get(FeedMessage.PROPERTY_KEY_AVATAR);
		String nickname 		= map.get(FeedMessage.PROPERTY_KEY_NICKNAME);
		String copiersCount 	= map.get(FeedMessage.PROPERTY_KEY_COPIERS);
		String profit 			= map.get(FeedMessage.PROPERTY_KEY_PROFIT + "_" + Application.get().getUser().getCurrencyId());
		String hitPercent 		= map.get(FeedMessage.PROPERTY_KEY_HIT_RATE);
		String lastHours 		= map.get(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS); 
		String copyingCount		= map.get(FeedMessage.PROPERTY_KEY_COPYING); 
		
		if(userId != null){
			try {
				hotCopier.setUserId(Long.valueOf(userId));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse user id.");
			}
		}
		hotCopier.setIconURL(avatar);
		if(hotCopier.getUserId() == Application.get().getUser().getId()){
			hotCopier.setNickname(Application.get().getString(R.string.hotNicknameMe));
		}else{
			hotCopier.setNickname(nickname);
		}
		if(copiersCount != null){
			try {
				hotCopier.setCopiersCount(Long.valueOf(copiersCount));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse copiers count.");
			}
		}
		if(profit != null){
			try {
				hotCopier.setProfit(Double.valueOf(profit));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse profit.");
			}
		}
		if(hitPercent != null){
			try {
				hotCopier.setHitPercent(Integer.valueOf(hitPercent));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse HIt Percent.");
			}
		}
		if(lastHours != null){
			try {
				hotCopier.setLastHours(Integer.valueOf(lastHours));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrader -> Could NOT parse trading hours");
			}
		}
		if(copyingCount != null){
			try {
				hotCopier.setCopyingCount(Long.valueOf(copyingCount));
			} catch (Exception e) {
				Log.e(TAG, "convertFeedMessageToHotTrade -> Could NOT parse copying count.");
			}
		}
		
		Log.d(TAG, "Complete converting FeedMessage To HotCopier.");
		return hotCopier;
	}
	
}
