package com.copyop.android.app.util;

import com.anyoption.android.app.util.constants.Constants;

public class COConstants extends Constants{
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Jimi)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.dzhamaldv.bg.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String LS_COPYOP_URL = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.bgtestenv.anyoption.com/mobile/";
//	public static final String BONUS_LINK = "http://www.bgtestenv.anyoption.com/jsonService/html/mobile/bonus-terms.html";
	//################################################################################################
	
	//################################################################################################
	//////////////////			LOCAL ENVIRONMENT (Pavkata)		//////////////////////////////////////////////
	//################################################################################################ 
	
//	public static final String SERVICE_URL = "http://www.pavelhe.bg.anyoption.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String LS_COPYOP_URL = "http://ls.bgtestenv.anyoption.com:80";
//	public static final String MERCHANT_ID = "000203AYP";
//	public static final String ONE_CLICK_URL = "http://test.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.bgtestenv.anyoption.com/mobile/";
//	public static final String BONUS_LINK = "http://www.bgtestenv.anyoption.com/jsonService/html/mobile/bonus-terms.html";
	//################################################################################################
	
	//################################################################################################
	////////////////////////		BG TEST ENVIRONMENT			/////////////////////////////////////
	//################################################################################################ 
	
	public static final String SERVICE_URL = "https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/";
	public static final String LS_URL = "http://ls.bgtestenv.anyoption.com:80";//TODO 443 for https, 80 for http
	public static final String LS_COPYOP_URL = "http://ls.bgtestenv.anyoption.com:80";//TODO 443 for https, 80 for http
	public static final String MERCHANT_ID = "000203AYP";
	public static final String ONE_CLICK_URL = "https://test.envoytransfers.com";
	public static final String WEB_SITE_LINK = "https://m.bgtestenv.anyoption.com/mobile/";
	public static final String TERMS_LINK = "https://www.bgtestenv.anyoption.com/jsonService/html/mobile/agreement.html";
	public static final String BONUS_LINK = "http://www.bgtestenv.anyoption.com/jsonService/html/mobile/bonus-terms.html";
	//################################################################################################
	
	
	//################################################################################################
    //////////////////         LIVE ENVIRONMENT		////////////////////////////////////////////////
	//################################################################################################
	
//	public static final String SERVICE_URL = "https://www.copyop.com/jsonService/AnyoptionService/";
//	public static final String LS_URL = "http://ls.copyop.com:80";
//	public static final String LS_COPYOP_URL = "http://ls.copyop.com:80";
//	public static final String MERCHANT_ID = "000266AYP";
//	public static final String ONE_CLICK_URL = "http://www.envoytransfers.com";
//	public static final String WEB_SITE_LINK = "http://m.anyoption.com/mobile/";
//	public static final String TERMS_LINK = "https://www.copyop.com/html/mobile/agreement.html";
//	public static final String BONUS_LINK = "https://www.copyop.com/html/mobile/bonus-terms.html";
	
	//################################################################################################
	
	public static final long MOBILE_WRITER_ID = 20000l;
	
	//################################################################################################
	
	//################################################################################################
	/////////////////////////				SERVICES				//////////////////////////////////
	//################################################################################################

	public static final String SERVICE_GET_PROFILE							= "getCopyopProfile";
	public static final String SERVICE_GET_COPYOP_USER						= "getCopyopUser";
	public static final String SERVICE_GET_FACEBOOK_FRIENDS					= "getFacebookFriends";
	public static final String SERVICE_GET_COPY_USER_CONFIG					= "getCopyopCopyUserConfig";
	public static final String SERVICE_GET_WATCH_USER_CONFIG				= "getCopyopWatchUserConfig";
	public static final String SERVICE_GET_COPYING							= "getCopyopCopying";
	public static final String SERVICE_GET_COPIERS							= "getCopyopCopiers";
	public static final String SERVICE_GET_WATCHING							= "getCopyopWatching";
	public static final String SERVICE_GET_WATCHERS							= "getCopyopWatchers";
	public static final String SERVICE_GET_PROFILE_COINS					= "getCopyopProfileCoins";
	public static final String SERVICE_GET_COPYOP_NEWS						= "getCopyopNews";
	public static final String SERVICE_GET_COPYOP_EXPLORE					= "copyopExplore";
	public static final String SERVICE_GET_COPYOP_HOT_TRADERS				= "getCopyopHotTraders";
	public static final String SERVICE_GET_COPYOP_HOT_TRADES				= "getCopyopHotTrades";
	public static final String SERVICE_GET_COPYOP_HOT_COPIERS				= "getCopyopHotCopiers";
	public static final String SERVICE_GET_COPYOP_INBOX						= "getCopyopInbox";	
	public static final String SERVICE_GET_COPYOP_HITS						= "getCopyopHits";		
	public static final String SERVICE_GET_PROFILE_BEST_ASSETS				= "getCopyopProfileBestAssets";
	public static final String SERVICE_GET_ORIGINAL_INVEST_PROFILE			= "getCopyopOriginalInvProfile";	
	public static final String SERVICE_GET_LAST_LOGIN_SUMMARY				= "getLastLoginSummary";
	public static final String SERVICE_GET_COPY_TOOLTIP_INFO				= "getCopyStepsEncourageToolTip";
	
	public static final String SERVICE_ACCEPT_TERMS_AND_CONDITIONS			= "acceptedTermsAndConditions";
	public static final String SERVICE_INSERT_PROFILE						= "insertCopyopProfile";
	public static final String SERVICE_MY_PROFILE							= "myCopyopProfile";
	public static final String SERVICE_COPY_USER_CONFIG						= "copyopCopyUserConfig";
	public static final String SERVICE_WATCH_CONFIG							= "copyopWatchConfig";
	public static final String SERVICE_UPDATE_FACEBOOK_FRIENDS				= "updateFacebookFriends";
	public static final String SERVICE_DEPOSIT_COINS						= "depositCopyopCoins";
	public static final String SERVICE_FOLLOW								= "copyopFollow";
	public static final String SERVICE_UPDATE_COPYOP_VATAR					= "updateCopyopProfileAvatar";
	public static final String SERVICE_CHANGE_COPYOP_NICKNAME				= "changeCopyopNickname";	
	
	public static final String SERVICE_UPLOAD_AVATAR						= "UploadMobileService";
	public static final String SERVICE_SHOW_OFF								= "copyopShowOff";
	
	//################################################################################################
	
	//################################################################################################
	/////////////////////////				EXTRAS					//////////////////////////////////
	//################################################################################################
	public static final String EXTRA_NEWS_HOT_EXPLORE_TAB				= "news_hot_explore_tab";
	public static final String EXTRA_PROFILE							= "profile";
	public static final String EXTRA_PROFILE_ID							= "profile_id";
	public static final String EXTRA_NICKNAME							= "nickname";
	public static final String EXTRA_IS_COPYING							= "is_copying";
	public static final String EXTRA_IS_WATCHING						= "is_watching";
	public static final String EXTRA_IS_FROZEN							= "is_frozen";
	public static final String EXTRA_FACEBOOK_ID						= "facebook_id";
	public static final String EXTRA_LANGUAGES_LIST						= "languages_list";
	public static final String EXTRA_LANGUAGE							= "language";
	public static final String EXTRA_HOT_GROUP							= "hot_group";
	public static final String EXTRA_MARKET_ID							= "market_id";
	public static final String EXTRA_INVESTMENT_ID						= "inv_id";
	public static final String EXTRA_INVESTMENT_TYPE					= "inv_type";
	public static final String EXTRA_AMOUNT								= "amount";
	public static final String EXTRA_EXPIRY_TIME						= "expiry_time";
	public static final String EXTRA_COPY_AMOUNTS						= "copy_amounts";
	public static final String EXTRA_TOP_TRADERS						= "top_traders";	
	public static final String EXTRA_OPEN_FOLLOW_DIALOG					= "open_follow_dialog";
	public static final String EXTRA_PUSH_NOTIFICATION_BUNDLE			= "push_not_bundle";
	public static final String EXTRA_COPY_ODDS_WIN						= "copy_odds_win";
	public static final String EXTRA_COPY_ODDS_LOSE						= "copy_odds_lose";
	public static final String EXTRA_MAX_COPIERS_PER_ACCOUNT			= "max_copiers_per_account";
	//################################################################################################
	
	//################################################################################################
	///////////////////				PREFERENCES			/////////////////////////////////////////
	//################################################################################################
	
	public static final String PREFERENCES_IS_NEWS_WALKTHROUGH_SHOWN	 	= "pref_is_news_wt_shown";
	public static final String PREFERENCES_IS_HOT_WALKTHROUGH_SHOWN	 		= "pref_is_hot_wt_shown";
	public static final String PREFERENCES_IS_PROFILE_WALKTHROUGH_SHOWN	 	= "pref_is_profile_wt_shown";
	public static final String PREFERENCES_IS_COPY_WALKTHROUGH_SHOWN	 	= "pref_is_copy_wt_shown";
	
	public static final String PREFERENCES_IS_AUTO_WATCHED_SHOWN	 	= "pref_is_auto_watched_shown";
	
	//################################################################################################
	
	//################################################################################################
	////////////////////			ERROR CODES				//////////////////////////////////////////
	//################################################################################################
	    
    public static final int ERROR_CODE_COPYOP_MAX_COPIERS						= 1001;
    public static final int ERROR_CODE_USER_REMOVED          					= 800;

	//################################################################################################

	//################################################################################################
	///										REQUEST CODES											//
	//					(Used in the onActivityResult(....)  callback method)						//
	//################################################################################################
	
	public final static int REQUEST_CODE_PICK_FROM_GALLERY			= 2;
	public final static int REQUEST_CODE_TAKE_PHOTO					= 3;
	public final static int REQUEST_CODE_CROP_IMAGE					= 4;
	
	//################################################################################################
	
	//################################################################################################
	////////////////////////					FORM FIELD NAMES				//////////////////////
	//################################################################################################
	public static final String FIELD_NICKNAME 					= "nickname";
	//################################################################################################
}