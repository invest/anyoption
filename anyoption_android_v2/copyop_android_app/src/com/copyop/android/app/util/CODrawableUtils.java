package com.copyop.android.app.util;

import java.util.Random;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.DrawableUtils;
import com.copyop.android.app.R;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.Log;

public class CODrawableUtils extends DrawableUtils{

	public static final String TAG = CODrawableUtils.class.getSimpleName();

	public static LayerDrawable createClickableDrawable(Context context, int color, int shadowColor, int shadowSize, boolean isBorder, boolean isPressed) {
		float radius = context.getResources().getDimension(R.dimen.button_radius);
        return CODrawableUtils.createClickableDrawable(context, color, shadowColor, shadowSize, isBorder, isPressed, radius);
    }
	
	public static LayerDrawable createClickableDrawable(Context context, int color, int shadowColor, int shadowSize, boolean isBorder, boolean isPressed, float radius) {
		
        float[] outerRadius = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};

        //Border
        RoundRectShape borderRoundRect = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable borderShapeDrawable = new ShapeDrawable(borderRoundRect);
        borderShapeDrawable.getPaint().setColor(context.getResources().getColor(R.color.button_border));
       
        //Shadow
        RoundRectShape shadowRectShape1 = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable shadowShapeDrawable1 = new ShapeDrawable(shadowRectShape1);
        shadowShapeDrawable1.getPaint().setColor(shadowColor);
        
        //Content
        RoundRectShape contentRoundRect = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable contentShapeDrawable = new ShapeDrawable(contentRoundRect);
        contentShapeDrawable.getPaint().setColor(color);
        
        //Create array
        Drawable[] drawableArray = null;
        if(!isPressed){
        	drawableArray = new Drawable[]{borderShapeDrawable, shadowShapeDrawable1, contentShapeDrawable};     
        }else{
        	drawableArray = new Drawable[]{borderShapeDrawable, contentShapeDrawable};        	
        }
        LayerDrawable layerDrawable = new LayerDrawable(drawableArray);

        int borderWidth;
        if(isBorder){
        	borderWidth =  (int) context.getResources().getDimension(R.dimen.button_border_width);        	
        }else{
        	borderWidth = 0;
        }
        int contentOffset = shadowSize + borderWidth;
        if(!isPressed){
        	layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        	layerDrawable.setLayerInset(1, borderWidth, borderWidth, borderWidth, borderWidth);   
        	layerDrawable.setLayerInset(2, borderWidth, borderWidth, borderWidth, contentOffset); 
        }else{
        	layerDrawable.setLayerInset(0, 0, shadowSize, 0, 0);
        	layerDrawable.setLayerInset(1, borderWidth, contentOffset, borderWidth, borderWidth);    
        }
        
        return layerDrawable;
    }
	
	public static LayerDrawable createClickableDrawable(Context context, int color, int shadowColor, int iconBGColor, int iconBGWidth, int shadowSize, boolean isBorder, boolean isPressed) {
		float radius = context.getResources().getDimension(R.dimen.button_radius);
        return CODrawableUtils.createClickableDrawable(context, color, shadowColor, iconBGColor, iconBGWidth, shadowSize, isBorder, isPressed, radius);
	}

	public static LayerDrawable createClickableDrawable(Context context, int color, int shadowColor, int iconBGColor, int iconBGWidth, int shadowSize, boolean isBorder, boolean isPressed, float radius) {
		
        float[] outerRadius = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};

        //Border
        RoundRectShape borderRoundRect = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable borderShapeDrawable = new ShapeDrawable(borderRoundRect);
        borderShapeDrawable.getPaint().setColor(context.getResources().getColor(R.color.button_border));
        ///////////////
        
        //Shadow
        RoundRectShape shadowRectShape1 = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable shadowShapeDrawable1 = new ShapeDrawable(shadowRectShape1);
        shadowShapeDrawable1.getPaint().setColor(shadowColor);
        //////////
        
        //Content
        RoundRectShape contentRoundRect1 = null;
        ShapeDrawable contentIconBGShapeDrawable = null;
        if(iconBGWidth > 0){
        	contentRoundRect1 = new RoundRectShape(outerRadius, null, null);
        	contentIconBGShapeDrawable = new ShapeDrawable(contentRoundRect1);
        	contentIconBGShapeDrawable.getPaint().setColor(iconBGColor);
        }

        float[] outerRadiusContent = null;
        if(iconBGWidth == 0){
        	outerRadiusContent = outerRadius;
        }else{
        	outerRadiusContent = new float[]{0, 0, radius, radius, radius, radius, 0, 0};
        }
        RoundRectShape contentRoundRect2 = new RoundRectShape(outerRadiusContent, null, null);
        ShapeDrawable contentMainShapeDrawable = new ShapeDrawable(contentRoundRect2);
        contentMainShapeDrawable.getPaint().setColor(color);
        ///////////////////
        
        //Create array
        Drawable[] drawableArray = null;
        if(!isPressed){
        	if(contentIconBGShapeDrawable != null){
        		drawableArray = new Drawable[]{borderShapeDrawable, shadowShapeDrawable1, contentIconBGShapeDrawable, contentMainShapeDrawable};        	        		
        	}else{
        		drawableArray = new Drawable[]{borderShapeDrawable, shadowShapeDrawable1, contentMainShapeDrawable};        	        		
        	}
        }else{
        	if(contentIconBGShapeDrawable != null){
        		drawableArray = new Drawable[]{borderShapeDrawable, contentIconBGShapeDrawable, contentMainShapeDrawable};        	        		
        	}else{
        		drawableArray = new Drawable[]{borderShapeDrawable, contentMainShapeDrawable};  
        	}
        }
        LayerDrawable layerDrawable = new LayerDrawable(drawableArray);

        int borderWidth;
        if(isBorder){
        	borderWidth =  (int) context.getResources().getDimension(R.dimen.button_border_width);        	
        }else{
        	borderWidth = 0;
        }
        int contentOffset = shadowSize + borderWidth;
        if(!isPressed){
        	layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        	layerDrawable.setLayerInset(1, borderWidth, borderWidth, borderWidth, borderWidth);    
        	if(contentIconBGShapeDrawable != null){
        		layerDrawable.setLayerInset(2, borderWidth, borderWidth, borderWidth, contentOffset); 
        		layerDrawable.setLayerInset(3, (iconBGWidth > 0) ? iconBGWidth : borderWidth, borderWidth, borderWidth, contentOffset); 
        	}else{
        		layerDrawable.setLayerInset(2, borderWidth, borderWidth, borderWidth, contentOffset); 
        	}
        }else{
        	layerDrawable.setLayerInset(0, 0, shadowSize, 0, 0);
        	if(contentIconBGShapeDrawable != null){
        		layerDrawable.setLayerInset(1, borderWidth, contentOffset, borderWidth, borderWidth); 
        		layerDrawable.setLayerInset(2, (iconBGWidth > 0) ? iconBGWidth : borderWidth, contentOffset, borderWidth, borderWidth);  
        	}else{
        		layerDrawable.setLayerInset(1, borderWidth, contentOffset, borderWidth, borderWidth); 
        	}
        }
        return layerDrawable;
	}
	
	/**
	 * Returns the size of the Bitmap.
	 * [0] = width 
	 * [1] = height
	 * @param resId
	 * @return
	 */
	public static int[] getDrawableSize(int resId){
		int[] result = new int[2];
		try {
			BitmapDrawable bd = (BitmapDrawable) Application.get().getResources().getDrawable(resId);
			result[0] = bd.getBitmap().getWidth();
			result[1] = bd.getBitmap().getHeight();
		} catch (Exception e) {
			Log.e(TAG, "getDrawableSize()", e);
		}
		return result;
	}
	
	public static int generateRandomPromotionResId(){
		Random rand = new Random();
		int number = 1 + rand.nextInt(9);
		String name = "present_0" + number;
		return getImageResID(name);
	}
	
}
