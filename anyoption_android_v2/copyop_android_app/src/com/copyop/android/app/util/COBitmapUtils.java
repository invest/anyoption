package com.copyop.android.app.util;

import java.io.ByteArrayOutputStream;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.BitmapUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

public class COBitmapUtils extends BitmapUtils {

	public static final String TAG = COBitmapUtils.class.getSimpleName();
	private static Picasso picasso = Picasso.with(Application.get());
	
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//&&&&&&&&&&&&&&&					PUBLIC METHODS						&&&&&&&&&&&&&&&&&%%
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	/**
	 * Loads the Bitmap from the given URI.
	 * @param uri The URI to get the Bitmap from.
	 * @param target Acts like a listener for the Bitmap loading.
	 */
	public static void loadBitmap(String uri, Target target) {
		picasso
		.load(uri)
		.into(target);
	}
	
	/**
	 * Loads the Bitmap from the given URI.
	 * @param uri The URI to get the Bitmap from.
	 * @param target Acts like a listener for the Bitmap loading.
	 * @param reqWidth The width we want the Bitmap to be scaled.
	 * @param reqHeight The height we want the Bitmap to be scaled.
	 */
	public static void loadBitmap(String uri, int reqWidth, int reqHeight, Target target) {
		picasso
		.load(uri)
		.resize(reqWidth, reqHeight)
		.centerCrop()//Must call resize before that (Also with the centerInside)
		.into(target);
	}
	
	/**
	 * Loads the image from URL into the ImageView.
	 * @param uri The URL to download the Image from.
	 * @param imageView The ImageView.
	 */
	public static void loadBitmap(String uri, ImageView imageView) {
		picasso
		.load(uri)
		.into(imageView);
	}

	/**
	 * Loads the image from URL into the ImageView.
	 * @param uri The URL to download the Image from.
	 * @param imageView The ImageView.
	 * @param reqWidth The width we want the Bitmap to be scaled.
	 * @param reqHeight The height we want the Bitmap to be scaled.
	 */
	public static void loadBitmap(String uri, ImageView imageView, int reqWidth, int reqHeight) {
		picasso
		.load(uri)
		.resize(reqWidth, reqHeight)
		.centerCrop()
		.into(imageView);
	}

	/**
	 * Converts the given Bitmap into a byte array.
	 * The compression format is JPEG with MAX quality.
	 * @param bitmap
	 * @return
	 */
	public static byte[] convertBitmapToByteArray(Bitmap bitmap){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] byteArr = baos.toByteArray(); 
		return byteArr;
	}
	
	/**
	 * Creates new resized Bitmap.
	 * @param bitmap
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap resizeBitmap(Bitmap bitmap, int reqWidth, int reqHeight) {
		try {
			Bitmap result = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, true);
			return result;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "resizeBitmap()", e);
		}
		return bitmap;
	}
	
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

}
