package com.copyop.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.copyop.android.app.R;
import com.copyop.android.app.model.Update;
import com.copyop.common.enums.base.QueueTypeEnum;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;

public class COTextUtils extends TextUtils {

	public static final String TAG = COTextUtils.class.getSimpleName();
	
	public static String makeTextUnbreakable(String text){
		if(!TextUtils.containsWhiteSpace(text.trim())){
			return text;
		}else{
			//Replace inner white spaces with invisible chars:
			return text.replace(" ", "\u200e");
		}
	}
	
	public static float getTextWidth(String text){
		Context context = Application.get();
		Paint paint = new Paint();
		paint.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.default_text));
		return paint.measureText(text);
	}
	
	public static float getTextWidth(String text, int fontSize){
		Context context = Application.get();
		Paint paint = new Paint();
		paint.setTextSize(context.getResources().getDimensionPixelSize(fontSize));
		return paint.measureText(text);
	}
	
	public static String makeInnerTextUnbreakable(String text, String innerText, Update update){
		if(text == null || text.isEmpty() || innerText == null || innerText.isEmpty() || !text.contains(innerText)){
			return text;
		}
		try {
			int startIndex = text.indexOf(innerText);
			int endIndex = startIndex + innerText.length();
			
			if(startIndex == 0){
				return text;
			}
			
			String preText = text.substring(0, startIndex);
			String postText = "";
			if(endIndex < text.length()){
				postText = text.substring(endIndex, text.length());
			}
			if(COTextUtils.getTextWidth(preText + innerText) > getUpdateContentWidth(update)){
				return preText + "\n" + innerText + postText;
			}else{
				return text;
			}
			
		} catch (Exception e) {
			Log.e(TAG, "Could NOT make Inner Text Unbreakable");
			return text;
		}
	}
	
	public static int getUpdateContentWidth(Update update){
		Context context = Application.get();
		int screenWidth = ScreenUtils.getScreenWidth()[0];
		int paddings = 2 * context.getResources().getDimensionPixelSize(R.dimen.news_item_layout_padding_left);
		int iconWidth = context.getResources().getDimensionPixelSize(R.dimen.news_item_image_size);
		int offset = 40 ;
		if(update.getQueueType() != QueueTypeEnum.INBOX){
			if(update.isCopyWatchButtons()){
				offset += 2 * context.getResources().getDimensionPixelSize(R.dimen.news_item_buttons_min_width);
			}else if(update.isFollowButton()){
				offset += 30 + context.getResources().getDimensionPixelSize(R.dimen.news_item_buttons_min_width);
			}else if(update.isCoinsButton()){
				offset += context.getResources().getDimensionPixelSize(R.dimen.news_item_buttons_min_width);
			}
		}else{
			offset += 50;
		}
		
		return screenWidth - paddings - iconWidth - offset;
	}
	
}
