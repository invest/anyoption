package com.copyop.android.app.popup;

import com.anyoption.android.app.popup.BalanceDialogfragment;
import com.copyop.android.app.R;

import android.os.Bundle;
import android.util.Log;

public class COBalanceDialogfragment extends BalanceDialogfragment {
	public static final String TAG = COBalanceDialogfragment.class.getSimpleName();

	public static COBalanceDialogfragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BalanceDialogfragment.");
		COBalanceDialogfragment fragment = new COBalanceDialogfragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		return fragment;
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.balance_dialog;
	}
	
}
