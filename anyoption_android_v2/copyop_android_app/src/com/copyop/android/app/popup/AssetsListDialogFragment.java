package com.copyop.android.app.popup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.StatusFragment;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketGroup;
import com.copyop.android.app.R;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class AssetsListDialogFragment extends BaseDialogFragment implements OnChildClickListener,
		OnGroupClickListener, OnClickListener {

	private static final String TAG = AssetsListDialogFragment.class.getSimpleName();

	private static final String SEARCH_GLASS = "\uD83D\uDD0D ";

	public static final String SELECTED_ASSETS_ARG_KEY = "selected_assets_arg_key";
	public static final String BEST_ASSETS_ARG_KEY = "best_assets_arg_key";

	public interface OnAssetsChangeListener {

		/**
		 * Called on dialog dismiss if there is selected assets.
		 */
		public void onAssetsChanged(Set<Long> assetsSet);

	}

	private OnAssetsChangeListener onAssetsChangeListener;

	private ExpandableListView listView;
	private ListViewAdapter listAdapter;
	private TextView titleView;
	private View cancelButton;
	private View doneButton;
	private EditText searchView;

	private Set<Long> checkedItemsSet;
	private List<Long> bestMarkets;

	private List<Market> bestMarketsList;
	private List<Market> copyingMarketsList;
	private List<Market> allMarketsList;

	private int marketsCount;

	private StringBuilder titleStringBuilder;

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static AssetsListDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AssetsListDialogFragment.");
		AssetsListDialogFragment fragment = new AssetsListDialogFragment();
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, 0);
		fragment.setArguments(bundle);

		return fragment;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			checkedItemsSet = (Set<Long>) args.getSerializable(SELECTED_ASSETS_ARG_KEY);
			bestMarkets = (List<Long>) args.getSerializable(BEST_ASSETS_ARG_KEY);
		}

		if (checkedItemsSet == null) {
			checkedItemsSet = new HashSet<Long>();
		}

		setUpMarketGroups();
	}

	private void setUpMarketGroups() {
		// Set up best assets group
		if (bestMarkets != null && !bestMarkets.isEmpty()) {
			bestMarketsList = new ArrayList<Market>();
			for (int i = 0; i < bestMarkets.size(); i++) {
				Market market = application.getMarketForID(bestMarkets.get(i));
				if (market != null) {
					bestMarketsList.add(market);
				}
			}

			sortMarkets(bestMarketsList);
		}

		// Set up copying assets group
		if (!checkedItemsSet.isEmpty()) {
			copyingMarketsList = new ArrayList<Market>();
			ArrayList<Long> copyingMarketsIds = new ArrayList<Long>(checkedItemsSet);

			for (int i = 0; i < copyingMarketsIds.size(); i++) {
				Market market = application.getMarketForID(copyingMarketsIds.get(i));
				if (market != null && !bestMarketsList.contains(market)) {
					copyingMarketsList.add(market);
				}
			}

			sortMarkets(copyingMarketsList);
		}

		if (copyingMarketsList != null && copyingMarketsList.isEmpty()) {
			// We won't need this list.
			copyingMarketsList = null;
		}

		// Set up all markets group
		allMarketsList = new ArrayList<Market>();
		MarketGroup[] marketGroupsArray = application.getMarketGroups();

		for (int i = 0; i < marketGroupsArray.length; i++) {
			MarketGroup marketGroup = marketGroupsArray[i];
			if (marketGroup.getMarkets().get(0).isOptionPlusMarket()) {
				// Option plus markets must not be shown.
				continue;
			}

			allMarketsList.addAll(marketGroup.getMarkets());
		}

		marketsCount = 0;
		if (bestMarketsList != null) {
			marketsCount = marketsCount + bestMarkets.size();
			allMarketsList.removeAll(bestMarketsList);
		}

		if (copyingMarketsList != null) {
			marketsCount = marketsCount + bestMarkets.size();
			allMarketsList.removeAll(copyingMarketsList);
		}

		sortMarkets(allMarketsList);

		marketsCount = marketsCount + allMarketsList.size();
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		setUpStatusBar();

		titleView = (TextView) contentView.findViewById(R.id.assets_list_d_title);
		titleStringBuilder = new StringBuilder();
		updateTitle();

		listView = (ExpandableListView) contentView.findViewById(R.id.assets_list_d_list);
		setUpList();

		cancelButton = contentView.findViewById(R.id.assets_list_d_cancel_button);
		cancelButton.setOnClickListener(this);
		doneButton = contentView.findViewById(R.id.assets_list_d_done_button);
		doneButton.setOnClickListener(this);
		setUpButtonsVisibility();

		searchView = (EditText) contentView.findViewById(R.id.assets_list_d_search);
		setUpSearch();
	}

	@Override
	protected int getContentLayout() {
		return R.layout.assets_list_dialog;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (!checkedItemsSet.isEmpty() && onAssetsChangeListener != null) {
			onAssetsChangeListener.onAssetsChanged(checkedItemsSet);
		} else {
			super.onDismiss(dialog);
		}
	}

	private void setUpList() {
		listAdapter = new ListViewAdapter();
		listView.setAdapter(listAdapter);
		listView.setOnChildClickListener(this);
		listView.setOnGroupClickListener(this);
		expandAll();
	}

	private void setUpStatusBar() {
		Bundle args = new Bundle();
		@SuppressWarnings("unchecked")
		StatusFragment statusFragment = (StatusFragment) BaseFragment.newInstance(
				application.getSubClassForClass(StatusFragment.class), args);
		FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.assets_list_d_status_fr, statusFragment);
		fragmentTransaction.commit();
	}

	private void setUpSearch() {
		searchView.setHint(SEARCH_GLASS + getString(R.string.assetsListDialogSearch));
		searchView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				listAdapter.filter(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// Not needed
			}

			@Override
			public void afterTextChanged(Editable s) {
				// Not needed
			}
		});
	}

	private void setUpButtonsVisibility() {
		if (checkedItemsSet.isEmpty()) {
			doneButton.setVisibility(View.GONE);
			cancelButton.setVisibility(View.VISIBLE);
		} else {
			cancelButton.setVisibility(View.GONE);
			doneButton.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
			int childPosition, long id) {
		long marketId = ((Market) listAdapter.getChild(groupPosition, childPosition)).getId();
		if (checkedItemsSet.contains(marketId)) {
			checkedItemsSet.remove(marketId);
		} else {
			checkedItemsSet.add(marketId);
		}

		// Changes check box state
		CheckBox checkBox = (CheckBox) v.findViewById(R.id.assets_list_d_item_checkbox);
		checkBox.setChecked(!checkBox.isChecked());

		updateTitle();
		setUpButtonsVisibility();

		return true;
	}

	@Override
	public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
		// Keeps all groups open.
		listView.expandGroup(groupPosition);
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == cancelButton.getId()) {
			dismiss();
		} else if (v.getId() == doneButton.getId()) {
			dismiss();
		}
	}

	/**
	 * Method to expand all groups in the ListView.
	 **/
	private void expandAll() {
		int count = listAdapter.getGroupCount();
		for (int i = 0; i < count; i++) {
			listView.expandGroup(i);
		}
	}

	private void updateTitle() {
		titleStringBuilder.setLength(0);
		titleStringBuilder.append(getString(R.string.assetsList));
		titleStringBuilder.append(" (").append(checkedItemsSet.size());
		titleStringBuilder.append("/").append(marketsCount).append(")");
		titleView.setText(titleStringBuilder.toString());
	}

	private static void sortMarkets(List<Market> markets) {
		int n = markets.size();
		Market temp = null;

		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (markets.get(j - 1).getDisplayName().compareTo(markets.get(j).getDisplayName()) > 0) {
					// Swap the elements
					temp = markets.get(j - 1);
					markets.remove(j - 1);
					markets.add(j, temp);
				}
			}
		}
	}

	public OnAssetsChangeListener getOnAssetsChangeListener() {
		return onAssetsChangeListener;
	}

	public void setOnAssetsChangeListener(OnAssetsChangeListener onAssetsChangeListener) {
		this.onAssetsChangeListener = onAssetsChangeListener;
	}

	/**
	 * ADAPTER for the List View
	 */
	private class ListViewAdapter extends BaseExpandableListAdapter {

		private LayoutInflater inflater;

		private List<List<Market>> marketGroups;
		private List<String> marketGroupsNames;
		private List<Long> marketGroupsIds;

		private List<Market> filteredBestMarketsList;
		private List<Market> filteredCopyingMarketsList;
		private List<Market> filteredAllMarketsList;

		private String bestAssetsText;
		private String copyingAssetsText;
		private String allAssetsText;

		private long bestAssetsId;
		private long copyingAssetsId;
		private long allAssetsId;

		public ListViewAdapter() {
			super();

			inflater = (LayoutInflater) application
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (bestMarketsList != null) {
				filteredBestMarketsList = new ArrayList<Market>();
				filteredBestMarketsList.addAll(bestMarketsList);
				bestAssetsText = getString(R.string.copyBestAssets);
				bestAssetsId = bestMarketsList.get(0).getId();
			}

			if (copyingMarketsList != null) {
				filteredCopyingMarketsList = new ArrayList<Market>();
				filteredCopyingMarketsList.addAll(copyingMarketsList);
				copyingAssetsText = getString(R.string.copyCurrentlyCopying);
				copyingAssetsId = copyingMarketsList.get(0).getId();
			}

			if (allMarketsList != null) {
				filteredAllMarketsList = new ArrayList<Market>();
				filteredAllMarketsList.addAll(allMarketsList);
				allAssetsText = getString(R.string.copyAllAssets);
				allAssetsId = allMarketsList.get(0).getId();
			}

			marketGroups = new ArrayList<List<Market>>();
			marketGroupsNames = new ArrayList<String>();
			marketGroupsIds = new ArrayList<Long>();

			setUpFilteredGroups();
		}

		private void setUpFilteredGroups() {
			marketGroups.clear();
			marketGroupsNames.clear();
			marketGroupsIds.clear();

			if (filteredBestMarketsList != null && !filteredBestMarketsList.isEmpty()) {
				marketGroups.add(filteredBestMarketsList);
				marketGroupsNames.add(bestAssetsText);
				marketGroupsIds.add(bestAssetsId);
			}

			if (filteredCopyingMarketsList != null && !filteredCopyingMarketsList.isEmpty()) {
				marketGroups.add(filteredCopyingMarketsList);
				marketGroupsNames.add(copyingAssetsText);
				marketGroupsIds.add(copyingAssetsId);
			}

			if (filteredAllMarketsList != null && !filteredAllMarketsList.isEmpty()) {
				marketGroups.add(filteredAllMarketsList);
				marketGroupsNames.add(allAssetsText);
				marketGroupsIds.add(allAssetsId);
			}
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return marketGroups.get(groupPosition).get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return marketGroups.get(groupPosition).get(childPosition).getId();
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
				View convertView, ViewGroup parent) {
			View childView = convertView;
			if (childView == null) {
				childView = inflater
						.inflate(R.layout.assets_list_dialog_item_layout, parent, false);
			}

			Market market = marketGroups.get(groupPosition).get(childPosition);

			// Set up the Icon:
			ImageView iconImageView = (ImageView) childView
					.findViewById(R.id.assets_list_d_item_icon);
			iconImageView.setImageDrawable(null);
			int resID = DrawableUtils.getMarketIconResID(market);
			Bitmap icon = null;
			BitmapUtils.loadBitmap(resID, icon, iconImageView);

			// Set up the Name:
			TextView nameTextView = (TextView) childView.findViewById(R.id.assets_list_d_item_name);
			nameTextView.setText(market.getDisplayName());

			// Changes check box state
			CheckBox checkBox = (CheckBox) childView.findViewById(R.id.assets_list_d_item_checkbox);
			checkBox.setChecked(checkedItemsSet.contains(market.getId()));

			return childView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return marketGroups.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return marketGroups.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return marketGroups.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return marketGroupsIds.get(groupPosition);
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded, View convertView,
				ViewGroup parent) {
			View groupView = convertView;
			if (groupView == null) {
				groupView = inflater.inflate(R.layout.assets_list_dialog_group_layout, parent,
						false);
			}

			TextView marketGroupTextView = (TextView) groupView
					.findViewById(R.id.assets_list_d_group_view);
			marketGroupTextView.setText(marketGroupsNames.get(groupPosition));

			return groupView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		public void filter(String filter) {
			String filterL;
			if (filter != null) {
				filterL = filter.toLowerCase().trim();
			} else {
				// The markets will be filtered only to remove all option plus markets.
				filterL = "";
			}

			if (bestMarketsList != null) {
				filter(bestMarketsList, filteredBestMarketsList, filterL);
			}

			if (copyingMarketsList != null) {
				filter(copyingMarketsList, filteredCopyingMarketsList, filterL);
			}

			if (allMarketsList != null) {
				filter(allMarketsList, filteredAllMarketsList, filterL);
			}

			setUpFilteredGroups();
			updateList();
		}

		private void filter(List<Market> allMarkets, List<Market> filteredMarkets, String filter) {
			filteredMarkets.clear();

			// Goes through each Item and check the filter
			for (Market market : allMarkets) {
				if (market.getDisplayName().toLowerCase().contains(filter)) {
					filteredMarkets.add(market);
				}
			}
		}

		public void updateList() {
			notifyDataSetChanged();
			expandAll();
		}
	}

}
