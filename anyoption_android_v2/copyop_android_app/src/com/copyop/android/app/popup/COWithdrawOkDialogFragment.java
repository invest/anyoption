package com.copyop.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.WithdrawOkDialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class COWithdrawOkDialogFragment extends WithdrawOkDialogFragment{

	public static final String TAG = COWithdrawOkDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COWithdrawOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COWithdrawOkDialogFragment.");
		COWithdrawOkDialogFragment fragment = new COWithdrawOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		View okButton = contentView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

}
