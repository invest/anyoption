package com.copyop.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.QuestionnaireBlockedOkDialogFragment;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CheckBox;

import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class COQuestionnaireBlockedOkDialogFragment extends QuestionnaireBlockedOkDialogFragment {

	public static final String TAG = COQuestionnaireBlockedOkDialogFragment.class.getSimpleName();
	
	public static COQuestionnaireBlockedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COQuestionnaireRestrictedOkDialogFragment.");
		COQuestionnaireBlockedOkDialogFragment fragment = new COQuestionnaireBlockedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);	
	}
	
}
