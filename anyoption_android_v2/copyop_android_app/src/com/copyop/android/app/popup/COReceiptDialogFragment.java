package com.copyop.android.app.popup;

import java.text.SimpleDateFormat;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.popup.ReceiptDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.InvestmentUtil;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.widget.PutCallView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class COReceiptDialogFragment extends ReceiptDialogFragment {
	private static final String TAG = COReceiptDialogFragment.class.getSimpleName();

	private View bottomLayout;
	private boolean isPut;
	private double winAmount = 0;
	private double loseAmount = 0;


	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static COReceiptDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COReceiptDialogFragment.");
		COReceiptDialogFragment fragment = new COReceiptDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		bottomLayout = contentView.findViewById(R.id.receipt_bottom_layout);

		View dismissButton = contentView.findViewById(R.id.receipt_dismiss_button);
		dismissButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		TextView levelTextView = (TextView) contentView.findViewById(R.id.receipt_level);
		levelTextView.setText(investment.getLevel());

		TextView assetNameTextView = (TextView) contentView.findViewById(R.id.receipt_asset_name);
		assetNameTextView.setText(investment.getAsset());

		TextView expiryTimeTextView = (TextView) contentView.findViewById(R.id.receipt_expiry_time);
		SimpleDateFormat expiryTimeFormat = new SimpleDateFormat(TIME_FORMAT_PATTERN);
		String expriryTimeFormatted = expiryTimeFormat.format(investment.getTimeEstClosing());
		expiryTimeTextView.setText(expriryTimeFormatted);

		Currency currency = user.getCurrency();
		TextView amountTextView = (TextView) contentView.findViewById(R.id.receipt_amount);
		amountTextView.setText(AmountUtil.getFormattedAmount(investment.getAmount(), currency));

		TextView profitLabelTextView = (TextView) contentView
				.findViewById(R.id.receipt_profit_label);
		profitLabelTextView.setText(getString(R.string.receiptProfit) + ":");

		TextView profitTextView = (TextView) contentView.findViewById(R.id.receipt_profit_percent);
		profitTextView.setText(Math.round((investment.getOddsWin() - 1) * 100) + "%");

		PutCallView putCallView = (PutCallView) contentView
				.findViewById(R.id.receipt_put_call_view);

		if (investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL
				|| (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment
						.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_CALL)) {
			isPut = false;
			
			// Call icon
			putCallView.setPut(false);

			winAmount = InvestmentUtil.getInvestmentReturn(investment,
					investment.getCurrentLevel() + 1);
			loseAmount = InvestmentUtil.getInvestmentReturn(investment,
					investment.getCurrentLevel() - 1);
		} else if (investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT
				|| (investment.getTypeId() == Investment.INVESTMENT_TYPE_ONE && investment
						.getOneTouchUpDown() == Investment.INVESTMENT_TYPE_PUT)) {
			isPut = true;
			
			// Put icon
			putCallView.setPut(true);

			winAmount = InvestmentUtil.getInvestmentReturn(investment,
					investment.getCurrentLevel() - 1);
			loseAmount = InvestmentUtil.getInvestmentReturn(investment,
					investment.getCurrentLevel() + 1);
		}

		TextView ifCorrectAmountTextView = (TextView) contentView
				.findViewById(R.id.receipt_if_correct_amount);
		ifCorrectAmountTextView.setText(AmountUtil.getFormattedAmount(winAmount, currency));

		TextView ifIncorrectAmountTextView = (TextView) contentView
				.findViewById(R.id.receipt_if_incorrect_amount);
		ifIncorrectAmountTextView.setText(AmountUtil.getFormattedAmount(loseAmount, currency));

		// Set up asset icon
		Market market = Application.get().getMarketForID(investment.getMarketId());
		if (market != null) {
			int resID = DrawableUtils.getMarketIconResID(market);
			ImageView iconImageView = (ImageView) contentView.findViewById(R.id.receipt_asset_icon);
			BitmapUtils.loadBitmap(resID, iconImageView);
		}

		View shareButton = contentView.findViewById(R.id.receipt_share_button);
		shareButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String type = getString(isPut ? R.string.investments_put : R.string.investments_call);
				COApplication.get().getShareRateManager().receiptShare(type, investment.getAsset(), AmountUtil.getFormattedAmount(winAmount, user.getCurrency()));
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		startLayoutAnimation();
	}

	/**
	 * Show the bottom layout with animation
	 */
	private void startLayoutAnimation() {
		Animation showAnimation = AnimationUtils.loadAnimation(getActivity(),
				R.anim.slide_in_top_bottom);
		bottomLayout.startAnimation(showAnimation);
	}
}