package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.FragmentManager;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.WatchDialogFragment.OnWatchStateChangedListener;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class FullyCopiedDialogFragment extends TextDialogFragment {

	private static final String TAG = FullyCopiedDialogFragment.class.getSimpleName();

	private long profileId;
	private String nickname;
	private boolean isWatching;

	private OnWatchStateChangedListener watchStateChangedListener;

	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static FullyCopiedDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new FullyCopiedDialogFragment.");
		FullyCopiedDialogFragment fragment = new FullyCopiedDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	public static FullyCopiedDialogFragment newInstance(long profileId, String nickname,
			boolean isWatching) {
		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
		Application application = Application.get();
		args.putString(TITLE_ARG_KEY, application.getString(R.string.fullyCopiedDialogTitle));
		args.putString(TEXT_ARG_KEY,
				application.getString(R.string.fullyCopiedDialogText, nickname));
		args.putString(DISMISS_TEXT_ARG_KEY,
				application.getString(R.string.fullyCopiedDialogCancel));
		args.putBoolean(COConstants.EXTRA_IS_WATCHING, isWatching);

		String actionText = null;
		if (isWatching) {
			actionText = application.getString(R.string.fullyCopiedDialogWatching);
		} else {
			actionText = application.getString(R.string.fullyCopiedDialogWatch);
		}
		args.putString(ACTION_TEXT_ARG_KEY, actionText);

		return newInstance(args);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			profileId = args.getLong(COConstants.EXTRA_PROFILE_ID, 0);
			nickname = args.getString(COConstants.EXTRA_NICKNAME, "");
			isWatching = args.getBoolean(COConstants.EXTRA_IS_WATCHING, false);
		}

		if (profileId == 0) {
			Log.e(TAG, "Can not create FullyCopiedDialogFragment without profileId");
			dismiss();
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);

		if (isWatching) {
			enableActionButton(false);
		} else {
			setActionButtonListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showWatchDialog();
					dismiss();
				}
			});
		}
	}

	private void showWatchDialog() {
		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
		args.putString(COConstants.EXTRA_NICKNAME, nickname);
		args.putBoolean(COConstants.EXTRA_IS_WATCHING, false);
		args.putBoolean(COConstants.EXTRA_IS_COPYING, false);

		FragmentManager fragmentManager = application.getFragmentManager();
		WatchDialogFragment dialog = (WatchDialogFragment) fragmentManager.showDialogFragment(
				WatchDialogFragment.class, args);
		if (watchStateChangedListener != null) {
			dialog.setOnWatchStateChangedListener(watchStateChangedListener);
		}
	}

	public OnWatchStateChangedListener getOnWatchStateChangedListener() {
		return watchStateChangedListener;
	}

	public void setOnWatchStateChangedListener(OnWatchStateChangedListener watchStateChangedListener) {
		this.watchStateChangedListener = watchStateChangedListener;
	}

}