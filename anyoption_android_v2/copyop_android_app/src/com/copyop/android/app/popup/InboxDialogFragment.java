package com.copyop.android.app.popup;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.copyop.android.app.widget.inbox.InboxView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class InboxDialogFragment extends BaseDialogFragment{

	private static final String TAG = InboxDialogFragment.class.getSimpleName();
	
	public static InboxDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new InboxDialogFragment.");
		if (null == bundle) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		InboxDialogFragment fragment = new InboxDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			//Get args
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		InboxView inboxView = new InboxView(application);
		//TODO
		return inboxView;
	}

	@Override
	protected int getContentLayout() {
		return 0;
	}
	
}
