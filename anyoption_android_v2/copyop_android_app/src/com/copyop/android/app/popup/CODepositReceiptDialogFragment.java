package com.copyop.android.app.popup;

import com.anyoption.android.app.popup.DepositReceiptDialogFragment;

import android.os.Bundle;

public class CODepositReceiptDialogFragment extends DepositReceiptDialogFragment {

	public static CODepositReceiptDialogFragment newInstance(Bundle bundle) {
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);

		CODepositReceiptDialogFragment fragment = new CODepositReceiptDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected String getIdValue() {
		return String.valueOf(id);
	}
}