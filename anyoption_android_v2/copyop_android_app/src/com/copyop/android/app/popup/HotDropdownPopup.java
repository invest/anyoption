package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;
import com.copyop.android.app.model.HotGroup;
import com.copyop.android.app.util.CODrawableUtils;
import com.copyop.android.app.widget.adapter.HotAdapter.HotHoursFilterListener;
import com.copyop.common.enums.base.UpdateTypeEnum;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class HotDropdownPopup extends PopupWithAnchor implements OnItemClickListener{

	public static void showPopup(HotGroup group, View anchor, HotHoursFilterListener hotHoursFilterListener){
		
		View contentView = View.inflate(Application.get(), R.layout.hot_dropdown_popup, null);
		
		int[] coordinates = new int[2];
		anchor.getLocationOnScreen(coordinates);
		int screenWidth = ScreenUtils.getScreenWidth()[0];
		int anchorRight = coordinates[0];
		int anchorWidth = CODrawableUtils.getDrawableSize(R.drawable.hot_group_dropdown_icon)[0];
		int pointerMarginRight = Application.get().getResources().getDimensionPixelSize(R.dimen.hot_dropdown_pointer_icon_margin_right);
		int pointerWidth = Application.get().getResources().getDimensionPixelSize(R.dimen.hot_dropdown_pointer_icon_width);
		
		int paddingRight = screenWidth - anchorRight - anchorWidth/2 - pointerMarginRight - pointerWidth/2;

		contentView.setPadding(0, 0, paddingRight, 0);
		
		final HotDropdownPopup popup = new HotDropdownPopup(group, anchor, contentView, hotHoursFilterListener);
		contentView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popup.dismiss();
			}
		});
		popup.show();
	}
	
	private HotHoursFilterListener hotHoursFilterListener;
	private HotGroup group;
	
	private HotDropdownPopup(HotGroup group, View anchor, View content, HotHoursFilterListener hotHoursFilterListener) {
		super(anchor, content);
		this.group = group;
		this.hotHoursFilterListener = hotHoursFilterListener;
		ListView listView = (ListView) content.findViewById(R.id.hot_dropdown_list_view);
		listView.setAdapter(new HotDropdownAdapter(group));
		listView.setOnItemClickListener(this);
	}

	public HotHoursFilterListener getHotHoursFilter() {
		return hotHoursFilterListener;
	}

	public void setHotHoursFilter(HotHoursFilterListener hotHoursFilterListener) {
		this.hotHoursFilterListener = hotHoursFilterListener;
	}

	public static class HotDropdownAdapter extends BaseAdapter {

		public final String TAG = HotDropdownAdapter.class.getSimpleName();
		
		public static class ItemViewHolder{
			public int position;
			public TextView textView;
		}
		
		private Context context;
		private HotGroup hotGroup;
		private LayoutInflater inflater;
		private int itemHeight;
		
		public HotDropdownAdapter(HotGroup hotGroup){
			context = Application.get();
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.hotGroup = hotGroup;
			this.itemHeight = (int)context.getResources().getDimension(R.dimen.hot_dropdown_item_height);
		}
		
		@Override
		public int getCount() {
			return hotGroup.getFilters().size();
		}

		@Override
		public Object getItem(int position) {
			return hotGroup.getFilters().get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			UpdateTypeEnum item = hotGroup.getFilters().get(position);
			
			ItemViewHolder itemViewHolder;
			if(convertView == null){
				convertView = inflater.inflate(R.layout.hot_dropdown_item, parent, false);
				itemViewHolder = new ItemViewHolder();
				itemViewHolder.textView = (TextView) convertView.findViewById(R.id.hot_dropdown_item_text_view);
				
				convertView.setTag(itemViewHolder);
			}else{
				itemViewHolder = (ItemViewHolder) convertView.getTag();
			}
			
			itemViewHolder.position = position;
			
			//Hide the current filter from the list:
			android.widget.AbsListView.LayoutParams params = (android.widget.AbsListView.LayoutParams) convertView.getLayoutParams();
			if(hotGroup.getUpdateType() == item){
				params.height = 0;
				hideAllChildren((ViewGroup) convertView);
				convertView.setLayoutParams(params);
				convertView.setVisibility(View.GONE);
			}else{
				showAllChildren((ViewGroup) convertView);
				params.height = itemHeight;
				convertView.setLayoutParams(params);
				convertView.setVisibility(View.VISIBLE);
				try {
					setupItem(hotGroup, item, itemViewHolder);
				} catch (Exception e) {
					Log.e(TAG, "getView", e);
				}
			}

			return convertView;
		}

		private void setupItem(HotGroup hotGroup, UpdateTypeEnum updateTypeEnum, ItemViewHolder itemViewHolder) {
			String itemText = "";
			if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS || updateTypeEnum == UpdateTypeEnum.BEST_TRADES || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS){
				itemText = Application.get().getString(R.string.hotGroupFilterHours, hotGroup.getLastInitHours());
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_WEEK || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_WEEK){
				itemText = Application.get().getString(R.string.hotGroupFilter1Week, 1);
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_ONE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH){
				itemText = Application.get().getString(R.string.hotGroupFilter1Month, 1);
			}else if(updateTypeEnum == UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH || updateTypeEnum == UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH){
				itemText = Application.get().getString(R.string.hotGroupFilter3Months, 3);
			}
			
			itemViewHolder.textView.setText(itemText);
		}

		private void hideAllChildren(ViewGroup viewGroup){
			for(int i = 0; i < viewGroup.getChildCount(); i++){
				View view = viewGroup.getChildAt(i);
				view.setVisibility(View.GONE);
			}
		}
		
		private void showAllChildren(ViewGroup viewGroup){
			for(int i = 0; i < viewGroup.getChildCount(); i++){
				View view = viewGroup.getChildAt(i);
				view.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		UpdateTypeEnum updateTypeEnum = group.getFilters().get(position);
		if(hotHoursFilterListener != null){
			this.hotHoursFilterListener.onFilter(updateTypeEnum);
			dismiss();
		}
	}
	
}
