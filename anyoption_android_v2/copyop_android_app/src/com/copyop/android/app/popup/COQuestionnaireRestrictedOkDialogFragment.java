package com.copyop.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.QuestionnaireRestrictedOkDialogFragment;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CheckBox;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class COQuestionnaireRestrictedOkDialogFragment extends QuestionnaireRestrictedOkDialogFragment {

	public static final String TAG = COQuestionnaireRestrictedOkDialogFragment.class.getSimpleName();

	public static COQuestionnaireRestrictedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COQuestionnaireRestrictedOkDialogFragment.");
		COQuestionnaireRestrictedOkDialogFragment fragment = new COQuestionnaireRestrictedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_COLOR_KEY, R.color.black_alpha_40);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
		isClicked = false;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
	}

}
