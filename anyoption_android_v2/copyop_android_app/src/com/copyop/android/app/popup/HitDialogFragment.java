package com.copyop.android.app.popup;

import java.util.List;

import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.AssetResult;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.TradesGroup;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.util.COTimeUtils;
import com.copyop.android.app.widget.hit.BarChartItemView;
import com.copyop.android.app.widget.hit.CircleChartView;
import com.copyop.json.requests.ProfileMethodRequest;
import com.copyop.json.results.HitsMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class HitDialogFragment extends BaseDialogFragment {

	private static final String TAG = HitDialogFragment.class.getSimpleName();
	
	public static HitDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new HitDialogFragment.");
		HitDialogFragment fragment = new HitDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	private TextView headerTextView;
	private TextView subHeaderTextView;
	
	private String hitPercentFormatted;
	private int tradesCount;
	private String lastTradeDateFormatted;
	private List<TradesGroup> tradesGroupList;
	private List<AssetResult> marketResultList;
	
	private boolean isResponseDone;
	private BarChartItemView barChart1;
	private BarChartItemView barChart2;
	private BarChartItemView barChart3;
	private BarChartItemView barChart4;
	private View circleChartItemLayout1;
	private View circleChartItemLayout2;
	private View circleChartItemLayout3;
	private View circleChartItemLayout4;
	private TextView circleChartItemPercentTextView_1;
	private TextView circleChartItemPercentTextView_2;
	private TextView circleChartItemPercentTextView_3;
	private TextView circleChartItemPercentTextView_4;
	private TextView circleChartItemMarketTextView_1;
	private TextView circleChartItemMarketTextView_2;
	private TextView circleChartItemMarketTextView_3;
	private TextView circleChartItemMarketTextView_4;
	private CircleChartView circleChartView;
	private long userId;
	
	
	@Override
	protected int getContentLayout() {
		return R.layout.hit_dialog_layout;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			userId = bundle.getLong(COConstants.EXTRA_PROFILE_ID, 0); 
		}
		getHits();
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		contentView.setOnClickListener(null);
		headerTextView = (TextView) contentView.findViewById(R.id.hit_dialog_header_text_view);
		subHeaderTextView = (TextView) contentView.findViewById(R.id.hit_dialog_sub_header_text_view);
		barChart1 = (BarChartItemView) contentView.findViewById(R.id.hit_dialog_bar_chart_1);
		barChart2 = (BarChartItemView) contentView.findViewById(R.id.hit_dialog_bar_chart_2);
		barChart3 = (BarChartItemView) contentView.findViewById(R.id.hit_dialog_bar_chart_3);
		barChart4 = (BarChartItemView) contentView.findViewById(R.id.hit_dialog_bar_chart_4);
		circleChartItemLayout1 = contentView.findViewById(R.id.hit_dialog_circle_chart_item_layout_1);
		circleChartItemLayout2 = contentView.findViewById(R.id.hit_dialog_circle_chart_item_layout_2);
		circleChartItemLayout3 = contentView.findViewById(R.id.hit_dialog_circle_chart_item_layout_3);
		circleChartItemLayout4 = contentView.findViewById(R.id.hit_dialog_circle_chart_item_layout_4);
		circleChartItemPercentTextView_1 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_1_percent_text_view);
		circleChartItemPercentTextView_2 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_2_percent_text_view);
		circleChartItemPercentTextView_3 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_3_percent_text_view);
		circleChartItemPercentTextView_4 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_4_percent_text_view);
		circleChartItemMarketTextView_1 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_1_market_text_view);
		circleChartItemMarketTextView_2 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_2_market_text_view);
		circleChartItemMarketTextView_3 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_3_market_text_view);
		circleChartItemMarketTextView_4 = (TextView) contentView.findViewById(R.id.hit_dialog_circle_chart_item_4_market_text_view);
		circleChartView = (CircleChartView) contentView.findViewById(R.id.hit_dialog_circle_chart_view);
		
		View closeButton = contentView.findViewById(R.id.hit_dialog_close_button);
		closeButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		if(isResponseDone){
			setupUI();
		}
	}
	
	private void setupUI() {
		//Set up the HEADER:
		headerTextView.setText(getString(R.string.hitDialogHeader, hitPercentFormatted));
		TextUtils.decorateInnerText(headerTextView, null, headerTextView.getText().toString(), hitPercentFormatted, Font.ROBOTO_MEDIUM, 0, 0);
		///////////////////
		
		//Set up the Sub Header:
		String subHeader1 = getString(R.string.hitDialogSubHeader1, tradesCount);
		String subHeader2 = getString(R.string.hitDialogSubHeader2, lastTradeDateFormatted);
		subHeaderTextView.setText(subHeader1 + "\n" + subHeader2);
		TextUtils.decorateInnerText(subHeaderTextView, null, subHeaderTextView.getText().toString(), subHeader2, Font.ROBOTO_LIGHT, 0, 0);
		//////////////////////
	
		//Set up the Bar Charts:
		if(tradesGroupList != null){
			try {
				TradesGroup group1 = tradesGroupList.get(0);
				barChart1.setValue(group1.getHitRate());
				barChart1.setRange(group1.getStartIndex(), group1.getEndIndex());
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such TradesGroup");
				barChart1.setVisibility(View.GONE);
			}
			try {
				TradesGroup group2 = tradesGroupList.get(1);
				barChart2.setValue(group2.getHitRate());
				barChart2.setRange(group2.getStartIndex(), group2.getEndIndex());
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such TradesGroup");
				barChart2.setVisibility(View.GONE);
			}
			try {
				TradesGroup group3 = tradesGroupList.get(2);
				barChart3.setValue(group3.getHitRate());
				barChart3.setRange(group3.getStartIndex(), group3.getEndIndex());
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such TradesGroup");
				barChart3.setVisibility(View.GONE);
			}
			try {
				TradesGroup group4 = tradesGroupList.get(3);
				barChart4.setValue(group4.getHitRate());
				barChart4.setRange(group4.getStartIndex(), group4.getEndIndex());
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such TradesGroup");
				barChart4.setVisibility(View.GONE);
			}
		}
		///////////////////////
		
		//Set up the Circle Chart:
		int rate1 = 0;
		int rate2 = 0;
		int rate3 = 0;
		int rate4 = 0;
		
		if(marketResultList != null){
			
			try {
				AssetResult group = marketResultList.get(0);
				rate1 = group.getHitRate();
				String hitrateFormatted = rate1 + "% |";
				circleChartItemPercentTextView_1.setText(hitrateFormatted);
				if(group.getMarketId() > 0){
					Market market = application.getMarketForID(group.getMarketId());
					if(market != null){
						circleChartItemMarketTextView_1.setText(market.getDisplayName());
					}else{
						Log.e(TAG, "setupUI -> CANNOT find Market for id.");
					}
				}else{
					//Set the "Other" label:
					circleChartItemMarketTextView_1.setText(getString(R.string.hitDialogCircleChartItemOther));
				}
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such AssetResult");
				//Hide this label:
				circleChartItemLayout1.setVisibility(View.GONE);
			}
			try {
				AssetResult group = marketResultList.get(1);
				rate2 = group.getHitRate();
				String hitrateFormatted = rate2 + "% |";
				circleChartItemPercentTextView_2.setText(hitrateFormatted);
				if(group.getMarketId() > 0){
					Market market = application.getMarketForID(group.getMarketId());
					if(market != null){
						circleChartItemMarketTextView_2.setText(market.getDisplayName());
					}else{
						Log.e(TAG, "setupUI -> CANNOT find Market for id.");
					}
				}else{
					//Set the "Other" label:
					circleChartItemMarketTextView_2.setText(getString(R.string.hitDialogCircleChartItemOther));
				}
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such AssetResult");
				//Hide this label:
				circleChartItemLayout2.setVisibility(View.GONE);
			}
			try {
				AssetResult group = marketResultList.get(2);
				rate3 = group.getHitRate();
				String hitrateFormatted = rate3 + "% |";
				circleChartItemPercentTextView_3.setText(hitrateFormatted);
				if(group.getMarketId() > 0){
					Market market = application.getMarketForID(group.getMarketId());
					if(market != null){
						circleChartItemMarketTextView_3.setText(market.getDisplayName());
					}else{
						Log.e(TAG, "setupUI -> CANNOT find Market for id.");
					}
				}else{
					//Set the "Other" label:
					circleChartItemMarketTextView_3.setText(getString(R.string.hitDialogCircleChartItemOther));
				}
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such AssetResult");
				//Hide this label:
				circleChartItemLayout3.setVisibility(View.GONE);
			}
			try {
				AssetResult group = marketResultList.get(3);
				rate4 = group.getHitRate();
				String hitrateFormatted = rate4 + "% |";
				circleChartItemPercentTextView_4.setText(hitrateFormatted);
				circleChartItemMarketTextView_4.setText(getString(R.string.hitDialogCircleChartItemOther));
			} catch (IndexOutOfBoundsException e) {
				Log.e(TAG, "setupUI -> No such AssetResult");
				//Hide this label:
				circleChartItemLayout4.setVisibility(View.GONE);
			}
		}
		
		circleChartView.setValues(rate1, rate2, rate3, rate4);
		/////////////////////////
		
		
		//Start the Animations
		barChart1.startAnimation();
		barChart2.startAnimation();
		barChart3.startAnimation();
		barChart4.startAnimation();
		
		circleChartView.startAnimation();
	}
	
	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	
	protected void getHits(){
		ProfileMethodRequest request = new ProfileMethodRequest();
		request.setRequestedUserId(userId);
		application.getCommunicationManager().requestService(this, "gotHits", COConstants.SERVICE_GET_COPYOP_HITS,  request, HitsMethodResult.class, false, false);
	}
	
	public void gotHits(Object result){
		if(result != null && result instanceof HitsMethodResult && ((HitsMethodResult) result).getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			Log.d(TAG, COConstants.SERVICE_GET_COPYOP_HITS + " callback -> Result is OK.");
			HitsMethodResult hitsMethodResult = (HitsMethodResult) result;
			
			hitPercentFormatted = hitsMethodResult.getHitRate() + "%";
			tradesCount = hitsMethodResult.getInvestmentsCount();
			lastTradeDateFormatted = COTimeUtils.formatHitLastTradeDate(hitsMethodResult.getLastTradeTime());
			tradesGroupList = hitsMethodResult.getTradesGroups();
			marketResultList = hitsMethodResult.getAssetResults();
			
			isResponseDone = true;
			if(this.headerTextView != null){
				setupUI();
			}
		}else{
			Log.e(TAG, COConstants.SERVICE_GET_COPYOP_HITS + " callback -> Could NOT parse result.");
		}
	}
	
	//#####################################################################
	
}
