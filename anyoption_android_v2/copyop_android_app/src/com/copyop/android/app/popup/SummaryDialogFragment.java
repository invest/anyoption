package com.copyop.android.app.popup;

import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.model.TopLogTrader;
import com.copyop.android.app.util.COAmountUtil;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.android.app.widget.COButton;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

/**
 * POPUP that displays a summary of the last activities.
 * @author Anastas Arnaudov
 *
 */
public class SummaryDialogFragment extends BaseDialogFragment implements OnClickListener{

	private static final String TAG = SummaryDialogFragment.class.getSimpleName();

	public static final long TOTAL_ID  	= 0;
	public static final long OTHERS_ID 	= 1;
	
	/**
	 * Use this method to create new instance of the dialog.
	 * 
	 * @return
	 */
	public static SummaryDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new CopyDialogFragment.");
		SummaryDialogFragment fragment = new SummaryDialogFragment();
		fragment.setArguments(bundle);

		return fragment;
	}

	private List<TopLogTrader> topLogTradersList;

	@Override
	protected int getContentLayout() {
		return R.layout.summary_popup_layout;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null && args.containsKey(COConstants.EXTRA_TOP_TRADERS)) {
			//Get parameters:
			topLogTradersList = (List<TopLogTrader>) args.getSerializable(COConstants.EXTRA_TOP_TRADERS);
		}else{
			dismiss();
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		contentView.setOnClickListener(null);
		TextView headerTextView = (TextView) contentView.findViewById(R.id.summary_dialog_header_text_view);
		
		ImageView profitTotalImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_total_profit_icon);
		TextView amountTotalTextView = (TextView) contentView.findViewById(R.id.summary_dialog_total_amount_text_view);
		ImageView winLoseTotalImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_total_win_lose_icon);
		TextView winLoseTotalTextView = (TextView) contentView.findViewById(R.id.summary_dialog_total_win_lose_label_text_view);
		
		ImageView avatarItem1ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_1_avatar_icon);
		TextView nicknameItem1TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_1_nickname_text_view);
		TextView copiedOptionsItem1TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_1_copied_options_text_view);
		COButton increaseAmountItem1Button = (COButton) contentView.findViewById(R.id.summary_dialog_item_1_increase_amount_button);
		TextView amountItem1TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_1_amount_text_view);
		ImageView winLoseItem1ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_1_win_lose_icon);
		TextView winLoseItem1TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_1_win_lose_label_text_view);

		View item2Layout = contentView.findViewById(R.id.summary_dialog_item_2_layout);
		View item2Separator = contentView.findViewById(R.id.summary_dialog_item_2_separator);
		ImageView avatarItem2ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_2_avatar_icon);
		TextView nicknameItem2TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_2_nickname_text_view);
		TextView copiedOptionsItem2TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_2_copied_options_text_view);
		COButton increaseAmountItem2Button = (COButton) contentView.findViewById(R.id.summary_dialog_item_2_increase_amount_button);
		TextView amountItem2TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_2_amount_text_view);
		ImageView winLoseItem2ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_2_win_lose_icon);
		TextView winLoseItem2TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_2_win_lose_label_text_view);

		View item3Layout = contentView.findViewById(R.id.summary_dialog_item_3_layout);
		View item3Separator = contentView.findViewById(R.id.summary_dialog_item_3_separator);
		ImageView avatarItem3ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_3_avatar_icon);
		TextView nicknameItem3TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_3_nickname_text_view);
		TextView copiedOptionsItem3TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_3_copied_options_text_view);
		COButton increaseAmountItem3Button = (COButton) contentView.findViewById(R.id.summary_dialog_item_3_increase_amount_button);
		TextView amountItem3TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_3_amount_text_view);
		ImageView winLoseItem3ImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_3_win_lose_icon);
		TextView winLoseItem3TextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_3_win_lose_label_text_view);

		View itemOthersLayout = contentView.findViewById(R.id.summary_dialog_item_others_layout);
		View itemOthersSeparator = contentView.findViewById(R.id.summary_dialog_item_others_separator);
		ImageView profitOthersImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_others_profit_icon);
		TextView copiedOptionsItemOthersTextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_others_copied_options_text_view);
		TextView winLoseItemOthersTextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_others_win_lose_label_text_view);
		TextView amountItemOthersTextView = (TextView) contentView.findViewById(R.id.summary_dialog_item_others_amount_text_view);
		ImageView winLoseItemOthersImageView = (ImageView) contentView.findViewById(R.id.summary_dialog_item_others_win_lose_icon);
		
		//TOTAL:	
		try {
			TopLogTrader topLogTrader = topLogTradersList.get(0);
			//HEADER:
			setupHeader(headerTextView, topLogTrader.isAfterLastLogOff());
			//////////////////
			
			setupProfitIcon(profitTotalImageView, topLogTrader.getProfit());
			setupAmount(amountTotalTextView, topLogTrader.getProfit());
			setupWinLose(winLoseTotalImageView, winLoseTotalTextView, topLogTrader.getProfit());
		} catch (Exception e) {}
		////////////////////////	
			
		//TRADER 1:	
		try {
			TopLogTrader topLogTrader = topLogTradersList.get(1);
			setupAvatar(avatarItem1ImageView, topLogTrader);
			setupNickname(nicknameItem1TextView, topLogTrader);
			setupCopiedOptions(copiedOptionsItem1TextView, topLogTrader.getInvestmentsCount());
			setupIncreaseAmountButton(increaseAmountItem1Button, topLogTrader);
			setupAmount(amountItem1TextView, topLogTrader.getProfit());
			setupWinLose(winLoseItem1ImageView, winLoseItem1TextView, topLogTrader.getProfit());
		} catch (Exception e) {}
		////////////////////////
		
		//TRADER 2:	
		try {
			TopLogTrader topLogTrader = topLogTradersList.get(2);
			setupAvatar(avatarItem2ImageView, topLogTrader);
			setupNickname(nicknameItem2TextView, topLogTrader);
			setupCopiedOptions(copiedOptionsItem2TextView, topLogTrader.getInvestmentsCount());
			setupIncreaseAmountButton(increaseAmountItem2Button, topLogTrader);
			setupAmount(amountItem2TextView, topLogTrader.getProfit());
			setupWinLose(winLoseItem2ImageView, winLoseItem2TextView, topLogTrader.getProfit());
		}catch (IndexOutOfBoundsException e) {
			item2Layout.setVisibility(View.GONE);
			item2Separator.setVisibility(View.GONE);
			item3Layout.setVisibility(View.GONE);
			item3Separator.setVisibility(View.GONE);
			itemOthersLayout.setVisibility(View.GONE);
			itemOthersSeparator.setVisibility(View.GONE);
		}
		////////////////////////
		
		//TRADER 3:	
		try {
			TopLogTrader topLogTrader = topLogTradersList.get(3);
			setupAvatar(avatarItem3ImageView, topLogTrader);
			setupNickname(nicknameItem3TextView, topLogTrader);
			setupCopiedOptions(copiedOptionsItem3TextView, topLogTrader.getInvestmentsCount());
			setupIncreaseAmountButton(increaseAmountItem3Button, topLogTrader);
			setupAmount(amountItem3TextView, topLogTrader.getProfit());
			setupWinLose(winLoseItem3ImageView, winLoseItem3TextView, topLogTrader.getProfit());
		} catch (IndexOutOfBoundsException ie) {
			item3Layout.setVisibility(View.GONE);
			item3Separator.setVisibility(View.GONE);
			itemOthersLayout.setVisibility(View.GONE);
			itemOthersSeparator.setVisibility(View.GONE);
		}
		////////////////////////
		
		//OTHERS:	
		try {
			TopLogTrader topLogTrader = topLogTradersList.get(4);
			setupProfitIcon(profitOthersImageView, topLogTrader.getProfit());
			setupCopiedOptions(copiedOptionsItemOthersTextView, topLogTrader.getInvestmentsCount());
			setupAmount(amountItemOthersTextView, topLogTrader.getProfit());
			setupWinLose(winLoseItemOthersImageView, winLoseItemOthersTextView, topLogTrader.getProfit());
		} catch (IndexOutOfBoundsException e) {
			itemOthersLayout.setVisibility(View.GONE);
			itemOthersSeparator.setVisibility(View.GONE);
		}
		////////////////////////
	}
	
	private void setupAvatar(ImageView imageView, TopLogTrader topLogTrader){
		imageView.setTag(topLogTrader);
		imageView.setOnClickListener(this);
		COBitmapUtils.loadBitmap(topLogTrader.getAvatar(), imageView);
	}
	
	private void setupNickname(TextView textView, TopLogTrader topLogTrader){
		textView.setText(topLogTrader.getNickname());
		textView.setTag(topLogTrader);
		textView.setOnClickListener(this);
	}
	
	private void setupCopiedOptions(TextView textView, int copiedOptions){
		textView.setText(getString(R.string.summaryDialogCopiedOptions, copiedOptions));
	}
	
	private void setupIncreaseAmountButton(View button, TopLogTrader topLogTrader){
		if(topLogTrader.isShowIncreaseAmount()){
			button.setVisibility(View.VISIBLE);
			button.setTag(topLogTrader);
			button.setOnClickListener(this);
		}else{
			button.setVisibility(View.GONE);
		}
	}
	
	private void setupHeader(TextView headerTextView, boolean isLastLogin){
		if(isLastLogin){
			headerTextView.setText(getString(R.string.summaryDialogHeader, getString(R.string.summaryDialogPeriodLastLogin)));
		}else{
			headerTextView.setText(getString(R.string.summaryDialogHeader, getString(R.string.summaryDialogPeriod24Hours)));
		}
	}

	private void setupAmount(TextView textView, double profit){
		textView.setText(COAmountUtil.formatSummaryWinLossProfit(profit));
		
	}

	private void setupWinLose(ImageView imageView, TextView label, double profit){
		if(profit >= 0){
			imageView.setImageDrawable(getResources().getDrawable(R.drawable.summary_dialog_item_win_icon));	
			label.setText(getString(R.string.summaryDialogProfit));
		}else{
			imageView.setImageDrawable(getResources().getDrawable(R.drawable.summary_dialog_item_lose_icon));	
			label.setText(getString(R.string.summaryDialogLoss));
		}
	}
	
	private void setupProfitIcon(ImageView icon, double profit){
		if(profit > 0){
			icon.setImageDrawable(getResources().getDrawable(R.drawable.summary_dialog_item_profit_icon_win));
		}else if(profit < 0){
			icon.setImageDrawable(getResources().getDrawable(R.drawable.summary_dialog_item_profit_icon_loss));
		}else{
			icon.setImageDrawable(getResources().getDrawable(R.drawable.summary_dialog_item_profit_icon_not_invetsed));
		}
	}

	@Override
	public void onClick(View view) {
		if(view.getTag() instanceof TopLogTrader){
			TopLogTrader topLogTrader = (TopLogTrader) view.getTag();
			if(view instanceof ImageView || view instanceof TextView){
				goToProfile(topLogTrader.getUserId());
				dismiss();
			}else if(view instanceof COButton){
				openCopyDialog(topLogTrader.getUserId(), topLogTrader.getNickname(), true);
			}
			
		}
	}
	
	protected void goToProfile(long userId){
		if(userId > 0){
			Bundle bundle = new Bundle();
			bundle.putLong(COConstants.EXTRA_PROFILE_ID, userId);
			Application.get().postEvent(new NavigationEvent(COScreen.USER_PROFILE, NavigationType.DEEP, bundle));
		}else{
			Log.e(TAG, "User ID is null");
		}
	}
	
	protected void openCopyDialog(long userId, String nickname, boolean isCopying){
		Bundle bundle = new Bundle();
		bundle.putBoolean(COConstants.EXTRA_IS_COPYING, isCopying);
		bundle.putLong(COConstants.EXTRA_PROFILE_ID, userId);
		bundle.putString(COConstants.EXTRA_NICKNAME, nickname);
		CopyDialogFragment copyDialogFragment = CopyDialogFragment.newInstance(bundle);
		Application.get().getFragmentManager().showDialogFragment(copyDialogFragment);
	}
	
	
}