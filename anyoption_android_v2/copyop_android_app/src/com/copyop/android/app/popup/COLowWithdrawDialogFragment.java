package com.copyop.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class COLowWithdrawDialogFragment extends LowWithdrawDialogFragment implements OnClickListener {

	public static final String TAG = COLowWithdrawDialogFragment.class.getSimpleName();
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static COLowWithdrawDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new COLowWithdrawDialogFragment.");
		COLowWithdrawDialogFragment fragment = new COLowWithdrawDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, true);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

}
