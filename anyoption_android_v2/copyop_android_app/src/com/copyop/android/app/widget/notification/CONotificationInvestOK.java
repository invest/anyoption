package com.copyop.android.app.widget.notification;

import com.anyoption.common.beans.base.Investment;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class CONotificationInvestOK extends com.anyoption.android.app.widget.notification.NotificationInvestOK{

	public CONotificationInvestOK(Investment investment) {
		super(investment);
	}
	public CONotificationInvestOK(Context context) {
		super(context);
	}
	public CONotificationInvestOK(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public CONotificationInvestOK(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void setupText(){
		TextView levelTextView = (TextView) view.findViewById(R.id.notification_invest_ok_level_text_view);
		
		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL){
			levelTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.notification_invest_call, 0, 0, 0);
		}else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT){
			levelTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.notification_invest_put, 0, 0, 0);
		}
		levelTextView.setText(investment.getLevel());
	}
}
