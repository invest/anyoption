package com.copyop.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.event.InboxNewUpdateEvent;
import com.copyop.android.app.event.InboxReadingEvent;
import com.copyop.android.app.popup.InboxPopup;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class InboxButton extends RelativeLayout{

	private boolean isReadingInbox;
	private TextView indicatorTextView;
	
	public InboxButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	public InboxButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public InboxButton(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void registerForEvents(){
		Application.get().registerForEvents(this, InboxNewUpdateEvent.class, InboxReadingEvent.class);
	}
	
	private void unregisterForEvents(){
		Application.get().unregisterForAllEvents(this);
	}
	
	@Override
	protected void onAttachedToWindow() {
		setupIndicator();
		registerForEvents();
		super.onAttachedToWindow();
	}
	
	@Override
	protected void onDetachedFromWindow() {
		unregisterForEvents();
		super.onDetachedFromWindow();
	}
	
	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if(changedView == this){
			if(visibility == View.VISIBLE){
				registerForEvents();
			}else{
				unregisterForEvents();
			}
			setupIndicator();
		}
	}
	
	private void init(Context context, AttributeSet attrs, int defStyle) {
		View rootView = View.inflate(context, R.layout.inbox_notifications_button_layout, this);
		indicatorTextView = (TextView) rootView.findViewById(R.id.inbox_notifications_button_indicator_text);
		
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				InboxPopup.showInboxPopup(InboxButton.this);
			}
		};
		
		setOnClickListener(onClickListener);
		rootView.setOnClickListener(onClickListener);
	}

	private void setupIndicator(){
		int unreadMessages = COApplication.get().getInboxManager().getUnreadMessages();
		if(unreadMessages > 0){
			indicatorTextView.setVisibility(View.VISIBLE);
			indicatorTextView.setText(String.valueOf(unreadMessages));
		}else{
			indicatorTextView.setVisibility(View.INVISIBLE);
		}
	}
	
	//#####################################################################
	//#########					EVENTs CALLBACKs				###########
	//#####################################################################
	
	public void onEventMainThread(InboxNewUpdateEvent event){
		if(!isReadingInbox){
			setupIndicator();
		}
	}
	
	public void onEventMainThread(InboxReadingEvent event){
		isReadingInbox = event.isReadingInbox();
		setupIndicator();
	}
	
	//#####################################################################
	
}
