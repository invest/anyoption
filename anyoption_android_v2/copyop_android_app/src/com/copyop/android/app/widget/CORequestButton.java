package com.copyop.android.app.widget;

import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.RequestButton;
import com.copyop.android.app.R;
import com.copyop.android.app.util.CODrawableUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

/**
 * Custom Button that has a special pressed effect.
 * @author Anastas Arnaudov
 *
 */
public class CORequestButton extends RequestButton implements View.OnTouchListener{


	private int color;
	private int shadowColor;
	private int shadowSize;
	private boolean isBorder;
	private int paddingLeft;
	private int paddingTop;
	private int paddingRight;
	private int paddingBottom;
	private Drawable normalBackground;
	private Drawable pressedBackground;
	private Drawable loadingBackground;

	private boolean isEnabled;
	private int disabledColor;
	private int disabledColorShadow;

	public CORequestButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public CORequestButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	
	@SuppressLint("ClickableViewAccessibility")
	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		
		setupAttributes(context, attrs, defStyleAttr);

		if (!isInEditMode()) {
			isEnabled = true;
			disabledColor = getResources().getColor(R.color.button_disabled_bg);
			disabledColorShadow = getResources().getColor(R.color.button_disabled_shadow);

			textView.setFont(Font.ROBOTO_REGULAR);
			if(textSize == 0){
				//Default text size:
				textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.button_text));				
			}

			normalBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, shadowSize, isBorder, false);
			pressedBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, shadowSize, isBorder, true);
			int loadingColor = getResources().getColor(R.color.button_grey_bg);
			int loadingShadowColor = getResources().getColor(R.color.button_grey_shadow);
			loadingBackground = CODrawableUtils.createClickableDrawable(context, loadingColor, loadingShadowColor, shadowSize, isBorder, true);
		}else{
			setBackgroundColor(color);
		}
		
		this.setOnTouchListener(this);
	}

	private void setupAttributes(Context context, AttributeSet attrs, int defStyleAttr){
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.COButton);
		color = attributesTypedArray.getColor(R.styleable.COButton_color, getResources().getColor(R.color.transparent));
		shadowColor = attributesTypedArray.getColor(R.styleable.COButton_shadowColor, getResources().getColor(R.color.transparent));
		shadowSize = (int)attributesTypedArray.getDimension(R.styleable.COButton_shadowSize, getResources().getDimension(R.dimen.button_shadow_width));
		isBorder = attributesTypedArray.getBoolean(R.styleable.COButton_isBorder, true);
		attributesTypedArray.recycle();
		//////////////////////////	
		//Get paddingLeft, paddingRight
        int[] attrsArray = new int[]{
                android.R.attr.paddingLeft,  // 0
                android.R.attr.paddingRight, // 1
        };
        TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
        if (ta == null) return;
        paddingLeft = ta.getDimensionPixelSize(0, 0);
        paddingRight = ta.getDimensionPixelSize(1, 0);
        ta.recycle();

        //Get paddingTop, paddingBottom
        int[] attrsArray2 = new int[]{
                android.R.attr.paddingTop,   // 0
                android.R.attr.paddingBottom,// 1
        };
        TypedArray ta1 = context.obtainStyledAttributes(attrs, attrsArray2);
        if (ta1 == null) return;
        paddingTop = ta1.getDimensionPixelSize(0, 0);
        paddingBottom = ta1.getDimensionPixelSize(1, 0);
        ta1.recycle();
		////////////////////////////////////////
	}
	
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(!isInEditMode()){
        	updateBackground(normalBackground);
        }else{
        	setBackgroundColor(color);
        }
        //Set padding
        this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
    }
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
    public boolean onTouch(View view, MotionEvent event) {
		
		switch (event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	            updateBackground(pressedBackground);
	            this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom);
	            break;
	        case MotionEvent.ACTION_MOVE:
	            Rect r = new Rect();
	            view.getLocalVisibleRect(r);
	            if (!r.contains((int) event.getX(), (int) event.getY() + 3 * shadowSize) &&
	                !r.contains((int) event.getX(), (int) event.getY() - 3 * shadowSize)) {
	            	updateBackground(normalBackground);
	            	this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
	            }
	            break;
	        case MotionEvent.ACTION_OUTSIDE:
	        case MotionEvent.ACTION_CANCEL:
	        case MotionEvent.ACTION_UP:
	        	updateBackground(normalBackground);
	        	this.setPadding(paddingLeft, paddingTop + shadowSize, paddingRight, paddingBottom + shadowSize);
	            break;
		}
		return false;
	}

	@Override
	protected void setupLoadingBG() {
		updateBackground(loadingBackground);
	}
	
	@Override
	protected void setupNormalBG() {
		updateBackground(normalBackground);
		setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom + shadowSize);
	}

	private void updateBackground(Drawable drawable){
		DrawableUtils.setBackground(this, drawable);
	}

	public void setStyle(int colorRes, int shadowColorRes) {
		this.color = getResources().getColor(colorRes);
		this.shadowColor = getResources().getColor(shadowColorRes);
		normalBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, false);
		pressedBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, true);
		updateBackground(normalBackground);
	}
	
	/**
	 * Use to enable/disable the button changing its style.
	 * 
	 * @param enabled
	 */
	public void enableButton(boolean enabled) {
		isEnabled = enabled;
		refresh();
	}

	protected void refresh() {
		setEnabled(isEnabled);
		if (isEnabled) {
			normalBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, false);
			pressedBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, true);
		} else {
			normalBackground = CODrawableUtils.createClickableDrawable(getContext(), disabledColor, disabledColorShadow, shadowSize, isBorder, false);
			pressedBackground = CODrawableUtils.createClickableDrawable(getContext(), disabledColor, disabledColorShadow, shadowSize, isBorder, true);
		}

		updateBackground(normalBackground);
	}

}
