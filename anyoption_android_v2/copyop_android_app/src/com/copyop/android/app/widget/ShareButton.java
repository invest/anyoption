package com.copyop.android.app.widget;

import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class ShareButton extends RelativeLayout{

	public ShareButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	public ShareButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public ShareButton(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(final Context context, AttributeSet attrs, int defStyle) {
		View rootView = View.inflate(context, R.layout.share_button_layout, this);
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				COApplication.get().getShareRateManager().homeScreenShare();
			}
		};
		setOnClickListener(onClickListener);
		rootView.setOnClickListener(onClickListener);
	}
}