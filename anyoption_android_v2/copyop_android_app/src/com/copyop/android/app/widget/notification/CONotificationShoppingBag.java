package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.widget.notification.NotificationShoppingBag;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CONotificationShoppingBag extends NotificationShoppingBag{
	
	public CONotificationShoppingBag(int expiredOptionsCount, String marketName, long profitAmount, boolean isWinning) {
		super(expiredOptionsCount, marketName, profitAmount, isWinning);
	}
	public CONotificationShoppingBag(Context context) {
		super(context);
	}
	public CONotificationShoppingBag(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public CONotificationShoppingBag(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
	}
	protected void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_expiry, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		messageTextView.setText(setupMessage());
		
		Button showOffButton = (Button) view.findViewById(R.id.notification_show_off_button);
		if(isWinning){
			showOffButton.setVisibility(View.VISIBLE);
			if(COApplication.get().getProfile().getRate() > 0){
				//The user has rated the application:
				showOffButton.setText(Application.get().getString(R.string.notificationShowOffButton));
			}else{
				//The user has NOT rate the application:
				showOffButton.setText(Application.get().getString(R.string.notificationRateButton));
			}
			showOffButton.setOnClickListener(new OnClickListener() {
				private boolean isPressed = false;
				@Override
				public void onClick(View v) {
					if(!isPressed){
						isPressed = true;
						if(COApplication.get().getProfile().getRate() > 0){
							//The user has rated the application:
							goToShare();
						}else{
							//The user has NOT rate the application:
							goToRate();
						}
					}
				}
			});
		}else{
			showOffButton.setVisibility(View.GONE);
		}
	}
	
	private void goToShare(){
		COApplication.get().getShareRateManager().homeScreenShare();
	}

	private void goToRate(){
		COApplication.get().getShareRateManager().rate();
	}
	
}
