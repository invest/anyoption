package com.copyop.android.app.widget;

import com.copyop.android.app.R;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.AttributeSet;

/**
 * Custom refreshing layout. It can ONLY have 1 child.
 * @author anastasa
 *
 */
public class RefreshingView extends SwipeRefreshLayout implements OnRefreshListener {

	private boolean isColorsSet = false;
	
	public RefreshingView(Context context) {
		super(context);
		init(context, null);
	}
	
	public RefreshingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		if(!isInEditMode()){
			setOnRefreshListener(this);
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		if(!isColorsSet){
			isColorsSet = true;
			setColorSchemeResources(
					R.color.green_6,
					R.color.red_1,
					R.color.blue_11, 
					R.color.yellow_2
					);
		}
	}
	
	@Override
	public void onRefresh(){
		setRefreshing(true);
	}
	
	public void stopRefreshing(){
		setRefreshing(false);
	}
	
}
