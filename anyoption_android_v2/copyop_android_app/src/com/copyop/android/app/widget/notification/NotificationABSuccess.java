package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationABSuccess extends LinearLayout{

	private String message;
	
	public NotificationABSuccess(String message) {
		super(Application.get());
		this.message = message;
		init(Application.get(), null, 0);
	}
	public NotificationABSuccess(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationABSuccess(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationABSuccess(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_ab_success, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		messageTextView.setText(message);
	}

}
