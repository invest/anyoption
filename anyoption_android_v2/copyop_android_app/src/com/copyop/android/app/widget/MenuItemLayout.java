package com.copyop.android.app.widget;

import com.anyoption.android.app.util.FontsUtils;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class MenuItemLayout extends FrameLayout {

	private ImageView selectedIndicatorImageView;
	private TextView titleTextView;
	private ImageView iconImageView;

	public MenuItemLayout(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public MenuItemLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public MenuItemLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		boolean isItemBig = false;
		Drawable icon = null;
		String title = null;
		int fontID = 0;
		boolean drawIconBorder = false;

		if (attrs != null) {
			// Parse custom attributes:
			TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,
					R.styleable.MenuItemLayout);
			isItemBig = attributesTypedArray.getBoolean(R.styleable.MenuItemLayout_isItemBig, isItemBig);
			icon = attributesTypedArray.getDrawable(R.styleable.MenuItemLayout_menuIcon);
			title = attributesTypedArray.getString(R.styleable.MenuItemLayout_menuTitle);
			fontID = attributesTypedArray.getInt(R.styleable.MenuItemLayout_font, fontID);
			drawIconBorder = attributesTypedArray.getBoolean(R.styleable.MenuItemLayout_drawIconBorder,
					drawIconBorder);
			attributesTypedArray.recycle();
		}

		View rootView = null;
		if (isItemBig) {
			rootView = View.inflate(context, R.layout.menu_item_big, this);
		} else {
			rootView = View.inflate(context, R.layout.menu_item, this);
		}

		iconImageView = (ImageView) rootView.findViewById(R.id.menu_item_icon);
		titleTextView = (TextView) rootView.findViewById(R.id.menu_item_title);
		selectedIndicatorImageView = (ImageView) rootView
				.findViewById(R.id.menu_item_selected_indicator_image);

		if (!isInEditMode()) {
			if (attrs != null) {
				titleTextView.setFont(FontsUtils.getFontForFontID(fontID));

				titleTextView.setText(title);

				if (drawIconBorder) {
					iconImageView.setBackgroundResource(R.drawable.menu_item_icon_big_bg);
					iconImageView.setScaleType(ScaleType.CENTER);
				}

				iconImageView.setImageDrawable(icon);
			}
		} else {
			titleTextView.setText("Menu Item");
			iconImageView.setImageResource(R.drawable.ic_launcher);
		}
	}

	@Override
	public void setActivated(boolean activated) {
		if(selectedIndicatorImageView != null){
			if(activated){
				selectedIndicatorImageView.setVisibility(View.VISIBLE);
			}else{
				selectedIndicatorImageView.setVisibility(View.INVISIBLE);
			}
		}
		super.setActivated(activated);
	}
	
	public void setTitle(String title){
		titleTextView.setText(title);
	}
	
	public void setIcon(Drawable icon){
		iconImageView.setImageDrawable(icon);
	}
}
