package com.copyop.android.app.widget;

import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;

public class FollowButtonSmall extends FollowButton {

	public FollowButtonSmall(Context context) {
		super(context);
		init(context, null, 0);
	}
	public FollowButtonSmall(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public FollowButtonSmall(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
	
	@Override
	protected int getContentViewId() {
		return R.layout.follow_button_small_content;
	}
	
}
