package com.copyop.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.Market;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.FollowDialogFragment;
import com.copyop.android.app.util.COConstants;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationNewInvestment extends LinearLayout{
	
	private String message;
	private Market market;
	private long invetsmentId;
	private int direction;
	private double amount;
	
	public NotificationNewInvestment(String message, Market market, long investmentId, int direction, double amount) {
		super(Application.get());
		this.message = message;
		this.market = market;
		this.invetsmentId = investmentId;
		this.direction = direction;
		this.amount = amount;
		init(Application.get(), null, 0);
	}
	public NotificationNewInvestment(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationNewInvestment(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationNewInvestment(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_new_investment, this);
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		messageTextView.setText(message);
		
		Button buyButton = (Button) view.findViewById(R.id.notification_buy_button);
		buyButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					//Check if the user has enough money to follow:
					if(amount > AmountUtil.getDoubleAmount(Application.get().getUser().getBalance())){
						Application.get().postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.INIT));
					}else{
						openFollowDialog();
					}
				}
			}
		});
	}
	
	private void openFollowDialog(){
		Bundle args = new Bundle();
		args.putLong(COConstants.EXTRA_MARKET_ID, market.getId());
		args.putLong(COConstants.EXTRA_INVESTMENT_ID, invetsmentId);
		args.putInt(COConstants.EXTRA_INVESTMENT_TYPE, direction);
		Application.get().getFragmentManager().showDialogFragment(FollowDialogFragment.class, args);
	}

}
