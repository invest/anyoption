package com.copyop.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COTextUtils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;

public class HitButton extends COButton {

	public HitButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public HitButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public HitButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
  		
	@Override
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		super.init(context, attrs, defStyleAttr);
		setFont(Font.ROBOTO_LIGHT);
		setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.user_profile_hit_button_text_size));
		setTextColor(getResources().getColor(R.color.user_profile_text));
		setText(context.getString(R.string.hitPercent));
	}

	@Override
	protected void setupAttributes(Context context, AttributeSet attrs, int defStyleAttr) {
		super.setupAttributes(context, attrs, defStyleAttr);
		Drawable drawable = getResources().getDrawable(R.drawable.hit_icon_big);
		setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		color = getResources().getColor(R.color.user_profile_hit_button);
		shadowColor = getResources().getColor(R.color.user_profile_hit_button_shadow);
		shadowSize = getResources().getDimensionPixelSize(R.dimen.button_shadow_width);
		isBorder = false;
		iconBGColor = getResources().getColor(R.color.user_profile_hit_button_icon_bg);
		iconBGSize = getResources().getDimensionPixelSize(R.dimen.user_profile_hit_button_icon_size);
		paddingLeft = getResources().getDimensionPixelSize(R.dimen.user_profile_hit_button_icon_margin_left_right);
		setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
		radius = 0;
	}
	
	public void setHit(String hit){
		setText(hit + " " + Application.get().getString(R.string.hitPercent));
		int hitTextSize;
		if(hit.equals("--")){
			hitTextSize = (int) getResources().getDimension(R.dimen.user_profile_hit_button_text_size);
		}else{
			hitTextSize = (int) getResources().getDimension(R.dimen.user_profile_hit_button_text_big_size);
		}
		COTextUtils.decorateInnerText(this, null, getText().toString(), hit, Font.ROBOTO_REGULAR, R.color.user_profile_text, hitTextSize);
	}
	
}