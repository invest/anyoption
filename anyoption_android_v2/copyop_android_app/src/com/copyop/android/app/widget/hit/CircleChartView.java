package com.copyop.android.app.widget.hit;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.ScreenUtils;
import com.copyop.android.app.R;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class CircleChartView extends View {

	public final String TAG = CircleChartView.class.getSimpleName();
	
	private float value1;
	private float value2;
	private float value3;
	private float value4;

	private float progress1;
    private float progress2;
    private float progress3;
    private float progress4;
    
	private Paint paint;
	private RectF rect1;
	private RectF rect2;
	private RectF rect3;
	private RectF rect4;
	
	private long ANIMATION_DURATION 	= 1500l;
	private long ANIMATION_START_DELAY 	= 200l;	
	private float RING_WIDTH;
	
	public CircleChartView(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public CircleChartView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public CircleChartView(Context context, AttributeSet attrs,int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		if(!isInEditMode()){
			rect1.set(RING_WIDTH/2, RING_WIDTH/2, getWidth()-RING_WIDTH/2, getHeight()-RING_WIDTH/2);
			rect2.set(RING_WIDTH*3/2, RING_WIDTH*3/2, getWidth()-RING_WIDTH*3/2, getHeight()-RING_WIDTH*3/2);
			rect3.set(RING_WIDTH*5/2, RING_WIDTH*5/2, getWidth()-RING_WIDTH*5/2, getHeight()-RING_WIDTH*5/2);
			rect4.set(RING_WIDTH*7/2, RING_WIDTH*7/2, getWidth()-RING_WIDTH*7/2, getHeight()-RING_WIDTH*7/2);
		}
	}
	
	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		if(!isInEditMode()){
			RING_WIDTH = ScreenUtils.convertDpToPx(Application.get(), 11);
			
			paint = new Paint();
			rect1 = new RectF();
			rect2 = new RectF();
			rect3 = new RectF();
			rect4 = new RectF();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if(!isInEditMode()){
			drawBackground(canvas);
			drawProgress(canvas);
			drawBorders(canvas);
		}
	}
	
	private void drawProgress(Canvas canvas){
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE); 
		paint.setStrokeWidth(RING_WIDTH);
		
		paint.setColor(getResources().getColor(R.color.circle_chart_ring_1_bg));
		canvas.drawArc(rect1, -90f, progress1, false, paint);
		
		paint.setColor(getResources().getColor(R.color.circle_chart_ring_2_bg));
		canvas.drawArc(rect2, -90f, progress2, false, paint);
		
		paint.setColor(getResources().getColor(R.color.circle_chart_ring_3_bg));
		canvas.drawArc(rect3, -90f, progress3, false, paint);
		
		paint.setColor(getResources().getColor(R.color.circle_chart_ring_4_bg));
		canvas.drawArc(rect4, -90f, progress4, false, paint);
	}
	
	private void drawBackground(Canvas canvas){
		paint.setColor(getResources().getColor(R.color.white));
		paint.setStyle(Paint.Style.FILL); 
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2, paint);
	}
	
	private void drawBorders(Canvas canvas){
		paint.setColor(getResources().getColor(R.color.circle_chart_border));
		paint.setStyle(Paint.Style.STROKE); 
		paint.setStrokeWidth(1f);
		
		
		//Draw circles:
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2, paint);
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2-RING_WIDTH, paint);
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2-RING_WIDTH*2, paint);
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2-RING_WIDTH*3, paint);
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2-RING_WIDTH*4, paint);
		
		//Draw radiuses:
		canvas.drawLine(0, getHeight()/2, getWidth(), getHeight()/2, paint);
		canvas.drawLine(getWidth()/2, 0, getWidth()/2, getHeight(), paint);
		canvas.drawLine(getWidth()/2 - (float)Math.sin(Math.PI/4)*getWidth()/2, 
						getHeight()/2 - (float)Math.sin(Math.PI/4)*getHeight()/2, 
						getWidth()/2 + (float)Math.sin(Math.PI/4)*getWidth()/2, 
						getHeight()/2 + (float)Math.sin(Math.PI/4)*getHeight()/2, paint);
		canvas.drawLine(getWidth()/2 - (float)Math.sin(Math.PI/4)*getWidth()/2, 
						getHeight()/2 + (float)Math.sin(Math.PI/4)*getHeight()/2, 
						getWidth()/2 + (float)Math.sin(Math.PI/4)*getWidth()/2, 
						getHeight()/2 - (float)Math.sin(Math.PI/4)*getHeight()/2, paint);
		
		//Draw center circle:
		paint.setColor(getResources().getColor(R.color.white));
		paint.setStyle(Paint.Style.FILL); 
		canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2-RING_WIDTH*4-1, paint);
	}
	
	public void startAnimation(){
		AnimatorSet set = new AnimatorSet();
		
		ValueAnimator animator1 = ValueAnimator.ofFloat(0, value1);
		animator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update value:
				float newValue = (Float) valueAnimator.getAnimatedValue();
				progress1 = newValue;
				invalidate();
			}
		});
		
		ValueAnimator animator2 = ValueAnimator.ofFloat(0, value2);
		animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update value:
				float newValue = (Float) valueAnimator.getAnimatedValue();
				progress2 = newValue;
				invalidate();
			}
		});
		
		ValueAnimator animator3 = ValueAnimator.ofFloat(0, value3);
		animator3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update value:
				float newValue = (Float) valueAnimator.getAnimatedValue();
				progress3 = newValue;
				invalidate();
			}
		});
		
		ValueAnimator animator4 = ValueAnimator.ofFloat(0, value4);
		animator4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update value:
				float newValue = (Float) valueAnimator.getAnimatedValue();
				progress4 = newValue;
				invalidate();
			}
		});
		
		set.playTogether(animator1, animator2, animator3, animator4);
		set.setDuration(ANIMATION_DURATION);
		set.setInterpolator(new LinearInterpolator());
		set.setStartDelay(ANIMATION_START_DELAY);
		set.start();
	}

	/**
	 * All values must be between 0 and 100
	 */
	public void setValues(int value1, int value2, int value3, int value4){
		if(value1 < 0 || value1 > 100){
			Log.e(TAG, "setValues ---> Value1 is incorrect. Setting Value1 to 0.");
			value1 = 0;
		}
		if(value2 < 0 || value2 > 100){
			Log.e(TAG, "setValues ---> Value2 is incorrect. Setting Value2 to 0.");
			value2 = 0;
		}
		if(value3 < 0 || value3 > 100){
			Log.e(TAG, "setValues ---> Value3 is incorrect. Setting Value3 to 0.");
			value3 = 0;
		}
		if(value4 < 0 || value4 > 100){
			Log.e(TAG, "setValues ---> Value4 is incorrect. Setting Value4 to 0.");
			value4 = 0;
		}
		
		this.value1 = 360 * value1 / 100;
		this.value2 = 360 * value2 / 100;
		this.value3 = 360 * value3 / 100;
		this.value4 = 360 * value4 / 100;
	}
	
}
