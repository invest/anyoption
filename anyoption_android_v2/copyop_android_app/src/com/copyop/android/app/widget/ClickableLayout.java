package com.copyop.android.app.widget;

import com.anyoption.android.app.util.DrawableUtils;
import com.copyop.android.app.R;
import com.copyop.android.app.util.CODrawableUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Custom Layout that has a special pressed effect.
 * @author Anastas Arnaudov
 *
 */
public class ClickableLayout extends RelativeLayout implements View.OnTouchListener{

	protected int color = getResources().getColor(R.color.transparent);
	protected int shadowColor = getResources().getColor(R.color.transparent);;
	protected int shadowSize = (int) getResources().getDimension(R.dimen.button_shadow_width);
	protected boolean isBorder = true;
	protected int paddingLeft;
	protected int paddingTop;
	protected int paddingRight;
	protected int paddingBottom;
	protected Drawable normalBackground;
	private Drawable pressedBackground;
	private Padding[] childrenPaddings;
	protected float radius = getResources().getDimension(R.dimen.button_radius);
	
	public ClickableLayout(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public ClickableLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public ClickableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}
	
	@SuppressLint("ClickableViewAccessibility")
	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		// Get custom attributes:
		TypedArray attributesTypedArray = context.obtainStyledAttributes(attrs,R.styleable.ClickableLayout);
		color = attributesTypedArray.getColor(R.styleable.ClickableLayout_color, color);
		shadowColor = attributesTypedArray.getColor(R.styleable.ClickableLayout_shadowColor, shadowColor);
		radius = attributesTypedArray.getDimension(R.styleable.ClickableLayout_radius, radius);
		if(!isInEditMode()){
			shadowSize = (int)attributesTypedArray.getDimension(R.styleable.ClickableLayout_shadowSize, shadowSize);
			isBorder = attributesTypedArray.getBoolean(R.styleable.ClickableLayout_isBorder, isBorder);
		}
		
		attributesTypedArray.recycle();
			
		// We save the initial paddings as the setting of the background will remove them:
		paddingLeft = getPaddingLeft();
		paddingTop = getPaddingTop();
		paddingRight = getPaddingRight();
		paddingBottom = getPaddingBottom();
			
		if(!isInEditMode()){
			normalBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, shadowSize, isBorder, false, radius);
			pressedBackground = CODrawableUtils.createClickableDrawable(context, color, shadowColor, shadowSize, isBorder, true, radius);
		}else{
			setBackgroundColor(color);
		}
		
		setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom + shadowSize);
		
		setClickable(true);
		setOnTouchListener(this);
	}

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(!isInEditMode()){
        	updateBackground(normalBackground);
        	setupChildrenPadding(false);
        }else{
        	setBackgroundColor(color);
        }
    }
	
    @SuppressLint("ClickableViewAccessibility")
	@Override
    public boolean onTouch(View view, MotionEvent event) {
    	int[] coord = new int[2];
    	view.getLocationOnScreen(coord);
		switch (event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	            updateBackground(pressedBackground);
	            setupChildrenPadding(true);
	            break;
	        case MotionEvent.ACTION_MOVE:
	            Rect r = new Rect();
	            view.getLocalVisibleRect(r);
	            if (!r.contains((int) event.getX(), (int) event.getY() + 3 * shadowSize) &&
	                !r.contains((int) event.getX(), (int) event.getY() - 3 * shadowSize)) {
	            	updateBackground(normalBackground);
	            	setupChildrenPadding(false);
	            }
	            break;
	        case MotionEvent.ACTION_OUTSIDE:
	        case MotionEvent.ACTION_CANCEL:
	        case MotionEvent.ACTION_UP:
	        	updateBackground(normalBackground);
	        	setupChildrenPadding(false);
	            break;
		}
		return false;
	}
	
	private void setupChildrenPadding(boolean selected){
		int childrenCount = getChildCount();
		if(childrenCount == 0){
			return;
		}
		if(childrenPaddings == null){
			getChildrenPadding();
		}
		int offset = (selected) ? -shadowSize : shadowSize;
		for(int i=0; i<getChildCount(); i++){
			View view = getChildAt(i);
			Padding padding = childrenPaddings[i];
			view.setPadding(padding.paddingLeft, padding.paddingTop, padding.paddingRight, padding.paddingBottom + offset);
		}
	}
	
	private void getChildrenPadding(){
		childrenPaddings = new Padding[getChildCount()];
		for(int i=0; i<getChildCount(); i++){
			View view = getChildAt(i);
			Padding padding = new Padding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
			childrenPaddings[i] = padding;
		}
	}
	
	public void setStyle(int colorRes, int shadowColorRes){
		this.color = getResources().getColor(colorRes);
		this.shadowColor = getResources().getColor(shadowColorRes);
		normalBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, false);
		pressedBackground = CODrawableUtils.createClickableDrawable(getContext(), color, shadowColor, shadowSize, isBorder, true);
		updateBackground(normalBackground);
	}
	
	protected void updateBackground(Drawable drawable){
		DrawableUtils.setBackground(this, drawable);
	}
	
	public static class Padding{
		int paddingLeft;
		int paddingTop;
		int paddingRight;
		int paddingBottom;
		public Padding(int paddingLeft, int paddingTop, int paddingRight, int paddingBottom){
			this.paddingLeft = paddingLeft;
			this.paddingTop = paddingTop;
			this.paddingRight = paddingRight;
			this.paddingBottom = paddingBottom;
		}
	}

}
