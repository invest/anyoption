package com.copyop.android.app.widget;

import com.anyoption.android.app.Application;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class DoneButton extends RelativeLayout{

	public DoneButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	public DoneButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public DoneButton(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View rootView = View.inflate(context, R.layout.done_button_layout, this);
		View doneButton = rootView.findViewById(R.id.doneButton);
		doneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Application.get().getCurrentActivity().onBackPressed();
			}
		});
	}

}
