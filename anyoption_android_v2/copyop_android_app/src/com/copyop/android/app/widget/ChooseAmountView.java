package com.copyop.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.Currency;
import com.copyop.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ChooseAmountView extends FrameLayout implements OnClickListener {

	private static final float MIN_VALUE = 0.0f;

	public interface OnAmountChangeListener {

		public void onAmountChange(double newAmount);

	}

	private Application application;
	private Currency currency;
	private float amount;
	private float amountUpdateStep;

	private View rootView;
	private ImageView plusView;
	private ImageView minusView;
	private TextView amountView;

	private OnAmountChangeListener amountChangeListener;

	public ChooseAmountView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public ChooseAmountView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ChooseAmountView(Context context) {
		this(context, null);
	}

	private void init(Context context) {
		rootView = View.inflate(context, R.layout.choose_amount_view, this);

		if (isInEditMode()) {
			return;
		}

		application = Application.get();
		currency = application.getUser().getCurrency();
		amount = application.getDefaultInvestAmount().floatValue();

		plusView = (ImageView) rootView.findViewById(R.id.choose_amount_plus);
		minusView = (ImageView) rootView.findViewById(R.id.choose_amount_minus);
		amountView = (TextView) rootView.findViewById(R.id.choose_amount_amount);

		amountView.setText(AmountUtil.getFormattedAmount(amount, currency));
		updateAmount(amount);
		updateAmountStep();

		plusView.setOnClickListener(this);
		minusView.setOnClickListener(this);
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		updateAmount(amount);
		updateAmountStep();
	}

	public OnAmountChangeListener getAmountChangeListener() {
		return amountChangeListener;
	}

	public void setAmountChangeListener(OnAmountChangeListener amountChangeListener) {
		this.amountChangeListener = amountChangeListener;
	}

	private void updateAmount(double newAmount) {
		amount = (float) newAmount;
		amountView.setText(AmountUtil.getFormattedAmount(amount, currency));

		if (amountChangeListener != null) {
			amountChangeListener.onAmountChange(amount);
		}
	}

	/**
	 * Update step is 10% of the amount. It is rounded up (3.7 = 4; 3.3 = 4; 3.0 = 0)
	 */
	private void updateAmountStep() {
		amountUpdateStep = (float) Math.ceil(amount / 10d);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == plusView.getId()) {
			amount += amountUpdateStep;

		} else {
			if (amount > MIN_VALUE + amountUpdateStep) {
				amount -= amountUpdateStep;
			} else {
				amount = 0.0f;
			}
		}

		updateAmount(amount);
	}

}
