package com.copyop.android.app.widget;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.activity.MainActivity;
import com.anyoption.android.app.event.LanguageChangeEvent;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.TextView;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.Skin;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.LanguageSelectorDialogFragment;
import com.copyop.android.app.popup.LanguageSelectorDialogFragment.OnLanguageSelectedListener;
import com.copyop.android.app.util.COConstants;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class LanguageButton extends TextView implements View.OnClickListener, OnLanguageSelectedListener {

	private boolean shouldGoToMainOnRestart;
	private List<Language> languageList;
	private Long skinID = null;
	private Language language;
	
	public LanguageButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	public LanguageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public LanguageButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		setTextColor(getResources().getColor(R.color.default_text));
		setFont(Font.ROBOTO_REGULAR);
		setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.language_selector_text));				
		setCompoundDrawablePadding((int)getResources().getDimension(R.dimen.language_button_drawable_padding));
		setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.action_bar_language_selector_arrow), null);
		
		if(!isInEditMode()){
			setupLanguages();
			shouldGoToMainOnRestart = Application.get().getCurrentActivity() instanceof MainActivity;
			
			//Set the default language:
			language = getLanguageForLocale(Application.get().getLocale().toString());
			if(language == null){
				//Get the default language:
				language = getLanguageForDisplayName(Application.get().getSkins().get(Skin.SKIN_ENGLISH).getName());
			}
			////////////////////////////////////
			
			setText(language.getDisplayName().toUpperCase(Application.get().getLocale()));		
			setOnClickListener(this);
		}else{
			setText("Language Selector");
		}
	}
	
	private void setupLanguages() {
		languageList = new ArrayList<Language>();
		Application application = Application.get();
		Map<Long, Skin> skinsMap = application.getSkins();
		if(skinID != null && (skinID == Skin.SKIN_EN_US || skinID == Skin.SKIN_ES_US)){
			
			Language langEnglish = new Language();
			langEnglish.setDisplayName(application.getString(R.string.languageEnglish));
			langEnglish.setCode(skinsMap.get(Skin.SKIN_EN_US).getLocale());
			
			Language langSpanish = new Language();
			langSpanish.setDisplayName(application.getString(R.string.languageSpanish));
			langSpanish.setCode(skinsMap.get(Skin.SKIN_ES_US).getLocale());
			
			languageList.add(langEnglish);
			languageList.add(langSpanish);
			
		}else if((skinID != null && skinID == Skin.SKIN_ETRADER) || application.getSkinId() == Skin.SKIN_ETRADER){
			
			Language langHebrew = new Language();
			langHebrew.setDisplayName(skinsMap.get(Skin.SKIN_ETRADER).getName());
			langHebrew.setCode(skinsMap.get(Skin.SKIN_ETRADER).getLocale());
			
			languageList.add(langHebrew);
			
		}else{
			
			Language langEnglish = new Language();
			langEnglish.setDisplayName(application.getString(R.string.languageEnglish));
			langEnglish.setCode(skinsMap.get(Skin.SKIN_ENGLISH).getLocale());
			
			Language langSpanish = new Language();
			langSpanish.setDisplayName(application.getString(R.string.languageSpanish));
			langSpanish.setCode(skinsMap.get(Skin.SKIN_SPAIN).getLocale());
			
			Language langGerman = new Language();
			langGerman.setDisplayName(application.getString(R.string.languageGerman));
			langGerman.setCode(skinsMap.get(Skin.SKIN_GERMAN).getLocale());
			
			Language langItalian = new Language();
			langItalian.setDisplayName(application.getString(R.string.languageItalian));
			langItalian.setCode(skinsMap.get(Skin.SKIN_ITALIAN).getLocale());
			
			Language langFrench = new Language();
			langFrench.setDisplayName(application.getString(R.string.languageFrench));
			langFrench.setCode(skinsMap.get(Skin.SKIN_FRANCE).getLocale());
			
			languageList.add(langEnglish);
			languageList.add(langSpanish);
			languageList.add(langGerman);
			languageList.add(langItalian);
			languageList.add(langFrench);
		}
		
	}
	
	public Language getLanguageForLocale(String locale){
		Language result = null;
		if(locale != null){
			for(Language language : languageList){
				if(language.getCode().startsWith(locale)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}
	
	public Language getLanguageForDisplayName(String displayName){
		Language result = null;
		if(displayName != null){
			for(Language language : languageList){
				if(language.getDisplayName().equals(displayName)){
					result = language;
					break;
				}
			}			
		}
		return result;
	}
	
	@Override
	public void onClick(View v) {
		setupLanguageDialog();
	}

	private void setupLanguageDialog(){
		Bundle bundle = new Bundle();
		bundle.putSerializable(COConstants.EXTRA_LANGUAGES_LIST, (Serializable) this.languageList);
		bundle.putSerializable(COConstants.EXTRA_LANGUAGE, (Serializable) this.language);
		LanguageSelectorDialogFragment dialog = LanguageSelectorDialogFragment.newInstance(bundle);
		dialog.setOnLanguageSelectedListener(this);
		Application.get().getFragmentManager().showDialogFragment(dialog);
	}
	
	@Override
	public void onLanguageSelected(Language language) {
		this.language = language;
		setText(language.getDisplayName().toUpperCase(Application.get().getLocale()));	
		Application.get().postEvent(new LanguageChangeEvent(language.getCode(), shouldGoToMainOnRestart));
	}
	
}
