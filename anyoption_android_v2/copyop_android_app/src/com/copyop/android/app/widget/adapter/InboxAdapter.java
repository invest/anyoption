package com.copyop.android.app.widget.adapter;

import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.widget.TextView;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.model.Update.Type;
import com.copyop.android.app.widget.COButton;
import com.copyop.android.app.widget.CopySmallButton;
import com.copyop.android.app.widget.FollowButton;
import com.copyop.android.app.widget.WatchSmallButton;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Custom {@link BaseAdapter} for the Update Feed ListView.
 * @author Anastas Arnaudov
 *
 */
public class InboxAdapter extends NewsAdapter{

	public static final String TAG = InboxAdapter.class.getSimpleName();
	
	public static class UpdateViewHolder extends com.copyop.android.app.widget.adapter.NewsAdapter.UpdateViewHolder{
		
		public View rootView;
		public ImageView dotImageView;
		public COButton depositButton;
		public COButton coinsButton;
		
	}

	protected int readBG;
	protected int unreadBG;

	public InboxAdapter(List<Update> updatesList) {
    	super(updatesList);
    	readBG = context.getResources().getColor(R.color.inbox_item_bg_read);
    	unreadBG = context.getResources().getColor(R.color.inbox_item_bg_unread);
    }

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {
		return IGNORE_ITEM_VIEW_TYPE;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Update update = updatesList.get(position);
		UpdateViewHolder updateViewHolder;
		if(convertView == null){
			updateViewHolder = new UpdateViewHolder();
			convertView = layoutInflater.inflate(R.layout.inbox_item, parent, false);
			updateViewHolder.rootView = convertView.findViewById(R.id.news_item_root_layout);
			updateViewHolder.topBorderView = convertView.findViewById(R.id.news_item_divider_top);
			updateViewHolder.bottomBorderView = convertView.findViewById(R.id.news_item_divider_bottom);
			updateViewHolder.iconImageView = (ImageView) convertView.findViewById(R.id.news_item_icon_image_view);
			updateViewHolder.dotImageView = (ImageView) convertView.findViewById(R.id.news_item_icon_dot_image_view);
			updateViewHolder.titleTextView = (TextView) convertView.findViewById(R.id.news_item_title_text_view);
			updateViewHolder.bodyTextView = (TextView) convertView.findViewById(R.id.news_item_content_text_view);
			updateViewHolder.timestampTextView = (TextView) convertView.findViewById(R.id.news_item_timestamp_text_view);
			updateViewHolder.copyButton = (CopySmallButton) convertView.findViewById(R.id.news_item_copy_button);
			updateViewHolder.watchButton = (WatchSmallButton) convertView.findViewById(R.id.news_item_watch_button);
			updateViewHolder.followButton = (FollowButton) convertView.findViewById(R.id.news_item_follow_button);
			updateViewHolder.coinsButton = (COButton) convertView.findViewById(R.id.news_item_coins_button);
			updateViewHolder.depositButton = (COButton) convertView.findViewById(R.id.news_item_deposit_button);
			
			convertView.setTag(updateViewHolder);
		}else{
			updateViewHolder = (UpdateViewHolder) convertView.getTag();
		}
		updateViewHolder.position = position;
		
		try {
			setupListItem(update, updateViewHolder);
		} catch (Exception e) {
			Log.e(TAG, "ListView getView()", e);
		}
		
		return convertView;
	}

	@Override
	protected void setupListItem(Update update, com.copyop.android.app.widget.adapter.NewsAdapter.UpdateViewHolder u) throws Exception{
		UpdateViewHolder updateViewHolder = (UpdateViewHolder) u;
		clearSpannable();
		if(update.getType() == Type.INBOX){
			setupInbox(update, updateViewHolder);
		}else{
			throw new Exception("Unknown Update Type!!!");
		}
	}

	protected void setupInbox(Update update, UpdateViewHolder updateViewHolder){
		setupBackground(update, updateViewHolder);
		setupDotIcon(update, updateViewHolder);
		setupTitle(update, updateViewHolder);
		setupBody(update, updateViewHolder);
		setupBorders(update, updateViewHolder);
		setupIcon(update, updateViewHolder);
		setupButtons(update, updateViewHolder);
		setupTimestamp(update, updateViewHolder);
	}

	protected void setupBackground(Update update, UpdateViewHolder updateViewHolder) {
		if(update.isRead()){
			updateViewHolder.rootView.setBackgroundColor(readBG);
		}else{
			updateViewHolder.rootView.setBackgroundColor(unreadBG);
		}
	}
	
	protected void setupDotIcon(Update update, UpdateViewHolder updateViewHolder) {
		if(update.isRead()){
			updateViewHolder.dotImageView.setVisibility(View.GONE);
		}else{
			updateViewHolder.dotImageView.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	protected void setupButtons(Update update, com.copyop.android.app.widget.adapter.NewsAdapter.UpdateViewHolder u) {
		super.setupButtons(update, u);
		UpdateViewHolder updateViewHolder = (UpdateViewHolder) u;
		updateViewHolder.coinsButton.setTag(updateViewHolder);	
		updateViewHolder.depositButton.setTag(updateViewHolder);
		updateViewHolder.coinsButton.setOnClickListener(this);	
		updateViewHolder.depositButton.setOnClickListener(this);
		if(update.isCoinsButton()){
			updateViewHolder.coinsButton.setVisibility(View.VISIBLE);
		}else{
			updateViewHolder.coinsButton.setVisibility(View.GONE);
		}
		if(update.isDepositButton()){
			updateViewHolder.depositButton.setVisibility(View.VISIBLE);
		}else{
			updateViewHolder.depositButton.setVisibility(View.GONE);
		}
	}
	
	protected void goToCoins() {
		Application.get().postEvent(new NavigationEvent(COScreen.COPYOP_COINS, NavigationType.INIT));
	}

	
	protected void goToDeposit(){
		Application.get().postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.INIT));
	}
	
	@Override
	protected void handleOnClick(View view, Update update, com.copyop.android.app.widget.adapter.NewsAdapter.UpdateViewHolder vH){
		UpdateViewHolder viewHolder = (UpdateViewHolder) vH;
		if (view.getId() == viewHolder.coinsButton.getId()) {
			goToCoins();
			return;
		} else if (view.getId() == viewHolder.depositButton.getId()) {
			goToDeposit();
			return;
		}
		super.handleOnClick(view, update, vH);
	}

}
