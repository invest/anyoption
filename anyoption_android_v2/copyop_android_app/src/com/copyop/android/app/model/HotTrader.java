package com.copyop.android.app.model;

/**
 * Model for the Hot Traders List.
 * @see {@link }
 * @author Anastas Arnaudov
 *
 */
public class HotTrader extends HotItem{

	private static final long serialVersionUID = 1L;
	protected int hitPercent;
	protected String hitPercentFormated;
	protected long copiersCount;
	
	public double getHitPercent() {
		return hitPercent;
	}
	public HotTrader setHitPercent(int hitPercent) {
		this.hitPercent = hitPercent;
		this.hitPercentFormated = this.hitPercent + "%";
		return this;
	}
	public String getHitPercentFormated() {
		return hitPercentFormated;
	}
	public long getCopiersCount() {
		return copiersCount;
	}
	public HotTrader setCopiersCount(long copiersCount) {
		this.copiersCount = copiersCount;
		return this;
	}
	
}
