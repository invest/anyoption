package com.copyop.android.app.model;

public class HotCopier extends HotTrader {

	private long copyingCount;

	public long getCopyingCount() {
		return copyingCount;
	}

	public void setCopyingCount(long copyingCount) {
		this.copyingCount = copyingCount;
	}

}
