package com.copyop.android.app.model;

import java.io.Serializable;

import com.copyop.android.app.util.COAmountUtil;
import com.copyop.common.enums.base.UpdateTypeEnum;

/**
 * Base Model of a List Item in a Hot list.
 * @author Anastas Arnaudov
 *
 */
public abstract class HotItem implements Serializable{

	private static final long serialVersionUID = 1L;

	private UpdateTypeEnum updateTypeEnum;
	protected long userId;
	protected String iconURL;
	protected String nickname;
	protected double profit;
	protected String profitFormated;
	private int lastHours;
	
	public long getUserId() {
		return userId;
	}
	public HotItem setUserId(long userId) {
		this.userId = userId;
		return this;
	}
	public String getIconURL() {
		return iconURL;
	}
	public HotItem setIconURL(String iconURL) {
		this.iconURL = iconURL;
		return this;
	}
	public String getNickname() {
		return nickname;
	}
	public HotItem setNickname(String nickname) {
		this.nickname = nickname;
		return this;
	}
	public double getProfit() {
		return profit;
	}
	public HotItem setProfit(double profit) {
		this.profit = profit;
		this.profitFormated = COAmountUtil.formatHotProfit(profit);
		return this;
	}
	public String getProfitFormated() {
		return profitFormated;
	}
	public int getLastHours() {
		return lastHours;
	}
	public void setLastHours(int lastHours) {
		this.lastHours = lastHours;
	}
	public UpdateTypeEnum getUpdateType() {
		return updateTypeEnum;
	}
	public void setUpdateType(UpdateTypeEnum updateTypeEnum) {
		this.updateTypeEnum = updateTypeEnum;
	}

}
