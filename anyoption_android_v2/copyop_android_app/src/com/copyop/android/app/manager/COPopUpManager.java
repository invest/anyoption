package com.copyop.android.app.manager;

import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.R;
import com.copyop.android.app.popup.CopiedOptionDialogFragment;
import com.copyop.android.app.popup.DozedOffDialogFragment;
import com.copyop.android.app.popup.FullyCopiedDialogFragment;
import com.copyop.android.app.popup.TextDialogFragment;
import com.copyop.android.app.popup.WatchDialogFragment.OnWatchStateChangedListener;
import com.copyop.android.app.util.COConstants;

import android.os.Bundle;

public class COPopUpManager extends PopUpManager {

	public COPopUpManager(COApplication application) {
		super(application);
	}

	/**
	 * Displays a Default Alert POPUP.
	 * 
	 * @param title
	 * @param message
	 */
	public void showAlertPopUp(String title, String message) {
		TextDialogFragment popup = TextDialogFragment.newInstance(title, message,
				application.getString(R.string.dismiss));
		application.getFragmentManager().showDialogFragment(popup);
	}

	/**
	 * Displays "Can not unwatch" dialog.
	 * 
	 * @param nickname
	 * @param dismissListener
	 */
	public void showCanNotUnwatchPopUp(String nickname, OnDialogDismissListener dismissListener) {
		String title = application.getString(R.string.watchingCap) + " " + nickname;
		TextDialogFragment popup = TextDialogFragment.newInstance(title,
				application.getString(R.string.canNotUnwatchMsg));
		popup.setOnDismissListener(dismissListener);
		application.getFragmentManager().showDialogFragment(popup);
	}

	/**
	 * Displays "Fully copied" dialog.
	 * 
	 * @param profileId
	 * @param nickname
	 * @param isWatching
	 *            is the user watching this profile
	 * @param watchStateChangedListener
	 *            used if the user is not watching this trader and clicks watch.
	 * @param dismissListener
	 */
	public void showFullyCopiedPopUp(long profileId, String nickname, boolean isWatching,
			OnWatchStateChangedListener watchStateChangedListener,
			OnDialogDismissListener dismissListener) {
		FullyCopiedDialogFragment fullyCopiedDialog = FullyCopiedDialogFragment.newInstance(
				profileId, nickname, isWatching);
		fullyCopiedDialog.setOnDismissListener(dismissListener);
		fullyCopiedDialog.setOnWatchStateChangedListener(watchStateChangedListener);
		application.getFragmentManager().showDialogFragment(fullyCopiedDialog);
	}

	/**
	 * Displays "Dozed off" (Low activity) dialog.
	 * 
	 * @param dismissListener
	 */
	public void showDozedOffPopUp(OnDialogDismissListener dismissListener) {
		DozedOffDialogFragment dialog = (DozedOffDialogFragment) application.getFragmentManager()
				.showDialogFragment(DozedOffDialogFragment.class, new Bundle());
		dialog.setOnDismissListener(dismissListener);
	}

	/**
	 * Displays POPUP with info about the User from which the option is copied from.
	 * @param investmentId
	 * @param dismissListener
	 */
	public void showCopiedOptionPopUp(long investmentId, OnDialogDismissListener dismissListener) {
		Bundle bundle = new Bundle();
		bundle.putLong(COConstants.EXTRA_INVESTMENT_ID, investmentId);
		CopiedOptionDialogFragment dialog = (CopiedOptionDialogFragment) application.getFragmentManager().showDialogFragment(CopiedOptionDialogFragment.class, bundle);
		dialog.setOnDismissListener(dismissListener);
	}
	
}