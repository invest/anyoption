package com.copyop.android.app.manager;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.manager.FacebookManager;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.R;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.requests.UpdateFacebookFriendsMethodRequest;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

public class COFacebookManager extends FacebookManager {
	
	private static final String TAG = COFacebookManager.class.getSimpleName();

	private static final String FACEBOOK_AVATAR_URL_PATTERN = "https://graph.facebook.com/fbId/picture?type=large";
	
	public COFacebookManager(Application application) {
		super(application);
	}
	
	public void updateFriends(String fbId, List<GraphUser> users, final Object resultObject, final String resultMethodName) {
		List<String> fIds = new ArrayList<String>();
		for (GraphUser user : users) {
			fIds.add(user.getId());
		}
		UpdateFacebookFriendsMethodRequest request = new UpdateFacebookFriendsMethodRequest();
		request.setFacebookId(fbId);
		request.setFriends(fIds);
		Application.get().getCommunicationManager().requestService(resultObject, resultMethodName, COConstants.SERVICE_UPDATE_FACEBOOK_FRIENDS, request , MethodResult.class, false, false);
	}
	
	public static void inviteFriends(final Context context, Fragment fragment) {
		Session session = Session.getActiveSession();
		if (null == session || !session.isOpened()) {
			openSession(context, fragment, new SessionOpenedListener() {
				@Override
				public void onSessionOpened(Session session) {
					inviteFriends(context, session);
				}
			});
		} else {
			inviteFriends(context, session);
		}
	}
	
	private static void inviteFriends(Context context, Session session) {
		if (null != session && session.isOpened()) {
			Bundle params = new Bundle();
			params.putString("filters", "app_non_users");
	
			WebDialog requestsDialog = (
					new WebDialog.RequestsDialogBuilder(context,
							session,
							params))
							.setTitle(context.getResources().getString(R.string.socialNetworksConnectedInviteFriendsTitle))
							.setMessage(context.getResources().getString(R.string.socialNetworksConnectedInviteFriendsMessage))
							.setOnCompleteListener(new OnCompleteListener() {
								@Override
								public void onComplete(Bundle values,
										FacebookException error) {
									Log.d(TAG, "onComplete - values: " + values + " error: " + error);
									if (error != null) {
										if (error instanceof FacebookOperationCanceledException) {
											Log.d(TAG, "Request cancelled");
										} else {
											Log.d(TAG, "Network Error");
										}
									} else {
										final String requestId = values.getString("request");
										if (requestId != null) {
											Log.d(TAG, "Request sent requestId: " + requestId + " to: " + values.getString("to[0]"));
										} else {
											Log.d(TAG, "Request cancelled");
										}
									}   
								}
							})
							.build();
			requestsDialog.show();
		}
	}
	
	public static String getAvatarURL(String fbId) {
		return FACEBOOK_AVATAR_URL_PATTERN.replace("fbId", fbId);
	}
	
	public static void publishStory(final Context context, final Fragment fragment, final String message) {
		Session session = Session.getActiveSession();
		if (null == session || !session.isOpened()) {
			Log.d(TAG, "openSession");
			openSession(context, fragment, new SessionOpenedListener() {
				@Override
				public void onSessionOpened(Session session) {
					publishStory(session, context, fragment, message);
				}
			});
		} else {
			publishStory(session, context, fragment, message);
		}
	}
	
	private static void publishStory(Session session, Context context, Fragment fragment, final String message) {
	    if (null != session && session.isOpened()) {
	    	String showOff = context.getString(R.string.showOff);
	    	if (FacebookDialog.canPresentShareDialog(context, FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
	    		Log.d(TAG, "FacebookDialog.ShareDialogBuilder");
	    		// Publish the post using the Share Dialog
	    		new FacebookDialog.ShareDialogBuilder((Activity) context)
	    		.setName(showOff)
//	    		.setCaption(showOff)
	    		.setDescription(message)
	    		.setLink("https://www.copyop.com")
//	    		.setPicture("https://www.copyop.com/logo.png")
	    		.build()
	    		.present();
	    	} else {
	    		Log.d(TAG, "WebDialog.FeedDialogBuilder");
	    	    Bundle params = new Bundle();
	    	    params.putString("name", showOff);
//	    	    params.putString("caption", showOff);
	    	    params.putString("description", message);
	    	    params.putString("link", "https://www.copyop.com");
//	    	    params.putString("picture", "https://www.copyop.com/logo.png");
	    	    
	    	    new WebDialog.FeedDialogBuilder((Activity) context, session, params)
	    	    .build()
	    	    .show();
	    	}
	    }
	}
	
	@Override
	protected void parseUri(Uri uri){
		super.parseUri(uri);
		Screenable screen = (Screenable) deepLinkBundle.get(Constants.EXTRA_SCREEN);
		if(screen != null){
			if(screen == Screen.REGISTER){
				screen = COScreen.LSS_SIGNUP;
				deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, screen);
			}
		}
	}
	
}