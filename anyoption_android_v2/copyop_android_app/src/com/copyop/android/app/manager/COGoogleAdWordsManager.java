package com.copyop.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.manager.GoogleAdWordsManager;

public class COGoogleAdWordsManager extends GoogleAdWordsManager {

	private static final String CONVERSION_ID = "966492944";
	private static final String REGISTRATION_EVENT_LABEL = "MJc-CIqG-VoQkIbuzAM";
	private static final String DEPOSIT_EVENT_LABEL = "qx9ECJDGgVsQkIbuzAM";

	public COGoogleAdWordsManager(Application application) {
		super(application);
	}

	@Override
	protected String getConversionId() {
		return CONVERSION_ID;
	}

	@Override
	protected String getRegistrationEventLabel() {
		return REGISTRATION_EVENT_LABEL;
	}

	@Override
	protected String getDepositEventLabel() {
		return DEPOSIT_EVENT_LABEL;
	}

}
