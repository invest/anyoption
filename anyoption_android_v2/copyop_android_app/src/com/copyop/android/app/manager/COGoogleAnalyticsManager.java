package com.copyop.android.app.manager;

import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
import com.copyop.android.app.COApplication.COScreen;

public class COGoogleAnalyticsManager extends GoogleAnalyticsManager{

	public COGoogleAnalyticsManager(int configResId) {
		super(configResId);
	}
	
	@Override
	protected Screenable getRegisterScreen(){
		return COScreen.LSS_SIGNUP_EMPTY;
	}
}
