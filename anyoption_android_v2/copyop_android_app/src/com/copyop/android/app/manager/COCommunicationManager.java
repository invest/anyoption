package com.copyop.android.app.manager;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

import com.anyoption.android.app.manager.CommunicationManager;
import com.anyoption.common.service.util.ErrorMessage;
import com.copyop.android.app.COApplication;
import com.copyop.android.app.event.ProfileUpdateEvent;
import com.copyop.android.app.util.AvatarUtils;
import com.copyop.android.app.util.COBitmapUtils;
import com.copyop.android.app.util.COConstants;
import com.copyop.json.results.UserMethodResult;
import com.google.gson.Gson;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Extension of the {@link CommunicationManager}.
 * @author Anastas Arnaudov
 *
 */
public class COCommunicationManager extends CommunicationManager {
	
	public static final String TAG = COCommunicationManager.class.getSimpleName();
	
	public COCommunicationManager(COApplication application) {
		super(application);
	}
	
	/**
	 * Uploads the given bitmap to our server. The response is handled by the {@link COCommunicationManager}.
	 * @param bitmap
	 */
	public void uploadAvatarImage(Bitmap bitmap){
		AvatarUploadTask avatarUploadTask = new AvatarUploadTask(bitmap, this, "onAvatarUploaded");
		avatarUploadTask.execute();
	}
	
	/**
	 * Uploads the given bitmap to our server.
	 * @param bitmap The Bitmap to be uploaded.
	 * @param callback The callback class which will receive the response.
	 * @param callbackMethodName The method in the response class which will handle the response.
	 */
	public void uploadAvatarImage(Bitmap bitmap, Object callback, String callbackMethodName){
		COApplication.get().setAvatarBitmap(bitmap);
		AvatarUploadTask avatarUploadTask = new AvatarUploadTask(bitmap, callback, callbackMethodName);
		avatarUploadTask.execute();
	}
	
	public void onAvatarUploaded(UserMethodResult userMethodResult){
		Log.d(TAG, "onAvatarUploaded");
		if(userMethodResult != null){
			ErrorMessage message = userMethodResult.getErrorMessages()[0];
			String newAvatarURL = message.getMessage();
			COApplication.get().getProfile().setAvatar(newAvatarURL);
			application.postEvent(new ProfileUpdateEvent());
		}else{
			Log.e(TAG, "onAvatarUploaded -> UserMethodResult is null");
			COApplication.get().setAvatarBitmap(null);
		}
	}
	
	/**
	 * Custom {@link AsyncTask} that uploads the given Bitmap to our server.
	 * @author Anastas Arnaudov
	 *
	 */
	public static class AvatarUploadTask extends AsyncTask<Void, Void, UserMethodResult>{

		public final String TAG = AvatarUploadTask.class.getSimpleName();
		
		private Bitmap bitmap;
		private Object callback;
		private String callbackMethodName; 
		
		public AvatarUploadTask(Bitmap bitmap, Object callback, String callbackMethodName){
			super();
			this.bitmap = bitmap;
			this.callback = callback;
			this.callbackMethodName = callbackMethodName;
		}
		
		@Override
		protected UserMethodResult doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
	        UserMethodResult result = null;
	        HttpURLConnection connection = null;
	        try {     
	        	
	        	URL url = new URL(AvatarUtils.getBaseURL() + COConstants.SERVICE_UPLOAD_AVATAR);
	        	
	            connection = (HttpURLConnection) url.openConnection();
	            connection.setDoInput(true);
	            connection.setDoOutput(true);
	            connection.setRequestMethod("POST");
	            connection.setRequestProperty("Content-Type", "image/jpeg");
	            OutputStream outputStream = connection.getOutputStream();
	            outputStream.write(COBitmapUtils.convertBitmapToByteArray(bitmap));
	            outputStream.close();
	            
	            Gson gson = new Gson();

	            if (!isCancelled()) {
	                int responseCode = connection.getResponseCode();
	                Log.d(TAG, "UploadMobileService, ResponseCode: " + String.valueOf(responseCode));
	               
	                result = gson.fromJson(new InputStreamReader(connection.getInputStream(), "UTF-8"), UserMethodResult.class);
	                Log.v(TAG, "UploadMobileService, Done parsing response.");
	            }
	        } catch (Exception e) {
	            Log.e(TAG, "Problem executing UploadMobileService", e);
	        } finally {
	            if (null != connection) {
	                connection.disconnect();
	            }
	        }
	        return result;
		}
		
	    @Override
	    protected void onCancelled() {
	        Log.v(TAG, "onCancelled");
	        invokeCallback(null);
	        super.onCancelled();
	    }
		
		@Override
		protected void onPostExecute(UserMethodResult result) {
			Log.d(TAG, "onPostExecute");
			invokeCallback(result);
		}
		
	    private void invokeCallback(UserMethodResult result){
	    	if (callback != null && callbackMethodName != null) {
	            try {
	            	Method resultMethod = callback.getClass().getMethod(callbackMethodName, UserMethodResult.class);
	            	resultMethod.invoke(callback, result);
	            } catch (Exception e) {
	                Log.e(TAG, "Cannot invoke the callback class method", e);
	            }
	        }
	    }
		
	}
	
}
