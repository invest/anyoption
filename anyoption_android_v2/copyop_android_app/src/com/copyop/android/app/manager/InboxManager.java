package com.copyop.android.app.manager;

import com.copyop.android.app.COApplication;
import com.copyop.android.app.event.InboxLightstreamerEvent;
import com.copyop.android.app.event.InboxNewUpdateEvent;
import com.copyop.android.app.model.Update;
import com.copyop.android.app.util.ConvertUtils;
import com.copyop.common.enums.base.QueueTypeEnum;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

/**
 * Manager for the INBOX Notifications.
 * @author Anastas Arnaudov
 *
 */
public class InboxManager implements Callback {

	public static final String TAG = InboxManager.class.getSimpleName();

	private static final int MESSAGE_ADD_UPDATE = 1;

	private COApplication application;
	private int unreadMessages = 0;
	
	private Handler handler;
	
	public InboxManager(COApplication application){
		Log.d(TAG, "On Create");
		this.application = application;
		application.registerForEvents(this, InboxLightstreamerEvent.class);
		handler = new Handler(Looper.getMainLooper(), this);
	}
	
	private void addUpdate(Update update){
		unreadMessages++;
		application.postEvent(new InboxNewUpdateEvent(update));
	}

	public int getUnreadMessages() {
		return unreadMessages;
	}

	public void clearUnreadMessages(){
		unreadMessages = 0;
	}

	//##########################################################################################
	//##################			EVENT BUS CALLBACKs			################################
	//##########################################################################################

	public void onEvent(InboxLightstreamerEvent event) {
		Update update = ConvertUtils.convertLSUpdateToUpdate(event.getUpdateInfo(), QueueTypeEnum.INBOX);
		Message message = handler.obtainMessage(MESSAGE_ADD_UPDATE, update);
		message.sendToTarget();
	}

	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == MESSAGE_ADD_UPDATE) {
			Update update = (Update) msg.obj;
			addUpdate(update);

			return true;
		}

		return false;
	}
	
	//##########################################################################################

	
}
