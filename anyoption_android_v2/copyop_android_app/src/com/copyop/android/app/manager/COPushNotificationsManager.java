package com.copyop.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.manager.PushNotificationsManager;
import com.copyop.android.app.COApplication.COScreen;
import com.copyop.android.app.util.COConstants;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.UpdateTypeEnum;

import android.os.Bundle;
import android.util.Log;

public class COPushNotificationsManager extends PushNotificationsManager {

	private static final String TAG = COPushNotificationsManager.class.getSimpleName();

	protected static final String EXTRA_PROFILE_ID = "user_id_dpl";

	public COPushNotificationsManager(Application application) {
		super(application);
	}

	@Override
	public void handlePush(Bundle arguments) {
		Log.d(TAG, "handlePush");
		super.handlePush(arguments);

		// Check if the screen that has to be opened is set up.
		if (getToScreen() != null) {
			return;
		}

		if (!arguments.containsKey(FeedMessage.PROPERTY_KEY_UPDATE_TYPE)) {
			return;
		}

		int updateTypeId = Integer.valueOf(arguments.getString(FeedMessage.PROPERTY_KEY_UPDATE_TYPE));

		if (UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.W_NEW_INVESTMENT) {
			// Follow push
			setToScreen(COScreen.NEWS_HOT_EXPLORE);
		} else if (UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.W_NEARLY_FULL
				|| UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.W_100_EXPIRY
				|| UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.W_NOW_AVAILABLE
				|| UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.CP_FROZE) {
			setToScreen(COScreen.USER_PROFILE);
		} else if (UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.AB_REACHED_COINS) {
			setToScreen(COScreen.COPYOP_COINS);
		}
	}

	@Override
	protected void setUpScreenArguments() {
		super.setUpScreenArguments();
		if (toScreen == COScreen.USER_PROFILE) {
			if (pushArguments.containsKey(EXTRA_PROFILE_ID)) {
				long profileId = pushArguments.getLong(EXTRA_PROFILE_ID);
				screenArguments.putLong(COConstants.EXTRA_PROFILE_ID, profileId);
				checkDeposit = true;
			}
		} else if (toScreen == COScreen.NEWS_HOT_EXPLORE) {
			if (pushArguments.containsKey(FeedMessage.PROPERTY_KEY_UPDATE_TYPE)) {
				int updateTypeId = Integer.valueOf(pushArguments
						.getString(FeedMessage.PROPERTY_KEY_UPDATE_TYPE));

				if (UpdateTypeEnum.getById(updateTypeId) == UpdateTypeEnum.W_NEW_INVESTMENT) {
					screenArguments.putBoolean(COConstants.EXTRA_OPEN_FOLLOW_DIALOG, true);
					screenArguments.putString(FeedMessage.PROPERTY_KEY_MARKET_ID,
							pushArguments.getString(FeedMessage.PROPERTY_KEY_MARKET_ID));
					screenArguments.putString(FeedMessage.PROPERTY_KEY_INV_ID,
							pushArguments.getString(FeedMessage.PROPERTY_KEY_INV_ID));
					screenArguments.putString(FeedMessage.PROPERTY_KEY_DIRECTION,
							pushArguments.getString(FeedMessage.PROPERTY_KEY_DIRECTION));
					checkDeposit = true;
				}
			}
		}
	}

	@Override
	protected Screenable getScreen(String screenArg) {
		if (screenArg == null) {
			return null;
		}

		return COScreen.getScreenByTag(screenArg);
	}

}
