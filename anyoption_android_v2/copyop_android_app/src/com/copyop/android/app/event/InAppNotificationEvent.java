package com.copyop.android.app.event;

import com.lightstreamer.ls_client.UpdateInfo;

public class InAppNotificationEvent {

	private UpdateInfo update;
	
	public InAppNotificationEvent(UpdateInfo update){
		this.setUpdate(update);
	}

	public UpdateInfo getUpdate() {
		return update;
	}

	public void setUpdate(UpdateInfo update) {
		this.update = update;
	}
	
}
