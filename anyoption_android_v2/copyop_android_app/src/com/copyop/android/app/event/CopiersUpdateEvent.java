package com.copyop.android.app.event;

/**
 * Event that is triggered when the cached copiers list is updated.
 * @author Anastas Arnaudov
 *
 */
public class CopiersUpdateEvent {

}
