package com.copyop.android.app.event;

/**
 * Event that is triggered when the user STARTS / STOPS reading the INBOX Messages.
 * @author Anastas Arnaudov
 *
 */
public class InboxReadingEvent {

	private boolean isReadingInbox;
	
	public InboxReadingEvent(boolean isReadingInbox){
		this.isReadingInbox = isReadingInbox;
	}

	public boolean isReadingInbox() {
		return isReadingInbox;
	}

}
