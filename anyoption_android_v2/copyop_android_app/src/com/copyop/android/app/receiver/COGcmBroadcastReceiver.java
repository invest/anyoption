package com.copyop.android.app.receiver;

import com.anyoption.android.app.receiver.GcmBroadcastReceiver;

import android.content.Context;

public class COGcmBroadcastReceiver extends GcmBroadcastReceiver{

    /**
     * Gets the class name of the intent service that will handle GCM messages.
     */
    @Override
    protected String getGCMIntentServiceClassName(Context context) {
        String className = "com.copyop.android.app.service.COGcmIntentService";
        return className;
    }
	
}
