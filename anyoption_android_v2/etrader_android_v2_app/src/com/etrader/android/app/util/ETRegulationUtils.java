package com.etrader.android.app.util;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.popup.AdditionalFieldsDialogFragment;
import com.etrader.android.app.popup.KnowledgeDialogFragment;
import com.etrader.android.app.popup.ThresholdDialogFragment;

import android.os.Bundle;

public class ETRegulationUtils extends RegulationUtils {

	/**
	 * Check if the user is regulated.
	 */
	public static boolean isRegulated(){
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if (regulationBase != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check if the user has made a deposit..
	 * @return True if the user has made a deposit.
	 */
	public static boolean checkRegulatedFirstDeposit(){
		User user = Application.get().getUser();
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if (user != null && regulationBase != null && user.getFirstDepositId() == 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Check if the user has made a deposit..
	 * @return True if the user has made a deposit.
	 */
	public static boolean checkFirstDeposit(){
		User user = Application.get().getUser();
		if (user.getFirstDepositId() == 0) {
			return false;
		}
		return true;
	}
	
	public static boolean checkAdditionalFieldsAndopenScreen(){
		User user = Application.get().getUser();
		boolean isHasIdNum = user.getIdNum() != null && user.getIdNum().length() > 0;
		boolean isHasStreetName = user.getStreet() != null && user.getStreet().length() > 0;
		boolean isHasStreetNumber = user.getStreetNo() != null && user.getStreetNo().length() > 0;
		boolean isHasCityName = user.getCityName() != null && user.getCityName().length() > 0;
//		boolean isHasCityId = user.getCityId() > 0;//Without this because in the WEB it is not set
		boolean isHasZipCode = user.getZipCode() != null && user.getZipCode().length() > 0;
		boolean isHasBirthDate = user.getTimeBirthDate() != null;
		boolean isHasGender = user.getGender() != null && user.getGender().length() > 0;
		
		boolean result = isHasIdNum && isHasStreetName && isHasStreetNumber && isHasCityName && isHasZipCode && isHasBirthDate && isHasGender;
		if(!result){
			Bundle bundle = new Bundle();
			bundle.putBoolean(Constants.EXTRA_MENU_BUTTON, false);
			Application.get().getFragmentManager().showDialogFragment(AdditionalFieldsDialogFragment.class, null);
		}
		
		return result;
	}
	
	/**
	 * Check if the user has made a deposit. If NOT - open the First New Card Deposit Screen.
	 * @return True if the user has made a deposit.
	 */
	public static boolean checkFirstDepositAndOpenScreen(){
		User user = Application.get().getUser();
		if (user != null && user.getFirstDepositId() == 0) {
			Application.get().postEvent(new NavigationEvent(Screen.FIRST_NEW_CARD, NavigationType.INIT));
			return false;
		}
		return true;
	}
	
	/**
	 * Check if the user has to answer the Knowledge Question.
	 * If yes - open the Knowledge Pop-up.
	 * @return True if the user has to answer the Knowledge Question.
	 */
	public static boolean checkKnowledgeQuestionAndOpenPopup(){
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if(regulationBase != null){
			if(!regulationBase.isKnowledgeQuestion()){
				Application.get().getFragmentManager().showDialogFragment(KnowledgeDialogFragment.class, null);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check if the user has to fill the Questionnaire.
	 * If yes - navigate to this Screen.
	 * @return True if the user has to fill the Questionnaire.
	 */
	public static boolean checkQuestionnaireAndOpenScreen(){
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if(regulationBase != null){
			if(regulationBase.isKnowledgeQuestion() && regulationBase.getApprovedRegulationStep() == UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER){
				Application.get().postEvent(new NavigationEvent(ETScreen.ETQUESTIONNAIRE, NavigationType.INIT));
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check if the user has to fill the Questionnaire.
	 * If yes - navigate to this Screen.
	 * @return True if the user has to fill the Questionnaire.
	 */
	public static boolean checkThresholdAndOpenPopup(){
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if(regulationBase != null){
			if(regulationBase.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP || regulationBase.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP  || regulationBase.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP){
				Bundle bundle = new Bundle();
				bundle.putBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD, false);
				Application.get().getFragmentManager().showDialogFragment(ThresholdDialogFragment.class, bundle);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check if the user has to fill the Questionnaire.
	 * If yes - navigate to this Screen.
	 * @return True if the user has to fill the Questionnaire.
	 */
	public static boolean checkLowGroupAndOpenPopup(){
		UserRegulationBase regulationBase = Application.get().getUserRegulation();
		if(regulationBase != null){
			if(regulationBase.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP){
				Bundle bundle = new Bundle();
				bundle.putBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD, false);
				Application.get().getFragmentManager().showDialogFragment(ThresholdDialogFragment.class, bundle);
				return true;
			}
		}
		return false;
	}
	
}
