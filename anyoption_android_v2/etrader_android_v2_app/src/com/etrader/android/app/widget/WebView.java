package com.etrader.android.app.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class WebView extends android.webkit.WebView{

	public interface OnScrollChangeListener{
		public void onScrollChanged(int horizontalScroll, int verticalScroll);
	}
	
	public static final int NONE 		= 0;
	public static final int QUICKSCROLL = 1;
	
	private OnScrollChangeListener onScrollChangeListener;
	private float scrollbarTop;
	private float scrollbarBtm;
	private float scrollOffset;
	private int mode;
	private float previousY;
	private boolean isFastScroll;
	
	
	public WebView(Context context) {
		super(context);
	}
	
	public WebView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public WebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onScrollChanged(int horizontalScroll, int verticalScroll, int oldl, int oldt) {
		super.onScrollChanged(horizontalScroll, verticalScroll, oldl, oldt);
		if(getOnScrollChangeListener() != null){
			getOnScrollChangeListener().onScrollChanged(horizontalScroll, verticalScroll);
		}
	}

	public OnScrollChangeListener getOnScrollChangeListener() {
		return onScrollChangeListener;
	}

	public void setOnScrollChangeListener(OnScrollChangeListener onScrollChangeListener) {
		this.onScrollChangeListener = onScrollChangeListener;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!isFastScroll){
			return false;
		}
		
	    switch (event.getAction() & MotionEvent.ACTION_MASK) { 
	        case MotionEvent.ACTION_DOWN:
//	            if(canScrollVertically(0) || canScrollVertically(1)){
	                scrollbarTop = (float)computeVerticalScrollExtent()/computeVerticalScrollRange()*computeVerticalScrollOffset();
	                scrollbarBtm = (float)computeVerticalScrollExtent()/computeVerticalScrollRange()*(computeVerticalScrollExtent()+computeVerticalScrollOffset());
	                if((scrollbarBtm-scrollbarTop) < 50){
	                    scrollbarTop = scrollbarTop-20;
	                    scrollbarBtm = scrollbarBtm+20;
	                }
					scrollOffset = getHeight() - event.getY() - scrollbarTop;
					mode = QUICKSCROLL;
					return true;
//	            }
//	            break;
	        case MotionEvent.ACTION_UP:
	            mode = NONE; 
	            break;
	        case MotionEvent.ACTION_MOVE: 
	            if(mode==QUICKSCROLL){ 
	            	boolean isFingerMovingDown = previousY > event.getY();
	            	
	                scrollbarTop = (float)computeVerticalScrollExtent()/computeVerticalScrollRange()*computeVerticalScrollOffset();
	                scrollbarBtm = (float)computeVerticalScrollExtent()/computeVerticalScrollRange()*(computeVerticalScrollExtent()+computeVerticalScrollOffset());
	                if((scrollbarBtm-scrollbarTop)<50){
	                    scrollbarTop = scrollbarTop-20;
	                    scrollbarBtm = scrollbarBtm+20;
	                }
	                int scrollto = Math.round((float)computeVerticalScrollRange()/computeVerticalScrollExtent()*(getHeight() - event.getY() - scrollOffset));
	                
	                if(scrollto > -1 && (scrollto + computeVerticalScrollExtent()) < computeVerticalScrollRange() + 1){
	                	scrollTo(0,scrollto);
	                	//Check if we are near the top or bottom:
	                	if(!isFingerMovingDown){
	                		//Scrolling Down:
	                		if(getScrollY() < 1000){
	                			scrollTo(0, 0);
	                		}
	                	}else{
	                		//Scrolling Up:
	                		int bottom = (int) Math.floor(getContentHeight() * getScale());
	                		if(getScrollY() > bottom - 2000){
	                			scrollTo(0, bottom);
	                		}
	                	}
	                }
	                previousY = event.getY();
	                return true;
	            }
	            break; 
	    } 
	    return false; 
	}

	public boolean isFastScroll() {
		return isFastScroll;
	}

	public void setFastScroll(boolean isFastScroll) {
		this.isFastScroll = isFastScroll;
	}
	
}
