package com.etrader.android.app.widget;


import com.etrader.android.app.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ETGenderButton extends LinearLayout{

	private boolean isFemale;
	private TextView femaleText;
	private TextView maleText;
	private ImageView swithchImageView;
	
	public ETGenderButton(Context context) {
		super(context);
		init(context, null, 0);
	}
	
	public ETGenderButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	
	public ETGenderButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}
	
	public void init(Context context, AttributeSet attrs, int defStyle) {
		View rootView = View.inflate(context, R.layout.switch_button_layout, this);
		femaleText = (TextView) rootView.findViewById(R.id.switch_button_left_text);
		maleText = (TextView) rootView.findViewById(R.id.switch_button_right_text);
		swithchImageView = (ImageView) rootView.findViewById(R.id.switch_button_image_view);
		
		OnClickListener switchClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeState(!isFemale);
			}
		};
		
		rootView.setOnClickListener(switchClickListener);
		femaleText.setOnClickListener(switchClickListener);
		maleText.setOnClickListener(switchClickListener);
		swithchImageView.setOnClickListener(switchClickListener);
		
		//Set the default state to be Male:
		changeState(false);
	}
	
	public void changeState(boolean isFemale){
		this.isFemale = isFemale;
		int activeColor = getResources().getColor(R.color.form_item_hint_focused);
		int inactiveColor = getResources().getColor(R.color.form_item_hint);
		Drawable leftDrawable = getResources().getDrawable(R.drawable.switch_left);
		Drawable rightDrawable = getResources().getDrawable(R.drawable.switch_right);
		if(isFemale){
			femaleText.setTextColor(activeColor);
			maleText.setTextColor(inactiveColor);
			swithchImageView.setImageDrawable(leftDrawable);
		}else{
			maleText.setTextColor(activeColor);
			femaleText.setTextColor(inactiveColor);
			swithchImageView.setImageDrawable(rightDrawable);
		}
	}
	
	public boolean isMale(){
		return !isFemale;
	}
	
}
