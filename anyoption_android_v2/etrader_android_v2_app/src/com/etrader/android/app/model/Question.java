package com.etrader.android.app.model;

public class Question {
	
	private long id;
	private String description;
	private Answer[] answers;
	
	public Question(){}
	
	public Question(long id, String description, Answer...answers){
		this.id = id;
		this.description = description;
		this.answers = answers;
		for(Answer answer : answers){
			answer.setQuestion(this);
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Answer[] getAnswers() {
		return answers;
	}
	public void setAnswers(Answer[] answers) {
		this.answers = answers;
	}
	
	public int getAnswerPosition(Answer answer){
		int position = -1;
		if(answer != null){
			for(int i=0; i<answers.length; i++){
				Answer a = answers[i];
				if(a.getId() == answer.getId()){
					position = i;
					break;
				}
			}
		}
		return position;
	}
	
	public Answer getAnswerForId(Long id){
		Answer answer = null;
		if(id != null){
			for (int i = 0; i < answers.length; i++) {
				Answer a = answers[i];
				if (a.getId() == id) {
					answer = a;
					break;
				}
			}
		}
		return answer;
	}
}
