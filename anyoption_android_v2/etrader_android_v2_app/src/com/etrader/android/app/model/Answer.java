package com.etrader.android.app.model;

public class Answer {

	private long id;
	private String description;
	private Question question;
	
	public Answer(){}
	
	public Answer(long id, String description){
		this.id = id;
		this.description = description;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	
}
