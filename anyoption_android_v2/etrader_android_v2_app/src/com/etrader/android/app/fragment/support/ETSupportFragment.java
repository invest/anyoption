package com.etrader.android.app.fragment.support;

import com.anyoption.android.app.fragment.support.SupportFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormSimpleSelector;
import com.anyoption.beans.base.Contact;
import com.etrader.android.app.ETApplication;
import com.etrader.android.app.R;

import android.os.Bundle;

public class ETSupportFragment extends SupportFragment {

	private FormSimpleSelector phonePrefixSelector;
	
	/**
	 * Use this method to get a new instance of the fragment.
	 * 
	 * @return
	 */
	public static ETSupportFragment newInstance(Bundle bundle) {
		ETSupportFragment etSupportFragment = new ETSupportFragment();
		etSupportFragment.setArguments(bundle);
		return etSupportFragment;
	}

	@Override
	protected void init() {
		countryId = Constants.COUNTRY_ISRAEL_ID;
	}
	
	@Override
	protected void setupPhonePrefixSelector(){
		phonePrefixSelector = (FormSimpleSelector) rootView.findViewById(R.id.support_phone_prefix_button);
		phonePrefixSelector.setItems(ETApplication.get().getMobilePrefixes());
		form.addItem(phonePrefixSelector);
	}
	
	@Override
	protected void setContactPhone(Contact contact){
		contact.setPhone(phonePrefixSelector.getValue() + phoneField.getValue());
	}
	
	@Override
	protected void setupUserFieldsValidators() {
		firstNameFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_ONLY);
		lastNameFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_ONLY);
		emailFormEditText.addValidator(ValidatorType.EMPTY, ValidatorType.EMAIL);
		phoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE,ValidatorType.PHONE_MINIMUM_CHARACTERS);
	}

}
