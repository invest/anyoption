package com.etrader.android.app.fragment.login_register;

import com.anyoption.android.app.fragment.login_register.ForgotPasswordFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.form.FormSimpleSelector;
import com.anyoption.beans.base.Register;
import com.etrader.android.app.ETApplication;
import com.etrader.android.app.R;

import android.os.Bundle;

public class ETForgotPasswordFragment extends ForgotPasswordFragment {

	private FormSimpleSelector phonePrefixSelector;
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static ETForgotPasswordFragment newInstance(Bundle bundle){
		ETForgotPasswordFragment etForgotPasswordFragment = new ETForgotPasswordFragment();
		etForgotPasswordFragment.setArguments(bundle);
		return etForgotPasswordFragment;
	}
	
	@Override
	protected void init() {
		countryId = Constants.COUNTRY_ISRAEL_ID;
	}

	@Override
	protected void setupPhonePrefixButton(){
		phonePrefixSelector = (FormSimpleSelector) view.findViewById(R.id.forgot_pass_phone_prefix_button);
		phonePrefixSelector.setItems(ETApplication.get().getMobilePrefixes());
		form.addItem(phonePrefixSelector);
	}
	
	@Override
	protected void setupRequestPhone(Register user){
		user.setMobilePhonePrefix(application.getCountriesMap().get(Constants.COUNTRY_ISRAEL_ID).getPhoneCode());
		//Remove the zero:
		String prefixNoZero = phonePrefixSelector.getValue().replace(String.valueOf(phonePrefixSelector.getValue().charAt(0)), "");
		user.setMobilePhone(prefixNoZero + phoneEditText.getValue());
	}
	
}
