package com.etrader.android.app.fragment.my_account.banking.deposit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.DepositFailEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.my_account.banking.deposit.FirstNewCardFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormEditText.FocusChangeListener;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.CardMethodRequest;
import com.anyoption.json.requests.CcDepositMethodRequest;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.results.CardMethodResult;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.TransactionMethodResult;
import com.etrader.android.app.R;
import com.etrader.android.app.popup.AgreementDialogFragment;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.util.ETRegulationUtils;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ETFirstNewCardFragment extends FirstNewCardFragment {

	private static final String TAG = ETFirstNewCardFragment.class.getSimpleName();

	public static ETFirstNewCardFragment newInstance(Bundle bundle) {
		ETFirstNewCardFragment newCardFragment = new ETFirstNewCardFragment();
		newCardFragment.setArguments(bundle);
		return newCardFragment;
	}

	private View 			mainView;
	private View 			funnelView;
	private FormEditText 	amountEditText;
	private ImageView 		logoCcImageView;
	private FormDatePicker 	expiryDatePicker;
	private FormEditText 	cvvEditText;
	private FormEditText 	cardNumberEditText;
	private FormEditText 	cardHolderNameEditText;
	private FormEditText 	cardHolderIdEdittext;
	private RequestButton 	depositButton;
	private Form 			form;
	private boolean 		isFirstDeposit;
	private boolean			isFirstDepositChecked;
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		checkFirstDepositRequest();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.first_new_card_layout, container, false);	

		funnelView = mainView.findViewById(R.id.new_card_funnel_steps_layout); 
		funnelView.setVisibility((ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit()) ? View.VISIBLE : View.GONE);
		
		View scanButton = mainView.findViewById(R.id.scan_button);
		scanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				scanCard();
			}
		});
		
		//Amount:
		amountEditText = (FormEditText) mainView.findViewById(R.id.new_card_amount_edit_text); 
		amountEditText.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
		amountEditText.setFieldName(Constants.FIELD_AMOUNT);
        if(application.getUser().getPredefinedDepositAmount() != null){
        	amountEditText.setValue(application.getUser().getPredefinedDepositAmount());
        }
		
		//Card LOGO Image:
		logoCcImageView = (ImageView) mainView.findViewById(R.id.new_card_logo_cc_image_view);
		
		//Card Number:
		cardNumberEditText = (FormEditText) mainView.findViewById(R.id.new_card_number_edit_text); 
		cardNumberEditText.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS, ValidatorType.LONG);
		cardNumberEditText.setFieldName(Constants.FIELD_CARD_NUMBER);
		cardNumberEditText.setAddTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// Logo CC by type
		        Drawable imgCard = DrawableUtils.getImageByCCType(Utils.getCcTypeByNumber(cardNumberEditText.getValue()));
		        if (null != imgCard) {
		        	logoCcImageView.setImageDrawable(imgCard);
				}
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				cardNumberEditText.setupHint(true);
			}
		});
		cardNumberEditText.getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		cardNumberEditText.addOnEditorActionListener(new OnEditorActionListener() {
			
		    @Override
		    public boolean onEditorAction(android.widget.TextView view, int actionId, KeyEvent event) {
		        int result = actionId & EditorInfo.IME_MASK_ACTION;
		        switch(result) {
			        case EditorInfo.IME_ACTION_NEXT:
			        	Log.d(TAG, "IME_ACTION_NEXT");
			        	ScreenUtils.hideKeyboard(cardNumberEditText.getEditText());
			        	expiryDatePicker.performClick();
			            break;
		        }
		        return true;
		    }
	
		});

		//Expire Date:
		expiryDatePicker = (FormDatePicker) mainView.findViewById(R.id.new_card_expiry_date_picker);
		expiryDatePicker.setMonthAndYearOnly(true);		
		Calendar initTime = Calendar.getInstance();
		initTime.set(Calendar.MONTH, Calendar.JANUARY);
		initTime.set(Calendar.YEAR, initTime.get(Calendar.YEAR) + 2);
		expiryDatePicker.setInitialDate(initTime, false);
		expiryDatePicker.addValidator(ValidatorType.EMPTY, ValidatorType.CC_EXP_DATE);
		expiryDatePicker.setFieldName(Constants.FIELD_EXPIRY_DATE);
		
		//CVV:
		cvvEditText = (FormEditText) mainView.findViewById(R.id.new_card_cvv_edit_text); 
		cvvEditText.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS, ValidatorType.CC_PASS_MINIMUM_CHARACTERS);
		cvvEditText.setFieldName(Constants.FIELD_CVV);
		cvvEditText.addFocusChangeListener(new FocusChangeListener() {
			@Override
			public void onFocusChanged(boolean hasFocus) {
				if(hasFocus){
					logoCcImageView.setImageDrawable(getResources().getDrawable(R.drawable.cvv));
				}else{
					// Logo CC by type
			        Drawable imgCard = DrawableUtils.getImageByCCType(Utils.getCcTypeByNumber(cardNumberEditText.getValue()));
			        if (null != imgCard) {
			        	logoCcImageView.setImageDrawable(imgCard);
					}
				}
			}
		});

		//Card Holder Name:
		cardHolderNameEditText = (FormEditText) mainView.findViewById(R.id.new_card_holder_name_edit_text);
		cardHolderNameEditText.addValidator(ValidatorType.EMPTY);	
		cardHolderNameEditText.setFieldName(Constants.FIELD_HOLDER_NAME);

		//Card Holder Id:
		cardHolderIdEdittext = (FormEditText) mainView.findViewById(R.id.new_card_holder_id_edit_text);
		cardHolderIdEdittext.setVisibility(View.VISIBLE);
		cardHolderIdEdittext.addValidator(ValidatorType.EMPTY, ValidatorType.ID_NUMBER);

		//Submit button:
		depositButton = (RequestButton) mainView.findViewById(R.id.new_card_deposit_button);
		depositButton.setEnabled(isFirstDepositChecked);
		if (application.getUser() != null && application.getUser().getFirstDepositId() == 0L) {
			depositButton.setText(R.string.newCardScreenTradeNowButton);
		}
		depositButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(form.checkValidity()){
					if(!application.getUser().isAcceptedTerms()){
						application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, null);
					}
					else if(!ETRegulationUtils.isRegulated() && !ETRegulationUtils.checkFirstDeposit() && !ETRegulationUtils.checkAdditionalFieldsAndopenScreen()){
						
					}
					else{
						depositButton.startLoading(new InitAnimationListener() {
							@Override
							public void onAnimationFinished() {
								form.submit();
							}
						});
					}
				}
			}
		});
		
		//Other funding methods link:
		TextView goToOtherFundingMethodsView = (TextView) mainView.findViewById(R.id.deposit_other_funding_methods_text_view);
		TextUtils.underlineText(goToOtherFundingMethodsView);
		goToOtherFundingMethodsView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				Bundle bundle = new Bundle();
				bundle.putBoolean(Constants.EXTRA_IS_FROM_REG_FUNNEL, true);
				application.postEvent(new NavigationEvent(Screen.DEPOSIT_MENU, NavigationType.DEEP, bundle));
			} 
		});	
		
		form = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				insertCardRequest();
			}
		});
		
		form.addItem(amountEditText, cardNumberEditText, expiryDatePicker, cvvEditText, cardHolderNameEditText, cardHolderIdEdittext);
		
		return mainView;
	}
	
	@Override
	protected void handleScanResult(io.card.payment.CreditCard scanResult){
		//Card number:
		cardNumberEditText.setValue(scanResult.cardNumber);
        
        //Expire Date:
        Calendar expiryTime = Calendar.getInstance();
        expiryTime.set(Calendar.MONTH, scanResult.expiryMonth);
        expiryTime.set(Calendar.YEAR, scanResult.expiryYear);
        expiryDatePicker.setInitialDate(expiryTime, true);
        
        //CVV:
        cvvEditText.setValue(scanResult.cvv);
	}
	
	@Override
	public String getAmount() {
		double amount = Double.parseDouble(amountEditText.getValue());
		return AmountUtil.getFormattedAmount(amount, application.getCurrency());
	}
	
	private void handleSuccessfulDeposit(TransactionMethodResult result) {
		Log.d(TAG, "handleSuccessfulDeposit");
		User user = application.getUser();

		double newBalance = result.getBalance();
		double amount = AmountUtil.getDoubleAmount(AmountUtil.parseAmount(getAmount(),user.getCurrency()));
		double dollarAmount = result.getDollarAmount();

		// Update the user with new balance.
		user.setBalance(AmountUtil.getLongAmount(newBalance));
		user.setBalanceWF(AmountUtil.getFormattedAmount(user.getBalance(), user.getCurrency()));
		if (application.isRegulated() && !RegulationUtils.isAfterFirstDeposit()) {
			RegulationUtils.setAfterFirstDeposit();
		}
		application.setUser(user);
		///////////////////////////////////////

		application.postEvent(new DepositEvent(amount, dollarAmount, result.isFirstDeposit()));

		SimpleDateFormat dateFormat = new SimpleDateFormat(TRANSACTION_DATE_FORMAT_PATTERN,application.getLocale());
		String dateFormatted = dateFormat.format(new Date(TimeUtils.getCurrentServerTime()));
		application.getNotificationManager().showNotificationDepositOK(getAmount(), dateFormatted, result.getTransactionId());

		// Navigate to next screen:
		if(!ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen() && !ETRegulationUtils.checkThresholdAndOpenPopup()){
			NavigationEvent navigationEvent = new NavigationEvent(application.getHomeScreen(), NavigationType.INIT);
			application.postEvent(navigationEvent);
		}
		
	}
	
	///////////////				REQUESTs and CALLBACKs			//////////////////////////
	
	private void checkFirstDepositRequest() {
		Log.d(TAG, "checkFirstDepositRequest");
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		request.setUserId(Application.get().getUser().getId());
		if(!Application.get().getCommunicationManager().requestService(ETFirstNewCardFragment.this, "checkFirstDepositCallBack", Constants.SERVICE_GET_FIRST_EVER_DEPOSIT_CHECK, request , FirstEverDepositCheckResult.class)){
			isFirstDepositChecked = true;
			if(depositButton != null){
				depositButton.setEnabled(true);
			}
		}	
	}
	
	public void checkFirstDepositCallBack(Object resultObject) {
		Log.d(TAG, "firstDepositCallBack");
		isFirstDepositChecked = true;
		if(depositButton != null){
			depositButton.setEnabled(true);
		}
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObject;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			isFirstDeposit = result.isFirstEverDeposit();
//			if(funnelView != null){
//				funnelView.setVisibility((ETRegulationUtils.isRegulated() && isFirstDeposit) ? View.VISIBLE : View.GONE);
//			}
			Log.d(TAG, "Is First Deposit = " + isFirstDeposit);
		}else{
			ETErrorUtils.handleResultError(result);
		}
	}
	
	private void insertCardRequest() {
		Log.d(TAG, "insertCardRequest");
		CardMethodRequest request = new CardMethodRequest();           
        CreditCard card = new CreditCard();
        StringTokenizer expiryDate = new StringTokenizer(expiryDatePicker.getValue(), FormDatePicker.DATE_FORMAT_DELIMITER);

        card.setHolderId(Constants.NO_ID_NUM);
        card.setCcNumber(Long.valueOf(cardNumberEditText.getValue()));
        card.setCcPass(cvvEditText.getValue());
        card.setHolderName(cardHolderNameEditText.getValue());
		card.setExpMonth(expiryDate.nextElement().toString());
		card.setExpYear(expiryDate.nextElement().toString().substring(2));
		card.setTypeId(Utils.getCcTypeByNumber(cardNumberEditText.getValue()));
		card.setHolderId(cardHolderIdEdittext.getValue());

        request.setCard(card);
        
		if(!Application.get().getCommunicationManager().requestService(ETFirstNewCardFragment.this, "insertCardCallBack", Constants.SERVICE_INSERT_CARD, request , CardMethodResult.class, false, false)){
			depositButton.stopLoading();
		}
		
	}
	
	public void insertCardCallBack(Object resultObj){
		Log.d(TAG, "insertCardCallBack");
		CardMethodResult result = (CardMethodResult) resultObj;
		if(result != null){
			if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				Log.d(TAG, "Insert card succefully");
				
				insertDepositRequest(result);
			} else {
				Log.d(TAG, "Error inserting card failed");
				depositButton.stopLoading();
				ErrorUtils.displayFieldError(form, result);
			}
		}else{
			Log.d(TAG, "Error inserting crad failed -> result is NULL");
			depositButton.stopLoading();
			ETErrorUtils.handleResultError(result);
		}
	}
	
	private void insertDepositRequest(CardMethodResult result){
		Log.d(TAG, "insertDepositRequest");
		CcDepositMethodRequest request = new CcDepositMethodRequest();
		request.setCcPass(cvvEditText.getValue());
		request.setAmount(amountEditText.getValue());
		request.setCardId(result.getCard().getId());
		request.setError(result.getError());
		request.setFirstNewCardChanged(false); //For repeat failure  

		if(!Application.get().getCommunicationManager().requestService(ETFirstNewCardFragment.this, "insertDepositCallBack", Constants.SERVICE_INSERT_DEPOSIT_CARD, request , TransactionMethodResult.class, false,false)){
			depositButton.stopLoading();
		}
	}
	
	public void insertDepositCallBack(Object resultObj){
		Log.d(TAG, "insertDepositCallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if(result != null){
			if (result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
				Log.d(TAG, "Insert deposit succefully");
				handleSuccessfulDeposit(result);
			} else {
				Log.d(TAG, "Insert deposit failed");
				depositButton.stopLoading();
				ETErrorUtils.handleResultError(result);
			}
		}else{
			Log.d(TAG, "Insert deposit failed -> result is NULL");
			depositButton.stopLoading();
			ErrorUtils.displayError();
			application.postEvent(new DepositFailEvent());
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////
	

}
