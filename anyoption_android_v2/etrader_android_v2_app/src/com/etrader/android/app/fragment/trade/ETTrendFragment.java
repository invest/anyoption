package com.etrader.android.app.fragment.trade;

import com.anyoption.android.app.fragment.trade.TrendFragment;

import android.os.Bundle;
import android.util.Log;

public class ETTrendFragment extends TrendFragment {

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ETTrendFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ETTrendFragment.");
		ETTrendFragment fragment = new ETTrendFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void subscribeSocialFeedLightstreamer() {
		// Do not subscribe!
	}

	@Override
	protected void unsubscribeSocialFeedLightstreamer() {
		// Do nothing.
	}

}
