package com.etrader.android.app.fragment.launch;

import com.anyoption.android.app.fragment.launch.WelcomeFragment;

import android.os.Bundle;
import android.util.Log;

public class ETWelcomeFragment extends WelcomeFragment {

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static ETWelcomeFragment newInstance(Bundle bundle){
		Log.d(TAG, "Created new ETWelcomeFragment.");
		ETWelcomeFragment welcomeFragment = new ETWelcomeFragment();
		welcomeFragment.setArguments(bundle);	
		return welcomeFragment;
	}
	
	@Override
	protected void setupLanguageButton() {}

	
}
