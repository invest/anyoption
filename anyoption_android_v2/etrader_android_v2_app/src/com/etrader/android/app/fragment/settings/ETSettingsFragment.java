package com.etrader.android.app.fragment.settings;

import com.anyoption.android.app.fragment.settings.SettingsFragment;

import android.os.Bundle;

public class ETSettingsFragment extends SettingsFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static ETSettingsFragment newInstance(Bundle bundle){
		ETSettingsFragment etSettingsFragment = new ETSettingsFragment();
		etSettingsFragment.setArguments(bundle);
		return etSettingsFragment;
	}
	
	@Override
	protected void setupLanguageButton(){
		//No language button
	}
	
}
