package com.etrader.android.app.fragment.login_register;

import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.login_register.LoginFragment;
import com.anyoption.android.app.manager.LoginManager;
import com.etrader.android.app.manager.ETLoginManager;

import android.os.Bundle;

/**
 * A simple {@link BaseFragment} subclass. 
 * <p>It manages the Login screen and functionality.
 * 
 */
public class ETLoginFragment extends LoginFragment {
	public static final String TAG = ETLoginFragment.class.getSimpleName();

	public static ETLoginFragment newInstance(Bundle bundle){
		ETLoginFragment eTLoginFragment = new ETLoginFragment();
		eTLoginFragment.setArguments(bundle);
		return eTLoginFragment;
	}
	
	protected LoginManager setupLoginManager(){
		return new ETLoginManager();
	}
	
}