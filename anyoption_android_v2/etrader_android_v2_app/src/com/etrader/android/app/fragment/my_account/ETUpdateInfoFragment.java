package com.etrader.android.app.fragment.my_account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.anyoption.android.app.fragment.my_account.UpdateInfoFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.android.app.widget.form.FormRadioButton;
import com.anyoption.android.app.widget.form.FormSelector.ItemSelectedListener;
import com.anyoption.android.app.widget.form.FormSimpleSelector;
import com.anyoption.common.beans.base.User;
import com.etrader.android.app.ETApplication;
import com.etrader.android.app.R;
import com.etrader.android.app.widget.form.FormCitySelector;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;

public class ETUpdateInfoFragment extends UpdateInfoFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static ETUpdateInfoFragment newInstance(Bundle bundle){
		ETUpdateInfoFragment etUpdateInfoFragment = new ETUpdateInfoFragment();	
		etUpdateInfoFragment.setArguments(bundle);
		return etUpdateInfoFragment;
	}

	private FormCitySelector citySelector;
	
	private View landPhoneValidView;
	private FormSimpleSelector landPrefixSelector;
	private FormEditText landPhoneField;
	
	@Override
	protected void setupForm(){
		
		// first name
		firstNameField = (FormEditText) rootView.findViewById(R.id.update_first_name_edittext);
		firstNameField.addValidator(ValidatorType.VACUOUS);
		firstNameField.getEditText().setText(user.getFirstName());
		firstNameField.setEditable(false);
		
		// last name
		lastNameField = (FormEditText) rootView.findViewById(R.id.update_last_name_edittext);
		lastNameField.addValidator(ValidatorType.VACUOUS);
		lastNameField.getEditText().setText(user.getLastName());
		lastNameField.setEditable(false);
		
		// user name
		userName = (FormEditText) rootView.findViewById(R.id.update_username_edittext);
		userName.getEditText().setText(user.getUserName());
		userName.addValidator(ValidatorType.VACUOUS);
		userName.setEditable(false);
		
		// birth day
		birthDateField = (FormDatePicker) rootView.findViewById(R.id.update_formDatePicker);
		if (user.getTimeBirthDate() != null) {
			birthDateField.setEditable(false);
			SimpleDateFormat formatDate = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR,application.getLocale());
			birthDateField.setValue(formatDate.format(user.getTimeBirthDate()));
			birthDateField.addValidator(ValidatorType.VACUOUS);
		} else {
			birthDateField.addValidator(ValidatorType.VACUOUS,ValidatorType.BIRTH_DAY);
			birthDateField.setMin18(true);
		}
		
		// gender
		String genederStr = user.getGender();
		genderMan = (FormRadioButton) rootView.findViewById(R.id.update_formRadioButtonMan);
		genderWoman = (FormRadioButton) rootView.findViewById(R.id.update_formRadioButtonWoman);
		if (genederStr != null && !genederStr.equals("")) {
			genderMan.setEditable(false);
			genderWoman.setEditable(false);
			if (genederStr.equalsIgnoreCase("M")) {
				genderMan.setChecked(true);
			} else { 
				// Female
				genderWoman.setChecked(true);
			}
		}

		// city selector
		citySelector = (FormCitySelector) rootView.findViewById(R.id.update_city_selector);
		citySelector.setValue(application.getCityByName(application.getUser().getCityName()));
		
		// street
		streetField = (FormEditText) rootView.findViewById(R.id.update_street_edittext);
		streetField.getEditText().setText(user.getStreet());
		streetField.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_HEBREW);
		
		// house
		houseNumber = (FormEditText) rootView.findViewById(R.id.update_house_edittext);
		houseNumber.getEditText().setText(user.getStreetNo());
		houseNumber.addValidator(ValidatorType.VACUOUS);
		
		// zip
		zipField = (FormEditText) rootView.findViewById(R.id.update_zip_edittext);
		zipField.getEditText().setText(user.getZipCode());
		zipField.addValidator(ValidatorType.VACUOUS);
		
		// mobile phone
		mobilePhoneField = (FormEditText) rootView.findViewById(R.id.update_mobile_edittext);
		mobilePhoneField.setFieldName(Constants.FIELD_MOBILE_PHONE);
		mobilePhoneField.addValidator(ValidatorType.EMPTY, ValidatorType.PHONE,ValidatorType.PHONE_ISRAEL);
		mobilePhoneField.getEditText().setText(user.getMobilePhone());
		
		// mobile validity check image
		landPhoneValidView = rootView.findViewById(R.id.update_land_phone_prefix_check);
		
		// land phone prefix
		landPrefixSelector = (FormSimpleSelector) rootView.findViewById(R.id.update_land_phone_prefix_button);
		landPrefixSelector.setValue(user.getLandLinePhonePrefix());		
		landPrefixSelector.setItems(ETApplication.get().getLandPhonePrefixes());
		landPrefixSelector.addItemListener(new ItemSelectedListener<String>() {
			@Override
			public void onItemSelected(String selectedItem) {
				landPhoneField.requestFocus();
			}
		});
		landPrefixSelector.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					landPrefixSelector.performClick();
					InputMethodManager input = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
					if (input != null) {
						input.hideSoftInputFromWindow(landPrefixSelector.getWindowToken(), 0);
					}
				}
			}
		});
		
		// land phone
		landPhoneField = (FormEditText) rootView.findViewById(R.id.update_land_phone_edittext);
		landPhoneField.addValidator(ValidatorType.VACUOUS);
		landPhoneField.getEditText().setText(user.getLandLinePhoneSuffix());
		landPhoneField.setOkViewVisible(false);
		landPhoneField.addFocusChangeListener(new FormEditText.FocusChangeListener() {
			@Override
			public void onFocusChanged(boolean hasFocus) {
				if (!hasFocus) {
					boolean phoneValid = landPhoneField.checkValidity();
					boolean prefixValid = landPrefixSelector.checkValidity();
					if (phoneValid && prefixValid) {
						landPhoneValidView.setVisibility(View.VISIBLE);
					} else {
						landPhoneValidView.setVisibility(View.INVISIBLE);
					}
				}
			}
		});
		
		// email
		emailField = (FormEditText) rootView.findViewById(R.id.update_email_edittext);
		emailField.addValidator(ValidatorType.VACUOUS);
		emailField.getEditText().setText(user.getEmail());
		// Check whether it is an 'OLD' user (which has EMAIL field separate
		// from the USERNAME field)
		if (user.getUserName().equalsIgnoreCase(user.getEmail())) {
			// The User is 'NEW' (The USERNAME is his email):
			emailField.setEditable(false);
		} else {
			// The User is 'OLD' so he has separate email:
			emailField.setEditable(true);
		}

		form.addItem(firstNameField, lastNameField, emailField, mobilePhoneField,landPrefixSelector, landPhoneField, birthDateField, streetField, houseNumber, zipField);
	
	}

	@Override
	protected void setupUpdatedUser(User user) {
		if (birthDateField.getValue() != null && !birthDateField.getValue().equalsIgnoreCase("")) {			
			SimpleDateFormat sdf = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()); 
			Date timeBirthDate = null;
			try {			
				timeBirthDate = sdf.parse(birthDateField.getValue());
			} catch (ParseException e) {			
				timeBirthDate = null;
			}
			user.setTimeBirthDate(timeBirthDate);
		}
		if (user.getGender() == null || user.getGender().equalsIgnoreCase("")) {
			if (genderMan.isChecked()) {
				user.setGender("M");	
			} else if (genderWoman.isChecked()) {
				user.setGender("F");
			}
		}
		user.setStreet(streetField.getValue());
		user.setCityName(citySelector.getValue().getName());
		user.setCityId(citySelector.getValue().getId());
		user.setZipCode(zipField.getValue());
		user.setStreetNo(houseNumber.getValue());
		String mobilePrefix = ((String)mobilePhoneField.getValue()).substring(0, 3);
		String mobile		= ((String)mobilePhoneField.getValue()).substring(3);
		user.setMobilePhonePrefix(mobilePrefix);
		user.setMobilePhoneSuffix(mobile);
		user.setMobilePhone(mobilePrefix + mobile);
		
		//TODO In the old app. it is most likely wrong????
		user.setLandLinePhonePrefix(landPrefixSelector.getValue());
		user.setLandLinePhoneSuffix(landPhoneField.getValue());
		user.setLandLinePhoneCD(user.getLandLinePhonePrefix() + user.getLandLinePhoneSuffix());
		////////////////
		user.setEmail(emailField.getValue());
		
		// The logic is reversed because of the tricky question:
		user.setIsContactByEmail(isContactByMail.isChecked() ? 0 : 1);
		user.setIsContactBySMS(isContactBySMS.isChecked() ? 0 : 1);
	}
	
}
