package com.etrader.android.app.fragment.login_register;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.etrader.android.app.R;
import com.etrader.android.app.widget.WebView;
import com.etrader.android.app.widget.WebView.OnScrollChangeListener;

import android.R.anim;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.WebSettings.PluginState;
import android.widget.ScrollView;
import android.webkit.WebViewClient;

public class AgreementTermsFragment extends NavigationalFragment implements OnClickListener, OnScrollChangeListener {

	public static final String TAG = AgreementTermsFragment.class.getSimpleName();
	
	private View tabTerms;
	private View tabAgreement;
	private WebView webView;
	private View loadingView;

	@Override
	protected Screenable setupScreen() {
		return Screen.AGREEMENT;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static AgreementTermsFragment newInstance(Bundle bundle) {
		AgreementTermsFragment fragment = new AgreementTermsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_agreement_terms, container, false);
		tabAgreement = rootView.findViewById(R.id.agreement_terms_tab_agreement);
		tabTerms = rootView.findViewById(R.id.agreement_terms_tab_terms);

		loadingView = rootView.findViewById(R.id.agreement_terms_loading_view);

		webView = (WebView) rootView.findViewById(R.id.agreement_terms_web_view);
		webView.setFastScroll(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setOnScrollChangeListener(this);
		webView.setWebViewClient(new MyWebViewClient());
		if (Build.VERSION.SDK_INT <= 18) {
			webView.getSettings().setPluginState(PluginState.ON);
		}
		pressTab(tabAgreement);
		
		tabAgreement.setOnClickListener(this);
		tabTerms.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		pressTab(v);
	}

	private void pressTab(View v) {
		setupLoading(true);
		int colorActive = getResources().getColor(R.color.agreement_terms_tab_active);
		int colorInactive = getResources().getColor(R.color.agreement_terms_tab_inactive);
		if (v == tabAgreement) {
			tabAgreement.setBackgroundColor(colorActive);
			tabTerms.setBackgroundColor(colorInactive);
			openContent(true, false);
		} else if (v == tabTerms) {
			tabTerms.setBackgroundColor(colorActive);
			tabAgreement.setBackgroundColor(colorInactive);
			openContent(false, true);
		}
	}

	private void openContent(boolean isAgreement, boolean isTerms) {
		long skinId = application.getUser().getSkinId();
		long countryId = application.getUser().getCombinationId();
		int agreement = (isAgreement) ? 1 : 0;
		int terms = (isTerms) ? 1 : 0;
		String url = Application.get().getWebSiteLink() + "/anyoption_agreement.jsf?fromApp=true&s=" + skinId + "&cid="
				+ countryId + "&version=2-mobile" + "&agreement=" + agreement + "&terms=" + terms;
		webView.loadUrl(url);
	}

	private void setupLoading(boolean isLoading) {
		if (loadingView != null) {
			if (isLoading) {
				loadingView.setVisibility(View.VISIBLE);
			} else {
				loadingView.setVisibility(View.GONE);
			}
		}
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(android.webkit.WebView view, String url) {
			super.onPageFinished(view, url);
			setupLoading(false);
		}

		@Override
		public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	int tempScrollY_1 = 0;
	int tempScrollY_2 = 1000;
	ValueAnimator animator = ValueAnimator.ofInt(tempScrollY_1, tempScrollY_2);
	boolean testScroll;
	int previousY;
	@Override
	public void onScrollChanged(int horizontalScroll, int verticalScroll) {
	}
}
