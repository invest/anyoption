package com.etrader.android.app.fragment;

import com.anyoption.android.app.fragment.MenuFragment;
import com.etrader.android.app.util.ETRegulationUtils;

import android.os.Bundle;
import android.util.Log;

public class ETMenuFragment extends MenuFragment {

	private final String TAG = ETMenuFragment.class.getSimpleName();

	/**
	 * Use this method to obtain new instance of the fragment.
	 */
	public static ETMenuFragment newInstance(Bundle bundle) {
		ETMenuFragment fragment = new ETMenuFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void handlePressedItem(MenuItem clickedItem) {
		
		if(clickedItem == MenuItem.DEPOSIT || clickedItem == MenuItem.MY_ACCOUNT){
			Log.d(TAG, "handlePressedItem -> Check Regulation steps");
			if(!ETRegulationUtils.checkRegulatedFirstDeposit()){
				super.handlePressedItem(clickedItem);
			}else if(ETRegulationUtils.checkFirstDepositAndOpenScreen() && !ETRegulationUtils.checkKnowledgeQuestionAndOpenPopup() && !ETRegulationUtils.checkQuestionnaireAndOpenScreen()){
				if(clickedItem == MenuItem.MY_ACCOUNT || (clickedItem == MenuItem.DEPOSIT && !ETRegulationUtils.checkThresholdAndOpenPopup())){
					super.handlePressedItem(clickedItem);
				}
			}
		}else{
			super.handlePressedItem(clickedItem);
		}
		
	}
	
}
