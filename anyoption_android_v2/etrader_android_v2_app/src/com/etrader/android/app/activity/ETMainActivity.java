package com.etrader.android.app.activity;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.activity.MainActivity;
import com.anyoption.android.app.fragment.BaseFragment;
import com.etrader.android.app.ETApplication.ETScreen;
import com.etrader.android.app.fragment.login_register.AgreementTermsFragment;
import com.etrader.android.app.fragment.my_account.BalancesFragment;
import com.etrader.android.app.fragment.my_account.questionnaire.ETQuestionnaireFragment;

public class ETMainActivity extends MainActivity {

	@Override
	protected Class<? extends BaseFragment> getFragmentClass(Screenable screen) {
		if (screen == ETScreen.BALANCES) {
			return BalancesFragment.class;
		}
		else if (screen == ETScreen.ETQUESTIONNAIRE) {
			return ETQuestionnaireFragment.class;
		}
		else if (screen == Screen.AGREEMENT_TERMS) {
			return AgreementTermsFragment.class;
		}
		
		return super.getFragmentClass(screen);
	}

}
