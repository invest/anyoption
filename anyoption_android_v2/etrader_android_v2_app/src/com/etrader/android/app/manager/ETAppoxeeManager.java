package com.etrader.android.app.manager;

import com.anyoption.android.app.manager.AppoxeeManager;
import com.etrader.android.app.ETApplication;

public class ETAppoxeeManager extends AppoxeeManager {

	public static final String APP_KEY 					= "54aa75711090a8.38097134";
	public static final String SECRET_KEY 				= "54aa7571109260.97448908";
	
	public ETAppoxeeManager(ETApplication application) {
		super(application);
	}
	
	protected String getApiKey(){
		return ETAppoxeeManager.APP_KEY;
	}
	
	protected String getSecretKey(){
		return ETAppoxeeManager.SECRET_KEY;
	}
	
}
