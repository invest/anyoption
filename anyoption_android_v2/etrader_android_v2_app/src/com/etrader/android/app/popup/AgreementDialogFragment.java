package com.etrader.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.StatusFragment;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.widget.WebView;
import com.etrader.android.app.widget.WebView.OnScrollChangeListener;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebViewClient;

public class AgreementDialogFragment extends BaseDialogFragment implements OnScrollChangeListener {

	public static final String TAG = AgreementDialogFragment.class.getSimpleName();

	public static AgreementDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AgreementDialogFragment.");
		AgreementDialogFragment fragment = new AgreementDialogFragment();
		if(bundle == null){
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, 0);
		fragment.setArguments(bundle);

		return fragment;
	}
	
	private Screen screen;
	protected View rootView;
	private View loadingView;
	private WebView webView;
	private RequestButton submitButton;
	private FormCheckBox reportCheckBox;
	
	@Override
	protected int getContentLayout() {
		return R.layout.agreement_dialog_layout;
	}

	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		Bundle bundle = getArguments();
		if(bundle != null){
			this.screen = (Screen) bundle.getSerializable(Constants.EXTRA_SCREEN);
		}
		setCancelable(false);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreateContentView(View contentView) {
		rootView = contentView;
		setUpStatusBar();

		webView = (WebView) rootView.findViewById(R.id.terms_and_conditins_web_view);
		webView.setFastScroll(true);
		webView.getSettings().setJavaScriptEnabled(true);
		if (Build.VERSION.SDK_INT <= 18) {
			webView.getSettings().setPluginState(PluginState.ON);
		}
	    webView.setWebViewClient(new MyWebViewClient());
	    webView.loadUrl(getTermsUrl(application.getSkinId(), Constants.COUNTRY_ISRAEL_ID));
	    webView.setOnScrollChangeListener(this);
	    
		loadingView = rootView.findViewById(R.id.terms_and_conditions_loading_view);
		
		reportCheckBox = (FormCheckBox) rootView.findViewById(R.id.agreement_dialog_report_checkbox);
		
		submitButton = (RequestButton) rootView.findViewById(R.id.terms_and_conditions_submit_button);
		submitButton.disable();
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submitButton.startLoading(new InitAnimationListener() {
					@Override
					public void onAnimationFinished() {
						submit();
					}
				});
			}
		});
		
	}
	
	private void setupLoading(boolean isLoading){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setUpStatusBar() {
		Bundle args = new Bundle();
		StatusFragment statusFragment = (StatusFragment) BaseFragment.newInstance(application.getSubClassForClass(StatusFragment.class), args);
		FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.additional_info_status_fr, statusFragment);
		fragmentTransaction.commit();
	}
	
	protected String getTermsUrl(long skinId, long currCountryId) {
		return Application.get().getWebSiteLink() + "/anyoption_agreement.jsf?fromApp=true&s=" + skinId + "&cid=" + currCountryId + "&version=2-mobile";
	}
	
	////////////				REQUESTS and CALLBACKS		//////////////////////////////
	
	private void submit(){
		User user = application.getUser();
		if(user == null){
			application.getNotificationManager().showNotificationError("Sorry, you are not logged in.");
			submitButton.stopLoading();
			return;
		}
		user.setAcceptedTerms(true);
		
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		request.setRegularReportMail(reportCheckBox.isChecked());
		request.setUser(user);
		
		if(!Application.get().getCommunicationManager().requestService(AgreementDialogFragment.this, "callback", Constants.SERVICE_UPDATE_USER, request, MethodResult.class, false, false)){
			submitButton.stopLoading();
		}
	}
	
	public void callback(Object resultObject){
		Log.d(TAG, "Accept Terms&Conds. callback.");
		submitButton.stopLoading();
		MethodResult result = (MethodResult) resultObject;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Accept Terms&Conds. OK.");
			dismiss();
			
			if(application.getUser().getFirstDepositId() > 0 && application.getUserRegulation() != null && !application.getUserRegulation().isKnowledgeQuestion()){
				application.getFragmentManager().showDialogFragment(KnowledgeDialogFragment.class, null);
			}else{
				if(screen == null){
					screen = Screen.FIRST_NEW_CARD;
				}
				application.postEvent(new NavigationEvent(this.screen, NavigationType.INIT));
			}
			
		} else {
			Log.d(TAG, "Accept Terms&Conds. ERROR.");
			ETErrorUtils.handleResultError(result);
		}
	}
	
	////////////////////////////////////////////////////////////////////
	

	@SuppressWarnings("deprecation")
	@Override
	public void onScrollChanged(int horizontalScroll, int verticalScroll) {
		int tek = (int) Math.floor(webView.getContentHeight() * webView.getScale());
        if(tek - webView.getScrollY() < webView.getHeight() + 100){
        	submitButton.enable();
        }
	}
	
	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(android.webkit.WebView view, String url) {
			super.onPageFinished(view, url);
			setupLoading(false);
		}
		@Override
		public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
}
