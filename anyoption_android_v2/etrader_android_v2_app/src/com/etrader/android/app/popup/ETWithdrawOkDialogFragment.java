package com.etrader.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.WithdrawOkDialogFragment;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * A POPUP that is shown when the user attempts to make a withdrawal that is lower than a MIN amount. 
 * @author Anastas Arnaudov
 *
 */
public class ETWithdrawOkDialogFragment extends WithdrawOkDialogFragment{

	public static final String TAG = ETWithdrawOkDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static ETWithdrawOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new WithdrawOkDialogFragment.");
		ETWithdrawOkDialogFragment fragment = new ETWithdrawOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		
		TextView message_1_TextView = (TextView) contentView.findViewById(R.id.withdraw_ok_popup_message_1_text_view);
		TextView message_2_TextView = (TextView) contentView.findViewById(R.id.withdraw_ok_popup_message_2_text_view);
		
		TextUtils.decorateInnerText(message_1_TextView, null, message_1_TextView.getText().toString(), amount, Font.NOTO_SANS_BOLD, 0, 0);
		TextUtils.decorateInnerText(message_2_TextView, null, message_2_TextView.getText().toString(), fee, Font.NOTO_SANS_BOLD, 0, 0);
	}

}
