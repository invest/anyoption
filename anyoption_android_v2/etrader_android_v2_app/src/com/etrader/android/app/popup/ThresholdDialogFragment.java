package com.etrader.android.app.popup;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETConstants;
import com.etrader.android.app.util.ETErrorUtils;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ThresholdDialogFragment extends BaseDialogFragment {
	
	public static final String TAG = ThresholdDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static ThresholdDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new ThresholdDialogFragment.");
		ThresholdDialogFragment fragment = new ThresholdDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		fragment.setArguments(bundle);
		return fragment;
	}
	
	private Button okButton;
	private boolean showThreshold;
	private boolean isUserRefreshed;
	private TextView headerTextView;
	private TextView messageTextView;
	private View loadingView;
	
	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		Bundle bundle = getArguments();
		if(bundle != null){
			showThreshold = bundle.getBoolean(ETConstants.EXTRA_THRESHOLD_SHOW_THRESHOLD);
		}
		refreshUser();
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.threshold_popup_layout;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		setCancelable(false);
		
		loadingView = contentView.findViewById(R.id.threshold_loading);
		headerTextView = (TextView) contentView.findViewById(R.id.threshold_header_text_view);
		messageTextView = (TextView) contentView.findViewById(R.id.threshold_message_text_view);
		setupText();
		setupLoading();
		
		okButton = (Button) contentView.findViewById(R.id.popup_positive_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goToHomeScreen();
			}
		});
		
		TextView linkTextView = (TextView) contentView.findViewById(R.id.threshold_link);
		TextUtils.underlineText(linkTextView);
		linkTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				unsuspendUserRequest();
			}
		});
	}
	
	private void setupLoading(){
		if(loadingView != null){
			if(isUserRefreshed){
				loadingView.setVisibility(View.GONE);
			}else{
				loadingView.setVisibility(View.VISIBLE);
			}
		}
	}
	
	private void goToHomeScreen(){
		dismiss();
		application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
	}
	
	private void unsuspendUserRequest(){
		Log.d(TAG, "unsuspendUserRequest");
		if(!application.getCommunicationManager().requestService(ThresholdDialogFragment.this, "unsuspendUserRequestCallback", ETConstants.SERVICE_UNSUSPEND_USER, new UserMethodRequest() , UserMethodResult.class, false, false)){
			goToHomeScreen();
		}
	}
	
	public void unsuspendUserRequestCallback(Object resultObj){
		Log.d(TAG, "unsuspendUserRequestCallback");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "unsuspendUserRequestCallback OK.");
			application.setUserRegulation(result.getUserRegulation());
		}else {
			Log.e(TAG, "unsuspendUserRequestCallback failed");
			ETErrorUtils.handleResultError(result);
		}
		goToHomeScreen();
	}
	
	private void setupText(){
		if(headerTextView != null){
			UserRegulationBase regulationBase = application.getUserRegulation();
			if(showThreshold){
				headerTextView.setVisibility(View.GONE);
				String threshold = AmountUtil.getFormattedAmount(regulationBase.getTresholdLimit(), AmountUtil.getCurrency(application.getUser().getCurrencyId()));
				messageTextView.setText(getString(R.string.thresholdPopupMessageTypeLimit, threshold));
			}else{
				headerTextView.setVisibility(View.VISIBLE);
				messageTextView.setText(getString(R.string.thresholdPopupMessageTypeLowScore));
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void refreshUser(){
		if (application.getUser() != null) {
			Log.d(TAG, "refreshUser");
			if(!application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), application.getUserMethodResultClass(), false, false)){
				setupLoading();
			}
		}
	}
	
	public void getUserCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.saveUser(result);
			isUserRefreshed = true;
			setupText();
		}
		setupLoading();
	}
}
