package com.etrader.android.app.popup;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.widget.Button;
import com.etrader.android.app.R;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class BlockedDialogFragment extends BaseDialogFragment {
	
	public static final String TAG = BlockedDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static BlockedDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new BlockedDialogFragment.");
		BlockedDialogFragment fragment = new BlockedDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		fragment.setArguments(bundle);
		return fragment;
	}

	private Button okButton;

	@Override
	protected int getContentLayout() {
		return R.layout.blocked_popup_layout;
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		setCancelable(false);
		okButton = (Button) contentView.findViewById(R.id.popup_positive_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				goToHomeScreen();
			}
		});
	}
	
	private void goToHomeScreen(){
		application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
	}
}
