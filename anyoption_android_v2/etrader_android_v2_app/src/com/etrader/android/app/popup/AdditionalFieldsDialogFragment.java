package com.etrader.android.app.popup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.BaseFragment;
import com.anyoption.android.app.fragment.StatusFragment;
import com.anyoption.android.app.popup.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.etrader.android.app.R;
import com.etrader.android.app.util.ETErrorUtils;
import com.etrader.android.app.util.ETRegulationUtils;
import com.etrader.android.app.widget.ETGenderButton;
import com.etrader.android.app.widget.form.FormCitySelector;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class AdditionalFieldsDialogFragment extends BaseDialogFragment {

	public static final String TAG = AdditionalFieldsDialogFragment.class.getSimpleName();

	public static AdditionalFieldsDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new AdditionalFieldsDialogFragment.");
		AdditionalFieldsDialogFragment fragment = new AdditionalFieldsDialogFragment();
		if(bundle == null){
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, 0);
		fragment.setArguments(bundle);

		return fragment;
	}
	
	private Screen screen;
	
	protected View rootView;
	private FormEditText idNumberEdittext;
	private FormEditText streetEdittext;
	private FormEditText streetNumberEdittext;
	private FormCitySelector citySelector;
	private FormEditText zipEdittext;
	private FormDatePicker dateOfBirthPicker;
	private ETGenderButton genderSwitch;
	private User user;
	private RequestButton submitButton;
	private Form form;
	
	@Override
	protected int getContentLayout() {
		return R.layout.additional_info_fragment_layout;
	}

	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		Bundle bundle = getArguments();
		if(bundle != null){
			this.screen = (Screen) bundle.getSerializable(Constants.EXTRA_SCREEN);
		}
		setCancelable(false);
	}
	
	@Override
	protected void onCreateContentView(View contentView) {
		rootView = contentView;
		user = application.getUser();
		
		setUpStatusBar();
		
		View funnelProgressLayout = rootView.findViewById(R.id.additional_info_funnel_progress_layout);
		funnelProgressLayout.setVisibility((ETRegulationUtils.isRegulated() ? View.VISIBLE : View.GONE));
		
		//ID NUMBER:
		idNumberEdittext = (FormEditText) rootView.findViewById(R.id.additional_info_id_number_edittext);
		idNumberEdittext.addValidator(ValidatorType.EMPTY, ValidatorType.ID_NUMBER);
		if(!ETRegulationUtils.isRegulated()){
			idNumberEdittext.setValue(user.getIdNum());
			idNumberEdittext.setEditable(false);
			idNumberEdittext.setClickable(false);
		}
		
		//STREET:
		streetEdittext = (FormEditText) rootView.findViewById(R.id.additional_info_steet_edittext);
		streetEdittext.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_HEBREW);
		streetEdittext.setValue(user.getStreet());
		streetEdittext.setFieldName(Constants.FIELD_STREET);
		
		//STREET NUMBER:
		streetNumberEdittext = (FormEditText) rootView.findViewById(R.id.additional_info_steet_number_edittext);
		streetNumberEdittext.addValidator(ValidatorType.EMPTY, ValidatorType.ZIP);//TODO
		streetNumberEdittext.setValue(user.getStreetNo());
		streetNumberEdittext.setFieldName(Constants.FIELD_STREET_NO);
		
		//CITY:
		citySelector = (FormCitySelector) rootView.findViewById(R.id.additional_info_city_selector);
		citySelector.setValue(application.getCityByName(application.getUser().getCityName()));
		
		//ZIP
		zipEdittext = (FormEditText) rootView.findViewById(R.id.additional_info_zip_edittext);
		zipEdittext.addValidator(ValidatorType.EMPTY, ValidatorType.ZIP);//TODO
		zipEdittext.setValue(user.getZipCode());
		zipEdittext.setFieldName(Constants.FIELD_ZIP_CODE);
		
		//DATE OF BIRTH:
		dateOfBirthPicker = (FormDatePicker) rootView.findViewById(R.id.additional_info_date_of_birth);
		if (user.getTimeBirthDate() != null) {
			SimpleDateFormat formatDate = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR,application.getLocale());
			dateOfBirthPicker.setValue(formatDate.format(user.getTimeBirthDate()));
		}
		dateOfBirthPicker.addValidator(ValidatorType.EMPTY, ValidatorType.BIRTH_DAY);
		dateOfBirthPicker.setMin18(true);
		
		//GENDER:
		genderSwitch = (ETGenderButton) rootView.findViewById(R.id.additional_info_gender_switch);
		String genederStr = user.getGender();
		if (genederStr != null && !genederStr.equals("")) {
			if (genederStr.equalsIgnoreCase("M")) {
				//Male:
				genderSwitch.changeState(false);
			}else { 
				//Female:
				genderSwitch.changeState(true);
			}
		}
		
		form = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				submit();
			}
		});
		form.addItem(idNumberEdittext, streetEdittext, streetNumberEdittext, citySelector, zipEdittext, dateOfBirthPicker);

		submitButton = (RequestButton) rootView.findViewById(R.id.additional_info_submit_button);
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(form.checkValidity()){
					submitButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});									
				}
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private void setUpStatusBar() {
		Bundle args = new Bundle();
		StatusFragment statusFragment = (StatusFragment) BaseFragment.newInstance(application.getSubClassForClass(StatusFragment.class), args);
		FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.additional_info_status_fr, statusFragment);
		fragmentTransaction.commit();
	}
	
	private void setupUser(){
		user.setIdNum(idNumberEdittext.getValue());
		if (dateOfBirthPicker.getValue() != null && !dateOfBirthPicker.getValue().equalsIgnoreCase("")) {			
			SimpleDateFormat sdf = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()); 
			Date timeBirthDate = null;
			try {			
				timeBirthDate = sdf.parse(dateOfBirthPicker.getValue());
			} catch (ParseException e) {			
				timeBirthDate = null;
			}
			user.setTimeBirthDate(timeBirthDate);
		}
		
		user.setGender((genderSwitch.isMale())?"M":"F");
		user.setStreet(streetEdittext.getValue());
		user.setStreetNo(streetNumberEdittext.getValue());
		user.setCityName(citySelector.getValue().getName());
		user.setCityId(citySelector.getValue().getId());
		user.setZipCode(zipEdittext.getValue());
	}
	
	private void submit(){
		setupUser();
		Log.d(TAG, "Submiting user additional fields.");
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		request.setUser(user);
		
		if(!Application.get().getCommunicationManager().requestService(AdditionalFieldsDialogFragment.this, "callback", Constants.SERVICE_UPDATE_USER_ADDITIONAL_FIELDS, request, UserMethodResult.class, false, false)){
			submitButton.stopLoading();
		}
	}
	
	public void callback(Object resultObject){
		Log.d(TAG, "Update additional user fields callback.");
		submitButton.stopLoading();
		UserMethodResult result = (UserMethodResult) resultObject;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Update additional user fields OK");
			User user = result.getUser();
			if(user != null){
				application.saveUser(result);
				
				if(!ETRegulationUtils.isRegulated()){
					application.postEvent(new NavigationEvent(Screen.FIRST_NEW_CARD, NavigationType.INIT));
				}
				else if(!user.isAcceptedTerms()){
					Bundle bundle = new Bundle();
					bundle.putSerializable(Constants.EXTRA_SCREEN, Screen.FIRST_NEW_CARD);
					application.getFragmentManager().showDialogFragment(AgreementDialogFragment.class, bundle);
				}
				else{
					if(screen != null){
						application.postEvent(new NavigationEvent(this.screen, NavigationType.INIT));
					}
				}
				dismiss();
			}
    		return;
		} else {
			Log.e(TAG, "Update additional user fields ERROR");
			ETErrorUtils.displayFieldError(form, result);
		}
	}
	
}
