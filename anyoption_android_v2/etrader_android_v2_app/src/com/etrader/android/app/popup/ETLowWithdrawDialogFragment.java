package com.etrader.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.LowWithdrawDialogFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ETLowWithdrawDialogFragment extends LowWithdrawDialogFragment {
	public static final String TAG = ETLowWithdrawDialogFragment.class.getSimpleName();
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static ETLowWithdrawDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new LowWithdrawDialogFragment.");
		ETLowWithdrawDialogFragment fragment = new ETLowWithdrawDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		super.onCreateContentView(contentView);
		
		String minAmount = AmountUtil.getUserFormattedAmount(amount);
		
		TextView message_1_TextView = (TextView) contentView.findViewById(R.id.low_withdraw_popup_message_1_text_view);
		TextView message_2_TextView = (TextView) contentView.findViewById(R.id.low_withdraw_popup_message_2_text_view);
		
		TextUtils.decorateInnerText(message_1_TextView, null, message_1_TextView.getText().toString(), minAmount, Font.NOTO_SANS_BOLD, 0, 0);
		TextUtils.decorateInnerText(message_2_TextView, null, message_2_TextView.getText().toString(), fee, Font.NOTO_SANS_BOLD, 0, 0);
			
	}
}
