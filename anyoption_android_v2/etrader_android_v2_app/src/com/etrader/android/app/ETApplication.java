package com.etrader.android.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.anyoption.android.app.Application;
import com.etrader.android.app.R;
import com.anyoption.android.app.manager.GoogleAdWordsManager;
import com.anyoption.android.app.manager.GoogleAnalyticsManager;
//import com.anyoption.android.app.R;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.Skin;
import com.etrader.android.app.manager.ETAppoxeeManager;
import com.etrader.android.app.manager.ETGoogleAdWordsManager;
import com.etrader.android.app.util.ETConstants;

public class ETApplication extends Application {

	public static final String SUBCLASS_PREFIX = "ET";
	
	private static final String[] LANGUAGES_ARR = new String[] { "iw" };
	private static final Set<String> LANGUAGES_SET = new HashSet<String>(Arrays.asList(LANGUAGES_ARR));
	private static final String DEFAULT_LANGUAGE = "iw";
	
	protected ETAppoxeeManager 	appoxeeManager;
	
	private List<String> mobilePrefixes;
	private List<String> landPhonePrefixes;
	
	//#################################################################################################################
	//######################						ET SCREENs						###################################
	//#################################################################################################################
	
	/**
	 * Constant names for each main screen (Map each screen to a Constant). This is used in the
	 * navigation flow.
	 */
	public enum ETScreen implements Screenable {

		BALANCES("balances", get().getResources().getString(R.string.balancesTitle), 0),
		ETQUESTIONNAIRE("ETQUESTIONNAIRE", get().getResources().getString(R.string.questionnaireScreenTitle), 0);
		
		/**
		 * Tag used to search.
		 */
		private String tag;

		/**
		 * A default title. "" - if no default title is needed.
		 */
		private String title;

		/**
		 * Id of an image used instead of title. 0 - if no image is needed.
		 */
		private int imageID;

		/**
		 * Additional details for the Screen
		 */
		private int[] flags;
		
		ETScreen(String tag, String title, int imageID, int... flags){
			this.tag = tag;
			this.title = title;
			this.imageID = imageID;
			this.flags = flags;
		}

		public String getTag() {
			return tag;
		}
		public String getTitle() {
			return title;
		}
		public int getImageID() {
			return imageID;
		}
		public boolean isHasHelpInfo() {
			return false;
		}
		public int[] getFlags(){
			return this.flags;
		}

	}
	
	
	public void onCreate() {
		super.onCreate();
		singletonAplication = this;
		setupMobilePrefixes();
		setupLandPhonePrefixes();
	}

	//#################################################################################################################
	//###########################					MANAGERS						###################################
	//#################################################################################################################
	
	@Override
	public synchronized ETAppoxeeManager getAppoxeeManager(){
		if(appoxeeManager == null){
			appoxeeManager = new ETAppoxeeManager(this);
		}
		return appoxeeManager;
	}

	@Override
	public GoogleAdWordsManager getGoogleAdWordsManager() {
		if (googleAdWordsManager == null) {
			googleAdWordsManager = new ETGoogleAdWordsManager(this);
		}
		return googleAdWordsManager;
	}

	@Override
	public synchronized GoogleAnalyticsManager getGoogleAnalyticsManager() {
		return new GoogleAnalyticsManager(R.xml.app_tracker);
	}
	//#################################################################################################################
	
	
	
	/**
	 * Returns the singleton instance of the {@link Application}.
	 * @return
	 */
	public static ETApplication get(){
		return (ETApplication) singletonAplication;
	}
	
	private void setupMobilePrefixes() {
		setMobilePrefixes(new ArrayList<String>());
		getMobilePrefixes().add("050");
		getMobilePrefixes().add("052");
		getMobilePrefixes().add("053");
		getMobilePrefixes().add("054");
		getMobilePrefixes().add("055");
		getMobilePrefixes().add("057");
		getMobilePrefixes().add("058");
	}

	private void setupLandPhonePrefixes(){
		setLandPhonePrefixes(new ArrayList<String>());
		getLandPhonePrefixes().add("02");
		getLandPhonePrefixes().add("03");
		getLandPhonePrefixes().add("04");
		getLandPhonePrefixes().add("08");
		getLandPhonePrefixes().add("09");
		getLandPhonePrefixes().add("072");
		getLandPhonePrefixes().add("073");
		getLandPhonePrefixes().add("074");
		getLandPhonePrefixes().add("076");
		getLandPhonePrefixes().add("077");
		getLandPhonePrefixes().add("078");
	}
	
	@Override
	public String getServiceURL() {
		return ETConstants.SERVICE_URL;
	}

	@Override
	public String getLSURL() {
		return ETConstants.LS_URL;
	}

	@Override
	public String getMerchantID() {
		return ETConstants.MERCHANT_ID;
	}

	@Override
	public String getOneClickURL() {
		return ETConstants.ONE_CLICK_URL;
	}

	@Override
	public String getWebSiteLink() {
		return ETConstants.WEB_SITE_LINK;
	}

	@Override
	protected void setupDefaultLanguageAndLocale() {
		getSharedPreferencesManager().putLong(Constants.PREFERENCES_SKINID, Skin.SKIN_ETRADER);
		getSharedPreferencesManager().putString(Constants.PREFERENCES_LOCALE, getDefaultLanguage());

		super.setupDefaultLanguageAndLocale();
	}

	@Override
	protected String getSubclassPrefix() {
		return ETApplication.SUBCLASS_PREFIX;
	}

	public List<String> getMobilePrefixes() {
		return mobilePrefixes;
	}

	public void setMobilePrefixes(List<String> mobilePrefixes) {
		this.mobilePrefixes = mobilePrefixes;
	}

	public List<String> getLandPhonePrefixes() {
		return landPhonePrefixes;
	}

	public void setLandPhonePrefixes(List<String> landPhonePrefixes) {
		this.landPhonePrefixes = landPhonePrefixes;
	}

	@Override
	protected Set<String> getSupportedLanguages() {
		return LANGUAGES_SET;
	}

	@Override
	protected String getDefaultLanguage() {
		return DEFAULT_LANGUAGE;
	}
	
}
