package com.anyoption.android.app.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.constants.Constants;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class TextUtils {

	public static final String TAG = TextUtils.class.getSimpleName();
	
	/**
	 * Value = {@value} <br>
	 * Placeholder key used in strings to specify the position of an image in the text.
	 */
	public static final String IMAGE_PLACEHOLDER_START = "{image";
	/**
	 * Value = {@value} <br>
	 * Placeholder key used in strings to specify the position of an image in the text.
	 */
	public static final String IMAGE_PLACEHOLDER_END = "}";
	
	public interface OnInnerTextClickListener{
		public void onInnerTextClick(TextView textView, String text, String innerText);
	}
	
	/**
	 * Returns the text res id for the given name.
	 * 
	 * @param name
	 * @return 0 if no res was found.
	 */
	public static int getTextResID(String name) {
		Context context = Application.get();
		int result = 0;
		try {
			result = context.getResources().getIdentifier(name, "string", context.getPackageName());
		} catch (Exception e) {
			Log.e(Utils.TAG, "NO res get text for the name = " + name, e);
		}
	
		return result;
	}

	public static SpannableStringBuilder makeUnderlineClickable(CharSequence sequence, ClickableSpan clickableSpan, int color) {
	    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
	    UnderlineSpan[] underlines = strBuilder.getSpans(0, sequence.length(), UnderlineSpan.class);
	    for (UnderlineSpan span : underlines) {
	        int start = strBuilder.getSpanStart(span);
	        int end = strBuilder.getSpanEnd(span);
	        int flags = strBuilder.getSpanFlags(span);
	        strBuilder.setSpan(clickableSpan, start, end, flags);
	        if (color != 0) {
	            strBuilder.setSpan(new ForegroundColorSpan(color), start, end, flags);
	        }
	    }
	    return strBuilder;
	}

	/**
	 * Gets url string of values and keys and return them in map
	 * @param urlString url query string 
	 * @return map with the keys and values
	 */
	public static Map<String, String> parseUrlToStringPairs(String urlString) {
		Map<String, String> urlMapping = new HashMap<String, String>();
		
		String[] params = urlString.split("&");
	    for (String param : params) {
	    	String[] pair = param.split("=");
	    	if(pair.length == 2) {
	    		urlMapping.put(pair[0], pair[1]);
	    	}
	    }       
		return urlMapping;
	}

	/**
	 * Underlines the text in the TextView.
	 */
	public static void underlineText(TextView textView){
		SpannableString content = new SpannableString(textView.getText());
	    content.setSpan(new UnderlineSpan(), 0, textView.getText().length(), 0);
	    textView.setText(content, BufferType.SPANNABLE);
	}
	
	public static Spannable underlineText(TextView textView, Spannable spannable){
		if(spannable == null){
			spannable = new SpannableString(textView.getText());
		}
		spannable.setSpan(new UnderlineSpan(), 0, textView.getText().length(), 0);
	    textView.setText(spannable, BufferType.SPANNABLE);
	    return spannable;
	}
	
	/**
	 * Underlines the inner text in the TextView.
	 * @param spannableString Pass <b>null</b>  if no other formatting is going to be done on the TextView. If null : 
	 * this means that this formatting will remove all previous formatting.
	 */
	public static Spannable underlineInnerText(TextView textView,Spannable spannable, String text, String innerText){
		if(spannable == null){
			spannable = new SpannableStringBuilder(text);			
		}else if(spannable.length() == 0){
			((SpannableStringBuilder) spannable).append(text);
		}
		
		int start = text.indexOf(innerText);
		int end = start + innerText.length(); 
		SpannableString underLineSpan = new SpannableString(textView.getText());
		underLineSpan.setSpan(new UnderlineSpan(), start, end, 0);
	    textView.setText(underLineSpan, BufferType.SPANNABLE);
	    return underLineSpan;
	}
	
	/**
	 * Create part of the text CLICKABLE.
	 * @param textView The TextView
	 * @param spannableString Pass <b>null</b>  if no other formatting is going to be done on the TextView. If null : 
	 * this means that this formatting will remove all previous formatting.
	 * @param text The whole text.
	 * @param innerText The inner text, which is part of the whole text.
	 * @param font The inner text font. Pass <b>null</b> if the inner text should use the font of the rest of the text.
	 * @param textSizeResId The text size DIMENS resource id. Pass <b>0</b> if the CLICKABLE should use the same size as the rest of the text.
	 * @param colorResId The resource id of the CLICKABLE text color. Pass 0 if the CLICKABLE text should use the parent color.
	 * @param isUnderlined Whether the CLICKABLE part should be underlined like a link.
	 * @param onInnerTextClickListener
	 * @return The modified SpannableString to be used for more formatting.
	 */
	public static Spannable makeClickableInnerText(final TextView textView, Spannable spannable, final String text, final String innerText, Font font, final int textSizeResId, final int colorResId, final boolean isUnderlined, final OnInnerTextClickListener onInnerTextClickListener){
		final Context context = Application.get();
		
		if(text == null || !text.contains(innerText)){
			return spannable;
		}
		
		if(spannable == null){
			spannable = new SpannableStringBuilder(text);			
		}else if(spannable.length() == 0){
			((SpannableStringBuilder) spannable).append(textView.getText());
		}
		
		
		ClickableSpan clickableSpan = new ClickableSpan() {
			TextPaint tp;
			
			@Override
		     public void updateDrawState(TextPaint tp){
				this.tp = tp;
				tp.bgColor = context.getResources().getColor(R.color.transparent);         
				tp.setUnderlineText(isUnderlined);
				if(textSizeResId != 0){
					try {
						int textSizePixel = context.getResources().getDimensionPixelSize(textSizeResId);
						tp.setTextSize(textSizePixel);
					} catch (Exception e) {
						Log.e(TAG, "Cannot set TextSize in makeClickableInnerText");
					}
				}
		    }
			
			@Override
			public void onClick(View widget) {
				updateDrawState(tp);
		        widget.invalidate();
		        if(onInnerTextClickListener != null){
		        	onInnerTextClickListener.onInnerTextClick(textView, text, innerText);		        	
		        }
			}
		};
		
		int start = text.indexOf(innerText);
		int end = start + innerText.length();

		if(start < 0 || end > text.length()){
			Log.d(TAG, "ta6ak");
			return spannable;
		}
		
		if(colorResId != 0){
			spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(colorResId)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		if(font != null){
			spannable.setSpan(new FontSpan(font), start, end, 0);
		}
		
		spannable.setSpan(clickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		textView.setText(spannable);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		textView.setHighlightColor(context.getResources().getColor(R.color.transparent));
		
		return spannable;
	}
	
	public static Spannable decorateInnerText(TextView textView, Spannable spannable, String text, String innerText, Font font, int colorResId, int textSize){
		if(innerText == null || !text.contains(innerText)){
			return spannable;
		}
		final Context context = Application.get();
		
		if(spannable == null){
			spannable = new SpannableStringBuilder(text);			
		}else if(spannable.length() == 0){
			((SpannableStringBuilder) spannable).append(text);
		}
		
		int start = text.indexOf(innerText);
		int end = start + innerText.length();

		if(colorResId != 0){
			spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(colorResId)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		if(font != null){
			spannable.setSpan(new FontSpan(font), start, end, 0);
		}
		if(textSize > 0){
			spannable.setSpan(new AbsoluteSizeSpan(textSize), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		
		textView.setText(spannable);

		return spannable;
	}
	
	/**
	 * Replaces all images placeholder with the given images. The placeholder key is {@link #IMAGE_PLACEHOLDER_START} + imageNumber + {@link #IMAGE_PLACEHOLDER_END}
	 * @param textView
	 * @param spannableString Pass <b>null</b>  if no other formatting is going to be done on the TextView. If null : 
	 * this means that this formatting will remove all previous formatting. 
	 * @param isImagesScaledToTextHeight Whether to scale the images to fit the text height.
	 * @param resIds Resource IDs for the images.
	 * @return The modified SpannableString to be used for more formatting.
	 */
	public static Spannable insertImages(TextView textView, Spannable spannable, boolean isImagesScaledToTextHeight, int... resIds){
		String text = textView.getText().toString();
		if(spannable == null){
			spannable = new SpannableStringBuilder(text);
		}else if(spannable.length() == 0){
			((SpannableStringBuilder) spannable).append(textView.getText());
		}
		
		Drawable[] drawables = new Drawable[resIds.length];
		for(int i=0; i<resIds.length; i++){
			int resId = resIds[i];
			Drawable drawable = DrawableUtils.getImage(resId);
			drawables[i] = drawable;
	
			if(isImagesScaledToTextHeight){
				int textHeight = TextUtils.getTextHeight(textView);
		        int drawableHeight = textHeight;
		        int drawableWidth = drawableHeight * drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight();
		        drawable.setBounds(0, 0, drawableWidth, drawableHeight); 
			}else{
				drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight()); 
			}
			
			ImageSpan imageSpan = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
			int placeHolderNumber = i + 1;
			String imagePlaceholder = TextUtils.IMAGE_PLACEHOLDER_START + placeHolderNumber + TextUtils.IMAGE_PLACEHOLDER_END;
			int dStart = text.indexOf(imagePlaceholder);
			int dEnd = dStart + imagePlaceholder.length();
			spannable.setSpan(imageSpan, dStart, dEnd, 0);
		}
        
        textView.setText(spannable);
        
        return spannable;
	}
	
	public static class FontSpan extends TypefaceSpan {

		private Font font;

		public FontSpan(Font font) {
			super("");
			this.font = font;
		}

		@Override
		public void updateDrawState(TextPaint tp) {
			applyCustomTypeFace(tp);
		}

		@Override
		public void updateMeasureState(TextPaint paint) {
			applyCustomTypeFace(paint);
		}

		private void applyCustomTypeFace(Paint paint) {
			Typeface typeface = Typefaces.get(Application.get(), Constants.FONTS_DIR + font.getFontName());
			
			int oldStyle;
			Typeface old = paint.getTypeface();
			if (old == null) {
				oldStyle = 0;
			} else {
				oldStyle = old.getStyle();
			}
			
			int fake = oldStyle & ~typeface.getStyle();
			if ((fake & Typeface.BOLD) != 0) {
				paint.setFakeBoldText(true);
			}

			if ((fake & Typeface.ITALIC) != 0) {
				paint.setTextSkewX(-0.25f);
			}

			paint.setTypeface(typeface);
		}
	}

	/**
	 * Calculates the text width inside the given TextView.
	 * @param textView
	 * @return
	 */
	public static int getTextWidth(TextView textView){
		Rect bounds = new Rect(); 
		TextPaint paint = textView.getPaint();
		paint.getTextBounds(textView.getText().toString(), 0, textView.getText().length(), bounds); 
		return bounds.width();
	}

	/**
	 * Calculates the Inner Text width inside the given TextView.
	 * @param textView
	 * @return
	 */
	public static int getInnerTextWidth(TextView textView, String innerText){
		Rect bounds = new Rect(); 
		TextPaint paint = textView.getPaint();
		int startIndex 	= 0;
		int endIndex 	= 0;
		try {
			startIndex 	= textView.getText().toString().indexOf(innerText);
			endIndex 	= startIndex + innerText.length();
			paint.getTextBounds(textView.getText().toString(), startIndex, endIndex, bounds); 
		} catch (Exception e) {
			Log.e(TAG, "getInnerTextWidth -> Could not find Inner Text inside the Text", e);
		}
		return bounds.width();
	}
	
	/**
	 * Calculates the text height inside the given TextView.
	 * @param textView
	 * @return
	 */
	public static int getTextHeight(TextView textView){
		Rect bounds = new Rect(); 
		TextPaint paint = textView.getPaint();
		paint.getTextBounds(textView.getText().toString(), 0, textView.getText().length(), bounds); 
		return bounds.height();
	}

	/**
	 * Checks whether the text in the TextView fits in the view bounds.
	 * @param textView
	 * @return
	 */
	public static boolean checkTextFit(TextView textView){
		int viewWidth = textView.getWidth();
		int textWidth = getTextWidth(textView);
		
		return viewWidth >= textWidth;
	}
	/**
	 * Ensures that if the TextView needs to multiple lines, that the innerText won't get broken in the middle.
	 * <br><br><b>Example:</b><br> Bla bla bla <br> FirstName LastName
	 * <br><br><b>NOT WANTED:</b><br>Bla bla bla FirstName <br> LastName
	 * 
	 * <br><br><b>IMPORTANT:</b> But if the innerText alone is longer than the TextView's width then it will get broken eventually. SHIT HAPPENS!!!
	 */
	public static String makeInnerTextUnbreakable(TextView textView, String text, String innerText){

		if(getLinesCount(textView.getText().toString(), textView) == 1){
			//OK
			return innerText;
		}else{
			try {
				if(!TextUtils.containsWhiteSpace(innerText)){
					//OK
					return innerText;
				}
				
				int firstIndexOfInnertext = text.indexOf(innerText);
				if(firstIndexOfInnertext == 0){
					//OK
					return innerText;
				}
				
				String subTextPre = text.substring(0, firstIndexOfInnertext);
				String subTextPrePlusInner = subTextPre + innerText;
				String[] innerTextsArray = innerText.split("\\s+");
				boolean shouldAddNewLine = false;
				String temp = "";
				for(int i=0; i<innerTextsArray.length-1; i++){
					temp += innerTextsArray[i];
					String subText = subTextPre + temp;
					if(getLinesCount(subText, textView) != getLinesCount(subTextPrePlusInner, textView)){
						shouldAddNewLine = true;
						break;
					}
				}
				
				if(shouldAddNewLine){
					//The innerText is broken into multiple lines.
					//We add a new line char at the beginning of the innerText. 
					innerText = "\n" + innerText;
					return innerText;
				}else{
					//OK
					return innerText;
				}
			} catch (Exception e) {
				Log.e(TAG, "makeInnerTextUnbreakable", e);
				return innerText;
			}
		}
	}
	
	/**
	 * Ensures that the TextView is rendered before calling {@link {@link #makeInnerTextUnbreakable(TextView, String)}}.
	 */
	public static void makeInnerTextUnbreakableAfterRendering(final TextView textView, final String text, final String innerText){
		OnLayoutChangeListener onLayoutChangeListener = new OnLayoutChangeListener() {
			private boolean isTextRefactored = false;
			@Override
			public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight,int oldBottom) {
				if(view.getWidth() > 0 && !isTextRefactored){
					isTextRefactored = true;
					makeInnerTextUnbreakable(textView, text, innerText);
					textView.removeOnLayoutChangeListener(this);
				}
			}
		};
		
		textView.addOnLayoutChangeListener(onLayoutChangeListener);
	}
	
	/**
	 * Calculates how many lines will the Text occupy in this TextView.
	 * @param text
	 * @param textView
	 * @return
	 */
	public static int getLinesCount(String text, TextView textView){
		StaticLayout staticlayout = getStaticLayout(text, textView);
		return staticlayout.getLineCount();
	}
	
	/**
	 * Returns a {@link StaticLayout} for this Text and TextView.
	 * @param text
	 * @param textView
	 * @return
	 */
	public static StaticLayout getStaticLayout(String text, TextView textView){
		int width;
		if(textView.getWidth() > 0){
			width = textView.getWidth();
		}else{
			width = ScreenUtils.getScreenWidth()[0];
		}
		TextPaint textPaint = textView.getPaint();
		StaticLayout staticlayout = new StaticLayout(text, textPaint, width, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
		return staticlayout;
	}
	
	/**
	 * Checks whether the text contains white spacees.
	 * @param text
	 * @return
	 */
	public static boolean containsWhiteSpace(String text){
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}
	
}
