package com.anyoption.android.app.event;

/**
 * Event that occurs when the user fail to login.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class LoginFailedEvent {

}
