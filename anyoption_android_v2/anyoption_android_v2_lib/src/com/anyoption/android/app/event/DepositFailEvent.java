package com.anyoption.android.app.event;

/**
 * Event that occurs when the user fail to deposit.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class DepositFailEvent {

}
