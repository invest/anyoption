package com.anyoption.android.app.event;

/**
 * Event that occurs when the user fail to register.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class RegisterFailEvent {

}
