package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.common.beans.base.User;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationShoppingBag extends LinearLayout{

	protected int expiredOptionsCount;
	protected String marketName;
	protected long profitAmount;
	protected boolean isWinning;
	
	public NotificationShoppingBag(int expiredOptionsCount, String marketName, long profitAmount, boolean isWinning) {
		super(Application.get());
		this.expiredOptionsCount = expiredOptionsCount;
		this.marketName = marketName;
		this.profitAmount = profitAmount;
		this.isWinning = isWinning;
		init(Application.get(), null, 0);
	}
	public NotificationShoppingBag(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationShoppingBag(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationShoppingBag(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	protected void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_shopping_bag_layout, this);

		TextView textView = (TextView) view.findViewById(R.id.notification_message_text_view);
		ImageView winningBadge = (ImageView) view.findViewById(R.id.notification_winning_badge);

		textView.setText(setupMessage());
		
		if(isWinning){
			winningBadge.setVisibility(View.VISIBLE);
		}else{
			winningBadge.setVisibility(View.GONE);
		}
	}

	protected String setupMessage(){
		Context context = Application.get();
		String message;
		String expiredOptionsMessage = "";
		String profitMessage = "";
		if(expiredOptionsCount > 0){
			expiredOptionsMessage = context.getResources().getString(R.string.investmentsPerAssetExpired, expiredOptionsCount, marketName);
		}
				
		User user = Application.get().getUser();
		if(profitAmount > 0){
			String profitString = AmountUtil.getFormattedAmountWithoutSymbol(profitAmount, user.getCurrency());
			profitMessage = context.getResources().getString(R.string.addedToYourBalance, profitString);
		}
		message = expiredOptionsMessage + " " + profitMessage;
		return message;
	}
	
}
