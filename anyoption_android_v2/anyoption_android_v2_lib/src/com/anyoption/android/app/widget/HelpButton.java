package com.anyoption.android.app.widget;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.ActionFragment;
import com.anyoption.android.app.popup.BaseDialogFragment.OnDialogDismissListener;
import com.anyoption.android.app.popup.HelpInfoDialogFragment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class HelpButton extends RelativeLayout {
	
	private int type;
	private boolean isOptionPlusHelp;
	
	public HelpButton(Context context) {
		super(context);
		init(context, null, 0);
	}

	public HelpButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public HelpButton(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View.inflate(context,R.layout.help_button_layout, this);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!isEnabled()) {
					return;
				}
				setEnabled(false);

				HelpInfoDialogFragment helpDialog;
				if (type == ActionFragment.TYPE_ASSET) {
					helpDialog = HelpInfoDialogFragment.newInstance(Screen.ASSET_PAGE, isOptionPlusHelp);
				} else {
					helpDialog = HelpInfoDialogFragment.newInstance(isOptionPlusHelp);
				}

				helpDialog.setOnDismissListener(new OnDialogDismissListener() {

					@Override
					public void onDismiss() {
						setEnabled(true);
					}
				});
				
				Application.get().getFragmentManager().showDialogFragment(helpDialog);
			}
		});
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isOptionPlusHelp() {
		return isOptionPlusHelp;
	}

	public void setOptionPlusHelp(boolean isOptionPlusHelp) {
		this.isOptionPlusHelp = isOptionPlusHelp;
	}

}
