package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.popup.ReceiptDialogFragment;
import com.anyoption.common.beans.base.Investment;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationInvestOK extends LinearLayout{

	protected Investment investment;
	protected View view;
	
	public NotificationInvestOK(Investment investment) {
		super(Application.get());
		this.investment = investment;
		init(Application.get(), null, 0);
	}
	
	public NotificationInvestOK(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationInvestOK(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationInvestOK(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		view = View.inflate(context, R.layout.notification_invest_ok_layout, this);
		setupText();
		Button receiptButton = (Button) view.findViewById(R.id.notification_purchase_receipt_button);
		receiptButton.setOnClickListener(new OnClickListener() {
			private boolean isPressed = false;
			@Override
			public void onClick(View v) {
				if(!isPressed){
					isPressed = true;
					
					Bundle args = new Bundle();
					args.putSerializable(ReceiptDialogFragment.ARG_INVESTMENT_KEY, investment);
					Application.get().getFragmentManager().showDialogFragment(ReceiptDialogFragment.class, args);
				}
			}
		});
	}
	
	protected void setupText(){
		TextView messageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		
		
		if(investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL){
			messageTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.triangle_small_up_white, 0, 0, 0);
		}else if(investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT){
			messageTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.triangle_small_down_white, 0, 0, 0);
		}

		StringBuilder message = new StringBuilder();
		
		message.
		append(investment.getLevel()).
		append("   ").
		append(Application.get().getString(R.string.notificationInvestOKMessage)); 
		
		messageTextView.setText(message.toString());
	}
}
