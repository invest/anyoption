package com.anyoption.android.app.widget.form;

import java.util.Arrays;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.popup.QuestionnaireAnswersDialogFragment;
import com.anyoption.android.app.popup.QuestionnaireAnswersDialogFragment.OnItemSelectedListener;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class FormQuestionnaireItem extends LinearLayout implements OnItemSelectedListener, OnClickListener {

	public interface OnAnswerSelectedListener{
		void onAnswerSelected(Question question, Answer answer);
	}
	
	private boolean isEnabled;
	private QuestionnaireAnswersDialogFragment dialog;
	private OnAnswerSelectedListener answerSelectedListener;
	private Question question;
	private Answer answer;
	
	private LinearLayout rootView;
	private TextView questionTextView;
	private TextView answerTextView;
	private TextView errorTextView;

	public FormQuestionnaireItem(Context context) {
		super(context);
		init(context, null, 0);
	}

	public FormQuestionnaireItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	public FormQuestionnaireItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}
	
	protected void init(Context context, AttributeSet attrs, int defStyle) {
		rootView = (LinearLayout) View.inflate(getContext(), R.layout.questionanaire_item, this);
		questionTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_question_text_view);
		answerTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_answer_text_view);
		errorTextView = (TextView) rootView.findViewById(R.id.questionnaire_item_error_text_view);
		
		if(!isInEditMode()){
			rootView.setOnClickListener(this);
			questionTextView.setOnClickListener(this);
			answerTextView.setOnClickListener(this);
		}
	}

	public void showError(){
		answerTextView.setVisibility(View.GONE);
		errorTextView.setVisibility(View.VISIBLE);
	}
	
	public void setQuestion(Question question){
		this.question = question;
		questionTextView.setText(question.getDescription());
	}
	
	public void setAnswer(Answer answer){
		this.answer = answer;
		//Check if the Answer requires custom User Input:
		if(answer.getDescription().equals(Application.get().getString(R.string.questionnaireScreen1Question_5_Answer_29))){
			answerTextView.setVisibility(View.GONE);
		}else{
			answerTextView.setText(answer.getDescription());
			answerTextView.setVisibility(View.GONE);
			answerTextView.setVisibility(View.VISIBLE);
		}
		errorTextView.setVisibility(View.GONE);
	}
	
	public Question getQuestion(){
		return question;
	}
	
	public Answer getAnswer(){
		return answer;
	}

	public void setAnswerSelectedListener(OnAnswerSelectedListener answerSelectedListener) {
		this.answerSelectedListener = answerSelectedListener;
	}
	
	private void showDialog(){
		if(dialog == null){
			dialog = (QuestionnaireAnswersDialogFragment) Application.get().getFragmentManager().showDialogFragment(QuestionnaireAnswersDialogFragment.class, null);
			dialog.setOnItemSelectedListener(this);
			dialog.setItems(Arrays.asList(question.getAnswers()));
		}else{
			Application.get().getFragmentManager().showDialogFragment(dialog);
		}
	}

	@Override
	public void onItemSelected(Answer item, int position) {
		setAnswer(item);
		if(answerSelectedListener != null){
			answerSelectedListener.onAnswerSelected(question, item);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == rootView || v == questionTextView || v == answerTextView) {
			if (isEnabled) {
				showDialog();
			}
		}
	}
	
	public void setEnabled(boolean isEnabled){
		this.isEnabled = isEnabled;
	}
}
