package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NotificationError extends RelativeLayout{

	private String message;
	
	public NotificationError(String message) {
		super(Application.get());
		this.message = message;
		init(Application.get(), null, 0);
	}
	
	public NotificationError(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationError(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationError(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View view = View.inflate(context, R.layout.notification_error_layout, this);

		TextView errorMessageTextView = (TextView) view.findViewById(R.id.notification_message_text_view);
		if (message != null && !message.isEmpty()) {
			errorMessageTextView.setText(message);
		} else {
			// Default message:
			errorMessageTextView.setText(context.getResources().getString(R.string.error));
		}
	}

}
