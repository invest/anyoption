package com.anyoption.android.app.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

/**
 * View that adjust its height based on the image set as background.
 * @author Anastas Arnaudov
 *
 */
public class AutoScaleView extends View {
	  private final Drawable logo;

	  public AutoScaleView(Context context) {
	    super(context);
	    logo = getBackground();
	  }

	  public AutoScaleView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    logo = getBackground();
	  }

	  public AutoScaleView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    logo = getBackground();
	  }

	  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    if(logo != null){
	    	int width = MeasureSpec.getSize(widthMeasureSpec);
	    	int height = width * logo.getIntrinsicHeight() / logo.getIntrinsicWidth();
	    	setMeasuredDimension(width, height);	    	
	    }
	  }
}
