package com.anyoption.android.app.widget.notification;

import com.anyoption.android.app.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class NotificationNoInternet extends RelativeLayout{

	public NotificationNoInternet(Context context) {
		super(context);
		init(context, null, 0);
	}
	public NotificationNoInternet(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}
	public NotificationNoInternet(Context context, AttributeSet attrs,int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		View.inflate(context, R.layout.notification_no_internet_layout, this);
	}

}
