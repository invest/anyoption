package com.anyoption.android.app.popup;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.CreditCardItem;
import com.anyoption.android.app.popup.QuestionnaireAnswersDialogFragment.OnItemSelectedListener;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CreditCardListView;
import com.anyoption.android.app.widget.CreditCardListView.CcListType;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.json.results.CreditCardsMethodResult;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class DocumentsCreditCardDialogFragment extends BaseDialogFragment implements OnItemClickListener {

	public static final String TAG = DocumentsCreditCardDialogFragment.class.getSimpleName();

	public interface OnCreditCardSelectedListener{
		void onCreditCardSelected(CreditCardItem item, int position);
	}
	
	private OnCreditCardSelectedListener onCreditCardSelectedListener;
	private List<CreditCardItem> items = new ArrayList<CreditCardItem>();
	private ListView listView;
	private ListViewAdapter adapter;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static DocumentsCreditCardDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireAnswersDialogFragment.");
		DocumentsCreditCardDialogFragment fragment = new DocumentsCreditCardDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected int getContentLayout() {
		return R.layout.questionnaire_answers_popup;
	}

	@Override
	protected void onCreateContentView(View contentView) {
		listView = (ListView) contentView.findViewById(R.id.questionnaire_answers_popup_list_view);
		adapter = new ListViewAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	public void setOnCreditCardSelectedListener(OnCreditCardSelectedListener onCreditCardSelectedListener){
		this.onCreditCardSelectedListener = onCreditCardSelectedListener;
	}
	
	public void setItems(List<CreditCardItem> items){
		this.items = items;
		if(adapter != null){
			adapter.notifyDataSetChanged();
		}
	}

	private class ListViewItemHolder {
		public TextView textView;
		public ImageView imageView;
	}

    private class ListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	CreditCardItem item = items.get(position);
            ListViewItemHolder holder;
            if (convertView == null) {
            	LayoutInflater inf = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inf.inflate(R.layout.documents_credit_card_popup_item, parent, false);
                holder = new ListViewItemHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.upload_documents_cc_item_name);
                holder.imageView = (ImageView) convertView.findViewById(R.id.upload_documents_cc_item_image);
                convertView.setTag(holder);
            }else{
                holder = (ListViewItemHolder) convertView.getTag();
            }

            holder.textView.setText(item.getName());
            holder.imageView.setImageDrawable(item.getDrawable());

            return convertView;
        }
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(onCreditCardSelectedListener != null){
			onCreditCardSelectedListener.onCreditCardSelected(items.get(position), position);
			dismiss();
		}
	}

}