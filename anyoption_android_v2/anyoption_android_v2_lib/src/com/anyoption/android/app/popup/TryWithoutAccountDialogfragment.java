package com.anyoption.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.manager.PopUpManager.OnConfirmPopUpButtonPressedListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class TryWithoutAccountDialogfragment extends BaseDialogFragment {
	public static final String TAG = TryWithoutAccountDialogfragment.class.getSimpleName();

	private OnConfirmPopUpButtonPressedListener onConfirmPopUpButtonPressedListener;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * @return
	 */
	public static TryWithoutAccountDialogfragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new TryWithoutAccountDialogfragment.");
		TryWithoutAccountDialogfragment fragment = new TryWithoutAccountDialogfragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		View rootView = contentView.findViewById(R.id.try_without_account_popup_root_view);
		rootView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Do nothing
			}
		});
		View noButton = contentView.findViewById(R.id.try_without_account_confirm_no_button);
		View yesButton = contentView.findViewById(R.id.try_without_account_confirm_yes_button);
		
		OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(onConfirmPopUpButtonPressedListener != null){
					onConfirmPopUpButtonPressedListener.onNegative();
				}
			}
		};

		noButton.setOnClickListener(clickListener);
		
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(onConfirmPopUpButtonPressedListener != null){
					onConfirmPopUpButtonPressedListener.onPositive();
				}
			}
		});
		
		setCancelable(false);
	}

	public OnConfirmPopUpButtonPressedListener getOnConfirmPopUpButtonPressedListener() {
		return onConfirmPopUpButtonPressedListener;
	}

	public void setOnConfirmPopUpButtonPressedListener(OnConfirmPopUpButtonPressedListener onConfirmPopUpButtonPressedListener) {
		this.onConfirmPopUpButtonPressedListener = onConfirmPopUpButtonPressedListener;
	}
	@Override
	protected int getContentLayout() {
		return R.layout.try_without_account_confirm_popup_layout;
	}

}
