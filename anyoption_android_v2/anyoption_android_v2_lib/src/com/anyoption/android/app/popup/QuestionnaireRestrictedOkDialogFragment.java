package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.common.service.requests.IsKnowledgeQuestionMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.CheckBox;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class QuestionnaireRestrictedOkDialogFragment extends OkDialogFragment {

	public static final String TAG = QuestionnaireRestrictedOkDialogFragment.class.getSimpleName();

	CheckBox acceptCheckbox;
	TextView thirdMessageTextView;

	protected boolean isClicked = false;

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static QuestionnaireRestrictedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireRestrictedOkDialogFragment.");
		QuestionnaireRestrictedOkDialogFragment fragment = new QuestionnaireRestrictedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);

		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
		isClicked = false;
	}

	@SuppressWarnings("unchecked")
	public void updateIsKnowledgeQuestionCallback(Object resultObj) {
		application.getCommunicationManager().requestService(this, "getUserCallback", application.getServiceMethodGetUser(), new UserMethodRequest(), application.getUserMethodResultClass(), false, false);
	}

	public void getUserCallback(Object resultObj) {
		UserMethodResult result = (UserMethodResult) resultObj;
		
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			application.saveUser(result);
			RegulationUtils.showUploadDocuments();
		}
		isClicked = false;
		dismiss();
		
	}
	
	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {

		thirdMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_restricted_popup_third_message);

		acceptCheckbox = (CheckBox) contentView.findViewById(R.id.questionnaire_restricted_popup_checkbox);

		TextUtils.decorateInnerText(thirdMessageTextView, null, thirdMessageTextView.getText().toString(),
				getString(R.string.questionnaire_restricted_popup_third_message), Font.ROBOTO_BOLD, 0, 0);

		View okButton = contentView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (acceptCheckbox.isChecked()) {
					if (!isClicked) {
						IsKnowledgeQuestionMethodRequest request = new IsKnowledgeQuestionMethodRequest();
						request.setKnowledge(true);
						request.setUserId(application.getUser().getId());
						request.setUserName(application.getUser().getUserName());
						application.getCommunicationManager().requestService(QuestionnaireRestrictedOkDialogFragment.this, "updateIsKnowledgeQuestionCallback",
								Constants.SERVICE_UPDATE_IS_KNOWLEDGE_QUESTION, request, MethodResult.class, false,
								false);
						isClicked = true;
					}
				} else {
					isClicked = false;
					dismiss();
				}
			}
		});
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_questionnaire_restricted;
	}

	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_QUEST_RESTRICTED;
	}
	
}
