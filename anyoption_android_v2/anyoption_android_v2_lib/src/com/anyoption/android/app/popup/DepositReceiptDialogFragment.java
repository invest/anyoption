package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Custom Dialog Fragment showing DEPOSIT receipt.
 * 
 * @author Anastas Arnaudov
 * 
 */
public class DepositReceiptDialogFragment extends BaseDialogFragment {

	private String amount;
	private String date;
	protected long id;

	public static DepositReceiptDialogFragment newInstance(Bundle bundle) {
		DepositReceiptDialogFragment fragment = new DepositReceiptDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getArguments();
		if (bundle != null) {
			// Parse arguments:
			amount = bundle.getString(Constants.EXTRA_DEPOSIT_AMOUNT);
			date = bundle.getString(Constants.EXTRA_DEPOSIT_DATE);
			id = bundle.getLong(Constants.EXTRA_DEPOSIT_ID);
		}

	}

	@Override
	protected void onCreateContentView(View contentView) {
		Button okButton = (Button) contentView.findViewById(R.id.deposit_dialog_ok_button);
		okButton.setText(getString(R.string.ok).toUpperCase(Application.get().getLocale()));
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DepositReceiptDialogFragment.this.dismiss();
			}
		});

		TextView amountTextView = (TextView) contentView.findViewById(R.id.deposit_dialog_amount);
		amountTextView.setText(amount);

		TextView balanceLabel = (TextView) contentView
				.findViewById(R.id.deposit_dialog_balance_label);
		balanceLabel.setText(getString(R.string.balance) + ":");

		TextView balanceTextView = (TextView) contentView.findViewById(R.id.deposit_dialog_balance);
		balanceTextView.setText(Application.get().getUser().getBalanceWF());

		TextView dateTextView = (TextView) contentView.findViewById(R.id.deposit_dialog_date);
		dateTextView.setText(date);

		TextView idTextView = (TextView) contentView.findViewById(R.id.deposit_dialog_id);
		idTextView.setText(getIdValue());
	}
	
	protected String getIdValue() {
		return getString(R.string.receipt_transactionid, id);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.deposit_dialog;
	}

}
