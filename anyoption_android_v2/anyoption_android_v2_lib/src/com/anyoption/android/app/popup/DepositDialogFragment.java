package com.anyoption.android.app.popup;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.BaseDialogFragment;
import com.anyoption.android.app.util.constants.Constants;

/**
 * Custom Dialog Fragment showing DEPOSIT receipt.
 * @author Anastas Arnaudov
 *
 */
public class DepositDialogFragment extends BaseDialogFragment {

	private String amount;
	private String date;
	private long id;

	public static DepositDialogFragment newInstance(Bundle bundle) {
		DepositDialogFragment fragment = new DepositDialogFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullscreenDialog);

		Bundle bundle = getArguments();
		if(bundle != null){
			//Parse arguments:
			amount = bundle.getString(Constants.EXTRA_DEPOSIT_AMOUNT);
			date = bundle.getString(Constants.EXTRA_DEPOSIT_DATE);
			id = bundle.getLong(Constants.EXTRA_DEPOSIT_ID);
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.deposit_dialog, container, false);
		root.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
		Button okButton = (Button) root.findViewById(R.id.deposit_dialog_ok_button);
		okButton.setText(getString(R.string.ok).toUpperCase(Application.get().getLocale()));
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DepositDialogFragment.this.dismiss();
			}
		});

		TextView amountTextView = (TextView) root.findViewById(R.id.deposit_dialog_amount);
		amountTextView.setText(amount);
		
		TextView balanceLabel = (TextView) root.findViewById(R.id.deposit_dialog_balance_label);
		balanceLabel.setText(getString(R.string.balance) + ":");
		
		TextView balanceTextView = (TextView) root.findViewById(R.id.deposit_dialog_balance);
		balanceTextView.setText(Application.get().getUser().getBalanceWF());
		
		TextView dateTextView = (TextView) root.findViewById(R.id.deposit_dialog_date);
		dateTextView.setText(date);

		TextView idTextView = (TextView) root.findViewById(R.id.deposit_dialog_id);
		idTextView.setText(getString(R.string.id) + ": " + String.valueOf(id));
		
		return root;
	}

}
