package com.anyoption.android.app.popup;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.manager.PopUpManager;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class QuestionnaireBlockedOkDialogFragment extends OkDialogFragment {

	public static final String TAG = QuestionnaireBlockedOkDialogFragment.class.getSimpleName();
	
	TextView firstMessageTextView;
	TextView secondMessageTextView;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static QuestionnaireBlockedOkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new QuestionnaireRestrictedOkDialogFragment.");
		QuestionnaireBlockedOkDialogFragment fragment = new QuestionnaireBlockedOkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, false));
		}
	}
	
	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		
		firstMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_blocked_popup_first_message);
		secondMessageTextView = (TextView) contentView.findViewById(R.id.questionnaire_blocked_popup_second_message);
		
		OnInnerTextClickListener supportTeamLinkListener = new OnInnerTextClickListener() {
			@Override
			public void onInnerTextClick(TextView textView, String text, String innerText) {
				dismiss();
				application.postEvent(new NavigationEvent(Screen.SUPPORT, NavigationType.DEEP));
			}
		};
		
		TextUtils.decorateInnerText(firstMessageTextView, null, firstMessageTextView.getText().toString(), firstMessageTextView.getText().toString(), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.makeClickableInnerText(secondMessageTextView, null, secondMessageTextView.getText().toString(), secondMessageTextView.getText().toString(), null, 0, 0, false, supportTeamLinkListener);
		

		View okButton = contentView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_questionnaire_blocked;
	}
	
	@Override
	public int getPopupCode(){
		return PopUpManager.POPUP_QUEST_BLOCKED;
	}
}
