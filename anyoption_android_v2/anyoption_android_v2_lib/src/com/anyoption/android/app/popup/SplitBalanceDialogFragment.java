package com.anyoption.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.event.UserUpdatedEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SplitBalanceDialogFragment extends BaseDialogFragment {

	public static final String TAG = SplitBalanceDialogFragment.class.getSimpleName();

	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static SplitBalanceDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new SplitBalanceDialogFragment.");
		SplitBalanceDialogFragment fragment = new SplitBalanceDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		bundle.putBoolean(ARG_CLOSE_DIALOG_ON_BG_CLICK_KEY, false);
		
		
		fragment.setArguments(bundle);
		return fragment;
	}

	private boolean isRequestOK;
	private boolean shouldShowPopup;
	protected String cashBalance;
	protected String bonusBalance;
	protected String totalBalance;
	
	protected TextView cashBalanceTextView;
	protected TextView bonusBalanceTextView;
	protected TextView totalBalanceTextView;

	private View loadingView;
	private int type;
	private long investmentId;
	private View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			type = args.getInt(Constants.EXTRA_TYPE);
			investmentId = args.getLong(Constants.EXTRA_INVESTMENT_ID);
		}
		getSplitBalance();
	}

	protected void getSplitBalance(){
		setupLoading(true);
		DepositBonusBalanceMethodRequest request = new DepositBonusBalanceMethodRequest();
		request.setUserId(application.getUser().getId());
		request.setBalanceCallType(type);
		request.setInvestmentId(investmentId);
		if(!application.getCommunicationManager().requestService(this, "getSplitBalanceCallback", Constants.SERVICE_GET_SPLIT_BALANCE, request, DepositBonusBalanceMethodResult.class, false, false)){
			setupLoading(false);
			dismiss();
		}
	}
	
	public void getSplitBalanceCallback(Object resObj){
		DepositBonusBalanceMethodResult result = (DepositBonusBalanceMethodResult) resObj;
		if(result != null && result instanceof DepositBonusBalanceMethodResult && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
			isRequestOK = true;
			if(result.getDepositBonusBalanceBase() != null){
				DepositBonusBalanceBase depositBonusBalanceBase = result.getDepositBonusBalanceBase();
				shouldShowPopup = depositBonusBalanceBase.isShow();
				if(!shouldShowPopup){
					dismiss();
					return;
				}else{
					if(rootView != null){
						rootView.setVisibility(View.VISIBLE);
					}
				}
				
				cashBalance  = AmountUtil.getUserFormattedAmount(depositBonusBalanceBase.getDepositCashBalance());
				bonusBalance = AmountUtil.getUserFormattedAmount(depositBonusBalanceBase.getBonusBalance());
				totalBalance = AmountUtil.getUserFormattedAmount(depositBonusBalanceBase.getTotalBalance());
				
				setupLoading(false);
				setupBalances();
				
				//Update User Balance:
				if(type == DepositBonusBalanceMethodRequest.CALL_TOTAL_AMOUNT_BALANCE){
					User user = application.getUser();
					long newBalance = depositBonusBalanceBase.getTotalBalance();
					user.setBalance(newBalance);
					user.setBalanceWF(AmountUtil.getFormattedAmount(newBalance,user.getCurrency()));
					application.postEvent(new UserUpdatedEvent());
				}
				/////////////////////
			}else{
				Log.e(TAG, "Error in getSplitBalanceCallback");
				dismiss();
			}
		}else{
			Log.e(TAG, "Error in getSplitBalanceCallback");
			ErrorUtils.handleResultError(result);
			dismiss();
		}
	}
	
	private void setupLoading(boolean isLoading){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	private void setupBalances() {
		if(totalBalanceTextView != null){
			cashBalanceTextView.setText(cashBalance);
			bonusBalanceTextView.setText(bonusBalance);
			totalBalanceTextView.setText(totalBalance);
		}
	}

	@Override
	protected void onCreateContentView(View contentView) {
		rootView = contentView;
		loadingView = contentView.findViewById(R.id.loadingView);
		
		cashBalanceTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_cash_text_view);
		bonusBalanceTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_bonus_text_view);
		totalBalanceTextView = (TextView) contentView.findViewById(R.id.split_balance_popup_total_text_view);
		
		if(totalBalance != null){
			setupLoading(false);
		}
		
		setupOKButton();
		
		if(isRequestOK){
			if(shouldShowPopup){
				rootView.setVisibility(View.VISIBLE);
			}else{
				dismiss();
			}
		}
	}

	protected void setupOKButton(){
		View okButton = rootView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
	@Override
	protected int getContentLayout() {
		return R.layout.popup_split_balance;
	}

}
