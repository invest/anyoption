package com.anyoption.android.app.popup;

import com.anyoption.android.app.R;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class OkDialogFragment extends BaseDialogFragment {

	public static final String TAG = OkDialogFragment.class.getSimpleName();

	private String message;
	
	/**
	 * Use this method to create new instance of the fragment.
	 * 
	 * @return
	 */
	public static OkDialogFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created new OkDialogFragment.");
		OkDialogFragment fragment = new OkDialogFragment();
		if (bundle == null) {
			bundle = new Bundle();
		}
		bundle.putBoolean(ARG_SHOW_CLOSE_BUTTON_KEY, false);
		bundle.putInt(ARG_BACKGROUND_RESOURCE_ID_KEY, R.color.transparent);
		
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
			message = args.getString(Constants.EXTRA_MESSAGE);
			setCancelable(args.getBoolean(Constants.EXTRA_CANCELABLE, true));
		}

	}
	
	/**
	 * Set up content.
	 * 
	 * @param contentView
	 */
	@Override
	protected void onCreateContentView(View contentView) {
		TextView messageTextView = (TextView) contentView.findViewById(R.id.min_deposit_popup_message_text);
		messageTextView.setText(message);
		//TODO format the amount to be bold
		View okButton = contentView.findViewById(R.id.ok_button);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
//		setCancelable(false);
	}

	@Override
	protected int getContentLayout() {
		return R.layout.popup_min_deposit;
	}

}
