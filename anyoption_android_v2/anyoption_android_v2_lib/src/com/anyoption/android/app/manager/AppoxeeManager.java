package com.anyoption.android.app.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.OnActivityResultListener;
import com.anyoption.android.app.activity.LaunchActivity;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.ExpiredOptionsEvent;
import com.anyoption.android.app.event.InsuranceBoughtEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.PushNotificationsPermissionEvent;
import com.anyoption.android.app.event.ReverseWithdrawEvent;
import com.anyoption.android.app.event.UpdateUserInfoEvent;
import com.anyoption.android.app.event.WithdrawEvent;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.User;
import com.appoxee.Appoxee;
import com.appoxee.asyncs.initAsync;
import com.appoxee.inbox.Message;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * Manager for the APPOXEE functionality.
 * @author Anastas Arnaudov
 *
 */
public class AppoxeeManager implements OnActivityResultListener {

	public static final String TAG = AppoxeeManager.class.getSimpleName();

	public static boolean isInit = false;
	
	public enum FieldType{
		NUMERIC, STRING, DATE;
	}

	public interface InboxListener{
		public void gotInboxMessages(List<Message> inboxMessagesList);
	}
	
	public static final String APP_KEY 					= "";
	public static final String SECRET_KEY 				= "";

	public static final String ALIAS_PREFIX_LIVE 		= "live_";
	public static final String ALIAS_PREFIX_TEST 		= "test_";
	public static final String ALIAS_PREFIX_BGTEST 		= "bgtest_";

	//###################################################################
	//###########				PARAMETERS KEYS				#############
	//###################################################################

	public static final String USER_ID 								= "UserID";
	public static final String USER_NAME 							= "UserName";
	public static final String USER_EMAIL	 						= "Email";
	public static final String USER_FIRST_NAME 						= "FirstName";
	public static final String USER_LAST_NAME 						= "LastName";
	public static final String USER_BALANCE							= "Balance";
	public static final String USER_FIRST_DEPOSIT_AMOUNT			= "FTDAmount";
	public static final String USER_FIRST_DEPOSIT_DOLLAR_AMOUNT		= "FTDAmountUSD";
	public static final String USER_DEPOSIT_AMOUNT					= "LastDepositAmount";
	public static final String USER_DEPOSIT_DOLLAR_AMOUNT			= "LastDepositAmount USD";
	public static final String USER_TIME_LAST_INVEST				= "LastInvestmentDate";
	public static final String USER_IS_DEPOSIT_MADE 				= "IsDepositMade";
	public static final String USER_COUNTRY 						= "Country";
	public static final String USER_SKIN 							= "Skin";
	public static final String USER_CURRENCY 						= "Currency";

	public static final String IS_NOTIFICATIONS_ALLOWED 			= "NotificationsAllowed";

	public static final String DEVICE_OS_VERSION 					= "OSVersion";
	public static final String DEVICE_INFO 							= "Device";

	public static final String COMBINATION_ID 						= "CombinationID";
	public static final String DYNAMIC_PARAM	 					= "DP";

	//###################################################################

	private Application application;

	public AppoxeeManager(Application application){
		Log.d(TAG, "On Create");
		this.application = application;
		application.registerForEvents(this, LoginLogoutEvent.class, InvestmentEvent.class, UpdateUserInfoEvent.class, DepositEvent.class, PushNotificationsPermissionEvent.class, ExpiredOptionsEvent.class, WithdrawEvent.class, ReverseWithdrawEvent.class, InsuranceBoughtEvent.class);
		application.addOnActivityResultListener(this);
	}

	protected String getApiKey(){
		return AppoxeeManager.APP_KEY;
	}
	
	protected String getSecretKey(){
		return AppoxeeManager.SECRET_KEY;
	}
	
	public void init(Activity activity) {
		if(!AppoxeeManager.isInit){
			try {
				String activityClassName = application.getSubClassForClass(LaunchActivity.class).getName();
				
				// This will cause the pushToken value to be reseted locally even on an upgrade, thus the push token received by GCM will be updated.
				com.appoxee.AppoxeeManager.setmContext(activity.getApplicationContext());
				if (com.appoxee.AppoxeeManager.getSharedPreferences().getBoolean("resetRegistration", false)==false){
					com.appoxee.AppoxeeManager.setConfiguration("registration_values_pt", "");
					com.appoxee.AppoxeeManager.setConfiguration("resetRegistration",true);
				}
				/////////////////////////////////////////////	
				
				new initAsync(activity.getApplicationContext(), getApiKey(), getSecretKey(), activityClassName, true).execute();
				com.appoxee.AppoxeeManager.setDebug(false);
				//TODO check the Intent bundle for null values in the map
				Appoxee.parseExtraFields(activity.getIntent().getExtras());
				AppoxeeManager.isInit = true;
			} catch (Exception e) {
				Log.e(TAG, "Could NOT init", e);
			}
		}
	}

	public void hasInboxMessages(final InboxListener inboxListener){
		new AsyncTask<Void, Void, List<Message>>() {
			@Override
			protected List<Message> doInBackground(Void... params) {
				Log.d(TAG, "Trying to get the Inbox Messages");
				List<Message> inboxMessagesList = null;
				if(Appoxee.isReady()){
					inboxMessagesList = Appoxee.getInboxMessages();
				}
				if(inboxMessagesList != null){
					Log.d(TAG, "Inbox Messages List size is " + inboxMessagesList.size());
					if(!inboxMessagesList.isEmpty()){
						try {
							com.appoxee.AppoxeeManager.getClient().reportDeviceMessageActionOpen_V3(inboxMessagesList.get(0).getId());
						} catch (Exception e) {
							Log.e(TAG, "Trying to report In-App Message opened.",e);
						}
					}
					clearInboxMessages(inboxMessagesList);
				}else{
					Log.e(TAG, "Inbox Messages List is null");
				}
				
				return inboxMessagesList;
			}
			@Override
			protected void onCancelled(List<Message> result) {
				inboxListener.gotInboxMessages(result);
				super.onCancelled(result);
			}
			@Override
			protected void onPostExecute(List<Message> result) {
				inboxListener.gotInboxMessages(result);
			}
		}.execute();
	}
	
	
	protected void clearInboxMessages(final List<Message> inboxMessagesList){
		if(inboxMessagesList != null){
			new AsyncTask<String, Void, String>() {
				@Override
				protected String doInBackground(String... params) {
					try {
						Log.d(TAG, "Trying to clear the inbox messages");
						if(Appoxee.isReady()){
							for(Message message : inboxMessagesList){
								Appoxee.deleteMessage(message.getId());
							}
						}
					} catch (Exception ex) {
						Log.e(TAG, ex.getLocalizedMessage());
					}
					return null;
				}
			}.execute();
		}else{
			Log.e(TAG, "Could not clear messages. Inbox messages list is null");
		}
	}
	
	/*private void unregisterOnBackground() {
		new AsyncTask<String, Void, String>() {
			@Override
			protected String doInBackground(String... params) {
			    try {
			        	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(application);
			       
			       gcm.unregister();
			    } catch (Exception ex) {
			    	Log.e(TAG, ex.getLocalizedMessage());
			    }
			    return null;
			}
		}.execute();
	}*/
	
	public void onStart(){
		Appoxee.onStart();
	}
	public void onStop(){
		Appoxee.onStop();
	}
	public void onNewIntent(Intent intent){
		Bundle bundle = intent.getExtras();
		Appoxee.parseExtraFields(bundle);
	}
	public void onResume(){
		 // If Inbox implemented and you have a method for updating your inbox�s Badge
		 //Appoxee.updateMsgBadge();
	}

	protected void setDeviceAlias(){
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				if(!application.isLoggedIn()){
					Log.e(TAG, "Could NOT set Device Alias : User is NOT logged in.");
					return null;
				}

				long userId = application.getUser().getId();
				String deviceAlias = ALIAS_PREFIX_LIVE;
				if(application.getServiceURL().startsWith("http://www.bgtestenv")){
					deviceAlias = ALIAS_PREFIX_BGTEST;
				}else if(application.getServiceURL().startsWith("http://www.testenv")){
					deviceAlias = ALIAS_PREFIX_TEST;
				}else if(application.getServiceURL().startsWith("http://www.pavelhe.bg")){
					deviceAlias = ALIAS_PREFIX_BGTEST;
				}

				deviceAlias += userId;

				if(Appoxee.isReady()){
					if (Appoxee.setDeviceAlias(deviceAlias)) {
						com.appoxee.utils.Utils.Debug("setDeviceAlias Success");
						Log.d(TAG, "Device Alias = " + deviceAlias);
					} else {
						com.appoxee.utils.Utils.Debug("setDeviceAlias Failed");
						Log.e(TAG, "Could NOT set Device Alias");
					}
				}else{
					Log.e(TAG, "Could NOT set Device Alias");
				}
				return null;
			}
		}.execute();
	}

	protected void enablePushNotifications(final boolean enable){
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				if(Appoxee.isReady()){
					if(Appoxee.OptOut("pushToken", enable)){
						Log.d(TAG, "Changing Push Notifications Settings --> Success.");
					}else{
						Log.e(TAG, "Changing Push Notifications Settings --> Failure.");
					}
				}else{
					Log.e(TAG, "Changing Push Notifications Settings --> Failure.");
				}
				return null;
			}
		}.execute();
	}

	protected void setUserProperties(final Map<String, String> map){
		putMandatoryParameters(map);
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				if(Appoxee.isReady()){
					for(Map.Entry<String, String> entity : map.entrySet()){
						String key = entity.getKey();
						String value = entity.getValue();
						try {
							if(Appoxee.setCustomField(key, value)){
								Log.d(TAG, "Setting User Property : " + key + " = " + value + " --> Success.");
							}else{
								Log.e(TAG, "Setting User Property : " + key + " = " + value + " --> Failure.");
							}
						} catch (Exception e) {
							Log.e(TAG, "Setting User Property : " + key + " = " + value + " --> Failure.", e);
						}
					}
				}else{
					Log.e(TAG, "Setting User Properties --> Failure.");
				}
				return null;
			}
		}.execute();
	}

	private void putMandatoryParameters(Map<String, String> map){
		if(Utils.getCombinationId() != null){
			map.put(COMBINATION_ID, String.valueOf(Utils.getCombinationId()));
		}else{
			map.put(COMBINATION_ID, "null");
		}
		if(Utils.getDynamicParam() != null){
			map.put(DYNAMIC_PARAM, Utils.getDynamicParam());
		}else{
			map.put(DYNAMIC_PARAM, "null");
		}
	}

	private Map<String, String> putDeviceParameters(Map<String, String> map){
		map.put(DEVICE_OS_VERSION, "OS " + Utils.getDeviceOSVersion());
		map.put(DEVICE_INFO, Utils.getDeviceManufacturer() + " " + Utils.getDeviceModel());
		return map;
	}

	private Map<String, String> putUserParameters(User user, Map<String, String> map){
		map.put(USER_ID, String.valueOf(user.getId()));
		map.put(USER_NAME, user.getUserName());
		map.put(USER_EMAIL, user.getEmail());
		map.put(USER_FIRST_NAME, user.getFirstName());
		map.put(USER_LAST_NAME, user.getLastName());
		if(user.getBalanceWF() != null){
//			map.put(USER_BALANCE, user.getBalanceWF().replace(user.getCurrencySymbol(), ""));
		}
		map.put(USER_BALANCE, String.valueOf(user.getBalance()));
		map.put(USER_COUNTRY, String.valueOf(user.getCountryId()));
		if(user.getCurrency() != null){
			map.put(USER_CURRENCY, user.getCurrency().getCode());
		}
		map.put(USER_SKIN, String.valueOf(user.getSkinId()));
		map.put(USER_IS_DEPOSIT_MADE, (user.getFirstDepositId() > 0) ? "true" : "false");
		return map;
	}

	protected void inApp(){
	}

	public void showFeedback(){
		Appoxee.ShowFeedback();
	}
	
	//########################################################################################
	//###############					EVENTS CALLBACKs				######################
	//########################################################################################

	public void onEvent(LoginLogoutEvent event) {
		if(event.getType() == Type.REGISTER || event.getType() == Type.LOGIN){
			setDeviceAlias();
			User user = event.getUserResult().getUser();
			Map<String, String> map = new HashMap<String, String>();
			putUserParameters(user, map);
			putDeviceParameters(map);
			if(event.getType() == Type.REGISTER){
				map.remove(USER_IS_DEPOSIT_MADE);
				map.remove(USER_BALANCE);
			}
			setUserProperties(map);
		}
	}

	public void onEvent(UpdateUserInfoEvent event) {
		User user = Application.get().getUser();
		Map<String, String> map = new HashMap<String, String>();
		putUserParameters(user, map);
		putDeviceParameters(map);
		setUserProperties(map);
	}

	public void onEvent(InvestmentEvent event) {
		if(event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVEST_SUCCESS || event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVESTMENT_SOLD){
			User user = Application.get().getUser();
			Map<String, String> map = new HashMap<String, String>();
			map.put(USER_ID, String.valueOf(user.getId()));
			map.put(USER_BALANCE, String.valueOf(user.getBalance()));
			if(event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVEST_SUCCESS){
				map.put(USER_TIME_LAST_INVEST, String.valueOf(event.getInvestment().getTimeCreated().getTime()));
			}
			setUserProperties(map);
		}
	}

	public void onEvent(DepositEvent event) {
		boolean isFirstDeposit = event.isFirstDeposit();
		User user = Application.get().getUser();
		Map<String, String> params = new HashMap<String, String>();
		params.put(USER_ID, String.valueOf(user.getId()));
		params.put(USER_BALANCE, String.valueOf(user.getBalance()));
		if(isFirstDeposit){
			Map<String, String> paramsFTD = new HashMap<String, String>();
			paramsFTD.putAll(params);
			paramsFTD.put(USER_FIRST_DEPOSIT_AMOUNT, String.valueOf(event.getAmount()));
			paramsFTD.put(USER_FIRST_DEPOSIT_DOLLAR_AMOUNT, String.valueOf(event.getDollarAmount()));
			setUserProperties(paramsFTD);
		}
		//If it is First Deposit -> we send also a Deposit event along with it:
		params.put(USER_DEPOSIT_AMOUNT, String.valueOf(event.getAmount()));
		params.put(USER_DEPOSIT_DOLLAR_AMOUNT, String.valueOf(event.getDollarAmount()));
		setUserProperties(params);
	}

	public void onEvent(PushNotificationsPermissionEvent event) {
		User user = Application.get().getUser();
		if(user != null){
			enablePushNotifications(event.isPushNotificationsAllowed());
			Map<String, String> params = new HashMap<String, String>();
			params.put(USER_ID, String.valueOf(user.getId()));
			params.put(IS_NOTIFICATIONS_ALLOWED, (event.isPushNotificationsAllowed()) ? "true" : "false");
			setUserProperties(params);
		}
	}

	public void onEvent(ExpiredOptionsEvent event) {
		User user = Application.get().getUser();
		Map<String, String> params = new HashMap<String, String>();
		params.put(USER_ID, String.valueOf(user.getId()));
		params.put(USER_BALANCE, String.valueOf(user.getBalance()));
		setUserProperties(params);
	}

	public void onEvent(WithdrawEvent event) {
		User user = Application.get().getUser();
		Map<String, String> params = new HashMap<String, String>();
		params.put(USER_ID, String.valueOf(user.getId()));
		params.put(USER_BALANCE, String.valueOf(user.getBalance()));
		setUserProperties(params);
	}

	public void onEvent(ReverseWithdrawEvent event) {
		User user = Application.get().getUser();
		Map<String, String> params = new HashMap<String, String>();
		params.put(USER_ID, String.valueOf(user.getId()));
		params.put(USER_BALANCE, String.valueOf(user.getBalance()));
		setUserProperties(params);
	}

	public void onEvent(InsuranceBoughtEvent event) {
		User user = Application.get().getUser();
		Map<String, String> params = new HashMap<String, String>();
		params.put(USER_ID, String.valueOf(user.getId()));
		params.put(USER_BALANCE, String.valueOf(user.getBalance()));
		setUserProperties(params);
	}
	//########################################################################################

	@Override
	public void onActivityResultEvent(int requestCode, int resultCode, Intent intent) {
		Log.d(TAG, "onActivityResultEvent");
	}

}
