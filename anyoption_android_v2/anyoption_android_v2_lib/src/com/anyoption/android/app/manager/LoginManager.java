package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.LoginFailedEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class LoginManager {
	public static final String TAG = LoginManager.class.getSimpleName();

	protected Application application;
	protected SharedPreferencesManager sharedPreferencesManager;
	protected PushNotificationsManager pushNotificationsManager;
	protected FacebookManager facebookManager;

	protected Fragment fragment;
	private Form form;
	protected RequestButton loginButton;
	private boolean isTablet;
	protected String userName;
	protected String password;
	private boolean rememberPassword;

	public LoginManager() {
		application = Application.get();
		sharedPreferencesManager = application.getSharedPreferencesManager();
		pushNotificationsManager = application.getPushNotificationsManager();
		facebookManager = application.getFacebookManager();
	}

	public void onCreateView(Fragment fragment, boolean isTablet, final FormEditText emailField,
			final FormEditText passwordField, final TextView forgotPassTextView, final RequestButton loginButton,
			final FormCheckBox rememberPasswordCheckBox) {

		this.fragment = fragment;
		this.isTablet = isTablet;
		this.loginButton = loginButton;

		rememberPassword = false;

		setUpUsernameField(emailField);
		setUpPasswordFields(passwordField, rememberPasswordCheckBox);

		form = new Form(new Form.OnSubmitListener() {
			@Override
			public void onSubmit() {
				UserMethodRequest request = new UserMethodRequest();
				userName = emailField.getValue();
				request.setUserName(userName);
				request.setLogin(true);
				password = passwordField.getValue();
				request.setPassword(password);
				rememberPassword = rememberPasswordCheckBox.isChecked();

				Log.d(TAG, "getUser Request");
				if (!Application.get().getCommunicationManager().requestService(LoginManager.this, "getUserCallBack",
						getServiceMethodName(), request, getResultClass(), isHandleError(), false)) {
					loginButton.stopLoading();
				}
			}
		});
		form.addItem(emailField, passwordField);

		forgotPassTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Application.get().postEvent(new NavigationEvent(Screen.FORGOT_PASSWORD, NavigationType.DEEP));
			}
		});

		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (form.checkValidity()) {
					loginButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});
				}
			}
		});

	}

	protected String getServiceMethodName() {
		return application.getServiceMethodGetUser();
	}

	protected Class<? extends MethodResult> getResultClass() {
		return UserMethodResult.class;
	}

	protected boolean isHandleError() {
		return true;
	}

	public void getUserCallBack(Object result) {
		Log.d(TAG, "getUser Response");

		UserMethodResult userMethodResult = (UserMethodResult) result;
		processLoginAttemptResult(userMethodResult, loginButton, fragment, isTablet, form);
	}

	public void processLoginAttemptResult(UserMethodResult userMethodResult, RequestButton loginButton,
			Fragment fragment, boolean isTablet, Form form) {
		if (userMethodResult == null || userMethodResult.getErrorCode() != Constants.ERROR_CODE_SUCCESS) {
			if (userMethodResult != null) {
				Log.e(TAG, "Could not log in -> Error_Code: " + userMethodResult.getErrorCode());
			} else {
				Log.e(TAG, "Could not log in! Result is null.");
			}
			loginButton.stopLoading();
			application.postEvent(new LoginFailedEvent());
			return;
		}

		// Etrader users should not log in AO app and vice versa. This check
		// does not allow it in
		// case the service does not return error.
		if ((application.getSkinId() != Skin.SKIN_ETRADER
				&& userMethodResult.getUser().getSkinId() == Skin.SKIN_ETRADER)
				|| (application.getSkinId() == Skin.SKIN_ETRADER
						&& userMethodResult.getUser().getSkinId() != Skin.SKIN_ETRADER)) {
			loginButton.stopLoading();

			application.getNotificationManager()
					.showNotificationError(fragment.getResources().getString(R.string.error));
			return;
		}

		///////////////////////////////
		if (!sharedPreferencesManager.getBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, false)) {
			sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_LOGGED_ATLEAST_ONCE, true);
			sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_TRADE_TOOLTIP_SHOWN, false);
		}

		sharedPreferencesManager.putBoolean(Constants.PREFERENCES_IS_FIRST_LOGIN, false);

		boolean shouldChangeLocale = false;
		if (application.getLocale() == null) {
			shouldChangeLocale = true;
		} else
			if (!application.getLocale().getLanguage().equals(userMethodResult.getUser().getLocale().getLanguage())) {
			shouldChangeLocale = true;
		}

		// Save the User:
		if (userMethodResult.getmId() != null && userMethodResult.getmId().length() > 0) {
			sharedPreferencesManager.putString(Constants.PREFERENCES_MID, userMethodResult.getmId());
		}
		if (userMethodResult.getEtsMid() != null && userMethodResult.getEtsMid().length() > 0) {
			sharedPreferencesManager.putString(Constants.PREFERENCES_ETS_MID, userMethodResult.getEtsMid());
		}

		if (rememberPassword) {
			sharedPreferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, password);
		} else {
			sharedPreferencesManager.putString(Constants.PREFERENCES_PASSWORD_LOGIN, null);
		}

		ScreenUtils.hideKeyboard();
		application.postEvent(new LoginLogoutEvent(Type.LOGIN, userMethodResult, shouldChangeLocale));
		// Save the login count:
		int loginCount = application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_LOGIN_COUNT, 0);
		application.getSharedPreferencesManager().putInt(Constants.PREFERENCES_LOGIN_COUNT, ++loginCount);
		////////////////////////////////////////////////////

		Bundle bundle = new Bundle();
		Screenable toScreen = configAfterLoginScreen(userMethodResult, bundle);

		// Change the Locale (which means we have to restart the Application, to
		// load appropriate resources)
		// after the next Screenable is chosen (depending on the Login flow)
		if (shouldChangeLocale) {
			// TODO add the bundle
			application.changeLocale(userMethodResult.getUser().getLocale().getLanguage(), toScreen);
		} else {
			doAfterLoginNavigation(toScreen, bundle);
		}
		form.clear();
	}

	protected Screenable configAfterLoginScreen(UserMethodResult userMethodResult, Bundle bundle) {
		Screenable toScreen = null;

		if (!(application.isLoggedIn() && application.getUser().getIsNeedChangePassword())) {
			User user = userMethodResult.getUser();
			if (!user.isAcceptedTerms()) {
				// The user has not accepted the Terms&Conditions (Example: He
				// was created on the Back-end):
				toScreen = Screen.AGREEMENT;
			} else if (pushNotificationsManager.isAppOpenedWithPush()) {
				boolean openDeposit = false;
				if (pushNotificationsManager.shouldCheckDeposit()) {
					openDeposit = !(userMethodResult.getUser().getBalance() > 0);
				}

				if (openDeposit) {
					toScreen = Screen.DEPOSIT_MENU;
				} else {
					toScreen = pushNotificationsManager.getToScreen();
					if (isTablet && toScreen == Screen.ASSET_PAGE) {
						// Closes the popup for login screen.
						application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
					}

					bundle.putAll(pushNotificationsManager.getScreenArguments());
				}
				pushNotificationsManager.setPushHandled();
			} else if (facebookManager.isDeepLink()) {
				Bundle deepLinkBundle = facebookManager.getDeepLinkBundle();
				toScreen = (Screenable) deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN);
				if (toScreen == Screen.REGISTER) {
					toScreen = application.getHomeScreen();
				}
				bundle.putAll(deepLinkBundle);
				facebookManager.handledDeepLink();
			} else if (userMethodResult.getUser().getFirstDepositId() == 0) {
				// The user has not made a Deposit and we show the First New
				// Credit Card Screen:
				toScreen = Screen.FIRST_NEW_CARD;
			} else if (application.isRegulated() && RegulationUtils.isAfterFirstDeposit()
					&& !RegulationUtils.isQuestionnaireFilled() && !RegulationUtils.isSuspended()) {
				// The user has not filled in the mandatory questionnaire:
				/*
				 * 
				 * SAME AS IN MainActivity.java : handleRestart()
				 * 
				 */
				UserRegulationBase regulationBase = Application.get().getUserRegulation();
				if (regulationBase != null) {
					if (regulationBase.getRegulationVersion() == 2) {
						toScreen = Screen.QUESTIONNAIRE_1;
					} else {
						toScreen = Screen.MANDATORY_QUESTIONNAIRE;
					}
				}
			} else if (RegulationUtils.isQuestionnaireFilled()) {
				/*
				 * 
				 * SAME AS IN MainActivity.java : handleRestart()
				 * 
				 */

				toScreen = application.getHomeScreen();

				if (RegulationUtils.showBlockedOrRestrictedPopups()) {
				}
				if (RegulationUtils.showAccountBlocked()) {
					if (RegulationUtils.showUploadDocuments()) {

					} else {
						return null;
					}
				} else {
					return null;
				}
			} else if (showBonuses(userMethodResult)) {
				// The user has new bonuses, show bonuses screen.
				toScreen = Screen.BONUSES;
			} else {
				toScreen = application.getHomeScreen();
			}

			// Check the Appoxee Rate:
			int loginCount = application.getSharedPreferencesManager().getInt(Constants.PREFERENCES_LOGIN_COUNT, 0);
			if (loginCount == 15) {
				application.getAppoxeeManager().showFeedback();
			}
			/////////////////////////

			// Final Check:
			if (toScreen == null) {
				Log.d(TAG, "The After Login Screen could NOT be configured. Setting the Home Screen.");
				toScreen = application.getHomeScreen();
			}
		} else {
			application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP, Screenable.SCREEN_FLAG_REMOVE_MENU, Screenable.SCREEN_FLAG_REMOVE_BACK));
		}
		return toScreen;
	}

	protected boolean showBonuses(UserMethodResult userMethodResult) {
		return userMethodResult.getUser().isHasBonus();
	}

	protected void doAfterLoginNavigation(Screenable toScreen, Bundle bundle) {
		if (toScreen != null) {
			application.postEvent(new NavigationEvent(toScreen, NavigationType.INIT, bundle));
		}
	}

	public Form getForm() {
		return form;
	}

	private void setUpUsernameField(FormEditText field) {
		field.addValidator(ValidatorType.EMPTY, ValidatorType.USERNAME_MINIMUM_CHARACTERS);
		// Set the email of the previously logged user:
		field.setValue(sharedPreferencesManager.getString(Constants.PREFERENCES_USER_NAME, null));
		String emailHint = fragment.getString(R.string.username) + " / " + fragment.getString(R.string.email);
		field.getHintTextView().setText(emailHint);
	}

	private void setUpPasswordFields(FormEditText passwordField, FormCheckBox rememberPasswordCheckBox) {
		passwordField.addValidator(ValidatorType.EMPTY, ValidatorType.ALNUM, ValidatorType.PASSWORD_MINIMUM_CHARACTERS);

		String savedPassword = sharedPreferencesManager.getString(Constants.PREFERENCES_PASSWORD_LOGIN, null);
		if (savedPassword != null) {
			passwordField.setValue(savedPassword);
		}

		rememberPasswordCheckBox.setChecked(savedPassword != null);
	}

}