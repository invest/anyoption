package com.anyoption.android.app.manager;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.InvestmentEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.util.constants.Constants;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import bolts.AppLinks;

/**
 * Manages the FACEBOOK connection, deep links and other logics. 
 * @author Anastas Arnaudov
 */
public class FacebookManager {
	
	private static final String TAG = FacebookManager.class.getSimpleName();
	
	public static final String EVENT_LOGIN 		= "Login";
	public static final String EVENT_DEPOSIT 	= "Deposit";
	public static final String EVENT_REGISTER 	= "Register";
	public static final String EVENT_TRADE	 	= "Trade";
	
	public static final String DEEP_LINK_DEPOSIT 			= "Deposit";
	public static final String DEEP_LINK_REGISTER 			= "Register";
	public static final String DEEP_LINK_TRADE 				= "Trade";
	public static final String DEEP_LINK_BONUS 				= "Bonus";
	public static final String DEEP_LINK_ASSET_BINARY 		= "Bin";
	public static final String DEEP_LINK_ASSET_OPTION_PLUS 	= "Optplus";
	
	protected Application application;
	protected boolean pendingPublishReauthorization;
	protected Bundle deepLinkBundle;
	protected boolean isDeepLink;
	protected AppEventsLogger logger;
	protected Screenable secondScreen;
	
	protected boolean isLoginNeeded;
	
	public interface SessionOpenedListener {
		public void onSessionOpened(Session session);
	}
	
	public interface UserListener {
		public void onUser(GraphUser user);
	}
	
	public interface FriendsListener {
		public void onFriends(List<GraphUser> friends);
	}
	
	public FacebookManager(Application application) {
		this.application = application;
		this.pendingPublishReauthorization = false;
		
		registerForEvents();
	}
	
	/**
	 * Parse the Intent to figure out whether it is a FB Deep Link.
	 * The data bundle is saved in the {@link FacebookManager}.
	 * @return True if it is a Deep Link.
	 */
	public boolean parseIntent(Context context, Intent intent) {
		Log.d(TAG, "Parsing Intent...");
	    Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(context, intent);
	    if (targetUrl != null) {
	    	Log.d(TAG, "Is from Deep Link = true");
	    	Log.d(TAG, "Extracting the target Screen...");
	    	deepLinkBundle = new Bundle();
	    	try {
	    		parseUri(intent.getData());
			} catch (Exception e) {
				Log.e(TAG, "Error parsing the Deep Link.", e);
			}
	    }
	    setupLoginNeeded();
	    isDeepLink = deepLinkBundle != null;
	    return isDeepLink;
	}
	
	protected void parseUri(Uri uri){
		String data = uri.toString();
		String scheme = uri.getScheme();
		//Extracting the screen from the data : scheme://screen?parameters
		String screen = data.substring(scheme.length() + 3, data.indexOf('?'));
		Log.d(TAG, "Target Screen = " + screen);
		if(screen.startsWith(DEEP_LINK_DEPOSIT)){
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.DEPOSIT_MENU);
		}else if(screen.startsWith(DEEP_LINK_REGISTER)){
			if(screen.contains("_")){
				//A second Screen is attached:
				String secondScreenString = screen.split("_")[1];
				if(secondScreenString.startsWith(DEEP_LINK_TRADE)){
					setSecondScreen(Screen.TRADE);
				}
			}
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.REGISTER);
		}else if(screen.startsWith(DEEP_LINK_TRADE)){
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.TRADE);
		}else if(screen.startsWith(DEEP_LINK_BONUS)){
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.BONUSES);
		}else if(screen.startsWith(DEEP_LINK_ASSET_BINARY)){
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.ASSET_VIEW_PAGER);
			deepLinkBundle.putBoolean(Constants.EXTRA_OPTION_PLUS_MARKET, false);
			String[] assetDetails = screen.split("_");
			String assetId = assetDetails[assetDetails.length-1];
			deepLinkBundle.putLong(Constants.EXTRA_MARKET_ID, Long.valueOf(assetId));
		}else if(screen.startsWith(DEEP_LINK_ASSET_OPTION_PLUS)){
			deepLinkBundle.putSerializable(Constants.EXTRA_SCREEN, Screen.ASSET_VIEW_PAGER);
			deepLinkBundle.putBoolean(Constants.EXTRA_OPTION_PLUS_MARKET, true);
			String[] assetDetails = screen.split("_");
			String assetId = assetDetails[assetDetails.length-1];
			deepLinkBundle.putLong(Constants.EXTRA_MARKET_ID, Long.valueOf(assetId));
		}
		
		
	}
	
	public boolean isDeepLink(){
		return isDeepLink;
	}
	
	public Bundle getDeepLinkBundle(){
		return deepLinkBundle;
	}
	
	public void clearBundle(){
		deepLinkBundle = null;
	}
	
	public void handledDeepLink(){
		isDeepLink = false;
	}
	
	private void setupLoginNeeded() {
		if (application.isLoggedIn()) {
			isLoginNeeded = false;
			return;
		}

		isLoginNeeded = true;
		if (deepLinkBundle != null){
			Screenable screen = (Screenable) deepLinkBundle.getSerializable(Constants.EXTRA_SCREEN);
			if(screen == null) return;
			if(screen == Screen.LOGIN || screen.equals(Screen.REGISTER)) {
				isLoginNeeded = false;
			}
		}
	}
	
	public boolean isLoginNeeded(){
		return isLoginNeeded;
	}
	
	public void changeActivity(Activity activity){
		Log.d(TAG, "Setting logger.");
		logger = AppEventsLogger.newLogger(application.getCurrentActivity());
	}
	
	private void registerForEvents() {
		application.registerForEvents(this, LoginLogoutEvent.class, DepositEvent.class, InvestmentEvent.class);
	}

	public void onEvent(LoginLogoutEvent event) {
		if(event.getType() == Type.LOGIN){
			Log.d(TAG, "Sending login event.");
			logger.logEvent(EVENT_LOGIN);
		}else if(event.getType() == Type.REGISTER){
			Log.d(TAG, "Sending register event.");
			logger.logEvent(EVENT_REGISTER);
		}
	}
	
	public void onEvent(DepositEvent event) {
		Log.d(TAG, "Sending deposit event.");
		logger.logEvent(EVENT_DEPOSIT);
	}
	
	public void onEvent(InvestmentEvent event) {
		if(event.getType() == com.anyoption.android.app.event.InvestmentEvent.Type.INVEST_SUCCESS){
			Log.d(TAG, "Sending trade event.");
			logger.logEvent(EVENT_TRADE);
		}
	}
	
	public static boolean hasActiveSession() {
		return null != Session.getActiveSession();
	}
	
	protected static void openSession(Context context, Fragment fragment, final SessionOpenedListener listener) {
		List<String> permissions = new ArrayList<String>();
		permissions.add("public_profile");
		permissions.add("user_friends");
		permissions.add("email");
		
		Session.openActiveSession(context, fragment, true, permissions, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {
					Log.d(TAG, "New Facebook session opened. User access tooken: " + session.getAccessToken());
					listener.onSessionOpened(session);
				}
			}
		});
	}
	
	public static void getUser(Context context, Fragment fragment, final UserListener listener) {
		Session session = Session.getActiveSession();
		if (null == session || !session.isOpened()) {
			openSession(context, fragment, new SessionOpenedListener() {
				@Override
				public void onSessionOpened(Session session) {
					getUser(session, listener);
				}
			});
		} else {
			getUser(session, listener);
		}
	}
	
	private static void getUser(Session session, final UserListener listener) {
		if (null != session && session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						Log.d(TAG, "Got Facebook user - fb id: " + user.getId() + " firstName: " + user.getFirstName() + " lastName: " + user.getLastName() + " email: " + user.asMap().get("email"));
					} else {
						Log.d(TAG, "Failed to get Facebook user - response: " + response);
					}
					listener.onUser(user);
				}
			}).executeAsync();
		} else {
			listener.onUser(null);
		}
	}
	
	public void getFriends(Context context, Fragment fragment, final FriendsListener listener) {
		Session session = Session.getActiveSession();
		if (null == session || !session.isOpened()) {
			Log.d(TAG, "openSession");
			openSession(context, fragment, new SessionOpenedListener() {
				@Override
				public void onSessionOpened(Session session) {
					getFriends(session, listener);
				}
			});
		} else {
			getFriends(session, listener);
		}
	}
	
	private void getFriends(Session session, final FriendsListener listener) {
		Log.d(TAG, "getFriends");
		if (null != session && session.isOpened()) {
			// make request to the /me/friends API
			Request.newMyFriendsRequest(session, new Request.GraphUserListCallback() {
				@Override
				public void onCompleted(List<GraphUser> users, Response response) {
					Log.d(TAG, "getFriends.onCompleted users: " + users + " response: " + response);
					if (null != users) {
						Log.d(TAG, "Got friends: " + users);
					} else {
						Log.d(TAG, "No friends received");
					}
					if (null != listener) {
						listener.onFriends(users);
					}
				}
			}).executeAsync();
		} else {
			listener.onFriends(null);
		}
	}

	public Screenable getSecondScreen() {
		return secondScreen;
	}

	public void setSecondScreen(Screenable secondScreen) {
		this.secondScreen = secondScreen;
	}
	
}