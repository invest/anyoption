package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.google.ads.conversiontracking.AdWordsConversionReporter;

import android.util.Log;

/**
 * Manager for the Google AdWords logic : integration, messaging etc.
 * <p>
 * the Google AdWords API is used for tracking several events that occur in the application:
 * <li>Application Installation
 * <li>User Registration
 * <li>User Deposit
 */
public abstract class GoogleAdWordsManager {

	public static final String TAG = GoogleAdWordsManager.class.getSimpleName();

	private static final boolean IS_EVENT_REPEATABLE = true;

	private Application application;

	/**
	 * {@link GoogleAdWordsManager}
	 * 
	 * @param application
	 */
	public GoogleAdWordsManager(Application application) {
		Log.d(TAG, "On Create");
		this.application = application;
		application.registerForEvents(this, LoginLogoutEvent.class, DepositEvent.class);
	}

	/**
	 * Returns conversion id.
	 * 
	 * @return id as string
	 */
	protected abstract String getConversionId();

	/**
	 * Returns the label for registration event.
	 * 
	 * @return label as string
	 */
	protected abstract String getRegistrationEventLabel();

	/**
	 * Returns the label for deposit event.
	 * 
	 * @return label as string
	 */
	protected abstract String getDepositEventLabel();

	/**
	 * Sends event message to the AppsFlyer tracker.
	 * 
	 * @param key
	 * @param value
	 */
	public void sendEvent(String conversionId, String label, String value) {
		Log.d(TAG, "Sending Event " + label);
		AdWordsConversionReporter.reportWithConversionId(application.getApplicationContext(),
				conversionId, label, value, IS_EVENT_REPEATABLE);
	}

	/**
	 * Sends a Register Event to the AppsFlyer tracker.
	 */
	public void sendDepositEvent() {
		long userId = application.getUser().getId();
		sendEvent(getConversionId(), getDepositEventLabel(), String.valueOf(userId));
	}

	/**
	 * Sends a Register Event to the AppsFlyer tracker.
	 */
	public void sendRegisterEvent() {
		long userId = application.getUser().getId();
		sendEvent(getConversionId(), getRegistrationEventLabel(), String.valueOf(userId));
	}

	/**
	 * Called when a {@link LoginLogoutEvent} occurs.
	 */
	public void onEvent(LoginLogoutEvent event) {
		if (event.getType() == Type.REGISTER) {
			sendRegisterEvent();
		}
	}

	/**
	 * Called when a {@link DepositEvent} occurs.
	 */
	@SuppressWarnings("unused")
	public void onEvent(DepositEvent event) {
		// Checks if the user is a TESTER:
		// if (!application.isUserTester()) {
		// Send the dollar amount:
		sendDepositEvent();
		// }
	}

}
