package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.util.AmountUtil;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.util.Log;

/**
 * Manages calls to the Google Analytics SDK.
 * 
 * @author anastasa
 *
 */
public class GoogleAnalyticsManager {

	public static final String TAG = GoogleAnalyticsManager.class.getSimpleName();
	
	public static final String EVENT_CATEGORY_NAVIGATION	= "Navigation";
	public static final String EVENT_CATEGORY_FIRST_DEPOSIT = "First Deposit";
	public static final String EVENT_CATEGORY_DEPOSIT  		= "Deposit";
	
	public static final String EVENT_REGISTER_SCREEN 	= "Register Screen";
	public static final String EVENT_FIRST_DEPOSIT 		= "First Deposit";
	public static final String EVENT_DEPOSIT 			= "Deposit";
	
	
	protected Tracker tracker;

	public GoogleAnalyticsManager(int configResId) {
		Log.d(TAG, "Init.");
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(Application.get());
		tracker = analytics.newTracker(configResId);
		
		registerForEvents();
	}

	private void registerForEvents() {
		Application.get().registerForEvents(this, NavigationEvent.class, DepositEvent.class);
	}

	
	///////////					EVENTS				////////////////////
	
	public void onEvent(NavigationEvent event) {
		Screenable screenable = event.getToScreen();
		
		if(screenable == getRegisterScreen()){
			Log.d(TAG, "Sending Register Screen Event.");
			tracker.send(new HitBuilders.EventBuilder()
					.setCategory(EVENT_CATEGORY_NAVIGATION)
					.setAction(EVENT_REGISTER_SCREEN)
				    .build());
		}
	}
	
	protected Screenable getRegisterScreen(){
		return Screen.REGISTER;
	}
	
	public void onEvent(DepositEvent event) {
		if(event.isFirstDeposit()){
			Log.d(TAG, "Sending First Deposit Event.");
			tracker.send(new HitBuilders.EventBuilder()
					.setCategory(EVENT_CATEGORY_FIRST_DEPOSIT)
					.setAction(EVENT_FIRST_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
				    .build());
		}else{
			Log.d(TAG, "Sending Deposit Event.");
			tracker.send(new HitBuilders.EventBuilder()
					.setCategory(EVENT_CATEGORY_DEPOSIT)
					.setAction(EVENT_DEPOSIT + " : " + AmountUtil.getUserFormattedAmount(event.getAmount()))
				    .build());
		}
	}
	
	///////////////////////////////////////////////////////////////////
}
