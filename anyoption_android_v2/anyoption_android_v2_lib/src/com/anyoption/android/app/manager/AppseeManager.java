package com.anyoption.android.app.manager;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.event.DepositFailEvent;
import com.anyoption.android.app.event.LoginFailedEvent;
import com.anyoption.android.app.event.LoginLogoutEvent;
import com.anyoption.android.app.event.LoginLogoutEvent.Type;
import com.anyoption.android.app.event.RegisterFailEvent;
import com.appsee.Appsee;

import android.util.Log;

/**
 * Manager for the APPSEE tracker.
 * @author Anastas Arnaudov
 */
public class AppseeManager {
	
	private static final String TAG = AppseeManager.class.getSimpleName();
	
	private static final String EVENT_LOGIN_OK 			= "Login";
	private static final String EVENT_LOGIN_FAILED 		= "Login failed";
	private static final String EVENT_DEPOSIT_OK 		= "Deposit";
	private static final String EVENT_DEPOSIT_FAILED 	= "Deposit failed";
	private static final String EVENT_REISTER_OK 		= "Register";
	private static final String EVENT_REGISTER_FAILED 	= "Register failed";
	
	public AppseeManager(){
		Log.d(TAG, "On Create");
		Application.get().registerForEvents(this, LoginLogoutEvent.class, DepositEvent.class, LoginFailedEvent.class, DepositFailEvent.class, RegisterFailEvent.class);
	}
	
	public static void onStart(){
		Log.d(TAG, "onStart");
		Appsee.start(Application.get().getString(R.string.appseeApiKey));
	}
	
	public void onEvent(LoginLogoutEvent event) {
		if(event.getType() == Type.REGISTER){
			Log.d(TAG, "Sending Register event.");
			Appsee.addEvent(EVENT_REISTER_OK);
		}else if (event.getType() == Type.LOGIN){
			Log.d(TAG, "Sending Login event.");
			Appsee.addEvent(EVENT_LOGIN_OK);
		}
	}
	
	public void onEvent(LoginFailedEvent event) {
		Log.d(TAG, "Sending Login Failed event.");
		Appsee.addEvent(EVENT_LOGIN_FAILED);
	}

	public void onEvent(DepositEvent event) {
		Log.d(TAG, "Sending Deposit event.");
		Appsee.addEvent(EVENT_DEPOSIT_OK);
	}
	
	public void onEvent(DepositFailEvent event) {
		Log.d(TAG, "Sending Deposit Failed event.");
		Appsee.addEvent(EVENT_DEPOSIT_FAILED);
	}
	
	public void onEvent(RegisterFailEvent event) {
		Log.d(TAG, "Sending Register Failed event.");
		Appsee.addEvent(EVENT_REGISTER_FAILED);
	}
}
