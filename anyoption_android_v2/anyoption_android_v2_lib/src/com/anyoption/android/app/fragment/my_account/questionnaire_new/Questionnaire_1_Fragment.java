package com.anyoption.android.app.fragment.my_account.questionnaire_new;

import java.util.Map;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.form.FormQuestionnaireItem;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.results.UserMethodResult;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

@SuppressLint("UseSparseArrays")
public class Questionnaire_1_Fragment extends QuestionnaireBaseFragment{

	public final String TAG = Questionnaire_1_Fragment.class.getSimpleName();
	
	public static Questionnaire_1_Fragment newInstance(Bundle bundle){
		Questionnaire_1_Fragment fragment = new Questionnaire_1_Fragment();
		fragment.setArguments(bundle);
		return fragment;
	}
	
	private boolean isNext;
	private FormQuestionnaireItem item1;
	private FormQuestionnaireItem item2;
	private FormQuestionnaireItem item3;
	private FormQuestionnaireItem item4;
	private FormQuestionnaireItem item5;
	private EditText item5_EditText;
	private Question question1;
	private Question question2;
	private Question question3;
	private Question question4;
	private Question question5;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.QUESTIONNAIRE_1;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.questionnaire_1_fragment, container, false);
		
		loadingView = rootView.findViewById(R.id.loadingView);
		
		item1 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_1_item_1);
		item2 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_1_item_2);
		item3 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_1_item_3);
		item4 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_1_item_4);
		item5 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_1_item_5);
		item5_EditText = (EditText) rootView.findViewById(R.id.questionnaire_1_item_5_edit_text);
		
		item1.setAnswerSelectedListener(this);
		item2.setAnswerSelectedListener(this);
		item3.setAnswerSelectedListener(this);
		item4.setAnswerSelectedListener(this);
		item5.setAnswerSelectedListener(this);
		
		TextView headerTextView = (TextView) rootView.findViewById(R.id.questionnaire_1_header_text_view);
		TextUtils.decorateInnerText(headerTextView, null, headerTextView.getText().toString(), getString(R.string.questionnaireScreen1HeaderNote), Font.ROBOTO_BOLD	, 0, 0);
		
		loadQuestions();
		
		submitButton = (Button) rootView.findViewById(R.id.questionnaire_1_next_buttton);
		submitButton.setTag("Screen1");
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});
		
		return rootView;
	}

	@Override
	protected void loadQuestions(){
		
		question1 = new Question(81l, getString(R.string.questionnaireScreen1Question_1), new Answer[]{
			new Answer(321l, getString(R.string.questionnaireScreen1Question_1_Answer_1)),
			new Answer(322l, getString(R.string.questionnaireScreen1Question_1_Answer_2)),
			new Answer(323l, getString(R.string.questionnaireScreen1Question_1_Answer_3)),
			new Answer(324l, getString(R.string.questionnaireScreen1Question_1_Answer_4))
		});
		question2 = new Question(82l, getString(R.string.questionnaireScreen1Question_2), new Answer[]{
			new Answer(325l, getString(R.string.questionnaireScreen1Question_2_Answer_1)),
			new Answer(326l, getString(R.string.questionnaireScreen1Question_2_Answer_2)),
			new Answer(327l, getString(R.string.questionnaireScreen1Question_2_Answer_3)),
			new Answer(328l, getString(R.string.questionnaireScreen1Question_2_Answer_4))
		});
		question3 = new Question(83l, getString(R.string.questionnaireScreen1Question_3), new Answer[]{
			new Answer(329l, getString(R.string.questionnaireScreen1Question_3_Answer_1)),
			new Answer(330l, getString(R.string.questionnaireScreen1Question_3_Answer_2)),
			new Answer(331l, getString(R.string.questionnaireScreen1Question_3_Answer_3)),
			new Answer(332l, getString(R.string.questionnaireScreen1Question_3_Answer_4))
		});
		question4 = new Question(84l, getString(R.string.questionnaireScreen1Question_4), new Answer[]{
			new Answer(333l, getString(R.string.questionnaireScreen1Question_4_Answer_1)),
			new Answer(334l, getString(R.string.questionnaireScreen1Question_4_Answer_2)),
			new Answer(335l, getString(R.string.questionnaireScreen1Question_4_Answer_3)),
			new Answer(336l, getString(R.string.questionnaireScreen1Question_4_Answer_4)),
			new Answer(401l, getString(R.string.questionnaireScreen1Question_4_Answer_5))
		});
		question5 = new Question(85l, getString(R.string.questionnaireScreen1Question_5), new Answer[]{
			new Answer(337l, getString(R.string.questionnaireScreen1Question_5_Answer_1)),
			new Answer(338l, getString(R.string.questionnaireScreen1Question_5_Answer_2)),
			new Answer(339l, getString(R.string.questionnaireScreen1Question_5_Answer_3)),
			new Answer(340l, getString(R.string.questionnaireScreen1Question_5_Answer_4)),
			new Answer(341l, getString(R.string.questionnaireScreen1Question_5_Answer_5)),
			new Answer(342l, getString(R.string.questionnaireScreen1Question_5_Answer_6)),
			new Answer(343l, getString(R.string.questionnaireScreen1Question_5_Answer_7)),
			new Answer(344l, getString(R.string.questionnaireScreen1Question_5_Answer_8)),
			new Answer(345l, getString(R.string.questionnaireScreen1Question_5_Answer_9)),
			new Answer(346l, getString(R.string.questionnaireScreen1Question_5_Answer_10)),
			new Answer(347l, getString(R.string.questionnaireScreen1Question_5_Answer_11)),
			new Answer(348l, getString(R.string.questionnaireScreen1Question_5_Answer_12)),
			new Answer(349l, getString(R.string.questionnaireScreen1Question_5_Answer_13)),
			new Answer(350l, getString(R.string.questionnaireScreen1Question_5_Answer_14)),
			new Answer(351l, getString(R.string.questionnaireScreen1Question_5_Answer_15)),
			new Answer(352l, getString(R.string.questionnaireScreen1Question_5_Answer_16)),
			new Answer(353l, getString(R.string.questionnaireScreen1Question_5_Answer_17)),
			new Answer(354l, getString(R.string.questionnaireScreen1Question_5_Answer_18)),
			new Answer(355l, getString(R.string.questionnaireScreen1Question_5_Answer_19)),
			new Answer(356l, getString(R.string.questionnaireScreen1Question_5_Answer_20)),
			new Answer(357l, getString(R.string.questionnaireScreen1Question_5_Answer_21)),
			new Answer(358l, getString(R.string.questionnaireScreen1Question_5_Answer_22)),
			new Answer(359l, getString(R.string.questionnaireScreen1Question_5_Answer_23)),
			new Answer(360l, getString(R.string.questionnaireScreen1Question_5_Answer_24)),
			new Answer(361l, getString(R.string.questionnaireScreen1Question_5_Answer_25)),
			new Answer(362l, getString(R.string.questionnaireScreen1Question_5_Answer_26)),
			new Answer(363l, getString(R.string.questionnaireScreen1Question_5_Answer_27)),
			new Answer(364l, getString(R.string.questionnaireScreen1Question_5_Answer_28)),
			new Answer(365l, getString(R.string.questionnaireScreen1Question_5_Answer_29))
		});
		
		item1.setQuestion(question1);
		item2.setQuestion(question2);
		item3.setQuestion(question3);
		item4.setQuestion(question4);
		item5.setQuestion(question5);
		
		questionItemsMap.put(question1.getId(), item1);
		questionItemsMap.put(question2.getId(), item2);
		questionItemsMap.put(question3.getId(), item3);
		questionItemsMap.put(question4.getId(), item4);
		questionItemsMap.put(question5.getId(), item5);
		
		if(answers == null){
			enableQuestions(false);
		}
	}
	
	@Override
	public void onAnswerSelected(Question question, Answer answer) {
		isNext = false;
		if(question == question5){
			if(answer.getDescription().equals(getString(R.string.questionnaireScreen1Question_5_Answer_29))){
				if(item5_EditText.getVisibility() != View.VISIBLE){
					item5_EditText.setVisibility(View.VISIBLE);
				}
			}else{
				item5_EditText.setVisibility(View.GONE);
			}
		}
		super.onAnswerSelected(question, answer);
	}
	
	@Override
	protected void updateUserAnswers(){
		super.updateUserAnswers();
		if(answers != null){
			for(QuestionnaireUserAnswer answer : answers){
				FormQuestionnaireItem formQuestionSelector = item5;
				if(formQuestionSelector != null && formQuestionSelector.getQuestion().getId() == answer.getQuestionId()){
					Answer value = formQuestionSelector.getAnswer();
					if(value != null){
						answer.setAnswerId(value.getId());
						//Check if the user has put in a custom answer text:
						if(answer.getAnswerId()!=null){
							if(answer.getAnswerId() == 365l) {
							answer.setTextAnswer(item5_EditText.getText().toString());
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	protected void updateQuestionsSelectors() {
		super.updateQuestionsSelectors();
		if(answers != null && !questionItemsMap.isEmpty()){
			for(QuestionnaireUserAnswer answer : answers){
				if(answer.getQuestionId() == question5.getId() && answer.getAnswerId()!=null){
					if(answer.getAnswerId() == 365l){
						item5_EditText.setVisibility(View.VISIBLE);
						item5_EditText.setText(answer.getTextAnswer());
					}
				}
			}
		}
	}
	
	///////////				REQUESTs and RESPONSEs			////////////////////
	
	@Override
	public void updateQuestionnaireCallback(Object resultObj){
		super.updateQuestionnaireCallback(resultObj);
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			if(isNext){
				application.postEvent(new NavigationEvent(Screen.QUESTIONNAIRE_2, NavigationType.DEEP));
			}
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
			application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
		}
	}
	
	@Override
	protected void submit(){
		boolean isFormValid = true;
		//Check if all Questions are answered:
		for (Map.Entry<Long, FormQuestionnaireItem> entry : questionItemsMap.entrySet()){
			FormQuestionnaireItem item = entry.getValue();
			if(item.getAnswer() == null || (item.getAnswer() != null && item.getId() == R.id.questionnaire_1_item_5 && item5_EditText.getVisibility() == View.VISIBLE && item5_EditText.getText().toString().trim().isEmpty())){
				isFormValid = false;
				item.showError();
			}

		}
		if(isFormValid){
			isNext = true;
			updateQuestionnaire();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////
}
