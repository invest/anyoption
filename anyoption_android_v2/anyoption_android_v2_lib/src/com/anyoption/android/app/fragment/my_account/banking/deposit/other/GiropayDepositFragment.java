package com.anyoption.android.app.fragment.my_account.banking.deposit.other;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;

import android.os.Bundle;

public class GiropayDepositFragment extends BaseOtherDepositFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static GiropayDepositFragment newInstance(Bundle bundle){
		GiropayDepositFragment fragment = new GiropayDepositFragment();	
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.DIRECT24_DEPOSIT;
	}

	@Override
	protected long getType() {
		return TYPE_GIROPAY;
	}

}
