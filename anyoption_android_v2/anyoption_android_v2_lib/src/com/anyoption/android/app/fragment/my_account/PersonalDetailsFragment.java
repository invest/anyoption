package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.manager.PopUpManager.OnPasswordPopUpCommitPressedListener;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class PersonalDetailsFragment extends NavigationalFragment implements OnClickListener {

	protected View rootView;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.PERSONAL_DETAILS_MENU;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static PersonalDetailsFragment newInstance(Bundle bundle){
		PersonalDetailsFragment personalDetailsMenuFragment = new PersonalDetailsFragment();
		personalDetailsMenuFragment.setArguments(bundle);
		return personalDetailsMenuFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.personal_details_layout, container, false);
		
		setupUI();
		
		setupQuestionnaire();
		
		setUpGreeting();
		return rootView;
	}
	
	protected void setupUI() {
		rootView.findViewById(R.id.personal_details_update_info_layout).setOnClickListener(this);
		rootView.findViewById(R.id.personal_details_update_password_layout).setOnClickListener(this);
	}

	private void setupQuestionnaire(){
		View questionnaireView = rootView.findViewById(R.id.personal_details_questionnaire_layout);

		/*if (application.isRegulated()
				&& RegulationUtils.isAfterFirstDeposit()
				&& !RegulationUtils.isSuspended()
				&& (!RegulationUtils.isMandQuestionnaireAnswered()
				|| !RegulationUtils.isOptionalQuestionnaireAnswered())) {*/
		if (!RegulationUtils.isQuestionnaireFilled() && RegulationUtils.isAfterFirstDeposit()) {
			questionnaireView.setVisibility(View.VISIBLE);
			questionnaireView.setOnClickListener(this);
		} else {
			questionnaireView.setVisibility(View.GONE);
		}
	}
	
	protected void setUpGreeting() {
		TextView textviewHelloUser = (TextView) rootView.findViewById(R.id.textViewHelloUserName);
		User user = application.getUser();
		if (user != null) {
			textviewHelloUser.setText(getString(R.string.personalDetailsScreenHeader) + " " + user.getFirstName() + " " + user.getLastName());
		} else {
			textviewHelloUser.setText(R.string.notloggedin);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.personal_details_update_info_layout) {
			//Show password confirmation dialog:
			application.getPopUpManager().showPasswordPopUp(new OnPasswordPopUpCommitPressedListener() {	
				@Override
				public void onCommit(String password) {
					UserMethodRequest request = new UserMethodRequest();
				    request.setUserName(application.getUser().getUserName());
				    request.setPassword(password);
				    application.getCommunicationManager().requestService(PersonalDetailsFragment.this, "passConfirmationCallback", application.getServiceMethodGetUser(), request, UserMethodResult.class);
				}
			});
		} else if (v.getId() == R.id.personal_details_update_password_layout) {
			application.postEvent(new NavigationEvent(Screen.UPDATE_PASSWORD, NavigationType.DEEP));
		} else if (v.getId() == R.id.personal_details_questionnaire_layout) {
			if(RegulationUtils.showQuestionnaire()) {}
		}
	}

	/**
	 * Callback for the password confirmation request.
	 * @param result
	 */
	public void passConfirmationCallback(Object result){
		if (result == null || result instanceof UserMethodResult) {
			UserMethodResult userMethodResult = (UserMethodResult) result;
			if(userMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS){
				application.postEvent(new NavigationEvent(Screen.UPDATE_INFO, NavigationType.DEEP));
			}
		}
	}
	
}
