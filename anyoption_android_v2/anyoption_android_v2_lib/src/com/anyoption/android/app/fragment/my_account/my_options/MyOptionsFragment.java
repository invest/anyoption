package com.anyoption.android.app.fragment.my_account.my_options;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.my_account.SettledOptionsFragment;
import com.anyoption.android.app.fragment.trade.OpenOptionsFragment;
import com.anyoption.android.app.util.constants.Constants;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class MyOptionsFragment extends NavigationalFragment {

	public static MyOptionsFragment newInstance(Bundle bundle) {
		Log.d(TAG, "Created fragment MyOptionsFragment");
		MyOptionsFragment fragment = new MyOptionsFragment();
		fragment.setArguments(bundle);
		return fragment;
	}


	public static final byte TAB_OPEN_OPTIONS 		= 1;
	public static final byte TAB_SETTLED_OPTIONS 	= 2;

	protected byte currentTab = TAB_OPEN_OPTIONS;
	
	protected View rootView;
	protected View openOptionsTab;
	protected View settledOptionsTab;
	protected View currentTabLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "On Create");
		Bundle bundle = getArguments();
		if(bundle != null){
			//Get arguments:
			currentTab = bundle.getByte(Constants.EXTRA_MY_OPTIONS_TAB, currentTab);
		}
		
		application.registerForEvents(this, NavigationEvent.class);
	}

	@Override
	public void onDestroy() {
		application.unregisterForAllEvents(this);
		super.onDestroy();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.my_options_fragment, container, false);

		setupTabs();
		
		//Initial Tab:
		selectTab();
		openTab();
		
		return rootView;
	}

	public void onEventMainThread(NavigationEvent event) {
		if (event.getToScreen() != Screen.MY_OPTIONS) {
			return;
		}

		Bundle args = event.getBundle();
		if (args != null && args.containsKey(Constants.EXTRA_MY_OPTIONS_TAB)) {
			byte newTab = args.getByte(Constants.EXTRA_MY_OPTIONS_TAB, currentTab);
			if (currentTab != newTab) {
				currentTab = newTab;
				selectTab();
				openTab();
			}
		}
	}

	protected void setupTabs(){
		openOptionsTab = rootView.findViewById(R.id.my_options_open_options_tab);
		settledOptionsTab = rootView.findViewById(R.id.my_options_settled_options_tab);

		currentTabLayout = rootView.findViewById(R.id.my_options_current_tab_layout);
		
		OnClickListener tabClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(v == openOptionsTab){
					if(currentTab == TAB_OPEN_OPTIONS){ 
						return;
					}
					currentTab = TAB_OPEN_OPTIONS;
				}else if(v == settledOptionsTab){
					if(currentTab == TAB_SETTLED_OPTIONS){ 
						return;
					}
					currentTab = TAB_SETTLED_OPTIONS;
				}
				
				selectTab();
				openTab();
			}
		};
		
		openOptionsTab.setOnClickListener(tabClickListener);
		settledOptionsTab.setOnClickListener(tabClickListener);
	}
	
	protected void selectTab(){
		if(currentTab == TAB_OPEN_OPTIONS){
			openOptionsTab.setBackgroundColor(getResources().getColor(R.color.my_options_tab_pressed_bg));
			settledOptionsTab.setBackgroundColor(getResources().getColor(R.color.my_options_tab_bg));
		}else if(currentTab == TAB_SETTLED_OPTIONS){
			settledOptionsTab.setBackgroundColor(getResources().getColor(R.color.my_options_tab_pressed_bg));
			openOptionsTab.setBackgroundColor(getResources().getColor(R.color.my_options_tab_bg));
		}
	}
	
	protected void openTab(){
		if(currentTab == TAB_OPEN_OPTIONS){
			application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), OpenOptionsFragment.class, this);			
		}else if(currentTab == TAB_SETTLED_OPTIONS){
			application.getFragmentManager().replaceInnerFragment(currentTabLayout.getId(), SettledOptionsFragment.class, this);		
		}
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.MY_OPTIONS;
	}

}
