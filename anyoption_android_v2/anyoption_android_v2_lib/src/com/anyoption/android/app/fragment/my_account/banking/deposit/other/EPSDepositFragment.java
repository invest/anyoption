package com.anyoption.android.app.fragment.my_account.banking.deposit.other;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;

import android.os.Bundle;
import android.view.View;

public class EPSDepositFragment extends BaseOtherDepositFragment {

	/**
	 * Use this method to get new instance of the fragment.
	 * @return
	 */
	public static EPSDepositFragment newInstance(Bundle bundle){
		EPSDepositFragment fragment = new EPSDepositFragment();	
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	protected Screenable setupScreen() {
		return Screen.DIRECT24_DEPOSIT;
	}
	
	@Override
	protected void setupFormFields(){
		rootView.findViewById(R.id.deposit_other_account_number_layout).setVisibility(View.GONE);
		rootView.findViewById(R.id.deposit_other_bank_code_layout).setVisibility(View.GONE);
		form.addItem(amountField, accountHolderField);
	}

	@Override
	protected long getType() {
		return TYPE_EPS;
	}
	
}
