package com.anyoption.android.app.fragment.my_account.banking.withdraw;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.common.beans.base.Skin;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass. 
 * <p>It manages the "Existing Card" screen and functionality.
 * @author Eyal O
 *
 */
public class WithdrawClosedFragment extends NavigationalFragment {
	
	private final static String TAG = WithdrawClosedFragment.class.getSimpleName();
	
	/**
	 * Use this method to obtain new instance of the fragment.
	 * @return
	 */
	public static WithdrawClosedFragment newInstance(Bundle bundle){
		Log.d(TAG, "Create new WithdrawClosedFragment");
		WithdrawClosedFragment withdrawFragment = new WithdrawClosedFragment();
		withdrawFragment.setArguments(bundle);
		return withdrawFragment;
	}

	protected ImageView imageView;
	protected TextView messageteTextView;
	
	@Override
	protected Screenable setupScreen() {
		return Screen.WITHDRAW_CLOSED;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.withdraw_closed_layout, container, false);
		
		imageView = (ImageView) rootView.findViewById(R.id.withdraw_closed_screen_image);
		imageView.setImageDrawable(getDrawableForSkin());
		
		messageteTextView = (TextView) rootView.findViewById(R.id.withdraw_closed_screen_text);
		TextUtils.decorateInnerText(messageteTextView, null, messageteTextView.getText().toString(), getString(R.string.withdrawClosedScreenMessage_2), Font.ROBOTO_MEDIUM, 0, 0);
		
		return rootView;
	}	
	
	protected Drawable getDrawableForSkin(){
		Skin skin = application.getSkins().get(application.getUser().getSkinId());
		String locale = skin.getLocale().split("_")[0];
		Drawable drawable = DrawableUtils.getImage("withdraw_closed_table_image_" + locale);
		return drawable;
	}
}

