package com.anyoption.android.app.fragment.my_account.questionnaire_new;

import java.util.Map;

import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.TextUtils.OnInnerTextClickListener;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.form.FormCheckBox;
import com.anyoption.android.app.widget.form.FormQuestionnaireItem;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.results.UserQuestionnaireResult;

import android.os.Bundle;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Questionnaire_2_Fragment extends QuestionnaireBaseFragment {

	public final String TAG = Questionnaire_2_Fragment.class.getSimpleName();

	public static Questionnaire_2_Fragment newInstance(Bundle bundle) {
		Questionnaire_2_Fragment fragment = new Questionnaire_2_Fragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	private FormQuestionnaireItem item1;
	private FormQuestionnaireItem item2;
	private FormQuestionnaireItem item3;
	private FormQuestionnaireItem item4;
	private FormCheckBox item6;
	private FormCheckBox item7;
	private FormCheckBox item5;
	private View item5_ErrorTextView;
	private View item6_ErrorTextView;
	private View item7_ErrorTextView;

	private Question question1;
	private Question question2;
	private Question question3;
	private Question question4;
	private Question question5;
	private Question question6;
	private Question question7;

	@Override
	protected Screenable setupScreen() {
		return Screen.QUESTIONNAIRE_2;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.questionnaire_2_fragment, container, false);

		loadingView = rootView.findViewById(R.id.loadingView);

		item1 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_2_item_1);
		item2 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_2_item_2);
		item3 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_2_item_3);
		item4 = (FormQuestionnaireItem) rootView.findViewById(R.id.questionnaire_2_item_4);

		item5 = (FormCheckBox) rootView.findViewById(R.id.questionnaire_2_item_5);
		item6 = (FormCheckBox) rootView.findViewById(R.id.questionnaire_2_item_6);
		item7 = (FormCheckBox) rootView.findViewById(R.id.questionnaire_2_item_7);

		// Create the Links to the Terms&Conditions Screen
		TextView item5TextView = (TextView) rootView.findViewById(R.id.questionnaire_2_item_5_text_view);
		OnInnerTextClickListener termsAndCondLinkListener = new OnInnerTextClickListener() {
			@Override
			public void onInnerTextClick(TextView textView, String text, String innerText) {
				application.postEvent(new NavigationEvent(Screen.TERMS_AND_CONDITIONS, NavigationType.DEEP));
			}
		};
		Spannable spannable = TextUtils.makeClickableInnerText(item5TextView, null, item5TextView.getText().toString(),
				getString(R.string.questionnaireScreen2Question_5_Link_1), null, 0, 0, true, termsAndCondLinkListener);
		TextUtils.makeClickableInnerText(item5TextView, spannable, item5TextView.getText().toString(),
				getString(R.string.questionnaireScreen2Question_5_Link_2), null, 0, 0, true, termsAndCondLinkListener);
		TextUtils.makeClickableInnerText(item5TextView, spannable, item5TextView.getText().toString(),
				getString(R.string.questionnaireScreen2Question_5_Link_3), null, 0, 0, true, termsAndCondLinkListener);

		item5_ErrorTextView = rootView.findViewById(R.id.questionnaire_item_5_error_text_view);
		item6_ErrorTextView = rootView.findViewById(R.id.questionnaire_item_6_error_text_view);
		item7_ErrorTextView = rootView.findViewById(R.id.questionnaire_item_7_error_text_view);

		item1.setAnswerSelectedListener(this);
		item2.setAnswerSelectedListener(this);
		item3.setAnswerSelectedListener(this);
		item4.setAnswerSelectedListener(this);

		loadQuestions();

		submitButton = (Button) rootView.findViewById(R.id.questionnaire_2_submit_buttton);
		submitButton.setTag("Screen2");
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});

		return rootView;
	}

	@Override
	protected void loadQuestions() {

		question1 = new Question(86l, getString(R.string.questionnaireScreen2Question_1),
				new Answer[] { new Answer(366l, getString(R.string.questionnaireScreen2Question_1_Answer_1)),
						new Answer(367l, getString(R.string.questionnaireScreen2Question_1_Answer_2)),
						new Answer(368l, getString(R.string.questionnaireScreen2Question_1_Answer_3)),
						new Answer(369l, getString(R.string.questionnaireScreen2Question_1_Answer_4)),
						new Answer(370l, getString(R.string.questionnaireScreen2Question_1_Answer_5)) });
		question2 = new Question(87l, getString(R.string.questionnaireScreen2Question_2),
				new Answer[] { new Answer(371l, getString(R.string.questionnaireScreen2Question_2_Answer_1)),
						new Answer(372l, getString(R.string.questionnaireScreen2Question_2_Answer_2)) });
		question3 = new Question(88l, getString(R.string.questionnaireScreen2Question_3),
				new Answer[] { new Answer(373l, getString(R.string.questionnaireScreen2Question_3_Answer_1)),
						new Answer(374l, getString(R.string.questionnaireScreen2Question_3_Answer_2)),
						new Answer(375l, getString(R.string.questionnaireScreen2Question_3_Answer_3)),
						new Answer(376l, getString(R.string.questionnaireScreen2Question_3_Answer_4)),
						new Answer(377l, getString(R.string.questionnaireScreen2Question_3_Answer_5)) });
		question4 = new Question(89l, getString(R.string.questionnaireScreen2Question_4),
				new Answer[] { new Answer(378l, getString(R.string.questionnaireScreen2Question_4_Answer_1)),
						new Answer(379l, getString(R.string.questionnaireScreen2Question_4_Answer_2)),
						new Answer(380l, getString(R.string.questionnaireScreen2Question_4_Answer_3)) });
		question5 = new Question(90l, getString(R.string.questionnaireScreen2Question_5),
				new Answer[] { new Answer(381l, getString(R.string.yes)), new Answer(382l, getString(R.string.no)) });
		question6 = new Question(91l, getString(R.string.questionnaireScreen2Question_6),
				new Answer[] { new Answer(383l, getString(R.string.yes)), new Answer(384l, getString(R.string.no)) });
		question7 = new Question(92l, getString(R.string.questionnaireScreen2Question_7),
				new Answer[] { new Answer(387l, getString(R.string.yes)), new Answer(388l, getString(R.string.no)) });

		item1.setQuestion(question1);
		item2.setQuestion(question2);
		item3.setQuestion(question3);
		item4.setQuestion(question4);

		questionItemsMap.put(question1.getId(), item1);
		questionItemsMap.put(question2.getId(), item2);
		questionItemsMap.put(question3.getId(), item3);
		questionItemsMap.put(question4.getId(), item4);

		if (answers == null) {
			enableQuestions(false);
		}
	}

	@Override
	protected void updateQuestionsSelectors() {
		super.updateQuestionsSelectors();
		if (answers != null && !questionItemsMap.isEmpty()) {
			for (QuestionnaireUserAnswer answer : answers) {
				long questionId = answer.getQuestionId();
				if (questionId == question5.getId() && answer.getAnswerId() != null) {
					item5.setChecked(answer.getAnswerId() == 381l);
				} else if (questionId == question6.getId() && answer.getAnswerId() != null) {
					item6.setChecked(answer.getAnswerId() == 383l);
				} else if (questionId == question7.getId() && answer.getAnswerId() != null) {
					item7.setChecked(answer.getAnswerId() == 387l);
				}
			}
		}
	}

	@Override
	protected void updateUserAnswers() {
		super.updateUserAnswers();
		if (answers != null) {
			for (QuestionnaireUserAnswer answer : answers) {
				long questionId = answer.getQuestionId();
				if (questionId == question5.getId()) {
					answer.setAnswerId((item5.isChecked()) ? 381l : 382l);
				} else if (questionId == question6.getId()) {
					answer.setAnswerId((item6.isChecked()) ? 383l : 384l);
				} else if (questionId == question7.getId()) {
					answer.setAnswerId((item7.isChecked()) ? 387l : 388l);
				}
			}
		}
	}

	protected boolean checkFormValidity() {
		boolean isFormValid = true;
		// Check if all Questions are answered:
		for (Map.Entry<Long, FormQuestionnaireItem> entry : questionItemsMap.entrySet()) {
			FormQuestionnaireItem item = entry.getValue();
			if (item.getAnswer() == null) {
				isFormValid = false;
				item.showError();
			}
		}

		// Check Check-Boxes:
		if (!item5.isChecked()) {
			isFormValid = false;
			item5_ErrorTextView.setVisibility(View.VISIBLE);
		} else {
			item5_ErrorTextView.setVisibility(View.GONE);
		}
		if (!item6.isChecked()) {
			isFormValid = false;
			item6_ErrorTextView.setVisibility(View.VISIBLE);
		} else {
			item6_ErrorTextView.setVisibility(View.GONE);
		}
		if (!item7.isChecked()) {
			isFormValid = false;
			item7_ErrorTextView.setVisibility(View.VISIBLE);
		} else {
			item7_ErrorTextView.setVisibility(View.GONE);
		}

		return isFormValid;
	}

	/////////// REQUESTs and RESPONSEs ////////////////////
	@Override
	protected void submit() {
		if (checkFormValidity()) {
			Log.d(TAG, "Submitting...");
			UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
			updateUserAnswers();
			request.setUserAnswers(answers);

			if (application.getCommunicationManager().requestService(this, "submitQuestionnaireCallback",
					Constants.SERVICE_UPDATE_SINGLE_QUESTIONNAIRIE, request, UserQuestionnaireResult.class, false,
					false)) {
			}
		}
	}

	public void submitQuestionnaireCallback(Object resultObj) {
		Log.d(TAG, "submitQuestionnaireCallback");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "submitQuestionnaireCallback OK.");
			application.setUserRegulation(result.getUserRegulation());
			boolean goToMain = false;
			if (RegulationUtils.showBlockedOrRestrictedPopups()) {
				if (RegulationUtils.showAccountBlocked()) {
					if (RegulationUtils.showUploadDocuments()) {
						goToMain = true;
					}
				}
			} else {
				goToMain = true;
			}

			if (goToMain) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			}
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE) {
			application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT) {
			application.setUserRegulation(result.getUserRegulation());
			RegulationUtils.showBlockedPopup();
			application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_USER_RESTRICTED) {
			application.setUserRegulation(result.getUserRegulation());
			RegulationUtils.showRestrictedPopup();
			application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS) {
			application.setUserRegulation(result.getUserRegulation());
			if (RegulationUtils.showAccountBlocked()) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			}
		} else if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS) {
			application.setUserRegulation(result.getUserRegulation());
			if (RegulationUtils.showUploadDocuments()) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			}
		} else {
			Log.e(TAG, "submitQuestionnaireCallback failed");
			ErrorUtils.handleResultError(result);
		}
	}
	/////////////////////////////////////////////////////////////////////////////////

}
