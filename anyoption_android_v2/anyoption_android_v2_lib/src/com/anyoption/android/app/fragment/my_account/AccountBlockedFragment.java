package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.R;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.widget.RequestButton;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccountBlockedFragment extends UploadDocumentsFragment {

	protected Screenable setupScreen() {
		return Screen.ACCOUNT_BLOCKED;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static AccountBlockedFragment newInstance(Bundle bundle) {
		AccountBlockedFragment accountBlockedFragment = new AccountBlockedFragment();
		accountBlockedFragment.setArguments(bundle);
		return accountBlockedFragment;
	}

	@Override
	protected void setupUI() {
		topTextViewForBolding = (TextView) rootView.findViewById(R.id.account_blocked_top_text1);
		bottomTextViewForBolding = (TextView) rootView.findViewById(R.id.account_blocked_top_text2);

		rootView.findViewById(R.id.upload_documents_top_text).setVisibility(View.GONE);
		rootView.findViewById(R.id.upload_documents_bottom_bold_text).setVisibility(View.GONE);
		rootView.findViewById(R.id.account_blocked_top_text1).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.account_blocked_top_text2).setVisibility(View.VISIBLE);
		TextUtils.decorateInnerText(topTextViewForBolding, null, topTextViewForBolding.getText().toString(),
				topTextViewForBolding.getText().toString(), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(bottomTextViewForBolding, null, bottomTextViewForBolding.getText().toString(),
				getString(R.string.account_blocked_top_text2_bold), Font.ROBOTO_BOLD, 0, 0);

		uploadedFilesMessage = (LinearLayout) rootView.findViewById(R.id.upload_documents_uploaded_document_message);
		uploadedFilesMessage.setVisibility(View.GONE);
	}
}
