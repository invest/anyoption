package com.anyoption.android.app.fragment.my_account.banking.deposit.other;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.event.DepositEvent;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.fragment.my_account.banking.deposit.ReceiptFragment;
import com.anyoption.android.app.util.AmountUtil;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.ScreenUtils;
import com.anyoption.android.app.util.TimeUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.util.validator.FormValidation.ValidatorType;
import com.anyoption.android.app.widget.RequestButton;
import com.anyoption.android.app.widget.RequestButton.InitAnimationListener;
import com.anyoption.android.app.widget.form.Form;
import com.anyoption.android.app.widget.form.Form.OnSubmitListener;
import com.anyoption.android.app.widget.form.FormCurrencySelector;
import com.anyoption.android.app.widget.form.FormDatePicker;
import com.anyoption.android.app.widget.form.FormEditText;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.FirstEverDepositCheckRequest;
import com.anyoption.json.requests.InatecDepositFinishMethodRequest;
import com.anyoption.json.requests.InatecDepositMethodRequest;
import com.anyoption.json.requests.UpdateUserMethodRequest;
import com.anyoption.json.results.FirstEverDepositCheckResult;
import com.anyoption.json.results.InatecDepositMethodResult;
import com.anyoption.json.results.TransactionMethodResult;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public abstract class BaseOtherDepositFragment extends NavigationalFragment {

	public static final String TAG = BaseOtherDepositFragment.class.getSimpleName();
	
	//Taken from the WEB. Better to use enum placed in the json jar
	public static final long TYPE_DIRECT24 	= 2;
	public static final long TYPE_GIROPAY 	= 3;
	public static final long TYPE_EPS		= 4;	
	
	public static final String PARAM_TRANSACTION_ID = "txid";
	public static final String PARAM_CS				= "cs";
	
	protected Form form;
	protected View rootView;
	protected FormCurrencySelector currencySelector;
	protected FormEditText amountField;
	protected FormEditText accountNumberField;
	protected FormEditText bankCodeField;
	protected FormEditText accountHolderField;
	
	//Common fields:
	private View commonFieldsLayout;
	protected FormEditText addressField;
	protected FormEditText cityField;
	protected FormEditText zipField;
	protected FormDatePicker birthDateField;
	////////////////
	
	protected RequestButton submitButton;
	protected WebView webView;
	protected View loadingView;
	protected boolean isFirstDeposit = false;
	protected boolean isFirstDepositCheckDone = false;
	protected boolean isLoading = false;
	protected String url;
	protected long transactionId;
	protected long transactionIdOther;
	protected String cs;
	protected double amount;
	
	/**
	 * Setup the type of the payment method.
	 * @return
	 */
	protected abstract long getType();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		checkFirstDeposit();
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.base_other_deposit, container, false);	
		loadingView = rootView.findViewById(R.id.deposit_other_loading_view);
		webView = (WebView) rootView.findViewById(R.id.deposit_other_web_view);
		webView.getSettings().setJavaScriptEnabled(true);
	    webView.getSettings().setPluginState(PluginState.ON);
	    webView.setWebViewClient(new DepositPaymentMethodClient());
		
		currencySelector = (FormCurrencySelector) rootView.findViewById(R.id.deposit_other_currency_selector);
		amountField = (FormEditText) rootView.findViewById(R.id.deposit_other_amount_edittext);
		accountNumberField = (FormEditText) rootView.findViewById(R.id.deposit_other_account_number_edittext);
		bankCodeField = (FormEditText) rootView.findViewById(R.id.deposit_other_bank_code_edittext);
		accountHolderField = (FormEditText) rootView.findViewById(R.id.deposit_other_account_holder_edittext);
		commonFieldsLayout = rootView.findViewById(R.id.deposit_other_common_fields_layout);
		addressField = (FormEditText) rootView.findViewById(R.id.deposit_other_address_edittext);
		cityField = (FormEditText) rootView.findViewById(R.id.deposit_other_city_edittext);
		zipField = (FormEditText) rootView.findViewById(R.id.deposit_other_zip_edittext);
		birthDateField =  (FormDatePicker) rootView.findViewById(R.id.deposit_other_date_of_birth_date_picker);
		submitButton = (RequestButton) rootView.findViewById(R.id.deposit_other_submit_button);
		
		currencySelector.setEditable(isFirstDeposit);
		amountField.addValidator(ValidatorType.EMPTY, ValidatorType.CURRENCY_AMOUNT);
		accountNumberField.addValidator(ValidatorType.EMPTY, ValidatorType.DIGITS);
		bankCodeField.addValidator(ValidatorType.EMPTY);
		accountHolderField.addValidator(ValidatorType.EMPTY);
		
		addressField.addValidator(ValidatorType.EMPTY);
		addressField.setFieldName(Constants.FIELD_STREET);
		cityField.addValidator(ValidatorType.EMPTY, ValidatorType.LETTERS_ENGLISH_AND_SPACE);
		cityField.setFieldName(Constants.FIELD_CITY_NAME);
		zipField.addValidator(ValidatorType.EMPTY);
		zipField.setFieldName(Constants.FIELD_ZIP_CODE);
		if (application.getUser().getTimeBirthDate() != null) {
			birthDateField.setEditable(false);
			birthDateField.setValue(new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()).format(application.getUser().getTimeBirthDate()));
			birthDateField.addValidator(ValidatorType.EMPTY, ValidatorType.VACUOUS);
		} else {
			birthDateField.addValidator(ValidatorType.EMPTY, ValidatorType.VACUOUS, ValidatorType.BIRTH_DAY);
			birthDateField.setMin18(true);
		}
		
		form = new Form();
		setupFormFields();
		form.setOnSubmitListener(new OnSubmitListener() {
			@Override
			public void onSubmit() {
				if(isFirstDeposit){
					submitCommonFields();
				}else{
					insertDeposit();
				}
			}
		});
		
		submitButton.setEnabled(isFirstDepositCheckDone);
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ScreenUtils.hideKeyboard();
				if(form.checkValidity()){
					submitButton.startLoading(new InitAnimationListener() {
						@Override
						public void onAnimationFinished() {
							form.submit();
						}
					});									
				}
			}
		});
		
		setupCommonFields();
		setupLoading();
		
		return rootView;
	}
	
	//#####################################################################
	//#########					REQUEST - RESPONSE				###########
	//#####################################################################	
	
	protected void checkFirstDeposit(){
		Log.d(TAG, "checkFirstDeposit");
		isLoading = true;
		setupLoading();
		setSubmitButtonEnabled(false);
		FirstEverDepositCheckRequest request = new FirstEverDepositCheckRequest();
		request.setUserId(Application.get().getUser().getId());
		Application.get().getCommunicationManager().requestService(BaseOtherDepositFragment.this, "checkFirstDepositCallBack", Constants.SERVICE_GET_FIRST_EVER_DEPOSIT_CHECK, request , FirstEverDepositCheckResult.class);
	}
	
	public void checkFirstDepositCallBack(Object resultObj) {
		Log.d(TAG, "firstDepositCallBack");
		isLoading = false;
		setupLoading();
		FirstEverDepositCheckResult result = (FirstEverDepositCheckResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			setSubmitButtonEnabled(true);
			isFirstDeposit = result.isFirstEverDeposit();
			if(currencySelector != null){
				currencySelector.setEditable(isFirstDeposit);
			}
			setupCommonFields();
		}
	}
	
	public void submitCommonFields() {
		Log.d(TAG, "submitCommonFields");
    	User user = new User();
    	
    	setUserCommonFields(user);
		user.setId(application.getUser().getId());
		
		UpdateUserMethodRequest request = new UpdateUserMethodRequest();
		request.setUser(user);
		
		Application.get().getCommunicationManager().requestService(this, "submitCommonFieldsCallback", Constants.SERVICE_UPDATE_USER_ADDITIONAL_FIELDS, request, UserMethodResult.class, false, false);
	}
	
	public void submitCommonFieldsCallback(Object resultObj){
		Log.d(TAG, "submitCommonFieldsCallback");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			insertDeposit();
		}
	}
	
	public void insertDeposit(){
		Log.d(TAG, "insertDeposit");
		InatecDepositMethodRequest request = new InatecDepositMethodRequest();
		setupInsertDepositParams(request);
		amount = Double.valueOf(amountField.getValue().toString());
		Application.get().getCommunicationManager().requestService(BaseOtherDepositFragment.this, "insertDepositCallBack", Constants.SERVICE_INSERT_DIRECT_DEPOSIT, request , InatecDepositMethodResult.class);
	}
	
	public void insertDepositCallBack(Object resultObj){
		Log.d(TAG, "insertDepositCallBack");
		submitButton.stopLoading();
		InatecDepositMethodResult result = (InatecDepositMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			url = result.getRedirectUrl();
			transactionId = result.getTransactionId();
			openWebView();
		}
	}
	
	public void finishDeposit(){
		Log.d(TAG, "finishDeposit");
		InatecDepositFinishMethodRequest request = new InatecDepositFinishMethodRequest();
		request.setInatecTransactionId(transactionIdOther);
		request.setCs(cs);
		Application.get().getCommunicationManager().requestService(BaseOtherDepositFragment.this, "finishDepositCallBack", Constants.SERVICE_FINISH_DIRECT_DEPOSIT, request , TransactionMethodResult.class);
	}
	
	public void finishDepositCallBack(Object resultObj){
		Log.d(TAG, "finishDepositCallBack");
		TransactionMethodResult result = (TransactionMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			//Check if the status is OK or PENDING:
			if(result.isSuccess()){
				
				User user = application.getUser();
				double newBalance = result.getBalance();
				double dollarAmount = result.getDollarAmount();

				// Update the user with new balance.
				user.setBalance(AmountUtil.getLongAmount(newBalance));
				user.setBalanceWF(AmountUtil.getFormattedAmount(user.getBalance(), user.getCurrency()));
				if (application.isRegulated() && !RegulationUtils.isAfterFirstDeposit()) {
					RegulationUtils.setAfterFirstDeposit();
				}
				application.setUser(user);
				///////////////////////////////////////

				application.postEvent(new DepositEvent(amount, dollarAmount, result.isFirstDeposit()));

				//Show RECEIPT Notification:
				SimpleDateFormat dateFormat = new SimpleDateFormat(ReceiptFragment.TRANSACTION_DATE_FORMAT_PATTERN,application.getLocale());
				String dateFormatted = dateFormat.format(new Date(TimeUtils.getCurrentServerTime()));
				application.getNotificationManager().showNotificationDepositOK(AmountUtil.getUserFormattedAmount(amount), dateFormatted, result.getTransactionId());
				/////////////////////////////////
				
			}else{
				//PENDING Status:
				application.getNotificationManager().showNotificationSuccess(getString(R.string.depositOtherScreenTransactionPendingMessage));
			}
			
			application.getCurrentActivity().onBackPressed();
		}
	}
	
	//#####################################################################
	
	protected void setupFormFields(){
		form.addItem(amountField, accountNumberField, bankCodeField, accountHolderField);
	}
	
	protected void setupCommonFields(){
		if(commonFieldsLayout != null){
			if(isFirstDeposit){
				commonFieldsLayout.setVisibility(View.VISIBLE);
				form.addItem(addressField, cityField, zipField, birthDateField);
			}else{
				commonFieldsLayout.setVisibility(View.GONE);
			}
		}
	}
	
	protected void setupInsertDepositParams(InatecDepositMethodRequest request){
		request.setPaymentTypeParam(getType());
		request.setDepositAmount(amountField.getValue());
		request.setAccountNum(accountNumberField.getValue());
		request.setBankSortCode(bankCodeField.getValue());
		request.setBeneficiaryName(accountHolderField.getValue());
	}
	
	protected void setUserCommonFields(User user) {
		user.setCurrencyId(currencySelector.getSelectedCurrency().getId());
		user.setCurrency(currencySelector.getSelectedCurrency());
		user.setStreet(addressField.getValue());
		user.setCityName(cityField.getValue());
		user.setZipCode(zipField.getValue());
		if (birthDateField.getValue() != null && !birthDateField.getValue().equalsIgnoreCase("")) {			
			Date timeBirthDate = null;
			try {			
				timeBirthDate = new SimpleDateFormat(FormDatePicker.DATE_FORMAT_DAY_MONTH_YEAR, application.getLocale()).parse(birthDateField.getValue());
			} catch (ParseException e) {			
				timeBirthDate = null;
			}
			user.setTimeBirthDate(timeBirthDate);
		}
	}
	
	private void setSubmitButtonEnabled(boolean enabled) {
		if(submitButton != null){
			submitButton.setEnabled(enabled);
		}
	}

	protected void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	protected void openWebView(){
		webView.setVisibility(View.VISIBLE);
		webView.loadUrl(url);
	}
	
	//#####################################################################
	//#########					WEB VIEW CLIENT					###########
	//#####################################################################	
	
	/**
	 * WebView Client for the Deposit Payment Method.
	 * @author Anastas Arnaudov
	 *
	 */
	private class DepositPaymentMethodClient extends WebViewClient {
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	if(url.contains("anyoption.com")){
	    		//The user has finished and he is navigated back to anyoption site:
	    		try {
					transactionIdOther = Long.valueOf(Utils.getParameterFromUrl(url, PARAM_TRANSACTION_ID));
					cs = Utils.getParameterFromUrl(url, PARAM_CS);
	    			finishDeposit();
				} catch (Exception e) {
					Log.e(TAG, "Could NOT parse Url params", e);
				}
	    	}else{
	    		view.loadUrl(url);
	    	}
	        return true;
	    }
	}
	//#####################################################################
}
