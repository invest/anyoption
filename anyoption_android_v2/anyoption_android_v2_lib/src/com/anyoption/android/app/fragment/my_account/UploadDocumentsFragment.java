package com.anyoption.android.app.fragment.my_account;

import com.anyoption.android.app.Application.OnActivityResultListener;
import com.anyoption.android.app.Application.Screen;
import com.anyoption.android.app.Application.Screenable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.anyoption.android.app.Application;
import com.anyoption.android.app.R;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.model.CreditCardItem;
import com.anyoption.android.app.popup.DocumentsCreditCardDialogFragment;
import com.anyoption.android.app.popup.DocumentsCreditCardDialogFragment.OnCreditCardSelectedListener;
import com.anyoption.android.app.popup.DocumentsProofOfIdDialogFragment;
import com.anyoption.android.app.popup.DocumentsProofOfIdDialogFragment.OnPopupDocumentTypeSelectedListener;
import com.anyoption.android.app.util.BitmapUtils;
import com.anyoption.android.app.util.DrawableUtils;
import com.anyoption.android.app.util.TextUtils;
import com.anyoption.android.app.util.Utils;
import com.anyoption.android.app.util.FontsUtils.Font;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.form.FormDocumentsUploadItem;
import com.anyoption.android.app.widget.form.FormDocumentsUploadItem.OnDocumentTypeSelectedListener;
import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.File;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.json.results.CreditCardsMethodResult;
import com.anyoption.json.results.RetrieveUserDocumentsMethodResult;
import com.anyoption.json.results.UpdateMethodResult;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UploadDocumentsFragment extends NavigationalFragment implements OnDocumentTypeSelectedListener,
		OnPopupDocumentTypeSelectedListener, OnActivityResultListener, OnCreditCardSelectedListener {

	protected View rootView;

	protected FormDocumentsUploadItem utilityBillItem;
	protected FormDocumentsUploadItem proofOfIdItem;
	protected FormDocumentsUploadItem creditCardItem;
	protected FormDocumentsUploadItem creditCardFrontItem;
	protected FormDocumentsUploadItem creditCardBackItem;

	protected TextView topTextViewForBolding;
	protected TextView bottomTextViewForBolding;

	protected LinearLayout uploadedFilesMessage;

	protected Bitmap bitmap;

	protected DocumentsProofOfIdDialogFragment documentsProofOfIdDialog;
	protected DocumentsCreditCardDialogFragment documentsCreditCardDialog;

	private List<CreditCardItem> ccListItems = new ArrayList<CreditCardItem>();

	private Map<Long, Map<Long, File>> ccFilesMap = new HashMap<Long, Map<Long, File>>();

	@Override
	protected Screenable setupScreen() {
		return Screen.UPLOAD_DOCUMENTS;
	}

	/**
	 * Use this method to get new instance of the fragment.
	 * 
	 * @return
	 */
	public static UploadDocumentsFragment newInstance(Bundle bundle) {
		UploadDocumentsFragment uploadDocumentsFragment = new UploadDocumentsFragment();
		uploadDocumentsFragment.setArguments(bundle);
		return uploadDocumentsFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application.addOnActivityResultListener(this);

		documentsProofOfIdDialog = DocumentsProofOfIdDialogFragment.newInstance(null);
		documentsProofOfIdDialog.setPopupDocumentTypeSelectedListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		application.removeOnActivityResultListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.upload_documents_layout, container, false);

		setupUI();

		setupUploadDocumentsListUI();

		return rootView;
	}

	protected void setupUI() {
		topTextViewForBolding = (TextView) rootView.findViewById(R.id.upload_documents_top_text);
		bottomTextViewForBolding = (TextView) rootView.findViewById(R.id.upload_documents_bottom_bold_text);

		rootView.findViewById(R.id.upload_documents_top_text).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.upload_documents_bottom_bold_text).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.account_blocked_top_text1).setVisibility(View.GONE);
		rootView.findViewById(R.id.account_blocked_top_text2).setVisibility(View.GONE);
		TextUtils.decorateInnerText(topTextViewForBolding, null, topTextViewForBolding.getText().toString(),
				getString(R.string.upload_documents_top_text_bold), Font.ROBOTO_BOLD, 0, 0);
		TextUtils.decorateInnerText(bottomTextViewForBolding, null, bottomTextViewForBolding.getText().toString(),
				getString(R.string.upload_documents_bottom_text2_bold), Font.ROBOTO_BOLD, 0, 0);

		uploadedFilesMessage = (LinearLayout) rootView.findViewById(R.id.upload_documents_uploaded_document_message);
		uploadedFilesMessage.setVisibility(View.GONE);
	}

	protected void setupUploadDocumentsListUI() {
		utilityBillItem = (FormDocumentsUploadItem) rootView.findViewById(R.id.upload_documents_item_utility_bill);
		proofOfIdItem = (FormDocumentsUploadItem) rootView.findViewById(R.id.upload_documents_item_proof_of_id);
		creditCardItem = (FormDocumentsUploadItem) rootView.findViewById(R.id.upload_documents_item_credit_card);
		creditCardFrontItem = (FormDocumentsUploadItem) rootView
				.findViewById(R.id.upload_documents_item_credit_card_front);
		creditCardBackItem = (FormDocumentsUploadItem) rootView
				.findViewById(R.id.upload_documents_item_credit_card_back);

		utilityBillItem.setName(application.getString(R.string.upload_documents_utility_bill_text));
		proofOfIdItem.setName(application.getString(R.string.upload_documents_proof_of_id_text));
		creditCardItem.setName(application.getString(R.string.upload_documents_credit_card_text));
		creditCardFrontItem.setName(application.getString(R.string.upload_documents_credit_card_front_text));
		creditCardBackItem.setName(application.getString(R.string.upload_documents_credit_card_back_text));
		creditCardFrontItem.setImage(R.drawable.cc_front_icon);
		creditCardBackItem.setImage(R.drawable.cc_back_icon);

		utilityBillItem.setDocumentTypeSelectedListener(this);
		proofOfIdItem.setDocumentTypeSelectedListener(this);
		creditCardItem.setDocumentTypeSelectedListener(this);
		creditCardFrontItem.setDocumentTypeSelectedListener(this);
		creditCardBackItem.setDocumentTypeSelectedListener(this);

		updateFileList();
	}

	@Override
	public void onDocumentTypeSelected(View view) {
		if (view == utilityBillItem) {
			openImageGallery(Constants.UPLOAD_DOCUMENTS_TYPE_UTILITY_BILL);
		} else if (view == proofOfIdItem) {
			documentsProofOfIdDialog.setChosenDocumentType(proofOfIdItem.getDocumentType());
			application.getFragmentManager().showDialogFragment(documentsProofOfIdDialog);
		} else if (view == creditCardItem) {
			showCreditCardDialog();
		} else if (view == creditCardFrontItem) {
			if (creditCardItem.getCreditCardID() != 0l) {
				openImageGallery(Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_FRONT);
			}
		} else if (view == creditCardBackItem) {
			if (creditCardItem.getCreditCardID() != 0l) {
				openImageGallery(Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_BACK);
			}
		}
	}

	@Override
	public void onPopupDocumentTypeSelected(long documentType) {
		if (documentType == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT) {
			openImageGallery(documentType);
		} else if (documentType == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID) {
			openImageGallery(documentType);
		} else if (documentType == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE) {
			openImageGallery(documentType);
		}

	}

	@Override
	public void onCreditCardSelected(CreditCardItem item, int position) {
		if (ccFilesMap.containsKey(item.getId())) {

			for (Entry<Long, File> ccFilesEntry : ccFilesMap.get(item.getId()).entrySet()) {
				changeDocumentStatuses(((Long) ccFilesEntry.getKey()).longValue(), 0l,
						((File) ccFilesEntry.getValue()).getFileStatusId(),
						((File) ccFilesEntry.getValue()).getStatusMsgId(), ((File) ccFilesEntry.getValue()).isLock(),
						((File) ccFilesEntry.getValue()).getFileNameForClient());

			}

			creditCardItem.setFileName(item.getName());
			creditCardItem.setCreditCardID(item.getId());
		}
	}

	private void showCreditCardDialog() {
		if (ccListItems.size() > 1) {
			if (documentsCreditCardDialog == null) {
				documentsCreditCardDialog = (DocumentsCreditCardDialogFragment) Application.get().getFragmentManager()
						.showDialogFragment(DocumentsCreditCardDialogFragment.class, null);
				documentsCreditCardDialog.setItems(ccListItems);
				documentsCreditCardDialog.setOnCreditCardSelectedListener(this);
			} else {
				documentsCreditCardDialog.setItems(ccListItems);
				Application.get().getFragmentManager().showDialogFragment(documentsCreditCardDialog);
			}
		}
	}

	protected static void openImageGallery(long uploadDocumentsType) {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		Application.get().getCurrentActivity().startActivityForResult(Intent.createChooser(intent, ""),
				(int) uploadDocumentsType);
	}

	@Override
	public void onActivityResultEvent(int requestCode, int resultCode, Intent intent) {
		if (resultCode == Activity.RESULT_OK) {
			Uri uri = intent.getData();
			try {
				String fileName = Utils.getFileNameFromURI(application.getCurrentActivity(), uri);
				String fileExtension = Utils.getFileNameExtension(fileName);
				if (fileName.length() > 2 && (fileExtension.equalsIgnoreCase("jpg")
						|| fileExtension.equalsIgnoreCase("png") || fileExtension.equalsIgnoreCase("jpeg"))) {
					bitmap = MediaStore.Images.Media.getBitmap(application.getCurrentActivity().getContentResolver(),
							uri);
					if (bitmap != null) {
						bitmap = BitmapUtils.resizeAndScaleDocumentBitmap(bitmap);
						uploadFile((long) requestCode, fileName, bitmap);
					} else {
						application.getNotificationManager().showNotificationError(getString(R.string.error));
					}
				} else {
					application.getNotificationManager().showNotificationError(getString(R.string.error));
				}
			} catch (Exception e) {
			}
		}
	}

	public void updateFileList() {
		application.getCommunicationManager().requestService(this, "getUserDocumentsCallback",
				Constants.SERVICE_GET_USER_DOCUMENTS, new UserMethodRequest(), RetrieveUserDocumentsMethodResult.class,
				true, false);
	}

	public void updateCreditCardList() {
		Application.get().getCommunicationManager().requestService(UploadDocumentsFragment.this, "ccListCallBack",
				Constants.SERVICE_GET_AVAILABLE_CREDIT_CARDS, new UserMethodRequest(), CreditCardsMethodResult.class);
	}

	public void changeDocumentStatuses(long type, long cardID, long status, int statusMsgId, boolean isLocked,
			String fileName) {

		FormDocumentsUploadItem document = null;

		if (type == Constants.UPLOAD_DOCUMENTS_TYPE_UTILITY_BILL) {
			document = utilityBillItem;
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_FRONT) {
			document = creditCardFrontItem;
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_BACK) {
			document = creditCardBackItem;
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT) {
			document = proofOfIdItem;
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID) {
			document = proofOfIdItem;
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE) {
			document = proofOfIdItem;
		} else {
			document = null;
		}

		if (document != null) {
			if (statusMsgId == (int) Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_REQUESTED) {
				document.showMissing();
				// document.showArrow();
			} else if (statusMsgId == (int) Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_IN_PROGRESS) {
				document.showWaiting();
				// document.showArrow();
			} else if (statusMsgId == (int) Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_DONE) {
				document.showApproved();
				// document.hideArrow();
			} else if (statusMsgId == (int) Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_INVALID) {
				document.showInvalid();
				// document.showArrow();
			} else if (statusMsgId == (int) Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_NOT_NEEDED) {
				document.hideAll();
				// document.showArrow();
			} else {
				document.hideAll();
				// document.showArrow();
			}

			if (fileName != null || (fileName != null && fileName.length() > 0)) {
				document.setFileName(fileName);
			} else {
				document.setFileName("");
			}

			document.setLocked(isLocked);
		}
	}

	protected void uploadFile(long type, String fileName, Bitmap bitmap) {
		application.getCommunicationManager().uploadDocumentImage(type, fileName, creditCardItem.getCreditCardID(),
				bitmap, this, "uploadedFileCallback");

		if (type == Constants.UPLOAD_DOCUMENTS_TYPE_UTILITY_BILL) {
			utilityBillItem.setFileName(fileName);
			utilityBillItem.hideAllStatuses();
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_FRONT) {
			creditCardFrontItem.setFileName(fileName);
			creditCardFrontItem.hideAllStatuses();
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_CREDIT_CARD_BACK) {
			creditCardBackItem.setFileName(fileName);
			creditCardBackItem.hideAllStatuses();
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_PASSPORT) {
			proofOfIdItem.setFileName(fileName);
			proofOfIdItem.hideAllStatuses();
			proofOfIdItem.setDocumentType(type);
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_ID) {
			proofOfIdItem.setFileName(fileName);
			proofOfIdItem.hideAllStatuses();
			proofOfIdItem.setDocumentType(type);
		} else if (type == Constants.UPLOAD_DOCUMENTS_TYPE_PROOF_OF_ID_DRIVER_LICENSE) {
			proofOfIdItem.setFileName(fileName);
			proofOfIdItem.hideAllStatuses();
			proofOfIdItem.setDocumentType(type);
		}
	}

	// TODO
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getUserDocumentsCallback(Object resultObj) {
		RetrieveUserDocumentsMethodResult result = (RetrieveUserDocumentsMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

			ccFilesMap = result.getCcFilesMap();
			if (ccFilesMap.containsKey(0l)) {
				ccFilesMap.remove(0l);
			}

			// DOCUMENTS
			long type = 0l;
			long status = 0l;

			boolean isAfterUtilityBill = false;

			for (Map.Entry<Long, File> documentsFilesEntry : result.getUserDocuments().entrySet()) {

				type = documentsFilesEntry.getKey().longValue();
				status = documentsFilesEntry.getValue().getStatusMsgId();

				changeDocumentStatuses(type, 0l, documentsFilesEntry.getValue().getFileStatusId(),
						documentsFilesEntry.getValue().getStatusMsgId(), documentsFilesEntry.getValue().isLock(),
						documentsFilesEntry.getValue().getFileNameForClient());
				if (isAfterUtilityBill && status != 0l
						&& status != Constants.UPLOAD_DOCUMENTS_DOCUMENT_STATUS_NOT_NEEDED) {
					break;
				}

				if (type == Constants.UPLOAD_DOCUMENTS_TYPE_UTILITY_BILL) {
					isAfterUtilityBill = true;
				}
			}

			// CC
			if(ccFilesMap.size() > 0) {
			Entry<Long, Map<Long, File>> minValue = Collections.min(ccFilesMap.entrySet(),
					new Comparator<Entry<Long, Map<Long, File>>>() {

						@Override
						public int compare(Entry<Long, Map<Long, File>> lhs, Entry<Long, Map<Long, File>> rhs) {
							Iterator leftSideIterator, rightSideIterator;
							long leftSideScore = 0l, rightSideScore = 0l;
							leftSideIterator = lhs.getValue().entrySet().iterator();
							rightSideIterator = rhs.getValue().entrySet().iterator();
							leftSideScore = ((Entry<Long, File>) leftSideIterator.next()).getValue().getFileStatusId()
									+ ((Entry<Long, File>) leftSideIterator.next()).getValue().getFileStatusId();
							rightSideScore = ((Entry<Long, File>) rightSideIterator.next()).getValue().getFileStatusId()
									+ ((Entry<Long, File>) rightSideIterator.next()).getValue().getFileStatusId();
							return leftSideScore < rightSideScore ? -1 : 1;
						}
					});

			if (creditCardItem.getCreditCardID() == 0l) {
				creditCardItem.setCreditCardID(minValue.getKey().longValue());
				for (Map.Entry ccFilesEntry : minValue.getValue().entrySet()) {
					if (creditCardItem.getCreditCardID() == ((Long) minValue.getKey()).longValue()) {
						changeDocumentStatuses(((Long) ccFilesEntry.getKey()).longValue(),
								minValue.getKey().longValue(), ((File) ccFilesEntry.getValue()).getFileStatusId(),
								((File) ccFilesEntry.getValue()).getStatusMsgId(),
								((File) ccFilesEntry.getValue()).isLock(),
								((File) ccFilesEntry.getValue()).getFileNameForClient());
					}
				}
			} else {
				for (Map.Entry<Long, Map<Long, File>> ccEntry : ccFilesMap.entrySet()) {
					// when server returns 0 cardId
					for (Map.Entry ccFilesEntry : ccEntry.getValue().entrySet()) {
						if (creditCardItem.getCreditCardID() == ((Long) ccEntry.getKey()).longValue()) {
							changeDocumentStatuses(((Long) ccFilesEntry.getKey()).longValue(),
									ccEntry.getKey().longValue(), ((File) ccFilesEntry.getValue()).getFileStatusId(),
									((File) ccFilesEntry.getValue()).getStatusMsgId(),
									((File) ccFilesEntry.getValue()).isLock(),
									((File) ccFilesEntry.getValue()).getFileNameForClient());
						}
					}
					break;
				}
			}
						
			updateCreditCardList();
			} else {
				creditCardItem.setCreditCardID(0l);
				creditCardItem.setFileName("");
				creditCardItem.setEnabled(false);
				creditCardItem.hideArrow();
				creditCardItem.hideAllStatuses();
				creditCardFrontItem.setEnabled(false);
				creditCardBackItem.setEnabled(false);
				creditCardFrontItem.hideArrow();
				creditCardBackItem.hideArrow();
			}
		}
	}

	public void uploadedFileCallback(Object resultObj) {
		UpdateMethodResult result = (UpdateMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {

			updateFileList();

			uploadedFilesMessage.setVisibility(View.VISIBLE);
		} else {
			uploadedFilesMessage.setVisibility(View.GONE);
			application.getNotificationManager().showNotificationError(getString(R.string.error));
		}
	}

	public void ccListCallBack(Object result) {
		CreditCardsMethodResult creditCardsMethodResult = (CreditCardsMethodResult) result;
		if (creditCardsMethodResult != null && creditCardsMethodResult.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			CreditCard[] ccList = creditCardsMethodResult.getOptions();
			if (null != ccList) {
				ccListItems.clear();
				for (CreditCard cc : ccList) {
					if (ccFilesMap.size() > 1) {
						if (null != cc && cc.getCcNumber() > 0) {
							if (ccFilesMap.containsKey(cc.getId())) {
								Drawable imgCard = DrawableUtils.getImageByCCType(
										(int) (cc.getTypeIdByBin() == 0l ? cc.getTypeId() : cc.getTypeIdByBin()));
								ccListItems.add(new CreditCardItem(cc.getId(),
										String.valueOf(cc.getTypeName() + " ("
												+ Utils.getCCHiddenNumberShort(String.valueOf(cc.getCcNumber())) + ")"),
										imgCard));
								if (creditCardItem.getCreditCardID() == cc.getId()) {
									creditCardItem.setFileName(String.valueOf(cc.getTypeName() + " ("
											+ Utils.getCCHiddenNumberShort(String.valueOf(cc.getCcNumber())) + ")"));
								}
							}
						}
					} else {
						if (ccFilesMap.containsKey(cc.getId())) {
							creditCardItem.setCreditCardID(cc.getId());
							creditCardItem.setFileName(String.valueOf(cc.getTypeName() + " ("
									+ Utils.getCCHiddenNumberShort(String.valueOf(cc.getCcNumber())) + ")"));
							Drawable imgCard = DrawableUtils.getImageByCCType(
									(int) (cc.getTypeIdByBin() == 0l ? cc.getTypeId() : cc.getTypeIdByBin()));
							ccListItems.add(new CreditCardItem(cc.getId(),
									String.valueOf(cc.getTypeName() + " ("
											+ Utils.getCCHiddenNumberShort(String.valueOf(cc.getCcNumber())) + ")"),
									imgCard));
							
							creditCardItem.setEnabled(false);
							creditCardItem.hideArrow();
							creditCardItem.hideAllStatuses();
						}
						break;
					}
				}

				if (ccListItems.size() < 1) {
					creditCardItem.setCreditCardID(0l);
					creditCardItem.setFileName("");
					creditCardItem.setEnabled(false);
					creditCardItem.hideArrow();
					creditCardItem.hideAllStatuses();
					creditCardFrontItem.setEnabled(false);
					creditCardBackItem.setEnabled(false);
					creditCardFrontItem.hideArrow();
					creditCardBackItem.hideArrow();
				}
				if (ccListItems.size() == 1) {
					creditCardItem.setEnabled(false);
					creditCardItem.hideArrow();
					creditCardItem.hideAllStatuses();
				}
				if (ccListItems.size() > 1) {
					creditCardItem.setEnabled(true);
					creditCardItem.showArrow();
					creditCardItem.hideAllStatuses();
				}

			} else {
				Log.d(TAG, "NO Credit Cards.");
				creditCardItem.setCreditCardID(0l);
				creditCardItem.setFileName("");
				creditCardItem.setEnabled(false);
				creditCardItem.hideArrow();
				creditCardItem.hideAllStatuses();
				creditCardFrontItem.setEnabled(false);
				creditCardBackItem.setEnabled(false);
				creditCardFrontItem.hideArrow();
				creditCardBackItem.hideArrow();
			}
		} else {
			Log.e(TAG, "Could NOT get the Credit Cards form the server.");
			creditCardItem.setCreditCardID(0l);
			creditCardItem.setFileName("");
			creditCardItem.setEnabled(false);
			creditCardItem.hideArrow();
			creditCardItem.hideAllStatuses();
			creditCardFrontItem.setEnabled(false);
			creditCardBackItem.setEnabled(false);
			creditCardFrontItem.hideArrow();
			creditCardBackItem.hideArrow();
		}
	}

}
