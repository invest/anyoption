package com.anyoption.android.app.fragment.my_account.questionnaire_new;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.android.app.event.NavigationEvent;
import com.anyoption.android.app.event.NavigationEvent.NavigationType;
import com.anyoption.android.app.fragment.NavigationalFragment;
import com.anyoption.android.app.model.Answer;
import com.anyoption.android.app.model.Question;
import com.anyoption.android.app.util.ErrorUtils;
import com.anyoption.android.app.util.RegulationUtils;
import com.anyoption.android.app.util.constants.Constants;
import com.anyoption.android.app.widget.Button;
import com.anyoption.android.app.widget.form.FormQuestionnaireItem;
import com.anyoption.android.app.widget.form.FormQuestionnaireItem.OnAnswerSelectedListener;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.json.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.json.results.UserQuestionnaireResult;

import android.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public abstract class QuestionnaireBaseFragment extends NavigationalFragment implements OnAnswerSelectedListener{

	public final String TAG = QuestionnaireBaseFragment.class.getSimpleName();
	
	protected boolean isLoading = true;
	@SuppressLint("UseSparseArrays")
	protected Map<Long, FormQuestionnaireItem> questionItemsMap = new HashMap<Long, FormQuestionnaireItem>();
	protected static List<QuestionnaireUserAnswer> answers;
	protected View loadingView;
	protected Button submitButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getQuestionnaire();
	}
	
	protected abstract void loadQuestions();
	
	@Override
	public void onAnswerSelected(Question question, Answer answer) {
		updateUserAnswers();
		updateQuestionnaire();
		setupSubmitButton();
	}

	protected void setupLoading(){
		if(loadingView != null){
			if(isLoading){
				loadingView.setVisibility(View.VISIBLE);
			}else{
				loadingView.setVisibility(View.GONE);
			}
		}
	}
	
	protected void enableQuestions(boolean isEnabled){
		if(!questionItemsMap.isEmpty()){
			for (Map.Entry<Long, FormQuestionnaireItem> entry : questionItemsMap.entrySet()){
				FormQuestionnaireItem item = entry.getValue();
				item.setEnabled(isEnabled);
			}
		}
	}
	
	protected void setupSubmitButton(){
		if(submitButton != null){
			boolean isAllQuestionsAnswered = false;
			if(submitButton.getTag().equals("Screen1"))
			{			
				isAllQuestionsAnswered = isFirstScreenQuestionsAnswered();
			} else {
				isAllQuestionsAnswered = isQuestionsAnswered();
			}
		}
	}
	
	protected boolean isFirstScreenQuestionsAnswered(){
		boolean result = true;
		if(answers != null){
			for(QuestionnaireUserAnswer answer : answers){
				if(answer.getQuestionId() == 81l || answer.getQuestionId() == 82l || answer.getQuestionId() == 83l || answer.getQuestionId() == 84l || answer.getQuestionId() == 85l){
					if(answer.getAnswerId() == null) {
					result = false;
					break;
					}
				}
			}
		}
		return result;
	}
	
	protected boolean isQuestionsAnswered(){
		boolean result = true;
		if(answers != null){
			for(QuestionnaireUserAnswer answer : answers){
				if(answer.getAnswerId() == null){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	
	protected void updateQuestionsSelectors() {
		if(answers != null && !questionItemsMap.isEmpty()){
			for(QuestionnaireUserAnswer answer : answers){
				FormQuestionnaireItem formQuestionSelector = questionItemsMap.get(answer.getQuestionId());
				if(formQuestionSelector != null){
					Question question = formQuestionSelector.getQuestion();
					Answer value = question.getAnswerForId(answer.getAnswerId());
					if(value != null){
						formQuestionSelector.setAnswer(value);
					}
				}
			}
		}
	}
	
	protected void updateUserAnswers(){
		if(answers != null){
			for(QuestionnaireUserAnswer answer : answers){
				FormQuestionnaireItem formQuestionSelector = questionItemsMap.get(answer.getQuestionId());
				if(formQuestionSelector != null){
					Answer value = formQuestionSelector.getAnswer();
					if(value != null){
						answer.setAnswerId(value.getId());
					}
				}
			}
		}
	}
	
	///////////				REQUESTs and RESPONSEs			////////////////////
	
	protected void getQuestionnaire(){
		Log.d(TAG, "getQuestionnaire");
		if(application.getCommunicationManager().requestService(this, "getQuestionnaireCallback", Constants.SERVICE_GET_SINGLE_QUESTIONNAIRIE_ANSWERS, new UserMethodRequest(), UserQuestionnaireResult.class, false, false)){
			isLoading = false;
			setupLoading();
		}
	}
	
	public void getQuestionnaireCallback(Object resultObj){
		Log.d(TAG, "getQuestionnaireCallback");
		isLoading = false;
		setupLoading();
		UserQuestionnaireResult result = (UserQuestionnaireResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "Got user saved answers.");
			answers = result.getUserAnswers();
			
			if (RegulationUtils.isQuestionnaireFilled()) {
				application.postEvent(new NavigationEvent(application.getHomeScreen(), NavigationType.INIT));
			}
			
			updateQuestionsSelectors();
			enableQuestions(true);
			setupSubmitButton();
		}else {
			Log.e(TAG, "Getting user answers failed");
		}
	}
	
	protected void updateQuestionnaire() {
		Log.d(TAG, "updateQuestionnaire");
		UpdateUserQuestionnaireAnswersRequest request = new UpdateUserQuestionnaireAnswersRequest();
		updateUserAnswers();
		request.setUserAnswers(answers);
		
		if(application.getCommunicationManager().requestService(this, "updateQuestionnaireCallback", Constants.SERVICE_UPDATE_QUESTIONNAIRIE_ANSWERS, request , UserMethodResult.class, false, false)){
		}
	}
	
	public void updateQuestionnaireCallback(Object resultObj){
		Log.d(TAG, "updateQuestionnaireCallback");
		UserMethodResult result = (UserMethodResult) resultObj;
		if (result != null && result.getErrorCode() == Constants.ERROR_CODE_SUCCESS) {
			Log.d(TAG, "updateQuestionnaireCallback OK.");
			application.setUserRegulation(result.getUserRegulation());
		}else {
			Log.e(TAG, "updateQuestionnaireCallback failed");
			ErrorUtils.handleResultError(result);
		}
	}
	
	protected abstract void submit();
	//////////////////////////////////////////////////////////////////////////////
	
}
