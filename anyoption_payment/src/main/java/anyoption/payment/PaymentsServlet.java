package anyoption.payment;

import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.util.ClearingUtil;


/**
 * @author liors
 *
 */
public class PaymentsServlet extends HttpServlet {
	
	private static final long serialVersionUID = 7688813724239230894L;
	private static final Logger logger = Logger.getLogger(PaymentsServlet.class);
	private static final String LOG_PREFIX = "payments servlet;";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		logger.info(LOG_PREFIX + "Start;");
		
		try {
			String providerResponse = ClearingUtil.executePOSTRequest("https://cguat2.creditguard.co.il/xpo/Relay", request.getQueryString());
			byte[] data = providerResponse.getBytes("UTF-8");
			OutputStream os = response.getOutputStream();
			response.setHeader("Content-Type", "application/json");
			response.setHeader("Content-Length", String.valueOf(data.length));
			os.write(data, 0, data.length);
			os.flush();
			os.close();					
		} catch (Exception e) {
			logger.error(LOG_PREFIX + "ERROR! ", e);
		}
		
		logger.info(LOG_PREFIX + "End;");
	}
}
