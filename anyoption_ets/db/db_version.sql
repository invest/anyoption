-- Table: marketing_tracking_activity
CREATE TABLE marketing_tracking_activity
(
  id serial NOT NULL,
  activity character varying(20),
  CONSTRAINT marketing_tracking_activity_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_tracking_activity
  OWNER TO traker;

  
-- Table: marketing_tracking_static
CREATE TABLE marketing_tracking_static
(
  id serial NOT NULL,
  mid character varying(400),
  time_static timestamp without time zone,
  time_created timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT marketing_tracking_static_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_tracking_static
  OWNER TO traker;
  
  
  
  
-- Table: marketing_tracking
CREATE TABLE marketing_tracking
(
  id serial NOT NULL,
  mid_id integer,
  combination_id integer,
  dynamic_param character varying(1000),
  http_referer character varying(5000),
  request_url character varying(5000),
  time_dynamic timestamp without time zone,
  activity_id integer,
  contact_id integer,
  user_id integer,
  time_created timestamp without time zone NOT NULL DEFAULT now(),
  ip character varying(40),
  CONSTRAINT marketing_tracking_pkey PRIMARY KEY (id),
  CONSTRAINT marketing_tracking_activity_id_fkey FOREIGN KEY (activity_id)
      REFERENCES marketing_tracking_activity (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT marketing_tracking_mid_id_fkey FOREIGN KEY (mid_id)
      REFERENCES marketing_tracking_static (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_tracking
  OWNER TO traker;
  
 
 
 -- Table: marketing_channel
CREATE TABLE marketing_channel
(
  id integer NOT NULL,
  name character varying(20),
  priority integer,
  CONSTRAINT marketing_channel_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_channel
  OWNER TO traker;
  
  
-- Table: marketing_campaigns
CREATE TABLE marketing_campaigns
(
  id integer NOT NULL,
  marketing_channel_priority_id integer,
  CONSTRAINT marketing_campaigns_pkey PRIMARY KEY (id),
  CONSTRAINT marketing_campaigns_marketing_channel_priority_id_fkey FOREIGN KEY (marketing_channel_priority_id)
      REFERENCES marketing_channel (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_campaigns
  OWNER TO traker;
  
  
-- Table: marketing_combinations
CREATE TABLE marketing_combinations
(
  id integer NOT NULL,
  campaign_id integer,
  CONSTRAINT marketing_combinations_pkey PRIMARY KEY (id),
  CONSTRAINT marketing_combinations_marketing_campaigns_id_fkey FOREIGN KEY (campaign_id)
      REFERENCES marketing_campaigns (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_combinations
  OWNER TO traker;
  
  
-- Table: skin_combination
CREATE TABLE skin_combination
(
  skin_id integer,
  default_combination_id integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE skin_combination
  OWNER TO traker;
  
  
-- Table: client_info
CREATE TABLE client_info
(
  id serial NOT NULL,
  mid character varying(400),
  user_agent character varying(500),
  user_resolution character varying(20),
  platform character varying(50),
  create_date timestamp without time zone DEFAULT now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE client_info
  OWNER TO traker;  

----------------------------------  
CREATE TABLE marketing_tracking_activity_lock
(
  id serial NOT NULL,
  mid character varying(400),
  mid_id integer,
  combination_id integer,
  dynamic_param character varying(1000),
  http_referer character varying(5000),
  request_url character varying(5000),
  contact_id integer,
  user_id integer,
  time_created timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT marketing_tracking_activity_lock_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE marketing_tracking_activity_lock
  OWNER TO traker;
  
  
 CREATE OR REPLACE FUNCTION first_deposits(uid text, umid text)
  RETURNS integer AS
$BODY$
	DECLARE 
		c_marketing_trac CURSOR  FOR
			SELECT * FROM marketing_tracking_activity_lock
			WHERE user_id =uid::INTEGER;

		v_mark_param marketing_tracking_activity_lock%rowtype;
		v_found_user_data integer;

		c_mid CURSOR FOR
			SELECT id FROM marketing_tracking_static
			WHERE mid = umid;
		v_mid_id integer;

		v_result integer;		

        BEGIN
        -- RESULT 1--> FOUND DATA FOR USER_ID 
        -- RESULT 2--> NOT FOUND DATA FOR USER_ID and FOUND MID_ID
        -- RESULT 3--> NOT FOUND DATA FOR USER_ID and NOT FOUND MID_ID set DEFAULY MID_ID
        
		open c_marketing_trac;
		fetch c_marketing_trac into v_mark_param;
		IF  FOUND THEN
			v_found_user_data :=1; 
			v_result := 1;
		ELSE
			v_found_user_data :=0;
			v_result := 2;
		END IF;
		close c_marketing_trac;

		IF (v_found_user_data != 0) THEN
			INSERT  INTO 
                             marketing_tracking 
                             (mid_id, combination_id, dynamic_param, http_referer, request_url,  
                             time_dynamic, activity_id, user_id )
                       VALUES 
			     (v_mark_param.mid_id, v_mark_param.combination_id, v_mark_param.dynamic_param, v_mark_param.http_referer, v_mark_param.request_url,  
                             now(), 5, v_mark_param.user_id );

			
		ELSE			
			open c_mid;
			fetch c_mid into v_mid_id;
			IF NOT FOUND THEN
				v_mid_id = 0;
				v_result := 3;
			END IF;
			close c_mid;		

			INSERT  INTO 
                             marketing_tracking 
                             (mid_id,  
                             time_dynamic, activity_id, user_id )
                       VALUES 
			     (v_mid_id,   
                             now(), 5,  uid::INTEGER );
                                   
		END IF;
		
               return v_result;
        END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION first_deposits(text, text)
  OWNER TO traker;

 
 
 
 