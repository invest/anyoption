package com.anyoption.ets.util;

public class ConstantsBase {
		
	public static final String REDIRECT_PARAM_TYPE = "type";
	public static final String REDIRECT_PARAM_TYPE_STATIC = "static";
	public static final String REDIRECT_PARAM_TYPE_DYNAMIC = "dynamic";
	public static final String ETS_COOKIE_NAME = "ets";
	public static final String ETS_COOKIE_ACTIVITY_NAME = "mla";
	
	
    public static final String SKIN_ID = "s";
    public static final String COMBINATION_ID = "combid";
    public static final String DYNAMIC_PARAM = "dp";
    public static final String HTTP_REFERE = "Referer";
    
    public static final String HOST_ANYOPTION = "anyoption.com";
    public static final String HOST_ETRADER = "etrader.co.il";
    public static final String HOST_VIP168 = "168qiquan.com";
    public static final String HOST_ANYOPTION_IT = "anyoption.it";
    public static final String HOST_ANYOPTION_TR = "anyoption.cc";
    
    public static final long MARKETING_TRACKING_CLICK = 1;
    public static final long MARKETING_TRACKING_CALL_ME = 2;
    public static final long MARKETING_TRACKING_SHORT_REG = 3;
    public static final long MARKETING_TRACKING_FULL_REG = 4;
    public static final long MARKETING_TRACKING_FIRST_DEPOSIT = 5;
    
    public static final long MARKETING_CAMPAIGN_REQUEST_TYPE = 1;
    public static final long MARKETING_COMBINATION_REQUEST_TYPE = 2;
    public static final long MARKETING_CAMPAIGN_COMBINATION_INSERT = 1;
    public static final long MARKETING_CAMPAIGN_COMBINATION_UPDATE = 2; 
    
    public static final long MARKETING_TRACKING_DAYS_CHECK = 45;
        
}