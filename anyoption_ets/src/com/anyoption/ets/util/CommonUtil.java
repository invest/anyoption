package com.anyoption.ets.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.ets.beans.EtsCookieValue;
import com.anyoption.ets.managers.MarketingTrackingManager;

public class CommonUtil {
    
    private static final Logger log = Logger.getLogger(CommonUtil.class); 
    
    private static HashMap<Long, Long> skinCombination;
    
    public static String generateRandomUUId(String prefix){
         UUID uniqueKey = UUID.randomUUID();         
         return (prefix + uniqueKey);
    }
    
    public static void addCookie(String name, String value, HttpServletRequest request, HttpServletResponse response, String property) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year 
        if (null != property) {
            cookie.setDomain(property);
        }
        response.addCookie(cookie);
        log.log(Level.DEBUG, "saving " + name + " in cookie with value : " + value);
    }
    
    public static String getCookieByHostRequest(HttpServletRequest request){
        
        String cookieHost = "";
        try {
            String host = new URL(request.getRequestURL().toString()).getHost();
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION) > -1) {
                cookieHost = "cookie.domain.AO";
            }
            if (host.indexOf(ConstantsBase.HOST_ETRADER) > -1) {
                cookieHost = "cookie.domain.ET";
            }
            if (host.indexOf(ConstantsBase.HOST_VIP168) > -1) {
                cookieHost = "cookie.domain.VIP";
            }
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION_IT) > -1) {
                cookieHost = "cookie.domain.IT";
            }
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION_TR) > -1) {
                cookieHost = "cookie.domain.TR";
            }
        } catch (MalformedURLException e) {
            log.error("Can't get property by host name : " + e);
        }
        return cookieHost;
    }
    
    public static String getIPAddress(HttpServletRequest req) {
        String ipNotFound = "IP NOT FOUND!";
        String ip = null;
        if (req != null) {
            ip = req.getHeader("x-forwarded-for");
            if (null == ip) {
                ip = req.getRemoteAddr();
            }
            log.info("Full ip from header = " + ip);
            int indexOfLastComma = ip.indexOf(',');    // for proxy users(list of ip's) we need only the first ip in the list           
            if (indexOfLastComma != -1) {
                ip = ip.substring(0, indexOfLastComma).trim();
                log.info("First ip only = " + ip);
            }
        }
        return ip == null ? ipNotFound : ip ;
    }
    
    public static EtsCookieValue parseEtsCookie(String value, String activity){
        EtsCookieValue ets = new EtsCookieValue();
        try {
            String ts = value.substring(0, value.indexOf("|"));          
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            ets.setCreateDateTime(sdf.parse(ts));
            
            ets.setLastActivity(Long.parseLong(activity));
            ets.setUId(value.substring(value.indexOf("|") + 1, value.length()));            
        } catch (Exception e) {
            log.error("ParseEtsCookie: ", e);
        }        
        return ets;
    }
    
    private static void loadCombinationSkinIfEmpty() throws SQLException{
        if(skinCombination == null){
            skinCombination = MarketingTrackingManager.getAllSkinCombination(); 
        }
    }
    
    public static long getCombinationIdBySkin (long skinId) throws SQLException{
        loadCombinationSkinIfEmpty();
        return skinCombination.get(skinId);        
    }

}
