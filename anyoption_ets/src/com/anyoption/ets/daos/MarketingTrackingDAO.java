package com.anyoption.ets.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.ClientInfo;
import com.anyoption.ets.beans.MarketingRequestedObj;
import com.anyoption.ets.beans.MarketingTrackingParam;
import com.anyoption.ets.util.ConstantsBase;

public class MarketingTrackingDAO extends DAOBase{
	
	private static final Logger logger = Logger.getLogger(MarketingTrackingDAO.class);
	
	public static String getPostGreVer(Connection con)
			throws SQLException {
		String ver = " ";
        Statement st = null;
        ResultSet rs = null;
		try {
			String sql = "select version()";
			st = con.createStatement();
            rs = st.executeQuery(sql);
			if (rs.next()) {
				ver = rs.getString(1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}
		return ver;
	}
		
    public static Long getMidId(Connection con, String mId) throws SQLException {
        Long midId = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            String sql = "select id from marketing_tracking_static where mid = '" + mId + "'";
            st = con.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                midId = rs.getLong(1);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(st);
        }
        return midId;
    }
    
	public static long insertMarketingStatic(Connection con, String mId)throws SQLException {	    
	        PreparedStatement pst = null;
	        long key = 0;
	        try {
	            String sql = "insert into marketing_tracking_static (mid, time_static) values (?, ?)";
	            pst = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
	            pst.setString(1, mId);
	            pst.setTimestamp(2, convertToTimestamp( new Date ()));
	            logger.debug("BEGIN INSERT ");
	            pst.execute();
	            ResultSet keyset = pst.getGeneratedKeys();
	            if ( keyset.next() ) {
	                // Retrieve the auto generated key(s).
	                key = keyset.getLong(1);
	                logger.debug("Inserted tracking_static_id:  " + key);
	            }
	        } finally {
	            closeStatement(pst);
	        }
	        return key;
	}
	
    public static long insertMarketing(Connection con, MarketingTrackingParam mt)throws SQLException {
        PreparedStatement pst = null;
        long key = 0;
        try {
            String sql = "INSERT INTO marketing_tracking( " +
                    " mid_id, combination_id, dynamic_param, http_referer, request_url, " + 
                    " time_dynamic, activity_id, ip, user_id, contact_id) " +            
               " VALUES ( ?, ?, ?, ?, ?, " + 
                        " ?, ?, ?, ?, ?) ";
            pst = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setLong(1, mt.getMidId());
            pst.setLong(2,Long.parseLong(mt.getCombinationId()));
            pst.setString(3,mt.getDyanmicParameter());
            pst.setString(4,mt.getHttpReferer());
            pst.setString(5,mt.getRequestUrl());
            pst.setTimestamp(6, convertToTimestamp(new Date()));
            pst.setLong(7, mt.getMarketingTrackingActivityId());
            pst.setString(8,mt.getIp());
            pst.setLong(9, mt.getUserId());
            pst.setLong(10, mt.getContactId());
           
            pst.execute();
            ResultSet keyset = pst.getGeneratedKeys();
            if (keyset.next()) {
                // Retrieve the auto generated key(s).
                key = keyset.getLong(1);
                logger.debug("Inserted tracking_id :  " + key);
            }
        } finally {
            closeStatement(pst);
        }
        return key;
    }
    
    public static MarketingTrackingParam getMarketingParams(Connection con, String mId) throws SQLException {
        MarketingTrackingParam mt = new MarketingTrackingParam();
         Statement st = null;
        ResultSet rs = null;
        try {
            String sql = " select  pr.priority, mt.mid_id, mt.combination_id, mt.dynamic_param, mt.http_referer, mt.request_url " +
            		            " from marketing_tracking_static mts, " + 
                                   " marketing_tracking mt, " +
                                   "(select mc.id as combination_id, coalesce(mcm.marketing_channel_priority_id,0) as priority from " +
                                               " marketing_combinations mc, " +
                                               " marketing_campaigns mcm " +
                                               " where   mc.campaign_id=mcm.id) pr " +
                                   " where mts.id =mt.mid_id " +
                                   " and mt.combination_id = pr.combination_id " +
                                   " and mts.mid='" + mId + "' " +
                                   " order by pr.priority, time_dynamic ";
            st = con.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                mt.setMidId(rs.getLong("mid_id"));
                mt.setCombinationId(rs.getString("combination_id"));
                mt.setDyanmicParameter(rs.getString("dynamic_param"));
                mt.setHttpReferer(rs.getString("http_referer"));
                mt.setRequestUrl(rs.getString("request_url"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(st);
        }
        return mt;
    }
    
    public static MarketingTrackingParam getMarketingParamsStatic(Connection con, String mId) throws SQLException {
        MarketingTrackingParam mt = new MarketingTrackingParam();
         Statement st = null;
        ResultSet rs = null;
        try {
            String sql ="SELECT  mid, mid_id, coalesce(combination_id,-1) as combination_id, dynamic_param, http_referer, request_url " +
                           "FROM marketing_tracking_activity_lock " + 
                           " where mid='" + mId + "' " ;
            st = con.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                mt.setMidId(rs.getLong("mid_id"));
                mt.setCombinationId(rs.getString("combination_id"));
                mt.setDyanmicParameter(rs.getString("dynamic_param"));
                mt.setHttpReferer(rs.getString("http_referer"));
                mt.setRequestUrl(rs.getString("request_url"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(st);
        }
        return mt;
    }
    
    public static void insertMarketingFirstDeposit(Connection con, String userId, String mId) throws SQLException {
        Statement st = null;
        ResultSet rs = null;
        try {
            String sql = "select first_deposits('" + userId + "', '" + mId + "')";
            st = con.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                logger.debug("Inserted FD for  userId: "+ userId + "and mId: " + mId +" with result:"+ rs.getString(1));                
            }

        } finally {
            closeResultSet(rs);
            closeStatement(st);
        }

    }
    
    public static HashMap<Long, Long> getAllSkinCombination(Connection con) throws SQLException {
        HashMap<Long, Long> hm = new HashMap<Long, Long>();
        Statement st = null;
        ResultSet rs = null;
        try {
            String sql = "select * from skin_combination";
            st = con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                hm.put(rs.getLong(1),rs.getLong(2));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(st);
        }
        return hm;
    }
    
    public static long insertClientInfo(Connection con, ClientInfo ci)throws SQLException {
        PreparedStatement pst = null;
        long key = 0;
        try {
            String sql = "INSERT INTO client_info " +
            " ( mid, user_agent, user_resolution, platform ) " +
            " VALUES " +
            "   ( ?, ?, ?, ?) ";
            pst = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, ci.getEtsCookie());
            pst.setString(2, ci.getUserAgent());
            pst.setString(3, ci.getUserResolution());
            pst.setString(4, ci.getPlatform());
            
            pst.execute();
            ResultSet keyset = pst.getGeneratedKeys();
            if (keyset.next()) {
                // Retrieve the auto generated key(s).
                key = keyset.getLong(1);
                logger.debug("Inserted tracking_id :  " + key);
            }
        } finally {
            closeStatement(pst);
        }
        return key;
    }
    
    public static long insertMarketingTrackingEtsActivityLock(Connection con, MarketingRequestedObj mt)throws SQLException {
        PreparedStatement pst = null;
        long key = 0;
        try {
            String sql = " INSERT INTO marketing_tracking_activity_lock " +
                    " (mid, mid_id, combination_id, dynamic_param, " + 
                    " http_referer, request_url, contact_id, user_id) " +
               " VALUES (?, ?, ?, ?, " + 
                       " ?, ?, ?, ?)" ;
            pst = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, mt.getmId());
            pst.setLong(2, mt.getMidId());
            pst.setLong(3, new Long(mt.getCombinationId()));
            pst.setString(4, mt.getDyanmicParameter());
            pst.setString(5, mt.getHttpReferer());
            pst.setString(6, mt.getRequestUrl());
            pst.setLong(7, mt.getRequestedType() != ConstantsBase.MARKETING_TRACKING_FULL_REG ? mt.getObjectId() : 0);
            pst.setLong(8, mt.getRequestedType() == ConstantsBase.MARKETING_TRACKING_FULL_REG ? mt.getObjectId() : 0);
            
            pst.execute();
            ResultSet keyset = pst.getGeneratedKeys();
            if (keyset.next()) {
                // Retrieve the auto generated key(s).
                key = keyset.getLong(1);
                logger.debug("Inserted ActivityLock: " + key + " for mid: " +  mt.getmId());
            }
        } finally {
            closeStatement(pst);
        }
        return key;
    }

}
