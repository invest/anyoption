package com.anyoption.ets.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.MarketingCampCombinationRequestedObj;


public class MarketingCampaignDAO extends DAOBase{
	
	private static final Logger logger = Logger.getLogger(MarketingCampaignDAO.class);
    
	public static void insertMarketingCampaign(Connection con, MarketingCampCombinationRequestedObj mcc)throws SQLException {	    
	        PreparedStatement pst = null;
	        try {
	            String sql = "insert  into marketing_campaigns (id, marketing_channel_priority_id) values (?, ?)";
	            pst = con.prepareStatement(sql);
	            pst.setLong(1, mcc.getPrimaryId());
	            pst.setLong(2, mcc.getForeignId());
	            logger.debug("BEGIN INSERT ");
	            pst.execute();

	            logger.debug("Inserted marketing_campaigns PK:" +  mcc.getPrimaryId() + " FK:" + mcc.getForeignId());
	        } finally {
	            closeStatement(pst);
	        }
	}
	
    public static void updateMarketingCampaign(Connection con, MarketingCampCombinationRequestedObj mcc)throws SQLException {       
        PreparedStatement pst = null;
        try {
            String sql = "update marketing_campaigns set  marketing_channel_priority_id = ? where  id = ? ";
            pst = con.prepareStatement(sql);
            pst.setLong(1, mcc.getForeignId());
            pst.setLong(2, mcc.getPrimaryId());
            logger.debug("BEGIN Update ");
            pst.execute();

            logger.debug("Update marketing_campaigns PK:" +  mcc.getPrimaryId() + " FK:" + mcc.getForeignId());
        } finally {
            closeStatement(pst);
        }
    }
    
    public static void insertMarketingCombination(Connection con, MarketingCampCombinationRequestedObj mcc)throws SQLException {       
        PreparedStatement pst = null;
        try {
            String sql = "insert into marketing_combinations (id, campaign_id) values (?, ?)";
            pst = con.prepareStatement(sql);
            pst.setLong(1, mcc.getPrimaryId());
            pst.setLong(2, mcc.getForeignId());
            logger.debug("BEGIN INSERT ");
            pst.execute();

            logger.debug("Inserted marketing_combinations PK:" +  mcc.getPrimaryId() + " FK:" + mcc.getForeignId());
        } finally {
            closeStatement(pst);
        }
    }

    public static void updateMarketingCombination(Connection con, MarketingCampCombinationRequestedObj mcc)throws SQLException {       
        PreparedStatement pst = null;
        try {
            String sql = "update marketing_combinations set  campaign_id = ? where  id = ? ";
            pst = con.prepareStatement(sql);
            pst.setLong(1, mcc.getForeignId());
            pst.setLong(2, mcc.getPrimaryId());
            logger.debug("BEGIN Update ");
            pst.execute();
    
            logger.debug("Update marketing_combinations PK:" +  mcc.getPrimaryId() + " FK:" + mcc.getForeignId());
        } finally {
            closeStatement(pst);
        }
    }    
}
