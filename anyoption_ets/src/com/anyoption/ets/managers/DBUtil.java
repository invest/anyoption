package com.anyoption.ets.managers;

import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * DB utils.
 * 
 * @author Tony
 */
public class DBUtil {
    private static Logger log = Logger.getLogger(DBUtil.class);
    
    private static DataSource dataSource;
    
    /**
     * @return The <code>DataSource</code> to obtain db connection from. Create
     *      it if not created.
     * @throws SQLException
     */    
    public static DataSource getDataSource() throws SQLException {
        if (null != dataSource) {
            return dataSource;
        }

        try {
            Context initCtx = new InitialContext();

                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                dataSource = (DataSource) envCtx.lookup("jdbc/postgres");
        } catch (Exception e) {
            log.fatal("Cannot lookup datasource", e);
        }

        return dataSource;
    }
}