package com.anyoption.ets.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.ClientInfo;
import com.anyoption.ets.beans.MarketingRequestedObj;
import com.anyoption.ets.beans.MarketingTrackingParam;
import com.anyoption.ets.beans.MarketingTrackingParamBase;
import com.anyoption.ets.daos.MarketingTrackingDAO;
import com.anyoption.ets.util.ConstantsBase;

public class MarketingTrackingManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(MarketingTrackingManager.class);

    public static String testConn() throws SQLException {
        Connection con = getConnection();
        String res = "";
        try {
            res = MarketingTrackingDAO.getPostGreVer(con);
            return res;
        } finally {
            closeConnection(con);
        }
    }

    public static Long getMidId(String mId) throws SQLException {
        Connection con = getConnection();
        Long res = null;
        try {
            res = MarketingTrackingDAO.getMidId(con, mId);
            if (res == null) {
                res = MarketingTrackingDAO.insertMarketingStatic(con, mId);
            }
            return res;

        } finally {
            closeConnection(con);
        }
    }

    public static void insertMarketingStatic(String mId) throws SQLException {
        Connection con = getConnection();
        try {
            MarketingTrackingDAO.insertMarketingStatic(con, mId);
        } finally {
            closeConnection(con);
        }
    }

    public static void insertMarketingClick(MarketingTrackingParam mt) throws SQLException {
        Connection con = getConnection();
        try {
            MarketingTrackingDAO.insertMarketing(con, mt);
        } finally {
            closeConnection(con);
        }
    }

    public static MarketingTrackingParamBase getMarketingParams(MarketingRequestedObj mro) throws SQLException {
        Connection con = getConnection();
        boolean isNeedUpdateStatic = false;
        try {
            MarketingTrackingParam mt =  MarketingTrackingDAO.getMarketingParamsStatic(con, mro.getmId());
            if ( mt.getCombinationId() == null){
                mt = MarketingTrackingDAO.getMarketingParams(con, mro.getmId());
                isNeedUpdateStatic = true;
            }           
            if (mt.getMidId() != 0) {
                if (mro.getRequestedType() == ConstantsBase.MARKETING_TRACKING_FULL_REG) {
                    mt.setUserId(mro.getObjectId());
                } else {
                    mt.setContactId(mro.getObjectId());
                }
                mt.setMarketingTrackingActivityId(mro.getRequestedType());
                mt.setIp(mro.getIp());
                MarketingTrackingDAO.insertMarketing(con, mt);

                if (isNeedUpdateStatic){
                    
                    mro.setMidId(mt.getMidId());
                    mro.setCombinationId(mt.getCombinationId());
                    mro.setDyanmicParameter(mt.getDyanmicParameter());
                    mro.setHttpReferer(mt.getHttpReferer());
                    mro.setRequestUrl(mt.getRequestUrl());
                    
                    MarketingTrackingDAO.insertMarketingTrackingEtsActivityLock(con, mro);
                }
                
                MarketingTrackingParamBase mtb = new MarketingTrackingParamBase();
                mtb.setCombinationId(mt.getCombinationId());
                mtb.setDyanmicParameter(mt.getDyanmicParameter());
                mtb.setHttpReferer(mt.getHttpReferer());

                return mtb;
            }
        } finally {
            closeConnection(con);
        }
        return null;
    }

    public static void insertMarketingFirstDeposit(List<String> usersIdMid) throws SQLException {
        Connection con = getConnection();
        try {
            for (String s : usersIdMid) {
                logger.debug("Try inserted FD for params:" + s);
                String[] parts = s.split(",");
                MarketingTrackingDAO.insertMarketingFirstDeposit(con, parts[0], parts[1]);
            }
        } finally {
            closeConnection(con);
        }
    }

    public static void insertMarketingOrganicClik(MarketingTrackingParam mtp) throws SQLException {
        Connection con = getConnection();
        Long midId = null;
        try {
            if (mtp.isMobile()) {
                midId = MarketingTrackingDAO.getMidId(con, mtp.getmId());
            }
            if (midId == null) {
                midId = MarketingTrackingDAO.insertMarketingStatic(con, mtp.getmId());
            }
            mtp.setMidId(midId);
            MarketingTrackingDAO.insertMarketing(con, mtp);
        } finally {
            closeConnection(con);
        }
    }

    public static HashMap<Long, Long> getAllSkinCombination() throws SQLException {
        Connection con = getConnection();
        try {
            return MarketingTrackingDAO.getAllSkinCombination(con);

        } finally {
            closeConnection(con);
        }
    }

    public static void insertClientInfo(ClientInfo ci) throws SQLException {
        Connection con = getConnection();
        try {
            MarketingTrackingDAO.insertClientInfo(con, ci);
        } finally {
            closeConnection(con);
        }
    }
}