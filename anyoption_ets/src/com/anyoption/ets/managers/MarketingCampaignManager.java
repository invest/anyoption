package com.anyoption.ets.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.ets.beans.MarketingCampCombinationRequestedObj;
import com.anyoption.ets.beans.MarketingRequestedObj;
import com.anyoption.ets.beans.MarketingTrackingParam;
import com.anyoption.ets.beans.MarketingTrackingParamBase;
import com.anyoption.ets.daos.MarketingCampaignDAO;
import com.anyoption.ets.daos.MarketingTrackingDAO;
import com.anyoption.ets.util.ConstantsBase;

public class MarketingCampaignManager extends BaseBLManager {    
    
    public static void insertMarketingCampaign( MarketingCampCombinationRequestedObj mcc) throws SQLException {
        Connection con = getConnection();        
        try {
                MarketingCampaignDAO.insertMarketingCampaign(con, mcc);            
        } finally {
            closeConnection(con);
        }
    }
    
    public static void updateMarketingCampaign( MarketingCampCombinationRequestedObj mcc) throws SQLException {
        Connection con = getConnection();        
        try {
                MarketingCampaignDAO.updateMarketingCampaign(con, mcc);            
        } finally {
            closeConnection(con);
        }
    }
    
    public static void insertMarketingCombination( MarketingCampCombinationRequestedObj mcc) throws SQLException {
        Connection con = getConnection();        
        try {
                MarketingCampaignDAO.insertMarketingCombination(con, mcc);            
        } finally {
            closeConnection(con);
        }
    }
    
    public static void updateMarketingCombination( MarketingCampCombinationRequestedObj mcc) throws SQLException {
        Connection con = getConnection();        
        try {
                MarketingCampaignDAO.updateMarketingCombination(con, mcc);            
        } finally {
            closeConnection(con);
        }
    }
    
    
}