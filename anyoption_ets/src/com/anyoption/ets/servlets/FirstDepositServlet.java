package com.anyoption.ets.servlets;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.managers.MarketingTrackingManager;

public class FirstDepositServlet extends HttpServlet {

    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(FirstDepositServlet.class);

    private List<String> getRequestParam(HttpServletRequest request) {
        List<String> usersIdMidId = null;
        String params;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }
            params = stringBuilder.toString();
            log.debug("In Params FD: " + params);
            usersIdMidId = Arrays.asList(params.split(";"));
        } catch (Exception e) {
            log.error("When getRequestParam FD:", e);
        }

        return usersIdMidId;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug("DoPost Marketing  FD");
        List<String> usersIdMid = null;
        int status = HttpServletResponse.SC_OK;
       
        try {
            usersIdMid = getRequestParam(request);
            MarketingTrackingManager.insertMarketingFirstDeposit(usersIdMid);
        } catch (Exception e) {
            log.error("When insert FD:", e);
            status = HttpServletResponse.SC_EXPECTATION_FAILED;
        }
        response.setStatus(status);
        PrintWriter out = response.getWriter();
        out.print(status);
        out.flush();
    }
}