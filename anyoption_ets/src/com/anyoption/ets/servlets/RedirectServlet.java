package com.anyoption.ets.servlets;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.EtsCookieValue;
import com.anyoption.ets.beans.MarketingTrackingParam;
import com.anyoption.ets.managers.MarketingTrackingManager;
import com.anyoption.ets.util.CommonUtil;
import com.anyoption.ets.util.ConstantsBase;


public class RedirectServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(RedirectServlet.class);
    private static final String HAVE_FD = "FD";
    private static final String AO_IT_DOMAIN = "anyoption.it";
    private static final String ET_DOMAIN = "etrader.co.il";
    EtsCookieValue ets;
    
    private String getCookieValueParam(){
        String cookieValue = "";
        try {
            String mId = CommonUtil.generateRandomUUId("");
            
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            String dateForDisplay = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
            
            cookieValue = dateForDisplay +"|" + mId;
        } catch (Exception e) {
            log.error("RedirectServlet getCookieValue: ", e);
        }
        
        return cookieValue;
    }
    
    private String cerateEtsCookie(HttpServletRequest request, HttpServletResponse response){
        String param = "";
        try {
            param = getCookieValueParam();
            String property = getServletContext().getInitParameter(CommonUtil.getCookieByHostRequest(request));
            CommonUtil.addCookie(ConstantsBase.ETS_COOKIE_NAME, param, request, response, property);
            CommonUtil.addCookie(ConstantsBase.ETS_COOKIE_ACTIVITY_NAME, String.valueOf(ConstantsBase.MARKETING_TRACKING_CLICK), request, response, property);
        } catch (Exception e) {
            log.error("RedirectServlet cerateEtsCookie: ", e);
        }  
        return param;
    }
    
    public static boolean isAfterCheckDatesOrNull (Date ts){
        boolean result = true;
        
        if(ts != null){
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            long millsPerDay = 1000 * 60 * 60 * 24;
            long d2 = date.getTime() / millsPerDay;
            long d1 = ts.getTime() / millsPerDay;
            long daysBetween = d2 - d1;
            if(daysBetween >= ConstantsBase.MARKETING_TRACKING_DAYS_CHECK) {
                result = true;
            } else {
                result = false;
            }
        } else {
            result = true;
        }
        return result;
    }
    
    private boolean isNeedReplace(String mId, String etsActivity){
        boolean res = false;
        ets = CommonUtil.parseEtsCookie(mId, etsActivity);
        if(isAfterCheckDatesOrNull(ets.getCreateDateTime())){
            if(ets.getLastActivity() == ConstantsBase.MARKETING_TRACKING_CLICK){
                res = true;
            }
        }
        return res;
    }
    
    private MarketingTrackingParam getTrackingDataParams(HttpServletRequest request, String mId) {
        MarketingTrackingParam mt = new MarketingTrackingParam();
        try {
            String requestUrl = request.getRequestURL().toString();
            if (request.getQueryString() != null) {
                requestUrl = requestUrl + "?" + request.getQueryString();
            }            
            log.debug("Get params from url: " + requestUrl);
            String ipAddress = CommonUtil.getIPAddress(request);
            long midId = MarketingTrackingManager.getMidId(mId);
            String combId = (String) request.getParameter(ConstantsBase.COMBINATION_ID);
            if (null == combId) {
                log.debug("Combination ID is null set default: " + mId);
                String skinId = (String) request.getParameter(ConstantsBase.SKIN_ID);
                if (skinId != null){
                    try{
                        combId = String.valueOf(CommonUtil.getCombinationIdBySkin( new Long(skinId)));
                    } catch (Exception e) {
                        log.error("Can't get default combinationId for skin " , e);
                        combId = "22";
                        log.debug("Set default combinationId 22 ");
                    }                    
                } else {
                    combId = "22";  
                    log.debug("Set default combinationId 22 ");
                }                
            }
            String dynamicParam = (String) request.getParameter(ConstantsBase.DYNAMIC_PARAM);
            String httpReferer = (String) request.getHeader(ConstantsBase.HTTP_REFERE);
            if (httpReferer != null){
                httpReferer =  URLDecoder.decode(httpReferer, "UTF-8");
            }

            mt.setMidId(midId);
            mt.setCombinationId(combId);
            mt.setDyanmicParameter(dynamicParam);
            mt.setHttpReferer(httpReferer);
            mt.setRequestUrl(requestUrl);
            mt.setMarketingTrackingActivityId(ConstantsBase.MARKETING_TRACKING_CLICK);
            mt.setIp(ipAddress);

        } catch (Exception e) {
            log.error("RedirectServlet getTrackingDataParams: ", e);
        }
        return mt;
    }
    
    private String getRedirectUrl(HttpServletRequest request){
        String url = null;
        String query = request.getQueryString();
        String typeParam = ConstantsBase.REDIRECT_PARAM_TYPE + "=" +  request.getParameter(ConstantsBase.REDIRECT_PARAM_TYPE) + "&";
        String redirectParams = query.replace(typeParam, "");
        String landindUrl = "";
        if(request.getParameter(ConstantsBase.REDIRECT_PARAM_TYPE).contains(ConstantsBase.REDIRECT_PARAM_TYPE_STATIC)){
            if (request.getRequestURL().toString().contains(AO_IT_DOMAIN)){
                landindUrl = getServletContext().getInitParameter("staticLPIT");
            } else if (request.getRequestURL().toString().contains(ET_DOMAIN)){
                landindUrl = getServletContext().getInitParameter("staticLPET");
            } else {
                landindUrl = getServletContext().getInitParameter("staticLPAO");
            }
            url = landindUrl + redirectParams;
        } else {
            if (request.getRequestURL().toString().contains(AO_IT_DOMAIN)){
                landindUrl = getServletContext().getInitParameter("dynamicLPIT");
            } else if (request.getRequestURL().toString().contains(ET_DOMAIN)){
                landindUrl = getServletContext().getInitParameter("dynamicLPET");
            } else {
                landindUrl = getServletContext().getInitParameter("dynamicLPAO");
            }
            url = landindUrl + redirectParams;
        }        
        return url;
    }
    
    private Cookie getCookie(HttpServletRequest request, String cookieName) {
        Cookie etsCookie = null;
        Cookie[] cs = request.getCookies();
        try {
            if (cs != null) {

                for (int i = 0; i < cs.length; i++) {
                    if (cs[i].getName().equals(cookieName)) {
                        etsCookie = cs[i];
                    }
                }
            }
        } catch (Exception e) {
            log.error("Get Ets Cookie Exc:", e);
        }
        return etsCookie;
    }

    private String checkEtsCookie(HttpServletRequest request, HttpServletResponse response){
        String param = null;
        
        Cookie etsCookie = getCookie(request, ConstantsBase.ETS_COOKIE_NAME);
        Cookie etsActivityCookie = getCookie(request, ConstantsBase.ETS_COOKIE_ACTIVITY_NAME);
        
        if (etsCookie != null && etsActivityCookie!= null){
            param = etsCookie.getValue();
            log.debug("Found ETS Cookie: " + param);
            log.debug("Found ETS Activity Cookie: " + etsActivityCookie.getValue());
            if(isNeedReplace(param, etsActivityCookie.getValue())){
                log.debug("ETS need replace: " + param);
                param = cerateEtsCookie(request, response);
            } else if (ets.getLastActivity() == ConstantsBase.MARKETING_TRACKING_FIRST_DEPOSIT) {
                param = HAVE_FD;                
            }
        } else {
            log.debug("Not Found ETS Cookie Begin Create: ");
            param = cerateEtsCookie(request, response);            
        }        
        return param;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        
        String mId = checkEtsCookie(request, response);
         try {
            if (mId == HAVE_FD){
                log.debug("Have First Deposit: " + mId);
            } else {
                log.debug("Try insert click for mid: " + mId);
                MarketingTrackingManager.insertMarketingClick(getTrackingDataParams(request, mId));   
            }
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            log.error("When insert click:  ", e);
        }
        String reUrl = getRedirectUrl(request);
        log.debug("Redirect to :" + reUrl);
        response.sendRedirect(reUrl);
    }
}