package com.anyoption.ets.servlets;

import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.MarketingCampCombinationRequestedObj;
import com.anyoption.ets.managers.MarketingCampaignManager;
import com.anyoption.ets.util.ConstantsBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class MarketingCampaignCombinationServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(MarketingCampaignCombinationServlet.class);
    
    private MarketingCampCombinationRequestedObj getRequestParam(HttpServletRequest request){
        MarketingCampCombinationRequestedObj mcc = new MarketingCampCombinationRequestedObj();
        try {
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }

            String body = stringBuilder.toString();        
            log.debug("In Params: " + body);            
            
            //JSON
            JsonElement jelement = new JsonParser().parse(body);;
            JsonObject jobject = jelement.getAsJsonObject();
            log.debug("jobject  " + jobject);              
            Gson gson = new GsonBuilder().create();
            mcc = gson.fromJson(jobject, MarketingCampCombinationRequestedObj.class);            
        } catch (Exception e) {
            log.error("When getRequestParam :", e);
        }
        
        return mcc;
    }
        
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug( "Do Post Marketing Campaign");
        MarketingCampCombinationRequestedObj mcc = null;        
        try {
            mcc = getRequestParam(request);
            if (mcc.getRequestedType() == ConstantsBase.MARKETING_CAMPAIGN_REQUEST_TYPE){
                // Campaign
                if (mcc.getActionType() == ConstantsBase.MARKETING_CAMPAIGN_COMBINATION_INSERT){
                    MarketingCampaignManager.insertMarketingCampaign(mcc);
                } else {
                    MarketingCampaignManager.updateMarketingCampaign(mcc);
                }
            } else {
                // Combination
                if (mcc.getActionType() == ConstantsBase.MARKETING_CAMPAIGN_COMBINATION_INSERT){
                    MarketingCampaignManager.insertMarketingCombination(mcc);
                } else {
                    MarketingCampaignManager.updateMarketingCombination(mcc);
                }
            }
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            log.error("getMarketingParams:", e);
        }        
        response.setStatus(HttpServletResponse.SC_OK);
    }
}