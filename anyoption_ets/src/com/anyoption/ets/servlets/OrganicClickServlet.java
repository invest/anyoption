package com.anyoption.ets.servlets;

import java.net.URLDecoder;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.MarketingTrackingParam;
import com.anyoption.ets.managers.MarketingTrackingManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class OrganicClickServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(OrganicClickServlet.class);
    
    private MarketingTrackingParam getRequestParam(HttpServletRequest request){
        MarketingTrackingParam mro = new MarketingTrackingParam();
        try {
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }
            String body = stringBuilder.toString();        
            log.debug("In Params: " + body);            
            
            //JSON
            JsonElement jelement = new JsonParser().parse(body);;
            JsonObject jobject = jelement.getAsJsonObject();
            log.debug("jobject  " + jobject);              
            Gson gson = new GsonBuilder().create();
            mro = gson.fromJson(jobject, MarketingTrackingParam.class);  
            
            if (mro.getHttpReferer() != null){
                mro.setHttpReferer(URLDecoder.decode(mro.getHttpReferer(), "UTF-8"));
            }
        } catch (Exception e) {
            log.error("When getRequestParam :", e);
        }                        
        return mro;
    }
        
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug( "Do Post Organick Servlet");
        try {            
            MarketingTrackingManager.insertMarketingOrganicClik(getRequestParam(request));
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            log.error("getMarketingParams:", e);
        }        
    }
}