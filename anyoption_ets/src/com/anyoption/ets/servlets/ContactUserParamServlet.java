package com.anyoption.ets.servlets;

import java.io.PrintWriter;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.MarketingRequestedObj;
import com.anyoption.ets.beans.MarketingTrackingParamBase;
import com.anyoption.ets.managers.MarketingTrackingManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ContactUserParamServlet extends HttpServlet {

    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(ContactUserParamServlet.class);

    private MarketingRequestedObj getRequestParam(HttpServletRequest request) {
        MarketingRequestedObj mro = new MarketingRequestedObj();
        try {
            StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(request.getInputStream());
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }
            String body = stringBuilder.toString();
            log.debug("In Params: " + body);

            // JSON
            JsonElement jelement = new JsonParser().parse(body);            
            JsonObject jobject = jelement.getAsJsonObject();
            log.debug("jobject  " + jobject);
            Gson gson = new GsonBuilder().create();
            mro = gson.fromJson(jobject, MarketingRequestedObj.class);
        } catch (Exception e) {
            log.error("When getRequestParam :", e);
        }
        return mro;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug("Do Post ContactUser Servlet");
        MarketingTrackingParamBase mtb = null;
        try {
            mtb = MarketingTrackingManager.getMarketingParams(getRequestParam(request));
            Gson gson = new Gson();
            String json = gson.toJson(mtb);
            log.debug("Send JSON:" + json);
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(json);
            out.flush();
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            log.error("When post:", e);
        }
    }
}