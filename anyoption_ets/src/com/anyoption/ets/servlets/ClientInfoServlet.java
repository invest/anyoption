package com.anyoption.ets.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.ets.beans.ClientInfo;
import com.anyoption.ets.managers.MarketingTrackingManager;


public class ClientInfoServlet extends HttpServlet {

    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(ClientInfoServlet.class);
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug("Client Info Servlet doGet");
        try {
            ClientInfo ci = new ClientInfo();
            ci.setEtsCookie(request.getParameter("mid"));
            ci.setPlatform(request.getParameter("platform"));
            ci.setUserAgent(request.getParameter("useragent"));
            ci.setUserResolution(request.getParameter("userresolution"));
            MarketingTrackingManager.insertClientInfo(ci);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            log.error("Clinet Info:", e);
        }
    }      
}