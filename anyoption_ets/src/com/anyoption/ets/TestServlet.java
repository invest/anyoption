package com.anyoption.ets;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.anyoption.ets.managers.MarketingTrackingManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class TestServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -6233831873948879567L;
    private static final Logger log = Logger.getLogger(TestServlet.class);

    public void init() throws ServletException {
        log.debug("TestServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        log.debug("TestServlet doGet");
        response.setContentType("text/plain");
        String postGreVer = "Can't conn to PostgreSQL!!!";
        try {
             postGreVer = MarketingTrackingManager.testConn();
        } catch (SQLException e) {
            
            log.error("Can't get sql conn: ", e);
        }
        response.getWriter().println( "Test servlet. SQL: " + postGreVer);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
      
        log.debug("TestServlet doPo st Testov"); 
        StringBuilder stringBuilder = new StringBuilder(1000);
        Scanner scanner = new Scanner(request.getInputStream());
        while (scanner.hasNextLine()) {
            stringBuilder.append(scanner.nextLine());
        }

        String body = stringBuilder.toString();        
        log.debug("In Params: " + body);
        
        
        //JSON
        JsonElement jelement = new JsonParser().parse(body);
        log.debug( "reson se jsonObject " + jelement);
        JsonObject jobject = jelement.getAsJsonObject();
        log.debug("resonse job ject " + jobject);  
        log.debug("MID: " +  jobject.get("mId"));
        
        
        response.setContentType("text/plain");
     // Get the printwriter object from response to write the required json object to the output stream      
        PrintWriter out = response.getWriter();
     // Assuming your json object is **jsonObject**, perform the following, it will return your json object  
        out.print("alabala");
        out.flush();
    }
    
    public void destroy() {
        log.debug("TestServlet destroy"); 
    }
}