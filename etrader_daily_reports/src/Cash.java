/*public class Cash {

	private double depositCCAmount;

	private long depositCCCount;

	private double depositOtherAmount;

	private long depositOtherCount;

	private double depositDomesticAmount;

	private long depositDomesticCount;

	private double depositAdminAmount;

	private long depositAdminCount;

	private double depositBonusAmount;

	private long depositBonusCount;

	private double pointsToCashAmount;

	private long pointsToCashCount;

	private double withdrawBonusAmount;

	private long withdrawBonusCount;

	private double withdrawAdminAmount;

	private long withdrawAdminCount;

	private double reverseWithdrawsAmount;

	private long reverseWithdrawsCount;

	private double executedWithdrawsAmount;

	private long executedWithdrawsCount;

	private double requestedWithdrawsAmount;

	private long requestedWithdrawsCount;

	private double netInOut;

	private long headCount;

	private String currency;

	private double tranCanceledFraudsAmount;

	private long tranCanceledFraudsCount;

	private double fixBalanceDepositAmount;

	private long fixBalanceDepositCount;

	private double fixBalanceWithdrawalAmount;

	private long fixBalanceWithdrawalCount;

	private double companyDepositAmount;

	private long companyDepositCount;

	private double depositPaypalAmount;

	private long depositPaypalCount;

	private double withdrawalPaypalAmount;

	private long withdrawalPaypalCount;

	private double depositACHAmount;

	private long depositACHCount;

	private double depositCashUAmount;

	private long depositCashUCount;

	private double depositUkashAmount;

	private long depositUkashCount;

	private double depositMoneybookersAmount;

	private long depositMoneybookersCount;

	public Cash() {

	}

	public double getDepositCCAmount() {
		return depositCCAmount;
	}

	public String getDepositCCAmountStr() {
		return Utils.displayAmount(depositCCAmount);
	}

	public void setDepositCCAmount(double depositCCAmount) {
		this.depositCCAmount = depositCCAmount;
	}

	public long getDepositCCCount() {
		return depositCCCount;
	}

	public void setDepositCCCount(long depositCCCount) {
		this.depositCCCount = depositCCCount;
	}

	public double getDepositOtherAmount() {
		return depositOtherAmount;
	}

	public String getDepositOtherAmountStr() {
		return Utils.displayAmount(depositOtherAmount);
	}

	public void setDepositOtherAmount(double depositOtherAmount) {
		this.depositOtherAmount = depositOtherAmount;
	}

	public long getDepositOtherCount() {
		return depositOtherCount;
	}

	public void setDepositOtherCount(long depositOtherCount) {
		this.depositOtherCount = depositOtherCount;
	}

	public double getDepositAdminAmount() {
		return depositAdminAmount;
	}

	public String getDepositAdminAmountStr() {
		return Utils.displayAmount(depositAdminAmount);
	}

	public void setDepositAdminAmount(double depositAdminAmount) {
		this.depositAdminAmount = depositAdminAmount;
	}

	public long getDepositAdminCount() {
		return depositAdminCount;
	}

	public void setDepositAdminCount(long depositAdminCount) {
		this.depositAdminCount = depositAdminCount;
	}

	public double getDepositBonusAmount() {
		return depositBonusAmount;
	}

	public String getDepositBonusAmountStr() {
		return Utils.displayAmount(depositBonusAmount);
	}

	public void setDepositBonusAmount(double depositBonusAmount) {
		this.depositBonusAmount = depositBonusAmount;
	}

	public long getDepositBonusCount() {
		return depositBonusCount;
	}

	public void setDepositBonusCount(long depositBonusCount) {
		this.depositBonusCount = depositBonusCount;
	}


	public double getWithrawAdminAmount() {
		return withdrawAdminAmount;
	}

	public String getWithdrawAdminAmountStr() {
		return Utils.displayAmount(withdrawAdminAmount);
	}

	public void setWithdrawAdminAmount(double withdrawAdminAmount) {
		this.withdrawAdminAmount = withdrawAdminAmount;
	}

	public long getWithdrawAdminCount() {
		return withdrawAdminCount;
	}

	public void setWithdrawAdminCount(long withdrawAdminCount) {
		this.withdrawAdminCount = withdrawAdminCount;
	}


	public double getWithrawBonusAmount() {
		return withdrawBonusAmount;
	}

	public String getWithdrawBonusAmountStr() {
		return Utils.displayAmount(withdrawBonusAmount);
	}

	public void setWithdrawBonusAmount(double withdrawBonusAmount) {
		this.withdrawBonusAmount = withdrawBonusAmount;
	}

	public long getWithdrawBonusCount() {
		return withdrawBonusCount;
	}

	public void setWithdrawBonusCount(long withdrawBonusCount) {
		this.withdrawBonusCount = withdrawBonusCount;
	}

	public double getExecutedWithdrawsAmount() {
		return executedWithdrawsAmount;
	}

	public String getExecutedWithdrawsAmountStr() {
		return Utils.displayAmount(executedWithdrawsAmount);
	}

	public void setExecutedWithdrawsAmount(double executedWithdrawsAmount) {
		this.executedWithdrawsAmount = executedWithdrawsAmount;
	}

	public long getExecutedWithdrawsCount() {
		return executedWithdrawsCount;
	}

	public void setExecutedWithdrawsCount(long executedWithdrawsCount) {
		this.executedWithdrawsCount = executedWithdrawsCount;
	}

	public long getHeadCount() {
		return headCount;
	}

	public void setHeadCount(long headCount) {
		this.headCount = headCount;
	}

	public double getNetInOut() {
		return netInOut;
	}

	public String getNetInOutStr() {
		return Utils.displayAmount(netInOut);
	}

	public void setNetInOut() {
		this.netInOut = this.depositCCAmount + this.depositOtherAmount + this.depositAdminAmount
				- this.executedWithdrawsAmount - this.withdrawAdminAmount;
	}

	public double getRequestedWithdrawsAmount() {
		return requestedWithdrawsAmount;
	}

	public String getRequestedWithdrawsAmountStr() {
		return Utils.displayAmount(requestedWithdrawsAmount);
	}

	public void setRequestedWithdrawsAmount(double requestedWithdrawsAmount) {
		this.requestedWithdrawsAmount = requestedWithdrawsAmount;
	}

	public long getRequestedWithdrawsCount() {
		return requestedWithdrawsCount;
	}

	public void setRequestedWithdrawsCount(long requestedWithdrawsCount) {
		this.requestedWithdrawsCount = requestedWithdrawsCount;
	}

	public double getReverseWithdrawsAmount() {
		return reverseWithdrawsAmount;
	}

	public void setReverseWithdrawsAmount(double reverseWithdrawsAmount) {
		this.reverseWithdrawsAmount = reverseWithdrawsAmount;
	}

	public String getReverseWithdrawsAmountStr() {
		return Utils.displayAmount(reverseWithdrawsAmount);
	}

	public long getReverseWithdrawsCount() {
		return reverseWithdrawsCount;
	}

	public void setReverseWithdrawsCount(long reverseWithdrawsCount) {
		this.reverseWithdrawsCount = reverseWithdrawsCount;
	}

	*//**
	 * @return the currency
	 *//*
	public String getCurrency() {
		return currency;
	}

	*//**
	 * @param currency the currency to set
	 *//*
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	*//**
	 * @return the pointsToCashAmount
	 *//*
	public double getPointsToCashAmount() {
		return pointsToCashAmount;
	}

	*//**
	 * @param pointsToCashAmount the pointsToCashAmount to set
	 *//*
	public void setPointsToCashAmount(double pointsToCashAmount) {
		this.pointsToCashAmount = pointsToCashAmount;
	}

	*//**
	 * @return the pointsToCashCount
	 *//*
	public long getPointsToCashCount() {
		return pointsToCashCount;
	}

	*//**
	 * @param pointsToCashCount the pointsToCashCount to set
	 *//*
	public void setPointsToCashCount(long pointsToCashCount) {
		this.pointsToCashCount = pointsToCashCount;
	}

	*//**
	 * @return the pointsToCashAmountStr
	 *//*
	public String getPointsToCashAmountStr() {
		return Utils.displayAmount(pointsToCashAmount);
	}

	*//**
	 * @return the tranCanceledFraudsAmount
	 *//*
	public double getTranCanceledFraudsAmount() {
		return tranCanceledFraudsAmount;
	}

	*//**
	 * @param tranCanceledFraudsAmount the tranCanceledFraudsAmount to set
	 *//*
	public void setTranCanceledFraudsAmount(double tranCanceledFraudsAmount) {
		this.tranCanceledFraudsAmount = tranCanceledFraudsAmount;
	}

	*//**
	 * @return the tranCanceledFraudsCount
	 *//*
	public long getTranCanceledFraudsCount() {
		return tranCanceledFraudsCount;
	}

	*//**
	 * @param tranCanceledFraudsCount the tranCanceledFraudsCount to set
	 *//*
	public void setTranCanceledFraudsCount(long tranCanceledFraudsCount) {
		this.tranCanceledFraudsCount = tranCanceledFraudsCount;
	}

	public String getTranCanceledFraudsAmountStr() {
		return Utils.displayAmount(tranCanceledFraudsAmount);
	}

	*//**
	 * @return the companyDepositAmount
	 *//*
	public double getCompanyDepositAmount() {
		return companyDepositAmount;
	}

	*//**
	 * @param companyDepositAmount the companyDepositAmount to set
	 *//*
	public void setCompanyDepositAmount(double companyDepositAmount) {
		this.companyDepositAmount = companyDepositAmount;
	}

	*//**
	 * @return the companyDepositCount
	 *//*
	public long getCompanyDepositCount() {
		return companyDepositCount;
	}

	*//**
	 * @param companyDepositCount the companyDepositCount to set
	 *//*
	public void setCompanyDepositCount(long companyDepositCount) {
		this.companyDepositCount = companyDepositCount;
	}

	*//**
	 * @return the fixBalanceDepositAmount
	 *//*
	public double getFixBalanceDepositAmount() {
		return fixBalanceDepositAmount;
	}

	*//**
	 * @param fixBalanceDepositAmount the fixBalanceDepositAmount to set
	 *//*
	public void setFixBalanceDepositAmount(double fixBalanceDepositAmount) {
		this.fixBalanceDepositAmount = fixBalanceDepositAmount;
	}

	*//**
	 * @return the fixBalanceDepositCount
	 *//*
	public long getFixBalanceDepositCount() {
		return fixBalanceDepositCount;
	}

	*//**
	 * @param fixBalanceDepositCount the fixBalanceDepositCount to set
	 *//*
	public void setFixBalanceDepositCount(long fixBalanceDepositCount) {
		this.fixBalanceDepositCount = fixBalanceDepositCount;
	}

	*//**
	 * @return the fixBalanceWithdrawalAmount
	 *//*
	public double getFixBalanceWithdrawalAmount() {
		return fixBalanceWithdrawalAmount;
	}

	*//**
	 * @param fixBalanceWithdrawalAmount the fixBalanceWithdrawalAmount to set
	 *//*
	public void setFixBalanceWithdrawalAmount(double fixBalanceWithdrawalAmount) {
		this.fixBalanceWithdrawalAmount = fixBalanceWithdrawalAmount;
	}

	*//**
	 * @return the fixBalanceWithdrawalCount
	 *//*
	public long getFixBalanceWithdrawalCount() {
		return fixBalanceWithdrawalCount;
	}

	*//**
	 * @param fixBalanceWithdrawalCount the fixBalanceWithdrawalCount to set
	 *//*
	public void setFixBalanceWithdrawalCount(long fixBalanceWithdrawalCount) {
		this.fixBalanceWithdrawalCount = fixBalanceWithdrawalCount;
	}


	public String getFixBalanceDepositAmountStr() {
		return Utils.displayAmount(fixBalanceDepositAmount);
	}

	public String getFixBalanceWithdrawalAmountStr() {
		return Utils.displayAmount(fixBalanceWithdrawalAmount);
	}

	public String getCompanyDepositAmountStr() {
		return Utils.displayAmount(companyDepositAmount);
	}

	*//**
	 * @return the depositDomesticAmount
	 *//*
	public double getDepositDomesticAmount() {
		return depositDomesticAmount;
	}

	*//**
	 * @param depositDomesticAmount the depositDomesticAmount to set
	 *//*
	public void setDepositDomesticAmount(double depositDomesticAmount) {
		this.depositDomesticAmount = depositDomesticAmount;
	}

	*//**
	 * @return the depositDomesticCount
	 *//*
	public long getDepositDomesticCount() {
		return depositDomesticCount;
	}

	*//**
	 * @param depositDomesticCount the depositDomesticCount to set
	 *//*
	public void setDepositDomesticCount(long depositDomesticCount) {
		this.depositDomesticCount = depositDomesticCount;
	}

	public String getDepositDomesticAmountStr() {
		return Utils.displayAmount(depositDomesticAmount);
	}

	*//**
	 * @return the paypalAmount
	 *//*
	public double getDepositPaypalAmount() {
		return depositPaypalAmount;
	}

	*//**
	 * @param paypalAmount the paypalAmount to set
	 *//*
	public void setDepositPaypalAmount(double depositPaypalAmount) {
		this.depositPaypalAmount = depositPaypalAmount;
	}

	*//**
	 * @return the paypalCount
	 *//*
	public long getDepositPaypalCount() {
		return depositPaypalCount;
	}

	*//**
	 * @param paypalCount the paypalCount to set
	 *//*
	public void setDepositPaypalCount(long depositPaypalCount) {
		this.depositPaypalCount = depositPaypalCount;
	}

	public String getDepositPaypalAmountStr() {
		return Utils.displayAmount(depositPaypalAmount);
	}

	*//**
	 * @return the withdrawalPaypalAmount
	 *//*
	public double getWithdrawalPaypalAmount() {
		return withdrawalPaypalAmount;
	}

	*//**
	 * @param withdrawalPaypalAmount the withdrawalPaypalAmount to set
	 *//*
	public void setWithdrawalPaypalAmount(double withdrawalPaypalAmount) {
		this.withdrawalPaypalAmount = withdrawalPaypalAmount;
	}

	*//**
	 * @return the withdrawalPaypalCount
	 *//*
	public long getWithdrawalPaypalCount() {
		return withdrawalPaypalCount;
	}

	*//**
	 * @param withdrawalPaypalCount the withdrawalPaypalCount to set
	 *//*
	public void setWithdrawalPaypalCount(long withdrawalPaypalCount) {
		this.withdrawalPaypalCount = withdrawalPaypalCount;
	}

	public String getWithdrawalPaypalAmountStr() {
		return Utils.displayAmount(withdrawalPaypalAmount);
	}

	public double getDepositACHAmount() {
		return depositACHAmount;
	}

	public void setDepositACHAmount(double depositACHAmount) {
		this.depositACHAmount = depositACHAmount;
	}

	public long getDepositACHCount() {
		return depositACHCount;
	}

	public void setDepositACHCount(long depositACHCount) {
		this.depositACHCount = depositACHCount;
	}

	public String getDepositACHAmountStr() {
		return Utils.displayAmount(depositACHAmount);
	}

	public double getDepositCashUAmount() {
		return depositCashUAmount;
	}

	public void setDepositCashUAmount(double depositCashUAmount) {
		this.depositCashUAmount = depositCashUAmount;
	}

	public long getDepositCashUCount() {
		return depositCashUCount;
	}

	public void setDepositCashUCount(long depositCashUCount) {
		this.depositCashUCount = depositCashUCount;
	}

	public String getDepositCashUAmountStr() {
		return Utils.displayAmount(depositCashUAmount);
	}

	*//**
	 * @return the depositUkashAmount
	 *//*
	public double getDepositUkashAmount() {
		return depositUkashAmount;
	}

	*//**
	 * @param depositUkashAmount the depositUkashAmount to set
	 *//*
	public void setDepositUkashAmount(double depositUkashAmount) {
		this.depositUkashAmount = depositUkashAmount;
	}

	*//**
	 * @return the depositUkashCount
	 *//*
	public long getDepositUkashCount() {
		return depositUkashCount;
	}

	*//**
	 * @param depositUkashCount the depositUkashCount to set
	 *//*
	public void setDepositUkashCount(long depositUkashCount) {
		this.depositUkashCount = depositUkashCount;
	}

	public String getDepositUkashAmountStr() {
		return Utils.displayAmount(depositUkashAmount);
	}

	*//**
	 * @return the depositMoneybookersAmount
	 *//*
	public double getDepositMoneybookersAmount() {
		return depositMoneybookersAmount;
	}

	*//**
	 * @param depositMoneybookersAmount the depositMoneybookersAmount to set
	 *//*
	public void setDepositMoneybookersAmount(double depositMoneybookersAmount) {
		this.depositMoneybookersAmount = depositMoneybookersAmount;
	}

	*//**
	 * @return the depositMoneybookersCount
	 *//*
	public long getDepositMoneybookersCount() {
		return depositMoneybookersCount;
	}

	*//**
	 * @param depositMoneybookersCount the depositMoneybookersCount to set
	 *//*
	public void setDepositMoneybookersCount(long depositMoneybookersCount) {
		this.depositMoneybookersCount = depositMoneybookersCount;
	}

	public String getDepositMoneybookersAmountStr() {
		return Utils.displayAmount(depositMoneybookersAmount);
	}

}
*/