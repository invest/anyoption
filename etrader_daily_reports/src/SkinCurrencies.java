/*

import java.util.ArrayList;
import java.util.HashMap;

*//**
 * SkinCurrencies vo class
 * @author Kobi
 *
 *//*
public class SkinCurrencies implements java.io.Serializable {

	private long currencyId;
	private String code;

    private Cash cash;
    private Investments invests;
    private Other other;
    private ArrayList<MarketingCampaignDr> campaignList = new ArrayList<MarketingCampaignDr>();
    private HashMap<Long, Market> marktes = new HashMap<Long, Market>();
    private ArrayList<Market> marketsList;    // after sorting

	public SkinCurrencies() {
		code = "";
	}

	*//**
	 * @return the code
	 *//*
	public String getCode() {
		return code;
	}

	*//**
	 * @param code the code to set
	 *//*
	public void setCode(String code) {
		this.code = code;
	}

	*//**
	 * @return the currencyId
	 *//*
	public long getCurrencyId() {
		return currencyId;
	}

	*//**
	 * @param currencyId the currencyId to set
	 *//*
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	*//**
	 * @return the adList
	 *//*
	public ArrayList<MarketingCampaignDr> getCampaignList() {
		return campaignList;
	}

	*//**
	 * @param adList the adList to set
	 *//*
	public void setCampaignList(ArrayList<MarketingCampaignDr> campaignList) {
		this.campaignList = campaignList;
	}

	*//**
	 * @return the cash
	 *//*
	public Cash getCash() {
		return cash;
	}

	*//**
	 * @param cash the cash to set
	 *//*
	public void setCash(Cash cash) {
		this.cash = cash;
	}

	*//**
	 * @return the invests
	 *//*
	public Investments getInvests() {
		return invests;
	}

	*//**
	 * @param invests the invests to set
	 *//*
	public void setInvests(Investments invests) {
		this.invests = invests;
	}

	*//**
	 * @return the other
	 *//*
	public Other getOther() {
		return other;
	}

	*//**
	 * @param other the other to set
	 *//*
	public void setOther(Other other) {
		this.other = other;
	}

	*//**
	 * @return the marktes
	 *//*
	public HashMap<Long, Market> getMarktes() {
		return marktes;
	}

	*//**
	 * @param marktes the marktes to set
	 *//*
	public void setMarktes(HashMap<Long, Market> marktes) {
		this.marktes = marktes;
	}

	*//**
	 * @return the marketsList
	 *//*
	public ArrayList<Market> getMarketsList() {
		return marketsList;
	}

	*//**
	 * @param marketsList the marketsList to set
	 *//*
	public void setMarketsList(ArrayList<Market> marketsList) {
		this.marketsList = marketsList;
	}



}
*/