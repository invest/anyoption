/*import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.bl_vos.Opportunity;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.Encryptor;


public class DailyRiskReportJob {

	private static Logger log = Logger.getLogger(DailyRiskReportJob.class);

	private static Template emailTemplate;

	String path = "";

	// The server properties
	private static Hashtable<String, String> serverProperties;

	// The specific email Properties
	private static Hashtable<String, String> emailProperties;

	public static final String PARAM_EMAIL = "email";

	private static HashMap<Long, Market> marktes = new HashMap<Long, Market>();

	// Constants for running type
	private static final int RUN_TYPE_ALL_SKINS = 0;
	private static final int RUN_TYPE_SKIN_ETRADER = 1;
	private static final int RUN_TYPE_AO_SKINS = 2;

	// Constants for date range type
	private static final int RUN_DAILY = 0;
	private static final int RUN_WEEKLY = 1;
	private static final int RUN_MONTHLY = 2;

	private static final int X_DIFF_CC_DEPOSITS_ATTEMPTS = 3;

	private static String SECURITY_MAILING_LIST = "emails.security.check";

	public static void main(String[] args) throws Exception {

		if (null == args || args.length < 1) {
			log.log(Level.FATAL,"Configuration file not specified, insert properties file name.");
			return;
		}

		Utils.propFile = args[0];

		DateRange dateRange = new DateRange();

		int runType = RUN_TYPE_SKIN_ETRADER;

		// take run type property
		try {
			runType = Integer.parseInt(args[1]);
		}  catch ( Exception e ) {
			log.warn("wrong property of run type, must be number (0-2). take etrader run type");
		}

//		if (args.length >= 3) {  // date range reports
//			dateRange = getDatesFromCommandLine();
//			calculateAndSendDateRangeReport(dateRange, runType);
//			calculateAndSendRangeReportMarkteBreakDown(dateRange, runType);
//			if ( args.length >= 4) {
//				if(args[3].equals("true") ) {  // skins partitions (skin/currency)
//					calculateAndSendDateRangeReportSkinsPartition(dateRange);
//					calculateAndSendRangeReportMarkteBreakDownSkinsPartition(dateRange);
//				}
//			}
//			return;
//		}
//
//		Calendar cal = Calendar.getInstance();
//
//		int weekDay  = cal.get(Calendar.DAY_OF_WEEK);
//		int monthDay  = cal.get(Calendar.DAY_OF_MONTH);
//
//		if (!Utils.IsParameterEmptyOrNull(Utils.getProperty("run.weekly"))){
//			weekDay = Calendar.SUNDAY;
//		}
//
//		// Since this report runs for both skins, only activate when run type is AO
//		// to prevent from sending 2 mails.
//		if (runType != RUN_TYPE_SKIN_ETRADER ){
//
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Starting Daily Managers Report.");
//			}
//
//			calculateAndSendBaseReport(RUN_DAILY);
//
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Daily Managers Report completed.");
//			}
//
//			if (weekDay == Calendar.SUNDAY){
//
//				if (log.isEnabledFor(Level.INFO)) {
//					log.log(Level.INFO,"Starting Weekly Managers Report.");
//				}
//
//				calculateAndSendBaseReport(RUN_WEEKLY);
//
//				if (log.isEnabledFor(Level.INFO)) {
//					log.log(Level.INFO,"Weekly Report completed.");
//				}
//			}
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Daily Report Market Break Down.");
//		}
//		calculateAndSendDailyReportMarkteBreakDown(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Daily Report Market Break Down completed.");
//		}
//
//		if ( runType != RUN_TYPE_AO_SKINS ) {
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Starting the Daily Report Need Files.");
//			}
//
//			SendDailyReportNeedFiles();  // etrader report
//
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Daily Report Need Files completed.");
//			}
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Non Depositros Report.");
//		}
//
//		SendNonDepositors(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Non Depositros report completed.");
//		}
//
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Non Depositros 20 days Report.");
//		}
//
//		SendNonDepositors20Days(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Non Depositros 20 days  report completed.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the users with duplicate email Report.");
//		}
//
//    	SendDailyReportDupicateEmail(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"users with duplicate email report completed.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the users with duplicate ip Report.");
//		}

//    	SendDailyReportDupicateIp(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"users with duplicate ip report completed.");
//		}

//     Disabled for Anna's request
//		--------------------------
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting : transactions with duplicate Ip's Report.");
//		}
//
//	   SendDailyReportTrxDuplicateIP(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Finished : transactions with duplicate Ip's Report.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting : investments with duplicate Ip's Report.");
//		}
//
//		SendDailyReportInvsDuplicateIP(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Finished : investments with duplicate Ip's Report.");
//		}

//		if ( runType != RUN_TYPE_SKIN_ETRADER ) {
//
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Starting : different names report.");
//			}
//
//			SendDailyReportDifferentNames(runType);
//
//			if (log.isEnabledFor(Level.INFO)) {
//				log.log(Level.INFO,"Finished : different names report.");
//			}
//		}
//
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : equal passwords report.");
		}

		calcAndSendEqualPasswordsReport(runType);

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : equal passwords report.");
		}

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : different ip country report.");
		}

		differentIpCountryReport(runType);

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : different ip country report.");
		}


		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : different cc country report.");
		}

		differentCcCountryReport(runType);

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : different cc country report.");
		}
//
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : depositors With X Different Cc.");
		}

		depositorsWithMoreThanXDiffCcReport(runType);

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : depositors With X Different Cc.");
		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting : users with same phone.");
//		}

		SendDailyReportDupicatePhones(runType);

//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Finished : users with same phone.");
//		}
//
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting : SendCcDepositsStatusReport.");
//		}
//
//		SendCcDepositsStatusReport(runType, RUN_DAILY);
//
//		if (weekDay == Calendar.SUNDAY){
//			SendCcDepositsStatusReport(runType, RUN_WEEKLY);
//		}
//
//		if (monthDay == 1){
//			SendCcDepositsStatusReport(runType, RUN_MONTHLY);
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Finished : SendCcDepositsStatusReport.");
//		}
}


	private static void SendNonDepositors(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
		// set currency for display
		Other other = new Other();
		other.setCurrency(getCurrencyByRunType(runType));

		try {

			conn = Utils.getConnection();
			String sqlNeedFiles =
								"select u.id as id,u.USER_NAME as username,s.display_name as skin from users u,skins s " +
								"where u.TIME_CREATED > trunc(sysdate - 3) " +
								"and u.TIME_CREATED < trunc(sysdate - 2 ) " +
								"and u.skin_id = s.id " +
								"and u.CLASS_ID <> 0 "+
								getSqlByRunType(runType) +
								"and u.ID not in ( "+
													"select u1.id "+
													"from users u1,transactions t , transaction_types tt " +
													"where t.user_id = u1.id " +
													"and t.type_id = tt.id " +
													"and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
													"and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
													"and u1.CLASS_ID <> 0 " +
													getSqlByRunType(runType) +
													"and t.TIME_CREATED > trunc(sysdate -3)" +
													" )";

		pstmt = conn.prepareStatement(sqlNeedFiles);

		rs = pstmt.executeQuery();
		//String skin = new String();
		while(rs.next()) {
			NeedFiles needFiles = new NeedFiles();
			needFiles.setAccountNumber(rs.getString("id"));
			needFiles.setSumDeposits(0);
			needFiles.setUsername(rs.getString("username"));
			needFiles.setSkin(Utils.getProperty(rs.getString("skin")));
			needFilesArr.add(needFiles);

		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get non depositors depositors " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailNonDepositors(needFilesArr, other, getPageHeaderByRunType(runType), runType);

		log.log(Level.DEBUG,"finished to send emails");


	}

	private static void SendNonDepositors20Days(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
		// set currency for display
		Other other = new Other();
		other.setCurrency(getCurrencyByRunType(runType));

		try {
			conn = Utils.getConnection();
			String sql20DaysNonDeposits =
										"select distinct U.id, u.USER_NAME  as username,s.display_name as skin "+
										"from users u , transactions t , transaction_types tt,skins s "+
										"where t.user_id = u.id  "+
										"and t.type_id = tt.id  "+
										"and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "+
										"and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "+
										"and u.skin_id=s.id " +
										"and t.TYPE_ID not in (6,12) "+
										"and u.CLASS_ID <> 0 "+
										"and u.BALANCE < 10000 "+
										getSqlByRunType(runType) +
										"and trunc(t.time_created) <= trunc(sysdate - 20) "+
										"and ( "+
												"select count(distinct t2.id) "+
												"from users u2 , transactions t2 , transaction_types tt2 "+
												"where t2.user_id = u2.id "+
												"and t2.type_id = tt2.id "+
												"and tt2.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "+
												"and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "+
												"and t2.TYPE_ID not in (6,12) "+
												"and u2.CLASS_ID <> 0 "+
												"and u2.id = u.id " +
										") = 1 " +
										"and ( " +
												"select MAX(i.time_created) from investments i "+
												"where i.user_id = u.id " +
										") <= trunc(sysdate - 15) " +
										"and ( " +
												"select count(distinct iss.id) " +
												"from issues iss  " +
												"where iss.user_id = u.id " +
												"and iss.subject_id = 9" +
										") = 0 ";


		pstmt = conn.prepareStatement(sql20DaysNonDeposits);

		rs = pstmt.executeQuery();

		while(rs.next()) {
			NeedFiles needFiles = new NeedFiles();
			needFiles.setAccountNumber(rs.getString("id"));
			needFiles.setSumDeposits(0);
			needFiles.setUsername(rs.getString("username"));
			needFiles.setSkin(Utils.getProperty(rs.getString("skin")));
			needFilesArr.add(needFiles);
		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get non depositors depositors 20 days" + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailNonDepositors20Days(needFilesArr, other, getPageHeaderByRunType(runType), runType);

		log.log(Level.DEBUG,"finished to send emails");


	}

	private static void calculateAndSendDateRangeReport(DateRange dr, int runType) {
		Cash cash = new Cash();
		cash.setCurrency(getCurrencyByRunType(runType));

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		//  Deposits by Credit cards

		try {
			conn = Utils.getConnection();

			String sqlDepositCC =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t,users u "
							+ "where t.USER_ID = u.ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 1 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlDepositCC += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositCC);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositCCAmount(rs.getDouble("amount"));
				cash.setDepositCCCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits CC "+dr.getDateRange());
		}

		// Deposits by other then Credit cards

		try {
			String sqlDepositOther =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u, transaction_types tt "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = tt.id "
							+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "  // real deposits
							+ "and t.type_id not in (1,17,18,21,25) "     // without credit card
							+ "and t.status_id in ( " + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlDepositOther += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositOther);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositOtherAmount(rs.getDouble("amount"));
				cash.setDepositOtherCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits other " +dr.getDateRange() + "  " + e);
		}

		// Deposits by domestic

		try {
			String sqlDepositDomestic =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id in (17,18,21) " // domestic
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";

			sqlDepositDomestic += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositDomestic);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositDomesticAmount(rs.getDouble("amount"));
				cash.setDepositDomesticCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits other " + e);
		}

		// Deposits by Admin

		try {
			String sqlDepositAdmin =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 4 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlDepositAdmin += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositAdmin);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositAdminAmount(rs.getDouble("amount"));
				cash.setDepositAdminCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits admin " +dr.getDateRange() + "  " + e);
		}

		//	Deposits bonus

		try {
			String sqlDepositBonus =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 12 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlDepositBonus += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositBonus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositBonusAmount(rs.getDouble("amount"));
				cash.setDepositBonusCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits Bonus " +dr.getDateRange() + "  " + e);
		}

		//	points to cash deposit
		try {
			String sqlpointsToCash =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 20 "
							+ "and t.status_id in (2) "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlpointsToCash += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlpointsToCash);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setPointsToCashAmount(rs.getDouble("amount"));
				cash.setPointsToCashCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get points to Cash deposit " +dr.getDateRange() + "  " + e);
		}

		// withdraws bonus

		try {
			String sqlWithdrawBonus =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 13 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlWithdrawBonus += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlWithdrawBonus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setWithdrawBonusAmount(rs.getDouble("amount"));
				cash.setWithdrawBonusCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
		}


		//	 withdraws by Admin

		try {
			String sqlWithdrawAdmin =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 5 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlWithdrawAdmin += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlWithdrawAdmin);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setWithdrawAdminAmount(rs.getDouble("amount"));
				cash.setWithdrawAdminCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
		}


		//	Reverse Withdraws
		try {
			String sqlReverseWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 6 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";

			sqlReverseWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlReverseWithdraws);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
				cash.setReverseWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Reverse Withdraws " +dr.getDateRange() + "  "+ e);
		}


		// Executed Withdraws - withdraw that processed yesterday

		try {
			String sqlExecutedWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 2 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";

			sqlExecutedWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlExecutedWithdraws);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
				cash.setExecutedWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Executed Withdraws "  +dr.getDateRange() + "  "+ e);
		}

		// Requested Withdraws - withdraw that Requested yesterday

		try {
			String sqlRequestedWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 2 "
							+ "and t.status_id in(4,9,6) "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

			sqlRequestedWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlRequestedWithdraws);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
				cash.setRequestedWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Requested Withdraws " + dr.getDateRange() +  " " +e);
		}

		// Net Cash = total deposits - executed withdraws

		cash.setNetInOut();

		// deposits headcount - how many users did any success deposit

		try {
			String sqlDepositHeadCount =
					"select count(distinct user_id) as count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";

			sqlDepositHeadCount += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositHeadCount);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setHeadCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Deposit Head Count  " + dr.getDateRange() + " " +e);
		}

		Investments invests = new Investments();
		invests.setCurrency(getCurrencyByRunType(runType));
		Investments investsOppPlus = new Investments();
		investsOppPlus.setCurrency(getCurrencyByRunType(runType));

		// investments - first the total of investements # of investments and
		// headcount
		try {
			String sqlTotalTurnover =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" count(distinct i.user_id) as headcount " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
				    " WHERE " +
				  		" i.is_canceled = 0 " +
				  		" and o.id = i.opportunity_id " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ?" +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTotalTurnover);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setTurnoverAmount(rs.getDouble("amount"));
				invests.setTurnoverCount(rs.getLong("count"));
				invests.setHeadCount(rs.getLong("headcount"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover " + dr.getDateRange() + " " +e);
		}

		// investments Opp Plus - first the total of investements # of investments and
		// headcount
		try {
			String sqlTotalTurnoverOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" count(distinct i.user_id) as headcount " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
				    " WHERE " +
				  		" i.is_canceled = 0 " +
				  		" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ?" +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTotalTurnoverOppPlus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setTurnoverAmount(rs.getDouble("amount"));
				investsOppPlus.setTurnoverCount(rs.getLong("count"));
				investsOppPlus.setHeadCount(rs.getLong("headcount"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover " + dr.getDateRange() + " Opp Plus" +e);
		}

		// investments - that settled yesterday.
		try {
			String sqlIntraday =
					"SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" sum(i.house_result/100) as profit, " +
						" sum(i.house_result/100)/sum(amount/100)*100 as precentage " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
				  		" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <= ? " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntraday);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
				invests.setIntradayTurnoverCount(rs.getLong("count"));
				invests.setIntradayprofitAmount(rs.getDouble("profit"));
				invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday " + dr.getDateRange() + " " +e);
		}

		// investments Opp Plus- that settled yesterday.
		try {
			String sqlIntradayOppPlus =
					"SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" sum(i.house_result/100) as profit, " +
						" sum(i.house_result/100)/sum(amount/100)*100 as precentage " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
				  		" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <= ? " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntradayOppPlus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setIntradayTurnoverAmount(rs.getDouble("amount"));
				investsOppPlus.setIntradayTurnoverCount(rs.getLong("count"));
				investsOppPlus.setIntradayprofitAmount(rs.getDouble("profit"));
				investsOppPlus.setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday " + dr.getDateRange() + " Opp Plus " +e);
		}

		try {
			String sqlIntradayWinningsCount =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 "+
						" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 "+
						" and u.ID = i.USER_ID "+
						" and u.CLASS_ID <> 0  "+
						" and i.WIN > 0 "+
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setWinTurnover(rs.getDouble("amount"));
				invests.setWinCount(rs.getLong("count"));
				invests.setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings Count - " + dr.getDateRange() + " " + e);
		}

		try {
			String sqlIntradayWinningsCountOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 "+
						" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 "+
						" and u.ID = i.USER_ID "+
						" and u.CLASS_ID <> 0  "+
						" and i.WIN > 0 "+
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayWinningsCountOppPlus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setWinTurnover(rs.getDouble("amount"));
				investsOppPlus.setWinCount(rs.getLong("count"));
				investsOppPlus.setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings Count - " + dr.getDateRange() + " Opp Plus " + e);
		}

		try {
			String sqlIntradayLossingCount =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0  " +
						" and i.LOSE > 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayLossingCount);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setLoseTurnover(rs.getDouble("amount"));
				invests.setLoseCount(rs.getLong("count"));
				invests.setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose Count " + dr.getDateRange() + " " + e);
		}

		try {
			String sqlIntradayLossingCountOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0  " +
						" and i.LOSE > 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayLossingCountOppPlus);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setLoseTurnover(rs.getDouble("amount"));
				investsOppPlus.setLoseCount(rs.getLong("count"));
				investsOppPlus.setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose Count " + dr.getDateRange() + " OppPlus" + e);
		}

		Other other = new Other();
		other.setCurrency(getCurrencyByRunType(runType));
		// gets other stuff - new accounts
		try {
			String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
										 "where to_char(TIME_CREATED,'YYYYMMDD') >=  ? and u.class_id<>0 "+
										 "and to_char(TIME_CREATED,'YYYYMMDD') <=  ? ";

			sqlOtherNewAccounts += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlOtherNewAccounts);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setNewAccounts(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new accounts " + dr.getDateRange()+  " " +e);
		}

		// gets other stuff - new depositor
		try {
			String sqlOtherNewDepositors =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct u.id) as count "
							+ "from users u , transactions t , transaction_types tt "
							+ "where t.user_id = u.id "
							+ "and t.type_id = tt.id "
							+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and u.CLASS_ID <> 0 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
							+ "and not exists (select distinct t2.user_id "
							+ "from transactions t2, transaction_types tt2 "
							+ "where t2.type_id = tt2.id "
							+ "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and tt2.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
							+ "and t2.user_id=t.user_id "
							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ? )";

			sqlOtherNewDepositors += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlOtherNewDepositors);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			pstmt.setString(3,dr.getStartDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
				other.setNewDepositingAccountsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new depositors " + dr.getDateRange()+ " " + e);
		}
		// get chargbacks that created yesterday
		try {
			String sqlChargebacks =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct t.id) as count "
							+ "from users u ,transactions t , charge_backs cb "
							+ "where t.charge_back_id = cb.id "
							+ "and u.ID = t.USER_ID and u.class_id<>0 "
							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  ? ";

			sqlChargebacks += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlChargebacks);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getStartDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setChargbacksAmount(rs.getDouble("amount"));
				other.setChargbacksCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get chargebacks " + dr.getDateRange() + " " + e);
		}

		ArrayList<MarketingCampaignDr> campaignList = new ArrayList<MarketingCampaignDr>();
		/// gets new accounts by ad
		try {
			String sqlOtherNewAccountsByCampaigns = "select count(distinct u.id) as count, ca.id as id, ca.name " +
											  "from users u, marketing_combinations co, marketing_campaigns ca " +
											  "where u.combination_id = co.id " +
											  		"and co.campaign_id = ca.id " +
											  		"and to_char(u.TIME_CREATED,'YYYYMMDD') >=  ? " +
											  		"and u.class_id <> 0 " +
											  		"and to_char(u.TIME_CREATED,'YYYYMMDD') <= ?  " +
											  		getSqlByRunType(runType) +
											  " group by ca.id, ca.name";

			pstmt = conn.prepareStatement(sqlOtherNewAccountsByCampaigns);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Other otherTmp = new Other();
				otherTmp.setNewAccounts(rs.getLong("count"));
				otherTmp.setCurrency(getCurrencyByRunType(runType));
				MarketingCampaignDr campaign = new MarketingCampaignDr();
				campaign.setId(rs.getLong("id"));
				campaign.setName(rs.getString("name"));
				campaign.setOther(otherTmp);
				campaignList.add(campaign);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new accounts by campaigns " + dr.getDateRange() + " " + e);
		}

		// gets other stuff - new depositor by campaigns
		try {
			String sqlOtherNewDepositorsByCampaigns =
							  "select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct u.id) as count, ca.id , ca.name "
							+ "from users u , transactions t , transaction_types tt, marketing_combinations co, marketing_campaigns ca "
							+ "where u.combination_id = co.id "
							+ "and co.campaign_id = ca.id "
							+ "and t.user_id = u.id "
							+ "and t.type_id = tt.id "
							+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and u.CLASS_ID <> 0 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
							+ "and not exists (select distinct t2.user_id "
											+ "from transactions t2, transaction_types tt2 "
											+ "where t2.type_id = tt2.id "
											+ "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
											+ "and tt2.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
											+ "and t2.user_id=t.user_id "
											+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ?) " +
							getSqlByRunType(runType) +
							" group by ca.id, ca.name";

			pstmt = conn.prepareStatement(sqlOtherNewDepositorsByCampaigns);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			pstmt.setString(3,dr.getStartDateForSql());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				long campaignId = rs.getLong("id");
				boolean found = false;
				for(int i=0;i < campaignList.size() && !found ;i++){
					MarketingCampaignDr campaign = campaignList.get(i);
					if (campaign.getId() == campaignId){
						Other otherTmp = campaign.getOther();
						otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
						otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
						found = true;
					} else {
						if (i == campaignList.size() -1){
							Other otherTmp = new Other();
							otherTmp.setNewAccounts(0);
							otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
							otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
							otherTmp.setCurrency(getCurrencyByRunType(runType));
							MarketingCampaignDr campaign_new = new MarketingCampaignDr();
							campaign_new.setId(rs.getLong("id"));
							campaign_new.setName(rs.getString("name"));
							campaign_new.setOther(otherTmp);
							campaignList.add(campaign_new);
							break;
						}
					}

				}

			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new depositors by campaigns " + dr.getDateRange() + " " +e);
		}

		// transactions that canceled for frauds activities
		try {
			String sqlTrxCanceld =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " "
							+ "and t.status_id = 12 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? ";

			sqlTrxCanceld += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTrxCanceld);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getStartDateForSql());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setTranCanceledFraudsAmount(rs.getDouble("amount"));
				cash.setTranCanceledFraudsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get transactions that canceled for frauds activities weekly" + e);
		}

		log.log(Level.DEBUG,"finished all queries - " + dr.getDateRange());
		// Now lets send emails with the report :

		log.log(Level.DEBUG,"going to send emails - " + dr.getDateRange());

		String subject = getSubjectByRunType(runType) + "report :"+dr.getDateRange();

		sendEmail(cash, invests, investsOppPlus, Investments.getDifference(invests, investsOppPlus), other,campaignList, getPageHeaderByRunType(runType), "report_date_range.html",subject, runType);

		log.log(Level.DEBUG,"finished to send emails - " + dr.getDateRange());

		try {
			rs.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			pstmt.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			conn.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}


	}

	*//**
	 * Send and Create Date Range Report by skins partition
	 * @param dr
	 *//*
	private static void calculateAndSendDateRangeReportSkinsPartition(DateRange dr) {

		ArrayList<Skins> skins = getAllSkins();   // get all skins list

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// Run the report for each skin and currencies

		for( Skins skin : skins ) {   // over all skins

			for ( SkinCurrencies sc : skin.getSkinCurrenciesList() ) {  // over all currencies of the skin

				Cash cash = new Cash();
				// / Deposits by Credit cards

				try {
					conn = Utils.getConnection();

					String sqlDepositCC =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t,users u "
									+ "where t.USER_ID = u.ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 1 "
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlDepositCC += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositCC);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setDepositCCAmount(rs.getDouble("amount"));
						cash.setDepositCCCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get deposits CC "+dr.getDateRange());
				}

				// Deposits by other then Credit cards

				try {
					String sqlDepositOther =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, transaction_types tt, users u "
									+ "where t.type_id = tt.id "
									+ "and u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " // deposits
									+ "and tt.id not in (1,17,18,21,25) " // no cc
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlDepositOther += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositOther);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setDepositOtherAmount(rs.getDouble("amount"));
						cash.setDepositOtherCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get deposits other " +dr.getDateRange() + "  " + e);
				}

				// Deposits by domestic

				try {
					String sqlDepositDomestic =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and t.type_id in (17,18,21) " // domestic
									+ "and u.CLASS_ID <> 0 "
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";

					sqlDepositDomestic += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositDomestic);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setDepositDomesticAmount(rs.getDouble("amount"));
						cash.setDepositDomesticCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get deposits other " + e);
				}

				// Deposits by Admin

				try {
					String sqlDepositAdmin =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 4 "
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlDepositAdmin += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositAdmin);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setDepositAdminAmount(rs.getDouble("amount"));
						cash.setDepositAdminCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get deposits admin " +dr.getDateRange() + "  " + e);
				}

				//	Deposits bonus

				try {
					String sqlDepositBonus =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 12 "
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlDepositBonus += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositBonus);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setDepositBonusAmount(rs.getDouble("amount"));
						cash.setDepositBonusCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get deposits Bonus " +dr.getDateRange() + "  " + e);
				}

				//	points to cash deposit

				try {
					String sqlpointsToCash =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 20 "
									+ "and t.status_id in (2) "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlpointsToCash += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlpointsToCash);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setPointsToCashAmount(rs.getDouble("amount"));
						cash.setPointsToCashCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get points to Cash deposit " +dr.getDateRange() + "  " + e);
				}


				// withdraws bonus
				try {
					String sqlWithdrawBonus =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 13 " // bonus withdraw
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlWithdrawBonus += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlWithdrawBonus);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setWithdrawBonusAmount(rs.getDouble("amount"));
						cash.setWithdrawBonusCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
				}


				//	 withdraws by Admin

				try {
					String sqlWithdrawAdmin =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 5 " // Withdraw admin
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlWithdrawAdmin += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlWithdrawAdmin);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setWithdrawAdminAmount(rs.getDouble("amount"));
						cash.setWithdrawAdminCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
				}


				//	Reverse Withdraws
				try {
					String sqlReverseWithdraws =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, users u "
									+ "where u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and t.type_id = 6 " // Reverse withdraw
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >=  ? "
									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";

					sqlReverseWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlReverseWithdraws);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();
					if (rs.next()) {
						cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
						cash.setReverseWithdrawsCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Reverse Withdraws " +dr.getDateRange() + "  "+ e);
				}


				// Executed Withdraws - withdraw that processed yesterday

				try {
					String sqlExecutedWithdraws =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, transaction_types tt, users u "
									+ "where t.type_id = tt.id "
									+ "and u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and tt.class_type = 2 "
									+ "and t.status_id = 2 "
									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";

					sqlExecutedWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlExecutedWithdraws);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
						cash.setExecutedWithdrawsCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Executed Withdraws "  +dr.getDateRange() + "  "+ e);
				}

				// Requested Withdraws - withdraw that Requested yesterday

				try {
					String sqlRequestedWithdraws =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, transaction_types tt, users u "
									+ "where t.type_id = tt.id "
									+ "and u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and tt.class_type = 2 "
									+ "and t.status_id in(4,9,6) "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlRequestedWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlRequestedWithdraws);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
						cash.setRequestedWithdrawsCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Requested Withdraws " + dr.getDateRange() +  " " +e);
				}

				// Net Cash = total deposits - executed withdraws

				cash.setNetInOut();

				// deposits headcount - how many users did any success deposit

				try {
					String sqlDepositHeadCount =
							"select count(distinct user_id) as count "
									+ "from transactions t, transaction_types tt, users u "
									+ "where t.type_id = tt.id "
									+ "and u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and tt.class_type = 1  " // Deposits
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";

					sqlDepositHeadCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlDepositHeadCount);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						cash.setHeadCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Deposit Head Count  " + dr.getDateRange() + " " +e);
				}

				Investments invests = new Investments();

				// investments - first the total of investements # of investments and
				// headcount

				try {
					String sqlTotalTurnover =
							"select sum(amount/100) as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount "
									+ "from investments i, users u "
									+ "where i.is_canceled = 0 "
									+ "and u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ?";

					sqlTotalTurnover += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlTotalTurnover);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						invests.setTurnoverAmount(rs.getDouble("amount"));
						invests.setTurnoverCount(rs.getLong("count"));
						invests.setHeadCount(rs.getLong("headcount"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Turnover " + dr.getDateRange() + " " +e);
				}

				// investments - that settled yesterday.

				try {
					String sqlIntraday =
							"select sum(amount/100) as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage "
									+ "from investments i, users u "
									+ "where i.is_settled = 1 "
									+ "and i.is_canceled = 0 "
									+ "and u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <= ?";

					sqlIntraday += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlIntraday);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
						invests.setIntradayTurnoverCount(rs.getLong("count"));
						invests.setIntradayprofitAmount(rs.getDouble("profit"));
						invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday " + dr.getDateRange() + " " +e);
				}

				try {
					String sqlIntradayWinningsCount =
							"select sum(amount/100) as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
							"from investments i, users u "+
							"where i.is_settled = 1 "+
							"and i.is_canceled = 0 "+
							"and u.ID = i.USER_ID "+
							"and u.CLASS_ID <> 0  "+
							"and i.WIN > 0 "+
							"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
							"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? ";

					sqlIntradayWinningsCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());


					pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						invests.setWinTurnover(rs.getDouble("amount"));
						invests.setWinCount(rs.getLong("count"));
						invests.setWinAmount(rs.getDouble("profit"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday Winnings Count - " + dr.getDateRange() + " " + e);
				}


				try {
					String sqlIntradayLossingCount =
							"select sum(amount/100) as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
							"from investments i, users u "+
							"where i.is_settled = 1 "+
							"and i.is_canceled = 0 "+
							"and u.ID = i.USER_ID "+
							"and u.CLASS_ID <> 0  "+
							"and i.LOSE > 0 "+
							"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? "+
							"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ?";

					sqlIntradayLossingCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());


					pstmt = conn.prepareStatement(sqlIntradayLossingCount);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						invests.setLoseTurnover(rs.getDouble("amount"));
						invests.setLoseCount(rs.getLong("count"));
						invests.setLoseAmount(rs.getDouble("profit"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday Lose Count " + dr.getDateRange() + " " + e);
				}



				Other other = new Other();
				// gets other stuff - new accounts
				try {
					String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
												 "where to_char(TIME_CREATED,'YYYYMMDD') >=  ? "+
												 "and to_char(TIME_CREATED,'YYYYMMDD') <=  ? and u.class_id <> 0 ";

					sqlOtherNewAccounts += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlOtherNewAccounts);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						other.setNewAccounts(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get new accounts " + dr.getDateRange()+  " " +e);
				}

				// gets other stuff - new depositor
				try {
					String sqlOtherNewDepositors =
							"select sum(amount/100) as amount, count(distinct u.id) as count "
									+ "from users u , transactions t , transaction_types tt "
									+ "where t.user_id = u.id "
									+ "and t.type_id = tt.id "
									+ "and tt.class_type = 1 "
									+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and u.CLASS_ID <> 0 "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
									+ "and not exists (select distinct t2.user_id "
									+ "from transactions t2, transaction_types tt2 "
									+ "where t2.type_id = tt2.id "
									+ "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
									+ "and tt2.class_type = 1 "
									+ "and t2.user_id=t.user_id "
									+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ? )";

					sqlOtherNewDepositors += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlOtherNewDepositors);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					pstmt.setString(3,dr.getStartDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
						other.setNewDepositingAccountsCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get new depositors " + dr.getDateRange()+ " " + e);
				}
				// get chargbacks that created yesterday
				try {
					String sqlChargebacks =
							"select sum(amount/100) as amount, count(distinct t.id) as count "
									+ "from users u ,transactions t , charge_backs cb "
									+ "where t.charge_back_id = cb.id "
									+ "and u.ID = t.USER_ID and u.class_id<>0 "
									+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  ? ";

					sqlChargebacks += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlChargebacks);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getStartDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						other.setChargbacksAmount(rs.getDouble("amount"));
						other.setChargbacksCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get chargebacks " + dr.getDateRange() + " " + e);
				}

				ArrayList<MarketingCampaignDr> campaignList = new ArrayList<MarketingCampaignDr>();
				/// gets new accounts by ad
				try {
					String sqlOtherNewAccountsByCampaigns = "select count(distinct u.id) as count,  ca.id as id, ca.name " +
												 	  "from users u, marketing_combinations co, marketing_campaigns ca  " +
												 	  "where u.combination_id = co.id " +
												 	  		"and co.campaign_id = ca.id " +
												 	  		"and to_char(u.TIME_CREATED,'YYYYMMDD') >= ? "+
												 	  		"and to_char(u.TIME_CREATED,'YYYYMMDD') <= ? " +
												 	  		"and u.class_id <> 0 "+
												 	  		getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) +
												 	 " group by ca.id, ca.name";

					pstmt = conn.prepareStatement(sqlOtherNewAccountsByCampaigns);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while (rs.next()) {
						Other otherTmp = new Other();
						otherTmp.setNewAccounts(rs.getLong("count"));
						MarketingCampaignDr campaign = new MarketingCampaignDr();
						campaign.setId(rs.getLong("id"));
						campaign.setName(rs.getString("name"));
						campaign.setOther(otherTmp);
						campaignList.add(campaign);
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get new accounts by campaigns " + dr.getDateRange() + " " + e);
				}

				// gets other stuff - new depositor by campaigns
				try {
					String sqlOtherNewDepositorsByCampaigns =
									  "select sum(amount/100) as amount, count(distinct u.id) as count, ca.id , ca.name  "
									+ "from users u , transactions t , transaction_types tt, marketing_combinations co, marketing_campaigns ca "
									+ "where  u.combination_id = co.id "
										   + "and co.campaign_id = ca.id "
										   + "and t.user_id = u.id "
										   + "and t.type_id = tt.id "
										   + "and tt.class_type = 1 "
										   + "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
										   + "and u.CLASS_ID <> 0 "
										   + "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
										   + "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
										   + "and not exists (select distinct t2.user_id "
										   				   + "from transactions t2, transaction_types tt2 "
										   				   + "where t2.type_id = tt2.id "
										   				   		 + "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
										   				   		 + "and tt2.class_type = 1 "
										   				   		 + "and t2.user_id=t.user_id "
										   				   		 + "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ?) " +
									getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId())
									+ " group by ca.id, ca.name";

					pstmt = conn.prepareStatement(sqlOtherNewDepositorsByCampaigns);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					pstmt.setString(3,dr.getStartDateForSql());
					rs = pstmt.executeQuery();

					while (rs.next()) {
						long campaignId = rs.getLong("id");
						boolean found = false;
						for(int i=0;i < campaignList.size() && !found ;i++){
							MarketingCampaignDr campaign = campaignList.get(i);
							if (campaign.getId() == campaignId){
								Other otherTmp = campaign.getOther();
								otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
								otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
								found = true;
							} else {
								if (i == campaignList.size() -1){
									Other otherTmp = new Other();
									otherTmp.setNewAccounts(0);
									otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
									otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
									MarketingCampaignDr campaign_new = new MarketingCampaignDr();
									campaign_new.setId(rs.getLong("id"));
									campaign_new.setName(rs.getString("name"));
									campaign_new.setOther(otherTmp);
									campaignList.add(campaign_new);
									break;
								}
							}

						}

					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get new depositors by campaigns " + dr.getDateRange() + " " +e);
				}

				// transactions that canceled for frauds activities
				try {
					String sqlTrxCanceld =
							"select sum(amount/100) as amount, count(t.id) count "
									+ "from transactions t, transaction_types tt, users u "
									+ "where t.type_id = tt.id "
									+ "and u.ID = t.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and tt.class_type = 1 "
									+ "and t.status_id = 12 "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? ";

					sqlTrxCanceld += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());

					pstmt = conn.prepareStatement(sqlTrxCanceld);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					if (rs.next()) {
						cash.setTranCanceledFraudsAmount(rs.getDouble("amount"));
						cash.setTranCanceledFraudsCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get transactions that canceled for frauds activities weekly" + e);
				}

				// set all objects for this run (skin-currency)
				sc.setCash(cash);
				sc.setInvests(invests);
				sc.setOther(other);
				sc.setCampaignList(campaignList);

				log.log(Level.DEBUG,"finished all queries for skinid: " + skin.getId() +  " currency: " + sc.getCode() +
						" " + dr.getDateRange());

				try {
					rs.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					pstmt.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					conn.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

			}  // end currencies loop

		}  // end skins loop

		// Now lets send emails with the report :
		log.log(Level.DEBUG,"going to send emails - " + dr.getDateRange());
		String subject = "All skins report - skins partition :"+dr.getDateRange();
		sendSkinsPartitionEmail(skins,"report_date_range_skinsPartition.html",subject);

		log.log(Level.DEBUG,"finished to send emails - " + dr.getDateRange());

	}

	private static DateRange getDatesFromCommandLine() {
		System.out.println("Start Running Report:");
		System.out.println("Please enter start date dd-mm-yyyy");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String startDate = "";
		boolean getStartDate = true;
		while (getStartDate){
			try {
				startDate = br.readLine();
				getStartDate = dateNotOK(startDate);
		      } catch (IOException ioe) {
		         System.out.println("IO error trying to read start date, please try again");
		      }
		}
		System.out.println("Please enter end date dd-mm-yyyy");
		String endDate = "";
		boolean getEndDate = true;
		while (getEndDate){
			try {
				endDate = br.readLine();
				getEndDate = dateNotOK(endDate);
		      } catch (IOException ioe) {
		         System.out.println("IO error trying to read end date, please try again");
		      }
		}
		DateRange dr = new DateRange();
		dr.setStartDate(startDate);
		dr.setEndDate(endDate);
		return dr;
	}

	private static boolean dateNotOK(String strDate) {
		String [] arr =  strDate.split("-");
		if (arr.length != 3){
			System.out.println("Problem with date, please try again format must be dd-mm-yyyy");
			return true;
		}
		try {
			new SimpleDateFormat("dd-MM-yyyyy").parse(strDate);
		} catch (Exception e) {
			System.out.println("Problem with date, please try again format must be dd-mm-yyyy");
			e.printStackTrace();
			return true;
		}

		return false;
	}

	private static void calculateAndSendWeeklyReport(int runType) {

		Cash cash = new Cash();
		cash.setCurrency(getCurrencyByRunType(runType));

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		//  Deposits by Credit cards

		try {
			conn = Utils.getConnection();

			String sqlDepositCC =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t,users u "
							+ "where t.USER_ID = u.ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 1 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD')"
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositCC += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositCC);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositCCAmount(rs.getDouble("amount"));
				cash.setDepositCCCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits CC weekly");
		}

       //  Deposits by other then Credit cards No Admin

		try {
			String sqlDepositOther =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 1 " // Deposits
							+ "and t.type_id not in (1,17,18,21,25,28,29,30) "    // Not cc
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositOther += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositOther);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositOtherAmount(rs.getDouble("amount"));
				cash.setDepositOtherCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits other weekly " + e);
		}

		// Deposits by domestic

		try {
			String sqlDepositDomestic =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id in (17,18,21) " // domestic
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositDomestic  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositDomestic);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositDomesticAmount(rs.getDouble("amount"));
				cash.setDepositDomesticCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits other " + e);
		}


		// Deposits by paypal

		try {
			String sqlPaypal =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id = 25 "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlPaypal  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlPaypal);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositPaypalAmount(rs.getDouble("amount"));
				cash.setDepositPaypalCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits paypal " + e);
		}


		// Deposits by ACH

		try {
			String sqlPaypal =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id = 28 "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlPaypal  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlPaypal);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositACHAmount(rs.getDouble("amount"));
				cash.setDepositACHCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits ACH " + e);
		}

		// Deposits by cashU

		try {
			String cashU =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id = 29 "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			cashU  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(cashU);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositCashUAmount(rs.getDouble("amount"));
				cash.setDepositCashUCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits cashU " + e);
		}

		// Deposits by Ukash

		try {
			String Ukash =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id = 30 "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			Ukash  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(Ukash);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositUkashAmount(rs.getDouble("amount"));
				cash.setDepositUkashCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits Ukash " + e);
		}

		// Deposits by Moneybookers

		try {
			String moneybookers =
				"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and t.type_id = 32 "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			moneybookers  += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(moneybookers);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositMoneybookersAmount(rs.getDouble("amount"));
				cash.setDepositMoneybookersCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits Moneybookers " + e);
		}

		// Deposits by other then Credit cards - ADMIN

		try {
			String sqlDepositAdmin =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 4 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositAdmin += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositAdmin);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositAdminAmount(rs.getDouble("amount"));
				cash.setDepositAdminCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits Admin weekly " + e);
		}

		// Fix balance deposit

		try {
			String sqlFixBDeposit =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 22 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlFixBDeposit += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlFixBDeposit);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setFixBalanceDepositAmount(rs.getDouble("amount"));
				cash.setFixBalanceDepositCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get fix balance deposits " + e);
		}

		// Deposit by company

		try {
			String sqlDCeposit =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 24 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDCeposit += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDCeposit);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setCompanyDepositAmount(rs.getDouble("amount"));
				cash.setCompanyDepositCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits by company " + e);
		}


    // Deposits Bonus

		try {
			String sqlDepositBonus =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 12 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositBonus += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositBonus);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setDepositBonusAmount(rs.getDouble("amount"));
				cash.setDepositBonusCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get deposits Bonus weekly " + e);
		}

	    // points to cash deposit

		try {
			String sqlPointsTocash =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 20 "
							+ "and t.status_id in (2) "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlPointsTocash += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlPointsTocash);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setPointsToCashAmount(rs.getDouble("amount"));
				cash.setPointsToCashCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get points to cash deposit weekly " + e);
		}


		// Withdraws - Bonus

		try {
			String sqlWithdrawBonus =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 13 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlWithdrawBonus += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlWithdrawBonus);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setWithdrawBonusAmount(rs.getDouble("amount"));
				cash.setWithdrawBonusCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Withdraw Bonus weekly " + e);
		}


		//	Withdraws - ADMIN

		try {
			String sqlWithdrawAdmin =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 5 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlWithdrawAdmin += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlWithdrawAdmin);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setWithdrawAdminAmount(rs.getDouble("amount"));
				cash.setWithdrawAdminCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Withdraw Admin weekly " + e);
		}

		// Fix balance withdrawal

		try {
			String sqlFixBWeposit =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 23 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlFixBWeposit += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlFixBWeposit);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setFixBalanceWithdrawalAmount(rs.getDouble("amount"));
				cash.setFixBalanceWithdrawalCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get fix balance withdrawal " + e);
		}

		// paypal withdrawal

		try {
			String sqlpaypal =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 26 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlpaypal += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlpaypal);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setWithdrawalPaypalAmount(rs.getDouble("amount"));
				cash.setWithdrawalPaypalCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get paypal withdrawal " + e);
		}

		//	Reverse Withdraws
		try {
			String sqlReverseWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, users u "
							+ "where u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and t.type_id = 6 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlReverseWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlReverseWithdraws);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
				cash.setReverseWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Reverse Withdraws weekly" + e);
		}


		// Executed Withdraws - withdraw that processed yesterday - no admin

		try {
			String sqlExecutedWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 2 "
							+ "and t.status_id = 2 "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlExecutedWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlExecutedWithdraws);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
				cash.setExecutedWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Executed Withdraws weekly" + e);
		}

		// Requested Withdraws - withdraw that Requested yesterday

		try {
			String sqlRequestedWithdraws =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 2 "
							+ "and t.status_id in(4,9,6) "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlRequestedWithdraws += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlRequestedWithdraws);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
				cash.setRequestedWithdrawsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Requested Withdraws weekly" + e);
		}

		// Net Cash = total deposits - executed withdraws

		cash.setNetInOut();

		// deposits headcount - how many users did any success deposit

		try {
			String sqlDepositHeadCount =
					"select count(distinct user_id) as count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 1  " // Deposits
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlDepositHeadCount += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlDepositHeadCount);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setHeadCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Deposit Head Count weekly " + e);
		}

		Investments invests = new Investments();
		invests.setCurrency(getCurrencyByRunType(runType));
		Investments investsOppPlus = new Investments();
		investsOppPlus.setCurrency(getCurrencyByRunType(runType));

		// investments - first the total of investements # of investments and
		// headcount
		try {
			String sqlTotalTurnover =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" count(distinct i.user_id) as headcount " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
				    " WHERE " +
				  		" i.is_canceled = 0 " +
				  		" and o.id = i.opportunity_id " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTotalTurnover);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setTurnoverAmount(rs.getDouble("amount"));
				invests.setTurnoverCount(rs.getLong("count"));
				invests.setHeadCount(rs.getLong("headcount"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover weekly" + e);
		}


		// investments Opp Plus - first the total of investements # of investments and headcount
		try {
			String sqlTotalTurnoverOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" count(distinct i.user_id) as headcount " +
					" FROM " +
						" investments i, " +
				    	" opportunities o, " +
				    	" users u " +
				    " WHERE " +
				  		" i.is_canceled = 0 " +
				  		" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTotalTurnoverOppPlus);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setTurnoverAmount(rs.getDouble("amount"));
				investsOppPlus.setTurnoverCount(rs.getLong("count"));
				investsOppPlus.setHeadCount(rs.getLong("headcount"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover weekly Opp Plus" + e);
		}

		// investments - that settled yesterday.
		try {
			String sqlIntraday =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count," +
						" sum(i.house_result/100) as profit," +
						" sum(i.house_result/100)/sum(amount/100)*100 as precentage " +
					" FROM " +
						" investments i, " +
						" opportunities o, " +
						" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntraday);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
				invests.setIntradayTurnoverCount(rs.getLong("count"));
				invests.setIntradayprofitAmount(rs.getDouble("profit"));
				invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday weekly" + e);
		}

		// investments Opp Plus - that settled yesterday.
		try {
			String sqlIntradayOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count," +
						" sum(i.house_result/100) as profit," +
						" sum(i.house_result/100)/sum(amount/100)*100 as precentage " +
					" FROM " +
						" investments i, " +
						" opportunities o, " +
						" users u " +
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntradayOppPlus);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setIntradayTurnoverAmount(rs.getDouble("amount"));
				investsOppPlus.setIntradayTurnoverCount(rs.getLong("count"));
				investsOppPlus.setIntradayprofitAmount(rs.getDouble("profit"));
				investsOppPlus.setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday weekly Opp Plus " + e);
		}

		try {
			String sqlIntradayWinningsCount =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
						" opportunities o, " +
						" users u "+
					" WHERE " +
						" i.is_settled = 1 "+
						" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 "+
						" and u.ID = i.USER_ID "+
						" and u.CLASS_ID <> 0  "+
						" and i.WIN > 0 "+
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setWinTurnover(rs.getDouble("amount"));
				invests.setWinCount(rs.getLong("count"));
				invests.setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings Count - weekly " + e);
		}


		try {
			String sqlIntradayWinningsCountOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM "+
						" investments i, " +
						" opportunities o, " +
						" users u "+
					" WHERE " +
						" i.is_settled = 1 "+
						" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 "+
						" and u.ID = i.USER_ID "+
						" and u.CLASS_ID <> 0  "+
						" and i.WIN > 0 "+
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlIntradayWinningsCountOppPlus);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setWinTurnover(rs.getDouble("amount"));
				investsOppPlus.setWinCount(rs.getLong("count"));
				investsOppPlus.setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings Count - weekly Opp Plus" + e);
		}

		try {
			String sqlIntradayLossingCount =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
						" opportunities o, " +
						" users u "+
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0  " +
						" and i.LOSE > 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayLossingCount);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				invests.setLoseTurnover(rs.getDouble("amount"));
				invests.setLoseCount(rs.getLong("count"));
				invests.setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose Count weekly" + e);
		}

		try {
			String sqlIntradayLossingCountOppPlus =
					" SELECT " +
						getSqlAmountSumByRunType(runType) + " as amount, " +
						" count(distinct i.id) as count, " +
						" -sum(i.house_result/100) as profit "+
					" FROM " +
						" investments i, " +
						" opportunities o, " +
						" users u "+
					" WHERE " +
						" i.is_settled = 1 " +
						" and o.id = i.opportunity_id " +
				  		" and o.opportunity_type_id = " + Opportunity.TYPE_OPTION_PLUS + " " +
						" and i.is_canceled = 0 " +
						" and u.ID = i.USER_ID " +
						" and u.CLASS_ID <> 0  " +
						" and i.LOSE > 0 " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
						" and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') " +
						getSqlByRunType(runType);


			pstmt = conn.prepareStatement(sqlIntradayLossingCountOppPlus);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investsOppPlus.setLoseTurnover(rs.getDouble("amount"));
				investsOppPlus.setLoseCount(rs.getLong("count"));
				investsOppPlus.setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose Count weekly Opp Plus " + e);
		}

		Other other = new Other();
		other.setCurrency(getCurrencyByRunType(runType));
		// gets other stuff - new accounts
		try {
			String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
										 "where to_char(TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "+
										 "and to_char(TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') and u.class_id <> 0 ";

			sqlOtherNewAccounts += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlOtherNewAccounts);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setNewAccounts(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new accounts weekly" + e);
		}

		// gets other stuff - new depositor
		try {
			String sqlOtherNewDepositors =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct u.id) as count "
							+ "from users u , transactions t , transaction_types tt "
							+ "where t.user_id = u.id "
							+ "and t.type_id = tt.id "
							+ "and tt.class_type = 1 "
							+ "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and u.CLASS_ID <> 0 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
							+ "and not exists (select distinct t2.user_id "
							+ "from transactions t2, transaction_types tt2 "
							+ "where t2.type_id = tt2.id "
							+ "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
							+ "and tt2.class_type = 1 "
							+ "and t2.user_id=t.user_id "
							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate-8,'YYYYMMDD')) ";

			sqlOtherNewDepositors += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlOtherNewDepositors);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
				other.setNewDepositingAccountsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new depositors weekly" + e);
		}
		// get chargbacks that created yesterday
		try {
			String sqlChargebacks =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct t.id) as count "
							+ "from users u ,transactions t , charge_backs cb "
							+ "where t.charge_back_id = cb.id "
							+ "and u.ID = t.USER_ID and u.class_id<>0 "
							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') ";

			sqlChargebacks += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlChargebacks);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				other.setChargbacksAmount(rs.getDouble("amount"));
				other.setChargbacksCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get chargebacks weekly" + e);
		}

		ArrayList<MarketingCampaignDr> campaignList = new ArrayList<MarketingCampaignDr>();
		/// gets new accounts by campaigns
		try {
			String sqlOtherNewAccountsByCampaigns = "select count(distinct u.id) as count, ca.id as id, ca.name "
											+ "from users u, marketing_combinations co, marketing_campaigns ca "
											+ "where u.combination_id = co.id "
											      + "and co.campaign_id = ca.id "
											      + "and to_char(u.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
											  	  + "and to_char(u.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
											  	  + "and u.class_id <> 0 "
											  	  + getSqlByRunType(runType)
										   + " group by ca.id, ca.name";

			pstmt = conn.prepareStatement(sqlOtherNewAccountsByCampaigns);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Other otherTmp = new Other();
				otherTmp.setNewAccounts(rs.getLong("count"));
				otherTmp.setCurrency(getCurrencyByRunType(runType));
				MarketingCampaignDr campaign = new MarketingCampaignDr();
				campaign.setId(rs.getLong("id"));
				campaign.setName(rs.getString("name"));
				campaign.setOther(otherTmp);
				campaignList.add(campaign);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new accounts by campaigns weekly" + e);
		}

		// gets other stuff - new depositor by campaigns
		try {
			String sqlOtherNewDepositorsByCampaigns =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct u.id) as count, ca.id , ca.name "
							+ "from users u , transactions t , transaction_types tt, marketing_combinations co, marketing_campaigns ca "
							+ "where u.combination_id = co.id "
								  + "and co.campaign_id = ca.id "
								  + "and t.user_id = u.id "
								  + "and t.type_id = tt.id "
								  + "and tt.class_type = 1 "
								  + "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
								  + "and u.CLASS_ID <> 0 "
								  + "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
								  + "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
								  + "and not exists (select distinct t2.user_id "
								  				  + "from transactions t2, transaction_types tt2 "
								  				  + "where t2.type_id = tt2.id "
								  				  		+ "and t2.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "
								  				  		+ "and tt2.class_type = 1 "
								  				  		+ "and t2.user_id=t.user_id "
								  				  		+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate-8,'YYYYMMDD')) "
								  + getSqlByRunType(runType)
								 + " group by ca.id, ca.name";

			pstmt = conn.prepareStatement(sqlOtherNewDepositorsByCampaigns);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				long campaignId = rs.getLong("id");
				boolean found = false;
				for(int i=0;i < campaignList.size() && !found ;i++){
					MarketingCampaignDr campaign = campaignList.get(i);
					if (campaign.getId() == campaignId){
						Other otherTmp = campaign.getOther();
						otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
						otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
						found = true;
					} else {
						if (i == campaignList.size() -1){
							Other otherTmp = new Other();
							otherTmp.setNewAccounts(0);
							otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
							otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
							otherTmp.setCurrency(getCurrencyByRunType(runType));
							MarketingCampaignDr campaign_new = new MarketingCampaignDr();
							campaign_new.setId(rs.getLong("id"));
							campaign_new.setName(rs.getString("name"));
							campaign_new.setOther(otherTmp);
							campaignList.add(campaign_new);
							break;
						}
					}

				}

			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get new depositors by campaigns weekly" + e);
		}

		// transactions that canceled for frauds activities
		try {
			String sqlTrxCanceld =
					"select " + getSqlAmountSumByRunType(runType) + " as amount, count(t.id) count "
							+ "from transactions t, transaction_types tt, users u "
							+ "where t.type_id = tt.id "
							+ "and u.ID = t.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and tt.class_type = 1 "
							+ "and t.status_id = 12 "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";

			sqlTrxCanceld += getSqlByRunType(runType);

			pstmt = conn.prepareStatement(sqlTrxCanceld);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				cash.setTranCanceledFraudsAmount(rs.getDouble("amount"));
				cash.setTranCanceledFraudsCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get transactions that canceled for frauds activities weekly" + e);
		}

		log.log(Level.DEBUG,"finished all queries - weekly ");
		// Now lets send emails with the report :

		log.log(Level.DEBUG,"going to send emails - weekly");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String endDate = sdf.format(cal.getTime());
		cal.add(Calendar.DAY_OF_YEAR,-6);
		String startDate = sdf.format(cal.getTime());
		String subject = getSubjectByRunType(runType) + "weekly report :" + startDate + " - " + endDate;

		String templateName = "report_weekly.html";;

		sendEmail(cash, invests, investsOppPlus, Investments.getDifference(invests, investsOppPlus), other, campaignList, getPageHeaderByRunType(runType), templateName, subject, runType);

		log.log(Level.DEBUG,"finished to send emails - weekly");

		try {
			rs.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			pstmt.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			conn.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

	}

	*//**
	 * This report just for etrader, in Ao we are doning this automatically
	 *//*
	private static void SendDailyReportNeedFiles() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
		int totalDepositsAmountForFiles = 10000; // defualt value.

		try {

			conn = Utils.getConnection();
			String sqlNeedFiles =
								"select u.ID as id,u.USER_NAME as username "+
								"from transactions t, transaction_types tt, users u "+
								"where t.type_id = tt.id  "+
								"and u.ID = t.USER_ID "+
								"and u.CLASS_ID <> 0 "+
								"and tt.class_type = 1 "+
								"and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "+
								"and to_char(t.TIME_CREATED,'YYYYMMDD') < to_char(sysdate,'YYYYMMDD') " +
								"and u.skin_id = 1 " +
								"group by u.ID,u.USER_NAME " +
								"having sum(amount/100) > ? "+
								"minus "+
								"select u.ID as id,u.USER_NAME as username "+
								"from transactions t, transaction_types tt, users u "+
								"where t.type_id = tt.id "+
								"and u.ID = t.USER_ID "+
								"and u.CLASS_ID <> 0 "+
								"and tt.class_type = 1 "+
								"and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") "+
								"and to_char(t.TIME_CREATED,'YYYYMMDD') < to_char(sysdate -1,'YYYYMMDD') " +
								"and u.skin_id = 1 " +
								"group by u.ID,u.USER_NAME " +
								"having sum(amount/100) > ? ";

		pstmt = conn.prepareStatement(sqlNeedFiles);

		try {
			totalDepositsAmountForFiles = Integer.parseInt(Utils.getProperty("total.deposits.need.files"));
		} catch (NumberFormatException e){
			log.log(Level.WARN,"cant get total.deposits.need.files asetting it to 10K" + e);
			totalDepositsAmountForFiles = 10000;
		}

		pstmt.setInt(1,totalDepositsAmountForFiles);
		pstmt.setInt(2,totalDepositsAmountForFiles);

		rs = pstmt.executeQuery();

		while(rs.next()) {
			NeedFiles needFiles = new NeedFiles();
			needFiles.setAccountNumber(rs.getString("id"));
			needFiles.setSumDeposits(totalDepositsAmountForFiles);
			needFiles.setUsername(rs.getString("username"));
			needFilesArr.add(needFiles);
		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get need files depositors " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailNeedFiles(needFilesArr,totalDepositsAmountForFiles);

		log.log(Level.DEBUG,"finished to send emails");

	}

	private static void calculateAndSendRangeReportMarkteBreakDown(DateRange dr, int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// investments - first the total of investements # of investments and
		// headcount

		try {

			conn = Utils.getConnection();

			String sqlTotalTurnover =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ," +
							"m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u, investments i, opportunities o, markets m, market_groups g,skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "

							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "


							+ "and i.is_canceled = 0 "
							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME ";

			pstmt = conn.prepareStatement(sqlTotalTurnover);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Investments inv = new Investments();
				inv.setTurnoverAmount(rs.getDouble("amount"));
				inv.setTurnoverCount(rs.getLong("count"));
				inv.setHeadCount(rs.getLong("headcount"));
				inv.setCurrency(getCurrencyByRunType(runType));
				Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
				marktes.put(rs.getLong("market_id"),market);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
		}

		// investments - that settled yesterday.

		try {
			String sqlIntraday =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntraday);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {

				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
				m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
				m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
				m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
		}

		try {
			String sqlIntradayWinnings =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count," +
							"-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.win > 0 "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntradayWinnings);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
				m.getInvetsments().setWinCount(rs.getLong("count"));
				m.getInvetsments().setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
		}



		try {
			String sqlIntradayLose =
							"select " + getSqlAmountSumByRunType(runType) + " as amount," +
							" count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.lose > 0 "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntradayLose);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
				m.getInvetsments().setLoseCount(rs.getLong("count"));
				m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
		}

		try {
			String sqlCalls =
							"select " + getSqlAmountSumByRunType(runType) + " as amount," +
							" count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.TYPE_ID = 1 " // calls
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlCalls);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setLoseTurnover(0);
					inv.setLoseCount(0);
					inv.setLoseAmount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setCallAmount(rs.getDouble("amount"));
				m.getInvetsments().setCallCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
		}

		try {
			String sqlPuts =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, " +
							"count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.TYPE_ID = 2 " //puts
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlPuts);
			pstmt.setString(1,dr.getStartDateForSql());
			pstmt.setString(2,dr.getEndDateForSql());
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setLoseTurnover(0);
					inv.setLoseCount(0);
					inv.setLoseAmount(0);
					inv.setCallAmount(0);
					inv.setCallCount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setPutAmount(rs.getDouble("amount"));
				m.getInvetsments().setPutCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
		}

		sendEmailDateRangeMarketBreakDown(dr, getPageHeaderByRunType(runType), runType);

	}


	*//**
	 * Calaulate and Send Range Report MarkteBreakDown with Skins Partition
	 * @param dr
	 *//*
	private static void calculateAndSendRangeReportMarkteBreakDownSkinsPartition(DateRange dr) {

		ArrayList<Skins> skins = getAllSkins();   // get all skins list

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

//		 Run the report for each skin and currencies

		for( Skins skin : skins ) {   // over all skins

			for ( SkinCurrencies sc : skin.getSkinCurrenciesList() ) {  // over all currencies of the skin

				marktes = new HashMap<Long, Market>();   // set markets list for each skin-currency

				// investments - first the total of investements # of investments and
				// headcount

				try {

					conn = Utils.getConnection();

					String sqlTotalTurnover =
									"select sum(amount/100) as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ," +
									"m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u, investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg  "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_canceled = 0 "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME ";

					pstmt = conn.prepareStatement(sqlTotalTurnover);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {
						Investments inv = new Investments();
						inv.setTurnoverAmount(rs.getDouble("amount"));
						inv.setTurnoverCount(rs.getLong("count"));
						inv.setHeadCount(rs.getLong("headcount"));
						Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
						marktes.put(rs.getLong("market_id"),market);
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
				}

				// investments - that settled yesterday.

				try {
					String sqlIntraday =
							"select sum(amount/100) as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_settled =1 "
									+ "and i.is_canceled = 0 "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";

					pstmt = conn.prepareStatement(sqlIntraday);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {

						Market m = marktes.get(rs.getLong("market_id"));
						if (m == null) {
							Investments inv = new Investments();
							inv.setTurnoverAmount(0);
							inv.setTurnoverCount(0);
							inv.setHeadCount(0);
							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
							marktes.put(rs.getLong("market_id"),m);
						}
						m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
						m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
						m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
						m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
				}

				try {
					String sqlIntradayWinnings =
									"select sum(amount/100) as amount, count(distinct i.id) as count," +
									"-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_settled =1 "
									+ "and i.is_canceled = 0 "
									+ "and i.win > 0 "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";

					pstmt = conn.prepareStatement(sqlIntradayWinnings);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {
						Market m = marktes.get(rs.getLong("market_id"));
						if (m == null) {
							Investments inv = new Investments();
							inv.setTurnoverAmount(0);
							inv.setTurnoverCount(0);
							inv.setHeadCount(0);
							inv.setIntradayTurnoverAmount(0);
							inv.setIntradayTurnoverCount(0);
							inv.setIntradayprofitAmount(0);
							inv.setIntradayprofitPrecentage(0);
							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
							marktes.put(rs.getLong("market_id"),m);
						}
						m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
						m.getInvetsments().setWinCount(rs.getLong("count"));
						m.getInvetsments().setWinAmount(rs.getDouble("profit"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
				}

				try {
					String sqlIntradayLose =
									"select sum(amount/100) as amount," +
									" count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_settled =1 "
									+ "and i.is_canceled = 0 "
									+ "and i.lose > 0 "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";

					pstmt = conn.prepareStatement(sqlIntradayLose);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {
						Market m = marktes.get(rs.getLong("market_id"));
						if (m == null) {
							Investments inv = new Investments();
							inv.setTurnoverAmount(0);
							inv.setTurnoverCount(0);
							inv.setHeadCount(0);
							inv.setIntradayTurnoverAmount(0);
							inv.setIntradayTurnoverCount(0);
							inv.setIntradayprofitAmount(0);
							inv.setIntradayprofitPrecentage(0);
							inv.setWinTurnover(0);
							inv.setWinCount(0);
							inv.setWinAmount(0);
							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
							marktes.put(rs.getLong("market_id"),m);
						}
						m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
						m.getInvetsments().setLoseCount(rs.getLong("count"));
						m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
				}

				try {
					String sqlCalls =
									"select sum(amount/100) as amount," +
									" count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_settled =1 "
									+ "and i.is_canceled = 0 "
									+ "and i.TYPE_ID = 1 " // calls
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";

					pstmt = conn.prepareStatement(sqlCalls);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {
						Market m = marktes.get(rs.getLong("market_id"));
						if (m == null) {
							Investments inv = new Investments();
							inv.setTurnoverAmount(0);
							inv.setTurnoverCount(0);
							inv.setHeadCount(0);
							inv.setIntradayTurnoverAmount(0);
							inv.setIntradayTurnoverCount(0);
							inv.setIntradayprofitAmount(0);
							inv.setIntradayprofitPrecentage(0);
							inv.setWinTurnover(0);
							inv.setWinCount(0);
							inv.setWinAmount(0);
							inv.setLoseTurnover(0);
							inv.setLoseCount(0);
							inv.setLoseAmount(0);
							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
							marktes.put(rs.getLong("market_id"),m);
						}
						m.getInvetsments().setCallAmount(rs.getDouble("amount"));
						m.getInvetsments().setCallCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
				}

				try {
					String sqlPuts =
									"select sum(amount/100) as amount, " +
									"count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
									+ "where u.ID = i.USER_ID "
									+ "and u.CLASS_ID <> 0 "
									+ "and i.OPPORTUNITY_ID = o.ID "
									+ "and o.MARKET_ID =  m.ID "
									+ "and smgm.skin_market_group_id = smg.id "
									+ "and smg.market_group_id = g.ID "
									+ "and m.id = smgm.market_id "
									+ "and smg.skin_id = u.skin_id "
									+ "and i.is_settled =1 "
									+ "and i.is_canceled = 0 "
									+ "and i.TYPE_ID = 2 " //puts
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";

					pstmt = conn.prepareStatement(sqlPuts);
					pstmt.setString(1,dr.getStartDateForSql());
					pstmt.setString(2,dr.getEndDateForSql());
					rs = pstmt.executeQuery();

					while(rs.next()) {
						Market m = marktes.get(rs.getLong("market_id"));
						if (m == null) {
							Investments inv = new Investments();
							inv.setTurnoverAmount(0);
							inv.setTurnoverCount(0);
							inv.setHeadCount(0);
							inv.setIntradayTurnoverAmount(0);
							inv.setIntradayTurnoverCount(0);
							inv.setIntradayprofitAmount(0);
							inv.setIntradayprofitPrecentage(0);
							inv.setWinTurnover(0);
							inv.setWinCount(0);
							inv.setWinAmount(0);
							inv.setLoseTurnover(0);
							inv.setLoseCount(0);
							inv.setLoseAmount(0);
							inv.setCallAmount(0);
							inv.setCallCount(0);
							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
							marktes.put(rs.getLong("market_id"),m);
						}
						m.getInvetsments().setPutAmount(rs.getDouble("amount"));
						m.getInvetsments().setPutCount(rs.getLong("count"));
					}
				} catch (Exception e) {
					log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
				}

				// set market for this run (skin-currency)
				sc.setMarktes(marktes);

				log.log(Level.DEBUG,"finished all queries for skinid: " + skin.getId() +  " currency: " + sc.getCode() +
						" " + dr.getDateRange());

				try {
					rs.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					pstmt.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					conn.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

			} //  end currencies loop

		}  // end skins loop

		sendEmailDateRangeMarketBreakDownSkinsPartition(dr, skins);

	}

	private static void calculateAndSendDailyReportMarkteBreakDown(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// investments - first the total of investements # of investments and
		// headcount

		try {

			conn = Utils.getConnection();

			String sqlTotalTurnover =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u, investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg  "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_canceled = 0 "
							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME ";

			pstmt = conn.prepareStatement(sqlTotalTurnover);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Investments inv = new Investments();
				inv.setTurnoverAmount(rs.getDouble("amount"));
				inv.setTurnoverCount(rs.getLong("count"));
				inv.setHeadCount(rs.getLong("headcount"));
				inv.setCurrency(getCurrencyByRunType(runType));
				Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
				marktes.put(rs.getLong("market_id"),market);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
		}

		// investments - that settled yesterday.

		try {
			String sqlIntraday =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType)	+ " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntraday);
			rs = pstmt.executeQuery();

			while(rs.next()) {

				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
				m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
				m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
				m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
		}

		try {
			String sqlIntradayWinnings =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.win > 0 "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType)	+ " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntradayWinnings);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
				m.getInvetsments().setWinCount(rs.getLong("count"));
				m.getInvetsments().setWinAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
		}



		try {
			String sqlIntradayLose =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled = 1 "
							+ "and i.is_canceled = 0 "
							+ "and i.lose > 0 "
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlIntradayLose);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
				m.getInvetsments().setLoseCount(rs.getLong("count"));
				m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
		}

		try {
			String sqlCalls =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.TYPE_ID = 1 " // calls
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlCalls);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setLoseTurnover(0);
					inv.setLoseCount(0);
					inv.setLoseAmount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setCallAmount(rs.getDouble("amount"));
				m.getInvetsments().setCallCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
		}

		try {
			String sqlPuts =
							"select " + getSqlAmountSumByRunType(runType) + " as amount, count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = i.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and i.OPPORTUNITY_ID = o.ID "
							+ "and o.MARKET_ID =  m.ID "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = g.ID "
							+ "and m.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and i.is_settled =1 "
							+ "and i.is_canceled = 0 "
							+ "and i.TYPE_ID = 2 " //puts
							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";

			pstmt = conn.prepareStatement(sqlPuts);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setWinTurnover(0);
					inv.setWinCount(0);
					inv.setWinAmount(0);
					inv.setLoseTurnover(0);
					inv.setLoseCount(0);
					inv.setLoseAmount(0);
					inv.setCallAmount(0);
					inv.setCallCount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}
				m.getInvetsments().setPutAmount(rs.getDouble("amount"));
				m.getInvetsments().setPutCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
		}
		// gets open positions for weekly

		try {
			String sqlOpenPositionsWeek =
							"select	" + getSqlAmountSumByRunType(runType) + " as amount,count (distinct A.id) as count, c.id as market_id , c.NAME as market, D.NAME as mgroup "
							+ "from users u, investments A, opportunities B, markets C , market_groups D, skin_market_group_markets smgm, skin_market_groups smg "
							+ "where u.ID = A.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and A.opportunity_id = B.id  "
							+ "and  B.market_id = C.id  "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = D.ID "
							+ "and C.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and B.scheduled = 3 "
							+ // weekly
							"and A.is_settled = 0 "
							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp "
							+ getSqlByRunType(runType) + " group by c.id , c.NAME , D.NAME";

			pstmt = conn.prepareStatement(sqlOpenPositionsWeek);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}

				m.getInvetsments().setOpenPositionsEndWeekAmount(rs.getDouble("amount"));
				m.getInvetsments().setOpenPositionsEndWeekCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get OpenPositionsWeek market breakdown " + e);
		}

		// gets open positions for monthly

		try {
			String sqlOpenPositionsMonth =
							"select	" + getSqlAmountSumByRunType(runType) + " as amount,count (distinct A.id) as count, c.id as market_id , c.NAME as market, D.NAME as mgroup "
							+ "from users u, investments A, opportunities B, markets C , market_groups D, skin_market_group_markets smgm, skin_market_groups smg  "
							+ "where u.ID = A.USER_ID "
							+ "and u.CLASS_ID <> 0 "
							+ "and A.opportunity_id = B.id  "
							+ "and  B.market_id = C.id  "
							+ "and smgm.skin_market_group_id = smg.id "
							+ "and smg.market_group_id = D.ID "
							+ "and C.id = smgm.market_id "
							+ "and smg.skin_id = u.skin_id "
							+ "and B.scheduled = 4 "
							+ // monthly
							"and A.is_settled = 0 "
							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp "
							+ getSqlByRunType(runType) + " group by c.id , c.NAME , D.NAME";

			pstmt = conn.prepareStatement(sqlOpenPositionsMonth);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				Market m = marktes.get(rs.getLong("market_id"));
				if (m == null) {
					Investments inv = new Investments();
					inv.setTurnoverAmount(0);
					inv.setTurnoverCount(0);
					inv.setHeadCount(0);
					inv.setIntradayTurnoverAmount(0);
					inv.setIntradayTurnoverCount(0);
					inv.setIntradayprofitAmount(0);
					inv.setIntradayprofitPrecentage(0);
					inv.setOpenPositionsEndWeekAmount(0);
					inv.setOpenPositionsEndWeekCount(0);
					inv.setCurrency(getCurrencyByRunType(runType));
					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
					marktes.put(rs.getLong("market_id"),m);
				}

				m.getInvetsments().setOpenPositionsEndMonthAmount(rs.getDouble("amount"));
				m.getInvetsments().setOpenPositionsEndMonthCount(rs.getLong("count"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get OpenPositionsMonth market breakdown " + e);
		}

		sendEmailMarketBreakDown(getPageHeaderByRunType(runType), runType);
	}

	private static void calculateAndSendBaseReport(int dateRangeType) throws Exception {

		ArrayList<BaseReportRow> depositTypes = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> depositTotals = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> withdrawals = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> other = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> investments = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> newCustomersPerSkin = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> newCustomersTotals = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> newCustomersSource = new ArrayList<BaseReportRow>();
		ArrayList<BaseReportRow> mobile = new ArrayList<BaseReportRow>();

		double totalDeposits = 0;
		double totalWithdrawals = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BaseReportRow row = null;

		int daysBefore = 0;
		String subject = "";

		if (RUN_DAILY == dateRangeType){
			daysBefore = 1;
			subject = "Daily Managers Report of ";
		}else if (RUN_WEEKLY == dateRangeType){
			daysBefore = 8;
			subject = "Weekly Managers Report Starting ";
		}

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-daysBefore);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		subject += sdf.format(cal.getTime());


		// New Customers Per Skin
		try {
			conn = Utils.getConnection();

			String sqlNewCustomersPerSkin =
				" select  " +
					" s.display_name skin_name, " +
					" sum(t.reg_amount) reg_amount,  " +
					" sum(t.reg_headcount) reg_headcount, " +
					" sum(t.dep_1st_amount) dep_1st_amount, " +
					" sum(t.dep_1st_headcount) dep_1st_headcount, " +
					" sum(t.dep_1st_reg_amount) dep_1st_reg_amount, " +
					" sum(t.dep_1st_reg_headcount) dep_1st_reg_headcount, " +
					" sum(t.dep_1st_sales_amount) dep_1st_sales_amount, " +
					" sum(t.dep_1st_sales_headcount) dep_1st_sales_headcount, " +
					" case when sum(t.dep_1st_amount) > 0 then sum(t.dep_1st_sales_amount)/sum(t.dep_1st_amount)*100 " +
						" else 0 end per_dep_1st_sales_amount, " +
					" case when sum(t.dep_1st_headcount) > 0 then sum(t.dep_1st_sales_headcount)/sum(t.dep_1st_headcount)*100  " +
						" else 0 end per_dep_1st_sales_headcount" +
				" from  " +
					" skins s, " +
					" (select  " +
						" u.skin_id,  " +
						" 0 reg_amount,  " +
						" 0 reg_headcount, " +
						" sum(case when u.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) then t.amount * t.rate else 0 end)/100 dep_1st_reg_amount, " +
						" count(case when u.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) then t.user_id else null end) dep_1st_reg_headcount, " +
						" sum(t.amount * t.rate)/100 dep_1st_amount, " +
						" count(t.user_id) dep_1st_headcount, " +
						" sum(case when ti.transaction_id is not null then t.amount * t.rate else 0 end)/100 dep_1st_sales_amount, " +
						" count(case when ti.transaction_id is not null then t.user_id else null end) dep_1st_sales_headcount " +
					" from  " +
						" users u, " +
						" transactions t " +
							" left join transactions_issues ti on ti.transaction_id = t.id " +
					" where  " +
						" u.id = t.user_id " +
						" and u.class_id <> 0 " +
						" and t.id = u.first_deposit_id " +
						" and t.type_id in (select tt.id " +
										  " from transaction_types tt " +
										  " where tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + ") " +
						" and t.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) " +
						" and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
					" group by  " +
						" u.skin_id  " +

					" union  " +

					" select  " +
						" u.skin_id,  " +
						" 0 reg_amount,  " +
						" count(*) reg_headcount, " +
						" 0 dep_1st_reg_amount, " +
						" 0 dep_1st_reg_headcount, " +
						" 0 dep_1st_amount, " +
						" 0 dep_1st_headcount, " +
						" 0 dep_1st_sales_amount, " +
						" 0 dep_1st_sales_headcount " +
					" from  " +
						" users u " +
					" where " +
						" u.class_id <> 0 " +
						" and u.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) " +
					" group by  " +
						" u.skin_id) t  " +
				" where " +
					" t.skin_id = s.id " +
				" group by " +
					" s.display_name," +
					" t.skin_id " +
				" order by " +
					" t.skin_id ";

			pstmt = conn.prepareStatement(sqlNewCustomersPerSkin);
			rs = pstmt.executeQuery();

			BaseReportRow row1Total = new BaseReportRow();
			row1Total.setRowName("Registered (New accounts)");
			BaseReportRow row2Total = new BaseReportRow();
			row2Total.setRowName("First dep");
			BaseReportRow row3Total = new BaseReportRow();
			row3Total.setRowName("First regdepdep");
			BaseReportRow row4Total = new BaseReportRow();
			row4Total.setRowName("New SALES conversions");
			BaseReportRow row5Total = new BaseReportRow();
			row5Total.setRowName("% of SALES from first dep");

			while (rs.next()) {
				String skinName = Utils.getProperty(rs.getString("skin_name"));

				row = new BaseReportRow();
				row.setRowName(skinName);
				row.setTotalAmountUsd(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setHeadcount(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setBold();
				newCustomersPerSkin.add(row);


				row = new BaseReportRow();
				row.setRowName("Registered (New accounts)");
				row.setTotalAmountUsd(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setHeadcount(rs.getLong("reg_headcount"));
				newCustomersPerSkin.add(row);

				row = new BaseReportRow();
				row.setRowName("First dep");
				row.setTotalAmountUsd(rs.getDouble("dep_1st_amount"));
				row.setHeadcount(rs.getLong("dep_1st_headcount"));
				newCustomersPerSkin.add(row);

				row = new BaseReportRow();
				row.setRowName("First regdepdep");
				row.setTotalAmountUsd(rs.getDouble("dep_1st_reg_amount"));
				row.setHeadcount(rs.getLong("dep_1st_reg_headcount"));
				newCustomersPerSkin.add(row);

				row = new BaseReportRow();
				row.setRowName("New SALES conversions");
				row.setTotalAmountUsd(rs.getDouble("dep_1st_sales_amount"));
				row.setHeadcount(rs.getLong("dep_1st_sales_headcount"));
				newCustomersPerSkin.add(row);

				row = new BaseReportRow();
				row.setRowName("% of SALES from first dep");
				row.setTotalAmountUsd(rs.getDouble("per_dep_1st_sales_amount"));
				row.setHeadcount(rs.getLong("per_dep_1st_sales_headcount"));
				row.setPercentStr();
				newCustomersPerSkin.add(row);

				row = new BaseReportRow();
				row.setRowName("-----------------------------------");
				row.setTotalAmountUsd(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setHeadcount(ConstantsBase.REPORT_EMPTY_VALUE);
				newCustomersPerSkin.add(row);

				row1Total.addHeadcount(rs.getLong("reg_headcount"));
				row2Total.addTotalAmountUsd(rs.getDouble("dep_1st_amount"));
				row2Total.addHeadcount(rs.getLong("dep_1st_headcount"));
				row3Total.addTotalAmountUsd(rs.getDouble("dep_1st_reg_amount"));
				row3Total.addHeadcount(rs.getLong("dep_1st_reg_headcount"));
				row4Total.addTotalAmountUsd(rs.getDouble("dep_1st_sales_amount"));
				row4Total.addHeadcount(rs.getLong("dep_1st_sales_headcount"));
			}

			row1Total.setTotalAmountUsd(-1);
			if (row2Total.getHeadcount() > 0){
				row5Total.setTotalAmountUsd(row4Total.getTotalAmountUsd()*100/row2Total.getTotalAmountUsd());
				row5Total.setHeadcount((long)Math.round(new Double(row4Total.getHeadcount())*100/row2Total.getHeadcount()));
			}
			newCustomersTotals.add(row1Total);
			newCustomersTotals.add(row2Total);
			newCustomersTotals.add(row3Total);
			newCustomersTotals.add(row4Total);
			row5Total.setPercentStr();
			newCustomersTotals.add(row5Total);


		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get New Coustomers Per Skin");
		}

		// New Customers Per Source
		try {
			conn = Utils.getConnection();

			String freeCampaigns = MarketingCampaign.FREE_AO_68 + "," + MarketingCampaign.FREE_ET_112;
		  String googlePaymentRecipients = "25,31,341,105,333,534,28,320,591";/*MarketingPaymentRecipient.GOOGLE + "," +
											 MarketingPaymentRecipient.YAHOO_PPC + "," +
											 MarketingPaymentRecipient.MSN + "," +
											 MarketingPaymentRecipient.FACEBOOK ;

			String marketingSourceIds = MarketingSource.GOOGLE + "," + MarketingSource.MSN + "," +
										MarketingSource.FACEBOOK + "," + MarketingSource.YANDEX + "," +
										MarketingSource.VKONTAKTE + "," + MarketingSource.YOTTOS + "," +
										MarketingSource.YAHOO_PPC + "," + MarketingSource.MERLIN_NETWORKS_PPC + "," +
										MarketingSource.BAIDU;

			String sqlCustomersSource =
				" select " +
					" sum(A.mobile_reg) mobile_reg, " +
					" sum(A.total_reg) total_reg,  " +
					" sum(A.total_first_dep) total_first_dep, " +
					" sum(A.reg_free) reg_free, " +
					" sum(A.first_dep_free) first_dep_free, " +
					" sum(A.reg_google) reg_google, " +
					" sum(A.first_dep_google) first_dep_google, " +
					" sum(A.reg_netrefer) reg_netrefer, " +
					" sum(A.first_dep_netrefer) first_dep_netrefer, " +
					" sum(A.reg_media) reg_media, " +
					" sum(A.first_dep_media) first_dep_media " +
				" from    " +
					" (select " +
						" 0 mobile_reg, " +
						" 0 total_reg, " +
						" count(u.id) total_first_dep, " +
						" 0 reg_free, " +
						" count(case when mc.campaign_id in (" + freeCampaigns + ") " +
											" and u.netrefer_user_time is null then u.id else null end) first_dep_free, " +
						" 0 reg_google, " +
						" count(case when mca.SOURCE_ID in (" + marketingSourceIds + ") and u.netrefer_user_time is null then u.id else null end) first_dep_google, " +
						" 0 reg_netrefer, " +
						" count(case when u.netrefer_user_time is not null then u.id else null end) first_dep_netrefer, " +
						" 0 reg_media, " +
						" count(case when mc.campaign_id not in (" + freeCampaigns + ") and mca.SOURCE_ID not in (" + marketingSourceIds + ") and u.netrefer_user_time is null " +
								   " then u.id else null end) first_dep_media " +
					" from  " +
						" users u, " +
						" transactions t, " +
						" marketing_combinations mc, " +
						" marketing_campaigns mca " +
					" where  " +
						" u.id = t.user_id " +
						" and u.class_id <> 0 " +
						" and u.combination_id = mc.id " +
						" and mca.id = mc.campaign_id " +
						" and t.id = u.first_deposit_id " +
						" and t.type_id in (select tt.id from  transaction_types tt where tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + ") " +
						" and t.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) " +
						" and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +


					" union " +

					" select " +
						" count(case when u.writer_id = " + Writer.WRITER_ID_MOBILE + " then u.id else null end) mobile_reg, " +
						" count(u.id) total_reg, " +
						" 0 total_first_dep, " +
						" count(case when mc.campaign_id in (" + freeCampaigns + ") and u.netrefer_user_time is null then u.id else null end) reg_free, " +
						" 0 first_dep_free, " +
						" count(case when mca.SOURCE_ID in (" + marketingSourceIds + ") and u.netrefer_user_time is null then u.id else null end) reg_google, " +
						" 0 first_dep_google, " +
						" count(case when u.netrefer_user_time is not null then u.id else null end) reg_netrefer, " +
						" 0 first_dep_netrefer, " +
						" count(case when mc.campaign_id not in (" + freeCampaigns + ") and mca.SOURCE_ID not in (" + marketingSourceIds + ") and u.netrefer_user_time is null " +
								   " then u.id else null end) reg_media, " +
						" 0 first_dep_media " +
					" from  " +
						" users u, " +
						" marketing_combinations mc, " +
						" marketing_campaigns mca " +
					" where  " +
						" u.combination_id = mc.id " +
						" and u.class_id <> 0 " +
						" and mca.id = mc.campaign_id " +
						" and u.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) " +
					" ) A ";

			pstmt = conn.prepareStatement(sqlCustomersSource);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				long totalReg = rs.getLong("total_reg");
				long totalFstDep = rs.getLong("total_first_dep");

				row = new BaseReportRow();
				row.setRowName("Free (campaigns: 68, 112)");
				row.setRegUsersNumAndPercent(rs.getLong("reg_free"),totalReg);
				row.setFirstDepsNumAndPercent(rs.getLong("first_dep_free"),totalFstDep);
				newCustomersSource.add(row);

				row = new BaseReportRow();
				row.setRowName("Google (Sources: Google, msn, Yahoo_PPC,Yandex,Yottos,7search,Facebook,Vkontakte, Merlin_networks, Baidu");
				row.setRegUsersNumAndPercent(rs.getLong("reg_google"),totalReg);
				row.setFirstDepsNumAndPercent(rs.getLong("first_dep_google"),totalFstDep);
				newCustomersSource.add(row);

				row = new BaseReportRow();
				row.setRowName("Netrefer");
				row.setRegUsersNumAndPercent(rs.getLong("reg_netrefer"),totalReg);
				row.setFirstDepsNumAndPercent(rs.getLong("first_dep_netrefer"),totalFstDep);
				newCustomersSource.add(row);

				row = new BaseReportRow();
				row.setRowName("Media");
				row.setRegUsersNumAndPercent(rs.getLong("reg_media"),totalReg);
				row.setFirstDepsNumAndPercent(rs.getLong("first_dep_media"),totalFstDep);
				newCustomersSource.add(row);


				row = new BaseReportRow();
				row.setRowName("Total");
				row.setRegUsersNum(totalReg);
				row.setRegUsersPercent(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setFirstDepsNum(totalFstDep);
				row.setFirstDepsPercent(ConstantsBase.REPORT_EMPTY_VALUE);
				newCustomersSource.add(row);

				row = new BaseReportRow();
				row.setRowName("New accounts");
				row.setTotalAmountUsd(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setCount(ConstantsBase.REPORT_EMPTY_VALUE);
				row.setHeadcount(rs.getLong("mobile_reg"));
				mobile.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get New Coustomers Per Source",e);
		}


		// / Settled Transactions
		try {
			conn = Utils.getConnection();

			String sqlSettledTran =
				" select  " +
					" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_WITHDRAW + " " +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + " then t.amount * t.rate else 0 end)/100 wtr_amount, " +
					" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_WITHDRAW +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + " then t.id else null end) wtr_count, " +
					" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_WITHDRAW +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + " then t.user_id else null end) wtr_headcount, " +

					" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS + " then t.amount * t.rate else 0 end)/100 dep_cnl_fraud_amount, " +
					" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS + " then t.id else null end) dep_cnl_fraud_count, " +
					" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS + " then t.user_id else null end) dep_cnl_fraud_headcount, " +

					" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M + " then t.amount * t.rate else 0 end)/100 dep_cnl_sup_amount, " +
					" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M + " then t.id else null end) dep_cnl_supp_count, " +
					" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M + " then t.user_id else null end) dep_cnl_sup_headcount, " +

					" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R + " then t.amount * t.rate else 0 end)/100 dep_cnl_mnr_amount, " +
					" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R + " then t.id else null end) dep_cnl_mnr_count, " +
					" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R + " then t.user_id else null end) dep_cnl_mnr_headcount, " +

					" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT + " then t.amount * t.rate else 0 end)/100 dep_cnl_succ_amount, " +
					" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT + " then t.id else null end) dep_cnl_succ_count, " +
					" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
						" and t.status_id = " + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT + " then t.user_id else null end) dep_cnl_succ_headcount " +
				" from  " +
					" users u, " +
					" transaction_types tt, " +
					" transactions t " +
				" where  " +
					" u.id = t.user_id " +
					" and u.class_id <> 0 " +
					" and t.type_id = tt.id " +
					" and t.time_settled between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) ";

			pstmt = conn.prepareStatement(sqlSettledTran);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				row = new BaseReportRow();
				row.setRowName("Executed");
				totalWithdrawals = rs.getDouble("wtr_amount");
				row.setTotalAmountUsd(totalWithdrawals);
				row.setCount(rs.getLong("wtr_count"));
				row.setHeadcount(rs.getLong("wtr_headcount"));
				withdrawals.add(row);

				row = new BaseReportRow();
				row.setRowName("Deposits canceled yesterday 'fraud'");
				row.setTotalAmountUsd(rs.getDouble("dep_cnl_fraud_amount"));
				row.setCount(rs.getLong("dep_cnl_fraud_count"));
				row.setHeadcount(rs.getLong("dep_cnl_fraud_headcount"));
				other.add(row);

				row = new BaseReportRow();
				row.setRowName("Deposits canceled yesterday 'support mistake' (wires/cash)");
				row.setTotalAmountUsd(rs.getDouble("dep_cnl_sup_amount"));
				row.setCount(rs.getLong("dep_cnl_supp_count"));
				row.setHeadcount(rs.getLong("dep_cnl_sup_headcount"));
				other.add(row);

				row = new BaseReportRow();
				row.setRowName("Deposits canceled yesterday 'money not received' (wires/cash)");
				row.setTotalAmountUsd(rs.getDouble("dep_cnl_mnr_amount"));
				row.setCount(rs.getLong("dep_cnl_mnr_count"));
				row.setHeadcount(rs.getLong("dep_cnl_mnr_headcount"));
				other.add(row);

				row = new BaseReportRow();
				row.setRowName("Deposits canceled yesterday 'cancel success deposit'");
				row.setTotalAmountUsd(rs.getDouble("dep_cnl_succ_amount"));
				row.setCount(rs.getLong("dep_cnl_succ_count"));
				row.setHeadcount(rs.getLong("dep_cnl_succ_headcount"));
				other.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Settled Transactions ",e);
		}

		// / Charged Back Transactions
		try {
			conn = Utils.getConnection();

			String sqlChargeBackTran =
				" select " +
					" sum(t.amount * t.rate)/100 cb_amount, " +
					" count(t.id) cb_count, " +
					" count(distinct t.user_id) cb_headcount " +
				" from  " +
					" users u, " +
					" charge_backs cb, " +
					" transactions t " +
				" where  " +
					" u.id = t.user_id " +
					" and u.class_id <> 0 " +
					" and t.charge_back_id = cb.id " +
					" and cb.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) ";

			pstmt = conn.prepareStatement(sqlChargeBackTran);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				row = new BaseReportRow();
				row.setRowName("Deposits charged back yesterday");
				row.setTotalAmountUsd(rs.getDouble("cb_amount"));
				row.setCount(rs.getLong("cb_count"));
				row.setHeadcount(rs.getLong("cb_headcount"));
				other.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Charged Back Transactions ",e);
		}

		// / Created Transactions
		try {
			conn = Utils.getConnection();

			String wireDeposit = TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT + "," +
                                    TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT + "," +
									TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT;
			String cancelBonusStates = ConstantsBase.BONUS_STATE_REFUSED + "," +
											ConstantsBase.BONUS_STATE_CANCELED + "," +
											ConstantsBase.BONUS_STATE_MISSED;

			String sqlCreatedTran =
				" select " +
				   " A.*, " +
				   " case when A.dep_total_amount > 0 then A.dep_sales_amount/A.dep_total_amount*100 else 0 end per_sales_amount, " +
				   " case when A.dep_total_count > 0 then A.dep_sales_count/A.dep_total_count*100 else 0 end per_sales_count, " +
				   " case when A.dep_total_headcount > 0 then A.dep_sales_headcount/A.dep_total_headcount*100 else 0 end per_sales_headcount " +
				" from " +
					" (select  " +
					   	" sum(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " then t.amount * t.rate else 0 end)/100 dep_cc_amount, " +
					   	" count(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " then t.id else null end) dep_cc_count, " +
					   	" count(distinct case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " then t.user_id else null end) dep_cc_headcount, " +

					   	" sum(case when t.type_id in (" + wireDeposit + ") then t.amount * t.rate else 0 end)/100 dep_wire_amount, " +
						" count(case when t.type_id in (" + wireDeposit + ") then t.id else null end) dep_wire_count, " +
						" count(distinct case when t.type_id in (" + wireDeposit + ")  then t.user_id else null end) dep_wire_headcount, " +

						" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
							" and t.type_id not in(" + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + "," + wireDeposit + ") then t.amount * t.rate else 0 end)/100 dep_other_amount, " +
						" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
							" and t.type_id not in(" + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + "," + wireDeposit + ")  then t.id else null end) dep_other_count, " +
						" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT +
							" and t.type_id not in(" + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + "," + wireDeposit + ")  then t.user_id else null end) dep_other_headcount, " +

						" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " then t.amount * t.rate else 0 end)/100 dep_total_amount, " +
						" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + "  then t.id else null end) dep_total_count, " +
						" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + "  then t.user_id else null end) dep_total_headcount, " +

						" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and ti.transaction_id is not null  then t.amount * t.rate else 0 end)/100 dep_sales_amount, " +
						" count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and ti.transaction_id is not null  then t.id else null end) dep_sales_count, " +
						" count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and ti.transaction_id is not null  then t.user_id else null end) dep_sales_headcount, " +

						" sum(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" and bu.bonus_state_id not in (" + cancelBonusStates + ") then t.amount * t.rate else 0 end)/100 dep_bonus_amount, " +
						" count(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" and bu.bonus_state_id not in (" + cancelBonusStates + ") then t.id else null end) dep_bonus_count, " +
						" count(distinct case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" and bu.bonus_state_id not in (" + cancelBonusStates + ") then t.user_id else null end) dep_bonus_headcount, " +

						" sum(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" then t.amount * t.rate else 0 end)/100 wtr_bonus_amount, " +
						" count(case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" then t.id else null end) wtr_bonus_count, " +
						" count(distinct case when t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW + " and t.writer_id != " + Writer.WRITER_ID_AUTO +
							" then t.user_id else null end) wtr_bonus_headcount, " +

						" sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and u.writer_id = " + Writer.WRITER_ID_MOBILE + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id  " +
					    	" then t.amount * t.rate else 0 end)/100 dep_mobile_reg_amount, " +
					    " count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and u.writer_id = " + Writer.WRITER_ID_MOBILE + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id  " +
					        " then t.id else null end) dep_mobile_reg_count, " +
					    " count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and u.writer_id = " + Writer.WRITER_ID_MOBILE + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id   " +
					        " then t.user_id else null end) dep_mobile_reg_headcount, " +

					    " sum(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id  " +
					             " then t.amount * t.rate else 0 end)/100 dep_mobile_amount, " +
					    " count(case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id  " +
					               " then t.id else null end) dep_mobile_count, " +
					    " count(distinct case when tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " and t.writer_id = " + Writer.WRITER_ID_MOBILE + " and u.first_deposit_id = t.id " +
					                        " then t.user_id else null end) dep_mobile_headcount " +
					" from  " +
						" users u," +
						" transaction_types tt, " +
						" transactions t " +
							" left join transactions_issues ti on ti.transaction_id = t.id " +
							" left join bonus_users bu on t.bonus_user_id = bu.id " +
					" where  " +
						" u.id = t.user_id " +
						" and u.class_id <> 0 " +
						" and t.type_id = tt.id " +
						" and t.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) " +
						" and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
					" )A ";

			pstmt = conn.prepareStatement(sqlCreatedTran);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				row = new BaseReportRow();
				row.setRowName("CC deposit");
				row.setTotalAmountUsd(rs.getDouble("dep_cc_amount"));
				row.setCount(rs.getLong("dep_cc_count"));
				row.setHeadcount(rs.getLong("dep_cc_headcount"));
				depositTypes.add(row);

				row = new BaseReportRow();
				row.setRowName("Wire deposit");
				row.setTotalAmountUsd(rs.getDouble("dep_wire_amount"));
				row.setCount(rs.getLong("dep_wire_count"));
				row.setHeadcount(rs.getLong("dep_wire_headcount"));
				depositTypes.add(row);

				row = new BaseReportRow();
				row.setRowName("Other");
				row.setTotalAmountUsd(rs.getDouble("dep_other_amount"));
				row.setCount(rs.getLong("dep_other_count"));
				row.setHeadcount(rs.getLong("dep_other_headcount"));
				depositTypes.add(row);

				row = new BaseReportRow();
				row.setRowName("Total deposits (all)");
				totalDeposits = rs.getDouble("dep_total_amount");
				row.setTotalAmountUsd(totalDeposits);
				row.setCount(rs.getLong("dep_total_count"));
				row.setHeadcount(rs.getLong("dep_total_headcount"));
				depositTotals.add(row);

				row = new BaseReportRow();
				row.setRowName("Total SALES dep");
				row.setTotalAmountUsd(rs.getDouble("dep_sales_amount"));
				row.setCount(rs.getLong("dep_sales_count"));
				row.setHeadcount(rs.getLong("dep_sales_headcount"));
				depositTotals.add(row);

				row = new BaseReportRow();
				row.setRowName("% SALES from all");
				row.setTotalAmountUsd(rs.getDouble("per_sales_amount"));
				row.setCount(rs.getLong("per_sales_count"));
				row.setHeadcount(rs.getLong("per_sales_headcount"));
				row.setPercentStr();
				depositTotals.add(row);

				row = new BaseReportRow();
				row.setRowName("Bonus deposits");
				row.setTotalAmountUsd(rs.getDouble("dep_bonus_amount"));
				row.setCount(rs.getLong("dep_bonus_count"));
				row.setHeadcount(rs.getLong("dep_bonus_headcount"));
				other.add(row);

				row = new BaseReportRow();
				row.setRowName("Bonus withdrawals");
				row.setTotalAmountUsd(rs.getDouble("wtr_bonus_amount"));
				row.setCount(rs.getLong("wtr_bonus_count"));
				row.setHeadcount(rs.getLong("wtr_bonus_headcount"));
				other.add(row);

				row = new BaseReportRow();
				row.setRowName("New depositors");
				row.setTotalAmountUsd(rs.getDouble("dep_mobile_reg_amount"));
				row.setCount(rs.getLong("dep_mobile_reg_count"));
				row.setHeadcount(rs.getLong("dep_mobile_reg_headcount"));
				mobile.add(row);

				row = new BaseReportRow();
				row.setRowName("Deposits");
				row.setTotalAmountUsd(rs.getDouble("dep_mobile_amount"));
				row.setCount(rs.getLong("dep_mobile_count"));
				row.setHeadcount(rs.getLong("dep_mobile_headcount"));
				mobile.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Created Transactions ",e);
		}

		// / Investments
		try {
			conn = Utils.getConnection();

			String sqlInvestmentsTran =
				" select " +
					" sum(i.amount * i.rate)/100 inv_total_turnover, " +
					" sum(i.house_result * i.rate)/100 inv_total_house_res, " +
					" count(i.id) inv_total_count, " +
					" count(distinct i.user_id) inv_total_headcount, " +

					" sum(case when ot.product_type_id = " + Opportunity.TYPE_REGULAR + " then i.amount * i.rate else 0 end)/100 inv_bin_turnover, " +
					" sum(case when ot.product_type_id = " + Opportunity.TYPE_REGULAR + " then i.house_result * i.rate else 0 end)/100 inv_bin_house_res, " +
					" count(case when ot.product_type_id = " + Opportunity.TYPE_REGULAR + " then i.id else null end) inv_bin_count, " +
					" count(distinct case when ot.product_type_id = " + Opportunity.TYPE_REGULAR + " then i.user_id else null end) inv_bin_headcount, " +

					" sum(case when ot.product_type_id = " + Opportunity.TYPE_OPTION_PLUS + " then i.amount * i.rate else 0 end)/100 inv_oplus_turnover, " +
					" sum(case when ot.product_type_id = " + Opportunity.TYPE_OPTION_PLUS + " then i.house_result * i.rate else 0 end)/100 inv_oplus_house_res, " +
					" count(case when ot.product_type_id = " + Opportunity.TYPE_OPTION_PLUS + " then i.id else null end) inv_oplus_count, " +
					" count(distinct case when ot.product_type_id = " + Opportunity.TYPE_OPTION_PLUS + " then i.user_id else null end) inv_oplus_headcount, " +

					" sum(case when ot.product_type_id = " + Opportunity.TYPE_ONE_TOUCH + " then i.amount * i.rate else 0 end)/100 inv_1t_turnover, " +
					" sum(case when ot.product_type_id = " + Opportunity.TYPE_ONE_TOUCH + " then i.house_result * i.rate else 0 end)/100 inv_1t_house_res, " +
					" count(case when ot.product_type_id = " + Opportunity.TYPE_ONE_TOUCH + " then i.id else null end) inv_1t_count, " +
					" count(distinct case when ot.product_type_id = " + Opportunity.TYPE_ONE_TOUCH + " then i.user_id else null end) inv_1t_headcount, " +

					" sum(case when ot.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + " then i.amount * i.rate else 0 end)/100 inv_0100_turnover, " +
					" sum(case when ot.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + " then i.house_result * i.rate else 0 end)/100 inv_0100_house_res, " +
					" count(case when ot.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + " then i.id else null end) inv_0100_count, " +
					" count(distinct case when ot.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + " then i.user_id else null end) inv_0100_headcount " +
				" from  " +
					" users u," +
					" investments i, " +
					" opportunities o, " +
					" opportunity_types ot " +
				" where  " +
					" i.opportunity_id = o.id " +
					" and i.user_id = u.id " +
					" and u.class_id <> 0 " +
					" and i.is_canceled = 0 " +
					" and o.opportunity_type_id = ot.id " +
					" and i.time_settled between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) ";

			pstmt = conn.prepareStatement(sqlInvestmentsTran);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				row = new BaseReportRow();
				row.setRowName("Binary");
				row.setTotalAmountUsd(rs.getDouble("inv_bin_turnover"));
				row.setProfitUsd(rs.getDouble("inv_bin_house_res"));
				row.setCount(rs.getLong("inv_bin_count"));
				row.setHeadcount(rs.getLong("inv_bin_headcount"));
				investments.add(row);

				row = new BaseReportRow();
				row.setRowName("Option+");
				row.setTotalAmountUsd(rs.getDouble("inv_oplus_turnover"));
				row.setProfitUsd(rs.getDouble("inv_oplus_house_res"));
				row.setCount(rs.getLong("inv_oplus_count"));
				row.setHeadcount(rs.getLong("inv_oplus_headcount"));
				investments.add(row);

				row = new BaseReportRow();
				row.setRowName("1T");
				row.setTotalAmountUsd(rs.getDouble("inv_1t_turnover"));
				row.setProfitUsd(rs.getDouble("inv_1t_house_res"));
				row.setCount(rs.getLong("inv_1t_count"));
				row.setHeadcount(rs.getLong("inv_1t_headcount"));
				investments.add(row);

				row = new BaseReportRow();
				row.setRowName("BInary 0-100");
				row.setTotalAmountUsd(rs.getDouble("inv_0100_turnover"));
				row.setProfitUsd(rs.getDouble("inv_0100_house_res"));
				row.setCount(rs.getLong("inv_0100_count"));
				row.setHeadcount(rs.getLong("inv_0100_headcount"));
				investments.add(row);

				row = new BaseReportRow();
				row.setRowName("Total");
				row.setTotalAmountUsd(rs.getDouble("inv_total_turnover"));
				row.setProfitUsd(rs.getDouble("inv_total_house_res"));
				row.setCount(rs.getLong("inv_total_count"));
				row.setHeadcount(rs.getLong("inv_total_headcount"));
				investments.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Investments",e);
		}

		// / Mobile Created Investment
		try {
			conn = Utils.getConnection();

			String sqlInvestmentsTran =
				" select " +
					" sum(i.amount * i.rate)/100 inv_mobile_turnover, " +
					" sum(i.house_result * i.rate)/100 inv_mobile_house_res, " +
					" count(i.id) inv_mobile_count, " +
					" count(distinct i.user_id) inv_mobile_headcount " +
				" from  " +
					" users u, " +
					" investments i " +
				" where  " +
					" u.id = i.user_id " +
					" and u.class_id <> 0 " +
					" and i.is_canceled = 0" +
					" and i.writer_id = " + Writer.WRITER_ID_MOBILE + " " +
					" and i.time_created between trunc(sysdate - " + daysBefore + ") and trunc(sysdate) ";

			pstmt = conn.prepareStatement(sqlInvestmentsTran);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				row = new BaseReportRow();
				row.setRowName("Investments");
				row.setTotalAmountUsd(rs.getDouble("inv_mobile_turnover"));
				row.setCount(rs.getLong("inv_mobile_count"));
				row.setHeadcount(rs.getLong("inv_mobile_headcount"));
				mobile.add(row);
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get Mobile New Customers",e);
		}


		log.log(Level.DEBUG,"finished all queries ");
		// Now lets send emails with the report :

		log.log(Level.DEBUG,"going to send emails ");

		String templateName = "report.html";
		String netInOut = Utils.displayAmount(totalDeposits - totalWithdrawals);

		sendEmail(depositTypes, depositTotals, withdrawals, netInOut, other, investments, newCustomersPerSkin, newCustomersTotals, newCustomersSource,
					mobile,"Report", templateName, subject);

		log.log(Level.DEBUG,"finished to send emails");

		try {
			rs.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			pstmt.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			conn.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}
	}

	private static void sendEmail(Cash cash, Investments invests, Investments investsOppPlus, Investments investsNoOppPlus, Other other,ArrayList<MarketingCampaignDr> campaignList,
			String header, String file,String subject, int runType) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		// put all cash params
		params.put("cash",cash);

		params.put("invest",invests);

		params.put("investsOppPlus",investsOppPlus);

		params.put("investsNoOppPlus",investsNoOppPlus);

		params.put("other",other);

		params.put("campaigns",campaignList);

		params.put("header",header);

		try {
			initEmail(subject,file, runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, "emails").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	private static void sendEmail(ArrayList<BaseReportRow> depositTypes,
									ArrayList<BaseReportRow> depositTotals,
									ArrayList<BaseReportRow> withdrawals,
									String netInOut,
									ArrayList<BaseReportRow> other,
									ArrayList<BaseReportRow> investments,
									ArrayList<BaseReportRow> newCustomersPerSkin,
									ArrayList<BaseReportRow> newCustomersTotals,
									ArrayList<BaseReportRow> newCustomersSource,
									ArrayList<BaseReportRow> mobile,
									String header, String file,String subject) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		// put all cash params
		params.put("depositTypes",depositTypes);

		params.put("depositTotals",depositTotals);

		params.put("withdrawals",withdrawals);

		params.put("netInOut",netInOut);

		params.put("other",other);

		params.put("investments",investments);

		params.put("newCustomersPerSkin",newCustomersPerSkin);

		params.put("newCustomersTotals",newCustomersTotals);

		params.put("newCustomersSource",newCustomersSource);

		params.put("mobile",mobile);

		params.put("header",header);

		try {
			initEmail(subject,file, RUN_TYPE_ALL_SKINS);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(RUN_TYPE_AO_SKINS, "emails.managers").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	@SuppressWarnings("unchecked")
	private static void sendEmailMarketBreakDown(String header, int runType) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		// put all investments params
		log.info("number of markets " + marktes.size());

		ArrayList<Market> marketsList = new ArrayList<Market>(marktes.values());

		Collections.sort(marketsList,new GroupComparator());

		params.put("markets",new ArrayList<Market>(marketsList));
		params.put("header", header);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String subject = getSubjectByRunType(runType) + "daily report market breakdown:" + sdf.format(cal.getTime());

		try {
			initEmail(subject,"report_market.html", runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, "emails.market.breakdown").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}


	@SuppressWarnings("unchecked")
	private static void sendEmailDateRangeMarketBreakDown(DateRange dr, String header, int runType) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		// put all investments params
		log.info("number of markets " + marktes.size());

		ArrayList<Market> marketsList = new ArrayList<Market>(marktes.values());

		Collections.sort(marketsList,new GroupComparator());

		params.put("markets",new ArrayList<Market>(marketsList));
		params.put("header",header);

		String subject = getSubjectByRunType(runType) + "report market breakdown:" + dr.getDateRange();

		try {
			initEmail(subject,"report_market_date_range.html", runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, "emails").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	@SuppressWarnings("unchecked")
	private static void sendEmailDateRangeMarketBreakDownSkinsPartition(DateRange dr, ArrayList<Skins> skins) {
		HashMap<String, ArrayList<Skins>> params = new HashMap<String, ArrayList<Skins>>();

		// put all investments params per skin and currency

		for( Skins skin : skins ) {
			for ( SkinCurrencies sc : skin.getSkinCurrenciesList() ) {
				log.info("skinid: " + skin.getId() + " currency: " + sc.getCode() + " number of markets " + sc.getMarktes().size());
				ArrayList<Market> marketsList = new ArrayList<Market>(sc.getMarktes().values());
				Collections.sort(marketsList,new GroupComparator());
				sc.setMarketsList(new ArrayList<Market>(marketsList));   // save markets list
			}
		}

		params.put("skins",skins);

		String subject = "All skins report market breakdown - skins partition:" + dr.getDateRange();

		try {
			initEmail(subject,"report_market_date_range_skinsPartition.html", RUN_TYPE_ALL_SKINS);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(RUN_TYPE_ALL_SKINS, "emails").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	private static void sendEmailNeedFiles(ArrayList<NeedFiles> needFilesArr, int deposits) {

		// put all investments params
		log.info("number of deposits that need files " + needFilesArr.size());

		HashMap<String, ArrayList> params = new HashMap<String, ArrayList>();

		params.put("needFilesArr",needFilesArr);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String subject = "etrader daily report depositors above " + deposits + " " + sdf.format(cal.getTime());

		try {
			initEmail(subject,"report_need_files.html", RUN_TYPE_SKIN_ETRADER);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = Utils.getProperty("emails.need.files.et").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	private static void sendEmailNonDepositors(ArrayList<NeedFiles> needFilesArr, Other other,
			String header, int runType) {

		// put all investments params
		log.info("number of non depositors  " + needFilesArr.size());

		HashMap<String, Object> params = new HashMap<String, Object>();

		params.put("needFilesArr",needFilesArr);
		params.put("other",other);
		params.put("header",header);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String subject = getSubjectByRunType(runType) + "daily non depositors " + sdf.format(cal.getTime());

		try {
			initEmail(subject,"report_non_depositors.html", runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, "emails.non.depositors").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	private static void sendEmailNonDepositors20Days(ArrayList<NeedFiles> needFilesArr, Other other,
			String header, int runType) {

		// put all investments params
		log.info("number of non depositors 20 days" + needFilesArr.size());

		HashMap<String, Object> params = new HashMap<String, Object>();

		params.put("needFilesArr",needFilesArr);
		params.put("other",other);
		params.put("header",header);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String subject = getSubjectByRunType(runType) + "daily no 2nd deposit 20 days " + sdf.format(cal.getTime());

		try {
			initEmail(subject,"report_non_depositors.html", runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, "emails.non.depositors.20.days").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}


	private static void sendSkinsPartitionEmail(ArrayList<Skins> skins, String file, String subject) {

		HashMap<String, ArrayList<Skins>> params = new HashMap<String, ArrayList<Skins>>();

		// put skins list params
		params.put("skins",skins);

		try {
			initEmail(subject,file, RUN_TYPE_ALL_SKINS);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(RUN_TYPE_ALL_SKINS, "emails").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}

	private static void initEmail(String subject, String name, int runType) throws Exception {

		// get the emailTemplate
		emailTemplate = getEmailTemplate(name);

		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject",subject);

		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			emailProperties.put("from",Utils.getProperty("email.from.et"));
		} else {
			emailProperties.put("from",Utils.getProperty("email.from.ao"));
		}

	}

	private static Template getEmailTemplate(String fileName) throws Exception {
		try {
			// String templatePath = CommonUtil.getProperty("templates.path");
			Properties props = new Properties();
			props.setProperty("file.resource.loader.path",Utils.getProperty("email.template.path"));
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			Velocity.init(props);
			return Velocity.getTemplate(fileName,"iso-8859-1");

		} catch (Exception ex) {
			log.fatal("!!! ERROR >> Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}

	public static String getEmailBody(HashMap params) throws Exception {

		// set the context and the parameters
		VelocityContext context = new VelocityContext();

		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");

		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		emailTemplate.merge(context,sw);

		return sw.toString();
	}


	*//**
	 * Get sql conditions for the run
	 * @param runType
	 * 		type of the run we needed
	 * @return
	 * 		SQL String
	 *//*
	private static String getSqlByRunType(int runType) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return " and u.skin_id = 1 ";
		}
		else if ( runType == RUN_TYPE_AO_SKINS ) {
			return " and u.skin_id <> 1 ";
		}
		return "";
	}


	*//**
	 * Get sql conditions for the run
	 * @param runType type of the run we needed
	 * @param user 	user string
	 * @return
	 * 		SQL String
	 *//*
	private static String getSqlByRunType(int runType, String user) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return " and " + user + ".skin_id = 1 ";
		}
		else if ( runType == RUN_TYPE_AO_SKINS ) {
			return " and " + user + ".skin_id <> 1 ";
		}
		return "";
	}


	*//**
	 * Get sql amount condition for the run
	 * @param runType
	 * 		type of the run we needed
	 * @return
	 *//*
	private static String getSqlAmountSumByRunType(int runType) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return " sum(amount/100) ";
		}
		else {  // UDS convert
			return " sum((amount/100) * rate) ";
		}
	}

	*//**
	 * Get currency str by run type
	 * @param runType
	 * 		type of the run we needed
	 * @return
	 *//*
	private static String getCurrencyByRunType(int runType) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return Utils.CURRENCY_ILS;
		}
		else {  // UDS convert
			return Utils.CURRENCY_USD;
		}
	}

	*//**
	 * Get Header for the page by run type
	 * @param runType
	 * 		type of the run we needed
	 * @return
	 *//*
	private static String getPageHeaderByRunType(int runType) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return "etrader Skin Report";
		}
		else if ( runType == RUN_TYPE_AO_SKINS){
			return "anyoption Skins Report";
		}
		else {
			return "All Skins Report";
		}
	}

	*//**
	 * Get email subject by run type
	 * @param runType
	 * 		type of the run we needed
	 * @return
	 *//*
	private static String getSubjectByRunType(int runType) {
		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
			return "etrader ";
		}
		else if ( runType == RUN_TYPE_AO_SKINS){
			return "anyoption ";
		}
		else {
			return "All Skins ";
		}
	}

	*//**
	 * Get sql for skin and currency partition
	 * @param skinId
	 * 		user skin id
	 * @param currencyId
	 * 		user currency id
	 * @return
	 *//*
	public static String getSqlBySkinPartintion(long skinId, long currencyId) {
		return " and u.skin_id = " + skinId + " and u.currency_id = " + currencyId + " ";
	}


	*//**
	 * Return email "to" property by runType
	 * @param runType
	 * @return
	 * 		email property
	 *//*
	public static String getEmailByRunType(int runType, String generalKey) {

		String toEmail = "";

		switch(runType){
			case RUN_TYPE_ALL_SKINS :
								toEmail = Utils.getProperty(generalKey + ".et");
								toEmail += ";"+Utils.getProperty(generalKey + ".ao");
								break;
			case RUN_TYPE_AO_SKINS:
								toEmail = Utils.getProperty(generalKey + ".ao");
								break;
			case RUN_TYPE_SKIN_ETRADER:
								toEmail = Utils.getProperty(generalKey + ".et");
								break;
		}

		return toEmail;
	}


	*//**
	 * Return skins list,
	 * For each skin, get currencies list
	 * @return
	 * 		ArrayList of Skins instances
	 *//*
	private static ArrayList<Skins> getAllSkins() {

		ArrayList<Skins> skins = new ArrayList<Skins>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		log.debug("Starting to get all skins with currencies list");

		// Get all skins and skinCurrencies list
		try {
			conn = Utils.getConnection();
			String sqlSkins = "select * from skins ";

			pstmt = conn.prepareStatement(sqlSkins);
			rs = pstmt.executeQuery();

			while ( rs.next() ) {
				Skins s = new Skins();
				s.setId(rs.getInt("id"));
				s.setName(rs.getString("name"));
				s.setDisplayName(rs.getString("display_name"));
				skins.add(s);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get skins list");
		}

		// set skin currencies for each skin
		for( Skins skin : skins ) {

			ArrayList<SkinCurrencies> skinCurrenciesList = new ArrayList<SkinCurrencies>();
			try {
				String sqlCurrencies = "select s.currency_id, c.code " +
				 					   "from skin_currencies s, currencies c " +
				 					   "where s.currency_id = c.id and s.skin_id = ? ";

				pstmt = conn.prepareStatement(sqlCurrencies);
				pstmt.setLong(1, skin.getId());
				rs = pstmt.executeQuery();

				while ( rs.next() ) {
					SkinCurrencies sc = new SkinCurrencies();
					sc.setCurrencyId(rs.getLong("currency_id"));
					sc.setCode(rs.getString("code"));
					skinCurrenciesList.add(sc);
				}
				skin.setSkinCurrenciesList(skinCurrenciesList);

			} catch (Exception e) {
				log.log(Level.FATAL,"Can't get currencies list for skin : " + skin.getId() );
			}
		}

		log.debug("Get all skins with currencies list finished!");

		try {
			rs.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			pstmt.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		try {
			conn.close();
		} catch (Exception e) {
			log.log(Level.ERROR,"Can't close",e);
		}

		return skins;

	}

	*//**
	 * This report, send all users with duplicate email's
	 * First query return all users with duplicate emails that created today(u1 and also u2),
	 * Second query return all users with duplicate emails, u1 created today and u2 not created today.
	 *//*
	private static void SendDailyReportDupicateEmail(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		String dupEmail;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		String userName1, userName2;
		HashMap<String, String> usersDupEmail = new HashMap<String, String>();

		try {

			conn = Utils.getConnection();
			String sqlDupicateEmail ="SELECT " +
										"email_user.*, " +
									 	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as user1_suc_deposit " +
									 "FROM " +
										 "(select distinct u1.id user1_id, u2.id user2_id, u1.email email, " +
													"u1.user_name username1, u2.user_name username2, " +
													SecurityCheck.USER_LIST_TYPE_NEW_USERS_ID + " user_list_type " +
										  "from users u1, users u2 " +
										  "where u1.email = u2.email and u1.id <> u2.id " +
											     "and u1.class_id <> 0 and u2.class_id <> 0 " +
											     getSqlByRunType(runType,"u1") +
											     getSqlByRunType(runType,"u2") +
											     "and to_char(u1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
											     "and to_char(u2.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +

										  "UNION "+

										  "select distinct u1.id user1_id, u2.id user2_id, u1.email  email, " +
										  			"u1.user_name username1, u2.user_name username2, " +
										  			SecurityCheck.USER_LIST_TYPE_OLD_USERS_ID + " user_list_type " +
										  "from users u1, users u2 "+
										  "where u1.email = u2.email and u1.id <> u2.id " +
										  		 "and u1.class_id <> 0 and u2.class_id <> 0 " +
											     getSqlByRunType(runType,"u1") +
											     getSqlByRunType(runType,"u2") +
											     "and trunc(u1.time_created) = trunc( sysdate - 1) " +
											     "and trunc(u2.time_created) < trunc(sysdate - 1) " +
											     "and NOT (u2.is_false_account = 1 OR " +
											          	  "u2.is_active = 0 OR " +
											          	  "EXISTS (select 1 " +
												          		   "from transactions t " +
												          		   "where t.charge_back_id is not null and " +
												          		   		  "t.charge_back_id <> 0 and " +
												          		   		  "t.user_id = u2.id) " +
								          		   	") " +

									     "UNION "+

										  "select distinct u1.id user1_id, u2.id user2_id, u1.email  email, " +
										  			"u1.user_name username1, u2.user_name username2, " +
										  			SecurityCheck.USER_LIST_TYPE_BLACK_LIST_ID + " user_list_type " +
										  "from users u1, users u2 "+
										  "where u1.email = u2.email and u1.id <> u2.id " +
										  		 "and u1.class_id <> 0 and u2.class_id <> 0 " +
											     getSqlByRunType(runType,"u1") +
											     getSqlByRunType(runType,"u2") +
											     "and trunc(u1.time_created) = trunc( sysdate - 1) " +
											     "and trunc(u2.time_created) < trunc(sysdate - 1) " +
											     "and (u2.is_false_account = 1 OR " +
										          	  "u2.is_active = 0 OR " +
										          	  "EXISTS (select 1 " +
											          		   "from transactions t " +
											          		   "where t.charge_back_id is not null and " +
											          		   		  "t.charge_back_id <> 0 and " +
											          		   		  "t.user_id = u2.id) " +
								          		   	") " +

										   "order by user1_id,user2_id) email_user " +

									"LEFT JOIN (SELECT " +
													"t.user_id " +
												"FROM " +
													"transactions t, transaction_types tt " +
												"WHERE " +
													"t.type_id = tt.id " +
													"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
													"AND tt.class_type =  1 " +
												"GROUP BY t.user_id ) u_suc_deposit ON email_user.user1_id = u_suc_deposit.user_id ";

			pstmt = conn.prepareStatement(sqlDupicateEmail);

			rs = pstmt.executeQuery();

			while(rs.next()) {
				dupEmail = rs.getString("email");
				user1Id = rs.getLong("user1_id");
				user2Id = rs.getLong("user2_id");
				user1IdString = Long.toString(user1Id);
				user2IdString = Long.toString(user2Id);
				userName1 = rs.getString("username1");
				userName2 = rs.getString("username2");


				// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated Email.
				if (usersDupEmail.containsKey(user2IdString + ';' + user1IdString) == false ||
						!usersDupEmail.get(user2IdString + ';' + user1IdString).equals(dupEmail)) {

					usersDupEmail.put(user1IdString + ';' + user2IdString, dupEmail);

					SecurityCheck temp = new SecurityCheck();
					temp.setEmail(dupEmail);
					temp.setUserId(user1Id);
					temp.setUserId2(user2Id);
					temp.setUsername(userName1);
					temp.setUserName2(userName2);

					int userListTypeId = rs.getInt("user_list_type");

					switch (userListTypeId) {
						case SecurityCheck.USER_LIST_TYPE_NEW_USERS_ID:
							temp.setUserListType(SecurityCheck.USER_LIST_TYPE_NEW_USERS);
							break;
						case SecurityCheck.USER_LIST_TYPE_OLD_USERS_ID:
							temp.setUserListType(SecurityCheck.USER_LIST_TYPE_OLD_USERS);
							break;
						case SecurityCheck.USER_LIST_TYPE_BLACK_LIST_ID:
							temp.setUserListType(SecurityCheck.USER_LIST_TYPE_BLACK_LIST);
							break;
					}

					temp.setSucceedDeposit(rs.getString("user1_suc_deposit"));
					users.add(temp);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get for duplicate email's report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "daily - duplicate emails", getPageHeaderByRunType(runType),runType,"report_duplicate_email.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	*//**
	 * this report, send all users with ips like those of users in black list
	 *
	 *//*
	private static void SendDailyReportDupicateIp(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		String dupIp;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		String userName1, userName2;
		HashMap<String, String> usersDupIp = new HashMap<String, String>();

		String omittedIp = "";

		// handle omitted ip's
		String[] omittedIpArr = Utils.getProperty("security.check.omitted.ip").split(";");
		for (String ip : omittedIpArr) {
			omittedIp += "'" + ip + "',";
		}
		omittedIp += "'" + SecurityCheck.IP_NOT_FOUND + "'";

		try {

			conn = Utils.getConnection();
			String sqlDupicateIp = "select distinct u1.id user1_id, u2.id user2_id, u1.user_name username1, u2.user_name username2, u1.ip ip," +
										"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as user1_suc_deposit " +
								   "from issues i , issue_actions ia , users u1 " +
								   			"LEFT JOIN (SELECT " +
								   							"t.user_id " +
								   						"FROM " +
								   							"transactions t, transaction_types tt " +
								   						"WHERE " +
								   							"t.type_id = tt.id " +
								   							"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
								   							"AND tt.class_type =  1 " +
								   						"GROUP BY t.user_id ) u_suc_deposit ON u1.id = u_suc_deposit.user_id, " +
								   "users u2 " +
								   "where u1.ip = u2.ip and u1.id <> u2.id " +
									      "and u1.class_id <> 0 and u2.class_id <> 0 " +
										  getSqlByRunType(runType,"u1") +
										  getSqlByRunType(runType,"u2") +
									      "and u1.ip not in ("+ omittedIp +") " +
									      "and (to_char(u1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') or " +
									      		"to_char(u1.time_modified,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
									      "and u2.id = i.user_id " +
									      "and i.id = ia.issue_id " +
									      "and ia.issue_action_type_id in ( 9 , 10 ) " +
									      "and u2.is_active = 0 " +
									      "and u2.skin_id <> 15 " +
									      "and u1.skin_id <> 15 " +

//									      "and to_char(u2.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
//
//									"UNION " +
//
//									"select distinct u1.id user1_id, u2.id user2_id, u1.ip ip " +
//									"from users u1, users u2 " +
//									"where u1.ip = u2.ip and u1.id <> u2.id " +
//									       "and u1.class_id <> 0 and u2.class_id <> 0 " +
//										   getSqlByRunType(runType,"u1") +
//										   getSqlByRunType(runType,"u2") +
//									       "and u1.ip not in ("+ omittedIp +") " +
//									       "and trunc(u2.time_created) < trunc(sysdate - 1) " +
//									       "and trunc(u1.time_created) = trunc( sysdate - 1) " +

									 "order by user1_id,user2_id";

			pstmt = conn.prepareStatement(sqlDupicateIp);

			rs = pstmt.executeQuery();

			while(rs.next()) {

				dupIp = rs.getString("ip");
				user1Id = rs.getLong("user1_id");
				user2Id = rs.getLong("user2_id");
				userName1 = rs.getString("username1");
				userName2 = rs.getString("username2");
				user1IdString = Long.toString(user1Id);
				user2IdString = Long.toString(user2Id);

				// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated ip.
				if ((usersDupIp.containsKey(user2IdString + ';' + user1IdString) == false ||
						!usersDupIp.get(user2IdString + ';' + user1IdString).equals(dupIp)) &&
						!dupIp.equals(ConstantsBase.CALL_CENTER_IP)) {

					usersDupIp.put(user1IdString + ';' + user2IdString, dupIp);

					SecurityCheck temp = new SecurityCheck();
					temp.setIp(dupIp);
					temp.setUserId(user1Id);
					temp.setUserId2(user2Id);
					temp.setUsername(userName1);
					temp.setUserName2(userName2);
					temp.setSucceedDeposit(rs.getString("user1_suc_deposit"));
					users.add(temp);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get for duplicate ip's report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "daily - duplicate ips", getPageHeaderByRunType(runType),runType,"report_duplicate_ip.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}


	*//**
	 * this report, send all users that have transactions with the same ip's and from different users
	 * First query return all users with transactions that created today(t1 and also t2),
	 * Second query return all users with transactions that, t1 created today and t2 not created today.
	 * @param runType
	 *//*
	private static void SendDailyReportTrxDuplicateIP(int runType){

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		String dupIp;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		HashMap<String, String> usersDupIp = new HashMap<String, String>();

		String omittedIp="";
		String[] omittedIpArr = Utils.getProperty("security.check.omitted.ip").split(";");
		for (String ip : omittedIpArr) {
			omittedIp += "'" + ip + "',";
		}
		omittedIp += "'" + SecurityCheck.IP_NOT_FOUND + "'";

		try {

			conn = Utils.getConnection();

			String sql = "select distinct t1.user_id user1_id, t2.user_id user2_id, t1.ip ip " +
						 "from transactions t1, transactions t2 " +
			             "where " +
								 "t1.ip = t2.ip and t1.user_id <> t2.user_id " +
								 "and to_char(t1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
								 "and to_char(t2.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
								 "and exists ( select 1 " +
								 			   "from transactions t, transaction_types tt " +
								 			   "where t.id = t1.id and t.type_id = tt.id " +
								 			   "and tt.class_type = 1 and tt.is_risky = 1 ) " +

								 "and exists ( select 1 " +
								 			   "from transactions t, transaction_types tt " +
								 			   "where t.id = t2.id and t.type_id = tt.id " +
								 			   "and tt.class_type = 1 and tt.is_risky = 1 ) " +

								 "and exists ( select 1 " +
			                                  "from users u where u.id = t1.user_id " +
			                                  "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and exists ( select 1 " +
			                                  "from users u where u.id = t2.user_id " +
			                                  "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and t1.ip not in ("+ omittedIp +") " +

						 "UNION " +

						 "select distinct t1.user_id user1_id, t2.user_id user2_id, t1.ip ip " +
						 "from transactions t1, transactions t2 " +
						 "where " +
								 "t1.ip = t2.ip and t1.user_id <> t2.user_id " +
							     "and trunc(t2.time_created) < trunc(sysdate - 1) " +
							     "and trunc(t1.time_created) = trunc(sysdate - 1) " +
							     "and exists ( select 1 " +
								 			   "from transactions t, transaction_types tt " +
								 			   "where t.id = t1.id and t.type_id = tt.id " +
								 			   "and tt.class_type = 1 and tt.is_risky = 1 ) " +

								 "and exists ( select 1 " +
								 			   "from transactions t, transaction_types tt " +
								 			   "where t.id = t2.id and t.type_id = tt.id " +
								 			   "and tt.class_type = 1 and tt.is_risky = 1 ) " +

								 "and exists ( select 1 " +
			                                  "from users u where u.id = t1.user_id " +
			                                  "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and exists ( select 1 " +
			                                  "from users u where u.id = t2.user_id " +
			                                  "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and t1.ip not in ("+ omittedIp +") " +


						 "order by user1_id,user2_id";

		pstmt = conn.prepareStatement(sql);

		rs = pstmt.executeQuery();

		while(rs.next()) {

			dupIp = rs.getString("ip");
			user1Id = rs.getLong("user1_id");
			user2Id = rs.getLong("user2_id");
			user1IdString = Long.toString(user1Id);
			user2IdString = Long.toString(user2Id);

			// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated ip.
			if (usersDupIp.containsKey(user2IdString + ';' + user1IdString) == false ||
					!usersDupIp.get(user2IdString + ';' + user1IdString).equals(dupIp)) {

				usersDupIp.put(user1IdString + ';' + user2IdString, dupIp);

				SecurityCheck temp = new SecurityCheck();
				temp.setIp(dupIp);
				temp.setUserId(user1Id);
				temp.setUserId2(user2Id);
				users.add(temp);
			}
		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users, for transactions of different users with same ips " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close resultSet",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close statement",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close connection",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "transactions of different users with same ips ", getPageHeaderByRunType(runType),runType,"report_duplicate_ip.html", SECURITY_MAILING_LIST, true);


		log.log(Level.DEBUG,"finished to send emails");
	}

	*//**
	 * This report, send all users that have investments with the same ip's and from different users
	 * First query return all users with investments that created today(i1 and also i2),
	 * Second query return all users with investments that, i1 created today and i2 not created today.
	 * @param runType
	 *//*
	private static void SendDailyReportInvsDuplicateIP(int runType){

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		String dupIp;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		HashMap<String, String> usersDupIp = new HashMap<String, String>();

		String omittedIp="";
		String[] omittedIpArr = Utils.getProperty("security.check.omitted.ip").split(";");
		for (String ip : omittedIpArr) {
			omittedIp += "'" + ip + "',";
		}
		omittedIp += "'" + SecurityCheck.IP_NOT_FOUND + "'";

		try {

			conn = Utils.getConnection();

			String sql = "select distinct i1.user_id user1_id, i2.user_id user2_id, i1.ip ip " +
						 "from investments i1, investments i2 " +
                         "where " +
								 "i1.ip = i2.ip and i1.user_id <> i2.user_id " +
								 "and to_char(i1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
								 "and to_char(i2.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
								 "and exists ( select 1 " +
		                                       "from users u where u.id = i1.user_id " +
		                                       "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and exists ( select 1 " +
		                                       "from users u where u.id = i2.user_id " +
		                                       "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

								 "and i1.ip not in ("+ omittedIp +") " +

						 "UNION " +

						 "select distinct i1.user_id user1_id, i2.user_id user2_id, i1.ip ip " +
						 "from investments i1, investments i2 " +
						 "where " +
						       "i1.ip = i2.ip and i1.user_id <> i2.user_id " +
						       "and trunc(i2.time_created) < trunc(sysdate - 1) " +
						       "and trunc(i1.time_created) = trunc(sysdate - 1) " +
						       "and exists ( select 1 " +
						                     "from users u where u.id = i1.user_id " +
						                     "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +
						       "and exists ( select 1 " +
						                     "from users u where u.id = i2.user_id " +
						                     "and u.class_id <> 0 " + getSqlByRunType(runType) + " ) " +

						       "and i1.ip not in ("+ omittedIp +") " +


						 "order by user1_id,user2_id";

		pstmt = conn.prepareStatement(sql);

		rs = pstmt.executeQuery();

		while(rs.next()) {
			dupIp = rs.getString("ip");
			user1Id = rs.getLong("user1_id");
			user2Id = rs.getLong("user2_id");
			user1IdString = Long.toString(user1Id);
			user2IdString = Long.toString(user2Id);

			// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated ip.
			if (usersDupIp.containsKey(user2IdString + ';' + user1IdString) == false ||
					!usersDupIp.get(user2IdString + ';' + user1IdString).equals(dupIp)) {

				usersDupIp.put(user1IdString + ';' + user2IdString, dupIp);

				SecurityCheck temp = new SecurityCheck();
				temp.setIp(dupIp);
				temp.setUserId(user1Id);
				temp.setUserId2(user2Id);
				users.add(temp);
			}
		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users, for investments of different users with same ips " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close resultSet",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close statement",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close connection",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "investments of different users with same ips ", getPageHeaderByRunType(runType),runType,"report_duplicate_ip.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");
	}


//	/**
//	 * Send equal passwords report (70% matching)
//	 * This is done with levenshtein distance algorithm
//	 */
//	private static void calcAndSendEqualPasswordsReport(int runType) {
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		ArrayList<SecurityCheck> newUsers = new ArrayList<SecurityCheck>();
//		ArrayList<SecurityCheck> oldUsers = new ArrayList<SecurityCheck>();
//		ArrayList<SecurityCheck> equalsUsers = new ArrayList<SecurityCheck>();
//
//		try {
//
//			conn = Utils.getConnection();
//
//			// new depositors / try to deposit users
//			String sql =
//					"SELECT " +
//		            	"u.user_name , " +
//		            	"u.id , " +
//		            	"u.password " +
//		            "FROM " +
//		            	"users u " +
//		            "WHERE " +
//			          "u.class_id <> 0  AND " +
//			          "u.password not like '123456%'  " +
//			          getSqlByRunType(runType) + " AND " +
//			          "EXISTS (select 1 " +
//	                           "from  transactions t , transaction_types tt " +
//	                           "where t.user_id = u.id and " +
//	                                 "t.type_id = tt.id and " +
//	                                 "tt.class_type = 1 and " +
//	                                 "to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') ) AND " +
//	                  "NOT EXISTS (select distinct t2.user_id " +
//		                         "from transactions t2, transaction_types tt2 " +
//	                             "where t2.type_id = tt2.id and " +
//	                                   "tt2.class_type = 1 and " +
//	                                   "t2.user_id = u.id and " +
//	                                   "to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') ) " +
//		        "ORDER BY user_name ";
//
//			pstmt = conn.prepareStatement(sql);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				SecurityCheck user = new SecurityCheck();
//				user.setUsername(rs.getString("user_name"));
//				user.setPassword(rs.getString("password"));
//				user.setUserId(rs.getInt("id"));
//				newUsers.add(user);
//			}
//
//			//	old depositors / try to deposit users
//			String sql1 =
//				"SELECT " +
//	            	"u.user_name , " +
//	            	"u.id , " +
//	            	"u.password " +
//	            "FROM " +
//	            	"users u " +
//	            "WHERE " +
//		          "u.class_id <> 0  AND " +
//		          "u.password not like '123456%'  " +
//		          getSqlByRunType(runType) + " AND " +
//		          "NOT EXISTS (select 1 " +
//                               "from  transactions t , transaction_types tt " +
//                               "where t.user_id = u.id and " +
//                                      "t.type_id = tt.id and " +
//                                      "tt.class_type = 1 and " +
//                                      "to_char(t.TIME_CREATED,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') ) AND " +
//                  "EXISTS (select distinct t2.user_id " +
//	                       "from transactions t2, transaction_types tt2 " +
//                           "where t2.type_id = tt2.id and " +
//                                  "tt2.class_type = 1 and " +
//                                  "t2.user_id = u.id and " +
//                                  "to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') ) " +
//	        "ORDER BY user_name ";
//
//			pstmt = conn.prepareStatement(sql1);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				SecurityCheck user = new SecurityCheck();
//				user.setUsername(rs.getString("user_name"));
//				user.setPassword(rs.getString("password"));
//				user.setUserId(rs.getInt("id"));
//				oldUsers.add(user);
//			}
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get equal passwords report " + e);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				conn.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//		}
//
//
//		// operate algorithm on new users -> new users
//		equalsUsers = getEqualPassList(newUsers, newUsers);
//
//		// operate algorithm on new users -> old users
//		equalsUsers.addAll(getEqualPassList(newUsers, oldUsers));
//
//		log.log(Level.DEBUG,"going to send emails ");
//
//		sendEmailEqualPasswords(equalsUsers, getSubjectByRunType(runType) + "passwords report (70% matching) ",
//				getPageHeaderByRunType(runType), runType);
//
//		log.log(Level.DEBUG,"finished to send emails");
//
//	}

/*	*//**
	 * Send equal passwords report (70% matching)
	 * This is done with levenshtein distance algorithm
	 *//*
	private static void calcAndSendEqualPasswordsReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> newDepositors = new ArrayList<SecurityCheck>();
		ArrayList<SecurityCheck> newUsersNonDepositors = new ArrayList<SecurityCheck>();
		ArrayList<SecurityCheck> blackListUsers = new ArrayList<SecurityCheck>();

		try {

			conn = Utils.getConnection();

			// new depositors / try to deposit users
			String sql =
					"SELECT " +
		            	"u.user_name , " +
		            	"u.id , " +
		            	"u.password, " +
		            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
		            "FROM " +
		            	"users u " +
		            		"LEFT JOIN (SELECT " +
		            						"t.user_id " +
		            					"FROM " +
		            						"transactions t, transaction_types tt " +
		            					"WHERE " +
		            						"t.type_id = tt.id " +
		            						"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
		            						"AND tt.class_type =  1 " +
		            					"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id " +
		            "WHERE " +
			          "u.class_id <> 0  AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.skin_id not in (" + Skin.SKIN_CHINESE + "," + Skin.SKIN_CHINESE_VIP + ") " + 
			          getSqlByRunType(runType) + " AND " +
			          "EXISTS (select 1 " +
	                           "from  transactions t , transaction_types tt " +
	                           "where t.user_id = u.id and " +
	                                 "t.type_id = tt.id and " +
	                                 "tt.class_type = 1 and " +
	                                 "to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') ) AND " +
	                  "NOT EXISTS (select distinct t2.user_id " +
		                         "from transactions t2, transaction_types tt2 " +
	                             "where t2.type_id = tt2.id and " +
	                                   "tt2.class_type = 1 and " +
	                                   "t2.user_id = u.id and " +
	                                   "to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') ) " +
		        "ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
			rs = pstmt.executeQuery();
			log.log(Level.DEBUG,"Finish run first Query");

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				user.setUsername(rs.getString("user_name"));
				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
				user.setUserId(rs.getInt("id"));
				user.setSucceedDeposit(rs.getString("suc_deposit"));
				user.setUserListType(SecurityCheck.USER_LIST_TYPE_NEW_DEPOSITERS);
				newDepositors.add(user);
			}

			log.log(Level.DEBUG,"Save al new depositors in list");
			// new users with no deposits
			String sql1 =
					"SELECT " +
		            	"u.user_name , " +
		            	"u.id , " +
		            	"u.password " +
		            "FROM " +
		            	"users u " +
		            "WHERE " +
			          "u.class_id <> 0  AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.password not like ? AND " +
			          "u.skin_id not in (" + Skin.SKIN_CHINESE + "," + Skin.SKIN_CHINESE_VIP + ") " + 
			          getSqlByRunType(runType) + " AND " +
			          "to_char(u.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') AND "+
			          "NOT EXISTS (select 1 " +
		                           "from  transactions t , transaction_types tt " +
		                           "where t.user_id = u.id and " +
		                                 "t.type_id = tt.id and " +
		                                 "tt.class_type = 1 ) " +
		        "ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql1);
			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
			rs = pstmt.executeQuery();
			log.log(Level.DEBUG,"Finish run second Query");
			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				user.setUsername(rs.getString("user_name"));
				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
				user.setUserId(rs.getInt("id"));
				user.setSucceedDeposit("NO");
				user.setUserListType(SecurityCheck.USER_LIST_TYPE_NEW_USERS);
				newUsersNonDepositors.add(user);
			}

			//	black list depositors / try to deposit users
			String sql2 =
				"SELECT " +
	            	"u.user_name , " +
	            	"u.id , " +
	            	"u.password, " +
	            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
	            "FROM " +
	            	"users u " +
	            		" left join CLOSE_ACCOUNT_USERS bl on u.id = bl.user_id " +
		            	"LEFT JOIN (SELECT " +
										"t.user_id " +
									"FROM " +
										"transactions t, transaction_types tt " +
									"WHERE " +
										"t.type_id = tt.id " +
										"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
										"AND tt.class_type =  1 " +
									"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id " +
	            "WHERE " +
		          "u.class_id <> 0   " +
		          " AND u.password not like ? AND " +
		          "u.password not like ? AND " +
		          "u.password not like ? AND " +
		          "u.password not like ? AND " +
		          "u.skin_id not in (" + Skin.SKIN_CHINESE + "," + Skin.SKIN_CHINESE_VIP + ") " + 
		          getSqlByRunType(runType) +
		          " AND ((u.is_active = 0 " +
			     			" AND bl.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_CHB_CLOSE+ "" +
			     											","+ IssuesManagerBase.ISSUE_ACTION_FRAUD + ")) " +
			     		" OR " +
			          	  " EXISTS (select 1 " +
				          		   " from transactions t " +
				          		   " where t.charge_back_id is not null and " +
				          		   		  " t.charge_back_id <> 0 and " +
				          		   		  " t.user_id = u.id) " +
		        		   " ) " +
	        "ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
			rs = pstmt.executeQuery();
			log.log(Level.DEBUG,"Finish run third Query");

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				user.setUsername(rs.getString("user_name"));
				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
				user.setUserId(rs.getInt("id"));
				user.setSucceedDeposit(rs.getString("suc_deposit"));
				user.setUserListType(SecurityCheck.USER_LIST_TYPE_BLACK_LIST);
				blackListUsers.add(user);
			}
			log.log(Level.DEBUG,"dasdasdsa");

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get equal passwords report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		ArrayList<SecurityCheck> newDepositorsCompareList = new ArrayList<SecurityCheck>();
		newDepositorsCompareList.addAll(newDepositors);
		newDepositorsCompareList.addAll(newUsersNonDepositors);
		newDepositorsCompareList.addAll(blackListUsers);

		// operate algorithm on new Depositors -> new Depositors + new Users + black List Users
		ArrayList<SecurityCheck> equalsNewDepositors= getEqualPassList(newDepositors, newDepositorsCompareList);


		ArrayList<SecurityCheck> newUsersCompareList = new ArrayList<SecurityCheck>();
		newUsersCompareList.addAll(newUsersNonDepositors);
		newUsersCompareList.addAll(blackListUsers);

		// operate algorithm on new Users -> new Users + black List Users
		ArrayList<SecurityCheck> equalsNewUsersNonDepositors = getEqualPassList(newUsersNonDepositors, newUsersCompareList);

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailEqualPasswords(equalsNewDepositors, equalsNewUsersNonDepositors, getSubjectByRunType(runType) + "passwords report (70% matching) ",
				getPageHeaderByRunType(runType), runType);

		log.log(Level.DEBUG,"finished to send emails");

	}


	*//**
	 * Get 2 SecurityCheck lists and return a new list that contains users with password 70% matching.
	 * @param users1  SecurityCheck list of users to compare with users2 list
	 * @param users2  SecurityCheck list of users(list 2)
	 * @return new SecurityCheck list
	 *//*
	public static ArrayList<SecurityCheck> getEqualPassList(ArrayList<SecurityCheck> users1, ArrayList<SecurityCheck> users2) {

		ArrayList<SecurityCheck> equalsUsers = new ArrayList<SecurityCheck>();
		ArrayList<String> checkedList = new ArrayList<String>();

		for ( SecurityCheck u1 : users1 ) {
			String userName1 = u1.getUsername();
			long userId1 = u1.getUserId();
			String pass1 = u1.getPassword();
			String succeedDeposit = u1.getSucceedDeposit();

			boolean firstUser = true;
			for ( SecurityCheck u2 : users2 ) {

				String userName2 = u2.getUsername();
				String pass2 = u2.getPassword();
				long userId2 = u2.getUserId();
				String userListType = u2.getUserListType();

				if ( u1.getUserId() != u2.getUserId()  &&						   // no need to run on the same user and
						!checkedList.contains(userName2 + "-" + userName1)  &&     // on some pair that already checked, like
						!checkedList.contains(userName1 + "-" + userName2) ) {	   // (1-2) and (2-1)

					int distance = levenshteinDistance(pass1, pass2);
					int maxLength = Math.max(pass1.length(), pass2.length());
					Double prec = 0.7 * maxLength;
					int eqCharacters = maxLength - distance;
					Double matching = new Double(eqCharacters) / new Double(maxLength);
					String matchingPrec = String.valueOf((matching * 100));
					if ( distance == 0 ) {  // 100% matching
						matchingPrec = matchingPrec.substring(0, 3) + "%";
					} else {
						matchingPrec = matchingPrec.substring(0, 2) + "%";
					}

					if ( eqCharacters >= prec ) {  // add to equals list
						SecurityCheck s = new SecurityCheck();
						s.setUserId(userId1);

						// Group by user1Id and paddword1
						if (firstUser){
							// add new line separator
							SecurityCheck newLine = new SecurityCheck();
							newLine.setUserName1("---------------");
							newLine.setPassword1("---------------");
							newLine.setUserName2("---------------");
							newLine.setPassword2("---------------");
							newLine.setUserListType("---------------");
							newLine.setSucceedDeposit("---------------");
							equalsUsers.add(newLine);

							s.setUserId(u1.getUserId());
							s.setUserName1(userName1);
							s.setPassword1(pass1);
							firstUser = false;
						} else{
							s.setUserName1("");
							s.setPassword1("");
						}
						s.setUserId2(userId2);
						s.setUserName2(userName2);
						s.setPassword2(pass2);
						s.setDistance(distance);
						s.setMatching(matchingPrec);
//						s.setEqualCharacters(eqCharacters);
//						DecimalFormat sd = new DecimalFormat("###.###");
//						s.setPrec(sd.format(prec));
						s.setUserListType(userListType);
						s.setSucceedDeposit(succeedDeposit);

						equalsUsers.add(s);
					}
					// add to checkedList
					checkedList.add(userName2 + "-" + userName1);
					//System.out.println(userName2 + "-" + userName1);

				}
			}
		}

		return equalsUsers;
	}


    *//**
     * Levenshtein distance algorithm(dynamic programming)
     * The Levenshtein distance is a metric for measuring the amount of difference between two sequences (i.e., the so called edit distance).
     * The Levenshtein distance between two strings is given by the minimum number of operations needed to transform one string into the other,
     * where an operation is an insertion, deletion, or substitution of a single character.
     * @param str1
     * 		String 1 for compare
     * @param str2
     * 		String 2 for compare
     * @return
     * 		The minimum number of operations needed to transform one string into the other
     *//*
    public static int levenshteinDistance(String str1, String str2) {

    	int leng1 = str1.length() + 1;
    	int leng2 = str2.length() + 1;

    	int[][] d = new int[leng1][leng2];

    	for ( int i = 0 ; i < leng1 ; i++ ) {
    		d[i][0] = i;
    	}

    	for ( int j = 0 ; j < leng2 ; j++ ) {
    		d[0][j] = j;
    	}

    	int cost = 0;

    	for ( int i = 1 ; i < leng1 ; i++ )
    		for ( int j = 1 ; j < leng2 ; j++ ) {

    			 if ( str1.charAt(i-1) == str2.charAt(j-1) ) {
    				 cost = 0;
    			 } else {
    				 	cost = 1;
    			 }

                 // (d[i-1, j] + 1): deletion , (d[i, j-1] + 1): insertion, (d[i-1, j-1] + cost): substitution
    			 d[i][j] = Math.min( Math.min(d[i-1][j] + 1, d[i][j-1] + 1) , d[i-1][j-1] + cost );
   		}

    	return d[leng1-1][leng2-1];
    }


    *//**
     * Send passwords matching email
     *//*
	private static void sendEmailEqualPasswords(ArrayList<SecurityCheck> newDepositors, ArrayList<SecurityCheck> newUsers,
													String subjectP, String header, int runType) {

		// put all params
		log.info("number of newDepositors  " + newDepositors.size());
		log.info("number of newUsers  " + newUsers.size());

		HashMap<String, Object> params = new HashMap<String, Object>();

		params.put("newDepositors",newDepositors);
		params.put("newUsers",newUsers);
		params.put("header",header);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String subject = subjectP + sdf.format(cal.getTime());

		try {
			initEmail(subject,"report_passwords_matching.html", runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails =  getEmailByRunType(runType, "emails.security.check").split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}

	}


	*//**
	 * This report, send all users that their first name and last name are diffrent than
	 * the holder name of their cardit cards.
	 * @param runType
	 *//*
	private static void SendDailyReportDifferentNames(int runType){

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		try {

			conn = Utils.getConnection();

			String sql = " select " +
							" cc.id card_id, " +
							" cc.holder_name holder, " +
							" u.first_name first, " +
							" u.last_name last, " +
							" u.id user_id, " +
							" u.user_name, " +
							" s.display_name skin, " +
							" (CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit  " +
						 "from  users u " +
						 			"LEFT JOIN (SELECT " +
						 							"t.user_id " +
						 						"FROM " +
						 							"transactions t, transaction_types tt " +
						 						"WHERE " +
						 							"t.type_id = tt.id " +
						 							"AND t.status_id IN ( " + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
						 							"AND tt.id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " " +
						 						"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id, " +
						 "credit_cards cc, skins s " +
					 	 "where u.id = cc.user_id " +
					 	 		"and upper(replace(cc.holder_name,' ','')) <>  upper(replace((u.first_name ||  u.last_name),' ','')) " +
					 	 		"and upper(replace(cc.holder_name,' ','')) <> upper(replace((u.last_name ||  u.first_name),' ','')) " +
					 	 		"and to_char(cc.time_modified,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') " +
					 	 		"and u.skin_id = s.id " +
					 	 		getSqlByRunType(runType,"u");

		pstmt = conn.prepareStatement(sql);

		rs = pstmt.executeQuery();

		while(rs.next()) {

			SecurityCheck temp = new SecurityCheck();
			temp.setCcId(rs.getLong("card_id"));
			temp.setCcHolderName(rs.getString("holder"));
			temp.setFirstName(rs.getString("first"));
			temp.setLastName(rs.getString("last"));
			temp.setUserId(rs.getLong("user_id"));
			temp.setSkin(Utils.getProperty(rs.getString("skin")));
			temp.setSucceedDeposit(rs.getString("suc_deposit"));
			temp.setUsername(rs.getString("user_name"));
			users.add(temp);

		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users with different names than their cardit card holder name " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close resultSet",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close statement",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close connection",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "Card Holder / Users different name ", getPageHeaderByRunType(runType),runType,"report_different_names.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");
	}

	*//**
	 * This report, send all users that their country is different than
	 * 	their registration ip country.
	 * @param runType
	 *//*
	private static void differentIpCountryReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> newUsersWithDifferentIp = new ArrayList<SecurityCheck>();

		try {

			String omittedIp = "";

			// handle omitted ip's
			String[] omittedIpArr = Utils.getProperty("security.check.omitted.ip").split(";");
			for (String ip : omittedIpArr) {
				omittedIp += "'" + ip + "',";
			}
			omittedIp += "'" + SecurityCheck.IP_NOT_FOUND + "'";

			conn = Utils.getConnection();

			// new users
			String sql1 =
					"SELECT " +
		            	"u.user_name, " +
		            	"u.id, " +
		            	"u.ip, " +
		            	"c.a2 country_code, " +
		            	"c.COUNTRY_NAME country_name, " +
		            	"s.name skin_name, " +
		            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
		            "FROM " +
		            	"users u " +
		            	"LEFT JOIN (SELECT " +
		            					"t.user_id " +
		            				"FROM " +
		            					"transactions t, transaction_types tt " +
		            				"WHERE " +
		            					"t.type_id = tt.id " +
		            					"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
		            					"AND tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
		            				"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id, " +
		            	"countries c, skins s " +
		            "WHERE " +
		              "u.skin_id <> 15 AND " +
			          "u.class_id <> 0  AND " +
			          "u.country_id = c.id  " +
			          getSqlByRunType(runType) + " AND " +
			          "(to_char(u.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-10,'YYYYMMDD')  or " +
							" to_char(u.time_modified,'YYYYMMDD') = to_char(sysdate-10,'YYYYMMDD')) AND " +
			          "s.id = u.skin_id AND "+
			          "u.ip not in ("+ omittedIp +") " +
			          " and u_suc_deposit.user_id is not null " +
		        "ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();

				String userIp = rs.getString("ip");
				String userCountryCode = rs.getString("country_code");
				String ipCountryCode = Utils.getCountryCodeByIp(userIp);

				if (!userCountryCode.equals(ipCountryCode) && !ipCountryCode.equals("IL") ){
					user.setUsername(rs.getString("user_name"));
					user.setUserId(rs.getInt("id"));
					user.setSucceedDeposit(rs.getString("suc_deposit"));
					user.setIp(userIp);
					user.setUserCountryName(rs.getString("country_name"));

					String countryName = null;
					if(null != ipCountryCode){
						countryName = Utils.getCountryNameByCode(ipCountryCode,ipCountryCode.length());

						if(null == countryName){
							countryName = "--";
						}
					} else {
						countryName = "--";
					}

					user.setIpCountryName(countryName);
					user.setSkin(rs.getString("skin_name"));
					newUsersWithDifferentIp.add(user);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Ip Country Report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(newUsersWithDifferentIp,
				null,
				getSubjectByRunType(runType) + "Different Ip Country Report ", getPageHeaderByRunType(runType),runType,"report_different_ip_country.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	*//**
	 * This report, send all users that their country is different than
	 * 	their credit card issue country.
	 * @param runType
	 *//*
	private static void differentCcCountryReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<SecurityCheck> tempList = new ArrayList<SecurityCheck>();
		ArrayList<SecurityCheck> usersWithDiffCardCountry = new ArrayList<SecurityCheck>();

		try {
			conn = Utils.getConnection();
			String sql1 =
					" SELECT " +
						" u.user_name, " +
						" cc.id cc_id, " +
						" cc.cc_number ccnumber, " +
						" co.country_name reg_country " +
					" From " +
						" users u, " +
						" credit_cards cc, " +
						" countries co " +
						//", cc_bins_country cbn " +
					" Where " +
					    " u.skin_id <> 15 AND " +
						" u.id = cc.user_id " +
						" and u.class_id <> 0 " +
						" and u.country_id = co.id " +
						getSqlByRunType(runType) +
						" and cc.id in (select t.credit_card_id " +
										" from transactions t " +
										" where t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " " + // cc_deposit
										"   and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " + // success or pending
										"   and to_char(t.TIME_CREATED,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
		        " ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				user.setUsername(rs.getString("user_name"));
				user.setCcId(rs.getLong("cc_id"));
				String ccNumber = Encryptor.decryptStringToString(rs.getString("ccnumber"));
				user.setCcNumber(ccNumber);
				user.setCcLast4Digits(ccNumber.substring(ccNumber.length()-4));
				user.setUserCountryName(rs.getString("reg_country"));
				user.setSucceedDeposit("YES");
				tempList.add(user);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Card Country Report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		try {
			conn = Utils.getConnection();
			String sql1 =
					" SELECT cbn.country_name_a3 cc_country " +
					" From " +
						" users u, " +
						" credit_cards cc, " +
						" countries co, " +
						" bins cbn " +
					" Where " +
						" u.id = cc.user_id " +
						" and u.country_id = co.id " +
						" and cc.id = ? " +
						" and substr(?, 0, 6) between cbn.from_bin and cbn.to_bin  " +
						" and co.a3 != cbn.country_name_a3 ";

			for (int i=0; i<tempList.size(); i++) {
				SecurityCheck user = tempList.get(i);
				pstmt = conn.prepareStatement(sql1);
				pstmt.setLong(1, user.getCcId());
				pstmt.setString(2, user.getCcNumber());
				rs = pstmt.executeQuery();

				if (rs.next()) {
					String countryCode = rs.getString("cc_country");
					String countryName = Utils.getCountryNameByCode(countryCode,countryCode.length());
					if (null == countryName){
						user.setCcCountryName("--");
					} else {
						user.setCcCountryName(countryName);
					}
					usersWithDiffCardCountry.add(user);
				}
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Card Country Report " + e);
		} finally {
			try {
				if (null != rs) {
					rs.close();
				}
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				if (null != pstmt) {
					pstmt.close();
				}
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(usersWithDiffCardCountry,
				null,
				getSubjectByRunType(runType) + "Different Cc Country Report ", getPageHeaderByRunType(runType),runType,"report_different_card_country.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	*//**
	 * This report, send all users that tried to deposit with more than x cc
	 * @param runType
	 *//*
	private static void depositorsWithMoreThanXDiffCcReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> depositorsWithMoreThanXDiffCc = new ArrayList<SecurityCheck>();

		try {

			conn = Utils.getConnection();

			String sql1 =
						"select " +
						      "u.id user_id, " +
						      "u.user_name, " +
						      "cc.id cc_id, " +
						      "cc.is_allowed, " +
						      "cc.holder_name, " +
						      "count(tran.credit_card_id) deposits_num " +
						"from " +
						      "users u, " +
						      "credit_cards cc left outer join transactions tran on cc.id = tran.credit_card_id " +
						        														"and tran.type_id = 1  " + // cc deposit
						        														"and tran.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
						"where " +
						      "u.id = cc.user_id and " +
						      "u.skin_id <> 15 and " +
						      "EXISTS(select 1 " +
				                     "from    transactions t " +
				                     "where t.user_id = u.id " +
				                         "and t.type_id = 1 " + // cc deposit
				                         "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")) and " +
						      "u.id in (select " +
						                        "DISTINCT(u.id) " +
						                  "from " +
						                        "users u, " +
						                        "credit_cards cc " +
						                  "where " +
						                       "u.id = cc.user_id and " +
						                       "EXISTS (select 1 " +
				                                       "from    transactions t " +
				                                       "where t.user_id = u.id " +
				                                           "and t.type_id = 1 " + // cc deposit
				                                           "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') " +
				                                           "and t.credit_card_id = cc.id) and " +
						                        "NOT EXISTS (select 1 " +
			                                                 "from    transactions t , transaction_types tt " +
			                                                 "where t.user_id = u.id " +
			                                                     "and t.type_id = 1 " + // cc deposit
			                                                     "and to_char(t.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') " +
			                                                     "and t.credit_card_id = cc.id) and " +
						                        X_DIFF_CC_DEPOSITS_ATTEMPTS + " < (select  count(DISTINCT(t.credit_card_id)) " +
													                               "from  transactions t " +
													                               "where t.user_id = u.id " +
													                                 "and t.type_id = 1)) " +// cc deposit
								getSqlByRunType(runType) +
						"group by u.id, u.user_name, cc.id, cc.is_allowed, cc.holder_name " +
						"order by u.id, cc.id ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			String tempUsername = "";
			String username;
			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				username = rs.getString("user_name");

				if (!tempUsername.equals(username)){
					user.setUsername(username);
					tempUsername = username;
				} else {
					user.setUsername("");
				}

				user.setUserId(rs.getInt("user_id"));
				user.setCcId(rs.getLong("cc_id"));
				user.setCcAllowed(rs.getInt("is_allowed")==1?true:false);
				user.setCcHolderName(rs.getString("holder_name"));
				user.setDepositsNum(rs.getLong("deposits_num"));
				depositorsWithMoreThanXDiffCc.add(user);

			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get depositors with more than " + X_DIFF_CC_DEPOSITS_ATTEMPTS + " Diff Cc " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(depositorsWithMoreThanXDiffCc,
				null,
				getSubjectByRunType(runType) + "Depositors With " + X_DIFF_CC_DEPOSITS_ATTEMPTS + " Different Cc Report ", getPageHeaderByRunType(runType),runType,"report_dspositors_with_x_different_cc.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	*//**
	 * This report, send all users with duplicate phones
	 * First query return all users with duplicate phones that created today(u1 and also u2),
	 * Second query return all users with duplicate phones, u1 created today and u2 not created today.
	 *//*
	private static void SendDailyReportDupicatePhones(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		String dupPhone ="";
		String mobilePhone1, landLinePhone1,mobilePhone2, landLinePhone2;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		String userName1, userName2;
		String succeedDeposit;
		HashMap<String, String> usersDupPhone = new HashMap<String, String>();

		try {

			conn = Utils.getConnection();
			String sqlDupicatePhone = " select distinct u1.id user1_id, u2.id user2_id, u1.mobile_phone mobile1, u1.land_line_phone landline1," +
												" u1.user_name username1, u2.user_name username2, u2.mobile_phone mobile2, u2.land_line_phone landline2, " +
												" (CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
									  " from users u1 " +
									  			"LEFT JOIN (SELECT " +
									  							"t.user_id " +
									  						"FROM " +
									  							"transactions t, transaction_types tt " +
									  						"WHERE " +
									  							"t.type_id = tt.id " +
									  							"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
									  							"AND tt.class_type =  " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
									  						"GROUP BY t.user_id) u_suc_deposit ON u1.id = u_suc_deposit.user_id, "+
									  " users u2 " +
									  	" left join CLOSE_ACCOUNT_USERS bl on u2.id = bl.user_id " +
									  " where (u1.mobile_phone = u2.mobile_phone " +
									  			" or u1.land_line_phone = u2.land_line_phone) " +
									  		 " and u1.id <> u2.id " +
									  		 " and u1.skin_id <> 15 and u2.skin_id <> 15 " +
										     " and u1.class_id <> 0 " +
										     " and u2.class_id <> 0 " +
										     getSqlByRunType(runType,"u1") +
										     getSqlByRunType(runType,"u2") +
										     " and (to_char(u1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') or " +
									      			" to_char(u1.time_modified,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
										     " and ((u2.is_active = 0 " +
										     			" AND bl.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_CHB_CLOSE+ "" +
										     											","+ IssuesManagerBase.ISSUE_ACTION_FRAUD + ")) " +
										     		" OR " +
										          	  " EXISTS (select 1 " +
											          		   " from transactions t " +
											          		   " where t.charge_back_id is not null and " +
											          		   		  " t.charge_back_id <> 0 and " +
											          		   		  " t.user_id = u2.id) " +
									          		   " ) " +
//
//									  "UNION "+
//
//									  "select distinct u1.id user1_id, u2.id user2_id, u1.mobile_phone mobile1, u1.land_line_phone landline1," +
//										"u1.user_name username1, u2.user_name username2, u2.mobile_phone mobile2, u2.land_line_phone landline2 " +
//									  "from users u1, users u2 "+
//									  "where (u1.mobile_phone = u2.mobile_phone " +
//									  			"or u1.land_line_phone = u2.land_line_phone) " +
//							  				 "and u1.id <> u2.id " +
//									  		 "and u1.class_id <> 0 " +
//									  		 "and u2.class_id <> 0 " +
//										     getSqlByRunType(runType,"u1") +
//										     getSqlByRunType(runType,"u2") +
//										     "and trunc(u1.time_created) = trunc( sysdate - 1) " +
//										     "and (u2.is_false_account = 1 OR " +
//									          	  "u2.is_active = 0 OR " +
//									          	  "EXISTS (select 1 " +
//										          		   "from transactions t " +
//										          		   "where t.charge_back_id is not null and " +
//										          		   		  "t.charge_back_id <> 0 and " +
//										          		   		  "t.user_id = u2.id) " +
//										         	") " +

									   " order by user1_id,user2_id";

			pstmt = conn.prepareStatement(sqlDupicatePhone);

			rs = pstmt.executeQuery();

			while(rs.next()) {
				mobilePhone1 = rs.getString("mobile1");
				landLinePhone1 = rs.getString("landline1");
				mobilePhone2 = rs.getString("mobile2");
				landLinePhone2 = rs.getString("landline2");
				user1Id = rs.getLong("user1_id");
				user2Id = rs.getLong("user2_id");
				user1IdString = Long.toString(user1Id);
				user2IdString = Long.toString(user2Id);
				userName1 = rs.getString("username1");
				userName2 = rs.getString("username2");
				succeedDeposit = rs.getString("suc_deposit");
				if (null != mobilePhone1 && null != mobilePhone2 && mobilePhone1.equals(mobilePhone2)){
					dupPhone = mobilePhone1;
				} else if (null != landLinePhone1 && null != landLinePhone2 && landLinePhone1.equals(landLinePhone2)){
					dupPhone = landLinePhone1;
				}


				// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated Phone.
				if (usersDupPhone.containsKey(user2IdString + ';' + user1IdString) == false ||
						!usersDupPhone.get(user2IdString + ';' + user1IdString).equals(dupPhone)) {

					usersDupPhone.put(user1IdString + ';' + user2IdString, dupPhone);

					SecurityCheck temp = new SecurityCheck();
					temp.setPhone(dupPhone);
					temp.setUserId(user1Id);
					temp.setUserId2(user2Id);
					temp.setUsername(userName1);
					temp.setUserName2(userName2);
					temp.setSucceedDeposit(succeedDeposit);
					users.add(temp);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get for duplicate phones report ", e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send phones ");

		sendEmailForMailingList(users, null,
				getSubjectByRunType(runType) + "daily - duplicate phones", getPageHeaderByRunType(runType), runType,"report_duplicate_phones.html", SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send phones");

	}

	*//**
	 * This report, send cc deposits status per provider and currency
	 * @param runType
	 *//*
	private static void SendCcDepositsStatusReport(int runType, int dateRangeType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<CcDepositsStatus> list = new ArrayList<CcDepositsStatus>();
		String lastProvider = "";
		String strDateCondition = "";
		String mailSubject = getSubjectByRunType(runType);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf;

		switch (dateRangeType) {
		case RUN_DAILY:
			strDateCondition +=  " and to_char(T.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') ";

			sdf = new SimpleDateFormat("dd-MM-yyyy");
			cal.add(Calendar.DAY_OF_YEAR,-1);
			mailSubject += " CC Deposits Status Daily Report " + sdf.format(cal.getTime());
			break;
		case RUN_WEEKLY:
			strDateCondition +=  " and T.TIME_CREATED between trunc(sysdate-7) and trunc(sysdate) ";

			sdf = new SimpleDateFormat("dd-MM-yyyy");
			cal.add(Calendar.DAY_OF_YEAR,-7);
			mailSubject += " CC Deposits Status Weekly Report " + sdf.format(cal.getTime()) + " until ";
			cal.add(Calendar.DAY_OF_YEAR,+6);
			mailSubject += sdf.format(cal.getTime());
			break;
		case RUN_MONTHLY:
			strDateCondition +=  " and to_char(T.TIME_CREATED,'YYYYMM') =  to_char(sysdate-1,'YYYYMM') ";

			sdf = new SimpleDateFormat("MM-yyyy");
			cal.add(Calendar.DAY_OF_YEAR,-1);
			mailSubject += " CC Deposits Status Monthly Report " + sdf.format(cal.getTime());
			break;
		}

		try {
			conn = Utils.getConnection();
			String sql =
					" SELECT  " +
						" cp.name provider_name, " +
						" c.code currency, " +
						" c.id, " +
		            	" SUM(case when t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") then 1 else 0 end) success, " +
		            	" SUM(case when t.status_id in (3) then 1 else 0 end) fail, " +
		            	" count(*) total " +
					" FROM " +
						" transactions t , " +
						" users u ," +
						" currencies c, " +
						" clearing_providers cp " +
					" WHERE " +
						" t.type_id  = 1 " +
						" and t.user_id = u.id " +
						" and u.class_id != 0 " +
					    " and u.currency_id = c.id " +
					    " and t.clearing_provider_id = cp.id " +
					    " and t.xor_id_authorize is not null " +
					    getSqlByRunType(runType,"u") +
					    strDateCondition +
					" GROUP BY " +
						" cp.name, " +
						" c.code, " +
						" c.id " +
					" ORDER BY " +
						" cp.name, " +
						" c.id ";

			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				CcDepositsStatus vo = new CcDepositsStatus();
				String currProvider = rs.getString("provider_name");

				if (!currProvider.equals(lastProvider)){
					vo.setProvider(currProvider);
					lastProvider = currProvider;
				}else{
					vo.setProvider(ConstantsBase.EMPTY_STRING);
				}

				vo.setCurrencyCode(rs.getString("currency"));

				long success = rs.getLong("success");
				long total = rs.getLong("total");
				String percent = 100*success/total + "%";
				vo.setNumSuccess(success);
				vo.setNumTotal(total);
				vo.setPercentSuccess(percent);
				vo.setNumFailed(rs.getLong("fail"));

				list.add(vo);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get ccDepositsStatusReport " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails " + mailSubject);

		sendEmailForMailingList(null, list,	mailSubject, getPageHeaderByRunType(runType),runType,
				"report_cc_deposits_status.html", SECURITY_MAILING_LIST, false);

		log.log(Level.DEBUG,"finished to send emails" + mailSubject);

	}

	*//** Send Mail for the selected users list
	 * @param users
	 * @param ccDepositsStatusList
	 * @param isAddTodayDateToSubject
	 *//*
	private static void sendEmailForMailingList(ArrayList<SecurityCheck> users, ArrayList<CcDepositsStatus> ccDepositsStatusList, String subjectP,
			String header, int runType, String htmlName, String mailingList, boolean isAddTodayDateToSubject) {

		// put all users params
		if (null != users){
			log.info("number of users for " + subjectP + " mail is: " + users.size());
		}

		HashMap<String, Object> params = new HashMap<String, Object>();
		String subject = subjectP;

		params.put("users",users);
		params.put("ccDepositsStatus",ccDepositsStatusList);
		params.put("header",header);

		if (isAddTodayDateToSubject){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR,-1);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			subject += sdf.format(cal.getTime());
		}

		try {
			initEmail(subject,htmlName, runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = getEmailByRunType(runType, mailingList).split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}
	}
}*/