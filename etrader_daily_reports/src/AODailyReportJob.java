/*import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;

public class AODailyReportJob {

	private static Logger log = Logger.getLogger(AODailyReportJob.class);

	private static Template emailTemplate;

	String path = "";

	// The server properties
	private static Hashtable<String, String> serverProperties;

	// The specific email Properties
	private static Hashtable<String, String> emailProperties;

	public static final String PARAM_EMAIL = "email";

	private static HashMap<Long, Market> marktes = new HashMap<Long, Market>();

	// Constants for running type
	private static final int RUN_TYPE_SKIN_ETRADER = 1;
	private static final int RUN_TYPE_SKIN_ENGLISH_USD = 2;
	private static final int RUN_TYPE_SKIN_TURKISH_TRY = 3;
	private static final int RUN_TYPE_SKIN_RUSSIAN_RUB = 4;
	private static final int RUN_TYPE_SKIN_SPANISH_EUR = 5;
	private static final int RUN_TYPE_SKIN_ITALIAN_EUR = 6;
	private static final int RUN_TYPE_SKIN_ENGLISH_EUR = 7;

	public static void main(String[] args) throws Exception {
//		for (int i=1; i<8; i++) {
//			runForeachSkin(i, args);
//		}

	}*/

//	private static void runForeachSkin(int runType, String[] args) throws Exception {
//
//		if (null == args || args.length < 1) {
//			log.log(Level.FATAL,"Configuration file not specified, insert properties file name.");
//			return;
//		}
//		Utils.propFile = args[0];
//
//		DateRange dateRange = new DateRange();
//
//		if (args.length >= 2) {
//			if(args[1].equals("true")) {
//				try {
//					runType = Integer.parseInt(args[3]);   // take run type
//				} catch ( Exception e ) {
//					log.warn("wrong property of run type, must be number (0-7).");
//				}
//				dateRange = getDatesFromCommandLine();
//				calculateAndSendDateRangeReport(dateRange, runType);
//				calculateAndSendRangeReportMarkteBreakDown(dateRange, runType);
//				if(args[2].equals("true") ) {
//					calculateAndSendDateRangeReportSkinsPartition(dateRange, runType);  // report in skins partition
//					calculateAndSendRangeReportMarkteBreakDownSkinsPartition(dateRange, runType); // report in skins partition
//				}
//				return;
//			}
//			else if( !args[1].equals("false") ) {
//				try {
//					runType = Integer.parseInt(args[1]);   // take run type for regular reports
//				}  catch ( Exception e ) {
//					log.warn("wrong property of run type, must be number (0-2). take etrader run type");
//				}
//			}
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Daily Report.");
//		}
//
//		Calendar cal = Calendar.getInstance();
//
//		int day  = cal.get(Calendar.DAY_OF_WEEK);
//
//
//		if (day == Calendar.SUNDAY){
//			calculateAndSendWeeklyReport(runType);
//		}
//
//		calculateAndSendDailyReport(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Daily Report completed.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Daily Report Market Break Down.");
//		}
//		calculateAndSendDailyReportMarkteBreakDown(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Daily Report Market Break Down completed.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Daily Report Need Files.");
//		}
//
//		SendDailyReportNeedFiles(runType);  // etrader report
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Daily Report Need Files completed.");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Non Depositros Report.");
//		}
//
//		SendNonDepositors(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Non Depositros report completed.");
//		}
//
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting the Non Depositros 20 days Report.");
//		}
//
//		SendNonDepositors20Days(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Non Depositros 20 days  report completed.");
//		}
//	}
//
//	private static void SendNonDepositors(int runType) {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
//		// set currency for display
//		Other other = new Other();
//		other.setCurrency(getCurrencyByRunType(runType));
//
//		try {
//
//			conn = Utils.getConnection();
//			String sqlNeedFiles =
//								"select u.id as id,u.USER_NAME as username from users u " +
//								"where u.TIME_CREATED > trunc(sysdate - 3) " +
//								"and u.TIME_CREATED < trunc(sysdate - 2 ) " +
//								getSqlByRunType(runType) +
//								"and u.ID not in ( "+
//													"select u1.id "+
//													"from users u1,transactions t , transaction_types tt " +
//													"where t.user_id = u1.id " +
//													"and t.type_id = tt.id " +
//													"and tt.class_type = 1 " +
//													"and t.status_id in (2,7) " +
//													"and u1.CLASS_ID <> 0 " +
//													getSqlByRunType(runType) +
//													"and t.TIME_CREATED > trunc(sysdate -3)" +
//												" )";
//
//		pstmt = conn.prepareStatement(sqlNeedFiles);
//
//		rs = pstmt.executeQuery();
//
//		while(rs.next()) {
//			NeedFiles needFiles = new NeedFiles();
//			needFiles.setAccountNumber(rs.getString("id"));
//			needFiles.setSumDeposits(0);
//			needFiles.setUsername(rs.getString("username"));
//			needFilesArr.add(needFiles);
//		}
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get non depositors depositors " + e);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				conn.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//		}
//
//		log.log(Level.DEBUG,"going to send emails ");
//
//		sendEmailNonDepositors(needFilesArr, other, getPageHeaderByRunType(runType), runType);
//
//		log.log(Level.DEBUG,"finished to send emails");
//
//
//	}
//
//	private static void SendNonDepositors20Days(int runType) {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
//		// set currency for display
//		Other other = new Other();
//		other.setCurrency(getCurrencyByRunType(runType));
//
//		try {
//			conn = Utils.getConnection();
//			String sql20DaysNonDeposits =
//										"select distinct U.id, u.USER_NAME  as username "+
//										"from users u , transactions t , transaction_types tt "+
//										"where t.user_id = u.id  "+
//										"and t.type_id = tt.id  "+
//										"and tt.class_type = 1 "+
//										"and t.status_id in (2,7) "+
//										"and t.TYPE_ID <> 6 "+
//										"and u.CLASS_ID <> 0 "+
//										"and u.BALANCE < 10000 "+
//										getSqlByRunType(runType) +
//										"and trunc(t.time_created) <= trunc(sysdate - 20) "+
//										"and ( "+
//												"select count(distinct t2.id) "+
//												"from users u2 , transactions t2 , transaction_types tt2 "+
//												"where t2.user_id = u2.id "+
//												"and t2.type_id = tt2.id "+
//												"and tt2.class_type = 1 "+
//												"and t2.status_id in (2,7) "+
//												"and t2.TYPE_ID <> 6 "+
//												"and u2.CLASS_ID <> 0 "+
//												"and u2.id = u.id " +
//										") = 1 " +
//										"and ( " +
//												"select MAX(i.time_created) from investments i "+
//												"where i.user_id = u.id " +
//										") <= trunc(sysdate - 15) " +
//										"and ( " +
//												"select count(distinct iss.id) " +
//												"from issues iss  " +
//												"where iss.user_id = u.id " +
//												"and iss.subject_id = 9" +
//										") = 0 ";
//
//
//		log.log(Level.ERROR, "b4 20days sql");
//		pstmt = conn.prepareStatement(sql20DaysNonDeposits);
//		log.log(Level.ERROR, "after 20days sql");
//
//		rs = pstmt.executeQuery();
//
//		while(rs.next()) {
//			NeedFiles needFiles = new NeedFiles();
//			needFiles.setAccountNumber(rs.getString("id"));
//			needFiles.setSumDeposits(0);
//			needFiles.setUsername(rs.getString("username"));
//			needFilesArr.add(needFiles);
//		}
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get non depositors depositors 20 days" + e);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				conn.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//		}
//
//		log.log(Level.DEBUG,"going to send emails ");
//
//		sendEmailNonDepositors20Days(needFilesArr, other, getPageHeaderByRunType(runType), runType);
//
//		log.log(Level.DEBUG,"finished to send emails");
//
//
//	}
//
//	private static void calculateAndSendDateRangeReport(DateRange dr, int runType) {
//		Cash cash = new Cash();
//		cash.setCurrency(getCurrencyByRunType(runType));
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		// / Deposits by Credit cards
//
//		try {
//			conn = Utils.getConnection();
//
//			String sqlDepositCC =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t,users u "
//							+ "where t.USER_ID = u.ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and t.type_id = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlDepositCC += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositCC);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositCCAmount(rs.getDouble("amount"));
//				cash.setDepositCCCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits CC "+dr.getDateRange());
//		}
//
//		// Deposits by other then Credit cards
//
//		try {
//			String sqlDepositOther =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id <> 1 " // no cc
//							+ "and t.type_id <> 6 " // no revese
//							+ "and t.type_id <> 4 " // no admin
//							+ "and t.type_id <> 12 " // no bonus
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlDepositOther += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositOther);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositOtherAmount(rs.getDouble("amount"));
//				cash.setDepositOtherCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits other " +dr.getDateRange() + "  " + e);
//		}
//
//		// Deposits by Admin
//
//		try {
//			String sqlDepositAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 4 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlDepositAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositAdmin);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositAdminAmount(rs.getDouble("amount"));
//				cash.setDepositAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits admin " +dr.getDateRange() + "  " + e);
//		}
//
//		//	Deposits bonus
//
//		try {
//			String sqlDepositBonus =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 12 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlDepositBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositBonus);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositBonusAmount(rs.getDouble("amount"));
//				cash.setDepositBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits Bonus " +dr.getDateRange() + "  " + e);
//		}
//
//
//
//		// withdraws bonus
//
//		try {
//			String sqlWithdrawBonus =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 13 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlWithdrawBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawBonus);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawBonusAmount(rs.getDouble("amount"));
//				cash.setWithdrawBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
//		}
//
//
//		//	 withdraws by Admin
//
//		try {
//			String sqlWithdrawAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 5 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlWithdrawAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawAdmin);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawAdminAmount(rs.getDouble("amount"));
//				cash.setWithdrawAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
//		}
//
//
//		//	Reverse Withdraws
//		try {
//			String sqlReverseWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 6 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >=  ? "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//			sqlReverseWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlReverseWithdraws);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
//				cash.setReverseWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Reverse Withdraws " +dr.getDateRange() + "  "+ e);
//		}
//
//
//		// Executed Withdraws - withdraw that processed yesterday
//
//		try {
//			String sqlExecutedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and tt.id <> 5 "
//							+ "and tt.id <> 13 "
//							+ "and t.status_id = 2 "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//			sqlExecutedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlExecutedWithdraws);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setExecutedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Executed Withdraws "  +dr.getDateRange() + "  "+ e);
//		}
//
//		// Requested Withdraws - withdraw that Requested yesterday
//
//		try {
//			String sqlRequestedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.status_id in(4,9,6) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlRequestedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlRequestedWithdraws);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setRequestedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Requested Withdraws " + dr.getDateRange() +  " " +e);
//		}
//
//		// Net Cash = total deposits - executed withdraws
//
//		cash.setNetInOut();
//
//		// deposits headcount - how many users did any success deposit
//
//		try {
//			String sqlDepositHeadCount =
//					"select count(distinct user_id) as count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1  "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";
//
//			sqlDepositHeadCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositHeadCount);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setHeadCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Deposit Head Count  " + dr.getDateRange() + " " +e);
//		}
//
//		Investments invests = new Investments();
//		invests.setCurrency(getCurrencyByRunType(runType));
//
//		// investments - first the total of investements # of investments and
//		// headcount
//
//		try {
//			String sqlTotalTurnover =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount "
//							+ "from investments i, users u "
//							+ "where i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//			sqlTotalTurnover += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlTotalTurnover);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setTurnoverAmount(rs.getDouble("amount"));
//				invests.setTurnoverCount(rs.getLong("count"));
//				invests.setHeadCount(rs.getLong("headcount"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Turnover " + dr.getDateRange() + " " +e);
//		}
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntraday =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage "
//							+ "from investments i, users u "
//							+ "where i.is_settled = 1 "
//							+ "and i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <= ?";
//
//			sqlIntraday += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntraday);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
//				invests.setIntradayTurnoverCount(rs.getLong("count"));
//				invests.setIntradayprofitAmount(rs.getDouble("profit"));
//				invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday " + dr.getDateRange() + " " +e);
//		}
//
//		try {
//			String sqlIntradayWinningsCount =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.WIN > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? ";
//
//			sqlIntradayWinningsCount += getSqlByRunType(runType);
//
//
//			pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setWinTurnover(rs.getDouble("amount"));
//				invests.setWinCount(rs.getLong("count"));
//				invests.setWinAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Winnings Count - " + dr.getDateRange() + " " + e);
//		}
//
//
//		try {
//			String sqlIntradayLossingCount =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.LOSE > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//			sqlIntradayLossingCount += getSqlByRunType(runType);
//
//
//			pstmt = conn.prepareStatement(sqlIntradayLossingCount);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setLoseTurnover(rs.getDouble("amount"));
//				invests.setLoseCount(rs.getLong("count"));
//				invests.setLoseAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Lose Count " + dr.getDateRange() + " " + e);
//		}
//
//
//
//		Other other = new Other();
//		other.setCurrency(getCurrencyByRunType(runType));
//		// gets other stuff - new accounts
//		try {
//			String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
//										 "where to_char(TIME_CREATED,'YYYYMMDD') >=  ? "+
//										 "and to_char(TIME_CREATED,'YYYYMMDD') <=  ? ";
//
//			sqlOtherNewAccounts += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccounts);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewAccounts(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts " + dr.getDateRange()+  " " +e);
//		}
//
//		// gets other stuff - new depositor
//		try {
//			String sqlOtherNewDepositors =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count "
//							+ "from users u , transactions t , transaction_types tt "
//							+ "where t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and tt.id not in (6,12) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ? )";
//
//			sqlOtherNewDepositors += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositors);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			pstmt.setString(3,dr.getStartDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//				other.setNewDepositingAccountsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors " + dr.getDateRange()+ " " + e);
//		}
//		// get chargbacks that created yesterday
//		try {
//			String sqlChargebacks =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct t.id) as count "
//							+ "from users u ,transactions t , charge_backs cb "
//							+ "where t.charge_back_id = cb.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  ? ";
//
//			sqlChargebacks += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlChargebacks);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getStartDateForSql());
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setChargbacksAmount(rs.getDouble("amount"));
//				other.setChargbacksCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get chargebacks " + dr.getDateRange() + " " + e);
//		}
//
//		ArrayList<Ad> adList = new ArrayList<Ad>();
//		/// gets new accounts by ad
//		try {
//			String sqlOtherNewAccountsByAds = "select count(distinct u.id) as count, ads.id as id, ads.name "
//										+ "from users u, ads " +
//										 "where u.ad_id = ads.id and to_char(TIME_CREATED,'YYYYMMDD') >=  ? "+
//										 "and to_char(TIME_CREATED,'YYYYMMDD') <= ? "+
//										 getSqlByRunType(runType) + " group by ads.id, ads.name";
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccountsByAds);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				Other otherTmp = new Other();
//				otherTmp.setNewAccounts(rs.getLong("count"));
//				otherTmp.setCurrency(getCurrencyByRunType(runType));
//				Ad ad = new Ad();
//				ad.setAdId(rs.getLong("id"));
//				ad.setAdName(rs.getString("name"));
//				ad.setOther(otherTmp);
//				adList.add(ad);
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts by ads " + dr.getDateRange() + " " + e);
//		}
//
//		// gets other stuff - new depositor by ads
//		try {
//			String sqlOtherNewDepositorsByAds =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count, ads.id , ads.name "
//							+ "from users u , transactions t , transaction_types tt, ads "
//							+ "where ads.id = u.ad_id "
//							+ "and t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and tt.id not in (6,12) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ?) " +
//							getSqlByRunType(runType) + " group by  ads.id , ads.name";
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositorsByAds);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			pstmt.setString(3,dr.getStartDateForSql());
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				long adid = rs.getLong("id");
//				boolean found = false;
//				for(int i=0;i < adList.size() && !found ;i++){
//					Ad ad = adList.get(i);
//					if (ad.getAdId() == adid){
//						Other otherTmp = ad.getOther();
//						otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//						otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//						found = true;
//					} else {
//						if (i == adList.size() -1){
//							Other otherTmp = new Other();
//							otherTmp.setNewAccounts(0);
//							otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//							otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//							otherTmp.setCurrency(getCurrencyByRunType(runType));
//							Ad ad_new = new Ad();
//							ad_new.setAdId(rs.getLong("id"));
//							ad_new.setAdName(rs.getString("name"));
//							ad_new.setOther(otherTmp);
//							adList.add(ad_new);
//							break;
//						}
//					}
//
//				}
//
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors by ads " + dr.getDateRange() + " " +e);
//		}
//
//		log.log(Level.DEBUG,"finished all queries - " + dr.getDateRange());
//		// Now lets send emails with the report :
//
//		log.log(Level.DEBUG,"going to send emails - " + dr.getDateRange());
//
//		String subject = getEmailSubjectByRunType(runType) + " report :"+dr.getDateRange();
//
//		sendEmail(cash,invests,other,adList,getPageHeaderByRunType(runType),"report_date_range.html",subject);
//
//		log.log(Level.DEBUG,"finished to send emails - " + dr.getDateRange());
//
//		try {
//			rs.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			pstmt.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			conn.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//
//	}
//
//	/**
//	 * Send and Create Date Range Report by skins partition
//	 * @param dr
//	 */
//	private static void calculateAndSendDateRangeReportSkinsPartition(DateRange dr, int runType) {
//
//		ArrayList<Skins> skins = getAllSkins();   // get all skins list
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		// Run the report for each skin and currencies
//
//		for( Skins skin : skins ) {   // over all skins
//
//			for ( SkinCurrency sc : skin.getSkinCurrenciesList() ) {  // over all currencies of the skin
//
//				Cash cash = new Cash();
//				// / Deposits by Credit cards
//
//				try {
//					conn = Utils.getConnection();
//
//					String sqlDepositCC =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t,users u "
//									+ "where t.USER_ID = u.ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and t.type_id = 1 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlDepositCC += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlDepositCC);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setDepositCCAmount(rs.getDouble("amount"));
//						cash.setDepositCCCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get deposits CC "+dr.getDateRange());
//				}
//
//				// Deposits by other then Credit cards
//
//				try {
//					String sqlDepositOther =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 1 "
//									+ "and t.type_id <> 1 " // no cc
//									+ "and t.type_id <> 6 " // no revese
//									+ "and t.type_id <> 4 " // no admin
//									+ "and t.type_id <> 12 " // no bonus
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlDepositOther += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlDepositOther);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setDepositOtherAmount(rs.getDouble("amount"));
//						cash.setDepositOtherCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get deposits other " +dr.getDateRange() + "  " + e);
//				}
//
//				// Deposits by Admin
//
//				try {
//					String sqlDepositAdmin =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 1 "
//									+ "and t.type_id = 4 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlDepositAdmin += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlDepositAdmin);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setDepositAdminAmount(rs.getDouble("amount"));
//						cash.setDepositAdminCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get deposits admin " +dr.getDateRange() + "  " + e);
//				}
//
//				//	Deposits bonus
//
//				try {
//					String sqlDepositBonus =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 1 "
//									+ "and t.type_id = 12 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlDepositBonus += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlDepositBonus);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setDepositBonusAmount(rs.getDouble("amount"));
//						cash.setDepositBonusCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get deposits Bonus " +dr.getDateRange() + "  " + e);
//				}
//
//
//
//				// withdraws bonus
//
//				try {
//					String sqlWithdrawBonus =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 2 "
//									+ "and t.type_id = 13 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlWithdrawBonus += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlWithdrawBonus);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setWithdrawBonusAmount(rs.getDouble("amount"));
//						cash.setWithdrawBonusCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
//				}
//
//
//				//	 withdraws by Admin
//
//				try {
//					String sqlWithdrawAdmin =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 2 "
//									+ "and t.type_id = 5 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlWithdrawAdmin += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlWithdrawAdmin);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setWithdrawAdminAmount(rs.getDouble("amount"));
//						cash.setWithdrawAdminCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Withdraws admin " +dr.getDateRange() + "  " + e);
//				}
//
//
//				//	Reverse Withdraws
//				try {
//					String sqlReverseWithdraws =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 1 "
//									+ "and t.type_id = 6 "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >=  ? "
//									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//					sqlReverseWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlReverseWithdraws);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//					if (rs.next()) {
//						cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
//						cash.setReverseWithdrawsCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Reverse Withdraws " +dr.getDateRange() + "  "+ e);
//				}
//
//
//				// Executed Withdraws - withdraw that processed yesterday
//
//				try {
//					String sqlExecutedWithdraws =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 2 "
//									+ "and tt.id <> 5 "
//									+ "and tt.id <> 13 "
//									+ "and t.status_id = 2 "
//									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//					sqlExecutedWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlExecutedWithdraws);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
//						cash.setExecutedWithdrawsCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Executed Withdraws "  +dr.getDateRange() + "  "+ e);
//				}
//
//				// Requested Withdraws - withdraw that Requested yesterday
//
//				try {
//					String sqlRequestedWithdraws =
//							"select sum(amount/100) as amount, count(t.id) count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 2 "
//									+ "and t.status_id in(4,9,6) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlRequestedWithdraws += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlRequestedWithdraws);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
//						cash.setRequestedWithdrawsCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Requested Withdraws " + dr.getDateRange() +  " " +e);
//				}
//
//				// Net Cash = total deposits - executed withdraws
//
//				cash.setNetInOut();
//
//				// deposits headcount - how many users did any success deposit
//
//				try {
//					String sqlDepositHeadCount =
//							"select count(distinct user_id) as count "
//									+ "from transactions t, transaction_types tt, users u "
//									+ "where t.type_id = tt.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and tt.class_type = 1  "
//									+ "and t.status_id in (2,7) "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <= ?";
//
//					sqlDepositHeadCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlDepositHeadCount);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						cash.setHeadCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Deposit Head Count  " + dr.getDateRange() + " " +e);
//				}
//
//				Investments invests = new Investments();
//
//				// investments - first the total of investements # of investments and
//				// headcount
//
//				try {
//					String sqlTotalTurnover =
//							"select sum(amount/100) as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount "
//									+ "from investments i, users u "
//									+ "where i.is_canceled = 0 "
//									+ "and u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ?";
//
//					sqlTotalTurnover += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlTotalTurnover);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						invests.setTurnoverAmount(rs.getDouble("amount"));
//						invests.setTurnoverCount(rs.getLong("count"));
//						invests.setHeadCount(rs.getLong("headcount"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Turnover " + dr.getDateRange() + " " +e);
//				}
//
//				// investments - that settled yesterday.
//
//				try {
//					String sqlIntraday =
//							"select sum(amount/100) as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage "
//									+ "from investments i, users u "
//									+ "where i.is_settled = 1 "
//									+ "and i.is_canceled = 0 "
//									+ "and u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <= ?";
//
//					sqlIntraday += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlIntraday);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
//						invests.setIntradayTurnoverCount(rs.getLong("count"));
//						invests.setIntradayprofitAmount(rs.getDouble("profit"));
//						invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday " + dr.getDateRange() + " " +e);
//				}
//
//				try {
//					String sqlIntradayWinningsCount =
//							"select sum(amount/100) as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//							"from investments i, users u "+
//							"where i.is_settled = 1 "+
//							"and i.is_canceled = 0 "+
//							"and u.ID = i.USER_ID "+
//							"and u.CLASS_ID <> 0  "+
//							"and i.WIN > 0 "+
//							"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? " +
//							"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? ";
//
//					sqlIntradayWinningsCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//
//					pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						invests.setWinTurnover(rs.getDouble("amount"));
//						invests.setWinCount(rs.getLong("count"));
//						invests.setWinAmount(rs.getDouble("profit"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday Winnings Count - " + dr.getDateRange() + " " + e);
//				}
//
//
//				try {
//					String sqlIntradayLossingCount =
//							"select sum(amount/100) as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//							"from investments i, users u "+
//							"where i.is_settled = 1 "+
//							"and i.is_canceled = 0 "+
//							"and u.ID = i.USER_ID "+
//							"and u.CLASS_ID <> 0  "+
//							"and i.LOSE > 0 "+
//							"and to_char(i.TIME_SETTLED,'YYYYMMDD') >=  ? "+
//							"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ?";
//
//					sqlIntradayLossingCount += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//
//					pstmt = conn.prepareStatement(sqlIntradayLossingCount);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						invests.setLoseTurnover(rs.getDouble("amount"));
//						invests.setLoseCount(rs.getLong("count"));
//						invests.setLoseAmount(rs.getDouble("profit"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday Lose Count " + dr.getDateRange() + " " + e);
//				}
//
//
//
//				Other other = new Other();
//				// gets other stuff - new accounts
//				try {
//					String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
//												 "where to_char(TIME_CREATED,'YYYYMMDD') >=  ? "+
//												 "and to_char(TIME_CREATED,'YYYYMMDD') <=  ? ";
//
//					sqlOtherNewAccounts += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlOtherNewAccounts);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						other.setNewAccounts(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get new accounts " + dr.getDateRange()+  " " +e);
//				}
//
//				// gets other stuff - new depositor
//				try {
//					String sqlOtherNewDepositors =
//							"select sum(amount/100) as amount, count(distinct u.id) as count "
//									+ "from users u , transactions t , transaction_types tt "
//									+ "where t.user_id = u.id "
//									+ "and t.type_id = tt.id "
//									+ "and tt.class_type = 1 "
//									+ "and t.status_id in (2,7) "
//									+ "and tt.id not in (6,12) "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
//									+ "and not exists (select distinct t2.user_id "
//									+ "from transactions t2, transaction_types tt2 "
//									+ "where t2.type_id = tt2.id "
//									+ "and t2.status_id in (2,7) "
//									+ "and tt2.class_type = 1 "
//									+ "and tt2.id not in (6,12) "
//									+ "and t2.user_id=t.user_id "
//									+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ? )";
//
//					sqlOtherNewDepositors += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlOtherNewDepositors);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					pstmt.setString(3,dr.getStartDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//						other.setNewDepositingAccountsCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get new depositors " + dr.getDateRange()+ " " + e);
//				}
//				// get chargbacks that created yesterday
//				try {
//					String sqlChargebacks =
//							"select sum(amount/100) as amount, count(distinct t.id) as count "
//									+ "from users u ,transactions t , charge_backs cb "
//									+ "where t.charge_back_id = cb.id "
//									+ "and u.ID = t.USER_ID "
//									+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  ? ";
//
//					sqlChargebacks += getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId());
//
//					pstmt = conn.prepareStatement(sqlChargebacks);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getStartDateForSql());
//					rs = pstmt.executeQuery();
//
//					if (rs.next()) {
//						other.setChargbacksAmount(rs.getDouble("amount"));
//						other.setChargbacksCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get chargebacks " + dr.getDateRange() + " " + e);
//				}
//
//				ArrayList<Ad> adList = new ArrayList<Ad>();
//				/// gets new accounts by ad
//				try {
//					String sqlOtherNewAccountsByAds = "select count(distinct u.id) as count, ads.id as id, ads.name "
//												+ "from users u, ads " +
//												 "where u.ad_id = ads.id and to_char(TIME_CREATED,'YYYYMMDD') >=  ? "+
//												 "and to_char(TIME_CREATED,'YYYYMMDD') <= ? "+
//												 getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by ads.id, ads.name";
//
//					pstmt = conn.prepareStatement(sqlOtherNewAccountsByAds);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while (rs.next()) {
//						Other otherTmp = new Other();
//						otherTmp.setNewAccounts(rs.getLong("count"));
//						Ad ad = new Ad();
//						ad.setAdId(rs.getLong("id"));
//						ad.setAdName(rs.getString("name"));
//						ad.setOther(otherTmp);
//						adList.add(ad);
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get new accounts by ads " + dr.getDateRange() + " " + e);
//				}
//
//				// gets other stuff - new depositor by ads
//				try {
//					String sqlOtherNewDepositorsByAds =
//							"select sum(amount/100) as amount, count(distinct u.id) as count, ads.id , ads.name "
//									+ "from users u , transactions t , transaction_types tt, ads "
//									+ "where ads.id = u.ad_id "
//									+ "and t.user_id = u.id "
//									+ "and t.type_id = tt.id "
//									+ "and tt.class_type = 1 "
//									+ "and t.status_id in (2,7) "
//									+ "and tt.id not in (6,12) "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >=  ? "
//									+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  ? "
//									+ "and not exists (select distinct t2.user_id "
//									+ "from transactions t2, transaction_types tt2 "
//									+ "where t2.type_id = tt2.id "
//									+ "and t2.status_id in (2,7) "
//									+ "and tt2.class_type = 1 "
//									+ "and tt2.id not in (6,12) "
//									+ "and t2.user_id=t.user_id "
//									+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  ?) " +
//									getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by  ads.id , ads.name";
//
//					pstmt = conn.prepareStatement(sqlOtherNewDepositorsByAds);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					pstmt.setString(3,dr.getStartDateForSql());
//					rs = pstmt.executeQuery();
//
//					while (rs.next()) {
//						long adid = rs.getLong("id");
//						boolean found = false;
//						for(int i=0;i < adList.size() && !found ;i++){
//							Ad ad = adList.get(i);
//							if (ad.getAdId() == adid){
//								Other otherTmp = ad.getOther();
//								otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//								otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//								found = true;
//							} else {
//								if (i == adList.size() -1){
//									Other otherTmp = new Other();
//									otherTmp.setNewAccounts(0);
//									otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//									otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//									Ad ad_new = new Ad();
//									ad_new.setAdId(rs.getLong("id"));
//									ad_new.setAdName(rs.getString("name"));
//									ad_new.setOther(otherTmp);
//									adList.add(ad_new);
//									break;
//								}
//							}
//
//						}
//
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get new depositors by ads " + dr.getDateRange() + " " +e);
//				}
//
//				// set all objects for this run (skin-currency)
//				sc.setCash(cash);
//				sc.setInvests(invests);
//				sc.setOther(other);
//				sc.setAdList(adList);
//
//				log.log(Level.DEBUG,"finished all queries for skinid: " + skin.getId() +  " currency: " + sc.getCode() +
//						" " + dr.getDateRange());
//
//				try {
//					rs.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//				try {
//					pstmt.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//				try {
//					conn.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//			}  // end currencies loop
//
//		}  // end skins loop
//
//		// Now lets send emails with the report :
//		log.log(Level.DEBUG,"going to send emails - " + dr.getDateRange());
//		String subject = getEmailSubjectByRunType(runType) +  " report - skins partition :"+dr.getDateRange();
//		sendSkinsPartitionEmail(skins,"report_date_range_skinsPartition.html",subject);
//
//		log.log(Level.DEBUG,"finished to send emails - " + dr.getDateRange());
//
//	}
//
//	private static DateRange getDatesFromCommandLine() {
//		System.out.println("Start Running Report:");
//		System.out.println("Please enter start date dd-mm-yyyy");
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		String startDate = "";
//		boolean getStartDate = true;
//		while (getStartDate){
//			try {
//				startDate = br.readLine();
//				getStartDate = dateNotOK(startDate);
//		      } catch (IOException ioe) {
//		         System.out.println("IO error trying to read start date, please try again");
//		      }
//		}
//		System.out.println("Please enter end date dd-mm-yyyy");
//		String endDate = "";
//		boolean getEndDate = true;
//		while (getEndDate){
//			try {
//				endDate = br.readLine();
//				getEndDate = dateNotOK(endDate);
//		      } catch (IOException ioe) {
//		         System.out.println("IO error trying to read end date, please try again");
//		      }
//		}
//		DateRange dr = new DateRange();
//		dr.setStartDate(startDate);
//		dr.setEndDate(endDate);
//		return dr;
//	}
//
//	private static boolean dateNotOK(String strDate) {
//		String [] arr =  strDate.split("-");
//		if (arr.length != 3){
//			System.out.println("Problem with date, please try again format must be dd-mm-yyyy");
//			return true;
//		}
//		try {
//			Date date = (Date) new SimpleDateFormat("dd-MM-yyyyy").parse(strDate);
//		} catch (Exception e) {
//			System.out.println("Problem with date, please try again format must be dd-mm-yyyy");
//			e.printStackTrace();
//			return true;
//		}
//
//		return false;
//	}
//
//	private static void calculateAndSendWeeklyReport(int runType) {
//
//		Cash cash = new Cash();
//		cash.setCurrency(getCurrencyByRunType(runType));
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		//  Deposits by Credit cards
//
//		try {
//			conn = Utils.getConnection();
//
//			String sqlDepositCC =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t,users u "
//							+ "where t.USER_ID = u.ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and t.type_id = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD')"
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositCC += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositCC);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositCCAmount(rs.getDouble("amount"));
//				cash.setDepositCCCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits CC weekly");
//		}
//
//       //  Deposits by other then Credit cards No Admin
//
//		try {
//			String sqlDepositOther =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id <> 1 "
//							+ "and t.type_id <> 6 "
//							+ "and t.type_id <> 4 "
//							+ "and t.type_id <> 12 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositOther += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositOther);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositOtherAmount(rs.getDouble("amount"));
//				cash.setDepositOtherCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits other weekly " + e);
//		}
//
//		// Deposits by other then Credit cards - ADMIN
//
//		try {
//			String sqlDepositAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 4 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositAdmin);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositAdminAmount(rs.getDouble("amount"));
//				cash.setDepositAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits Admin weekly " + e);
//		}
//
//
//    // Deposits Bonus
//
//		try {
//			String sqlDepositBonus =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 12 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositBonus);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositBonusAmount(rs.getDouble("amount"));
//				cash.setDepositBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits Bonus weekly " + e);
//		}
//
//
//		// Withdraws - Bonus
//
//		try {
//			String sqlWithdrawBonus =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 13 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlWithdrawBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawBonus);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawBonusAmount(rs.getDouble("amount"));
//				cash.setWithdrawBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Withdraw Bonus weekly " + e);
//		}
//
//
//		//	Withdraws - ADMIN
//
//		try {
//			String sqlWithdrawAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 5 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlWithdrawAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawAdmin);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawAdminAmount(rs.getDouble("amount"));
//				cash.setWithdrawAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Withdraw Admin weekly " + e);
//		}
//
//		//	Reverse Withdraws
//		try {
//			String sqlReverseWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 6 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlReverseWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlReverseWithdraws);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
//				cash.setReverseWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Reverse Withdraws weekly" + e);
//		}
//
//
//		// Executed Withdraws - withdraw that processed yesterday - no admin
//
//		try {
//			String sqlExecutedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.status_id = 2 "
//							+ "and tt.id <> 5 "
//							+ "and tt.id <> 13 "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') > to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlExecutedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlExecutedWithdraws);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setExecutedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Executed Withdraws weekly" + e);
//		}
//
//		// Requested Withdraws - withdraw that Requested yesterday
//
//		try {
//			String sqlRequestedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.status_id in(4,9,6) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlRequestedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlRequestedWithdraws);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setRequestedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Requested Withdraws weekly" + e);
//		}
//
//		// Net Cash = total deposits - executed withdraws
//
//		cash.setNetInOut();
//
//		// deposits headcount - how many users did any success deposit
//
//		try {
//			String sqlDepositHeadCount =
//					"select count(distinct user_id) as count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1  "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositHeadCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositHeadCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setHeadCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Deposit Head Count weekly " + e);
//		}
//
//		Investments invests = new Investments();
//		invests.setCurrency(getCurrencyByRunType(runType));
//
//		// investments - first the total of investements # of investments and
//		// headcount
//
//		try {
//			String sqlTotalTurnover =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount "
//							+ "from investments i, users u "
//							+ "where i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlTotalTurnover += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlTotalTurnover);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setTurnoverAmount(rs.getDouble("amount"));
//				invests.setTurnoverCount(rs.getLong("count"));
//				invests.setHeadCount(rs.getLong("headcount"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Turnover weekly" + e);
//		}
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntraday =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage "
//							+ "from investments i, users u "
//							+ "where i.is_settled = 1 "
//							+ "and i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlIntraday += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntraday);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
//				invests.setIntradayTurnoverCount(rs.getLong("count"));
//				invests.setIntradayprofitAmount(rs.getDouble("profit"));
//				invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday weekly" + e);
//		}
//
//		try {
//			String sqlIntradayWinningsCount =
//					"select " + getSqlAmountByRunType(runType) + "  as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.WIN > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') " +
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//
//			sqlIntradayWinningsCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setWinTurnover(rs.getDouble("amount"));
//				invests.setWinCount(rs.getLong("count"));
//				invests.setWinAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Winnings Count - weekly " + e);
//		}
//
//
//		try {
//			String sqlIntradayLossingCount =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.LOSE > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlIntradayLossingCount += getSqlByRunType(runType);
//
//
//			pstmt = conn.prepareStatement(sqlIntradayLossingCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setLoseTurnover(rs.getDouble("amount"));
//				invests.setLoseCount(rs.getLong("count"));
//				invests.setLoseAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Lose Count weekly" + e);
//		}
//
//
//
//		Other other = new Other();
//		other.setCurrency(getCurrencyByRunType(runType));
//		// gets other stuff - new accounts
//		try {
//			String sqlOtherNewAccounts = "select count(distinct id) as count " + "from users u " +
//										 "where to_char(TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "+
//										 "and to_char(TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') ";
//
//			sqlOtherNewAccounts += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccounts);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewAccounts(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts weekly" + e);
//		}
//
//		// gets other stuff - new depositor
//		try {
//			String sqlOtherNewDepositors =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count "
//							+ "from users u , transactions t , transaction_types tt "
//							+ "where t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and tt.id not in (6,12) "
//							+ "and t.status_id in (2,7) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate-8,'YYYYMMDD')) ";
//
//			sqlOtherNewDepositors += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositors);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//				other.setNewDepositingAccountsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors weekly" + e);
//		}
//		// get chargbacks that created yesterday
//		try {
//			String sqlChargebacks =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct t.id) as count "
//							+ "from users u ,transactions t , charge_backs cb "
//							+ "where t.charge_back_id = cb.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') ";
//
//			sqlChargebacks += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlChargebacks);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setChargbacksAmount(rs.getDouble("amount"));
//				other.setChargbacksCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get chargebacks weekly" + e);
//		}
//
//		ArrayList<Ad> adList = new ArrayList<Ad>();
//		/// gets new accounts by ad
//		try {
//			String sqlOtherNewAccountsByAds = "select count(distinct u.id) as count, ads.id as id, ads.name "
//											  + "from users u, ads "
//											  + "where u.ad_id = ads.id and to_char(TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//										      + "and to_char(TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
//										      + getSqlByRunType(runType) + " group by ads.id, ads.name";
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccountsByAds);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				Other otherTmp = new Other();
//				otherTmp.setNewAccounts(rs.getLong("count"));
//				otherTmp.setCurrency(getCurrencyByRunType(runType));
//				Ad ad = new Ad();
//				ad.setAdId(rs.getLong("id"));
//				ad.setAdName(rs.getString("name"));
//				ad.setOther(otherTmp);
//				adList.add(ad);
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts by ads weekly" + e);
//		}
//
//		// gets other stuff - new depositor by ads
//		try {
//			String sqlOtherNewDepositorsByAds =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count, ads.id , ads.name "
//							+ "from users u , transactions t , transaction_types tt, ads "
//							+ "where ads.id = u.ad_id "
//							+ "and t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and tt.id not in (6,12) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') >  to_char(sysdate -8,'YYYYMMDD') "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate -1,'YYYYMMDD') "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <=  to_char(sysdate-8,'YYYYMMDD')) "
//							+ getSqlByRunType(runType) + " group by  ads.id , ads.name";
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositorsByAds);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				long adid = rs.getLong("id");
//				boolean found = false;
//				for(int i=0;i < adList.size() && !found ;i++){
//					Ad ad = adList.get(i);
//					if (ad.getAdId() == adid){
//						Other otherTmp = ad.getOther();
//						otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//						otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//						found = true;
//					} else {
//						if (i == adList.size() -1){
//							Other otherTmp = new Other();
//							otherTmp.setNewAccounts(0);
//							otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//							otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//							otherTmp.setCurrency(getCurrencyByRunType(runType));
//							Ad ad_new = new Ad();
//							ad_new.setAdId(rs.getLong("id"));
//							ad_new.setAdName(rs.getString("name"));
//							ad_new.setOther(otherTmp);
//							adList.add(ad_new);
//							break;
//						}
//					}
//
//				}
//
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors by ads weekly" + e);
//		}
//
//		log.log(Level.DEBUG,"finished all queries - weekly ");
//		// Now lets send emails with the report :
//
//		log.log(Level.DEBUG,"going to send emails - weekly");
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String endDate = sdf.format(cal.getTime());
//		cal.add(Calendar.DAY_OF_YEAR,-6);
//		String startDate = sdf.format(cal.getTime());
//		String subject = getEmailSubjectByRunType(runType) + " weekly report :" + startDate + " - " + endDate;
//
//		sendEmail(cash,invests,other,adList,getPageHeaderByRunType(runType),"report_weekly.html",subject);
//
//		log.log(Level.DEBUG,"finished to send emails - weekly");
//
//		try {
//			rs.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			pstmt.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			conn.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//	}
//
//	/**
//	 * This report just for etrader, in Ao we are doning this automatically
//	 */
//	private static void SendDailyReportNeedFiles(int runType) {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<NeedFiles> needFilesArr = new ArrayList<NeedFiles>();
//		int totalDepositsAmountForFiles = 10000; // defualt value.
//
//		try {
//
//			conn = Utils.getConnection();
//			String sqlNeedFiles =
//								"select u.ID as id,u.USER_NAME as username "+
//								"from transactions t, transaction_types tt, users u "+
//								"where t.type_id = tt.id  "+
//								"and u.ID = t.USER_ID "+
//								"and u.CLASS_ID <> 0 "+
//								"and tt.class_type = 1 "+
//								"and t.type_id <> 4 "+
//								"and t.type_id <> 6 "+
//								"and t.type_id <> 12 "+
//								"and t.status_id in (2,7) "+
//								"and to_char(t.TIME_CREATED,'YYYYMMDD') < to_char(sysdate,'YYYYMMDD') " +
//								"and u.skin_id = 1 " +
//								"group by u.ID,u.USER_NAME " +
//								"having sum(amount/100) > ? "+
//								"minus "+
//								"select u.ID as id,u.USER_NAME as username "+
//								"from transactions t, transaction_types tt, users u "+
//								"where t.type_id = tt.id "+
//								"and u.ID = t.USER_ID "+
//								"and u.CLASS_ID <> 0 "+
//								"and tt.class_type = 1 "+
//								"and t.type_id <> 4 "+
//								"and t.type_id <> 6 "+
//								"and t.type_id <> 12 "+
//								"and t.status_id in (2,7) "+
//								"and to_char(t.TIME_CREATED,'YYYYMMDD') < to_char(sysdate -1,'YYYYMMDD') " +
//								"and u.skin_id = 1 " +
//								"group by u.ID,u.USER_NAME " +
//								"having sum(amount/100) > ? ";
//
//		pstmt = conn.prepareStatement(sqlNeedFiles);
//
//		try {
//			totalDepositsAmountForFiles = Integer.parseInt(Utils.getProperty("total.deposits.need.files"));
//		} catch (NumberFormatException e){
//			log.log(Level.WARN,"cant get total.deposits.need.files asetting it to 10K" + e);
//			totalDepositsAmountForFiles = 10000;
//		}
//
//		pstmt.setInt(1,totalDepositsAmountForFiles);
//		pstmt.setInt(2,totalDepositsAmountForFiles);
//
//		rs = pstmt.executeQuery();
//
//		while(rs.next()) {
//			NeedFiles needFiles = new NeedFiles();
//			needFiles.setAccountNumber(rs.getString("id"));
//			needFiles.setSumDeposits(totalDepositsAmountForFiles);
//			needFiles.setUsername(rs.getString("username"));
//			needFilesArr.add(needFiles);
//		}
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get need files depositors " + e);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				conn.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//		}
//
//		log.log(Level.DEBUG,"going to send emails ");
//
//		sendEmailNeedFiles(needFilesArr,totalDepositsAmountForFiles, runType);
//
//		log.log(Level.DEBUG,"finished to send emails");
//
//	}
//
//	private static void calculateAndSendRangeReportMarkteBreakDown(DateRange dr, int runType) {
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		// investments - first the total of investements # of investments and
//		// headcount
//
//		try {
//
//			conn = Utils.getConnection();
//
//			String sqlTotalTurnover =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ," +
//							"m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u, investments i, opportunities o, markets m, market_groups g,skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//
//
//							+ "and i.is_canceled = 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME ";
//
//			pstmt = conn.prepareStatement(sqlTotalTurnover);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Investments inv = new Investments();
//				inv.setTurnoverAmount(rs.getDouble("amount"));
//				inv.setTurnoverCount(rs.getLong("count"));
//				inv.setHeadCount(rs.getLong("headcount"));
//				inv.setCurrency(getCurrencyByRunType(runType));
//				Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//				marktes.put(rs.getLong("market_id"),market);
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
//		}
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntraday =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntraday);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
//				m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
//				m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
//				m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
//		}
//
//		try {
//			String sqlIntradayWinnings =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count," +
//							"-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.win > 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntradayWinnings);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
//				m.getInvetsments().setWinCount(rs.getLong("count"));
//				m.getInvetsments().setWinAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
//		}
//
//
//
//		try {
//			String sqlIntradayLose =
//							"select " + getSqlAmountByRunType(runType) + " as amount," +
//							" count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.lose > 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntradayLose);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
//				m.getInvetsments().setLoseCount(rs.getLong("count"));
//				m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
//		}
//
//		try {
//			String sqlCalls =
//							"select " + getSqlAmountByRunType(runType) + " as amount," +
//							" count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.TYPE_ID = 1 " // calls
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlCalls);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setLoseTurnover(0);
//					inv.setLoseCount(0);
//					inv.setLoseAmount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setCallAmount(rs.getDouble("amount"));
//				m.getInvetsments().setCallCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
//		}
//
//		try {
//			String sqlPuts =
//							"select " + getSqlAmountByRunType(runType) + " as amount, " +
//							"count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.TYPE_ID = 2 " //puts
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlPuts);
//			pstmt.setString(1,dr.getStartDateForSql());
//			pstmt.setString(2,dr.getEndDateForSql());
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setLoseTurnover(0);
//					inv.setLoseCount(0);
//					inv.setLoseAmount(0);
//					inv.setCallAmount(0);
//					inv.setCallCount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setPutAmount(rs.getDouble("amount"));
//				m.getInvetsments().setPutCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
//		}
//
//		sendEmailDateRangeMarketBreakDown(dr, getPageHeaderByRunType(runType), runType);
//
//	}
//
//
//	/**
//	 * Calaulate and Send Range Report MarkteBreakDown with Skins Partition
//	 * @param dr
//	 */
//	private static void calculateAndSendRangeReportMarkteBreakDownSkinsPartition(DateRange dr, int runType) {
//
//		ArrayList<Skins> skins = getAllSkins();   // get all skins list
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
////		 Run the report for each skin and currencies
//
//		for( Skins skin : skins ) {   // over all skins
//
//			for ( SkinCurrency sc : skin.getSkinCurrenciesList() ) {  // over all currencies of the skin
//
//
//				// investments - first the total of investements # of investments and
//				// headcount
//
//				try {
//
//					conn = Utils.getConnection();
//
//					String sqlTotalTurnover =
//									"select sum(amount/100) as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ," +
//									"m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u, investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg  "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_canceled = 0 "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME ";
//
//					pstmt = conn.prepareStatement(sqlTotalTurnover);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//						Investments inv = new Investments();
//						inv.setTurnoverAmount(rs.getDouble("amount"));
//						inv.setTurnoverCount(rs.getLong("count"));
//						inv.setHeadCount(rs.getLong("headcount"));
//						Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//						marktes.put(rs.getLong("market_id"),market);
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
//				}
//
//				// investments - that settled yesterday.
//
//				try {
//					String sqlIntraday =
//							"select sum(amount/100) as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_settled =1 "
//									+ "and i.is_canceled = 0 "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_CREATED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";
//
//					pstmt = conn.prepareStatement(sqlIntraday);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//
//						Market m = marktes.get(rs.getLong("market_id"));
//						if (m == null) {
//							Investments inv = new Investments();
//							inv.setTurnoverAmount(0);
//							inv.setTurnoverCount(0);
//							inv.setHeadCount(0);
//							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//							marktes.put(rs.getLong("market_id"),m);
//						}
//						m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
//						m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
//						m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
//						m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
//				}
//
//				try {
//					String sqlIntradayWinnings =
//									"select sum(amount/100) as amount, count(distinct i.id) as count," +
//									"-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_settled =1 "
//									+ "and i.is_canceled = 0 "
//									+ "and i.win > 0 "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";
//
//					pstmt = conn.prepareStatement(sqlIntradayWinnings);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//						Market m = marktes.get(rs.getLong("market_id"));
//						if (m == null) {
//							Investments inv = new Investments();
//							inv.setTurnoverAmount(0);
//							inv.setTurnoverCount(0);
//							inv.setHeadCount(0);
//							inv.setIntradayTurnoverAmount(0);
//							inv.setIntradayTurnoverCount(0);
//							inv.setIntradayprofitAmount(0);
//							inv.setIntradayprofitPrecentage(0);
//							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//							marktes.put(rs.getLong("market_id"),m);
//						}
//						m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
//						m.getInvetsments().setWinCount(rs.getLong("count"));
//						m.getInvetsments().setWinAmount(rs.getDouble("profit"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
//				}
//
//				try {
//					String sqlIntradayLose =
//									"select sum(amount/100) as amount," +
//									" count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_settled =1 "
//									+ "and i.is_canceled = 0 "
//									+ "and i.lose > 0 "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";
//
//					pstmt = conn.prepareStatement(sqlIntradayLose);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//						Market m = marktes.get(rs.getLong("market_id"));
//						if (m == null) {
//							Investments inv = new Investments();
//							inv.setTurnoverAmount(0);
//							inv.setTurnoverCount(0);
//							inv.setHeadCount(0);
//							inv.setIntradayTurnoverAmount(0);
//							inv.setIntradayTurnoverCount(0);
//							inv.setIntradayprofitAmount(0);
//							inv.setIntradayprofitPrecentage(0);
//							inv.setWinTurnover(0);
//							inv.setWinCount(0);
//							inv.setWinAmount(0);
//							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//							marktes.put(rs.getLong("market_id"),m);
//						}
//						m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
//						m.getInvetsments().setLoseCount(rs.getLong("count"));
//						m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
//				}
//
//				try {
//					String sqlCalls =
//									"select sum(amount/100) as amount," +
//									" count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_settled =1 "
//									+ "and i.is_canceled = 0 "
//									+ "and i.TYPE_ID = 1 " // calls
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";
//
//					pstmt = conn.prepareStatement(sqlCalls);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//						Market m = marktes.get(rs.getLong("market_id"));
//						if (m == null) {
//							Investments inv = new Investments();
//							inv.setTurnoverAmount(0);
//							inv.setTurnoverCount(0);
//							inv.setHeadCount(0);
//							inv.setIntradayTurnoverAmount(0);
//							inv.setIntradayTurnoverCount(0);
//							inv.setIntradayprofitAmount(0);
//							inv.setIntradayprofitPrecentage(0);
//							inv.setWinTurnover(0);
//							inv.setWinCount(0);
//							inv.setWinAmount(0);
//							inv.setLoseTurnover(0);
//							inv.setLoseCount(0);
//							inv.setLoseAmount(0);
//							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//							marktes.put(rs.getLong("market_id"),m);
//						}
//						m.getInvetsments().setCallAmount(rs.getDouble("amount"));
//						m.getInvetsments().setCallCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
//				}
//
//				try {
//					String sqlPuts =
//									"select sum(amount/100) as amount, " +
//									"count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//									+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//									+ "where u.ID = i.USER_ID "
//									+ "and u.CLASS_ID <> 0 "
//									+ "and i.OPPORTUNITY_ID = o.ID "
//									+ "and o.MARKET_ID =  m.ID "
//									+ "and smgm.skin_market_group_id = smg.id "
//									+ "and smg.market_group_id = g.ID "
//									+ "and m.id = smgm.market_id "
//									+ "and smg.skin_id = u.skin_id "
//									+ "and i.is_settled =1 "
//									+ "and i.is_canceled = 0 "
//									+ "and i.TYPE_ID = 2 " //puts
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') >= ? "
//									+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') <=  ? "
//									+ getSqlBySkinPartintion(skin.getId(), sc.getCurrencyId()) + " group by m.id , m.NAME , g.NAME";
//
//					pstmt = conn.prepareStatement(sqlPuts);
//					pstmt.setString(1,dr.getStartDateForSql());
//					pstmt.setString(2,dr.getEndDateForSql());
//					rs = pstmt.executeQuery();
//
//					while(rs.next()) {
//						Market m = marktes.get(rs.getLong("market_id"));
//						if (m == null) {
//							Investments inv = new Investments();
//							inv.setTurnoverAmount(0);
//							inv.setTurnoverCount(0);
//							inv.setHeadCount(0);
//							inv.setIntradayTurnoverAmount(0);
//							inv.setIntradayTurnoverCount(0);
//							inv.setIntradayprofitAmount(0);
//							inv.setIntradayprofitPrecentage(0);
//							inv.setWinTurnover(0);
//							inv.setWinCount(0);
//							inv.setWinAmount(0);
//							inv.setLoseTurnover(0);
//							inv.setLoseCount(0);
//							inv.setLoseAmount(0);
//							inv.setCallAmount(0);
//							inv.setCallCount(0);
//							m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//							marktes.put(rs.getLong("market_id"),m);
//						}
//						m.getInvetsments().setPutAmount(rs.getDouble("amount"));
//						m.getInvetsments().setPutCount(rs.getLong("count"));
//					}
//				} catch (Exception e) {
//					log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
//				}
//
//				// set market for this run (skin-currency)
//				sc.setMarktes(marktes);
//
//				log.log(Level.DEBUG,"finished all queries for skinid: " + skin.getId() +  " currency: " + sc.getCode() +
//						" " + dr.getDateRange());
//
//				try {
//					rs.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//				try {
//					pstmt.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//				try {
//					conn.close();
//				} catch (Exception e) {
//					log.log(Level.ERROR,"Can't close",e);
//				}
//
//			} //  end currencies loop
//
//		}  // end skins loop
//
//		sendEmailDateRangeMarketBreakDownSkinsPartition(dr, skins, runType);
//
//	}
//
//	private static void calculateAndSendDailyReportMarkteBreakDown(int runType) {
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		// investments - first the total of investements # of investments and
//		// headcount
//
//		try {
//
//			conn = Utils.getConnection();
//
//			String sqlTotalTurnover =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u, investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg  "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_canceled = 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME ";
//
//			pstmt = conn.prepareStatement(sqlTotalTurnover);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Investments inv = new Investments();
//				inv.setTurnoverAmount(rs.getDouble("amount"));
//				inv.setTurnoverCount(rs.getLong("count"));
//				inv.setHeadCount(rs.getLong("headcount"));
//				inv.setCurrency(getCurrencyByRunType(runType));
//				Market market = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//				marktes.put(rs.getLong("market_id"),market);
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Turnover - market breakdown" + e);
//		}
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntraday =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage  ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType)	+ " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntraday);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setIntradayTurnoverAmount(rs.getDouble("amount"));
//				m.getInvetsments().setIntradayTurnoverCount(rs.getLong("count"));
//				m.getInvetsments().setIntradayprofitAmount(rs.getDouble("profit"));
//				m.getInvetsments().setIntradayprofitPrecentage(rs.getDouble("precentage"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday market breakdown " + e);
//		}
//
//		try {
//			String sqlIntradayWinnings =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.win > 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType)	+ " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntradayWinnings);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setWinTurnover(rs.getDouble("amount"));
//				m.getInvetsments().setWinCount(rs.getLong("count"));
//				m.getInvetsments().setWinAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Winnings market breakdown " + e);
//		}
//
//
//
//		try {
//			String sqlIntradayLose =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit ,m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled = 1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.lose > 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlIntradayLose);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setLoseTurnover(rs.getDouble("amount"));
//				m.getInvetsments().setLoseCount(rs.getLong("count"));
//				m.getInvetsments().setLoseAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Lose market breakdown " + e);
//		}
//
//		try {
//			String sqlCalls =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.TYPE_ID = 1 " // calls
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlCalls);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setLoseTurnover(0);
//					inv.setLoseCount(0);
//					inv.setLoseAmount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setCallAmount(rs.getDouble("amount"));
//				m.getInvetsments().setCallCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Calls breakdown market breakdown " + e);
//		}
//
//		try {
//			String sqlPuts =
//							"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, m.id as market_id , m.NAME as market, g.NAME as mgroup "
//							+ "from users u,investments i, opportunities o, markets m, market_groups g, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and i.OPPORTUNITY_ID = o.ID "
//							+ "and o.MARKET_ID =  m.ID "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = g.ID "
//							+ "and m.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and i.is_settled =1 "
//							+ "and i.is_canceled = 0 "
//							+ "and i.TYPE_ID = 2 " //puts
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ getSqlByRunType(runType) + " group by m.id , m.NAME , g.NAME";
//
//			pstmt = conn.prepareStatement(sqlPuts);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setWinTurnover(0);
//					inv.setWinCount(0);
//					inv.setWinAmount(0);
//					inv.setLoseTurnover(0);
//					inv.setLoseCount(0);
//					inv.setLoseAmount(0);
//					inv.setCallAmount(0);
//					inv.setCallCount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//				m.getInvetsments().setPutAmount(rs.getDouble("amount"));
//				m.getInvetsments().setPutCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Puts breakdown market breakdown " + e);
//		}
//		// gets open positions for weekly
//
//		try {
//			String sqlOpenPositionsWeek =
//							"select	" + getSqlAmountByRunType(runType) + " as amount,count (distinct A.id) as count, c.id as market_id , c.NAME as market, D.NAME as mgroup "
//							+ "from users u, investments A, opportunities B, markets C , market_groups D, skin_market_group_markets smgm, skin_market_groups smg "
//							+ "where u.ID = A.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and A.opportunity_id = B.id  "
//							+ "and  B.market_id = C.id  "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = D.ID "
//							+ "and C.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and B.scheduled = 3 "
//							+ // weekly
//							"and A.is_settled = 0 "
//							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp "
//							+ getSqlByRunType(runType) + " group by c.id , c.NAME , D.NAME";
//
//			pstmt = conn.prepareStatement(sqlOpenPositionsWeek);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//
//				m.getInvetsments().setOpenPositionsEndWeekAmount(rs.getDouble("amount"));
//				m.getInvetsments().setOpenPositionsEndWeekCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get OpenPositionsWeek market breakdown " + e);
//		}
//
//		// gets open positions for monthly
//
//		try {
//			String sqlOpenPositionsMonth =
//							"select	" + getSqlAmountByRunType(runType) + " as amount,count (distinct A.id) as count, c.id as market_id , c.NAME as market, D.NAME as mgroup "
//							+ "from users u, investments A, opportunities B, markets C , market_groups D, skin_market_group_markets smgm, skin_market_groups smg  "
//							+ "where u.ID = A.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and A.opportunity_id = B.id  "
//							+ "and  B.market_id = C.id  "
//							+ "and smgm.skin_market_group_id = smg.id "
//							+ "and smg.market_group_id = D.ID "
//							+ "and C.id = smgm.market_id "
//							+ "and smg.skin_id = u.skin_id "
//							+ "and B.scheduled = 4 "
//							+ // monthly
//							"and A.is_settled = 0 "
//							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp "
//							+ getSqlByRunType(runType) + " group by c.id , c.NAME , D.NAME";
//
//			pstmt = conn.prepareStatement(sqlOpenPositionsMonth);
//			rs = pstmt.executeQuery();
//
//			while(rs.next()) {
//				Market m = marktes.get(rs.getLong("market_id"));
//				if (m == null) {
//					Investments inv = new Investments();
//					inv.setTurnoverAmount(0);
//					inv.setTurnoverCount(0);
//					inv.setHeadCount(0);
//					inv.setIntradayTurnoverAmount(0);
//					inv.setIntradayTurnoverCount(0);
//					inv.setIntradayprofitAmount(0);
//					inv.setIntradayprofitPrecentage(0);
//					inv.setOpenPositionsEndWeekAmount(0);
//					inv.setOpenPositionsEndWeekCount(0);
//					inv.setCurrency(getCurrencyByRunType(runType));
//					m = new Market(rs.getLong("market_id"),rs.getString("market"),rs.getString("mgroup"),inv);
//					marktes.put(rs.getLong("market_id"),m);
//				}
//
//				m.getInvetsments().setOpenPositionsEndMonthAmount(rs.getDouble("amount"));
//				m.getInvetsments().setOpenPositionsEndMonthCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get OpenPositionsMonth market breakdown " + e);
//		}
//
//		sendEmailMarketBreakDown(getPageHeaderByRunType(runType), runType);
//	}
//
//	private static void calculateAndSendDailyReport(int runType) throws Exception {
//
//		Cash cash = new Cash();
//		cash.setCurrency(getCurrencyByRunType(runType));
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		// / Deposits by Credit cards
//
//		try {
//			conn = Utils.getConnection();
//
//			String sqlDepositCC =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t,users u "
//							+ "where t.USER_ID = u.ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and t.type_id = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositCC += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositCC);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositCCAmount(rs.getDouble("amount"));
//				cash.setDepositCCCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits CC ");
//		}
//
//		// Deposits by Admin
//
//		try {
//			String sqlDepositAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 4 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositAdmin);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositAdminAmount(rs.getDouble("amount"));
//				cash.setDepositAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits admin " + e);
//		}
//
//		//	Deposits by other then Credit cards no Admin
//
//		try {
//			String sqlDepositOther =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id <> 1 "
//							+ "and t.type_id <> 6 "
//							+ "and t.type_id <> 4 "
//							+ "and t.type_id <> 12 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositOther += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositOther);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositOtherAmount(rs.getDouble("amount"));
//				cash.setDepositOtherCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits other " + e);
//		}
//
// //		Deposits Bonus :
//
//		try {
//			String sqlDepositBonus =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 12 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositBonus);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setDepositBonusAmount(rs.getDouble("amount"));
//				cash.setDepositBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get deposits bonus " + e);
//		}
//
//
//		// Withdraw Bonus
//		try {
//			String sqlWithdrawBonus =
//					"select " + getSqlAmountByRunType(runType) + "  as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 13 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlWithdrawBonus += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawBonus);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawBonusAmount(rs.getDouble("amount"));
//				cash.setWithdrawBonusCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get withdraw bonus " + e);
//		}
//
//
//
//		// Withdraw by Admin
//		try {
//			String sqlWithdrawAdmin =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.type_id = 5 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlWithdrawAdmin += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlWithdrawAdmin);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setWithdrawAdminAmount(rs.getDouble("amount"));
//				cash.setWithdrawAdminCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get withdraw admin " + e);
//		}
//
//		//	Reverse Withdraws
//		try {
//			String sqlReverseWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1 "
//							+ "and t.type_id = 6 "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlReverseWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlReverseWithdraws);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				cash.setReverseWithdrawsAmount(rs.getDouble("amount"));
//				cash.setReverseWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Reverse Withdraws " + e);
//		}
//
//
//		// Executed Withdraws - withdraw that processed yesterday
//
//		try {
//			String sqlExecutedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and tt.id <> 5 " // TODO: remove also bonuses
//							+ "and t.status_id = 2 "
//							+ "and to_char(t.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlExecutedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlExecutedWithdraws);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setExecutedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setExecutedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Executed Withdraws " + e);
//		}
//
//		// Requested Withdraws - withdraw that Requested yesterday
//
//		try {
//			String sqlRequestedWithdraws =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(t.id) count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 2 "
//							+ "and t.status_id in(4,9,6) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlRequestedWithdraws += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlRequestedWithdraws);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setRequestedWithdrawsAmount(rs.getDouble("amount"));
//				cash.setRequestedWithdrawsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Requested Withdraws " + e);
//		}
//
//		// Net Cash = total deposits - executed withdraws
//
//		cash.setNetInOut();
//
//		// deposits headcount - how many users did any success deposit
//
//		try {
//			String sqlDepositHeadCount =
//					"select count(distinct user_id) as count "
//							+ "from transactions t, transaction_types tt, users u "
//							+ "where t.type_id = tt.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and tt.class_type = 1  "
//							+ "and t.status_id in (2,7) "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlDepositHeadCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlDepositHeadCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				cash.setHeadCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Deposit Head Count " + e);
//		}
//
//		Investments invests = new Investments();
//		invests.setCurrency(getCurrencyByRunType(runType));
//
//		// investments - first the total of investements # of investments and
//		// headcount
//
//		try {
//			String sqlTotalTurnover =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count, count(distinct i.user_id) as headcount "
//							+ "from investments i, users u "
//							+ "where i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlTotalTurnover += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlTotalTurnover);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setTurnoverAmount(rs.getDouble("amount"));
//				invests.setTurnoverCount(rs.getLong("count"));
//				invests.setHeadCount(rs.getLong("headcount"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Turnover " + e);
//		}
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntraday =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,sum(i.house_result/100) as profit,sum(i.house_result/100)/sum(amount/100)*100 as precentage "
//							+ "from investments i, users u "
//							+ "where i.is_settled = 1 "
//							+ "and i.is_canceled = 0 "
//							+ "and u.ID = i.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlIntraday += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntraday);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setIntradayTurnoverAmount(rs.getDouble("amount"));
//				invests.setIntradayTurnoverCount(rs.getLong("count"));
//				invests.setIntradayprofitAmount(rs.getDouble("profit"));
//				invests.setIntradayprofitPrecentage(rs.getDouble("precentage"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday " + e);
//		}
//
//
//		// investments - that settled yesterday.
//
//		try {
//			String sqlIntradayWinningsCount =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.WIN > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlIntradayWinningsCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntradayWinningsCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setWinTurnover(rs.getDouble("amount"));
//				invests.setWinCount(rs.getLong("count"));
//				invests.setWinAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Winnings Count " + e);
//		}
//
//
//		try {
//			String sqlIntradayLossingCount =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct i.id) as count,-sum(i.house_result/100) as profit "+
//					"from investments i, users u "+
//					"where i.is_settled = 1 "+
//					"and i.is_canceled = 0 "+
//					"and u.ID = i.USER_ID "+
//					"and u.CLASS_ID <> 0  "+
//					"and i.LOSE > 0 "+
//					"and to_char(i.TIME_SETTLED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlIntradayLossingCount += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlIntradayLossingCount);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setLoseTurnover(rs.getDouble("amount"));
//				invests.setLoseCount(rs.getLong("count"));
//				invests.setLoseAmount(rs.getDouble("profit"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get Intraday Lose Count " + e);
//		}
//
//
//		// gets open positions for weekly
//		try {
//			String sqlOpenPositionsWeek =
//					"select	" + getSqlAmountByRunType(runType) + " as amount,count (distinct A.id) as count "
//							+ "from users u, investments A, opportunities B, markets C "
//							+ "where u.ID = A.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and A.opportunity_id = B.id  "
//							+ "and  B.market_id = C.id  "
//							+ "and B.scheduled = 3 "
//							+ // weekly
//							"and A.is_settled = 0 "
//							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp";
//
//			sqlOpenPositionsWeek += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOpenPositionsWeek);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setOpenPositionsEndWeekAmount(rs.getDouble("amount"));
//				invests.setOpenPositionsEndWeekCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get OpenPositionsWeek " + e);
//		}
//
//		// gets open positions for monthly
//		try {
//			String sqlOpenPositionsMonth =
//					"select	" + getSqlAmountByRunType(runType) + " as amount,count (distinct A.id) as count "
//							+ "from users u, investments A, opportunities B, markets C "
//							+ "where u.ID = A.USER_ID "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and A.opportunity_id = B.id  "
//							+ "and  B.market_id = C.id  "
//							+ "and B.scheduled = 4 "
//							+ // monthly
//							"and A.is_settled = 0 "
//							+ "and B.time_first_invest <= current_timestamp AND B.time_est_closing >= current_timestamp";
//
//			sqlOpenPositionsMonth += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOpenPositionsMonth);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				invests.setOpenPositionsEndMonthAmount(rs.getDouble("amount"));
//				invests.setOpenPositionsEndMonthCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get OpenPositionsMonth " + e);
//		}
//
//		Other other = new Other();
//		other.setCurrency(getCurrencyByRunType(runType));
//		// gets other stuff - new accounts
//		try {
//			String sqlOtherNewAccounts = "select count(distinct id) as count " +
//										 "from users u " +
//										 "where to_char(TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD')";
//
//			sqlOtherNewAccounts += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccounts);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewAccounts(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts " + e);
//		}
//
//		// gets other stuff - new depositor
//		try {
//			String sqlOtherNewDepositors =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count "
//							+ "from users u , transactions t , transaction_types tt "
//							+ "where t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and t.status_id in (2,7) "
//							+ "and tt.id not in (6,12) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD')) ";
//
//			sqlOtherNewDepositors += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositors);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//				other.setNewDepositingAccountsCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors " + e);
//		}
//		// get chargbacks that created yesterday
//		try {
//			String sqlChargebacks =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct t.id) as count "
//							+ "from users u ,transactions t , charge_backs cb "
//							+ "where t.charge_back_id = cb.id "
//							+ "and u.ID = t.USER_ID "
//							+ "and to_char(cb.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') ";
//
//			sqlChargebacks += getSqlByRunType(runType);
//
//			pstmt = conn.prepareStatement(sqlChargebacks);
//			rs = pstmt.executeQuery();
//
//			if (rs.next()) {
//				other.setChargbacksAmount(rs.getDouble("amount"));
//				other.setChargbacksCount(rs.getLong("count"));
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get chargebacks " + e);
//		}
//
//		ArrayList<Ad> adList = new ArrayList<Ad>();
//		/// gets new accounts by ad
//		try {
//			String sqlOtherNewAccountsByAds = "select count(distinct u.id) as count, ads.id as id, ads.name " +
//											  "from users u, ads " +
//										      "where u.ad_id = ads.id and to_char(TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//											  + getSqlByRunType(runType) + " group by ads.id, ads.name";
//
//			pstmt = conn.prepareStatement(sqlOtherNewAccountsByAds);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				Other otherTmp = new Other();
//				otherTmp.setNewAccounts(rs.getLong("count"));
//				otherTmp.setCurrency(getCurrencyByRunType(runType));
//				Ad ad = new Ad();
//				ad.setAdId(rs.getLong("id"));
//				ad.setAdName(rs.getString("name"));
//				ad.setOther(otherTmp);
//				adList.add(ad);
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new accounts by ads" + e);
//		}
//
//		// gets other stuff - new depositor by ads
//		try {
//			String sqlOtherNewDepositorsByAds =
//					"select " + getSqlAmountByRunType(runType) + " as amount, count(distinct u.id) as count, ads.id , ads.name "
//							+ "from users u , transactions t , transaction_types tt, ads "
//							+ "where ads.id = u.ad_id "
//							+ "and t.user_id = u.id "
//							+ "and t.type_id = tt.id "
//							+ "and tt.class_type = 1 "
//							+ "and tt.id not in (6,12) "
//							+ "and t.status_id in (2,7) "
//							+ "and u.CLASS_ID <> 0 "
//							+ "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate -1,'YYYYMMDD') "
//							+ "and not exists (select distinct t2.user_id "
//							+ "from transactions t2, transaction_types tt2 "
//							+ "where t2.type_id = tt2.id "
//							+ "and t2.status_id in (2,7) "
//							+ "and tt2.class_type = 1 "
//							+ "and tt2.id not in (6,12) "
//							+ "and t2.user_id=t.user_id "
//							+ "and to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD')) "
//							+ getSqlByRunType(runType) + " group by  ads.id , ads.name";
//
//
//			pstmt = conn.prepareStatement(sqlOtherNewDepositorsByAds);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				long adid = rs.getLong("id");
//				boolean found = false;
//				for(int i=0;i < adList.size() && !found ;i++){
//					Ad ad = adList.get(i);
//					if (ad.getAdId() == adid){
//						Other otherTmp = ad.getOther();
//						otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//						otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//						found = true;
//					} else {
//						if (i == adList.size() -1){
//							Other otherTmp = new Other();
//							otherTmp.setNewAccounts(0);
//							otherTmp.setNewDepositingAccountsAmount(rs.getDouble("amount"));
//							otherTmp.setNewDepositingAccountsCount(rs.getLong("count"));
//							otherTmp.setCurrency(getCurrencyByRunType(runType));
//							Ad ad_new = new Ad();
//							ad_new.setAdId(rs.getLong("id"));
//							ad_new.setAdName(rs.getString("name"));
//							ad_new.setOther(otherTmp);
//							adList.add(ad_new);
//							break;
//						}
//					}
//
//				}
//
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get new depositors by ads" + e);
//		}
//
//		log.log(Level.DEBUG,"finished all queries ");
//		// Now lets send emails with the report :
//
//		log.log(Level.DEBUG,"going to send emails ");
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = getEmailSubjectByRunType(runType) + " daily report :" + sdf.format(cal.getTime());
//
//
//		sendEmail(cash,invests,other,adList,getPageHeaderByRunType(runType),"report.html",subject);
//
//		log.log(Level.DEBUG,"finished to send emails");
//
//		try {
//			rs.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			pstmt.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			conn.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//	}
//
//	private static void sendEmail(Cash cash, Investments invests, Other other,ArrayList<Ad> adList,String header,
//			String file, String subject) {
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//		// put all cash params
//		params.put("cash",cash);
//
//		params.put("invest",invests);
//
//		params.put("other",other);
//
//		params.put("ads",adList);
//
//		params.put("header",header);
//
//		try {
//			initEmail(subject,file);
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void sendEmailMarketBreakDown(String header, int runType) {
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//		// put all investments params
//		log.info("number of markets " + marktes.size());
//
//		ArrayList<Market> marketsList = new ArrayList<Market>(marktes.values());
//
//		Collections.sort(marketsList,new GroupComparator());
//
//		params.put("markets",new ArrayList<Market>(marketsList));
//		params.put("header", header);
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = getEmailSubjectByRunType(runType) + " daily report market breakdown:" + sdf.format(cal.getTime());
//
//		try {
//			initEmail(subject,"report_market.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails.market.breakdown").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//
//	private static void sendEmailDateRangeMarketBreakDown(DateRange dr, String header, int runType) {
//		HashMap<String, Object> params = new HashMap<String, Object>();
//		// put all investments params
//		log.info("number of markets " + marktes.size());
//
//		ArrayList<Market> marketsList = new ArrayList<Market>(marktes.values());
//
//		Collections.sort(marketsList,new GroupComparator());
//
//		params.put("markets",new ArrayList<Market>(marketsList));
//		params.put("header",header);
//
//		String subject = getEmailSubjectByRunType(runType) + " report market breakdown:" + dr.getDateRange();
//
//		try {
//			initEmail(subject,"report_market_date_range.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void sendEmailDateRangeMarketBreakDownSkinsPartition(DateRange dr, ArrayList<Skins> skins, int runType) {
//		HashMap<String, ArrayList<Skins>> params = new HashMap<String, ArrayList<Skins>>();
//
//		// put all investments params per skin and currency
//
//		for( Skins skin : skins ) {
//			for ( SkinCurrency sc : skin.getSkinCurrenciesList() ) {
//				log.info("skinid: " + skin.getId() + " currency: " + sc.getCode() + " number of markets " + sc.getMarktes().size());
//				ArrayList<Market> marketsList = new ArrayList<Market>(sc.getMarktes().values());
//				Collections.sort(marketsList,new GroupComparator());
//				sc.setMarketsList(new ArrayList<Market>(marketsList));   // save markets list
//			}
//		}
//
//		params.put("skins",skins);
//
//		String subject = getEmailSubjectByRunType(runType) + " report market breakdown - skins partition:" + dr.getDateRange();
//
//		try {
//			initEmail(subject,"report_market_date_range_skinsPartition.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void sendEmailNeedFiles(ArrayList<NeedFiles> needFilesArr, int deposits, int runType) {
//
//		// put all investments params
//		log.info("number of deposits that need files " + needFilesArr.size());
//
//		HashMap<String, ArrayList> params = new HashMap<String, ArrayList>();
//
//		params.put("needFilesArr",needFilesArr);
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = getEmailSubjectByRunType(runType) + " daily report depositors above " + deposits + " " + sdf.format(cal.getTime());
//
//		try {
//			initEmail(subject,"report_need_files.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails.need.files").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void sendEmailNonDepositors(ArrayList<NeedFiles> needFilesArr, Other other, String header, int runType) {
//
//		// put all investments params
//		log.info("number of non depositors  " + needFilesArr.size());
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//
//		params.put("needFilesArr",needFilesArr);
//		params.put("other",other);
//		params.put("header",header);
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = getEmailSubjectByRunType(runType) + " daily non depositors " + sdf.format(cal.getTime());
//
//		try {
//			initEmail(subject,"report_non_depositors.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails.non.depositors").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void sendEmailNonDepositors20Days(ArrayList<NeedFiles> needFilesArr, Other other, String header, int runType) {
//
//		// put all investments params
//		log.info("number of non depositors 20 days" + needFilesArr.size());
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//
//		params.put("needFilesArr",needFilesArr);
//		params.put("other",other);
//		params.put("header",header);
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = getEmailSubjectByRunType(runType) + " daily no 2nd deposit 20 days " + sdf.format(cal.getTime());
//
//		try {
//			initEmail(subject,"report_non_depositors.html");
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails.non.depositors.20.days").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//
//	private static void sendSkinsPartitionEmail(ArrayList<Skins> skins, String file, String subject) {
//
//		HashMap<String, ArrayList<Skins>> params = new HashMap<String, ArrayList<Skins>>();
//
//		// put skins list params
//		params.put("skins",skins);
//
//		try {
//			initEmail(subject,file);
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body",getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails = Utils.getProperty("emails").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//
//	private static void initEmail(String subject, String name) throws Exception {
//
//		// get the emailTemplate
//		emailTemplate = getEmailTemplate(name);
//
//		// set the server properties
//		serverProperties = new Hashtable<String, String>();
//		serverProperties.put("url",Utils.getProperty("email.server"));
//		serverProperties.put("auth","true");
//		serverProperties.put("user",Utils.getProperty("email.uname"));
//		serverProperties.put("pass",Utils.getProperty("email.pass"));
//
//		// Set the email properties
//		emailProperties = new Hashtable<String, String>();
//		emailProperties.put("subject",subject);
//		emailProperties.put("from",Utils.getProperty("email.from"));
//
//	}
//
//	private static Template getEmailTemplate(String fileName) throws Exception {
//		try {
//			// String templatePath = CommonUtil.getProperty("templates.path");
//			Properties props = new Properties();
//			props.setProperty("file.resource.loader.path",Utils.getProperty("email.template.path"));
//			props.setProperty("resource.loader","file");
//			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
//			props.setProperty("file.resource.loader.cache","true");
//			Velocity.init(props);
//			return Velocity.getTemplate(fileName,"iso-8859-1");
//
//		} catch (Exception ex) {
//			log.fatal("!!! ERROR >> Cannot find Report template : " + ex.getMessage());
//			throw ex;
//		}
//	}
//
//	public static String getEmailBody(HashMap params) throws Exception {
//
//		// set the context and the parameters
//		VelocityContext context = new VelocityContext();
//
//		// get all the params and add them to the context
//		String paramName = "";
//		log.debug("Adding parrams");
//
//		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
//			paramName = (String) keys.next();
//			context.put(paramName,params.get(paramName));
//		}
//		StringWriter sw = new StringWriter();
//		emailTemplate.merge(context,sw);
//
//		return sw.toString();
//	}
//
//
//	/**
//	 * Get sql conditions for the run
//	 * @param runType
//	 * 		type of the run we needed
//	 * @return
//	 * 		SQL String
//	 */
//	private static String getSqlByRunType(int runType) {
//		String sql = "";
//		switch (runType) {
//		case RUN_TYPE_SKIN_ETRADER:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_ETRADER + " ";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_USD:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_ENGLISH_USD + " ";
//			break;
//		case RUN_TYPE_SKIN_TURKISH_TRY:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_TURKISH_TRY + " ";
//			break;
//		case RUN_TYPE_SKIN_RUSSIAN_RUB:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_RUSSIAN_RUB + " ";
//			break;
//		case RUN_TYPE_SKIN_SPANISH_EUR:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_SPANISH_EUR + " ";
//			break;
//		case RUN_TYPE_SKIN_ITALIAN_EUR:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_ITALIAN_EUR + " ";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_EUR:
//			sql+= " and u.skin_id = " + RUN_TYPE_SKIN_ENGLISH_EUR + " ";
//			break;
//		}
//		return sql;
//	}
//
//	/**
//	 * Get sql amount condition for the run
//	 * @param runType
//	 * 		type of the run we needed
//	 * @return
//	 */
//	private static String getSqlAmountByRunType(int runType) {
//		if ( runType == RUN_TYPE_SKIN_ETRADER ) {
//			return " sum(amount/100) ";
//		}
//		else {  // UDS convert
//			return " sum((amount/100) * rate) ";
//		}
//	}
//
//	/**
//	 * Get currency str by run type
//	 * @param runType
//	 * 		type of the run we needed
//	 * @return
//	 */
//	private static String getCurrencyByRunType(int runType) {
//		String currency = "";
//		switch (runType) {
//		case RUN_TYPE_SKIN_ETRADER:
//			currency = Utils.CURRENCY_ILS;
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_USD:
//			currency = Utils.CURRENCY_USD;
//			break;
//		case RUN_TYPE_SKIN_TURKISH_TRY:
//			currency = Utils.CURRENCY_TRY;
//			break;
//		case RUN_TYPE_SKIN_RUSSIAN_RUB:
//			currency = Utils.CURRENCY_RUB;
//			break;
//		case RUN_TYPE_SKIN_SPANISH_EUR:
//			currency = Utils.CURRENCY_EUR;
//			break;
//		case RUN_TYPE_SKIN_ITALIAN_EUR:
//			currency = Utils.CURRENCY_EUR;
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_EUR:
//			currency = Utils.CURRENCY_EUR;
//			break;
//		}
//		return currency;
//	}
//
//	/**
//	 * Get Header for the page by run type
//	 * @param runType
//	 * 		type of the run we needed
//	 * @return
//	 */
//	private static String getPageHeaderByRunType(int runType) {
//		String header = "";
//		switch (runType) {
//		case RUN_TYPE_SKIN_ETRADER:
//			header+= "Etrader Skin Report";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_USD:
//			header+= "English USD Skin Report";
//			break;
//		case RUN_TYPE_SKIN_TURKISH_TRY:
//			header+= "Turkish TRY Skin Report";
//			break;
//		case RUN_TYPE_SKIN_RUSSIAN_RUB:
//			header+= "Russian RUB Skin Report";
//			break;
//		case RUN_TYPE_SKIN_SPANISH_EUR:
//			header+= "Spanih EUR Skin Report";
//			break;
//		case RUN_TYPE_SKIN_ITALIAN_EUR:
//			header+= "Italian EUR Skin Report";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_EUR:
//			header+= "English EUR Skin Report";
//			break;
//		}
//		return header;
//	}
//
//	/**
//	 * Get Header for the page by run type
//	 * @param runType
//	 * 		type of the run we needed
//	 * @return
//	 */
//	private static String getEmailSubjectByRunType(int runType) {
//		String header = "";
//		switch (runType) {
//		case RUN_TYPE_SKIN_ETRADER:
//			header+= "etrader";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_USD:
//			header+= "English USD";
//			break;
//		case RUN_TYPE_SKIN_TURKISH_TRY:
//			header+= "Turkish TRY";
//			break;
//		case RUN_TYPE_SKIN_RUSSIAN_RUB:
//			header+= "Russian RUB";
//			break;
//		case RUN_TYPE_SKIN_SPANISH_EUR:
//			header+= "Spanih EUR";
//			break;
//		case RUN_TYPE_SKIN_ITALIAN_EUR:
//			header+= "Italian EUR";
//			break;
//		case RUN_TYPE_SKIN_ENGLISH_EUR:
//			header+= "English EUR";
//			break;
//		}
//		return header;
//	}
//
//	/**
//	 * Get sql for skin and currency partition
//	 * @param skinId
//	 * 		user skin id
//	 * @param currencyId
//	 * 		user currency id
//	 * @return
//	 */
//	public static String getSqlBySkinPartintion(long skinId, long currencyId) {
//		return " and u.skin_id = " + skinId + " and u.currency_id = " + currencyId + " ";
//	}
//
//
//	/**
//	 * Return skins list,
//	 * For each skin, get currencies list
//	 * @return
//	 * 		ArrayList of Skins instances
//	 */
//	private static ArrayList<Skins> getAllSkins() {
//
//		ArrayList<Skins> skins = new ArrayList<Skins>();
//
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		log.debug("Starting to get all skins with currencies list");
//
//		// Get all skins and skinCurrencies list
//		try {
//			conn = Utils.getConnection();
//			String sqlSkins = "select * from skins ";
//
//			pstmt = conn.prepareStatement(sqlSkins);
//			rs = pstmt.executeQuery();
//
//			while ( rs.next() ) {
//				Skins s = new Skins();
//				s.setId(rs.getInt("id"));
//				s.setName(rs.getString("name"));
//				s.setDisplayName(rs.getString("display_name"));
//				skins.add(s);
//			}
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get skins list");
//		}
//
//		// set skin currencies for each skin
//		for( Skins skin : skins ) {
//
//			ArrayList<SkinCurrency> skinCurrenciesList = new ArrayList<SkinCurrency>();
//			try {
//				String sqlCurrencies = "select s.currency_id, c.code " +
//				 					   "from skin_currencies s, currencies c " +
//				 					   "where s.currency_id = c.id and s.skin_id = ? ";
//
//				pstmt = conn.prepareStatement(sqlCurrencies);
//				pstmt.setLong(1, skin.getId());
//				rs = pstmt.executeQuery();
//
//				while ( rs.next() ) {
//					SkinCurrency sc = new SkinCurrency();
//					sc.setCurrencyId(rs.getLong("currency_id"));
//					sc.setCode(rs.getString("code"));
//					skinCurrenciesList.add(sc);
//				}
//				skin.setSkinCurrenciesList(skinCurrenciesList);
//
//			} catch (Exception e) {
//				log.log(Level.FATAL,"Can't get currencies list for skin : " + skin.getId() );
//			}
//		}
//
//		log.debug("Get all skins with currencies list finished!");
//
//		try {
//			rs.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			pstmt.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		try {
//			conn.close();
//		} catch (Exception e) {
//			log.log(Level.ERROR,"Can't close",e);
//		}
//
//		return skins;
//
//	}

//}
