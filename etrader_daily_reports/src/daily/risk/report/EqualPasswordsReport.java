//package daily.risk.report;
//
//import il.co.etrader.bl_managers.IssuesManagerBase;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import java.io.StringWriter;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Iterator;
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//import org.apache.velocity.Template;
//import org.apache.velocity.VelocityContext;
//import com.anyoption.common.util.Encryptor;
//
//import daily.common.Constants;
//import daily.common.SecurityCheck;
//import daily.common.Utils;
//
//public class EqualPasswordsReport {
//	private static Logger log = Logger.getLogger(EqualPasswordsReport.class);
//	private static Template emailTemplate;
//	String path = "";
//	// The server properties
//	private static Hashtable<String, String> serverProperties;
//	// The specific email Properties
//	private static Hashtable<String, String> emailProperties;
//	public static final String PARAM_EMAIL = "email";
//
//	public static void main(String[] args) throws Exception {
//		if (null == args || args.length < 1) {
//			log.log(Level.FATAL,"Configuration file not specified, insert properties file name.");
//			return;
//		}
//
//		Utils.propFile = args[0];
//
//		int runType = Constants.RUN_TYPE_SKIN_ETRADER;
//
//		// take run type property
//		try {
//			runType = Integer.parseInt(args[1]);
//		}  catch ( Exception e ) {
//			log.warn("wrong property of run type, must be number (0-3). take etrader run type");
//		}
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Starting : equal passwords report.");
//		}
//
//		calcAndSendEqualPasswordsReport(runType);
//
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"Finished : equal passwords report.");
//		}
//	}
//	
//	/**
//	 * Send equal passwords report (70% matching)
//	 * This is done with levenshtein distance algorithm
//	 */
//	private static void calcAndSendEqualPasswordsReport(int runType) {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<SecurityCheck> newDepositors = new ArrayList<SecurityCheck>();
//		ArrayList<SecurityCheck> newUsersNonDepositors = new ArrayList<SecurityCheck>();
//		ArrayList<SecurityCheck> blackListUsers = new ArrayList<SecurityCheck>();
//		
//		HashMap<Long, String> skins = Utils.getAllSkins();   // get all skins list
//
//		try {
//			conn = Utils.getConnection();
//
//			// new depositors / try to deposit users
//			String sql =
//					"SELECT " +
//		            	"u.user_name , " +
//		            	"u.id , " +
//		            	"u.skin_id , " +
//		            	"u.password, " +
//		            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
//		            "FROM " +
//		            	"users u " +
//		            		"LEFT JOIN (SELECT " +
//		            						"t.user_id " +
//		            					"FROM " +
//		            						"transactions t, transaction_types tt " +
//		            					"WHERE " +
//		            						"t.type_id = tt.id " +
//		            						"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
//		            						"AND tt.class_type =  1 " +
//		            					"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id " +
//		            "WHERE " +
//			          "u.class_id <> 0  AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? " +
//			          Utils.getSqlByRunType(runType) + " AND " +
//			          "EXISTS (select 1 " +
//	                           "from  transactions t , transaction_types tt " +
//	                           "where t.user_id = u.id and " +
//	                                 "t.type_id = tt.id and " +
//	                                 "tt.class_type = 1 and " +
//	                                 "to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') ) AND " +
//	                  "NOT EXISTS (select distinct t2.user_id " +
//		                         "from transactions t2, transaction_types tt2 " +
//	                             "where t2.type_id = tt2.id and " +
//	                                   "tt2.class_type = 1 and " +
//	                                   "t2.user_id = u.id and " +
//	                                   "to_char(t2.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') ) " +
//		        "ORDER BY user_name ";
//
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
//			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
//			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
//			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
//			rs = pstmt.executeQuery();
//			log.log(Level.DEBUG,"Finish run first Query");
//
//			while(rs.next()) {
//				SecurityCheck user = new SecurityCheck();
//				user.setUsername(rs.getString("user_name"));
//				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
//				long skinId = rs.getLong("skin_id");
//				String skinDisplayName = skins.get(skinId);
//				user.setSkin(Utils.getProperty(skinDisplayName));
//				user.setUserId(rs.getInt("id"));
//				user.setSucceedDeposit(rs.getString("suc_deposit"));
//				user.setUserListType(SecurityCheck.USER_LIST_TYPE_NEW_DEPOSITERS);
//				newDepositors.add(user);
//			}
//			log.log(Level.DEBUG,"Saved all new depositors in list - list size is: " + newDepositors.size());
//			
//			// new users with no deposits
//			String sql1 =
//					"SELECT " +
//		            	"u.user_name , " +
//		            	"u.id , " +
//		            	"u.skin_id , " +
//		            	"u.password " +
//		            "FROM " +
//		            	"users u " +
//		            "WHERE " +
//			          "u.class_id <> 0  AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? AND " +
//			          "u.password not like ? " +
//			          Utils.getSqlByRunType(runType) + " AND " +
//			          "to_char(u.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') AND "+
//			          "NOT EXISTS (select 1 " +
//		                           "from  transactions t , transaction_types tt " +
//		                           "where t.user_id = u.id and " +
//		                                 "t.type_id = tt.id and " +
//		                                 "tt.class_type = 1 ) " +
//		        "ORDER BY user_name ";
//
//			pstmt = conn.prepareStatement(sql1);
//			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
//			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
//			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
//			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
//			rs = pstmt.executeQuery();
//			log.log(Level.DEBUG,"Finish run second Query");
//			while(rs.next()) {
//				SecurityCheck user = new SecurityCheck();
//				user.setUsername(rs.getString("user_name"));
//				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
//				long skinId = rs.getLong("skin_id");
//				String skinDisplayName = skins.get(skinId);
//				user.setSkin(Utils.getProperty(skinDisplayName));
//				user.setUserId(rs.getInt("id"));
//				user.setSucceedDeposit("NO");
//				user.setUserListType(SecurityCheck.USER_LIST_TYPE_NEW_USERS);
//				newUsersNonDepositors.add(user);
//			}
//			log.log(Level.DEBUG,"Saved all new users with no deposits in list - list size is: " + newUsersNonDepositors.size());
//
//			//	black list depositors / try to deposit users
//			String sql2 =
//				"SELECT " +
//	            	"u.user_name , " +
//	            	"u.id , " +
//	            	"u.skin_id , " +
//	            	"u.password, " +
//	            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
//	            "FROM " +
//	            	"users u " +
//	            		" left join CLOSE_ACCOUNT_USERS bl on u.id = bl.user_id " +
//		            	"LEFT JOIN (SELECT " +
//										"t.user_id " +
//									"FROM " +
//										"transactions t, transaction_types tt " +
//									"WHERE " +
//										"t.type_id = tt.id " +
//										"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
//										"AND tt.class_type =  1 " +
//									"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id " +
//	            "WHERE " +
//		          "u.class_id <> 0   " +
//		          " AND u.password not like ? AND " +
//		          "u.password not like ? AND " +
//		          "u.password not like ? AND " +
//		          "u.password not like ? " +
//		          Utils.getSqlByRunType(runType) +
//		          " AND ((u.is_active = 0 " +
//			     			" AND bl.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_CHB_CLOSE+ "" +
//			     											","+ IssuesManagerBase.ISSUE_ACTION_FRAUD + ")) " +
//			     		" OR " +
//			          	  " EXISTS (select 1 " +
//				          		   " from transactions t " +
//				          		   " where t.charge_back_id is not null and " +
//				          		   		  " t.charge_back_id <> 0 and " +
//				          		   		  " t.user_id = u.id) " +
//		        		   " ) " +
//	        "ORDER BY user_name ";
//
//			pstmt = conn.prepareStatement(sql2);
//			pstmt.setString(1, Encryptor.encryptStringToString("123456"));
//			pstmt.setString(2, Encryptor.encryptStringToString("1234567"));
//			pstmt.setString(3, Encryptor.encryptStringToString("12345678"));
//			pstmt.setString(4, Encryptor.encryptStringToString("123456789"));
//			rs = pstmt.executeQuery();
//			log.log(Level.DEBUG,"Finish run third Query");
//
//			while(rs.next()) {
//				SecurityCheck user = new SecurityCheck();
//				user.setUsername(rs.getString("user_name"));
//				user.setPassword(Encryptor.decryptStringToString(rs.getString("password")));
//				long skinId = rs.getLong("skin_id");
//				String skinDisplayName = skins.get(skinId);
//				user.setSkin(Utils.getProperty(skinDisplayName));
//				user.setUserId(rs.getInt("id"));
//				user.setSucceedDeposit(rs.getString("suc_deposit"));
//				user.setUserListType(SecurityCheck.USER_LIST_TYPE_BLACK_LIST);
//				blackListUsers.add(user);
//			}
//			log.log(Level.DEBUG,"Saved all blacklist users in list - list size is: " + blackListUsers.size());
//
//		} catch (Exception e) {
//			log.log(Level.FATAL,"Can't get equal passwords report " + e);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				pstmt.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//
//			try {
//				conn.close();
//			} catch (Exception e) {
//				log.log(Level.ERROR,"Can't close",e);
//			}
//		}
//
//		/**********Problematic section**********/
//		log.log(Level.DEBUG,"Start Problematic section 1/2 - Levenshtein algorithm");
//		ArrayList<SecurityCheck> newDepositorsCompareList = new ArrayList<SecurityCheck>();
//		newDepositorsCompareList.addAll(newDepositors);
//		newDepositorsCompareList.addAll(newUsersNonDepositors);
//		newDepositorsCompareList.addAll(blackListUsers);
//		// operate algorithm on new Depositors -> new Depositors + new Users + black List Users
//		ArrayList<SecurityCheck> equalsNewDepositors = getEqualPassList(newDepositors, newDepositorsCompareList);
//		log.log(Level.DEBUG,"Finished Problematic section 1/2 - Levenshtein algorithm");
//		
//		log.log(Level.DEBUG,"Start Problematic section 2/2 - Levenshtein algorithm");
//		ArrayList<SecurityCheck> newUsersCompareList = new ArrayList<SecurityCheck>();
//		newUsersCompareList.addAll(newUsersNonDepositors);
//		newUsersCompareList.addAll(blackListUsers);
//		// operate algorithm on new Users -> new Users + black List Users
//		ArrayList<SecurityCheck> equalsNewUsersNonDepositors = getEqualPassList(newUsersNonDepositors, newUsersCompareList);
//		log.log(Level.DEBUG,"Finished Problematic section 2/2 - Levenshtein algorithm");
//		/**********END Problematic section**********/
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"going to send emails ");
//		}
//		sendEmailEqualPasswords(equalsNewDepositors, equalsNewUsersNonDepositors, Utils.getSubjectByRunType(runType) + "passwords report (70% matching) ",
//				Utils.getPageHeaderByRunType(runType), runType);
//		if (log.isEnabledFor(Level.INFO)) {
//			log.log(Level.INFO,"finished to send emails");
//		}
//	}
//	
//	/**
//	 * Get 2 SecurityCheck lists and return a new list that contains users with password 70% matching.
//	 * @param users1  SecurityCheck list of users to compare with users2 list
//	 * @param users2  SecurityCheck list of users(list 2)
//	 * @return new SecurityCheck list
//	 */
//	public static ArrayList<SecurityCheck> getEqualPassList(ArrayList<SecurityCheck> users1, ArrayList<SecurityCheck> users2) {
//		log.log(Level.DEBUG,"About to compare 2 Lists: " +
//				users1.size() + " Users. " +
//				"Against " +
//				users2.size() + " Users. " +
//				"Total: " + users1.size() * users2.size() + " Comparisons");
//			
//		ArrayList<SecurityCheck> equalsUsers = new ArrayList<SecurityCheck>();
//		HashMap<Integer, Boolean> checkedList = new HashMap<Integer, Boolean>();
//		long countCompare = 1;
//		for ( SecurityCheck u1 : users1 ) {
//			log.log(Level.DEBUG,"Start to compare " + countCompare + "/" + users1.size() + " User, Against " + users2.size());
//			String userName1 = u1.getUsername();
//			long userId1 = u1.getUserId();
//			String pass1 = u1.getPassword();
//			String skin1 = u1.getSkin();
//			String succeedDeposit = u1.getSucceedDeposit();
//			
//			boolean firstUser = true;
//			for ( SecurityCheck u2 : users2 ) {
//				String userName2 = u2.getUsername();
//				String pass2 = u2.getPassword();
//				String skin2 = u2.getSkin();
//				long userId2 = u2.getUserId();
//				String userListType = u2.getUserListType();
//
//				if ( u1.getUserId() != u2.getUserId()  &&						   // no need to run on the same user and
//						checkedList.get((userName2 + userName1).hashCode()) == null  &&     // on some pair that already checked, like
//						checkedList.get((userName1 + userName2).hashCode()) == null ) {	   // (1-2) and (2-1)
//					
//					//log.log(Level.DEBUG, "Before levenshtein distance Algorithm");
//					//CommonUtil.getCheckHeapMemorySize();
//					int distance = levenshteinDistance(pass1, pass2);
//					//log.log(Level.DEBUG, "After levenshtein distance Algorithm");
//					//CommonUtil.getCheckHeapMemorySize();
//					
//					int maxLength = Math.max(pass1.length(), pass2.length());
//					Double prec = 0.7 * maxLength;
//					int eqCharacters = maxLength - distance;
//					Double matching = new Double(eqCharacters) / new Double(maxLength);
//					String matchingPrec = String.valueOf((matching * 100));
//					if ( distance == 0 ) {  // 100% matching
//						matchingPrec = matchingPrec.substring(0, 3) + "%";
//					} else {
//						matchingPrec = matchingPrec.substring(0, 2) + "%";
//					}
//
//					if ( eqCharacters >= prec ) {  // add to equals list
//						SecurityCheck s = new SecurityCheck();
//						s.setUserId(userId1);
//
//						// Group by user1Id and paddword1
//						if (firstUser){
//							// add new line separator
//							SecurityCheck newLine = new SecurityCheck();
//							newLine.setUserName1("---------------");
//							newLine.setPassword1("---------------");
//							newLine.setSkin1("---------------");
//							newLine.setUserName2("---------------");
//							newLine.setPassword2("---------------");
//							newLine.setSkin2("---------------");
//							newLine.setUserListType("---------------");
//							newLine.setSucceedDeposit("---------------");
//							equalsUsers.add(newLine);
//
//							s.setUserId(u1.getUserId());
//							s.setUserName1(userName1);
//							s.setPassword1(pass1);
//							s.setSkin1(skin1);
//							firstUser = false;
//						} else{
//							s.setUserName1("");
//							s.setPassword1("");
//							s.setSkin1("");
//						}
//						s.setUserId2(userId2);
//						s.setUserName2(userName2);
//						s.setPassword2(pass2);
//						s.setSkin2(skin2);
//						s.setDistance(distance);
//						s.setMatching(matchingPrec);
////						s.setEqualCharacters(eqCharacters);
////						DecimalFormat sd = new DecimalFormat("###.###");
////						s.setPrec(sd.format(prec));
//						s.setUserListType(userListType);
//						s.setSucceedDeposit(succeedDeposit);
//
//						equalsUsers.add(s);
//					}
//					// add to checkedList
//					checkedList.put((userName2 + userName1).hashCode(), true);
//					//System.out.println(userName2 + "-" + userName1);
//
//				}
//			}
//			log.log(Level.DEBUG,"Finish to compare " + countCompare++ + "/" + users1.size() + " User, Against " + users2.size());
//		}
//
//		return equalsUsers;
//	}
//	
//    /**
//     * Levenshtein distance algorithm(dynamic programming)
//     * The Levenshtein distance is a metric for measuring the amount of difference between two sequences (i.e., the so called edit distance).
//     * The Levenshtein distance between two strings is given by the minimum number of operations needed to transform one string into the other,
//     * where an operation is an insertion, deletion, or substitution of a single character.
//     * @param str1
//     * 		String 1 for compare
//     * @param str2
//     * 		String 2 for compare
//     * @return
//     * 		The minimum number of operations needed to transform one string into the other
//     */
//    public static int levenshteinDistance(String str1, String str2) {
//
//    	int leng1 = str1.length() + 1;
//    	int leng2 = str2.length() + 1;
//
//    	int[][] d = new int[leng1][leng2];
//
//    	for ( int i = 0 ; i < leng1 ; i++ ) {
//    		d[i][0] = i;
//    	}
//
//    	for ( int j = 0 ; j < leng2 ; j++ ) {
//    		d[0][j] = j;
//    	}
//
//    	int cost = 0;
//
//    	for ( int i = 1 ; i < leng1 ; i++ )
//    		for ( int j = 1 ; j < leng2 ; j++ ) {
//
//    			 if ( str1.charAt(i-1) == str2.charAt(j-1) ) {
//    				 cost = 0;
//    			 } else {
//    				 	cost = 1;
//    			 }
//
//                 // (d[i-1, j] + 1): deletion , (d[i, j-1] + 1): insertion, (d[i-1, j-1] + cost): substitution
//    			 d[i][j] = Math.min( Math.min(d[i-1][j] + 1, d[i][j-1] + 1) , d[i-1][j-1] + cost );
//   		}
//
//    	return d[leng1-1][leng2-1];
//    }
//    
//    /**
//     * Send passwords matching email
//     */
//	private static void sendEmailEqualPasswords(ArrayList<SecurityCheck> newDepositors, ArrayList<SecurityCheck> newUsers,
//													String subjectP, String header, int runType) {
//
//		// put all params
//		log.info("number of newDepositors  " + newDepositors.size());
//		log.info("number of newUsers  " + newUsers.size());
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//
//		params.put("newDepositors",newDepositors);
//		params.put("newUsers",newUsers);
//		params.put("header",header);
//		params.put("runType", runType);
//
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_YEAR,-1);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//		String subject = subjectP + sdf.format(cal.getTime());
//
//		try {
//			initEmail(subject,"report_passwords_matching.html", runType);
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
//		}
//
//		try {
//			emailProperties.put("body", getEmailBody(params));
//		} catch (Exception e) {
//			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//		}
//
//		String[] emails =  Utils.getEmailByRunType(runType, "emails.security.check").split(";");
//
//		for (int i = 0; i < emails.length; i++) {
//			emailProperties.put("to",emails[i]);
//			Utils.sendEmail(serverProperties,emailProperties);
//			emailProperties.remove("to");
//		}
//
//	}
//	
//	//TODO need to put it in Utils.
//	private static void initEmail(String subject, String name, int runType) throws Exception {
//
//		// get the emailTemplate
//		emailTemplate = Utils.getEmailTemplate(name);
//
//		// set the server properties
//		serverProperties = new Hashtable<String, String>();
//		serverProperties.put("url",Utils.getProperty("email.server"));
//		serverProperties.put("auth","true");
//		serverProperties.put("user",Utils.getProperty("email.uname"));
//		serverProperties.put("pass",Utils.getProperty("email.pass"));
//
//		// Set the email properties
//		emailProperties = new Hashtable<String, String>();
//		emailProperties.put("subject",subject);
//
//		if ( runType == Constants.RUN_TYPE_SKIN_ETRADER ) {
//			emailProperties.put("from",Utils.getProperty("email.from.et"));
//		} else {
//			emailProperties.put("from",Utils.getProperty("email.from.ao"));
//		}
//
//	}
//	
//	//TODO need to put it in Utils.
//	public static String getEmailBody(HashMap params) throws Exception {
//
//		// set the context and the parameters
//		VelocityContext context = new VelocityContext();
//
//		// get all the params and add them to the context
//		String paramName = "";
//		log.debug("Adding parrams");
//
//		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
//			paramName = (String) keys.next();
//			context.put(paramName,params.get(paramName));
//		}
//		StringWriter sw = new StringWriter();
//		emailTemplate.merge(context,sw);
//
//		return sw.toString();
//	}
//}
