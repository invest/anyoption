package daily.risk.report;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import com.anyoption.common.util.AESUtil;

import daily.common.CcDepositsStatus;
import daily.common.Constants;
import daily.common.SecurityCheck;
import daily.common.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class DailyRiskReportJob {

	private static Logger log = Logger.getLogger(DailyRiskReportJob.class);
	private static Template emailTemplate;
	String path = "";
	// The server properties
	private static Hashtable<String, String> serverProperties;
	// The specific email Properties
	private static Hashtable<String, String> emailProperties;
	public static final String PARAM_EMAIL = "email";

	public static void main(String[] args) throws Exception {

		if (null == args || args.length < 1) {
			log.log(Level.FATAL,"Configuration file not specified, insert properties file name.");
			return;
		}

		Utils.propFile = args[0];

		int runType = Constants.RUN_TYPE_SKIN_ETRADER;

		// take run type property
		try {
			runType = Integer.parseInt(args[1]);
		}  catch ( Exception e ) {
			log.warn("wrong property of run type, must be number (0-3). take etrader run type");
		}
		
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : Daily RiskReport Job.");
		}
		
		if(runType != Constants.RUN_TYPE_CHINESE_SKINS) {
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Starting : different ip country report.");
			}
	
			differentIpCountryReport(runType);
	
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Finished : different ip country report.");
			}
		}

		if(runType != Constants.RUN_TYPE_CHINESE_SKINS) {
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Starting : different cc country report.");
			}
	
			differentCcCountryReport(runType);
	
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Finished : different cc country report.");
			}
		}
		
		if(runType != Constants.RUN_TYPE_CHINESE_SKINS) {
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Starting : depositors With X Different Cc.");
			}
	
			depositorsWithMoreThanXDiffCcReport(runType);
	
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,"Finished : depositors With X Different Cc.");
			}
		}
		
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Starting : Send Daily Report Dupicate Phones.");
		}
		
		SendDailyReportDuplicatePhones(runType);
		
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : Send Daily Report Dupicate Phones.");
		}
		
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,"Finished : Daily RiskReport Job.");
		}
	}

	
	private static void initEmail(String subject, String name, int runType) throws Exception {

		// get the emailTemplate
		emailTemplate = Utils.getEmailTemplate(name);

		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject",subject);

		if ( runType == Constants.RUN_TYPE_SKIN_ETRADER ) {
			emailProperties.put("from",Utils.getProperty("email.from.et"));
		} else {
			emailProperties.put("from",Utils.getProperty("email.from.ao"));
		}

	}

	public static String getEmailBody(HashMap params) throws Exception {

		// set the context and the parameters
		VelocityContext context = new VelocityContext();

		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");

		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		emailTemplate.merge(context,sw);

		return sw.toString();
	}


	/**
	 * Get sql for skin and currency partition
	 * @param skinId
	 * 		user skin id
	 * @param currencyId
	 * 		user currency id
	 * @return
	 */
	public static String getSqlBySkinPartintion(long skinId, long currencyId) {
		return " and u.skin_id = " + skinId + " and u.currency_id = " + currencyId + " ";
	}
	
	/**
	 * This report, send all users that their country is different than
	 * 	their registration ip country.
	 * @param runType
	 */
	private static void differentIpCountryReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> newUsersWithDifferentIp = new ArrayList<SecurityCheck>();

		try {

			String omittedIp = "";

			// handle omitted ip's
			String[] omittedIpArr = Utils.getProperty("security.check.omitted.ip").split(";");
			for (String ip : omittedIpArr) {
				omittedIp += "'" + ip + "',";
			}
			omittedIp += "'" + SecurityCheck.IP_NOT_FOUND + "'";

			conn = Utils.getConnection();

			// new users
			String sql1 =
					"SELECT " +
		            	"u.user_name, " +
		            	"u.id, " +
		            	"u.ip, " +
		            	"c.a2 country_code, " +
		            	"c.COUNTRY_NAME country_name, " +
		            	"s.name skin_name, " +
		            	"(CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
		            "FROM " +
		            	"users u " +
		            	"LEFT JOIN (SELECT " +
		            					"t.user_id " +
		            				"FROM " +
		            					"transactions t, transaction_types tt " +
		            				"WHERE " +
		            					"t.type_id = tt.id " +
		            					"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
		            					"AND tt.class_type = " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
		            				"GROUP BY t.user_id ) u_suc_deposit ON u.id = u_suc_deposit.user_id, " +
		            	"countries c, skins s " +
		            "WHERE " +
		              "u.skin_id <> 15 AND " +
			          "u.class_id <> 0  AND " +
			          "u.country_id = c.id  " +
			          Utils.getSqlByRunType(runType) + " AND " +
			          "(to_char(u.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-10,'YYYYMMDD')  or " +
							" to_char(u.time_modified,'YYYYMMDD') = to_char(sysdate-10,'YYYYMMDD')) AND " +
			          "s.id = u.skin_id AND "+
			          "u.ip not in ("+ omittedIp +") " +
			          " and u_suc_deposit.user_id is not null " +
		        "ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();

				String userIp = rs.getString("ip");
				String userCountryCode = rs.getString("country_code");
				String ipCountryCode = CommonUtil.getCountryCodeByIp(userIp);

				if (!userCountryCode.equals(ipCountryCode) && !ipCountryCode.equals("IL") ){
					user.setUsername(rs.getString("user_name"));
					user.setUserId(rs.getInt("id"));
					user.setSucceedDeposit(rs.getString("suc_deposit"));
					user.setIp(userIp);
					user.setUserCountryName(rs.getString("country_name"));

					String countryName = null;
					if(null != ipCountryCode){
						countryName = Utils.getCountryNameByCode(ipCountryCode,ipCountryCode.length());

						if(null == countryName){
							countryName = "--";
						}
					} else {
						countryName = "--";
					}

					user.setIpCountryName(countryName);
					user.setSkin(rs.getString("skin_name"));
					newUsersWithDifferentIp.add(user);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Ip Country Report " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(newUsersWithDifferentIp,
				null,
				Utils.getSubjectByRunType(runType) + "Different Ip Country Report ", Utils.getPageHeaderByRunType(runType),runType,"report_different_ip_country.html", Constants.SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	/**
	 * This report, send all users that their country is different than
	 * 	their credit card issue country.
	 * @param runType
	 */
	private static void differentCcCountryReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<SecurityCheck> tempList = new ArrayList<SecurityCheck>();
		ArrayList<SecurityCheck> usersWithDiffCardCountry = new ArrayList<SecurityCheck>();

		try {
			conn = Utils.getConnection();
			String sql1 =
					" SELECT " +
						" u.user_name, " +
						" u.ip, " + 
						" cc.id cc_id, " +
						" cc.cc_number ccnumber, " +
						" cc.bin, " +
						(runType == Constants.RUN_TYPE_SKIN_ETRADER ? " cct.description, " : "") +
						" co.country_name reg_country " +
					" From " +
						" users u, " +
						" credit_cards cc, " +
						(runType == Constants.RUN_TYPE_SKIN_ETRADER ? " credit_card_types cct, " : "") +
						" countries co " +
						//", cc_bins_country cbn " +
					" Where " +
					    " u.skin_id <> 15 AND " +
						" u.id = cc.user_id " +
						" and u.class_id <> 0 " +
						" and u.country_id = co.id " +
						Utils.getSqlByRunType(runType) +
						" and cc.id in (select t.credit_card_id " +
										" from transactions t " +
										" where t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " " + // cc_deposit
										"   and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " + // success or pending
										"   and to_char(t.TIME_CREATED,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
						(runType == Constants.RUN_TYPE_AO_SKINS ? " and cc.is_documents_sent = 0 " : "") +
						(runType == Constants.RUN_TYPE_SKIN_ETRADER ? " and cct.id = cc.type_id " : "") +
						(runType == Constants.RUN_TYPE_SKIN_ETRADER ? " and cct.id <> " + ConstantsBase.CREDIT_CARD_ISRACARD_TYPE : "") +
		        " ORDER BY user_name ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				user.setUsername(rs.getString("user_name"));
				String userIp = rs.getString("ip");
				String ipCountryCode = CommonUtil.getCountryCodeByIp(userIp);
				String countryName = null;
				if(null != ipCountryCode){
					countryName = Utils.getCountryNameByCode(ipCountryCode, ipCountryCode.length());
					if(null == countryName){
						countryName = "--";
					}
				} else {
					countryName = "--";
				}
				user.setIpCountryName(countryName);
				user.setCcId(rs.getLong("cc_id"));
				String ccNumber = AESUtil.decrypt(rs.getString("ccnumber"));
				String ccLast4Digits = ccNumber.substring(ccNumber.length()-4);
				String ccBin = String.valueOf(rs.getLong("bin"));
				user.setCcNumber(ccBin + "XXXXXX" + ccLast4Digits);
				user.setCcLast4Digits(ccLast4Digits);
				user.setUserCountryName(rs.getString("reg_country"));
				user.setSucceedDeposit("YES");
				if(runType == Constants.RUN_TYPE_SKIN_ETRADER) {
					user.setCcTypeDescription(rs.getString("description"));
				}
				tempList.add(user);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Card Country Report ", e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		try {
			conn = Utils.getConnection();
			String sql1 =
					" SELECT cbn.country_name_a3 cc_country " +
					" From " +
						" users u, " +
						" credit_cards cc, " +
						" countries co, " +
						" bins cbn " +
					" Where " +
						" u.id = cc.user_id " +
						" and u.country_id = co.id " +
						" and cc.id = ? " +
						" and substr(?, 0, 6) between cbn.from_bin and cbn.to_bin  " +
						" and co.a3 != cbn.country_name_a3 ";

			for (int i=0; i<tempList.size(); i++) {
				SecurityCheck user = tempList.get(i);
				pstmt = conn.prepareStatement(sql1);
				pstmt.setLong(1, user.getCcId());
				pstmt.setString(2, user.getCcNumber());
				rs = pstmt.executeQuery();

				if (rs.next()) {
					String countryCode = rs.getString("cc_country");
					String countryName = Utils.getCountryNameByCode(countryCode,countryCode.length());
					if (null == countryName){
						user.setCcCountryName("--");
					} else {
						user.setCcCountryName(countryName);
					}
					usersWithDiffCardCountry.add(user);
				}
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get different Card Country Report " + e);
		} finally {
			try {
				if (null != rs) {
					rs.close();
				}
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				if (null != pstmt) {
					pstmt.close();
				}
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(usersWithDiffCardCountry,
				null,
				Utils.getSubjectByRunType(runType) + "Different Cc Country Report ", Utils.getPageHeaderByRunType(runType),runType,"report_different_card_country.html", Constants.SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	/**
	 * This report, send all users that tried to deposit with more than x cc
	 * @param runType
	 */
	private static void depositorsWithMoreThanXDiffCcReport(int runType) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> depositorsWithMoreThanXDiffCc = new ArrayList<SecurityCheck>();

		try {

			conn = Utils.getConnection();

			String sql1 =
						"select " +
						      "u.id user_id, " +
						      "u.user_name, " +
						      "cc.id cc_id, " +
						      "cc.is_allowed, " +
						      "cc.holder_name, " +
						      "count(tran.credit_card_id) deposits_num " +
						"from " +
						      "users u, " +
						      "credit_cards cc left outer join transactions tran on cc.id = tran.credit_card_id " +
						        														"and tran.type_id = 1  " + // cc deposit
						        														"and tran.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
						"where " +
						      "u.id = cc.user_id and " +
						      "u.skin_id <> 15 and " +
						      "u.class_id <> 0 and " +
						      "EXISTS(select 1 " +
				                     "from    transactions t " +
				                     "where t.user_id = u.id " +
				                         "and t.type_id = 1 " + // cc deposit
				                         "and t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ")) and " +
						      "u.id in (select " +
						                        "DISTINCT(u.id) " +
						                  "from " +
						                        "users u, " +
						                        "credit_cards cc " +
						                  "where " +
						                       "u.id = cc.user_id and " +
						                       "EXISTS (select 1 " +
				                                       "from    transactions t " +
				                                       "where t.user_id = u.id " +
				                                           "and t.type_id = 1 " + // cc deposit
				                                           "and to_char(t.TIME_CREATED,'YYYYMMDD') =  to_char(sysdate-1,'YYYYMMDD') " +
				                                           "and t.credit_card_id = cc.id) and " +
						                        "NOT EXISTS (select 1 " +
			                                                 "from    transactions t , transaction_types tt " +
			                                                 "where t.user_id = u.id " +
			                                                     "and t.type_id = 1 " + // cc deposit
			                                                     "and to_char(t.TIME_CREATED,'YYYYMMDD') <  to_char(sysdate-1,'YYYYMMDD') " +
			                                                     "and t.credit_card_id = cc.id) and " +
						                        Constants.X_DIFF_CC_DEPOSITS_ATTEMPTS + " < (select  count(DISTINCT(t.credit_card_id)) " +
													                               "from  transactions t " +
													                               "where t.user_id = u.id " +
													                                 "and t.type_id = 1)) " +// cc deposit
								Utils.getSqlByRunType(runType) +
						"group by u.id, u.user_name, cc.id, cc.is_allowed, cc.holder_name " +
						"order by u.id, cc.id ";

			pstmt = conn.prepareStatement(sql1);
			rs = pstmt.executeQuery();

			String tempUsername = "";
			String username;
			while(rs.next()) {
				SecurityCheck user = new SecurityCheck();
				username = rs.getString("user_name");

				if (!tempUsername.equals(username)){
					user.setUsername(username);
					tempUsername = username;
				} else {
					user.setUsername("");
				}

				user.setUserId(rs.getInt("user_id"));
				user.setCcId(rs.getLong("cc_id"));
				user.setCcAllowed(rs.getInt("is_allowed")==1?true:false);
				user.setCcHolderName(rs.getString("holder_name"));
				user.setDepositsNum(rs.getLong("deposits_num"));
				depositorsWithMoreThanXDiffCc.add(user);

			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get depositors with more than " + Constants.X_DIFF_CC_DEPOSITS_ATTEMPTS + " Diff Cc " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send emails ");

		sendEmailForMailingList(depositorsWithMoreThanXDiffCc,
				null,
				Utils.getSubjectByRunType(runType) + "Depositors With " + Constants.X_DIFF_CC_DEPOSITS_ATTEMPTS + " Different Cc Report ", Utils.getPageHeaderByRunType(runType),runType,"report_dspositors_with_x_different_cc.html", Constants.SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send emails");

	}

	/**
	 * This report, send all users with duplicate phones
	 * First query return all users with duplicate phones that created today(u1 and also u2),
	 * Second query return all users with duplicate phones, u1 created today and u2 not created today.
	 */
	private static void SendDailyReportDuplicatePhones(int runType) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		HashMap<String, String> usersDupPhone = new HashMap<String, String>();

		try {

			conn = Utils.getConnection();
			String sqlDupicatePhone = " select distinct u1.id user1_id, u2.id user2_id, u1.mobile_phone mobile1, u1.land_line_phone landline1," +
												" u1.user_name username1, u2.user_name username2, u2.mobile_phone mobile2, u2.land_line_phone landline2, " +
												" (CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
									  " from users u1 " +
									  			"LEFT JOIN (SELECT " +
									  							"t.user_id " +
									  						"FROM " +
									  							"transactions t, transaction_types tt " +
									  						"WHERE " +
									  							"t.type_id = tt.id " +
									  							"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
									  							"AND tt.class_type =  " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
									  						"GROUP BY t.user_id) u_suc_deposit ON u1.id = u_suc_deposit.user_id, "+
										    " users u2 " +
										  	    " left join CLOSE_ACCOUNT_USERS bl on u2.id = bl.user_id " +
									  " where (u1.mobile_phone = u2.mobile_phone " +
									  			" or u1.land_line_phone = u2.land_line_phone) " +
									  		 " and u1.id <> u2.id " +
										     " and u1.class_id <> 0 " +
										     " and u2.class_id <> 0 " +
										     Utils.getSqlByRunType(runType,"u1") +
										     Utils.getSqlByRunType(runType,"u2") +
										     " and (to_char(u1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') or " +
									      			" to_char(u1.time_modified,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
										     " and ((u2.is_active = 0 " +
										     			" AND bl.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_CHB_CLOSE+ "" +
										     											","+ IssuesManagerBase.ISSUE_ACTION_FRAUD + ")) " +
										     		" OR " +
										          	  " EXISTS (select 1 " +
											          		   " from transactions t " +
											          		   " where t.charge_back_id is not null and " +
											          		   		  " t.charge_back_id <> 0 and " +
											          		   		  " t.user_id = u2.id) " +
									          		   " ) " +
									   " order by user1_id,user2_id";

			pstmt = conn.prepareStatement(sqlDupicatePhone);
			rs = pstmt.executeQuery();
			users.addAll(handleDuplicatePhoneResults(runType, usersDupPhone, rs, SecurityCheck.USER_LIST_TYPE_BLACK_LIST));
			
			// [DEV-2422] addition for anyoption & chinese skins
			if(runType != Constants.RUN_TYPE_SKIN_ETRADER) {
				sqlDupicatePhone = " select distinct u1.id user1_id, u2.id user2_id, u1.mobile_phone mobile1, u1.land_line_phone landline1," +
											" u1.user_name username1, u2.user_name username2, u2.mobile_phone mobile2, u2.land_line_phone landline2, " +
											" (CASE WHEN u_suc_deposit.user_id is null THEN 'NO' ELSE 'YES' END) as suc_deposit " +
								   " from users u1 " +
								  			"LEFT JOIN (SELECT " +
								  							"t.user_id " +
								  						"FROM " +
								  							"transactions t, transaction_types tt " +
								  						"WHERE " +
								  							"t.type_id = tt.id " +
								  							"AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
								  							"AND tt.class_type =  " + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + " " +
								  						"GROUP BY t.user_id) u_suc_deposit ON u1.id = u_suc_deposit.user_id, "+
								          " users u2 " +
								   " where (u1.mobile_phone = u2.mobile_phone " +
								  			" or u1.land_line_phone = u2.land_line_phone) " +
								  		 " and u1.id <> u2.id " +
									     " and u1.class_id <> 0 " +
									     " and u2.class_id <> 0 " +
									     Utils.getSqlByRunType(runType,"u1") +
									     Utils.getSqlByRunType(runType,"u2") +
									     " and (to_char(u1.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD') or " +
								      			" to_char(u1.time_modified,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
								      	 "and (to_char(u2.time_created,'YYYYMMDD') = to_char(sysdate-1,'YYYYMMDD')) " +
								    " order by user1_id,user2_id";

				pstmt = conn.prepareStatement(sqlDupicatePhone);
				rs = pstmt.executeQuery();
				users.addAll(handleDuplicatePhoneResults(runType, usersDupPhone, rs, SecurityCheck.USER_LIST_TYPE_NEW_ACCOUNT));
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get for duplicate phones report ", e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		log.log(Level.DEBUG,"going to send phones ");

		sendEmailForMailingList(users, null,
				Utils.getSubjectByRunType(runType) + "daily - duplicate phones ", Utils.getPageHeaderByRunType(runType), runType,"report_duplicate_phones.html", Constants.SECURITY_MAILING_LIST, true);

		log.log(Level.DEBUG,"finished to send phones");

	}
	
	private static ArrayList<SecurityCheck> handleDuplicatePhoneResults(int runType, HashMap<String, String> usersDupPhone, ResultSet rs, String fromAccount) throws SQLException {		
		String mobilePhone1, landLinePhone1,mobilePhone2, landLinePhone2;
		long user1Id, user2Id;
		String user1IdString, user2IdString;
		String userName1, userName2;
		String succeedDeposit;
		
		ArrayList<SecurityCheck> users = new ArrayList<SecurityCheck>();

		while(rs.next()) {
			String dupPhone ="";
			mobilePhone1 = rs.getString("mobile1");
			landLinePhone1 = rs.getString("landline1");
			mobilePhone2 = rs.getString("mobile2");
			landLinePhone2 = rs.getString("landline2");
			user1Id = rs.getLong("user1_id");
			user2Id = rs.getLong("user2_id");
			user1IdString = Long.toString(user1Id);
			user2IdString = Long.toString(user2Id);
			userName1 = rs.getString("username1");
			userName2 = rs.getString("username2");
			succeedDeposit = rs.getString("suc_deposit");
			
			if(runType == Constants.RUN_TYPE_SKIN_ETRADER) {
				// check if duplicate phones
				if (null != mobilePhone1 && null != mobilePhone2 && !mobilePhone1.equalsIgnoreCase("null") && !mobilePhone2.equalsIgnoreCase("null") && mobilePhone1.equals(mobilePhone2)){
					dupPhone = mobilePhone1;
				} else if (null != landLinePhone1 && null != landLinePhone2 &&  !landLinePhone1.equalsIgnoreCase("null") && !landLinePhone2.equalsIgnoreCase("null") && landLinePhone1.equals(landLinePhone2)){
					dupPhone = landLinePhone1;
				}
			} else {
				// check that phone number doesn't contain only 0 (and not any other digits), like "00", "0000", etc...
				String regex = "[0]+";
				boolean mobilePhone1ContainsOnlyZeros = false;
				boolean mobilePhone2ContainsOnlyZeros = false;
				boolean landLinePhone1ContainsOnlyZeros = false;
				boolean landLinePhone2ContainsOnlyZeros = false;
				
				if(null != mobilePhone1 && !mobilePhone1.equalsIgnoreCase("null")) {
					mobilePhone1ContainsOnlyZeros = mobilePhone1.matches(regex);
				}
				if(null != mobilePhone2 && !mobilePhone2.equalsIgnoreCase("null")) {
					mobilePhone2ContainsOnlyZeros = mobilePhone2.matches(regex);
				}
				if(null != landLinePhone1 && !landLinePhone1.equalsIgnoreCase("null")) {
					landLinePhone1ContainsOnlyZeros = landLinePhone1.matches(regex);
				}
				if(null != landLinePhone2 && !landLinePhone2.equalsIgnoreCase("null")) {
					landLinePhone2ContainsOnlyZeros = landLinePhone2.matches(regex);
				}
				
				// check if duplicate phones
				if (null != mobilePhone1 && null != mobilePhone2 && !mobilePhone1.equalsIgnoreCase("null") && !mobilePhone2.equalsIgnoreCase("null") && !mobilePhone1ContainsOnlyZeros && !mobilePhone2ContainsOnlyZeros && mobilePhone1.equals(mobilePhone2)){
					dupPhone = mobilePhone1;
				} else if (null != landLinePhone1 && null != landLinePhone2 && !landLinePhone1.equalsIgnoreCase("null") && !landLinePhone2.equalsIgnoreCase("null") && !landLinePhone1ContainsOnlyZeros && !landLinePhone2ContainsOnlyZeros && landLinePhone1.equals(landLinePhone2)){
					dupPhone = landLinePhone1;
				}
			}
			
			// check duplicate records by using a combination of (userId1 + ; + userId2) and the duplicated Phone.
			if (!dupPhone.equals("") && (usersDupPhone.containsKey(user1IdString + ';' + user2IdString) == false ||
					!usersDupPhone.get(user1IdString + ';' + user2IdString).equals(dupPhone))) {

				usersDupPhone.put(user1IdString + ';' + user2IdString, dupPhone);

				SecurityCheck temp = new SecurityCheck();
				temp.setPhone(dupPhone);
				temp.setUserId(user1Id);
				temp.setUserId2(user2Id);
				temp.setUsername(userName1);
				temp.setUserName2(userName2);
				temp.setSucceedDeposit(succeedDeposit);
				temp.setFromAccount(fromAccount);
				users.add(temp);
			}
		}
		
		return users;
	}

	/** Send Mail for the selected users list
	 * @param users
	 * @param ccDepositsStatusList
	 * @param isAddTodayDateToSubject
	 */
	private static void sendEmailForMailingList(ArrayList<SecurityCheck> users, ArrayList<CcDepositsStatus> ccDepositsStatusList, String subjectP,
			String header, int runType, String htmlName, String mailingList, boolean isAddTodayDateToSubject) {

		// put all users params
		if (null != users){
			log.info("number of users for " + subjectP + " mail is: " + users.size());
		}

		HashMap<String, Object> params = new HashMap<String, Object>();
		String subject = subjectP;

		params.put("users",users);
		params.put("ccDepositsStatus",ccDepositsStatusList);
		params.put("header",header);
		params.put("runType", runType);

		if (isAddTodayDateToSubject){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_YEAR,-1);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			subject += sdf.format(cal.getTime());
		}

		try {
			initEmail(subject,htmlName, runType);
		} catch (Exception e) {
			log.fatal("!!! ERROR >> INIT email: " + e.getMessage());
		}

		try {
			emailProperties.put("body",getEmailBody(params));
		} catch (Exception e) {
			log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		}

		String[] emails = Utils.getEmailByRunType(runType, mailingList).split(";");

		for (int i = 0; i < emails.length; i++) {
			emailProperties.put("to",emails[i]);
			Utils.sendEmail(serverProperties,emailProperties);
			emailProperties.remove("to");
		}
	}
}