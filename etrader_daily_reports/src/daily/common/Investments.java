package daily.common;

public class Investments {

	private double turnoverAmount;

	private long turnoverCount;

	private double intradayTurnoverAmount;

	private long intradayTurnoverCount;

	private double intradayprofitAmount;

	private double intradayprofitPrecentage;

	private double openPositionsEndWeekAmount;

	private long openPositionsEndWeekCount;

	private double openPositionsEndMonthAmount;

	private long openPositionsEndMonthCount;

	private long headCount;

	private long winCount;

	private double winTurnover;

	private double winAmount;

	private long loseCount;

	private double loseTurnover;

	private double loseAmount;

	private double callAmount;

	private double putAmount;

	private long callCount;

	private long putCount;

	private String currency;


	public double getLoseAmount() {
		return loseAmount;
	}

	public String getLoseAmountStr() {
		return Utils.displayAmount(-loseAmount);
	}

	public void setLoseAmount(double loseAmount) {
		this.loseAmount = loseAmount;
	}

	public long getLoseCount() {
		return loseCount;
	}

	public void setLoseCount(long loseCount) {
		this.loseCount = loseCount;
	}

	public double getLoseTurnover() {
		return loseTurnover;
	}

	public String getLoseTurnoverStr() {
		return  Utils.displayAmount(loseTurnover);
	}

	public void setLoseTurnover(double loseTurnover) {
		this.loseTurnover = loseTurnover;
	}

	public double getWinAmount() {
		return winAmount;
	}

	public String getWinAmountStr() {
		return Utils.displayAmount(winAmount);
	}

	public void setWinAmount(double winAmount) {
		this.winAmount = winAmount;
	}

	public long getWinCount() {
		return winCount;
	}

	public void setWinCount(long winCount) {
		this.winCount = winCount;
	}

	public double getWinTurnover() {
		return winTurnover;
	}

	public String getWinTurnoverStr() {
		return Utils.displayAmount(winTurnover);
	}

	public void setWinTurnover(double winTurnover) {
		this.winTurnover = winTurnover;
	}

	public Investments() {

	}

	public long getHeadCount() {
		return headCount;
	}

	public void setHeadCount(long headCount) {
		this.headCount = headCount;
	}

	public double getIntradayprofitAmount() {
		return intradayprofitAmount;
	}

	public String getIntradayprofitAmountStr() {
		return Utils.displayAmount(intradayprofitAmount);
	}

	public void setIntradayprofitAmount(double intradayprofitAmount) {
		this.intradayprofitAmount = intradayprofitAmount;
	}

	public double getIntradayprofitPrecentage() {
		return intradayprofitPrecentage;
	}

	public String getIntradayprofitPrecentageStr() {
		return Utils.displayAmount(intradayprofitPrecentage) + "%";
	}

	public void setIntradayprofitPrecentage(double intradayprofitPrecentage) {
		this.intradayprofitPrecentage = intradayprofitPrecentage;
	}

	public double getIntradayTurnoverAmount() {
		return intradayTurnoverAmount;
	}

	public String getIntradayTurnoverAmountStr() {
		return Utils.displayAmount(intradayTurnoverAmount);
	}

	public void setIntradayTurnoverAmount(double intradayTurnoverAmount) {
		this.intradayTurnoverAmount = intradayTurnoverAmount;
	}

	public long getIntradayTurnoverCount() {
		return intradayTurnoverCount;
	}

	public void setIntradayTurnoverCount(long intradayTurnoverCount) {
		this.intradayTurnoverCount = intradayTurnoverCount;
	}

	public double getOpenPositionsEndMonthAmount() {
		return openPositionsEndMonthAmount;
	}

	public String getOpenPositionsEndMonthAmountStr() {
		return Utils.displayAmount(openPositionsEndMonthAmount);
	}

	public void setOpenPositionsEndMonthAmount(
			double openPositionsEndMonthAmount) {
		this.openPositionsEndMonthAmount = openPositionsEndMonthAmount;
	}

	public long getOpenPositionsEndMonthCount() {
		return openPositionsEndMonthCount;
	}

	public void setOpenPositionsEndMonthCount(long openPositionsEndMonthCount) {
		this.openPositionsEndMonthCount = openPositionsEndMonthCount;
	}

	public double getOpenPositionsEndWeekAmount() {
		return openPositionsEndWeekAmount;
	}

	public String getOpenPositionsEndWeekAmountStr() {
		return Utils.displayAmount(openPositionsEndWeekAmount);
	}

	public void setOpenPositionsEndWeekAmount(double openPositionsEndWeekAmount) {
		this.openPositionsEndWeekAmount = openPositionsEndWeekAmount;
	}

	public long getOpenPositionsEndWeekCount() {
		return openPositionsEndWeekCount;
	}

	public void setOpenPositionsEndWeekCount(long openPositionsEndWeekCount) {
		this.openPositionsEndWeekCount = openPositionsEndWeekCount;
	}

	public double getTurnoverAmount() {
		return turnoverAmount;
	}

	public String getTurnoverAmountStr() {
		return Utils.displayAmount(turnoverAmount);
	}

	public void setTurnoverAmount(double turnoverAmount) {
		this.turnoverAmount = turnoverAmount;
	}

	public long getTurnoverCount() {
		return turnoverCount;
	}

	public void setTurnoverCount(long turnoverCount) {
		this.turnoverCount = turnoverCount;
	}

	public double getCallAmount() {
		return callAmount;
	}

	public String getCallAmountStr() {
		return Utils.displayAmount(callAmount);
	}

	public void setCallAmount(double callAmount) {
		this.callAmount = callAmount;
	}

	public long getCallCount() {
		return callCount;
	}

	public void setCallCount(long callCount) {
		this.callCount = callCount;
	}

	public double getPutAmount() {
		return putAmount;
	}

	public String getPutAmountStr() {
		return Utils.displayAmount(putAmount);
	}

	public void setPutAmount(double putAmount) {
		this.putAmount = putAmount;
	}

	public long getPutCount() {
		return putCount;
	}

	public void setPutCount(long putCount) {
		this.putCount = putCount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public static Investments getDifference(Investments inv1, Investments inv2) {
		Investments i = new Investments();

		i.turnoverAmount = inv1.turnoverAmount - inv2.turnoverAmount;
		i.turnoverCount = inv1.turnoverCount - inv2.turnoverCount;
		i.intradayTurnoverAmount = inv1.intradayTurnoverAmount - inv2.intradayTurnoverAmount;
		i.intradayTurnoverCount = inv1.intradayTurnoverCount - inv2.intradayTurnoverCount;
		i.intradayprofitAmount = inv1.intradayprofitAmount - inv2.intradayprofitAmount;

		double profit = (inv1.getIntradayprofitAmount() - inv2.getIntradayprofitAmount());
		double sum =  inv1.getIntradayTurnoverAmount() - inv2.getIntradayTurnoverAmount();
		i.intradayprofitPrecentage = (profit / sum)*100 ;

		i.openPositionsEndWeekAmount = inv1.openPositionsEndWeekAmount - inv2.openPositionsEndWeekAmount;
		i.openPositionsEndWeekCount = inv1.openPositionsEndWeekCount - inv2.openPositionsEndWeekCount;
		i.openPositionsEndMonthAmount = inv1.openPositionsEndMonthAmount - inv2.openPositionsEndMonthAmount;
		i.openPositionsEndMonthCount = inv1.openPositionsEndMonthCount - inv2.openPositionsEndMonthCount;
		i.headCount = inv1.headCount - inv2.headCount;
		i.winCount = inv1.winCount - inv2.winCount;
		i.winTurnover = inv1.winTurnover - inv2.winTurnover;
		i.winAmount = inv1.winAmount - inv2.winAmount;
		i.loseCount = inv1.loseCount - inv2.loseCount;
		i.loseTurnover = inv1.loseTurnover - inv2.loseTurnover;
		i.loseAmount = inv1.loseAmount - inv2.loseAmount;
		i.callAmount = inv1.callAmount - inv2.callAmount;
		i.putAmount = inv1.putAmount - inv2.putAmount;
		i.callCount = inv1.callCount - inv2.callCount;
		i.putCount = inv1.putCount - inv2.putCount;
		if (inv1.currency.equals(inv2.currency)) {
			i.currency = inv1.currency;
		}

		return i;
	}
}
