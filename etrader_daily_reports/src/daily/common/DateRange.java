package daily.common;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateRange {
	String startDate;
	String endDate;

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartDateForSql() throws ParseException{
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
		String temp = inputFormat.format((Date) new SimpleDateFormat("dd-MM-yyyyy").parse(startDate));
		return temp;
	}

	public String getEndDateForSql() throws ParseException{
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
		String temp = inputFormat.format((Date) new SimpleDateFormat("dd-MM-yyyyy").parse(endDate));
		return temp;
	}

	public String getDateRange(){
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
		String temp = "";
		try {
			temp = inputFormat.format((Date) new SimpleDateFormat("dd-MM-yyyyy").parse(startDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat inputFormat2 = new SimpleDateFormat("dd-MM-yyyy");
		String temp2 = "";
		try {
			temp2 = inputFormat2.format((Date) new SimpleDateFormat("dd-MM-yyyyy").parse(endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "Date Range - from :" + temp + " to :" + temp2;
	}
	
	public Date getStartDateTime() throws ParseException {
		Date startDateTime;
		String mask = "dd-MM-yyyy";
		DateFormat df = new SimpleDateFormat(mask);
		startDateTime = df.parse(startDate);
		return startDateTime;
	}
	
	public Date getEndDateTime() throws ParseException {
		Date endDateTime;
		String mask = "dd-MM-yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(mask);
		endDateTime = df.parse(endDate + " 23:59:59");
		return endDateTime;
	}

}
