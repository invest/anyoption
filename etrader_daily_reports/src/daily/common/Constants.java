package daily.common;

public class Constants {
	// Constants for running type
	public static final int RUN_TYPE_ALL_SKINS = 0;
	public static final int RUN_TYPE_SKIN_ETRADER = 1;
	public static final int RUN_TYPE_AO_SKINS = 2;
	public static final int RUN_TYPE_CHINESE_SKINS = 3;
	
	// Constants for date range type
	public static final int RUN_DAILY = 0;
	public static final int RUN_WEEKLY = 1;
	public static final int RUN_MONTHLY = 2;
	public static final int X_DIFF_CC_DEPOSITS_ATTEMPTS = 3;
	public static String SECURITY_MAILING_LIST = "emails.security.check";
	
	public static String MOBILE_WRITER = "in (200)";
	public static String NON_MOBILE_WRITER = "not in (200, 10000, 20000)";
	public static String TOTAL_AO_WRITER = "not in (10000, 20000)";
	
	public static String COPYOP_MOBILE_WRITER = "in (20000)";
	public static String COPYOP_NON_MOBILE_WRITER = " in (10000)";
	public static String TOTAL_COPYOP_WRITER = " in (10000, 20000)";
	
	public static final int OUT_OF_THIS_WEEK_FD = 1;
	public static final int OUT_OF_TOTAL_FD_EVER = 2;
	
	public static String AFFILIATES_CAMPAIGN = "affiliates";
	public static String TONY_JASON_CAMPAIGN = "tony_jason";
	public static String MEDIA_TLV_CAMPAIGN = "media_tlv";
	public static String BENI_CAMPAIGN = "beni";
	public static String PPC_CAMPAIGN = "ppc";
	public static String MEDIA_TLT_CAMPAIGN = "media_tlt";
	public static String FREE_CAMPAIGN = "free";
	public static String GOOGLE_CAMPAIGN = "google";
	public static String TOTAL_CAMPAIGN = "total";
	


	
	
}
