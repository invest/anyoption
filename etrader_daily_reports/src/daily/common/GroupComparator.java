package daily.common;
import java.util.Comparator;


public class GroupComparator implements Comparator {

	public int compare(Object arg0, Object arg1) {
		Market m1 = (Market)arg0;
		Market m2 = (Market)arg1;
		return m1.getGroup().compareToIgnoreCase(m2.getGroup());
	}

}
