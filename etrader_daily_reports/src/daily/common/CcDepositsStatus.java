package daily.common;

/**
 * CcDepositsStatus vo class
 * @author Eliran
 *
 */
public class CcDepositsStatus implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4249577288326924882L;

	private String provider;
	private String currencyCode;
	private long numSuccess;
	private long numFailed;
	private long numTotal;
	private String percentSuccess;

	/**
	 * @return the numFailed
	 */
	public long getNumFailed() {
		return numFailed;
	}

	/**
	 * @param numFailed the numFailed to set
	 */
	public void setNumFailed(long numFailed) {
		this.numFailed = numFailed;
	}

	/**
	 * @return the numSuccess
	 */
	public long getNumSuccess() {
		return numSuccess;
	}

	/**
	 * @param numSuccess the numSuccess to set
	 */
	public void setNumSuccess(long numSuccess) {
		this.numSuccess = numSuccess;
	}

	/**
	 * @return the numTotal
	 */
	public long getNumTotal() {
		return numTotal;
	}

	/**
	 * @param numTotal the numTotal to set
	 */
	public void setNumTotal(long numTotal) {
		this.numTotal = numTotal;
	}

	/**
	 * @return the percentSuccess
	 */
	public String getPercentSuccess() {
		return percentSuccess;
	}

	/**
	 * @param percentSuccess the percentSuccess to set
	 */
	public void setPercentSuccess(String percentSuccess) {
		this.percentSuccess = percentSuccess;
	}

	public CcDepositsStatus() {
		currencyCode = "";
	}

	/**
	 * @return the code
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param code the code to set
	 */
	public void setCurrencyCode(String code) {
		this.currencyCode = code;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}




}
