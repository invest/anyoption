package daily.common;
import il.co.etrader.util.ConstantsBase;

public class BaseReportRow {

	private String rowName;
	
	//AO
	private double totalAmountUsd;
	private double count;
	private double headcount;
	
	//AO Mobile
	private double totalAmountUsdMobile;
	private double countMobile;
	private double headcountMobile;
	
	//AO Total
	private double totalAmountUsdAOAll = 0;
	private double headcountAOAll;
	private double countAOAllpct = -1;
	
	//Copyop	
	private double totalAmountUsdCopyop;
	private double countCopyop;
	private double headcountCopyop;

	//Copyop Mobile
	private double totalAmountUsdCopyopMobile;
	private double countCopyopMobile;
	private double headcountCopyopMobile;
	
	//Copyop Total
	private double totalAmountUsdCopyopAll = 0;
	private double headcountCopyopAll;
	private double countCopyopAllpct = -1;
	
	//Total
	private double totalAmountUsdAll = 0;	
	private double headcountAll;
	private double countAllpct = -1;
	
	private double profitUsd;
	private double profitUsdMobile;

	private double profitUsdCopyop;
	private double profitUsdCopyopMobile;

	
	private long regUsersNum;
	private long regUsersNumMobile;
	private long regUsersNumCopyop;
	private long regUsersNumCopyopMobile;
	
	private double regUsersPercent;
	private double regUsersPercentMobile;
	private double regUsersPercentCopyop;
	private double regUsersPercentCopyopMobile;
	private double regUsersPercentAOAll;
	private double regUsersPercentCopyopAll;
	private double regUsersPercentAll;
	
	private long firstDepsNum;
	private long firstDepsNumMobile;
	private long firstDepsNumCopyop;
	private long firstDepsNumCopyopMobile;
	
	private double firstDepsPercent;
	private double firstDepsPercentMobile;
	private double firstDepsPercentCopyop;
	private double firstDepsPercentCopyopMobile;
	private double firstDepsPercentAOAll;
	private double firstDepsPercentCopyopAll;
	private double firstDepsPercentAll;
	
	
	private String percentStr;
	private String style;
	
	private double precentProfitUsd;
	private double precentProfitUsdMobile;
	private double precentProfitUsdAOAll;
	private double precentProfitUsdCopyop;
	private double precentProfitUsdCopyopMobile;
	private double precentProfitUsdCopyopAll;
	private double precentProfitUsdAll;
	
	private long  outOfThisWeekFd;
	private long  outOfThisWeekFdMobile;
	private long  outOfThisWeekFdCopyop;
	private long  outOfThisWeekFdCopyopMobile;
	
	private long  outOfTotalFDOfEver;
	private long  outOfTotalFDOfEverMobile;
	private long  outOfTotalFDOfEverCopyop;
	private long  outOfTotalFDOfEverCopyopMobile;
	

	public BaseReportRow() {
		percentStr = "";
		style = "";
	}

	public void setPercentStr() {
		percentStr = "%";
	}

	public void setBold() {
		style = "font-weight:bold;";
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @return the count
	 */
	public double getCount() {
		return count;
	}

	public String getCountStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == count){
			return "";
		}
		return Utils.displayAmountRounded(count, percentStr);
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(double count) {
		this.count = count;
	}
	/**
	 * @return the firstDepsNum
	 */
	public long getFirstDepsNum() {
		return firstDepsNum;
	}
	
	public String getFirstDepsNumStr() {
		return Utils.displayAmountRounded(getFirstDepsNum(), "");
	}
	
	/**
	 * @param firstDepsNum the firstDepsNum to set
	 */
	public void setFirstDepsNumAndPercent(long firstDepsNum, long totalFirstDeps) {
		this.firstDepsNum = firstDepsNum;

		if (firstDepsNum > 0){
			this.firstDepsPercent =  new Double(firstDepsNum) * 100 / new Double(totalFirstDeps);
		}
	}
	
	public void setFirstDepsNumAndPercentMobile(long firstDepsNumMobile, long totalFirstDepsMobile) {
		this.firstDepsNumMobile = firstDepsNumMobile;

		if (totalFirstDepsMobile > 0){
			this.firstDepsPercentMobile =  new Double(firstDepsNumMobile) * 100 / new Double(totalFirstDepsMobile);
		}
	}
	
	public void setFirstDepsNumAndPercentCopyop(long firstDepsNumCopyop, long totalFirstDepsCopyop) {
		this.firstDepsNumCopyop = firstDepsNumCopyop;

		if (totalFirstDepsCopyop > 0){
			firstDepsPercentCopyop = new Double(firstDepsNumCopyop) * 100 / new Double(totalFirstDepsCopyop);
		}
	}
	
	public void setFirstDepsNumAndPercentCopyopMobile(long firstDepsNumCopyopMobile, long totalFirstDepsCopyopMobile) {
		this.firstDepsNumCopyopMobile = firstDepsNumCopyopMobile;

		if (totalFirstDepsCopyopMobile > 0){
			this.firstDepsPercentCopyopMobile = new Double(firstDepsNumCopyopMobile) * 100 / new Double(totalFirstDepsCopyopMobile);
		}
	}
	
	public void setFirstDepsPercentAOAll(long totalFirstDepsAOAll) {

		if (totalFirstDepsAOAll > 0){
			this.firstDepsPercentAOAll = new Double((firstDepsNum + firstDepsNumMobile)) * 100 / new Double(totalFirstDepsAOAll);
		}
	}
	
	public void setFirstDepsPercentCopyopAll(long totalFirstDepsCopyopAll) {

		if (totalFirstDepsCopyopAll > 0){
			this.firstDepsPercentCopyopAll = new Double((firstDepsNumCopyop + firstDepsNumCopyopMobile)) * 100 / new Double(totalFirstDepsCopyopAll);
		}
	}
	
	public void setFirstDepsPercentAll(long totalFirstDepsAll) {

		if (totalFirstDepsAll > 0){
			this.firstDepsPercentAll = new Double((firstDepsNum + firstDepsNumMobile + firstDepsNumCopyop + firstDepsNumCopyopMobile)) * 100 / new Double(totalFirstDepsAll);
		}
	}
	/**
	 * @return the firstDepsPercent
	 */
	public double getFirstDepsPercent() {
		return firstDepsPercent;
	}

	public String getFirstDepsPercentStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercent){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercent, "%");				 
	}
	
	public String getFirstDepsPercentMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercentMobile){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentMobile, "%");				 
	}
	
	public String getFirstDepsPercentCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercentCopyop){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentCopyop, "%");				 
	}
	
	public String getFirstDepsPercentCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercentCopyopMobile){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentCopyopMobile, "%");				 
	}
	
	public String getFirstDepsPercentAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercent){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentAOAll, "%");				
	}
	
	public String getFirstDepsPercentCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercentCopyop){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentCopyopAll, "%");				
	}
	
	public String getFirstDepsPercentAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercent){
			return "";
		}
		return Utils.displayAmountRounded(firstDepsPercentAll, "%");				
	}
	/**
	 * @return the headcount
	 */
	public double getHeadcount() {
		return headcount;
	}

	/**
	 * @return the headcount
	 */
	public String getHeadcountStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcount){
			return "";
		}
		return Utils.displayAmountRounded(headcount, percentStr);
	}

	/**
	 * @param headcount the headcount to set
	 */
	public void setHeadcount(double headcount) {
		this.headcount = headcount;
	}
	/**
	 * @param headcount the headcount to set
	 */
	public void addHeadcount(long headcount) {
		this.headcount += headcount;
	}
	
	public void addHeadcountMobile(long headcountMobile) {
		this.headcountMobile += headcountMobile;
	}
	
	public void addHeadcountAOAll(long headcountAOAll) {
		this.headcountAOAll += headcountAOAll;
	}
	
	public void addHeadcountCopyop(long headcountCopyop) {
		this.headcountCopyop += headcountCopyop;
	}
	
	public void addHeadcountCopyopMobile(long headcountCopyopMobile) {
		this.headcountCopyopMobile += headcountCopyopMobile;
	}
	
	public void addHeadcountCopyopAll(long headcountCopyopAll) {
		this.headcountCopyopAll += headcountCopyopAll;
	}
	
	public void addHeadcountAll(long headcountAll) {
		this.headcountAll += headcountAll;
	}
	
	/**
	 * @return the getPrecentProfitUsd
	 */
	public String getPrecentProfitUsdStr() {
		return Utils.displayAmountRounded(precentProfitUsd, "%");
	}	
	/**
	 * @return the profitUsd
	 */
	
	public void setPrecentProfitUsd() {
		if (totalAmountUsd != 0) {
			this.precentProfitUsd  = (profitUsd * 100) / totalAmountUsd;
		}
	}
	
	public String getPrecentProfitUsdMobile() {
		return Utils.displayAmountRounded(precentProfitUsdMobile, "%");
	}	
	
	public void setPrecentProfitUsdMobile() {
		if (totalAmountUsdMobile != 0) {
			this.precentProfitUsdMobile  = (profitUsdMobile * 100) / totalAmountUsdMobile;
		}
	}
	
	public void setPrecentProfitUsdAOAll() {
		if ((totalAmountUsd + totalAmountUsdMobile) != 0) {
			precentProfitUsdAOAll  = ((profitUsd + profitUsdMobile) * 100) / (totalAmountUsd + totalAmountUsdMobile);
		}
	}
	
	public String getPrecentProfitUsdAOAll() {
		return Utils.displayAmountRounded(precentProfitUsdAOAll, "%");
	}
	
	public String getPrecentProfitUsdCopyop() {
		return Utils.displayAmountRounded(precentProfitUsdCopyop, "%");
	}	
	
	public void setPrecentProfitUsdCopyop() {
		if (totalAmountUsdCopyop != 0) {
			this.precentProfitUsdCopyop  = (profitUsdCopyop * 100) / totalAmountUsdCopyop;
		}
	}
	
	public String getPrecentProfitUsdCopyopMobile() {
		return Utils.displayAmountRounded(precentProfitUsdCopyopMobile, "%");
	}
	
	public void setPrecentProfitUsdCopyopMobile() {
		if (totalAmountUsdCopyopMobile != 0) {
			this.precentProfitUsdCopyopMobile  = (profitUsdCopyopMobile * 100) / totalAmountUsdCopyopMobile;
		}
	}
	
	public void setPrecentProfitUsdCopyopAll() {
		if ((totalAmountUsdCopyop + totalAmountUsdCopyopMobile) != 0) {
			precentProfitUsdCopyopAll  = ((profitUsdCopyop + profitUsdCopyopMobile) * 100) / (totalAmountUsdCopyop + totalAmountUsdCopyopMobile);
		}
	}
	
	public String getPrecentProfitUsdCopyopAll() {
		
		return Utils.displayAmountRounded(precentProfitUsdCopyopAll, "%");
	}
	
	public void setPrecentProfitUsdAll() {
		if ((totalAmountUsdCopyop + totalAmountUsdCopyopMobile + totalAmountUsd + totalAmountUsdMobile) != 0) {
			precentProfitUsdAll  = ((profitUsdCopyop + profitUsdCopyopMobile + profitUsd + profitUsdMobile) * 100) / (totalAmountUsdCopyop + totalAmountUsdCopyopMobile + totalAmountUsd + totalAmountUsdMobile);
		}		
	}
	
	public String getPrecentProfitUsdAll() {
		return Utils.displayAmountRounded(precentProfitUsdAll, "%");
	}
	
	/**
	 * @return the profitUsd
	 */
	public void setPrecentProfitUsd(double totalAmount,double totalProfit) {
		if (totalAmount != 0) {
			this.precentProfitUsd  = (totalProfit * 100) / totalAmount;
		}		
	}
	
	public void setPrecentProfitUsdMobile(double totalAmountMobile,double totalProfitMobile) {
		if (totalAmountMobile != 0) {
			this.precentProfitUsdMobile  = (totalProfitMobile * 100) / totalAmountMobile;
		}
	}
	
	public void setPrecentProfitUsdAoAll(double totalAmountAoAll,double totalProfitAoAll) {
		if (totalAmountAoAll != 0) {
			this.precentProfitUsdAOAll  = (totalProfitAoAll * 100) / totalAmountAoAll;
		}
	}

	public void setPrecentProfitUsdCopyop(double totalAmountUsdCopyop,double totalProfitUsdCopyop) {
		if (totalAmountUsdCopyop != 0) {
			this.precentProfitUsdCopyop  = (totalProfitUsdCopyop * 100) / totalAmountUsdCopyop;
		}
	}

	public void setPrecentProfitUsdCopyopMobile(double totalAmountUsdCopyopMobile,double totalProfitUsdCopyopMobile) {
		if (totalAmountUsdCopyopMobile != 0) {
			this.precentProfitUsdCopyopMobile  = (totalProfitUsdCopyopMobile * 100) / totalAmountUsdCopyopMobile;
		}
	}

	public void setPrecentProfitUsdCopyopAll(double totalAmountUsdCopyopAll, double totalProfitUsdCopyopAll) {
		if (totalAmountUsdCopyopAll != 0) {
			precentProfitUsdCopyopAll  = (totalProfitUsdCopyopAll * 100) / totalAmountUsdCopyopAll;
		}
	}

	public void setPrecentProfitUsdAll(double totalAmountUsdAll,double totalProfitUsdAll) {
		if (totalAmountUsdAll != 0) {
			precentProfitUsdAll  = ((profitUsdCopyop + profitUsdCopyopMobile + profitUsd + profitUsdMobile) * 100) / totalAmountUsdAll;
		}		
	}
	
	/**
	 * @param profitUsd the profitUsd to set
	 */
	
	public double getProfitUsd() {
		return profitUsd;
	}
	/**
	 * @param profitUsd the profitUsd to set
	 */
	public void setProfitUsd(double profitUsd) {
		this.profitUsd = profitUsd;
	}

	public String getProfitUsdStr() {
		return Utils.displayAmountRounded(profitUsd,"");
	}
	
	public String getProfitUsdMobileStr() {
		return Utils.displayAmountRounded(profitUsdMobile, "");
	}
	
	public String getProfitUsdAOAllStr() {
		return Utils.displayAmountRounded(getProfitUsdAoAll(), "");
	}
	
	public String getProfitUsdCopyopStr() {
		return Utils.displayAmountRounded(profitUsdCopyop, "");
	}
	
	public String getProfitUsdCopyopMobileStr() {
		return Utils.displayAmountRounded(profitUsdCopyopMobile, "");
	}
	
	public String getProfitUsdCopyopAllStr() {
		return Utils.displayAmountRounded(getProfitUsdCopyopAll(), "");
	}
	
	public String getProfitUsdAllStr() {
		return Utils.displayAmountRounded(getProfitUsdAll(), "");
	}

	/**
	 * @return the regUsersNum
	 */
	public long getRegUsersNum() {
		return regUsersNum;
	}
	
	public String getRegUsersNumStr() {
		return Utils.displayAmountRounded(getRegUsersNum(), "");
	}
	/**
	 * @param regUsersNum the regUsersNum to set
	 */
	public void setRegUsersNumAndPercent(long regUsersNum, long totalReg) {
		this.regUsersNum = regUsersNum;

		if (totalReg > 0){
			this.regUsersPercent = new Double(regUsersNum)* 100 / new Double(totalReg);
		}
	}
	
	public void setRegUsersNumAndPercentMobile(long regUsersNumMobile, long totalRegMobile) {
		this.regUsersNumMobile = regUsersNumMobile;

		if (totalRegMobile > 0){
			this.regUsersPercentMobile = new Double(regUsersNumMobile) * 100 / new Double(totalRegMobile);
		}
	}
	
	public void setRegUsersNumAndPercentCopyop(long regUsersNumCopyop, long totalRegCopyop) {
		this.regUsersNumCopyop = regUsersNumCopyop;

		if (totalRegCopyop > 0){
			this.regUsersPercentCopyop = new Double(regUsersNumCopyop) * 100 / new Double(totalRegCopyop);
		}
	}
	
	public void setRegUsersNumAndPercentCopyopMobile(long regUsersNumCopyopMobile, long totalRegCopyopMobile) {
		this.regUsersNumCopyopMobile = regUsersNumCopyopMobile;

		if (totalRegCopyopMobile > 0){
			this.regUsersPercentCopyopMobile = new Double(regUsersNumCopyopMobile) * 100 / new Double(totalRegCopyopMobile);
		}
	}
	
	public void setRegUsersPercentAOAll(long totalRegAOAll) {

		if (totalRegAOAll > 0){
			this.regUsersPercentAOAll = new Double((regUsersNum + regUsersNumMobile)) * 100 / new Double(totalRegAOAll);
		}
	}
	
	public void setRegUsersPercentCopyopAll(long totalRegCopyopAll) {

		if (totalRegCopyopAll > 0){
			this.regUsersPercentCopyopAll = new Double((regUsersNumCopyop + regUsersNumCopyopMobile)) * 100 / new Double(totalRegCopyopAll);
		}
	}
	
	public void setRegUsersPercentAll(long totalRegAll) {

		if (totalRegAll > 0){
			this.regUsersPercentAll = new Double((regUsersNumCopyop + regUsersNumCopyopMobile + regUsersNum + regUsersNumMobile)) * 100 / new Double(totalRegAll);
		}
	}
	/**
	 * @return the regUsersPercent
	 */
	public double getRegUsersPercent() {
		return regUsersPercent;
	}

	public String getRegUsersPercentStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercent){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercent, "%");
	}
	
	public String getRegUsersPercentMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercentMobile){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentMobile, "%");
	}
	
	public String getRegUsersPercentCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercentCopyop){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentCopyop, "%");
	}
	
	public String getRegUsersPercentCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercentCopyopMobile){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentCopyopMobile, "%");
	}
	
	public String getRegUsersPercentAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercent){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentAOAll, "%");
	}
	
	
	public String getRegUsersPercentCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercentCopyop){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentCopyopAll, "%");
	}
	
	
	public String getRegUsersPercentAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercent){
			return "";
		}
		return Utils.displayAmountRounded(regUsersPercentAll, "%");
	}

	/**
	 * @return the rowName
	 */
	public String getRowName() {
		return rowName;
	}
	/**
	 * @param rowName the rowName to set
	 */
	public void setRowName(String rowName) {
		this.rowName = rowName;
	}
	/**
	 * @return the totalAmountUsd
	 */
	public double getTotalAmountUsd() {
		return totalAmountUsd;
	}

	public String getTotalAmountUsdStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(totalAmountUsd, percentStr);
	}

	/**
	 * @param totalAmountUsd the totalAmountUsd to set
	 */
	public void setTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	/**
	 * @param totalAmountUsd the totalAmountUsd to set
	 */
	public void addTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd += totalAmountUsd;
	}
	
	public void addTotalAmountUsdMobile(double totalAmountUsdMobile) {
		this.totalAmountUsdMobile += totalAmountUsdMobile;
	}
	
	public void addTotalAmountUsdCopyop(double totalAmountUsdCopyop) {
		this.totalAmountUsdCopyop += totalAmountUsdCopyop;
	}
	
	public void addTotalAmountUsdCopyopMobile(double totalAmountUsdCopyopMobile) {
		this.totalAmountUsdCopyopMobile += totalAmountUsdCopyopMobile;
	}
	
	public void addTotalAmountUsdAOAll(double totalAmountUsdAOAll) {
		this.totalAmountUsdAOAll += totalAmountUsdAOAll;
	}
	
	public void addTotalAmountUsdCopyopAll(double totalAmountUsdCopyopAll) {
		this.totalAmountUsdCopyopAll += totalAmountUsdCopyopAll;
	}
	
	public void addTotalAmountUsdAll(double totalAmountUsdAll) {
		this.totalAmountUsdAll += totalAmountUsdAll;
	}
	/**
	 * @param firstDepsNum the firstDepsNum to set
	 */
	public void setFirstDepsNum(long firstDepsNum) {
		this.firstDepsNum = firstDepsNum;
	}
	/**
	 * @param regUsersNum the regUsersNum to set
	 */
	public void setRegUsersNum(long regUsersNum) {
		this.regUsersNum = regUsersNum;
	}

	/**
	 * @param firstDepsPercent the firstDepsPercent to set
	 */
	public void setFirstDepsPercent(double firstDepsPercent) {
		this.firstDepsPercent = firstDepsPercent;
	}

	/**
	 * @param regUsersPercent the regUsersPercent to set
	 */
	public void setRegUsersPercent(double regUsersPercent) {
		this.regUsersPercent = regUsersPercent;
	}

	public double getCountMobile() {
		return countMobile;
	}
	
	public String getCountMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == countMobile){
			return "";
		}
		return Utils.displayAmountRounded(countMobile, percentStr);
	}

	public void setCountMobile(double countMobile) {
		this.countMobile = countMobile;
	}

	public double getHeadcountMobile() {
		return headcountMobile;
	}
	
	public String getHeadcountMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountMobile){
			return "";
		}
		return Utils.displayAmountRounded(headcountMobile, percentStr);
	}

	public void setHeadcountMobile(double headcountMobile) {
		this.headcountMobile = headcountMobile;
	}
	

	public long getCountAOAll() {
		return (long) (count + countMobile);
	}
	
	public String getCountAOAllFormatted() {
		return Utils.displayAmountRounded(getCountAOAll(), "");
	}
	
	public String getCountAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == count){
			return "";
		}
		
		if (countAOAllpct > ConstantsBase.REPORT_EMPTY_VALUE){
			return getCountAOAllpctStr();
		}
		
		return String.valueOf(getCountAOAll()) + percentStr;
	}

	public double getHeadcountAOAll() {
		return headcountAOAll;
	}
	
	public String getHeadcountAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountAOAll){
			return "";
		}
		return Utils.displayAmountRounded(headcountAOAll, percentStr);
	}

	public void setHeadcountAOAll(double headcountAOAll) {
		this.headcountAOAll = headcountAOAll;
	}

	public double getCountCopyop() {
		return countCopyop;
	}
	
	public String getCountCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == countCopyop){
			return "";
		}
		return Utils.displayAmountRounded(countCopyop, percentStr);
	}

	public void setCountCopyop(double countCopyop) {
		this.countCopyop = countCopyop;
	}

	public double getHeadcountCopyop() {
		return headcountCopyop;
	}
	
	public String getHeadcountCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountCopyop){
			return "";
		}
		return Utils.displayAmountRounded(headcountCopyop, percentStr);
	}

	public void setHeadcountCopyop(double headcountCopyop) {
		this.headcountCopyop = headcountCopyop;
	}

	public double getCountCopyopMobile() {
		return countCopyopMobile;
	}
	
	public String getCountCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == countCopyopMobile){
			return "";
		}
		return Utils.displayAmountRounded(countCopyopMobile, percentStr);
	}

	public void setCountCopyopMobile(double countCopyopMobile) {
		this.countCopyopMobile = countCopyopMobile;
	}

	public double getHeadcountCopyopMobile() {
		return headcountCopyopMobile;
	}
	
	public String getHeadcountCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountCopyopMobile){
			return "";
		}
		return Utils.displayAmountRounded(headcountCopyopMobile, percentStr);
	}

	public void setHeadcountCopyopMobile(double headcountCopyopMobile) {
		this.headcountCopyopMobile = headcountCopyopMobile;
	}


	public long getCountCopyopAll() {
		return (long) (countCopyop + countCopyopMobile);
	}
	
	public String getCountCopyopAllFormatted() {
		 return Utils.displayAmountRounded(getCountCopyopAll(), "");
	}
	
	public String getCountCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == countCopyop){
			return "";
		}
		if (countCopyopAllpct > ConstantsBase.REPORT_EMPTY_VALUE){
			return getCountCopyopAllpctStr();
		}
		return String.valueOf(getCountCopyopAll()) + percentStr;
	}

	public double getHeadcountCopyopAll() {
		return headcountCopyopAll;
	}
	
	public String getHeadcountCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountCopyopAll){
			return "";
		}
		return Utils.displayAmountRounded(headcountCopyopAll, percentStr);
	}

	public void setHeadcountCopyopAll(double headcountCopyopAll) {
		this.headcountCopyopAll = headcountCopyopAll;
	}

	public long getCountAll() {
		return getCountAOAll() + getCountCopyopAll();
	}
	
	public String getCountAllFormatted() {
		return Utils.displayAmountRounded(getCountAll(), "");
	}
	
	public String getCountAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == count){
			return "";
		}
		if (countAllpct > ConstantsBase.REPORT_EMPTY_VALUE){
			return getCountAllpctStr();
		}
		return String.valueOf(getCountAll()) + percentStr;
	}

	public double getHeadcountAll() {
		return headcountAll;
	}
	
	public String getHeadcountAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcountAll){
			return "";
		}
		return Utils.displayAmountRounded(headcountAll, percentStr);
	}

	public void setHeadcountAll(double headcountAll) {
		this.headcountAll = headcountAll;
	}

	public double getTotalAmountUsdMobile() {
		return totalAmountUsdMobile;
	}
	
	public String getTotalAmountUsdMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(totalAmountUsdMobile, percentStr);
	}

	public void setTotalAmountUsdMobile(double totalAmountUsdMobile) {
		this.totalAmountUsdMobile = totalAmountUsdMobile;
	}
	
	public void setTotalAmountUsdAOAll(double totalAmountUsdAOAll) {
		this.totalAmountUsdAOAll = totalAmountUsdAOAll;
	}

	public double getTotalAmountUsdAOAll() {
		if(totalAmountUsdAOAll != 0){
			return totalAmountUsdAOAll; 
		} 
		return totalAmountUsd + totalAmountUsdMobile;		
	}
	
	public String getTotalAmountUsdAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(getTotalAmountUsdAOAll(), percentStr);
	}

	public double getTotalAmountUsdCopyop() {
		return totalAmountUsdCopyop;
	}
	
	public String getTotalAmountUsdCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(totalAmountUsdCopyop, percentStr);
	}

	public void setTotalAmountUsdCopyop(double totalAmountUsdCopyop) {
		this.totalAmountUsdCopyop = totalAmountUsdCopyop;
	}

	public double getTotalAmountUsdCopyopMobile() {
		return totalAmountUsdCopyopMobile;
	}
	
	public String getTotalAmountUsdCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(totalAmountUsdCopyopMobile, percentStr);
	}

	public void setTotalAmountUsdCopyopMobile(double totalAmountUsdCopyopMobile) {
		this.totalAmountUsdCopyopMobile = totalAmountUsdCopyopMobile;
	}
	
	public void setTotalAmountUsdCopyopAll(double totalAmountUsdCopyopAll) {
		this.totalAmountUsdCopyopAll = totalAmountUsdCopyopAll;
	}

	public double getTotalAmountUsdCopyopAll() {
		if(totalAmountUsdCopyopAll != 0){
			return totalAmountUsdCopyopAll;
		}
		return totalAmountUsdCopyop + totalAmountUsdCopyopMobile;
	}
	
	public String getTotalAmountUsdCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(getTotalAmountUsdCopyopAll(), percentStr);
	}
	
	public void setTotalAmountUsdAll(double totalAmountUsdAll) {
		this.totalAmountUsdAll = totalAmountUsdAll;
	}

	public double getTotalAmountUsdAll() {
		if(totalAmountUsdAll != 0){
			return totalAmountUsdAll;
		}
		return getTotalAmountUsdAOAll() + getTotalAmountUsdCopyopAll();
	}
	
	public String getTotalAmountUsdAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmountRounded(getTotalAmountUsdAll(), percentStr);
	}

	public double getProfitUsdMobile() {
		return profitUsdMobile;
	}

	public void setProfitUsdMobile(double profitUsdMobile) {
		this.profitUsdMobile = profitUsdMobile;
	}

	public double getProfitUsdAoAll() {
		return profitUsd + profitUsdMobile;
	}

	public double getProfitUsdCopyop() {
		return profitUsdCopyop;
	}

	public void setProfitUsdCopyop(double profitUsdCopyop) {
		this.profitUsdCopyop = profitUsdCopyop;
	}

	public double getProfitUsdCopyopMobile() {
		return profitUsdCopyopMobile;
	}

	public void setProfitUsdCopyopMobile(double profitUsdCopyopMobile) {
		this.profitUsdCopyopMobile = profitUsdCopyopMobile;
	}

	public double getProfitUsdCopyopAll() {
		return profitUsdCopyop + profitUsdCopyopMobile;
	}

	public double getProfitUsdAll() {
		return getProfitUsdAoAll() + getProfitUsdCopyopAll();
	}

	public long getRegUsersNumMobile() {
		return regUsersNumMobile;
	}
	
	public String getRegUsersNumMobileStr() {
		return Utils.displayAmountRounded(getRegUsersNumMobile(), "");
	}

	public void setRegUsersNumMobile(long regUsersNumMobile) {
		this.regUsersNumMobile = regUsersNumMobile;
	}

	public long getRegUsersNumCopyop() {
		return regUsersNumCopyop;
	}
	
	public String getRegUsersNumCopyopStr() {
		return Utils.displayAmountRounded(getRegUsersNumCopyop(), "");
	}

	public void setRegUsersNumCopyop(long regUsersNumCopyop) {
		this.regUsersNumCopyop = regUsersNumCopyop;
	}

	public long getRegUsersNumCopyopMobile() {
		return regUsersNumCopyopMobile;
	}
	
	public String getRegUsersNumCopyopMobileStr() {
		return Utils.displayAmountRounded(getRegUsersNumCopyopMobile(), "");
	}
	
	public long getRegUsersNumAOAll() {
		return regUsersNum + regUsersNumMobile;
	}
	
	public String getRegUsersNumAOAllStr() {
		return Utils.displayAmountRounded(getRegUsersNumAOAll(), "");
	}
	
	public long getRegUsersNumCopyopAll() {
		return regUsersNumCopyop + regUsersNumCopyopMobile;
	}
	
	public String getRegUsersNumCopyopAllStr() {
		return Utils.displayAmountRounded(getRegUsersNumCopyopAll(), "");
	}
	
	public long getRegUsersNumAll() {
		return getRegUsersNumAOAll() + getRegUsersNumCopyopAll();
	}
	
	public String getRegUsersNumAllStr() {
		return Utils.displayAmountRounded(getRegUsersNumAll(), "");
	}

	public void setRegUsersNumCopyopMobile(long regUsersNumCopyopMobile) {
		this.regUsersNumCopyopMobile = regUsersNumCopyopMobile;
	}

	public double getRegUsersPercentMobile() {
		return regUsersPercentMobile;
	}

	public void setRegUsersPercentMobile(double regUsersPercentMobile) {
		this.regUsersPercentMobile = regUsersPercentMobile;
	}

	public double getRegUsersPercentCopyop() {
		return regUsersPercentCopyop;
	}

	public void setRegUsersPercentCopyop(double regUsersPercentCopyop) {
		this.regUsersPercentCopyop = regUsersPercentCopyop;
	}

	public double getRegUsersPercentCopyopMobile() {
		return regUsersPercentCopyopMobile;
	}

	public void setRegUsersPercentCopyopMobile(double regUsersPercentCopyopMobile) {
		this.regUsersPercentCopyopMobile = regUsersPercentCopyopMobile;
	}

	public double getRegUsersPercentAOAll() {
		return regUsersPercentAOAll;
	}

	public void setRegUsersPercentAOAll(double regUsersPercentAOAll) {
		this.regUsersPercentAOAll = regUsersPercentAOAll;
	}

	public double getRegUsersPercentCopyopAll() {
		return regUsersPercentCopyopAll;
	}

	public void setRegUsersPercentCopyopAll(double regUsersPercentCopyopAll) {
		this.regUsersPercentCopyopAll = regUsersPercentCopyopAll;
	}

	public double getRegUsersPercentAll() {
		return regUsersPercentAll;
	}

	public void setRegUsersPercentAll(double regUsersPercentAll) {
		this.regUsersPercentAll = regUsersPercentAll;
	}

	public long getFirstDepsNumMobile() {
		return firstDepsNumMobile;
	}
	
	public String getFirstDepsNumMobileStr() {
		return Utils.displayAmountRounded(getFirstDepsNumMobile(), "");
	}

	public void setFirstDepsNumMobile(long firstDepsNumMobile) {
		this.firstDepsNumMobile = firstDepsNumMobile;
	}

	public long getFirstDepsNumCopyop() {
		return firstDepsNumCopyop;
	}
	
	public String getFirstDepsNumCopyopStr() {
		return Utils.displayAmountRounded(getFirstDepsNumCopyop(), "");
	}

	public void setFirstDepsNumCopyop(long firstDepsNumCopyop) {
		this.firstDepsNumCopyop = firstDepsNumCopyop;
	}

	public long getFirstDepsNumCopyopMobile() {
		return firstDepsNumCopyopMobile;
	}
	
	public String getFirstDepsNumCopyopMobileStr() {
		return Utils.displayAmountRounded(getFirstDepsNumCopyopMobile(), "");
	}
	
	public long getFirstDepsNumAOAll() {
		return firstDepsNum + firstDepsNumMobile;
	}
	
	public String getFirstDepsNumAOAllStr() {
		return Utils.displayAmountRounded(getFirstDepsNumAOAll(), "");
	}
	
	public long getFirstDepsNumCopyopAll() {
		return firstDepsNumCopyop + firstDepsNumCopyopMobile;
	}
	
	public String getFirstDepsNumCopyopAllStr() {
		return Utils.displayAmountRounded(getFirstDepsNumCopyopAll(), "");
	}
	
	public long getFirstDepsNumAll() {
		return getFirstDepsNumAOAll() + getFirstDepsNumCopyopAll();
	}
	
	public String getFirstDepsNumAllStr() {
		return Utils.displayAmountRounded(getFirstDepsNumAll(), "");
	}

	public void setFirstDepsNumCopyopMobile(long firstDepsNumCopyopMobile) {
		this.firstDepsNumCopyopMobile = firstDepsNumCopyopMobile;
	}

	public double getFirstDepsPercentMobile() {
		return firstDepsPercentMobile;
	}

	public void setFirstDepsPercentMobile(double firstDepsPercentMobile) {
		this.firstDepsPercentMobile = firstDepsPercentMobile;
	}

	public double getFirstDepsPercentCopyop() {
		return firstDepsPercentCopyop;
	}

	public void setFirstDepsPercentCopyop(double firstDepsPercentCopyop) {
		this.firstDepsPercentCopyop = firstDepsPercentCopyop;
	}

	public double getFirstDepsPercentCopyopMobile() {
		return firstDepsPercentCopyopMobile;
	}

	public void setFirstDepsPercentCopyopMobile(double firstDepsPercentCopyopMobile) {
		this.firstDepsPercentCopyopMobile = firstDepsPercentCopyopMobile;
	}

	public double getFirstDepsPercentAOAll() {
		return firstDepsPercentAOAll;
	}

	public void setFirstDepsPercentAOAll(double firstDepsPercentAOAll) {
		this.firstDepsPercentAOAll = firstDepsPercentAOAll;
	}

	public double getFirstDepsPercentCopyopAll() {
		return firstDepsPercentCopyopAll;
	}

	public void setFirstDepsPercentCopyopAll(double firstDepsPercentCopyopAll) {
		this.firstDepsPercentCopyopAll = firstDepsPercentCopyopAll;
	}

	public double getFirstDepsPercentAll() {
		return firstDepsPercentAll;
	}

	public void setFirstDepsPercentAll(double firstDepsPercentAll) {
		this.firstDepsPercentAll = firstDepsPercentAll;
	}

	public long getOutOfThisWeekFd() {
		return outOfThisWeekFd;
	}
	
	public String getOutOfThisWeekFdStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFd){
			return "";
		}
		return String.valueOf(outOfThisWeekFd);
	}

	public void setOutOfThisWeekFd(long outOfThisWeekFd) {
		this.outOfThisWeekFd = outOfThisWeekFd;
	}

	public long getOutOfThisWeekFdMobile() {
		return outOfThisWeekFdMobile;
	}
	
	public String getOutOfThisWeekFdMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFdMobile){
			return "";
		}
		return String.valueOf(outOfThisWeekFdMobile);
	}

	public void setOutOfThisWeekFdMobile(long outOfThisWeekFdMobile) {
		this.outOfThisWeekFdMobile = outOfThisWeekFdMobile;
	}

	public long getOutOfThisWeekFdCopyop() {
		return outOfThisWeekFdCopyop;
	}
	
	public String getOutOfThisWeekFdCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFdCopyop){
			return "";
		}
		return String.valueOf(outOfThisWeekFdCopyop);
	}

	public void setOutOfThisWeekFdCopyop(long outOfThisWeekFdCopyop) {
		this.outOfThisWeekFdCopyop = outOfThisWeekFdCopyop;
	}

	public long getOutOfThisWeekFdCopyopMobile() {
		return outOfThisWeekFdCopyopMobile;
	}
	
	public String getOutOfThisWeekFdCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFdCopyopMobile){
			return "";
		}
		return String.valueOf(outOfThisWeekFdCopyopMobile);
	}
	
	public String getOutOfThisWeekFdAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFd){
			return "";
		}
		return String.valueOf(outOfThisWeekFd + outOfThisWeekFdMobile);
	}
	
	public String getOutOfThisWeekFdCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFdCopyop){
			return "";
		}
		return String.valueOf(outOfThisWeekFdCopyop + outOfThisWeekFdCopyopMobile);
	}
	
	public String getOutOfThisWeekFdAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfThisWeekFd){
			return "";
		}
		return String.valueOf(outOfThisWeekFd + outOfThisWeekFdMobile + outOfThisWeekFdCopyop + outOfThisWeekFdCopyopMobile);
	}

	public void setOutOfThisWeekFdCopyopMobile(long outOfThisWeekFdCopyopMobile) {
		this.outOfThisWeekFdCopyopMobile = outOfThisWeekFdCopyopMobile;
	}

	public long getOutOfTotalFDOfEver() {
		return outOfTotalFDOfEver;
	}
	
	public String getOutOfTotalFDOfEverStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEver){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEver);
	}

	public void setOutOfTotalFDOfEver(long outOfTotalFDOfEver) {
		this.outOfTotalFDOfEver = outOfTotalFDOfEver;
	}

	public long getOutOfTotalFDOfEverMobile() {
		return outOfTotalFDOfEverMobile;
	}
	
	public String getOutOfTotalFDOfEverMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEverMobile){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEverMobile);
	}
	
	public void setOutOfTotalFDOfEverMobile(long outOfTotalFDOfEverMobile) {
		this.outOfTotalFDOfEverMobile = outOfTotalFDOfEverMobile;
	}

	public long getOutOfTotalFDOfEverCopyop() {
		return outOfTotalFDOfEverCopyop;
	}
	
	public String getOutOfTotalFDOfEverCopyopStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEverCopyop){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEverCopyop);
	}
	

	public void setOutOfTotalFDOfEverCopyop(long outOfTotalFDOfEverCopyop) {
		this.outOfTotalFDOfEverCopyop = outOfTotalFDOfEverCopyop;
	}

	public long getOutOfTotalFDOfEverCopyopMobile() {
		return outOfTotalFDOfEverCopyopMobile;
	}
	
	public String getOutOfTotalFDOfEverCopyopMobileStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEverCopyopMobile){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEverCopyopMobile);
	}

	public void setOutOfTotalFDOfEverCopyopMobile(
			long outOfTotalFDOfEverCopyopMobile) {
		this.outOfTotalFDOfEverCopyopMobile = outOfTotalFDOfEverCopyopMobile;
	}
	
	public String getOutOfTotalFDOfEverAOAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEver){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEver + outOfTotalFDOfEverMobile);
	}
	
	public String getOutOfTotalFDOfEverCopyopAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEverCopyop){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEverCopyop + outOfTotalFDOfEverCopyopMobile);
	}
	
	public String getOutOfTotalFDOfEverAllStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == outOfTotalFDOfEver){
			return "";
		}
		return String.valueOf(outOfTotalFDOfEver + outOfTotalFDOfEverMobile + outOfTotalFDOfEverCopyop + outOfTotalFDOfEverCopyopMobile);
	}

	public double getCountAOAllpct() {
		return countAOAllpct;
	}
	
	public String getCountAOAllpctStr() {
		return Utils.displayAmountRounded(countAOAllpct, "%");
	}

	public void setCountAOAllpct(double countAOAllpct) {
		this.countAOAllpct = countAOAllpct;
	}

	public double getCountCopyopAllpct() {
		return countCopyopAllpct;
	}
	
	public String getCountCopyopAllpctStr() {
		return Utils.displayAmountRounded(countCopyopAllpct, "%");
	}

	public void setCountCopyopAllpct(double countCopyopAllpct) {
		this.countCopyopAllpct = countCopyopAllpct;
	}

	public double getCountAllpct() {
		return countAllpct;
	}
	
	public String getCountAllpctStr() {
		return Utils.displayAmountRounded(countAllpct, "%");
	}

	public void setCountAllpct(double countAllpct) {
		this.countAllpct = countAllpct;
	}
}
