package daily.common;


import java.util.ArrayList;

/**
 * Skin vo class
 * @author Kobi
 *
 */
public class Skins implements java.io.Serializable {

	private int id;
	private String name;
	private String displayName;
    private ArrayList<SkinCurrencies> skinCurrenciesList;


	public Skins() {
		name = "";
		displayName = "";
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the skinCurrenciesList
	 */
	public ArrayList<SkinCurrencies> getSkinCurrenciesList() {
		return skinCurrenciesList;
	}

	/**
	 * @param skinCurrenciesList the skinCurrenciesList to set
	 */
	public void setSkinCurrenciesList(ArrayList<SkinCurrencies> skinCurrenciesList) {
		this.skinCurrenciesList = skinCurrenciesList;
	}


}
