package daily.common;

public class SecurityCheck {

	private String email;
	private long userId;
	private String username;
	private String password;
	private String ip;
	private String skin;
	public static final String IP_NOT_FOUND = "IP NOT FOUND!";
	public static final String USER_LIST_TYPE_NEW_DEPOSITERS = "NewDepositors";
	public static final String USER_LIST_TYPE_NEW_USERS = "NewUsers";
	public static final String USER_LIST_TYPE_OLD_USERS = "OldUsers";
	public static final String USER_LIST_TYPE_BLACK_LIST = "BlackList";
	public static final String USER_LIST_TYPE_NEW_ACCOUNT = "New Account";

	public static final int USER_LIST_TYPE_NEW_USERS_ID = 1;
	public static final int USER_LIST_TYPE_OLD_USERS_ID  = 2;
	public static final int USER_LIST_TYPE_BLACK_LIST_ID  = 3;

	private String userName1;
	private String userName2;
	private String password1;
	private String password2;
	private String skin1;
	private String skin2;
	private long userId2;
	private String userListType;
	private String matching;
	private String phone;

	private String firstName;
	private String lastName;
	// credit card fields
	private String ccHolderName;
	private long ccId;
	private String ccLast4Digits;
	private String ccNumber;
	private String userCountryName;
	private String ipCountryName;
	private String ccCountryName;
	private boolean isCcAllowed;
	private long depositsNum;

	private String succeedDeposit;
	
	private String ccTypeDescription;
	
	private String fromAccount;

	//	for testing
	private int distance;
	private int equalCharacters;
	private String prec;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the userName1
	 */
	public String getUserName1() {
		return userName1;
	}

	/**
	 * @param userName1 the userName1 to set
	 */
	public void setUserName1(String userName1) {
		this.userName1 = userName1;
	}

	/**
	 * @return the userName2
	 */
	public String getUserName2() {
		return userName2;
	}

	/**
	 * @param userName2 the userName2 to set
	 */
	public void setUserName2(String userName2) {
		this.userName2 = userName2;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the distance
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * @return the equalCharacters
	 */
	public int getEqualCharacters() {
		return equalCharacters;
	}

	/**
	 * @param equalCharacters the equalCharacters to set
	 */
	public void setEqualCharacters(int equalCharacters) {
		this.equalCharacters = equalCharacters;
	}

	/**
	 * @return the prec
	 */
	public String getPrec() {
		return prec;
	}

	/**
	 * @param prec the prec to set
	 */
	public void setPrec(String prec) {
		this.prec = prec;
	}

	/**
	 * @return the password1
	 */
	public String getPassword1() {
		return password1;
	}

	/**
	 * @param password1 the password1 to set
	 */
	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	/**
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @param password2 the password2 to set
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the matching
	 */
	public String getMatching() {
		return matching;
	}

	/**
	 * @param matching the matching to set
	 */
	public void setMatching(String matching) {
		this.matching = matching;
	}

	/**
	 * @return the ccHolderName
	 */
	public String getCcHolderName() {
		return ccHolderName;
	}

	/**
	 * @param ccHolderName the ccHolderName to set
	 */
	public void setCcHolderName(String ccHolderName) {
		this.ccHolderName = ccHolderName;
	}

	/**
	 * @return the ccId
	 */
	public long getCcId() {
		return ccId;
	}

	/**
	 * @param ccId the ccId to set
	 */
	public void setCcId(long ccId) {
		this.ccId = ccId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the skin
	 */
	public String getSkin() {
		return skin;
	}

	/**
	 * @param skin the skin to set
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * @return the userListType
	 */
	public String getUserListType() {
		return userListType;
	}

	/**
	 * @param userListType the userListType to set
	 */
	public void setUserListType(String userListType) {
		this.userListType = userListType;
	}

	/**
	 * @return the userId2
	 */
	public long getUserId2() {
		return userId2;
	}

	/**
	 * @param userId2 the userId2 to set
	 */
	public void setUserId2(long userId2) {
		this.userId2 = userId2;
	}

	/**
	 * @return the ccCountryName
	 */
	public String getCcCountryName() {
		return ccCountryName;
	}

	/**
	 * @param ccCountryName the ccCountryName to set
	 */
	public void setCcCountryName(String ccCountryName) {
		this.ccCountryName = ccCountryName;
	}

	/**
	 * @return the ipCountryName
	 */
	public String getIpCountryName() {
		return ipCountryName;
	}

	/**
	 * @param ipCountryName the ipCountryName to set
	 */
	public void setIpCountryName(String ipCountryName) {
		this.ipCountryName = ipCountryName;
	}

	/**
	 * @return the userCountryName
	 */
	public String getUserCountryName() {
		return userCountryName;
	}

	/**
	 * @param userCountryName the userCountryName to set
	 */
	public void setUserCountryName(String userCountryName) {
		this.userCountryName = userCountryName;
	}

	/**
	 * @return the depositsNum
	 */
	public long getDepositsNum() {
		return depositsNum;
	}

	/**
	 * @param depositsNum the depositsNum to set
	 */
	public void setDepositsNum(long depositsNum) {
		this.depositsNum = depositsNum;
	}

	/**
	 * @return the isCcAllowed
	 */
	public boolean isCcAllowed() {
		return isCcAllowed;
	}

	/**
	 * @param isCcAllowed the isCcAllowed to set
	 */
	public void setCcAllowed(boolean isCcAllowed) {
		this.isCcAllowed = isCcAllowed;
	}


	public String getCcAllowed() {
		if (isCcAllowed){
			return "Yes";
		}
		return "No";
	}

	public String getDepositsYesNo() {
		if (depositsNum > 0 ){
			return "Yes";
		}
		return "No";
	}

	/**
	 * @return the ccLast4Digits
	 */
	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	/**
	 * @param ccLast4Digits the ccLast4Digits to set
	 */
	public void setCcLast4Digits(String ccLast4Digits) {
		this.ccLast4Digits = ccLast4Digits;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the succeedDeposit
	 */
	public String getSucceedDeposit() {
		return succeedDeposit;
	}

	/**
	 * @param succeedDeposit the succeedDeposit to set
	 */
	public void setSucceedDeposit(String succeedDeposit) {
		this.succeedDeposit = succeedDeposit;
	}

	public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}

	/**
	 * @return the skin1
	 */
	public String getSkin1() {
		return skin1;
	}

	/**
	 * @param skin1 the skin1 to set
	 */
	public void setSkin1(String skin1) {
		this.skin1 = skin1;
	}

	/**
	 * @return the skin2
	 */
	public String getSkin2() {
		return skin2;
	}

	/**
	 * @param skin2 the skin2 to set
	 */
	public void setSkin2(String skin2) {
		this.skin2 = skin2;
	}

	/**
	 * @return the ccTypeDescription
	 */
	public String getCcTypeDescription() {
		return ccTypeDescription;
	}

	/**
	 * @param ccTypeDescription the ccTypeDescription to set
	 */
	public void setCcTypeDescription(String ccTypeDescription) {
		this.ccTypeDescription = ccTypeDescription;
	}

	/**
	 * @return the fromAccount
	 */
	public String getFromAccount() {
		return fromAccount;
	}

	/**
	 * @param fromAccount the fromAccount to set
	 */
	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}
}
