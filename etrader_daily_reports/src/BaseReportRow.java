/*import il.co.etrader.util.ConstantsBase;

public class BaseReportRow {

	private String rowName;
	private double totalAmountUsd;
	private long count;
	private long headcount;
	private double profitUsd;
	private long regUsersNum;
	private long regUsersPercent;
	private long firstDepsNum;
	private long firstDepsPercent;
	private String percentStr;
	private String style;

	public BaseReportRow() {
		percentStr = "";
		style = "";
	}

	public void setPercentStr() {
		percentStr = "%";
	}

	public void setBold() {
		style = "font-weight:bold;";
	}

	*//**
	 * @return the style
	 *//*
	public String getStyle() {
		return style;
	}

	*//**
	 * @return the count
	 *//*
	public long getCount() {
		return count;
	}

	public String getCountStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == count){
			return "";
		}
		return String.valueOf(count) + percentStr;
	}
	*//**
	 * @param count the count to set
	 *//*
	public void setCount(long count) {
		this.count = count;
	}
	*//**
	 * @return the firstDepsNum
	 *//*
	public long getFirstDepsNum() {
		return firstDepsNum;
	}
	*//**
	 * @param firstDepsNum the firstDepsNum to set
	 *//*
	public void setFirstDepsNumAndPercent(long firstDepsNum,long totalFirstDeps) {
		this.firstDepsNum = firstDepsNum;

		if (firstDepsNum > 0){
			this.firstDepsPercent = (long)Math.round((new Double(firstDepsNum)*100/totalFirstDeps));
		}
	}
	*//**
	 * @return the firstDepsPercent
	 *//*
	public long getFirstDepsPercent() {
		return firstDepsPercent;
	}

	public String getFirstDepsPercentStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == firstDepsPercent){
			return "";
		}
		return String.valueOf(firstDepsPercent) + "%";
	}
	*//**
	 * @return the headcount
	 *//*
	public long getHeadcount() {
		return headcount;
	}

	*//**
	 * @return the headcount
	 *//*
	public String getHeadcountStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == headcount){
			return "";
		}
		return String.valueOf(headcount) + percentStr;
	}

	*//**
	 * @param headcount the headcount to set
	 *//*
	public void setHeadcount(long headcount) {
		this.headcount = headcount;
	}
	*//**
	 * @param headcount the headcount to set
	 *//*
	public void addHeadcount(long headcount) {
		this.headcount += headcount;
	}
	*//**
	 * @return the profitUsd
	 *//*
	public double getProfitUsd() {
		return profitUsd;
	}
	*//**
	 * @param profitUsd the profitUsd to set
	 *//*
	public void setProfitUsd(double profitUsd) {
		this.profitUsd = profitUsd;
	}

	public String getProfitUsdStr() {
		return Utils.displayAmount(profitUsd);
	}

	*//**
	 * @return the regUsersNum
	 *//*
	public long getRegUsersNum() {
		return regUsersNum;
	}
	*//**
	 * @param regUsersNum the regUsersNum to set
	 *//*
	public void setRegUsersNumAndPercent(long regUsersNum,long totalReg) {
		this.regUsersNum = regUsersNum;

		if (regUsersNum > 0){
			this.regUsersPercent = (long)Math.round((new Double(regUsersNum)*100/totalReg));
		}
	}
	*//**
	 * @return the regUsersPercent
	 *//*
	public long getRegUsersPercent() {
		return regUsersPercent;
	}

	public String getRegUsersPercentStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == regUsersPercent){
			return "";
		}
		return String.valueOf(regUsersPercent)+ "%";
	}

	*//**
	 * @return the rowName
	 *//*
	public String getRowName() {
		return rowName;
	}
	*//**
	 * @param rowName the rowName to set
	 *//*
	public void setRowName(String rowName) {
		this.rowName = rowName;
	}
	*//**
	 * @return the totalAmountUsd
	 *//*
	public double getTotalAmountUsd() {
		return totalAmountUsd;
	}

	public String getTotalAmountUsdStr() {
		if (ConstantsBase.REPORT_EMPTY_VALUE == totalAmountUsd){
			return "";
		}
		return Utils.displayAmount(totalAmountUsd) + percentStr;
	}

	*//**
	 * @param totalAmountUsd the totalAmountUsd to set
	 *//*
	public void setTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	*//**
	 * @param totalAmountUsd the totalAmountUsd to set
	 *//*
	public void addTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd += totalAmountUsd;
	}
	*//**
	 * @param firstDepsNum the firstDepsNum to set
	 *//*
	public void setFirstDepsNum(long firstDepsNum) {
		this.firstDepsNum = firstDepsNum;
	}
	*//**
	 * @param regUsersNum the regUsersNum to set
	 *//*
	public void setRegUsersNum(long regUsersNum) {
		this.regUsersNum = regUsersNum;
	}

	*//**
	 * @param firstDepsPercent the firstDepsPercent to set
	 *//*
	public void setFirstDepsPercent(long firstDepsPercent) {
		this.firstDepsPercent = firstDepsPercent;
	}

	*//**
	 * @param regUsersPercent the regUsersPercent to set
	 *//*
	public void setRegUsersPercent(long regUsersPercent) {
		this.regUsersPercent = regUsersPercent;
	}

}
*/