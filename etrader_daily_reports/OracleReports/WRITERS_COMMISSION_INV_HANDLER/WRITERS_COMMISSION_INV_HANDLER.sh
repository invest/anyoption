#!/bin/bash
#
#  this script will run procedure populations_handler with an argument

# Get the argumet into var
#myvar=$1

cd /usr/local/etrader-jobs/WRITERS_COMMISSION_INV_HANDLER

## Check that we are the only one running

if [ -f run.pd ];then
	echo "process is running"
	exit 1
else
touch run.pd
file=WRITERS_COMMISSION_INV_HANDLER-`date +%F`.log
export TNS_ADMIN=/usr/share/tomcat-7.0.12/conf/
/opt/oracle-client/11g/sqlplus -s "etrader_web/W3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI:SS' ;
SET SERVEROUTPUT ON ;
spool $file append
EXEC PKG_WRITERS_COMMISSION_INV.PRC_WRITERS_COMMISSION_INV() ;
spool off
exit
eof1
find . -type f -ctime +15 -name "*.log" -exec rm -i {} \;
rm -f run.pd

fi
