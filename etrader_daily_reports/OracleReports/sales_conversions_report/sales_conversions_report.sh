#!/bin/bash
#

cd /usr/local/etrader-jobs/sales_conversions_report


/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
set pages5000
SET MARKUP HTML ON SPOOL ON
spool sales_conversions_report.xls
@sales_conversions_report.sql
spool off
exit
eof1


echo "Attached report"|mutt -s "Sales conversions `date +%F -d yesterday`" -a sales_conversions_report.xls omrik@anyoption.com,shiftmanagers@anyoption.com

