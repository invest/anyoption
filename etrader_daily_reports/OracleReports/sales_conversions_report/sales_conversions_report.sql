select b.rep as "Sales represantative",
b.month_dep as "Deposited amount (USD) monthly",
b.month_conv as "# of conversions monthly",
b.month_avg as "AVG deposited monthly",
a.day_dep as "deposited amount",
a.day_conv as "# of conversions",
a.day_avg as "Avarage of conversions"
from
(select   w.user_name as rep,
w.id,
sum(t.amount/100*t.rate) as month_dep, 
count(distinct (u.id)) as month_conv, 
round(sum(t.amount/100*t.rate)/count(distinct (u.id)),0) month_avg
from users u, transactions t, transactions_issues ti,  issue_actions ia, writers w
where u.first_deposit_id=t.id
and t.id=ti.transaction_id
and ti.issue_action_id=ia.id
and ia.writer_id=w.id
and u.class_id<>0
and to_char(t.time_created, 'yyyymm')=to_char(sysdate,'yyyymm')
and to_char(t.time_created,'yyyymmdd')<to_char(sysdate,'yyyymmdd')
and u.id not in
(select distinct(u.id)
from users u, transactions t, transaction_types tt
where  t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id =2
and u.id not in (select  distinct(u.id)
from users u, investments i
where i.user_id=u.id
and i.is_canceled=0))
group by w.user_name, w.id)b
left join 
( select w.id ,
sum(t.amount/100*t.rate) as day_dep, 
count(distinct (u.id)) as day_conv, 
round(sum(t.amount/100*t.rate)/count(distinct (u.id)),0) day_avg
from users u, transactions t, transactions_issues ti, writers w, issue_actions ia
where u.first_deposit_id=t.id
and t.id=ti.transaction_id
and ti.issue_action_id=ia.id
and ia.writer_id=w.id
and u.class_id<>0
and to_char(t.time_created, 'yyyymmdd')=to_char(sysdate-1,'yyyymmdd')
and u.id not in
(select distinct(u.id)
from users u, transactions t, transaction_types tt
where  t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id =2
and u.id not in (select  distinct(u.id)
from users u, investments i
where i.user_id=u.id
and i.is_canceled=0))
group by w.id ) a on b.id=a.id ;
