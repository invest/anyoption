#!/bin/bash
#
#  this script will run procedure populations_handler with an argument

# Get the argumet into var
myvar=$1

export TNS_ADMIN=/usr/share/tomcat7/conf/
cd /usr/local/etrader-jobs/Special-Care/

/opt/oracle-client/11g/sqlplus -s " etrader_web/w3tr4d3r@DBLIVE " <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI:SS' ;
set pages100
set line200
spool Special-Care.out append
@/usr/local/etrader-jobs/Special-Care/Special-Care.sql
spool off
exit
eof1
