-- reason 1
update users u
set u.time_sc_turnover = sysdate
where
   u.class_id<>0
   and u.is_active=1
   and u.time_sc_turnover is null
   and u.first_deposit_id is not null
   and exists(    
            select 
              i.user_id, 
              sum(i.amount/100*i.rate) as turnover, 
              count(i.id) as num_ivest , 
              sum(i.house_result/100*i.rate) as profit
            from 
              investments i, 
              transactions t
            where i.user_id=u.id
              and u.first_deposit_id= t.id
              and i.is_settled=1
              and i.is_canceled<>1
              and i.time_settled between t.time_created and t.time_created + 14
            group by 
              i.user_id
            having 
              sum(i.amount/100*i.rate) >= (case when u.skin_id = 1 then 9860 else 14281 end)
              and count(i.id)>=79) ;

-- reason 2
update users u
set u.time_sc_house_result = sysdate
where
  u.class_id<>0
  and u.is_active=1
  and u.time_sc_house_result is null
  and 10000 <= (select sum(i.house_result/100*i.rate)
                from investments i
                where i.user_id= u.id
                  and i.is_settled=1
                  and i.is_canceled=0) ;

