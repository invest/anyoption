----------------------
-- Deprecated !!!
----------------------

#!/bin/bash
#
#  this script will run procedure populations_manual with an argument

# Get the argumet into var
#myvar=$1

cd /usr/local/etrader-jobs/WRITERS_COMMISSION_DEP_MANUAL

## Check that we are the only one running

export TNS_ADMIN=/usr/share/tomcat-7.0.12/conf/
/opt/oracle-client/11g/sqlplus -s "etrader_web/w3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI:SS' ;
SET SERVEROUTPUT ON ;
spool WRITERS_COMMISSION_DEP_MANUAL-`date +%F`.log append
EXEC WRITERS_COMMISSION_DEP_MANUAL() ;
spool off
exit
eof1
find . -type f -name '*.log' -ctime +14 -exec rm -f {} \;
