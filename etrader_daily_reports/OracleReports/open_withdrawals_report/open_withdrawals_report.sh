#!/bin/bash
#
# This script get the list of unsubscribed users from ActiveCampaign 
# and update Oracle database to set contact_by_email=0 .

# first lets get the list 
cd /usr/local/etrader-jobs/open_withdrawals_report



/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='DD-MON-YYYY HH24:MI' ;
SET MARKUP HTML ON
spool open_withdrawals_report.xls
@/usr/local/etrader-jobs/open_withdrawals_report/open_withdrawals_report.sql
exit
eof1


echo "Attached log" | mutt -a open_withdrawals_report.xls -s "open withdrawals `date +'%F %T'`" reutg@etrader.co.il,yohis@anyoption.com,Omrik@anyoption.com,shiftmanagers@anyoption.com

