(select e.*, j.sum_inv
from
(select  i.user_id, sum(i.amount/100) as sum_inv
from investments i,
(select f.*, t.id as dep_ID, t.amount/100 as dep_amount, t.time_created as deposit_date
from transactions t,
(select a.id,  max (t.id) as max
from transactions t, transaction_types tt,
(select t.time_created, u.id, t.id as tx
from users u, transactions t,  transaction_types tt
where t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)a
where t.user_id=a.id
and t.time_created<a.time_created
and t.type_id=tt.id
and tt.class_type=1
and t.status_id in (2,7,8)
group by  a.id)b,
(select u.id as user_ID,t.time_created as withd_date, t.id as withd_id, t.amount/100 as withd_amount
from users u, transactions t,  transaction_types tt
where t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)f
where t.id=b.max
and f.user_Id=b.id)g
where g.user_id=i.user_id
and i.is_canceled=0
and i.time_created>g.deposit_date 
group by i.user_id)j,
(select f.*, t.id as dep_ID, case when ti.transaction_id is null then 0 else 1 end as "Sales deposit", t.amount/100 as dep_amount, t.time_created as deposit_date
from 
(select a.id,  max (t.id) as max
from transactions t, transaction_types tt,
(select t.time_created, u.id, t.id as tx
from users u, transactions t,  transaction_types tt
where t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)a
where t.user_id=a.id
and t.time_created<a.time_created
and t.type_id=tt.id
and tt.class_type=1
and t.status_id in (2,7,8)
group by  a.id)b,
(select u.id as user_ID, u.user_name, s.name as "Skin", t.time_created as withd_date, t.id as withd_id, t.amount/100 as withd_amount
from users u, transactions t,  transaction_types tt, skins s
where t.user_id=u.id
and t.type_id=tt.id
and u.skin_id=s.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)f,
 transactions t 
 left join transactions_issues ti on t.id=ti.transaction_id
where t.id=b.max
and f.user_Id=b.id)e
where j.user_id=e.user_id
and j.sum_inv<e.dep_amount)
union
(select f.*, t.id as dep_ID, case when ti.transaction_id is null then 0 else 1 end as "Sales deposit",  t.amount/100 as dep_amount, t.time_created as deposit_date, u.sc_updated_by as sum_inv
from  users u,
(select a.id,  max (t.id) as max
from transactions t, transaction_types tt,
(select t.time_created, u.id, t.id as tx
from users u, transactions t,  transaction_types tt
where t.user_id=u.id
and t.type_id=tt.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)a
where t.user_id=a.id
and t.time_created<a.time_created
and t.type_id=tt.id
and tt.class_type=1
and t.status_id in (2,7,8)
group by  a.id)b,
(select u.id as user_ID, u.user_name, s.name as "Skin", t.time_created as withd_date, t.id as withd_id, t.amount/100 as withd_amount
from users u, transactions t,  transaction_types tt, skins s
where t.user_id=u.id
and t.type_id=tt.id
and u.skin_id=s.id
and u.class_id<>0
and tt.class_type=2
and t.status_id in (4,9)
and t.time_created>sysdate-12/24)f,
transactions t
 left join transactions_issues ti on t.id=ti.transaction_id
where t.id=b.max
and f.user_Id=b.id
and f.user_id=u.id
and f.user_id not in (select i.user_id
from investments i
where i.is_canceled=0));
