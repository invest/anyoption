#!/bin/bash
#
# This script get the list of unsubscribed users from ActiveCampaign 
# and update Oracle database to set contact_by_email=0 .

# first lets get the list 
cd /usr/local/etrader-jobs/google_click_id



/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
set pages0
set line80
col  GCLID FORMAT A50
col date FORMAT A20
spool google_click_id.csv
@google_click_id.sql
spool off
exit
eof1


## remove unwanted lines and spaces ##
sed -i -e 's/ //g' -e 's/.*rowsselected\.//g' -e '/^$/d' -e 's/,/,ATGUCJG_oAIQ19qt2wM,300,/g'  google_click_id.csv
sed -i "1i\\${n}gclid,label,value,date " google_click_id.csv


mv google_click_id.csv 699-873-4978_`date +%Y%m%d`.txt

####  here comes the part of the FTP ... one we have the details 

HOST=uploads.google.com
FTPTEMPLOG=google_click_id.log
USER=conversion-feeds
PASSWD=t62N2Ax
FILE1=699-873-4978_`date +%Y%m%d`.txt

## clear log 
echo "">$FTPTEMPLOG

ftp -n -vvv $HOST <<END_SCRIPT >>$FTPTEMPLOG
quote USER $USER
quote PASS $PASSWD
put $FILE1
quit
END_SCRIPT


###  End FTP  ####

echo "Attached google_click_id output"|mutt -a 699-873-4978_`date +%Y%m%d`.txt -a google_click_id.log -s "google_click_id"  nataliag@etrader.co.il
rm -vf 699-873-4978_`date +%Y%m%d`.txt
