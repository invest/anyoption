select *
from 
(select ms.name Campaign , count(u.id) First_dep
from marketing_sources ms, marketing_campaigns mc, users u, marketing_combinations mco,transactions t
where u.combination_id=mco.id
and mco.campaign_id=mc.id
and mc.source_id=ms.id
and u.first_deposit_id=t.id
and u.class_id<>0
and to_char(t.time_created,'yyyymmdd')=to_char(sysdate-1,'yyyymmdd')
and u.skin_id <>1
and  ms.id in (28,25,316,341,105,333,534,320,591) 
group by ms.name) 
UNION ALL
(select 'Google_ET' AS  Campaign, count(u.id) First_dep
from marketing_sources ms, marketing_campaigns mc, users u, marketing_combinations mco,transactions t
where u.combination_id=mco.id
and mco.campaign_id=mc.id
and mc.source_id=ms.id
and u.first_deposit_id=t.id
and u.class_id<>0
and to_char(t.time_created,'yyyymmdd')=to_char(sysdate-1,'yyyymmdd')
and u.skin_id =1
and  ms.id in (25)  
group by ms.name) ;
