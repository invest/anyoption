select ff.*
from
((select c.id as "Contact_ID", c.email, c.mobile_phone,
c.time_created as "Contact created",ia.action_time as Time_rejected, substr(iat.name,14) as "Reason of rejection"
FROM population_entries pe, populations p, population_users pu, 
contacts c , issues i, issue_actions ia  , issue_action_types iat, marketing_campaigns mc, marketing_combinations mcom
WHERE pe.population_id = p.id       
AND pe.population_users_id = pu.id       
and pu.contact_id=c.id
and i.population_entry_id=pe.id
and c.combination_id=mcom.id
and mcom.campaign_id=mc.id
and mc.source_id=345
and ia.issue_id=i.id
and ia.issue_action_type_id=iat.id
and iat.id in (19,23,30,33)
AND p.population_type_id in (20) 
AND p.skin_id =1 )
union
(select f.contact_id as "Contact_ID", f.email, f.mobile_phone,f.time_created as "Contact created",
f.action_time as Time_rejected, '4th unreached call'   as "Reason of rejection"
from
(select *
from 
(select pe.id as lead_id, c.id as contact_id, c.email, c.mobile_phone, ia.action_time, c.time_created,
ROW_NUMBER ()OVER (PARTITION BY c.id ORDER BY  (ia.action_time ) asc) rnum  
FROM population_entries pe, populations p, population_users pu, 
contacts c , issues i, issue_actions ia  , issue_action_types iat, marketing_campaigns mc,
marketing_combinations mcom,  writers w
WHERE pe.population_id = p.id       
AND pe.population_users_id = pu.id       
and pu.contact_id=c.id
and i.population_entry_id=pe.id
and ia.issue_id=i.id
and c.combination_id=mcom.id
and ia.issue_action_type_id= iat.id 
and mcom.campaign_id=mc.id
 AND ia.writer_id = w.id  
 and mc.source_id=345
and iat.reached_status_id<>2
and iat.channel_id=1 
AND w.dept_id = 3  
AND p.population_type_id in (20) 
AND p.skin_id =1 )
WHERE rnum = 4)f))ff
where to_char(ff.time_rejected, 'yyyymmdd')=to_char(sysdate-1,'yyyymmdd') ;
