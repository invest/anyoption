#!/bin/bash
#

cd /usr/local/etrader-jobs/Reports/Closed_accounts_from_affiliates_in_the_last_month


/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
set pages5000
ALTER SESSION SET NLS_DATE_FORMAT='DD-MON-YYYY HH24:MI' ;
SET MARKUP HTML ON SPOOL ON
spool Closed_accounts_from_affiliates_in_the_last_month.xls
@Closed_accounts_from_affiliates_in_the_last_month.sql
spool off
exit
eof1


echo "Attached report"|mutt -s "Closed_accounts_from_affiliates `date +%B`" -a Closed_accounts_from_affiliates_in_the_last_month.xls  elliot@anyoption.com,ilanitr@etrader.co.il


