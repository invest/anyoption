select a.id "User id",a.affiliate_key "Affiliate id" ,
a.name "Reason for close",a.time_created " Registration date" 
,a.action_time "Date of closing account",a.time_created2 "First deposit date",
a.t_usd "First deposit amount USD", a.t "First deposit amount",sum((i.amount/100)*i.rate) "Turnover USD"
from (
select u.id,u.affiliate_key  ,iat.name ,u.time_created,ia.action_time
,t.time_created time_created2,t.amount/100*t.rate t_usd, t.amount/100 t
from users u , transactions t 
,issues i ,issue_actions ia,issue_action_types iat 
where u.id=i.user_id 
and i.id=ia.issue_id 
and u.first_deposit_id=t.id
and ia.issue_action_type_id=iat.id 
and u.affiliate_system_user_time is not null 
and to_char(ia.action_time,'yyyymm')=to_char(sysdate -1,'yyyymm')
and u.class_id<>0 
and u.is_active=0 
and iat.id in (9,10,1,11) ) a
left join 
investments i 
on a.id=i.user_id
and i.is_settled=1
and i.is_canceled=0
Group by  a.id ,a.affiliate_key  ,
a.name ,a.time_created  
,a.action_time ,a.time_created2 ,
a.t_usd , a.t ;
