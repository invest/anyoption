SELECT distinct u.id "User id",u.user_name "Registration date",numb.number_of, ---- Number_of_rejections_on_D3_in_the_past_week,
case when s.name='English USD' then 'English'  
 when s.name='German EUR' then 'German'  
 when s.name='Hebrew ILS' then 'etrader' 
 when s.name='Turkish TRY' then 'Turkish' 
 when s.name='Spanish EUR' then 'Spanish' 
 else s.name 
end as "Skin"
,a.time_created, b.turnover "Turnover after reject",b.profit  "Profit after reject", round((b.profit/b.turnover*100),2) | | '%' "%profit after reject"
, c.turnover "Turnover befor reject",c.profit  "Profit befor reject", round((c.profit/c.turnover*100),2) | | '%' "%profit befor reject"
,d.turnover "Total turnover", d.profit "Total profit",  round((d.profit/d.turnover*100),2) | | '%' " Total %profit "
from users u
left join (sELECT count(DISTINCT ir.id) as number_of, ir.user_id
FROM investment_rejects ir, users u, investment_reject_types irt, opportunities o, markets m, investment_types it  
WHERE ir.user_id = u.id           
AND ir.reject_type_id = irt.id           
AND ir.opportunity_id = o.id           
AND o.market_id = m.id           
AND ir.type_id = it.id           
AND u.class_id <> 0          
and ir.reject_type_id=11
and to_char(ir.time_created, 'yyyymmdd')<to_char(sysdate,'yyyymmdd')
and to_char(ir.time_created, 'yyyymmdd')>=to_char(sysdate-7,'yyyymmdd')
group by ir.user_id
) numb
on u.id=numb.user_id
left join (
select aa.user_id,ir.time_created 
from ( 
select min(ir.id) min, ir.user_id, u.user_name, u.skin_id          
FROM investment_rejects ir, users u, investment_reject_types irt, opportunities o, markets m, investment_types it 
WHERE ir.user_id = u.id               
AND ir.reject_type_id = irt.id               
AND ir.opportunity_id = o.id               
AND o.market_id = m.id 
AND ir.type_id = it.id               
AND u.class_id <> 0      
and ir.reject_type_id=11
and u.id in (SELECT u.id
FROM user_market_disable UMD,users u
where umd.user_id=u.id
and umd.is_active=1
and u.class_id<>0
and umd.is_dev3=1)
AND ir.time_created < sysdate            
group by  ir.user_id, u.user_name, u.skin_id   ) aa,investment_rejects ir
where ir.id=aa.min) a on u.id=a.user_id
left join
(Select aa.user_id, aa.user_name, aa.skin_id ,ir.time_created ,sum((i.amount/100)*i.rate) turnover, sum((i.house_result/100)*i.rate) profit
from (       
select min(ir.id) min, ir.user_id, u.user_name, u.skin_id   
FROM investment_rejects ir, users u, investment_reject_types irt, opportunities o, markets m, investment_types it 
WHERE ir.user_id = u.id               
AND ir.reject_type_id = irt.id               
AND ir.opportunity_id = o.id               
AND o.market_id = m.id
AND ir.type_id = it.id               
AND u.class_id <> 0      
and ir.reject_type_id=11
and u.id in (SELECT u.id
FROM user_market_disable UMD,users u
where umd.user_id=u.id
and umd.is_active=1
and u.class_id<>0
and umd.is_dev3=1)
AND ir.time_created< sysdate            
group by  ir.user_id, u.user_name, u.skin_id   ) aa,investment_rejects ir, investments i
where ir.id=aa.min
and i.user_id=aa.user_id
and i.time_created>ir.time_created
and i.is_canceled=0
and i.is_settled=1
AND i.time_created < sysdate      
group by  aa.min, aa.user_id, aa.user_name, aa.skin_id ,ir.time_created)b
on u.id=b.user_id
left join
(select aa.user_id, aa.user_name, aa.skin_id ,ir.time_created ,sum((i.amount/100)*i.rate) turnover, sum((i.house_result/100)*i.rate) profit
from (              
select min(ir.id) min, ir.user_id, u.user_name, u.skin_id          
FROM investment_rejects ir, users u, investment_reject_types irt, opportunities o, markets m, investment_types it
WHERE ir.user_id = u.id               
AND ir.reject_type_id = irt.id               
AND ir.opportunity_id = o.id               
AND o.market_id = m.id
AND ir.type_id = it.id               
AND u.class_id <> 0      
and ir.reject_type_id=11
and u.id in(SELECT u.id
FROM user_market_disable UMD,users u
where umd.user_id=u.id
and umd.is_active=1
and u.class_id<>0
and umd.is_dev3=1)
AND ir.time_created <sysdate                
group by  ir.user_id, u.user_name, u.skin_id   ) aa,investment_rejects ir, investments i
where ir.id=aa.min
and i.user_id=aa.user_id
and i.time_created<ir.time_created
and i.is_canceled=0
and i.is_settled=1 
group by  aa.min, aa.user_id, aa.user_name, aa.skin_id ,ir.time_created)c
on u.id=c.user_id
left join
(sELECT u.id,u.skin_id,sum((i.house_result/100)* i.rate)  profit, sum ((i.amount/100)*i.rate) turnover
FROM investments i ,users u, skins s , currencies c  , opportunities O                                                                                             
WHERE i.user_id= u.id                                                                      
AND u.skin_id = s.id                                                                    
AND u.currency_id = c.id                                      
AND O.ID=I.opportunity_id                                                                  
AND i.is_settled = 1                                                                                                 
AND i.is_canceled = 0                                                                                                 
AND u.class_id <> 0
AND i.time_settled <sysdate                                                                                                         
GROUP BY u.id, u.skin_id) d
on u.id=d.id,
 user_market_disable UMD,skins s
where umd.user_id=u.id
and s.id=u.skin_id
and umd.is_active=1
and u.class_id<>0
and umd.is_dev3=1 ;
