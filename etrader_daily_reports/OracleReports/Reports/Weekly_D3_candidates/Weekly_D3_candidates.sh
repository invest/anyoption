#!/bin/bash
#
#
# Script Variables (change them to suit your needs)


cd /usr/local/etrader-jobs/Reports/Weekly_D3_candidates

/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=scan.dblive.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=etrader)))" <<eof1
set pages5000
ALTER SESSION SET NLS_DATE_FORMAT='DD-MON-YYYY HH24:MI' ;
SET MARKUP HTML ON SPOOL ON
spool Weekly_D3_candidates.xls
@Weekly_D3_candidates.sql
spool off
exit
eof1


##----------------- 
#sqlplus -s " etrader_anal/ANAL3tr4d3r " <<eof1
#set pages5000
#SET MARKUP HTML ON SPOOL ON
#spool Weekly_D3_candidates.xls
#@Weekly_D3_candidates.sql
#spool off
#CLEAR BUFFER
#exit
#eof1
## -----------------

echo -e "All customers that are defined as candidates to examination for D3 until `date`"|mutt -a Weekly_D3_candidates.xls -s "Weekly D3 candidates `date +%F`" yb@etrader.co.il -c maia@etrader.co.il,ilanitr@etrader.co.il,idan@etrader.co.il

rm -vf Weekly_D3_candidates.xls

