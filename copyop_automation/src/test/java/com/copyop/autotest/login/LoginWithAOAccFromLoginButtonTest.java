package com.copyop.autotest.login;

import java.io.IOException;
import java.util.HashMap;

import junit.framework.Assert;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.RegisterPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LoginWithAOAccFromLoginButtonTest extends BaseTest {
	public LoginWithAOAccFromLoginButtonTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/LoginWitAOAccTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Logins with anyoption.com account
	 * @param skin
	 * @param headerMess
	 * @param detailsMess
	 * @param chooseMess
	 * @param acceptMess
	 * @param helloMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void loginWithAOAccFromLoginButtonTest(String skin, String headerMess, String detailsMess, String chooseMess,
			String acceptMess, String helloMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		RegisterPage registerPage = new RegisterPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		HomePage homePage = new HomePage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getAnyoptionUser(skinId);
		homePage.changeLanguage(skin);
		homePage.clickHeaderLoginButton();
		HashMap<String, String> data = loginPage.loginWithAnyOptionUser(sqlQuery);
		currentUserId = data.get("id");
		String nickName = data.get("first_name").replace("Autotest", "");
		registerPage.setNickName(nickName);
		Assert.assertEquals(headerMess, loginPage.getHeaderMessage());
		Assert.assertEquals(data.get("first_name") + " " + data.get("last_name"), loginPage.getAnyoptionDetails());
		Assert.assertEquals(detailsMess, loginPage.getDetailsMessage());
		Assert.assertEquals(chooseMess, loginPage.getChooseNickMessage());
		Assert.assertEquals(acceptMess, loginPage.getTermAndCondMessage());
		loginPage.clickCheckBoxForTermAndCond();
		loginPage.editAvatar();
		registerPage.clickFemaleAvatarButton();
		Assert.assertTrue(loginPage.getAvatarImage("female"));
		loginPage.confirmRegistrationWithAOAcc();
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		Assert.assertEquals(helloMess + " " + data.get("first_name") + " " + data.get("last_name"),  homePage.getHeaderName());
		Assert.assertEquals(data.get("first_name") + " " + data.get("last_name"), homePage.getSideUserName());
		Assert.assertEquals(data.get("first_name") + " " + data.get("last_name"), homePage.getUserMenuIconName());
		String balance = homePage.getBalance();
		balance = balance.replaceAll("[^\\d]", "");
		Assert.assertEquals(data.get("balance"), balance);
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
