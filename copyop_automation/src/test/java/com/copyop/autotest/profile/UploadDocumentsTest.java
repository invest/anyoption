package com.copyop.autotest.profile;

import java.io.IOException;
import java.util.HashMap;

import junit.framework.Assert;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.DocumentsPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.Configuration;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UploadDocumentsTest extends BaseTest {
	private String docsPath;
	
	public UploadDocumentsTest() throws IOException {
		super();
		docsPath = Configuration.getProperty("pathToDocsDir");
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/UploadDocumentsTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	/**
	 * Uploads documents
	 * @param skin
	 * @param headerMess
	 * @param missingStatus
	 * @param waitingStatus
	 * @param selectStatus
	 * @param approvedStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void uploadDocumentsTest(String skin, String headerMess, String missingStatus, String waitingStatus, String selectStatus, 
			String approvedStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		DocumentsPage documentsPage = new DocumentsPage(driver, browserType);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			homePage.changeLanguage(skin);
		}
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanUploadDocuments(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		if (feedPage.isFeedPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		String sumDeposits = UserDB.getSumDeposits(currentUserId);
		int neededDeposit = 300000 - Integer.parseInt(sumDeposits);
		if (neededDeposit > 0) {
			String amount = String.valueOf(neededDeposit / 100);
			homePage.openDepositScreen();
			depositPage.setAmountForDepositWithExistedCard(amount);
			depositPage.setCvv("123");
			depositPage.submitDepositWithExistedCard();
			depositPage.submitPopUp();
		}	
		Assert.assertTrue(documentsPage.isDocumentPopUpDisplayed());
		Assert.assertTrue(documentsPage.getDearMessage().contains(userData.get("first_name")));
		Assert.assertEquals(headerMess, documentsPage.getHeaderMessage());
		int documentIndex = 0;
		Assert.assertTrue(documentsPage.isStatusError(documentIndex));
		Assert.assertEquals(missingStatus, documentsPage.getStatusMessage(documentIndex, missingStatus));
		String path = docsPath + "Utillity_bill_scan.png";
		documentsPage.attachFile(path, documentIndex);
		Assert.assertTrue(documentsPage.isStatusWaitingToApproval(documentIndex));
		Assert.assertEquals(waitingStatus, documentsPage.getStatusMessage(documentIndex, waitingStatus));
		documentIndex = 1;
		documentsPage.clickIdBrowseButton();
		Assert.assertTrue(documentsPage.isStatusError(documentIndex));
		Assert.assertEquals(selectStatus, documentsPage.getStatusMessage(documentIndex, selectStatus));
		documentsPage.selectId(2);
		Assert.assertTrue(documentsPage.isStatusError(documentIndex));
		Assert.assertEquals(missingStatus, documentsPage.getStatusMessage(documentIndex, missingStatus));
		path = docsPath + "ID_scan.png";
		documentsPage.attachFile(path, documentIndex);
		if (documentsPage.isDocumentPopUpDisplayed() == false) {
			driver.navigate().refresh();
			UtilsMethods.sleep(1000);
		}
		Assert.assertTrue(documentsPage.isStatusWaitingToApproval(documentIndex));
		Assert.assertEquals(waitingStatus, documentsPage.getStatusMessage(documentIndex, waitingStatus));
		documentIndex = 2;
		Assert.assertTrue(documentsPage.isStatusError(documentIndex));
		Assert.assertEquals(missingStatus, documentsPage.getStatusMessage(documentIndex, missingStatus));
		path = docsPath + "CC_FRONT_scan.png";
		documentsPage.attachFile(path, documentIndex);
		if (documentsPage.isDocumentPopUpDisplayed() == false) {
			driver.navigate().refresh();
			UtilsMethods.sleep(1000);
		}
		Assert.assertTrue(documentsPage.isStatusWaitingToApproval(documentIndex));
		Assert.assertEquals(waitingStatus, documentsPage.getStatusMessage(documentIndex, waitingStatus));
		documentIndex = 3;
		Assert.assertTrue(documentsPage.isStatusError(documentIndex));
		Assert.assertEquals(missingStatus, documentsPage.getStatusMessage(documentIndex, missingStatus));
		path = docsPath + "back.png";
		documentsPage.attachFile(path, documentIndex);
		if (documentsPage.isDocumentPopUpDisplayed() == false) {
			driver.navigate().refresh();
			UtilsMethods.sleep(1000);
		}
		Assert.assertTrue(documentsPage.isStatusWaitingToApproval(documentIndex));
		Assert.assertEquals(waitingStatus, documentsPage.getStatusMessage(documentIndex, waitingStatus));
		UserDB.approveDocument(currentUserId, "Utillity_bill_scan.png".replace("_", ""));
		UserDB.approveDocument(currentUserId, "ID_scan.png".replace("_", ""));
		UserDB.approveDocument(currentUserId, "back.png".replace("_", ""));
		UserDB.approveDocument(currentUserId, "CC_FRONT_scan.png".replace("_", ""));
		driver.navigate().refresh();
		UtilsMethods.sleep(2000);
		for (int i = 0; i < 4; i++) {
			Assert.assertTrue(documentsPage.isStatusApproved(0));
			Assert.assertEquals(approvedStatus, documentsPage.getStatusMessage(i, approvedStatus));
			Assert.assertTrue(documentsPage.isBrowseButtonDisabled(i));
		}
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
