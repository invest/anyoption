package com.copyop.autotest.investment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToInvestNotAvailableAmountTest extends BaseTest {
	public TryToInvestNotAvailableAmountTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/TryToInvestNotAvailableAmountTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Tries to make investment with not available amounts
	 * @param skin
	 * @param errorMessage
	 * @param depositMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToInvestNotAvailableAmountTest(String skin, String errorMessage, String depositMessage) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		investmentPage.setActiveMarket();
		String amount = "24";
		investmentPage.setAmount(amount);
		Assert.assertEquals(moneyType + amount, investmentPage.getTheAmountForInvest());
		investmentPage.clickUpButton();
		Assert.assertTrue(investmentPage.isFooterMessageDisplayed());
		Assert.assertEquals(errorMessage, investmentPage.getFooterErrorText());
		investmentPage.closeFooterError();
		investmentPage.clickDownButton();
		Assert.assertTrue(investmentPage.isFooterMessageDisplayed());
		Assert.assertEquals(errorMessage, investmentPage.getFooterErrorText());
		investmentPage.closeFooterError();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		amount = String.valueOf(balanceInt + 100);
		amount = amount.substring(0, amount.length() - 2);
		investmentPage.setAmount(amount);
		investmentPage.clickUpButton();
		Assert.assertTrue(investmentPage.isDepositPopUpDisplayed("UP"));
		Assert.assertEquals(depositMessage, investmentPage.getNoDepositMessage());
		investmentPage.closeNoDepostiPopUp();
		investmentPage.clickDownButton();
		Assert.assertTrue(investmentPage.isDepositPopUpDisplayed("DOWN"));
		Assert.assertEquals(depositMessage, investmentPage.getNoDepositMessage());
		investmentPage.clickNoDepositPopUpButton();
		Assert.assertTrue(depositPage.isDepositExistingCardCurrent());
		String balanceAfter = homePage.getBalance();
		Assert.assertEquals(balance, balanceAfter);
		String balanceInDb = UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId);
		Assert.assertEquals(userData.get("balance"), balanceInDb);
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
