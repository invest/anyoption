package com.copyop.autotest.investment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.pages.CopyAndWatchPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.QuestionnairePage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToInvestWithNotRegulatedUserTest extends BaseTest {

	public TryToInvestWithNotRegulatedUserTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/MakeQuestionnaireTestData.xlsx", numberRows, 3);
	    return(retObjArr);
	}
	/**
	 * Tries to make investment with user who is not made questionnaire.
	 * @param skin
	 * @param title
	 * @param subTitle
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToInvestWithNotRegulatedUserTest(String skin, String title, String subTitle) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		CopyAndWatchPage copyAndWatchPage = new CopyAndWatchPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeQuestionnaire(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		questionnairePage.closeQuestionnairePopUp();
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		investmentPage.setActiveMarket();
		investmentPage.clickUpButton();
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		questionnairePage.closeQuestionnairePopUp();
		String balanceAfter = homePage.getBalance();
		Assert.assertEquals(balance, balanceAfter);
		homePage.openSocialScreen();
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		copyAndWatchPage.openUserProfile(0);
		if (feedPage.isWatchPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		copyAndWatchPage.watchTheUser();
		copyAndWatchPage.confirmPopUp();
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		questionnairePage.closeQuestionnairePopUp();
		copyAndWatchPage.copyTheUser();
		if (feedPage.isCopyPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}	
		copyAndWatchPage.confirmPopUp();
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		questionnairePage.closeQuestionnairePopUp();
		homePage.openWatchers();
		copyAndWatchPage.openConectionsTab(3);
		Assert.assertEquals(0, copyAndWatchPage.getCountWatchedUsers());
		copyAndWatchPage.openConectionsTab(2);
		Assert.assertEquals(0, copyAndWatchPage.getCountCopiedUsers());
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
