package com.copyop.autotest.investment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.MyOptionsPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TradeMySelfTest extends BaseTest {
	public TradeMySelfTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/TradeMySelfTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Makes binary option investment
	 * @param skin
	 * @param headerMess
	 * @param popUpText1
	 * @param popUpText2
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tradeMySelfTest(String skin, String headerMess, String popUpText1, String popUpText2) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		MyOptionsPage myOptionsPage = new MyOptionsPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}	
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == false) {
			homePage.clickHelpIcon();
		}
		Assert.assertTrue(feedPage.isInvestmentPopUpDisplayed());
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.closePopUp();
		investmentPage.setActiveMarket();
		investmentPage.chooseAmount(0);
		String amountToInvest = investmentPage.getTheAmountForInvest();
		investmentPage.clickProfitDropDown();
		String chooseProfit[] = investmentPage.chooseProfit(0);
		String profitToInvest = investmentPage.getTheProfit();
  		Assert.assertEquals(chooseProfit[0] + "%", profitToInvest);
  		int correct = investmentPage.getReturnValueIfCorrect();
  		int incorrect = investmentPage.getReturnValueIfIncorrect();
  		double profitPercent = Double.parseDouble(chooseProfit[0]);
  		double refundPercent = Double.parseDouble(chooseProfit[1]);
  		double returnValue = 75 * (1 + (profitPercent / 100)) * 100;
  		returnValue = Math.round(returnValue);
  		Assert.assertEquals(correct, (int)returnValue);
  		returnValue = (75 * (refundPercent / 100)) * 100;
  		Assert.assertEquals(incorrect, (int)returnValue);
  		String marketName = investmentPage.getMarketName();
  		investmentPage.clickUpButton();
  		Assert.assertTrue(investmentPage.afterInvest());
  		Assert.assertEquals(headerMess, investmentPage.getHeaderTextFromAfterInvestPopUp());
  		String amountAfterPopUp = investmentPage.getAmountFormAfterInvestPopUp();
  		Assert.assertEquals(amountToInvest + ".00", amountAfterPopUp);
  		Assert.assertEquals(correct, investmentPage.getCorrectAmountFromAfterInvestPopUp());
  		Assert.assertEquals(incorrect, investmentPage.getIncorrectAmountFromAfterInvestPopUp());
  		Assert.assertEquals(marketName, investmentPage.getMarketNameFromAfterInvestPopUp());
  		Assert.assertEquals(amountToInvest + ".00", investmentPage.getTotalInvestmentAmount());
  		String investmentId = investmentPage.getInvestmentId();
  		String level = investmentPage.getLevelFromAfterInvest();
  		amountAfterPopUp = amountAfterPopUp.replaceAll("[^\\d]", "");
  		UserDB.compareInvestmentData(investmentId, userData.get("id"), level, amountAfterPopUp, chooseProfit);
  		String balanceAfter = homePage.getBalance();
  		String userBalanceInDBAfter = UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId);
  		int ballanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int amountInt = Integer.parseInt(amountToInvest.replace(moneyType, "") + "00");
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(ballanceInt - amountInt, balanceAfterInt);
  		int balanceInDbInt = Integer.parseInt(userData.get("balance"));
  		int balanceInDbAfterInt = Integer.parseInt(userBalanceInDBAfter);
  		Assert.assertEquals(balanceInDbInt - amountInt, balanceInDbAfterInt);
  		homePage.openMyOptions();
  		if (skin.equals("DE") && marketName.equals("Oil")) {
  			marketName = "Öl";
  		}
  		if (skin.equals("ES") && marketName.equals("Gold")) {
  			marketName = "Oro";
  		}
  		Assert.assertEquals(marketName, myOptionsPage.getOptionMarketName(0));
  		Assert.assertEquals(amountToInvest + ".00", myOptionsPage.getOptionAmount(0));
  		String levelFromOptions = myOptionsPage.getOptionLevel(0);
  		if (! level.equals(levelFromOptions)) {
  			Assert.assertEquals(levelFromOptions.replace("0", "").replace(".", ""), level.replace("0", "").replace(".", ""));
  		}	
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
