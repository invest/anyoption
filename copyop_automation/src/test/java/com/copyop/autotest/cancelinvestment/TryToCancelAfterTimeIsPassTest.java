package com.copyop.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.MyOptionsPage;
import com.copyop.autotest.pages.OptionsPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class TryToCancelAfterTimeIsPassTest extends BaseTest {

	public TryToCancelAfterTimeIsPassTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/CancelInvestmentTestData.xlsx", numberRows, 2);
	    return(retObjArr);
	}
	/**
	 * Tries to cancel investment after time is pass
	 * @param skin
	 * @param canceledMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void tryToCancelAfterTimeIsPassTest(String skin, String canceledMess) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		OptionsPage optionsPage = new OptionsPage(driver);
		MyOptionsPage myOptionsPage = new MyOptionsPage(driver);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			homePage.changeLanguage(skin);
		}
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeInvestment(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}	
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int balanceInDbInt = Integer.parseInt(userData.get("balance"));
		investmentPage.setActiveMarket();
		String amount = "50";
		int amountInt = Integer.parseInt(amount + "00");
		investmentPage.setAmount(amount);
		String marketName = investmentPage.getMarketName();
		boolean isCancelInvDisp = false;
		for (int i = 0; i < 10; i++) {
			investmentPage.clickUpButton();
			if (investmentPage.isCancelInvestmentDisplayed() == true) {
				isCancelInvDisp = true;
				break;
			}
		}	
		Assert.assertTrue(isCancelInvDisp);
		String cancelInvLevel = investmentPage.getCancelInvLevel();
		UtilsMethods.sleep(8000);
		investmentPage.showCancelInvPopUp();
		investmentPage.cancelInvestment();
		Assert.assertEquals(canceledMess, investmentPage.getCanceledInvMessage());
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		int balanceInDbAfterInt = Integer.parseInt(UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId));
		Assert.assertEquals(balanceInt - amountInt, balanceAfterInt);
		Assert.assertEquals(balanceInDbInt - amountInt, balanceInDbAfterInt);
		Assert.assertEquals(marketName, optionsPage.getOpenOptionMarketName(0));
		if (! cancelInvLevel.equals(optionsPage.getOpenOptionLevel(0))) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), optionsPage.getOpenOptionLevel(0).replace("0", ""));
		}
		Assert.assertEquals(moneyType + amount + ".00".replace(",", ""), optionsPage.getOpenOptionAmount(0).replace(",", ""));
		Assert.assertEquals(moneyType + amount, optionsPage.getInvestedAmount());
		homePage.openMyOptions();
		Assert.assertEquals(marketName, myOptionsPage.getOptionMarketName(0));
		String levelFromMyOptions = myOptionsPage.getOptionLevel(0);
		if (! cancelInvLevel.equals(levelFromMyOptions)) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), levelFromMyOptions.replace("0", ""));
		}	
		Assert.assertEquals(moneyType + amount + ".00", myOptionsPage.getOptionAmount(0));
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
