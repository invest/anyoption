package com.copyop.autotest.cancelinvestment;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.MyOptionsPage;
import com.copyop.autotest.pages.OptionsPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelDev2InvestmentTest extends BaseTest {

	public CancelDev2InvestmentTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/CancelInvestmentTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Cancel dev 2 investment
	 * @param skin
	 * @param canceledMess
	 * @param feeMess
	 * @param linkText
	 * @param optionStatus
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void cancelDev2InvestmentTest(String skin, String canceledMess, String feeMess, String linkText, String optionStatus) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		MyOptionsPage myOptionsPage = new MyOptionsPage(driver);
		OptionsPage optionsPage = new OptionsPage(driver);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			homePage.changeLanguage(skin);
		}
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDev2Investment(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}	
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int balanceInDbInt = Integer.parseInt(userData.get("balance"));
		investmentPage.setActiveMarket();
		int count = investmentPage.getMarketsCount();
		HashMap<String, String> dataToInvest = new HashMap<>();
		for (int i = 0; i < count; i++) {
			String marketId = investmentPage.getCurrentMarketId();
			dataToInvest = UserDB.getDev2Data(marketId);
			boolean isMarketActive = investmentPage.isMarketActive();
			if (dataToInvest.isEmpty() || (balanceInDbInt /100 < Integer.parseInt(dataToInvest.get("amount"))) || isMarketActive == false) {
				investmentPage.chooseMarketByIndex(i);
			} else {
				System.out.println(dataToInvest);
				break;
			}
		}
		String amount = dataToInvest.get("amount");
		investmentPage.setAmount(amount);
  		String marketName = investmentPage.getMarketName();
  		boolean isCancelInvDisp = false;
  		for (int i = 0; i < 10; i++) {
  			investmentPage.clickUpButton();
  			if (investmentPage.isCancelInvestmentDisplayed() == true) {
  				isCancelInvDisp = true;
  				break;
  			}
  		}
  		Assert.assertTrue(investmentPage.isCancelInvestmentDisplayed());
  		int secs = investmentPage.getCancelInvLeftTime();
  		if (Integer.parseInt(dataToInvest.get("secs")) - 1 != secs) {
  			Assert.assertEquals(Integer.parseInt(dataToInvest.get("secs")) - 2, secs);
  		}
  		Assert.assertTrue(isCancelInvDisp);
		investmentPage.cancelInvestment();
		Assert.assertEquals(canceledMess, investmentPage.getCanceledInvMessage());
		investmentPage.closesCancelInvPopUp();
		investmentPage.showCancelInvPopUp();
		String cancelInvLevel = investmentPage.getCancelInvLevel();
		String cancelInvAmount = investmentPage.getCancelInvAmount();
		int cancelInvTimeLeft = investmentPage.getCancelInvLeftTime();
		String feeMessage = investmentPage.getCancelInvFeeMessage();
		String cancelInvLinkText = investmentPage.getCancelInvLinkText();
		Assert.assertEquals(moneyType + amount + ".00", cancelInvAmount.replace(",", ""));
		Assert.assertTrue(cancelInvTimeLeft <= Integer.parseInt(dataToInvest.get("secs")));
		double amountInt = Double.parseDouble(amount);
  		double am = (amountInt * 0.1);
  		String loseAm = String.valueOf(am);
  		if (loseAm.contains(".10") || loseAm.contains(".20") || loseAm.contains(".30") || loseAm.contains(".40") || loseAm.contains(".50")
  				|| loseAm.contains(".60") || loseAm.contains(".70") || loseAm.contains(".80") || loseAm.contains(".90")) {
  			loseAm = loseAm.substring(0, loseAm.indexOf(".") + 3);
  		} else {
  			loseAm = loseAm + "0";
  		}
  		feeMess = feeMess.replace("5", loseAm);
		Assert.assertEquals(feeMess, feeMessage);
		Assert.assertEquals(linkText, cancelInvLinkText);
		double fee = Double.parseDouble(loseAm) * 100;
		UtilsMethods.sleep(3000);
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		String balanceInDbAfter = UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId);
		int balanceInDbAfterInt = Integer.parseInt(balanceInDbAfter);
		Assert.assertEquals(balanceInt - (int)fee, balanceAfterInt);
		Assert.assertEquals(balanceInDbInt - (int)fee, balanceInDbAfterInt);
		UserDB.verifyCanceledInvestment(currentUserId, amount + "00", String.valueOf((int)fee), cancelInvLevel);
		int retValue = Integer.parseInt(amount + "00") - (int)fee;
		optionsPage.openSettledOption();
		Assert.assertEquals(marketName, optionsPage.getSettledOptionMarketName(0));
		if (! cancelInvLevel.equals(optionsPage.getSettledOptionLevel(0))) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), optionsPage.getSettledOptionLevel(0).replace("0", ""));
		}
		Assert.assertEquals(moneyType + amount + ".00".replace(",", ""), optionsPage.getSettledOptionAmount(0).replace(",", ""));
		Assert.assertEquals(optionStatus, optionsPage.getSetteledOptionStatus(0));
		Assert.assertEquals(retValue, optionsPage.getSettledOptionReturnValue(0));
		homePage.openMyOptions();
		myOptionsPage.openSettledOption();
		Assert.assertEquals(marketName, myOptionsPage.getOptionMarketName(0));
		String levelFromMyOptions = myOptionsPage.getOptionLevel(0);
		if (! cancelInvLevel.equals(levelFromMyOptions)) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), levelFromMyOptions.replace("0", ""));
		}	
		Assert.assertEquals(moneyType + amount + ".00", myOptionsPage.getOptionAmount(0).replace(",", ""));
		Assert.assertEquals(optionStatus, myOptionsPage.getSetteledOptionStatus(0));
		Assert.assertEquals(retValue, myOptionsPage.getSettledOptionReturnValue(0));
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
