package com.copyop.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.BankingHistoryPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositWithExistingCardTest extends BaseTest {
	public DepositWithExistingCardTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/DepositWithExistedCardTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with existing card
	 * @param skin
	 * @param succesMessage
	 * @param amountMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void depositWithExistingCardTest(String skin, String succesMessage, String amountMess, String description, String type) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		FeedPage feedPage = new FeedPage(driver);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			homePage.changeLanguage(skin);
		}
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeDeposit(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int depositInDbInt = Integer.parseInt(userData.get("balance"));
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		homePage.openDepositScreen();
		Assert.assertEquals(true, depositPage.isDepositExistingCardCurrent());
		Assert.assertEquals(moneyType, depositPage.getCurrencyFromDepositWithExistCard());
		String amount = "200";
		if (! skin.equals("EN")) {
			amount = "250";
		}
		int amountInt = Integer.parseInt(amount + "00");
		depositPage.setAmountForDepositWithExistedCard(amount);
		depositPage.setCvv("123");
		depositPage.submitDepositWithExistedCard();
		Assert.assertEquals(succesMessage, depositPage.getSuccesMessage());
		String message[] = depositPage.getAmountMessage().split("\n");
		Assert.assertEquals(moneyType + amount + " " + amountMess, message[0]);
		String transactionId = message[1].replaceAll("[^\\d]", "");
		depositPage.submitPopUp();
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt + amountInt, balanceAfterInt);
		String depositInDbAfter = UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId);
		int depositInDbAfterInt = Integer.parseInt(depositInDbAfter);
		Assert.assertEquals(depositInDbInt + amountInt, depositInDbAfterInt);
		UserDB.verifyDepositTransaction(transactionId, amount);
		UtilsMethods.sleep(2000);
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(description, bankingHistoryPage.getTransactionDescription(0));
		Assert.assertEquals(moneyType + amount + ".00 " + type, bankingHistoryPage.getAmountDescription(0));
		Assert.assertEquals(transactionId, bankingHistoryPage.getTransactionId(0));
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
