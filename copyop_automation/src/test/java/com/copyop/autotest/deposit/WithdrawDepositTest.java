package com.copyop.autotest.deposit;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.BankingHistoryPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class WithdrawDepositTest extends BaseTest {
	private int currentDayOfWeek;
	public WithdrawDepositTest() throws IOException {
		super();
		Calendar calendar = Calendar.getInstance();
		currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); 
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/WithdrawDepositTestData.xlsx", numberRows, 8);
	    return(retObjArr);
	}
	/**
	 * Withdraws deposit
	 * @param skin
	 * @param dearMess
	 * @param reasonMess
	 * @param thanksMess
	 * @param finalMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void withdrawDepositTest(String skin, String dearMess, String reasonMess, String thanksMess, 
			String finalMess, String errMess, String description, String type) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanWithdrawDeposit(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		homePage.openDepositScreen();
		depositPage.clickWithdrawLink();
		depositPage.selectCardToWithdraw("0");
		String amount = "200";
		depositPage.setAmountForDeposit(amount);
		Assert.assertEquals(moneyType, depositPage.getCurrencyFromWithdrawScreen());
		depositPage.submitWithdraw();
		Assert.assertEquals(dearMess + " " + userData.get("first_name") + ",", depositPage.getWithdrawPopUpTitle());
		Assert.assertEquals(reasonMess, depositPage.getWithdrawPopUpQuestion());
		depositPage.chooseRadioButton(0);
		depositPage.submitPopUp();
		Assert.assertEquals(thanksMess, depositPage.getThanksMess());
		depositPage.submitPopUp();
		depositPage.checkForBonusPopUp();
		if (currentDayOfWeek != 7 && currentDayOfWeek != 1) {
			switch (moneyType) {
			case "$":
				Assert.assertEquals("3000", depositPage.getFeeFromImportantMessage(moneyType));
				break;
			case "€":
				Assert.assertEquals("2500", depositPage.getFeeFromImportantMessage(moneyType));
				break;
			}
			depositPage.confirmsWithdraw();
			String finalMessage = depositPage.getWithdrawFinalMessage();
			Assert.assertTrue(finalMessage.contains(moneyType + amount));
			Assert.assertTrue(finalMessage.startsWith(finalMess));
			depositPage.closeDepositPopUp();
			String balaceAfter = homePage.getBalance();
			int balanceAfterInt = Integer.parseInt(balaceAfter.replaceAll("[^\\d]", ""));
			int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
			int amountInt = Integer.parseInt(amount + "00");
			int userBalanceDb = Integer.parseInt(userData.get("balance")); 
			int userBalanceDbAfter = Integer.parseInt(UserDB.getUserBalanceFromDB(userData.get("email"), userData.get("id")));
			Assert.assertEquals(balanceInt - amountInt, balanceAfterInt);
			Assert.assertEquals(userBalanceDb - amountInt, userBalanceDbAfter);
			bankingHistoryPage.openBankingHistory();
			Assert.assertEquals(description, bankingHistoryPage.getTransactionDescription(0));
			Assert.assertEquals(moneyType + amount + ".00 " + type, bankingHistoryPage.getAmountDescription(0));
			String transactionId = bankingHistoryPage.getTransactionId(0);
			UserDB.verifyWithdrawTransaction(currentUserId, amount, transactionId);
		} else {
			Assert.assertTrue(depositPage.isGlobalErrorWithdrawDiplayed());
			Assert.assertEquals(errMess, depositPage.getGlobalErrorWithdraw());
			String balaceAfter = homePage.getBalance();
			Assert.assertEquals(balance, balaceAfter);
		}
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
