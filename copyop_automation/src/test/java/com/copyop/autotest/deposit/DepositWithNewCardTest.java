package com.copyop.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.BankingHistoryPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.QuestionnairePage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositWithNewCardTest extends BaseTest {
	public DepositWithNewCardTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/DepositWithNewCardTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with new credit card
	 * @param skin
	 * @param depositTitle
	 * @param positiveMess
	 * @param autoWatchTitle
	 * @param autoWatchMess
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void depositWithNewCardTest(String skin, String depositTitle, String positiveMess, String autoWatchTitle, 
			String autoWatchMess, String description, String type) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		DepositPage depositPage = new DepositPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			homePage.changeLanguage(skin);
		}
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeNewDeposit(skinId);
		HashMap<String, String> data = loginPage.loginWithUserFromDB(sqlQuery, true);
		currentUserId = data.get("id");
		Assert.assertTrue(depositPage.isDepositPopUpDisplayed());
		Assert.assertEquals(depositTitle, depositPage.getDepositPopUpTitle());
		String balance = homePage.getBalance();
		String moneyType = balance.replaceAll("\\d+.*", "");
		Assert.assertEquals("0.00", balance.replace(moneyType, ""));
		String depositInDb = data.get("balance");
		String amount = "400";
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			amount = "500";
		} else {
			depositPage.setAmountForDeposit(amount);
		}	
		Assert.assertEquals(moneyType, depositPage.getCurrentCurrency());
		depositPage.selectCurrency(0);
		Assert.assertEquals("$", depositPage.getCurrentCurrency());
		depositPage.setCardNumber("4200000000000000");
		depositPage.selectExpMonth(3);
		depositPage.selectExpYear(2);
		depositPage.setCvv("123");
		depositPage.setHolderName("CopyOpTest");
		depositPage.setStreet("StreetTest");
		depositPage.setCity("Sofia");
		depositPage.setZipCode("1234");
		depositPage.selectBirthDay("19");
		depositPage.selectBirthMount(7);
		depositPage.selectBirthYear("1986");
		depositPage.submitDeposit();
		depositPage.waitError();
		UserDB.allowCreditCard(currentUserId);
		UtilsMethods.sleep(4000);
		depositPage.setAmountForDeposit(amount);
		depositPage.submitDepositNext();
		Assert.assertTrue(depositPage.isMessagePopUpDisplayed());
		Assert.assertEquals(positiveMess, depositPage.getSuccesMessage());
		String message = depositPage.getAmountMessage();
		Assert.assertTrue(message.contains("$" + amount));
		String mess[] = message.split("\n");
		String transactionId = mess[1].replaceAll("[^\\d]", "");
		depositPage.submitPopUp();
		Assert.assertTrue(depositPage.getAutoWatchingPopUpTitle().contains(autoWatchTitle));
		Assert.assertEquals(autoWatchMess, depositPage.getAutoWatchingMessage());
		depositPage.clickAutoWatchingDoneButton();
		questionnairePage.closeQuestionnairePopUp();
		String balanceAfter = homePage.getBalance();
		Assert.assertTrue(balanceAfter.contains("$"));
		moneyType = "$";
		Assert.assertEquals(moneyType + amount + ".00", balanceAfter);
		String depositInDbAfter = UserDB.getUserBalanceFromDB(data.get("email"), currentUserId);
		Assert.assertEquals(Integer.parseInt(depositInDb) + Integer.parseInt(amount + "00"), Integer.parseInt(depositInDbAfter));
		UserDB.verifyDepositTransaction(transactionId, amount);
		homePage.openDepositScreen();
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(description, bankingHistoryPage.getTransactionDescription(0));
		Assert.assertEquals(moneyType + amount + ".00 " + type, bankingHistoryPage.getAmountDescription(0));
		Assert.assertEquals(transactionId, bankingHistoryPage.getTransactionId(0));
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
