package com.copyop.autotest.deposit;

import java.io.IOException;
import java.util.HashMap;

import junit.framework.Assert;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.pages.BankingHistoryPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ReverseWithdrawTest extends BaseTest {
	
	public ReverseWithdrawTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/ReverseWithdrawTestData.xlsx", numberRows, 5);
	    return(retObjArr);
	}
	/**
	 * Reverses withdraw
	 * @param skin
	 * @param descMess
	 * @param succMessage
	 * @param descMessNext
	 * @param type
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void reverseWithdrawTest(String skin, String descMess, String succMessage, String descMessNext, String type) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanReverseWithdraw(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		String balance = homePage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int balanceInDb = Integer.parseInt(userData.get("balance"));
		String moneyType = balance.replaceAll("\\d+.*", "");
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		homePage.openDepositScreen();
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(descMess, bankingHistoryPage.getTransactionDescription(0));
		String amount = bankingHistoryPage.getAmountDescription(0);
		Assert.assertTrue(amount.contains(moneyType));
		amount = amount.replaceAll("[^\\d]", "");
		String amountText = amount.substring(0, amount.length() - 2);
		depositPage.clickReverseButton();
		String message = depositPage.getReverseMessage();
		Assert.assertTrue(message.contains(moneyType + amountText + ".00"));
		Assert.assertEquals(succMessage, message);
		depositPage.submitPopUp();
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt =  Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		int balanceInDbAfterInt = Integer.parseInt(UserDB.getUserBalanceFromDB(userData.get("email"), currentUserId));
		int amountInt = Integer.parseInt(amount);
		Assert.assertEquals(balanceInt + amountInt, balanceAfterInt);
		Assert.assertEquals(balanceInDb + amountInt, balanceInDbAfterInt);
		Assert.assertEquals(descMessNext, bankingHistoryPage.getTransactionDescription(0));
		String nextAmount = bankingHistoryPage.getAmountDescription(0);
		String mess = moneyType + amountText + ".00 " + type;
		Assert.assertEquals(mess, nextAmount);
		String transactionId = bankingHistoryPage.getTransactionId(0);
		UserDB.verifyReverseWithdrawTransaction(currentUserId, amount, transactionId);
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
