package com.copyop.autotest.register;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.common.UserDB.Skin;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.RegisterPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.CreateUser;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class RegisterFromLoginFormTest extends BaseTest {
	public RegisterFromLoginFormTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/RegisterFromLoginFormTestData.xlsx", numberRows, 6);
	    return(retObjArr);
	}
	@Test(dataProvider = "dp")
	public void registerFromLoginFormTest(String skin, String haveAccMess, String signUpMess, String homePageMess, String depositTitle, 
			String homePageHello) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		CreateUser user = new CreateUser();
		RegisterPage registerPage = new RegisterPage(driver, browserType);
		DepositPage depositPage = new DepositPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		HomePage homePage = new HomePage(driver);
		LoginPage loginPage = new LoginPage(driver, browserType);
		homePage.changeLanguage(skin);
		homePage.clickHeaderLoginButton();
		Assert.assertEquals(haveAccMess, loginPage.getHaveAccountMessage());
		loginPage.clickSignUpButton();
		Assert.assertEquals(signUpMess, registerPage.getSignUpTitle());
		Assert.assertEquals(homePageMess, registerPage.getSignUpScreenMessage());
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String nickName = user.getNickname();
		String email = user.getEMail();
		String phone = user.getPhone();
		String password = "123456";
		registerPage.setFirstName(firstName);
		registerPage.setLastName(lastName);
		registerPage.setNickName(nickName);
		registerPage.setEmail(email);
		registerPage.setPhone(phone);
		registerPage.setPassword(password);
		registerPage.retypePassword(password);
		String value = "28";
		if (skin.equals("DE")) {
			value = "28";
		}
		if (skin.equals("IT")) {
			value = "31";
		}
		if (skin.equals("FR")) {
			value = "32";
		}
		registerPage.selectCountry(value);
		registerPage.checkTermAndConds();
		Assert.assertTrue(registerPage.isCheckBoxChecked());
		registerPage.editAvatar();
		registerPage.clickFemaleAvatarButton();
		Assert.assertTrue(registerPage.getAvatarImage("female"));
		registerPage.clickDoneButton();
		Assert.assertTrue(depositPage.isDepositPopUpDisplayed());
		Assert.assertEquals(depositTitle, depositPage.getDepositPopUpTitle());
		depositPage.closeDepositPopUp();
		Skin sk = UtilsMethods.getSkin(skin);
		UserDB.compareUserDataAfterCreationOfAccount(email, firstName, lastName, sk);
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		Assert.assertEquals(homePageHello + " " + firstName + " " + lastName,  homePage.getHeaderName());
		Assert.assertEquals(firstName + " " + lastName, homePage.getSideUserName());
		Assert.assertEquals(firstName + " " + lastName, homePage.getUserMenuIconName());
		String balance = homePage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		Assert.assertEquals(0, balanceInt);
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.not(ExpectedConditions.urlContains("/home")));
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("/login")) {
			currentUrl = currentUrl.replace("/login", "");
			driver.get(currentUrl);
			String waitCss = "#main-container > section > div > form > div > div:nth-child(1) > div:nth-child(3) > button";
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(waitCss)));
		}
	}
}
