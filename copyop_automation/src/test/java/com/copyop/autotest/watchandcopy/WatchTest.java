package com.copyop.autotest.watchandcopy;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.pages.CopyAndWatchPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class WatchTest extends BaseTest {
	public WatchTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/WatchTestData.xlsx", numberRows, 8);
	    return(retObjArr);
	}
	/**
	 * Watches another user
	 * @param skin
	 * @param watch
	 * @param watchMessage
	 * @param title
	 * @param chBoxMess
	 * @param popUpTitle
	 * @param popUpText1
	 * @param popUpText2
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void watchTest(String skin, String watch, String watchMessage, String title, String chBoxMess, 
			String popUpTitle, String popUpText1, String popUpText2) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		CopyAndWatchPage copyAndWatchPage = new CopyAndWatchPage(driver, browserType);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeCopyWatch(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		homePage.openWatchers();
		if (feedPage.isWatchPopUpDisplayed() == false) {
			homePage.clickHelpIcon();
		}
		Assert.assertTrue(feedPage.isWatchPopUpDisplayed());
		Assert.assertEquals(popUpTitle.toUpperCase(), feedPage.getWatchPopUpTitle());
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.closePopUp();
		copyAndWatchPage.openConectionsTab(3);
		copyAndWatchPage.removeAllWatchers();
		homePage.openSocialScreen();
		copyAndWatchPage.openUserProfile(0);
		String name = copyAndWatchPage.getUserNameFromProfile();
		copyAndWatchPage.watchTheUser();
		Assert.assertTrue(copyAndWatchPage.getWatchPopUpTitle().contains(name));
		Assert.assertEquals(watchMessage, copyAndWatchPage.getWatchPopUpText());
		copyAndWatchPage.confirmPopUp();
		Assert.assertTrue(copyAndWatchPage.isWatchButtonMarked());
		homePage.openWatchers();
		copyAndWatchPage.openConectionsTab(3);
		Assert.assertEquals(name, copyAndWatchPage.getWatchedOrCopiedUserName(0));
		copyAndWatchPage.stopWatch(0);
		Assert.assertEquals(title, copyAndWatchPage.getWatchSettingsTitle());
		copyAndWatchPage.clickStopWatchCheckBox();
		Assert.assertEquals(chBoxMess.toUpperCase(), copyAndWatchPage.getStopWatchCheckBoxText());
		copyAndWatchPage.confirmPopUp();
		Assert.assertEquals(0, copyAndWatchPage.getCountWatchedUsers());
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
