package com.copyop.autotest.watchandcopy;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.pages.CopyAndWatchPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CopyTest extends BaseTest {
	public CopyTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/CopyTestData.xlsx", numberRows, 7);
	    return(retObjArr);
	}
	/**
	 * Copies another user
	 * @param skin
	 * @param popUpText1
	 * @param popUpText2
	 * @param popUpText3
	 * @param popUpText4
	 * @param alreadyWatched
	 * @param unCopy
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void copyTest(String skin, String popUpText1, String popUpText2, String popUpText3, String popUpText4, 
			String alreadyWatched, String unCopy) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		CopyAndWatchPage copyAndWatchPage = new CopyAndWatchPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		HomePage homePage = new HomePage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeCopyWatch(skinId);
		HashMap<String, String> userData = loginPage.loginWithUserFromDB(sqlQuery, false);
		currentUserId = userData.get("id");
		if (homePage.isPopOldInvestDisplayed() == true) {
			homePage.closePopUpOldInvest();
		}
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		homePage.openCopiers();
		if (feedPage.isWatchPopUpDisplayed()) {
			feedPage.closePopUp();
		}	
		copyAndWatchPage.openConectionsTab(2);
		copyAndWatchPage.removeAllCopiers();
		homePage.openSocialScreen();
		copyAndWatchPage.openUserProfile(0);
		String name = copyAndWatchPage.getUserNameFromProfile();
		copyAndWatchPage.copyTheUser();
		if (! feedPage.isCopyPopUpDisplayed()) {
			copyAndWatchPage.openCopyPopUp();
		}
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText3, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText4, feedPage.getPopUpText());
		feedPage.closePopUp();
		copyAndWatchPage.editAmount(2);
		int numberTrades = copyAndWatchPage.getTradeCount();
		copyAndWatchPage.editCount();
		int count = 1;
		copyAndWatchPage.chooseAssets(count);
		Assert.assertEquals("$100", copyAndWatchPage.getAmount());
		Assert.assertEquals(numberTrades - 2, copyAndWatchPage.getTradeCount());
		Assert.assertEquals(count, copyAndWatchPage.getAssetsCount());
		copyAndWatchPage.confirmPopUp();
		Assert.assertTrue(copyAndWatchPage.isWatchButtonMarked());
		Assert.assertTrue(copyAndWatchPage.isCopyButtonMarked());
		copyAndWatchPage.watchTheUser();
		Assert.assertEquals(alreadyWatched, copyAndWatchPage.getAlreadyWatchedMessage());
		copyAndWatchPage.confirmPopUp();
		homePage.openCopiers();
		copyAndWatchPage.openConectionsTab(2);
		Assert.assertEquals(name, copyAndWatchPage.getWatchedOrCopiedUserName(0));
		copyAndWatchPage.stopCopy(0);
		copyAndWatchPage.clickStopCopyCheckBox();
		Assert.assertEquals(unCopy.toUpperCase(), copyAndWatchPage.getUncopyCheckBoxLabel());
		copyAndWatchPage.confirmPopUp();
		Assert.assertEquals(0, copyAndWatchPage.getCountCopiedUsers());
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
