package com.copyop.autotest.smoketest;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.copyop.autotest.utils.ReadTestData;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class SmokeTestFR extends SmokeBaseTest {
	private String testSkin;
	private String currentBalance;
	/**
	 * 
	 * @throws Exception
	 */
	public SmokeTestFR() throws Exception {
		super("FR");
		testSkin = "FR";
	}
	/**
	 * Data provider method for registerUserTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "registerUser")
	private Object[][] registerUserDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/RegisterAccountTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Register new user
	 * @param skin
	 * @param signUpMess
	 * @param homePageMess
	 * @param depositTitle
	 * @param homePageHello
	 */
	@Test(dataProvider = "registerUser", priority = 1)
	public void registerUserTest(String skin, String signUpMess, String homePageMess, String depositTitle, String homePageHello) {
		Registration registerUser = new Registration(driver, browserType, user, testSkin, registerData);
		registerData = registerUser.registerUser(signUpMess, homePageMess, depositTitle, homePageHello);
		currentBalance = registerData.get("balance");
	}
	/**
	 * Data provider method for makeFirstDepositTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "firstDeposit")
	private Object[][] makeFirstDepositDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/DepositWithNewCardTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Makes first deposit after registration
	 * @param skin
	 * @param depositTitle
	 * @param positiveMess
	 * @param autoWatchTitle
	 * @param autoWatchMess
	 * @throws IOException
	 * @throws AWTException 
	 */
	@Test(dataProvider = "firstDeposit", priority = 2, dependsOnMethods = {"registerUserTest"})
	public void makeFirstDepositTest(String skin, String depositTitle, String positiveMess, String autoWatchTitle, String autoWatchMess) throws IOException {
		FirstDeposit makeFirstDeposit = new FirstDeposit(driver, browserType, currentBalance, user, firstDepositData);
		firstDepositData = makeFirstDeposit.makeFirstDeposit(depositTitle, positiveMess, autoWatchTitle, autoWatchMess);
		currentBalance = firstDepositData.get("balance");
	}
	/**
	 * Data provider method for makeQuestionnaireTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "makeQuestionnaire")
	private Object[][] makeQuestionnaireDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/MakeQuestionnaireTestData.xlsx", testSkin, 4);
	    return(retObjArr);
	}
	/**
	 * Makes questionnaire
	 * @param skin
	 * @param title
	 * @param subTitle
	 * @param confirmMessage
	 */
	@Test(dataProvider = "makeQuestionnaire", priority = 3, dependsOnMethods = {"makeFirstDepositTest"})
	public void makeQuestionnaireTest(String skin, String title, String subTitle, String confirmMessage) {
		Questionnaire questionnaire = new Questionnaire(driver);
		questionnaire.makeQuestionnaire(title, subTitle, confirmMessage);
	}
	/**
	 * Data provider method for withDrawDepositTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "withdrawDeposit")
	private Object[][] withdrawDepositDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/WithdrawDepositTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Withdraw deposit
	 * @param dearMess
	 * @param reasonMess
	 * @param thanksMess
	 * @param finalMess
	 */
	@Test(dataProvider = "withdrawDeposit", priority = 4, dependsOnMethods = "makeQuestionnaireTest")
	public void withdrawDepositTest(String skin, String dearMess, String reasonMess, String thanksMess, String finalMess) {
		WithdrawDeposit withdrawDeposit = new WithdrawDeposit(driver, browserType, currentBalance, user);
		currentBalance = withdrawDeposit.makeWithdrawDeposit(dearMess, reasonMess, thanksMess, finalMess);
	}
	/**
	 * Data provider method for reverseWithdrawTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "reverseWithdraw")
	private Object[][] reverseWithdrawDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/ReverseWithdrawTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Reverses withdraw
	 * @param skin
	 * @param descMess
	 * @param succMessage
	 * @param descMessNext
	 * @param type
	 * @throws IOException 
	 */
	@Test(dataProvider = "reverseWithdraw", priority = 5, dependsOnMethods = {"withdrawDepositTest"})
	public void reverseWithdrawTest(String skin, String descMess, String succMessage, String descMessNext, String type) throws IOException {
		ReverseWithdraw reverseWithdraw = new ReverseWithdraw(driver, browserType, currentBalance);
		currentBalance = reverseWithdraw.reverseWithdraw(descMess, succMessage, descMessNext, type);
	}
	/**
	 * Data provider method for depositWithExistingCardTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "depositWithExistingCard")
	private Object[][] depositWithExistingCardDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/DepositWithExistedCardTestData.xlsx", testSkin, 3);
	    return(retObjArr);
	}
	/**
	 * Makes deposit with existing card
	 * @param skin
	 * @param succesMessage
	 * @param amountMess
	 */
	@Test(dataProvider = "depositWithExistingCard", priority = 6, dependsOnMethods = {"reverseWithdrawTest"})
	public void depositWithExistingCardTest(String skin, String succesMessage, String amountMess) {
		DepositWithExistingCard depositWithExistingCard = new DepositWithExistingCard(driver, browserType, testSkin, currentBalance);
		currentBalance = depositWithExistingCard.makeDepositWithExistingCard(succesMessage, amountMess);
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "cancelInvestment")
	private Object[][] cancelInvestmentDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/CancelInvestmentTestData.xlsx", testSkin, 5);
	    return(retObjArr);
	}
	/**
	 * Makes cancel investment
	 * @param skin
	 * @param canceledMess
	 * @param feeMess
	 * @param linkText
	 * @param optionStatus
	 */
	@Test(dataProvider = "cancelInvestment", priority = 7, dependsOnMethods = {"depositWithExistingCardTest"})
	public void cancelInvestmentTest(String skin, String canceledMess, String feeMess, String linkText, String optionStatus) {
		CancelInvestment cancelInvestment = new CancelInvestment(driver, browserType, currentBalance);
		currentBalance = cancelInvestment.makeCancelInvestment(canceledMess, feeMess, linkText, optionStatus);
	}
	/**
	 * Data provider method for makeInvestmentTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "makeInvestment")
	private Object[][] makeInvestmentDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/TradeMySelfTestData.xlsx", testSkin, 4);
	    return(retObjArr);
	}
	/**
	 * Makes binary option investment
	 * @param skin
	 * @param popUpText1
	 * @param popUpText2
	 * @param headerMess
	 */
	@Test(dataProvider = "makeInvestment", priority = 8, dependsOnMethods = {"cancelInvestmentTest"})
	public void makeInvestmentTest(String skin, String popUpText1, String popUpText2, String headerMess) {
		Investment investment = new Investment(driver, browserType, testSkin, currentBalance);
		currentBalance = investment.makeInvestment(popUpText1, popUpText2, headerMess);
	}
	/**
	 * Data provider method for watchUserTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "watchUser")
	private Object[][] watchUserDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/WatchTestData.xlsx", testSkin, 8);
	    return(retObjArr);
	}
	/**
	 * Watches user
	 * @param skin
	 * @param watch
	 * @param watchMessage
	 * @param title
	 * @param chBoxMess
	 * @param popUpTitle
	 * @param popUpText1
	 * @param popUpText2
	 */
	@Test(dataProvider = "watchUser", priority = 9, dependsOnMethods = {"makeInvestmentTest"})
	public void watchUserTest(String skin, String watch, String watchMessage, String title, String chBoxMess, String popUpTitle, 
			String popUpText1, String popUpText2) {
		UserWatch userWatch = new UserWatch(driver, browserType);
		userWatch.watchUser(watch, watchMessage, title, chBoxMess, popUpTitle, popUpText1, popUpText2);
	}
	/**
	 * Data provider method for copyUserTest
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "copyUser")
	private Object[][] copyUserDataProvider() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/CopyTestData.xlsx", testSkin, 7);
	    return(retObjArr);
	}
	/**
	 * Copies user
	 * @param skin
	 * @param popUpText1
	 * @param popUpText2
	 * @param popUpText3
	 * @param popUpText4
	 * @param alreadyWatched
	 * @param unCopy
	 */
	@Test(dataProvider = "copyUser", priority = 10, dependsOnMethods = {"watchUserTest"})
	public void copyUserTest(String skin, String popUpText1, String popUpText2, String popUpText3, String popUpText4, 
			String alreadyWatched, String unCopy) {
		UserCopy userCopy = new UserCopy(driver, browserType);
		userCopy.copyUser(popUpText1, popUpText2, popUpText3, popUpText4, alreadyWatched, unCopy);
	}
}
