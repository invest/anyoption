package com.copyop.autotest.questionnaire;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.LoginPage;
import com.copyop.autotest.pages.QuestionnairePage;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.ReadTestData;
import com.copyop.autotest.utils.UserSQL;
import com.copyop.autotest.utils.UtilsMethods;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class MakeQuestionnaireTest extends BaseTest {
	public MakeQuestionnaireTest() throws IOException {
		super();
	}
	/**
	 * Data provider method
	 * @return
	 * @throws Exception
	 */
	@DataProvider(name = "dp")
	public Object[][] createData() throws Exception {
		ReadTestData read = new ReadTestData();
		Object[][] retObjArr = read.getDataFromExcelFile("testdata/MakeQuestionnaireTestData.xlsx", numberRows, 4);
	    return(retObjArr);
	}
	/**
	 * Makes questionnaire
	 * @param skin
	 * @param title
	 * @param subTitle
	 * @param confirmMessage
	 * @throws Exception
	 */
	@Test(dataProvider = "dp")
	public void makeQuestionnaireTest(String skin, String title, String subTitle, String confirmMessage) throws Exception {
		afterMethod = false;
		parameterSkin = skin;
		LoginPage loginPage = new LoginPage(driver, browserType);
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		String skinId = UtilsMethods.getSkinId(skin);
		String sqlQuery = UserSQL.getUserWhoCanMakeQuestionnaire(skinId);
		HashMap<String, String> data = loginPage.loginWithUserFromDB(sqlQuery, false);
		System.out.println(data);
		currentUserId = data.get("id");
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		int value = 1;
		boolean dissabledNextButton = questionnairePage.isNextButtonEnabled();
		if (! dissabledNextButton) {
			for (int i = 1; i < 6; i++) {
				questionnairePage.select(i, value);
			}
		}	
		if (questionnairePage.moveNext() == false) {
			for (int i = 3; i < 6; i++) {
				questionnairePage.select(i, value + 1);
			}
			questionnairePage.moveNext();
		}
		
		boolean disabledTradeNowButton = questionnairePage.isTradeNowButtonEnabled();
		if (! disabledTradeNowButton) {
			for (int i = 6; i < 10; i++) {
				questionnairePage.select(i, value + 1);
			}
			if (questionnairePage.isRiskCheckBoxChecked() == false) {
				questionnairePage.clickRiskCheckBox();
			}
		}	
		Assert.assertEquals(true, questionnairePage.isRiskCheckBoxChecked());
		questionnairePage.tradeNow();
		Assert.assertEquals(confirmMessage, questionnairePage.getConfirmCheckBoxMessage());
		questionnairePage.clickConfirmCheckBox();
		questionnairePage.confirm();
		Assert.assertTrue(questionnairePage.isPopUpInvisible());
	}
	/**
	 * Logout
	 * @param result
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void logout(ITestResult result, @Optional String browser) throws IOException {
		HomePage homePage = new HomePage(driver);
		afterMethod = homePage.logout(result, parameterSkin, browser);
	}
}
