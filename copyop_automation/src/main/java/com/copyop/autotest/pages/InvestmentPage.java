package com.copyop.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class InvestmentPage {
	private WebDriver driver;
	private String browserType;
	@FindBy(id = "tradeBox_inv_amount_0")
	private WebElement investDropDownElement;
	@FindBys({@FindBy(css = "#tradeBox_invest_drop_ul_0 > li")})
	private List<WebElement> investmentAmountOptions;
	@FindBy(id = "tradeBox_profit_drop_amount_0")
	private WebElement profitDropDownElement;
	@FindBys({@FindBy(css = "#tradeBox_profit_drop_ul_0 > li")})
	private List<WebElement> profitList;
	@FindBy(id = "tradeBox_profit_procent_0")
	private WebElement profitLabel;
	@FindBy(id = "tradeBox_profit_amount_0")
	private WebElement returnIfCorrectLabel;
	@FindBy(id = "tradeBox_incorrect_amount_0")
	private WebElement returnIfIncorrectLabel;
	@FindBys({@FindBy(css = "#leftColumHeight > div.scroll-content > div > div > div.assets-list > ul > li > a > div.assets-state > span:nth-child(2)")})
	private List<WebElement> activeMarketsTimer;
	@FindBy(id = "tradeBox_call_btn_0")
	private WebElement upButton;
	@FindBy(id = "tradeBox_after_invest_0")
	private WebElement afterInvestPopUp;
	@FindBy(id = "tradeBox_AI_amount_0")
	private WebElement afterInvestAmountInPopUp;
	@FindBy(id = "tradeBox_AI_correct_0")
	private WebElement correctAmountInAfterInvestPopUp;
	@FindBy(id = "tradeBox_AI_incorrect_0")
	private WebElement incorrectAmountInAfterInvestPopUp;
	@FindBy(css = "#tradeBox_after_invest_0 > div.tradeBox_slipHeader > div.tradeBox_slipHeader_2.ng-binding")
	private WebElement marketNameFormAfterInvestPopUp;
	@FindBy(id = "tradeBox_marketName_0")
	private WebElement marketName;
	@FindBy(id = "profitLine_total_inv_val_0")
	private WebElement totalInvestmentLabel;
	@FindBy(css = "#tradeBox_after_invest_0 > div.tradeBox_slipHeader > div.tradeBox_slipHeader_1.ng-binding")
	private WebElement afterInvestHeaderLabel;
	@FindBy(id = "tradeBox_error_footer_0")
	private WebElement errorFooterElement;
	@FindBy(id = "tradeBox_invest_drop_custom_0")
	private WebElement customInvestInput;
	@FindBy(id = "tradeBox_error_footer_txt_0")
	private WebElement footerErrorText;
	@FindBy(id = "tradeBox_error_footer_close_0")
	private WebElement footerErrorCloseButton;
	@FindBy(id = "tradeBox_put_btn_0")
	private WebElement downButton;
	@FindBy(css = "#tradeBox_pop_noDeposit_0 > span.ng-binding")
	private WebElement noDepistMessage;
	@FindBy(css = "#tradeBox_pop_noDeposit_0 > a")
	private WebElement noDepoitPopUpDepositButtton;
	@FindBy(id = "tradeBox_pop_noDeposit_close_0")
	private WebElement noDepositPopUpCloseButton;
	@FindBy(id = "tradeBox_AI_level_0")
	private WebElement levelFromAfterInvestmentPopUp;
	@FindBy(id = "tradeBox_AI_option_id_0")
	private WebElement optionIdFromAfterInvestmentPopUp;
	@FindBy(id = "chart-overlays_0")
	private WebElement cancelInvPopUp;
	@FindBy(id = "cancel-inv-level_0")
	private WebElement cancelInvLevel;
	@FindBy(id = "cancel-inv-amount_0")
	private WebElement cancelInvAmount;
	@FindBy(id = "cancel-inv-time-left-sec_0")
	private WebElement cancelInvLeftTime;
	@FindBy(id = "cancelInvText_0")
	private WebElement cancelInvFeeMessage;
	@FindBy(id = "submit-link_0")
	private WebElement cancelInvSubmitLink;
	@FindBy(css = "#cancel-inv-msg-slip_0 > div.text.ng-binding")
	private WebElement canceledInvMessage;
	@FindBy(id = "cancel-cool-close_0")
	private WebElement closeCancelInvPopUp;
	@FindBy(css = "#tradeBox_main_0 > div.trading-title.ng-scope > div:nth-child(1) > img")
	private WebElement currentMarketImage;
	@FindBys({@FindBy(css = "#leftColumHeight > div.scroll-content > div > div > div.assets-list > ul > li > a > div.assets-name.ng-binding")})
	private List<WebElement> marketNames;
	@FindBy(id = "tradeBox_time_invest_d_0")
	private WebElement timeLeftToInvestCurrentMarket;
	/**
	 * 
	 * @param driver
	 */
	public InvestmentPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Chooses market by index
	 * @param index
	 */
	public void chooseMarketByIndex(int index) {
		marketNames.get(index).click();
	}
	/**
	 * Is current market active
	 * @return boolean
	 */
	public boolean isMarketActive() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					String time = timeLeftToInvestCurrentMarket.getText();
					if (time.equals("")) {
						return false;
					} else {
						return true;
					}
				}
			});
		} catch (TimeoutException e) {
			return false;
		}
		String timeLeft = timeLeftToInvestCurrentMarket.getText();
		String times[] = timeLeft.split(":");
		String mins = times[0];
		String seconds = times[1];
		int secs = (Integer.parseInt(mins) * 60) + Integer.parseInt(seconds);
		if (secs > 180) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets markets count
	 * @return Integer
	 */
	public int getMarketsCount() {
		return marketNames.size();
	}
	/**
	 * Gets current market id
	 * @return String
	 */
	public String getCurrentMarketId() {
		String src = currentMarketImage.getAttribute("ng-src");
		String arr[] = src.split("/");
		String id = arr[arr.length - 1].replaceAll("[^\\d]", "");
		return id;
	}
	/**
	 * Closes cancel investment pop up
	 */
	public void closesCancelInvPopUp() {
		closeCancelInvPopUp.click();
	}
	/**
	 * Gets cancel investment message
	 * @return String
	 */
	public String getCanceledInvMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				if (canceledInvMessage.getText().equals("")) {
					return false;
				} else {
					return true;
				}
			}
		});
		return canceledInvMessage.getText();
	}
	/**
	 * Shows cancel investment pop up
	 */
	public void showCancelInvPopUp() {
		String script = "document.getElementById('chart-overlays_0').setAttribute('class', 'chart-overlays cancel-inv')";
		((JavascriptExecutor) driver).executeScript(script);
	}
	/**
	 * Makes double click to cancel investment
	 */
	public void doubleClickCancelInvLink() {
		Actions action = new Actions(driver);
  		action.doubleClick(cancelInvSubmitLink).perform();
	}
	/**
	 * Cancels investment
	 */
	public void cancelInvestment() {
		cancelInvSubmitLink.click();
	}
	/**
	 * Gets cancel investment submit link text
	 * @return String
	 */
	public String getCancelInvLinkText() {
		return cancelInvSubmitLink.getText();
	}
	/**
	 * Gets cancel investment fee message
	 * @return String
	 */
	public String getCancelInvFeeMessage() {
		return cancelInvFeeMessage.getText();
	}
	/**
	 * Gets time left in seconds
	 * @return Integer
	 */
	public int getCancelInvLeftTime() {
		String timeText = cancelInvLeftTime.getText();
		String times[] = timeText.split(":");
		String secs = times[1];
		int timeSecs = Integer.parseInt(secs);
		return timeSecs;
	}
	/**
	 * Gets cancel investment amount
	 * @return String
	 */
	public String getCancelInvAmount() {
		return cancelInvAmount.getText();
	}
	/**
	 * Gets cancel investment level
	 * @return String
	 */
	public String getCancelInvLevel() {
		return cancelInvLevel.getText();
	}
	/**
	 * Is cancel investment displayed
	 * @return boolean
	 */
	public boolean isCancelInvestmentDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.attributeToBe(cancelInvPopUp, "class", "chart-overlays cancel-inv"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets option id
	 * @return
	 */
	public String getInvestmentId() {
		return optionIdFromAfterInvestmentPopUp.getText();
	}
	/**
	 * Gets level
	 * @return String
	 */
	public String getLevelFromAfterInvest() {
		return levelFromAfterInvestmentPopUp.getText();
	}
	/**
	 * Closes no deposit pop up
	 */
	public void closeNoDepostiPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		noDepositPopUpCloseButton.click();
		wait.until(ExpectedConditions.attributeToBe(By.id("tradeBox_pop_holder_0"), "class", "tradeBox_pop_holder hidden"));
	}
	/**
	 * Clicks deposit button in no deposit pop up
	 */
	public void clickNoDepositPopUpButton() {
		noDepoitPopUpDepositButtton.click();
	}
	/**
	 * Gets no deposit message
	 * @return
	 */
	public String getNoDepositMessage() {
		String message = noDepistMessage.getText();
		message = message.replace("\n", " ");
		return message;
	}
	/**
	 * Is deposit pop up displayed
	 * @return
	 */
	public boolean isDepositPopUpDisplayed(String type) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		boolean result = false;
		for (int i = 0; i < 3; i++) {
			try {
				wait.until(ExpectedConditions.attributeToBe(By.id("tradeBox_pop_holder_0"), "class", "tradeBox_pop_holder noDeposit"));
				result = true;
			} catch (TimeoutException e) {
				System.out.println("time out");
			}
			if (result == true) {
				break;
			} else {
				if (type.equals("UP")) {
					this.clickUpButton();
				} else if (type.equals("DOWN")) {
					this.clickDownButton();
				}
			}
		}
		return result;
	}
	/**
	 * Clicks down button
	 */
	public void clickDownButton() {
		downButton.click();
	}
	/**
	 * Closes footer error 
	 */
	public void closeFooterError() {
		footerErrorCloseButton.click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.attributeToBe(errorFooterElement, "style", "display: none;"));
	}
	/**
	 * Gets footer error message text
	 * @return
	 */
	public String getFooterErrorText() {
		return footerErrorText.getText();
	}
	/**
	 * Is footer message displayed
	 * @return
	 */
	public boolean isFooterMessageDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.attributeContains(errorFooterElement, "style", "display: block;"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Gets header text from after invest pop up
	 * @return String
	 */
	public String getHeaderTextFromAfterInvestPopUp() {
		return afterInvestHeaderLabel.getText();
	}
	/**
	 * Gets total investment amount
	 * @return String
	 */
	public String getTotalInvestmentAmount() {
		WebDriverWait wait = new WebDriverWait(driver, 45);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String am = totalInvestmentLabel.getText();
				if (! am.contains("0.00")) {
					return true;
				} else {
					return false;
				}
			}
		});
		return totalInvestmentLabel.getText();
	}
	/**
	 * Gets market name
	 * @return String
	 */
	public String getMarketName() {
		return marketName.getText();
	}
	/**
	 * Gets market name from after invest pop up
	 * @return String
	 */
	public String getMarketNameFromAfterInvestPopUp() {
		return marketNameFormAfterInvestPopUp.getText();
	}
	/**
	 * Gets amount to return if incorrect from after invest pop up
	 * @return Integer
	 */
	public int getIncorrectAmountFromAfterInvestPopUp() {
		String amount = incorrectAmountInAfterInvestPopUp.getText();
		amount = amount.replaceAll("[^\\d]", "");
		return Integer.parseInt(amount);
	}
	/**
	 * Gets amount to return if correct from after invest pop up
	 * @return Integer
	 */
	public int getCorrectAmountFromAfterInvestPopUp() {
		String amount = correctAmountInAfterInvestPopUp.getText();
		amount = amount.replaceAll("[^\\d]", "");
		return Integer.parseInt(amount);
	}
	/**
	 * Gets amount in after invest pop up
	 * @return String
	 */
	public String getAmountFormAfterInvestPopUp() {
		return afterInvestAmountInPopUp.getText();
	}
	/**
	 * Is after investment pop up displayed
	 * @return Boolean
	 */
	public boolean afterInvest() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		boolean result = false;
		for (int i = 0; i < 5; i ++) {
			try {
				wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						if (afterInvestPopUp.getAttribute("style").equals("display: block;")) {
							return true;
						} else {
							return false;
						}	
					}
				});
				result = true;
			} catch (TimeoutException e) {
				System.out.println("Error investment");
				this.clickProfitDropDown();
				profitList.get(1).click();
				this.clickUpButton();
			}
		}	
		return result;
	}
	/**
	 * Clicks up button
	 */
	public void clickUpButton() {
		try {
			upButton.click();
		} catch (WebDriverException e) {
			System.out.println("Light streem refresh");
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.attributeToBe(By.id("tradeBox_pop_holder_0"), "class", "tradeBox_pop_holder hidden"));
			upButton.click();
		}
	}
	/**
	 * Sets active market
	 */
	public void setActiveMarket() {
		int size = activeMarketsTimer.size();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		for (int i = 0; i < size; i++) {
			final int index = i;
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String text = "";
					try {
						text = activeMarketsTimer.get(index).getText();
					} catch (StaleElementReferenceException e) {
						System.out.println("exception");
					}
					if (text.equals("")) {
						return false;
					} else {
						return true;
					}	
				}
			});
			String time = activeMarketsTimer.get(i).getText();
			int seconds = 0;
			String [] times = time.split(":");
			seconds = Integer.parseInt(times[0]) * 60 + Integer.parseInt(times[1]);
			
			if (seconds > 180) {
				if (i > 0) {
					activeMarketsTimer.get(i).click();
					try {
						wait.until(new ExpectedCondition<Boolean>() {
							@Override
							public Boolean apply(WebDriver driver) {
								String text = returnIfCorrectLabel.getText();
								if (text.contains("0000")) {
									return false;
								} else {
									return true;
								}
							}
						});
						break;
					} catch (TimeoutException e) {
						System.out.println("Don`t stop the loop it is proxy problem");
					}
				} else {	
					try {
						wait.until(new ExpectedCondition<Boolean>() {
							@Override
							public Boolean apply(WebDriver driver) {
								String text = returnIfCorrectLabel.getText();
								if (text.contains("0000")) {
									return false;
								} else {
									return true;
								}
							}
						});
						System.out.println("don`t change the market");
						break;
					} catch (TimeoutException e) {
						System.out.println("Don`t stop the loop it is proxy problem");
					}
				}	
			}
		}
		UtilsMethods.sleep(2500);
	}
	/**
	 * Gets return value if incorrect
	 * @return Integer
	 */
	public int getReturnValueIfIncorrect() {
		String value = returnIfIncorrectLabel.getText();
		value = value.replaceAll("[^\\d]", "");
		return Integer.parseInt(value);
	}
	/**
	 * Gets value to return if correct
	 * @return Integer
	 */
	public int getReturnValueIfCorrect() {
		String value = returnIfCorrectLabel.getText();
		value = value.replaceAll("[^\\d]", "");
		return Integer.parseInt(value);
	}
	/**
	 * Gets the profit from drop down
	 * @return String
	 */
	public String getTheProfit() {
		return profitLabel.getText();
	}
	/**
	 * Chooses profit from drop down
	 * @param index
	 * @return String[]
	 */
	public String[] chooseProfit(int index) {
		String pr[] = new String[2];
		index = index + 1;
		if (profitList.size() != 1) {
			pr[0] = profitList.get(index).getAttribute("data-profit");
			pr[1] = profitList.get(index).getAttribute("data-refund");
			profitList.get(index).click();
		} else {
			pr[0] = "65";
			pr[1] = "15";
		}
		return pr;
	}
	/**
	 * Clicks profit drop down
	 */
	public void clickProfitDropDown() {
		profitDropDownElement.click();
	}
	/**
	 * Gets amount for investing
	 * @return String
	 */
	public String getTheAmountForInvest() {
		return investDropDownElement.getText();
	}
	/**
	 * Chooses invesment amount
	 * @param index
	 */
	public void chooseAmount(int index) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		String id = "tradeBox_pop_holder_0";
		final WebElement holder = driver.findElement(By.id(id));
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				if (holder.getAttribute("class").contains("loading")) {
					return false;
				} else {
					return true;
				}
			}
		});
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tradeBox_inv_amount_0")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("tradeBox_inv_amount_0")));
		try {
			investDropDownElement.click();
		} catch (WebDriverException e) {
			UtilsMethods.sleep(1000);
			investDropDownElement.click();
		}
		investmentAmountOptions.get(index).click();
		if (browserType.equalsIgnoreCase("phantomjs")) {
			UtilsMethods.sleep(1000);
		}
	}
	/**
	 * Sets custom amount for invest
	 * @param value
	 */
	public void setAmount(String value) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		String id = "tradeBox_pop_holder_0";
		final WebElement holder = driver.findElement(By.id(id));
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				if (holder.getAttribute("class").contains("loading")) {
					return false;
				} else {
					return true;
				}
			}
		});
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tradeBox_inv_amount_0")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("tradeBox_inv_amount_0")));
		try {
			investDropDownElement.click();
		} catch (WebDriverException e) {
			UtilsMethods.sleep(1000);
			investDropDownElement.click();
		}
		customInvestInput.sendKeys(value);
		customInvestInput.clear();
		customInvestInput.sendKeys(Keys.ENTER);
		
		if (browserType.equalsIgnoreCase("phantomjs")) {
			UtilsMethods.sleep(1000);
		}
	}
}
