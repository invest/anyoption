package com.copyop.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class FeedPage {
	private static final String INVESTMENT_POP_UP_CSS = "#walkthrough > div.walkthrough-menu.tr-page-small > div > div.bottom-part";
	private static final String feedPopUpCss = "#walkthrough > div.walkthrough-menu > div.menu-header.ttUC.ng-binding.ng-scope";
	private WebDriver driver;
	@FindBy(css = feedPopUpCss)
	private WebElement feedPopUp;
	@FindBy(css = "#walkthrough > div.walkthrough-menu.small-box > div.menu-header.ttUC.ng-binding.ng-scope")
	private WebElement watchPopUpTitle;
	@FindBy(css = "#walkthrough > div > i")
	private WebElement closeWatchPopUpButton;
	@FindBy(css = "#walkthrough > div > div > div.ng-binding")
	private WebElement popUpText;
	@FindBy(css = "#walkthrough > div > div.menu-body > div.bottom-part > i")
	private WebElement popUpNextButton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-footer.clear > button")
	private WebElement holidayPopUpOkButton;
	/**
	 * 
	 * @param driver
	 */
	public FeedPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Confirms holiday pop up
	 */
	public void confirmHolidayPopUp() {
		try {
			holidayPopUpOkButton.click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", holidayPopUpOkButton);
		}
	}
	/**
	 * Is holiday pop up displayed
	 */
	public boolean isHolidayPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div.popUp-content.information-popUp.holiday-popUp")));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Moves next page
	 */
	public void moveNext() {
		popUpNextButton.click();
		UtilsMethods.sleep(300);
		
	}
	/**
	 * Gets text of pop up
	 * @return String
	 */
	public String getPopUpText() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				if (popUpText.getText().equals("")) {
					return false;
				} else {
					return true;
				}	
			}
		});
		String text = popUpText.getText().replace("\n", "");
		return text;
	}
	/**
	 * Gets watch pop up title
	 * @return String
	 */
	public String getWatchPopUpTitle() {
		return watchPopUpTitle.getText();
	}
	/**
	 * Is watch pop up displayed
	 * @return
	 */
	public boolean isWatchPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.visibilityOf(watchPopUpTitle));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Closes watch pop up
	 */
	public void closePopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#walkthrough > div > i")));
		try {
			closeWatchPopUpButton.click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", closeWatchPopUpButton);
		}
		UtilsMethods.sleep(300);
	}
	/**
	 * Is displayed investment pop up
	 * @return
	 */
	public boolean isInvestmentPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(INVESTMENT_POP_UP_CSS)));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is displayed feed pop up
	 * @return
	 */
	public boolean isFeedPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(feedPopUpCss)));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is displayed no more copy pop up
	 * @return
	 */
	public boolean isCopyPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#walkthrough > div.walkthrough-menu.copy-popup")));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
}
