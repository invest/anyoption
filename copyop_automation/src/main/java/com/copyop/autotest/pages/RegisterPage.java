package com.copyop.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class RegisterPage {
	private static final String DOME_BUTTON_CSS = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) > div:nth-child(5) > button > span";
	private static final String START_NOW_BUTTON_CSS = "#intro > div > div.ng-scope > div > div.desktop-buttons > div > div:nth-child(2) > div > a";
	private static final String firstNameId = "firstName";
	private static final String lastNameId = "lastName";
	private static final String nicknameId = "nickname";
	private static final String emailId = "email";
	private static final String phoneId = "mobilePhone";
	private static final String passwordId = "password";
	private static final String retypePassId = "password2";
	private static final String signUpTitleCss = "#main-container > section > div > h1";
	
	private WebDriver driver;
	private String browserType;
	@FindBy(css = START_NOW_BUTTON_CSS)
	private WebElement startNowButton;
	@FindBy(css = signUpTitleCss)
	private WebElement signUpTitle;
	@FindBy(css = "#main-container > section > div > div > div:nth-child(1) > div:nth-child(3) > p")
	private WebElement signUpScreenMessage;
	@FindBy(id = firstNameId)
	private WebElement firstNameInput;
	@FindBy(id = lastNameId)
	private WebElement lastNameInput;
	@FindBy(id = nicknameId)
	private WebElement nickNameInput;
	@FindBy(id = emailId)
	private WebElement emailInput;
	@FindBy(id = phoneId)
	private WebElement phoneInput;
	@FindBy(id = passwordId)
	private WebElement passwordInput;
	@FindBy(id = retypePassId)
	private WebElement retypePassInput;
	@FindBy(id = "countryId")
	private WebElement countrySelect;
	@FindBy(id = "acceptedTermsAndConditions")
	private WebElement termAndCondCheckBoxInput;
	@FindBy(css = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) > div.row.ng-binding > label")
	private WebElement termAndCondCheckBox;
	@FindBy(css = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(1) > div.row.avatar-box-holder > div.avatar-holder > div.edit.icon")
	private WebElement editAvatarButton;
	@FindBy(css = "#popUp-avatar > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div:nth-child(3)")
	private WebElement femaleAvatarButton;
	@FindBy(css = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(1) > div.row.avatar-box-holder > div.avatar-holder > img")
	private WebElement avatarImage;
	@FindBy(css = DOME_BUTTON_CSS)
	private WebElement doneButton;
	@FindBy(css = "#header > div > div.sub-header.ng-scope > div > nav.header-menu > ul > li:nth-child(1) > a")
	private WebElement startNowLink;
	/**
	 * 
	 * @param driver
	 */
	public RegisterPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Clicks on start now link
	 */
	public void clickStartNowLink() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(START_NOW_BUTTON_CSS)));
		startNowButton.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(DOME_BUTTON_CSS)));
	}
	/**
	 * Clicks I`m done button
	 */
	public void clickDoneButton() {
		if (! browserType.equalsIgnoreCase("firefox")) {
			doneButton.click();
		} else {
			try {
				doneButton.click();
			} catch (WebDriverException e) {
				System.out.println("FF exception");
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", doneButton);
			}
		}
	}
	/**
	 * Gets avatar image
	 * @param type
	 * @return
	 */
	public boolean getAvatarImage(String type) {
		String image = avatarImage.getAttribute("ng-src");
		if (type.equals("female") && image.contains("f")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Clicks female avatar button
	 */
	public void clickFemaleAvatarButton() {
		femaleAvatarButton.click();
	}
	/**
	 * Clicks edit avatar button
	 */
	public void editAvatar() {
		if (browserType.equalsIgnoreCase("firefox")) {
			try {
				editAvatarButton.click();
			} catch (WebDriverException e) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", editAvatarButton);
			}
		} else {
			editAvatarButton.click();
		}	
	}
	/**
	 * Returns is check box checked
	 * @return
	 */
	public boolean isCheckBoxChecked() {
		String className = termAndCondCheckBoxInput.getAttribute("class");
		if (className.contains("ng-valid-required")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Checks term and conditions
	 */
	public void checkTermAndConds() {
		if (browserType.equalsIgnoreCase("firefox")) {
			try {
				termAndCondCheckBox.click();
			} catch (WebDriverException e) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", termAndCondCheckBox);
			}
		} else {
			termAndCondCheckBox.click();
		}	
	}
	/**
	 * Selects country
	 * @param value
	 */
	public void selectCountry(String value) {
		Select select = new Select(countrySelect);
		select.selectByValue(value);
	}
	/**
	 * Retypes password
	 * @param pass
	 */
	public void retypePassword(String pass) {
		retypePassInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = pass.length() - 1;
			String lastChar = String.valueOf(pass.charAt(lastIndex));
			pass = pass.substring(0, lastIndex);
			String script = "document.getElementById('"+ retypePassId + "').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
			retypePassInput.sendKeys(lastChar);
		} else {
			retypePassInput.sendKeys(pass);
		}	
	}
	/**
	 * Sets password
	 * @param pass
	 */
	public void setPassword(String pass) {
		passwordInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = pass.length() - 1;
			String lastChar = String.valueOf(pass.charAt(lastIndex));
			pass = pass.substring(0, lastIndex);
			String script = "document.getElementById('"+ passwordId + "').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
			passwordInput.sendKeys(lastChar);
		} else {
			passwordInput.sendKeys(pass);
		}	
	}
	/**
	 * Sets phone number
	 * @param phone
	 */
	public void setPhone(String phone) {
		phoneInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = phone.length() - 1;
			String lastChar = String.valueOf(phone.charAt(lastIndex));
			phone = phone.substring(0, lastIndex);
			String script = "document.getElementById('"+ phoneId + "').value='" + phone +"'";
			((JavascriptExecutor) driver).executeScript(script);
			phoneInput.sendKeys(lastChar);
		} else {
			phoneInput.sendKeys(phone);
		}	
	}
	/**
	 * Sets email
	 * @param email
	 */
	public void setEmail(String email) {
		emailInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = email.length() - 1;
			String lastChar = String.valueOf(email.charAt(lastIndex));
			email = email.substring(0, lastIndex);
			String script = "document.getElementById('"+ emailId + "').value='" + email +"'";
			((JavascriptExecutor) driver).executeScript(script);
			emailInput.sendKeys(lastChar);
		} else {	
			emailInput.sendKeys(email);
		}	
	}
	/**
	 * Sets nickname 
	 * @param nickName
	 */
	public void setNickName(String nickName) {
		nickNameInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = nickName.length() - 1;
			String lastChar = String.valueOf(nickName.charAt(lastIndex));
			nickName = nickName.substring(0, lastIndex);
			String script = "document.getElementById('"+ nicknameId + "').value='" + nickName +"'";
			((JavascriptExecutor) driver).executeScript(script);
			nickNameInput.sendKeys(lastChar);
		} else {
			nickNameInput.sendKeys(nickName);
		}	
	}
	/**
	 * Sets last name
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		lastNameInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = lastName.length() - 1;
			String lastChar = String.valueOf(lastName.charAt(lastIndex));
			lastName = lastName.substring(0, lastIndex);
			String script = "document.getElementById('"+ lastNameId + "').value='" + lastName +"'";
			((JavascriptExecutor) driver).executeScript(script);
			lastNameInput.sendKeys(lastChar);
		} else {
			lastNameInput.sendKeys(lastName);
		}	
	}
	/**
	 * Sets first name
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		firstNameInput.clear();
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = firstName.length() - 1;
			String lastChar = String.valueOf(firstName.charAt(lastIndex));
			firstName = firstName.substring(0, lastIndex);
			String script = "document.getElementById('"+ firstNameId + "').value='" + firstName +"'" ;
			((JavascriptExecutor) driver).executeScript(script);
			firstNameInput.sendKeys(lastChar);
		} else {
			firstNameInput.sendKeys(firstName);
		}	
	}
	/**
	 * Gets message in sign up screen
	 * @return
	 */
	public String getSignUpScreenMessage() {
		return signUpScreenMessage.getText();
	}
	/**
	 * Gets sign up screen title
	 * @return
	 */
	public String getSignUpTitle() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(signUpTitleCss)));
		return signUpTitle.getText();
	}
	/**
	 * Clicks start now button
	 */
	public void clickStartNowButton() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		startNowButton.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) > div:nth-child(5) > button")));
	}
}
