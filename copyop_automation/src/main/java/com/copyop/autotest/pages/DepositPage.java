package com.copyop.autotest.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.copyop.autotest.utils.Configuration;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositPage {
	private static final String SUCCESS_MESSAGE_CSS = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div "
			+ "> div > div.popUp-box > div.popUp-header.ng-binding";
	private static final String DEPOSIT_INPUT = "deposit";
	private static final String ZIP_CODE_INPUT_ID = "zipCode";
	private static final String CITY_NAME_INPUT_ID = "cityName";
	private static final String STREET_INPUT_ID = "street";
	private static final String HOLDER_NAME_INPUT_ID = "holderName";
	private static final String CC_PASS_INPUT_ID = "ccPass";
	private static final String CARD_NUMBER_INPUT_ID = "ccNumber";
	private static final String AMOUNT_ID = "amount";
	private static final String DEPOSIT_POPUp_CSS = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div "
			+ "> div > div.popUp-header.ng-binding";
	private WebDriver driver;
	private String browserType;
	@FindBy(css = DEPOSIT_POPUp_CSS)
	private WebElement depositPopUp;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > i")
	private WebElement depositPopUpCloseButton;
	@FindBy(id = AMOUNT_ID)
	private WebElement amountInput;
	@FindBy(id = "currencyId")
	private WebElement currencySelect;
	@FindBy(id = CARD_NUMBER_INPUT_ID)
	private WebElement cardNumberInput;
	@FindBy(id = "expMonth")
	private WebElement expMonthSelect;
	@FindBy(id = "expYear")
	private WebElement expYearSelect;
	@FindBy(id = CC_PASS_INPUT_ID)
	private WebElement cvvPassInput;
	@FindBy(id = HOLDER_NAME_INPUT_ID)
	private WebElement holderNameInput;
	@FindBy(id = STREET_INPUT_ID)
	private WebElement streetInput;
	@FindBy(id = CITY_NAME_INPUT_ID)
	private WebElement cityInput;
	@FindBy(id = ZIP_CODE_INPUT_ID)
	private WebElement zipCodeInput;
	@FindBy(id = "birthDayUpdate")
	private WebElement birthDaySelect;
	@FindBy(id = "birthMonthUpdate")
	private WebElement birthMountSelect;
	@FindBy(id = "birthYearUpdate")
	private WebElement birthYearSelect;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > form > div "
			+ "> div:nth-child(2) > div > div.currency-box > div > span")
	private WebElement depositPopUpCurrencyLabel;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > form > "
			+ "div > div:nth-child(9) > button")
	private WebElement submitDepositButton;
	@FindBy(css = SUCCESS_MESSAGE_CSS)
	private WebElement successMessage;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box "
			+ "> div.popUp-body.ng-binding")
	private WebElement amountMessage;
	@FindBy(id = "submitBtn")
	private WebElement submitPopUpButton;
	@FindBy(css = "#body > popups > div:nth-child(2) > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box "
			+ "> div.popUp-header.ng-binding.ng-scope")
	private WebElement autoWatchingTitle;
	@FindBy(css = "#body > popups > div:nth-child(2) > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box "
			+ "> div.popUp-header.ng-binding.ng-scope > span")
	private WebElement autoWatchingMessage;
	@FindBy(css = "#body > popups > div:nth-child(2) > div > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-footer.ng-scope > button")
	private WebElement autoWatchingDoneButton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > form > div "
			+ "> div:nth-child(6) > button")
	private WebElement submitDepositButtonNext;
	@FindBy(css = "#inContent > section > div.content-inner > section.box.panel.active > div > div.ng-scope > ul > li:nth-child(1) > a")
	private WebElement depositExistingCardLink;
	@FindBy(css = "#inContent > section > div.content-inner > section.box.panel.active > div > div:nth-child(3) > div > form "
			+ "> div.compact-box.row > div:nth-child(1) > div > div.currency-box > span")
	private WebElement currencySign;
	@FindBy(id = DEPOSIT_INPUT)
	private WebElement depositAmountInput;
	@FindBy(css = "#inContent > section > div.content-inner > section.box.panel.active > div > div:nth-child(3) > div > form "
			+ "> div.compact-box.row > div:nth-child(2) > button")
	private WebElement submitDepositWithExistedCardButton;
	@FindBy(css = "#inContent > section > div.content-inner > section:nth-child(2) > div > h2")
	private WebElement withdrawLink;
	@FindBy(id = "cardId")
	private WebElement withdrawCardSelect;
	@FindBy(css = "#inContent > section > div.content-inner > section.box.panel.active > div > div > div.ng-scope > div > div > div:nth-child(2) > form > div > div:nth-child(3) > button")
	private WebElement submitWithdrawButton;
	@FindBy(css = "#inContent > section > div.content-inner > section.box.panel.active > div > div > div.ng-scope > div > div > div:nth-child(2) > form > div > div:nth-child(2) > div > div.currency-box > span")
	private WebElement currencySignFromWithdrawScreen;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div > h1")
	private WebElement withDrawPopupTitle;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div > div:nth-child(5) "
			+ "> strong")
	private WebElement withdrawPopUpQuestion;
	@FindBys({@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div "
			+ "> div.field-holder > label > span")})
	private List<WebElement> withdrawPopUpRadioButtons;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div")
	private WebElement thanksFeedbackMessage;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div "
			+ "> div.withdraw_popUp_heading.ng-binding")
	private WebElement importantNotePopUpTitle;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div "
			+ "> div:nth-child(2)")
	private WebElement importantNotePopUpMessage;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-footer.clear "
			+ "> button.button.submit.ng-binding")
	private WebElement confirmButton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div > div")
	private WebElement withdrawFinalMessagePopUp;
	@FindBy(css = "#UserTransactionsCtrlHolder > table > tbody > tr:nth-child(1) > td:nth-child(1) > div.end.ng-scope > button")
	private WebElement reverseWithdrawButton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box "
			+ "> div.popUp-body.ng-binding")
	private WebElement reverseMessage;
	@FindBy(id = "globalErrorField_withrow")
	private WebElement globalErrorWithdrawElement;
	@FindBy(css = "#inContent > section > div.content-inner > section:nth-child(1) > h2")
	private WebElement depositSubTab;
	@FindBy(css = "#body > popups > div:nth-child(2) > div > div > div.popUp-wrapper2.scroll-content > div > div > div > i")
	private WebElement closeAutoWatchingPopUpButton;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 */
	public DepositPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Checks for bonus pop up
	 */
	public void checkForBonusPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("info-popup-container")));
			driver.findElement(By.id("submitBtn")).click();
		} catch (TimeoutException e) {
			System.out.println("No bonus");
		}
	}
	/**
	 * Opens deposit sub tab
	 */
	public void openDeposit() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#inContent > section > div.content-inner > section:nth-child(1) > h2")));
		try {	
			depositSubTab.click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", depositSubTab);
		}
	}
	/**
	 * Gets global error withdraw message text
	 * @return
	 */
	public String getGlobalErrorWithdraw() {
		return globalErrorWithdrawElement.getText();
	}
	/**
	 * Is global error withdraw displayed
	 * @return
	 */
	public boolean isGlobalErrorWithdrawDiplayed() {
		boolean result = false;
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("globalErrorField_withrow")));
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets reverse message
	 * @return
	 */
	public String getReverseMessage() {
		return reverseMessage.getText();
	}
	/**
	 * Clicks reverse button
	 * @throws IOException 
	 */
	public void clickReverseButton() throws IOException {
		if (Configuration.getProperty("operationSystem").equals("Linux")) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", reverseWithdrawButton);
		} else {
			reverseWithdrawButton.click();
		}	
	}
	/**
	 * Gets final message withdraw
	 */
	public String getWithdrawFinalMessage() {
		return withdrawFinalMessagePopUp.getText();
	}
	/**
	 * Confirms withdraw
	 */
	public void confirmsWithdraw() {
		confirmButton.click();
	}
	/**
	 * Gets fee
	 * @param moneyType
	 * @return
	 */
	public String getFeeFromImportantMessage(String moneyType) {
		String message = importantNotePopUpMessage.getText();
//		String mess[] = message.split(moneyType);
		return message.replaceAll("[^\\d]", "").replace("1", "");
	}
	/**
	 * Gets important not pop up title
	 * @return
	 */
	public String getImportantPopUpTitle() {
		return importantNotePopUpTitle.getText();
	}
	/**
	 * Gets thanks message
	 * @return
	 */
	public String getThanksMess() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div")));
		return thanksFeedbackMessage.getText();
	}
	/**
	 * Chooses radio button
	 * @param index
	 */
	public void chooseRadioButton(int index) {
		withdrawPopUpRadioButtons.get(index).click();
	}
	/**
	 * Gets withdraw pop up question
	 * @return
	 */
	public String getWithdrawPopUpQuestion() {
		return withdrawPopUpQuestion.getText();
	}
	/**
	 * Gets withdraw pop up title
	 * @return
	 */
	public String getWithdrawPopUpTitle() {
		return withDrawPopupTitle.getText();
	}
	/**
	 * Gets currency from withdraw screen
	 * @return
	 */
	public String getCurrencyFromWithdrawScreen() {
		return currencySignFromWithdrawScreen.getText();
	}
	/**
	 * Submits withdraw
	 */
	public void submitWithdraw() {
		submitWithdrawButton.click();
	}
	/**
	 * Selects card to withdraw
	 * @param index
	 */
	public void selectCardToWithdraw(String value) {
		Select select = new Select(withdrawCardSelect);
		select.selectByValue(value);
	}
	/**
	 * Clicks withdraw link
	 */
	public void clickWithdrawLink() {
		withdrawLink.click();
	}
	/**
	 * Waits for error when register new card
	 */
	public void waitError() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#globalErrorFieldPopup > span")));
	}
	/**
	 * Submit deposit with existed card
	 */
	public void submitDepositWithExistedCard() {
		submitDepositWithExistedCardButton.click();
	}
	/**
	 * Gets currency from deposit screen
	 * @return String
	 */
	public String getCurrencyFromDepositWithExistCard() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver arg0) {
				String sign = currencySign.getText();
				if (sign.equals("")) {
					return false;
				} else {
					return true;
				}	
			}
		});
		String sign = currencySign.getText();
		return sign;
	}
	/**
	 * Is link for deposit with existing card current
	 * @return
	 */
	public boolean isDepositExistingCardCurrent() {
		String className = depositExistingCardLink.getAttribute("class");
		if (className.contains("current")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Submits deposit after allowing the credit card
	 */
	public void submitDepositNext() {
		submitDepositButtonNext.click();
	}
	/**
	 * Clicks on Done button in auto watching pop up
	 */
	public void clickAutoWatchingDoneButton() {
		autoWatchingDoneButton.click();
	}
	/**
	 * Closes auto watch pop up
	 */
	public void closeAutoWatchPopUp() {
		try {
			closeAutoWatchingPopUpButton.click();
		} catch (WebDriverException e) {
			System.out.println("Exception for closing pop up");
			Actions action = new Actions(driver);
			action.sendKeys(Keys.ESCAPE);
		}
	}
	/**
	 * Gets auto watching pop up message
	 * @return
	 */
	public String getAutoWatchingMessage() {
		return autoWatchingMessage.getText();
	}
	/**
	 * Gets auto watching pop up title
	 * @return
	 */
	public String getAutoWatchingPopUpTitle() {
		String text = autoWatchingTitle.getText();
		return text;
	}
	/**
	 * Submits pop up
	 */
	public void submitPopUp() {
		submitPopUpButton.click();
	}
	/**
	 * Gets amount message
	 * @return
	 */
	public String getAmountMessage() {
		return amountMessage.getText();
	}
	/**
	 * Gets success message
	 * @return
	 */
	public String getSuccesMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 45);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(SUCCESS_MESSAGE_CSS)));
		return successMessage.getText();
	}
	/**
	 * Is pop up with message displayed
	 * @return
	 */
	public boolean isMessagePopUpDisplayed() {
		boolean flag = true;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOf(successMessage));
		} catch (TimeoutException e) {
			flag = false;
		}
		return flag;
	}
	/**
	 * Submits deposit
	 */
	public void submitDeposit() {
		submitDepositButton.click();
	}
	/**
	 * Gets current currency
	 * @return
	 */
	public String getCurrentCurrency() {
		return depositPopUpCurrencyLabel.getText();
	}
	/**
	 * Selects birth year
	 * @param text
	 */
	public void selectBirthYear(String text) {
		Select select = new Select(birthYearSelect);
		select.selectByVisibleText(text);
	}
	/**
	 * Selects birth mount
	 * @param index
	 */
	public void selectBirthMount(int index) {
		Select select = new Select(birthMountSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects birth day
	 * @param text
	 */
	public void selectBirthDay(String text) {
		Select select = new Select(birthDaySelect);
		select.selectByVisibleText(text);
	}
	/**
	 * Sets zip code
	 * @param zip
	 */
	public void setZipCode(String zip) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = zip.length() - 1;
			String lastChar = String.valueOf(zip.charAt(lastIndex));
			zip = zip.substring(0, lastIndex);
			zipCodeInput.click();
			zipCodeInput.clear();
			String script = "document.getElementById('" + ZIP_CODE_INPUT_ID + "').value='" + zip +"'";
			((JavascriptExecutor) driver).executeScript(script);
			zipCodeInput.sendKeys(lastChar);
		} else {
			zipCodeInput.sendKeys(zip);
		}	
	}
	/**
	 * Sets city
	 * @param city
	 */
	public void setCity(String city) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = city.length() - 1;
			String lastChar = String.valueOf(city.charAt(lastIndex));
			city = city.substring(0, lastIndex);
			cityInput.click();
			cityInput.clear();
			String script = "document.getElementById('" + CITY_NAME_INPUT_ID + "').value='" + city +"'";
			((JavascriptExecutor) driver).executeScript(script);
			cityInput.sendKeys(lastChar);
		} else {
			cityInput.sendKeys(city);
		}	
	}
	/**
	 * Sets street
	 * @param street
	 */
	public void setStreet(String street) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			streetInput.click();
			streetInput.clear();
			int lastIndex = street.length() - 1;
			String lastChar = String.valueOf(street.charAt(lastIndex));
			street = street.substring(0, lastIndex);
			String script = "document.getElementById('" + STREET_INPUT_ID + "').value='" + street +"'";
			((JavascriptExecutor) driver).executeScript(script);
			streetInput.sendKeys(lastChar);
		} else {
			streetInput.sendKeys(street);
		}	
	}
	/**
	 * Sets holder name
	 * @param name
	 */
	public void setHolderName(String name) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = name.length() - 1;
			String lastChar = String.valueOf(name.charAt(lastIndex));
			name = name.substring(0, lastIndex);
			String script = "document.getElementById('" + HOLDER_NAME_INPUT_ID + "').value='" + name +"'";
			((JavascriptExecutor) driver).executeScript(script);
			holderNameInput.sendKeys(lastChar);
		} else {
			holderNameInput.sendKeys(name);
		}	
	}
	/**
	 * Sets cvv password
	 * @param cvv
	 */
	public void setCvv(String cvv) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = cvv.length() - 1;
			String lastChar = String.valueOf(cvv.charAt(lastIndex));
			cvv = cvv.substring(0, lastIndex);
			String script = "document.getElementById('" + CC_PASS_INPUT_ID + "').value='" + cvv +"'";
			((JavascriptExecutor) driver).executeScript(script);
			cvvPassInput.sendKeys(lastChar);
		} else {
			cvvPassInput.sendKeys(cvv);
		}	
	}
	/**
	 * Selects expiry year
	 * @param index
	 */
	public void selectExpYear(int index) {
		Select select = new Select(expYearSelect);
		select.selectByIndex(index);
	}
	/**
	 * Selects expiry month
	 * @param index
	 */
	public void selectExpMonth(int index) {
		Select select = new Select(expMonthSelect);
		select.selectByIndex(index);
	}
	/**
	 * Sets card number
	 * @param number
	 */
	public void setCardNumber(String number) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = number.length() - 1;
			String lastChar = String.valueOf(number.charAt(lastIndex));
			number = number.substring(0, lastIndex);
			String script = "document.getElementById('" + CARD_NUMBER_INPUT_ID + "').value='" + number +"'";
			((JavascriptExecutor) driver).executeScript(script);
			cardNumberInput.sendKeys(lastChar);
		} else {
			cardNumberInput.sendKeys(number);
		}	
	}
	/**
	 * Selects currency
	 * @param text
	 */
	public void selectCurrency(int index) {
		Select select = new Select(currencySelect);
		select.selectByIndex(index);
	}
	/**
	 * Sets amount for deposit with new card or amount for withdraw
	 * @param amount
	 */
	public void setAmountForDeposit(String amount) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			amountInput.clear();
			int lastIndex = amount.length() - 1;
			String lastChar = String.valueOf(amount.charAt(lastIndex));
			amount = amount.substring(0, lastIndex);
			String script = "document.getElementById('" + AMOUNT_ID + "').value='" + amount +"'";
			((JavascriptExecutor) driver).executeScript(script);
			amountInput.sendKeys(lastChar);
		} else {
			amountInput.clear();
			amountInput.sendKeys(amount);
		}	
	}
	/**
	 * Sets amount for deposit with new card
	 * @param amount
	 */
	public void setAmountForDepositWithExistedCard(String amount) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			depositAmountInput.clear();
			int lastIndex = amount.length() - 1;
			String lastChar = String.valueOf(amount.charAt(lastIndex));
			amount = amount.substring(0, lastIndex);
			String script = "document.getElementById('" + DEPOSIT_INPUT + "').value='" + amount +"'";
			((JavascriptExecutor) driver).executeScript(script);
			depositAmountInput.sendKeys(lastChar);
		} else {
			depositAmountInput.clear();
			depositAmountInput.sendKeys(amount);
		}	
	}
	/**
	 * Closes deposit pop up
	 */
	public void closeDepositPopUp() {
		depositPopUpCloseButton.click();
	}
	/**
	 * Gets deposit pop up title
	 * @return
	 */
	public String getDepositPopUpTitle() {
		return depositPopUp.getText();
	}
	/**
	 * Is deposit pop up displayed
	 * @return
	 */
	public boolean isDepositPopUpDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(DEPOSIT_POPUp_CSS)));
		if (depositPopUp.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
}
