package com.copyop.autotest.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.Optional;

import com.copyop.autotest.utils.ScreenShot;
import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class HomePage {
	private static final String CHANNGE_LANGUAGE_ELEMENT_CSS = "#header > div > div.header-status.ng-scope > div > div > div";
	private WebDriver driver;
	@FindBy(css = "#header > div > div.clear.header-status.ng-scope > div > div.fL.header-name.ng-binding")
	private WebElement headerNameLabel;
	@FindBy(css = "#main-container > div > div.home-container > aside > div > h1")
	private WebElement sideUserName;
	@FindBy(css = "#header > div > div.clear.sub-header.ng-scope > div > div.fR > div > div > span")
	private WebElement userMenuIcon;
	@FindBy(css = "#header > div > div.clear.header-status.ng-scope > div > div.header-balance.taC > b")
	private WebElement balanceLabel;
	@FindBy(css = "#header > div > div.clear.header-status.ng-scope > div > span")
	private WebElement logoutButton;
	@FindBy(css = CHANNGE_LANGUAGE_ELEMENT_CSS)
	private WebElement currentLanguageElement;
	@FindBy(css = "#header > div > div.sub-header.ng-scope > div > nav.header-menu > a.button.login.ng-binding")
	private WebElement headerLoginButton;
	@FindBy(css = "#header > div > div.clear.sub-header.ng-scope > div > div.fL > ul > li:nth-child(1) > a")
	private WebElement headerDepositButton;
	@FindBy(id = "header-button-trade")
	private WebElement tradeMySelfButton;
	@FindBy(css = "#user-menu-dropdown > div > section > ul > li:nth-child(2) > a")
	private WebElement myOptionLink;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div")
	private WebElement popUpOldInvest;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > i")
	private WebElement popUpOldInvestCloseButton;
	@FindBy(css = "#user-stat-copy-watch > li:nth-child(2) > a > span:nth-child(1)")
	private WebElement watchersLink;
	@FindBy(css = "#user-stat-copy-watch > li:nth-child(2) > a > span:nth-child(2)")
	private WebElement watchersNumber;
	@FindBy(css = "#header > div > div.clear.sub-header.ng-scope > div > div.fR > nav > ul > li:nth-child(2) > a")
	private WebElement socialLink;
	@FindBy(css = "#user-stat-copy-watch > li:nth-child(1) > a > span:nth-child(1)")
	private WebElement copiersLink;
	@FindBy(css = "#header > div > div.clear.sub-header.ng-scope > div > div.fL > ul > li:nth-child(4) > div")
	private WebElement helpIcon;
	/**
	 * 
	 * @param driver
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Clicks help icon
	 */
	public void clickHelpIcon() {
		helpIcon.click();
	}
	/**
	 * Opens copiers
	 */
	public void openCopiers() {
		userMenuIcon.click();
		copiersLink.click();
	}
	/**
	 * Opens social screen
	 */
	public void openSocialScreen() {
		socialLink.click();
	}
	/**
	 * Gets watchers number from my menu
	 * @return Integer
	 */
	public int getWatchersNumber() {
		userMenuIcon.click();
		String number = watchersNumber.getText();
		return Integer.parseInt(number);
	}
	/**
	 * Opens watchers screen
	 */
	public void openWatchers() {
		userMenuIcon.click();
		watchersLink.click();
	}
	/**
	 * Closes pop up for old investment
	 */
	public void closePopUpOldInvest() {
		popUpOldInvestCloseButton.click();
	}
	/**
	 * Is Pop up for old investment displayed
	 * @return boolean
	 */
	public boolean isPopOldInvestDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.visibilityOf(popUpOldInvest));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Opens my options menu
	 */
	public void openMyOptions() {
		userMenuIcon.click();
		myOptionLink.click();
	}
	/**
	 * Opens trade my self screen
	 */
	public void openTradeMySelfScreen() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("header-button-trade")));
		try {
			tradeMySelfButton.click();
		} catch (WebDriverException e) {
			UtilsMethods.sleep(500);
			tradeMySelfButton.click();
		}
	}
	/**
	 * Opens deposit screen
	 */
	public void openDepositScreen() {
		headerDepositButton.click();
	}
	/**
	 * Clicks header login button
	 */
	public void clickHeaderLoginButton() {
		headerLoginButton.click();
	}
	/**
	 * Change language
	 * @param skin
	 */
	public void changeLanguage(String skin) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		final String check = skin.toLowerCase();
		String currentLanguage = currentLanguageElement.getAttribute("class");
		if (! currentLanguage.contains(check)) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(CHANNGE_LANGUAGE_ELEMENT_CSS)));
			currentLanguageElement.click();
			String css = "#header > div > div.header-status.ng-scope > div > div > ul > li." + check + ".icon.flag";
			driver.findElement(By.cssSelector(css)).click();
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String currentLanguage = currentLanguageElement.getAttribute("class");
					if (currentLanguage.contains(check)) {
						return true;
					} else {
						return false;
					}	
				}
			});
		}
	}
	/**
	 * Logout
	 * @param result
	 * @param parameterSkin
	 * @param browser
	 * @return boolean
	 * @throws IOException
	 */
	public boolean logout(ITestResult result, String parameterSkin, @Optional String browser) throws IOException {
		if (ITestResult.FAILURE == result.getStatus()) {
			String brName = "";
			if (browser != null) {
				brName = browser.replace(" ", "");
			}
			String name = result.getMethod().getMethodName();
			String screenShotName = name + parameterSkin + brName;
			ScreenShot scShot = new ScreenShot();
			scShot.takeScreenShot(driver, screenShotName);
			System.out.println("Screenshot generated");
		} else {
			System.out.println("The test pass");
		}
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#header > div > div.clear.header-status.ng-scope > div > span")));
			try {
				logoutButton.click();
			} catch (WebDriverException e) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", logoutButton);
			}
		} catch (WebDriverException e) {
			
		}
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.urlContains("/login"));
		} catch (TimeoutException e) {
			System.out.println("Logout exception");
		}
		UtilsMethods.sleep(300);
		return true;
	}
	/**
	 * Get balance
	 * @return String
	 */
	public String getBalance() {
		return balanceLabel.getText();
	}
	/**
	 * Gets balance
	 * @param oldBalance
	 * @return
	 */
	public String getBalance(final String oldBalance) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				String bal = balanceLabel.getText();
				if (bal.equals(oldBalance)) {
					System.out.println("Balance is not changed");
					return false;
				} else {
					return true;
				}
			}
		});
		return balanceLabel.getText();
	}
	/**
	 * Gets name from user menu icon
	 * @return String
	 */
	public String getUserMenuIconName() {
		return userMenuIcon.getText();
	}
	/**
	 * Gets side user name
	 * @return String
	 */
	public String getSideUserName() {
		return sideUserName.getText();
	}
	/**
	 * Gets header name
	 * @return String
	 */
	public String getHeaderName() {
		return headerNameLabel.getText();
	}
}
