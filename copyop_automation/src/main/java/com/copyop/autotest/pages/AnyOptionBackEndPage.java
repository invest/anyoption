package com.copyop.autotest.pages;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.copyop.autotest.utils.Configuration;
import com.copyop.autotest.utils.CreateUser;
import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class AnyOptionBackEndPage {
	private WebDriver driver;
	private String browserType;
	@FindBy(id = "login:j_username")
	private WebElement userNameInput;
	@FindBy(id = "login:j_password")
	private WebElement passInput;
	@FindBy(css = "#login > div.footer.table > div:nth-child(2) > button")
	private WebElement submitButton;
	@FindBy(id = "menuForm:supportbutton")
	private WebElement supportMenuButton;
	@FindBy(id = "userStripForm:emailSearch")
	private WebElement emailInput;
	@FindBy(id = "userStripForm:searchByUserName")
	private WebElement searchButton;
	@FindBy(id = "userStripForm:userName")
	private WebElement userNameInputFormAfter;
	@FindBy(name = "j_id1812350310_1270f185.5")
	private WebElement changeDetails;
	@FindBy(css = "#userForm > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > span")
	private WebElement userNameInChangeDetails;
	@FindBy(id = "userForm:classId")
	private WebElement userTypeSelect;
	@FindBy(css = "#userForm > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td > a")
	private WebElement saveButton;
	@FindBy(id = "menuForm:logoutLink")
	private WebElement logoutLink;
	@FindBy(name = "j_id1812350310_1270f185.2")
	private WebElement deposits;
	@FindBy(name = "deposit:j_id1812350310_1270f039.1")
	private WebElement creditCardsMenu;
	@FindBy(xpath = "//*[@id='cardsForm:data:tbody_element']/tr/td[2]/a")
	private WebElement creditCardNumber;
	@FindBy(id = "cardsForm:cc_blocked")
	private WebElement blockCreditCardCheckBox;
	@FindBy(id = "cardsForm:comment")
	private WebElement commentTextArea;
	@FindBy(id = "cardsForm:btnImage")
	private WebElement submitCreditCardButton;
	@FindBy(css = "#cardsForm > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > span")
	private WebElement cardHolderName;
	/**
	 * 
	 * @param driver
	 */
	public AnyOptionBackEndPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Logins in back end
	 * @throws IOException
	 */
	public void loginInBackEnd() throws IOException {
		String userName = Configuration.getProperty("backEndUser");
		String pass = Configuration.getProperty("backEndPass");
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			userNameInput.click();
			String script = "document.getElementById('login:j_username').value='" + userName +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			userNameInput.sendKeys(userName);
		}	
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			passInput.click();
			String script = "document.getElementById('login:j_password').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			passInput.sendKeys(pass);
		}	
		submitButton.click();
	}
	/**
	 * Makes user tester
	 * @param user
	 */
	public boolean makeTester(CreateUser user) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.name("j_id1812350310_1270f185.5")));
		if (browserType.equalsIgnoreCase("firefox")) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", changeDetails);
		} else {
			changeDetails.click();
		}	
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("content"));
		wait.until(ExpectedConditions.visibilityOf(userNameInChangeDetails));
		if (userNameInChangeDetails.getText().equalsIgnoreCase(user.getEMail())) {
			Select userType = new Select(userTypeSelect);
			userType.selectByVisibleText("Tester");
			UtilsMethods.sleep(5000);
			saveButton.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > table > tbody > tr:nth-child(1) > td > ul > li")));
			driver.switchTo().defaultContent();
			System.out.println("User is made tester");
			return true;
		} else {
			System.out.println("User error");
			return false;
		}
	}
	/**
	 * Searches user in back end
	 * @param user
	 */
	public void searchUser(CreateUser user) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("menuForm:supportbutton")));
		supportMenuButton.click();
		
		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("user_strip"));
		} catch (TimeoutException e) {
			System.out.println("support menu not opened");
			supportMenuButton.click();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("user_strip"));
		}
		
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			String script = "document.getElementById('userStripForm:emailSearch').value='" + user.getEMail() +"'";
			((JavascriptExecutor) driver).executeScript(script);
		} else {
			emailInput.sendKeys(user.getEMail());
		}	
		
		try {
			searchButton.click();
		} catch (WebDriverException e) {
			
		}
		if (! browserType.equalsIgnoreCase("phantomjs")) {
			wait.until(ExpectedConditions.alertIsPresent());
			try {
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				
			}
		}	
		driver.switchTo().defaultContent();
	}
	/**
	 * Logout from back end
	 */
	public void logoutFromBackEnd() {
		logoutLink.click();
	}
	/**
	 * Allows credit card
	 * @param user
	 */
	public boolean allowCard(CreateUser user, String holderName) {
		if (browserType.equalsIgnoreCase("firefox")) {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", deposits);
			jse.executeScript("arguments[0].click();", creditCardsMenu);
		} else {
			deposits.click();
			creditCardsMenu.click();
		}	
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("content"));
		} catch (WebDriverException e) {
			System.out.println("Mot opened iframe");
			driver.switchTo().frame("content");
		}
		creditCardNumber.click();
		
		String cardHolderNameFromBackEnd = cardHolderName.getText();
		if (cardHolderNameFromBackEnd.equals(holderName)) {
			blockCreditCardCheckBox.click();
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					String atr = blockCreditCardCheckBox.getAttribute("checked");
					if (atr == null) {
						return true;
					} else {
						System.out.println("wait");
						return false;
					}
				}
			});
			if (browserType.equalsIgnoreCase("internetexplorer")) {
				String script = "document.getElementById('cardsForm:comment').value='" + "Smoke automation test!" +"'";
				((JavascriptExecutor) driver).executeScript(script);
			} else {
				commentTextArea.sendKeys("Smoke automation test!");
			}	
			submitCreditCardButton.click();
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > table > tbody > tr:nth-child(1) > td > ul > li")));
			driver.switchTo().defaultContent();
			System.out.println("Credit card is allowed");
			return true;
		} else {
			driver.switchTo().defaultContent();
			System.out.println("Wrong user in back end");
			return false;
		}
	}
}
