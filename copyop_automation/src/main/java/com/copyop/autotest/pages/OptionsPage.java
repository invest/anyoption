package com.copyop.autotest.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author vladimir.mladenov
 * This class contains methods for options in trade screen
 */
public class OptionsPage {
	WebDriver driver;
	@FindBy(css = "#inContent > section > div > div:nth-child(4) > div > ul > li:nth-child(2) > span")
	private WebElement settledOptionTab;
	@FindBys({@FindBy(css = "#inContent > section > div > div:nth-child(4) > div > div:nth-child(3) > div > table > tbody > tr > td.first-col "
			+ "> div.pl10.ng-binding > a")})
	private List<WebElement> marketNameSetteledOptions;
	@FindBys({@FindBy(css = "#inContent > section > div > div:nth-child(4) > div > div:nth-child(3) > div > table > tbody > tr "
			+ "> td:nth-child(2) > span.ng-binding")})
	private List<WebElement> settledOptionsPurchased;
	@FindBys({@FindBy(css = "#inContent > section > div > div:nth-child(4) > div > div:nth-child(3) > div > table > tbody > tr "
			+ "> td.pR.ng-binding")})
	private List<WebElement> settledOptionClosing;
	@FindBys({@FindBy(css = "#optionOptionsTab > table > tbody > tr > td.first-col > div.pl10.ng-binding > a")})
	private List<WebElement> marketNameOpenOptions;
	@FindBys({@FindBy(css = "#optionOptionsTab > table > tbody > tr > td:nth-child(2) > span.ng-binding")})
	private List<WebElement> openOptonsPurchased;
	@FindBy(css = "#optionOptionsTab > table > tfoot > tr > td > span:nth-child(1) > strong")
	private WebElement investedAmount;
	/**
	 * 
	 * @param driver
	 */
	public OptionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets invested amount
	 * @return String
	 */
	public String getInvestedAmount() {
		return investedAmount.getText();
	}
	/**
	 * Gets settled option return value
	 * @param index
	 * @return Integer
	 */
	public int getSettledOptionReturnValue(int index) {
		String text = settledOptionClosing.get(index).getText();
		String arr[] = text.split("\n");
		String value = arr[1].replaceAll("[^\\d]", "");
		return Integer.parseInt(value);
	}
	/**
	 * Gets settled option status
	 * @param index
	 * @return String
	 */
	public String getSetteledOptionStatus(int index) {
		String text = settledOptionClosing.get(index).getText();
		String arr[] = text.split("\n");
		String status = arr[0];
		return status;
	}
	/**
	 * Gets settled option amount
	 * @param index
	 * @return String
	 */
	public String getSettledOptionAmount(int index) {
		String text = settledOptionsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String amount = arr[1];
		return amount;
	}
	/**
	 * Gets open option amount
	 * @param index
	 * @return String
	 */
	public String getOpenOptionAmount(int index) {
		String text = openOptonsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String amount = arr[1];
		return amount;
	}
	/**
	 * Gets settled option level
	 * @param index
	 * @return String
	 */
	public String getSettledOptionLevel(int index) {
		String text = settledOptionsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String level = arr[0];
		return level;
	}
	/**
	 * Gets open option level
	 * @param index
	 * @return String
	 */
	public String getOpenOptionLevel(int index) {
		String text = openOptonsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String level = arr[0];
		return level;
	}
	/**
	 * Gets settled option market name
	 * @param index
	 * @return String
	 */
	public String getSettledOptionMarketName(int index) {
		return marketNameSetteledOptions.get(index).getText();
	}
	/**
	 * Gets open option market name
	 * @param index
	 * @return String
	 */
	public String getOpenOptionMarketName(int index) {
		String name = marketNameOpenOptions.get(index).getText();
		return name;
	}
	/**
	 * Opens settled options in trade screen
	 */
	public void openSettledOption() {
		settledOptionTab.click();
	}
}
