package com.copyop.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class QuestionnairePage {
	private static final String TEXT_CONFIRM_CHECK_BOX_CSS = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div "
			+ "> div > div > div.popUp-box > div > div > div > div > label > span.regulation-status-label.ng-binding";
	private static final String QUEST_POP_UP_CLOSE_BUTTON = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div "
			+ "> div "
			+ "> div > i";
	private static final String TITLE_CSS = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-header.ng-binding";
	private WebDriver driver;
	@FindBy(css = QUEST_POP_UP_CLOSE_BUTTON)
	private WebElement questionnaireCloseButton;
	@FindBy(css = TITLE_CSS)
	private WebElement questionnaireTitle;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > div > form "
			+ "> div:nth-child(1) > div:nth-child(2) > h3")
	private WebElement questionnaireSubtitle;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > div > form "
			+ "> div:nth-child(1) > div.row.mt20.taC > button")
	private WebElement nextbutton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > div > form "
			+ "> div:nth-child(2) > div:nth-child(5) > label > span.checkbox.small-check")
	private WebElement riskCheckBox;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-body > div > form "
			+ "> div:nth-child(2) > div.row.mt20.taC > button")
	private WebElement tradeNowButton;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div > div "
			+ "> div > label > span.regulation-status-label.ng-binding")
	private WebElement confirmCheckBoxText;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div > div > div "
			+ "> div > label > span.checkbox.small-check")
	private WebElement confirmCheckBox;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-footer.clear > button")
	private WebElement confirmButton;
	/**
	 * 
	 * @param driver
	 */
	public QuestionnairePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public boolean isTradeNowButtonEnabled() {
		String dis = tradeNowButton.getAttribute("disabled");
		if (dis == null) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is next button enabled
	 * @return
	 */
	public boolean isNextButtonEnabled() {
		String dis = nextbutton.getAttribute("disabled");
		if (dis == null) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is questionnaire pop invisible
	 * @return
	 */
	public boolean isPopUpInvisible() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			String waitCss = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content";
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(waitCss)));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Confirms pop up
	 */
	public void confirm() {
		confirmButton.click();
	}
	/**
	 * Clicks confirm check box
	 */
	public void clickConfirmCheckBox() {
		confirmCheckBox.click();
	}
	/**
	 * Gets confirm check box message
	 * @return
	 */
	public String getConfirmCheckBoxMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 45);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(TEXT_CONFIRM_CHECK_BOX_CSS)));
		return confirmCheckBoxText.getText();
	}
	/**
	 * Clicks trade now button
	 */
	public void tradeNow() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String disabled = tradeNowButton.getAttribute("disabled");
				String loading = tradeNowButton.getAttribute("class");
				if (disabled == null && ! loading.contains("loading")) {
					return true;
				} else {
					return false;
				}	
			}
		});
		tradeNowButton.click();
	}
	/**
	 * Clicks risk check box
	 */
	public void clickRiskCheckBox() {
		riskCheckBox.click();
	}
	/**
	 * Is check box for risk checked
	 * @return
	 */
	public boolean isRiskCheckBoxChecked() {
		boolean checked = true;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					boolean statment = false;
					String className = driver.findElement(By.id("question_92")).getAttribute("class");
					if (className.contains("ng-invalid-required")) {
						statment = false;
					} else if (className.contains("ng-valid-required")) {
						statment = true;
					}
					return statment;	
				}
			});
		} catch (TimeoutException e) {
			checked = false;
		}
		return checked;
	}
	/**
	 * Moves to next
	 * @return boolean
	 */
	public boolean moveNext() {
		boolean result = false;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					String disabled = nextbutton.getAttribute("disabled");
					if (disabled == null) {
						return true;
					} else {
						return false;
					}	
				}
			});
			result = true;
		} catch (TimeoutException e) {
			result = false;
		}
		nextbutton.click();
		return result;
	}
	/**
	 * Selects annual income
	 * @param index - number of selects. Starts from 1.
	 * @param value - value to select
	 */
	public void select(int index, int value) {
		String name = "question_8" + index;
		Select select = new Select(driver.findElement(By.name(name)));
		select.selectByValue(String.valueOf(value));
	}
	/**
	 * Gets questionnaire subtitle
	 * @return
	 */
	public String getSubtitle() {
		return questionnaireSubtitle.getText();
	}
	/**
	 * Gets questionnaire title
	 * @return
	 */
	public String getTitle(String text) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(TITLE_CSS)));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(TITLE_CSS), text));
		return questionnaireTitle.getText();
	}
	/**
	 * Closes questionnaire pop up
	 */
	public void closeQuestionnairePopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(QUEST_POP_UP_CLOSE_BUTTON)));
		questionnaireCloseButton.click();
	}
}
