package com.copyop.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CopyAndWatchPage {
	private static final String COPY_AMOUNT_ELEMENT_CSS = "#popup-copy-amount > div > div > ul > li > div > div";
	private static final String POP_UP_REMOVE_COPY_CSS = "#body > popups > div > div.popUp-container.ng-scope > div "
			+ "> div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box > div.popUp-body.ng-scope.add-mask";
	private static final String COPY_BUTTONS_CSS = "#inContent > div.my-page-container.my-page-top.ng-scope.loading-section > ul > li "
			+ "> div.buttons-field > button.button.tall.icon.copy.ng-binding.ng-scope.ing";
	private static final String WATCH_BUTTONS_CSS = "#inContent > div.my-page-container.my-page-top.ng-scope.loading-section > ul > li "
			+ "> div.buttons-field > button.button.tall.icon.watch.ng-binding.ng-scope.ing";
	private WebDriver driver;
	private String browserType;
	@FindBys({@FindBy(css = WATCH_BUTTONS_CSS)})
	private List<WebElement> watchButtons;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-body.ng-scope > ul > li.panel.edit-copy-field.ng-scope > label > span")			
	private WebElement stopWatchCheckBox;
	@FindBy(id = "submitBtn")
	private WebElement submitButton;
	@FindBys({@FindBy(css = "#inContent > div:nth-child(2) > div > ul > li")})
	private List<WebElement> conectionsTabs;
	@FindBys({@FindBy(css = "#newsFeedHome > ul > li > div.entry-header > h3 > a > div:nth-child(2) > span")})
	private List<WebElement> userToWatchOrCopy;
	@FindBy(css = "#column-first > div > div.side-user-box > div.side-user-buttons > button.button.large.icon.watch.ng-binding")
	private WebElement watchNewUserButton;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-header.ng-binding.ng-scope")
	private WebElement watchPopUpTitle;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-body.ng-scope > ul > li:nth-child(1) > div")
	private WebElement watchPopUpText;
	@FindBys({@FindBy(css = "#inContent > div.my-page-container.my-page-top.ng-scope.loading-section > ul > li > div:nth-child(1) > span > a")})
	private List<WebElement> watchedOrCopiedUserNames;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-header.ng-binding.ng-scope")
	private WebElement watchSettingsTitle;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-body.ng-scope > ul > li.panel.edit-copy-field.ng-scope > label")
	private WebElement stopWatchCheckBoxTest;
	@FindBys({@FindBy(css = COPY_BUTTONS_CSS)})
	private List<WebElement> copyButtons;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-body.ng-scope > ul > div.ng-scope > li > label > span")
	private WebElement stopCopyCheckBox;
	@FindBy(css = "#column-first > div > div.side-user-box > div.side-user-buttons > button.button.large.icon.copy.ng-binding")
	private WebElement newCopyButton;
	@FindBy(css = "#popup-copy-amount > div.copy-field.toggle > span.icon.big.ng-binding")
	private WebElement amountLabel;
	@FindBys({@FindBy(css = COPY_AMOUNT_ELEMENT_CSS)})
	private List<WebElement> amountOptions; 	
	@FindBy(css = "#popup-copy-how-many > div.copy-field.toggle > span.icon.big.ng-binding")
	private WebElement tradeCountElement;
	@FindBy(css = "#popup-copy-how-many > div.option-section.panel-content > div:nth-child(2) > label > span")
	private WebElement limitRadioButton;
	@FindBy(css = "#popup-copy-how-many > div.option-section.panel-content > div:nth-child(2) > span > span.limit-range-element.minus")
	private WebElement minusElement;
	@FindBy(css = "#popup-copy-assets > div.copy-field.toggle > span.icon.big.ng-binding")
	private WebElement assetsEditElement;
	@FindBy(css = "#popup-copy-assets > div.option-section.panel-content > div.element.specific-asset > label > span")
	private WebElement specificRadioButton;
	@FindBys({@FindBy(css = "#popUp-assets-list > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div "
			+ "> div.asstes-list-content.scroll-wrapper > div.popUp-body.scroll-content > ul.ng-scope > li > div:nth-child(2) "
			+ "> input[type='checkbox']")})
	private List<WebElement> assetsCheckBoxes;
	@FindBy(css = "#popUp-assets-list > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div.popUp-footer "
			+ "> button.button.submit.ng-binding")
	private WebElement selectButton;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-header.ng-binding.ng-scope > i")
	private WebElement openCopyPopUpIcon;
	@FindBy(css = "#body > popups > div > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div.popUp-box "
			+ "> div.popUp-body.ng-binding")
	private WebElement alreadyWatchedMessage;
	@FindBy(css = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
			+ "> div.popUp-box > div.popUp-body.ng-scope > ul > div.ng-scope > li > label")
	private WebElement uncopyCheckBoxLabel;
	@FindBy(css = "#column-first > div > h1")
	private WebElement userName;
	/**
	 * 
	 * @param driver
	 */
	public CopyAndWatchPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets count check boxes in first group
	 * @return
	 */
	public int getCountCheckBoxes() {
		return assetsCheckBoxes.size();
	}
	/**
	 * Gets user name from open profile
	 * @return
	 */
	public String getUserNameFromProfile() {
		return userName.getText();
	}
	/**
	 * Gets uncopy check box label text
	 * @return
	 */
	public String getUncopyCheckBoxLabel() {
		return uncopyCheckBoxLabel.getText();
	}
	/**
	 * Clicks stop copy check box
	 */
	public void clickStopCopyCheckBox() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(stopCopyCheckBox));
		stopCopyCheckBox.click();
		UtilsMethods.sleep(1000);
		try {
			String atrCss = POP_UP_REMOVE_COPY_CSS;
			wait.until(ExpectedConditions.attributeToBe(By.cssSelector(atrCss), "class", "popUp-body ng-scope add-mask"));
		} catch (TimeoutException e) {
			System.out.println("Exception");
			stopCopyCheckBox.click();
			UtilsMethods.sleep(2000);
			String atrCss = POP_UP_REMOVE_COPY_CSS;
			wait.until(ExpectedConditions.attributeToBe(By.cssSelector(atrCss), "class", "popUp-body ng-scope add-mask"));
		}
	}
	/**
	 * Stops copy
	 * @param index
	 */
	public void stopCopy(int index) {
		copyButtons.get(index).click();
	}
	/**
	 * Gets already watched message
	 * @return String
	 */
	public String getAlreadyWatchedMessage() {
		return alreadyWatchedMessage.getText();
	}
	/**
	 * Opens copy op pop up 
	 */
	public void openCopyPopUp() {
		openCopyPopUpIcon.click();
	}
	/**
	 * Gets count chosen assets
	 * @return Integer
	 */
	public int getAssetsCount() {
		String count = assetsEditElement.getText();
		return Integer.parseInt(count);
	}
	/**
	 * Chooses assets
	 * @param count
	 */
	public void chooseAssets(int count) {
		assetsEditElement.click();
		specificRadioButton.click();
		for (int i = 0; i < count; i++) {
			assetsCheckBoxes.get(i).click();
		}
		selectButton.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				String atr = selectButton.getAttribute("disabled"); 
				if (atr == null) {
					return true;
				} else {
					return false;
				}	
			}
		});
	}
	/**
	 * Gets count trades
	 * @return Integer
	 */
	public int getTradeCount() {
		String count = tradeCountElement.getText();
		int number = 0;
		try {
			number = Integer.parseInt(count);
		} catch (NumberFormatException e) {
			number = 10;
		}
		return number;
	}
	/**
	 * Gets amount 
	 * @return String
	 */
	public String getAmount() {
		return amountLabel.getText();
	}
	/**
	 * Edits count
	 */
	public void editCount() {
		tradeCountElement.click();
		limitRadioButton.click();
		minusElement.click();
		UtilsMethods.sleep(250);
		minusElement.click();
	}
	/**
	 * Edits amount
	 * @param index
	 */
	public void editAmount(int index) {
		amountLabel.click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(COPY_AMOUNT_ELEMENT_CSS)));
		amountOptions.get(index).click();
	}
	/**
	 * Copy the user
	 */
	public void copyTheUser() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(newCopyButton));
		try {
			newCopyButton.click();
		} catch (WebDriverException e) {
			UtilsMethods.clickWithJavaScript(driver, newCopyButton);
		}
	}
	/**
	 * Clicks watch button
	 * @param index
	 */
	public void stopWatch(int index) {
		if (browserType.equalsIgnoreCase("firefox")) {
			try {
				watchButtons.get(index).click();
			} catch (WebDriverException e) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", watchButtons.get(index));
			}
		} else {
			watchButtons.get(index).click();
		}	
	}
	/**
	 * Gets count watchers
	 * @return Integer
	 */
	public int getCountWatchedUsers() {
		int count = 0;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(WATCH_BUTTONS_CSS)));
			System.out.println("No watchers");
		} catch (TimeoutException e) {
			count = watchButtons.size();
			System.out.println("Count watched users is " + count);
		}
		return count;
	}
	/**
	 * Gets count copiers
	 * @return Integer
	 */
	public int getCountCopiedUsers() {
		int count = 0;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(COPY_BUTTONS_CSS)));
			System.out.println("No copied users");
		} catch (TimeoutException e) {
			count = watchedOrCopiedUserNames.size();
		}
		return count;
	}
	/**
	 * Gets check box for stop watch text
	 * @return String
	 */
	public String getStopWatchCheckBoxText() {
		return stopWatchCheckBoxTest.getText();
	}
	/**
	 * Clicks stop watch check box
	 */
	public void clickStopWatchCheckBox() {
		stopWatchCheckBox.click();
		UtilsMethods.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		String waitCss = "#body > popups > div > div.popUp-container.ng-scope > div > div.popUp-wrapper2.scroll-content > div > div > div "
				+ "> div.popUp-box > div.popUp-body.ng-scope.add-mask";
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
		} catch (TimeoutException e) {
			System.out.println("Exception");
			stopWatchCheckBox.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(waitCss)));
			UtilsMethods.sleep(2000);
		}
	}
	/**
	 * Gets watch settings pop up title
	 * @return String
	 */
	public String getWatchSettingsTitle() {
		return watchSettingsTitle.getText();
	}
	/**
	 * Gets watched user name
	 * @param index
	 * @return String
	 */
	public String getWatchedOrCopiedUserName(int index) {
		return watchedOrCopiedUserNames.get(index).getText();
	}
	/**
	 * Is user watched
	 * @param name
	 * @return
	 */
	public boolean isUserWatched(String name) {
		boolean result = false;
		for (WebElement el : watchedOrCopiedUserNames) {
			if (el.getText().equals(name)) {
				result = true;
				break;
			}
		}
		return result;
	}
	/**
	 * Is button for watch marked
	 * @return boolean
	 */
	public boolean isWatchButtonMarked() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try{
			wait.until(ExpectedConditions.attributeToBe(watchNewUserButton, "class", "button large icon watch ng-binding ing"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Is button for copy marked
	 * @return boolean
	 */
	public boolean isCopyButtonMarked() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try{
			wait.until(ExpectedConditions.attributeToBe(newCopyButton, "class", "button large icon copy ng-binding ing"));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
	/**
	 * Confirms pop up
	 */
	public void confirmPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("submitBtn")));
	}
	/**
	 * Gets watch pop up text
	 * @return String
	 */
	public String getWatchPopUpText() {
		return watchPopUpText.getText();
	}
	/**
	 * Gets watch pop up title
	 * @return String
	 */
	public String getWatchPopUpTitle() {
		return watchPopUpTitle.getText();
	}
	/**
	 * Watches new user
	 */
	public void watchTheUser() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(watchNewUserButton));
		try {
			watchNewUserButton.click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", watchNewUserButton);
		}
	}
	/**
	 * Gets user name
	 * @param index
	 * @return
	 */
	public String getUserName(int index) {
		return userToWatchOrCopy.get(index).getText();
	}
	/**
	 * Opens user profile
	 * @param index
	 */
	public void openUserProfile(int index) {
		try {
			userToWatchOrCopy.get(index).click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", userToWatchOrCopy.get(index));
		}
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#column-first > div > div.side-user-box > div.side-user-buttons > button")));
	}
	/**
	 * Opens connections tab
	 * @param index
	 */
	public void openConectionsTab(int index) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("#inContent > div:nth-child(2) > div > ul > li")));
		try {
			conectionsTabs.get(index).click();
		} catch (WebDriverException e) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", conectionsTabs.get(index));
		}
	}
	/**
	 * Removes all watchers
	 */
	public void removeAllWatchers() {
		boolean hasWatchers = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(WATCH_BUTTONS_CSS)));
			hasWatchers = true;
		} catch (TimeoutException e) {
			System.out.println("No watchers");
		}
		if (hasWatchers == true) {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			for (WebElement button : watchButtons) {
				final int size = watchButtons.size();
				button.click();
				this.clickStopWatchCheckBox();
				submitButton.click();
				wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						int newSize = watchButtons.size();
						if (newSize != size) {
							return true;
						} else {
							return false;
						}	
					}
				});
			}
		}
	}
	/**
	 * Removes all watchers
	 */
	public void removeAllCopiers() {
		boolean hasWatchers = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(COPY_BUTTONS_CSS)));
			hasWatchers = true;
		} catch (TimeoutException e) {
			System.out.println("No copied users");
		}
		if (hasWatchers == true) {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			FeedPage feedPage = new FeedPage(driver);
			final int size = copyButtons.size();
			for (WebElement button : copyButtons) {
				button.click();
				if (feedPage.isCopyPopUpDisplayed() == true) {
					feedPage.closePopUp();
				}
				stopCopyCheckBox.click();
				submitButton.click();
				wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						int newSize = copyButtons.size();
						if (newSize != size) {
							return true;
						} else {
							return false;
						}	
					}
				});
			}
		}
	}
}
