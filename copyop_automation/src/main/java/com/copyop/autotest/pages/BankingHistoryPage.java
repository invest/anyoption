package com.copyop.autotest.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BankingHistoryPage {
	private static final String DESCRIPTION_BANK_HISTORY_CSS = "#UserTransactionsCtrlHolder > table > tbody > tr > td:nth-child(1) "
			+ "> div.start.ng-binding > h4";
	WebDriver driver;
	@FindBys({@FindBy(css = "#UserTransactionsCtrlHolder > table > tbody > tr > td:nth-child(1) > div")})
	private List<WebElement> descriptions;
	@FindBy(css = "#inContent > section > div.content-inner > section:nth-child(3) > div > h2")
	private WebElement bankingHistoryLink;
	@FindBys({@FindBy(css = DESCRIPTION_BANK_HISTORY_CSS)})
	private List<WebElement> transactionDescription;
	@FindBys({@FindBy(css = "#UserTransactionsCtrlHolder > table > tbody > tr:nth-child(1) > td:nth-child(3)")})
	private List<WebElement> amountDescription;
	/**
	 * 
	 * @param driver
	 */
	public BankingHistoryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets amount description from banking history
	 * @param index
	 * @return String
	 */
	public String getAmountDescription(int index) {
		return amountDescription.get(index).getText();
	}
	/**
	 * Gets transaction description
	 * @return String
	 */
	public String getTransactionDescription(int index) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(DESCRIPTION_BANK_HISTORY_CSS)));
		return transactionDescription.get(index).getText();
	}
	/**
	 * Gets transaction id
	 * @param index
	 * @return String
	 */
	public String getTransactionId(int index) {
		String description = descriptions.get(index).getText();
		String descs[] = description.split(":");
		String id = descs[1].replace(" ", "");
		return id;
	}
	/**
	 * Opens banking history
	 */
	public void openBankingHistory() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(bankingHistoryLink));
		bankingHistoryLink.click();
	}
}
