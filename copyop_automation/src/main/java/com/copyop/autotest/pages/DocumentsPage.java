package com.copyop.autotest.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DocumentsPage {
	private WebDriver driver;
	private String browserType;
	@FindBy(css = "#regulationDocuments > div > div > div.popUp-wrapper2.scroll-content > div > div > div")
	private WebElement documentsPopUp;
	@FindBy(css = "#regulationDocuments > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div "
			+ "> div.regulation-docs-top > p.b.ng-binding")
	private WebElement dearMessage;
	@FindBy(css = "#regulationDocuments > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div "
			+ "> div.regulation-docs-top > p.fs16.ng-binding.ng-scope")
	private WebElement headerMessage;
	@FindBys({@FindBy(xpath = "//*[@id='regulationDocuments']/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/span[5]")})
	private List<WebElement> status;
	@FindBy(css = "#regulationDocuments > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div "
			+ "> div.regulation-docs-middle > div > div > div:nth-child(2) > span.ng-scope > button")
	private WebElement idBrowseButton;
	@FindBy(css = "#regulationDocuments > div > div > div.popUp-wrapper2.scroll-content > div > div > div > div > div "
			+ "> div.regulation-docs-middle > div > div > div:nth-child(2) > span.docs-holder-w > div > select")
	private WebElement idSelect;
	@FindBys({@FindBy(xpath = "//*[@id='regulationDocuments']/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/span[3]/button")})
	private List<WebElement> browseButtons;
	/**
	 * 
	 * @param driver
	 */
	public DocumentsPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Is browse button disabled
	 * @param index - 0 - utility bill, 1 - ID, 2 - front CC, 3 - back CC 
	 * @return boolean
	 */
	public boolean isBrowseButtonDisabled(int index) {
		String className = browseButtons.get(index).getAttribute("disabled");
		if (className.contains("true")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is status approved
	 * @param index - 0 - utility bill, 1 - ID, 2 - front CC, 3 - back CC 
	 * @return boolean
	 */
	public boolean isStatusApproved(int index) {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.attributeToBe(status.get(index), "class", "ok-message icon"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Selects ID type
	 * @param index
	 */
	public void selectId(int index) {
		Select select = new Select(idSelect);
		select.selectByIndex(index);
	}
	/**
	 * Clicks id browse button
	 */
	public void clickIdBrowseButton() {
		idBrowseButton.click();
	}
	/**
	 * Is utility bill status waiting for approval
	 * @param index - 0 - utility bill, 1 - ID, 2 - front CC, 3 - back CC 
	 * @return
	 */
	public boolean isStatusWaitingToApproval(int index) {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.attributeToBe(status.get(0), "class", "info-message"));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Gets utility bill status message
	 * @param index - 0 - utility bill, 1 - ID, 2 - front CC, 3 - back CC 
	 * @return String
	 */
	public String getStatusMessage(final int index, final String mess) {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				if (status.get(index).getText().equals(mess)) {
					return true;
				} else {
					return false;
				}
			}
		});
		return status.get(index).getText();
	}
	/**
	 * Is utility bill status missing
	 * @param index - 0 - utility bill, 1 - ID, 2 - front CC, 3 - back CC 
	 * @return boolean
	 */
	public boolean isStatusError(int index) {
		String className = status.get(index).getAttribute("class");
		if (className.equals("error-message icon")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Gets header message
	 * @return String
	 */
	public String getHeaderMessage() {
		return headerMessage.getText();
	}
	/**
	 * Gets dear message
	 * @return String
	 */
	public String getDearMessage() {
		return dearMessage.getText();
	}
	/**
	 * Is document pop up displayed
	 * @return boolean
	 */
	public boolean isDocumentPopUpDisplayed() {
		boolean result = true;
		WebDriverWait wait = new WebDriverWait(driver, 8);
		try {
			wait.until(ExpectedConditions.visibilityOf(documentsPopUp));
		} catch (TimeoutException e) {
			result = false;
		}
		return result;
	}
	/**
	 * Attach file
	 * @param path
	 * @param index
	 * @throws AWTException
	 */
	public void attachFile(String path, int index) throws AWTException {
		if (browserType.equalsIgnoreCase("firefox")) {
			List<WebElement> list = driver.findElements(By.xpath("//*[@id='regulationDocuments']/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/span[3]/button"));
			list.get(index).click();
			Robot r = new Robot();
			
			StringSelection ss = new StringSelection(path);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss,null);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
			r.keyPress(KeyEvent.VK_ESCAPE);
			r.keyRelease(KeyEvent.VK_ESCAPE);
		} else if (browserType.equalsIgnoreCase("chrome")) {
			List<WebElement> fileInput = driver.findElements(By.xpath("//input[@type='file']"));
			fileInput.get(index).sendKeys(path);
		}
	}
}
