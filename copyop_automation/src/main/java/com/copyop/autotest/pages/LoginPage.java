package com.copyop.autotest.pages;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.copyop.autotest.common.DBLayer;
import com.copyop.autotest.common.UserDB;
import com.copyop.autotest.utils.BaseTest;
import com.copyop.autotest.utils.UtilsMethods;


/**
 * 
 * @author vladimir.mladenov
 *
 */
public class LoginPage {
	private static final String USER_NAME_ID = "userName";
	private WebDriver driver;
	private String browserType;
	@FindBy(css = "#main-container > section > div > form > div > div.grid.signup > div:nth-child(1) > h2")
	private WebElement haveNotAccountLabel;
	@FindBy(css = "#main-container > section > div > form > div > div.grid.signup > div:nth-child(2) > a")
	private WebElement signUpButton;
	@FindBy(css = "#header > div > div.sub-header.ng-scope > div > nav.header-menu > a.button.login.ng-binding")
	private WebElement loginButton;
	@FindBy(id = USER_NAME_ID)
	private WebElement userNameInput;
	@FindBy(id = "password")
	private WebElement passwordInput;
	@FindBy(css = "#main-container > section > div > form > div > div:nth-child(1) > div:nth-child(3) > button")
	private WebElement loginLoginButton;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div:nth-child(1) > p")
	private WebElement headerMessageAnyOption;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.avatar-box-holder > div:nth-child(2) > div:nth-child(1) "
			+ "> h2 > span")
	private WebElement anyOptionDetails;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div:nth-child(3) > p")
	private WebElement detailsMessageAnyoption;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.double-line > p")
	private WebElement chooseNickMessage;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.ng-binding > label > span")
	private WebElement acceptTermAndCondCheckBox;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.ng-binding")
	private WebElement termAndCondMessage;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div:nth-child(6) > button")
	private WebElement confirmAnyoptionRegistrationButton;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.avatar-box-holder > div.avatar-holder > div.edit.icon")
	private WebElement editAvatarButton;
	@FindBy(css = "#main-container > section > div > div > form > div.grid > div.row.avatar-box-holder > div.avatar-holder > img")
	private WebElement avatarImage;
	@FindBy(css = "#intro > div > div.ng-scope > div > div.desktop-buttons > div > div:nth-child(1) > button.button.icon.arrow.facebook.ng-binding")
	private WebElement connectWithFacebookButton;
	@FindBy(id = "email")
	private WebElement facebookEmailInput;
	@FindBy(id = "pass")
	private WebElement facebookPassInput;
	@FindBy(name = "login")
	private WebElement facebookLoginButton;
	/**
	 * 
	 * @param driver
	 */
	public LoginPage(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Logins in facebook
	 */
	public void loginInFacebook() {
		facebookLoginButton.click();
	}
	/**
	 * Sets facebook pass
	 * @param pass
	 */
	public void setFacebookPass(String pass) {
		facebookPassInput.sendKeys(pass);
	}
	/**
	 * Sets facebook email
	 * @param email
	 */
	public void setFacebookEmail(String email) {
		facebookEmailInput.sendKeys(email);
	}
	/**
	 * Clicks connect with facebook button
	 */
	public void connectWithFaceBook() {
		connectWithFacebookButton.click();
	}
	/**
	 * Gets avatar image
	 * @param type
	 * @return
	 */
	public boolean getAvatarImage(String type) {
		String image = avatarImage.getAttribute("src");
		if (type.equals("female") && image.contains("f")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Clicks edit avatar button
	 */
	public void editAvatar() {
		editAvatarButton.click();
	}
	/**
	 * Confirms registration with anyoption account
	 */
	public void confirmRegistrationWithAOAcc() {
		confirmAnyoptionRegistrationButton.click();
	}
	/**
	 * Gets term and conditions text
	 * @return
	 */
	public String getTermAndCondMessage() {
		return termAndCondMessage.getText();
	}
	/**
	 * Clicks term and conditions check box
	 */
	public void clickCheckBoxForTermAndCond() {
		acceptTermAndCondCheckBox.click();
	}
	/**
	 * Gets choose nick message from anyoption registration
	 * @return
	 */
	public String getChooseNickMessage() {
		return chooseNickMessage.getText();
	}
	/**
	 * Gets details message from anyoption registration
	 * @return
	 */
	public String getDetailsMessage() {
		return detailsMessageAnyoption.getText();
	}
	/**
	 * Gets anyoption account details
	 * @return
	 */
	public String getAnyoptionDetails() {
		return anyOptionDetails.getText();
	}
	/**
	 * Gets header message from anyoption registration
	 * @return
	 */
	public String getHeaderMessage() {
		return headerMessageAnyOption.getText();
	}
	/**
	 * Types password
	 * @param pass
	 */
	public void setPassword(String pass) {
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = pass.length() - 1;
			String lastChar = String.valueOf(pass.charAt(lastIndex));
			pass = pass.substring(0, lastIndex);
			String script = "document.getElementById('password').value='" + pass +"'";
			((JavascriptExecutor) driver).executeScript(script);
			passwordInput.sendKeys(lastChar);
		} else {
			passwordInput.sendKeys(pass);
		}	
	}
	/**
	 * Types username
	 * @param userName
	 */
	public void setUsername(String userName) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(USER_NAME_ID)));
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			int lastIndex = userName.length() - 1;
			String lastChar = String.valueOf(userName.charAt(lastIndex));
			userName = userName.substring(0, lastIndex);
			String script = "document.getElementById('" + USER_NAME_ID + "').value='" + userName +"'";
			((JavascriptExecutor) driver).executeScript(script);
			userNameInput.sendKeys(lastChar);
		} else {
			try {
				userNameInput.sendKeys(userName);
			} catch (StaleElementReferenceException e) {
				UtilsMethods.sleep(250);
				userNameInput.sendKeys(userName);
			}
		}	
	}
	/**
	 * Gets currently used users
	 * @return
	 */
	private String getCurrentUsers() {
		String returnedUsers = null;
		if (! BaseTest.users.isEmpty()) {
			returnedUsers = BaseTest.users.toString();
			System.out.println(returnedUsers);
			returnedUsers = returnedUsers.replaceAll("\\[", "");
			returnedUsers = returnedUsers.replaceAll("\\]", "");
		} else {
			returnedUsers = "1111";
		}
		return returnedUsers;
	}
	/**
	 * Logins with user from data base
	 * @param sqlQuery
	 * @param makeTester
	 * @return HasMap<String, String> - keys are names of columns in table users with lower case 
	 * @throws Exception
	 */
	public HashMap<String, String> loginWithUserFromDB(String sqlQuery, boolean makeTester) throws Exception {
		DBLayer db = new DBLayer();
  		String replace = getCurrentUsers();
  		String sqlQueryNow = sqlQuery.replace("check", replace);
  		ArrayList<String[]> results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
  		if (results.size() == 0) {
  			throw new SkipException("Skipping the test case no users");
  		}
  		if (BaseTest.users.contains(results.get(0)[0])) {
  			System.out.println("The user is already used");
  			replace = getCurrentUsers();
  			sqlQueryNow = sqlQuery.replace("check", replace);
			results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
		}
		System.out.println(results.get(0)[3]);
		String userName = results.get(0)[3];
		String userId = results.get(0)[0];
		String email = results.get(0)[14];
		String firstName = results.get(0)[6];
		String lastName = results.get(0)[7];
		String balance = results.get(0)[1];
		System.out.println("User_id: " + userId);
		String pass = "123456";
		HashMap<String, String> map = new HashMap<>();
		map.put("user_name", userName);
		map.put("id", userId);
		map.put("email", email);
		map.put("first_name", firstName);
		map.put("last_name", lastName);
		map.put("balance", balance);
		BaseTest.users.add(userId);
		if (makeTester == true) {
			UserDB.updateUserToTester(email, userId);
		}
		String currentUrl = driver.getCurrentUrl();
		if (! currentUrl.contains("/login")) {
			loginButton.click();
		}	
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(USER_NAME_ID)));
		this.setUsername(userName);
		this.setPassword(pass);
		loginLoginButton.click();
		
		FeedPage feedPage = new FeedPage(driver);
		if (feedPage.isHolidayPopUpDisplayed() == true) {
			feedPage.confirmHolidayPopUp();
		}
		return map;
	}
	/**
	 * Logins with user from data base
	 * @param sqlQuery
	 * @param makeTester
	 * @return HasMap<String, String> - keys are names of columns in table users with lower case 
	 * @throws Exception
	 */
	public HashMap<String, String> loginWithAnyOptionUser(String sqlQuery) throws Exception {
		DBLayer db = new DBLayer();
  		String replace = getCurrentUsers();
  		String sqlQueryNow = sqlQuery.replace("check", replace);
  		ArrayList<String[]> results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
  		if (results.size() == 0) {
  			throw new SkipException("Skipping the test case no users");
  		}
  		if (BaseTest.users.contains(results.get(0)[0])) {
  			System.out.println("The user is already used");
  			replace = getCurrentUsers();
  			sqlQueryNow = sqlQuery.replace("check", replace);
			results = db.getResultsFromDBImmediately(sqlQueryNow, false, 2);
		}
		System.out.println(results.get(0)[3]);
		String userName = results.get(0)[3];
		String userId = results.get(0)[0];
		String email = results.get(0)[14];
		String firstName = results.get(0)[6];
		String lastName = results.get(0)[7];
		String balance = results.get(0)[1];
		System.out.println("User_id: " + userId);
		String pass = "123456";
		HashMap<String, String> map = new HashMap<>();
		map.put("user_name", userName);
		map.put("id", userId);
		map.put("email", email);
		map.put("first_name", firstName);
		map.put("last_name", lastName);
		map.put("balance", balance);
		BaseTest.users.add(userId);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(USER_NAME_ID)));
		this.setUsername(userName);
		this.setPassword(pass);
		loginLoginButton.click();
		return map;
	}
	/**
	 * Clicks sign up button
	 */
	public void clickSignUpButton() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#main-container > section > div > form > div > div.grid.signup > div:nth-child(2) > a")));
		signUpButton.click();
		String waitCss = "#main-container > section > div > div > div:nth-child(2) > form > div.table > div:nth-child(2) > div:nth-child(5) > button > span";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(waitCss)));
	}
	/**
	 * Gets have not account yet text
	 * @return
	 */
	public String getHaveAccountMessage() {
		return haveNotAccountLabel.getText();
	}
}
