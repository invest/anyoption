package com.copyop.autotest.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author vladimir.mladenov
 * This class contains methods for my options screen (Opened from drop down menu)
 */
public class MyOptionsPage {
	WebDriver driver;
	@FindBy(css = "#inContent > section > div.content-inner.loading-section > div > ul > li:nth-child(2) > span")
	WebElement settledOptionsTab;
	@FindBys({@FindBy(css = "#inContent > section > div.content-inner.loading-section > div > div > table > tbody > tr > td.first-col > div.pl10.ng-binding > a")})
	private List<WebElement> optionsMarketName;
	@FindBys({@FindBy(css = "#inContent > section > div.content-inner.loading-section > div > div > table > tbody > tr > td:nth-child(2) > span.ng-binding")})
	private List<WebElement> optionsPurchased;
	@FindBys({@FindBy(css = "#inContent > section > div.content-inner.loading-section > div > div > table > tbody > tr > td.pR.ng-binding")})
	private List<WebElement> settledOptionClosing;
	/**
	 * 
	 * @param driver
	 */
	public MyOptionsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/**
	 * Gets settled option return value
	 * @param index
	 * @return Integer
	 */
	public int getSettledOptionReturnValue(int index) {
		String text = settledOptionClosing.get(index).getText();
		String arr[] = text.split("\n");
		String value = arr[1].replaceAll("[^\\d]", "");
		return Integer.parseInt(value);
	}
	/**
	 * Gets settled option status
	 * @param index
	 * @return String
	 */
	public String getSetteledOptionStatus(int index) {
		String text = settledOptionClosing.get(index).getText();
		String arr[] = text.split("\n");
		String status = arr[0];
		return status;
	}
	/**
	 * Gets settled option amount
	 * @param index
	 * @return String
	 */
	public String getOptionAmount(int index) {
		String text = optionsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String amount = arr[1];
		return amount;
	}
	/**
	 * Gets settled option level
	 * @param index
	 * @return String
	 */
	public String getOptionLevel(int index) {
		String text = optionsPurchased.get(index).getText();
		String arr[] = text.split("\n");
		String level = arr[0];
		return level;
	}
	/**
	 * Gets settled option market name
	 * @param index
	 * @return String
	 */
	public String getOptionMarketName(int index) {
		return optionsMarketName.get(index).getText();
	}
	/**
	 * Opens settled options in trade screen
	 */
	public void openSettledOption() {
		settledOptionsTab.click();
	}
}
