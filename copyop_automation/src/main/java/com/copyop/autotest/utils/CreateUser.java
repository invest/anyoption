package com.copyop.autotest.utils;

import com.copyop.autotest.utils.RandomStringGenerator.Mode;
/**
 * 
 * @author vladimirm
 *
 */
public class CreateUser {
	private String firstName;
	private String lastName;
	private String eMail;
	private String phone;
	private String nickName;
	/**
	 * 
	 * @throws Exception
	 */
	public CreateUser() throws Exception {
		String randomString = RandomStringGenerator.generateRandomString(6, Mode.CAPITAL_LETTERS);
		String randomNumeric = RandomStringGenerator.generateRandomString(8, Mode.NUMERIC);
		if (randomNumeric.startsWith("0")) {
			randomNumeric.replace("0", "9");
		}
		firstName = "AutoCopyOp" + randomString;
		lastName = "Family" + randomString;
		eMail = firstName + "@anyoption.com";
		phone = randomNumeric;
		nickName = "Cp" + randomString;
	}
	/**
	 * Used in smoke test
	 * @param skin
	 * @throws Exception
	 */
	public CreateUser(String skin) throws Exception {
		String randomString = RandomStringGenerator.generateRandomString(6, Mode.CAPITAL_LETTERS);
		String randomNumeric = RandomStringGenerator.generateRandomString(8, Mode.NUMERIC);
		if (randomNumeric.startsWith("0")) {
			randomNumeric.replace("0", "9");
		}
		firstName = "AutoCopyOp" + skin + randomString;
		lastName = "Family" + randomString;
		eMail = firstName + "@anyoption.com";
		phone = randomNumeric;
		nickName = "Cp" + randomString;
	}
	/**
	 * Gets first name
	 * @return String
	 */
	public String getFirstName() {
		return this.firstName;
	}
	/**
	 * Gets last name
	 * @return String
	 */
	public String getLastName() {
		return this.lastName;
	}
	/**
	 * Gets email
	 * @return String
	 */
	public String getEMail() {
		return this.eMail;
	}
	/**
	 * Gets phone number
	 * @return String
	 */
	public String getPhone() {
		return this.phone;
	}
	/**
	 * Gets nickname
	 * @return String
	 */
	public String getNickname() {
		return this.nickName;
	}
}
