package com.copyop.autotest.utils;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.copyop.autotest.utils.Configuration;

/**
 * 
 * @author vladimirm
 *
 */
public class ReadTestData {
	/**
	 * Reads data from excel file
	 * @param filePath - String path to excel file
	 * @param rowSize - count of rows in the file
	 * @param cellSize - count of cells in the file
	 * @return Array
	 */
	public String[][] getDataFromExcelFile(String filePath, int rowSize, int cellSize) throws IOException {
		String skin = Configuration.getProperty("desiredSkin");
		String[][] returnString = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet worksheet = workbook.getSheet("Sheet1");
			returnString = new String[rowSize][cellSize];
			if (skin != null) {
				boolean stop = false;
				int i = 0;
				do {
					XSSFRow row = worksheet.getRow(i);
					XSSFCell cell = row.getCell((short) 0);
					String cellVal = cell.getStringCellValue();
					if (cellVal.equals(skin)) {
						for (int j = 0; j < cellSize; j++) {
							cell = row.getCell((short) j);
							cellVal = cell.getStringCellValue();
							returnString[0][j] = cellVal;
						}
						break;
					} else {
						i++;
					}	
				} while (stop == false || i > 10);
			} else {
				for (int i = 0; i < rowSize; i++) {
					for (int j = 0; j < cellSize; j++) {
						XSSFRow row = worksheet.getRow(i);
						XSSFCell cell = row.getCell((short) j);
						String cellVal = cell.getStringCellValue();
						returnString[i][j] = cellVal;
					}
				}	
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnString;
	}
	/**
	 * Reads from excel file
	 * @param filePath - String path to excel file
	 * @param desiredSkin - skin who have to read from file
	 * @param cellSize - count of cells in the file
	 * @return Array
	 * @throws IOException
	 */
	public String[][] getDataFromExcelFile(String filePath, String desiredSkin, int cellSize) throws IOException {
		int rowSize = 1;
		String[][] returnString = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet worksheet = workbook.getSheet("Sheet1");
			returnString = new String[rowSize][cellSize];
			if (desiredSkin != null) {
				boolean stop = false;
				int i = 0;
				do {
					XSSFRow row = worksheet.getRow(i);
					XSSFCell cell = row.getCell((short) 0);
					String cellVal = cell.getStringCellValue();
					if (cellVal.equals(desiredSkin)) {
						for (int j = 0; j < cellSize; j++) {
							cell = row.getCell((short) j);
							cellVal = cell.getStringCellValue();
							returnString[0][j] = cellVal;
						}
						break;
					} else {
						i++;
					}	
				} while (stop == false || i > 10);
			} else {
				for (int i = 0; i < rowSize; i++) {
					for (int j = 0; j < cellSize; j++) {
						XSSFRow row = worksheet.getRow(i);
						XSSFCell cell = row.getCell((short) j);
						String cellVal = cell.getStringCellValue();
						returnString[i][j] = cellVal;
					}
				}	
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnString;
	}
}
