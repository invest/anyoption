package com.copyop.autotest.utils;

public class RandomStringGenerator {

	public static enum Mode {
		SMALL_LETTERS, CAPITAL_LETTERS, ALPHANUMERIC, NUMERIC, SPECIAL_CHARS, ALL_CHARS
	}

	public static String generateRandomString(int length, Mode mode) throws Exception {

		StringBuffer buffer = new StringBuffer();
		String characters = "";

		switch (mode) {

		case SMALL_LETTERS:
			characters = "abcdefghijklmnopqrstuvwxyz";
			break;

		case CAPITAL_LETTERS:
			characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			break;

		case ALPHANUMERIC:
			characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			break;

		case NUMERIC:
			characters = "1234567890";
			break;

		case SPECIAL_CHARS:
			characters = "~!@#$%^&*()_+=`{}[]\\|':;.,/?<>";
			break;

		case ALL_CHARS:
			characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*()_+=`{}[]\\|':;.,/?<>";
			break;
		}

		int charactersLength = characters.length();

		for (int i = 0; i < length; i++) {
			double index = Math.random() * charactersLength;
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}
}
