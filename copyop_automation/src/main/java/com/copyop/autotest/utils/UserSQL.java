package com.copyop.autotest.utils;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UserSQL {
	private static String returnedUsers = "check";
	
	/**
	 * Gets user who can make deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanMakeDeposit(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "INNER JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID AND CREDIT_CARDS.CC_NUMBER LIKE '114_-40_-80_61_-52_-1_32_116_62_21_-89_-100_29_4_-39_38_85_-98_-87_118_-65_84_-52_41' "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE != 0 "
				+ "AND BALANCE < 100000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND users.COUNTRY_ID = 33 "
				+ "AND PLATFORM_ID = 3 "
				+ "AND users.id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY users.TIME_CREATED DESC";
		return sqlQuery;
	}	
	/**
	 * Gets user who has not credit card
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeNewDeposit(String skinId) {
		String query = "SELECT * FROM users "  
				+ "LEFT JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID " 
				+ "WHERE " 
				+ "EMAIL LIKE '%@anyoption.com%' "
				+ "AND PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' " 
				+ "AND PLATFORM_ID = 3 "
				+ "AND users.BALANCE = 0 "
				+ "AND users.skin_id = " + skinId + " "
				+ "AND CREDIT_CARDS.USER_ID IS NULL "
				+ "AND users.id NOT IN (" + returnedUsers + ") "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY users.TIME_CREATED DESC";
		return query;
	}
	/**
	 * Gets user who can make questionnaire
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanMakeQuestionnaire(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE != 0 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 4 > (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}	
	/**
	 * Gets user who is anyoption.com user and is not register in copyop.com
	 * @param skinId
	 * @return
	 */
	public static String getAnyoptionUser(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE "
				+ "EMAIL LIKE '%@anyoption.com%' "
				+ "AND PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%Autotest%' "
				+ "AND BALANCE > 10000 "
				+ "AND skin_id = " + skinId + " "
				+ "AND PLATFORM_ID = 2 "
//				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND users.id NOT IN (" + returnedUsers + ") "
				+ "AND 0 = (SELECT COUNT(*) FROM LOGINS WHERE users.id = LOGINS.USER_ID AND LOGINS.WRITER_ID = 10000) "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can withdraw deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanWithdrawDeposit(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 20000 AND BALANCE < 100000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND users.TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY TIME_CREATED DESC";
		return sqlQuery;
	}
	/**
	 * Gets user who can reverse deposit
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanReverseWithdraw(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 10000 "
				+ "AND BALANCE < 45000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "ORDER BY users.TIME_LAST_LOGIN";
		return sqlQuery;
	}
	/**
	 * Gets user who can make investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeInvestment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 10000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
				+ "ORDER BY TIME_CREATED";
		return sqlQuery;
	}	
	/**
	 * Gets user to copy or watch
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeCopyWatch(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 10000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "ORDER BY TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can make dev 2 investment and has no current investments
	 * @param skinId
	 * @return
	 */
	public static String getUserWhoCanMakeDev2Investment(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE >= 50000 "
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 "
				+ "AND COUNTRY_ID = 33 "
				+ "AND id NOT IN (" + returnedUsers + ") "
				+ "AND TIME_LAST_LOGIN < SYSDATE - INTERVAL '5' MINUTE "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND id NOT IN (SELECT user_id FROM TRANSACTIONS WHERE status_id = 4) "
				+ "AND id NOT IN (SELECT user_id FROM INVESTMENTS WHERE INVESTMENTS.IS_SETTLED = 0) "
				+ "ORDER BY TIME_CREATED";
		return sqlQuery;
	}
	/**
	 * Gets user who can upload documents
	 * @param skinId
	 * @return String
	 */
	public static String getUserWhoCanUploadDocuments(String skinId) {
		String sqlQuery = "SELECT * FROM users "
				+ "INNER JOIN CREDIT_CARDS ON CREDIT_CARDS.USER_ID = users.ID AND CREDIT_CARDS.CC_NUMBER LIKE '114_-40_-80_61_-52_-1_32_116_62_21_-89_-100_29_4_-39_38_85_-98_-87_118_-65_84_-52_41' "
				+ "WHERE PASSWORD LIKE '108_-95_51_-34_84_111_26_23' "
				+ "AND FIRST_NAME LIKE '%AutoCopyOp%' "
				+ "AND EMAIL LIKE '%@anyoption.com%' "
				+ "AND BALANCE != 0 " 
				+ "AND SKIN_ID = " + skinId + " "
				+ "AND CLASS_ID = 0 " 
				+ "AND users.COUNTRY_ID = 33 " 
				+ "AND users.id NOT IN (" + returnedUsers + ") "
				+ "AND PLATFORM_ID = 3 "
				+ "AND 5 < (SELECT COUNT (*) FROM users_regulation_history WHERE users.id = user_id) "
				+ "AND 0 = (SELECT COUNT (*) FROM files WHERE users.id = user_id AND files.FILE_STATUS_ID != 1) "
				+ "ORDER BY users.TIME_CREATED DESC";
		return sqlQuery;
	}
}
