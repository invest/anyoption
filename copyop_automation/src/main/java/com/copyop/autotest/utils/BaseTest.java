package com.copyop.autotest.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
/**
 * 
 * @author vladimir.mladenov
 *
 */
public class BaseTest {
	protected WebDriver driver;
	protected String browserType;
	protected String testUrl;
	protected String clName;
	protected String parameterSkin;
	protected boolean afterMethod;
	protected int numberRows;
	protected String operationSys;
	protected String currentUserId = null;
	public static ArrayList<String> users = new ArrayList<>();
	private String mode;
	
	public BaseTest() throws IOException {
		testUrl = Configuration.getProperty("testUrl");
		Class<? extends BaseTest> className = this.getClass();
		clName = className.toString().replace("class com.copyop.autotest.", "");
		afterMethod = false;
		numberRows = Integer.parseInt(Configuration.getProperty("numberRows"));
		operationSys = Configuration.getProperty("operationSystem");
		mode = Configuration.getProperty("mode");
	}
	
	/**
	 * Set up 
	 * @throws IOException 
	 */
	private void setUp() throws IOException {
		browserType = Configuration.getProperty("browserType");
		System.out.println("Browser type is " + browserType);
		System.out.println();
		System.out.println("Running " + clName);
		System.out.println();
		if (browserType.equals("FireFox")) {
			driver = new FirefoxDriver();
		} else if (browserType.equals("Chrome")) {
			String operationSys = Configuration.getProperty("operationSystem");
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "../copyop_automation/lib/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "../copyop_automation/lib/chromedriver");
				driver = new ChromeDriver();
			} else {
				System.out.println("No correct operational system");
			}
		} else if (browserType.equals("internetexplorer")) {
			System.setProperty("webdriver.ie.driver", "../copyop_automation/lib/IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			driver = new InternetExplorerDriver(capabilities);
		} else if (browserType.equals("phantomJs")) {
			System.setProperty("phantomjs.binary.path", "../copyop_automation/lib/phantomjs.exe");
			driver = new PhantomJSDriver();
		}
		driver.get(testUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	/**
	 * Starts selenium grid 
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	private void launchapp(String browser) throws IOException {
		String URL = testUrl;
		String ip = Configuration.getProperty("ip");
		browserType = browser.replace(" ", "");
		System.out.println("Browser type: " + browserType);
		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			String Node = "http://" + ip + ":5555/wd/hub";
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.navigate().to(URL);
//			driver.manage().window().maximize();
			Dimension targetSize = new Dimension(1920, 1080);
			driver.manage().window().setSize(targetSize);;
		} else if (browser.equalsIgnoreCase("chrome")) {
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
			} else {
				System.out.println("No correct operational system");
			}
			
			System.out.println(" Executing on CHROME");
	        DesiredCapabilities cap = DesiredCapabilities.chrome();
	        
	        if (operationSys.equals("Linux")) {
	        	ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--test-type");
	        	options.addArguments("--allow-running-insecure-content");
	        	options.setBinary("/usr/bin/google-chrome");
	        	options.addArguments(Arrays.asList("--window-size=1920,1080"));
	        	cap.setCapability(ChromeOptions.CAPABILITY, options);
	        }
	        cap.setBrowserName("chrome");
	        String Node = "http://" + ip + ":5556/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("internet explorer")) {
			System.out.println(" Executing on IE");
	        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	        cap.setBrowserName("internet explorer");
	        String Node = "http://" + ip + ":5558/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("phantomJs")) {
			System.out.println(" Executing on PhantomJs");
			if (operationSys.equals("Windows")) {
				System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
				driver = new PhantomJSDriver();
			} else {
				DesiredCapabilities cap = DesiredCapabilities.phantomjs();
				cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "lib/phantomjs");
				cap.setCapability("phantomjs.binary.path", "lib/phantomjs");
				System.setProperty("phantomjs.binary.path", "lib/phantomjs");
				driver = new PhantomJSDriver(cap);
			}
			driver.navigate().to(URL);
			driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println("------------------------------------------------------------------------------");
		System.out.println(clName + " " + "browser " + browser);
		System.out.println("------------------------------------------------------------------------------");
	}
	/**
	 * Starts selenium grid or set up browser according to "mode" parameter in config.properties file
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@BeforeClass
	public void startSuite(@Optional String browser) throws IOException {
		if (mode.equals("testMode")) {
			this.launchapp(browser);
		} else if (mode.equals("devMode")) {
			this.setUp();
		}
	}
	/**
	 * Generates screen shot if test fail
	 * @param result
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod(alwaysRun = true)
	public void afterMethodTearDown(ITestResult result, @Optional String browser) throws IOException {
		if (currentUserId != null) {
        	System.out.println("Delete current user " + currentUserId);
        	users.remove(currentUserId);
        	System.out.println(users);
        }	
		if (afterMethod == false) {
			if (ITestResult.FAILURE == result.getStatus()) {
				String brName = browser.replace(" ", "");
				String name = result.getMethod().getMethodName();
				String screenShotName = name + parameterSkin + brName;
				ScreenShot scShot = new ScreenShot();
				scShot.takeScreenShot(driver, screenShotName);
				System.out.println("Screenshot generated");
			} 
		}
		if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println("-");
			System.out.println(clName + " " + parameterSkin + " FAIL with user " + currentUserId);
			System.out.println("-");
		} else {
			System.out.println("-");
			System.out.println(clName + " " + parameterSkin +" PASS");
			System.out.println("-");
		}
		driver.manage().deleteAllCookies();
	}
	/**
	 * Closes browser
	 * @throws IOException 
	 */
	@AfterClass(alwaysRun = true)
	public void afterClassTearDown() throws IOException {
		driver.manage().deleteAllCookies();
        driver.quit();
	}
	/**
	 * Deletes all temporary fails after executing test suite (it is for windows only)
	 * @throws IOException 
	 */
	@AfterSuite
	public void deleteTempFails() throws IOException {
		if (operationSys.equals("Windows")) {
			System.out.println("Delete all temp fails");
			String filePath = "";
			filePath = Configuration.getProperty("tempDirPath");
			try {
				FileUtils.deleteDirectory(new File(filePath));
			} catch (IOException e) {
			}
			if (browserType.equalsIgnoreCase("internetexplorer")) {
				UtilsMethods.killIEBrowser();
			}
		}	
	}
}
