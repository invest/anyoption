package com.copyop.autotest.utils;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.copyop.autotest.common.UserDB.Skin;
/**
 * 
 * @author vladimirm
 *
 */
public class UtilsMethods {

	/**
	 * Gets skin type
	 * @param skin - EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
	 * @return
	 */
	public static Skin getSkin(String skin) {
		Skin returnSkin = null;
		switch (skin) {
		case "EN":
			returnSkin = Skin.EN;
			break;
		case "ES":
			returnSkin = Skin.ES;
			break;
		case "TR":
			returnSkin = Skin.TR;
			break;	
		case "DE":
			returnSkin = Skin.DE;
			break;	
		case "RU":
			returnSkin = Skin.RU;
			break;	
		case "IT":
			returnSkin = Skin.IT;
			break;	
		case "FR":
			returnSkin = Skin.FR;
			break;	
		case "SG":
			returnSkin = Skin.SG;
			break;	
		case "KR":
			returnSkin = Skin.KR;
			break;	
		case "ET":
			returnSkin = Skin.ET;
			break;	
		}
		
		return returnSkin;
	}
	/**
	 * Stops executions for time
	 * @param time
	 */
	public static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Gets skinId 
	 * @param skin - String the skin
	 * @return String
	 */
	public static String getSkinId(String skin) {
		String skinId = null;
		switch (skin) {
		case "EN":
			skinId = "16";
			break;
		case "ES":
			skinId = "18";
			break;
		case "TR":
			skinId = "3";
			break;
		case "DE":
			skinId = "19";
			break;
		case "RU":
			skinId = "10";
			break;
		case "IT":
			skinId = "20";
			break;
		case "FR":
			skinId = "21";
			break;
		case "SG":
			skinId = "15";
			break;
		case "KR":
			skinId = "17";
			break;
		case "ET":
			skinId = "1";
			break;
		}
		return skinId;
	}
	/**
	 * Kills IE browsers and IE drivers
	 */
	public static void killIEBrowser() {
		try {
		    Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
		    Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe");
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	/**
	 * Makes click with java script executor
	 * @param driver
	 * @param element
	 */
	public static void clickWithJavaScript(WebDriver driver, WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}
}
