package com.copyop.autotest.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;

import com.copyop.autotest.common.DBLayer;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UserDB {
	public static String skinId;
	public static String languageID;
	
	public enum Skin {
		EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
	}
	
	public static ArrayList<String[]> getUserDataFromDBAfterCreationfAccount(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT balance, user_name, "
				+ "first_name, last_name, is_active, "
				+ "email, is_contact_by_email, class_id, "
				+ "language_id, skin_id, is_accepted_terms, "
				+ "platform_id"
				+ " FROM users WHERE email = '" + eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}
	
	public static ArrayList<String[]> getContactsDataFromDBAfterCreationfAccount(String eMail) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT name, type, skin_id, "
				+ "country_id, first_name, last_name, "
				+ "class_id, mobile_phone" + " FROM contacts WHERE email = '"
				+ eMail + "'";

		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		return results;
	}
	/**
	 * Allows the credit card
	 * @param userId
	 */
	public static void allowCreditCard(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlUpdate = "UPDATE credit_cards SET is_allowed = 1 WHERE user_id = '" + userId + "'";
		String sqlCommit = "commit";
		dB.executeDBQuery(sqlUpdate, false);
		dB.executeDBQuery(sqlCommit, false);
		System.out.println("The user's credit card is successfuly updated (is_allowed = 1)! ");
	}
	/**
	 * Get balance from data base
	 * @param eMail - String
	 * @param userId - String
	 */
	public static String getUserBalanceFromDB(String eMail, String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT balance FROM users WHERE email = '"+ eMail + "' AND id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		String actBal = results.get(0)[0];
		return actBal;
	}	
	/**
	 * Updates user to tester
	 * @param email
	 * @param userId
	 * @throws Exception
	 */
	public static void updateUserToTester(String email, String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlUpdate = "UPDATE users SET class_id = 0 where email LIKE '"+ email + "' AND id = " + userId;
		String sqlCommit = "commit";
		dB.executeDBQuery(sqlUpdate, false);
		dB.executeDBQuery(sqlCommit, false);
	}
	/**
	 * 
	 * @param eMail
	 * @param firstName
	 * @param lastName
	 * @param skin
	 * @throws Exception
	 */
	public static void compareUserDataAfterCreationOfAccount(String eMail, String firstName, String lastName, Skin skin) throws Exception {
		ArrayList<String[]> resultsForUser = getUserDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying user info for the created player...");
		switch (skin) {
		// EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
		case EN:
			skinId = "16";
			languageID = "2";
			break;
		case ES:
			skinId = "18";
			languageID = "5";
			break;
		case TR:
			skinId = "3";
			languageID = "3";
			break;
		case DE:
			skinId = "19";
			languageID = "8";
			break;
		case RU:
			skinId = "10";
			languageID = "4";
			break;
		case IT:
			skinId = "20";
			languageID = "6";
			break;
		case FR:
			skinId = "21";
			languageID = "9";
			break;
		case SG:
			skinId = "15";
			languageID = "10";
			break;
		case KR:
			skinId = "17";
			languageID = "11";
			break;
		case ET:
			skinId = "1";
			languageID = "1";
			break;
		default:
			System.out.println("Incorrect skin is set for verification!!");
			break;
		}
		Assert.assertEquals("0", resultsForUser.get(0)[0], "Check user's balance");
		Assert.assertEquals(eMail.toUpperCase(), resultsForUser.get(0)[1], "Check the username");
		Assert.assertEquals(firstName, resultsForUser.get(0)[2], "Check the first name");
		Assert.assertEquals(lastName, resultsForUser.get(0)[3], "Check the last name");
		Assert.assertEquals("1", resultsForUser.get(0)[4], "Check the active");
		Assert.assertEquals(eMail, resultsForUser.get(0)[5], "Check user's email");
		Assert.assertEquals("1", resultsForUser.get(0)[6], "Check the contact by email value");
		Assert.assertEquals("1", resultsForUser.get(0)[7], "Check the class_id");
		Assert.assertEquals(languageID, resultsForUser.get(0)[8], "Check the language_id");
		Assert.assertEquals(skinId, resultsForUser.get(0)[9], "Check the skin_id");
		Assert.assertEquals("1", resultsForUser.get(0)[10], "Check the is_accepted_terms");
		Assert.assertEquals("3", resultsForUser.get(0)[11], "Check the platform");
	}
	
	/**
	 * Verifies users data in data base
	 * @param eMail
	 * @param firstName
	 * @param lastName
	 * @param skin
	 * @param phoneNumber
	 * @throws Exception
	 */
	public static void compareContactsDataAfterCreationOfAccount(String eMail, String firstName, String lastName, Skin skin,
			String phoneNumber) throws Exception {
		ArrayList<String[]> resultsForUser = getContactsDataFromDBAfterCreationfAccount(eMail);
		System.out.println("Verifying contacts info for the created player...");
		String skinId = null;
		switch (skin) {
		// EN, ES, TR, DE, RU, IT, FR, SG, KR, ET
		case EN:
			skinId = "16";
			break;
		case ES:
			skinId = "18";
			break;
		case TR:
			skinId = "3";
			break;
		case DE:
			skinId = "19";
			break;
		case RU:
			skinId = "10";
			break;
		case IT:
			skinId = "20";
			break;
		case FR:
			skinId = "21";
			break;
		case SG:
			skinId = "15";
			break;
		case KR:
			skinId = "17";
			break;
		case ET:
			skinId = "1";
			break;
		default:
			System.out.println("Incorrect skin is set for verification!!");
			break;
		}
		Assert.assertEquals(firstName + " " + lastName, resultsForUser.get(0)[0]);
		Assert.assertEquals("7", resultsForUser.get(0)[1]);
		Assert.assertEquals(skinId, resultsForUser.get(0)[2]);
		Assert.assertEquals(firstName, resultsForUser.get(0)[4]);
		Assert.assertEquals(lastName, resultsForUser.get(0)[5]);
		Assert.assertEquals("1", resultsForUser.get(0)[6]);
		Assert.assertEquals(phoneNumber, resultsForUser.get(0)[7]);
	}
	/**
	 * Verifies data in data base with data in test 
	 * @param investmentId
	 * @param userId
	 * @param level
	 * @param amount
	 * @param profit
	 * @throws Exception
	 */
	public static void compareInvestmentData(String investmentId, String userId, String level, String amount, 
			String[] profit) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE id = " + investmentId + " AND user_id = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("1", results.get(0)[3], "investment type");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		Assert.assertEquals(level.replace(",", ""), results.get(0)[5], "current level");
		Assert.assertEquals(Double.parseDouble(profit[0]) / 100, Double.parseDouble(results.get(0)[6]), "win");
		Assert.assertEquals((1 - Double.parseDouble(profit[1]) / 100), Double.parseDouble(results.get(0)[7]), "lose");
		Assert.assertEquals("10000", results.get(0)[8], "writer_id");
		Assert.assertEquals(null, results.get(0)[14], "time setteled");
		Assert.assertEquals("0", results.get(0)[15], "is_settled");
	}
	/**
	 * Verifies deposit transaction
	 * @param transactionId
	 * @param amount
	 * @throws Exception
	 */
	public static void verifyDepositTransaction(String transactionId, String amount) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where id = " + transactionId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("1", results.get(0)[3], "type_id");
		Assert.assertEquals(amount + "00", results.get(0)[5], "amount");
		Assert.assertEquals("7", results.get(0)[6], "status_id");
		Assert.assertTrue(! results.get(0)[11].equals(null));
	}
	/**
	 * Verifies withdraw transaction
	 * @param userId
	 * @param amount
	 * @throws Exception
	 */
	public static void verifyWithdrawTransaction(String userId, String amount, String transactionId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " ORDER BY TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals(transactionId, results.get(0)[0], "transaction_id");
		Assert.assertEquals("10", results.get(0)[3], "type_id");
		Assert.assertEquals(amount + "00", results.get(0)[5], "amount");
		Assert.assertEquals("4", results.get(0)[6], "status_id");
		Assert.assertTrue(results.get(0)[11] == null);
	}
	/**
	 * Verifies reverse withdraw transaction
	 * @param userId
	 * @param amount
	 * @param transactionId
	 * @throws Exception
	 */
	public static void verifyReverseWithdrawTransaction(String userId, String amount, String transactionId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select * from TRANSACTIONS where USER_ID = " + userId + " ORDER BY TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("6", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[5], "amount");
		Assert.assertEquals("2", results.get(0)[6], "status_id");
		Assert.assertTrue(results.get(0)[11] == null);
		Assert.assertEquals(transactionId, results.get(0)[0], "transaction_id");
		ArrayList<String[]> withdrawTransaction = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("2", withdrawTransaction.get(0)[6], "status_id");
	}
	/**
	 * Verifies data from canceled investment
	 * @param userId
	 * @param amount
	 * @param fee
	 * @param level
	 * @throws Exception
	 */
	public static void verifyCanceledInvestment(String userId, String amount, String fee, String level) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "SELECT * FROM investments WHERE user_id = " + userId + "order by TIME_CREATED DESC";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		Assert.assertEquals("1", results.get(0)[3], "type_id");
		Assert.assertEquals(amount, results.get(0)[4], "amount");
		if (! level.equals(results.get(0)[5])) {
			Assert.assertEquals(level.replace("0", "").replace(",", "").replace(".", ""), results.get(0)[5].replace("0", "").replace(",", "").replace(".", ""), "current_level");
		}	
		Assert.assertFalse(results.get(0)[5].equals("0"));
		Assert.assertEquals("0", results.get(0)[10], "win");
		Assert.assertEquals(fee, results.get(0)[11], "lose");
		Assert.assertEquals(fee, results.get(0)[12], "house_result");
		Assert.assertEquals("1", results.get(0)[15], "is_settled");
		Assert.assertEquals("1", results.get(0)[16], "is_canceled");
	}
	/**
	 * Gets data for deviation 2 check
	 * @param marketId
	 * @return HasMap
	 * @throws Exception
	 */
	public static HashMap<String, String> getDev2Data(String marketId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select AMOUNT_FOR_DEV2, SEC_FOR_DEV2 from markets where id = '" + marketId + "'";
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		HashMap<String, String> data = new HashMap<>();
		if (results.size() != 0) {
			int amount = Integer.parseInt(results.get(0)[0]) + 1;
			data.put("amount", String.valueOf(amount));
			data.put("secs", results.get(0)[1]);
		}	
		return data;
	}
	/**
	 * Gets user sum deposits
	 * @param userId 
	 * @return String
	 * @throws Exception
	 */
	public static String getSumDeposits(String userId) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlRequest = "select SUM_DEPOSITS from USERS_ACTIVE_DATA where USER_ID = " + userId;
		ArrayList<String[]> results = dB.getResultsFromDBImmediately(sqlRequest, false, 2);
		String sumDeposits = results.get(0)[0];
		return sumDeposits;
	}
	/**
	 * Approves attached document in data base
	 * @param userId
	 * @param fileName
	 * @throws Exception
	 */
	public static void approveDocument(String userId, String fileName) throws Exception {
		DBLayer dB = new DBLayer();
		String sqlUpdate = "UPDATE files SET is_approved = 1, file_status_id = 3, is_approved_control = 1 "
				+ "where user_id = " + userId + " and file_name like '%" + fileName + "%'"; 
		String sqlCommit = "commit";
		dB.executeDBQuery(sqlUpdate, false);
		dB.executeDBQuery(sqlCommit, false);
	}
}
