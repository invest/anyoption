package com.copyop.autotest.smoketest;

import java.awt.AWTException;
import java.io.IOException;
import java.util.HashMap;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.copyop.autotest.pages.AnyOptionBackEndPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.utils.Configuration;
import com.copyop.autotest.utils.CreateUser;
import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class FirstDeposit {
	private WebDriver driver;
	private String browserType;
	private String balance;
	private String backEndUrl;
	private CreateUser user;
	private HashMap<String, String> firstDepositData;
	private String cardHolderName = "CO automation tester";
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param balance
	 * @param user
	 * @throws IOException
	 */
	public FirstDeposit(WebDriver driver, String browserType, String balance, CreateUser user, HashMap<String, String> firstDepositData) throws IOException {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
		this.user = user;
		this.firstDepositData = firstDepositData;
		backEndUrl = Configuration.getProperty("backEndUrl");
	}
	/**
	 * Makes first deposit
	 * @param depositTitle
	 * @param positiveMess
	 * @param autoWatchTitle
	 * @param autoWatchMess
	 * @return
	 * @throws IOException
	 * @throws AWTException 
	 */
	public HashMap<String, String> makeFirstDeposit(String depositTitle, String positiveMess, String autoWatchTitle, String autoWatchMess) throws IOException {
		boolean tester = false;
		DepositPage depositPage = new DepositPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		Assert.assertTrue(depositPage.isDepositPopUpDisplayed());
		Assert.assertEquals(depositTitle, depositPage.getDepositPopUpTitle());
		String moneyType = balance.replaceAll("\\d+.*", "");
		Assert.assertEquals("0.00", balance.replace(moneyType, ""));
		String amount = "400";
		if (browserType.equalsIgnoreCase("internetexplorer")) {
			amount = "500";
		} else {
			depositPage.setAmountForDeposit(amount);
		}	
		Assert.assertEquals(moneyType, depositPage.getCurrentCurrency());
		depositPage.selectCurrency(0);
		Assert.assertEquals("$", depositPage.getCurrentCurrency());
		depositPage.setCardNumber("4200000000000000");
		depositPage.selectExpMonth(3);
		depositPage.selectExpYear(2);
		depositPage.setCvv("123");
		depositPage.setHolderName(cardHolderName);
		depositPage.setStreet("StreetTest");
		depositPage.setCity("Sofia");
		depositPage.setZipCode("1234");
		depositPage.selectBirthDay("19");
		depositPage.selectBirthMount(7);
		depositPage.selectBirthYear("1986");
		depositPage.submitDeposit();
		depositPage.waitError();
		String currentUrl = driver.getCurrentUrl();
		tester = this.makeUserTesterAndAllowCard(user);
		UtilsMethods.sleep(4000);
		Assert.assertTrue(tester);
		if (tester == true) {
			firstDepositData.put("isTester", "yes");
		}
		driver.get(currentUrl);
		depositPage.setAmountForDeposit(amount);
		depositPage.setCardNumber("4200000000000000");
		depositPage.selectExpMonth(3);
		depositPage.selectExpYear(2);
		depositPage.setCvv("123");
		depositPage.setHolderName(cardHolderName);
		depositPage.submitDepositNext();
		Assert.assertTrue(depositPage.isMessagePopUpDisplayed());
		Assert.assertEquals(positiveMess, depositPage.getSuccesMessage());
		String message = depositPage.getAmountMessage();
		Assert.assertTrue(message.contains("$" + amount));
		depositPage.submitPopUp();
		Assert.assertTrue(depositPage.getAutoWatchingPopUpTitle().contains(autoWatchTitle));
		Assert.assertEquals(autoWatchMess, depositPage.getAutoWatchingMessage());
		depositPage.closeAutoWatchPopUp();
		String balanceAfter = homePage.getBalance();
		Assert.assertTrue(balanceAfter.contains("$"));
		moneyType = "$";
		Assert.assertEquals(moneyType + amount + ".00", balanceAfter);
		firstDepositData.put("balance", balanceAfter);
		return firstDepositData;
	}
	/**
	 * Makes user tester and allows the credit card in back end
	 * @param user
	 * @return
	 * @throws IOException
	 * @throws AWTException 
	 */
	private boolean makeUserTesterAndAllowCard(CreateUser user) throws IOException {
		boolean tester = false;
	    driver.get(backEndUrl);
		AnyOptionBackEndPage anyOptionBackEndPage = new AnyOptionBackEndPage(driver, browserType);
		anyOptionBackEndPage.loginInBackEnd();
		anyOptionBackEndPage.searchUser(user);
		tester = anyOptionBackEndPage.makeTester(user);
		if (tester == true) {
			anyOptionBackEndPage.allowCard(user, cardHolderName);
			anyOptionBackEndPage.logoutFromBackEnd();
		} else {
			anyOptionBackEndPage.logoutFromBackEnd();
		}
		return tester;
	}
}
