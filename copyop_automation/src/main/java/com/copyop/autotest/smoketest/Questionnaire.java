package com.copyop.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.QuestionnairePage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class Questionnaire {
	private WebDriver driver;
	/**
	 * 
	 * @param driver
	 */
	public Questionnaire(WebDriver driver) {
		this.driver = driver;
	}
	/**
	 * Makes questionnaire
	 * @param title
	 * @param subTitle
	 * @param confirmMessage
	 */
	public void makeQuestionnaire(String title, String subTitle, String confirmMessage) {
		QuestionnairePage questionnairePage = new QuestionnairePage(driver);
		Assert.assertEquals(title, questionnairePage.getTitle(title));
		Assert.assertEquals(subTitle, questionnairePage.getSubtitle());
		int value = 1;
		Assert.assertFalse(questionnairePage.isNextButtonEnabled());
		for (int i = 1; i < 6; i++) {
			questionnairePage.select(i, value);
		}
		if (questionnairePage.moveNext() == false) {
			for (int i = 3; i < 6; i++) {
				questionnairePage.select(i, value + 1);
			}
			questionnairePage.moveNext();
		}
		Assert.assertFalse(questionnairePage.isTradeNowButtonEnabled());
		for (int i = 6; i < 10; i++) {
			questionnairePage.select(i, value + 1);
		}
		if (questionnairePage.isRiskCheckBoxChecked() == false) {
			questionnairePage.clickRiskCheckBox();
		}
		Assert.assertEquals(true, questionnairePage.isRiskCheckBoxChecked());
		questionnairePage.tradeNow();
		Assert.assertEquals(confirmMessage, questionnairePage.getConfirmCheckBoxMessage());
		questionnairePage.clickConfirmCheckBox();
		questionnairePage.confirm();
		Assert.assertTrue(questionnairePage.isPopUpInvisible());
	}
}
