package com.copyop.autotest.smoketest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.copyop.autotest.pages.AnyOptionBackEndPage;
import com.copyop.autotest.utils.Configuration;
import com.copyop.autotest.utils.CreateUser;
import com.copyop.autotest.utils.ScreenShot;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class SmokeBaseTest {
	protected WebDriver driver;
	protected String url;
	protected String browserType;
	protected String operationSys;
	protected String parameterSkin;
	protected String clName;
	protected CreateUser user;
	protected HashMap<String, String> registerData;
	protected HashMap<String, String> firstDepositData;
	
	public SmokeBaseTest(String parameterSkin) throws Exception {
		this.parameterSkin = parameterSkin;
		url = Configuration.getProperty("liveUrl");
		operationSys = Configuration.getProperty("operationSystem");
		if (! parameterSkin.equals("EN")) {
			url = url + "/" + parameterSkin.toLowerCase() + "/";
		}
		Class<? extends SmokeBaseTest> className = this.getClass();
		clName = className.toString().replace("class com.copyop.autotest.", "");
		user = new CreateUser(parameterSkin);
		System.out.println(user.getEMail());
		registerData = new HashMap<>();
		firstDepositData = new HashMap<>();
		registerData.put("IsRegistered", "no");
		firstDepositData.put("isTester", "no");
	}
	/**
	 * Starts selenium grid 
	 * @param browser
	 * @throws IOException
	 */
	@Parameters("browser")
	@BeforeClass
	public void launchapp(String browser) throws IOException {
		String URL = url;
		String ip = Configuration.getProperty("ip");
		browserType = browser.replace(" ", "");
		System.out.println("Browser type: " + browserType);
		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			String Node = "http://" + ip + ":5555/wd/hub";
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.navigate().to(URL);
//			driver.manage().window().maximize();
			
			Dimension targetSize = new Dimension(1920, 1080);
			driver.manage().window().setSize(targetSize);;
		} else if (browser.equalsIgnoreCase("chrome")) {
			if (operationSys.equals("Windows")) {
				System.out.println("Operational system: " + operationSys);
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			} else if (operationSys.equals("Linux")){
				System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
			} else {
				System.out.println("No correct operational system");
			}
			
			System.out.println(" Executing on CHROME");
	        DesiredCapabilities cap = DesiredCapabilities.chrome();
	        
	        if (operationSys.equals("Linux")) {
	        	ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--test-type");
	        	options.addArguments("--allow-running-insecure-content");
	        	options.setBinary("/usr/bin/google-chrome");
	        	options.addArguments(Arrays.asList("--window-size=1920,1080"));
	        	cap.setCapability(ChromeOptions.CAPABILITY, options);
	        }
	        
	        cap.setBrowserName("chrome");
	        String Node = "http://" + ip + ":5556/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("internet explorer")) {
			System.out.println(" Executing on IE");
	        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	        cap.setBrowserName("internet explorer");
	        cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	        String Node = "http://" + ip + ":5558/wd/hub";
	        driver = new RemoteWebDriver(new URL(Node), cap);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	         
	        driver.navigate().to(URL);
	        driver.manage().window().maximize();
		} else if (browser.equalsIgnoreCase("phantomJs")) {
			System.out.println(" Executing on PhantomJs");
			if (operationSys.equals("Windows")) {
				System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
				driver = new PhantomJSDriver();
			} else {
				DesiredCapabilities cap = DesiredCapabilities.phantomjs();
				cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "lib/phantomjs");
				cap.setCapability("phantomjs.binary.path", "lib/phantomjs");
				System.setProperty("phantomjs.binary.path", "lib/phantomjs");
				driver = new PhantomJSDriver(cap);
			}
			driver.navigate().to(URL);
			driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println("------------------------------------------------------------------------------");
		System.out.println(clName + " " + "browser " + browser);
		System.out.println("------------------------------------------------------------------------------");
	}
	/**
	 * Closes browser
	 * @throws IOException 
	 */
	@AfterClass
	public void afterClassTearDown() throws IOException {
		if (registerData.get("IsRegistered").equals("yes") && firstDepositData.get("isTester").equals("no")) {
			driver.get(Configuration.getProperty("backEndUrl"));
			AnyOptionBackEndPage anyOptionBackEndPage = new AnyOptionBackEndPage(driver, browserType);
			anyOptionBackEndPage.loginInBackEnd();
			anyOptionBackEndPage.searchUser(user);
			anyOptionBackEndPage.makeTester(user);
			anyOptionBackEndPage.logoutFromBackEnd();
		}
		driver.manage().deleteAllCookies();
        driver.quit();
	}
	/**
	 * Generates screen shot if test fail
	 * @param result
	 * @throws IOException
	 */
	@Parameters("browser")
	@AfterMethod
	public void afterMethodTearDown(ITestResult result, @Optional String browser) throws IOException {
		String methodName = result.getMethod().getMethodName();
		if (ITestResult.FAILURE == result.getStatus()) {
			String brName = browser.replace(" ", "");
			String screenShotName = methodName + parameterSkin + brName;
			ScreenShot scShot = new ScreenShot();
			scShot.takeScreenShot(driver, screenShotName);
			System.out.println("Screenshot generated");
		} 
		if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println("-");
			System.out.println(clName + " " + methodName + " FAIL with user " + user.getEMail());
			System.out.println("-");
		} else {
			System.out.println("-");
			System.out.println(clName + " " + methodName + " PASS");
			System.out.println("-");
		}
	}
	/**
	 * Deletes all temporary fails after executing test suite (it is for windows only)
	 * @throws IOException 
	 */
	@AfterSuite
	public void deleteTempFails() throws IOException {
		if (operationSys.equals("Windows")) {
			System.out.println("Delete all temp fails");
			String filePath = "";
			filePath = Configuration.getProperty("tempDirPath");
			try {
				FileUtils.deleteDirectory(new File(filePath));
			} catch (IOException e) {
			}
		}	
	}
}
