package com.copyop.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.CopyAndWatchPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UserWatch {
	private WebDriver driver;
	private String browserType;
	/**
	 * 
	 * @param driver
	 */
	public UserWatch(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
	}
	/**
	 * Watches user
	 * @param watch
	 * @param watchMessage
	 * @param title
	 * @param chBoxMess
	 * @param popUpTitle
	 * @param popUpText1
	 * @param popUpText2
	 */
	public void watchUser(String watch, String watchMessage, String title, String chBoxMess, String popUpTitle, String popUpText1, String popUpText2) {
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		CopyAndWatchPage copyAndWatchPage = new CopyAndWatchPage(driver, browserType);
		homePage.openWatchers();
		Assert.assertTrue(feedPage.isWatchPopUpDisplayed());
		Assert.assertEquals(popUpTitle.toUpperCase(), feedPage.getWatchPopUpTitle());
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.closePopUp();
		copyAndWatchPage.openConectionsTab(3);
		copyAndWatchPage.removeAllWatchers();
		homePage.openSocialScreen();
		if (feedPage.isFeedPopUpDisplayed()) {
			feedPage.closePopUp();
		}
		copyAndWatchPage.openUserProfile(0);
		String name = copyAndWatchPage.getUserNameFromProfile();
		copyAndWatchPage.watchTheUser();
		Assert.assertTrue(copyAndWatchPage.getWatchPopUpTitle().contains(name));
		Assert.assertEquals(watchMessage, copyAndWatchPage.getWatchPopUpText());
		copyAndWatchPage.confirmPopUp();
		Assert.assertTrue(copyAndWatchPage.isWatchButtonMarked());
		homePage.openWatchers();
		copyAndWatchPage.openConectionsTab(3);
		Assert.assertEquals(1, copyAndWatchPage.getCountWatchedUsers());
		Assert.assertTrue(copyAndWatchPage.isUserWatched(name));
		copyAndWatchPage.stopWatch(0);
		Assert.assertEquals(title, copyAndWatchPage.getWatchSettingsTitle());
		copyAndWatchPage.clickStopWatchCheckBox();
		Assert.assertEquals(chBoxMess.toUpperCase(), copyAndWatchPage.getStopWatchCheckBoxText());
		copyAndWatchPage.confirmPopUp();
		copyAndWatchPage.openConectionsTab(3);
		Assert.assertEquals(0, copyAndWatchPage.getCountWatchedUsers());
	}
}
