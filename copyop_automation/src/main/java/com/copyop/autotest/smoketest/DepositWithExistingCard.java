package com.copyop.autotest.smoketest;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;

import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class DepositWithExistingCard {
	private WebDriver driver;
	private String browserType;
	private String skin;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param skin
	 * @param balance
	 */
	public DepositWithExistingCard(WebDriver driver, String browserType, String skin, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.skin = skin;
		this.balance = balance;
	}
	/**
	 * Makes deposit with existing card
	 * @param succesMessage
	 * @param amountMess
	 * @return
	 */
	public String makeDepositWithExistingCard(String succesMessage, String amountMess) {
		HomePage homePage = new HomePage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		depositPage.openDeposit();
		Assert.assertEquals(true, depositPage.isDepositExistingCardCurrent());
		Assert.assertEquals(moneyType, depositPage.getCurrencyFromDepositWithExistCard());
		String amount = "200";
		if (! skin.equals("EN")) {
			amount = "250";
		}
		int amountInt = Integer.parseInt(amount + "00");
		depositPage.setAmountForDepositWithExistedCard(amount);
		depositPage.setCvv("123");
		depositPage.submitDepositWithExistedCard();
		Assert.assertEquals(succesMessage, depositPage.getSuccesMessage());
		String message[] = depositPage.getAmountMessage().split("\n");
		Assert.assertEquals(moneyType + amount + " " + amountMess, message[0]);
		depositPage.submitPopUp();
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt + amountInt, balanceAfterInt);
		return balanceAfter;
	}
}
