package com.copyop.autotest.smoketest;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;

import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.MyOptionsPage;
import com.copyop.autotest.pages.OptionsPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class CancelInvestment {
	private WebDriver driver;
	private String browserType;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param balance
	 */
	public CancelInvestment(WebDriver driver, String browserType, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
	}
	/**
	 * Makes cancel investment
	 * @param canceledMess
	 * @param feeMess
	 * @param linkText
	 * @param optionStatus
	 * @return
	 */
	public String makeCancelInvestment(String canceledMess, String feeMess, String linkText, String optionStatus) {
		HomePage homePage = new HomePage(driver);
		FeedPage feedPage = new FeedPage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		OptionsPage optionsPage = new OptionsPage(driver);
		MyOptionsPage myOptionsPage = new MyOptionsPage(driver);
		homePage.openTradeMySelfScreen();
		if (feedPage.isInvestmentPopUpDisplayed() == true) {
			feedPage.closePopUp();
		}
		String moneyType = balance.replaceAll("\\d+.*", "");
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		investmentPage.setActiveMarket();
		String amount = "50";
		investmentPage.setAmount(amount);
		String marketName = investmentPage.getMarketName();
		boolean isCancelInvDisp = false;
		for (int i = 0; i < 10; i++) {
			investmentPage.clickUpButton();
			if (investmentPage.isCancelInvestmentDisplayed() == true) {
				isCancelInvDisp = true;
				break;
			}
		}	
		Assert.assertTrue(isCancelInvDisp);
		investmentPage.doubleClickCancelInvLink();
		Assert.assertEquals(canceledMess, investmentPage.getCanceledInvMessage());
		investmentPage.closesCancelInvPopUp();
		investmentPage.showCancelInvPopUp();
		String cancelInvLevel = investmentPage.getCancelInvLevel();
		String cancelInvAmount = investmentPage.getCancelInvAmount();
		int cancelInvTimeLeft = investmentPage.getCancelInvLeftTime();
		String feeMessage = investmentPage.getCancelInvFeeMessage();
		String cancelInvLinkText = investmentPage.getCancelInvLinkText();
		Assert.assertEquals(moneyType + amount + ".00", cancelInvAmount);
		Assert.assertTrue(cancelInvTimeLeft <= 3);
		Assert.assertEquals(feeMess, feeMessage);
		Assert.assertEquals(linkText, cancelInvLinkText);
		int amountInt = Integer.parseInt(amount + "00");
		double fee = amountInt * 0.1;
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		Assert.assertEquals(balanceInt - (int)fee, balanceAfterInt);
		int retValue = Integer.parseInt(amount + "00") - (int)fee;
		optionsPage.openSettledOption();
		Assert.assertEquals(marketName, optionsPage.getSettledOptionMarketName(0));
		if (! cancelInvLevel.equals(optionsPage.getSettledOptionLevel(0))) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), optionsPage.getSettledOptionLevel(0).replace("0", ""));
		}
		Assert.assertEquals(moneyType + amount + ".00", optionsPage.getSettledOptionAmount(0));
		Assert.assertEquals(optionStatus, optionsPage.getSetteledOptionStatus(0));
		Assert.assertEquals(retValue, optionsPage.getSettledOptionReturnValue(0));
		homePage.openMyOptions();
		myOptionsPage.openSettledOption();
		Assert.assertEquals(marketName, myOptionsPage.getOptionMarketName(0));
		String levelFromMyOptions = myOptionsPage.getOptionLevel(0);
		if (! cancelInvLevel.equals(levelFromMyOptions)) {
			Assert.assertEquals(cancelInvLevel.replace("0", ""), levelFromMyOptions.replace("0", ""));
		}	
		Assert.assertEquals(moneyType + amount + ".00", myOptionsPage.getOptionAmount(0));
		Assert.assertEquals(optionStatus, myOptionsPage.getSetteledOptionStatus(0));
		Assert.assertEquals(retValue, myOptionsPage.getSettledOptionReturnValue(0));
		return balanceAfter;
	}
}
