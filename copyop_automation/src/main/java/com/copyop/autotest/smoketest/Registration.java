package com.copyop.autotest.smoketest;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.RegisterPage;
import com.copyop.autotest.utils.CreateUser;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class Registration {
	private WebDriver driver;
	private String browserType;
	private CreateUser user;
	private String skin;
	private HashMap<String, String> registerData;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param user
	 */
	public Registration(WebDriver driver, String browserType, CreateUser user, String skin, HashMap<String, String> registerData) {
		this.driver = driver;
		this.browserType = browserType;
		this.user = user;
		this.skin = skin;
		this.registerData = registerData;
	}
	/**
	 * Register new user
	 * @param signUpMess
	 * @param homePageMess
	 * @param depositTitle
	 * @param homePageHello
	 * @return
	 */
	public HashMap<String, String> registerUser(String signUpMess, String homePageMess, String depositTitle, String homePageHello) {
		RegisterPage registerPage = new RegisterPage(driver, browserType);
		DepositPage depositPage = new DepositPage(driver, browserType);
		HomePage homePage = new HomePage(driver);
		homePage.changeLanguage(skin);
		registerPage.clickStartNowButton();
		Assert.assertEquals(signUpMess, registerPage.getSignUpTitle());
		Assert.assertEquals(homePageMess, registerPage.getSignUpScreenMessage());
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String nickName = user.getNickname();
		String email = user.getEMail();
		String phone = user.getPhone();
		String password = "123456";
		registerPage.setFirstName(firstName);
		registerPage.setLastName(lastName);
		registerPage.setNickName(nickName);
		registerPage.setEmail(email);
		registerPage.setPhone(phone);
		registerPage.setPassword(password);
		registerPage.retypePassword(password);
		String value = "29";
		if (skin.equals("DE")) {
			value = "28";
		}
		if (skin.equals("IT")) {
			value = "31";
		}
		if (skin.equals("FR")) {
			value = "32";
		}
		registerPage.selectCountry(value);
		registerPage.checkTermAndConds();
		Assert.assertTrue(registerPage.isCheckBoxChecked());
		registerPage.editAvatar();
		registerPage.clickFemaleAvatarButton();
		Assert.assertTrue(registerPage.getAvatarImage("female"));
		registerPage.clickDoneButton();
		registerData.put("IsRegistered", "yes");
		Assert.assertTrue(depositPage.isDepositPopUpDisplayed());
		Assert.assertEquals(depositTitle, depositPage.getDepositPopUpTitle());
		Assert.assertEquals(homePageHello + " " + firstName + " " + lastName,  homePage.getHeaderName());
		Assert.assertEquals(firstName + " " + lastName, homePage.getSideUserName());
		Assert.assertEquals(firstName + " " + lastName, homePage.getUserMenuIconName());
		String balance = homePage.getBalance();
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		Assert.assertEquals(0, balanceInt);
		registerData.put("balance", balance);
		return registerData;
	}
}
