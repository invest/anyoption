package com.copyop.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.utils.CreateUser;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class WithdrawDeposit {
	private WebDriver driver;
	private String browserType;
	private String balance;
	private CreateUser user;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param balance
	 * @param user
	 */
	public WithdrawDeposit(WebDriver driver, String browserType, String balance, CreateUser user) {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
		this.user = user;
	}
	/**
	 * Makes deposit withdraw
	 * @param dearMess
	 * @param reasonMess
	 * @param thanksMess
	 * @param finalMess
	 * @return
	 */
	public String makeWithdrawDeposit(String dearMess, String reasonMess, String thanksMess, String finalMess) {
		HomePage homePage = new HomePage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		String moneyType = balance.replaceAll("\\d+.*", "");
		homePage.openDepositScreen();
		depositPage.clickWithdrawLink();
		depositPage.selectCardToWithdraw("0");
		String amount = "200";
		depositPage.setAmountForDeposit(amount);
		Assert.assertEquals(moneyType, depositPage.getCurrencyFromWithdrawScreen());
		depositPage.submitWithdraw();
		Assert.assertEquals(dearMess + " " + user.getFirstName() + ",", depositPage.getWithdrawPopUpTitle());
		Assert.assertEquals(reasonMess, depositPage.getWithdrawPopUpQuestion());
		depositPage.chooseRadioButton(0);
		depositPage.submitPopUp();
		Assert.assertEquals(thanksMess, depositPage.getThanksMess());
		depositPage.submitPopUp();
		switch (moneyType) {
		case "$":
			Assert.assertEquals("3000", depositPage.getFeeFromImportantMessage(moneyType));
			break;
		case "€":
			Assert.assertEquals("2500", depositPage.getFeeFromImportantMessage(moneyType));
			break;
		}
		depositPage.confirmsWithdraw();
		String finalMessage = depositPage.getWithdrawFinalMessage();
		Assert.assertTrue(finalMessage.contains(moneyType + amount));
		Assert.assertTrue(finalMessage.startsWith(finalMess));
		depositPage.closeDepositPopUp();
		String balaceAfter = homePage.getBalance();
		int balanceAfterInt = Integer.parseInt(balaceAfter.replaceAll("[^\\d]", ""));
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		int amountInt = Integer.parseInt(amount + "00");
		Assert.assertEquals(balanceInt - amountInt, balanceAfterInt);
		return balaceAfter;
	}
}
