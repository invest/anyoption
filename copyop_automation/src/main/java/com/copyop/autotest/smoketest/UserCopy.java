package com.copyop.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.CopyAndWatchPage;
import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class UserCopy {
	private WebDriver driver;
	private String browserType;
	/**
	 * 
	 * @param driver
	 */
	public UserCopy(WebDriver driver, String browserType) {
		this.driver = driver;
		this.browserType = browserType;
	}
	/**
	 * Copies user
	 * @param popUpText1
	 * @param popUpText2
	 * @param popUpText3
	 * @param popUpText4
	 * @param alreadyWatched
	 * @param unCopy
	 */
	public void copyUser(String popUpText1, String popUpText2, String popUpText3, String popUpText4, String alreadyWatched, String unCopy) {
		CopyAndWatchPage copyAndWatchPage = new CopyAndWatchPage(driver, browserType);
		FeedPage feedPage = new FeedPage(driver);
		HomePage homePage = new HomePage(driver);
		homePage.openSocialScreen();
		copyAndWatchPage.openUserProfile(0);
		String name = copyAndWatchPage.getUserNameFromProfile();
		copyAndWatchPage.copyTheUser();
		if (! feedPage.isCopyPopUpDisplayed()) {
			copyAndWatchPage.openCopyPopUp();
		}
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText3, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText4, feedPage.getPopUpText());
		feedPage.closePopUp();
		copyAndWatchPage.editAmount(2);
		int numberTrades = copyAndWatchPage.getTradeCount();
		copyAndWatchPage.editCount();
		int count = 2;
		copyAndWatchPage.chooseAssets(count);
		Assert.assertEquals("$100", copyAndWatchPage.getAmount());
		Assert.assertEquals(numberTrades - 2, copyAndWatchPage.getTradeCount());
		Assert.assertEquals(count, copyAndWatchPage.getAssetsCount());
		copyAndWatchPage.confirmPopUp();
		Assert.assertTrue(copyAndWatchPage.isWatchButtonMarked());
		Assert.assertTrue(copyAndWatchPage.isCopyButtonMarked());
		copyAndWatchPage.watchTheUser();
		Assert.assertEquals(alreadyWatched, copyAndWatchPage.getAlreadyWatchedMessage());
		copyAndWatchPage.confirmPopUp();
		homePage.openCopiers();
		copyAndWatchPage.openConectionsTab(2);
		Assert.assertEquals(name, copyAndWatchPage.getWatchedOrCopiedUserName(0));
		copyAndWatchPage.stopCopy(0);
		copyAndWatchPage.clickStopCopyCheckBox();
		Assert.assertEquals(unCopy.toUpperCase(), copyAndWatchPage.getUncopyCheckBoxLabel());
		copyAndWatchPage.confirmPopUp();
		copyAndWatchPage.openConectionsTab(2);
		Assert.assertEquals(0, copyAndWatchPage.getCountCopiedUsers());
	}
}
