package com.copyop.autotest.smoketest;

import java.io.IOException;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;

import com.copyop.autotest.pages.BankingHistoryPage;
import com.copyop.autotest.pages.DepositPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.utils.UtilsMethods;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class ReverseWithdraw {
	private WebDriver driver;
	private String browserType;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 */
	public ReverseWithdraw(WebDriver driver, String browserType, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.balance = balance;
	}
	/**
	 * Reverses withdraw
	 * @param descMess
	 * @param succMessage
	 * @param descMessNext
	 * @param type
	 * @return
	 * @throws IOException 
	 */
	public String reverseWithdraw(String descMess, String succMessage, String descMessNext, String type) throws IOException {
		HomePage homePage = new HomePage(driver);
		DepositPage depositPage = new DepositPage(driver, browserType);
		BankingHistoryPage bankingHistoryPage = new BankingHistoryPage(driver);
		int balanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
		String moneyType = balance.replaceAll("\\d+.*", "");
		bankingHistoryPage.openBankingHistory();
		Assert.assertEquals(descMess, bankingHistoryPage.getTransactionDescription(0));
		String amount = bankingHistoryPage.getAmountDescription(0);
		Assert.assertTrue(amount.contains(moneyType));
		amount = amount.replaceAll("[^\\d]", "");
		String amountText = amount.substring(0, amount.length() - 2);
		depositPage.clickReverseButton();
		String message = depositPage.getReverseMessage();
		Assert.assertTrue(message.contains(moneyType + amountText + ".00"));
		Assert.assertEquals(succMessage, message);
		depositPage.submitPopUp();
		UtilsMethods.sleep(1500);
		String balanceAfter = homePage.getBalance();
		int balanceAfterInt =  Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
		int amountInt = Integer.parseInt(amount);
		Assert.assertEquals(balanceInt + amountInt, balanceAfterInt);
		Assert.assertEquals(descMessNext, bankingHistoryPage.getTransactionDescription(0));
		String nextAmount = bankingHistoryPage.getAmountDescription(0);
		String mess = moneyType + amountText + ".00 " + type;
		Assert.assertEquals(mess, nextAmount);
		return balanceAfter;
	}
}
