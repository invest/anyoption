package com.copyop.autotest.smoketest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.copyop.autotest.pages.FeedPage;
import com.copyop.autotest.pages.HomePage;
import com.copyop.autotest.pages.InvestmentPage;
import com.copyop.autotest.pages.MyOptionsPage;

/**
 * 
 * @author vladimir.mladenov
 *
 */
public class Investment {
	private WebDriver driver;
	private String browserType;
	private String skin;
	private String balance;
	/**
	 * 
	 * @param driver
	 * @param browserType
	 * @param skin
	 */
	public Investment(WebDriver driver, String browserType, String skin, String balance) {
		this.driver = driver;
		this.browserType = browserType;
		this.skin = skin;
		this.balance = balance;
	}
	/**
	 * Makes investment
	 * @param popUpText1
	 * @param popUpText2
	 * @param headerMess
	 * @return
	 */
	public String makeInvestment(String headerMess, String popUpText1, String popUpText2) {
		HomePage homePage = new HomePage(driver);
		InvestmentPage investmentPage = new InvestmentPage(driver, browserType);
		MyOptionsPage myOptionsPage = new MyOptionsPage(driver);
		FeedPage feedPage = new FeedPage(driver);
		String moneyType = balance.replaceAll("\\d+.*", "");
		homePage.openTradeMySelfScreen();
		homePage.clickHelpIcon();
		Assert.assertTrue(feedPage.isInvestmentPopUpDisplayed());
		Assert.assertEquals(popUpText1, feedPage.getPopUpText());
		feedPage.moveNext();
		Assert.assertEquals(popUpText2, feedPage.getPopUpText());
		feedPage.closePopUp();
		investmentPage.setActiveMarket();
		investmentPage.chooseAmount(0);
		String amountToInvest = investmentPage.getTheAmountForInvest();
		investmentPage.clickProfitDropDown();
		String chooseProfit[] = investmentPage.chooseProfit(0);
		String profitToInvest = investmentPage.getTheProfit();
  		Assert.assertEquals(chooseProfit[0] + "%", profitToInvest);
  		int correct = investmentPage.getReturnValueIfCorrect();
  		int incorrect = investmentPage.getReturnValueIfIncorrect();
  		double profitPercent = Double.parseDouble(chooseProfit[0]);
  		double refundPercent = Double.parseDouble(chooseProfit[1]);
  		double returnValue = 75 * (1 + (profitPercent / 100)) * 100;
  		returnValue = Math.round(returnValue);
  		Assert.assertEquals(correct, (int)returnValue);
  		returnValue = (75 * (refundPercent / 100)) * 100;
  		Assert.assertEquals(incorrect, (int)returnValue);
  		String marketName = investmentPage.getMarketName();
  		investmentPage.clickUpButton();
  		Assert.assertTrue(investmentPage.afterInvest());
  		Assert.assertEquals(headerMess, investmentPage.getHeaderTextFromAfterInvestPopUp());
  		String amountAfterPopUp = investmentPage.getAmountFormAfterInvestPopUp();
  		Assert.assertEquals(amountToInvest + ".00", amountAfterPopUp);
  		Assert.assertEquals(correct, investmentPage.getCorrectAmountFromAfterInvestPopUp());
  		Assert.assertEquals(incorrect, investmentPage.getIncorrectAmountFromAfterInvestPopUp());
  		Assert.assertEquals(marketName, investmentPage.getMarketNameFromAfterInvestPopUp());
  		Assert.assertEquals(amountToInvest + ".00", investmentPage.getTotalInvestmentAmount());
  		String level = investmentPage.getLevelFromAfterInvest();
  		amountAfterPopUp = amountAfterPopUp.replaceAll("[^\\d]", "");
  		String balanceAfter = homePage.getBalance();
  		int ballanceInt = Integer.parseInt(balance.replaceAll("[^\\d]", ""));
  		int amountInt = Integer.parseInt(amountToInvest.replace(moneyType, "") + "00");
  		int balanceAfterInt = Integer.parseInt(balanceAfter.replaceAll("[^\\d]", ""));
  		Assert.assertEquals(ballanceInt - amountInt, balanceAfterInt);
  		homePage.openMyOptions();
  		if (skin.equals("DE") && marketName.equals("Oil")) {
  			marketName = "Öl";
  		}
  		if (skin.equals("ES") && marketName.equals("Gold")) {
  			marketName = "Oro";
  		}
  		Assert.assertEquals(marketName, myOptionsPage.getOptionMarketName(0));
  		Assert.assertEquals(amountToInvest + ".00", myOptionsPage.getOptionAmount(0));
  		String levelFromOptions = myOptionsPage.getOptionLevel(0);
  		if (! level.equals(levelFromOptions)) {
  			Assert.assertEquals(levelFromOptions.replace("0", "").replace(".", ""), level.replace("0", "").replace(".", ""));
  		}	
  		return balanceAfter;
	}
}

