This script runs by crond every x min

1. you will need to add CATALINA_PID into tomcat.conf

CATALINA_PID="/var/log/tomcat/tomcat.pid"

2. you will need to add changes to startup script 

inside "start" add  

touch /var/log/tomcat/tomcatup.txt

inside "stop" add

rm -f /var/log/tomcat/tomcatup.txt
rm -f /var/log/tomcat/tomcat.pid 

