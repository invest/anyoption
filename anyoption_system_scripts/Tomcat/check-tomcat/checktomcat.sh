#!/bin/bash

######### this script will check if tomcat is running ok #########


cd /usr/share/tomcat7

if [ -e /var/log/tomcat/tomcat.pid ]; then
	if [ -e /var/log/tomcat/tomcatup.txt ]; then
		curl -s -H 'port:443' --max-time 10 --retry 6 http://localhost:8080/jsp/common/main.jsf | grep -q "SNID"
		if [ $? -gt 0 ]; then
			echo -e "\n########\nChecking if Tomcat is working at `date` \n###########\n" >> checktomcat.txt
			echo "Killing Service and starting Tomcat" | mutt -s "Tomcat ERROR !!!" qa@anyoption.com
			echo  "Tomcat not running ok - `date`" >> checktomcat.txt 
			pid=`cat /var/log/tomcat7/tomcat.pid`	
			echo  "Killing tomcat service pid number - $pid - `date`" >> checktomcat.txt
			kill -9 $pid
			rm -f /var/log/tomcat/tomcat.pid
			rm -f /var/log/tomcat/tomcatup.txt
			service tomcat7 start >> checktomcat.txt
		fi
	fi
fi
