#!/bin/bash

clear
########## Variables ########################
yellow='\033[1;33m'
nocolor='\033[0m'
reportfolder=/usr/local/etrader-jobs/Reports
#############################################
cd $reportfolder
echo -e "\n ${yellow}########## Creating New Report #########${nocolor}"
echo
read -p "Folder name of the new Report : " foldername
echo -e "Folder Name : ${yellow}$foldername${nocolor}"
read -p "Type 1 if the name is correct or 2 to change the name : " choosefolder

        if [ $choosefolder -eq 2 ]; then

                read -p "Folder name of the new Report : " foldername
        fi

####### Creating New Folder inside Quarys #######################
####### And Downloding SQL Files ################################
find $foldername

        if [ $? -eq 1 ];then
                echo "Creating Folder..."
                mkdir $foldername
                cd $foldername
                pwd
        else

                echo "Folder Exist Entring ..."
                cd $foldername

        fi

until [[ "$next" -eq 2 ]]
        do

        read -p "Type 1 to insert link to download sql file or 2 to continue :  " next
                if [ $next -eq 1 ];then

                        read -p "Insert LINK : " link
                        curl -X GET -b 'OSTSESSID=jmr45ucon63v4pc07nrn7r7gq0' "$link" -O

                fi

for file in *.txt; do
    mv "$file" "`basename $file .txt`.sql"
done
done
###################################################################
###################################################################


############### Creating Email Subject ######################
read -p "Email Subject : " email_subject
read -p "Type 1 to add date to the subject or Type 2 to add yesterday date to the subject : " date_format

        if [ $date_format -eq 1 ]; then

                 date=`date +%F`
        fi
        if [ $date_format -eq 2 ]; then

                date=`date +%F -d yesterday`

        fi
################################################################


###################### Creating Script ######################################################
touch $foldername.sh
echo '#!/bin/bash' > $foldername.sh
echo >> $foldername.sh
echo "cd $reportfolder/$foldername" >> $foldername.sh
echo '/opt/oracle-client/11g/sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dg01.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SID = etrader3)))" <<eof1
set pages5000' >> $foldername.sh;
echo "ALTER SESSION SET NLS_DATE_FORMAT='DD-MON-YYYY HH24:MI';" >> $foldername.sh;
echo 'SET MARKUP HTML ON SPOOL ON' >> $foldername.sh
echo 'SET SQLBLANKLINES ON' >> $foldername.sh
for file in *.sql
do
              export  sqlfile$i=$file;
              echo "spool $sqlfile$i.xls" >> $foldername.sh
              echo "@$sqlfile$i" >> $foldername.sh
              echo "spool off" >> $foldername.sh
done
echo "exit" >> $foldername.sh
echo "eof1" >> $foldername.sh
echo -n 'echo "Attached report"|mutt -s "'$email_subject $date'" '>> $foldername.sh
for file in *.sql
do
        export  sqlfile$i=$file;
        echo -n "-a $sqlfile$i.xls " >> $foldername.sh
done

        read -p "Type Email Adress (you can type many as you want Example (test@anyoption.com,test@anyoption.com) : " emailaddress
        echo $emailaddress >> $foldername.sh
echo "rm -rf *.xls" >> $foldername.sh

echo "Report Created ! "
sleep 1
chmod +x $foldername.sh
########################################################################################

############################# Create Cron ##############################################
touch $foldername

echo "Choose MIN for Cron"
read -p "MIN : " m
echo "Choose HOUR for Cron"
read -p "HOUR : " h
echo "Choose DAY OF MONTH for Cron"
read -p "DAY OF MONTH : " dom
echo "Choose MONTH for Cron"
read -p "MONTH : " mo
echo "Choose DAY OF WEEK for Cron"
read -p "DAY OF WEEK : " dow

echo "PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/NX/bin:/root/bin:/usr/java/jdk1.5.0_12/bin

$m $h $dom $mo $dow root /usr/local/etrader-jobs/Reports/$foldername/$foldername.sh &>/dev/null" > $foldername

mv $foldername /etc/cron.d/
############################################################################################
