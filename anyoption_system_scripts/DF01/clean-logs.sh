#!/bin/sh
#
#  This script will clean logs

find /var/log/tomcat/ -type f -mtime +6 -exec rm -f {} \;
