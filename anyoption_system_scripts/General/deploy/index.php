<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Anyoption Production Deploy</title>
	<meta http-equiv="refresh" content="1">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="bootstrap-theme.min.css">
<style>

</style>
</head>
<body>
<div class="container ">
<hr>
<div class="row ">
<div class="col-md-11 text-center"><h1><b>Anyoption Deploy system</b> <?php echo date('H:i:s');?></h1><br><br></div>

</div>
<div id="row">
<?php
require_once 'functions.php';

if(isset($_GET['deployall']))
{
	deployall();
}

if(isset($_GET['retry']))
{
	retry($_GET['retry']);
}

$ar=selectdb();
$disabled="";
for($i=0;$i<count($ar);$i++)
{
	if($ar[$i]['status']=="deploy" || $_GET['deployall'])
	{
		$disabled="disabled";
	}
}

if(isset($_GET['srv']) and $disabled=="")
{
	$srv=$_GET['srv'] ;
	updatedb($srv);
	header("Location: /");
}

for($i=0;$i<count($ar);$i++)
{
	if($ar[$i]['status']=="deploy")
	{
		if($ar[$i]['errorcode'])
		{
			echo '<div class="col-md-2 text-center "><a href="?srv='.$ar[$i]['host'].'" class="btn btn-lg btn-danger" role="button" '.$disabled.'>'.$ar[$i]['host'].'</a><br><b>'; 
			switch ($ar[$i]['errorcode']) {
				case 900:
					echo "<br>Service tomcat <br>shutdown failure<br>";
					echo '<br><a href="?retry='.$ar[$i]['host'].'" class="btn btn-sm btn-warning" role="button" >Retry</a>';
					break ;
				case 901:
					echo "<br>Service tomcat <br>startup failure<br>";
					echo ' <br><a href="?retry='.$ar[$i]['host'].'" class="btn btn-sm btn-warning" role="button" >Retry</a>';
                                        break ;
				default:
					echo "<br>Global problem<br>error #$ar[$i]['errorcode'] <br>";
					echo ' <br><a href="?retry='.$ar[$i]['host'].'" class="btn btn-sm btn-warning" role="button" >Retry</a>';
                                        break ;

			}
			echo '</b></div>';
		}
		else
		{
			echo '<div class="col-md-2 text-center "><a href="?srv='.$ar[$i]['host'].'" class="btn btn-lg btn-success" role="button" '.$disabled ;
			echo '>'.$ar[$i]['host'].'</a><br><b> Deploy Start time :<br> '.$ar[$i]['lastdeploy'] ; 
			echo '</b>
			      <div class="progress">
        		     <div class="progress-bar" role="progressbar" aria-valuenow="'.$ar[$i]['progress'].'" aria-valuemin="0" 
				aria-valuemax="100" style="width: '.$ar[$i]['progress'].'%;"><span class="sr-only">'.$ar[$i]['progress'].'% Complete</span></div>
			      </div>';
			if($ar[$i]['progress']==25){echo "tomcat shutdown";}
			if($ar[$i]['progress']==50){echo "sync in progress";}
			if($ar[$i]['progress']==75){echo "tomcat startup";}
				
				echo '</div>';
		}
	}


	else 
	{
	
		echo '<div class="col-md-2 text-center ">
		<a href="?srv='.$ar[$i]['host'].'" class="btn btn-lg btn-primary" role="button" '.$disabled.'>'.$ar[$i]['host'].'</a><br> Last deploy :<br> '.$ar[$i]['lastdeploy'] . '</div>';

	}


}

                echo '<div class="col-md-2 text-center ">
                <a href="?deployall=allservers" class="btn btn-lg btn-primary" role="button" disabled>Deploy-All</a>
                <br> Last deploy :<br> '.$ar[3]['lastdeploy'] . '</div>';


?>
</div>
</div>
</body>
</html>
