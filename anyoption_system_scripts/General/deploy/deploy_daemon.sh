#!/bin/bash

# this daemon will deploy based on sqlite database 

export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin

if [ -e /var/run/deploy.pid ] ; then
	echo "Service already running please check deploy.pid"
	exit 1
else

echo $$ >/var/run/deploy.pid

cd /var/www/html/deploy/


while true
do
sleep 2

myhots=`sqlite3 db/.htsqlitedb "select host,status from servers" | grep -m1 "deploy"|cut -d "|" -f1`
errorcode=`sqlite3 db/.htsqlitedb "select * from servers" |grep -m1 "deploy" | cut -d "|" -f4`
deploytype=`sqlite3 db/.htsqlitedb "select * from servers" |grep -m1 "deploy" | cut -d "|" -f6`
tomcathome=/usr/share/tomcat7/webapps
err=""
if [ $myhots ] && [ $errorcode == "0" ] 
then
	echo -e "\n###########################################################\nStarting deploy -- $myhots --  at `date` \n###########################################################\n" >>/var/log/deploy.log
	# shut down tomcat and httpd
	echo -n "`date '+%F %T'` shuting tomcat down ... " >>/var/log/deploy.log
	ssh -p 6886 root@$myhots "/usr/share/tomcat7/bin/shutdown.sh"
	sqlite3 db/.htsqlitedb "update servers set progress=25 where host='$myhots' " 
	sleep 6
	# check that the app is down
	curl --max-time 10 --retry 3 http://$myhots:8080/jsp/db_check.jsf |grep -q "dbok"
	if [ $? -gt 0 ]; then
		echo "OK" >>/var/log/deploy.log

		################################################################################
		find /usr/share/tomcat7/webapps/ -name '*.war' >/tmp/wars.list
		find /usr/share/tomcat7/webapps/ -maxdepth 1 -type d >/tmp/dir.list
		sed -i -e '1d' -e 's/$/\//g'  /tmp/dir.list

		## delete the folders
		while read xxx
		do
			echo -n "`date '+%F %T'` Deleting content $xxx ... " >>/var/log/deploy.log
			0</dev/null ssh -p 6886 $myhots "rm -rf $xxx*"
			err=$?
			if [ $err -gt 0 ] ;then 
				sqlite3 db/.htsqlitedb "update servers set errorcode=903 where host='$myhots'"
				echo "ERROR # $err" >>/var/log/deploy.log
				continue
			else
				echo "Done" >>/var/log/deploy.log
			fi
		done</tmp/dir.list
		
		## copy the wars
		while read xxx
		do
			echo -n "`date '+%F %T'` Copy file $xxx ... " >>/var/log/deploy.log
			scp -P 6886 "$xxx" $myhots:"$xxx"
			err=$?
			if [ $err -gt 0 ] ;then 
				sqlite3 db/.htsqlitedb "update servers set errorcode=902 where host='$myhots'"
				echo "ERROR # $err" >>/var/log/deploy.log
				continue
			else
				echo "Done" >>/var/log/deploy.log
			fi
		done</tmp/wars.list
		
		################################################################################

		sqlite3 db/.htsqlitedb "update servers set progress=50 where host='$myhots' "
		sleep 2
		# get the rsync error code
		sqlite3 db/.htsqlitedb "update servers set errorcode=$err where host='$myhots'"
		# if rsync success , start tomcat
		if [ $err -eq 0 ];then
			echo -n "`date '+%F %T'` start tomcat ..." >>/var/log/deploy.log
			ssh -p 6886 root@$myhots "/usr/share/tomcat7/bin/startup.sh"
			sqlite3 db/.htsqlitedb "update servers set progress=75 where host='$myhots' "
			sleep 20
			curl --max-time 10 --retry 3 http://$myhots:8080/jsp/db_check.jsf |grep -q "dbok"
			# if tomcat fail to start , report
			if [ $? -gt 0 ]; then
				echo "ERROR" >>/var/log/deploy.log
				sqlite3 db/.htsqlitedb "update servers set errorcode=901 where host='$myhots'"
			else	
				echo "OK" >>/var/log/deploy.log
				mydate=`date "+%Y-%m-%d %H:%M:%S"`
				sqlite3 db/.htsqlitedb "update servers set status='active',lastdeploy='$mydate' where host='$myhots' "
			fi
			
		fi
			
	else
		echo "ERROR" >>/var/log/deploy.log
		# tell user that tomcat did not shut down 
		sqlite3 db/.htsqlitedb "update servers set errorcode=900 where host='$myhots'"
		sleep 3
	fi

	sqlite3 db/.htsqlitedb "update servers set progress=100 where host='$myhots' "
	sleep 2
fi



done
fi
