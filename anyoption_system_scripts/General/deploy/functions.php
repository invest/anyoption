<?php

function selectdb()
{
	$dbh = new PDO('sqlite:db/.htsqlitedb');
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$res=$dbh->query("select * from servers") ;
	$ar = array() ;
	$i=0 ;
        foreach($res as $row)
        {
		$ar[$i]['host']=$row['host'];
		$ar[$i]['status']=$row['status'];
		$ar[$i]['lastdeploy']=$row['lastdeploy'];
		$ar[$i]['errorcode']=$row['errorcode'];
		$ar[$i]['progress']=$row['progress'];
		$i++ ;
        }
	$dbh = NULL ;
	return $ar ;
	
}

function updatedb($srv)
{
        $dbh = new PDO('sqlite:db/.htsqlitedb');
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$curdate = date('Y-m-d H:i:s') ;
        $res=$dbh->exec("update servers set status='deploy',lastdeploy='$curdate',progress=1 where host='$srv'") ;
	$dbh = NULL ;
}

function retry($srv)
{
        $dbh = new PDO('sqlite:db/.htsqlitedb');
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $curdate = date('Y-m-d H:i:s') ;
        $res=$dbh->exec("update servers set errorcode=0,progress=1 where host='$srv'") ;
        $dbh = NULL ;
	header('Location: /');
}

function deployall()
{

        $dbh = new PDO('sqlite:db/.htsqlitedb');
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $curdate = date('Y-m-d H:i:s') ;
        $res=$dbh->exec("update servers set status='deploy',errorcode=0,progress=1") ;
        $dbh = NULL ;
        header('Location: /');

}


function deploytype($srv)
{

	$dbh = new PDO('sqlite:db/.htsqlitedb');
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$curdate = date('Y-m-d H:i:s') ;
	$res=$dbh->exec("update servers set deploytype='',errorcode=0,progress=1 where host='$srv'") ;
	$dbh = NULL ;
	header('Location: /');

}

?>
