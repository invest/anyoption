#!/bin/bash

# mount sisense shared folder 
sudo mount -t cifs //172.16.2.29/Reports -o username=reports,password=report1234! /home/sisense/sisense-reports

# check if it was mounted OK
if [ $? -ne 0 ] ; then
echo "Cant Mount"
sleep 4
exit 1
fi
 
# fetch all mails
/usr/bin/fetchmail

cd /home/sisense/sisense-reports/mail/new


for xxx in *
do
		# divide the attached based on "From" 
       from_name=`grep -E '^From:' "$xxx" |cut -d "<" -f2|cut -d ">" -f1`
       if [ "$from_name" = "SupportNA@mediamind.com" ] ;then
               mv -vf $xxx /home/sisense/sisense-reports/mail/attachments/MediaMind/
               munpack -C /home/sisense/sisense-reports/mail/attachments/MediaMind "$xxx"
		rm -vf /home/sisense/sisense-reports/mail/attachments/MediaMind/"$xxx"
		mv -vf /home/sisense/sisense-reports/mail/attachments/MediaMind/* /home/sisense/sisense-reports/mail/attachments/MediaMind-2/
       elif [ "$from_name" = "adctr@microsoft.com" ] ;then
               mv -vf $xxx /home/sisense/sisense-reports/mail/attachments/BingAds/
               munpack -C /home/sisense/sisense-reports/mail/attachments/BingAds "$xxx"
		rm -vf /home/sisense/sisense-reports/mail/attachments/BingAds/"$xxx"
		mv -vf /home/sisense/sisense-reports/mail/attachments/BingAds/* /home/sisense/sisense-reports/mail/attachments/BingAds-2/
       else
               mv -vf $xxx /home/sisense/sisense-reports/mail/attachments/unknown/
               munpack -C /home/sisense/sisense-reports/mail/attachments/unknown "$xxx"
		rm -vf /home/sisense/sisense-reports/mail/attachments/unknown/"$xxx"
		mv -vf /home/sisense/sisense-reports/mail/attachments/unknown/* /home/sisense/sisense-reports/mail/attachments/unknown-2/
       fi
done

# dismount the server 
cd /home/sisense
sleep 2
echo "running umount command ....."
sudo umount /home/sisense/sisense-reports
mount

