#!/bin/bash
clear
echo -e "\n #########Easy way to change username from uppercase to lowercase ###########\n";
read -p "Choose Username to change :  " uppercase_username

id $uppercase_username > /dev/null 2>&1

if [ $? -eq 0 ]; then
        echo "User exist ... Getting all groups uid and gid"
        id $uppercase_username > user.txt
        awk -F "[()]" '{ for (i=2; i<NF; i+=2) print $i }' user.txt > user2.txt
        sleep 1
        cat user.txt
        read -p "UID = " uid
        read -p "GID = " gid
        read -p "Ready to delete user Type Y to delete N to Cancel : " delete

        if [ "$delete" == "Y" ]; then

                smbldap-userdel -r $uppercase_username
            if [ $? -eq 0 ]; then
                echo -e "\nUser Deleted\n"
                echo -e "\nCreate new lowercase username\n"
                read -p "First name : " first_name
                read -p "Middle name : " middle_name
                read -p "Last name : " last_name
                username1=`echo $first_name|tr [:upper:] [:lower:]`
                username2=`echo $last_name|tr [:upper:] [:lower:]`
                username2=${username2:0:1}
                username="$username1$username2"
                smbldap-useradd -u $uid -g $gid -a -c "$first_name $last_name" $username
                pdbedit -r -f "$first_name $middle_name $last_name" $username
                pdbedit -z $username &>/dev/null
                smbldap-usermod -H -L $username
                echo -e "Aa123456\nAa123456" | smbldap-passwd $username
                smbldap-usermod -B 1 $username
                sleep 1
                echo "Adding user do Groups"

                while read groups
                do
                        smbldap-groupmod -m $username $groups
                done < user2.txt
             fi

        fi
        if [ "$delete" == "N" ]; then

                echo "Canceled!"
        fi
        sleep 2

else
        echo "Error user not exist"
        sleep 1

fi
