#!/bin/bash


smbldap-userlist > userlist.txt
sed -i 's/|//g' userlist.txt
sed -i 's/[0-9]//g' userlist.txt
sed -i 's/ //g' userlist.txt
sed -i '/^$/d' userlist.txt

file=userlist.txt

while read user
do

        date1=`ldapsearch -x "uid=$user" | grep sambaLogonTime: | awk '{print $2}'`
        echo $date1
        datenow=`date +%s`
        echo $datenow
        diff1=$(expr "$datenow" - "$date1") > /dev/null 2>&1
        echo $diff1
        month=2629743
        if [ "$diff1" -ge "$month" ]; then
                echo $user
                echo $user >> checkusers.txt

        fi

done < "$file"


echo ""|mutt -i checkusers.txt -s "Users that not login more than one month" davida@anyoption.com

rm -f checkusers.txt

