#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


## generate users.csv
ldapsearch -x -h localhost -b "ou=people,dc=abarzel,dc=com"  +|grep -E '^entryUUID:' |cut -d " " -f2 >/tmp/uuids/1.yan
ldapsearch -x -h localhost -b "ou=people,dc=abarzel,dc=com"  + |grep -E '^dn: uid=' | cut -d ',' -f1 | cut -d '=' -f2 >/tmp/uuids/2.yan
paste -d "," /tmp/uuids/1.yan /tmp/uuids/2.yan >/tmp/uuids/users.csv


## generate groups.csv
ldapsearch -x -h localhost -b "ou=group,dc=abarzel,dc=com"  +|grep -E '^entryUUID:' |cut -d " " -f2 >/tmp/uuids/1.yan
ldapsearch -x -h localhost -b "ou=group,dc=abarzel,dc=com"  + |grep -E '^dn: cn=' | cut -d ',' -f1|cut -d '=' -f2 >/tmp/uuids/2.yan
paste -d "," /tmp/uuids/1.yan /tmp/uuids/2.yan >/tmp/uuids/groups.csv


## generate computers.csv
ldapsearch -x -h localhost -b "ou=group,dc=abarzel,dc=com"  +|grep -E '^entryUUID:' |cut -d " " -f2 >/tmp/uuids/1.yan
ldapsearch -x -h localhost -b "ou=hosts,dc=abarzel,dc=com"  + |grep -E '^dn: uid='|cut -d ',' -f1 |cut -d '=' -f2 >/tmp/uuids/2.yan
paste -d "," /tmp/uuids/1.yan /tmp/uuids/2.yan >/tmp/uuids/computers.csv


# mount the remote Websense share

mount -t cifs //172.16.2.7/uuid -o username=websenseuuid,password=Wimd@7227 /tmp/websenseuuid/

mv -vf /tmp/uuids/*.csv /tmp/websenseuuid/

umount /tmp/websenseuuid