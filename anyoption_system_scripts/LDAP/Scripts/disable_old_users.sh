#!/bin/bash

#this script will lock users that didn't login during the last 2 months

cd /root/Scripts/disable_users/


now=`date +%s`
days=5184000
#create list
#pdbedit -L |grep -E '[a-z]+\:[0-9]+\:'|   cut -d":" -f1 | sort -d > user.list
ldapsearch -x -h localhost -b "dc=abarzel,dc=com"  '*' |grep -E '^uid: [a-z]+$' | cut -d " " -f2 |sort -d >user.list
while read U
        do
                sed -i "/$U/d" user.list
        done < do_not_disable.users
#check the users and disable the old ones, send mail with the name
echo -e "List of disabled Users `date` \n">/tmp/List_of_users_we_disabled_today.txt
while read I
        do
                last=`ldapsearch -x -h localhost -b "ou=People,dc=abarzel,dc=com" "uid=$I" | grep sambaLogonTime: | cut -d" " -f2`
                ##[ $last==0 ] && last=$now # this is for users new users who have not logged in yet
                diff=$((now - last))
                        if [ "$diff" -gt "$days" ]
                                then
                                        smbldap-usermod -I "$I"
                                        echo "`date` --- User $I is disabled after 60 days of inactivity" >> disabled_users.txt
                                        #echo "User $I was disabled"  | mutt -s "User disabled in LDAP" itsupport@anyoption.com
                                        echo "$I" >>/tmp/List_of_users_we_disabled_today.txt
                        fi
        done < user.list

echo ""|mutt -i /tmp/List_of_users_we_disabled_today.txt -s "User disabled in LDAP" itsupport@anyoption.com
