
#!/bin/bash

if [ $# == 1 ]
then
pdbedit -z $1 &>/dev/null
smbldap-usermod -H '[U]' -U $1
echo -e "Aa123456\nAa123456" | smbldap-passwd $1
smbldap-usermod -B 1 $1
pdbedit -vL $1
echo -e "\nUser $1 was reset to default password : Aa123456 , and unlocked\n"
fi
