#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

[ -e /var/run/rsync-Design.pid ] && exit 1


echo $$ >/var/run/rsync-Design.pid

/usr/bin/rsync -atrlP --delete-after --log-file=/root/Scripts/rsync/rsync-Design.log /home/samba2/SAMBA2/Design/ root@192.168.100.24:/home/samba2/SAMBA2/Design_sync/


rm -f /var/run/rsync-Design.pid

exit 0