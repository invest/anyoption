#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


[ -e /var/run/rsync-Creative.pid ] && exit 1

# create pid file
echo $$ >/var/run/rsync-Creative.pid


/usr/bin/rsync -atrlP --delete-after --log-file=/root/Scripts/rsync/rsync-Creative.log /home/samba2/SAMBA2/Creative/ root@192.168.100.24:/home/samba2/SAMBA2/Creative_sync/

rm -f /var/run/rsync-Creative.pid

exit 0
