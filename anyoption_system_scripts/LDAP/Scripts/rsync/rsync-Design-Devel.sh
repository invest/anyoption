#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# check if its not running first

[ -e /var/run/rsync-Design-Devel.pid ] && exit 1

# create pid file
echo $$ >/var/run/rsync-Design-Devel.pid

/usr/bin/rsync -atrlP --delete-after --log-file=/root/Scripts/rsync/rsync-Design-Devel.log /home/samba2/SAMBA2/Cross-Divisions/Design-Devel/ root@192.168.100.24:/home/samba2/SAMBA2/Cross-Divisions/Design-Devel_sync/ --bwlimit=400

# remove pid file
rm -f /var/run/rsync-Design-Devel.pid

exit 0
