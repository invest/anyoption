#!/bin/bash
#

## cd to backup directory
#cd /home/samba/SAMBA/Admin/LDAP_BACKUP
cd /home/samba2/SAMBA2/IT/AO/INT/LDAP_BACKUP

## stop LDAP server ##
service slapd stop
sleep 2
## dump BDB onto ldif file
slapcat -l `date +%A`-LDAP-bkp.ldif

## start LDAP server ##
service slapd start

## Backup all config files as well
tar -czvf `date +%A`-smb-ldap-conf.tar.gz /etc/samba /etc/smbldap-tools /etc/ldap /root

## create 1 file with both backups
tar -czvf `date +%A`-abarzeldap-backup.tar.gz `date +%A`-smb-ldap-conf.tar.gz `date +%A`-LDAP-bkp.ldif

# encrypt the file
#gpg --yes -e -r ldapbackup `date +%A`-abarzeldap-backup.tar.gz
echo 'wimd7227' | gpg --yes --batch -q --passphrase-fd 0 --cipher-algo AES256 -c /home/samba2/SAMBA2/IT/AO/INT/LDAP_BACKUP/`date +%A`-abarzeldap-backup.tar.gz

# remove all temp files
rm -f `date +%A`-LDAP-bkp.ldif
rm -f `date +%A`-smb-ldap-conf.tar.gz
rm -f `date +%A`-abarzeldap-backup.tar.gz
