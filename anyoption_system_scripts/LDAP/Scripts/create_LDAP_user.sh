#!/bin/bash

clear

echo -e "This Script will guide you through creating a LDAP user\nPlease provide folowing details \n";

read -p "First name : " first_name
read -p "Middle name : " middle_name
read -p "Last name : " last_name


echo -e "Division :
1) Sale
2) Support
3) Develop IL
4) Develop BG
5) QA IL
6) QA BG
7) Product
8) Design IL
9) Design BG
10) Marketing
11) Accounting
12) Traders
13) Legal
14) HR"
read -p "Choose division number (defaut 1) : " division

echo -e "\nYou are about to create the user with this details :
First name =\e[0;36m $first_name \e[0m
Middle name =\e[0;36m $middle_name \e[0m
Last name =\e[0;36m $last_name \e[0m"
echo -e -n "Divistion = \e[0;36m"
case $division in
        1) echo -n "Sales"
        ;;
        2) echo -n "Support"
        ;;
        3) echo -n "Develop IL"
        ;;
        4) echo -n "Develop BG"
        ;;
        5) echo -n "QA IL"
        ;;
        6) echo -n "QA BG"
        ;;
        7) echo -n "Product"
        ;;
        8) echo -n "Design IL"
        ;;
        9) echo -n "Design BG"
        ;;
        10) echo -n "Marketing"
        ;;
        11) echo -n "Accounting"
        ;;
        12) echo -n "Traders"
        ;;
        13) echo -n "Legal"
        ;;
        14) echo -n "HR"
        ;;
        *) echo -n "Sales"
        ;;
esac
echo -e -n "\e[0m"

username1=`echo $first_name|tr [:upper:] [:lower:]`
username2=`echo $last_name|tr [:upper:] [:lower:]`
username2=${username2:0:1}
username="$username1$username2"

id $username &>/dev/null

if [ $? -eq 0 ];then

        echo -e "\nUsername Already Exist Please Choose a Different Username"
        read -p "username :  " username

fi



echo -e "\nUsername =\e[0;36m $username \e[0m \n"

echo -e -n "Please confirm this details are corrent (y/n) : "
read confirmation
if [ $confirmation == "y" ];then
        echo "creating the user here .... "
        smbldap-useradd -a -c "$first_name $last_name" $username || exit 1
        pdbedit -r -f "$first_name $middle_name $last_name" $username || exit 13
        case $division in
                1) smbldap-usermod -g salesgroup -G Domain_Users,saledesign $username || exit 2
                ;;
                2) smbldap-usermod -g supportgroup -G Domain_Users,marksupport $username || exit 3
                ;;
                3) smbldap-usermod -G etrader,developers,sambagroup,svn_write,devel_prod,desdev,markdev $username || exit 4
                   smbldap-usermod -s /bin/bash $username || exit 15
                ;;
                4) smbldap-usermod -g bgoffice -G bgoffice,svn_write,devel_prod,desdev,markdev,users_tomcat $username || exit 5
                   smbldap-usermod -s /bin/bash $username || exit 15
                ;;
                5) smbldap-usermod -G etrader,developers,sambagroup,svn_read,qa_prod,markdev,users_tomcat $username || exit 6
                   smbldap-usermod -s /bin/bash $username || exit 15
                ;;
                6) smbldap-usermod -g bgoffice -G bgoffice,svn_read,qa_prod,users_tomcat $username || exit 7
                  smbldap-usermod -s /bin/bash $username || exit 15
                ;;
                7) smbldap-usermod -G etrader,product,sambagroup,product-accounting $username || exit 8
                ;;
                8) smbldap-usermod -G design,sambagroup,salesdesign,desdev $username || exit 9
                ;;
                9) smbldap-usermod -g design -G sambagroup,bgoffice,desdev,salesdesign $username || exit 10
                ;;
                10) smbldap-usermod -G  marketing,sambagroup,etrader,marketing-accounting,markdev,marksupport $username || exit 11
                ;;
                11) smbldap-usermod -g accounting -G finance_group,sambagroup,etrader,marketing-accounting,product-accounting $username || exit 12
                ;;
                12)  smbldap-usermod -G etrader,traders,sambagroup $username || exit 13
                ;;
                13) smbldap-usermod -G etrader,sambagroup,legal $username || exit 14
                ;;
                14) smbldap-usermod -G hr,hr_finance $username || exit 15
                ;;
                *) echo -n "No group was selected ... default Sales"
                ;;
        esac

else
        echo "Exiting"
        exit 100
fi


pdbedit -z $username &>/dev/null
smbldap-usermod -H -L $username
echo -e "Aa123456\nAa123456" | smbldap-passwd $username
smbldap-usermod -B 1 $username
pdbedit -vL $username
echo -e "\n The user $username is connected to this groups :"
id $username
echo -e "\nUser $1 was created with default password : Aa123456 \n"

read -p "Emplyee Number : " empnumber

/etc/samba/updateLogonTime.sh $username
/etc/samba/addemployeeNumber.sh $username $empnumber
