#!/bin/bash

if [ $# == 1 ]
then
pdbedit -z $1 &>/dev/null
smbldap-usermod -H -L $1
pdbedit -vL $1
fi
