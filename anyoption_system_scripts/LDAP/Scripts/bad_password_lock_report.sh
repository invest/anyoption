#!/bin/bash

# this script will check all users for bad password locks
# if it find new it will send a report to $EMAIL_LIST


EMAIL_LIST="itsupport@anyoption.com"


# find the list and put in tmp file
pdbedit -vL |grep -B1 -A20 'Account Flags:        \[UL' |grep -E "NT username|Last bad password|Bad password count" >/tmp/bp_lock.tmp

# check if we have differences in files
diff /tmp/bp_lock.tmp /tmp/bp_lock.old
# if so , send an email and save the changes
if [ $? -gt 0 ]
then
        cp /tmp/bp_lock.tmp /tmp/bp_lock.old
        cat /tmp/bp_lock.old |mutt -s "bad-password-lock `date`" $EMAIL_LIST
fi

exit 0
