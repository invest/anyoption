package il.co.etrader.tomcat;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import org.apache.log4j.Logger;
import org.apache.naming.ResourceRef;

import com.anyoption.common.util.AESUtil;

/**
 * A JNDI resource factory that create UCP pools.
 * 
 * @author Tony
 */
public class UCPFactory implements ObjectFactory {
    private static final Logger log = Logger.getLogger(UCPFactory.class);

    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {
        if (obj instanceof ResourceRef) {
            try {
                Reference ref = (Reference) obj;
                PoolDataSource pool = PoolDataSourceFactory.getPoolDataSource();
                Object bean = pool;
                Class beanClass = bean.getClass();
                BeanInfo bi = Introspector.getBeanInfo(beanClass);
                PropertyDescriptor[] pda = bi.getPropertyDescriptors();

                Enumeration e = ref.getAll();
                while (e.hasMoreElements()) {
                    RefAddr ra = (RefAddr) e.nextElement();
                    String propName = ra.getType();
                    if (propName.equals("factory") || propName.equals("scope") || propName.equals("auth")) {
                        continue;
                    }
                    
                    String value = (String)ra.getContent();

                    if (propName.equals("url")) {
                        pool.setURL(value);
                        continue;
                    }
                    if (propName.equals("sqlForValidateConnection")) {
                        pool.setSQLForValidateConnection(value);
                        continue;
                    }
                    if (propName.equals("onsConfiguration")) {
                        pool.setONSConfiguration(value);
                        continue;
                    }
                    
                    if (propName.equalsIgnoreCase("dbuser")) {
                        pool.setUser(value);
                        log.debug("Setting user from dbuser: " + value);
                        continue;
                    }
                    
                    if (propName.equalsIgnoreCase("password")) {
                    	pool.setPassword(AESUtil.decrypt(value));
                    	continue;
                    }

                    Object[] valueArray = new Object[1];
                    int i = 0;
                    for (i = 0; i < pda.length; i++) {
                        if (pda[i].getName().equals(propName)) {
                            Class propType = pda[i].getPropertyType();
                            if (propType.equals(String.class)) {
                                valueArray[0] = value;
                            } else if (propType.equals(Character.class) 
                                    || propType.equals(char.class)) {
                                valueArray[0] = new Character(value.charAt(0));
                            } else if (propType.equals(Byte.class) 
                                    || propType.equals(byte.class)) {
                                valueArray[0] = new Byte(value);
                            } else if (propType.equals(Short.class) 
                                    || propType.equals(short.class)) {
                                valueArray[0] = new Short(value);
                            } else if (propType.equals(Integer.class) 
                                    || propType.equals(int.class)) {
                                valueArray[0] = new Integer(value);
                            } else if (propType.equals(Long.class) 
                                    || propType.equals(long.class)) {
                                valueArray[0] = new Long(value);
                            } else if (propType.equals(Float.class) 
                                    || propType.equals(float.class)) {
                                valueArray[0] = new Float(value);
                            } else if (propType.equals(Double.class) 
                                    || propType.equals(double.class)) {
                                valueArray[0] = new Double(value);
                            } else if (propType.equals(Boolean.class)
                                    || propType.equals(boolean.class)) {
                                valueArray[0] = new Boolean(value);
                            } else {
                                throw new NamingException("String conversion for property type '" + propType.getName() + "' not available");
                            }

                            Method setProp = pda[i].getWriteMethod();
                            if (setProp != null) {
                                if (log.isDebugEnabled()) {
                                    log.debug("Setting " + propName + " = " + value);
                                }
                                setProp.invoke(bean, valueArray);
                            } else {
                                throw new NamingException("Write not allowed for property: " + propName);
                            }
                            break;
                        }
                    }
                    // There seems to be some bug in Tomcat 7 and it tries to set property "singleton"
                    // to the objects that is not configured and not expected.
                    if (i == pda.length && !propName.equals("singleton")) {
                        throw new NamingException("No set method found for property: " + propName);
                    } else if (propName.equals("singleton")) {
                        log.warn("Trying to set property singleton!");
                    }
                }
                return bean;
            } catch (java.beans.IntrospectionException ie) {
                NamingException ne = new NamingException(ie.getMessage());
                ne.setRootCause(ie);
                throw ne;
            } catch (java.lang.IllegalAccessException iae) {
                NamingException ne = new NamingException(iae.getMessage());
                ne.setRootCause(iae);
                throw ne;
            } catch (java.lang.reflect.InvocationTargetException ite) {
                NamingException ne = new NamingException(ite.getMessage());
                ne.setRootCause(ite);
                throw ne;
            }
        } else {
            return null;
        }
    }
}