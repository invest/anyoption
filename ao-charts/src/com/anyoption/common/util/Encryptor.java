package com.anyoption.common.util;

import java.io.UnsupportedEncodingException;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.PaddedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;

//A simple example that uses the Bouncy Castle
//lightweight cryptography API to perform DES
//encryption of arbitrary data.

@Deprecated
public class Encryptor {

	private static BufferedBlockCipher cipher;
	private static KeyParameter key;
	public static final String SECRET_KEY = "dh3dilb68";

	// Initialize the cryptographic engine.
	// The key array should be at least 8 bytes long.

	public static void main(String[] args) throws Exception {
		String text = 	"108_-95_51_-34_84_111_26_23";
		System.out.println("text: " + text);

		String enc = Encryptor.encryptStringToString(text);
		System.out.println("enc: " + enc);
		String dec = Encryptor.decryptStringToString(text);
		System.out.println("dec: " + dec);
		System.out.println(text.equals(dec));
	}

	static {
	     cipher = new PaddedBlockCipher(new CBCBlockCipher(new BlowfishEngine()));
	     key = new KeyParameter(SECRET_KEY.getBytes());
	 }

	 // Private routine that does the gritty work.
	 private static byte[] callCipher(byte[] data) throws CryptoException {
	     int size = cipher.getOutputSize(data.length);
	     byte[] result = new byte[size];
	     int olen = cipher.processBytes(data, 0, data.length, result, 0);
	     olen += cipher.doFinal(result, olen);

	     if (olen < size) {
	         byte[] tmp = new byte[olen];
	         System.arraycopy(result, 0, tmp, 0, olen);
	         result = tmp;
	     }
	     return result;
	 }

	 // Encrypt arbitrary byte array, returning the
	 // encrypted data in a different byte array.
	 private static synchronized byte[] encrypt(byte[] data) throws CryptoException {
	     if(data == null || data.length == 0){
	         return new byte[0];
	     }
	     cipher.init(true, key);
	     return callCipher(data);
	 }

//	 public static synchronized String encryptStringToString(long longData) throws CryptoException {
//		 return encryptStringToString(String.valueOf(longData));
//	 }

	 public static synchronized String encryptStringToString(String data) throws CryptoException {
		 byte[] byteArr;
		 byteArr = encryptString(data);
		 String res = "";
		 for (int i=0; i<byteArr.length; i++) {
			 res += new Byte(byteArr[i]).intValue() + "_";
		 }
		 res = res.substring(0, res.length()-1);
		 return res;
	 }

	 // Encrypts a string
	 private static byte[] encryptString(String data) throws CryptoException {
	     if(data == null || data.length() == 0) {
	         return new byte[0];
	     }
	     try {
	    	 return encrypt(data.getBytes("UTF-8"));
	     } catch (UnsupportedEncodingException e) {
			 throw new CryptoException("UnsupportedEncodingException");
		 }
	 }

	 // Decrypts arbitrary data.
	private static synchronized byte[] decrypt(byte[] data) throws CryptoException {
	     if (data == null || data.length == 0) {
	         return new byte[0];
	     }
	     cipher.init(false, key);
	     return callCipher(data);
	 }

	 // Decrypts a string that was previously encoded
	 // using encryptString.
	 private static String decryptString(byte[] data) throws CryptoException {
	     if(data == null || data.length == 0){
	         return "";
	     }
	     try {
	    	 return new String(decrypt(data), "UTF-8");
	     } catch (UnsupportedEncodingException e) {
			 throw new CryptoException("UnsupportedEncodingException");
		 }
	 }

	 public static synchronized String decryptStringToString(String data) throws CryptoException {

		 String[] stringArr = data.split("_");
		 byte[] byteArr = new byte[stringArr.length];
		 for (int i=0; i<stringArr.length; i++) {
			 byteArr[i] = Integer.valueOf(stringArr[i]).byteValue();
		 }
		 return decryptString(byteArr);
	}
}