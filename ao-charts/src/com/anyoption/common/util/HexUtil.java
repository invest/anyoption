package com.anyoption.common.util;

public class HexUtil {
    /**
     * Parse hex string to byte array.
     *
     * @param str
     * @return
     */
    public static byte[] hexStringToByteArray(String str) {
        byte[] arr = new byte[str.length() / 2];
        for (int i = 0; i < arr.length; i++) {
            if (str.length() > 2) {
                arr[i] = (byte) Integer.parseInt(str.substring(0, 2), 16);
                str = str.substring(2);
            } else {
                arr[i] = (byte) Integer.parseInt(str, 16);
            }
        }
        return arr;
    }

    /**
     * Makes a hex string to have a requested len. For example if you need hex string with len 4 you can pass one with
     * len 1 and this function will pad it on the left with the correct number of 0s so the len becomes 4.
     *
     * @param hex the current hex str
     * @param len the needed len
     * @return New string that contains the padded hex string.
     */
    public static String toFixedLenHexString(String hex, int len) {
        String pad = "0000000000";
        int ln = hex.length();
        if (ln < len) {
            return pad.substring(0, len - ln) + hex;
        }

        return hex;
    }

	/**
     * Turns a byte array to hex string.
     *
     * @param arr the byte array
     * @return A hex string presenting the byte array content.
     */
    public static String byteArrayToHexString(byte[] arr) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(toFixedLenHexString(Integer.toHexString(arr[i] & 0x000000FF), 2));
        }

        return sb.toString();
    }
}