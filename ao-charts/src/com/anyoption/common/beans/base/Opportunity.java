package com.anyoption.common.beans.base;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * Contain all the information that need to be on an opportunity.
 * 
 * 
 * @author liors
 *
 */
public class Opportunity implements java.io.Serializable {
   
    private static final long serialVersionUID = 1L;

    public static final int PUBLISHED_YES = 1;
    public static final int PUBLISHED_NO = 0;
    public static final int SETTLED_YES = 1;
    public static final int SETTLED_NO = 0;

    public static final long TYPE_REGULAR = 1;
    public static final long TYPE_ONE_TOUCH = 2;
    public static final long TYPE_OPTION_PLUS = 3;
    public static final long TYPE_BINARY_0_100_ABOVE = 4;
    public static final long TYPE_DYNAMICS = 7;

    public static final int STATE_CREATED = 1;
    public static final int STATE_OPENED = 2;
    public static final int STATE_LAST_10_MIN = 3;
    public static final int STATE_CLOSING_1_MIN = 4;
    public static final int STATE_CLOSING = 5;
    public static final int STATE_CLOSED = 6;
    public static final int STATE_DONE = 7;
    public static final int STATE_PAUSED = 8;
    
    public static final int OPTION_PLUS_COMMISSION = 50;
    public static final int OPTION_PLUS_COMMISSION_RUB = 1000;
    public static final int OPTION_PLUS_COMMISSION_YUAN = 100;
    public static final int OPTION_PLUS_COMMISSION_KOREAN = 50000;
    /**
     * This is not really an opportunity state. It is more display state. You
     * will not find opportunity in that state. This code is sent as state only
     * to LSs so the trading pages display the market as suspended.
     */
    public static final int STATE_SUSPENDED = 9;
    /**
     * This state is only for long term opps after they should be gone (after
     * day last time invest) before market close for the day.
     */
    public static final int STATE_WAITING_TO_PAUSE = 10;
    /**
     * This state is only for market id 436 = maof market its mean this market
     * is waiting for expiry level. we will be in this market 15 min after the
     * market is waiting for expiry level!
     */
    public static final int STATE_HIDDEN_WAITING_TO_EXPIRY = 11;
    
    /**
     * Not a real state. Display state for Binary 0-100 updating prices (auto halt).
     */
    public static final int STATE_BINARY0100_UPDATING_PRICES = 12;

    public static final int SCHEDULED_ALL = 0;
    public static final int SCHEDULED_HOURLY = 1;
    public static final int SCHEDULED_DAYLY = 2;
    public static final int SCHEDULED_WEEKLY = 3;
    public static final int SCHEDULED_MONTHLY = 4;
    public static final int SCHEDULED_QUARTERLY = 6;

    @Expose
	protected long id;
    @Expose
	protected long marketId;
	protected Date timeFirstInvest;
	protected Date timeEstClosing;
	protected Date timeLastInvest;
    @Expose
    protected long timeFirstInvestMillsec;
    @Expose
    protected long timeLastInvestMillsec;
    @Expose
    protected long timeEstClosingMillsec;
    @Expose
	protected double currentLevel;
	protected double closingLevel;
	//protected boolean published;
	protected int isPublished;
	//protected boolean settled;
	protected int isSettled;
	protected boolean disabled;
	protected int scheduled;
	protected long typeId;
	protected String marketName;
	@Expose
	protected float pageOddsWin;
	@Expose
	protected float pageOddsLose;
	
	/**
	 * for the LS data adaptor - the exposure calculation amounts
	 */
	protected int state;
	/**
	 * represent the formula and the parameters with there values
	 */
	protected String closeLevelTxt; 
	/**
	 * one touch variables
	 */
	protected long decimalPointOneTouch;
	protected String timeEstClosingTxt;

	/**
	 * @return timeEstClosingTxt
	 */
	public String getTimeEstClosingTxt() {
		return timeEstClosingTxt;
	}

	/**
	 * @param timeEstClosingTxt
	 */
	public void setTimeEstClosingTxt(String timeEstClosingTxt) {
		this.timeEstClosingTxt = timeEstClosingTxt;
	}

	public Opportunity() {
	}

	/**
	 * @return closingLevel
	 */
	public double getClosingLevel() {
		return closingLevel;
	}

	/**
	 * @param closingLevel
	 */
	public void setClosingLevel(double closingLevel) {
		this.closingLevel = closingLevel;
	}

	/**
	 * @return id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

    /**
     * @return published
     */
    public boolean isPublished() {
        if (this.isPublished == Opportunity.PUBLISHED_YES){
            return true;
        } else {
            return false;
        }        
    }

    /**
     * @param published
     */
    public void setPublished(boolean published) {
        if (published){
            setIsPublished(PUBLISHED_YES);
        } else {
            setIsPublished(PUBLISHED_NO);
        }
    }

	/**
	 * @return settled
	 */
	public boolean isSettled() {
	    if (this.isSettled == Opportunity.SETTLED_YES){
            return true;
        } else {
            return false;
        }        
    }

    /**
     * @param settled
     */
    public void setSettled(boolean settled) {
        if (settled){
            setIsSettled(SETTLED_YES);
        } else {
            setIsSettled(SETTLED_NO);
        }
    }

    /**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the timeFirstInvest
	 */
	public Date getTimeFirstInvest() {
		return timeFirstInvest;
	}

	/**
	 * @param timeFirstInvest the timeFirstInvest to set
	 */
	public void setTimeFirstInvest(Date timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	/**
	 * @return the timeEstClosing
	 */
	public Date getTimeEstClosing() {
		return timeEstClosing;
	}

	/**
	 * @param timeEstClosing the timeEstClosing to set
	 */
	public void setTimeEstClosing(Date timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	/**
	 * @return the timeLastInvest
	 */
	public Date getTimeLastInvest() {
		return timeLastInvest;
	}

	/**
	 * @param timeLastInvest the timeLastInvest to set
	 */
	public void setTimeLastInvest(Date timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	/**
	 * @return Returns the currentLevel.
	 */
	public double getCurrentLevel() {
		return currentLevel;
	}

	/**
	 * @param currentLevel
	 *            The currentLevel to set.
	 */
	public void setCurrentLevel(double currentLevel) {
		this.currentLevel = currentLevel;
	}

	/**
	 * @return Returns the typeId.
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId
	 *            The typeId to set.
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return scheduled
	 */
	public int getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled
	 */
	public void setScheduled(int scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return closeLevelTxt
	 */
	public String getCloseLevelTxt() {
		return closeLevelTxt;
	}

	/**
	 * @param tempCloseLevelTxt
	 */
	public void setCloseLevelTxt(String tempCloseLevelTxt) {
		this.closeLevelTxt = tempCloseLevelTxt;
	}

	/**
	 * @return the decimalPointOneTouch
	 */
	public long getDecimalPointOneTouch() {
		return decimalPointOneTouch;
	}

	/**
	 * @param decimalPointOneTouch
	 *            the decimalPointOneTouch to set
	 */
	public void setDecimalPointOneTouch(long decimalPointOneTouch) {
		this.decimalPointOneTouch = decimalPointOneTouch;
	}

	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	/**
	 * @return the pageOddsWin
	 */
	public float getPageOddsWin() {
		return pageOddsWin;
	}

	/**
	 * @param pageOddsWin the pageOddsWin to set
	 */
	public void setPageOddsWin(float pageOddsWin) {
		this.pageOddsWin = pageOddsWin;
	}

	/**
	 * @return the pageOddsLose
	 */
	public float getPageOddsLose() {
		return pageOddsLose;
	}

	/**
	 * @param pageOddsLose the pageOddsLose to set
	 */
	public void setPageOddsLose(float pageOddsLose) {
		this.pageOddsLose = pageOddsLose;
	}

	/**
	 * @return the timeFirstInvestMillsec
	 */
	public long getTimeFirstInvestMillsec() {
		return timeFirstInvestMillsec;
	}

	/**
	 * @param timeFirstInvestMillsec the timeFirstInvestMillsec to set
	 */
	public void setTimeFirstInvestMillsec(long timeFirstInvestMillsec) {
		this.timeFirstInvestMillsec = timeFirstInvestMillsec;
	}

	/**
	 * @return the timeLastInvestMillsec
	 */
	public long getTimeLastInvestMillsec() {
		return timeLastInvestMillsec;
	}

	/**
	 * @param timeLastInvestMillsec the timeLastInvestMillsec to set
	 */
	public void setTimeLastInvestMillsec(long timeLastInvestMillsec) {
		this.timeLastInvestMillsec = timeLastInvestMillsec;
	}

	/**
	 * @return the timeEstClosingMillsec
	 */
	public long getTimeEstClosingMillsec() {
		return timeEstClosingMillsec;
	}

	/**
	 * @param timeEstClosingMillsec the timeEstClosingMillsec to set
	 */
	public void setTimeEstClosingMillsec(long timeEstClosingMillsec) {
		this.timeEstClosingMillsec = timeEstClosingMillsec;
	}

    /**
     * @return the isPublished
     */
    public int getIsPublished() {
        return isPublished;
    }

    /**
     * @param isPublished the isPublished to set
     */
    public void setIsPublished(int isPublished) {
        this.isPublished = isPublished;
    }

    /**
     * @return the isSettled
     */
    public int getIsSettled() {
        return isSettled;
    }

    /**
     * @param isSettled the isSettled to set
     */
    public void setIsSettled(int isSettled) {
        this.isSettled = isSettled;
    }

    @Override
    public String toString() {
    	String ls = System.getProperty("line.separator");
    	return ls + "Opportunity:" + ls
    			+ super.toString() + ls
    			+ "id: " + id + ls
				+ "marketId: " + marketId + ls
				+ "timeFirstInvest: " + timeFirstInvest + ls
				+ "timeEstClosing: " + timeEstClosing + ls
				+ "timeLastInvest: " + timeLastInvest + ls
				+ "timeFirstInvestMillsec: " + timeFirstInvestMillsec + ls
				+ "timeLastInvestMillsec: " + timeLastInvestMillsec + ls
				+ "timeEstClosingMillsec: " + timeEstClosingMillsec + ls
				+ "currentLevel: " + currentLevel + ls
				+ "closingLevel: " + closingLevel + ls
				+ "isPublished: " + isPublished + ls
				+ "isSettled: " + isSettled + ls
				+ "disabled: " + disabled + ls
				+ "scheduled: " + scheduled + ls
				+ "typeId: " + typeId + ls
				+ "marketName: " + marketName + ls
				+ "pageOddsWin: " + pageOddsWin + ls
				+ "pageOddsLose: " + pageOddsLose + ls
				+ "state: " + state + ls
				+ "closeLevelTxt: " + closeLevelTxt + ls
				+ "decimalPointOneTouch: " + decimalPointOneTouch + ls
				+ "timeEstClosingTxt: " + timeEstClosingTxt + ls;
    }
} 