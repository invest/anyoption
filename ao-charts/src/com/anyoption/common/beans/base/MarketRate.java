package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MarketRate implements Serializable {
	private static final long serialVersionUID = -7511645178062909481L;
	
	protected Date rateTime;
    protected BigDecimal graphRate;
    protected BigDecimal dayRate;
    
    // Mobile client related
    protected float chartXPosition;

    public MarketRate() {
    }
    
    public MarketRate(Date rateTime,
    					BigDecimal graphRate,
    					BigDecimal dayRate,
    					float chartXPosition) {
        this.rateTime = rateTime;
        this.graphRate = graphRate;
        this.dayRate = dayRate;
        this.chartXPosition = chartXPosition;
    }
    
    public Date getRateTime() {
        return rateTime;
    }

    public void setRateTime(Date rateTime) {
        this.rateTime = rateTime;
    }

    public BigDecimal getGraphRate() {
        return graphRate;
    }

    public void setGraphRate(BigDecimal graphRate) {
        this.graphRate = graphRate;
    }

    public BigDecimal getDayRate() {
        return dayRate;
    }

    public void setDayRate(BigDecimal dayRate) {
        this.dayRate = dayRate;
    }

    public float getChartXPosition() {
        return chartXPosition;
    }

    public void setChartXPosition(float chartXPosition) {
        this.chartXPosition = chartXPosition;
    }

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "MarketRate" + ls
				+ super.toString() + ls
				+ "rateTime: " + rateTime + ls
				+ "graphRate: " + graphRate + ls
				+ "dayRate: " + dayRate + ls
				+ "chartXPosition: " + chartXPosition + ls;
	}
}