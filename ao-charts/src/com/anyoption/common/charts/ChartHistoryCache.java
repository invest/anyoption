package com.anyoption.common.charts;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.MarketWidgetData;
import com.anyoption.common.enums.SkinGroup;

public class ChartHistoryCache implements ChartsUpdaterListener {
    private static final Logger log = Logger.getLogger(ChartHistoryCache.class);
    
    private Hashtable<Long, Map<SkinGroup, List<MarketRate>>> histories;
    private Hashtable<Long, MarketWidgetData> marketsWidgetData;

    public ChartHistoryCache() {
        histories = new Hashtable<Long, Map<SkinGroup, List<MarketRate>>>();
        marketsWidgetData = new Hashtable<Long, MarketWidgetData>();
    }

    public void marketRates(ChartsUpdaterMarket m, Map<SkinGroup, List<MarketRate>> mrs) {
        if (log.isTraceEnabled()) {
            log.trace("Got for market " + m.getId() + " rates " + getRatesNumberPerGroupMap(mrs));
        }        
        Map<SkinGroup, List<MarketRate>> h = histories.get(m.getId());
        MarketWidgetData mwd = new MarketWidgetData();
        mwd.setPageOddsWin(m.getOddsWin());
        mwd.setTimeEstClosingMillsec(m.getOpenOppEstClosing().getTime());
        marketsWidgetData.put(m.getId(), mwd);
        boolean shouldAddToHistories = false;
        if (null == h) {
            h = new EnumMap<SkinGroup, List<MarketRate>>(SkinGroup.class);
            for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
				h.put(skinGroup, new ArrayList<MarketRate>());
			}
            shouldAddToHistories = true;
        }
        long lastHourOpeningTime = 0;
        if (null != m.getLastHourClosingTime()) { // markets that don't have closed opp yet
            lastHourOpeningTime = m.getLastHourClosingTime().getTime() - 60 * 60 * 1000;
        }
        
        for (Map.Entry<SkinGroup, List<MarketRate>> entry : h.entrySet()) {
        	SkinGroup skinGroup = entry.getKey();
        	List<MarketRate> cachedRates = entry.getValue();
        	
        	// remove all rates older than prev hour opening time
        	while (cachedRates.size() > 0 && cachedRates.get(0).getRateTime().getTime() < lastHourOpeningTime) {
        		cachedRates.remove(0);
        	}
        	
        	List<MarketRate> rates = mrs.get(skinGroup);
        	if (rates != null) {
	        	for (int i = 0; i < rates.size(); i++) {
	        		if (rates.get(i).getRateTime().getTime() >= lastHourOpeningTime) {
	        			cachedRates.add(rates.get(i));
	        		}
	        	}
        	}
		}
        
        if (log.isTraceEnabled()) {
            log.trace("In the end " + getRatesNumberPerGroupMap(h));
        }
        if (shouldAddToHistories) {
            histories.put(m.getId(), h);
            log.trace("Added to histories");
        }
    }

    public void marketClosed(ChartsUpdaterMarket m) {
        histories.remove(m.getId());
        marketsWidgetData.remove(m.getId());
    }

	public Map<SkinGroup, List<MarketRate>> getMarketHistory(long marketId) {
    	Map<SkinGroup, List<MarketRate>> h = histories.get(marketId);
        if (null != h) {
        	Map<SkinGroup, List<MarketRate>> map = new EnumMap<SkinGroup, List<MarketRate>>(h);
        	for (SkinGroup skinGroup : map.keySet()) {
				map.put(skinGroup, new ArrayList<MarketRate>(map.get(skinGroup)));
			}
            return map;
        }
        return null;
    }
	
	public MarketWidgetData getMarketWidgetData(long marketId) {
		return marketsWidgetData.get(marketId);        
	}
    
    private Map<SkinGroup, Integer> getRatesNumberPerGroupMap(Map<SkinGroup, List<MarketRate>> map) {
    	Map<SkinGroup, Integer> ratesNumberPerGroup = new EnumMap<SkinGroup, Integer>(SkinGroup.class);
    	for (Map.Entry<SkinGroup, List<MarketRate>> entry : map.entrySet()) {
			ratesNumberPerGroup.put(entry.getKey(), entry.getValue().size());
		}
    	return ratesNumberPerGroup;
    }
}