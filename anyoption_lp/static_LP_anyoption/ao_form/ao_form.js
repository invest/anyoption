var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;
var SKIN_ID_EN_US_AOPS	= 25;
var SKIN_ID_ES_AOPS 	= 26;
var SKIN_ID_CZ 			= 27;
var SKIN_ID_PL 			= 28;
var SKIN_ID_PT 			= 29;

function getLangCodeBySkinId(skinId) {
	//set default
	var langCode = "en";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
		case SKIN_ID_EN_US_AOPS:
			langCode = "en";
			break;
		case SKIN_ID_TR: 
			langCode = "tr";
			break;
		case SKIN_ID_AR:
			langCode = "ar";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
		case SKIN_ID_ES_AOPS:
			langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			langCode = "it";
			break;
		case SKIN_ID_RU:
			langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			langCode = "fr";
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			langCode = "zh";
			break;
		case SKIN_ID_KR: 
			langCode = "kr";
			break;
		case SKIN_ID_NL: 
			langCode = "nl";
			break;
		case SKIN_ID_SV: 
			langCode = "sv";
			break;
		case SKIN_ID_CZ: 
			langCode = "cs";
			break;
		case SKIN_ID_PL: 
			langCode = "pl";
			break;
		case SKIN_ID_PT: 
			langCode = "pt";
			break;
	}
	return langCode;
}


function getDomainBySkinId(skinId) {
	//set default
	var domain = "www.anyoption.com";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
		case SKIN_ID_EN_US_AOPS:
			domain = "www.anyoption.com";
			break;
		case SKIN_ID_TR: 
			domain = "tr.anyoption.com";
			break;
		case SKIN_ID_AR:
			domain = "ar.anyoption.com";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
		case SKIN_ID_ES_AOPS:
			domain = "es.anyoption.com";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			domain = "de.anyoption.com";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			domain = "www1.anyoption.it";
			break;
		case SKIN_ID_RU:
			domain = "ru.anyoption.com";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			domain = "fr.anyoption.com";
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			domain = "zh.anyoption.com";
			break;
		case SKIN_ID_KR: 
			domain = "kr.anyoption.com";
			break;
		case SKIN_ID_NL: 
			domain = "nl.anyoption.com";
			break;
		case SKIN_ID_SV: 
			domain = "sv.anyoption.com";
			break;
		case SKIN_ID_CZ: 
			domain = "cz.anyoption.com";
			break;
		case SKIN_ID_PL: 
			domain = "pl.anyoption.com";
			break;
		case SKIN_ID_PT: 
			domain = "pt.anyoption.com";
			break;
	}
	return domain;
}


var msgs = {};
var aoForm = $('[data-type="aoForm"]');

var aoFormSettings = {
	passwordPage: 'https://' + getDomainBySkinId(skinId) + '/jsp/password.jsf',
	get_countries_link: context_path_ajax + '/AnyoptionService/getSortedCountries',
	validate_email_link: context_path_ajax + '/jsonService/AnyoptionService/validateEmail',
	insert_register_attempts_link: context_path_ajax + '/jsp/ajax.jsf',
	ao_submit_action: 'https://' + getDomainBySkinId(skinId) + '/jsp/landingDirect.html',
	//ao_submit_action: 'https://' + getDomainBySkinId(skinId).replace('.anyoption.', '.bgtestenv.anyoption.') + '/jsp/landingDirect.html',
	termsLink: 'https://www.anyoption.com/jsp/agreement.jsf',
	regEx_lettersAndNumbers: /^[0-9a-zA-Z]+$/,
	min_password_length: 6,
	regEx_nickname: /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/,
	regEx_nickname_reverse: /[^a-zA-Z0-9!@#$%\^:;"']/g,
	regEx_phone : /^\d{7,20}$/,
	regEx_digits: /^\d+$/,
	regEx_digits_reverse : /\D/g,
	regEx_email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	regEx_englishLettersOnly: /^['a-zA-Z ]+$/,
	regEx_lettersOnly: "^['\\p{L} ]+$"
};


function initAoForm(template){
	if(!template){
		template = 'template_default';
	}
	$.ajax({
		url     : "../ao_form/templates/" + template + ".shtml",
		type    : "GET",
		data    : "",
		success : function(data) {
			document.getElementById('aoFormHolder').innerHTML = data;
			aoForm = $('[data-type="aoForm"]');
			getTranslations(getLangCodeBySkinId(skinId));
			getCountries();
		},
		error   : function( xhr, err ) {
			
		}
	});
}


var getTranslationsIterations = 0;
function getTranslations(langCode){
	getTranslationsIterations++;
	$.ajax({
		url     : "/ao_form/translations/" + langCode + ".js",
		type    : "GET",
		dataType: "json",
		data    : "",
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			msgs = data;
			if(msgs){
				initAoFormData();
			}else if(getTranslationsIterations <= 2){
				getTranslations("en");
			}
		},
		error   : function( xhr, err ) {
			if(getTranslationsIterations <= 2){
				getTranslations("en");
			}
		}
	});
}
//getTranslations(getLangCodeBySkinId(skinId));

var countriesMap = {};
function getCountries(){
	var request = {skinId: skinId};
	$.ajax({
		url     : aoFormSettings.get_countries_link,
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			countriesMap = data.countriesMap;
			var defaultCountryId = '';
			if (isUndefined(data.countriesMap[data.ipDetectedCountryId])) {
				defaultCountryId = data.skinDefautlCointryId;
			} else {
				defaultCountryId = (data.ipDetectedCountryId != 0) ? data.ipDetectedCountryId : data.skinDefautlCointryId; 
			}
			//Fill countries list
			var countriesSelect = $('[data-type="phoneCode"]');
			fillCountriesList(countriesSelect, defaultCountryId);
		},
		error   : function( xhr, err ) {
			
		}
	});
}
//getCountries();


function parseParams(params){
	return params;
}
function getMsgs(key, params){
	if(!params){
		return msgs[key];
	}else{
		params = eval(params);
		for(paramKey in params){
			if(params.hasOwnProperty(paramKey)){
				return msgs[key].replace('{' + paramKey + '}', params[paramKey]);
			}
		}
	}
}

function initAoFormData(){
	if(aoForm.length > 0){
		//Translate texts
		aoForm.find('[data-text]').each(function(index, el){
			el.innerHTML = getMsgs($(el).data('text'), $(el).data('text-params'));
		});
		
		//Translate placeholders
		aoForm.find('[data-placeholder]').each(function(index, el){
			$(el).attr('placeholder', getMsgs($(el).data('placeholder'), $(el).data('placeholder-params')));
		});
		
		aoForm.find('[name]').each(function(index, el){
			$(el).on('change keydown', function(event){
				clearError($(el).attr('name'));
			})
		});
	}
}

function fillCountriesList(countriesSelect, selectedItemKey){
	if(countriesSelect.length > 0){
		var countries = [];
		for(key in countriesMap){
			if(countriesMap.hasOwnProperty(key)){
				countries.push({key: key, phoneCode: countriesMap[key].phoneCode, text: countriesMap[key].displayName});
			}
		}
		countries.sort(function sortCountries(a, b){
			var nameA = a.text.toUpperCase();
			var nameB = b.text.toUpperCase();
			if(nameA < nameB){
				return -1;
			}
			if(nameA > nameB){
				return 1;
			}
			return 0;
		});
		for(var i = 0; i < countries.length; i++){
			var option = document.createElement('option');
			option.value = countries[i].key;
			option.innerHTML = countries[i].text;
			if(countries[i].key == selectedItemKey){
				option.selected = "selected";
			}
			countriesSelect.append(option);
		}
	}
	updatePhoneCode();
}

function getValueByName(name){
	if(!aoForm[0] || !aoForm[0][name]){
		return null;
	}
	if(aoForm[0][name].type == 'checkbox'){
		return aoForm[0][name].checked;
	}
	return aoForm[0][name].value;
}

function submitAoForm(){
	clearErrors();
	
	if(!validateCoErrors()){
		return false;
	}
	
	if(!aoForm[0]){
		return null;
	}
	
	var funnel_fingerPrint = document.getElementById('funnel_fingerPrint'); 
	if(funnel_fingerPrint.value == ''){
		funnel_fingerPrint.value = new Fingerprint().get();
	}
	appendHiddenInput("s", skinId);
	appendHiddenInput("funnel_countryId", getValueByName('funnel_phonePrefix'));
	appendHiddenInput("combid", getUrlValue("combid"));
	appendHiddenInput("dp", getUrlValue("dp"));
	appendHiddenInput("aff_sub1", getUrlValue("aff_sub1"));
	appendHiddenInput("aff_sub2", getUrlValue("aff_sub2"));
	if(getUrlValue("gclid") != ""){
		appendHiddenInput("gclid", getUrlValue("gclid"));
	}else{
		appendHiddenInput("aff_sub3", getUrlValue("aff_sub3"));
	}
	
	aoForm[0].action = aoFormSettings.ao_submit_action;
	aoForm[0].submit();
}

function appendHiddenInput(param, value){
	var newInput = document.createElement("input");
	newInput.type = "hidden";
	newInput.name = param;
	newInput.value = value;
	aoForm[0].appendChild(newInput);
}

function validateCoErrors(){
	var isValid = true;
	aoForm.find('[data-valid-password]').each(function(index, el){
		if(el.value.length < aoFormSettings.min_password_length){
			isValid = false;
			setError($(el).attr('name'), getMsgs('password-too-short'));
		}
	});
	aoForm.find('[data-valid-email]').each(function(index, el){
		if(!aoFormSettings.regEx_email.test(el.value)){
			isValid = false;
			setError($(el).attr('name'), getMsgs('invalid-email'));
		}
	});
	aoForm.find('[data-valid-phone]').each(function(index, el){
		if(!aoFormSettings.regEx_phone.test(el.value)){
			isValid = false;
			if(!aoFormSettings.regEx_digits.test(el.value)){
				setError($(el).attr('name'), getMsgs('invalid-phone'));
			}else{
				if(el.value.length < 7 || el.value.length > 20){
					setError($(el).attr('name'), getMsgs('invalid-phone-min-length'));
				}else{
					setError($(el).attr('name'), getMsgs('invalid-phone'));
				}
			}
		}
	});
	aoForm.find('[data-accept-terms]').each(function(index, el){
		if(!el.checked){
			isValid = false;
			setError($(el).attr('name'), getMsgs('you-must-accept-terms'));
		}
	});
	aoForm.find('[data-required]').each(function(index, el){
		if(el.value == ''){
			isValid = false;
			setError($(el).attr('name'), getMsgs('mandatory-field'));
		}
	});
	return isValid;
}

function clearError(forName){
	aoForm.find('[data-type="errorMessage"][data-for="' + forName + '"]').html('');
	aoForm.find('[name=' + forName + ']').removeClass('error');
}

function clearErrors(){
	aoForm.find('[data-type="errorMessage"]').html('');
	aoForm.find('[name]').removeClass('error');
}

function setError(forName, error){
	aoForm.find('[data-type="errorMessage"][data-for="' + forName + '"]').html(error);
	aoForm.find('[name="' + forName + '"]').addClass('error');
}

function updatePhoneCode(){
	var countriesSelect = $('[data-type="phoneCode"]')[0];
	if(countriesSelect){
		if(countriesMap[countriesSelect.value]){
			aoForm.find('[data-type="phone-code-country"]').html(countriesMap[countriesSelect.value].a2);
			aoForm.find('[data-type="phone-code-prefix"]').html('+' + countriesMap[countriesSelect.value].phoneCode);
		}
	}
}

function retypePassword(value){
	aoForm.find('[data-type="retype-password"]').val(value);
}

function validateEmail(){
	var isValid = true;
	aoForm.find('[data-valid-email]').each(function(index, el){
		if(!aoFormSettings.regEx_email.test(el.value)){
			isValid = false;
			if(el.value != ''){
				setError($(el).attr('name'), getMsgs('invalid-email'));
			}
		}
	});
	if(!isValid){
		return;
	}
	
	var request = {
		email: getValueByName('funnel_email')
	};
	$.ajax({
		url     : aoFormSettings.validate_email_link,
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			var msg = '';
			switch (data.returnCode) {
				case 0:
					// OK
					break;
				case 1:
					msg = getMsgs('invalid-email');
					break;
				case 2:
					msg = getMsgs('invalid-email-domain');
					break;
				case 3:
					msg = getMsgs('invalid-email-inuse').replace('{0}', aoFormSettings.passwordPage);
					break;
			}
			if(msg){
				setError('funnel_email', msg);
			}
		},
		error   : function( xhr, err ) {
			var msg = getMsgs('invalid-email');
			setError('funnel_email', msg);
		}
	});
}

function insertUpdateRegisterAttempts(){
	var isEmailValid = true;
	var isPhoneValid = true;
	aoForm.find('[data-valid-email]').each(function(index, el){
		if(!aoFormSettings.regEx_email.test(el.value)){
			isEmailValid = false;
		}
	});
	aoForm.find('[data-valid-phone]').each(function(index, el){
		if(!aoFormSettings.regEx_phone.test(el.value)){
			isPhoneValid = false;
		}
	});
	if(!isEmailValid && !isPhoneValid){
		return;
	}
	
	var request = {
		quickStartEmailRegisterAttempts: getValueByName('funnel_email'),
		firstNameRegisterAttempts: getValueByName('funnel_firstName'),
		lastNameRegisterAttempts: getValueByName('funnel_lastName'),
		countryRegisterAttempts: getValueByName('funnel_phonePrefix'),
		mobilePhoneRegisterAttempts: getValueByName('funnel_mobilePhone'),
		isShortForm: true,
		marketingTracking: false
	};
	$.ajax({
		url     : aoFormSettings.insert_register_attempts_link,
		type    : "POST",
		data: request,
		success : function(data) {
			
		},
		error   : function( xhr, err ) {
			
		}
	});
}