{
	"open-account": "ABRIR CONTA",
	"first-name": "Primeiro Nome",
	"last-name": "Último nome",
	"email": "Email",
	"phone": "Telefone",
	"password": "Contrasenha",
	"retype-password": "Reescrever senha",
	"i-agree-to-terms-and-conditions": "Eu aceito os <a href='{terms-link}' target='_blank'>termos e condi\u00e7\u00f5es</a> de anyoption",
	"mandatory-field": "Campo obrigat\u00f3rio",
	"invalid-email": "Endere\u00e7o de e-mail inv\u00e1lido",
	"invalid-email-domain": "Endere\u00e7o de e-mail inv\u00e1lido",
	"invalid-email-inuse": "Este email j\u00e1 est\u00e1 em uso. Escolha por favor um email diferente",
	"invalid-phone": "O n\u00famero de telefone \u00e9 inv\u00e1lido",
	"invalid-phone-min-length": "O m\u00f3vel \u00e9 mais curto do que o m\u00ednimo permitido de 7 caracteres",
	"you-must-accept-terms": "Voc\u00ea deve aceitar nossos Termos e Condi\u00e7\u00f5es primeiro",
	"password-too-short": "A senha deve consistir pelo menos em 6 caracteres",
	"general-error": "Erro. Por favor tente novamente"
}