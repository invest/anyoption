{
	"open-account": "Open account",
	"first-name": "First name",
	"last-name": "Last name",
	"email": "Email",
	"phone": "Phone",
	"password": "Password",
	"retype-password": "Retype password",
	"i-agree-to-terms-and-conditions": "I have read anyoption's <a href='{terms-link}' target='_blank'>Terms</a>",
	"mandatory-field": "Mandatory field",
	"invalid-email": "Invalid email address",
	"invalid-email-domain": "Invalid email address",
	"invalid-email-inuse": "This email is already in use. Please choose a different email or <a href='{0}'>recover your password</a>",
	"invalid-phone": "Invalid phone",
	"invalid-phone-min-length": "Mobile is shorter than the allowed minimum of 7 characters",
	"you-must-accept-terms": "You must accept our Terms and Conditions first",
	"password-too-short": "Password must consist of at least 6 characters",
	"general-error": "Error. Please try again"
}