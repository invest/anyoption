{
	"open-account": "ÖPPNA KONTO",
	"first-name": "Förnamn",
	"last-name": "Efternamn",
	"email": "E-post",
	"phone": "Telefon",
	"password": "Lösenord",
	"retype-password": "Ange lösenord igen",
	"i-agree-to-terms-and-conditions": "Jag har läst anyoptions <a href='{terms-link}' target='_blank'>regler</a>",
	"mandatory-field": "Obligatoriskt fält",
	"invalid-email": "Ogiltig e-postadress",
	"invalid-email-domain": "Ogiltig e-postadress",
	"invalid-email-inuse": "Den här e-postadressen används redan. Välj en annan e-postadress eller <a href='{0}'>återskapa ditt lösenord</a>",
	"invalid-phone": "Telefonnumret är ogiltigt",
	"invalid-phone-min-length": "Mobil är kortare än tillåtet minimum på 7 tecken",
	"you-must-accept-terms": "Du måste först godkänna regler och villkor",
	"password-too-short": "Lösenord måste bestå av minst 6 tecken",
	"general-error": "Fel. Försök igen"
}