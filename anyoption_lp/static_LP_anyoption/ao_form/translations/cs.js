{
	"open-account": "OTEVŘÍT ÚČET",
	"first-name": "Křestní jméno",
	"last-name": "Příjmení",
	"email": "Email",
	"phone": "Telefon",
	"password": "Heslo",
	"retype-password": "Napište znovu heslo",
	"i-agree-to-terms-and-conditions": "Přijímám <a href='{terms-link}' target='_blank'>obchodní podmínky anyoption™</a>",
	"mandatory-field": "Povinné pole.",
	"invalid-email": "Neplatná emailová adresa",
	"invalid-email-domain": "Neplatná emailová adresa",
	"invalid-email-inuse": "Tento email je již používán. Prosím vyberte si jiný email.",
	"invalid-phone": "Telefonní číslo není platné",
	"invalid-phone-min-length": "Mobil je kratší, než je povolené minimum 7 znaků.",
	"you-must-accept-terms": "Nejdříve musíte přijat obchodní podmínky.",
	"password-too-short": "Heslo musí obsahovat alespoň 6 znaků.",
	"general-error": "Chyba. prosím zkuste to znovu."
}