{
	"open-account": "KONTO ERÖFFNEN",
	"first-name": "Vorname",
	"last-name": "Nachname",
	"email": "E-Mail",
	"phone": "Telefon",
	"password": "Passwort",
	"retype-password": "Passwort wiederholen",
	"i-agree-to-terms-and-conditions": "Ich habe die <a href='{terms-link}' target='_blank'>Bedingungen</a> von anyoption gelesen",
	"mandatory-field": "Pflichtfeld",
	"invalid-email": "Ungültige E-Mail Adresse",
	"invalid-email-domain": "Ungültige E-Mail Adresse",
	"invalid-email-inuse": "Diese E-Mail-Adresse wird bereits verwendet. Wählen Sie bitte eine andere E-Mail-Adresse oder <a href='{0}'>stellen Sie Ihr Passwort wieder her</a>",
	"invalid-phone": "Telefonnummer ist ungültig",
	"invalid-phone-min-length": "Mobiltelefon muss mindestens 7 Zeichen lang sein",
	"you-must-accept-terms": "Sie müssen unsere Nutzungsbedingungen akzeptieren",
	"password-too-short": "Passwort muss aus mindestens 6 zeichen bestehen",
	"general-error": "Fehler. Bitte versuchen Sie es erneut"
}