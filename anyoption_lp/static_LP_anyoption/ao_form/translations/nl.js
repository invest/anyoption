{
	"open-account": "ACCOUNT OPENEN",
	"first-name": "Voornaam",
	"last-name": "Achternaam",
	"email": "E-mail",
	"phone": "Telefoon",
	"password": "Wachtwoord",
	"retype-password": "Wachtwoord overtypen",
	"i-agree-to-terms-and-conditions": "Ik heb de <a href='{terms-link}' target='_blank'>voorwaarden</a> van anyoption gelezen",
	"mandatory-field": "Verplicht veld",
	"invalid-email": "Ongeldig e-mailadres",
	"invalid-email-domain": "Ongeldig e-mailadres",
	"invalid-email-inuse": "Dit emailadres is al in gebruik. Kies een ander adres of <a href='{0}'>herstel je wachtwoord</a>",
	"invalid-phone": "Telefoonnummer is ongeldig",
	"invalid-phone-min-length": "Mobiel is korter dan de minimaal toegestane 7 tekens",
	"you-must-accept-terms": "U moet eerst akkoord gaan met onze algemene gebruiksvoorwaarden",
	"password-too-short": "Wachtwoord moet minstens uit 6 tekens bestaan",
	"general-error": "Foutmelding. Probeer het opnieuw"
}