{
	"open-account": "APRI UN CONTO",
	"first-name": "Nome",
	"last-name": "Cognome",
	"email": "E-mail",
	"phone": "Telefono",
	"password": "Password",
	"retype-password": "RIDIGITA LA PASSWORD",
	"i-agree-to-terms-and-conditions": "Ho letto i <a href='{terms-link}' target='_blank'>termini</a> di anyoption",
	"mandatory-field": "Campo obbligatorio",
	"invalid-email": "Indirizzo e-mail non valido",
	"invalid-email-domain": "Indirizzo e-mail non valido",
	"invalid-email-inuse": "Questo indirizzo e-mail è già in uso. Si prega di scegliere un indirizzo diverso o di accedere alla <a href='{0}'>procedura di recupero della password.</a>",
	"invalid-phone": "Il numero di telefono non è valido",
	"invalid-phone-min-length": "Cellulare è più corto del valore minimo consentito di 7 caratteri",
	"you-must-accept-terms": "Prima è necessario accettare i termini e le condizioni",
	"password-too-short": "La password deve contenere almeno 6 caratteri",
	"general-error": "Errore. Provate di nuovo"
}