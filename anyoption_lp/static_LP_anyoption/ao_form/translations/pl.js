{
	"open-account": "OTWÓRZ KONTO",
	"first-name": "Imię",
	"last-name": "Nazwisko",
	"email": "Email",
	"phone": "Phone",
	"password": "Hasło",
	"retype-password": "Wprowadź hasło ponownie",
	"i-agree-to-terms-and-conditions": "Akceptuję <a href='{terms-link}' target='_blank'>warunki korzystania</a> z anyoption",
	"mandatory-field": "Pole obowiązkowe",
	"invalid-email": "Nieprawidłowy adres email",
	"invalid-email-domain": "Nieprawidłowy adres email",
	"invalid-email-inuse": "Ten adres e-mail jest już w użyciu. Wybierz inny adres e-mail.",
	"invalid-phone": "Nr telefonu jest nieprawidłowy",
	"invalid-phone-min-length": "Komórka jest krótsza od dozwolonego minimum 7 znaków",
	"you-must-accept-terms": "Musisz zaakceptować warunki korzystania",
	"password-too-short": "Hasło musi się składać przynajmniej z 6 znaków",
	"general-error": "Błąd. Spróbuj ponownie"
}