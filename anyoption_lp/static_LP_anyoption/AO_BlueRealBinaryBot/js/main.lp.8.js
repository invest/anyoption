
var openOverlay = function (text) {
	$('.overlay').show();
	$('.whiteBox .frame').hide()
	$('.whiteBox').show();
	$('.whiteBox .frame[data-id="'+text+'"]').show();
	$('.whiteBox').css({
		left:$(window).width()/2-960/2,
		top: $(window).scrollTop()+50
	});
}
$(document).click(function (e) {
	if(!$(e.target).is('.whiteBox, .whiteBox *, [data-frame]')){
		$('.overlay').hide();
		$(".whiteBox").hide();
	}
})

$('[data-frame]').click(function () {
	openOverlay($(this).attr("data-frame"));
})

var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};