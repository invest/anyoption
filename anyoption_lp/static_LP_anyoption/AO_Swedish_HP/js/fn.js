function show_tab(th,tabContent){
	var lis = th.parentNode.getElementsByTagName('li');
	for(var i=0;i<lis.length;i++){
		lis[i].className = lis[i].className.replace(/hov/gi,'');
	}
	th.className += " hov";
	th.parentNode.className = "home_tab_h home_tab_h_"+th.id
	
	var tabContent = g(tabContent);
	var lis = tabContent.getElementsByTagName('li');
	for(var i=0;i<lis.length;i++){
		lis[i].className = lis[i].className.replace(/hov/gi,'');
	}
	g('content_'+th.id).className += " hov";
}

/*accordion*/
function getTopLevelByData(el,data_t,data){
 while(el.getAttribute){
  if(el.getAttribute(data_t) == data){
   return el;
  }
  el = el.parentNode;
 }
}

var accordion_last_open = null;
var accordion_last_timer = new Array();
function accordion(th){
	if((accordion_last_open != null)&&(accordion_last_open != th)){
		accordion(accordion_last_open);
	}
	var top = getTopLevelByData(th,'data-top','t');
	var ul = top.getElementsByTagName('ul')[0];
	var title = top.getElementsByTagName('span')[0];
	if(ul.style.display == "block"){
		var dir = 'up';
		var scr_left = ul.offsetHeight;
		title.className = title.className.replace(/hov/gi,'');
		accordion_last_open = null;
	}
	else{
		ul.style.display = "block";
		var scr_left = ul.offsetHeight;
		ul.style.height = 0;
		var dir = 'down';
		title.className += " hov";
		accordion_last_open = th;
	}
	
	var inc = 0;
	var doSetTimeout = function(){
		if(scr_left > 200){
			inc = Math.round(scr_left*0.20);
		}
		else if(scr_left > 2){
			inc = Math.round(scr_left*0.30);
		}
		if(scr_left > 0){
			if(dir == "up"){
				ul.style.height = ul.offsetHeight-inc+"px";
			}
			else{
				ul.style.height = ul.offsetHeight+inc+"px";
			}
			scr_left -= inc;
			accordion_last_timer[th] = setTimeout(doSetTimeout,30);
		}
		else{
			if(dir == "up"){
				ul.style.display = "none";
				ul.style.height = "auto";
			}
		}
	}
	doSetTimeout();
}

/* footer_strip */
function g(id){return document.getElementById(id);}
function cloneId(id,where){
	var el = g(id);
	var w = g(where);
	var clone = el.cloneNode(true);
	w.appendChild(clone);
}
function footer_strip(id){
	var d = g(id);
	var img = [];
	img[0] = g(id+'_1');
	var imgs = img[0].getElementsByTagName('img');
	var sum = 0;
	for(var i=0;i<imgs.length;i++){
		sum += imgs[i].offsetWidth;
	}
	img[0].style.width = sum+"px";
	cloneId(id+'_1',id);
	d.getElementsByTagName('div')[1].id = id+'_2';
	img[1] = d.getElementsByTagName('div')[1];
	img[0].style.left = img[0].offsetWidth+"px";
	
	img[1].getElementsByTagName('img')[0].onload = function(){
		var c = 0;
		var w = img[0].offsetWidth;
		var hw = d.offsetWidth;
		var inc = 1;
		var curr = img[1];
		var curr2 = img[0];
		var st = true;
		var doSetTimeout = function(){
			if(((Math.abs(curr.offsetLeft)+hw+inc) > w)&&(st)){
				curr2.style.left = hw+"px";
				st = false;
			}
			if(!st){
				curr2.style.left = curr2.offsetLeft-inc+"px";
			}
			curr.style.left = curr.offsetLeft-inc+"px";
			if(Math.abs(curr.offsetLeft) >= w){
				var curr3 = curr;
				curr = curr2;
				curr2 = curr3;
				st = true;
			}
			setTimeout(doSetTimeout,40);
		}
		doSetTimeout();
	}
}
/* close footer_strip */

/*********** clock.js ************/
var dstOffset = 0;
function tS(){
	var x = new Date();
	
	if (null == diffTimeMs) {
		diffTimeMs = Date.now() - x.getTime();
	}
	var diffTimeZoneOffsetMs = offset + (x.getTimezoneOffset() * 60 * 1000);

	x.setTime(x.getTime() + diffTimeMs + diffTimeZoneOffsetMs + dstOffset);
	return x;
}
function lZ(x){ return (x>9)?x:'0'+x; }
function dT(){ document.getElementById('header_clock').innerHTML=eval(oT); setTimeout('dT()',1000); }
function getTimeString() {
	return lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName +'.' + year ;
}
var time = new Date();
var offset = time.getTimezoneOffset()*60*1000*(-1);
var diffTimeMs = null;
var dateFormat = new Date();
var day = dateFormat.getDate();
var month = dateFormat.getMonth();
var year = dateFormat.getFullYear();
var MONTH_NAMES=new Array('01','02','03','04','05','06','07','08','09','10','11','12');
var monthName = MONTH_NAMES[month];
var oT = "";
function startClock(){

	oT="lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName + '.' + year ";
	//if(!document.all){ window.onload=dT; }else{ dT(); }
	dT();

	var timeStr = getTimeString();
	var clockId = document.getElementById("header_clock");
	clockId.innerHTML = timeStr;
}
/*********** close clock.js ************/