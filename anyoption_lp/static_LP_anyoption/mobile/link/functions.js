function g(id){return document.getElementById(id);}
var gd_slider_v2_t2 = null;
function gd_play_slider(id){
	var img = g(id);
	var imgs = img.getElementsByTagName('img');
	var div = img.getElementsByTagName('div')[0];
	var n = g('gd_slider_nav');
	// var sum = 0;
	for(var i=0;i<imgs.length;i++){
		var s = document.createElement('span');
		if(i == 0){s.className = "hov";}
		n.appendChild(s);
		n.appendChild(document.createTextNode(''));
		// sum += imgs[i].offsetWidth;
	}
	// div.style.width = sum+"px";
	gd_slider_st2(id,419);
}
function gd_slider_st2(id,d){
	var doSetTimeout2 = function(){
		gd_slider(id,d);
	}
	gd_slider_v2_t2 = setTimeout(doSetTimeout2,5000);
}
var gd_slider_v2_t1 = null;
function gd_slider(id,d){
	var div = g(id).getElementsByTagName('div')[0];
	var c_left = div.offsetLeft;

	if((div.offsetWidth+div.offsetLeft-(d)) <= 0){
		var scr_left = div.offsetWidth-(d*2);
		var dir = "left";
	}
	else{
		var scr_left = d;
		var dir = "right";
	}
	var n = g('gd_slider_nav').getElementsByTagName('span');
	for(var i=0;i<n.length;i++){
		if(n[i].className == "hov"){
			n[i].className = "";
			if((i+1) >= n.length){n[0].className = "hov";}
			else{n[i+1].className = "hov";}
			break;
		}
	}

	var inc = 0;
	var doSetTimeout = function(){
		inc = Math.round(scr_left*0.2);
		if(inc < 1){inc = 1};
		if(dir == "right"){
			div.style.left = div.offsetLeft-inc+"px";
		}
		else{
			div.style.left = div.offsetLeft+inc+"px";
		}
		scr_left -= inc;

		if(scr_left > 0){
			gd_slider_v2_t1 = setTimeout(doSetTimeout,30);
		}
		else{
			gd_slider_st2(id,d);
		}
	}
	doSetTimeout();
}
var gd_c_tab = 1;
function gd_ch_tab(th,tab){
	clearTimeout(gd_slider_v2_t2);
	clearTimeout(gd_slider_v2_t1);
	var div = g('gd_slider').getElementsByTagName('div')[0];
	div.style.left = 0;
	var n = g('gd_slider_nav').getElementsByTagName('span');
	for(var i=0;i<n.length;i++){
		if(i==0){
			n[i].className = "hov";
		}else{
			n[i].className = "";
		}
	}
	
	if(tab == 1){
		gd_slider_st2('gd_slider',419);
	}
	
	var n = g('gd_tabs1_h').getElementsByTagName('span');
	for(var i=0;i<n.length;i++){
		n[i].className = "";
	}
	th.className = "hov";
	g('id'+gd_c_tab).style.display = "none";
	gd_c_tab = tab;
	g('id'+tab).style.display = "block";
}
var gd_ctr_tab = "";
function gd_op_cl_trading(th,tab){
	var t = g('trade_'+tab);
	if(t.style.display == "block"){
		g('trade_'+tab).style.display = "none";
		th.getElementsByTagName('span')[1].className = th.getElementsByTagName('span')[1].className.replace('hov','_noHov_');
	}
	else{
		g('trade_'+tab).style.display = "block";
		th.getElementsByTagName('span')[1].className = th.getElementsByTagName('span')[1].className.replace('_noHov_','hov');
	}
	if((gd_ctr_tab != "")&&(gd_ctr_tab != tab)){
		g('trade_'+gd_ctr_tab).style.display = "none";
		g('trade_m'+gd_ctr_tab).getElementsByTagName('span')[1].className = g('trade_m'+gd_ctr_tab).getElementsByTagName('span')[1].className.replace('hov','_noHov_');
	}
	gd_ctr_tab = tab;
}