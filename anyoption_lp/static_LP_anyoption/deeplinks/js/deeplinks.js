//parseUri 1.2.2
//(c) Steven Levithan <stevenlevithan.com>
//MIT License
function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};


parseUri.options.strictMode = true;

var fullURL;
try {
	fullURL = parent.document.URL;
} catch(e) {
	fullURL = document.URL;
} 
if (fullURL.indexOf("@") != -1) {
	fullURL=fullURL.replace(/@/g,"%40");
}
var uri = parseUri(fullURL);

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);

var pageR = uri.queryKey['pageR'];
var skinId = uri.queryKey['s'];
if (skinId == 'undefined' || skinId == null) {
	skinId = 2;
}

//Separate AO from CO
var platform = uri.queryKey['p'];
if (platform == 'undefined' || platform == null) {
	platform = 'ao';
}
if(platform != 'co' && platform != 'ao'){//Prevent invalid input
	platform = 'ao';
}

var host_url = "https://www.anyoption.com/";

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;

function getLangCodeBySkinId(skinId) {
	//set default
	langCode = "en";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			langCode = "en";
			break;
		case SKIN_ID_TR: 
			langCode = "tr";
			break;
		case SKIN_ID_AR:
			langCode = "ar";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			langCode = "it";
			break;
		case SKIN_ID_RU:
			langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			langCode = "fr";
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			langCode = "zh";
			break;
		case SKIN_ID_KR: 
			langCode = "kr";
			break;
		case SKIN_ID_NL: 
			langCode = "nl";
			break;
		case SKIN_ID_SV: 
			langCode = "sv";
			break;
	}
	return langCode;
}


function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	if (document.URL.search('tradingbinaryoption.com') > -1) {
		var oldPage = getUrlValue('pageR');
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + pageR);
	} else {
		// window.location = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
		obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
	}	
}

function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}

