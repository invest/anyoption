<!DOCTYPE html>
<html>
<head>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="robots" content="noodp,noydir" />
	<script>
		<?php 
		if ( preg_match("/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/", $_GET['dom'])) { 
		?>
			document.domain = '<?php echo $_GET['dom']?>';
		<?php
		}
		?>			
	</script>
</head>
<body>
<?php
include('geoip.inc');

if (isset($_GET['domain_ip'])){
	$domain_ip=$_GET['domain_ip'];
}
else if($_SERVER['HTTP_TRUE_CLIENT_IP'] != ""){
	$domain_ip=$_SERVER['HTTP_TRUE_CLIENT_IP'];
}
else if($_SERVER['HTTP_X_REAL_IP'] != ""){
	$domain_ip=$_SERVER['HTTP_X_REAL_IP'];
}
else if($_SERVER['HTTP_X_FORWARDED_FOR'] != ""){
	$entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
	$domain_ip=current($entries);
	$domain_ip=trim($domain_ip);
} else {
	$domain_ip=$_SERVER['REMOTE_ADDR'];
}	

if ( preg_match("/^([0-9]+.[0-9]+.[0-9]+.[0-9]+)/", $domain_ip) != 1 ) {
	$domain_ip=$_SERVER['REMOTE_ADDR'];
}

//$domain_ip = '92.247.82.234';
//echo $domain_ip."<br />";
//echo $_SERVER['REMOTE_ADDR']."<br />";

geoip_load_shared_mem("/usr/share/GeoipCountry/GeoIP.dat");
$gi = geoip_open("/usr/share/GeoipCountry/GeoIP.dat",GEOIP_SHARED_MEMORY);

$countryId = $gi->GEOIP_COUNTRY_CODE_TO_DB[$gi->GEOIP_COUNTRY_CODES[geoip_country_id_by_addr($gi,$domain_ip)]];
if ($countryId == '') {
	$countryId = 2;
};
$allowIp = array('192.115.200.249', '82.81.193.152', '82.81.193.153', '82.81.193.154', '82.81.193.155', '82.81.193.156', '82.81.193.157', '82.81.193.158', '82.81.193.159','31.168.11.137', '212.179.246.19', '212.150.171.253', '212.150.171.222', '172.16.100.0', '82.166.29.184', '82.166.29.181', '74.125.67.100', '92.247.82.234', '82.80.157.18', '62.94.172.18');
if(in_array($domain_ip,$allowIp)){
	$allowIp_status = 'true';
}
else{
	$allowIp_status = 'false';
}
geoip_close($gi);
?>
<script>
parent.register_country_select(<?php echo $countryId?>,true);
try{
	parent.allowIp(<?php echo $allowIp_status?>);		
}
catch(e){;}
</script>
</body>
</html>