<?php

include('./includes/init.php');

if(isset($_POST['s'])){
	$fullName = getNameValue($_POST['fullName']);
	$countryCode = getCountryCodeValue($_POST['countryCode']);
	$phone = getPhoneValue($_POST['phoneNumber']);
	sendMail($fullName, $countryCode, $phone);
}
$showThanks = false;
if(isset($_GET['s']) && ($_GET['s'] == 1)){
	$showThanks = true;
}

?>
<!DOCTYPE html >
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700">
	<link rel="stylesheet" type="text/css" href="css/downtime_co.css">
	<script type="text/javascript" src="js/downtime.js"></script>
	<script>
		function contactSubmit(){
			document.getElementById('errorMessage').style.display = 'none';
			document.getElementById('fullName').className = document.getElementById('fullName').className.replace(/ error/gi, "");
			document.getElementById('countryCode').className = document.getElementById('countryCode').className.replace(/ error/gi, "");
			document.getElementById('phoneNumber').className = document.getElementById('phoneNumber').className.replace(/ error/gi, "");
			
			var isValidCountryCode = false;
			if(document.getElementById('countryCode').value != ''){
				for(phone in countriesPhone){
					if(countriesPhone.hasOwnProperty(phone)){
						if("" + countriesPhone[phone] == "" + document.getElementById('countryCode').value){
							isValidCountryCode = true;
						}
					}
				}
			}
			
			var isValid = true;
			if(document.getElementById('fullName').value == ''){
				isValid = false;
				document.getElementById('fullName').className += ' error';
			}
			if(document.getElementById('countryCode').value == '' || !isValidCountryCode){
				isValid = false;
				document.getElementById('countryCode').className += ' error';
			}
			if(document.getElementById('phoneNumber').value == ''){
				isValid = false;
				document.getElementById('phoneNumber').className += ' error';
			}
			/*if((document.getElementById('fullName').value == '') && (document.getElementById('countryCode').value == '') && (document.getElementById('phoneNumber').value == '')){
				isValid = false;
			}*/
			if(isValid){
				document.getElementById('contactForm').submit();
			}else{
				document.getElementById('errorMessage').style.display = 'block';
			}
			return isValid;
		}
	</script>
</head>
<body>
	<div class="wrapper">
		<div class="header"></div>
		<div class="content">
			<div class="info">
				<?php if(!$showThanks){ ?>
				<div class="info-text">
					<h2 class="content-header"><span><?php echo $TEXT['downtime.content.header']; ?></span></h2>
					<div class="content-text">
						<?php echo $TEXT['downtime.content.text1']; ?>
					</div>
				</div>
				<?php }else{ ?>
				<div class="info-text thanks">
					<h2 class="content-header"><span><?php echo $TEXT['downtime.content.header']; ?></span></h2>
					<div class="content-text">
						<div>
							<?php echo $TEXT['downtime.content.header.thanks']; ?>
							<br>
							<?php echo $TEXT['downtime.content.text1.thanks']; ?>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php if(!$showThanks){ ?>
			<form id="contactForm" method="POST" action="" class="form">
				<h3><span><?php echo $TEXT['downtime.form.header']; ?></span></h3>
				<div>
					<label class="input-holder full-name">
						<input id="fullName" name="fullName" placeholder="<?php echo $TEXT['downtime.form.full_name']; ?>">
					</label>
					<label class="input-holder country-code">
						<input id="countryCode" name="countryCode" placeholder="<?php echo $TEXT['downtime.form.country_code']; ?>">
					</label>
					<label class="input-holder phone-number">
						<input id="phoneNumber" name="phoneNumber" placeholder="<?php echo $TEXT['downtime.form.phone_number']; ?>">
					</label>
				</div>
				<a href="#" onclick="contactSubmit();return false;" class="button">
					<?php echo $TEXT['downtime.form.button']; ?>
				</a>
				<div id="errorMessage" class="error-message" style="display:none;"><?php echo $TEXT['downtime.please_fill_all']; ?></div>
				<input type="hidden" name="s" value="1">
			</form>
			<?php } ?>
		</div>
	</div>
</body>
</html>