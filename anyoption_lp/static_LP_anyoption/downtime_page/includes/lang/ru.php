<?php

$TEXT['downtime.content.header'] = 'Мы скоро вернемся';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Мы приносим извинения, но наш веб-сайт в настоящее время проходит плановое техническое обслуживание.<br>Мы работаем над улучшением скорости и эффективности работы нашего сайта.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Если у вас есть какие-либо вопросы, пожалуйста, оставьте свои данные, и мы свяжемся с вами как можно скорее.';
$TEXT['downtime.form.full_name'] = 'Полное имя';
$TEXT['downtime.form.country_code'] = 'Код страны';
$TEXT['downtime.form.phone_number'] = 'Номер телефона';
$TEXT['downtime.form.button'] = 'Связаться со мной';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
