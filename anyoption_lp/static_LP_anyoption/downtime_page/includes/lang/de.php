<?php

$TEXT['downtime.content.header'] = 'Wir sind in Kürze zurück';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Es tut uns leid, unsere Webseite wird momentan planmäßig gewartet.<br>Wir arbeiten intensiv an der Geschwindigkeit und Gesamtleistung unserer Seite.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Falls Sie ein Anliegen haben, hinterlassen Sie bitte Ihre Kontaktdaten und wir werden uns schnellstmöglich bei Ihnen melden.';
$TEXT['downtime.form.full_name'] = 'Vollständiger Name';
$TEXT['downtime.form.country_code'] = 'Landesvorwahl';
$TEXT['downtime.form.phone_number'] = 'Telefonnummer';
$TEXT['downtime.form.button'] = 'Telefonnummer';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
