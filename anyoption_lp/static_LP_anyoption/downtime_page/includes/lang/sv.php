<?php

$TEXT['downtime.content.header'] = 'Vi är strax tillbaka';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Vår webbplats genomgår för närvarande ett planerat underhåll.<br>Vi håller på att förbättra webbplatsens hastighet och den allmänna prestandan.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'För alla ärenden är du välkommen att lämna dina uppgifter så återkommer vi till dig så snart som möjligt.';
$TEXT['downtime.form.full_name'] = 'Fullständigt namn';
$TEXT['downtime.form.country_code'] = 'Landskod';
$TEXT['downtime.form.phone_number'] = 'Telefonnummer';
$TEXT['downtime.form.button'] = 'Kontakta mig';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
