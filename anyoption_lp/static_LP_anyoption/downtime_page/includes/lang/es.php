<?php

$TEXT['downtime.content.header'] = 'Volveremos en breve';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Lo sentimos, nuestro sitio web está actualmente bajo un mantenimiento programado.<br>Estamos ocupados mejorando la velocidad y el rendimiento general de nuestro sitio.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Para cualquier asunto, por favor, deje sus datos y nos pondremos en contacto con usted lo antes posible.';
$TEXT['downtime.form.full_name'] = 'Nombre completo';
$TEXT['downtime.form.country_code'] = 'Código de país';
$TEXT['downtime.form.phone_number'] = 'Número de teléfono';
$TEXT['downtime.form.button'] = 'Contactar conmigo';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
