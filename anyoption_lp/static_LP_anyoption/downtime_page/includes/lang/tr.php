<?php

$TEXT['downtime.content.header'] = 'Kısa bir süre sonra burada olacağız';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Üzgünüz, web sitemiz şu anda programlı bakım altında.<br>Sitemizin hızını ve genel performansını geliştirmekle meşgulüz.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Tüm sorularınız için bilgilerinizi bırakın; size en kısa zamanda geri dönüş yapacağız.';
$TEXT['downtime.form.full_name'] = 'Adı Soyadı';
$TEXT['downtime.form.country_code'] = 'Ülke kodu';
$TEXT['downtime.form.phone_number'] = 'Telefon numarası';
$TEXT['downtime.form.button'] = 'Bana Ulaşın';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
