<?php

$TEXT['downtime.content.header'] = 'Torneremo presto';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Al momento e in corso intervento di manutenzione programmata sul nostro sito.<br>Siamo impegnati al fine di migliorare la velocita e le prestazioni generali del sito.';
$TEXT['downtime.content.text1.thanks'] = 'We�ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Per qualsiasi problema, comunicaci i tuoi dati di contatto e ti rispondere il prima possibile.';
$TEXT['downtime.form.full_name'] = 'Nome e cognome';
$TEXT['downtime.form.country_code'] = 'Prefisso internazionale';
$TEXT['downtime.form.phone_number'] = 'Numero di telefono';
$TEXT['downtime.form.button'] = 'Contattami';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
