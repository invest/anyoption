<?php

$TEXT['downtime.content.header'] = 'Nous serons bientôt de retour';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Nous sommes désolés mais notre site est momentanément en cours de maintenance.<br>Nous travaillons pour améliorer la vitesse et la performance globale de notre site.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Pour toute question, nous vous invitons à nous laisser vos coordonnées et nous vous recontacterons dans les plus brefs délais.';
$TEXT['downtime.form.full_name'] = 'Nom complet ';
$TEXT['downtime.form.country_code'] = 'Indicatif';
$TEXT['downtime.form.phone_number'] = 'Numéro de téléphone';
$TEXT['downtime.form.button'] = 'Numéro de téléphone';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
