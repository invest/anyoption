<?php

$TEXT['downtime.content.header'] = 'We’ll be back shortly';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Sorry, our website is currently under scheduled maintenance.<br>We’re busy working on improving the speed and overall performance of our site.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'For any matters, please leave your details and we will get back to you as soon as possible.';
$TEXT['downtime.form.full_name'] = 'Full Name';
$TEXT['downtime.form.country_code'] = 'Country Code';
$TEXT['downtime.form.phone_number'] = 'Phone number';
$TEXT['downtime.form.button'] = 'Contact Me';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
