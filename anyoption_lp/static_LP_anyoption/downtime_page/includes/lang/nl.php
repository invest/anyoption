<?php

$TEXT['downtime.content.header'] = 'Wij komen heel snel weer terug';
$TEXT['downtime.content.header.thanks'] = 'Thank you.';
$TEXT['downtime.content.text1'] = 'Het spijt ons maar onze website ondergaat momenteel gepland onderhoud.<br>Wij zijn druk bezig met de verbetering van de snelheid en de algemene prestaties van onze site.';
$TEXT['downtime.content.text1.thanks'] = 'We’ll get back to you  as soon as possible';
$TEXT['downtime.form.header'] = 'Als u een vraag hebt, laat dan uw gegevens achter; wij zullen zo snel mogelijk contact met u opnemen.';
$TEXT['downtime.form.full_name'] = 'Volledige naam';
$TEXT['downtime.form.country_code'] = 'Landcode';
$TEXT['downtime.form.phone_number'] = 'Telefoonnummer';
$TEXT['downtime.form.button'] = 'Contact met mij opnemen';
$TEXT['downtime.please_fill_all'] = 'Field required'; 
