<?php

error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);


define('SKIN_ID_EN', 2);
define('SKIN_ID_TR', 3);
define('SKIN_ID_AR', 4);
define('SKIN_ID_ES', 5);
define('SKIN_ID_DE', 8);
define('SKIN_ID_IT', 9);
define('SKIN_ID_RU', 10);
define('SKIN_ID_FR', 12);
define('SKIN_ID_EN_US', 13);
define('SKIN_ID_ES_US', 14);
define('SKIN_ID_ZH', 15);
define('SKIN_ID_EN_REG', 16);
define('SKIN_ID_KR', 17);
define('SKIN_ID_ES_REG', 18);
define('SKIN_ID_DE_REG', 19);
define('SKIN_ID_IT_REG', 20);
define('SKIN_ID_FR_REG', 21);
define('SKIN_ID_168QIQUAN', 22);
define('SKIN_ID_NL', 23);
define('SKIN_ID_SV', 24);

$ALLOWED_LANGUAGES = array('de', 'en', 'es', 'fr', 'it', 'nl', 'ru', 'sv', 'tr');

$DEFAULT_SKIN = 16;
$DEFAULT_LANG = "en";


include('functions.php');


$lang = $DEFAULT_LANG;
$langInDomain = false;
$domainParts = explode('.', $_SERVER['HTTP_HOST']);
if(count($domainParts) > 2){
	for($i = 0; $i < (count($domainParts) - 2); $i++){
		if(in_array(strtolower($domainParts[$i]), $ALLOWED_LANGUAGES)){
			$lang = strtolower($domainParts[$i]);
			$langInDomain = true;
			break;
		}
	}
}
if(!$langInDomain){
	$skinId = $DEFAULT_SKIN;
	if(isset($_COOKIE['s'])){
		$skinId = intval($_COOKIE['s']);
	}
	$lang = strtolower(getLangCodeBySkinId($skinId, $DEFAULT_LANG));
}

$TEXT = array();
include('lang/'.$lang.'.php');




