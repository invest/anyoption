<?php

function getLangCodeBySkinId($skinId, $defaultLang) {
	//set default
	$langCode = $defaultLang;
	switch (intval($skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			$langCode = "en";
			break;
		case SKIN_ID_TR:
			$langCode = "tr";
			break;
		case SKIN_ID_AR:
			$langCode = "en";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			$langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			$langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			$langCode = "it";
			break;
		case SKIN_ID_RU:
			$langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			$langCode = "fr";
			break;
		case SKIN_ID_KR:
			break;
		case SKIN_ID_NL:
			$langCode = "nl";
			break;
		case SKIN_ID_SV:
			$langCode = "sv";
			break;
		default:
			$langCode = $defaultLang;
	}
	return $langCode;
}

function getNameValue($str){
	return preg_replace("/[^\p{L} \. ']/u", "", $str);
}

function getCountryCodeValue($str){
	return preg_replace("/[^0-9 \+]/u", "", $str);
}

function getPhoneValue($str){
	return preg_replace("/[^0-9 ]/u", "", $str);
}

function sendMail($fullName, $countryCode, $phone){
	$error = '';
	
	$mailFrom = 'support@anyoption.com';
	$mailTo = 'support@anyoption.com';
	$mailSubject = "Call me request on downtime: name: ".$fullName." - Phone #: ".$countryCode." ".$phone."";

	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer;
	//$mail->SMTPDebug = 3;									// Enable verbose debug output
	$mail->isSMTP();										// Set mailer to use SMTP
	$mail->Host = gethostbyname('smtp.anyoption.com');		// Specify main and backup SMTP servers
	$mail->SMTPAuth = false;								// Enable SMTP authentication
	$mail->Port = 25;										// TCP port to connect to

	$mail->setFrom($mailFrom);
	$mail->addAddress($mailTo);								// Add a recipient

	$mail->isHTML(true);									// Set email format to HTML

	$mail->Subject = $mailSubject;
	$mail->Body = "Name: ".$fullName."<br>Country code: ".$countryCode."<br>Phone: ".$phone."";

	if(!$mail->send()) {
		$error = 'Message could not be sent.';
	} else {
		header("Location: ?s=1");
	}
	
	if($error){
		return $error;
	}
}
