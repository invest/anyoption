var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;
var SKIN_ID_EN_US_AOPS	= 25;
var SKIN_ID_ES_AOPS 	= 26;
var SKIN_ID_CZ 			= 27;
var SKIN_ID_PL 			= 28;
var SKIN_ID_PT 			= 29;

function getLangCodeBySkinId(skinId) {
	//set default
	var langCode = "en";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
		case SKIN_ID_EN_US_AOPS:
			langCode = "en";
			break;
		case SKIN_ID_TR: 
			langCode = "tr";
			break;
		case SKIN_ID_AR:
			langCode = "ar";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
		case SKIN_ID_ES_AOPS:
			langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			langCode = "it";
			break;
		case SKIN_ID_RU:
			langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			langCode = "fr";
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			langCode = "zh";
			break;
		case SKIN_ID_KR: 
			langCode = "kr";
			break;
		case SKIN_ID_NL: 
			langCode = "nl";
			break;
		case SKIN_ID_SV: 
			langCode = "sv";
			break;
		case SKIN_ID_CZ: 
			langCode = "cs";
			break;
		case SKIN_ID_PL: 
			langCode = "pl";
			break;
		case SKIN_ID_PT: 
			langCode = "pt";
			break;
	}
	return langCode;
}

var msgs = {};
var coForm = null;

var coFormSettings = {
	get_countries_link: context_path_ajax + '/jsonService/AnyoptionService/getSortedCountries',
	insert_register_attempts_link: context_path_ajax + '/jsonService/AnyoptionService/insertRegisterAttempt',
	/*co_register_link: "https://www.copyop.com/jsonService/AnyoptionService/insertCopyopProfile",*/
	co_register_link: "https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/insertCopyopProfile",
	/*co_avatar_link: "https://www.copyop.com/jsonService/avatar/m9.png",*/
	co_avatar_link: "https://www.bgtestenv.anyoption.com/jsonService/avatar/m9.png",
	/*co_after_register_redirect: 'https://www.copyop.com/',*/
	co_after_register_redirect: 'https://www.bgtestenv.anyoption.com/jsonService',
	termsLink: 'http://www.copyop.com/' + getLangCodeBySkinId(skinId) + '/general-terms',
	regEx_lettersAndNumbers: /^[0-9a-zA-Z]+$/,
	regEx_nickname: /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/,
	regEx_nickname_reverse: /[^a-zA-Z0-9!@#$%\^:;"']/g,
	regEx_phone : /^\d{7,20}$/,
	regEx_digits: /^\d+$/,
	regEx_digits_reverse : /\D/g,
	regEx_email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	regEx_englishLettersOnly: /^['a-zA-Z ]+$/,
	regEx_lettersOnly: "^['\\p{L} ]+$"
};


function initCoForm(template){
	if(!template){
		template = 'template_default';
	}
	$.ajax({
		url     : "../co_form/templates/" + template + ".shtml",
		type    : "GET",
		data    : "",
		success : function(data) {
			document.getElementById('coFormHolder').innerHTML = data;
			coForm = $('[data-type="coForm"]');
			getTranslations(getLangCodeBySkinId(skinId));
			getCountries();
		},
		error   : function( xhr, err ) {
			
		}
	});
}


var getTranslationsIterations = 0;
function getTranslations(langCode){
	getTranslationsIterations++;
	$.ajax({
		url     : "../co_form/translations/" + langCode + ".js",
		type    : "GET",
		dataType: "json",
		data    : "",
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			msgs = data;
			if(msgs){
				initCoFormData();
			}else if(getTranslationsIterations <= 2){
				getTranslations("en");
			}
		},
		error   : function( xhr, err ) {
			if(getTranslationsIterations <= 2){
				getTranslations("en");
			}
		}
	});
}

var countriesMap = {};
function getCountries(){
	var request = {skinId: skinId};
	$.ajax({
		url     : coFormSettings.get_countries_link,
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			countriesMap = data.countriesMap;
			var defaultCountryId = '';
			if (isUndefined(data.countriesMap[data.ipDetectedCountryId])) {
				defaultCountryId = data.skinDefautlCointryId;
			} else {
				defaultCountryId = (data.ipDetectedCountryId != 0) ? data.ipDetectedCountryId : data.skinDefautlCointryId; 
			}
			//Fill countries list
			var countriesSelect = $('[data-type="phoneCode"]');
			fillCountriesList(countriesSelect, defaultCountryId);
		},
		error   : function( xhr, err ) {
			
		}
	});
	/*$.ajax({
		url     : "../co_form/countries.js",
		type    : "GET",
		dataType: "json",
		data    : "",
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			countriesMap = data;
			//Fill countries list
			var countriesSelect = $('[data-type="phoneCode"]');
			fillCountriesList(countriesSelect);
		},
		error   : function( xhr, err ) {
			
		}
	});*/
}


function parseParams(params){
	return params;
}
function getMsgs(key, params){
	if(!params){
		return msgs[key];
	}else{
		params = eval(params);
		for(paramKey in params){
			if(params.hasOwnProperty(paramKey)){
				return msgs[key].replace('{' + paramKey + '}', params[paramKey]);
			}
		}
	}
}

function initCoFormData(){
	if(coForm.length > 0){
		//Translate texts
		coForm.find('[data-text]').each(function(index, el){
			el.innerHTML = getMsgs($(el).data('text'), $(el).data('text-params'));
		});
		
		//Translate placeholders
		coForm.find('[data-placeholder]').each(function(index, el){
			$(el).attr('placeholder', getMsgs($(el).data('placeholder'), $(el).data('placeholder-params')));
		});
		
		coForm.find('[name]').each(function(index, el){
			$(el).on('change keydown', function(event){
				clearError($(el).attr('name'));
			})
		});
	}
}

function fillCountriesList(countriesSelect, selectedItemKey){
	if(countriesSelect.length > 0){
		var countries = [];
		for(key in countriesMap){
			if(countriesMap.hasOwnProperty(key)){
				countries.push({key: key, phoneCode: countriesMap[key].phoneCode, text: countriesMap[key].displayName});
			}
		}
		countries.sort(function sortCountries(a, b){
			var nameA = a.text.toUpperCase();
			var nameB = b.text.toUpperCase();
			if(nameA < nameB){
				return -1;
			}
			if(nameA > nameB){
				return 1;
			}
			return 0;
		});
		for(var i = 0; i < countries.length; i++){
			var option = document.createElement('option');
			option.value = countries[i].key;
			option.innerHTML = countries[i].text;
			if(countries[i].key == selectedItemKey){
				option.selected = "selected";
			}
			countriesSelect.append(option);
		}
	}
	updatePhoneCode();
}

function getValueByName(name){
	if(!coForm[0] || !coForm[0][name]){
		return null;
	}
	if(coForm[0][name].type == 'checkbox'){
		return coForm[0][name].checked;
	}
	return coForm[0][name].value;
}

function submitCoForm(){
	clearErrors();
	
	if(!validateCoErrors()){
		return false;
	}
	
	var request = {
		nickname: getValueByName('nickname'),
		acceptedTermsAndConditions: getValueByName('terms'),
		avatar: coFormSettings.co_avatar_link,
		register: {
			skinId: skinId,
			writerId: 10000,
			userName: getValueByName('email'),
			password: getValueByName('password'),
			password2: getValueByName('password'),
			email: getValueByName('email'),
			firstName: getValueByName('firstName'),
			lastName: getValueByName('lastName'),
			countryId: getValueByName('phoneCode'),
			mobilePhone: getValueByName('mobilePhone'),
			skinId: skinId,
			platformId: 0,
			terms: getValueByName('terms')
		},
		skinId: skinId
	};
	request.register.combinationId = getUrlValue("combid");
	if(request.register.combinationId == ""){
		request.register.combinationId = 0;
	}
	request.register.dynamicParam = getUrlValue("dp");
	request.register.aff_sub1 = getUrlValue("aff_sub1");
	request.register.aff_sub2 = getUrlValue("aff_sub2");
	if(getUrlValue("gclid") != ""){
		request.register.aff_sub3 = getUrlValue("gclid");
	}else{
		request.register.aff_sub3 = getUrlValue("aff_sub3");
	}
	$.ajax({
		url     : coFormSettings.co_register_link,
		xhrFields: {
			withCredentials: true
		},
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			if(data.errorCode == 0){
				window.location = coFormSettings.co_after_register_redirect;
			}else{
				var errorMessage = '';
				if(data.userMessages && data.userMessages[0]){
					for(var i = 0; i < data.userMessages.length; i++){
						if(errorMessage != ''){
							errorMessage += '<br/>';
						}
						if(data.userMessages[i].field == ''){
							errorMessage += data.userMessages[i].message;
						}else{
							setError(data.userMessages[i].field, data.userMessages[i].message);
						}
					}
				}
				if(errorMessage != ''){
					setError('generic', errorMessage);
				}
			}
		},
		error   : function( xhr, err ) {
			setError('generic', getMsgs('general-error'));
		}
	});
}

function validateCoErrors(){
	var isValid = true;
	coForm.find('[data-valid-email]').each(function(index, el){
		if(!coFormSettings.regEx_email.test(el.value)){
			isValid = false;
			setError($(el).attr('name'), getMsgs('invalid-email'));
		}
	});
	coForm.find('[data-valid-phone]').each(function(index, el){
		if(!coFormSettings.regEx_phone.test(el.value)){
			isValid = false;
			setError($(el).attr('name'), getMsgs('invalid-phone'));
		}
	});
	coForm.find('[data-accept-terms]').each(function(index, el){
		if(!el.checked){
			isValid = false;
			setError($(el).attr('name'), getMsgs('you-must-accept-terms'));
		}
	});
	coForm.find('[data-required]').each(function(index, el){
		if(el.value == ''){
			isValid = false;
			setError($(el).attr('name'), getMsgs('mandatory-field'));
		}
	});
	return isValid;
}

function clearError(forName){
	coForm.find('[data-type="errorMessage"][data-for="' + forName + '"]').html('');
	coForm.find('[name=' + forName + ']').removeClass('error');
}

function clearErrors(){
	coForm.find('[data-type="errorMessage"]').html('');
	coForm.find('[name]').removeClass('error');
}

function setError(forName, error){
	coForm.find('[data-type="errorMessage"][data-for="' + forName + '"]').html(error);
	coForm.find('[name="' + forName + '"]').addClass('error');
}

function updatePhoneCode(){
	var countriesSelect = $('[data-type="phoneCode"]')[0];
	if(countriesSelect){
		if(countriesMap[countriesSelect.value]){
			coForm.find('[data-type="phone-code-country"]').html(countriesMap[countriesSelect.value].a2);
			coForm.find('[data-type="phone-code-prefix"]').html('+' + countriesMap[countriesSelect.value].phoneCode);
		}
	}
}

function insertUpdateRegisterAttempts(){
	var isEmailValid = true;
	var isPhoneValid = true;
	coForm.find('[data-valid-email]').each(function(index, el){
		if(!coFormSettings.regEx_email.test(el.value)){
			isEmailValid = false;
		}
	});
	coForm.find('[data-valid-phone]').each(function(index, el){
		if(!coFormSettings.regEx_phone.test(el.value)){
			isPhoneValid = false;
		}
	});
	if(!isEmailValid && !isPhoneValid){
		return;
	}
	
	var httpReferer = getCookie('hr');
	if (httpReferer == null) {
		httpReferer = document.referrer;
	}
	var mId = getCookie('smt')
	mId = (mId == null) ? '' : mId;
	
	var request = {
		email: getValueByName('email'),
		firstName: getValueByName('firstName'),
		lastName: getValueByName('lastName'),
		mobilePhone: getValueByName('mobilePhone'),
		countryId: getValueByName('phoneCode'),
		marketingStaticPart: '',
		etsMId: '',
		mId: mId,
		httpReferer: httpReferer,
		skinId: skinId,
		locale: '',
		writerId: 10000
	};
	request.combinationId = getUrlValue("combid");
	if(request.combinationId == ""){
		request.combinationId = 0;
	}
	request.dynamicParameter = getUrlValue("dp");
	
	$.ajax({
		url     : coFormSettings.insert_register_attempts_link,
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			
		},
		error   : function( xhr, err ) {
			
		}
	});
}