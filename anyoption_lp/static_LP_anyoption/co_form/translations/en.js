{
	"im-done": "I'm done",
	"first-name": "First name",
	"last-name": "Last name",
	"nickname": "Nickname",
	"email": "Email",
	"phone": "Phone",
	"password": "Password",
	"i-agree-to-terms-and-conditions": "I have read copyop's <a href='{terms-link}' target='_blank'>terms</a>",
	"mandatory-field": "Mandatory field",
	"invalid-email": "Invalid email address",
	"invalid-phone": "Invalid phone",
	"you-must-accept-terms": "You must accept our Terms and Conditions first",
	"general-error": "Error. Please try again"
}