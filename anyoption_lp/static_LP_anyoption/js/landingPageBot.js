var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId 	= uri.queryKey['combid'];
var dp		= uri.queryKey['dp'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}

if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
if (null == dp ||  'undefined' == dp) {
	dp = '';
}
function buildURLWithParameters(url) {
	url += '&combid=' + combId + '&dp=' + dp + '&aff_sub1=' + aff_sub1 + '&aff_sub2=' + aff_sub2 + '&aff_sub3=' + aff_sub3;
	return url;
}