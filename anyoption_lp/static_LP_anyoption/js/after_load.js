
/* Get URL params and fill them inside the form  */
var urlParams;
(window.onpopstate = function () {			
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	urlParams = {};
	while (match = search.exec(query))
	   urlParams[decode(match[1])] = decode(match[2]);
})();

var first_name_url_value 	= urlParams["p_first_name"];
var last_name_url_value 	= urlParams["p_last_name"];
var email_url_value 		= urlParams["p_email"];
var mobile_url_value 		= urlParams["p_mobile"];
		
if (first_name_url_value != null && first_name_url_value != 'undefined') {
	var first_name_field = document.getElementById("first_name");
	if (first_name_field != null && first_name_field != 'undefined') {
		first_name_field.value = first_name_url_value;
	}			
}
if (last_name_url_value != null && last_name_url_value != 'undefined') {
	var last_name_field = document.getElementById("last_name");
	if (last_name_field != null && last_name_field != 'undefined') {
		last_name_field.value = last_name_url_value;
	}			
}
if (email_url_value != null && email_url_value != 'undefined') {
	var email_name_field = document.getElementById("email");
	if (email_name_field != null && email_name_field != 'undefined') {
		email_name_field.value = email_url_value;
	}			
}
if (mobile_url_value != null && mobile_url_value != 'undefined') {
	var mobile_field = document.getElementById("mobile_m");
	if (mobile_field != null && mobile_field != 'undefined') {
		mobile_field.value = mobile_url_value;
	}			
}