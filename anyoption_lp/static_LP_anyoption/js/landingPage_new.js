/*********** landingPage.js ************/

//constants
var FORM_TYPE_REGULAR	= 1;
var FORM_TYPE_LIGHTBOX	= 2;

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;

var COMB_ID_EN_DEFUALT	= 22;

//error messages
var first_name_mandatory;
var first_name_letters;
var last_name_mandatory;
var last_name_letters;
var email_mandatory;
var email_invalid;
var mobile_mandatory;
var mobile_invalid_number;
var mobile_min_length;
var letters_regexp;
var form_type = FORM_TYPE_REGULAR;

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = SKIN_ID_EN;
}
if (null == combId ||  'undefined' == combId) {
	combId = COMB_ID_EN_DEFUALT;
}
if (fullURL.indexOf("168qiquan.com") != -1) {
	host_url = host_url_168qiquan;
}
if (fullURL.indexOf("anyoption.it") != -1) {
	host_url = host_url_anyoption_it;
}
if (document.URL.search('tradingbinaryoption.com') > -1 ||
		document.URL.search('maxtradingtools.com') > -1 ||	
		document.URL.search('binaryoptionswiz.com') > -1 ||
		document.URL.search('binaryoptionswinners.com') > -1) {
	host_url 						= "https://www.anyoption.com/";
	if (Number(skinId) == SKIN_ID_IT_REG) {
		host_url 					= "https://www1.anyoption.it/";
	}
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var validate_fields = true;
var err_field;
var langCode;
var language;
var countryCode;
var phoneCode;
var defualtPhoneCode;
setLangCodeBySkinId(skinId);

function g(id){return document.getElementById(id);}
// get lang code by skinId
function setLangCodeBySkinId(skinId) {
	//set default
	language = "English";
	//error messages
	first_name_mandatory = "First name is mandatory field";
	first_name_letters	 = "First name: Please enter letters only";
	last_name_mandatory	 = "Last name is mandatory field";
	last_name_letters	 = "Last name: Please enter letters only";
	email_mandatory 	 = "Email is mandatory field";
	email_invalid		 = "Email address is invalid";
	mobile_mandatory	 = "Mobile is mandatory field";
	mobile_invalid_num	 = "Mobile: Invalid number";
	mobile_min_length 	 = "Mobile is shorter than the allowed minimum of 7 characters";
	letters_regexp 		 = /^['a-zA-Z ]+$/;
	defualtPhoneCode	 = "GB";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			language = "English";
			first_name_mandatory = "First name is mandatory field";
			first_name_letters	 = "First name: Please enter letters only";
			last_name_mandatory	 = "Last name is mandatory field";
			last_name_letters	 = "Last name: Please enter letters only";
			email_mandatory 	 = "Email is mandatory field";
			email_invalid		 = "Email address is invalid";
			mobile_mandatory	 = "Mobile is mandatory field";
			mobile_invalid_num	 = "Mobile: Invalid number";
			mobile_min_length 	 = "Mobile is shorter than the allowed minimum of 7 characters";
			letters_regexp 		 = /^['a-zA-Z ]+$/;
			defualtPhoneCode 	 = "GB";
			break;
		case SKIN_ID_TR:
			language = "Turkish";
			first_name_mandatory = "Ad Zorunlu alan";
			first_name_letters	 = "Ad: Lütfen sadece harf kullanın";
			last_name_mandatory	 = "Soyad Zorunlu alan";
			last_name_letters	 = "Soyad: Lütfen sadece harf kullanın";
			email_mandatory 	 = "e-posta Zorunlu alan";
			email_invalid		 = "E-posta adresi geçersiz";
			mobile_mandatory	 = "Cep telefonu Zorunlu alan";
			mobile_invalid_num	 = "Cep telefonu: Validasyon Hatası";
			mobile_min_length 	 = "Cep telefonu, izin verilen minimum 7 karakterden daha kısa";
			letters_regexp 		 = /^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$/;
			defualtPhoneCode 	 = "TR";
			break;
		case SKIN_ID_AR:
			language = "English";
			first_name_mandatory = "الاسم الأول خطأ التأكيد";
			first_name_letters	 = "الاسم الأول: الرجاء إدخال حروف فقط";
			last_name_mandatory	 = "الاسم الأخير خطأ التأكيد";
			last_name_letters	 = "الاسم الأخير: الرجاء إدخال حروف فقط";
			email_mandatory 	 = "البريد الإلكتروني خطأ التأكيد";
			email_invalid		 = "عنوان البريد الإلكتروني غير صالح";
			mobile_mandatory	 = "الهاتف المحمول خطأ التأكيد";
			mobile_invalid_num	 = "الهاتف المحمول: خطأ التأكيد";
			mobile_min_length 	 = "الهاتف المحمول أقصر من الحد الأدنى المسموح به وهو 7 حرفاً";
			letters_regexp 		 = /^['a-zA-Z ]+$/;
			defualtPhoneCode 	 = "SA";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			language = "Spanish";
			first_name_mandatory = "Nombre Campo obligatorio";
			first_name_letters	 = "Nombre: Por favor, utilice letras solamente";
			last_name_mandatory	 = "Apellidos Campo obligatorio";
			last_name_letters	 = "Apellidos: Por favor, utilice letras solamente";
			email_mandatory 	 = "email Campo obligatorio";
			email_invalid		 = "El correo electrónico no es válido";
			mobile_mandatory	 = "Móvil Campo obligatorio";
			mobile_invalid_num	 = "Móvil Error de validación";
			mobile_min_length 	 = "Móvil es más corto que el mínimo permitido de 7 caracteres";
			letters_regexp 		 = /^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$/;
			defualtPhoneCode 	 = "ES";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			language = "German";
			first_name_mandatory = "Vorname Pflichtfeld";
			first_name_letters	 = "Vorname: Bitte benutzen Sie nur Buchstaben";
			last_name_mandatory	 = "Nachname Pflichtfeld";
			last_name_letters	 = "Nachname: Bitte benutzen Sie nur Buchstaben";
			email_mandatory 	 = "E-Mail Pflichtfeld";
			email_invalid		 = "E-Mail Adresse ist ungültig";
			mobile_mandatory	 = "Mobiltelefon Pflichtfeld";
			mobile_invalid_num	 = "Mobiltelefon: Überprüfungsfehler";
			mobile_min_length 	 = "Mobiltelefon muss mindestens 7 Zeichen lang sein";
			letters_regexp 		 = /^['a-zA-ZäÄöÖüÜß ]+$/;
			defualtPhoneCode 	 = "DE";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			language = "Italian";
			first_name_mandatory = "Nome Campo obbligatorio";
			first_name_letters	 = "Nome: Inserite solo lettere";
			last_name_mandatory	 = "Cognome Campo obbligatorio";
			last_name_letters	 = "Cognome: Inserite solo lettere";
			email_mandatory 	 = "e-mail Campo obbligatorio";
			email_invalid		 = "indirizzo e-mail non valido";
			mobile_mandatory	 = "Cellulare Campo obbligatorio";
			mobile_invalid_num	 = "Cellulare: Errore di convalida";
			mobile_min_length 	 = "Cellulare è più corto del valore minimo consentito di 7 caratteri";
			letters_regexp 		 = /^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$/;
			defualtPhoneCode 	 = "IT";
			break;
		case SKIN_ID_RU:
			language = "Russian";
			first_name_mandatory = "Имя Обязательное поле";
			first_name_letters	 = "Имя: Вводите только буквы";
			last_name_mandatory	 = "Фамилия Обязательное поле";
			last_name_letters	 = "Фамилия: Вводите только буквы";
			email_mandatory 	 = "Электронная почта Обязательное поле";
			email_invalid		 = "Неверный адрес эл. почты";
			mobile_mandatory	 = "Мобильный телефон Обязательное поле";
			mobile_invalid_num	 = "Мобильный телефон Ошибка валидации";
			mobile_min_length 	 = "Мобильный телефон короче допустимой длины 7 символов";
			letters_regexp 		 = /^['a-zA-ZА-Яа-я ]+$/;
			defualtPhoneCode 	 = "RU";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			language = "French";
			first_name_mandatory = "Prénom est un champ obligatoire";
			first_name_letters	 = "Prénom: Veuillez saisir des lettres uniquement";
			last_name_mandatory	 = "Nom est un champ obligatoire";
			last_name_letters	 = "Nom: Veuillez saisir des lettres uniquement";
			email_mandatory 	 = "Email est un champ obligatoire";
			email_invalid		 = "Adresse Email incorrecte";
			mobile_mandatory	 = "Téléphone portable est un champ obligatoire";
			mobile_invalid_num	 = "Téléphone portable: Nombre incorrect";
			mobile_min_length 	 = "Téléphone portable est plus court que le minimum autorisé de 7 caractères";
			letters_regexp 		 = /^['a-zA-Zéèàùâêîôûëïç ]+$/;
			defualtPhoneCode 	 = "FR";
			break;
		case SKIN_ID_KR:
			first_name_mandatory = "이름 필수 필드";
			first_name_letters	 = "이름: 문자만 사용하셔야 합니다.";
			last_name_mandatory	 = "성 필수 필드";
			last_name_letters	 = "성: 문자만 사용하셔야 합니다.";
			email_mandatory 	 = "이메일 필수 필드";
			email_invalid		 = "유효하지 않은 이메일 주소";
			mobile_mandatory	 = "휴대전화 필수 필드";
			mobile_invalid_num	 = "전화번호가 유효하지 않습니다";
			mobile_min_length 	 = "휴대번호가 최소로 필요한 7 자리의 문자 아래입니다";
			letters_regexp 		 = /^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$/;
			defualtPhoneCode 	 = "KR";				
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			language = "Chinese";
			first_name_mandatory = "名字 是必填栏";
			first_name_letters	 = "名字: 请只使用字母";
			last_name_mandatory	 = "姓 是必填栏";
			last_name_letters	 = "姓: 请只使用字母";
			email_mandatory 	 = "电子邮件 是必填栏";
			email_invalid		 = "无效的电子邮件地址";
			mobile_mandatory	 = "移动电话 是必填栏";
			mobile_invalid_num	 = "移动电话: 无效号码";
			mobile_min_length 	 = "移动电话 短于所允许的最小7个字符";
			letters_regexp 		 = /^['a-zA-Z一-龠 ]+$/;
			defualtPhoneCode 	 = "CN";
			break;
		case SKIN_ID_NL:
			language = "Netherlands";
			first_name_mandatory = "Voornaam is verplicht veld";
			first_name_letters	 = "Voornaam: alleen letters invullen";
			last_name_mandatory	 = "Achternaam is verplicht veld";
			last_name_letters	 = "Achternaam: alleen letters invullen";
			email_mandatory 	 = "E-mail is verplicht veld";
			email_invalid		 = "E-mailadres is ongeldig";
			mobile_mandatory	 = "Mobiel is verplicht veld";
			mobile_invalid_num	 = "Mobiel: ongeldig nummer";
			mobile_min_length 	 = "Mobiel is korter dan de toegestane 7 tekens";
			letters_regexp 		 = "/^['a-zA-Zéëï ]+$/";
			defualtPhoneCode 	 = "NL";
			break;
		case SKIN_ID_SV:
			language = "Svenska";
			first_name_mandatory = "Förnamn är obligatoriskt fält";
			first_name_letters	 = "Förnamn: Ange endast bokstäver";
			last_name_mandatory	 = "Efternamn är obligatoriskt fält";
			last_name_letters	 = "Efternamn: Ange endast bokstäver";
			email_mandatory 	 = "E-post är obligatoriskt fält";
			email_invalid		 = "E-postadressen är ogiltig";
			mobile_mandatory	 = "Mobilnummer är obligatoriskt fält";
			mobile_invalid_num	 = "Mobil:  Ogiltigt nummer";
			mobile_min_length 	 = "Mobilnumret är kortare än tillåtet minimum på 7 tecken";
			letters_regexp 		 = "/^['a-zA-ZÅÄÖåäö ]+$/";
			defualtPhoneCode 	 = "SE";
			break;
	}
}
var emailRegExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
var numberRegExp = /^\d+$/;
//Phone code keyDown
// Allow tab key
function phoneKeys(e) {
	var keynum;
	if(window.event) { // IE
		keynum = e.keyCode;
 	} else if(e.which) {  // Netscape/Firefox/Opera
		keynum = e.which;
    }
	if (keynum != 9) {  // tab key
		return false;
  	}
}


//Phone code prefix by country code
var countriesPhone = {  "AD":376,
						"AE":971,
						"AF":93,
						"AG":1286,
						"AI":1265,
						"AL":355,
						"AM":374,
						"AN":599,
						"AO":244,
						"AQ":672,
						"AR":54,
						"AS":684,
						"AT":43,
						"AU":61,
						"AW":297,
						"AZ":994,
						"BA":387,
						"BB":1246,
						"BD":880,
						"BE":32,
						"BF":226,
						"BG":359,
						"BH":973,
						"BI":257,
						"BJ":229,
						"BM":1441,
						"BN":673,
						"BO":591,
						"BR":55,
						"BS":1242,
						"BT":975,
						"BV":0,
						"BW":267,
						"BY":375,
						"BZ":501,
						"CA":1,
						"CC":61,
						"CF":236,
						"CG":242,
						"CH":41,
						"CI":225,
						"CK":682,
						"CL":56,
						"CM":237,
						"CN":86,
						"CO":57,
						"CR":506,
						"CU":53,
						"CV":238,
						"CX":61,
						"CY":357,
						"CZ":420,
						"DE":49,
						"DJ":253,
						"DK":45,
						"DM":1767,
						"DO":1809,
						"DZ":213,
						"EC":593,
						"EE":372,
						"EG":20,
						"EH":0,
						"ER":291,
						"ES":34,
						"ET":251,
						"FI":358,
						"FJ":679,
						"FK":500,
						"FM":691,
						"FO":298,
						"FR":33,
						"GA":241,
						"GB":44,
						"GD":1473,
						"GE":995,
						"GF":594,
						"GH":233,
						"GI":350,
						"GL":299,
						"GM":220,
						"GN":224,
						"GP":590,
						"GQ":240,
						"GR":30,
						"GT":502,
						"GU":1671,
						"GY":592,
						"HK":852,
						"HN":504,
						"HR":385,
						"HT":509,
						"HU":36,
						"ID":62,
						"IE":353,
						"IL":972,
						"IN":91,
						"IQ":964,
						"IR":98,
						"IS":354,
						"IT":39,
						"JM":1876,
						"JO":962,
						"JP":81,
						"KE":254,
						"KG":996,
						"KH":855,
						"KI":686,
						"KM":269,
						"KN":1869,
						"KP":850,
						"KR":82,
						"KW":965,
						"KY":1345,
						"KZ":7,
						"LA":856,
						"LB":961,
						"LC":1758,
						"LI":423,
						"LK":94,
						"LR":231,
						"LS":266,
						"LT":370,
						"LU":352,
						"LV":371,
						"LY":218,
						"MA":212,
						"MC":377,
						"MD":373,
						"ME":381,
						"MG":261,
						"MH":692,
						"MK":389,
						"ML":223,
						"MM":95,
						"MN":976,
						"MO":853,
						"MQ":596,
						"MR":222,
						"MS":1664,
						"MT":356,
						"MU":230,
						"MV":960,
						"MW":265,
						"MX":52,
						"MY":60,
						"MZ":258,
						"NA":264,
						"NC":687,
						"NE":227,
						"NF":672,
						"NG":234,
						"NI":505,
						"NL":31,
						"NO":47,
						"NP":977,
						"NR":674,
						"NU":683,
						"NZ":64,
						"OM":968,
						"PA":507,
						"PE":51,
						"PF":689,
						"PG":675,
						"PH":63,
						"PK":92,
						"PL":48,
						"PN":0,
						"PR":1787,
						"PT":351,
						"PW":680,
						"PY":595,
						"QA":974,
						"RE":262,
						"RO":40,
						"RS":381,
						"RU":7,
						"RW":250,
						"SA":966,
						"SB":677,
						"SC":248,
						"SD":249,
						"SE":46,
						"SG":65,
						"SH":290,
						"SI":386,
						"SK":421,
						"SL":232,
						"SM":378,
						"SN":221,
						"SO":252,
						"SR":597,
						"SV":503,
						"SY":963,
						"SZ":268,
						"TD":235,
						"TG":228,
						"TH":66,
						"TJ":992,
						"TK":690,
						"TM":993,
						"TN":216,
						"TO":676,
						"TP":670,
						"TR":90,
						"TT":1868,
						"TV":688,
						"TW":886,
						"TZ":255,
						"UA":380,
						"UG":256,
						"US":1,
						"UY":598,
						"UZ":998,
						"VA":39,
						"VE":58,
						"VG":0,
						"VI":1340,
						"VN":84,
						"VU":678,
						"WS":685,
						"YE":967,
						"YT":269,
						"ZA":27,
						"ZM":260,
						"ZR":243,
						"ZW":263}

function getPhoneCodeByCountry(countryCode){
	return countriesPhone[countryCode];
}

function closeErrorBox(){
	g("error_box").style.display = "none";
	g("Overlay").style.display = "none";
	//set focus on the error field
	err_field.focus();
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout(validate,500);
}

function submitForm() {
	g("contactForm").submit();
}


//phone code
function setPhoneCode() {	
	try {
		countryCode = geoplugin_countryCode();
	} catch (e) {
		countryCode = defualtPhoneCode;
	}
	if (null != countryCode &&  'undefined' != countryCode) {
		countryCode = countryCode.toUpperCase();
	}
	var phoneCode = getPhoneCodeByCountry(countryCode);
	if (phoneCode == null || phoneCode == undefined) {
		phoneCode = 0;
	}
	g("mobile_c").value = "+" + phoneCode;
}

//display error msg
function display_error(msg) {
	var imp = g('contactForm').getElementsByTagName('*');
	for(var i=0;i<imp.length; i++){
		imp[i].className = imp[i].className.replace(/error/gi,'');
	}
	//Fill error msg
	var error_msg = g("error_message_label");
	error_msg.innerHTML = msg;
	if (form_type == FORM_TYPE_REGULAR) { //regular display error
		//Display error msg
		var box = g("error_box");
		var overlay = g("Overlay");
		box.style.display = "block";
		overlay.style.display = "block";
		//Move box to the center
		box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
		overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
	}
}

//diplay lightbox
function startBtn() {
	//Display error msg
	var box = g("lightform");
	var overlay = g("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

function flashStartTrading(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

//close light box
function closeLightBox(){
	g("lightform").style.display = 'none';
	g("Overlay").style.display = 'none';
}

function setFocusFirstInput() {
	window.scrollTo(0, 0);
	g("first_name").focus();
}

// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	if (document.URL.search('tradingbinaryoption.com') > -1 ||
			document.URL.search('maxtradingtools.com') > -1 ||	
			document.URL.search('binaryoptionswiz.com') > -1 ||
			document.URL.search('binaryoptionswinners.com') > -1) {
		var oldPage = getUrlValue('pageR');
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + pageR) + '&' + gaLinkerParam;
	} else {	
		obj.href= host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString + '&' + gaLinkerParam;
	}
}
function validCheck(val,reg){
	if(reg.test(val) == false){return false;}
	return true;
}
function intfn(){
	g('contactForm').action = host_url + 'jsp/paramAfterLanding.html?' + queryString;
	// START SETTING PHONE CODE
	setPhoneCode();
}
function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}


////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
function validate(){
	var valid = true;
	if(g('first_name').value == ""){
		display_error(first_name_mandatory);
		g('first_name').parentNode.className += " error";
		valid = false;
		err_field = g('first_name');
	}
	else if(!validCheck(g('first_name').value,letters_regexp)){
		display_error(first_name_letters);
		g('first_name').parentNode.className += " error";
		valid = false;
		err_field = g('first_name');
	}
	else if(g('last_name').value == ""){
		display_error(last_name_mandatory);
		g('last_name').parentNode.className += " error";
		valid = false;
		err_field = g('last_name');
	}
	else if(!validCheck(g('last_name').value,letters_regexp)){
		display_error(last_name_letters);
		g('last_name').parentNode.className += " error";
		valid = false;
		err_field = g('last_name');
	}
	else if(g('email').value == ""){
		display_error(email_mandatory);
		g('email').parentNode.className += " error";
		valid = false;
		err_field = g('email');
	}
	else if(!validCheck(g('email').value,emailRegExp)){
		display_error(email_invalid);
		g('email').parentNode.className += " error";
		valid = false;
		err_field = g('email').parentNode;
	}
	else if(g('mobile_m').value == ""){
		display_error(mobile_mandatory);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m').parentNode;
	}
	else if(!validCheck(g('mobile_m').value,numberRegExp)){
		display_error(mobile_invalid_num);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m');
	}
	else if(g('mobile_m').value.length < 7){
		display_error(mobile_min_length);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m');
	}
	
	if(valid){
		g('contactForm').submit();
	}
	else{
		return false;
	}
}

/* END VALIDATION */