//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

//constants
var domain_el = document.URL.replace('https://','').replace('http://','').split('/')[0].split('.')[0];
var proxy_url 						= location.protocol + "//www.anyoption-china.com/proxy/";
var baidu_domain 					= "www.anyoption-china.com";
var host_url_168qiquan 				= location.protocol + "//hk.168qiquan.com/";
var cookie_domain_168qiquan 		= "168qiquan.com"; 	
var host_url_anyoption_it			= "https://www1.anyoption.it/";
var cookie_domain_anyoption_it 		= "anyoption.it";
if (document.URL.search('anyoption.it') > -1) {
	var cookie_domain 				= "anyoption.it";
	var host_url 					= "https://www1.anyoption.it/";
	var context_path				= "https://www1.anyoption.it";
	var context_path_ajax			= location.protocol + "//"+domain_el+".anyoption.it";
	var cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.it/";
	var json_url 					= "https://www1.anyoption.it/";		
	document.domain					= "anyoption.it";
	var etsserv 					= location.protocol + "//tkr.anyoption.it/";
} else if (document.URL.search('tradingbinaryoption.com') > -1) {
	document.domain					= "tradingbinaryoption.com";
	var cookie_domain 				= "tradingbinaryoption.com";
} else if (document.URL.search('maxtradingtools.com') > -1) {
	document.domain					= "maxtradingtools.com";
	var cookie_domain 				= "maxtradingtools.com";
} else if (document.URL.search('binaryoptionswiz.com') > -1) {
	document.domain					= "binaryoptionswiz.com";
	var cookie_domain 				= "binaryoptionswiz.com";
} else if (document.URL.search('binaryoptionswinners.com') > -1) {
	document.domain					= "binaryoptionswinners.com";
	var cookie_domain 				= "binaryoptionswinners.com";
} else {
	var cookie_domain 				= "anyoption.com";
	var host_url 					= "https://www.anyoption.com/";
	var context_path				= "https://www.anyoption.com";
	var context_path_ajax			= location.protocol + "//"+domain_el+".anyoption.com";
	var cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.com/";
	var json_url 					= "https://www.anyoption.com/";		
	document.domain					= "anyoption.com";
	var etsserv 					= location.protocol + "//tkr.anyoption.com/";
}

var FORM_TYPE_REGULAR	= 1;
var FORM_TYPE_LIGHTBOX	= 2;

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;
var SKIN_ID_EN 			= 25;
var SKIN_ID_ES 			= 26;
var SKIN_ID_CS 			= 27;
var SKIN_ID_PL 			= 28;
var SKIN_ID_PT 			= 29;

var COMB_ID_EN_DEFUALT	= 22;

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};
parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

var language;
setLangCodeBySkinId(skinId);

// get lang code by skinId
function setLangCodeBySkinId(skinId) {
	//set default
	language = "English";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			language = "English";
			break;
		case SKIN_ID_TR:
			language = "Turkish";
			break;
		case SKIN_ID_AR:
			language = "English";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			language = "Spanish";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			language = "German";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			language = "Italian";
			break;
		case SKIN_ID_RU:
			language = "Russian";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			language = "French";
			break;
		case SKIN_ID_KR:
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			language = "Chinese";
			break;
		case SKIN_ID_NL:
			language = "Netherlands";
			break;
		case SKIN_ID_SV:
			language = "Svenska";
			break;
	}
}

//get combId and skinId from cookie if not set default values
var fullURL;
try {
	fullURL = parent.document.URL;
} catch(e) {
	fullURL = document.URL;
}
//TODO: Find/Create parser that know how to handle @ char.
if (fullURL.indexOf("@") != -1) {
	fullURL=fullURL.replace(/@/g,"%40");
}
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = SKIN_ID_EN_REG;
}
if (null == combId ||  'undefined' == combId) {
	combId = COMB_ID_EN_DEFUALT;
}
if (fullURL.indexOf("168qiquan.com") != -1) {
	host_url = host_url_168qiquan;
}
if (fullURL.indexOf("anyoption.it") != -1) {
	host_url = host_url_anyoption_it;
}
if (document.URL.search('tradingbinaryoption.com') > -1) {
	cookie_domain 				= "tradingbinaryoption.com";
	host_url 					= "https://www.anyoption.com/";
	context_path				= "https://www.anyoption.com";
	context_path_ajax			= location.protocol + "//"+domain_el+".tradingbinaryoption.com";
	cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.com/";
	json_url 					= "https://www.anyoption.com/";		
	document.domain				= "tradingbinaryoption.com";
	if (Number(skinId) == SKIN_ID_IT_REG) {
		host_url 					= "https://www1.anyoption.it/";
		context_path				= "https://www1.anyoption.it";
	}
}
if (document.URL.search('maxtradingtools.com') > -1) {
	cookie_domain 				= "maxtradingtools.com";
	host_url 					= "https://www.anyoption.com/";
	context_path				= "https://www.anyoption.com";
	context_path_ajax			= location.protocol + "//"+domain_el+".maxtradingtools.com";
	cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.com/";
	json_url 					= "https://www.anyoption.com/";		
	document.domain				= "maxtradingtools.com";
	if (Number(skinId) == SKIN_ID_IT_REG) {
		host_url 					= "https://www1.anyoption.it/";
		context_path				= "https://www1.anyoption.it";
	}
}
if (document.URL.search('binaryoptionswiz.com') > -1) {
	cookie_domain 				= "binaryoptionswiz.com";
	host_url 					= "https://www.anyoption.com/";
	context_path				= "https://www.anyoption.com";
	context_path_ajax			= location.protocol + "//"+domain_el+".binaryoptionswiz.com";
	cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.com/";
	json_url 					= "https://www.anyoption.com/";		
	document.domain				= "binaryoptionswiz.com";
	if (Number(skinId) == SKIN_ID_IT_REG) {
		host_url 					= "https://www1.anyoption.it/";
		context_path				= "https://www1.anyoption.it";
	}
}
if (document.URL.search('binaryoptionswinners.com') > -1) {
	cookie_domain 				= "binaryoptionswinners.com";
	host_url 					= "https://www.anyoption.com/";
	context_path				= "https://www.anyoption.com";
	context_path_ajax			= location.protocol + "//"+domain_el+".binaryoptionswinners.com";
	cdn_url_json 				= location.protocol + "//"+domain_el+".anyoption.com/";
	json_url 					= "https://www.anyoption.com/";		
	document.domain				= "binaryoptionswinners.com";
	if (Number(skinId) == SKIN_ID_IT_REG) {
		host_url 					= "https://www1.anyoption.it/";
		context_path				= "https://www1.anyoption.it";
	}
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);

// create link
var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
function getLink(obj) {
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	if (document.URL.search('tradingbinaryoption.com') > -1 ||
			document.URL.search('maxtradingtools.com') > -1 ||	
			document.URL.search('binaryoptionswiz.com') > -1 ||
			document.URL.search('binaryoptionswinners.com') > -1) {
		var page = 'registerAfterLanding';
		var oldPage = getUrlValue('pageR');	
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + page) + '&' + gaLinkerParam;
	} else {
		obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&' + queryString + '&' + gaLinkerParam;
	}
}

/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
/*********** ajax.js ************/

function getXMLHttp() {
	var xmlHttp;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	return xmlHttp;
}
function AJAXInteractionTxt(url, callback) {
  	var req = init();
    req.onreadystatechange = processRequest;

    function init() {
   		if (window.XMLHttpRequest) {
        	return new XMLHttpRequest();
        } else if (window.ActiveXObject) {
        	return new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (callback) {callback(req.responseText);}
			}
		}
    }

    this.doGet = function() {
		// make a HTTP GET request to the URL asynchronously
		req.open("GET", url, true);
		req.send(null);
   	}
}
/*START SHORT REG*/
function addScriptToHead(filename,scr_id,folder){
	if(!g(scr_id)){
		if(typeof folder == 'undefined'){
			var folder_new = '';
		}
		else{
			var folder_new = folder;
		}
		if(typeof funnel_version == 'undefined'){
			funnel_version = '0';
		}
		var js = document.createElement('script');
		js.id = scr_id;
		js.src = context_path+folder_new+"/js/"+filename+"?ver="+funnel_version;
		document.getElementsByTagName('head')[0].appendChild(js);
	}
}
var funnel_div_id = '';
var register_lp = true;
function initRegForm(id,skinId){
	funnel_div_id = id;
	addScriptToHead('regForm_all.js','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
	//addScriptToHead('clientInfo.js','etsClientInfo');
}
var global_deposit_prefix = '';
function inpFocus(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	var pass = (field.getAttribute('data-p') != undefined)?true:false;
	var tooltip = (field.getAttribute('data-tooltip') != undefined)?g(field.getAttribute('data-tooltip')):'';
	if(field.value == ""){
		field.value = def;
		if(pass){field.type = 'text';}
		field.className = field.className.replace('active','');
	}
	else if(field.value == def){
		field.value = "";
		if(pass){field.type = 'password';}
		field.className += " active";
	}
	else{
		field.className += " active";
	}
	if(field.getAttribute('data-tooltip_show') == 't'){
		if(tooltip != ""){
			tooltip.style.display = "none";
			field.setAttribute('data-tooltip_show','');
		}	
	}
	else{
		if(tooltip != ""){
			tooltip.style.display = "block";
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			/*if (typeof staticToolTip == 'undefined' || !staticToolTip) {
				tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			}*/
			field.setAttribute('data-tooltip_show','t');
		}	
	}
	if(noblur == undefined){
		field.onblur = function(){
			inpFocus(field,noblur);
			eval(field.getAttribute('onblur'));
		}
	}
}
function checkBox(th){
	var inp = th.getElementsByTagName('input')[0];
	if(inp.checked){
		inp.checked = false;
		th.className = th.className.replace(/ checked/g,'');
	}
	else{
		inp.checked = true;
		th.className += " checked";
	}
}
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    'use strict';
    if (this == null) {
      throw new TypeError();
    }
    var n, k, t = Object(this),
        len = t.length >>> 0;

    if (len === 0) {
      return -1;
    }
    n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  };
}
/*END short reg*/
/*dinamic header*/
function dynamicHeader() {
	//url parameter h1
	var h1 = uri.queryKey['h1'];
	var output;
	if (h1 != null) {
		 output = decodeURIComponent(h1);
		 g('dynamicHeader').innerHTML = output;
	}
}
// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	if (document.URL.search('tradingbinaryoption.com') > -1 ||
			document.URL.search('maxtradingtools.com') > -1 ||	
			document.URL.search('binaryoptionswiz.com') > -1 ||
			document.URL.search('binaryoptionswinners.com') > -1) {
		var oldPage = getUrlValue('pageR');
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + pageR) + '&' + gaLinkerParam;
	} else {
		// window.location = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
		obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString + '&' + gaLinkerParam;
	}	
}
//url parameters
var dp		= uri.queryKey['dp'];
var aff		= uri.queryKey['aff'];
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
if (null == dp ||  'undefined' == dp) {
	dp = '';
}

//function buildURLWithParameters(url) {
//	 url += '&combid=' + combId + '&dp=' + dp + '&aff_sub1=' + aff_sub1 + '&aff_sub2=' + aff_sub2 + '&aff_sub3=' + aff_sub3;
//	 return url;
//}
function buildURLWithParameters(url, param1, param2, source) {
	if (source != null && source == 'media') {
		url = url.replace(param1, param1 + dp);
		url = url.replace(param2, param2 + combId);
	} else if (source != null && source == 'affiliate') {
		url = url.replace('aff=', 'aff=' + aff);
	} 		
	return url;
}

// create link redirect to LP
function getRedirectLinkToLP(obj, page) {
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	var oldPage = getUrlValue('pageR');
	obj.href = context_path_ajax + '/landing.shtml?' + queryString.replace('pageR=' + oldPage,'pageR=' + page) + '&' + gaLinkerParam;
}
function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}
function add_agreement_to_iframe(id,new_url){
	if (null == new_url || 'undefined' == new_url) 
		new_url = host_url + 'jsp/agreement.jsf?refresh=true&s=' + skinId
	else 
		new_url  +='&s=' + skinId
		
	g(id).setAttribute('data-src',new_url);
}

function mpTrack(params) {
	//this is an empty function that eventualy one day in the future will use to track mixpanel events on short reg (funnel)
}

function getCookie(c_name){
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
	  	c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
	  	c_value = null;
	} else {
	  	c_start = c_value.indexOf("=", c_start) + 1;
	  	var c_end = c_value.indexOf(";", c_start);
	  	if (c_end == -1) {
			c_end = c_value.length;
	  	}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

//Add an iframe for the google analytics cookie
var analyticsInterval = setInterval(function(){
	if(document.body && window.ga && typeof(ga)=="function" && ga.getAll){
		clearInterval(analyticsInterval);
		var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
		var url = cdn_url_json + 'analytics.html?' + gaLinkerParam;
		var iframe = document.createElement('iframe');
		iframe.id = 'analyticsIframe';
		iframe.name = Date.now();
		iframe.dataset.src = url;
		iframe.style.width = "0px";
		iframe.style.height = "0px";
		iframe.allowtransparency = true;
		iframe.onload = function(){
			dataLayer.push({"event":"iframeLoaded"});
		}
		iframe.src = url;
		document.body.appendChild(iframe);
	}
}, 100);

function loadHexParam(param){
	if(uri && uri.queryKey){
		var hexParam = uri.queryKey[param];
		var matches = document.querySelectorAll('[data-load-hex-from-url="' + param + '"]');
		for(var i = 0; i < matches.length; i++){
			matches[i].value = hex2a(hexParam);
		}
	}
}

function hex2a(hexx) {
	var hex = hexx.toString();//force conversion
	var str = '';
	for (var i = 0; i < hex.length; i += 2)
		str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
	return str;
}

function submitContactMailForm(form, successRedirectLink, errorHolderId){
	clearErrorMessage();
	
	var contact = {};
	
	contact.email = form.email.value;
	
	contact.skinId = getUrlValue("s");
	contact.combId = getUrlValue("combid");
	if(contact.combId == ""){
		contact.combId = 0;
	}
	contact.dynamicParameter = getUrlValue("dp");
	contact.aff_sub1 = getUrlValue("aff_sub1");
	contact.aff_sub2 = getUrlValue("aff_sub2");
	if(getUrlValue("gclid") != ""){
		contact.aff_sub3 = getUrlValue("gclid");
	}else{
		contact.aff_sub3 = getUrlValue("aff_sub3");
	}
	
	var request = {
		contact: contact
	}
	
	$.ajax({
		url     : context_path_ajax + '/jsonService/AnyoptionService/insertContactMailForm',
		type    : "POST",
		dataType: "json",
		data    : JSON.stringify(request),
		contentType: "application/json;charset=utf-8",
		success : function(data) {
			if(data.errorCode != 0){//Error
				handleError(data);
			}else{//Success
				handleSuccess(data);
			}
		},
		error   : function( xhr, err ) {
			
		}
	});
	
	function handleSuccess(data){
		if(successRedirectLink != ''){
			window.location = './' + successRedirectLink + '?' + uri.query;
		}
	}
	
	function handleError(data){
		if(data.userMessages[0]){
			addErrorMessage(data.userMessages[0].message);
		}
	}
	
	function clearErrorMessage(){
		if(errorHolderId){
			document.getElementById(errorHolderId).innerHTML = '';
		}
	}
	
	function addErrorMessage(message){
		if(errorHolderId && message){
			document.getElementById(errorHolderId).innerHTML = message;
		}
	}
}
