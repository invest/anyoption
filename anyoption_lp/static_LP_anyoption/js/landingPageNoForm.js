
//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 2;
}
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
if (fullURL.indexOf("168qiquan.com") != -1) {
	host_url = host_url_168qiquan;
}
if (fullURL.indexOf("anyoption.it") != -1) {
	host_url = host_url_anyoption_it;
}

// create link
var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
function getLink(obj) {	
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	if (document.URL.search('tradingbinaryoption.com') > -1 ||
			document.URL.search('maxtradingtools.com') > -1 ||	
			document.URL.search('binaryoptionswiz.com') > -1 ||
			document.URL.search('binaryoptionswinners.com') > -1) {	
		var page = 'registerAfterLanding';
		var oldPage = getUrlValue('pageR');	
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + page) + '&' + gaLinkerParam;
	} else {
		obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&' + queryString + '&' + gaLinkerParam;
	}
}

// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	if (document.URL.search('tradingbinaryoption.com') > -1 ||
			document.URL.search('maxtradingtools.com') > -1 ||	
			document.URL.search('binaryoptionswiz.com') > -1 ||
			document.URL.search('binaryoptionswinners.com') > -1) {
		var oldPage = getUrlValue('pageR');
		obj.href = host_url + 'jsp/landing.jsf?' + queryString.replace('pageR=' + oldPage,'pageR=' + page) + '&' + gaLinkerParam;
	} else {
		obj.href= host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString + '&' + gaLinkerParam;
	}
}

function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}