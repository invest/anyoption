/*By Georgi Diamandiev*/
/*version 1.1*/
function g(id){return document.getElementById(id);}
function gd_add_gallery(){
	var as = document.getElementsByTagName('a');
	for(var i=0;i<as.length;i++){
		if(as[i].getAttribute('data-gdGallery') == 't'){
			as[i].onclick = function(){
				gd_open_gallery(this);
				return false;
			}
		}
	}
	gd_creat_gallery();
}
function gd_open_gallery(th){
	var iframes = document.getElementsByTagName('iframe');
	for(var i=0;i<iframes.length;i++){
		if(iframes[i].getAttribute('data-hide') == 't'){
			iframes[i].style.display = "none";
		}
	}
	var gd_gallery_bgr = g('gd_gallery_bgr');
	gd_gallery_bgr.style.display = "block";
	if(typeof th.getAttribute('data-popUpEl') != 'undefined'){
		var div = g(th.getAttribute('data-popUpEl'));
		div.style.display = "block";
		gd_gallery_bgr.setAttribute('data-popUpEl',th.getAttribute('data-popUpEl'));
		var iframs = div.getElementsByTagName('iframe');
		for(var i=0; i<iframs.length; i++){
			if(typeof iframs[i].getAttribute('data-src') != 'undefined'){
				iframs[i].src = iframs[i].getAttribute('data-src');
			}
		}
	}
	else{
		var div = g('gd_gallery_in');
		div.style.display = "block";
		div.innerHTML = "<img src='"+th.href+"' style='display:none;'/>";
		var img = div.getElementsByTagName('img')[0];
		img.onload = function(){
			this.style.display = "block";
			this.parentNode.style.marginTop = -(this.offsetHeight/2)+"px";
			this.parentNode.style.marginLeft = -(this.offsetWidth/2)+"px";
		}
	}
}
function gd_creat_gallery(){
	var bg = document.createElement('div');
	bg.className = "gd_gallery_bgr";
	bg.id = "gd_gallery_bgr";
	bg.onclick = function(){
		gd_close_gallery();
	}
	window.document.body.appendChild(bg);
	var div = document.createElement('div');
	div.className = "gd_gallery_in";
	div.id = "gd_gallery_in";
	window.document.body.appendChild(div);
}
function gd_close_gallery(){
	var bg = g('gd_gallery_bgr');
	bg.style.display = "none";
	if(typeof bg.getAttribute('data-popUpEl') != 'undefined'){
		g(bg.getAttribute('data-popUpEl')).style.display = "none";
	}
	else{
		g('gd_gallery_in').style.display = "none";
	}
	var iframes = document.getElementsByTagName('iframe');
	for(var i=0;i<iframes.length;i++){
		if(iframes[i].getAttribute('data-hide') == 't'){
			iframes[i].style.display = "";
		}
	}
}