/*********** default_values.js ************/

//set default
//parse url
parseUri.options.strictMode = true;

var fullURL;
try {
	fullURL = parent.document.URL;
} catch(e) {
	fullURL = document.URL;
} 
if (fullURL.indexOf("@") != -1) {
	fullURL=fullURL.replace(/@/g,"%40");
}
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
var gclid = uri.queryKey['gclid'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
	
} else {
	aff_sub3 = '';
}
if (gclid) {
	aff_sub3 = 'gclid_' + gclid.substr(0, 30);
}

var skinId = uri.queryKey['s'];
var dp = uri.queryKey['dp'];
var affid = uri.queryKey['affid'];
var tradedoubler = uri.queryKey['tduid'];
var dfaPlaId = uri.queryKey['placmentId'];
var dfaCreid = uri.queryKey['creativeId'];
var dfaMacro = uri.queryKey['macro'];
var pageR = uri.queryKey['pageR'];
var httpRefferer;
var host = uri['host'];
var utmMedium = uri.queryKey['utm_medium'];
var utmSource = uri.queryKey['utm_source'];

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_EN_REG		= 16;
var SKIN_ID_KR 			= 17;
var SKIN_ID_ES_REG		= 18;
var SKIN_ID_DE_REG		= 19;
var SKIN_ID_IT_REG		= 20;
var SKIN_ID_FR_REG		= 21;
var SKIN_ID_168QIQUAN 	= 22;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;
var SKIN_ID_EN 			= 25;
var SKIN_ID_ES 			= 26;
var SKIN_ID_CS 			= 27;
var SKIN_ID_PL 			= 28;
var SKIN_ID_PT 			= 29;


var isLive = false;
if (host == "http://cdn.anyoption.com" || host == "http://www.anyoption.com" || host == "https://www.anyoption.com") {
	isLive = true;
}

if (skinId == 'undefined' || skinId == null) {
	skinId = 2;
}
if (combId == 'undefined' || combId == null) {
	combId = getDefaultCombIdBySkinId(skinId);
}
if (get_cookie("dp") == null) {
	set_cookie("dp", dp, 365, '/', cookie_domain, '');
}
if (get_cookie("combid") == null) {
	set_cookie("combid", combId, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub1") == null) {
	set_cookie("aff_sub1", aff_sub1, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub2") == null) {
	set_cookie("aff_sub2", aff_sub2, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub3") == null) {
	set_cookie("aff_sub3", aff_sub3, 365, '/', cookie_domain, '');
}

function get_cookie_val(flag, httpRef) {
	var params;
	
	if (flag =='Static') {
       	params = "MS{" + mId + "^CS{" + combId + "^HTTP{" + httpRef +  "^DP{" + dp + "^TS{" + getFullDate() + "^UTM{" + utmSource + "^aff_sub1{" + aff_sub1 + "^aff_sub2{" + aff_sub2 + "^aff_sub3{" + aff_sub3;
	} else {
		params = "HTTP{" + httpRef + "^CD{" + combId + "^TD{" +getFullDate() + "^DP{" + dp  + "^UTM{" + utmSource + "^aff_sub1{" + aff_sub1 + "^aff_sub2{" + aff_sub2 + "^aff_sub3{" + aff_sub3;
	}
	
	return params;
}

function isLeadActivity() {
	var res = true;
	
	try {
		if (utmMedium.toUpperCase() == 'POP_UNDER' || utmMedium.toUpperCase() == 'POP_UNDER_SITE') {
			res = false
		}
	} catch(e) {
		res = true;
	}
	
	return res;	
}

function getClickCount(cookieName) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	var count = 1;

	for ( i = 0; i < a_all_cookies.length; i++ ) {
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );

		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// chech if contains 
		if ( cookie_name.indexOf(cookieName) != -1 ) {
			count++;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	
	return count;
}

//default combination by skin
function getDefaultCombIdBySkinId(skinId) {
	//set default
	var combId = 22;
	switch (Number(skinId)) {
		case SKIN_ID_EN:
			combId = 22;
			break;
		case SKIN_ID_TR:
			combId = 24
			break;
		case SKIN_ID_AR:
			combId = 1401;
			break;
		case SKIN_ID_ES:
			combId = 23;
			break;
		case SKIN_ID_DE:
			combId = 654;
			break;
		case SKIN_ID_IT:
			combId = 2134;
			break;
		case SKIN_ID_RU:
			combId = 2463;
			break;
		case SKIN_ID_FR:
			combId = 3908;
			break;
		case SKIN_ID_EN_US:
			combId = 4132;
			break;
		case SKIN_ID_ES_US:
			combId = 4133;
			break;
		case SKIN_ID_ZH:
			combId = 10803;
			break;
		case SKIN_ID_NL:
			combId = 41285;
			break;
		case SKIN_ID_EN:
			combId = 22;
			break;
		case SKIN_ID_ES:
			combId = 23;
			break;
		case SKIN_ID_CS:
			combId = 41286;
			break;
		case SKIN_ID_PL:
			combId = 41286;
			break;
		case SKIN_ID_PT:
			combId = 41286;
			break;
	}
	return combId;
}

//get langCode by skin
function getLangCodeBySkinId(skinId) {
	//set default
	langCode = "en";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
		case SKIN_ID_EN_REG:
			langCode = "en";
			break;
		case SKIN_ID_TR: 
			langCode = "tr";
			break;
		case SKIN_ID_AR:
			langCode = "ar";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
		case SKIN_ID_ES_REG:
			langCode = "es";
			break;
		case SKIN_ID_DE:
		case SKIN_ID_DE_REG:
			langCode = "de";
			break;
		case SKIN_ID_IT:
		case SKIN_ID_IT_REG:
			langCode = "it";
			break;
		case SKIN_ID_RU:
			langCode = "ru";
			break;
		case SKIN_ID_FR:
		case SKIN_ID_FR_REG:
			langCode = "fr";
			break;
		case SKIN_ID_ZH:
		case SKIN_ID_168QIQUAN:
			langCode = "zh";
			break;
		case SKIN_ID_KR: 
			langCode = "kr";
			break;
		case SKIN_ID_NL: 
			langCode = "nl";
			break;
		case SKIN_ID_SV: 
			langCode = "sv";
			break;
		case SKIN_ID_EN: 
			langCode = "en";
			break;
		case SKIN_ID_ES: 
			langCode = "es";
			break;
		case SKIN_ID_CS: 
			langCode = "cs";
			break;
		case SKIN_ID_PL: 
			langCode = "pl";
			break;
		case SKIN_ID_PT: 
			langCode = "pt";
			break;
	}
	return langCode;
}

function set_cookie( name, value, expires, path, domain, secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );

	/*
	if the expires variable is set, make the correct
	expires time, the current script below will set
	it for x number of days, to make it for hours,
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );

	document.cookie = name + "=" + escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
	( ( path ) ? ";path=" + path : "" ) +
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}

// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function get_cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for ( i = 0; i < a_all_cookies.length; i++ ) {
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name ) {
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 ) {
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	
	if ( !b_cookie_found ) {
		return null;
	}
}

//Create netrefer combinations
var netreferCombinations ={	"2":[869,2203,4117,4180,4181,4182,4183,4184,6880,6889,6897,7704,10136],
							"3":[870,6885,6894,6902,10163],
							"4":[872,6886,6887,6895,10140],
							"5":[871,4121,4130,4131,6881,6890,6898,6966,7706,10143],
							"8":[873,4185,4186,4187,4188,4189,4190,6884,6888,6896,10142],
							"9":[2133,4199,4200,4201,4202,4203,6882,6892,6900,6968,6970,7707,10155],
							"10":[2464,5575,5576,5577,6883,6893,6901,10159],
							"12":[3930,4204,4205,4206,4207,4294,6879,6891,6899,7699,10146],
							"13":[8878,8880],
							"14":[8879,8881]}

//Check if Netrefer combination
function isNetreferCombination(combinationId) {
	var combinations = new Array();
	for (var s = 2 ; s < 15 ; s++) {
		combinations = netreferCombinations[s];
		if (null != combinations && 'undefined' !=  combinations && combinations.length > 0) {
			for (var i = 0; i < combinations.length ; i++) {
				if (combinations[i] == combinationId){
					return true;
				}
			}
		}
	}
	return false;
}

//UTF-8 encoding / decoding (object).
UTF8 = {
	encode: function(s) {
			for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
				s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
			);
			return s.join("");
	},
	decode: function(s) {
			for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
				((a = s[i][c](0)) & 0x80) &&
				(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
			);
			return s.join("");
	}
};

function getFullDate() {
	var resultDateTime;
	try {
		resultDateTime = serverDateTime.substr(1, serverDateTime.length);
		if(resultDateTime.length != 17){
			resultDateTime = getFullDateClientSide();
		}
		
	} catch(e) {
		resultDateTime = getFullDateClientSide();
	};
	
	return resultDateTime;
}

function getFullDateClientSide() {
	var date = new Date();
	var day = date.getUTCDate();
	var month = date.getUTCMonth() + 1; //January is 0.
	var year = date.getUTCFullYear() - 2000; //get year in 2 digits.
	var hour = date.getUTCHours(); //In GMT.
	var minutes = date.getUTCMinutes();
	var seconds = date.getUTCSeconds();
	//var AMorPM;
	//if (hour < 12) { AMorPM = "AM"; } else { AMorPM = "PM"; hour = hour - 12; }
	if (day < 10) {
		day = '0' + day;
	}
	if (month < 10) {
		month ='0' + month;
	}
	if (hour < 10) {
		hour = '0' + hour;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	//var today = day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ':' + seconds;
	var today = hour + ':' + minutes + ':' + seconds + ' ' + day + '/' + month + '/' + year;
	//var today = hour + ':' + minutes + ' ' + day + '/' + month + '/' + year;
	//var today = day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ' ' + AMorPM;
	return today;
}

function toHexString(str) {
    var tmp = UTF8.encode(str);
    var output = "";
    for (var i = 0; i < tmp.length; i++) {
        output = output + tmp.charCodeAt(i).toString(16);
    }
    return output;
}

function fromHexString(str) {
    temp = "";
    for (var i = 0; i < str.length; i = i + 2) {
        temp = temp + String.fromCharCode(parseInt(str.substring(i, i + 2), 16));
    }
    return UTF8.decode(temp);
}

function insertClick(staticPart, dinamicPart, redirectTimeout, redirectFnc) {
	var insClick = new InsClick(staticPart, dinamicPart, redirectTimeout, redirectFnc);
	function InsClick(staticPart, dinamicPart, redirectTimeout, redirectFnc) {
		this.staticPart = staticPart;
		this.dinamicPart = dinamicPart;
		this.redirectTimeout = redirectTimeout;
		this.redirectFnc = redirectFnc;

		this.ticker = function() {
			insClick.xmlHttp = getXMLHttp();
			if (null == insClick.xmlHttp) {
				return false;
			}
			insClick.xmlHttp.onreadystatechange = function() {
				if (insClick.xmlHttp.readyState == 4) {
					if (insClick.xmlHttp.responseText == "true") {
						// TODO: success
					} else {
						// TODO: fail
					}
					clearTimeout(redirectTimeout);
					redirectFnc();
				}
			}
			insClick.xmlHttp.open("POST", "/jsonService/AnyoptionService/insertMarketingTrackerClick", true);
			insClick.xmlHttp.setRequestHeader("Content-Type", "json");
			insClick.xmlHttp.send(JSON.stringify({"staticPart" : insClick.staticPart, "dynamicPart" : insClick.dinamicPart}));
		}
	}
	insClick.ticker();
}

 //Get Marketing Tracking MID from cookie
function getMid() {	
	var mid = "";
	try {
		var cookieParams = get_cookie("smt")
		cookieParams = fromHexString(cookieParams);	
		mid = cookieParams.substr(3,cookieParams.indexOf("^CS")-3);
	} catch(e) {
		mid = "";
	}
    return mid;
}	


