var SKIN_ID_ZH 			= 15;
//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 2;
}
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
if (fullURL.indexOf("168qiquan.com") != -1) {
	host_url = host_url_168qiquan;
}
if (fullURL.indexOf("anyoption.it") != -1) {
	host_url = host_url_anyoption_it;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);


function replaceall(str,replace,with_this) {
    var str_hasil = "";
    var temp;

    for(var i = 0; i < str.length; i++) {// not need to be equal. it causes the last change: undefined..
        if (str[i] == replace) {
			temp = with_this;
        } else {
			temp = str[i];
        }
        str_hasil += temp;
    }

    return str_hasil;
}

// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function get_cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for ( i = 0; i < a_all_cookies.length; i++ ) {
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name ) {
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 ) {
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found ) {
		return null;
	}
}

//UTF-8 encoding / decoding (object).
UTF8 = {
	encode: function(s) {
			for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
				s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
			);
			return s.join("");
	},
	decode: function(s) {
			for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
				((a = s[i][c](0)) & 0x80) &&
				(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
			);
			return s.join("");
	}
};

function fromHexString(str) {
    temp = "";
    for (var i = 0; i < str.length; i = i + 2) {
        temp = temp + String.fromCharCode(parseInt(str.substring(i, i + 2), 16));
    }
    return UTF8.decode(temp);
}

 //Get Marketing Tracking MID from cookie
function getMid() {	
	var mid = "";
	try {
		var cookieParams = get_cookie("smt")
		cookieParams = fromHexString(cookieParams);	
		mid = cookieParams.substr(3,cookieParams.indexOf("^CS")-3);
	} catch(e) {
		mid = "";
	}
    return mid;
}
/* ************************************************************************************ */

window.onload = function() {
/*
	addDeviceDesign(document.getElementById('detection_design'));
	addDeviceDesign(document.getElementById('detection_design_box'));
	addDeviceDesign(document.getElementById('detection_design_box2'));
	*/
	var deviceButton = document.getElementById('device_button');
	if(deviceButton != null) {
		if (isMobileDevice.Android()) {
			deviceButton.className = "android_button";
		} else if (isMobileDevice.iOS()) {
			deviceButton.className = "iphone_button";
		} 
	}
	
	designNoChicken(document.getElementById('device_button_no_chicken'));
	designNoChicken(document.getElementById('device_button_no_chicken2'));
};

function addDeviceDesign(detectionDesign) {
	if(detectionDesign != null) {	
		/*if (deviceType.isTablet()) {
			detectionDesign.className += " clsTablet clsNotMobile";
		} else 
		*/
		if (isMobileDevice.Windows()) {
			detectionDesign.className += " clsNotTablet clsNotMobile";		
		} else if (isMobileDevice.Android() || isMobileDevice.iOS()) {
			detectionDesign.className += " clsNotTablet clsMobile";
		}
	}
}
function designNoChicken (noChickenButton) {
	if(noChickenButton != null) {
		if (isMobileDevice.Android()) {
			noChickenButton.className = "android_button_no_chicken";
		} else if (isMobileDevice.iOS()) {
			noChickenButton.className = "iphone_button_no_chicken";
		}
		noChickenButton.alt = "DOWNLOAD!"; 
	}
}

function generateDeviceButton(obj, page, site_id) {
	//var deviceButton = document.getElementById('device_button');
	if (typeof site_id == 'undefined') {
		site_id = 0;
	}
	if(obj != null) {
		obj.href = generateUrlDownloadApp(obj, page, site_id);
	}
}

function generateUrlDownloadApp(obj, page, site_id){
	if (typeof site_id == 'undefined') {
		site_id = 0;
	}
	var host_name = "https://www.anyoption.com";
	if (site_id !=0) {
		host_name = "https://www.copyop.com";
	}
	
	var translatedQueryString = getTranslatedQueryByMarketingDictionary();
	var appStoreUrl = host_name + '/appsflyerios?' + translatedQueryString; 
	var googlePlayUrl = host_name + '/appsflyerandroid?' + translatedQueryString;
	var zh91Url = 'http://apk.91.com/Soft/Android/com.anyoption.android.app-19-6.html';
	var temp = replaceall(queryString, '=', '%3D');
	var temp2 = replaceall(temp, '&', '%26');
	var midParam = '%26mid%3D' + getMid();
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
	var url = "";
	if (isMobileDevice.iOS()) {
		url = appStoreUrl;
	} else if (isMobileDevice.Android()) {
		if (SKIN_ID_ZH == skinId) {
			url = zh91Url;
		} else {
			url = googlePlayUrl;
		}	
	} else { //is want to be pc.
		if (site_id == 0){
			var pageR = 'registerAfterLanding';
			if (page != null && page != 'undefined' && page != 'register') {
				pageR = page;
			}
			url = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString + '&' + gaLinkerParam;
		} else {
			var pageR = '';
			if (page != null && page != 'undefined' && page != '') {
				pageR = page;
			}
			url = host_name + '/' + pageR + '?' + queryString + '&' + gaLinkerParam;
		}
	}
	return url;
}

function getRedirectLinkToPageWithMobileSupport(obj, page, site_id) {

	if (typeof site_id == 'undefined')
		site_id = 0;

	var translatedQueryString = getTranslatedQueryByMarketingDictionary();
	
	host_name = "https://www.anyoption.com";
	if (site_id !=0)
	{
		host_name = "https://www.copyop.com";
	}
    var appStoreUrl = host_name+ '/appsflyerios?' + translatedQueryString;
    var googlePlayUrl = host_name+ '/appsflyerandroid?' + translatedQueryString;
    //var zh91Url = 'http://apk.91.com/Soft/Android/com.anyoption.android.app-19-6.html';
    var temp = replaceall(queryString, '=', '%3D');
    var temp2 = replaceall(temp, '&', '%26');
    var midParam = '%26mid%3D' + getMid();
	var gaLinkerParam = (typeof(ga)=="function") ? ga.getAll()[0].get('linkerParam') : '';
    var url = "";
    if (isMobileDevice.iOS()) {
    	 obj.href = appStoreUrl;
    } else if (isMobileDevice.Android()) {
        if (SKIN_ID_ZH == skinId) {
                        obj.href = zh91Url;
        } else {
                        obj.href = googlePlayUrl;
        }             
    } else { //is want to be pc.
			if (site_id == 0)
                getRedirectLinkToPage(obj, page);
			else{
				
				var pageR = '';
				if (page != null && page != 'undefined' && page != '') {
					pageR = page;
				}
				
				url = host_name + '/' + pageR + '?' + queryString + '&' + gaLinkerParam;	
				obj.href = url;
				}
    }
    return url;
}

var isMobileDevice = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var translateParamsHM = {
	'utm_source':   'pid',
    'utm_campaign': 'c',
    'combid':       'af_siteid',
    'dp':           'af_sub1',
    'utm_medium':   'af_sub2',
    'utm_content':  'af_sub3'
};

function getTranslatedQueryByMarketingDictionary(){
	var query = window.location.search.substring(1);
	var result = "";
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
        var key = translateParamsHM[pair[0]];
        if (key != null) {
        	result += key + '=' + pair[1] + '&';
        } else if (pair[0] != 's' && pair[0] != 'S') {
        	result += vars[i] + '&';
        }
	}
	return result.substring(0, result.length - 1);
}