/*********** landingPageFullAfterLoad.js ************/

var year = document.getElementById("birthYear");	
var month = document.getElementById("birthMonth");	
var day = document.getElementById("birthDay");	
var countries = document.getElementById("countries");
var countryCode = geoplugin_countryCode();
var i = 1, t = 1;

//BirthYear.
for (t = 1995; t >= 1920; t--, i++) {
	year.options[i] = new Option(t.toString());
	year.options[i].value = t.toString();
}
//BirthMonth.
for (i = 1; i <= 12; i++) {
	month.options[i] = new Option(i.toString());
	month.options[i].value = i.toString();
}	
//BirthDay.
for (i = 1; i <= 31; i++) {
	day.options[i] = new Option(i.toString());
	day.options[i].value = i.toString();
}	
//List of countries.

var countriesList = {			
						2:{country_name:"阿富汗",A2:"AF"},
						3:{country_name:"阿尔巴尼亚",A2:"AL"},
						4:{country_name:"阿尔及利亚",A2:"DZ"},
						5:{country_name:"美属萨摩亚",A2:"AS"},
						6:{country_name:"安道尔",A2:"AD"},
						7:{country_name:"安哥拉",A2:"AO"},
						8:{country_name:"安圭拉",A2:"AI"},
						9:{country_name:"南极洲",A2:"AQ"},
						10:{country_name:"安提瓜和巴布达",A2:"AG"},
						11:{country_name:"阿根廷",A2:"AR"},
						12:{country_name:"亚美尼亚",A2:"AM"},
						13:{country_name:"阿鲁巴",A2:"AW"},
						14:{country_name:"澳大利亚",A2:"AU"},
						15:{country_name:"奥地利",A2:"AT"},
						16:{country_name:"阿塞拜疆",A2:"AZ"},
						17:{country_name:"巴哈马",A2:"BS"},
						18:{country_name:"巴林",A2:"BH"},
						19:{country_name:"孟加拉国",A2:"BD"},
						20:{country_name:"巴巴多斯",A2:"BB"},
						21:{country_name:"白俄罗斯",A2:"BY"},
						22:{country_name:"比利时",A2:"BE"},
						23:{country_name:"伯利兹",A2:"BZ"},
						24:{country_name:"贝宁",A2:"BJ"},
						25:{country_name:"百慕大",A2:"BM"},
						26:{country_name:"不丹",A2:"BT"},
						27:{country_name:"玻利维亚",A2:"BO"},
						28:{country_name:"波斯尼亚及赫塞哥维纳",A2:"BA"},
						29:{country_name:"博茨瓦纳",A2:"BW"},
						30:{country_name:"布维岛",A2:"BV"},
						31:{country_name:"巴西",A2:"BR"},
						32:{country_name:"汶莱",A2:"BN"},
						33:{country_name:"保加利亚",A2:"BG"},
						34:{country_name:"布基纳法索",A2:"BF"},
						35:{country_name:"布隆迪",A2:"BI"},
						36:{country_name:"柬埔寨",A2:"KH"},
						37:{country_name:"喀麦隆",A2:"CM"},
						38:{country_name:"加拿大",A2:"CA"},
						39:{country_name:"佛得角",A2:"CV"},
						40:{country_name:"开曼群岛",A2:"KY"},
						41:{country_name:"中部非洲",A2:"CF"},
						42:{country_name:"乍得",A2:"TD"},
						43:{country_name:"智利",A2:"CL"},
						44:{country_name:"中国",A2:"CN"},
						45:{country_name:"圣诞岛",A2:"CX"},
						46:{country_name:"科科斯群岛",A2:"CC"},
						47:{country_name:"哥伦比亚",A2:"CO"},
						48:{country_name:"科摩罗",A2:"KM"},
						49:{country_name:"刚果",A2:"CG"},
						50:{country_name:"库克群岛",A2:"CK"},
						51:{country_name:"哥斯达黎加",A2:"CR"},
						53:{country_name:"克罗地亚",A2:"HR"},
						54:{country_name:"古巴",A2:"CU"},
						55:{country_name:"塞浦路斯",A2:"CY"},
						56:{country_name:"捷克共和国",A2:"CZ"},
						57:{country_name:"丹麦",A2:"DK"},
						58:{country_name:"吉布提",A2:"DJ"},
						59:{country_name:"多米尼克",A2:"DM"},
						60:{country_name:"多米尼加共和国",A2:"DO"},
						61:{country_name:"东帝汶",A2:"TP"},
						62:{country_name:"厄瓜多尔",A2:"EC"},
						63:{country_name:"埃及",A2:"EG"},
						64:{country_name:"萨尔瓦多",A2:"SV"},
						65:{country_name:"赤道几内亚",A2:"GQ"},
						66:{country_name:"厄立特里亚",A2:"ER"},
						67:{country_name:"爱沙尼亚",A2:"EE"},
						68:{country_name:"埃塞俄比亚",A2:"ET"},
						69:{country_name:"福克兰群岛",A2:"FK"},
						70:{country_name:"法罗群岛",A2:"FO"},
						71:{country_name:"斐济",A2:"FJ"},
						72:{country_name:"芬兰",A2:"FI"},
						73:{country_name:"法国",A2:"FR"},
						74:{country_name:"法属圭亚那",A2:"GF"},
						75:{country_name:"法属波利尼西亚",A2:"PF"},
						77:{country_name:"加蓬",A2:"GA"},
						78:{country_name:"冈比亚",A2:"GM"},
						79:{country_name:"格鲁吉亚",A2:"GE"},
						80:{country_name:"德国",A2:"DE"},
						81:{country_name:"加纳",A2:"GH"},
						82:{country_name:"直布罗陀",A2:"GI"},
						83:{country_name:"希腊",A2:"GR"},
						84:{country_name:"格陵兰",A2:"GL"},
						85:{country_name:"格林纳达",A2:"GD"},
						86:{country_name:"瓜德罗普岛",A2:"GP"},
						87:{country_name:"关岛",A2:"GU"},
						88:{country_name:"危地马拉",A2:"GT"},
						89:{country_name:"几内亚",A2:"GN"},
						90:{country_name:"圭亚那",A2:"GY"},
						91:{country_name:"海地",A2:"HT"},
						93:{country_name:"洪都拉斯",A2:"HN"},
						94:{country_name:"香港",A2:"HK"},
						95:{country_name:"匈牙利",A2:"HU"},
						96:{country_name:"冰岛",A2:"IS"},
						97:{country_name:"印度",A2:"IN"},
						98:{country_name:"印尼",A2:"ID"},
						100:{country_name:"伊拉克",A2:"IQ"},
						101:{country_name:"爱尔兰",A2:"IE"},
						102:{country_name:"意大利",A2:"IT"},
						52:{country_name:"象牙海岸",A2:"CI"},
						103:{country_name:"牙买加",A2:"JM"},
						104:{country_name:"日本",A2:"JP"},
						105:{country_name:"约旦",A2:"JO"},
						106:{country_name:"哈萨克斯坦",A2:"KZ"},
						107:{country_name:"肯尼亚",A2:"KE"},
						108:{country_name:"基里巴斯",A2:"KI"},
						109:{country_name:"科威特",A2:"KW"},
						110:{country_name:"吉尔吉斯斯坦",A2:"KG"},
						111:{country_name:"老挝",A2:"LA"},
						112:{country_name:"拉脱维亚",A2:"LV"},
						113:{country_name:"黎巴嫩",A2:"LB"},
						114:{country_name:"莱索托",A2:"LS"},
						115:{country_name:"利比里亚",A2:"LR"},
						116:{country_name:"利比亚",A2:"LY"},
						117:{country_name:"列支敦士登",A2:"LI"},
						118:{country_name:"立陶宛",A2:"LT"},
						119:{country_name:"卢森堡",A2:"LU"},
						120:{country_name:"澳门",A2:"MO"},
						121:{country_name:"马其顿",A2:"MK"},
						122:{country_name:"马达加斯加",A2:"MG"},
						123:{country_name:"马拉维",A2:"MW"},
						124:{country_name:"马来西亚",A2:"MY"},
						125:{country_name:"马尔代夫",A2:"MV"},
						126:{country_name:"马里",A2:"ML"},
						127:{country_name:"马耳他",A2:"MT"},
						128:{country_name:"元帅群岛",A2:"MH"},
						129:{country_name:"马提尼克岛",A2:"MQ"},
						130:{country_name:"毛里塔尼亚",A2:"MR"},
						131:{country_name:"毛里求斯",A2:"MU"},
						132:{country_name:"马约特岛",A2:"YT"},
						133:{country_name:"墨西哥",A2:"MX"},
						134:{country_name:"密克罗尼西亚",A2:"FM"},
						135:{country_name:"摩尔多瓦",A2:"MD"},
						136:{country_name:"摩纳哥",A2:"MC"},
						137:{country_name:"蒙古",A2:"MN"},
						237:{country_name:"黑山",A2:"ME"},
						138:{country_name:"蒙特塞拉特",A2:"MS"},
						139:{country_name:"摩洛哥",A2:"MA"},
						140:{country_name:"莫桑比克",A2:"MZ"},
						141:{country_name:"缅甸",A2:"MM"},
						142:{country_name:"纳米比亚",A2:"NA"},
						143:{country_name:"瑙鲁",A2:"NR"},
						144:{country_name:"尼泊尔",A2:"NP"},
						145:{country_name:"荷兰",A2:"NL"},
						146:{country_name:"荷属安的列斯",A2:"AN"},
						147:{country_name:"新喀里多尼亚",A2:"NC"},
						148:{country_name:"新西兰",A2:"NZ"},
						149:{country_name:"尼加拉瓜",A2:"NI"},
						150:{country_name:"尼日尔",A2:"NE"},
						151:{country_name:"尼日利亚",A2:"NG"},
						152:{country_name:"纽埃",A2:"NU"},
						153:{country_name:"诺福克岛",A2:"NF"},
						154:{country_name:"朝鲜",A2:"KR"},
						156:{country_name:"挪威",A2:"NO"},
						157:{country_name:"阿曼",A2:"OM"},
						158:{country_name:"巴基斯坦",A2:"PK"},
						159:{country_name:"帕劳",A2:"PW"},
						160:{country_name:"巴拿马",A2:"PA"},
						161:{country_name:"巴布亚新几内亚",A2:"PG"},
						162:{country_name:"巴拉圭",A2:"PY"},
						163:{country_name:"秘鲁",A2:"PE"},
						164:{country_name:"菲律宾",A2:"PH"},
						165:{country_name:"皮特凯恩",A2:"PN"},
						166:{country_name:"波兰",A2:"PL"},
						167:{country_name:"葡萄牙",A2:"PT"},
						168:{country_name:"波多黎各",A2:"PR"},
						169:{country_name:"卡塔尔",A2:"QA"},
						170:{country_name:"留尼旺岛",A2:"RE"},
						171:{country_name:"罗马尼亚",A2:"RO"},
						172:{country_name:"俄罗斯",A2:"RU"},
						173:{country_name:"卢旺达",A2:"RW"},
						194:{country_name:"海伦娜",A2:"SH"},
						174:{country_name:"圣基茨和尼维斯",A2:"KN"},
						175:{country_name:"圣卢西亚",A2:"LC"},
						177:{country_name:"萨摩亚",A2:"WS"},
						178:{country_name:"圣马力诺",A2:"SM"},
						180:{country_name:"沙特阿拉伯",A2:"SA"},
						181:{country_name:"塞内加尔",A2:"SN"},
						236:{country_name:"塞尔维亚",A2:"RS"},
						182:{country_name:"塞舌尔",A2:"SC"},
						183:{country_name:"塞拉利昂",A2:"SL"},
						184:{country_name:"新加坡",A2:"SG"},
						185:{country_name:"斯洛伐克",A2:"SK"},
						186:{country_name:"斯洛文尼亚",A2:"SI"},
						187:{country_name:"所罗门群岛",A2:"SB"},
						188:{country_name:"索马里",A2:"SO"},
						189:{country_name:"南非",A2:"ZA"},
						191:{country_name:"韩国",A2:"KP"},
						192:{country_name:"西班牙",A2:"ES"},
						193:{country_name:"斯里兰卡",A2:"LK"},
						196:{country_name:"苏丹",A2:"SD"},
						197:{country_name:"苏里南",A2:"SR"},
						199:{country_name:"斯威士兰",A2:"SZ"},
						200:{country_name:"瑞典",A2:"SE"},
						201:{country_name:"瑞士",A2:"CH"},
						202:{country_name:"叙利亚",A2:"SY"},
						203:{country_name:"台湾",A2:"TW"},
						204:{country_name:"塔吉克斯坦",A2:"TJ"},
						205:{country_name:"坦桑尼亚",A2:"TZ"},
						206:{country_name:"泰国",A2:"TH"},
						207:{country_name:"多哥",A2:"TG"},
						208:{country_name:"托克劳",A2:"TK"},
						209:{country_name:"汤加群岛",A2:"TO"},
						210:{country_name:"特里尼达和多巴哥",A2:"TT"},
						211:{country_name:"突尼斯",A2:"TN"},
						212:{country_name:"土耳其",A2:"TR"},
						213:{country_name:"土库曼斯坦",A2:"TM"},
						215:{country_name:"图瓦卢",A2:"TV"},
						216:{country_name:"乌干达",A2:"UG"},
						217:{country_name:"乌克兰",A2:"UA"},
						218:{country_name:"阿拉伯联合酋长国",A2:"AE"},
						219:{country_name:"英国",A2:"GB"},
						220:{country_name:"美国",A2:"US"},
						221:{country_name:"乌拉圭",A2:"UY"},
						222:{country_name:"乌兹别克斯坦",A2:"UZ"},
						223:{country_name:"瓦努阿图",A2:"VU"},
						224:{country_name:"梵蒂冈",A2:"VA"},
						225:{country_name:"委内瑞拉",A2:"VE"},
						226:{country_name:"越南",A2:"VN"},
						227:{country_name:"英国维尔京群岛",A2:"VG"},
						228:{country_name:"美国维尔京群岛",A2:"VI"},
						230:{country_name:"西撒哈拉",A2:"EH"},
						231:{country_name:"也门",A2:"YE"},
						233:{country_name:"扎伊尔",A2:"ZR"},
						234:{country_name:"赞比亚",A2:"ZM"},
						235:{country_name:"津巴布韦",A2:"ZW"}							
					};
i = 0;					
for (country in countriesList) {
	countries.options[i] = new Option(countriesList[country].country_name);
	countries.options[i].value = country;
	i++;			
}
var countryId;
var countryFounded = false;
var selectCountryIndex = 0;
for (country in countriesList) {
	if (countriesList[country].A2 == countryCode) {
		countryId = country;
		countryFounded = true;
		break;
	}
	selectCountryIndex++;					
}	

//if country code is not in the list, select by default chinese country.
countries.selectedIndex = 42;
//Country code found in the list
if (countryFounded) {
	countries.selectedIndex = selectCountryIndex;
}
	
// Show the information pop up.
function showInfo(obj) {
	child = obj.childNodes;
	for(i = 0 ;i < child.length; i++) {
		if (child[i].id == "info_box") {
			child[i].className ="info_box show";
		}
	}
}

// Hide the information pop up.
function hideInfo(obj) {
	child = obj.childNodes;
	for(i = 0; i < child.length; i++) {
		if (child[i].id== "info_box") {
			child[i].className="info_box";
		}
	}
}

// anyoption terms
function popitup(url) {
	newwindow = window.open(url,'name','scrollbars=yes,resizable=yes,height=900,width=970,top=50,left=50,right=50');
	if (window.focus) {
		newwindow.focus()
	}		
	return false;
}