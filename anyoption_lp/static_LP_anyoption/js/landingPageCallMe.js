/*********** landingPage.js ************/

//constants
var FORM_TYPE_CALL_ME	= 1;
var FORM_TYPE_LIGHTBOX	= 2;

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;
var SKIN_ID_KR 			= 17;
var SKIN_ID_NL 			= 23;
var SKIN_ID_SV 			= 24;

var COMB_ID_EN_DEFUALT	= 22;

//error messages
var name_mandatory;
var name_letters;
var email_mandatory;
var email_invalid;
var mobile_mandatory;
var mobile_invalid_number;
var mobile_min_length;
var letters_regexp;
var form_type = FORM_TYPE_CALL_ME;
document.domain = "anyoption.com";

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
var dp = uri.queryKey['dp'];
if (null == skinId || 'undefined' == skinId) {
	skinId = SKIN_ID_EN;
}
if (null == combId ||  'undefined' == combId) {
	combId = COMB_ID_EN_DEFUALT;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var validate_fields = true;
var err_field;
var langCode;
var language;
var countryCode;
var phoneCode;
var defualtPhoneCode;
setLangCodeBySkinId(skinId);

// create link redirect to call me page
function redirectCallMe(obj, locale) {
	obj.href = 'call_' + locale + '.shtml?' + queryString;
}

// get lang code by skinId
function setLangCodeBySkinId(skinId) {
	//set default
	language = "English";
	//error messages
	name_mandatory = "Name: is mandatory field";
	name_letters	 = "Name: Please enter letters only";
	email_mandatory 	 = "Email is mandatory field";
	email_invalid		 = "Email address is invalid";
	mobile_mandatory	 = "Mobile is mandatory field";
	mobile_invalid_num	 = "Mobile: Invalid number";
	mobile_min_length 	 = "Mobile is shorter than the allowed minimum of 7 characters";
	letters_regexp 		 = "^['a-zA-Z ]+$";
	defualtPhoneCode	 = "GB";
	switch (Number(skinId)) {
		case SKIN_ID_EN:
		case SKIN_ID_EN_US:
			language = "English";
			name_mandatory = "Name: is mandatory field";
			name_letters	 = "Name: Please enter letters only";
			email_mandatory 	 = "Email is mandatory field";
			email_invalid		 = "Email address is invalid";
			mobile_mandatory	 = "Mobile is mandatory field";
			mobile_invalid_num	 = "Mobile: Invalid number";
			mobile_min_length 	 = "Mobile is shorter than the allowed minimum of 7 characters";
			letters_regexp 		 = "^['a-zA-Z ]+$";
			defualtPhoneCode 	 = "GB";
			break;
		case SKIN_ID_TR:
			language = "Turkish";
			name_mandatory = "AD: Zorunlu alan";
			name_letters	 = "AD: Lütfen sadece harf kullanın";
			email_mandatory 	 = "e-posta Zorunlu alan";
			email_invalid		 = "E-posta adresi geçersiz";
			mobile_mandatory	 = "Cep telefonu Zorunlu alan";
			mobile_invalid_num	 = "Cep telefonu: Validasyon HatasÄ±";
			mobile_min_length 	 = "Cep telefonu, izin verilen minimum 7 karakterden daha kÄ±sa";
			letters_regexp 		 = "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$";
			defualtPhoneCode 	 = "TR";
			break;
		case SKIN_ID_AR:
			language = "English";
			name_mandatory = "الاسم: هو حقل إلزامي";
			name_letters	 = "الاسم: الرجاء إدخال حروف فقط";
			email_mandatory 	 = "البريد الإلكتروني خطأ التأكيد";
			email_invalid		 = "عنوان البريد الإلكتروني غير صالح";
			mobile_mandatory	 = "الهاتف المحمول خطأ التأكيد";
			mobile_invalid_num	 = "الهاتف المحمول: خطأ التأكيد";
			mobile_min_length 	 = "الهاتف المحمول أقصر من الحد الأدنى المسموح به وهو 7 حرفاً";
			letters_regexp 		 = "^['a-zA-Z ]+$";
			defualtPhoneCode 	 = "SA";
			break;
		case SKIN_ID_ES:
		case SKIN_ID_ES_US:
			language = "Spanish";
			name_mandatory = "Nombre: es un campo obligatorio";
			name_letters	 = "Nombre: Por favor, utilice letras solamente";
			email_mandatory 	 = "email Campo obligatorio";
			email_invalid		 = "El correo electrónico no es válido";
			mobile_mandatory	 = "Móvil Campo obligatorio";
			mobile_invalid_num	 = "Móvil Error de validación";
			mobile_min_length 	 = "Móvil es más corto que el mínimo permitido de 7 caracteres";
			letters_regexp 		 = "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$";
			defualtPhoneCode 	 = "ES";
			break;
		case SKIN_ID_DE:
			language = "German";
			name_mandatory = "Vorname: ist ein Pflichtfeld";
			name_letters	 = "Vorname: Bitte benutzen Sie nur Buchstaben";
			email_mandatory 	 = "E-Mail Pflichtfeld";
			email_invalid		 = "E-Mail Adresse ist ungültig";
			mobile_mandatory	 = "Mobiltelefon Pflichtfeld";
			mobile_invalid_num	 = "Mobiltelefon: Überprüfungsfehler";
			mobile_min_length 	 = "Mobiltelefon muss mindestens 7 Zeichen lang sein";
			letters_regexp 		 = "^['a-zA-ZäÄöÖüÜß ]+$";
			defualtPhoneCode 	 = "DE";
			break;
		case SKIN_ID_IT:
			language = "Italian";
			name_mandatory = "Nome: è un campo obbligatorio";
			name_letters	 = "Nome: Inserite solo lettere";
			email_mandatory 	 = "e-mail Campo obbligatorio";
			email_invalid		 = "indirizzo e-mail non valido";
			mobile_mandatory	 = "Cellulare Campo obbligatorio";
			mobile_invalid_num	 = "Cellulare: Errore di convalida";
			mobile_min_length 	 = "Cellulare è più corto del valore minimo consentito di 7 caratteri";
			letters_regexp 		 = "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$";
			defualtPhoneCode 	 = "IT";
			break;
		case SKIN_ID_RU:
			language = "Russian";
			name_mandatory = "ИМЯ: поле, обязательное для заполнения";
			name_letters	 = "ИМЯ: Вводите только буквы";
			email_mandatory 	 = "Электронная почта Обязательное поле";
			email_invalid		 = "Неверный адрес эл. почты";
			mobile_mandatory	 = "Мобильный телефон Обязательное поле";
			mobile_invalid_num	 = "Мобильный телефон Ошибка валидации";
			mobile_min_length 	 = "Мобильный телефон короче допустимой длины 7 символов";
			letters_regexp 		 = "^['a-zA-ZА-Яа-я ]+$";
			defualtPhoneCode 	 = "RU";
			break;
		case SKIN_ID_FR:
			language = "French";
			name_mandatory = "Prénom: est un champ obligatoire";
			name_letters	 = "Prénom: Veuillez saisir des lettres uniquement";
			email_mandatory 	 = "Email est un champ obligatoire";
			email_invalid		 = "Adresse Email incorrecte";
			mobile_mandatory	 = "Téléphone portable est un champ obligatoire";
			mobile_invalid_num	 = "Téléphone portable: Nombre incorrect";
			mobile_min_length 	 = "Téléphone portable est plus court que le minimum autorisé de 7 caractères";
			letters_regexp 		 = "^['a-zA-Zéèàùâêîôûëïç ]+$";
			defualtPhoneCode 	 = "FR";
			break;
		case SKIN_ID_ZH:
			language = "Chinese";
			name_mandatory = "我的名字: 是必填栏";
			name_letters	 = "我的名字: 请只使用字母";
			email_mandatory 	 = "电子邮件 是必填栏";
			email_invalid		 = "无效的电子邮件地址";
			mobile_mandatory	 = "移动电话 是必填栏";
			mobile_invalid_num	 = "移动电话: 无效号码";
			mobile_min_length 	 = "移动电话 短于所允许的最小7个字符";
			letters_regexp 		 = "^['a-zA-Z一-龠 ]+$";
			defualtPhoneCode 	 = "CN";
			break;
		case SKIN_ID_KR:
			language = "Korean";
			name_mandatory = "내 이름: 필수 필드";
			name_letters	 = "내 이름: 문자만 사용하실 수 있습니다";
			email_mandatory 	 = "내 이메일: 필수 필드";
			email_invalid		 = "이메일 주소가 유효하지 않습니다";
			mobile_mandatory	 = "내 전화번호: 필수 필드";
			mobile_invalid_num	 = "내 전화번호: 은행계좌번호는 숫자여야 합니다.";
			mobile_min_length 	 = "내 전화번호: 휴대번호가 최소로 필요한 7 자리의 문자 아래입니다";
			letters_regexp 		 = "^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$";
			defualtPhoneCode 	 = "KR";
			break;
		case SKIN_ID_NL:
			language = "Netherlands";
			name_mandatory = "Naam: is verplicht veld";
			name_letters	 = "Naam: alleen letters invullen";
			email_mandatory 	 = "E-mail is verplicht veld";
			email_invalid		 = "E-mailadres is ongeldig";
			mobile_mandatory	 = "Mobiel is verplicht veld";
			mobile_invalid_num	 = "Mobiel: ongeldig nummer";
			mobile_min_length 	 = "Mobiel is korter dan de toegestane 7 tekens";
			letters_regexp 		 = "/^['a-zA-Zéëï ]+$/";
			defualtPhoneCode 	 = "NL";
			break;
		case SKIN_ID_SV:
			language = "Svenska";
			name_mandatory = "Namn: är obligatoriskt fält";
			name_letters	 = "Namn:  Ange endast bokstäver";
			email_mandatory 	 = "E-post är obligatoriskt fält";
			email_invalid		 = "E-postadressen är ogiltig";
			mobile_mandatory	 = "Mobilnummer är obligatoriskt fält";
			mobile_invalid_num	 = "Mobil:  Ogiltigt nummer";
			mobile_min_length 	 = "Mobilnumret är kortare än tillåtet minimum på 7 tecken";
			letters_regexp 		 = "/^['a-zA-ZÅÄÖåäö ]+$/";
			defualtPhoneCode 	 = "SE";
			break;
	}
}

//Phone code keyDown
// Allow tab key
function phoneKeys(e) {
	var keynum;
	if(window.event) { // IE
	  keynum = e.keyCode;
 	} else if(e.which) {  // Netscape/Firefox/Opera
	  keynum = e.which;
    }
	if (keynum != 9) {  // tab key
		return false;
  	}
}



//Phone code prefix by country code
var countriesPhone = {  "AD":376,
						"AE":971,
						"AF":93,
						"AG":1286,
						"AI":1265,
						"AL":355,
						"AM":374,
						"AN":599,
						"AO":244,
						"AQ":672,
						"AR":54,
						"AS":684,
						"AT":43,
						"AU":61,
						"AW":297,
						"AZ":994,
						"BA":387,
						"BB":1246,
						"BD":880,
						"BE":32,
						"BF":226,
						"BG":359,
						"BH":973,
						"BI":257,
						"BJ":229,
						"BM":1441,
						"BN":673,
						"BO":591,
						"BR":55,
						"BS":1242,
						"BT":975,
						"BV":0,
						"BW":267,
						"BY":375,
						"BZ":501,
						"CA":1,
						"CC":61,
						"CF":236,
						"CG":242,
						"CH":41,
						"CI":225,
						"CK":682,
						"CL":56,
						"CM":237,
						"CN":86,
						"CO":57,
						"CR":506,
						"CU":53,
						"CV":238,
						"CX":61,
						"CY":357,
						"CZ":420,
						"DE":49,
						"DJ":253,
						"DK":45,
						"DM":1767,
						"DO":1809,
						"DZ":213,
						"EC":593,
						"EE":372,
						"EG":20,
						"EH":0,
						"ER":291,
						"ES":34,
						"ET":251,
						"FI":358,
						"FJ":679,
						"FK":500,
						"FM":691,
						"FO":298,
						"FR":33,
						"GA":241,
						"GB":44,
						"GD":1473,
						"GE":995,
						"GF":594,
						"GH":233,
						"GI":350,
						"GL":299,
						"GM":220,
						"GN":224,
						"GP":590,
						"GQ":240,
						"GR":30,
						"GT":502,
						"GU":1671,
						"GY":592,
						"HK":852,
						"HN":504,
						"HR":385,
						"HT":509,
						"HU":36,
						"ID":62,
						"IE":353,
						"IL":972,
						"IN":91,
						"IQ":964,
						"IR":98,
						"IS":354,
						"IT":39,
						"JM":1876,
						"JO":962,
						"JP":81,
						"KE":254,
						"KG":996,
						"KH":855,
						"KI":686,
						"KM":269,
						"KN":1869,
						"KP":850,
						"KR":82,
						"KW":965,
						"KY":1345,
						"KZ":7,
						"LA":856,
						"LB":961,
						"LC":1758,
						"LI":423,
						"LK":94,
						"LR":231,
						"LS":266,
						"LT":370,
						"LU":352,
						"LV":371,
						"LY":218,
						"MA":212,
						"MC":377,
						"MD":373,
						"ME":381,
						"MG":261,
						"MH":692,
						"MK":389,
						"ML":223,
						"MM":95,
						"MN":976,
						"MO":853,
						"MQ":596,
						"MR":222,
						"MS":1664,
						"MT":356,
						"MU":230,
						"MV":960,
						"MW":265,
						"MX":52,
						"MY":60,
						"MZ":258,
						"NA":264,
						"NC":687,
						"NE":227,
						"NF":672,
						"NG":234,
						"NI":505,
						"NL":31,
						"NO":47,
						"NP":977,
						"NR":674,
						"NU":683,
						"NZ":64,
						"OM":968,
						"PA":507,
						"PE":51,
						"PF":689,
						"PG":675,
						"PH":63,
						"PK":92,
						"PL":48,
						"PN":0,
						"PR":1787,
						"PT":351,
						"PW":680,
						"PY":595,
						"QA":974,
						"RE":262,
						"RO":40,
						"RS":381,
						"RU":7,
						"RW":250,
						"SA":966,
						"SB":677,
						"SC":248,
						"SD":249,
						"SE":46,
						"SG":65,
						"SH":290,
						"SI":386,
						"SK":421,
						"SL":232,
						"SM":378,
						"SN":221,
						"SO":252,
						"SR":597,
						"SV":503,
						"SY":963,
						"SZ":268,
						"TD":235,
						"TG":228,
						"TH":66,
						"TJ":992,
						"TK":690,
						"TM":993,
						"TN":216,
						"TO":676,
						"TP":670,
						"TR":90,
						"TT":1868,
						"TV":688,
						"TW":886,
						"TZ":255,
						"UA":380,
						"UG":256,
						"US":1,
						"UY":598,
						"UZ":998,
						"VA":39,
						"VE":58,
						"VG":0,
						"VI":1340,
						"VN":84,
						"VU":678,
						"WS":685,
						"YE":967,
						"YT":269,
						"ZA":27,
						"ZM":260,
						"ZR":243,
						"ZW":263}

function getPhoneCodeByCountry(countryCode){
  return countriesPhone[countryCode];
}

function closeErrorBox(){
	$("#error_box").css("display", "none");
	$("#Overlay").css("display", "none");
    //set focus on the error field
    $(document).ready(function(){
    	err_field.focus();
    });
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

//flash submit
swfobject.registerObject("myId", "9.0.0", "swf/expressInstall.swf");
function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",5000);
	return false;
}

function submitForm() {
	$("#callMeForm").submit();
}


//phone code
function setPhoneCode() {
	if(location.pathname.substring(1).indexOf("call") > 0){
		try {
			countryCode = geoplugin_countryCode();
		} catch (e) {
			countryCode = defualtPhoneCode;
		}
		if (null != countryCode &&  'undefined' != countryCode) {
			countryCode = countryCode.toUpperCase();
		}
		var phoneCode = getPhoneCodeByCountry(countryCode);
		if (phoneCode == null || phoneCode == undefined) {
			phoneCode = 0;
		}
		document.getElementById("mobilePhone").value = phoneCode;
	}
}
function changePhoneCode(obj){
	var e = document.getElementById("countries");
	var country = e.options[e.selectedIndex].value;
	var countryCode = countriesList[country].A2;
	document.getElementById("mobilePhone").value = countriesPhone[countryCode];
}
//display error msg
function display_error(msg) {
	//Remove from all elements the red border
	$(".error").removeClass("error");
	//Fill error msg
	var error_msg = document.getElementById("error_message_label");
	error_msg.innerHTML = msg;
	if (form_type == FORM_TYPE_CALL_ME) { //regular display error
		//Display error msg
		//var box = document.getElementById("error_box");
		//var overlay = document.getElementById("Overlay");
		//box.style.display = "block";
		//overlay.style.display = "block";
		//Move box to the center
		//box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
		//overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
	}
}

//diplay lightbox
function startBtn() {
	//Display error msg
	var box = document.getElementById("lightform");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

function flashStartTrading(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

//close light box
function closeLightBox(){
	document.getElementById("lightform").style.display = 'none';
	document.getElementById("Overlay").style.display = 'none';
}

function setFocusFirstInput() {
	window.scrollTo(0, 0);
	$("#name").focus();
}

function updateDownloadURL() {
	var downloadButton= document.getElementById('download');
	if(downloadButton != null){
		downloadButton.href = getUrlToDownloadApp();
		if( /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
			downloadButton.className = "download_appstore";
		}
	}
}

////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
$(document).ready(function() {
	var downloadButton= document.getElementById('download');
   	if(downloadButton != null){
		downloadButton.href = getUrlToDownloadApp();
		if( /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
			downloadButton.className = "button_appStore download";
		}
	}
	//var callMeButton = document.getElementById('callLink');
	/*if(callMeButton != null){
		callMeButton.href = getLinkForCallScreen();
	}*/
	// START SETTING PHONE CODE
	setPhoneCode();
	//define email expression rule for validation
	$.validator.addMethod('email_regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//define regular expression rule for validation
	$.validator.addMethod('regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//validation implementation will go here.
	$("#callMeForm").validate({
		//if there is no error server validation for email
		submitHandler: function(form) {
			//parameters
		    var writerId = "200"; //mobile
		    var utcOffset = new Date().getTimezoneOffset();
			var request = new Object();
			request.name = document.getElementById("name").value;
			request.email = document.getElementById("email").value;
			request.mobilePhone = document.getElementById("mobilePhone").value;
			request.countryId = document.getElementById("countries").value;
			request.timeFirstVisit = UTF8.decode(get_cookie("landing_firstVisit"));
			request.combinationId = combId;
			request.userAgent = navigator.userAgent;
			request.httpReferer = get_cookie("landing_hr");

			$.ajax({
				url: cdn_url_json + "jsonService/AnyoptionService/insertContact",
				type: "POST",
				dataType: "json",
				contentType: "json",
		     	data: JSON.stringify({
					"skinId"  : skinId,
					"writerId" : writerId,
					"contact" : {
						"skinId"  			: skinId,
						"utcOffset"			: utcOffset,
						"writerId"			: writerId,
						"name"				: request.name,
						"email"				: request.email,
						"phone"				: request.mobilePhone,
						"countryId"			: request.countryId,
						"combId"			: request.combinationId,
						"userAgent"			: request.userAgent,
						"httpReferer"		: request.httpReferer,
						"timeFirstVisitTxt"	:  request.timeFirstVisit,
						"dynamicParameter"  : dp,
						"marketingStaticPart" :get_cookie("smt"),
						"type" 				: FORM_TYPE_CALL_ME,
						"aff_sub1"			: aff_sub1,
						"aff_sub2"			: aff_sub2,
						"aff_sub3"			: aff_sub3
					}
				}),
				success: function(result) {
					if (result.errorCode == 0) {
						if (get_cookie("landing_email") == null) {
							set_cookie("landing_email", UTF8.encode(request.email), 365, '/', cookie_domain, '');
						}

						if (result.contact.marketingStaticPart != null){
							set_cookie("smt", result.contact.marketingStaticPart, 16910, '/', cookie_domain, null);
						}
						
						var redirecrtURL = "success_" + getLangCodeBySkinId(skinId) + ".shtml?" + queryString;
						$(location).attr('href',redirecrtURL);
					} else { //error found
						showError(result);
					}
				},
				error: function(jqXHR, exception) {
					/*if (jqXHR.status === 0) {
						alert('Not connect.\n Verify Network.' + jqXHR.responseText + " " + exception);
					} else if (jqXHR.status == 404) {
						alert('Requested page not found. [404]' + jqXHR.responseText + " " + exception);
					} else if (jqXHR.status == 500) {
						alert('Internal Server Error [500].' + jqXHR.responseText + " " + exception);
					} else if (exception === 'parsererror') {
						alert('Requested JSON parse failed.' + jqXHR.responseText + " " + exception);
					} else if (exception === 'timeout') {
						alert('Time out error.' + jqXHR.responseText + " " + exception);
					} else if (exception === 'abort') {
						alert('Ajax request aborted.' + jqXHR.responseText + " " + exception);
					} else {
						alert('Uncaught Error.\n' + jqXHR.responseText);
					}*/
				},
				timeout: 10000
		    });
        },
     	errorPlacement: function(error, element) {
			if (validate_fields) {
				validate_fields = false;
				err_field = element[0];
				display_error(error[0].innerHTML);
				$(element[0].parentNode).addClass( "error" );
			}
		},
		onfocusout: false, 
		onBlur: false,
		rules: {
			name: {
				required: true,
				regexp: letters_regexp
			},
			email: {
				required: true,
				email: true,
				email_regexp: "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$"
			},
			mobilePhone: {
				required: true,
				number: true,
				minlength: 7
			}
		},
		messages: {
			name: {
				required: name_mandatory,
				regexp: name_letters
			},
			email: {
				required: email_mandatory,
				email: email_invalid,
				email_regexp: email_invalid
			},
			mobilePhone: {
				required: mobile_mandatory,
				number: mobile_invalid_num,
				minlength: mobile_min_length
			}
		}
	});
})
/* END VALIDATION */

function showError(result) {
	if (null != result.userMessages) {
		var message = result.userMessages[0].message;
		var field = result.userMessages[0].field;
	}
	err_field = document.getElementById(field);
	display_error(message);
	$(err_field.parentNode).addClass( "error" );
}


function getUrlToDownloadApp(){
	var appStoreUrl = "http://itunes.apple.com/us/app/anyoption/id403987356?mt=8";
	var googlePlayUrl = 'market://details?id=com.anyoption.android.app&referrer=';
	var zh91Url = 'http://apk.91.com/Soft/Android/com.anyoption.android.app-19-6.html';
	var temp = replaceall(queryString, '=', '%3D');
	var temp2 = replaceall(temp, '&', '%26');
	var midParam = '%26mid%3D' + getMid();
	var url = "";
	if( /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		url = appStoreUrl + "&" + queryString;
	} else {
		if (SKIN_ID_ZH == skinId) {
			url = zh91Url;
		} else {
			url = googlePlayUrl + temp2 + midParam;
		}	
	}
	return url;
}

function replaceall(str,replace,with_this) {
    var str_hasil ="";
    var temp;

    for(var i=0;i<str.length;i++) {// not need to be equal. it causes the last change: undefined..
        if (str[i] == replace) {
            temp = with_this;
        } else {
                temp = str[i];
        }

        str_hasil += temp;
    }

    return str_hasil;
}

function getLinkForCallScreen() {
	var url = "call_" + getLangCodeBySkinId(skinId) + ".shtml?s=" + skinId;
	if(dp != null) {
		url += "&=" + dp;
	}
	 return url;
}

//get langCode by skin
function getLangCodeBySkinId(skinId) {
	//set default
	langCode = "en";
	switch (Number(skinId)) {
		case 2:
		case 13:
			langCode = "en";
			break;
		case 3:
			langCode = "tr";
			break;
		case 4:
			langCode = "ar";
			break;
		case 5:
		case 14:
			langCode = "es";
			break;
		case 8:
			langCode = "de";
			break;
		case 9:
			langCode = "it";
			break;
		case 10:
			langCode = "ru";
			break;
		case 12:
			langCode = "fr";
			break;
		case 15:
			langCode = "zh";
			break;
		case 17:
			langCode = "ko";//korean - might be changed
			break;
		case 23:
			langCode = "nl";
			break;
		case 24:
			langCode = "sv";
			break;
	}
	return langCode;
}