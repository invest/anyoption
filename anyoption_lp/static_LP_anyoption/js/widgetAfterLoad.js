//get url
var fullURL = document.URL;
var uri = parseUri(fullURL);
//url parameter h1
var h1 = uri.queryKey['h1'];
var output;
if (h1 != null) {
	 output = decodeURIComponent(h1);
	 document.getElementById('header').innerHTML = output;
}
//url parameter h2
var h2 = uri.queryKey['h2'];
if (h2 != null) {
	 output = decodeURIComponent(h2);
	 document.getElementById('footer').innerHTML = output;
}	

function getIframeContent(width, height) {
	//skin parameter
	var skinId = uri.queryKey['s'];
	document.getElementById('content').src = host_url + 'jsp/widget/widget' + width +  'x' + height + '.jsf?s=' + skinId;
}