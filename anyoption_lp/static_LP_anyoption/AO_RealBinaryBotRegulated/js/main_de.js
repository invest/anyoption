$(".buttons li").click(function () {
    // ie does not support console.log
	//console.log('1');
	var slideID = $(this).attr("data-slide");

	$(".slide").hide();
	$(".slide"+slideID).show();

    //console.log(slideID)

	$(".buttons li.active").removeClass("active");
	$(this).addClass("active");
})

var ticks = [
	{
		text: "Erledigt die Arbeit, leicht zu verwenden und schnell einzurichten.",
		starsClass: "stars four"
	},

	{
		text: "Sehr gutes Tool für die technische Analyse. Ich freue mich auf mehr Anlagegüter.",
		starsClass: "stars four"
	},

    {
		text: "Ich liebe den Real Binary Bot - Ich verwende ihn bei JEDEM Handel, den ich durchführe!",
		starsClass: "stars"
	},

	{
		text: "Endlich einen streamenden Feed, mit dem ich Live-Kursnotierungen kostenlos sehen kann!",
		starsClass: "stars"
	},

	{
		text: "Das Tool mit der besten technischen Analyse, das ich bisher gefunden habe.",
		starsClass: "stars four"
	}

]

var lastTick = -1;
var changeTicker = function () {
	if (lastTick == ticks.length-1) {
		lastTick = 0;
	} else {
 		lastTick++;
	}

 	var ticker = $(".ticker"),
 		newTick = ticks[lastTick];

 	ticker.animate({opacity:0}, function () {
 		ticker.find('.stars').attr('class', newTick['starsClass']);
 		ticker.find('.text').text(newTick['text']);
 		$(this).animate({opacity:1});
 	});
}

setInterval(changeTicker, 3000);

$(document).ready(function () {
     changeTicker();
});



var openOverlay = function (text) {
	$('.overlay').show();
	$('.whiteBox .frame').hide()
	$('.whiteBox').show();
	$('.whiteBox .frame[data-id="'+text+'"]').show();
	$('.whiteBox').css({
		left:$(window).width()/2-960/2,
		top: $(window).scrollTop()+50
	});
}
$(document).click(function (e) {
	if(!$(e.target).is('.whiteBox, .whiteBox *, [data-frame]')){
		$('.overlay').hide();
		$(".whiteBox").hide();
	}
})

$('[data-frame]').click(function () {
	openOverlay($(this).attr("data-frame"));
})

var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};