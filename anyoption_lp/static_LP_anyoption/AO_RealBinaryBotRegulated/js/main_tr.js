$(".buttons li").click(function () {
    // ie does not support console.log
	//console.log('1');
	var slideID = $(this).attr("data-slide");

	$(".slide").hide();
	$(".slide"+slideID).show();

    //console.log(slideID)

	$(".buttons li.active").removeClass("active");
	$(this).addClass("active");
})

var ticks = [
	{
		text: "İş görüyor, kullanımı kolay ve kurulumu hızlı.",
		starsClass: "stars four"
	},

	{
		text: "Teknik analiz için çok iyi bir yatırım aracı, daha fazla aktif için sabırsızlanıyorum.",
		starsClass: "stars four"
	},

    {
		text: "Gerçek İkili Botu seviyorum - Yaptığım HER yatırımda kullanıyorum!",
		starsClass: "stars"
	},

	{
		text: "Sonunda canlı fiyatları ücretsiz görebileceğim muthiş bir kaynak!",
		starsClass: "stars"
	},

	{
		text: "Şu ana kadar gördüğüm en iyi teknik analiz aracı.",
		starsClass: "stars four"
	}

]

var lastTick = -1;
var changeTicker = function () {
	if (lastTick == ticks.length-1) {
		lastTick = 0;
	} else {
 		lastTick++;
	}

 	var ticker = $(".ticker"),
 		newTick = ticks[lastTick];

 	ticker.animate({opacity:0}, function () {
 		ticker.find('.stars').attr('class', newTick['starsClass']);
 		ticker.find('.text').text(newTick['text']);
 		$(this).animate({opacity:1});
 	});
}

setInterval(changeTicker, 3000);

$(document).ready(function () {
     changeTicker();
});



var openOverlay = function (text) {
	$('.overlay').show();
	$('.whiteBox .frame').hide()
	$('.whiteBox').show();
	$('.whiteBox .frame[data-id="'+text+'"]').show();
	$('.whiteBox').css({
		left:$(window).width()/2-960/2,
		top: $(window).scrollTop()+50
	});
}
$(document).click(function (e) {
	if(!$(e.target).is('.whiteBox, .whiteBox *, [data-frame]')){
		$('.overlay').hide();
		$(".whiteBox").hide();
	}
})

$('[data-frame]').click(function () {
	openOverlay($(this).attr("data-frame"));
})

var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};