$(".buttons li").click(function () {
    // ie does not support console.log
	//console.log('1');
	var slideID = $(this).attr("data-slide");

	$(".slide").hide();
	$(".slide"+slideID).show();

    //console.log(slideID)

	$(".buttons li.active").removeClass("active");
	$(this).addClass("active");
})

var ticks = [
	{
		text: "Fa il suo lavoro, è facile da usare e veloce da impostare.",
		starsClass: "stars four"
	},

	{
		text: "Un ottimo strumento per l'analisi tecnica, che permette di concentrarsi su più attività.",
		starsClass: "stars four"
	},

    {
		text: "Adoro Real Binary Bot - Lo uso per ogni operazione che faccio!",
		starsClass: "stars"
	},

	{
		text: "Finalmente un feed in streaming dove posso vedere le quotazioni in tempo reale e gratis!",
		starsClass: "stars"
	},

	{
		text: "Il migliore strumento di analisi tecnica con cui abbia avuto a che fare.",
		starsClass: "stars four"
	}

]

var lastTick = -1;
var changeTicker = function () {
	if (lastTick == ticks.length-1) {
		lastTick = 0;
	} else {
 		lastTick++;
	}

 	var ticker = $(".ticker"),
 		newTick = ticks[lastTick];

 	ticker.animate({opacity:0}, function () {
 		ticker.find('.stars').attr('class', newTick['starsClass']);
 		ticker.find('.text').text(newTick['text']);
 		$(this).animate({opacity:1});
 	});
}

setInterval(changeTicker, 3000);

$(document).ready(function () {
     changeTicker();
});



var openOverlay = function (text) {
	$('.overlay').show();
	$('.whiteBox .frame').hide()
	$('.whiteBox').show();
	$('.whiteBox .frame[data-id="'+text+'"]').show();
	$('.whiteBox').css({
		left:$(window).width()/2-960/2,
		top: $(window).scrollTop()+50
	});
}
$(document).click(function (e) {
	if(!$(e.target).is('.whiteBox, .whiteBox *, [data-frame]')){
		$('.overlay').hide();
		$(".whiteBox").hide();
	}
})

$('[data-frame]').click(function () {
	openOverlay($(this).attr("data-frame"));
})

var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};