$(".buttons li").click(function () {
    // ie does not support console.log
	//console.log('1');
	var slideID = $(this).attr("data-slide");

	$(".slide").hide();
	$(".slide"+slideID).show();

    //console.log(slideID)

	$(".buttons li.active").removeClass("active");
	$(this).addClass("active");
})

var ticks = [
	{
		text: "Отлично делает свою работу, легок в использовании, быстрая установка.",
		starsClass: "stars four"
	},

	{
		text: "Очень хороший инструмент для технического анализа, жду дополнительных активов.",
		starsClass: "stars four"
	},

    {
		text: "Мне нравится Real Binary Bot - я использую его в каждой сделке!",
		starsClass: "stars"
	},

	{
		text: "Наконец-то потоковый канал, на котором можно бесплатно увидеть котировки в реальном времени!",
		starsClass: "stars"
	},

	{
		text: "Лучший инструмент для технического анализа, который мне удалось найти.",
		starsClass: "stars four"
	}

]

var lastTick = -1;
var changeTicker = function () {
	if (lastTick == ticks.length-1) {
		lastTick = 0;
	} else {
 		lastTick++;
	}

 	var ticker = $(".ticker"),
 		newTick = ticks[lastTick];

 	ticker.animate({opacity:0}, function () {
 		ticker.find('.stars').attr('class', newTick['starsClass']);
 		ticker.find('.text').text(newTick['text']);
 		$(this).animate({opacity:1});
 	});
}

setInterval(changeTicker, 3000);

$(document).ready(function () {
     changeTicker();
});



var openOverlay = function (text) {
	$('.overlay').show();
	$('.whiteBox .frame').hide()
	$('.whiteBox').show();
	$('.whiteBox .frame[data-id="'+text+'"]').show();
	$('.whiteBox').css({
		left:$(window).width()/2-960/2,
		top: $(window).scrollTop()+50
	});
}
$(document).click(function (e) {
	if(!$(e.target).is('.whiteBox, .whiteBox *, [data-frame]')){
		$('.overlay').hide();
		$(".whiteBox").hide();
	}
})

$('[data-frame]').click(function () {
	openOverlay($(this).attr("data-frame"));
})

var downloadURL = function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};