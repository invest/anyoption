/*********** landingPage_full.js ************/

//error messages
var first_name_mandatory;
var first_name_letters;
var last_name_mandatory;
var last_name_letters;
var email_mandatory;
var email_invalid;
var mobile_mandatory;
var mobile_invalid_number;
var mobile_min_length;
var letters_regexp;
//error messages - long form
var phone_invalid_number;
var phone_min_length;
var user_name_mandatory;
var user_name_letters_digits;
var user_name_min_length;
var password_mandatory;
var password_letters_digits;
var password_retype_match;
var password_min_length;
var city_mandatory;
var city_letters;
var birthDay;
var birthMonth;
var birthYear;
var letters_digits_regexp;
var password_retype_mandatory;
var acceptTerms;
var invalidDate;
var ageUnder18;

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
if (null == skinId || 'undefined' == skinId) {
	skinId = 2;
}
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var validate_fields = true;
var err_field;
var langCode;
var language;
var countryCode;
var phoneCode;
setLangCodeBySkinId(skinId);

// get lang code by skinId
function setLangCodeBySkinId(skinId) {
	//set default
	language = "English";
	//error messages
	first_name_mandatory 		= "First name is mandatory field";
	first_name_letters			= "First name: Please enter letters only";
	last_name_mandatory			= "Last name is mandatory field";
	last_name_letters	 		= "Last name: Please enter letters only";
	email_mandatory 	 		= "Email is mandatory field";
	email_invalid		 		= "Email address is invalid";
	mobile_mandatory	 		= "Mobile is mandatory field";
	mobile_invalid_num	 		= "Mobile: Invalid number";
	mobile_min_length 	 		= "Mobile is shorter than the allowed minimum of 7 characters";
	letters_regexp 		 		= "^['a-zA-Z ]+$";
	//long form
	letters_digits_regexp 		= "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$";
	phone_invalid_number 		= "Phone: Invalid number";
	user_name_mandatory	 		= "Username is Mandatory field";
	user_name_letters_digits 	= "Username: Please use English letters or digits only";
	user_name_min_length		= "Username is shorter than the allowed minimum of 6 characters";
	password_mandatory			= "Password is Mandatory field";
	password_letters_digits 	= "Password: Please use English letters or digits only";
	password_retype_mandatory	= "Retype Password is Mandatory field";
	password_retype_match 		= "Retyped Password does not match";
	password_min_length			= "Password must consist of at least 6 characters";
	city_mandatory 				= "City is Mandatory field";
	city_letters				= "City: Please enter letters only";
	birthDay 					= "Day is Mandatory field";
	birthMonth 					= "Month is Mandatory field";
	birthYear					= "Year is Mandatory field";
	acceptTerms					= "You must accept our Terms and Conditions first";
	invalidDate					= "Invalid date";
	ageUnder18					= "We are sorry, but we do not accept customer under 18 years of age";
	phone_min_length			= "Phone is shorter than the allowed minimum of 7 characters";
		
	switch (Number(skinId)) {
		case 2:
		case 13:
			language = "English";
			first_name_mandatory = "First name is mandatory field";
			first_name_letters	 = "First name: Please enter letters only";
			last_name_mandatory	 = "Last name is mandatory field";
			last_name_letters	 = "Last name: Please enter letters only";
			email_mandatory 	 = "Email is mandatory field";
			email_invalid		 = "Email address is invalid";
			mobile_mandatory	 = "Mobile is mandatory field";
			mobile_invalid_num	 = "Mobile: Invalid number";
			mobile_min_length 	 = "Mobile is shorter than the allowed minimum of 7 characters";
			letters_regexp 		 = "^['a-zA-Z ]+$";
			//TODO:long form
			break;
		case 3:
			language = "Turkish";
			first_name_mandatory = "Ad Zorunlu alan";
			first_name_letters	 = "Ad: Lütfen sadece harf kullanın";
			last_name_mandatory	 = "Soyad Zorunlu alan";
			last_name_letters	 = "Soyad: Lütfen sadece harf kullanın";
			email_mandatory 	 = "e-posta Zorunlu alan";
			email_invalid		 = "E-posta adresi geçersiz";
			mobile_mandatory	 = "Cep telefonu Zorunlu alan";
			mobile_invalid_num	 = "Cep telefonu: Validasyon Hatası";
			mobile_min_length 	 = "Cep telefonu, izin verilen minimum 7 karakterden daha kısa";
			letters_regexp 		 = "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$";
			//TODO:long form
			break;
		case 4:
			language = "English";
			first_name_mandatory = "الاسم الأول خطأ التأكيد";
			first_name_letters	 = "الاسم الأول: الرجاء إدخال حروف فقط";
			last_name_mandatory	 = "الاسم الأخير خطأ التأكيد";
			last_name_letters	 = "الاسم الأخير: الرجاء إدخال حروف فقط";
			email_mandatory 	 = "البريد الإلكتروني خطأ التأكيد";
			email_invalid		 = "عنوان البريد الإلكتروني غير صالح";
			mobile_mandatory	 = "الهاتف المحمول خطأ التأكيد";
			mobile_invalid_num	 = "الهاتف المحمول: خطأ التأكيد";
			mobile_min_length 	 = "الهاتف المحمول أقصر من الحد الأدنى المسموح به وهو 7 حرفاً";
			letters_regexp 		 = "^['a-zA-Z ]+$";
			//TODO:long form
			break;
		case 5:
		case 14:
			language = "Spanish";
			first_name_mandatory = "Nombre Campo obligatorio";
			first_name_letters	 = "Nombre: Por favor, utilice letras solamente";
			last_name_mandatory	 = "Apellidos Campo obligatorio";
			last_name_letters	 = "Apellidos: Por favor, utilice letras solamente";
			email_mandatory 	 = "email Campo obligatorio";
			email_invalid		 = "El correo electrónico no es válido";
			mobile_mandatory	 = "Móvil Campo obligatorio";
			mobile_invalid_num	 = "Móvil Error de validación";
			mobile_min_length 	 = "Móvil es más corto que el mínimo permitido de 7 caracteres";
			letters_regexp 		 = "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$";
			//TODO:long form
			break;
		case 8:
			language = "German";
			first_name_mandatory = "Vorname Pflichtfeld";
			first_name_letters	 = "Vorname: Bitte benutzen Sie nur Buchstaben";
			last_name_mandatory	 = "Nachname Pflichtfeld";
			last_name_letters	 = "Nachname: Bitte benutzen Sie nur Buchstaben";
			email_mandatory 	 = "E-Mail Pflichtfeld";
			email_invalid		 = "E-Mail Adresse ist ungültig";
			mobile_mandatory	 = "Mobiltelefon Pflichtfeld";
			mobile_invalid_num	 = "Mobiltelefon: Überprüfungsfehler";
			mobile_min_length 	 = "Mobiltelefon muss mindestens 7 Zeichen lang sein";
			letters_regexp 		 = "^['a-zA-ZäÄöÖüÜß ]+$";
			//TODO:long form
			break;
		case 9:
			language = "Italian";
			first_name_mandatory = "Nome Campo obbligatorio";
			first_name_letters	 = "Nome: Inserite solo lettere";
			last_name_mandatory	 = "Cognome Campo obbligatorio";
			last_name_letters	 = "Cognome: Inserite solo lettere";
			email_mandatory 	 = "e-mail Campo obbligatorio";
			email_invalid		 = "indirizzo e-mail non valido";
			mobile_mandatory	 = "Cellulare Campo obbligatorio";
			mobile_invalid_num	 = "Cellulare: Errore di convalida";
			mobile_min_length 	 = "Cellulare è più corto del valore minimo consentito di 7 caratteri";
			letters_regexp 		 = "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$";
			//TODO:long form
			break;
		case 10:
			language = "Russian";
			first_name_mandatory = "Имя Обязательное поле";
			first_name_letters	 = "Имя: Вводите только буквы";
			last_name_mandatory	 = "Фамилия Обязательное поле";
			last_name_letters	 = "Фамилия: Вводите только буквы";
			email_mandatory 	 = "Электронная почта Обязательное поле";
			email_invalid		 = "Неверный адрес эл. почты";
			mobile_mandatory	 = "Мобильный телефон Обязательное поле";
			mobile_invalid_num	 = "Мобильный телефон Ошибка валидации";
			mobile_min_length 	 = "Мобильный телефон короче допустимой длины 7 символов";
			letters_regexp 		 = "^['a-zA-ZА-Яа-я ]+$";
			//TODO:long form
			break;
		case 12:
			language = "French";
			first_name_mandatory = "Prénom est un champ obligatoire";
			first_name_letters	 = "Prénom: Veuillez saisir des lettres uniquement";
			last_name_mandatory	 = "Nom est un champ obligatoire";
			last_name_letters	 = "Nom: Veuillez saisir des lettres uniquement";
			email_mandatory 	 = "Email est un champ obligatoire";
			email_invalid		 = "Adresse Email incorrecte";
			mobile_mandatory	 = "Téléphone portable est un champ obligatoire";
			mobile_invalid_num	 = "Téléphone portable: Nombre incorrect";
			mobile_min_length 	 = "Téléphone portable est plus court que le minimum autorisé de 7 caractères";
			letters_regexp 		 = "^['a-zA-Zéèàùâêîôûëïç ]+$";
			//TODO:long form
			break;
		case 15:
			language = "Chinese";
			first_name_mandatory = "名字 是必填栏";
			first_name_letters	 = "名字: 请只使用字母";
			last_name_mandatory	 = "姓 是必填栏";
			last_name_letters	 = "姓: 请只使用字母";
			email_mandatory 	 = "电子邮件 是必填栏";
			email_invalid		 = "无效的电子邮件地址";
			mobile_mandatory	 = "移动电话 是必填栏";
			mobile_invalid_num	 = "移动电话: 无效号码";
			mobile_min_length 	 = "移动电话 短于所允许的最小7个字符";
			letters_regexp 		 = "^['a-zA-Z一-龠 ]+$";
			//long form
			phone_invalid_number 		= "电话: 无效号码";
			user_name_mandatory	 		= "用户名: 是必填栏";
			user_name_letters_digits 	= "用户名: 请只使用英文字母或数字";
			user_name_min_length		= "用户名短于6个字符的最小允许值";
			password_mandatory			= "密码: 是必填栏";
			password_letters_digits 	= "密码: 请只使用英文字母或数字";
			password_retype_mandatory	= "重输密码: 是必填栏";
			password_retype_match		= "重输密码不匹配";
			password_min_length			= "密码必须包含至少6个字符";
			city_mandatory 				= "城市: 是必填栏";
			city_letters				= "城市: 请只使用字母";
			birthDay 					= "日: 是必填栏";
			birthMonth 					= "月: 是必填栏";
			birthYear					= "年: 是必填栏";
			acceptTerms					= "您必须首先接受我们的条款和条件";
			invalidDate					= "无效日期";
			ageUnder18					= "很抱歉，但我们不能接受未满18岁的客户";
			phone_min_length			= "电话号码短于7个字符的最小允许值";
			break;
	}
}

//Phone code keyDown
// Allow tab key
function phoneKeys(e) {
	var keynum;
	if(window.event) { // IE
	  keynum = e.keyCode;
 	} else if(e.which) {  // Netscape/Firefox/Opera
	  keynum = e.which;
    }
	if (keynum != 9) {  // tab key
		return false;
  	}
}

//Phone code prefix by country code
var countriesPhone = {  "AD":376,
						"AE":971,
						"AF":93,
						"AG":1286,
						"AI":1265,
						"AL":355,
						"AM":374,
						"AN":599,
						"AO":244,
						"AQ":672,
						"AR":54,
						"AS":684,
						"AT":43,
						"AU":61,
						"AW":297,
						"AZ":994,
						"BA":387,
						"BB":1246,
						"BD":880,
						"BE":32,
						"BF":226,
						"BG":359,
						"BH":973,
						"BI":257,
						"BJ":229,
						"BM":1441,
						"BN":673,
						"BO":591,
						"BR":55,
						"BS":1242,
						"BT":975,
						"BV":0,
						"BW":267,
						"BY":375,
						"BZ":501,
						"CA":1,
						"CC":61,
						"CF":236,
						"CG":242,
						"CH":41,
						"CI":225,
						"CK":682,
						"CL":56,
						"CM":237,
						"CN":86,
						"CO":57,
						"CR":506,
						"CU":53,
						"CV":238,
						"CX":61,
						"CY":357,
						"CZ":420,
						"DE":49,
						"DJ":253,
						"DK":45,
						"DM":1767,
						"DO":1809,
						"DZ":213,
						"EC":593,
						"EE":372,
						"EG":20,
						"EH":0,
						"ER":291,
						"ES":34,
						"ET":251,
						"FI":358,
						"FJ":679,
						"FK":500,
						"FM":691,
						"FO":298,
						"FR":33,
						"GA":241,
						"GB":44,
						"GD":1473,
						"GE":995,
						"GF":594,
						"GH":233,
						"GI":350,
						"GL":299,
						"GM":220,
						"GN":224,
						"GP":590,
						"GQ":240,
						"GR":30,
						"GT":502,
						"GU":1671,
						"GY":592,
						"HK":852,
						"HN":504,
						"HR":385,
						"HT":509,
						"HU":36,
						"ID":62,
						"IE":353,
						"IL":972,
						"IN":91,
						"IQ":964,
						"IR":98,
						"IS":354,
						"IT":39,
						"JM":1876,
						"JO":962,
						"JP":81,
						"KE":254,
						"KG":996,
						"KH":855,
						"KI":686,
						"KM":269,
						"KN":1869,
						"KP":850,
						"KR":82,
						"KW":965,
						"KY":1345,
						"KZ":7,
						"LA":856,
						"LB":961,
						"LC":1758,
						"LI":423,
						"LK":94,
						"LR":231,
						"LS":266,
						"LT":370,
						"LU":352,
						"LV":371,
						"LY":218,
						"MA":212,
						"MC":377,
						"MD":373,
						"ME":381,
						"MG":261,
						"MH":692,
						"MK":389,
						"ML":223,
						"MM":95,
						"MN":976,
						"MO":853,
						"MQ":596,
						"MR":222,
						"MS":1664,
						"MT":356,
						"MU":230,
						"MV":960,
						"MW":265,
						"MX":52,
						"MY":60,
						"MZ":258,
						"NA":264,
						"NC":687,
						"NE":227,
						"NF":672,
						"NG":234,
						"NI":505,
						"NL":31,
						"NO":47,
						"NP":977,
						"NR":674,
						"NU":683,
						"NZ":64,
						"OM":968,
						"PA":507,
						"PE":51,
						"PF":689,
						"PG":675,
						"PH":63,
						"PK":92,
						"PL":48,
						"PN":0,
						"PR":1787,
						"PT":351,
						"PW":680,
						"PY":595,
						"QA":974,
						"RE":262,
						"RO":40,
						"RS":381,
						"RU":7,
						"RW":250,
						"SA":966,
						"SB":677,
						"SC":248,
						"SD":249,
						"SE":46,
						"SG":65,
						"SH":290,
						"SI":386,
						"SK":421,
						"SL":232,
						"SM":378,
						"SN":221,
						"SO":252,
						"SR":597,
						"SV":503,
						"SY":963,
						"SZ":268,
						"TD":235,
						"TG":228,
						"TH":66,
						"TJ":992,
						"TK":690,
						"TM":993,
						"TN":216,
						"TO":676,
						"TP":670,
						"TR":90,
						"TT":1868,
						"TV":688,
						"TW":886,
						"TZ":255,
						"UA":380,
						"UG":256,
						"US":1,
						"UY":598,
						"UZ":998,
						"VA":39,
						"VE":58,
						"VG":0,
						"VI":1340,
						"VN":84,
						"VU":678,
						"WS":685,
						"YE":967,
						"YT":269,
						"ZA":27,
						"ZM":260,
						"ZR":243,
						"ZW":263}

function getPhoneCodeByCountry(countryCode){
  return countriesPhone[countryCode];
}

function closeErrorBox(){
	$("#error_box").css("display", "none");
	$("#Overlay").css("display", "none");
    //set focus on the error field
    $(document).ready(function(){
    	err_field.focus();
    });
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

//flash submit
swfobject.registerObject("myId", "9.0.0", "swf/expressInstall.swf");
function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

function submitForm() {
	$("#contactForm").submit();
}


//phone code
function setPhoneCode() {
	countryCode = geoplugin_countryCode();
	if (null != countryCode &&  'undefined' != countryCode) {
		countryCode = countryCode.toUpperCase();
	}
	var phoneCode = getPhoneCodeByCountry(countryCode);
	if (phoneCode == null || phoneCode == undefined) {
		phoneCode = 0;
	}
	document.getElementById("mobilePhonePrefix").value = "+" + phoneCode;
	//document.getElementById("landLinePhonePrefix").value = "+" + phoneCode;
}

//display error msg
function display_error(msg) {
	//Remove from all elements the red border
	$(".error").removeClass("error");
	//Fill error msg
	var error_msg = document.getElementById("error_message_label");
	error_msg.innerHTML = msg;
	//Display error msg
	var box = document.getElementById("error_box");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

/* ****************Validate birthday*************** */
// Add proper date check method
jQuery.validator.addMethod('validDate',function(value, element) {
    var valid = false;
    
    // First we're going to check if all three fields were provided. If not return true and check will fire after they're supplied
    if ($("#birthMonth").val() != '' && $("#birthDay").val() != '' && $("#birthYear").val() != '') {
        var birthmonth = parseInt($("#birthMonth").val(), 10)
        var birthday = parseInt($("#birthDay").val(), 10)
        var birthyear = parseInt($("#birthYear").val(), 10)
        
        var JDate = new Date(birthyear, birthmonth - 1, birthday)
        
        valid = (birthmonth - 1 == JDate.getMonth() && birthday == JDate.getDate() && birthyear == JDate.getFullYear())
    }

    return valid    
})

// Add check for birthdate entry. Validation doesn't seem to like a required hidden field.
jQuery.validator.addMethod('requiredBirthDay',function(value, element) {
    var valid = false;
    
    // First we're going to check if all three fields were provided. If not return false
    if ($("#birthDay").val()!= '') {                    
        valid = true;
    }
    return valid    
})

jQuery.validator.addMethod('requiredBirthMonth',function(value, element) {
    var valid = false;
    
    // First we're going to check if all three fields were provided. If not return false
    if ($("#birthMonth").val() != '') {                    
        valid = true;
    }
    return valid    
})

jQuery.validator.addMethod('requiredBirthYear',function(value, element) {
    var valid = false;
    
    // First we're going to check if all three fields were provided. If not return false
    if ($("#birthYear").val() != '') {                    
        valid = true;
    }
    return valid    
})

// Check for proper age
jQuery.validator.addMethod('validAge', function(value, element) {
    var valid = true;
    var age = 18
  
    // First we're going to check if all three fields were provided. If not return true and check will fire after they're supplied
    if ($("#birthMonth").val() != '' && $("#birthDay").val() != '' && $("#birthYear").val() != '') {
    
        // split birthdate into parts
        var birthmonth = parseInt($("#birthMonth").val(), 10)
        var birthday = parseInt($("#birthDay").val(), 10)
        var birthyear = parseInt($("#birthYear").val(), 10)
        
        // Convert to javascript date - Remember to subtrack 1 from month since javascript month starts at 0
        var JSbirthdate = new Date(birthyear, birthmonth - 1, birthday)
        
        // Set current date
        var JScurrdate = new Date()
        
        // Subtract age from current year
        JScurrdate.setFullYear(JScurrdate.getFullYear() - age)
        
        // compare dates and return boolean
        valid = (JScurrdate - JSbirthdate) >= 0
    }

    return valid
})
/* ****************END Validate birthday*************** */


////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
$(document).ready(function(){
	// START SETTING PHONE CODE
	setPhoneCode();
	//define email expression rule for validation
	$.validator.addMethod('email_regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//define regular expression rule for validation
	$.validator.addMethod('regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//validation implementation will go here.
	$("#contactForm").validate({
		//if there is no error server validation for email
		submitHandler: function(form) {
			//parameters 		   		    
		    var skinId = 15;
		    var writerId = 1; //Web.
		    var utcOffset = new Date().getTimezoneOffset();
			var request = new Object();		
			var marketingTracking = get_cookie("smt");	
			request.userName = document.getElementById("userName").value;
			request.password = document.getElementById("password").value;
			request.password2 = document.getElementById("password2").value;
			request.email = document.getElementById("email").value;
			request.firstName = document.getElementById("firstName").value;
			request.lastName = document.getElementById("lastName").value;
			request.terms = document.getElementById("terms").checked ? 'true' : 'false';
			request.mobilePhone = document.getElementById("mobilePhone").value;
			request.gender = document.getElementById("gender_M").checked ? 'M' : 'F';
			request.birthYear = document.getElementById("birthYear").value;			
			request.birthMonth = document.getElementById("birthMonth").value;
			request.birthDay = document.getElementById("birthDay").value;
			request.countryId = document.getElementById("countries").value;
			request.cityNameAO = document.getElementById("cityName").value;			
			request.receiveUpdate = document.getElementById("agree").checked ? 'true' : 'false';
			request.timeFirstVisit = UTF8.decode(get_cookie("landing_firstVisit"));
			request.street = "Not Available";
			request.currencyId = "7";
			request.combinationId = combId;	
			request.userAgent = navigator.userAgent;			
			request.httpReferer = get_cookie("landing_hr");
																												  
		    $.ajax({  				
				url: proxy_url + "proxy/insertUser",
				type: "POST",  
				dataType: "json",  
				contentType: "json",  
		     	data: JSON.stringify({	"skinId"  	: skinId, 
		     							"utcOffset"	: utcOffset,
		     							"writerId"	: writerId,
		     							"isBaidu"   : "true",
		     							"marketingStaticPart" : marketingTracking,
				  						"register": { "userName"		: request.userName,
				  									  "password"		: request.password, 
				  									  "password2"		: request.password2, 
				  									  "email"			: request.email, 
				  									  "firstName"		: request.firstName, 
				  									  "lastName" 		: request.lastName, 
				  									  "street"			: request.street, 
				  									  "mobilePhone"		: request.mobilePhone, 
				  									  "terms"			: request.terms, 
				  									  "birthDay"		: request.birthDay, 
				  									  "birthMonth"		: request.birthMonth, 
				  									  "birthYear"		: request.birthYear, 
				  									  "gender"			: request.gender, 
				  									  "countryId"		: request.countryId,
				  									  "cityNameAO"		: request.cityNameAO,				  									  
				  									  "contactByEmail" 	: request.receiveUpdate,
				  									  "contactBySms"	: request.receiveUpdate,
				  									  "currencyId"		: request.currencyId,
				  									  "combinationId"	: request.combinationId,
				  									  "userAgent"		: request.userAgent,
				  									  "httpReferer"		: request.httpReferer,
				  									  "timeFirstVisitTxt"	:  request.timeFirstVisit 				  									  
				  									    }}),
			 success: function(result){				
				if (result.errorCode == 0) {						
					if (get_cookie("landing_email") == null) {
						set_cookie("landing_email", UTF8.encode(request.email), 365, '/', baidu_domain, '');
					}
					
					if (result.marketingStaticPart != null){
						set_cookie("smt", result.marketingStaticPart, 16910, '/', baidu_domain, '');
					}
					window.location = 'chineseAuth.html'; 	
			  	} else { //error found				  		
			  		showError(result);							 
		  		}		   					    			           			      
			  },  
			  error: function(){  
			    //alert("Can't connect to the server insert user ");  
			  }  		    
		    });	
        	},
     	errorPlacement: function(error, element) {
		if (validate_fields) {
			validate_fields = false;
			err_field = element[0];
			display_error(error[0].innerHTML);
			$(element[0].parentNode).addClass( "error" );
		}
	}, onfocusout: false, onBlur: false,
			rules: {
             	firstName: {
					required: true,
					regexp: letters_regexp
            	},
                lastName: {
                	required: true,
					regexp: letters_regexp
				},
                email: {
					required: true,
					email: true,
					email_regexp: "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$"
               	},
				mobilePhone: {
					required: true,
					number: true,
					minlength: 7
                },           
                /*landLinePhone: {
					number: true,
					minlength: 7
                },*/    
                userName: {
                	required: true,
					regexp: letters_digits_regexp,
					minlength: 6
                },
                password: {
                	required: true,
					regexp: letters_digits_regexp,
					minlength: 6
                },
                password2: {
                	required: true,
					equalTo: "#password"
                },
                cityName: {
                	required: true,
					regexp: letters_regexp
                },
                birthDay: {
	                required: true
	            },
                birthMonth: {
                	required: true
	            },
	            birthYear: {
	                required: true,
	                validDate: true,
	                validAge: true
	            },
                terms: {
                	required: true
                }            
			},
			messages: {
             	firstName: {
           			required: first_name_mandatory,
					regexp: first_name_letters
				},
				lastName: {
                 	required: last_name_mandatory,
                 	regexp: last_name_letters
				},
                 email: {
     	        	required: email_mandatory,
					email: email_invalid,
					email_regexp: email_invalid
				},
				mobilePhone: {
					required: mobile_mandatory,
					number: mobile_invalid_num,
					minlength: mobile_min_length
               	},
               	/*landLinePhone: {
					number: phone_invalid_number,
					minlength: phone_min_length
                },*/
                userName: {
                	required: user_name_mandatory,
					regexp: user_name_letters_digits,
					minlength: user_name_min_length
                },
                password: {
                	required: password_mandatory,
					regexp: password_letters_digits,
					minlength: password_min_length
                },
                password2: {
                	required: password_retype_mandatory,
					equalTo: password_retype_match
                },  
                cityName: {
                	required: city_mandatory,
					regexp: city_letters
                },
                birthDay: {
	                required: birthDay
	            },
                birthMonth: {
                	required: birthMonth
	            },
	            birthYear: {
	                required: birthYear,
	                validDate: invalidDate,
	                validAge: ageUnder18
	            },
               	terms: {
                	required: acceptTerms
                }   
			}
		});
	})
/* END VALIDATION */

function showError(result) {
	if (null != result.userMessages) {
		var message = result.userMessages[0].message; 			 			
		var field = result.userMessages[0].field;
	}						 				 				
	err_field = document.getElementById(field);
	display_error(message);
	$(err_field.parentNode).addClass( "error" );
}

// Register attempt 
function insertUpdateRegisterAttempts(obj) { 
	var value = $(obj).val();
    if ( value.length < 1) {
    	return;
    }
    if (obj.id == 'email') {
    	var pattern = '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
    	if (!value.match(pattern)) {
    		return;
    	}
    }
    if (obj.id == 'mobilePhone') {
    	var pattern = '[0-9]{7,25}';
    	if (!value.match(pattern) || value.length < 7) {
    		return;
    	}
    }
    /*if (obj.id == 'landLinePhone') {
    	var pattern = '[0-9]{7,25}';
    	if (!value.match(pattern) || value.length < 7) {
    		return;
    	}
    }*/
    if (obj.id == 'agree') {
    	var regAttemptCookieId = get_cookie("landing_regAtmpId");
    	if (regAttemptCookieId == null || regAttemptCookieId == 'undefined') {
    		return;
    	}
    
    }
    
            
	//parameters 		   		    
    var skinId = 15;
    var utcOffset = new Date().getTimezoneOffset();				
	var email = document.getElementById("email").value;
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var mobilePhone = document.getElementById("mobilePhone").value;
	//var landLinePhone = document.getElementById("landLinePhone").value;	
	var countryId = document.getElementById("countries").value;
	var receiveUpdate = document.getElementById("agree").checked ? 'true' : 'false';		
	var combinationId = combId;	
	var userAgent = navigator.userAgent;			
	var httpReferer = get_cookie("landing_hr");
	var regAttemptId = get_cookie("landing_regAtmpId");
	var mId = getMid();
																												  
    $.ajax({  			 	
		url: proxy_url + "proxy/insertRegisterAttempt",
		type: "POST",  
		dataType: "json",  
		contentType: "json",  
     	data: JSON.stringify({	"skinId"  		: skinId, 
     							"utcOffset"		: utcOffset,		  						 
								"email"			: email, 
								"firstName"		: firstName, 
  								"lastName" 		: lastName,  
  								"mobilePhone"	: mobilePhone,
  								//"landLinePhone"	: landLinePhone,   								 
  								"countryId"		: countryId,
  								"contactByEmail": receiveUpdate,
  								"contactBySms"	: receiveUpdate,  								
  								"combinationId"	: combinationId,
  								"userAgent"		: userAgent,
  								"httpReferer"	: httpReferer,
  								"mId"           : mId,
  								"id"			: regAttemptId				  									  
		  					}),
		 success: function(result) {				
			if (result.errorCode == 0) {										
				if (get_cookie("landing_regAtmpId") == null) {
					set_cookie("landing_regAtmpId", result.registerAttemptId, 365, '/', baidu_domain, '');
				}
				
				if (result.marketingStaticPart != null) {
					set_cookie("smt", result.marketingStaticPart, 16910, '/', baidu_domain, '');
				}	  					  			  			
		  	} 	   					    			           			      
		  }  
   		 });
}
