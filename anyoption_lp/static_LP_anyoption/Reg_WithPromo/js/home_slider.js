
function g(id){return document.getElementById(id);}
var slider1_arr = [];
function slider1(id,slide){
	slider1_arr['curr'] = 0;
	slider1_arr['ul'] = g(id);
	slider1_arr['lis'] = [];
	slider1_arr['timer'] = null;
	slider1_arr['timer2'] = null;
	slider1_arr['slide'] = slide;
	var lis = slider1_arr['ul'].getElementsByTagName('li');
	var ii = 0;
	for(var i=0;i<lis.length;i++){
		if(lis[i].getAttribute('data-li') == 't'){
			slider1_arr['lis'][ii] = lis[i];
			if(slider1_arr['slide']){
				slider1_arr['lis'][ii].style.left = slider1_arr['lis'][ii].offsetWidth+'px';
			}
			ii++;
		}
	}
	slider1_arr['n_ul'] = g(id+"_nav");
	slider1_arr['n_li'] = slider1_arr['n_ul'].getElementsByTagName('li');
	for(var i=0;i<slider1_arr['n_li'].length;i++){
		slider1_arr['n_li'][i].setAttribute('data-i',i);
		slider1_arr['n_li'][i].onclick = function(){
			slider1_play(this,'');
		}
	}
	g(id+'_left').onclick = function(){slider1_play('','left');}
	g(id+'_right').onclick = function(){slider1_play('','right');}
	
	slider1_arr['lis'][slider1_arr['curr']].style.zIndex = 2;
	slider1_arr['lis'][slider1_arr['curr']].style.left = 0;
	
	var els = slider1_arr['lis'][slider1_arr['curr']].getElementsByTagName('*');
	for(var i=0;i<els.length;i++){
		if(els[i].getAttribute('data-el') == 't'){
			els[i].style.top = 0;
		}
	}
	slider1_arr['timer'] = setTimeout(function(){slider1_play('','right');},8000);
}
function slider1_play(th,dir){
	var same_slade = false;
	if(th != ''){
		if(th.getAttribute('data-i') == slider1_arr['curr']){
			same_slade = true;			
		}
	}
	if((slider1_arr['timer2'] == null)&&(!same_slade)){
		clearTimeout(slider1_arr['timer']);
		if(dir != ''){
			if(dir == 'left'){
				var n = slider1_arr['curr']-1;
				if(n < 0){n = slider1_arr['n_li'].length-1;}
			}
			else{
				var n = slider1_arr['curr']+1;
				if(n >= slider1_arr['n_li'].length){n = 0;}
			}
		}
		else{
			var n = parseInt(th.getAttribute('data-i'));
		}
		
		var inc = 0;
		var scr = slider1_arr['ul'].offsetWidth;
		var scr_left = scr;
		for(var i=0;i<slider1_arr['lis'].length;i++){
			slider1_arr['lis'][i].style.zIndex = 1;
		}
		if(dir == "right"){
			slider1_arr['lis'][n].style.left = scr+'px';
		}
		else{
			slider1_arr['lis'][n].style.left = -scr+'px';
		}
		slider1_arr['lis'][n].style.zIndex = 3;
		slider1_arr['lis'][slider1_arr['curr']].style.zIndex = 2;
		slider1_arr['n_li'][n].className += " hov";
		slider1_arr['n_li'][slider1_arr['curr']].className = slider1_arr['n_li'][slider1_arr['curr']].className.replace('hov','');
		
		var doSetTimeout = function(){
			inc = Math.round(scr_left*0.2);
			if(inc < 1){inc = 1;}

			if(dir == "right"){
				slider1_arr['lis'][n].style.left = slider1_arr['lis'][n].offsetLeft-inc+"px";
				if(slider1_arr['slide']){
					slider1_arr['lis'][slider1_arr['curr']].style.left = slider1_arr['lis'][slider1_arr['curr']].offsetLeft-inc+"px";
				}
			}
			else{
				slider1_arr['lis'][n].style.left = slider1_arr['lis'][n].offsetLeft+inc+"px";
				if(slider1_arr['slide']){
					slider1_arr['lis'][slider1_arr['curr']].style.left = slider1_arr['lis'][slider1_arr['curr']].offsetLeft+inc+"px";
				}
			}
			scr_left -= inc;
			if(scr_left > 0){
				slider1_arr['timer2'] = setTimeout(doSetTimeout,30);
			}
			else{
				slider1_arr['timer2'] = null;
				var els = slider1_arr['lis'][n].getElementsByTagName('*');
				for(var i=0;i<els.length;i++){
					if(els[i].getAttribute('data-el') == 't'){
						els[i].style.top = 0;
					}
				}
				var els = slider1_arr['lis'][slider1_arr['curr']].getElementsByTagName('*');
				for(var i=0;i<els.length;i++){
					if(els[i].getAttribute('data-el') == 't'){
						els[i].style.top = '';
					}
				}
				slider1_arr['timer'] = setTimeout(function(){slider1_play('','right');},8000);
				slider1_arr['curr'] = n;
			}
		}
		doSetTimeout();
	}
}