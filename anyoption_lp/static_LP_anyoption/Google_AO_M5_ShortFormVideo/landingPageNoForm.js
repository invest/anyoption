
//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
if (null == skinId || 'undefined' == skinId) {
	skinId = 2;
}
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
// create link
var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
function getLink(obj) {
	obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&' + queryString;
}

// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	obj.href= host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
}
