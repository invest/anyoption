<?php
	header('Content-type: application/javascript');
	require_once( 'Mobile-Detect-2.7.8/Mobile_Detect.php');
	$detect = new Mobile_Detect();
	
	$isMobile = $detect->isMobile() ? 'true' : 'false';
	$isTablet = $detect->isTablet() ? 'true' : 'false';
	$isiOS = $detect->isiOS() ? 'true' : 'false';
	$isAndroidOS = $detect->isAndroidOS() ? 'true' : 'false';
	$isPc = ($detect->isMobile() ? 'false' : ($detect->isTablet() ? 'false' : 'true'));
	$isBlackBerry = $detect->isBlackBerry() ? 'true' : 'false';
	
	echo
	"var deviceType = {
	    isMobile: function() {
	 		return $isMobile;
	    },
	    isTablet: function() {
	 		return $isTablet;
	    },
	    isiOS: function() {
	 		return $isiOS;
	    },
	    isAndroidOS: function() {
	 		return $isAndroidOS;
	    },
	    isPc: function() {
	 		return $isPc;
	    },
	    isBlackBerry: function() {
	 		return $isBlackBerry;
	    }
	};"
?>



