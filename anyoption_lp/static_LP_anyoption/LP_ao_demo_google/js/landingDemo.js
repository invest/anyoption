//close light box
function closeLightBoxCallMe(){
	document.getElementById("lightform").style.display = 'none';
	document.getElementById("Overlay").style.display = 'none';
	document.getElementById("lightbox").style.display = 'none';
}

//open light box
function openLightBoxCallMe(){
	var box = document.getElementById("lightbox");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

//diplay lightbox
function startToBtn() {
	//Display error msg
	var box = document.getElementById("lightform");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

//refresh page
function refreshPage() {
	window.location.reload();
}

//open new window call me
function openCallMeWindow() {
	var urlCallMe = "http://www.anyoption.com/jsp/contactMeHPSmallAllLanguages.jsf?s=";
	urlCallMe += skinId;
	window.open(urlCallMe, 'callmePopup', 'width=405,height=340,history=0,resizable=1,status=1,menubar=1,scrollbars=1');
}
