/*By Georgi Diamandiev*/
function g(id){return document.getElementById(id);}
function gd_add_gallery(){
	var as = document.getElementsByTagName('a');
	for(var i=0;i<as.length;i++){
		if(as[i].getAttribute('data-gdGallery') == 't'){
			as[i].onclick = function(){
				gd_open_gallery(this);
				return false;
			}
		}
	}
	gd_creat_gallery();
}
function gd_open_gallery(th){
	var iframes = document.getElementsByTagName('iframe');
	for(var i=0;i<iframes.length;i++){
		if(iframes[i].getAttribute('data-hide') == 't'){
			iframes[i].style.display = "none";
		}
	}
	g('gd_gallery_bgr').style.display = "block";
	var div = g('gd_gallery_in');
	div.style.display = "block";
	div.innerHTML = "<img src='"+th.href+"' style='display:none;'/>";
	var img = div.getElementsByTagName('img')[0];
	img.onload = function(){
		this.style.display = "block";
		this.parentNode.style.marginTop = -(this.offsetHeight/2)+"px";
		this.parentNode.style.marginLeft = -(this.offsetWidth/2)+"px";
	}
}
function gd_creat_gallery(){
	var bg = document.createElement('div');
	bg.className = "gd_gallery_bgr";
	bg.id = "gd_gallery_bgr";
	bg.onclick = function(){
		g('gd_gallery_bgr').style.display = "none";
		g('gd_gallery_in').style.display = "none";
		var iframes = document.getElementsByTagName('iframe');
		for(var i=0;i<iframes.length;i++){
			if(iframes[i].getAttribute('data-hide') == 't'){
				iframes[i].style.display = "";
			}
		}
	}
	window.document.body.appendChild(bg);
	var div = document.createElement('div');
	div.className = "gd_gallery_in";
	div.id = "gd_gallery_in";
	window.document.body.appendChild(div);
}
