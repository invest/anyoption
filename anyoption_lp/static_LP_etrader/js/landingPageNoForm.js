
//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 1;
}
if (null == combId ||  'undefined' == combId) {
	combId = 71;
}
// create link
var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
function getLink(obj) {
	obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&' + queryString;
}

// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	obj.href= host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
}