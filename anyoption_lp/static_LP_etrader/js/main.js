/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
function addScriptToHead(filename,scr_id){
	if(!g(scr_id)){
	  var js = document.createElement('script');
	  js.id = scr_id;
	  js.src = context_path+"/js/"+filename;
	  document.getElementsByTagName('head')[0].appendChild(js);
	}
}
var funnel_div_id = '';
var register_lp = true;
function initRegForm(id,skinId){
	funnel_div_id = id;
	addScriptToHead('regForm_all.js','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
}
var global_deposit_prefix = '';
function inpFocus(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	var pass = (field.getAttribute('data-p') != undefined)?true:false;
	var tooltip = (field.getAttribute('data-tooltip') != undefined)?g(field.getAttribute('data-tooltip')):'';
	if(field.value == ""){
		field.value = def;
		if(pass){field.type = 'text';}
		field.className = field.className.replace('active','');
	}
	else if(field.value == def){
		field.value = "";
		if(pass){field.type = 'password';}
		field.className += " active";
	}
	else{
		field.className += " active";
	}
	if(field.getAttribute('data-tooltip_show') == 't'){
		if(tooltip != ""){
			tooltip.style.display = "none";
			field.setAttribute('data-tooltip_show','');
		}	
	}
	else{
		if(tooltip != ""){
			tooltip.style.display = "block";
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			field.setAttribute('data-tooltip_show','t');
		}	
	}
	if(noblur == undefined){
		field.onblur = function(){
			inpFocus(field,noblur);
			eval(field.getAttribute('onblur'));
		}
	}
}
function checkBox(th){
	var inp = th.getElementsByTagName('input')[0];
	if(inp.checked){
		inp.checked = false;
		th.className = th.className.replace(/ checked/g,'');
	}
	else{
		inp.checked = true;
		th.className += " checked";
	}
}
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    'use strict';
    if (this == null) {
      throw new TypeError();
    }
    var n, k, t = Object(this),
        len = t.length >>> 0;

    if (len === 0) {
      return -1;
    }
    n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  };
}