//Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-KV2PNV');

//constants
var cookie_domain 				= ".etrader.co.il";
var host_url 					= "http://www.etrader.co.il/"; 
var json_url					= "http://www.etrader.co.il/";
var cdn_url_json 				= "http://cdn.etrader.co.il/";	
var context_path				= 'http://www.etrader.co.il';
var context_path_ajax			= 'http://cdn.etrader.co.il';
var context_path_json			= 'http://cdn.etrader.co.il';
document.domain					= 'etrader.co.il';
var etsserv 					= "http://tkr.etrader.co.il/";

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};
parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

var language = "Hebrew";

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 1;
}
if (null == combId ||  'undefined' == combId) {
	combId = 71;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);

// create link
var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
function getLink(obj) {
	obj.href = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&' + queryString;
}

/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
/*********** ajax.js ************/

function getXMLHttp() {
	var xmlHttp;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	return xmlHttp;
}
function AJAXInteractionTxt(url, callback) {
  	var req = init();
    req.onreadystatechange = processRequest;

    function init() {
   		if (window.XMLHttpRequest) {
        	return new XMLHttpRequest();
        } else if (window.ActiveXObject) {
        	return new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (callback) {callback(req.responseText);}
			}
		}
    }

    this.doGet = function() {
		// make a HTTP GET request to the URL asynchronously
		req.open("GET", url, true);
		req.send(null);
   	}
}
/*START SHORT REG*/
function addScriptToHead(filename,scr_id,folder){
	if(!g(scr_id)){
		if(typeof folder == 'undefined'){
			var folder_new = '';
		}
		else{
			var folder_new = folder;
		}
		if(typeof funnel_version == 'undefined'){
			funnel_version = '0';
		}
		var js = document.createElement('script');
		js.id = scr_id;
		js.src = context_path+folder_new+"/js/"+filename+"?ver="+funnel_version;
		document.getElementsByTagName('head')[0].appendChild(js);
	}
}
var funnel_div_id = '';
var register_lp = true;
function initRegForm(id,skinId){
	funnel_div_id = id;
	addScriptToHead('regForm_all.js','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
	//addScriptToHead('clientInfo.js','etsClientInfo');
}
var global_deposit_prefix = '';
function inpFocus(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	var pass = (field.getAttribute('data-p') != undefined)?true:false;
	var tooltip = (field.getAttribute('data-tooltip') != undefined)?g(field.getAttribute('data-tooltip')):'';
	if(field.value == ""){
		field.value = def;
		if(pass){field.type = 'text';}
		field.className = field.className.replace('active','');
	}
	else if(field.value == def){
		field.value = "";
		if(pass){field.type = 'password';}
		field.className += " active";
	}
	else{
		field.className += " active";
	}
	if(field.getAttribute('data-tooltip_show') == 't'){
		if(tooltip != ""){
			tooltip.style.display = "none";
			field.setAttribute('data-tooltip_show','');
		}	
	}
	else{
		if(tooltip != ""){
			tooltip.style.display = "block";
			if (typeof staticToolTip == 'undefined' || !staticToolTip) {
				tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			}
			field.setAttribute('data-tooltip_show','t');
		}	
	}
	if(noblur == undefined){
		field.onblur = function(){
			inpFocus(field,noblur);
			eval(field.getAttribute('onblur'));
		}
	}
}
function checkBox(th){
	var inp = th.getElementsByTagName('input')[0];
	if(inp.checked){
		inp.checked = false;
		th.className = th.className.replace(/ checked/g,'');
	}
	else{
		inp.checked = true;
		th.className += " checked";
	}
}
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
    'use strict';
    if (this == null) {
      throw new TypeError();
    }
    var n, k, t = Object(this),
        len = t.length >>> 0;

    if (len === 0) {
      return -1;
    }
    n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n != n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n != 0 && n != Infinity && n != -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  };
}
/*END short reg*/
/*dinamic header*/
function dynamicHeader() {
	//url parameter h1
	var h1 = uri.queryKey['h1'];
	var output;
	if (h1 != null) {
		 output = decodeURIComponent(h1);
		 g('dynamicHeader').innerHTML = output;
	}
}
// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	if (page != null && page != 'undefined') {
		pageR = page;
	}
	obj.href= host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;
}
// create link redirect to LP
function getRedirectLinkToLP(obj, page) {
	var oldPage = getUrlValue('pageR');
	obj.href = context_path_ajax + '/landing.shtml?' + queryString.replace('pageR=' + oldPage,'pageR=' + page);
}
function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}

//url parameters
var combId 	= uri.queryKey['combid'];
var dp		= uri.queryKey['dp'];
if (null == combId ||  'undefined' == combId) {
	combId = 22;
}
if (null == dp ||  'undefined' == dp) {
	dp = '';
}
function buildURLWithParameters(url) {
	url += '&combid=' + combId + '&dp=' + dp + '&aff_sub1=' + aff_sub1 + '&aff_sub2=' + aff_sub2 + '&aff_sub3=' + aff_sub3; 
	return url;
}
// create link redirect to LP
function getRedirectLinkToLP(obj, page) {
	var oldPage = getUrlValue('pageR');
	obj.href = context_path_ajax + '/landing.shtml?' + queryString.replace('pageR=' + oldPage,'pageR=' + page);
}
function add_agreement_to_iframe(id,new_url){
	if (null == new_url || 'undefined' == new_url) 
		new_url = host_url + 'jsp/agreement.jsf?refresh=true&s=' + skinId
	else 
		new_url  +='&s=' + skinId
		
	g(id).setAttribute('data-src',new_url);
}

function mpTrack(params) {
	//this is an empty function that eventualy one day in the future will use to track mixpanel events on short reg (funnel)
}
function getCookie(c_name){
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
	  	c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
	  	c_value = null;
	} else {
	  	c_start = c_value.indexOf("=", c_start) + 1;
	  	var c_end = c_value.indexOf(";", c_start);
	  	if (c_end == -1) {
			c_end = c_value.length;
	  	}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}