/*********** landingPage.js ************/

//error messages
var first_name_mandatory 	= "שם פרטי שדה חובה";
var first_name_letters	 	= "שם פרטי: הנתון שהוקלד חייב להכיל אותיות בלבד";
var last_name_mandatory		= "שם משפחה שדה חובה";
var last_name_letters		= "שם משפחה: הנתון שהוקלד חייב להכיל אותיות בלבד";
var email_mandatory 	 	= "email שדה חובה";
var email_invalid			= "email לא תקין";
var mobile_mandatory	 	= "טלפון נייד שדה חובה";
var mobile_invalid_num	 	= "טלפון נייד: הנתון שהוקלד אינו מספר תקין.";
var mobile_min_length 	 	= "טלפון נייד קצר מהמינימום המותר של 7 תווים";
var mobile_max_length 	 	= "טלפון נייד ארוך מהמקסימום המותר של 7 תווים";

var mobile_prefix_mandatory = "קידומת טלפון נייד שדה חובה";
var letters_regexp 		 	= /^['a-zA-Zא-ת ]+$/;
var host_url 					= "http://www.etrader.co.il/"; 

function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 1;
}
if (null == combId ||  'undefined' == combId) {
	combId = 71;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var emailRegExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
var numberRegExp = /^\d+$/;
//Phone code keyDown
// Allow tab key
function phoneKeys(e) {
	var keynum;
	if(window.event) { // IE
	  keynum = e.keyCode;
 	} else if(e.which) {  // Netscape/Firefox/Opera
	  keynum = e.which;
    }
	if (keynum != 9) {  // tab key
		return false;
  	}
}



//Phone code prefix by country code
var countriesPhone = {  "AD":376,
						"AE":971,
						"AF":93,
						"AG":1286,
						"AI":1265,
						"AL":355,
						"AM":374,
						"AN":599,
						"AO":244,
						"AQ":672,
						"AR":54,
						"AS":684,
						"AT":43,
						"AU":61,
						"AW":297,
						"AZ":994,
						"BA":387,
						"BB":1246,
						"BD":880,
						"BE":32,
						"BF":226,
						"BG":359,
						"BH":973,
						"BI":257,
						"BJ":229,
						"BM":1441,
						"BN":673,
						"BO":591,
						"BR":55,
						"BS":1242,
						"BT":975,
						"BV":0,
						"BW":267,
						"BY":375,
						"BZ":501,
						"CA":1,
						"CC":61,
						"CF":236,
						"CG":242,
						"CH":41,
						"CI":225,
						"CK":682,
						"CL":56,
						"CM":237,
						"CN":86,
						"CO":57,
						"CR":506,
						"CU":53,
						"CV":238,
						"CX":61,
						"CY":357,
						"CZ":420,
						"DE":49,
						"DJ":253,
						"DK":45,
						"DM":1767,
						"DO":1809,
						"DZ":213,
						"EC":593,
						"EE":372,
						"EG":20,
						"EH":0,
						"ER":291,
						"ES":34,
						"ET":251,
						"FI":358,
						"FJ":679,
						"FK":500,
						"FM":691,
						"FO":298,
						"FR":33,
						"GA":241,
						"GB":44,
						"GD":1473,
						"GE":995,
						"GF":594,
						"GH":233,
						"GI":350,
						"GL":299,
						"GM":220,
						"GN":224,
						"GP":590,
						"GQ":240,
						"GR":30,
						"GT":502,
						"GU":1671,
						"GY":592,
						"HK":852,
						"HN":504,
						"HR":385,
						"HT":509,
						"HU":36,
						"ID":62,
						"IE":353,
						"IL":972,
						"IN":91,
						"IQ":964,
						"IR":98,
						"IS":354,
						"IT":39,
						"JM":1876,
						"JO":962,
						"JP":81,
						"KE":254,
						"KG":996,
						"KH":855,
						"KI":686,
						"KM":269,
						"KN":1869,
						"KP":850,
						"KR":82,
						"KW":965,
						"KY":1345,
						"KZ":7,
						"LA":856,
						"LB":961,
						"LC":1758,
						"LI":423,
						"LK":94,
						"LR":231,
						"LS":266,
						"LT":370,
						"LU":352,
						"LV":371,
						"LY":218,
						"MA":212,
						"MC":377,
						"MD":373,
						"ME":381,
						"MG":261,
						"MH":692,
						"MK":389,
						"ML":223,
						"MM":95,
						"MN":976,
						"MO":853,
						"MQ":596,
						"MR":222,
						"MS":1664,
						"MT":356,
						"MU":230,
						"MV":960,
						"MW":265,
						"MX":52,
						"MY":60,
						"MZ":258,
						"NA":264,
						"NC":687,
						"NE":227,
						"NF":672,
						"NG":234,
						"NI":505,
						"NL":31,
						"NO":47,
						"NP":977,
						"NR":674,
						"NU":683,
						"NZ":64,
						"OM":968,
						"PA":507,
						"PE":51,
						"PF":689,
						"PG":675,
						"PH":63,
						"PK":92,
						"PL":48,
						"PN":0,
						"PR":1787,
						"PT":351,
						"PW":680,
						"PY":595,
						"QA":974,
						"RE":262,
						"RO":40,
						"RS":381,
						"RU":7,
						"RW":250,
						"SA":966,
						"SB":677,
						"SC":248,
						"SD":249,
						"SE":46,
						"SG":65,
						"SH":290,
						"SI":386,
						"SK":421,
						"SL":232,
						"SM":378,
						"SN":221,
						"SO":252,
						"SR":597,
						"SV":503,
						"SY":963,
						"SZ":268,
						"TD":235,
						"TG":228,
						"TH":66,
						"TJ":992,
						"TK":690,
						"TM":993,
						"TN":216,
						"TO":676,
						"TP":670,
						"TR":90,
						"TT":1868,
						"TV":688,
						"TW":886,
						"TZ":255,
						"UA":380,
						"UG":256,
						"US":1,
						"UY":598,
						"UZ":998,
						"VA":39,
						"VE":58,
						"VG":0,
						"VI":1340,
						"VN":84,
						"VU":678,
						"WS":685,
						"YE":967,
						"YT":269,
						"ZA":27,
						"ZM":260,
						"ZR":243,
						"ZW":263}

function getPhoneCodeByCountry(countryCode){
  return countriesPhone[countryCode];
}

function closeErrorBox(){
	g("error_box").style.display = "none";
	g("Overlay").style.display = "none";
    //set focus on the error field
   	err_field.focus();
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout(validate,500);
}

function submitForm() {
	g("contactForm").submit();
}


//phone code
function setPhoneCode() {	
	try {
		countryCode = geoplugin_countryCode();
	} catch (e) {
		countryCode = defualtPhoneCode;
	}
	if (null != countryCode &&  'undefined' != countryCode) {
		countryCode = countryCode.toUpperCase();
	}
	var phoneCode = getPhoneCodeByCountry(countryCode);
	if (phoneCode == null || phoneCode == undefined) {
		phoneCode = 0;
	}
	g("mobile_c").value = "+" + phoneCode;
}

//display error msg
function display_error(msg) {
	//Fill error msg
	var error_msg = g("error_message_label");
	error_msg.innerHTML = msg;
	//Display error msg
	var box = g("error_box");
	var overlay = g("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

//diplay lightbox
function startBtn() {
	//Display error msg
	var box = g("lightform");
	var overlay = g("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

function flashStartTrading(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

//close light box
function closeLightBox(){
	g("lightform").style.display = 'none';
	g("Overlay").style.display = 'none';
}

function setFocusFirstInput() {
	window.scrollTo(0, 0);
	g("first_name").focus();
}

function replaceall(str,replace,with_this)
{
    var str_hasil = "";
    var temp;

    for(var i = 0; i < str.length; i++) // not need to be equal. it causes the last change: undefined..
    {
        if (str[i] == replace)
        {
            temp = with_this;
        }
        else
        {
                temp = str[i];
        }

        str_hasil += temp;
    }

    return str_hasil;
}

// create link redirect to page
function getRedirectLinkToPage(obj, page) {
	var pageR = 'registerAfterLanding';
	if (page != null && page != 'undefined') {
		pageR = page;
	}

	var utm_source = getUrlValue('utm_source');
	var campaginid = getUrlValue('utm_campaign'); 
	var comb_id = getUrlValue('combid');
	var DP = getUrlValue('dp');
	var utm_medium = getUrlValue('utm_medium');
	var utm_content = getUrlValue('utm_content');
	
	var appStoreUrl =  host_url + "/appsflyerios?pid="+utm_source+"&c="+campaginid+"&af_siteid="+comb_id+"&af_sub1="+DP+"&af_sub2="+utm_medium+"&af_sub3="+utm_content;
	var googlePlayUrl = host_url + "/appsflyerandroid?pid="+utm_source+"&c="+campaginid+"&af_siteid="+comb_id+"&af_sub1="+DP+"&af_sub2="+utm_medium+"&af_sub3="+utm_content;
	var zh91Url = 'http://apk.91.com/Soft/Android/com.anyoption.android.app-19-6.html';
	var temp = replaceall(queryString, '=', '%3D');
	var temp2 = replaceall(temp, '&', '%26');
	var midParam = '%26mid%3D' + getMid();
	var url = "";

	if (isMobileDevice.iOS()) {
		url = appStoreUrl;
	} else if (isMobileDevice.Android()) {
		if (SKIN_ID_ZH == skinId) {
			url = zh91Url;
		} else {
			url = googlePlayUrl;
		}	
	} else { //is want to be pc.
		var pageR = 'registerAfterLanding';
		if (page != null && page != 'undefined' && page != 'register') {
			pageR = page;
		}
		
		url = host_url + 'jsp/paramAfterLanding.html?isNoForm=true&pageRedirect=' + pageR + '&' + queryString;	
	}
	obj.href= url;

}


function validCheck(val,reg){
	if(reg.test(val) == false){return false;}
	return true;
}
function intfn(){
	var contactForm = g('contactForm');
	if (contactForm != null) {
		contactForm .action = host_url + 'jsp/paramAfterLanding.html?' + queryString;
	}
	// START SETTING PHONE CODE
	// setPhoneCode();
}
////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
function validate(){
	var valid = true;
	if(g('first_name').value == ""){
		display_error(first_name_mandatory);
		g('first_name').parentNode.className += " error";
		valid = false;
		err_field = g('first_name');
	}
	else if(!validCheck(g('first_name').value,letters_regexp)){
		display_error(first_name_letters);
		g('first_name').parentNode.className += " error";
		valid = false;
		err_field = g('first_name');
	}
	else if(g('last_name').value == ""){
		display_error(last_name_mandatory);
		g('last_name').parentNode.className += " error";
		valid = false;
		err_field = g('last_name');
	}
	else if(!validCheck(g('last_name').value,letters_regexp)){
		display_error(last_name_letters);
		g('last_name').parentNode.className += " error";
		valid = false;
		err_field = g('last_name');
	}
	else if(g('email').value == "" && g('email').getAttribute("val-field") == null){
		display_error(email_mandatory);
		g('email').parentNode.className += " error";
		valid = false;
		err_field = g('email');
	}
	else if(g('email').getAttribute("val-field") == null && !validCheck(g('email').value,emailRegExp)){
		display_error(email_invalid);
		g('email').parentNode.className += " error";
		valid = false;
		err_field = g('email').parentNode;
	}
	else if(g('mobile_m').getAttribute("val-field") == null && g('mobile_m').value == ""){
		display_error(mobile_mandatory);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m').parentNode;
	}
	else if(g('mobile_m').getAttribute("val-field") == null && !validCheck(g('mobile_m').value,numberRegExp)){
		display_error(mobile_invalid_num);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m');
	}
	else if(g('mobile_m').getAttribute("val-field") == null && g('mobile_m').value.length < 7){
		display_error(mobile_min_length);
		g('mobile_m').parentNode.className += " error";
		valid = false;
		err_field = g('mobile_m');
	}
	
	if(valid){
		g('contactForm').submit();
	}
	else{
		return false;
	}
}


var isMobileDevice = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
/* END VALIDATION */