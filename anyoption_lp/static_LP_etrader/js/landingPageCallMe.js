/*********** landingPage.js ************/

//constants
var FORM_TYPE_CALL_ME	= 1;
var FORM_TYPE_LIGHTBOX	= 2;

var SKIN_ID_EN			= 2;
var SKIN_ID_TR 			= 3;
var SKIN_ID_AR 			= 4;
var SKIN_ID_ES 			= 5;
var SKIN_ID_DE 			= 8;
var SKIN_ID_IT 			= 9;
var SKIN_ID_RU 			= 10;
var SKIN_ID_FR 			= 12;
var SKIN_ID_EN_US		= 13;
var SKIN_ID_ES_US		= 14;
var SKIN_ID_ZH 			= 15;

var COMB_ID_EN_DEFUALT	= 22;

//error messages
var name_mandatory;
var name_letters;
var email_mandatory;
var email_invalid;
var mobile_mandatory;
var mobile_invalid_num;
var mobile_invalid_number;
var mobile_min_length;
var letters_regexp;
var form_type = FORM_TYPE_CALL_ME;
document.domain = "etrader.co.il";

//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
var dp = uri.queryKey['dp'];
if (null == skinId || 'undefined' == skinId) {
	skinId = SKIN_ID_EN;
}
if (null == combId ||  'undefined' == combId) {
	combId = COMB_ID_EN_DEFUALT;
}

// create link redirect to call me page
function redirectCallMe(obj, locale) {
	obj.href = 'call_' + locale + '.shtml?' + queryString;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var validate_fields = true;
var err_field;
var langCode;
var language;
var countryCode;
var phoneCode;
setLangCodeBySkinId(skinId);


// get lang code by skinId
function setLangCodeBySkinId(skinId) {
	//set default
	language = "English";
	//error messages
	name_mandatory = "שם שדה חובה";
	name_letters	 = "שם : הנתון שהוקלד חייב להכיל אותיות בלבד";
	email_mandatory 	 	= "email שדה חובה";
	email_invalid			= "email לא תקין";
	mobile_mandatory	 	= "טלפון נייד שדה חובה";
	mobile_invalid_num	 	= "טלפון נייד: הנתון שהוקלד אינו מספר תקין.";
	mobile_min_length 	 	= "טלפון נייד קצר מהמינימום המותר של 7 תווים";
	mobile_max_length 	 	= "טלפון נייד ארוך מהמקסימום המותר של 7 תווים";

	mobile_prefix_mandatory = "קידומת טלפון נייד שדה חובה";
	letters_regexp 		 	= "^['a-zA-Zא-ת ]+$";
}

function closeErrorBox(){
	$("#error_box").css("display", "none");
	$("#Overlay").css("display", "none");
    //set focus on the error field
    $(document).ready(function(){
    	err_field.focus();
    });
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

//flash submit
swfobject.registerObject("myId", "9.0.0", "swf/expressInstall.swf");
function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",5000);
	return false;
}

function submitForm() {
	$("#callMeForm").submit();
}

//display error msg
function display_error(msg) {
	//Remove from all elements the red border
	$(".error").removeClass("error");
	//Fill error msg
	var error_msg = document.getElementById("error_message_label");
	error_msg.innerHTML = msg;
	if (form_type == FORM_TYPE_CALL_ME) { //regular display error
		//Display error msg
		//var box = document.getElementById("error_box");
		//var overlay = document.getElementById("Overlay");
		//box.style.display = "block";
		//overlay.style.display = "block";
		//Move box to the center
		//box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
		//overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
	}
}

//diplay lightbox
function startBtn() {
	//Display error msg
	var box = document.getElementById("lightform");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

function flashStartTrading(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

//close light box
function closeLightBox(){
	document.getElementById("lightform").style.display = 'none';
	document.getElementById("Overlay").style.display = 'none';
}

function setFocusFirstInput() {
	window.scrollTo(0, 0);
	$("#name").focus();
}

////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
$(document).ready(function(){
	var downloadButton= document.getElementById('download');
   	if(downloadButton != null){
		downloadButton.href = getUrlToDownload();
	}
	/*var callMeButton = document.getElementById('callLink');
	if(callMeButton != null){
		callMeButton.href = getLinkForCallScreen();
	}*/
	//define email expression rule for validation
	$.validator.addMethod('email_regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//define regular expression rule for validation
	$.validator.addMethod('regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//validation implementation will go here.
	$("#callMeForm").validate({
		//if there is no error server validation for email
		submitHandler: function(form) {
			//parameters
		    var writerId = "200"; //mobile
		    skinId = 1;
		    var utcOffset = new Date().getTimezoneOffset();
			var request = new Object();
			request.name = document.getElementById("name").value;
			request.email = document.getElementById("email").value;
			request.mobilePhone = document.getElementById("mobilePhone").value;
			request.countryId = 1;
			request.timeFirstVisit = UTF8.decode(get_cookie("landing_firstVisit"));
			request.combinationId = combId;
			request.userAgent = navigator.userAgent;
			request.httpReferer = get_cookie("landing_hr");

			$.ajax({
				url: cdn_url_json + "jsonService/AnyoptionService/insertContact",
				type: "POST",  
				dataType: "json",  
				contentType: "json",  
		     	data: JSON.stringify({
					"skinId"  : skinId,
					"writerId" : writerId, 
					"contact" : { 
						"skinId"  			: skinId, 
						"utcOffset"			: utcOffset,
						"writerId"			: writerId,
						"name"				: request.name,
						"email"				: request.email,
						"phone"				: request.mobilePhone,
						"countryId"			: request.countryId,
						"combId"			: request.combinationId,
						"userAgent"			: request.userAgent,
						"httpReferer"		: request.httpReferer,
						"timeFirstVisitTxt"	:  request.timeFirstVisit,
						"dynamicParameter"  : dp,
						"marketingStaticPart" :get_cookie("smt"),
						"type" 				: FORM_TYPE_CALL_ME,
						"aff_sub1"			: aff_sub1,
						"aff_sub2"			: aff_sub2,
						"aff_sub3"			: aff_sub3											
					}
				}),
			 success: function(result){				
				if (result.errorCode == 0) {						
					if (get_cookie("landing_email") == null) {
						set_cookie("landing_email", UTF8.encode(request.email), 365, '/', cookie_domain, '');
					}
					if (result.contact.marketingStaticPart != null){
						set_cookie("smt", result.contact.marketingStaticPart, 16910, '/', cookie_domain, null);
					}
					var redirecrtURL = "success_" + getLangCodeBySkinId(skinId) + ".shtml?" + queryString;
					$(location).attr('href',redirecrtURL);
			  	} else { //error found				  		
			  		showError(result);							 
		  		}		   					    			       		    			           			      
			  },  
			  error: function(jqXHR, exception) {
				/*if (jqXHR.status === 0) {
					alert('Not connect.\n Verify Network.' + jqXHR.responseText + " " + exception);
				} else if (jqXHR.status == 404) {
					alert('Requested page not found. [404]' + jqXHR.responseText + " " + exception);
				} else if (jqXHR.status == 500) {
					alert('Internal Server Error [500].' + jqXHR.responseText + " " + exception);
				} else if (exception === 'parsererror') {
					alert('Requested JSON parse failed.' + jqXHR.responseText + " " + exception);
				} else if (exception === 'timeout') {
					alert('Time out error.' + jqXHR.responseText + " " + exception);
				} else if (exception === 'abort') {
					alert('Ajax request aborted.' + jqXHR.responseText + " " + exception);
				} else {
					alert('Uncaught Error.\n' + jqXHR.responseText);
				}*/
			  },
			  timeout: 10000  		    
		    });	
        	},
     	errorPlacement: function(error, element) {
		if (validate_fields) {
			validate_fields = false;
			err_field = element[0];
			display_error(error[0].innerHTML);
			$(element[0].parentNode).addClass( "error" );
		}
	}, onfocusout: false, onBlur: false,
			rules: {
             	name: {
					required: true,
					regexp: letters_regexp
            	},
                email: {
					required: true,
					email: true,
					email_regexp: "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$"
               	},
				mobilePhone: {
					required: true,
					number: true,
					minlength: 7
                }
             },
			messages: {
             	name: {
           			required: name_mandatory,
					regexp: name_letters
				},
                 email: {
     	        	required: email_mandatory,
					email: email_invalid,
					email_regexp: email_invalid
				},
				mobilePhone: {
					required: mobile_mandatory,
					number: mobile_invalid_num,
					minlength: mobile_min_length
               	}
			}
		});
	})
/* END VALIDATION */

function showError(result) {
	if (null != result.userMessages) {
		var message = result.userMessages[0].message;
		var field = result.userMessages[0].field;
	}
	err_field = document.getElementById(field);
	display_error(message);
	$(err_field.parentNode).addClass( "error" );
}

function getUrlValue(key){
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
    }
    return '';
}

function getUrlToDownload(){
	var utm_source = getUrlValue('utm_source');
	var campaginid = getUrlValue('utm_campaign'); 
	var comb_id = getUrlValue('combid');
	var DP = getUrlValue('dp');
	var utm_medium = getUrlValue('utm_medium');
	var utm_content = getUrlValue('utm_content');
	
	var appStoreUrl = 'http://www.etrader.co.il/appsflyerios?pid='+utm_source+'&c='+campaginid+'&af_siteid='+comb_id+'&af_sub1='+DP+'&af_sub2='+utm_medium+'&af_sub3='+utm_content;
	var googlePlayUrl = 'http://www.etrader.co.il/appsflyerandroid?pid='+utm_source+'&c='+campaginid+'&af_siteid='+comb_id+'&af_sub1='+DP+'&af_sub2='+utm_medium+'&af_sub3='+utm_content;
	
	if( /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		url = appStoreUrl;
	} else {
		url = googlePlayUrl;
	}
	return url;
}


function getUrlToGooglePlay(){	
	var googlePlayUrl = 'market://details?id=com.etrader.android.app&referrer=';
	var temp = replaceall(queryString, '=', '%3D');
	var temp2 = replaceall(temp, '&', '%26');
	var midParam = '%26mid%3D' + getMid();
	var url = googlePlayUrl + temp2 + midParam;
	return url;
}

function replaceall(str,replace,with_this) {
    var str_hasil ="";
    var temp;

    for(var i=0;i<str.length;i++) // not need to be equal. it causes the last change: undefined..
    {
        if (str[i] == replace)
        {
            temp = with_this;
        }
        else
        {
                temp = str[i];
        }

        str_hasil += temp;
    }

    return str_hasil;
}

function getLinkForCallScreen(){
	var url = "call_" + getLangCodeBySkinId(skinId) + ".shtml?s=" + skinId;
	if(dp != null){
		url += "&=" + dp;
	}
	 return url;
}

//get langCode by skin
function getLangCodeBySkinId(skinId) {
	//set default
	langCode = "en";
	switch (Number(skinId)) {
		case 1:
			langCode = "iw";
			break;
		case 2:
		case 13:
			langCode = "en";
			break;
		case 3:
			langCode = "tr";
			break;
		case 4:
			langCode = "ar";
			break;
		case 5:
		case 14:
			langCode = "es";
			break;
		case 8:
			langCode = "de";
			break;
		case 9:
			langCode = "it";
			break;
		case 10:
			langCode = "ru";
			break;
		case 12:
			langCode = "fr";
			break;
		case 15:
			langCode = "zh";
			break;
	}
	return langCode;
}