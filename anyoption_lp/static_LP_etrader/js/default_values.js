/*********** default_values.js ************/

//set default
//parse url
parseUri.options.strictMode = true;
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
var dp = uri.queryKey['dp'];
var affid = uri.queryKey['affid'];
var tradedoubler = uri.queryKey['tduid'];
var dfaPlaId = uri.queryKey['placmentId'];
var dfaCreid = uri.queryKey['creativeId'];
var dfaMacro = uri.queryKey['macro'];
var pageR = uri.queryKey['pageR'];
var httpRefferer;
var utmMedium = uri.queryKey['utm_medium'];
var utmSource = uri.queryKey['utm_source'];

var host = uri['host'];

var isLive = false;
if (host == "http://cdn.etrader.co.il" || host == "http://www.etrader.co.il") {
	isLive = true;
}

if (skinId == 'undefined' || skinId == null) {
	skinId = 1;
}
if (combId == 'undefined' || combId == null) {
	combId = 71;
}
if (get_cookie("dp") == null) {
	set_cookie("dp", dp, 365, '/', cookie_domain, '');
}
if (get_cookie("combid") == null) {
	set_cookie("combid", combId, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub1") == null) {
	set_cookie("aff_sub1", aff_sub1, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub2") == null) {
	set_cookie("aff_sub2", aff_sub2, 365, '/', cookie_domain, '');
}
if (get_cookie("aff_sub3") == null) {
	set_cookie("aff_sub3", aff_sub3, 365, '/', cookie_domain, '');
}

function get_cookie_val(flag,httpRef){
	var params;
	
	if(flag =='Static'){
        params = "MS{" + mId + "^CS{" + combId + "^HTTP{" + httpRef +  "^DP{" + dp +"^TS{" + getFullDate() + "^UTM{" + utmSource + "^aff_sub1{" + aff_sub1 + "^aff_sub2{" + aff_sub2 + "^aff_sub3{" + aff_sub3;
	} else {
		params =   "HTTP{" + httpRef + "^CD{" + combId + "^TD{" +getFullDate() + "^DP{" + dp + "^UTM{" + utmSource + "^aff_sub1{" + aff_sub1 + "^aff_sub2{" + aff_sub2 + "^aff_sub3{" + aff_sub3;
	}
	
	return params;	
}

function isLeadActivity(){
	
	var res = true;
	try {	
		if (utmMedium.toUpperCase() == 'POP_UNDER'){
			res = false
		}
	} catch(e) {
		res = true;
	}
	return res;	
}

function getClickCount(cookieName){
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	var count = 1;

	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// chech if contains 
		if ( cookie_name.indexOf(cookieName) != -1 )
		{
			count++;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	
	return count;
}


function set_cookie( name, value, expires, path, domain, secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );

	/*
	if the expires variable is set, make the correct
	expires time, the current script below will set
	it for x number of days, to make it for hours,
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );

	document.cookie = name + "=" + escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
	( ( path ) ? ";path=" + path : "" ) +
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}

// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function get_cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}


//UTF-8 encoding / decoding (object).
UTF8 = {
	encode: function(s) {
			for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
				s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
			);
			return s.join("");
	},
	decode: function(s) {
			for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
				((a = s[i][c](0)) & 0x80) &&
				(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
			);
			return s.join("");
	}
};

function getFullDate() {
	var resultDateTime;
	try {
		resultDateTime = serverDateTime.substr(1, serverDateTime.length);
		if(resultDateTime.length != 17){
			resultDateTime = getFullDateClientSide();
		}
		
	} catch(e) {
		resultDateTime = getFullDateClientSide();
	};
	
	return resultDateTime;
}

function getFullDateClientSide() {
	var date = new Date();
	var day = date.getUTCDate();
	var month = date.getUTCMonth() + 1; //January is 0.
	var year = date.getUTCFullYear() - 2000; //get year in 2 digits.
	var hour = date.getUTCHours(); //In GMT.
	var minutes = date.getUTCMinutes();
	var seconds = date.getUTCSeconds();
	//var AMorPM;
	//if (hour < 12) { AMorPM = "AM"; } else { AMorPM = "PM"; hour = hour - 12; }
	if (day < 10) {
		day = '0' + day;
	}
	if (month < 10) {
		month ='0' + month;
	}
	if (hour < 10) {
		hour = '0' + hour;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	//var today = day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ':' + seconds;
	var today = hour + ':' + minutes + ':' + seconds + ' ' + day + '/' + month + '/' + year;
	//var today = hour + ':' + minutes + ' ' + day + '/' + month + '/' + year;
	//var today = day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ' ' + AMorPM;
	return today;
}

function toHexString(str) {
    var tmp = UTF8.encode(str);
    var output = "";
    for (var i = 0; i < tmp.length; i++) {
        output = output + tmp.charCodeAt(i).toString(16);
    }
    return output;
}

function fromHexString(str) {
    temp = "";
    for (var i = 0; i < str.length; i = i + 2) {
        temp = temp + String.fromCharCode(parseInt(str.substring(i, i + 2), 16));
    }
    return UTF8.decode(temp);
}

function insertClick(staticPart, dinamicPart, redirectTimeout, redirectFnc) {
	var insClick = new InsClick(staticPart, dinamicPart, redirectTimeout, redirectFnc);
	function InsClick(staticPart, dinamicPart, redirectTimeout, redirectFnc) {
		this.staticPart = staticPart;
		this.dinamicPart = dinamicPart;
		this.redirectTimeout = redirectTimeout;
		this.redirectFnc = redirectFnc;

		this.ticker = function() {
			insClick.xmlHttp = getXMLHttp();
			if (null == insClick.xmlHttp) {
				return false;
			}
			insClick.xmlHttp.onreadystatechange = function() {
				if (insClick.xmlHttp.readyState == 4) {
					if (insClick.xmlHttp.responseText == "true") {
						// TODO: success
					} else {
						// TODO: fail
					}
					clearTimeout(redirectTimeout);
					redirectFnc();
				}
			}
			insClick.xmlHttp.open("POST", "/jsonService/AnyoptionService/insertMarketingTrackerClick", true);
			insClick.xmlHttp.setRequestHeader("Content-Type", "json");
			insClick.xmlHttp.send(JSON.stringify({"staticPart" : insClick.staticPart, "dynamicPart" : insClick.dinamicPart}));
		}
	}
	insClick.ticker();
}

 //Get Marketing Tracking MID from cookie
function getMid() {	
	var mid = "";
	try {
			var cookieParams = get_cookie("smt")
			cookieParams = fromHexString(cookieParams);	
			mid = cookieParams.substr(3,cookieParams.indexOf("^CS")-3);
		} catch(e) {
			mid = "";
		}
    return mid;
}	