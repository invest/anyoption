/*********** landingPage.js ************/


//get combId and skinId from cookie if not set default values
var fullURL = parent.document.URL;
var uri = parseUri(fullURL);
//url parameters
var combId = uri.queryKey['combid'];
var skinId = uri.queryKey['s'];
var charRestriction = 20;
var aff_sub1 = uri.queryKey['aff_sub1'];
if (aff_sub1) {
	aff_sub1 = aff_sub1.substr(0, charRestriction);
} else {
	aff_sub1 = '';
}
var aff_sub2 = uri.queryKey['aff_sub2'];
if (aff_sub2) {
	aff_sub2 = aff_sub2.substr(0, charRestriction);
} else {
	aff_sub2 = '';
}
var aff_sub3 = uri.queryKey['aff_sub3'];
if (aff_sub3) {
	aff_sub3 = aff_sub3.substr(0, charRestriction);
} else {
	aff_sub3 = '';
}
if (null == skinId || 'undefined' == skinId) {
	skinId = 1;
}
if (null == combId ||  'undefined' == combId) {
	combId = 71;
}

var queryString = fullURL.substring(fullURL.indexOf('?') + 1, fullURL.length);
var validate_fields = true;
var err_field;
var submit_form = false;

//error messages
var first_name_mandatory 	= "שם פרטי שדה חובה";
var first_name_letters	 	= "שם פרטי: הנתון שהוקלד חייב להכיל אותיות בלבד";
var last_name_mandatory		= "שם משפחה שדה חובה";
var last_name_letters		= "שם משפחה: הנתון שהוקלד חייב להכיל אותיות בלבד";
var email_mandatory 	 	= "email שדה חובה";
var email_invalid			= "email לא תקין";
var mobile_mandatory	 	= "טלפון נייד שדה חובה";
var mobile_invalid_num	 	= "טלפון נייד: הנתון שהוקלד אינו מספר תקין.";
var mobile_min_length 	 	= "טלפון נייד קצר מהמינימום המותר של 7 תווים";
var mobile_max_length 	 	= "טלפון נייד ארוך מהמקסימום המותר של 7 תווים";

var mobile_prefix_mandatory = "קידומת טלפון נייד שדה חובה";
var letters_regexp 		 	= "^['a-zA-Zא-ת ]+$";

function closeErrorBox(){
	$("#error_box").css("display", "none");
	$("#Overlay").css("display", "none");
    //set focus on the error field
    $(document).ready(function(){
    	err_field.focus();
    });
}

function getWidth() {
	return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
}

function focusError(){
	err_field.focus();
}

//flash submit
swfobject.registerObject("myId", "9.0.0", "swf/expressInstall.swf");
function flashSubmit(){
	validate_fields = true;
	//Fix problem with double click on the button
	setTimeout("submitForm();",500);
}

function submitForm() {
	$("#contactForm").submit();
}


//display error msg
function display_error(msg) {
	//Fill error msg
	var error_msg = document.getElementById("error_message_label");
	error_msg.innerHTML = msg;
	//Display error msg
	var box = document.getElementById("error_box");
	var overlay = document.getElementById("Overlay");
	box.style.display = "block";
	overlay.style.display = "block";
	//Move box to the center
	box.style.left = ((getWidth()/2)-(250 /2 )) + "px";
	overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + "px";
}

////////////////////////
/* STARTNG JQUERY VALIDATION */
var validate_fields = true;
var err_field;
$(document).ready(function(){
	// START SETTING PHONE CODE	
	//define email expression rule for validation
	$.validator.addMethod('email_regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//define regular expression rule for validation
	$.validator.addMethod('regexp', function(value, element, param) {
	       return this.optional(element) || value.match(param);
	   },
	   'This value doesn\'t match the acceptable pattern.');
	//validation implementation will go here.
	$("#contactForm").validate({
		//if there is no error server validation for email
		submitHandler: function(form) {
			if (submit_form == false) {
				submit_form = true;
			    $('form').get(0).setAttribute('action', host_url + 'jsp/paramAfterLanding.html?' + queryString);
				form.submit();
			}
			// Assign handlers immediately after making the request,
		    // and remember the jqxhr object for this request
			/*$.ajax({
			    type: 'GET',
			    url: host_url + "ajax.jsf?s=" + skinId  + "&landingEmailValidation=" + $('#email').val(),
			    contentType: 'application/json',
			    dataType: 'jsonp',
			    converters: {
			        'jsonp': jQuery.parseJSON,
			    },
			    success: function(data) {
					if (error_msg == "valid") {
						$('form').get(0).setAttribute('action', host_url + 'jsp/paramAfterLanding.jsf?' + queryString);
						form.submit();
					} else {
						err_field = $("#email")
						display_error(error_msg);
						$("#email").parent().addClass( "error" );
					}

			    },
			    error: function(jqXHR, textStatus, errorThrown) {
			    }
			});	*/
        },
     	errorPlacement: function(error, element) {
		if (validate_fields) {
			//reset all fields - remove red border from old field
			if (null != err_field && err_field != 'undefined') {
				var input_field = err_field;
				var input_width = 3;				
				if ($(err_field).is("select")) { //comboboxes
					input_field = err_field.parentNode.parentNode;				
					input_width = 2;
				}
	            input_field.style.border = 'none';
	           	$(input_field).width($(input_field).width() + input_width);
	            $(input_field).height($(input_field).height() + input_width);
	            
			}
			validate_fields = false;
			err_field = element[0];
			display_error(error[0].innerHTML);
			//mark field with red border
			var input_field = err_field; 
			var input_width = 3;									
	        if ($(err_field).is("select")) { //combobox
 				input_field = err_field.parentNode.parentNode;
				input_field = err_field.parentNode.parentNode;				
				input_width = 2;
	        } 
            input_field.style.border = '2px solid #990020';
           	$(input_field).width($(input_field).width() - input_width);
            $(input_field).height($(input_field).height() - input_width);	        	        	        
		}
	}, onfocusout: false, onBlur: false,
			rules: {
             	first_name: {
					required: true,
					regexp: letters_regexp
            	},
                last_name: {
                	required: true,
					regexp: letters_regexp
				},
                email: {
					required: true,
					email: true,
					email_regexp: "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$"
               	},
               	mobilePhonePref: {
	                required: true
	            },
				mobile_m: {
					required: true,
					number: true,
					minlength: 7,
					maxlength: 7
                }
			},
			messages: {
             	first_name: {
           			required: first_name_mandatory,
					regexp: first_name_letters
				},
				last_name: {
                 	required: last_name_mandatory,
                 	regexp: last_name_letters
				},
                 email: {
     	        	required: email_mandatory,
					email: email_invalid,
					email_regexp: email_invalid
				},
				mobilePhonePref: {
					required:mobile_prefix_mandatory
				},
				mobile_m: {
					required: mobile_mandatory,
					number: mobile_invalid_num,
					minlength: mobile_min_length,
					maxlength: mobile_max_length
               	}
			}
		});
	})
/* END VALIDATION */