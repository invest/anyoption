﻿import com.anyoption.Chart;
import com.anyoption.Investment;
import com.anyoption.ReturnRefundPair;

// this are the parameters the component expect from flashVars:
// pOpportunityId = ...;
// pMarketId = ...;
// pClassId = ...;
// pCalcGame = ...;

var LABEL_HEIGHT = 15;
var LABEL_FONT_SIZE = 9;
var LABEL_WIDTH = 60;
var LEVEL_COLOR_GREEN = 0x15A01F;
var LEVEL_COLOR_RED = 0xDF0000;
var LEVEL_COLOR_VOID = 0x000000; // black
var TOP_BOTTOM_OFFSET = 0.1;
var MIN_MAX_INITIAL_OFFSET = 0.0005;
var COLOR_RETURN_REFUND_HEADER = 0x0284d6;
var COLOR_RETURN_REFUND_TEXT = 0x093755;

// market settings
var marketName:String = "";
var timeFirstInvest:Date = null;
var timeLastInvest:Date = null;
var timeEstClosing:Date = null;
var decimalPoint:Number = 0;
var currencySign:String = "";
var currencyId:Number = 0;
var currencyLeftSymbol:Boolean = true;
var currencyDecimalPoint:Number = 2;
var skinId:Number = 0;
var loggedIn:Boolean = false;

// LS store vars
var currentLevelTxt:String = "";
var currentLevel:Number = 0;
var currentLevelColor:Number = 0;
var oddsWin:Number = 0;
var oddsLose:Number = 0;
var oddsGroup:String = "";
var invType:Number = 0;
var oppState:Number = 0;
var submitting:Boolean = false;
var assetLoading:Boolean = false;
var mute:Boolean = true;
var submittingSendSMS:Boolean = false;

//var textMyOptions:String;
var textBuyAnotherOption:String;
var textOptionDetails:String;
var textOptionConfirmation:String;
var textType:String;
var textAmount:String;
var textReturn:String;
var textExpiryAboveLevel:String;
var textExpiryBelowLevel:String;
var textBuy:String;
var textConfirmation:String;
var textWaitingForExpiry:String;
var textSendSMS:String;
var textSendSMSActive:String;
var textLoginLine1:String;
var textLoginLine2:String;
var textSuspendUpperLine:Array;
var textSuspendLowerLine:Array;
var textToday:String;
var textSelectorReturn:String;
var textSelectorRefund:String;
var textSelectorPercent:String;
var textSelectorPercentSlip:String;
var textSelectorReturnSelector:String;

var logging = false;
if (pClassId == 0 && pCalcGame == "false") {
	logging = true;
}

if (pCalcGame != "true") {
	calcLogo._x = -1000;
}

var labelsX:Array = new Array();
labelsX[0] = lblX0;
labelsX[1] = lblX1;
labelsX[2] = lblX2;
labelsX[3] = lblX3;
labelsX[4] = lblX4;
labelsX[5] = lblX5;
labelsX[6] = lblX6;
labelsX[7] = counterDown.lblX7;
labelsX[8] = counterDown.lblX8;

var MUTE_X = mcMute._x;
mcMute._x = -1000;
var BOX_CLOSING_LEVEL_X = boxClosingLevel._x;
boxClosingLevel._x = -1000;
errMsgClip._x = -1000;
counterDown._x = -1000;
var LBL_CLOCL_X = counterDown.lblClock._x;
counterDown.lblClock.text = "";
counterDown.lblClock._x = -1000;
var TIME_LEFT_X = counterDown.txtTimeLeftTo._x;
counterDown.txtTimeLeftTo._x = 10000;
var ERR_MSG_X = buyBoxErrMsg._x;
buyBoxErrMsg._x = 1000;
var ERR_MSG_LOGIN_X = buyBoxErrMsgLogin._x;
buyBoxErrMsgLogin._x = 1000;
zoom_mc._x = -2000;
zoom_mc.stop();
gradient._height = 0;
endArrowGray._y = -1000;
endArrowBlue._y = -1000;
var WAIT_X = wait._x;
wait._x = -1000;
logTextArea._x = -1000;
logTextArea.html = true;
if (!logging) {
	btnShowHideLog._x = -1000;
}
var lstAssetsSelectedIndex;
var LST_ASSETS_X = lstAssets._x;
lstAssets._x = -1000;
var LOADING_X = loadingClip._x;
var ASSETS_LOADING_X = assetsLoading._x;
assetsLoading._x = -1000;
assetsLoading.stop();
var CAPTION_PROFIT_LINE_X = captionProfitLine._x;
getPricePopup._x = -1000;
var BTN_DEPOSIT_X = btnDeposit._x;
btnDeposit._x = -1000;
var CAPTION_DEPOSIT_X = captionDeposite._x;
captionDeposite._x = -1000;
var OFFHOURS_X = offhours._x;
var OFFHOURS_MSG_X = offhours.offhoursMsg._x;
var OFFHOURS_SUSPENDED_X = offhours.suspendedMsg._x;
var OFFHOURS_SUSPENDED_BKG_X = offhours.suspendedBkg._x;
offhours._x = -1000;
var CURRENT_RETURN_INFO_POPUP_X = currentReturnInfoPopup._x;
currentReturnInfoPopup._x = -1000;
lstReturnRefund._visible = false;

callIconInstance._x = 1000;
putIconInstance._x = 1000;
invIconLevelPopup._x = 1000;

var returnRefundPairs:Array = null;

var closeSlipSuccessCounter:Number = 0;

writeToLog("Version " + VERSION);
writeToLog("Parameters - pOpportunityId: " + pOpportunityId + " pMarketId: " + pMarketId + " pClassId: " + pClassId + " pCalcGame: " + pCalcGame);

clearTexts();

var chart:Chart = null;
if (!optionPlus && profitLineZHFla) {
	chart = new Chart(mcPaintArea, mcPaintAreaBg, LABEL_WIDTH, 0, mcPaintArea._width - LABEL_WIDTH - 30, mcPaintArea._height, labelsX);
} else {
	chart = new Chart(mcPaintArea, mcPaintAreaBg, 0, 0, mcPaintArea._width, mcPaintArea._height, labelsX);
}
_root.onEnterFrame = function () {
	if (oppState != 6) {
		chart.timeTicker();
	}
};

keyListener = new Object();
keyListener.onKeyDown = function() {
	if (Key.getCode() == 13 && (_root.buyBoxInst._currentframe == 2 || _root.buyBoxInst._currentframe == 3)) {
		if (_root.buyBoxInst.amount_tf.text != "") {
			_root.submitInvestment();
		}
	}
};
Key.addListener(keyListener);

// start ---------------------- Lightstreamer -------------------------
var bridge = new JavaScriptBridge("flashObject");
bridge.onStatusChange = function(newStatus) {
	writeToLog("Lightstreamer engine status: " + newStatus);
};
var myTable = null;

function lightstreamerOnItemUpdate(et_ao_level, et_odds_win, et_odds_lose, et_state, et_clr, et_est_close, et_opp_id, ao_ts, et_suspended_message, odds_changed,et_oddsGroup) {
//	_root.writeToLog("lightstreamerOnItemUpdate");
	
	if (_root._currentframe == 1) {
		loadingClip._x = -1000;
		loadingClip.stop();
	}
	
	var oppNewState:Number = new Number(et_state);//itemUpdate.getNewValue(4)
	
	var ts:Date = new Date(new Number(ao_ts));
	if (logging && getPricePopup._x < 0) {
		_root.lblUpdateTimestamp.text = ts.getHours() + ":" + chart.padToTwoDigit(ts.getMinutes()) + ":" + chart.padToTwoDigit(ts.getSeconds()) + "." + ts.getMilliseconds() + " " + et_ao_level;
	}
	if (oppNewState >= 6) {
		_root.writeToLog("ts: " + _root.formatTimeForLog(ts) + " level: " + et_ao_level + " opp: " + et_opp_id + " state: " + oppNewState);
	}

	if (optionPlus && oppNewState == 7) {
		pOpportunityId = 0;
		reloadData();
		return;
	}

	if (oppState == 6) {
		// I didn't find a way to disconnect Lightstreamer feed
		// so once we get to state 6 just ignore the updates from there on
		return;
	}

	// make sure the update is for the opp we need. over aotps subscriptions in the
	// hour change there is a period when two opps exist
	if (et_opp_id != pOpportunityId) {
//		_root.writeToLog("ignore - et_opp_id: " + et_opp_id);
		return;
	}

	currentLevelTxt = et_ao_level;
	currentLevel = new Number(currentLevelTxt.replace(",", ""));
	if (currentLevel == 0 && oppNewState != 9) {
		wait._x = WAIT_X;
		return;
	} else {
		wait._x = -1000;
	}
	if (odds_changed || _root.oddsWin == 0) {
		_root.writeToLog("Changing odds.");
		oddsWin = new Number(et_odds_win);
		oddsLose = new Number(et_odds_lose);
		oddsGroup = et_oddsGroup;
		fillReturnRefuntSelect();
		_root.writeToLog("Changing odds done. _root.oddsWin: " + _root.oddsWin + " _root.oddsLose: " + _root.oddsLose);
	}
	
	currentLevelColor = new Number(et_clr);
//	timeEstClosing = et_est_close;
	if (oppNewState.valueOf() != oppState.valueOf()) {
		_root.writeToLog("State changed from " + oppState + " to " + oppNewState);
		if (oppState == 9) {
			offhours._x = -1000;
			buyBoxInst.gotoAndStop(1);
			if (optionPlus) {
				_root.listCover._x = -1000;
			}
		}
		oppState = oppNewState;
		if (optionPlus) {
			var i;
			for (i = 0; i < lstInvestments.length; i++) {
				if (oppNewState == 2 || oppNewState == 3) {
					var obj = _root.lstInvestments.getItemAt(i);
					if (obj.lblMarket._visible) {
						obj.sellButton._visible = true;
					}
				} else {
					_root.lstInvestments.getItemAt(i).sellButton._visible = false;
				}
			}
		}
		if (oppNewState == 3) {
			counterDown.lblClock.text = "";
//			if (chart.getInvestments().length > 0) {
			var inva:Array = chart.getInvestments();
			var i:Number;
			for (i = 0; i < inva.length; i++) {
				if (inva[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
					counterDown.txtTimeLeftTo._x = TIME_LEFT_X;
					break;
				}
			}
			chart.init();
		} else if (oppNewState == 4 || oppNewState == 5) {
			if (!submitting) {
				buyBoxInst.gotoAndStop(5);
			}
			counterDown.lblClock.text = "";
			counterDown.lblClock._x = -1000;
			counterDown.txtTimeLeftTo._x = 10000;
			chart.init();
		} else if (oppNewState == 6) {
			boxClosingLevel._x = BOX_CLOSING_LEVEL_X;
			boxClosingLevel.lblClosingLevel.text = currentLevelTxt;
			if (currentLevelColor == 2) {
				boxClosingLevel.lblClosingLevel.textColor = LEVEL_COLOR_GREEN;
			} else if (currentLevelColor == 1) {
				boxClosingLevel.lblClosingLevel.textColor = LEVEL_COLOR_VOID;
			} else {
				boxClosingLevel.lblClosingLevel.textColor = LEVEL_COLOR_RED;
			}
			buyBoxInst.gotoAndStop(7);
			
			_root.loadBalance();
			chart.init();
			
//			startReloadAfterCloseCountdown();
		} else if (oppNewState == 9) { // suspended
			_root.writeToLog("Suspending...");
			var msg:String = et_suspended_message;
			_root.writeToLog("Suspend with message: " + msg);
			var arr:Array = msg.split("s");
			_root.writeToLog("Suspend: " + arr[0] + " " + arr[1] + " arr.length: " + arr.length);
			msg = "";
			if (textSuspendUpperLine[new Number(arr[0]) - 1] != undefined) {
				msg = "<b>" + textSuspendUpperLine[new Number(arr[0]) - 1] + "</b>";
			}
			if (textSuspendLowerLine[new Number(arr[1]) - 1] != undefined && skinId > 1) { // no second line and time in eTrader
				msg = msg + "<br/><b>" + textSuspendLowerLine[new Number(arr[1]) - 1] + "</b>";
			}
			if (!isNull(arr[2]) && arr[2] != undefined && arr[2] != "" && skinId > 1) { // no second line and time in eTrader
				msg = msg + " <b>" + formatDate(parseDate(arr[2])) + "</b>";
			}
			offhours.suspendedMsg.setNewTextFormat(offhours.suspendedMsg.getTextFormat(1, 2));
			offhours.suspendedMsg.html = true;
			offhours.suspendedMsg.htmlText = msg;
			
			offhours.offhoursMsg._x = -1000;
			if (optionPlus) {
				offhours.offhoursMsgClick._x = -1000;
			}
			offhours.suspendedMsg._x = OFFHOURS_SUSPENDED_X;
			offhours.suspendedBkg._x = OFFHOURS_SUSPENDED_BKG_X;
			offhours._x = OFFHOURS_X;
			buyBoxInst.gotoAndStop(8);
			if (optionPlus) {
				_root.closeGetPricePopup();
				listCover._x = LIST_COVER_X;
			}
		}
	}
	
	if (currentLevel != 0) {
		chart.setCurrentLevel(currentLevel);
		_root.setSlipLevelAndColor();
	}
	
	if (buyBoxInst._currentframe == 2 || buyBoxInst._currentframe == 3) {
		// if odds changed to recalc the win/lose
		buyBoxInst.amount_tf.onChanged();
	}

	if (getInvestmentPriceAfterMarketSwitch != undefined) {
		_root.writeToLog("getInvestmentPriceAfterMarketSwitch: " + getInvestmentPriceAfterMarketSwitch);
		_root.lstInvestments.selectedIndex = getInvestmentPriceAfterMarketSwitch;
		getInvestmentPriceAfterMarketSwitch = undefined;
		_root.getOptionPlusSellPrice();
	}
};

function setSlipLevelAndColor():Void {
	if (!_root.submitting && _root.buyBoxInst._currentframe != 4 && _root.buyBoxInst._currentframe != 6) {
		_root.buyBoxInst.curr_level_tf.text = currentLevelTxt;
		if (_root.currentLevelColor == 2) {
			_root.buyBoxInst.curr_level_tf.textColor = _root.LEVEL_COLOR_GREEN;
		} else if (currentLevelColor == 1) {
			_root.buyBoxInst.curr_level_tf.textColor = _root.LEVEL_COLOR_VOID;
		} else {
			_root.buyBoxInst.curr_level_tf.textColor = _root.LEVEL_COLOR_RED;
		}
	}
};

function connectLightstreamer():Void {
	_root.writeToLog("connectLightstreamer...");
	
	_root.writeToLog("ExternalInterface.available: " + ExternalInterface.available);
	var addResult:Boolean = Boolean(ExternalInterface.call("optionPlusAddTable", pMarketId, opportunityTypeId, optionPlus, "" + skinId));
	
	
	// var lsGroup:String = "";
	// if (optionPlus) {
		// lsGroup = "op_" + pMarketId;
	// } else {
		// lsGroup = "aotps_1_" + pMarketId;
	// }
	// var lsSchema:String = "";
	// if (skinId == 1) {
		// lsSchema = "ET_LEVEL ET_ODDS_WIN ET_ODDS_LOSE ET_STATE ET_CLR ET_EST_CLOSE ET_OPP_ID AO_TS ET_SUSPENDED_MESSAGE";
	// } else {
		// lsSchema = "AO_LEVEL ET_ODDS_WIN ET_ODDS_LOSE ET_STATE ET_CLR ET_EST_CLOSE ET_OPP_ID AO_TS ET_SUSPENDED_MESSAGE";
	// }
	// if (!optionPlus) {
		// lsSchema = lsSchema + " ET_ODDS_GROUP";
	// }
	// _root.writeToLog("lsGroup: " + lsGroup + " lsSchema: " + lsSchema);
	// myTable = new FlashTable(lsGroup, lsSchema, "COMMAND");
	// myTable.setDataAdapter("JMS_ADAPTER");
	// myTable.setSnapshotRequired(true);
	// myTable.setRequestedMaxFrequency(1);
	// myTable.onItemUpdate = lightstreamerOnItemUpdate;
	// bridge.addTable(myTable, "FlashTable1");
	// bridge.bind();
	
	//notify the page that we are ready
	_root.writeToLog("connectLightstreamer done: " + addResult);

	if (!addResult) {
		_root.writeToLog("will retry...");
		setTimeout(connectLightstreamer, 1000);
	}
};


// end   ---------------------- Lightstreamer -------------------------

//lstInvestments.backgroundColor = 0xD3D3D3;
//lstInvestments.backgroundDisabledColor = 0x000000;
if (_root.skinId == 1){
	lstInvestments.selectionColor = 0xc0c0c0;
	lstInvestments.color = 0x282828;
}else{
	lstInvestments.selectionColor = 0xE1E1E1;
}
lstInvestments.textSelectedColor = 0x000000;
//lstInvestments.color = 0xFFFFFF;
lstInvestments.fontSize = 10;
lstInvestments.useRollOver = false;
lstInvestments.vScrollPolicy = "auto";
//lstInvestments.textAlign = "right";
lstInvestments.setStyle("borderStyle", "none");
if (!optionPlus) {
	lstInvestments.iconFunction = function(item:Object):String {
		if (item.data == "call") {
			return "callIcon";
		}
		if (item.data == "put") {
			return "putIcon";
		}
	};
}

lstInvestments.change = function(evt:Object) {
	if (!optionPlus || !soldInvestmentId > 0) {
		chart.setSelectedInList(evt.target.selectedIndex);
		chart.repaint();
		if (chart.getInvestments()[_root.lstInvestments.selectedIndex].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
			zoom_mc.gotoAndPlay(1);
		}
	}
	if (optionPlus) {
		closeGetPricePopup();
	}
//	setTimeout(clearListSelectionHighlight, 1000);
};
lstInvestments.addEventListener("change", lstInvestments);

function clearListSelectionHighlight():Void {
	chart.setSelectedInList(undefined);
	chart.repaint();
};

function handleInvestmentsListScroll(evt:Object) {
	if (getPricePopup._x > 0) {
		positionGetPricePopup();
	}
};


if (optionPlus) {
	if(etrader == true){
		lstInvestments.rowHeight = 30;
	}else{
		lstInvestments.rowHeight = 40;
	}
	lstInvestments.cellRenderer = "StopLossCellRenderer";
	lstInvestments.scroll = handleInvestmentsListScroll;
	lstInvestments.addEventListener("scroll", lstInvestments);
};

function logHttpStatus(httpStatus:Number):Void {
	_root.writeToLog("HTTP Status: " + httpStatus);
};

function logResponseBody(src:String):Void {
	_root.writeToLog("Server Response: " + src);
	if (src == undefined) {
		this.onLoad(false);
	} else {
		this.parseXML(src);
		this.loaded = true;
		this.onLoad(true);
	}
};

function loadData():Void {
	_root.writeToLog("Loading data...");
	var dataXml:XML = new XML();
	dataXml.ignoreWhite = true;
	dataXml.onHTTPStatus = logHttpStatus;
//	dataXml.onData = logResponseBody;
	dataXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading data " + success);
		if (success) {
			loadSettings(dataXml.childNodes[0].childNodes[1]);
			if (shouldLoadTexts) {
				loadTexts(dataXml.childNodes[0].childNodes[0]);
			}
			if (pOpportunityId > 0) {
				chart.parseInvestments(dataXml.childNodes[0].childNodes[2]);
				chart.parseRates(dataXml.childNodes[0].childNodes[3]);
				chart.init();
				if (!optionPlus) {
					parseReturnRefunPairs(dataXml.childNodes[0].childNodes[4]);
				}

				connectLightstreamer();
			}
		} else {
			errMsgClip._x = 306;
			_root.writeToLog("Loading investments failed - status: " + dataXml.status);
		}
	}
	dataXml.load(requestsPath + "/jsp/xml/profitLineData.xml?marketId=" + pMarketId + "&oppId=" + pOpportunityId + "&typeId=" + (optionPlus ? 3 : 1) + "&t=" + getRandomParam());
};

function clearTexts():Void {
	captionTotalInvestments.text = "";
	captionTotalReturn.text = "";
	captionBalance.text = "";
	captionDeposite.text = "";
	captionInTheMoney.text = "";
	captionOutOfTheMoney.text = "";
	captionOptionLevel.text = "";
	captionCurrentLevel.text = "";
	buyBoxInst.captionMyOptions.text = "";
	
	if (buyBoxInst._currentframe == 1) {
		buyBoxInst.captionBuyAnotherOption.text = "";
	}
	if (optionPlus) {
		clearTextsOptionPlus(xmlNode);
	}
};

function loadTexts(xmlNode:XMLNode):Void {
	captionTotalInvestments.text = xmlNode.childNodes[0].childNodes[0].nodeValue;
	captionTotalReturn.text = xmlNode.childNodes[1].childNodes[0].nodeValue;
	if (positionCurrentReturn == true) {
		var capWidth = captionTotalReturn.getNewTextFormat().getTextExtent(captionTotalReturn.text).width;
		if(_root.skinId != 1){
			currentReturnInfo._x = captionTotalReturn._x + captionTotalReturn._width - capWidth - currentReturnInfo._width - 6;
			CURRENT_RETURN_INFO_POPUP_X = currentReturnInfo._x + currentReturnInfo._width / 2;
		}
		currentReturnInfoPopup.gotoAndStop(skinId);
	}
	captionBalance.text = xmlNode.childNodes[2].childNodes[0].nodeValue;
	captionDeposite.text = xmlNode.childNodes[3].childNodes[0].nodeValue;
	captionInTheMoney.text = xmlNode.childNodes[4].childNodes[0].nodeValue;
	captionOutOfTheMoney.text = xmlNode.childNodes[5].childNodes[0].nodeValue;
	captionOptionLevel.text = xmlNode.childNodes[6].childNodes[0].nodeValue;
	captionCurrentLevel.text = xmlNode.childNodes[7].childNodes[0].nodeValue;
	boxClosingLevel.captionExpiryLevel.text = xmlNode.childNodes[8].childNodes[0].nodeValue;
	counterDown.txtTimeLeftTo.captionTimeLeftToInvest.text = xmlNode.childNodes[11].childNodes[0].nodeValue;
	buyBoxInst.captionMyOptions.text = xmlNode.childNodes[9].childNodes[0].nodeValue;
	
	textBuyAnotherOption = xmlNode.childNodes[10].childNodes[0].nodeValue;
	textOptionDetails = xmlNode.childNodes[12].childNodes[0].nodeValue;
	textOptionConfirmation = xmlNode.childNodes[19].childNodes[0].nodeValue;
	textType = xmlNode.childNodes[13].childNodes[0].nodeValue;
	textAmount = xmlNode.childNodes[14].childNodes[0].nodeValue;
	textReturn = xmlNode.childNodes[15].childNodes[0].nodeValue;
	textExpiryAboveLevel = xmlNode.childNodes[16].childNodes[0].nodeValue;
	textExpiryBelowLevel = xmlNode.childNodes[17].childNodes[0].nodeValue;
	textBuy = xmlNode.childNodes[18].childNodes[0].nodeValue;
	textConfirmation = xmlNode.childNodes[20].childNodes[0].nodeValue;
	textWaitingForExpiry = xmlNode.childNodes[21].childNodes[0].nodeValue;
	textSendSMS = xmlNode.childNodes[22].childNodes[0].nodeValue;
	textSendSMSActive = xmlNode.childNodes[23].childNodes[0].nodeValue;
	textLoginLine1 = xmlNode.childNodes[24].childNodes[0].nodeValue;
	textLoginLine2 = xmlNode.childNodes[25].childNodes[0].nodeValue;
	offhours.offhoursMsg.setNewTextFormat(offhours.offhoursMsg.getTextFormat(1, 2));
	offhours.offhoursMsg.text = xmlNode.childNodes[26].childNodes[0].nodeValue;
	textSuspendUpperLine = new Array();
	textSuspendUpperLine[0] = xmlNode.childNodes[27].childNodes[0].nodeValue;
	textSuspendUpperLine[1] = xmlNode.childNodes[28].childNodes[0].nodeValue;
	textSuspendUpperLine[2] = xmlNode.childNodes[29].childNodes[0].nodeValue;
	textSuspendUpperLine[3] = xmlNode.childNodes[30].childNodes[0].nodeValue;
	textSuspendLowerLine = new Array();
	textSuspendLowerLine[0] = xmlNode.childNodes[31].childNodes[0].nodeValue;
	textSuspendLowerLine[1] = xmlNode.childNodes[32].childNodes[0].nodeValue;
	textSuspendLowerLine[2] = xmlNode.childNodes[33].childNodes[0].nodeValue;
	textSuspendLowerLine[3] = xmlNode.childNodes[34].childNodes[0].nodeValue;
	textToday = xmlNode.childNodes[35].childNodes[0].nodeValue;
	if (currentReturnInfoPopup != undefined && currentReturnInfoPopup.infoText != undefined) {
		currentReturnInfoPopup.infoText.setNewTextFormat(currentReturnInfoPopup.getTextFormat(1, 2));
		currentReturnInfoPopup.infoText.html = true;
		currentReturnInfoPopup.infoText.htmlText = xmlNode.childNodes[36].childNodes[0].nodeValue;
	}
	if (buyBoxInst._currentframe == 1) {
		buyBoxInst.captionBuyAnotherOption.text = textBuyAnotherOption;
	}
	textSelectorReturn = xmlNode.childNodes[37].childNodes[0].nodeValue;
	textSelectorRefund = xmlNode.childNodes[38].childNodes[0].nodeValue;
	textSelectorPercent = xmlNode.childNodes[39].childNodes[0].nodeValue;
	textSelectorReturnSelector = xmlNode.childNodes[40].childNodes[0].nodeValue;
	textSelectorPercentSlip = xmlNode.childNodes[41].childNodes[0].nodeValue;

	if (optionPlus) {
		loadTextsOptionPlus(xmlNode);
	}
};

function loadSettings(xmlNode:XMLNode):Void {
	chart.setServerTimeOffset(chart.calcPeriodInSec(new Date(), new Date(new Number(xmlNode.childNodes[0].childNodes[0].nodeValue))));
	chart.setTimezoneOffset(new Number(xmlNode.childNodes[1].childNodes[0].nodeValue));
	currencySign = xmlNode.childNodes[2].childNodes[0].nodeValue;
	_root.lblBalance.text = xmlNode.childNodes[3].childNodes[0].nodeValue;
	skinId = new Number(xmlNode.childNodes[4].childNodes[0].nodeValue);
	pMarketId = xmlNode.childNodes[5].childNodes[0].nodeValue;
	pOpportunityId = xmlNode.childNodes[6].childNodes[0].nodeValue;
	loggedIn = xmlNode.childNodes[7].childNodes[0].nodeValue == "1" ? true : false;
	if (pOpportunityId > 0) {
		marketName = xmlNode.childNodes[8].childNodes[0].nodeValue;
		timeFirstInvest = new Date(new Number(xmlNode.childNodes[9].childNodes[0].nodeValue));
		chart.fixTimeZone(timeFirstInvest);
		timeLastInvest = new Date(new Number(xmlNode.childNodes[10].childNodes[0].nodeValue));
		chart.fixTimeZone(timeLastInvest);
		timeEstClosing = new Date(new Number(xmlNode.childNodes[11].childNodes[0].nodeValue));
		chart.fixTimeZone(timeEstClosing);
		decimalPoint = new Number(xmlNode.childNodes[12].childNodes[0].nodeValue);
		currencyId = new Number(xmlNode.childNodes[13].childNodes[0].nodeValue);
		currencyLeftSymbol = xmlNode.childNodes[14].childNodes[0].nodeValue == "true" ? true : false;
		currencyDecimalPoint = new Number(xmlNode.childNodes[15].childNodes[0].nodeValue);
	} else {
		currencyId = new Number(xmlNode.childNodes[8].childNodes[0].nodeValue);
		currencyLeftSymbol = xmlNode.childNodes[9].childNodes[0].nodeValue == "true" ? true : false;
		currencyDecimalPoint = new Number(xmlNode.childNodes[10].childNodes[0].nodeValue);
	}
	if (currencyId == 6) { // RUB
		OPTION_PLUS_FEE = OPTION_PLUS_FEE_RUB;
	}
	if (currencyId == 7) { // CNY
		OPTION_PLUS_FEE = OPTION_PLUS_FEE_YUAN;
	}
	if (currencyId == 8) { // KRW
		OPTION_PLUS_FEE = OPTION_PLUS_FEE_WON;
	}
	_root.writeToLog(
		"Settings - timeFirstInvest: " + timeFirstInvest +
		" timeLastInvest: " + timeLastInvest +
		" timeEstClosing: " + timeEstClosing +
		" decimalPoint: " + decimalPoint +
		" currencySign: " + currencySign +
		" currencyDecimalPoint: " + currencyDecimalPoint +
		" skinId: " + skinId +
		" pMarketId: " + pMarketId +
		" pOpportunityId: " + pOpportunityId +
		" loggedIn: " + loggedIn);

	if (pOpportunityId == 0) {
		if (pMarketId == 0) {
			offhours.offhoursMsg._x = OFFHOURS_MSG_X;
			if (optionPlus) {
				offhours.offhoursMsgClick._x = OFFHOURS_MSG_CLICK_X;
			}
			offhours.suspendedMsg._x = -1000;
			offhours.suspendedBkg._x = -1000;
			offhours._x = OFFHOURS_X;
			lblOpportunityCaption.text = "";
		} else {
			_root.writeToLog("Trying to get opened market...");
			pMarketId = 0;
			reloadData();
		}
	} else {
		lblOpportunityCaption.text = marketName + " - " + chart.formatToString(timeEstClosing, false);
	}
	if (loggedIn) {
		if (_root.mcMute._currentframe == 1) {
			mute = false;
		}
		mcMute._x = MUTE_X;
		
		btnDeposit._x = BTN_DEPOSIT_X;
		captionDeposite._x = CAPTION_DEPOSIT_X;
	}
	if (optionPlus) {
		loadSettingsOptionPlus(xmlNode);
	}
};

function parseReturnRefunPairs(xmlNode:XMLNode):Void {
	var pairsInXML:Number = xmlNode.childNodes.length;
	_root.writeToLog("pairsInXML: " + pairsInXML);
	returnRefundPairs = new Array();
	if (pairsInXML > 0) {
		var i:Number = 0;
		for (i = 0; i < pairsInXML; i++) {
			var cp:ReturnRefundPair = new ReturnRefundPair();
			cp.setId(new Number(xmlNode.childNodes[i].childNodes[0].childNodes[0].nodeValue));
			cp.setReturnValue(new Number(xmlNode.childNodes[i].childNodes[1].childNodes[0].nodeValue));
			cp.setRefundValue(new Number(xmlNode.childNodes[i].childNodes[2].childNodes[0].nodeValue));
			returnRefundPairs[returnRefundPairs.length] = cp;
		}
	}
	_root.writeToLog("returnRefundPairs.length: " + returnRefundPairs.length);
};

//var requestsPath:String = _root._url;
//var requestsDomain:String = _root._url;
//if (requestsPath.substr(0, 14) == "http://images.") {
//	requestsPath = "http://www." + requestsPath.substr(14);
//	requestsDomain = "www." + requestsDomain.substr(14);
//}
//if (requestsPath.substr(0, 15) == "https://images.") {
//	requestsPath = "https://www." + requestsPath.substr(15);
//	requestsDomain = requestsDomain.substr(15);
//}
//if (requestsPath.indexOf("?") != -1) {
//	requestsPath = requestsPath.substr(0, requestsPath.indexOf("?"));
//	requestsDomain = requestsDomain.substr(0, requestsDomain.indexOf("?"));
//}
//if (requestsPath.indexOf("/images") != -1) { // drop the "/images/aa_b/xxxx.swf"
//	requestsPath = requestsPath.substr(0, requestsPath.indexOf("/images"));
//} else if (requestsPath.lastIndexOf("/") != -1) { // drop the "/aa_b/xxxx.swf"
//	requestsPath = requestsPath.substr(0, requestsPath.lastIndexOf("/")); // drop the /xxxx.swf
//	requestsPath = requestsPath.substr(0, requestsPath.lastIndexOf("/")); // drop the /aa_b
//}
//if (requestsDomain.indexOf("/") != -1) {
//	requestsDomain = requestsDomain.substr(0, requestsDomain.indexOf("/"));
//}
//if (requestsDomain.indexOf(":") != -1) {
//	requestsDomain = requestsDomain.substr(0, requestsDomain.indexOf(":"));
//}
var requestsPath:String = String(ExternalInterface.call("getFlashRequestPath"));
_root.writeToLog("Requests path: " + requestsPath);
//_root.writeToLog("Requests domain: " + requestsDomain);
//System.security.allowDomain(requestsDomain);
//_root.writeToLog("Requests domain: *");
System.security.allowDomain("*");

loadData();

function loadBalance():Void {
	_root.writeToLog("Loading balance...");
	var balanceXml:XML = new XML();
	balanceXml.ignoreWhite = true;
	balanceXml.onHTTPStatus = logHttpStatus;
//	balanceXml.onData = logResponseBody;
	balanceXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading balance " + success);
		if (success) {
			_root.lblBalance.text = balanceXml.childNodes[0].childNodes[0].nodeValue;
		} else {
			_root.writeToLog("Loading balance failed " + balanceXml.status);
			if (!chart.getInvestmentsLoaded()) {
				errMsgClip._x = 306;
			}
		}
	}
	balanceXml.load(requestsPath + "/jsp/xml/profitLineBalance.xml?t=" + getRandomParam());
};

function submitInvestment():Void {
	if (_root.buyBoxInst.amount_tf.text == "" || _root.buyBoxInst.amount_tf.text == ".") {
		_root.buyBoxInst.amount_tf.text = "0";
	}
	if (submitting || !currentLevel > 0) {
		return;
	}
	submitting = true;
	var BUY_BTN_X = _root.buyBoxInst.buy_btn._x;
	_root.buyBoxErrMsg._x = 1000;
	_root.buyBoxErrMsgLogin._x = 1000;
	_root.buyBoxInst.buy_btn._x = -1000;
	_root.buyBoxInst.captionBuy._x = -1000;
	_root.buyBoxInst.buyBtnBg._x = -1000;
	_root.buyBoxInst.slip_waiting_mc.play();
	
	var invSubmit:LoadVars = new LoadVars();
	invSubmit.flashInvSubmit = "1";
	invSubmit.oppId = _root.pOpportunityId;
	invSubmit.amount = _root.buyBoxInst.amount_tf.text;
	invSubmit.slipEntryLevel = _root.currentLevel;
	invSubmit.slipEntryLevelTxt = _root.currentLevelTxt;
	invSubmit.slipEntryOddWin = _root.oddsWin;
	invSubmit.slipEntryOddLose = _root.oddsLose;
	invSubmit.choice = _root.invType;
	invSubmit.all = "0";
	invSubmit.utcOffset = new Date().getTimezoneOffset();
	invSubmit.t =  getRandomParam();
	invSubmit.onHTTPStatus = function(httpStatus:Number) {
	    writeToLog("HTTP status is: " + httpStatus);
	};
	invSubmit.onLoad = function(success:Boolean) {
		if (success) {
			writeToLog("Inv Submit Success - " + invSubmit.reply);
			if (substring(invSubmit.reply, 1, 2) == "OK") {
				var sendSMS:Boolean = substring(invSubmit.reply, invSubmit.reply.length, 1) == "1";
				writeToLog(substring(invSubmit.reply, invSubmit.reply.length, 1) + " " + sendSMS);
				var ninv:Investment = new Investment();
				ninv.setId(new Number(substring(invSubmit.reply, 3, invSubmit.reply.length - 7)));
				ninv.setMarket(_root.marketName);
				var ct:Date = new Date();
				chart.fixTime(ct);
//				chart.addSeconds(ct, chart.getServerTimeOffset());
				ninv.setTimeCreated(ct);
				ninv.setLevelTxt(invSubmit.slipEntryLevelTxt);
				ninv.setLevel(invSubmit.slipEntryLevel);
				ninv.setAmount(new Number(invSubmit.amount) + (_root.optionPlus ? _root.OPTION_PLUS_FEE : 0));
				ninv.setAmountTxt(formatAmount(ninv.getAmount()));
				ninv.setRollUpAmount(0);
				ninv.setType(invSubmit.choice);
				ninv.setOddsWin(new Number(invSubmit.slipEntryOddWin) - 1); // in the Investment we store the odds
				ninv.setOddsLose(1 - new Number(invSubmit.slipEntryOddLose));
				ninv.setMarketId(_root.pMarketId);
				ninv.setOppId(_root.pOpportunityId);
				ninv.setTimeEstClosing(_root.timeEstClosing);
				var invs:Array = chart.getInvestments();
				invs[invs.length] = ninv;

				var itemLabel:String = ninv.getLevelTxt() + "     " + chart.formatToString(ninv.getTimeCreated(), false) + "     " + ninv.getAmountTxt();
				var itemData:String = ninv.getType() == 1 ? "call" : "put";
				lstInvestments.addItem({label:itemLabel, data:itemData, amount:ninv.getAmountTxt(), market:_root.marketName, time:chart.formatToString(_root.timeEstClosing, false), level:ninv.getLevelTxt()});

				if (oppState == 3) {
					counterDown.txtTimeLeftTo._x = TIME_LEFT_X;
				}
				chart.init();
				if (ninv.getType() == 1) {
					_root.buyBoxInst.gotoAndStop(6);
					_root.buyBoxInst.captionExpiryAbovePercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round(ninv.getOddsWin() * 100));
					_root.buyBoxInst.captionExpiryBelowPercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round((1 - ninv.getOddsLose()) * 100));
				} else {
					_root.buyBoxInst.gotoAndStop(4);
					_root.buyBoxInst.captionExpiryAbovePercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round((1 - ninv.getOddsLose()) * 100));
					_root.buyBoxInst.captionExpiryBelowPercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round(ninv.getOddsWin() * 100));
				}
				_root.buyBoxInst.lblInvestmentId.text = ninv.getId();
				_root.buyBoxInst.lblInvestmentLvl.text = ninv.getLevelTxt();
				_root.buyBoxInst.lblInvestmentType.text = ninv.getType() == 1 ? "call" : "put";
				_root.buyBoxInst.lblInvestmentAmount.text = ninv.getAmountTxt();
				_root.buyBoxInst.lblInvestmentWin.text = formatAmount((ninv.getAmount() - (_root.optionPlus ? _root.OPTION_PLUS_FEE : 0)) * (1 + ninv.getOddsWin()));
				_root.buyBoxInst.lblInvestmentLose.text = formatAmount((ninv.getAmount() - (_root.optionPlus ? _root.OPTION_PLUS_FEE : 0)) * (1 - ninv.getOddsLose()));
				if (!sendSMS || optionPlus) {
					_root.buyBoxInst.chbSendSMS._x = -1000;
					_root.buyBoxInst.lblSendSMS._x = -1000; // for Arabic only
					_root.buyBoxInst.lblSendSmsSuccess._x = -1000;
				}
				
				_root.startCloseSlipSuccessCounter();
				_root.loadBalance();
				_root.refreshPageHeader();
			} else {
				if (substring(invSubmit.reply, 1, 1) == "1") {
					if (shouldLoadTexts) {
						var arrows:String = _root.buyBoxErrMsgLogin.lineLogin.lblLogin.text;
						arrows = arrows.substring(arrows.length - 2, arrows.length - 1);
						var tf:TextFormat = new TextFormat();
						tf.size = 9;
						tf.bold = true;
						_root.buyBoxErrMsgLogin.lineLogin.lblLogin.setNewTextFormat(tf);
						_root.buyBoxErrMsgLogin.lineLogin.lblLogin.htmlText = "<b>" + textLoginLine1 + arrows + "</b>";
						_root.buyBoxErrMsgLogin.lineRegister.lblRegister.setNewTextFormat(tf);
						_root.buyBoxErrMsgLogin.lineRegister.lblRegister.htmlText = "<b>" + textLoginLine2 + arrows + "</b>";
					}
					_root.buyBoxErrMsgLogin._x = ERR_MSG_LOGIN_X;
				} else {
					_root.buyBoxErrMsg.lblErrMsg.html = true;
					_root.buyBoxErrMsg.lblErrMsg.htmlText = invSubmit.reply;
					_root.buyBoxErrMsg._x = ERR_MSG_X;
				}
			}
		} else {
			writeToLog("Inv Submit Fail");
		}
		_root.buyBoxInst.slip_waiting_mc.gotoAndStop(1);
		_root.buyBoxInst.buy_btn._x = BUY_BTN_X;
		_root.buyBoxInst.captionBuy._x = BUY_BTN_X;
		_root.buyBoxInst.buyBtnBg._x = BUY_BTN_X;
		_root.copyLevelToSlip();
		submitting = false;
		_root.writeToLog("Inv Submit done.");
	}
	invSubmit.sendAndLoad(requestsPath + "/jsp/system/ajax.xml", invSubmit, "POST");
};
	
function copyLevelToSlip():Void {
	buyBoxInst.curr_level_tf.text = currentLevelTxt;
	if (currentLevelColor == 2) {
		buyBoxInst.curr_level_tf.textColor = LEVEL_COLOR_GREEN;
	} else if (currentLevelColor == 1) {
		buyBoxInst.curr_level_tf.textColor = LEVEL_COLOR_VOID;
	} else {
		buyBoxInst.curr_level_tf.textColor = LEVEL_COLOR_RED;
	}
};
	
function showInvestmentIconLevel() {
	invIconLevelPopup._x = mcPaintArea._x + this._x;
	invIconLevelPopup._y = mcPaintArea._y + this._y + 25;
	invIconLevelPopup.invIconLevelPopup.text = this.investmentLevel;
};

function hideInvestmentIconLevel() {
	invIconLevelPopup._x = 1000;
};

function extractCurrencySign(amount:String):String {
	var crrSign = "";
	var i:Number;
	for (i = 0; i < amount.length; i++) {
		if ("0123456789.,".indexOf(amount.charAt(i)) == -1) {
			crrSign = crrSign + amount.charAt(i);
		}
	}
	return crrSign;
};

function extractDecimalPoint(level:String):Number {
	if (level.indexOf(".") != -1) {
		return level.length - 1 - level.indexOf(".");
	} else if (level.indexOf(",") != -1) {
		return level.length - 1 - level.indexOf(",");
	}
	return 0;
};

//function correctFloatingPointError(number:Number, precision:int = 5):Number {
function correctFloatingPointError(number:Number):Number {
	//default returns (10000 * number) / 10000
	//should correct very small floating point errors

	var correction:Number = Math.pow(10, 5);
	return Math.round(correction * number) / correction;
};

function addThousandsSeparators(s:String):String {
	if (s.length <= 3) {
		return s;
	}
	var i:Number = s.length % 3;
	var tmp:String = "";
	if (i != 0) {
		tmp = s.substring(0, i);
	}
	do {
		if (tmp.length > 0) {
			tmp = tmp + ",";
		}
		if (i + 3 < s.length) {
			tmp = tmp + s.substring(i, i + 3);
		} else {
			tmp = tmp + s.substring(i);
		}
		i = i + 3;
	} while (i < s.length);
	return tmp;
};

// Format number with pattern #,##0.00 (the number of digits after the decimal point is specified).
function formatDecimalPoint(val:Number, decPoint:Number):String {
//	trace("formatDecimalPoint - val: " + val + " decPoint: " + decPoint);
	var tmp:Number = correctFloatingPointError(val);
	var tmpTxt:String = "" + tmp;
//	trace("formatDecimalPoint - val: " + tmp + " decPoint: " + decPoint);

	// here we handle only positive values ;)
	var intPart:Number = Math.floor(tmp);
	var fractPart:Number = tmp - intPart;
	var i:Number;
	if (fractPart > 0) {
		var fractPartDecimal:Number = extractDecimalPoint(tmpTxt);
//		trace("intPart: " + intPart + " fractPart: " + fractPart + " fractPartDecimal: " + fractPartDecimal);
		tmpTxt = tmpTxt.substring(tmpTxt.indexOf("."));
		if (fractPartDecimal.valueOf() == decPoint.valueOf()) { // we are ok
			return addThousandsSeparators("" + intPart) + tmpTxt;
		}
		if (fractPartDecimal.valueOf() < decPoint.valueOf()) { // pad with 0s
			var padding:String = "";
			for (i = fractPartDecimal; i < decPoint; i++) {
				padding = padding + "0";
			}
			return addThousandsSeparators("" + intPart) + tmpTxt + padding;
		}
		return formatDecimalPoint(Math.round(tmp * Math.pow(10, decPoint)) / Math.pow(10, decPoint), decPoint);
	} else if (decPoint > 0) {
		var fractPartStr:String = ".";
		for (i = 0; i < decPoint; i++) {
			fractPartStr = fractPartStr + "0";
		}
		return addThousandsSeparators("" + intPart) + fractPartStr;
	}
	return addThousandsSeparators(tmpTxt);
};

function formatAmount(val:Number):String {
	if (currencyLeftSymbol) {
		return currencySign + formatDecimalPoint(val, currencyDecimalPoint);
	} else {
		return formatDecimalPoint(val, currencyDecimalPoint) + currencySign;
	}
};

function formatAmountTxt(amount:String):String {
	if (currencyLeftSymbol) {
		return currencySign + amount;
	} else {
		return amount + currencySign;
	}
}

//function leaveOnlyDigits(val:String):String {
//	var tmp:String = val;
//	var point:Boolean = false;
//	var i:Number = 0;
//	while (tmp.length > 0 && i <= tmp.length - 1) {
//		if ("0123456789".indexOf(tmp.charAt(i)) != -1) {
//			i++;
//		} else {
//			if (!point) {
//				point = true;
//				i++
//			} else {
//				tmp = tmp.replace(tmp.charAt(i), "");
//			}
//		}
//	}
//	return tmp;
//};

function leaveOnlyDigits(val:String):String {
	var tmp:String = "";
	var point:Boolean = false;
	var i:Number = 0;
	for (i = 0; i < val.length; i++) {
		if ("0123456789".indexOf(val.charAt(i)) != -1) {
			if (!point || tmp.length - tmp.indexOf('.') <= currencyDecimalPoint) {
				tmp = tmp + val.charAt(i);
			}
		} else {
			if (val.charAt(i) == "." && !point && currencyDecimalPoint > 0) {
				point = true;
				tmp = tmp + val.charAt(i);
			}
		}
	}

	// clean meaningless leading 0s
	while (tmp.length > 1 && tmp.charAt(0) == "0" && val.charAt(1) != ".") {
		tmp = tmp.substring(1);
	}

	if (tmp.length > 9) {
		return tmp.substring(0, 9);
	}
	return tmp;
};

function getRandomParam():String {
	return  "" + new Date().getTime() + Math.round(Math.random() * 1000)
}

btnDeposit.onPress = function() {
	getURL(requestsPath + "/jsp/pages/deposit.jsf", "_top");
};

mcMute.onPress = function() {
	if (_root.mcMute._currentframe == 1) {
		_root.mcMute.gotoAndStop(2);
		_root.mute = true;
	} else {
		_root.mcMute.gotoAndStop(1);
		_root.mute = false;
	}
	_root.writeToLog("mcMute._currentframe: " + _root.mcMute._currentframe + " mute: " + _root.mute);
};

btnShowHideLog.onRollOver = function() {
//	if (logging) {
//		logTextArea._x = 5;
//	}
};

btnShowHideLog.onRollOut = function() {
//	logTextArea._x = -1000;
};

btnShowHideLog.onPress = function() {
//	logTextArea.text = "";
	if (logging && logTextArea._x == -1000) {
		logTextArea._x = 5;
	} else {
		logTextArea._x = -1000;
	}
};

function writeToLog(msg:String):Void {
	if (logging) {
		var time:String = formatTimeForLog(new Date());
		logTextArea.text = logTextArea.text + time + " " + msg + "<br>";
		trace(time + " " + msg);
		//ExternalInterface.call("console.log",time + " " + msg);
//		logTextArea.text = logTextArea.text + new Date() + " " + msg + "\n";
	}
};

function reloadInvestments():Void {
	loadInvestments(true);
};

function removeInvestment(id:Number):Boolean {
	loadBalance();
	return chart.removeInvestment(id);
};

ExternalInterface.addCallback("reloadInvestments", this, reloadInvestments);
ExternalInterface.addCallback("removeInvestment", this, removeInvestment);

//trace("leaveOnlyDigits: " + leaveOnlyDigits("a1..b2c3"));

//trace("formatDecimalPoint: " + formatDecimalPoint(1.47720204, 5));
//trace("formatDecimalPoint: " + formatDecimalPoint(0.000, 2));
//trace("formatDecimalPoint: " + formatDecimalPoint(0.0001, 2));

//trace("calcPeriodInSec: " + chart.calcPeriodInSec("23:58:45", "00:18:15"));

btnAssetSelect.onPress = function () {
	if (assetLoading || submitting) {
		return;
	}
	
	if (lstAssets._x >= 0) {
		lstAssets._x = -1000;
		return;
	}
	
	assetLoading = true;
	_root.writeToLog("Loading assets for select...");
	var assetsXml:XML = new XML();
	assetsXml.ignoreWhite = true;
	assetsXml.onHTTPStatus = logHttpStatus;
//	assetsXml.onData = logResponseBody;
	assetsXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading assets " + success);
		if (success) {
			lstAssets.removeAll();
			var i;
			var j = 0;
			_root.writeToLog("Assets count: " + assetsXml.childNodes[0].childNodes.length);
			var group = null;
			var subGroup = null;
			for (i = 0; i < assetsXml.childNodes[0].childNodes.length; i++) {
				var marketId = assetsXml.childNodes[0].childNodes[i].childNodes[0].childNodes[0].nodeValue;
				var crrGroup = assetsXml.childNodes[0].childNodes[i].childNodes[1].childNodes[0].nodeValue;
//				if (null != crrGroup) {
//					crrGroup = reverseIfNeeded(crrGroup);
//				}
				var crrSubGroup = assetsXml.childNodes[0].childNodes[i].childNodes[2].childNodes[0].nodeValue;
//				if (null != crrSubGroup) {
//					crrSubGroup = reverseIfNeeded(crrSubGroup);
//				}
				var market = assetsXml.childNodes[0].childNodes[i].childNodes[3].childNodes[0].nodeValue;
//				try {
//					market = reverseIfNeeded(market);
//				} catch (merr) {
//					_root.writeToLog(merr);
//				}
				var oppId = assetsXml.childNodes[0].childNodes[i].childNodes[4].childNodes[0].nodeValue;
				var closing = assetsXml.childNodes[0].childNodes[i].childNodes[5].childNodes[0].nodeValue;
				if (null != crrGroup && group != crrGroup && !optionPlus) {
					group = crrGroup;
					subGroup = null;
//					lstAssets.addItem({label:"<font face=\"Verdana\" size=\"" + assetSelectFontSize + "\" color=\"#" + assetSelectGroupColor + "\"><b>" + (skinId == 1 || skinId == 4 ? group + "|" : group) + "</b></font>", time:"", data:""});
					// lstAssets.addItem({label:(skinId == 1 || skinId == 4 || skinId == 11 ? group + "|" : group), time:"", data:"", type:"group"});
					lstAssets.addItem({label:group, time:"", data:"", type:"group"});
					j++;
				}
				if (null != crrSubGroup && subGroup != crrSubGroup && !optionPlus) {
					subGroup = crrSubGroup;
//					lstAssets.addItem({label:"<font face=\"Verdana\" size=\"" + assetSelectFontSize + "\" color=\"#" + assetSelectSubGroupColor + "\"><b>" + (skinId == 1 || skinId == 4 ? subGroup + "  |" : "  " + subGroup) + "</b></font>", time:"", data:""});
					// lstAssets.addItem({label:(skinId == 1 || skinId == 4 || skinId == 11 ? subGroup + "  |" : "  " + subGroup), time:"", data:"", type:"subGroup"});
					lstAssets.addItem({label:subGroup, time:"", data:"", type:"subGroup"});
					j++
				}
				var prefix = "";
				if (null != group) {
					prefix = "  ";
				}
				if (null != subGroup) {
					prefix = "    ";
				}
				var clrTxt = "000000";
				var tp = "market";
				if (marketId == pMarketId) {
					clrTxt = assetSelectHighlightColor;
					tp = "smarket";
				}
//				lstAssets.addItem({label:"<font face=\"Verdana\" size=\"" + assetSelectFontSize + "\" color=\"#" + clrTxt + "\">" + (skinId == 1 || skinId == 4 ? market + prefix + "|" : prefix + market) + "</font>", time:"<font face=\"Verdana\" size=\"" + assetSelectFontSize + "\" color=\"#" + clrTxt + "\">" + closing + "</font>", data:oppId, market:marketId});
				// lstAssets.addItem({label:(skinId == 1 || skinId == 4 || skinId == 11 ? market + prefix + "|" : prefix + market), time:closing, data:oppId, market:marketId, type:tp});
				lstAssets.addItem({label:market + prefix, time:closing, data:oppId, market:marketId, type:tp});
//				_root.writeToLog("pMarketId: " + pMarketId + " market: " + market + " j: " + j);
				if (marketId == pMarketId) {
					lstAssets.selectedIndex = j;
					lstAssetsSelectedIndex = j;
				}
				j++;
			}
			
			lstAssets._x = LST_ASSETS_X;
			assetsLoading._x = -1000;
			assetsLoading.stop();
			lblOpportunityCaption._visible = true;
		} else {
			_root.writeToLog("Loading assets failed " + assetsXml.status);
		}
		assetLoading = false;
	}
	lblOpportunityCaption._visible = false;
	assetsLoading._x = ASSETS_LOADING_X;
	assetsLoading.play();
	assetsXml.load(requestsPath + "/jsp/xml/profitLineAssetSelect.xml?typeId=" + (_root.optionPlus ? 3 : 1) + "&t=" + getRandomParam());
};

//function reverseIfNeeded(str:String):String {
////	_root.writeToLog("reverseIfNeeded - str : " + str + " str.charAt(0): " + str.charAt(0) + " rtlchars.indexOf(str.charAt(0)): " + rtlchars.indexOf(str.charAt(0)));
//	if (rtlchars.indexOf(str.charAt(0)) != -1) {
//		var tmp:String = "";
//		var i:Number;
//		var chunk:String = null;
//		for (i = str.length - 1; i >= 0; i--) {
//			if (rtlchars.indexOf(str.charAt(i)) != -1 || str.charAt(i) == ' ') {
//				if (null != chunk) {
//					tmp = tmp + chunk;
//					chunk = null;
//				}
//				tmp = tmp + str.charAt(i);
//			} else {
//				if (null == chunk) {
//					chunk = str.charAt(i);
//				} else {
//					chunk = str.charAt(i) + chunk;
//				}
//			}
//		}
//		if (null != chunk) {
//			tmp = tmp + chunk;
//		}
////		_root.writeToLog("tmp: " + tmp);
//		return tmp;
//	}
//	return str;
//}

function reloadData():Void {
	_root.writeToLog("Reloading data...");
	// bridge.removeTable("FlashTable1");
	ExternalInterface.call("OptionPlusRemoveTable");

	// reset
	currentLevelTxt = "";
	currentLevel = 0;
	oppState = 0;
	buyBoxInst.gotoAndStop(1);
	buyBoxInst.curr_level_tf.text = "";
	counterDown.lblClock.text = "";
	counterDown.lblClock._x = -1000;
	counterDown.txtTimeLeftTo._x = 10000;
	boxClosingLevel._x = -1000;
	errMsgClip._x = -1000;
	buyBoxErrMsg._x = 1000;
	endArrowGray._y = -1000;
	endArrowBlue._y = -1000;
	zoom_mc._x = -2000;
	zoom_mc.stop();
	_root.soldInvestmentId = 0;
	_root.closeGetPricePopup();
	offhours._x = -1000;

	loadingClip._x = LOADING_X;
	loadingClip.play();
	loadData();
};

lstAssets.change = function(evt:Object) {
	_root.writeToLog("Asset selected - label: " + evt.target.selectedItem.label +
			" data: " + evt.target.selectedItem.data +
			" closing: " + evt.target.selectedItem.time +
			" market: " + evt.target.selectedItem.market);
	if (evt.target.selectedItem.data != "") {
		lstAssets._x = -1000;

		pMarketId = evt.target.selectedItem.market;
		pOpportunityId = evt.target.selectedItem.data;
		
		reloadData();
	} else {
		lstAssets.selectedIndex = lstAssetsSelectedIndex;
	}
};
lstAssets.cellRenderer = "LabelCellRenderer";
lstAssets.addEventListener("change", lstAssets);
lstAssets.selectionColor = assetSelectHighlightBkg;
lstAssets.useRollOver = false;
_global.styles.ScrollSelectList.setStyle("selectionDuration", 0);

_root.onMouseDown = function() {
	if (lstAssets._x < 0 && !lstReturnRefund._visible) {
		return;
	}
	
	if (lstAssets._x >= 0 &&
		(_root._xmouse < lstAssets._x || _root._xmouse > lstAssets._x + lstAssets._width ||
		_root._ymouse < lstAssets._y || _root._ymouse > lstAssets._y + lstAssets._height)) {
		lstAssets._x = -1000;
		return;
	}
	
	if (lstReturnRefund._visible &&
		(_root._xmouse < lstReturnRefund._x || _root._xmouse > lstReturnRefund._x + lstReturnRefund._width ||
		_root._ymouse < lstReturnRefund._y || _root._ymouse > lstReturnRefund._y + lstReturnRefund._height)) {
		lstReturnRefund._visible = false;
		return;
	}
};

function sendSMSChbClick(evt:Object) {
	_root.writeToLog("Click Send SMS checkbox");
	if (evt.target.selected) {
		submitSendSMS();
	}
}

function submitSendSMS():Void {
	if (submittingSendSMS) {
		return;
	}
	submittingSendSMS = true;

	var invs = chart.getInvestments();
	
	var sendSMSSubmit:LoadVars = new LoadVars();
	sendSMSSubmit.turnSmsOnFlash = 1;
	sendSMSSubmit.invId = invs[invs.length - 1].getId();
	sendSMSSubmit.t = getRandomParam();
	sendSMSSubmit.onHTTPStatus = function(httpStatus:Number) {
	    writeToLog("HTTP status is: " + httpStatus);
	};
	sendSMSSubmit.onLoad = function(success:Boolean) {
		if (success) {
			writeToLog("Send SMS Submit Success - " + sendSMSSubmit.reply);
			if (sendSMSSubmit.reply == "ok") {
				_root.buyBoxInst.chbSendSMS.enabled = false;
				if (shouldLoadTexts) {
					_root.buyBoxInst.chbSendSMS.label = textSendSMSActive;
				} else {
					_root.buyBoxInst.lblSendSMS._x = -1000; // for Arabic only
				}
			}
		} else {
			writeToLog("Send SMS Submit Failed.");
		}
		submittingSendSMS = false;
	}
	sendSMSSubmit.sendAndLoad(requestsPath + "/jsp/system/ajax.jsf", sendSMSSubmit, "POST");
}

function startCloseSlipSuccessCounter():Void {
	_root.writeToLog("startCloseSlipSuccessCounter");
	_root.closeSlipSuccessCounter = 5 * 20; // 5 sec x 20fps
	_root.buyBoxInst.onEnterFrame = closeSlipSuccessTicker;
};

function closeSlipSuccessTicker():Void {
	_root.closeSlipSuccessCounter -= 1;
	if (_root.closeSlipSuccessCounter <= 0) {
		_root.closeSlipSuccess();
	}
};

function closeSlipSuccess():Void {
	_root.writeToLog("closeSlipSuccess");
	_root.buyBoxInst.onEnterFrame = null;
	_root.closeSlipSuccessCounter = 0;
	if (_root.oppState == 2 || _root.oppState == 3) {
		_root.writeToLog("Go to frame 1");
		_root.buyBoxInst.gotoAndStop(1);
	} else if (_root.oppState == 4 || _root.oppState == 5) {
		_root.writeToLog("Go to frame 5");
		_root.buyBoxInst.gotoAndStop(5);
	} else {
		_root.writeToLog("Go to frame opss....");
	}
	_root.setSlipLevelAndColor();
};

//function startReloadAfterCloseCountdown():Void {
//	_root.writeToLog("startReloadAfterCloseCountdown...");
//	_root.boxClosingLevel.reloadCounter = 60 * 20; // 60 sec * 20 fps
//	_root.boxClosingLevel.onEnterFrame = reloadAfterCloseTicker;
//};
//
//function reloadAfterCloseTicker():Void {
//	_root.boxClosingLevel.reloadCounter -= 1;
//	if (_root.boxClosingLevel.reloadCounter <= 0) {
//		_root.writeToLog("reloadAfterCloseTicker");
//		_root.boxClosingLevel.onEnterFrame = null;
//		pOpportunityId = 0;
//		reloadData();
//	}
//};

function refreshPageHeader():Void {
	if (_root.optionPlus) {
		getURL("javascript:document.getElementById('header').contentWindow.location.reload();");
	}
};

function formatTimeForLog(d:Date):String {
	if (chart == undefined) {
		return "";
	}
	return chart.padToTwoDigit(d.getHours()) + ":" +
		chart.padToTwoDigit(d.getMinutes()) + ":" +
		chart.padToTwoDigit(d.getSeconds()) + "." +
		(d.getMilliseconds() < 100 ? "0" : "") + chart.padToTwoDigit(d.getMilliseconds());
};

// parse string in format MM/DD/YYYY HH24:MI
function parseDate(s:String):Date {
	s = s.replace(" ", "/");
	s = s.replace(":", "/");
	var arr:Array = s.split("/");
	return new Date(Date.UTC(arr[2], arr[0] - 1, arr[1], arr[3], arr[4]));
};

function formatDate(d:Date):String {
	var currdate:Date = new Date();
	var min:Number = d.getMinutes();
	var s:String;
	if (d.getDate() == currdate.getDate() && d.getMonth() == currdate.getMonth() && d.getFullYear() == currdate.getFullYear()) {
		s = d.getHours() + ":" + chart.padToTwoDigit(min) + " " + textToday;
	} else {
		var year:String = "" + d.getFullYear();
		s = d.getHours() + ":" + chart.padToTwoDigit(min) + ", " + d.getDate() + "." + (d.getMonth() + 1) + "." + year.substring(2);
	}
	return s;
};

function updateWinLoseCall(tf:TextField) {
	if (_root.buyBoxInst.amount_tf.text != "") {
		_root.buyBoxInst.amount_tf.text = _root.leaveOnlyDigits(_root.buyBoxInst.amount_tf.text);
	}
	if (_root.buyBoxInst.amount_tf.text != "" && !isNaN(new Number(_root.buyBoxInst.amount_tf.text))) {
		_root.buyBoxInst.above_level_tf.text = _root.formatAmount(new Number(_root.buyBoxInst.amount_tf.text) * _root.oddsWin);
		_root.buyBoxInst.below_level_tf.text = _root.formatAmount(new Number(_root.buyBoxInst.amount_tf.text) * _root.oddsLose);
	} else {
		_root.buyBoxInst.above_level_tf.text = "";
		_root.buyBoxInst.below_level_tf.text = "";
	}
};

function updateWinLosePut(tf:TextField) {
	if (_root.buyBoxInst.amount_tf.text != "") {
		_root.buyBoxInst.amount_tf.text = _root.leaveOnlyDigits(_root.buyBoxInst.amount_tf.text);
	}
	if (_root.buyBoxInst.amount_tf.text != "" && !isNaN(new Number(_root.buyBoxInst.amount_tf.text))) {
		_root.buyBoxInst.above_level_tf.text = _root.formatAmount(new Number(_root.buyBoxInst.amount_tf.text) * _root.oddsLose);
		_root.buyBoxInst.below_level_tf.text = _root.formatAmount(new Number(_root.buyBoxInst.amount_tf.text) * _root.oddsWin);
	} else {
		_root.buyBoxInst.above_level_tf.text = "";
		_root.buyBoxInst.below_level_tf.text = "";
	}
};

function showReturnRefund() {
	_root.writeToLog("showReturnRefund");
	lstReturnRefund._visible = true;
};

var returnRefundSelectedIndex:Number = 0;
function fillReturnRefuntSelect() {
	oddsGroup = trim(oddsGroup);
	var pairIds:Array = oddsGroup.split(" ");
	_root.writeToLog("oddsGroup: " + oddsGroup + " pairIds: " + pairIds);
	lstReturnRefund.removeAll();
	lstReturnRefund.addItem({data:"", returnTxt:textSelectorReturn, refundTxt:textSelectorRefund});
	lstReturnRefund.getItemAt(0).enabled = false;
	var i:Number = 0;
	var op:ReturnRefundPair = null;
	for (i = 0; i < pairIds.length; i++) {
		var pid:Number = new Number(pairIds[i]);
		op = getReturnRefundPair(pid);
		_root.writeToLog("i: " + i + " pairIds[i]: " + pairIds[i] + " pid: " + pid + " op: " + op);
		var tmpRet:String = textSelectorPercent.replace("{0}", "" + op.getReturnValue());
		var tmpRef:String = textSelectorPercent.replace("{0}", "" + op.getRefundValue());
		lstReturnRefund.addItem({data:op.getId(), returnTxt:tmpRet, refundTxt:tmpRef});
		if (Math.round((oddsWin - 1) * 100) == op.getReturnValue().valueOf()) {
			lstReturnRefund.setSelectedIndex(i + 1);
			returnRefundSelectedIndex = lstReturnRefund.selectedIndex;
		}
	}
	
	lstReturnRefund.textColor = _root.skinId == 1 ? 0x282828 : 0x093755;
	lstReturnRefund.selectionColor = _root.skinId == 1 ? 0xC4D7E0 : 0xD8ECF7;
	lstReturnRefund.textSelectedColor = _root.skinId == 1 ? 0x282828 : 0x0284D6;
	lstReturnRefund.setStyle("borderStyle", "solid");
	lstReturnRefund.setStyle("borderColor", _root.skinId == 1 ? 0x282828 : 0x0284D6);
	lstReturnRefund.vScrollPolicy = "off";
	lstReturnRefund.fontSize = 12;
	lstReturnRefund.useRollOver = false;
	lstReturnRefund.change = function(evt:Object) {
		_root.writeToLog("selectedIndex: " + evt.target.selectedIndex);
		if (evt.target.selectedItem.data == "") {
			lstReturnRefund.setSelectedIndex(returnRefundSelectedIndex);
		} else {
			var op:ReturnRefundPair = getReturnRefundPair(evt.target.selectedItem.data);
			oddsWin = 1 + op.getReturnValue() / 100;
			oddsLose = op.getRefundValue() / 100;
			_root.writeToLog("oddsWin: " + oddsWin + " oddsLose: " + oddsLose);
			lstReturnRefund._visible = false;
			returnRefundSelectedIndex = lstReturnRefund.selectedIndex;
			_root.buyBoxInst.captionOptionDetails.text = _root.textSelectorReturnSelector.replace("{0}", Math.round((_root.oddsWin - 1) * 100));
			if (_root.buyBoxInst._currentframe == 2) {
				_root.buyBoxInst.captionExpiryAbovePercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round((_root.oddsWin - 1) * 100));
				_root.buyBoxInst.captionExpiryBelowPercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round(_root.oddsLose * 100));
				updateWinLoseCall(_root.buyBoxInst.amount_tf);
			} else {
				_root.buyBoxInst.captionExpiryAbovePercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round(_root.oddsLose * 100));
				_root.buyBoxInst.captionExpiryBelowPercent.text = _root.textSelectorPercentSlip.replace("{0}", Math.round((_root.oddsWin - 1) * 100));
				updateWinLosePut(_root.buyBoxInst.amount_tf);
			}
		}
	};
	lstReturnRefund.addEventListener("change", lstReturnRefund);
	lstReturnRefund.rowHeight = 20;
	lstReturnRefund.rowCount = lstReturnRefund.length;
	lstReturnRefund.cellRenderer = "ReturnRefundCellRenderer";
//	lstReturnRefund._height = lstReturnRefund.length * 20;
};

function getReturnRefundPair(pairId:Number):ReturnRefundPair {
	var i:Number = 0;
	for (i = 0; i < returnRefundPairs.length; i++) {
		if (pairId.valueOf() == returnRefundPairs[i].getId().valueOf()) {
			return returnRefundPairs[i];
		}
	}
	return null;
};

ExternalInterface.addCallback("lightstreamerOnItemUpdate", this, lightstreamerOnItemUpdate);
if(etrader == true){
	lstInvestments.backgroundColor = 0xd3d3d3;
	//lstInvestments.alternatingRowColors = [0xd3d3d3,0xc0c0c0];
	lstAssets.color = 0x282828;
	lstAssets.fontFamily = "Tohama";
	lstAssets.setStyle("borderStyle", "solid");
	lstAssets.setStyle("borderColor", 0x282828);
}
stop();