﻿function trim(str:String):String {
	var tmp:String = str;
	while (tmp.charAt(0) == ' ') {
		tmp = tmp.substring(1);
	}
	while (tmp.charAt(tmp.length - 1) == ' ') {
		tmp = tmp.substring(0, tmp.length - 1);
	}
	return tmp;
};