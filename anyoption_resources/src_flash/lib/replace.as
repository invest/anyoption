﻿String.prototype.replace = function(s1:String, s2:String):String {
	var b:Boolean;
	var i:Number;
	var tmp:String = this;
	do {
		b = false;
		i = tmp.indexOf(s1);
		if (i != -1) {
			if (i == 0) {
				tmp = s2 + tmp.substring(s1.length);
			} else if (i == tmp.length - s1.length) {
				tmp = tmp.substring(0, tmp.length - s1.length) + s2;
			} else {
				tmp = tmp.substring(0, i) + s2 + tmp.substring(i + s1.length);
			}
			b = true;
		}
	} while (b);
	return tmp;
};

//String.prototype.replace = function(str:String, s1:String, s2:String):String {
//	var b:Boolean;
//	var i:Number;
//	do {
//		b = false;
//		i = str.indexOf(s1);
//		trace(i);
//		if (i != -1) {
//			if (i == 0) {
//				str = str.substring(1);
//			} else if (i == str.length - 1) {
//				str = str.substring(0, str.length - 1);
//			} else {
//				str = str.substring(0, i) + s2 + str.substring(i + 1);
//			}
//			b = true;
//			trace(str);
//		}
//	} while (b);
//	return str;
//}