﻿import mx.core.UIComponent

class com.anyoption.LabelCellRenderer extends UIComponent {
	var label:MovieClip;
	var timeLabel:MovieClip;
//	var label:TextField;
//	var timeLabel:TextField;

	function LabelCellRenderer() {
		// do nothing
	}

	function createChildren():Void {
		label = createObject("Label", "label", 1, {styleName:this, owner:this});
//		label = this.createTextField("label", this.getNextHighestDepth(), 0, 0, 0, 0);
//		label.html = true;
		timeLabel = createObject("Label", "timeLabel", 2, {styleName:this, owner:this});
//		timeLabel = this.createTextField("timeLabel", this.getNextHighestDepth(), 0, 0, 0, 0);
//		timeLabel.html = true;
		size();
	}

	// setSize is implemented by UIComponent and calls size(), after setting
	// __width and __height
	function size():Void {
		label.setSize(__width * 3 / 4 + (_root.skinId == 1 || _root.skinId == 4 || _root.skinId == 11 ? 3 : 0), __height);
//		label._width = __width * 3 / 4 + (_root.skinId == 1 || _root.skinId == 4 ? 5 : 0);
//		label._height = __height;
		timeLabel.setSize(__width / 4, __height);
//		timeLabel._width = __width / 4;
//		timeLabel._height = __height;
		if (_root.skinId == 1 || _root.skinId == 4 || _root.skinId == 11) {
			label._x = __width / 4;
			timeLabel._x = 0;
		} else {
			label._x = 0;
			timeLabel._x = __width * 3 / 4;
		}
		label._y = 0;
		timeLabel._y = 0;
	}

	function setValue(str:String, item:Object, sel:Boolean):Void {
		if (item.type == "group") {
			label.setStyle("color", _root.assetSelectGroupColor);
			label.setStyle("fontWeight", "bold");
		} else if (item.type == "subGroup") {
			label.setStyle("color", _root.assetSelectSubGroupColor);
			label.setStyle("fontWeight", "bold");
		} else if (item.type == "market") {
			label.setStyle("color", 0x000000);
			label.setStyle("fontWeight", "none");
			timeLabel.setStyle("color", 0x000000);
		} else {
			label.setStyle("color", _root.assetSelectHighlightColor);
			label.setStyle("fontWeight", "none");
			timeLabel.setStyle("color", _root.assetSelectHighlightColor);
		}
		if (_root.skinId == 1 || _root.skinId == 4 || _root.skinId == 11) {
			label.setStyle("textAlign", "right");
			label.setStyle("marginRight", 10);
			label.setStyle("fontFamily", 'Tahoma');
			timeLabel.setStyle("textAlign", "right");
		}

		// hide the label if no data to display
		label._visible = (item != undefined);
		// this line actually sets htmlText 
		label.text = item.label;
//		label.setStyle("color", item.color);
//		label.setStyle("textAlign", "right");
//		label.text = "";
//		_root.RTLtex.RTLtext(item.label, label);
		
		timeLabel._visible = (item != undefined);
		timeLabel.text = item.time;
//		timeLabel.setStyle("color", item.color);
//		timeLabel.setStyle("textAlign", "right");
//		if (_root.skinId == 1 || _root.skinId == 4) {
//			var tf:TextFormat = new TextFormat();
//			tf.align = "right";
//			label.setTextFormat(tf);
//			timeLabel.setTextFormat(tf);
//		}
	}

	function getPreferredHeight():Number {
		// this is the height with the default font, you might
		// need to adjust this to suit your needs
		return 18; 
	}

	function getPreferredWidth():Number {
		// default to the width of the listbox
		return __width;
	}
}