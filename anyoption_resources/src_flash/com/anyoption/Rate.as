﻿class com.anyoption.Rate {
	private var time:Date;
	private var level:Number;
	private var chartXPosition:Number;
	
	public function Rate(time:Date, level:Number, chartXPosition:Number) {
		this.time = time;
		this.level = level;
		this.chartXPosition = chartXPosition;
//		_root.writeToLog("Rate - time: " + time + " level: " + level + " chartXPosition: " + chartXPosition);
	}
	
	public function setTime(time:Date):Void {
		this.time = time;
	}
	
	public function getTime():Date {
		return time;
	}
	
	public function setLevel(level:Number):Void {
		this.level = level;
	}
	
	public function getLevel():Number {
		return level;
	}
	
	public function setChartXPosition(chartXPosition:Number):Void {
		this.chartXPosition = chartXPosition;
	}
	
	public function getChartXPosition():Number {
		return chartXPosition;
	}
}