﻿import com.anyoption.Rate;

class com.anyoption.EtChart {
	private var paintArea:MovieClip;
	private var rates:Array;
	private var ratesLoaded:Boolean;
	private var yMinVal:Number;
	private var yMaxVal:Number;
	private var timeFirstInvest:Date;
	private var timeEstClosing:Date;
	private var ratesStartTime:Date;
	private var displayStartTime:Date;
	private var displayOffset:Number;
	private var firstSec:Number;
	private var crrMin:Number;
	private var crrSec:Number;
	private var showCall:Boolean;
	private var showPut:Boolean;
	private var freeze:Boolean;
	
	public function EtChart(paintArea:MovieClip) {
		this.paintArea = paintArea;
		crrMin = -1;
		crrSec = -1;
		showCall = false;
		showPut = false;
		freeze = false;
	}
	
	public function parseRates(xmlNode:XMLNode):Void {
		var crrt:Date = new Date();
		_root.fixTime(crrt);
		displayStartTime = new Date(timeEstClosing.getTime());
		var lenInMin:Number = _root.calcPeriodInMin(timeFirstInvest, timeEstClosing);
		do {
			_root.addMinutes(displayStartTime, -lenInMin);
		} while (crrt.getTime().valueOf() < displayStartTime.getTime().valueOf());

		rates = new Array();
		var ratesInXML:Number = xmlNode.childNodes.length;
		_root.writeToLog("ratesInXML: " + ratesInXML);
		if (ratesInXML > 0) {
			var t:Date;
			var r:String;
			var rn:Number;
			var i:Number;
			for (i = 0; i < ratesInXML; i++) {
				t = new Date(new Number(xmlNode.childNodes[i].childNodes[0].childNodes[0].nodeValue));
				_root.fixTimeZone(t);
				r = xmlNode.childNodes[i].childNodes[1].childNodes[0].nodeValue;
				rn = new Number(r);
				rates[rates.length] = new Rate(t, rn, 0);
			}
			_root.currentLevel = rates[rates.length - 1].getLevel();
		} else {
			firstSec = 0;
			ratesStartTime = new Date(displayStartTime.getTime());
		}
		
		cleanUnneededRates();

		displayOffset = (_root.calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / _root.SEC_PER_PIXEL;

		_root.writeToLog("ratesStartTime: " + ratesStartTime + " displayStartTime: " + displayStartTime + " displayOffset: " + displayOffset);
		_root.writeToLog("Rates loaded - " + rates.length + " yMinVal : " + yMinVal + " yMaxVal: " + yMaxVal);
		ratesLoaded = true;

		resetYLabels();
		repaint();
	}
	
	public function parsePreviousHourRates(xmlNode:XMLNode):Void {
		var ratesInXML:Number = xmlNode.childNodes.length;
		_root.writeToLog("ratesInXML: " + ratesInXML);
		var tmpRates:Array = new Array();
		if (ratesInXML > 0) {
			var t:Date;
			var r:String;
			var rn:Number;
			var i:Number;
			for (i = 0; i < ratesInXML; i++) {
				t = new Date(new Number(xmlNode.childNodes[i].childNodes[0].childNodes[0].nodeValue));
				_root.fixTimeZone(t);
				r = xmlNode.childNodes[i].childNodes[1].childNodes[0].nodeValue;
				rn = new Number(r);
				tmpRates[i] = new Rate(t, rn, 0);
			}
		}
		for (i = 0; i < rates.length; i++) {
			tmpRates[tmpRates.length] = rates[i];
		}
		rates = tmpRates;
		
		cleanUnneededRates();

		_root.writeToLog("ratesStartTime: " + ratesStartTime + " displayStartTime: " + displayStartTime + " displayOffset: " + displayOffset);
		_root.writeToLog("Rates loaded - " + rates.length + " yMinVal : " + yMinVal + " yMaxVal: " + yMaxVal);

		resetYLabels();
	}
	
	public function initXLabels():Void {
		var i:Number;
		for (i = 0; i < 7; i++) {
			_root.labelsX[i].text = _root.formatToString(displayStartTime, false);
			_root.addMinutes(displayStartTime, 10);
		}
		// TODO: see why addMinutes -70 doesn't work
		_root.addHours(displayStartTime, -1);
		_root.addMinutes(displayStartTime, -10);
	}

	public function repaint():Void {
		if (freeze) {
			return;
		}
		paintArea.clear();
		
		if (rates.length > 0) {
			var ySpread:Number = yMaxVal - yMinVal;
			var positioned:Boolean = false;
			paintArea.lineStyle(1, _root.CHART_LINE_COLOR);
			var i:Number;
			for (i = 0; i < rates.length; i++) {
				var chartX:Number = rates[i].getChartXPosition();
				var chartY:Number = rates[i].getLevel();
				if (displayOffset < chartX && chartX - displayOffset < paintArea._width - 1) {
					if (!positioned) {
						paintArea.moveTo(1 + chartX - displayOffset, 1 + paintArea._height * (1 - (chartY - yMinVal) / ySpread));
						positioned = true;
					} else {
						paintArea.lineTo(1 + chartX - displayOffset, 1 + paintArea._height * (1 - (chartY - yMinVal) / ySpread));
					}
				}
			}
		}

//		var crrX:Number = paintArea._x + 1 + rates[rates.length - 1].getChartXPosition() - displayOffset;
		var ct:Date = new Date();
		_root.fixTime(ct);
		if (rates.length > 0 && ct.getTime() < rates[rates.length - 1].getTime().getTime()) {
			// double check that the cursor is not positioned before last rate from the history
			ct = rates[rates.length - 1].getTime();
		}
		var secFromStart = _root.calcPeriodInSec(ratesStartTime, ct) - firstSec;
		var crrX:Number = paintArea._x + 1 + secFromStart / _root.SEC_PER_PIXEL - displayOffset;
		if (!getCanScrollForward() && crrX > paintArea._x + paintArea._width) {
			crrX = paintArea._x + paintArea._width;
		}
		var crrY:Number = paintArea._y + 1 + paintArea._height * (1 - (_root.currentLevel - yMinVal) / ySpread);
		_root.mcCrrH._y = crrY;
		if (paintArea._x < crrX && crrX <= paintArea._x + paintArea._width) {
			if (rates.length > 0) {
//				_root.writeToLog("rates[rates.length - 1].getChartXPosition(): " + (1 + rates[rates.length - 1].getChartXPosition() - displayOffset) + " crrX: " + crrX);
				paintArea.lineTo(crrX - paintArea._x, 1 + paintArea._height * (1 - (rates[rates.length - 1].getLevel() - yMinVal) / (yMaxVal - yMinVal)));
			}
			_root.mcCrrV._x = crrX;
			_root.icnCrrLvl._x = crrX - _root.icnCrrLvl._width / 2;
			_root.icnCrrLvl._y = crrY - _root.icnCrrLvl._height / 2;
		} else {
			_root.mcCrrV._x = -1000;
			_root.icnCrrLvl._x = -1000;
		}
		
		if (showCall) {
			_root.mcSelectedMask._height = crrY - paintArea._y - 1;
			_root.mcSelectedMask._y = paintArea._y;
			_root.txtCall._y = _root.mcSelectedMask._y + (_root.mcSelectedMask._height - _root.txtCall._height) / 2;
		}
		if (showPut) {
			_root.mcSelectedMask._height = paintArea._height - (crrY - paintArea._y - 1);
			_root.mcSelectedMask._y = crrY;
			_root.txtPut._y = _root.mcSelectedMask._y + (_root.mcSelectedMask._height - _root.txtPut._height) / 2;
		}
	}
	
	public function timeTicker():Void {
		if (_root.currentLevel > 0) {
			var ct:Date = new Date();
			_root.fixTime(ct);
			if (ct.getTime() < timeEstClosing.getTime()) {
				var m:Number = ct.getMinutes();
				var s:Number = ct.getSeconds();
	
				if (crrMin == -1) {
					crrMin = m;
				}
				if (m.valueOf() != crrMin.valueOf()) {
					crrMin = m;
					if (crrMin.valueOf() == timeEstClosing.getMinutes() && ct.getTime() < timeEstClosing.getTime()) {
						scrollChart();
					}
				}
				
				if (s.valueOf() != crrSec.valueOf()) {
					crrSec = s;
					var secFromStart = _root.calcPeriodInSec(ratesStartTime, ct) - firstSec;
					if (Math.floor((secFromStart - 1) / _root.SEC_PER_PIXEL) < Math.floor(secFromStart / _root.SEC_PER_PIXEL)) {
//						rates[rates.length] = new Rate(ct, _root.currentLevel, secFromStart / _root.SEC_PER_PIXEL);
						repaint();
					}
					
				}
			}
		}
	}
	
	public function setCurrentLevel(crrLvl:Number, crrLvlTime:Date):Void {
		if (yMinVal == -1 || crrLvl < yMinVal) {
			yMinVal = crrLvl;
		}
		if (yMaxVal == -1 || crrLvl > yMaxVal) {
			yMaxVal = crrLvl;
		}
		ensureYOffset(crrLvl);
		
		if (rates.length == 0 || crrLvlTime.getTime() - rates[rates.length - 1].getTime().getTime() > _root.SEC_PER_PIXEL) {
			var secFromStart = _root.calcPeriodInSec(ratesStartTime, crrLvlTime) - firstSec;
			rates[rates.length] = new Rate(crrLvlTime, crrLvl, secFromStart / _root.SEC_PER_PIXEL);
		}
	}

	private function ensureYOffset(crrLvl:Number):Void {
		// don't let the current level line go too close to the top/bottom of the chart
		var tooClose:Number = (yMaxVal - yMinVal) * 0.07;
		if (tooClose == 0) {
			tooClose = yMinVal * 0.0001;
		}
		if (crrLvl < yMinVal + tooClose) {
			var newMinVal = Math.min(yMinVal, crrLvl);
			yMinVal = newMinVal - (yMaxVal - newMinVal) * 0.07;
			if (yMinVal == crrLvl) {
				yMinVal = yMinVal - tooClose;
			}
		}
		if (crrLvl > yMaxVal - tooClose) {
			var newMaxVal = Math.max(yMaxVal, crrLvl);
			yMaxVal = newMaxVal + (newMaxVal - yMinVal) * 0.07;
			if (yMaxVal == crrLvl) {
				yMaxVal = yMaxVal + tooClose;
			}
		}
		resetYLabels();
	}
	
	public function resetYLabels():Void {
		if (_root.anyOptionChart) {
			if (yMinVal > 0 && yMaxVal > 0) {
				_root.labelsY[0].text = _root.formatDecimalPoint(yMinVal, _root.decimalPoint);
				_root.labelsY[3].text = _root.formatDecimalPoint(yMaxVal, _root.decimalPoint);
			} else {
				_root.labelsY[0].text = "";
				_root.labelsY[3].text = "";
			}
		} else {
			var i:Number;
			var step:Number = (yMaxVal - yMinVal) / _root.SEC_PER_PIXEL;
			var steps:Number = 1;
			for (i = 0; i < 4; i++) {
				_root.labelsY[i].text = _root.formatDecimalPoint(yMinVal + step * steps, _root.decimalPoint);
				steps = steps + 2;
			}
		}
	}
	
	private function cleanUnneededRates():Void {
		var crrt:Date = new Date();
		_root.fixTime(crrt);
		ratesStartTime = new Date(timeEstClosing.getTime());
		var lenInMin:Number = _root.calcPeriodInMin(timeFirstInvest, timeEstClosing);
		do {
			_root.addMinutes(ratesStartTime, -lenInMin);
		} while (crrt.getTime().valueOf() < ratesStartTime.getTime().valueOf());
		if (rates[0].getTime().getTime() < ratesStartTime.getTime()) {
			_root.addMinutes(ratesStartTime, -lenInMin);
		}

		_root.writeToLog("Cleaning old hour rates - length: " + rates.length + " yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal + " startTime: " + ratesStartTime);
		var newRates:Array = new Array();
		var newYMinVal:Number;
		var newYMaxVal:Number;
		var i:Number;
		for (i = 0; i < rates.length; i++) {
			if (rates[i].getTime().getTime() > ratesStartTime.getTime()) {
				if (newRates.length == 0) {
					firstSec = rates[i].getTime().getSeconds();
					newYMinVal = rates[i].getLevel();
					newYMaxVal = newYMinVal;
				}
				newRates[newRates.length] = rates[i];
				newRates[newRates.length - 1].setChartXPosition((_root.calcPeriodInSec(ratesStartTime, rates[i].getTime()) - firstSec) / _root.SEC_PER_PIXEL);
				if (newRates.length > 1) {
					if (rates[i].getLevel() < newYMinVal) {
						newYMinVal = rates[i].getLevel();
					}
					if (rates[i].getLevel() > newYMaxVal) {
						newYMaxVal = rates[i].getLevel();
					}
				}
			}
		}
		rates = newRates;
		if (newRates.length > 0) {
			var offset:Number = (newYMaxVal - newYMinVal) * 0.02;
			yMinVal = newYMinVal - offset;
			yMaxVal = newYMaxVal + offset;
		} else {
			yMinVal = -1;
			yMaxVal = -1;
		}
		_root.writeToLog("After cleaning - length: " + rates.length + " yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal + " startTime: " + ratesStartTime);
	}
	
	private function scrollChart():Void {
		cleanUnneededRates();
		scrollForward();
		_root.updateScrollButtons();
	}
	
	public function scrollBack():Void {
		if (_root.previousHourLoaded) {
			_root.addHours(displayStartTime, -1);
			displayOffset = (_root.calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / _root.SEC_PER_PIXEL;
			_root.showingPreviousHour = true;
			repaint();
			initXLabels();
		} else {
			_root.ratesToLoad = 2;
			_root.loadRates();
		}
	}
	
	public function scrollForward():Void {
		_root.addHours(displayStartTime, 1);
		displayOffset = (_root.calcPeriodInSec(ratesStartTime, displayStartTime) - firstSec) / _root.SEC_PER_PIXEL;
		_root.showingPreviousHour = false;
		repaint();
		initXLabels();
	}
	
	public function getCanScrollBack():Boolean {
//		if (displayOffset > 100) {
//			return true;
//		}
//		return false;
		return _root.hasPreviousHour && !_root.showingPreviousHour;
	}
	
	public function getCanScrollForward():Boolean {
//		var ct:Date = new Date();
//		_root.fixTime(ct);
//		if (ct.getTime() > timeEstClosing.getTime()) {
//			ct = new Date(timeEstClosing.getTime());
//		}
//		// if the display start time is more than an hour old and the closing is not late
//		// we must be showing the previous hour
//		_root.addHours(ct, -1);
//		if (displayStartTime.getTime() < ct.getTime()) {
//			return true;
//		}
//		return false;
		return _root.showingPreviousHour;
	}
	
	public function setShowCall(showCall:Boolean):Void {
		this.showCall = showCall;
	}
	
	public function getShowCall():Boolean {
		return showCall;
	}
	
	public function setShowPut(showPut:Boolean):Void {
		this.showPut = showPut;
	}
	
	public function getShowPut():Boolean {
		return showPut;
	}
	
	public function setTimeFirstInvest(timeFirstInvest:Date):Void {
		this.timeFirstInvest = timeFirstInvest;
	}
	
	public function getTimeFirstInvest():Date {
		return timeFirstInvest;
	}
	
	public function setTimeEstClosing(timeEstClosing:Date):Void {
		this.timeEstClosing = timeEstClosing;
	}
	
	public function getTimeEstClosing():Date {
		return timeEstClosing;
	}
	
	public function setFreeze(freeze:Boolean):Void {
		this.freeze = freeze;
	}
	
	public function getFreeze():Boolean {
		return freeze;
	}
	
	public function setYMinVal(yMinVal:Number):Void {
		this.yMinVal = yMinVal;
	}
	
	public function getYMinVal():Number {
		return yMinVal;
	}
	
	public function setYMaxVal(yMaxVal:Number):Void {
		this.yMaxVal = yMaxVal;
	}
	
	public function getYMaxVal():Number {
		return yMaxVal;
	}
}