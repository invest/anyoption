﻿import com.anyoption.Investment;
import com.anyoption.Rate;
import com.anyoption.ChartArea;

class com.anyoption.Chart {
	private var investments:Array;
	private var investmentsLoaded:Boolean;
	private var rates:Array;
	private var ratesLoaded:Boolean;
	private var paintArea:MovieClip;
	private var paintAreaBg:MovieClip;
	private var chartStartX:Number;
	private var chartStartY:Number;
	private var chartWidth:Number;
	private var chartHeight:Number;
	private var displayOffset:Number;

	private var yMinVal:Number;
	private var yMaxVal:Number;
	private var ratesMinVal:Number;
	private var ratesMaxVal:Number;
	private var ySpread:Number;
	private var currentLevel:Number;
	private var currentLevelIcon:MovieClip;
	private var firstSec:Number;
	private var crrMin:Number;
	private var crrSec:Number;
	private var lastProfit:Number;
	
	private var labelsX:Array;
	private var labelsY:Array;
	private var chartAreas:Array;
	private var serverTimeOffset:Number;
	private var timezoneOffset:Number;
	private var selectedInList:Number;
	private var closingSoundPlayed:Boolean;
	
	public function Chart(paintArea:MovieClip, paintAreaBg:MovieClip, chartStartX:Number, chartStartY:Number, chartWidth:Number, chartHeight:Number, labelsX:Array) {
		this.paintArea = paintArea;
		this.paintAreaBg = paintAreaBg;
		this.chartStartX = chartStartX;
		this.chartStartY = chartStartY;
		this.chartWidth = chartWidth;
		this.chartHeight = chartHeight;
		this.labelsX = labelsX;
		
		investments = null;
		investmentsLoaded = false;
		ratesLoaded = false;
		closingSoundPlayed = false;

		crrSec = -1;
		lastProfit = -100;
		yMinVal = -1;
		yMaxVal = -1;
		ratesMinVal = -1;
		ratesMaxVal = -1;
		
		serverTimeOffset = null;
	}

	public function timeTicker():Void {
		if (_root.currentLevel > 0) {
			var ct:Date = new Date();
			fixTime(ct);
			var s:Number = ct.getSeconds();
			var m:Number = ct.getMinutes();
			if (m.valueOf() != crrMin.valueOf()) {
				crrMin = m;
				init();
				if (crrMin == 1 || crrMin == 31) {
					_root.loadBalance();
				}
			}
			if (s.valueOf() != crrSec.valueOf()) {
				crrSec = s;
				var secFromStart = calcPeriodInSec(_root.timeFirstInvest, ct) - firstSec;
				if (Math.floor((secFromStart - 1) / _root.SEC_PER_PIXEL) < Math.floor(secFromStart / _root.SEC_PER_PIXEL)) {
					rates[rates.length] = new Rate(ct, currentLevel, secFromStart / _root.SEC_PER_PIXEL);
//					if (ratesMinVal < currentLevel) {
//						ratesMinVal = currentLevel;
//					}
//					if (ratesMaxVal > currentLevel) {
//						ratesMaxVal = currentLevel;
//					}
					repaint();
				}
				
				if (_root.oppState == 3) {
					var cdMin:Number = _root.timeLastInvest.getMinutes() - ct.getMinutes() - (s == 0 ? 0 : 1);
					if (cdMin >= 0) {
						_root.counterDown.lblClock.text = padToTwoDigit(cdMin) + ":" + padToTwoDigit(s == 0 ? 0 : 60 - s);
						_root.counterDown.lblClock._x = _root.LBL_CLOCL_X;
					}
				}
			}
		}
	}
	
	public function parseInvestments(xmlNode:XMLNode):Void {
		var i:Number;
		if (null != investments) {
			_root.lstInvestments.removeAll();

			for (i = 0; i < investments.length; i++) {
				if (investments[i].getLevelIcon() != undefined) {
					investments[i].getLevelIcon().removeMovieClip();
					investments[i].setLevelIcon(undefined);
				}
			}
			
//			yMinVal = -1;
//			yMaxVal = -1;
			ratesMinVal = -1;
			ratesMaxVal = -1;
		}
		
		investments = new Array();
		var relInv:Boolean = false;
		for (i = 0; i < xmlNode.childNodes.length; i++) {
			investments[i] = new Investment();
			investments[i].loadFromXMLNode(xmlNode.childNodes[i]);
			fixTimeZone(investments[i].getTimeCreated());
			fixTimeZone(investments[i].getTimeEstClosing());
			fixTimeZone(investments[i].getTimeLastInvest());
			fixTimeZone(investments[i].getTimeFirstInvest());
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				if (!relInv) {
					ratesMinVal = investments[i].getLevel();
					ratesMaxVal = investments[i].getLevel();
					relInv = true;
				} else {
					if (investments[i].getLevel() < yMinVal) {
						ratesMinVal = investments[i].getLevel();
					}
					if (investments[i].getLevel() > yMaxVal) {
						ratesMaxVal = investments[i].getLevel();
					}
				}
			}
		}

		for (i = 0; i < investments.length; i++) {
			var itemLabel:String = investments[i].getLevelTxt() + "     " + formatToString(investments[i].getTimeCreated(), false) + "     " + investments[i].getAmountTxt();
			var itemData:String = investments[i].getType() == 1 ? "call" : "put";
			_root.lstInvestments.addItem({label:itemLabel, data:itemData, amount:investments[i].getAmountTxt(), market:investments[i].getMarket(), time:formatToString(investments[i].getTimeEstClosing(), false), level:investments[i].getLevelTxt(), marketId:investments[i].getMarketId()});
//			_root.lstInvestments.addItem({label:itemLabel, data:itemData, amount:investments[i].getAmountTxt(), market:_root.marketName, time:formatToString(_root.timeEstClosing, false), level:investments[i].getLevelTxt()});
		}
		
		investmentsLoaded = true;

//		_root.writeToLog("Invs - " + investments.length + " yMinVal : " + yMinVal + " yMaxVal: " + yMaxVal);
		_root.writeToLog("Invs - " + investments.length + " ratesMinVal : " + ratesMinVal + " ratesMaxVal: " + ratesMaxVal);
	}
	
	public function parseRates(xmlNode:XMLNode):Void {
		rates = new Array();
		var ratesInXML:Number = xmlNode.childNodes.length;
		_root.writeToLog("ratesInXML: " + ratesInXML);
		if (ratesInXML > 0) {
			var t:Date;
			var r:String;
			var rn:Number;
			var i:Number;
			for (i = 0; i < ratesInXML; i++) {
				t = new Date(new Number(xmlNode.childNodes[i].childNodes[0].childNodes[0].nodeValue));
				fixTimeZone(t);
				r = xmlNode.childNodes[i].childNodes[1].childNodes[0].nodeValue;
				rn = new Number(r);
				if (rn < ratesMinVal || ratesMinVal == -1) {
					ratesMinVal = rn;
				}
				if (rn > ratesMaxVal || ratesMaxVal == -1) {
					ratesMaxVal = rn;
				}
				
				if (i == 0) {
					firstSec = t.getSeconds();
				}
				rates[rates.length] = new Rate(t, rn, (calcPeriodInSec(_root.timeFirstInvest, t) - firstSec) / _root.SEC_PER_PIXEL);
			}
		} else {
			firstSec = 0;
		}
		ratesLoaded = true;
		_root.writeToLog("Rates loaded - " + rates.length + " ratesMinVal : " + ratesMinVal + " ratesMaxVal: " + ratesMaxVal);
		
		var tboff = (ratesMaxVal - ratesMinVal) * _root.TOP_BOTTOM_OFFSET;
		yMinVal = ratesMinVal - tboff;
		yMaxVal = ratesMaxVal + tboff;
		_root.writeToLog("After offset yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal);
		
		if (ratesMinVal != -1) {
			yMinVal = Math.min(yMinVal, (ratesMinVal + ratesMaxVal) / 2 * (1 - _root.MIN_MAX_INITIAL_OFFSET));
			yMaxVal = Math.max(yMaxVal, (ratesMinVal + ratesMaxVal) / 2 * (1 + _root.MIN_MAX_INITIAL_OFFSET));
			_root.writeToLog("After enforce % offset yMinVal: " + yMinVal + " yMaxVal: " + yMaxVal);
		}
	}
	
	public function init():Void {
		_root.writeToLog("Chart.init");
		initChartAreas();
		initXLabels();
		initYLabels();
		initTotalLabels();
		initInvestmentsIcons();
		if (currentLevelIcon != undefined) {
			currentLevelIcon.removeMovieClip();
		}
		currentLevelIcon = createInvestmentIcon(0, 0, null);
		repaint();
	}
	
	private function initXLabels():Void {
		var d:Date = new Date(_root.timeFirstInvest.getTime());
		var ct:Date = new Date();
		fixTime(ct);
		var timePassed:Number = calcPeriodInSec(d, ct);
		var oppLength:Number = calcPeriodInSec(d, _root.timeEstClosing);
		_root.writeToLog("timePassed: " + timePassed + " oppLength: " + oppLength + " _root.oppState: " + _root.oppState);
		if (timePassed > 3600 || oppLength < 3600 || _root.oppState >= 3) {
			_root.writeToLog("Scroll...");
			d = new Date(_root.timeEstClosing.getTime());
			do {
				addHours(d, -1);
			} while (d.getTime() > ct.getTime());
		} else {
			if (d.getMinutes() % 10 != 0) {
				addMinutes(d, -(d.getMinutes() % 10));
			}
		}
		if (rates.length > 0 && rates[0].getTime().getTime() > d.getTime()) {
			_root.writeToLog("Calculated start time before history start.");
			while (d.getTime() + 600000 <= rates[0].getTime().getTime()) {
				_root.writeToLog("d: " + d + " rates[0].getTime(): " + rates[0].getTime() + " adding 10 min...");
				addMinutes(d, 10);
			}
		}
		displayOffset = (calcPeriodInSec(_root.timeFirstInvest, d) - firstSec) / _root.SEC_PER_PIXEL;
		_root.writeToLog("displayOffset: " + displayOffset);

		var i:Number;
		for (i = 0; i < 7; i++) {
			labelsX[i].text = formatToString(d, false);
			addMinutes(d, 10);
		}

		d = new Date(_root.timeLastInvest.getTime());
		addMinutes(d, -10);
		labelsX[7].text = formatToString(d, false);
		labelsX[8].text = formatToString(_root.timeLastInvest, false);

		if (chartAreas.length > 1) {
			_root.captionProfitLine._x = _root.CAPTION_PROFIT_LINE_X;
			_root.counterDown._x = _root.mcPaintArea._x + chartStartX - displayOffset + (calcPeriodInSec(_root.timeFirstInvest, d) - firstSec) / _root.SEC_PER_PIXEL;
		} else {
			_root.captionProfitLine._x = -1000;
			_root.counterDown._x = -1000;
		}
	}
	
	private function initChartAreas():Void {
		var invLvls:Array = new Array();
		var i:Number;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				invLvls[invLvls.length] = investments[i].getLevel();
			}
		}
		invLvls = sortInvLevelsAsc(invLvls);
		
		// make sure the inv are between the lowest and highest vals
		if (yMinVal == invLvls[0]) {
			yMinVal = yMinVal * 0.999;
		}
		if (yMaxVal == invLvls[invLvls.length - 1]) {
			yMaxVal = yMaxVal * 1.001;
		}
		
		if (null != chartAreas) {
			for (i = 0; i < chartAreas.length; i++) {
				chartAreas[i].cleanup();
			}
			chartAreas = null;
		}
		
		chartAreas = new Array();
		var btm:Number = yMinVal;
//		for (i = 0; i < investments.length; i++) {
		for (i = 0; i < invLvls.length; i++) {
//			chartAreas[i] = new ChartArea(paintAreaBg, btm, invLvls[i], investments, i, investments.length);
			chartAreas[i] = new ChartArea(paintAreaBg, btm, invLvls[i], investments, i, invLvls.length);
			btm = invLvls[i];
		}
//		chartAreas[chartAreas.length] = new ChartArea(paintAreaBg, btm, yMaxVal, investments, i, investments.length);
		chartAreas[chartAreas.length] = new ChartArea(paintAreaBg, btm, yMaxVal, investments, i, invLvls.length);
	}
	
	private function initYLabels():Void {
		var lblTextFormat:TextFormat = new TextFormat();
		lblTextFormat.size = 9;
		if (!_root.optionPlus && _root.skinId != 1) {
			lblTextFormat.align = "right";
		}
		if (_root.optionPlus) {			if(_root.skinId != 1){
				if (chartAreas.length > 1) {
					lblTextFormat.color = 0xFFFFFF;
				} else {
					lblTextFormat.color = 0x000000;
				}
			}else{
				if (chartAreas.length > 1) {
					lblTextFormat.color = 0xFFFFFF;
				} else {
					lblTextFormat.color = 0xFFFFFF;
				}			
			}
		}

		var i:Number;
		if (null != labelsY) {
			for (i = 0; i < labelsY.length; i++) {
				labelsY[i].removeTextField();
			}
			labelsY = null;
		}
		labelsY = new Array();
		labelsY[0] = paintArea.createTextField("lblY_0", paintArea.getNextHighestDepth(), 0, chartStartY + chartHeight - _root.LABEL_HEIGHT / (_root.optionPlus || _root.skinId == 1 ? 1 : 2), _root.LABEL_WIDTH, _root.LABEL_HEIGHT);
		labelsY[0].setNewTextFormat(lblTextFormat);
		labelsY[0].text = _root.formatDecimalPoint(yMinVal, _root.decimalPoint);
		for (i = 0; i < chartAreas.length; i++) {
			var tmpY:Number = chartStartY + chartHeight * (1 - (chartAreas[i].getTopLevel() - yMinVal) / (yMaxVal - yMinVal));
			if (!_root.optionPlus || i < chartAreas.length - 1) {
				tmpY -= _root.LABEL_HEIGHT / 2;
			}
			labelsY[labelsY.length] = paintArea.createTextField("lblY_" + (i + 1), paintArea.getNextHighestDepth(), 0, tmpY, _root.LABEL_WIDTH, _root.LABEL_HEIGHT);
			labelsY[labelsY.length - 1].setNewTextFormat(lblTextFormat);
			labelsY[labelsY.length - 1].text = _root.formatDecimalPoint(chartAreas[i].getTopLevel(), _root.decimalPoint);
		}
		
		doNotOverlapYLabels();
	}
	
	private function doNotOverlapYLabels():Void {
		var i:Number;
		var lastVisible:Number = 0; // the bottom label should be visible
		for (i = 1; i < labelsY.length - 1; i++) {
			// see if to close to previous visible label bottom up. or too close to the top one
			if ((labelsY[lastVisible]._y - labelsY[i]._y < labelsY[i]._height / 2) ||
					(labelsY[i]._y - labelsY[labelsY.length - 1]._y < labelsY[i]._height / 2)) {
				labelsY[i].text = "";
				chartAreas[i - 1].setTopLabelVisible(false);
			} else {
				lastVisible = i;
				chartAreas[i - 1].setTopLabelVisible(true);
			}
		}
	}
	
	private function initInvestmentsIcons():Void {
//		var firstSec:Number = new Number(substring(rates[0].getTime(), 7, 2));
		var i:Number;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				if (investments[i].getLevelIcon() == undefined) {
					investments[i].setLevelIcon(createInvestmentIcon(investments[i].getType(), i, investments[i].getLevelTxt()));
					investments[i].setLevelIconXPosition((calcPeriodInSec(_root.timeFirstInvest, investments[i].getTimeCreated()) - firstSec) / _root.SEC_PER_PIXEL);
				}
			}
		}
	}
	
	private function initTotalLabels():Void {
		var ttlInv:Number = 0;
		var i:Number;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				ttlInv += investments[i].getAmount();
			}
		}
		_root.lblTotalInvest.text = _root.formatAmount(ttlInv);
	}
	
	public function setCurrentLevel(crrLvl:Number):Void {
		currentLevel = crrLvl;
		
		if (ratesMinVal == -1 || ratesMinVal > currentLevel) {
//			_root.writeToLog("Update ratesMinVal - crr: " + ratesMinVal + " new: " + currentLevel);
			ratesMinVal = currentLevel;
		}
		if (ratesMaxVal == -1 || ratesMaxVal < currentLevel) {
//			_root.writeToLog("Update ratesMaxVal - crr: " + ratesMaxVal + " new: " + currentLevel);
			ratesMaxVal = currentLevel;
		}

		// don't let the current level line go too close to the top/bottom of the chart
		var tooClose:Number = (ratesMaxVal - ratesMinVal) * _root.TOP_BOTTOM_OFFSET;
		if (tooClose == 0) {
			tooClose = ratesMinVal * 0.0001;
		}
		var i:Number;
//		if (currentLevel < yMinVal + tooClose) {
//			var newMinVal = Math.min(yMinVal, currentLevel);
//			newMinVal = newMinVal - (yMaxVal - newMinVal) * _root.TOP_BOTTOM_OFFSET;
//			if (newMinVal == yMinVal) {
//				newMinVal = newMinVal - tooClose;
//			}
		if (yMinVal == -1 || yMinVal > ratesMinVal - tooClose) {
//			_root.writeToLog("Update yMinVal - crr: " + yMinVal + " tooClose: " + tooClose + " new: " + (ratesMinVal - tooClose));
			var newMinVal = (yMinVal == -1 ? ratesMinVal * (1 - _root.MIN_MAX_INITIAL_OFFSET) : ratesMinVal - tooClose);
			for (i = 0; i < chartAreas.length; i++) {
				if (chartAreas[i].getBottomLevel() == yMinVal) {
					chartAreas[i].setBottomLevel(newMinVal);
				}
			}
			yMinVal = newMinVal;
			labelsY[0].text = _root.formatDecimalPoint(yMinVal, _root.decimalPoint);
		}
//		if (currentLevel > yMaxVal - tooClose) {
//			var newMaxVal = Math.max(yMaxVal, currentLevel);
//			newMaxVal = newMaxVal + (newMaxVal - yMinVal) * _root.TOP_BOTTOM_OFFSET;
//			if (newMaxVal == yMaxVal) {
//				newMaxVal = newMaxVal + tooClose;
//			}
		if (yMaxVal == -1 || yMaxVal < ratesMaxVal + tooClose) {
//			_root.writeToLog("Update yMaxVal - crr " + yMaxVal + " tooClose: " + tooClose + " new: " + (ratesMaxVal + tooClose));
			var newMaxVal = (yMaxVal == -1 ? ratesMaxVal * (1 + _root.MIN_MAX_INITIAL_OFFSET) : ratesMaxVal + tooClose);
			for (i = 0; i < chartAreas.length; i++) {
				if (chartAreas[i].getTopLevel() == yMaxVal) {
					chartAreas[i].setTopLevel(newMaxVal);
				}
			}
			yMaxVal = newMaxVal;
			labelsY[labelsY.length - 1].text = _root.formatDecimalPoint(yMaxVal, _root.decimalPoint);
		}
		
		repaint();

		doNotOverlapYLabels();
	}
	
	public function repaint() {
		paintArea.clear();
		_root.gradient._y = -1000;

		var selectedInListLevel:Number;
		if (selectedInList != undefined) {
			selectedInListLevel = investments[selectedInList].getLevel();
		}
		labelsY[0].text = _root.formatDecimalPoint(yMinVal, _root.decimalPoint);
		var i:Number;
		for (i = 0; i < chartAreas.length; i++) {
			chartAreas[i].paint(chartStartX + 1, chartStartY, chartWidth - 1, chartHeight, yMinVal, yMaxVal, currentLevel, labelsY[i + 1], chartAreas[i].getTopLevel().valueOf() == selectedInListLevel.valueOf());
		}
		
		var ySpread:Number = yMaxVal - yMinVal;
		
		if (chartAreas.length > 1) {
			paintArea.lineStyle(1, 0xFFFFFF);
		} else {
			paintArea.lineStyle(1, _root.noInvestmentsHistoryColor);
		}
		if (rates.length > 0) {
			var positioned:Boolean = false;
			var passThroughInv:Number;
			var j:Number;
			for (i = 0; i < rates.length; i++) {
				passThroughInv = -1;
				var chartX:Number = rates[i].getChartXPosition();
				var chartY:Number = rates[i].getLevel();
				for (j = 0; j < investments.length; j++) {
					if (investments[j].getMarketId().valueOf() == _root.pMarketId.valueOf() &&
							chartX <= investments[j].getLevelIconXPosition() && 
							((i < rates.length - 1 && investments[j].getLevelIconXPosition() < rates[i + 1].getChartXPosition()) ||
							(i == rates.length - 1))) {
						passThroughInv = j;
//						_root.writeToLog(
//								"chartX: " + chartX +
//								" chartY: " + chartY +
//								" passThroughInv: " + passThroughInv +
//								" investments[j].getLevelIconXPosition(): " + investments[j].getLevelIconXPosition() +
//								" investments[j].getLevel(): " + investments[j].getLevel() +
//								" rates[i + 1].getChartXPosition(): " + rates[i + 1].getChartXPosition() +
//								" rates[i + 1].getLevel(): " + rates[i + 1].getLevel());
					}
				}
				if (chartX >= displayOffset && chartX < displayOffset + chartWidth) {
					if (!positioned) {
						paintArea.moveTo(chartStartX + 1 - displayOffset + chartX, chartStartY + 1 + chartHeight * (1 - (chartY - yMinVal) / ySpread));
						positioned = true;
					} else {
						paintArea.lineTo(chartStartX + 1 - displayOffset + chartX, chartStartY + 1 + chartHeight * (1 - (chartY - yMinVal) / ySpread));
					}
					if (passThroughInv != -1) {
						paintArea.lineTo(
								chartStartX + 1 - displayOffset + investments[passThroughInv].getLevelIconXPosition(),
								chartStartY + 1 + chartHeight * (1 - (investments[passThroughInv].getLevel() - yMinVal) / ySpread));
					}
				}
			}
		}
		
		var crrY:Number = chartStartY + chartHeight * (1 - (currentLevel - yMinVal) / ySpread);
		currentLevelIcon._x = chartStartX + 1 - displayOffset + rates[rates.length - 1].getChartXPosition() + 1 - currentLevelIcon._width / 2;
		if (chartStartX + chartWidth - currentLevelIcon._width / 2 < currentLevelIcon._x) {
			_root.writeToLog(
					"chartStartX: " + chartStartX +
					" displayOffset: " + displayOffset +
					" rates[rates.length - 1].getChartXPosition(): " + rates[rates.length - 1].getChartXPosition() +
					" currentLevelIcon._x: " + currentLevelIcon._x);
		}
		currentLevelIcon._x = Math.min(currentLevelIcon._x, chartStartX + chartWidth - currentLevelIcon._width / 2);
		currentLevelIcon._y = crrY - currentLevelIcon._height / 2;
		if (chartAreas.length > 1) {
			paintArea.lineStyle(2, _root.LINE_COLOR);
			paintArea.moveTo(chartStartX + 1, crrY);
			paintArea.lineTo(chartStartX - 1 + chartWidth, crrY);
		} else {
			paintArea.lineStyle(1, _root.noInvestmentsCursorColor);
			paintArea.dashTo(chartStartX + 1, crrY, chartStartX - 1 + chartWidth, crrY, 2, 3);
			paintArea.dashTo(currentLevelIcon._x + currentLevelIcon._width / 2, chartStartY + 1, currentLevelIcon._x + currentLevelIcon._width / 2, chartStartY - 1 + chartHeight, 2, 3);
		}
		
		var invIcn:MovieClip;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				invIcn = investments[i].getLevelIcon();
				if (investments[i].getLevelIconXPosition() >= displayOffset && investments[i].getLevelIconXPosition() < displayOffset + chartWidth) {
					invIcn._x = chartStartX + 1 - displayOffset + investments[i].getLevelIconXPosition() - invIcn._width / 2;
					invIcn._y = chartStartY + chartHeight * (1 - (investments[i].getLevel() - yMinVal) / ySpread) - invIcn._height / 2;
				} else {
					invIcn._y = 1000;
				}
			}
		}
		
		if ((_root.oppState == 4 || _root.oppState == 5) && chartAreas.length > 1) {
			drawWaiting4Progress();
		}

		var ttlProf:Number = 0;
		var ttlAmnt:Number = 0;
		var i:Number;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getMarketId().valueOf() == _root.pMarketId.valueOf()) {
				ttlProf += investments[i].getReturnAt(currentLevel);
				ttlAmnt += investments[i].getAmount();
			}
		}
		var newTtlProfTxt:String = _root.formatAmount(ttlProf);
		if (_root.lblTotalProfit.text != newTtlProfTxt) {
			_root.lblTotalProfit.text = newTtlProfTxt;
			_root.returnMask.gotoAndPlay(1);
		}
		var newProfit:Number = 0;
		if (ttlProf > ttlAmnt) {
			newProfit = 1;
		} else if (ttlProf < ttlAmnt) {
			newProfit = -1;
		}
		if (lastProfit.valueOf() != newProfit.valueOf()) {
			lastProfit = newProfit;
			if (!_root.mute) {
				if (ttlProf > ttlAmnt) {
					_root.sndUp.gotoAndPlay(2);
				} else if (ttlProf < ttlAmnt) {
					_root.sndDown.gotoAndPlay(2);
				}
			}
		}

		if (_root.oppState == 6) {
			currentLevelIcon.gotoAndStop(1);
			if (ttlProf >= ttlAmnt) {
				_root.endArrowBlue._y = _root.mcPaintArea._y + crrY - _root.endArrowBlue._height / 2;
				_root.endArrowGray._y = -1000;
			} else {
				_root.endArrowGray._y = _root.mcPaintArea._y + crrY - _root.endArrowGray._height / 2;
				_root.endArrowBlue._y = -1000;
			}
			if (!closingSoundPlayed) {
				_root.sndClose.gotoAndPlay(2);
				closingSoundPlayed = true;
			}
		}
	}

	public function padToTwoDigit(n:Number):String {
		if (n < 10) {
			return "0" + n;
		}
		return "" + n;
	}
	
	private function sortInvLevelsAsc(invLvls:Array):Array {
		if (invLvls.length < 2) {
			return invLvls;
		}
		
		var tmp:Number;
		var swapped:Boolean;
		var i:Number;
		do {
			swapped = false;
			for (i = 0; i < invLvls.length - 1; i++) {
				if (invLvls[i] > invLvls[i + 1]) {
					tmp = invLvls[i];
					invLvls[i] = invLvls[i + 1];
					invLvls[i + 1] = tmp;
					swapped = true;
				}
			}
		} while (swapped);
		return invLvls;
	}
	
	public function getInvestments():Array {
		return investments;
	}
	
	public function getRates():Array {
		return rates;
	}
	
	public function getAreas():Array {
		return chartAreas;
	}
	
	private function drawWaiting4Progress():Void {
		var lastInvest10Min:Number = _root.counterDown._x - paintArea._x - chartStartX;

//		paintArea.lineStyle(1, 0x000000);
//		paintArea.beginFill(0x000000, 100);
//		paintArea.moveTo(chartStartX + lastInvest10Min + 102,  chartStartY + chartHeight + 5);
//		paintArea.lineTo(chartStartX + chartWidth - 1, chartStartY + chartHeight + 5);
//		paintArea.lineTo(chartStartX + chartWidth - 1, chartStartY + chartHeight + 2);
//		paintArea.lineTo(chartStartX + lastInvest10Min + 102,  chartStartY + chartHeight + 2);
//		paintArea.lineTo(chartStartX + lastInvest10Min + 102,  chartStartY + chartHeight + 5);
//		paintArea.endFill();

		if (rates[rates.length - 1].getChartXPosition() - displayOffset > lastInvest10Min + 101) {
			var rightEndX:Number = chartStartX + 1 + rates[rates.length - 1].getChartXPosition() - displayOffset;
			rightEndX = Math.min(rightEndX, chartStartX + chartWidth - 2);
			paintArea.lineStyle(1, 0xFF0000);
			paintArea.beginFill(0xFF0000, 100);
			paintArea.moveTo(chartStartX + lastInvest10Min + _root.TEN_MIN_LEN + 2, chartStartY + chartHeight + 5);
			paintArea.lineTo(rightEndX, chartStartY + chartHeight + 5);
			paintArea.lineTo(rightEndX, chartStartY + chartHeight + 2);
			paintArea.lineTo(chartStartX + lastInvest10Min + _root.TEN_MIN_LEN + 2, chartStartY + chartHeight + 2);
			paintArea.lineTo(chartStartX + lastInvest10Min + _root.TEN_MIN_LEN + 2, chartStartY + chartHeight + 5);
			paintArea.endFill();
		}
	}
	
	private function drawArrow(tipY:Number, color:Number):Void {
		var tipX = chartStartX + chartWidth + 5;
		with (paintArea) {
			lineStyle(1, color);
			beginFill(color, 100);
			moveTo(tipX, tipY);
			lineTo(tipX + 7, tipY - 7);
			lineTo(tipX + 7, tipY - 3);
			lineTo(tipX + 15, tipY - 3);
			lineTo(tipX + 15, tipY + 3);
			lineTo(tipX + 7, tipY + 3);
			lineTo(tipX + 7, tipY + 7);
			lineTo(tipX, tipY);
			endFill();
		}
	}
	
//	private function calcSecSinceOppOpen(time:String, ignoreEndOfHour:Boolean):Number {
//		var endOfHour:Boolean = rates.length > 400; // if we have "many" rates this is the end of the hour ;)
//		if (ignoreEndOfHour) {
//			endOfHour = false;
//		}
//		var timeMins:Number = new Number(substring(time, 4, 2));
//		var timeSecs:Number = new Number(substring(time, 7, 2));
//		if (oppStartMin == 30) {
//			if (timeMins < 30) {
//				timeMins += 60;
//			} else {
//				if (endOfHour) {
//					timeMins += 60;
//				}
//			}
//		} else {
//			if (timeMins < 30 && endOfHour) {
//				timeMins += 60;
//			}
//		}
//		return (timeMins - oppStartMin) * 60 + timeSecs;
//	}

	public function addDay(time:Date, d:Number):Void {
		// TODO: implement this and the folowing - month, year
	}

	public function addHours(time:Date, h:Number):Void {
		time.setHours(time.getHours() + h);
//		var newH:Number = time.getHours() + h;
//		if (h > 0) {
//			time.setHours(newH % 24);
//			if (newH > 23) {
//				addDay(time, Math.floor(newH / 60));
//			}
//		}
//		if (h < 0) {
//			if (newH >= 0) {
//				time.setHours(newH);
//			} else {
//				time.setHours(24 + newH % 24);
//				addDay(time, Math.floor(newH / 24));
//			}
//		}
	};
	
	public function addMinutes(time:Date, min:Number):Void {
		time.setMinutes(time.getMinutes() + min);
//		var newMin:Number = time.getMinutes() + min;
//		if (min > 0) {
//			time.setMinutes(newMin % 60);
//			if (newMin > 59) {
//				addHours(time, Math.floor(newMin / 60));
//			}
//		}
//		if (min < 0) {
//			if (newMin >= 0) {
//				time.setMinutes(newMin);
//			} else {
//				time.setMinutes(60 + newMin % 60);
//				addHours(time, Math.floor(newMin / 60));
//			}
//		}
	};
	
	public function addSeconds(time:Date, sec:Number):Void {
		time.setSeconds(time.getSeconds() + sec);
//		var newSec:Number = time.getSeconds() + sec;
//		if (sec > 0) {
//			time.setSeconds(newSec % 60);
//			if (newSec > 59) {
//				addMinutes(time, Math.floor(newSec / 60));
//			}
//		}
//		if (sec < 0) {
//			if (newSec >= 0) {
//				time.setSeconds(newSec);
//			} else {
//				time.setSeconds(60 + newSec % 60);
//				addMinutes(time, Math.floor(newSec / 60));
//			}
//		}
	};
	
	public function getSelectedInList():Number {
		return selectedInList;
	}
	
	public function setSelectedInList(selectedInList:Number):Void {
		this.selectedInList = selectedInList;
	}
	
	public function getServerTimeOffset():Number {
		return serverTimeOffset;
	}
	
	public function setServerTimeOffset(serverTimeOffset:Number):Void {
		if (null == this.serverTimeOffset) {
			_root.writeToLog("Setting serverTimeOffset: " + serverTimeOffset);
			this.serverTimeOffset = serverTimeOffset;
		} else {
			_root.writeToLog("Ignore serverTimeOffset!");
		}
	}
	
	public function getTimezoneOffset():Number {
		return timezoneOffset;
	}
	
	public function setTimezoneOffset(timezoneOffset:Number):Void {
		_root.writeToLog("User timezoneOffset: " + timezoneOffset + " Local timezoneOffset: " + new Date().getTimezoneOffset());
		this.timezoneOffset = timezoneOffset;
	}
	
	private function createInvestmentIcon(type:Number, investmentIndex:Number, invLevel:String):MovieClip {
		var ni:MovieClip;
		if (type == 0) {
			if (chartAreas.length > 1) {
				ni = paintArea.attachMovie("lvlIcon", "crrLvlIcon", paintArea.getNextHighestDepth());
			} else {
				ni = paintArea.attachMovie("lvlIconNI", "crrLvlIcon", paintArea.getNextHighestDepth());
			}
		} else if (type == 1) {
			ni = paintArea.attachMovie("callIcon", "invIcon" + investmentIndex, paintArea.getNextHighestDepth());
		} else {
			ni = paintArea.attachMovie("putIcon", "invIcon" + investmentIndex, paintArea.getNextHighestDepth());
		}
		if (null != invLevel) {
			ni.investmentLevel = invLevel;
			ni.onRollOver = _root.showInvestmentIconLevel;
			ni.onRollOut = _root.hideInvestmentIconLevel;
		}
		return ni;
	}
	
	public function getInvestmentsLoaded():Boolean {
		return investmentsLoaded;
	}
	
	// Calculate period length in seconds.
	public function calcPeriodInSec(periodStart:Date, periodEnd:Date):Number {
		return Math.round((periodEnd.getTime() - periodStart.getTime()) / 1000);
	}
//	public function calcPeriodInSec(periodStart:String, periodEnd:String):Number {
//		var h1:Number = new Number(periodStart.substring(0, 2));
//		var h2:Number = new Number(periodEnd.substring(0, 2));
//		var m1:Number = new Number(periodStart.substring(3, 5));
//		var m2:Number = new Number(periodEnd.substring(3, 5));
//		var s1:Number = new Number(periodStart.substring(6, 8));
//		var s2:Number = new Number(periodEnd.substring(6, 8));
//		
//		var h:Number = h2 - h1;
//		var m:Number = m2 - m1;
//		var s:Number = s2 - s1;
//		
////		_root.writeToLog(
////				"periodStart: " + periodStart +
////				" periodEnd: " + periodEnd +
////				" h1: "  + h1 +
////				" h2: "  + h2 +
////				" m1: "  + m1 +
////				" m2: "  + m2 +
////				" s1: "  + s1 +
////				" s2: "  + s2 +
////				" h: "  + h +
////				" m: "  + m +
////				" s: "  + s);
//		
//		if (h < 0) {
//			h = h + 24;
//		}
//		if (m < 0) {
//			h = h - 1;
//			m = m + 60;
//		}
//		if (s < 0) {
//			m = m - 1;
//			s = s + 60;
//		}
//		return h * 3600 + m * 60 + s;
//	}
	
	// Parse a string in format hh:mm or hh:mm:ss and set its parts
	// to a Date ojbect with the current date.
	public function parseToDate(time:String):Date {
		var d:Date = new Date();
		d.setHours(new Number(substring(time, 1, 2)));
		d.setMinutes(new Number(substring(time, 4, 2)));
		if (time.length > 5) {
			d.setSeconds(new Number(substring(time, 7, 2)));
		} else {
			d.setSeconds(0);
		}
		return d;
	}
	
	// Format a Date to hh:mm:ss or hh:mm.
	public function formatToString(d:Date, addSeconds:Boolean):String {
		if (isNaN(d.getHours()) || isNaN(d.getMinutes())) {
			return "";
		}
		var str:String = padToTwoDigit(d.getHours()) + ":" + padToTwoDigit(d.getMinutes());
		if (addSeconds) {
			str = str + ":" + padToTwoDigit(d.getSeconds())
		}
		return str;
	}

	public function fixTimeZone(d:Date):Void {
		var tzDiff:Number = d.getTimezoneOffset() - timezoneOffset;
		addMinutes(d, tzDiff);
	}
	
	public function fixTime(d:Date):Void {
		addSeconds(d, serverTimeOffset);
		fixTimeZone(d);
//		var tzDiff:Number = d.getTimezoneOffset() - timezoneOffset;
//		addMinutes(d, tzDiff);
	}
	
	public function removeInvestment(id:Number):Boolean {
		_root.writeToLog("RemoveInvestment: " + id);
		var i;
		for (i = 0; i < investments.length; i++) {
			if (investments[i].getId() == id) {
//				if (investments.length == 1) {
//					return true;
//				}
				if (investments[i].getLevelIcon() != undefined) {
					investments[i].getLevelIcon().removeMovieClip();
				}
				_root.lstInvestments.removeItemAt(i);
				var newInvestments:Array = new Array();
				var j;
				for (j = 0; j < investments.length; j++) {
					if (j != i) {
						newInvestments[newInvestments.length] = investments[j];
					}
				}
				investments = newInvestments;
				init();
				return (investments.length == 0);
			}
		}
		return false;
	}
}