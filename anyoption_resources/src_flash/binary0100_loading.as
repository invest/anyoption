﻿_root.writeToLog("binary0100_loading.as");

mcWaitingForExpiry._visible = false;

// keep the design
tfLoading.setNewTextFormat(tfLoading.getTextFormat(1, 2));

// set texts
_root.writeToLog("tfEventTony: " + tfEventTony + " txtLoadingEvent: " + txtLoadingEvent + " tfBuyReturn: " + tfBuyReturn);
tfEventTony.html = true;
tfEventTony.htmlText = txtLoadingEvent;
tfLoading.text = txtLoading;
tfOffer.text = "";
tfBid.text = "";
tfBuyReturn.htmlText = txtReturn.replace("{0}", " ... ");
tfBuyMaxProfit.htmlText = txtMaxProfit.replace("{0}", " ... ");
tfBuyMaxRisk.htmlText = txtMaxRisk.replace("{0}", " ... ");
tfSellReturn.htmlText = txtReturn.replace("{0}", " ... ");
tfSellMaxProfit.htmlText = txtMaxProfitSell.replace("{0}", " ... ");
tfSellMaxRisk.htmlText = txtMaxRiskSell.replace("{0}", " ... ");
