﻿import com.anyoption.EtChart;

// pMarketId = ...;
// pOppId = ...;
// pClassId = ...;
// pCalcGame = ...;
// pCommand = ...;
// pDomain = ...;

writeToLog("Version " + VERSION);
writeToLog("Parameters - pMarketId: " + pMarketId + " pOppId: " + pOppId + " pClassId: " + pClassId + " pCalcGame: " + pCalcGame + " pCommand: " + pCommand + " pDomain: " + pDomain);

var TIME_LEFT_X = mcTimeLeft._x;
mcTimeLeft._x = -1000;
mcSelectedMask._y = -1000;
txtCall._y = -1000;
txtPut._y = -1000;
mcWaitingForExpiry._x = -1000;
var BTN_SCROLL_BACK_X = btnScrollBack._x;
btnScrollBack._x = -1000;
var BTN_SCROLL_FORWARD_X = btnScrollForward._x;
btnScrollForward._x = -1000;
var ERR_LOADING_X = errLoading._x;
errLoading._x = -1000;

//if (pCommand == 3) {
//	showLast10Min();
//}
var waitingForExpiry:Boolean = false;
//if (pCommand == 4 || pCommand == 5) {
if (pCommand == 6) {
	showWaitingForExpiry();
}

var serverTimeOffset:Number;
var timezoneOffset:Number;
var chart:EtChart = new EtChart(mcChart);
var labelsX:Array = new Array();
labelsX[0] = lblX0;
labelsX[1] = lblX1;
labelsX[2] = lblX2;
labelsX[3] = lblX3;
labelsX[4] = lblX4;
labelsX[5] = lblX5;
labelsX[6] = lblX6;
var labelsY:Array = new Array();
labelsY[0] = lblY0;
labelsY[1] = lblY1;
labelsY[2] = lblY2;
labelsY[3] = lblY3;
var currentLevel:Number = 0;
var currentLevelTime:Date = null;
var decimalPoint:Number = 0;
var hasPreviousHour:Boolean = false;
var showingPreviousHour:Boolean = false;
var previousHourLoaded:Boolean = false;
var ratesToLoad:Number = 1;
var oppState:Number = 0;
var scheduled:Number = 1;
var processLS:Boolean = false;

// writeToLog(pOppId + "_chart");
// var bridge = new JavaScriptBridge(pOppId + "_chart");

function loadRates():Void {
	writeToLog("Loading rates: " + ratesToLoad);
	trace("Loading rates: " + ratesToLoad);
	var loadingStartTime:Date = new Date();
	var histXml:XML = new XML();
	histXml.ignoreWhite = true;
	histXml.onHTTPStatus = logHttpStatus;
//	histXml.onData = logResponseBody;
	histXml.onLoad = function(success:Boolean) {
		writeToLog("Loading rates " + success);
		trace("Loading rates " + success);
		if (ratesToLoad == 1) { // current hour
			if (success && new Date().getTime() - loadingStartTime.getTime() < 10000) {
				errLoading._x = -1000;
				loadSettings(histXml.childNodes[0].childNodes[0]);
				chart.parseRates(histXml.childNodes[0].childNodes[1]);
				chart.initXLabels();
				chart.repaint();
				updateScrollButtons();
	
				processLS = true;
			} else {
				errLoading._x = ERR_LOADING_X;
				writeToLog("Loading rates failed " + histXml.status);
				trace("Loading rates failed " + histXml.status);
				setTimeout(loadRates, 10000);
			}
		} else { // previous hour
			if (success) {
				chart.parsePreviousHourRates(histXml.childNodes[0].childNodes[1]);
				_root.previousHourLoaded = true;
				chart.scrollBack();
				_root.updateScrollButtons();
			}
		}
	}
//	var u:String = _root._url;
////	_root.writeToLog("_root._url: " + u);
////	var domain:String = _root._url;
//	if (u.indexOf("?") != -1) {
//		u = u.substr(0, u.indexOf("?"));
////		domain = domain.substr(0, domain.indexOf("?"));
//	}
//	var hasSubdomain:Boolean = false;
//	if (u.substr(0, 14) == "http://images.") {
//		hasSubdomain = true;
//		u = "http://www." + u.substr(14);
////		domain = "www." + domain.substr(14);
//	}
//	if (u.substr(0, 15) == "https://images.") {
//		hasSubdomain = true;
//		u = "https://" + u.substr(15);
////		domain = domain.substr(15);
//	}
//	u = u.substr(0, u.length - 10); // drop from the end "/chart.swf"
//	u = u.substr(0, u.lastIndexOf("/")); // drop the images skin subfolder
//	if (!hasSubdomain) {
////		u = u.substr(0, u.length - 17); // drop from the end "/images"
//		u = u.substr(0, u.lastIndexOf("/")); // drop from the end "/images"
//	}
//	var u:String = String(ExternalInterface.call("getFlashRequestPath"));
//	u = u + "/jsp/xml/chartData.xml?marketId=" + pMarketId + "&oppId=" + pOppId + "&history=" + ratesToLoad + "&t=" + new Date().getTime() + Math.round(Math.random() * 1000);
	u = "http://" + pDomain + "/jsp/xml/chartData.xml?marketId=" + pMarketId + "&oppId=" + pOppId + "&history=" + ratesToLoad + "&t=" + new Date().getTime() + Math.round(Math.random() * 1000);
	_root.writeToLog("My URL: " + u);
	trace("My URL: " + u);
//	if (domain.indexOf("/") != -1) {
//		domain = domain.substr(0, domain.indexOf("/"));
//	}
//	if (domain.indexOf(":") != -1) {
//		domain = domain.substr(0, domain.indexOf(":"));
//	}
//	_root.writeToLog("Allow cross domain scripting from: " + domain);
//	System.security.allowDomain(domain);
	System.security.allowDomain("*");
//	histXml.load(u + "/jsp/xml/chartData.xml?marketId=" + pMarketId + "&oppId=" + pOppId + "&t=" + new Date().getTime() + Math.round(Math.random() * 1000));
	histXml.load(u);
};

//function startLS():Void {
//	writeToLog("Starting LS...");
////	var myTable = new FlashTable("aotps_" + scheduled + "_" + pMarketId, "ET_LEVEL ET_STATE", "COMMAND");
//	var myTable = new FlashTable("aotps_2_" + pMarketId, "ET_LEVEL ET_STATE", "COMMAND");
//	myTable.setDataAdapter("JMS_ADAPTER");
//	myTable.setSnapshotRequired(true);
//	myTable.setRequestedMaxFrequency(1);
//	myTable.onItemUpdate = function (item, itemUpdate) {
	function updateItem(pLevel, pState, pTime) {
		if (!processLS) {
			return;
		}
//		writeToLog("pLevel: " + pLevel + " pState: " + pState + " pTime: " + pTime);
		var crrLvlTxt:String = pLevel;
		var crrState:Number = new Number(pState);
//		var crrLvlTxt:String = itemUpdate.getNewValue(1);
//		var crrState:Number = new Number(itemUpdate.getNewValue(2));
		var crrLvl = new Number(crrLvlTxt.replace(",", ""));
		if (crrLvl == 0 || crrState < 2 || crrState > 6 || (waitingForExpiry && oppState == 6)) {
			return;
		}
//		if (currentLevel == 0) {
//			decimalPoint = extractDecimalPoint(crrLvlTxt);
//			chart.resetYLabels();
//			writeToLog("decimalPoint: " + decimalPoint);
//		}
		currentLevel = crrLvl;
		currentLevelTime = new Date(new Number(pTime));
		fixTimeZone(currentLevelTime);
		chart.setCurrentLevel(currentLevel, currentLevelTime);
		chart.repaint();
		
		oppState = crrState;
		if (oppState > 3) {
			hideCall();
			hidePut();
		}
	}
//
//	bridge.onStatusChange = function(newStatus) {
//		writeToLog("Lightstreamer engine status: " + newStatus);
//	};
//	bridge.addTable(myTable, "etChart" + pMarketId);
//
//	//notify the page that we are ready
//	bridge.bind();
//}

function loadSettings(xmlNode:XMLNode):Void {
	serverTimeOffset = calcPeriodInSec(new Date(), new Date(new Number(xmlNode.childNodes[0].childNodes[0].nodeValue)));
	writeToLog("ServerOffset: " + serverTimeOffset);
	timezoneOffset = new Number(xmlNode.childNodes[1].childNodes[0].nodeValue);
	writeToLog("User timezoneOffset: " + timezoneOffset + " Local timezoneOffset: " + new Date().getTimezoneOffset());
	var tec:Date = new Date(new Number(xmlNode.childNodes[2].childNodes[0].nodeValue));
	fixTimeZone(tec);
	chart.setTimeEstClosing(tec);
	writeToLog("TimeEstClosing: " + tec);
	var tfi:Date = new Date(tec.getTime());
	_root.addHours(tfi, -1);
	writeToLog("timeFirstInvest: " + tfi);
	chart.setTimeFirstInvest(tfi);
	scheduled = new Number(xmlNode.childNodes[3].childNodes[0].nodeValue);
	writeToLog("Scheduled: " + scheduled);
	decimalPoint = new Number(xmlNode.childNodes[4].childNodes[0].nodeValue);
	writeToLog("decimalPoint: " + decimalPoint);
	hasPreviousHour = xmlNode.childNodes[5].childNodes[0].nodeValue == "1";
	writeToLog("hasPreviousHour: " + hasPreviousHour);
}

_root.onEnterFrame = function () {
	if (!(waitingForExpiry && oppState == 6)) {
		chart.timeTicker();
	}
};

function updateScrollButtons():Void {
	if (chart.getCanScrollBack()) {
		btnScrollBack._x = BTN_SCROLL_BACK_X;
	} else {
		btnScrollBack._x = -1000;
	}
	if (chart.getCanScrollForward()) {
		btnScrollForward._x = BTN_SCROLL_FORWARD_X;
	} else {
		btnScrollForward._x = -1000;
	}
}

btnScrollBack.onPress = function() {
	chart.scrollBack();
	updateScrollButtons();
}

btnScrollForward.onPress = function() {
	chart.scrollForward();
	updateScrollButtons();
}

function showCall():Void {
	chart.setShowCall(true);
	chart.repaint();
};

function hideCall():Void {
	chart.setShowCall(false);
	mcSelectedMask._y = -1000;
	txtCall._y = -1000;
};

function showPut():Void {
	chart.setShowPut(true);
	chart.repaint();
};

function hidePut():Void {
	chart.setShowPut(false);
	mcSelectedMask._y = -1000;
	txtPut._y = -1000;
};

function showLast10Min():Void {
	mcTimeLeft._x = TIME_LEFT_X;
};

function showWaitingForExpiry():Void {
	mcTimeLeft._x = -1000;
	mcWaitingForExpiry._x = 0;
	mcBkgnd._x = -1000;
	waitingForExpiry = true;
//	mcWhiteBackground._alpha = 70;
};

function freezeChart():Void {
	chart.setFreeze(true);
}

function unfreezeChart():Void {
	chart.setFreeze(false);
	chart.repaint();
}

ExternalInterface.addCallback("showCall", this, showCall);
ExternalInterface.addCallback("hideCall", this, hideCall);
ExternalInterface.addCallback("showPut", this, showPut);
ExternalInterface.addCallback("hidePut", this, hidePut);
ExternalInterface.addCallback("showLast10Min", this, showLast10Min);
ExternalInterface.addCallback("showWaitingForExpiry", this, showWaitingForExpiry);
ExternalInterface.addCallback("freezeChart", this, freezeChart);
ExternalInterface.addCallback("unfreezeChart", this, unfreezeChart);
ExternalInterface.addCallback("updateItem", this, updateItem);

loadRates();