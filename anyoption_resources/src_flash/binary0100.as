﻿import mx.controls.ComboBox;
import com.anyoption.EtChart;

// this are the parameters the component expect from flashVars:
// pMarketId = ...;
// pOpportunityId = ...;
// pClassId = ...;
// pCalcGame = ...;

_root.writeToLog("Version " + _root.VERSION);
_root.writeToLog("Parameters - pOpportunityId: " + pOpportunityId + " pMarketId: " + pMarketId + " pClassId: " + pClassId + " pCalcGame: " + pCalcGame);

var COLOR_BUY = 0x4688AB;
var COLOR_OFFER = 0x093755;
var COLOR_BUY_DISABLED = 0x73A2C0;

var OPPORTUNITY_TYPE_ABOVE = 4;
var OPPORTUNITY_TYPE_BELOW = 5;

var FRAME_LOADING = 2;
var FRAME_ABOVE = 3;
var FRAME_BELOW = 4;
var FRAME_ABOVE_HAPPENED = 5;
var FRAME_ABOVE_NOT_HAPPENED = 6;
var FRAME_BELOW_HAPPENED = 7;
var FRAME_BELOW_NOT_HAPPENED = 8;
var FRAME_SUSPENDED = 9;
var FRAME_OFF_HOURS = 10;

// ILS (1), USD(2), EUR(3), GBP(4), TRY(5), RUB(6), CNY(7), KRW(8), SEK(9), AUD(10), ZAR(11), CZK(12)

var contractsMin:Array  = new Array( 10,  2,  2,  2,  4,  50,  10,  2000,  20,  2, 25, 50);
var contractsMax:Array  = new Array(100, 20, 20, 20, 40, 500, 100, 20000, 200, 20, 250, 500);
var contractsStep:Array = new Array( 10,  2,  2,  2,  4,  50,  10,  2000,  20,  2, 25, 50);
var contractsDef:Array  = new Array( 50, 10, 10, 10, 50, 250,  50, 10000, 100, 10, 125, 250);

// settings
var serverTimeOffset:Number;
var timezoneOffset:Number;
var marketName:String = "";
var timeFirstInvest:Date = null;
var timeLastInvest:Date = null;
var timeEstClosing:Date = null;
var decimalPoint:Number = 0;
var eventLevel:Number = 0;
var balance:Number = 0;
var currencySign:String = "";
var currencyId:Number = 0;
var currencyLeftSymbol:Boolean = true;
var currencyDecimalPoint:Number = 2;
var skinId:Number = 0;
var maxContracts:Number = 0;
var defaultContracts:Number = 0;
var loggedIn:Boolean = false;
var opportunityTypeId:Number = 0;
var commission_amount = 0.0;

// state
var processLS:Boolean = false;
var currentLevelTxt:String = "";
var currentLevel:Number = 0;
var currentOfferTxt:String = "";
var currentOffer:Number = 0;
var currentBidTxt:String = "";
var currentBid:Number = 0;
var currentOppState:Number = 0;

var chart:EtChart = new EtChart(mcChart);
chart.setFreeze(true);
icnCrrLvl._visible = false;
lstSelectEvent._visible = false;
lstSelectEvent.vScrollPolicy = "auto";
tfExpiry._visible = false;
mcWaitingForExpiry._visible = false;

var txtLoadingEvent:String = "";
var txtLoading:String = "";
var txtEventAbove:String = "";
var txtEventAboveDescription:String = "";
var txtEventBelow:String = "";
var txtEventBelowDescription:String = "";
var txtOtherEvents:String = "";
var	txtBuy:String = "";
var	txtBuyOptions:String = "";
var txtBuyNow:String = "";
var	txtSell:String = "";
var txtSellNow:String = "";
var txtReturn:String = "";
var txtMaxProfit:String = "";
var txtMaxRisk:String = "";
var txtMaxProfitSell:String = "";
var txtMaxRiskSell:String = "";
var txtITooltipBuy:String = "";
var txtITooltipSell:String = "";
var txtExpiryOccured:String = "";
var txtExpiryOccuredAbove:String = "";
var txtExpiryOccuredBelow:String = "";
var txtExpiryNotOccured:String = "";
var txtExpiryNotOccuredIdent:String = "";
var txtExpiryNotOccuredAbove:String = "";
var txtExpiryNotOccuredBelow:String = "";
var txtNoTradingTitle:String = "";
var txtNoTradingDetails:String = "";
var txtSuspendUpperLine:Array;
var txtSuspendLowerLine:Array;
var txtStartTime:String = "";
var txtLastTradeTime:String = "";
var txtExpiryTime:String = "";
var txtWaitingForExpiry:String = "";
loadTexts();

// -----------------------------------------------------------------------------------------
// ...
// -----------------------------------------------------------------------------------------

//var subDomain = String(ExternalInterface.call("getAnyoptionSubdomain"));
//_root.writeToLog("subDomain: " + subDomain);
//
//var requestsPath:String = _root._url;
//if (requestsPath.substr(0, 14) == "http://images.") {
//	requestsPath = "http://" + subDomain + "." + requestsPath.substr(14);
//}
//if (requestsPath.substr(0, 15) == "https://images.") {
//	requestsPath = "https://" + subDomain + "." + requestsPath.substr(15);
//}
//if (requestsPath.indexOf("?") != -1) {
//	requestsPath = requestsPath.substr(0, requestsPath.indexOf("?"));
//}
//if (requestsPath.indexOf("/images") != -1) { // drop the "/images/aa_b/xxxx.swf"
//	requestsPath = requestsPath.substr(0, requestsPath.indexOf("/images"));
//} else if (requestsPath.lastIndexOf("/") != -1) { // drop the "/aa_b/xxxx.swf"
//	requestsPath = requestsPath.substr(0, requestsPath.lastIndexOf("/")); // drop the /xxxx.swf
//	requestsPath = requestsPath.substr(0, requestsPath.lastIndexOf("/")); // drop the /aa_b
//}
var requestsPath:String = String(ExternalInterface.call("getFlashRequestPath"));
_root.writeToLog("Requests path: " + requestsPath);
System.security.allowDomain("*");


// -----------------------------------------------------------------------------------------
// Lightstreamer stuff
// -----------------------------------------------------------------------------------------

var OPP_STATE_WAITING_FOR_EXPIRY_1_MIN = 4;
var OPP_STATE_WAITING_FOR_EXPIRY = 5;
var OPP_STATE_CLOSED = 6;
var OPP_STATE_DONE = 7;
var OPP_STATE_SUSPENDED = 9;
var OPP_STATE_UPDATING_PRICES = 12;

var LS_FLD_KEY = 1;
var LS_FLD_OFFER = 2;
var LS_FLD_BID = 3;
var LS_FLD_STATE = 4;

var LS_FLD_CRR_LEVEL = 5; // 1;
var LS_FLD_CRR_LEVEL_TIME = 6; // 2;
var LS_FLD_SUSPENDED_MSG = 7;

var bridgeReady:Boolean = false;
function bridgeOnReady() {
	_root.writeToLog("Lightstreamer bridge ready.");
	bridgeReady = true;
//	connectLightstreamer();
};

function bridgeOnStatusChange(newStatus) {
	_root.writeToLog("Lightstreamer engine status: " + newStatus);
};

function bridgeRebind() {
	_root.writeToLog("bridgeRebind...");
	if (!bridgeReady) {
		bridgeBind();
	}
};

var bridge = new JavaScriptBridge("flashObject");
bridge.onReady = bridgeOnReady;
bridge.onStatusChange = bridgeOnStatusChange;

function bridgeBind() {
	_root.writeToLog("bridgeBind...");
	bridge = new JavaScriptBridge("flashObject");
	bridge.onReady = bridgeOnReady;
	bridge.onStatusChange = bridgeOnStatusChange;
	setTimeout(bridgeRebind, 5000);
	bridge.bind();
}

//bridgeBind();


var binary0100Table = null;
var binaryTable = null;

function connectLightstreamer():Void {
	_root.writeToLog("connectLightstreamer...");
	
	_root.writeToLog("ExternalInterface.available: " + ExternalInterface.available);
	var addResult:Boolean = Boolean(ExternalInterface.call("binary0100AddTable", pMarketId, "" + opportunityTypeId));
//	_root.writeToLog("bridge: " + bridge);
//	var lsGroup:String = "bz_" + pMarketId + "_" + opportunityTypeId;
//	// fields that me may not need: command BZ_MARKET_ID BZ_EVENT BZ_LAST_INV BZ_EST_CLOSE AO_TYPE
//	var lsSchema:String = "key BZ_OFFER BZ_BID BZ_STATE BZ_CURRENT AO_TS BZ_SUSPENDED_MESSAGE";
//	binary0100Table = new FlashTable(lsGroup, lsSchema, "COMMAND");
//	binary0100Table.setDataAdapter("JMS_ADAPTER");
//	binary0100Table.setSnapshotRequired(true);
//	binary0100Table.setRequestedMaxFrequency(1);
//	binary0100Table.onItemUpdate = binary0100OnItemUpdate;
//	bridge.addTable(binary0100Table, "binary0100Table");
//
//	_root.writeToLog("lsGroup: " + lsGroup + " lsSchema: " + lsSchema);
//
//	_root.writeToLog("ExternalInterface.available: " + ExternalInterface.available);
//	ExternalInterface.call("tonysFunction");
//
//	bridge.bind();
////	bridgeBind();

	//notify the page that we are ready
	_root.writeToLog("connectLightstreamer done: " + addResult);

	if (!addResult) {
		_root.writeToLog("will retry...");
		setTimeout(connectLightstreamer, 1000);
	}
};

function binary0100OnItemUpdate(itemPos, itemUpdate) {
	binary0100OnItemUpdateIU(
			itemUpdate.getNewValue(LS_FLD_KEY),
			itemUpdate.isValueChanged(LS_FLD_STATE),
			itemUpdate.getNewValue(LS_FLD_STATE),
			itemUpdate.getNewValue(LS_FLD_SUSPENDED_MSG),
			itemUpdate.getNewValue(LS_FLD_CRR_LEVEL),
			itemUpdate.getNewValue(LS_FLD_CRR_LEVEL_TIME),
			itemUpdate.getNewValue(LS_FLD_OFFER),
			itemUpdate.getNewValue(LS_FLD_BID));
}
	
function binary0100OnItemUpdateIU(iuKey, iuStateChanged, iuState, iuSuspendMsg, iuLevel, iuLevelTime, iuOffer, iuBid, ao_opp_state) {
//	var updateKey:Number = new Number(itemUpdate.getNewValue(LS_FLD_KEY));
	var updateKey:Number = new Number(iuKey);
	if (pOpportunityId != updateKey) {
		_root.writeToLog("pOpportunityId: " + pOpportunityId + " updateKey: " + updateKey);
		return;
	}
	
//	binaryOnItemUpdate(itemPos, itemUpdate);
	
//	if (itemUpdate.isValueChanged(LS_FLD_STATE)) {
	if (iuStateChanged) {
//		var newOppState = new Number(itemUpdate.getNewValue(LS_FLD_STATE));
		var newOppState = new Number(iuState);
		if (currentOppState != newOppState) {
			if (currentOppState == OPP_STATE_UPDATING_PRICES && !submitting) {
				_root.writeToLog("Going out of updating prices.");
				stopProcessingInternal(false);
			}
			_root.writeToLog("State changed from " + currentOppState + " to " + newOppState);
			currentOppState = newOppState;
			if (currentOppState < OPP_STATE_WAITING_FOR_EXPIRY_1_MIN || currentOppState == OPP_STATE_UPDATING_PRICES) {
				openEvent();
				setTextFieldsVisible(true);
			} else if (currentOppState == OPP_STATE_SUSPENDED) {
				_root.writeToLog("Suspending...");
				gotoAndStop(FRAME_SUSPENDED);
//				var msg:String = itemUpdate.getNewValue(LS_FLD_SUSPENDED_MSG);
				var msg:String = iuSuspendMsg;
				_root.writeToLog("Suspend with message: " + msg);
				var arr:Array = msg.split("s");
				_root.writeToLog("Suspend: " + arr[0] + " " + arr[1] + " " + arr[2] + " arr.length: " + arr.length);
				if (txtSuspendUpperLine[new Number(arr[0]) - 1] != undefined) {
					mcSuspended.tfSuspended.setNewTextFormat(mcSuspended.tfSuspended.getTextFormat(1, 2));
					mcSuspended.tfSuspended.text = txtSuspendUpperLine[new Number(arr[0]) - 1];
				}
				if (txtSuspendLowerLine[new Number(arr[1]) - 1] != undefined && skinId > 1) { // no second line and time in eTrader
					mcSuspended.tfDetails.setNewTextFormat(mcSuspended.tfDetails.getTextFormat(1, 2));
					mcSuspended.tfDetails.text = txtSuspendLowerLine[new Number(arr[1]) - 1];
				}
				if (!isNull(arr[2]) && arr[2] != undefined && arr[2] != "" && skinId > 1) { // no second line and time in eTrader
					var tmpDate:Date = parseDate(arr[2]);
					_root.writeToLog(tmpDate);
					var tmpStr:String = formatDate(tmpDate);
					_root.writeToLog(tmpStr);
					mcSuspended.tfDetails.text = mcSuspended.tfDetails.text + " " + tmpStr;
				}
				setTimeout(changeSuspended, 30000);
			} else {
				if (currentOppState == OPP_STATE_WAITING_FOR_EXPIRY_1_MIN || currentOppState == OPP_STATE_WAITING_FOR_EXPIRY) {
					setTimeout(stateChangeWaitingForExpiry, 100);
				}
				if (currentOppState == OPP_STATE_CLOSED) {
//					binaryOnItemUpdate(itemUpdate.getNewValue(LS_FLD_CRR_LEVEL), itemUpdate.getNewValue(LS_FLD_CRR_LEVEL_TIME));
					binaryOnItemUpdate(iuLevel, iuLevelTime);
					if (opportunityTypeId == OPPORTUNITY_TYPE_ABOVE) {
						if (currentLevel > eventLevel) {
							showExpiryMsg(FRAME_ABOVE_HAPPENED, txtExpiryOccured, txtExpiryOccuredAbove);
						} else if (currentLevel == eventLevel) {
							showExpiryMsg(FRAME_ABOVE_NOT_HAPPENED, txtExpiryNotOccured, txtExpiryNotOccuredIdent);
						} else {
							showExpiryMsg(FRAME_ABOVE_NOT_HAPPENED, txtExpiryNotOccured, txtExpiryNotOccuredBelow);
						}
					} else {
						if (currentLevel < eventLevel) {
							showExpiryMsg(FRAME_BELOW_HAPPENED, txtExpiryOccured, txtExpiryOccuredBelow);
						} else if (currentLevel == eventLevel) {
							showExpiryMsg(FRAME_BELOW_NOT_HAPPENED, txtExpiryNotOccured, txtExpiryNotOccuredIdent);
						} else {
							showExpiryMsg(FRAME_BELOW_NOT_HAPPENED, txtExpiryNotOccured, txtExpiryNotOccuredAbove);
						}
					}
				}
			}
		} else {
			_root.writeToLog("No real change in the state");
		}
		
		if (currentOppState == OPP_STATE_DONE || ao_opp_state == '1') {
			pOpportunityId = 0;
			pMarketId = 0;
			reloadData();
			return;
		}
	}
	
	if (submitting) {
		return;
	}
	
//	currentOfferTxt = itemUpdate.getNewValue(LS_FLD_OFFER);
	currentOfferTxt = iuOffer;
	currentOffer = new Number(currentOfferTxt);
	_root.tfOffer.setNewTextFormat(_root.tfOffer.getTextFormat(1, 2));
	if (currentOffer != 100) {
		_root.tfOffer.text = currentOfferTxt;
	} else {
		_root.tfOffer.text = "100";
	}
	
//	currentBidTxt = itemUpdate.getNewValue(LS_FLD_BID);
	currentBidTxt = iuBid;
	currentBid = new Number(currentBidTxt);
	_root.tfBid.setNewTextFormat(_root.tfBid.getTextFormat(1, 2));
	if (currentBid != 100) {
		_root.tfBid.text = currentBidTxt;
	} else {
		_root.tfBid.text = "100";
	}

//	_root.writeToLog("binary update - offer: " + currentOfferTxt + " bid: " + currentBidTxt);
//	binaryOnItemUpdate(itemPos, itemUpdate);
	setTimeout(binaryOnItemUpdate, 100, iuLevel, iuLevelTime);

//	updateReturnAndProfit();
	setTimeout(updateReturnAndProfit, 100);
	updateRightSideTable();
};

function stateChangeWaitingForExpiry() {
	if (_root._currentframe != FRAME_ABOVE && _root._currentframe != FRAME_BELOW) {
		_root.openEvent();
	}
	_root.setTextFieldsVisible(false);
//	_root.writeToLog("_root.mcWaitingForExpiry._visible : " + _root.mcWaitingForExpiry._visible);
	// keep the design
	_root.mcWaitingForExpiry.tfWaitingForExpiry.setNewTextFormat(_root.mcWaitingForExpiry.tfWaitingForExpiry.getTextFormat(1, 2));
	_root.mcWaitingForExpiry.tfWaitingForExpiry.text = txtWaitingForExpiry;
	_root.mcWaitingForExpiry._visible = true;
//	_root.writeToLog("_root.mcWaitingForExpiry._visible : " + _root.mcWaitingForExpiry._visible);
	_root.tfExpiry._visible = false;
	_root.mcError._visible = false;
};

function changeSuspended() {
	if (currentOppState == OPP_STATE_SUSPENDED) {
		pOpportunityId = 0;
		reloadData();
	}
};

function showExpiryMsg(frame, msgTitle, msgDescription) {
	tfExpiry._visible = true;

	gotoAndStop(frame);
	_root.mcMsgExpired.tfTitle.setNewTextFormat(_root.mcMsgExpired.tfTitle.getTextFormat(1, 2));
	_root.mcMsgExpired.tfTitle.text = msgTitle;
	
	msgDescription = msgDescription.replace("{0}", formatDecimalPoint(eventLevel, decimalPoint));
	msgDescription = msgDescription.replace("{1}", currentLevelTxt);
	_root.mcMsgExpired.tfDescription.html = true;
	_root.mcMsgExpired.tfDescription.htmlText = msgDescription;
};

function binaryOnItemUpdate(iuLevel, iuLevelTime) {
//	_root.writeToLog("binaryOnItemUpdate");
//	var tmpTxt = itemUpdate.getNewValue(LS_FLD_CRR_LEVEL);
//	if (tmpTxt == "0") {
	if (iuLevel == "0") {
		return;
	}
//	currentLevelTxt = tmpTxt;
	currentLevelTxt = iuLevel;
//	_root.writeToLog("update - crr lvl: " + currentLevelTxt);
	currentLevel = new Number(currentLevelTxt.replace(",", ""));
	_root.tfCurrentLevel.text = currentLevelTxt;
	
//	var currentLevelTime:Date = new Date(new Number(itemUpdate.getNewValue(LS_FLD_CRR_LEVEL_TIME)));
	var currentLevelTime:Date = new Date(new Number(iuLevelTime));
	fixTimeZone(currentLevelTime);
	chart.setCurrentLevel(currentLevel, currentLevelTime);
	
	// make sure the event level is in the middle of the Y axis
	if (Math.abs(chart.getYMaxVal() - eventLevel).valueOf() > Math.abs(eventLevel - chart.getYMinVal()).valueOf()) {
		chart.setYMinVal(eventLevel - chart.getYMaxVal() + eventLevel);
	} else {
		chart.setYMaxVal(eventLevel + eventLevel - chart.getYMinVal());
	}

	chart.repaint();

	// position the current level line and label
	_root.mcCurrentLevel._y = _root.mcChart._y + (1 - (currentLevel - chart.getYMinVal()) / (chart.getYMaxVal() - chart.getYMinVal())) * _root.mcChart._height;
	if (currentLevel > eventLevel) {
		_root.tfCurrentLevel._y = _root.mcCurrentLevel._y;
	} else {
		_root.tfCurrentLevel._y = _root.mcCurrentLevel._y - _root.tfCurrentLevel._height;
	}
	
	// adjust the background areas
//	_root.writeToLog("currentLevel: " + currentLevel + " eventLevel: " + eventLevel + " opportunityTypeId: " + opportunityTypeId);
//	var bgc:Number = 0;
	if (currentLevel < eventLevel) {
		if (opportunityTypeId == OPPORTUNITY_TYPE_ABOVE) {
//			bgc = 1;
			_root.mcBgLb._height = _root.mcMidScreen._y - _root.mcBgLb._y;
			_root.mcBgDb._height = 0;
			_root.mcBgDy._height = _root.mcCurrentLevel._y - _root.mcBgDy._y;
			_root.mcBgLy._y = _root.mcCurrentLevel._y;
			_root.mcBgLy._height = _root.mcChart._y + _root.mcChart._height - _root.mcCurrentLevel._y;
		} else {
//			bgc = 2;
			_root.mcBgLy._height = _root.mcMidScreen._y - _root.mcBgLy._y;
			_root.mcBgDy._height = 0;
			_root.mcBgDb._height = _root.mcCurrentLevel._y - _root.mcBgDb._y;
			_root.mcBgLb._y = _root.mcCurrentLevel._y;
			_root.mcBgLb._height = _root.mcChart._y + _root.mcChart._height - _root.mcCurrentLevel._y;
		}
	} else {
		if (opportunityTypeId == OPPORTUNITY_TYPE_ABOVE) {
//			bgc = 3;
			_root.mcBgDy._height = 0;
			_root.mcBgLy._y = _root.mcBgDy._y;
			_root.mcBgLy._height = _root.mcChart._y + _root.mcChart._height - _root.mcBgDy._y;
			_root.mcBgLb._height = _root.mcCurrentLevel._y - _root.mcBgLb._y;
			_root.mcBgDb._y = _root.mcCurrentLevel._y;
			_root.mcBgDb._height = _root.mcMidScreen._y - _root.mcCurrentLevel._y;
		} else {
//			bgc = 4;
			_root.mcBgDb._height = 0;
			_root.mcBgLb._y = _root.mcMidScreen._y;
			_root.mcBgLb._height = _root.mcChart._y + _root.mcChart._height - _root.mcMidScreen._y;
			_root.mcBgLy._height = _root.mcCurrentLevel._y - _root.mcBgLy._y;
			_root.mcBgDy._y = _root.mcCurrentLevel._y;
			_root.mcBgDy._height = _root.mcMidScreen._y - _root.mcCurrentLevel._y;
		}
	}
//	_root.writeToLog("bgc: " + bgc + " ly(" + _root.mcBgLy._y + ";" + _root.mcBgLy._height + ") dy(" + _root.mcBgDy._y + ";" + _root.mcBgDy._height + ") lb(" + _root.mcBgLb._y + ";" + _root.mcBgLb._height + ") db(" + _root.mcBgDb._y + ";" + _root.mcBgDb._height + ")");
};

function setTextFieldsVisible(vis:Boolean) {
	btnBuy.enabled = vis;
	if (vis) {
		tfBuyLbl.textColor = COLOR_BUY;
//		tfOffer.textColor = COLOR_OFFER;
	} else {
		tfBuyLbl.textColor = COLOR_BUY_DISABLED;
//		tfOffer.textColor = COLOR_BUY_DISABLED;
	}
	tfOffer._visible = vis;
	tfBuyReturn._visible = vis;
//	cbBuyOptions.visible = vis;
	cbBuyOptions._alpha = vis ? 100 : 0;
	mcBuyOptionsBorder._visible = false;
	tfBuyOptionsLbl._visible = vis;
	icnBuyInfo._visible = vis;
	tfBuyMaxProfit._visible = vis;
	tfBuyMaxRisk._visible = vis;
	btnBuyNow._visible = vis;
//	btnBuyNowDisabled._visible = !vis;
	tfBuyNowLbl._visible = vis;

	btnSell.enabled = vis;
	if (vis) {
		tfSellLbl.textColor = COLOR_BUY;
//		tfBid.textColor = COLOR_OFFER;
	} else {
		tfSellLbl.textColor = COLOR_BUY_DISABLED;
//		tfBid.textColor = COLOR_BUY_DISABLED;
	}
	tfBid._visible = vis;
	tfSellReturn._visible = vis;
//	cbSellOptions.visible = vis;
	cbSellOptions._alpha = vis ? 100 : 0;
	mcSellOptionsBorder._visible = false;
	tfSellOptionsLbl._visible = vis;
	icnSellInfo._visible = vis;
	tfSellMaxProfit._visible = vis;
	tfSellMaxRisk._visible = vis;
	btnSellNow._visible = vis;
//	btnSellNowDisabled._visible = !vis;
	tfSellNowLbl._visible = vis;
	
	mcLastTrade._visible = vis;
	tfLastTradeTime._visible = vis;
	tfLastTradeTimeLbl._visible = vis;
};

function updateReturnAndProfit() {
//	_root.writeToLog("updateReturnAndProfit");
	var buyRet:Number = (100 / currentOffer - 1) * 100;
	var dptf = 2;
	if (Math.round(buyRet) == buyRet) {
		dptf = 0;
	}
	tfBuyReturn.htmlText = txtReturn.replace("{0}", formatDecimalPoint(buyRet, dptf) + "%");
	
//	var maxContractsCanBuy = contractsMax[currencyId - 1];
	var maxContractsCanBuy = 10;
//	if (maxContractsCanBuy * currentOffer > balance / 100) {
	if (maxContractsCanBuy * contractsStep[currencyId - 1] * currentOffer > balance / 100) {
//		maxContractsCanBuy = Math.floor(Math.floor(balance / 100 / currentOffer) / contractsStep[currencyId - 1]) * contractsStep[currencyId - 1];
		maxContractsCanBuy = Math.floor(Math.floor(balance / 100 / currentOffer) / contractsStep[currencyId - 1]);
	}
	if (maxContractsCanBuy == 0) {
		maxContractsCanBuy = 1;
	}
	if (!loggedIn) {
		maxContractsCanBuy = 10;
	}
	updateComboBox(cbBuyOptions, maxContractsCanBuy);
	
	var buyOptions:Number = new Number(cbBuyOptions.value);
	tfBuyMaxProfit.htmlText = txtMaxProfit.replace("{0}", formatAmountRound(buyOptions * (100 - currentOffer), true));
	tfBuyMaxRisk.htmlText = txtMaxRisk.replace("{0}", formatAmountRound(buyOptions * currentOffer, true));
	
	var sellRet:Number = (currentBid / (100 - currentBid)) * 100;
	dptf = 2;
	if (Math.round(sellRet) == sellRet) {
		dptf = 0;
	}
	tfSellReturn.htmlText = txtReturn.replace("{0}", formatDecimalPoint(sellRet, dptf) + "%");
	
//	var maxContractsCanSell = contractsMax[currencyId - 1];
	var maxContractsCanSell = 10;
//	if (maxContractsCanSell * (100 - currentBid) > balance / 100) {
	if (maxContractsCanSell * contractsStep[currencyId - 1] * (100 - currentBid) > balance / 100) {
//		maxContractsCanSell = Math.floor(Math.floor(balance / 100 / (100 - currentBid)) / contractsStep[currencyId - 1]) * contractsStep[currencyId - 1];
		maxContractsCanSell = Math.floor(Math.floor(balance / 100 / (100 - currentBid)) / contractsStep[currencyId - 1]);
	}
	if (maxContractsCanSell == 0) {
		maxContractsCanSell = 1;
	}
	if (!loggedIn) {
		maxContractsCanSell = 10;
	}
	updateComboBox(cbSellOptions, maxContractsCanSell);
	
	var sellOptions:Number = new Number(cbSellOptions.value);
	tfSellMaxProfit.htmlText = txtMaxProfitSell.replace("{0}", formatAmountRound(sellOptions * currentBid, true));
	tfSellMaxRisk.htmlText = txtMaxRiskSell.replace("{0}", formatAmountRound(sellOptions * (100 - currentBid), true));
};

function updateComboBox(cb:ComboBox, max:Number) {
	if (cb.length == undefined || cb.length != max) {
		_root.writeToLog("Update combo - max: " + max + " cb: " + cb);
		if (cb.length == undefined) { //  || cb.length < max
//			_root.writeToLog("Adding...");
			var i:Number = 1;
			if (cb.length != undefined) {
				i += cb.length;
			}
			var d:Number = 0;
			for (; i <= max; i++) {
				d = i * contractsStep[currencyId - 1];
				_root.writeToLog("i: " + i + " d: " + d + " cb.length: " + cb.length);
				cb.addItem({label:d, data:d});
			}
			if (cb.length != undefined) {
//				cb.selectedIndex = cb.length - 1;
				if (defaultContracts < max) {
					cb.selectedIndex = defaultContracts - 1;
				} else {
					cb.selectedIndex = max - 1;
				}
			}
			_root.writeToLog("cb.length: " + cb.length + " cb.selectedIndex: " + cb.selectedIndex);
//		} else {
//			_root.writeToLog("Removing...");
//			var crrSel:Number = cb.selectedIndex;
//			do {
//				cb.removeItemAt(cb.length - 1);
//			} while (cb.length > max);
//			if (crrSel > cb.length - 1) {
//				cb.selectedIndex = cb.length - 1;
//			}
		}
	}
};

function updateRightSideTable() {
	if (currentOfferTxt != "" && currentBidTxt != "") {
//		_root.writeToLog(currentOfferTxt + " " + currentBidTxt + " " + currentOppState + " " + currentLevelTxt);
		ExternalInterface.call("updateOfferAndBid", currentOfferTxt, currentBidTxt, "" + currentOppState, currentLevelTxt);
//		getURL("javascript:updateOfferAndBid(" + currentOfferTxt + ", " + currentBidTxt + ", " + currentOppState + ", " + currentLevel + ");");
	} else {
		_root.writeToLog("No OFFER and BID yet.");
	}
};

// -----------------------------------------------------------------------------------------
// Load data from server
// -----------------------------------------------------------------------------------------

function loadData():Void {
	_root.writeToLog("Loading data...");
	var dataXml:XML = new XML();
	dataXml.ignoreWhite = true;
	dataXml.onHTTPStatus = logHttpStatus;
//	dataXml.onData = logResponseBody;
	dataXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading data " + success);
		if (success) {
//			if (shouldLoadTexts) {
//				loadTexts(dataXml.childNodes[0].childNodes[0]);
//			}
			loadSettings(dataXml.childNodes[0].childNodes[0]);
			if (pOpportunityId > 0) {
				chart.parseRates(dataXml.childNodes[0].childNodes[1]);
//				chart.init();
//				updateEvent();
				refreshRightSideTable(0, 0);

				setTimeout(connectLightstreamer, 1000);
			}
		} else {
			errMsgClip._x = 306;
			_root.writeToLog("Loading data failed - status: " + dataXml.status);
		}
	}
	dataXml.load(requestsPath + "/jsp/xml/binary0100Data.xml?marketId=" + pMarketId + "&oppId=" + pOpportunityId + "&t=" + getRandomParam());
};

function loadSettings(xmlNode:XMLNode):Void {
	serverTimeOffset = calcPeriodInSec(new Date(), new Date(new Number(xmlNode.childNodes[0].childNodes[0].nodeValue)));
	timezoneOffset = new Number(xmlNode.childNodes[1].childNodes[0].nodeValue);
	currencySign = xmlNode.childNodes[2].childNodes[0].nodeValue;
	balance = new Number(xmlNode.childNodes[3].childNodes[0].nodeValue);
	skinId = new Number(xmlNode.childNodes[4].childNodes[0].nodeValue);
	pMarketId = xmlNode.childNodes[5].childNodes[0].nodeValue;
	pOpportunityId = xmlNode.childNodes[6].childNodes[0].nodeValue;
	loggedIn = xmlNode.childNodes[7].childNodes[0].nodeValue == "1" ? true : false;
	commission_amount = xmlNode.childNodes[20].childNodes[0].nodeValue;
	_root.commision_txt.text = xmlNode.childNodes[19].childNodes[0].nodeValue;
	if (pOpportunityId > 0) {
		marketName = xmlNode.childNodes[8].childNodes[0].nodeValue;
		timeFirstInvest = new Date(new Number(xmlNode.childNodes[9].childNodes[0].nodeValue));
		fixTimeZone(timeFirstInvest);
		chart.setTimeFirstInvest(timeFirstInvest);
		timeLastInvest = new Date(new Number(xmlNode.childNodes[10].childNodes[0].nodeValue));
		fixTimeZone(timeLastInvest);
		timeEstClosing = new Date(new Number(xmlNode.childNodes[11].childNodes[0].nodeValue));
		fixTimeZone(timeEstClosing);
		chart.setTimeEstClosing(timeEstClosing);
		_root.SEC_PER_PIXEL = _root.calcPeriodInSec(timeFirstInvest, timeEstClosing) / mcChart._width;
		decimalPoint = new Number(xmlNode.childNodes[12].childNodes[0].nodeValue);
		eventLevel = new Number(xmlNode.childNodes[13].childNodes[0].nodeValue);
		opportunityTypeId = new Number(xmlNode.childNodes[14].childNodes[0].nodeValue);
		currencyId = new Number(xmlNode.childNodes[15].childNodes[0].nodeValue);
		currencyLeftSymbol = xmlNode.childNodes[16].childNodes[0].nodeValue == "true" ? true : false;
		maxContracts = new Number(xmlNode.childNodes[17].childNodes[0].nodeValue);
		currencyDecimalPoint = new Number(xmlNode.childNodes[18].childNodes[0].nodeValue);
	} else {
		currencyId = new Number(xmlNode.childNodes[8].childNodes[0].nodeValue);
		currencyLeftSymbol = xmlNode.childNodes[9].childNodes[0].nodeValue == "true" ? true : false;
		maxContracts = new Number(xmlNode.childNodes[10].childNodes[0].nodeValue);
		currencyDecimalPoint = new Number(xmlNode.childNodes[11].childNodes[0].nodeValue);
	}
//	if (loggedIn) {
//		defaultContracts = Math.round(maxContracts / 2);
//		defaultContracts = 2;
//	} else {
		defaultContracts = 5;
//	}
	_root.writeToLog(
		"Settings - serverTimeOffset: " + serverTimeOffset +
		" timezoneOffset: " + timezoneOffset +
		" timeFirstInvest: " + timeFirstInvest +
		" timeLastInvest: " + timeLastInvest +
		" timeEstClosing: " + timeEstClosing +
		" eventLevel: " + eventLevel +
		" decimalPoint: " + decimalPoint +
		" eventLevel: " + eventLevel +
		" opportunityTypeId: " + opportunityTypeId +
		" currencyId: " + currencyId +
		" currencySign: " + currencySign +
		" currencyDecimalPoint: " + currencyDecimalPoint +
		" skinId: " + skinId +
		" balance: " + balance +
		" pMarketId: " + pMarketId +
		" pOpportunityId: " + pOpportunityId +
		" loggedIn: " + loggedIn +
		" maxContracts: " + maxContracts +
		" defaultContracts: " + defaultContracts +
		" _root.SEC_PER_PIXEL: " + _root.SEC_PER_PIXEL);
	
	if (pOpportunityId > 0) {
//		openEvent();
	} else {
		_root.gotoAndStop(FRAME_OFF_HOURS);
		mcNoTrading.tfNotAvailable.text = txtNoTradingTitle;
		mcNoTrading.tfDetails.html = true;
		mcNoTrading.tfDetails.htmlText = txtNoTradingDetails;
		ExternalInterface.call("startOffHoursBinary0100");
	}
	
	if(_root.skinId == 1){
		cbOtherEvents.setStyle("textAlign", "right");
		lstSelectEvent.setStyle("textAlign", "right");
		lstSelectEvent.setStyle("marginRight", 10);
	}
};

function openEvent() {
	if (opportunityTypeId == 4) {
		_root.gotoAndStop(FRAME_ABOVE);
	} else {
		_root.gotoAndStop(FRAME_BELOW);
	}
	tfEventLevel.text = formatDecimalPoint(eventLevel, decimalPoint);
	tfStartTime.text = formatToString(timeFirstInvest, false);
	tfLastTradeTime.text = formatToString(timeLastInvest, false);
	tfExpiryTime.text = formatToString(timeEstClosing, false);
};

function getTextFromPage(i):String {
	return String(ExternalInterface.call("getFlashText", "" + i));
};

function loadTexts():Void {
	_root.writeToLog("Loading texts...");

// keep the design
	tfExpiry.setNewTextFormat(tfExpiry.getTextFormat(1, 2));

	// load texts
//	txtBuy = xmlNode.childNodes[0].childNodes[0].nodeValue;
//	txtReturn = xmlNode.childNodes[1].childNodes[0].nodeValue;
//	txtBuyOptions = xmlNode.childNodes[2].childNodes[0].nodeValue;
//	txtMaxProfit = xmlNode.childNodes[3].childNodes[0].nodeValue;
//	txtMaxRisk = xmlNode.childNodes[4].childNodes[0].nodeValue;
//	txtBuyNow = xmlNode.childNodes[5].childNodes[0].nodeValue;
//	txtSell = xmlNode.childNodes[6].childNodes[0].nodeValue;
//	txtSellNow = xmlNode.childNodes[7].childNodes[0].nodeValue;
//	txtStartTime = xmlNode.childNodes[8].childNodes[0].nodeValue;
//	txtLastTradeTime = xmlNode.childNodes[9].childNodes[0].nodeValue;
//	txtExpiryTime = xmlNode.childNodes[10].childNodes[0].nodeValue;
//	mcWaitingForExpiry.tfWaitingForExpiry.text = xmlNode.childNodes[11].childNodes[0].nodeValue;
//	txtEventAbove = xmlNode.childNodes[12].childNodes[0].nodeValue;
//	txtEventAboveDescription = xmlNode.childNodes[13].childNodes[0].nodeValue;
//	txtEventBelow = xmlNode.childNodes[14].childNodes[0].nodeValue;
//	txtEventBelowDescription = xmlNode.childNodes[15].childNodes[0].nodeValue;
//	txtOtherEvents = xmlNode.childNodes[16].childNodes[0].nodeValue;
//	txtNoTradingTitle = xmlNode.childNodes[17].childNodes[0].nodeValue;
//	txtNoTradingDetails = xmlNode.childNodes[18].childNodes[0].nodeValue;
//	txtITooltipBuy = xmlNode.childNodes[19].childNodes[0].nodeValue;
//	txtITooltipSell = xmlNode.childNodes[20].childNodes[0].nodeValue;
//	tfExpiry.text = xmlNode.childNodes[21].childNodes[0].nodeValue;
//	txtExpiryOccured = xmlNode.childNodes[22].childNodes[0].nodeValue;
//	txtExpiryOccuredAbove = xmlNode.childNodes[23].childNodes[0].nodeValue;
//	txtExpiryOccuredBelow = xmlNode.childNodes[24].childNodes[0].nodeValue;
//	txtExpiryNotOccured = xmlNode.childNodes[25].childNodes[0].nodeValue;
//	txtExpiryNotOccuredIdent = xmlNode.childNodes[26].childNodes[0].nodeValue;
//	txtExpiryNotOccuredAbove = xmlNode.childNodes[27].childNodes[0].nodeValue;
//	txtExpiryNotOccuredBelow = xmlNode.childNodes[28].childNodes[0].nodeValue;
//	txtSuspendUpperLine = new Array();
//	txtSuspendUpperLine[0] = xmlNode.childNodes[29].childNodes[0].nodeValue;
//	txtSuspendUpperLine[1] = xmlNode.childNodes[30].childNodes[0].nodeValue;
//	txtSuspendUpperLine[2] = xmlNode.childNodes[31].childNodes[0].nodeValue;
//	txtSuspendUpperLine[3] = xmlNode.childNodes[32].childNodes[0].nodeValue;
//	txtSuspendLowerLine = new Array();
//	txtSuspendLowerLine[0] = xmlNode.childNodes[33].childNodes[0].nodeValue;
//	txtSuspendLowerLine[1] = xmlNode.childNodes[34].childNodes[0].nodeValue;
//	txtSuspendLowerLine[2] = xmlNode.childNodes[35].childNodes[0].nodeValue;
//	txtSuspendLowerLine[3] = xmlNode.childNodes[36].childNodes[0].nodeValue;
	txtBuy = getTextFromPage(0);
	txtReturn = getTextFromPage(1);
	txtBuyOptions = getTextFromPage(2);
	txtMaxProfit = getTextFromPage(3);
	txtMaxRisk = getTextFromPage(4);
	txtBuyNow = getTextFromPage(5);
	txtSell = getTextFromPage(6);
	txtSellNow = getTextFromPage(7);
	txtStartTime = getTextFromPage(8);
	txtLastTradeTime = getTextFromPage(9);
	txtExpiryTime = getTextFromPage(10);
	txtWaitingForExpiry = getTextFromPage(11);
	txtEventAbove = getTextFromPage(12);
	txtEventAboveDescription = getTextFromPage(13);
	txtEventBelow = getTextFromPage(14);
	txtEventBelowDescription = getTextFromPage(15);
	txtOtherEvents = getTextFromPage(16);
	txtNoTradingTitle = getTextFromPage(17);
	txtNoTradingDetails = getTextFromPage(18);
	txtITooltipBuy = getTextFromPage(19);
	txtITooltipSell = getTextFromPage(20);
	tfExpiry.text = getTextFromPage(21);
	txtExpiryOccured = getTextFromPage(22);
	txtExpiryOccuredAbove = getTextFromPage(23);
	txtExpiryOccuredBelow = getTextFromPage(24);
	txtExpiryNotOccured = getTextFromPage(25);
	txtExpiryNotOccuredIdent = getTextFromPage(26);
	txtExpiryNotOccuredAbove = getTextFromPage(27);
	txtExpiryNotOccuredBelow = getTextFromPage(28);
	txtSuspendUpperLine = new Array();
	txtSuspendUpperLine[0] = getTextFromPage(29);
	txtSuspendUpperLine[1] = getTextFromPage(30);
	txtSuspendUpperLine[2] = getTextFromPage(31);
	txtSuspendUpperLine[3] = getTextFromPage(32);
	txtSuspendLowerLine = new Array();
	txtSuspendLowerLine[0] = getTextFromPage(33);
	txtSuspendLowerLine[1] = getTextFromPage(34);
	txtSuspendLowerLine[2] = getTextFromPage(35);
	txtSuspendLowerLine[3] = getTextFromPage(36);
	txtLoadingEvent = getTextFromPage(37);
	txtLoading = getTextFromPage(38);
	txtMaxProfitSell = getTextFromPage(39);
	txtMaxRiskSell = getTextFromPage(40);
};

function updateEvent() {
//	var js:String = "javascript:updateEvent('" + marketName + "', " + pOpportunityId + ", " + opportunityTypeId + ", '" + eventLevel + "', '" + tfExpiryTime.text + "');";
//	_root.writeToLog(js);
//	getURL(js);
	var evtLvl:String = formatDecimalPoint(eventLevel, decimalPoint);
	var expTim:String = formatToString(timeEstClosing, false);
	if (opportunityTypeId == OPPORTUNITY_TYPE_ABOVE) {
		var tmp = txtEventAbove.replace("{0}", _root.marketName);
		tmp = tmp.replace("{1}", evtLvl);
		tfEvent.html = true;
		tfEvent.htmlText = tmp.replace("{2}", expTim);
		tmp = txtEventAboveDescription.replace("{0}", _root.marketName);
		tmp = tmp.replace("{1}", evtLvl);
		tfEventDescription.html = true;
		tfEventDescription.htmlText = tmp.replace("{2}", expTim);
	} else {
		var tmp = txtEventBelow.replace("{0}", _root.marketName);
		tmp = tmp.replace("{1}", evtLvl);
		tfEvent.html = true;
		tfEvent.htmlText = tmp.replace("{2}", expTim);
		tmp = txtEventBelowDescription.replace("{0}", _root.marketName);
		tmp = tmp.replace("{1}", evtLvl);
		tfEventDescription.html = true;
		tfEventDescription.htmlText = tmp.replace("{2}", expTim);
	}
};

function reloadData():Void {
	_root.writeToLog("Reloading data...");
//	bridge.removeTable("binaryTable");
	ExternalInterface.call("binary0100RemoveTable");
	refreshRightSideTable(0, 0, true);
	currentOfferTxt = "";
	currentBidTxt = "";

	// reset
	gotoAndStop(FRAME_LOADING);
	_root.setTextFieldsVisible(true);
	
	loadData();
};

loadData();

// -----------------------------------------------------------------------------------------
// Buy/Sell stuff
// -----------------------------------------------------------------------------------------

var CONTRACT_TYPE_BUY = 4;
var CONTRACT_TYPE_SELL = 5;

var submitting:Boolean = false;
var sellNowColor:Number;

function submitInvestment(type:Number):Void {
	if (submitting || !currentLevel > 0) { //  || currentOppState == OPP_STATE_UPDATING_PRICES
		_root.writeToLog("submitInvestment - submitting: " + submitting + " currentLevel: " + currentLevel + " - return state: " + currentOppState);
		return;
	}
	var isETRegulation = ExternalInterface.call("get_isETRegulation");
	var shouldCheckRegulationAO = ExternalInterface.call("shouldCheckRegulationAO");
	
	//check if ET or AO regulation popup must be shown
	if ((skinId == 1 && isETRegulation) || (skinId != 1 && shouldCheckRegulationAO)) {
		var ajaxCall:LoadVars = new LoadVars();
		
		ajaxCall.prop = 'resp';

		ajaxCall.onHTTPStatus = function(httpStatus:Number) {
			writeToLog("HTTP status is (regulation popup): " + httpStatus);
		};
		ajaxCall.onLoad = function(success:Boolean) {
			var errorCode:String = "";
			if (success) {
				writeToLog("regulationPopupCheck Success - 3 -  " + ajaxCall.resp);
				//call the outside world
				var resp = ExternalInterface.call("parseRegulationResponse", ajaxCall.resp, "binary0100");
				writeToLog("parseRegulationResponse Response - " + resp);
				if (resp) {
					submitInvestmentMaint(type);
				} else {
					return;
				}
			} else {
				_root.writeToLog("regulationPopupCheck Fail");
			}
			// stopProcessingInternal(true);
			_root.writeToLog("regulationPopupCheck done.");
		}
		ajaxCall.sendAndLoad(requestsPath + "/jsp/single_pages/regulationPopupCheck.jsf", ajaxCall, "GET");
	} else {
		submitInvestmentMaint(type);
	}
};

function submitInvestmentMaint(type:Number):Void {
	//Fire optimove pixel
	var resp = ExternalInterface.call("optimoveReportTradeEvent", {from: "0-100", marketId: pMarketId, isUp: (type == CONTRACT_TYPE_BUY)});
	
	startProcessing(true);
	
	var invSubmit:LoadVars = new LoadVars();
	invSubmit.flashBinary0100Submit = "1";
	invSubmit.oppId = _root.pOpportunityId;
	invSubmit.contractType = type;
	if (type == CONTRACT_TYPE_BUY) {
		invSubmit.quote = currentOfferTxt;
		invSubmit.count = cbBuyOptions.value;
	} else {
		invSubmit.quote = currentBidTxt;
		invSubmit.count = cbSellOptions.value;
	}
	invSubmit.currentLevel = currentLevelTxt;
	invSubmit.utcOffset = new Date().getTimezoneOffset();
	invSubmit.t = getRandomParam();
	invSubmit.onHTTPStatus = function(httpStatus:Number) {
	    writeToLog("HTTP status is: " + httpStatus);
	};
	invSubmit.onLoad = function(success:Boolean) {
		var errorCode:String = "";
		if (success) {
			writeToLog("Inv Submit Success - " + invSubmit.reply);
			if (substring(invSubmit.reply, 1, 2) == "00") {
				_root.loadBalance();
				_root.refreshRightSideTable(invSubmit.contractType, invSubmit.count);
//				_root.refreshPageHeader();
			}
			if (invSubmit.reply.length > 4) {
				var frameNumber = new Number(substring(invSubmit.reply, 1, 2));
				_root.mcError.gotoAndStop(frameNumber + 1);
				_root.mcError.tfError.html = true;
				_root.mcError.tfError.htmlText = invSubmit.reply.substring(4);
				var msgHeight = _root.mcError.tfError.getNewTextFormat().getTextExtent(_root.mcError.tfError.text, _root.mcError.tfError._width).height;
				_root.mcError.tfError._y = 0; // reset first to bring back the box height to normal
				_root.mcError.tfError._y = (_root.mcError._height - msgHeight) / 2 - 3;
				_root.writeToLog("boxHeight: " + _root.mcError._height + " msgHeight: " + msgHeight + " y: " + _root.mcError.tfError._y);
				_root.mcError._visible = true;
				errorCode = substring(invSubmit.reply, 3, 2);
				_root.writeToLog("Error code: " + errorCode);
//				if (errorCode != "04" && errorCode != "05") {
					_root.writeToLog("setTimeout - closeErrorMsg");
					setTimeout(closeErrorMsg, 5000);
//				}
			}
		} else {
			_root.writeToLog("Inv Submit Fail");
		}
//		if (errorCode == "04" || errorCode == "05") {
//			_root.writeToLog("Enable page.");
//			ExternalInterface.call("stopProcessingFromFlash");
//			_root.cbOtherEvents.enabled = true;
//			submitting = false;
//		} else {
			stopProcessingInternal(true);
//		}
		_root.writeToLog("Inv Submit done.");
	}
	_root.writeToLog("Submitting investment...");
	invSubmit.sendAndLoad(requestsPath + "/jsp/system/ajax.xml", invSubmit, "POST");
}

function closeErrorMsg() {
	_root.writeToLog("closeErrorMsg");
	_root.mcError._visible = false;
};

function startProcessing(notifyPage:Boolean) {
	_root.writeToLog("startProcessing");
	submitting = true;
	closeErrorMsg();
	cbOtherEvents.enabled = false;
	cbBuyOptions.enabled = false;
	cbSellOptions.enabled = false;
	tfBuyLbl.textColor = COLOR_BUY_DISABLED;
	tfSellLbl.textColor = COLOR_BUY_DISABLED;
	btnBuyNow._visible = false;
	btnSellNow._visible = false;
	btnBuyNowDisabled._visible = true;
	btnSellNowDisabled._visible = true;
	tfOffer.textColor = COLOR_BUY_DISABLED;
	tfBid.textColor = COLOR_BUY_DISABLED;
//	sellNowColor = tfSellNowLbl.textColor;
//	tfSellNowLbl.textColor = tfBuyNowLbl.textColor;
	tfSellNowLbl._visible = false;
	tfSellNowLblDisabled._visible = true;
	if (notifyPage) {
		_root.writeToLog("startProcessing - notify page");
		ExternalInterface.call("startProcessingFromFlash");
	}
};

function startProcessingFromPage() {
	_root.writeToLog("startProcessingFromPage");
	startProcessing(false);
};

function stopProcessingInternal(notifyPage:Boolean) {
	_root.writeToLog("stopProcessing");
//	tfSellNowLbl.textColor = sellNowColor;
	tfSellNowLbl._visible = true;
	tfSellNowLblDisabled._visible = false;
	cbOtherEvents.enabled = true;
	cbBuyOptions.enabled = true;
	cbSellOptions.enabled = true;
	tfBuyLbl.textColor = COLOR_BUY;
	tfSellLbl.textColor = COLOR_BUY;
	btnBuyNow._visible = true;
	btnSellNow._visible = true;
	btnBuyNowDisabled._visible = false;
	btnSellNowDisabled._visible = false;
	tfOffer.textColor = COLOR_OFFER;
	tfBid.textColor = COLOR_OFFER;
	submitting = false;
	if (notifyPage) {
		_root.writeToLog("stopProcessingInternal - notify page");
		ExternalInterface.call("stopProcessingFromFlash");
	}
};

function stopProcessingFromPage() {
	_root.writeToLog("stopProcessingFromPage");
	stopProcessingInternal(false);
};

function loadBalance():Void {
	_root.writeToLog("Loading balance...");
	var balanceXml:XML = new XML();
	balanceXml.ignoreWhite = true;
	balanceXml.onHTTPStatus = logHttpStatus;
//	balanceXml.onData = logResponseBody;
	balanceXml.onLoad = function(success:Boolean) {
		_root.writeToLog("Loading balance " + success);
		if (success) {
			_root.balance = new Number(balanceXml.childNodes[0].childNodes[0].nodeValue);
		} else {
			_root.writeToLog("Loading balance failed " + balanceXml.status);
		}
	}
	balanceXml.load(requestsPath + "/jsp/xml/binary0100Balance.xml?t=" + getRandomParam());
};

function refreshRightSideTable(type, count, hideRightTable) {
	var oppIdToSend = pOpportunityId;
	if (hideRightTable) {
		oppIdToSend = 0;
	}
	_root.writeToLog("refreshRightSideTable - pMarketId: " + pMarketId + " oppIdToSend: " + oppIdToSend + " type: " + type + " count: " + count + " hideRightTable: " + hideRightTable);
//	getURL("javascript:refreshRightSideTable(" + pMarketId + ", " + pOpportunityId + ");");
	ExternalInterface.call("refreshRightSideTable", pMarketId, oppIdToSend, type, count);
};

// -----------------------------------------------------------------------------------------
// Event stuff
// -----------------------------------------------------------------------------------------

var eventsLoading:Boolean = false;

lstSelectEvent.change = function(evt:Object) {
	_root.writeToLog("Event selected - label: " + evt.target.selectedItem.label +
			" data: " + evt.target.selectedItem.data +
			" market: " + evt.target.selectedItem.market);
	lstSelectEvent._visible = false;

	pMarketId = evt.target.selectedItem.market;
	pOpportunityId = evt.target.selectedItem.data;
	
	reloadData();
};
lstSelectEvent.addEventListener("change", lstSelectEvent);

_root.onMouseDown = function() {
	if (!lstSelectEvent._visible) {
		return;
	}
	
	if (_root._xmouse < lstSelectEvent._x || _root._xmouse > lstSelectEvent._x + lstSelectEvent._width ||
		_root._ymouse < lstSelectEvent._y || _root._ymouse > lstSelectEvent._y + lstSelectEvent._height) {
		lstSelectEvent._visible = false;
	}
};

// -----------------------------------------------------------------------------------------
// Other stuff
// -----------------------------------------------------------------------------------------

function formatDate(d:Date):String {
	var currdate:Date = new Date();
	var min:Number = d.getMinutes();
	var s:String;
//	if (d.getDate() == currdate.getDate() && d.getMonth() == currdate.getMonth() && d.getFullYear() == currdate.getFullYear()) {
//		s = d.getHours() + ":" + padToTwoDigit(min) + " " + textToday;
//	} else {
		var year:String = "" + d.getFullYear();
		s = d.getHours() + ":" + padToTwoDigit(min) + ", " + padToTwoDigit(d.getDate()) + "." + padToTwoDigit(d.getMonth() + 1) + "." + year.substring(2);
//	}
	return s;
};

var lastSec:Number = 0;

_root.onEnterFrame = function () {
	if (currentOppState != OPP_STATE_CLOSED) {
		chart.timeTicker();
	}
	
	if (timeLastInvest != null) {
		// update the time line
		var ct:Date = new Date();
		_root.fixTime(ct);
		var crrSec:Number = ct.getSeconds();
		if (lastSec != crrSec) {
			var ctmillis:Number = ct.getTime().valueOf();
			var l5mmillis:Number = timeLastInvest.getTime().valueOf() - 6 * 60 * 1000; // 6 min
			var millisPassed:Number = ct.getTime().valueOf() - timeFirstInvest.getTime().valueOf();
			var fullWidth:Number = _root.mcChart._width - 3 - mcTimeLineOpen._x;
			var millisTotal:Number = timeEstClosing.getTime().valueOf() - timeFirstInvest.getTime().valueOf();
			if (millisPassed > millisTotal) {
				// make sure the red time progress bar doesn't go too much on the right
				millisPassed = millisTotal;
			}
			var timeWidth:Number = millisPassed / millisTotal * fullWidth;
			if (ctmillis <= l5mmillis) {
				mcTimeLineOpen._width = timeWidth;
			} else if (currentOppState < OPP_STATE_WAITING_FOR_EXPIRY_1_MIN) {
				mcTimeLineOpen._width = 0;
				mcTimeLineLast5Min._width = timeWidth;
			} else {
				mcTimeLineOpen._width = 0;
				mcTimeLineLast5Min._width = 0;
				mcTimeLineWaitingForExpiry._width = timeWidth;
			}
			mcTimeLineGlow._x = mcTimeLineOpen._x + timeWidth - mcTimeLineGlow._width + 3; // + 3 because of some error in the glow mc
			
//			if (crrSec == 0 || crrSec == 30) {
//				_root.writeToLog("BUY: " + tfOffer.text + " BUY RETURN: " + tfBuyReturn.text + " SELL: " + tfBid.text + " SELL RETURN: " + tfSellReturn.text + " event level: " + tfEventLevel.text + " level: " + currentLevelTxt);
//			}
			
			if (mcChart.visible) {
				chart.repaint();
			}
			
			lastSec = crrSec;
		}
	}
};

function requestOfferAndBidUpdate() {
	_root.writeToLog("requestOfferAndBidUpdate");
	updateRightSideTable();
};

ExternalInterface.addCallback("requestOfferAndBidUpdate", this, requestOfferAndBidUpdate);
ExternalInterface.addCallback("startProcessingFromPage", this, startProcessingFromPage);
ExternalInterface.addCallback("stopProcessingFromPage", this, stopProcessingFromPage);

function offHoursPopup() {
	_root.writeToLog("offHoursPopup");
	ExternalInterface.call("binary0100_general_exp_pop_up");
};

ExternalInterface.addCallback("binary0100OnItemUpdateIU", this, binary0100OnItemUpdateIU);

function changeMarketFromOutside(MarketId,OpportunityId){
	_root.writeToLog("Market updated from js -  data: " + OpportunityId + " market: " + MarketId);
	lstSelectEvent._visible = false;

	pMarketId = MarketId;
	pOpportunityId = OpportunityId;
	
	reloadData();
}
ExternalInterface.addCallback("changeMarketFromOutside", this, changeMarketFromOutside);