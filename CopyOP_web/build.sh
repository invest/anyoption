#!/bin/sh

case $1 in
  local|live|docker|staging|bgtest)
	if [ $1 = 'docker' -o $1 = 'staging' ] ; then
		base='/'
	else
		base='/jsonService/'
	fi
	echo "Installing grunt plugins" ;
	npm install;

	echo "Executing grunt tasks" ;
	echo $baseref
	grunt default --env=$1 --baseref=$base;
	grunt prepare-msgs ;
	grunt prepare-convert ;
	grunt convert ;
	grunt post-clear ;;
  *) echo "Provide environment: live, local, bgtest, staging or docker" ;;
esac
