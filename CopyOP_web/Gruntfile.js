module.exports = function(grunt) {
  var time = new Date().getTime();
  var base = grunt.option('baseref');
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    timestampVar: time,
    baseref: base,
    concat: {
		html : {
                    options: {
                        process: function(src, filepath) {
                          var filename = filepath.substring(filepath.lastIndexOf("/") + 1, filepath.length)
                          return '<script type="text/ng-template" id="html/templates/' + filename + '">' + '\n' +
                            src + '\n' + '</script>'
                        }
                    },
                    files: {
                        'build/html/templates/GeneralTemplate.html': 'build/html/templates/*.html'
                    }
		},
		jscopyop : {
		    src : [ 'war/js/*.js', 'res/'+ grunt.option('env') +'/_settings2.js' ],
                    dest: 'tmp/js/copyop-raw.js'
		},
		jslibs : {
		    src : 'war/js/lib/*.js',
		    dest : 'build/js/lib/libs.js'
		}
	    },
    jsObfuscate: {
		test: {
			files: {
				'build/js/copyop.js' : 'tmp/js/copyop-raw.js'
			}
		}
    	    },
    uglify: {
		all : {
		    files : {
			'build/js/copyop.js' : 'tmp/js/copyop-raw.js',
			'build/html/mobile/js/_settings.js' : 'tmp/js/_settings.js',
			'build/html/mobile/js/functions.js' : 'tmp/js/functions.js'
		    }
		}
             },
    propertiesToJSON: {
		main : {
		    src : [ 'temp-msgs/res/*.properties' ],
		    dest: 'build/msgs',
		    options: {
    			      splitKeysBy: '&SPLITkey&'
		             }
		      }
                },
    copy: {
    		msgs: {
		    files: [
		      { expand: true, src: ['res/*.properties'], dest: 'temp-msgs/', filter: 'isFile' }
		    ]
	  	},
		resources: {
		    files: [
		      { expand: true, cwd: 'war/avatar', src: '*' , dest: 'build/avatar/' },
		      { expand: true, cwd: 'war/img', src: '**/*', dest: 'build/img/' },
		      { expand: true, cwd: 'war/html', src: ['htaccess', 'minisite/*', 'mobile/**/*', 'index.html', 'templates/*'], dest: 'build/html/' },
		      { expand: true, cwd: 'war/html', src: 'index.html', dest: 'build/' },
		      { expand: true, cwd: 'war/root_includes', src: '*', dest: 'build/' },
		      { expand: true, cwd: 'war/js', src: ['_settings.js', 'functions.js'], dest: 'tmp/js' },
		      { expand: true, cwd: 'war/css', src: 'style.css', dest: 'build/css' }
		    ]
		}
    },
    search: {
        metadata: {
            files: {
                src: 'temp-msgs/res/*.properties'
            },
            options: {
                searchString: [ '#{.+#', '#}#' ],
                logFile: "tmp/results.json"
		}
            }
    },
    'string-replace': {
	dist: {
	    options: {
	      replacements: [
		{
		  pattern: '{base}',
  		  replacement: '<%= baseref %>'
		}
	      ]
	    },
	    files: {
	      'build/': 'build/index.html',
	      'build/html/': 'build/html/index.html',
	      'build/html/mobile/': [ 'build/html/mobile/agreement.html', 'build/html/mobile/bonus-terms.html' ]
	    }
        },
	timestamp: {
	    options: {
	      replacements: [
		{
		  pattern: /(cvsTimeStamp)+/g,
  		  replacement: '<%= timestampVar %>'
		}
	      ]
	    },
	    files: {
	      'build/': [ 'build/html/**/*', 'build/index.html' ]
	    }
        }
    },
    clean: {
	temps: [ 'tmp/', 'temp-msgs/' ],
	build: [ 'build/' ]
    },
    rename: {
 	 main: {
	    files: [
  		{ src: [ 'build/html/htaccess' ], dest: 'build/html/.htaccess'}
	    ]
 	 }
    }
  }
);

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('js-obfuscator');
  grunt.loadNpmTasks('grunt-properties-to-json');
  grunt.loadNpmTasks('grunt-search');
  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-rename');
  grunt.registerTask('default', [ 'clean:build', 'copy:resources', 'concat:html', 'concat:jscopyop', 'concat:jslibs', 'uglify:all', 'rename', 'string-replace:dist', 'string-replace:timestamp']);
  grunt.registerTask('prepare-msgs', ['copy:msgs', 'search:metadata']);
  grunt.registerTask('prepare-convert', function() {
	console.log('Adjusting properties files for convesion...');
	var json = grunt.file.readJSON('tmp/results.json');
	var results = json.results;
	var fs = require('fs');
	var text;
	for (resultFile in results) {
		var data = fs.readFileSync(resultFile).toString().split("\n");
		matchesInFile = results[resultFile]; //get the matches in a file
		for ( i=0; i < matchesInFile.length; i++ ) {
			if ( ( i + 2 ) % 2 != 0 ) {
				fromLine = matchesInFile[i-1].line;
				toLine = matchesInFile[i].line - 1;
				jsonObjAccessor = matchesInFile[i-1].match;
				jsonObjAccessor = jsonObjAccessor.replace('#', '');
				jsonObjAccessor = jsonObjAccessor.replace('#', '');
				jsonObjAccessor = jsonObjAccessor.replace('{', '');
				for ( line = fromLine; line < toLine; line++ ) {
					data[line] = jsonObjAccessor + "&SPLITkey&" + data[line];
				}
			}
		}
		text = data.join("\n");
		grunt.file.write(resultFile, text, { encoding: 'UTF-8' });
	}
  });
  grunt.registerTask('convert', 'propertiesToJSON');
  grunt.registerTask('post-clear', 'clean:temps');
};
