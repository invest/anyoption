copyOpApp.controller('sendPasswordCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	//Set email to be checked by default
	$scope.form = {};
	$scope.form.type = 'email';
	$scope.step = 1;
	
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.username_email = $rootScope.getMsgs('username_email').replace('&#92;','\\');
			$scope.headerText = $rootScope.getMsgs('password.subheading');
		}
	}

	$scope.getForgetPasswordContacts = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var UserMethodRequest = jsonClone(getUserMethodRequest(), {
					userName: $scope.form.userName
				});
				
				$http.post(settings.jsonLink + 'getForgetPasswordContacts', UserMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (data.errorCode != 0) {
							data.userMessages = [{field:'userName', message: $rootScope.getMsgs('error_username')}]
						}
						data.globalErrorField = 'globalErrorFieldFirst';
						if (!handleErrors(data, 'getForgetPasswordContacts')) {
							$scope.info = data;
							$scope.headerText = $rootScope.getMsgs('forgot_password_header2');
							$scope.step = 2;
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$scope.sendPassword = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var InsertUserMethodRequest = jsonClone(getMethodRequest(), {
					// register: {
						// email: '',
						// mobilePhone: '',
						// mobilePhonePrefix: ''
					// },
					sendMail: ($scope.form.type == 'email') ? true : false,
					sendSms: ($scope.form.type == 'sms') ? true : false
				});
				
			
				$http.post(settings.jsonLink + 'sendPassword', InsertUserMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.loginNotCheck = true;
						if (!handleErrors(data, 'sendPassword')) {
							$scope.step = 1;
							$scope.successMsgs = data.userMessages[0].message;
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	waitForIt();
}]);