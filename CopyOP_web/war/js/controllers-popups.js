//the purpose of this is to prevent minifying of function params
//TODO -> http://stackoverflow.com/questions/13944207/angularjs-dynamically-assign-controller-from-ng-repeat/23216850#23216850
eval("function holyShit($rootScope, $scope, $http){$scope.popUps[$scope.popUps.length-1].ctr($rootScope, $scope, $http)}");

copyOpApp.controller('copyPopupCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.toolTipInfo = {
		hide: 1,
		fullyCopyStep:-1
	}
	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		
		$scope.config.count = $rootScope.safeMod.isSafeMode ? $rootScope.safeMod.minCopyUsers : 10;//1
		$scope.config.howMany = $rootScope.safeMod.isSafeMode ? 1 : 0;//1
		$scope.config.safeModeOn = $rootScope.safeMod.isSafeMode;
		$scope.config.command = false;
		$scope.config.disabled = false;
		$scope.config.assets = [];
		$scope.config.assetsOrgn = [];
		$scope.config.assetsSelected = 0;
		$scope.config.maxCount =  $rootScope.safeMod.isSafeMode ? $rootScope.safeMod.minCopyUsers : 99;
		
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		$rootScope.popupInfo.destUserId = params.destUserId;
		
		$scope.amoutList = $rootScope.copyopUser.copyAmountsDsp;
		$scope.config.amount = $scope.amoutList[1].amount;
		$scope.config.amountDsp = $scope.amoutList[1].amountDsp;

		$scope.getConfig();
		$scope.getCopyStepsEncourageToolTip();
		$scope.maxAssets = settings.maxAssetSelected;
		
		var walkthroughCookieCopy = localStorage.getItem('walkthroughCopyPopup');
		//walkthroughCopyPopup
		if (isUndefined(walkthroughCookieCopy) || walkthroughCookieCopy == 0) {
			setTimeout(function() {
				$rootScope.openCopyOpPopup({type: 'walkthrough', caller: 'popup', doNotHidePageScroll: false})
				$rootScope.$apply();
				localStorage.setItem('walkthroughCopyPopup', 1);
			}, 1000);
		}
	}
	
	$scope.$watch('config.count', function() {
		if ($scope.config.count > $scope.config.maxCount) {
			$scope.config.count = $scope.config.maxCount;
		} else if ($scope.config.count < 1) {
			$scope.config.count = 1;
		}
	});
	
	$scope.$watch('config.howMany', function() {
		$scope.config.howManyDsp = ($scope.config.howMany == 0 || $scope.config.count == 0) ? $rootScope.getMsgs('max') : $scope.config.count;
	});
	
	$scope.$watch('config.assetsSelected', function() {
		if (!isUndefined($scope.config.assets)) {//for some reason watchCtrl triggers this whatch ?!?!
			$scope.config.assetsDsp = ($scope.config.assetsSelected == 0 || $scope.config.assets.length == 0) ? $rootScope.getMsgs('all') : $scope.config.assets.length;
			if ($scope.config.assetsSelected == 0) {
				$scope.config.assets = [];
			}
		}
	});
	
	$scope.$watch('config.assets.length', function() {
		if (!isUndefined($scope.config.assets)) {//for some reason watchCtrl triggers this whatch ?!?!
			$scope.config.assetsDsp = ($scope.config.assetsSelected == 0 || $scope.config.assets.length == 0) ? $rootScope.getMsgs('all') : $scope.config.assets.length;
		}
	});
	
	$scope.$watch('userInfo.copying', function() {
		$scope.popUpHeader = $rootScope.getMsgs('popUp-copy.header', {nickname: $scope.userInfo.name});
	});
	
	$scope.getConfig = function() {
		if ($scope.userInfo.copying) {
			var profileMethodRequest = jsonConcat(getUserMethodRequest(),{requestedUserId: $scope.config.destUserId})
			$http.post(settings.jsonLink + 'getCopyopCopyUserConfig', profileMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getCopyopCopyUserConfig')) {
						if (!isUndefined($scope.config.obj.marketId) && data.config.assets.length > 0) {
							if ($.inArray($scope.config.obj.marketId, data.config.assets) == -1) {
								data.config.assets.push($scope.config.obj.marketId);
							}
						}
						
						data.config.count = (data.config.count <= 0) ? 0 : data.config.count;
						$scope.config.count = data.config.count;
						$scope.config.howMany = (data.config.count == 0) ? 0 : 1;
						$scope.config.amount = data.config.amount;
						$scope.config.howManyDsp = ($scope.config.howMany == 0 || $scope.config.count == 0) ? $rootScope.getMsgs('max') : $scope.config.count;
						$scope.config.amountDsp = formatAmount(data.config.amount,2);
						$scope.config.command = false;
						if ($scope.userInfo.frozen) {
							$scope.config.command = true;
							$scope.config.disabled = true;
						}
						
						$scope.config.assets = data.config.assets;
						$scope.config.assetsOrgn = cloneObj($scope.config.assets);
						$scope.config.assetsSelected = (data.config.assets.length == 0)? 0 : 1;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		} else if (!isUndefined($scope.config.obj.marketId)) {
			$scope.config.assets.push($scope.config.obj.marketId);
			$scope.config.assetsOrgn = cloneObj($scope.config.assets);
			$scope.config.assetsSelected = 1;
		}
	}
	
	$scope.getCopyStepsEncourageToolTip = function() {
		var CopyStepsEncourageMethodRequest = jsonConcat(getMethodRequest(),{
			copiedUserId: $scope.config.destUserId,
			userId: $rootScope.copyopUser.user.id,
			stepAmount: $scope.amoutList[2].amount 
		})
		$http.post(settings.jsonLink + 'getCopyStepsEncourageToolTip', CopyStepsEncourageMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getCopyStepsEncourageToolTip')) {
					if (data.errorCode == 0) {
						$scope.toolTipInfo = {
							hide: 0,
							fullyCopyStep: data.fullyCopyStep,
							message: data.message.replace(data.profitAmount, '<span>' + data.profitAmount + '</span>').replace(data.stepAmount, '<span>' + data.stepAmount + '</span>')
						}
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
	
	$scope.action = function() {
		if (!$scope.config.command && $rootScope.copyopUser.user.balance <= $scope.config.amount * 100) {
			$rootScope.openCopyOpPopup({type: 'noMoney'});
			return;
		}
		var copyUserConfigMethodRequest = jsonConcat(getUserMethodRequest(),{
			config: {
				userId: $rootScope.copyopUser.userId,
				destUserId: $scope.config.destUserId,
				count: ($scope.config.howMany == 0) ? 0 : $scope.config.count,//max = 0
				amount: $scope.config.amount,
				assets: $scope.config.assets,
				command: ($scope.config.command) ? 'UNCOPY' : 'COPY'
			}
		});
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.jsonLink + 'copyopCopyUserConfig', copyUserConfigMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'copyopCopyUserConfig')) {
						var state = !$scope.config.command;
						$rootScope.usersInfo[$scope.config.destUserId].copying = state;
						var inx = searchJsonKeyInArray($rootScope.copyopCopying, 'userId', $scope.config.destUserId);
						if (inx == -1 && state) {
							$rootScope.copyopCopying.push({userId: $scope.config.destUserId});
							$rootScope.copyopUser.copyopProfile.copying++;
							if (!isUndefined($rootScope.cUser)) {
								if ($rootScope.cUser.sameUser) {
									$rootScope.cUser.copyingCount++;
								} else {
									$scope.cUser.copiersCount++
								}
							}
						} else if (inx > -1 && !state) {
							var inxW = searchJsonKeyInArray($rootScope.copyopWatching, 'userId', $scope.config.destUserId);
							if (inxW > -1) {
								$rootScope.usersInfo[$scope.config.destUserId].watching = false;
								$rootScope.copyopWatching.splice(inxW, 1);
								$rootScope.copyopUser.copyopProfile.watching--;
								if (!isUndefined($rootScope.cUser)) {
									$rootScope.cUser.watchingCount--;
								}
							}
							$rootScope.copyopCopying.splice(inx, 1);
							$rootScope.copyopUser.copyopProfile.copying--;
							if (!isUndefined($rootScope.cUser)) {
								if ($rootScope.cUser.sameUser) {
									$rootScope.cUser.copyingCount--;
								} else {
									$scope.cUser.copiersCount--;
								}
							}
						}
						if ($scope.config.command && !isUndefined($scope.config.obj)) {
							if (!isUndefined($scope.config.obj)) {
								$scope.config.obj.hide = true;
							}
						}
						$rootScope.closeCopyOpPopup();
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		}
	}
	
	$scope.selectAmout = function(amount) {
		$scope.config.amount = amount;
		$scope.config.amountDsp = formatAmount(amount,2);
	}
	
	$scope.changeCount = function(minus) {
		if (minus) {
			if ($scope.config.count > 1) {
				$scope.config.count--;
			}
		} else {
			if ($scope.config.count < $scope.config.maxCount) {
				$scope.config.count++;
			}
		}
		if ($scope.config.count == 0) {
			$scope.config.howManyDsp = $rootScope.getMsgs('max');
		} else {
			$scope.config.howManyDsp = $scope.config.count;
		}
	}
	
	$scope.bestAssets = [];
	
	$scope.openAssetList = function() {
		g('popUp-assets-list').style.display = 'block';
		if ($scope.bestAssets.length == 0) {
			var profileMethodRequestvar = jsonConcat(getUserMethodRequest(),{
				requestedUserId: $scope.config.destUserId
			});
			$http.post(settings.jsonLink + 'getCopyopProfileBestAssets', profileMethodRequestvar)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getCopyopProfileBestAssets')) {
						$scope.bestAssets = [];
						for (var i = 0; i < data.marketIds.length; i++) {
							$scope.bestAssets.push($scope.markets[data.marketIds[i]]);
						}
						$scope.filterAllAssets();
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		} else {
			$scope.filterAllAssets();
		}
	}
	
	$scope.allAssets = [];
	$scope.filterAllAssets = function() {
		$scope.allAssets = [];
		for (var i = 0; i < $scope.markets.length; i++) {
			if (!isUndefined($scope.markets[i]) && searchJsonKeyInArray($scope.bestAssets, 'id', i) == -1) {
				$scope.allAssets.push($scope.markets[i]);
			}
		}
	}
	
	$scope.selectCheckbox = function($event, id) {
		var idx = $scope.config.assets.indexOf(id);
		// is currently selected
		if (idx > -1) {
			$scope.config.assets.splice(idx, 1);
		}
		// is newly selected
		else {
			if ($scope.config.assets.length < $scope.maxAssets) {
				$scope.config.assets.push(id);
			} else {
				$event.preventDefault();
			}
		}
	}
	
	$scope.closeSelector = function(selected) {
		//selected is always true now - "cancel" button means "don't touch the selection", not "evaporate all"
		if (!selected) {
			$scope.config.assets = cloneObj($scope.config.assetsOrgn);
		} else {
			$scope.config.assetsOrgn = cloneObj($scope.config.assets);
		}
		$scope.config.assetsSelected = ($scope.config.assets.length == 0) ? 0 : 1;
		g('popUp-assets-list').style.display = "none";
	}
}]);

copyOpApp.controller('watchCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		$scope.config.command = false;
		$scope.config.watchingPushNotificationObj = [
			{
				valueBin: 1,
				value: true,
				valueDsp: $rootScope.getMsgs('yes'),
				index: 0
			},{
				valueBin: 0,
				value: false,
				valueDsp: $rootScope.getMsgs('no'),
				index: 1
			}
		];
		$scope.config.watchingPushNotification = 0;
		
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		$rootScope.popupInfo.destUserId = params.destUserId;
		
		$scope.getConfig();
	}
	
	$scope.$watch('userInfo.watching', function() {
		$scope.popUpHeader = (!$scope.userInfo.watching ? $rootScope.getMsgs('popUp-watch.header', {nickname: $scope.userInfo.name}) : $rootScope.getMsgs('settings'));
	});
	
	$scope.getConfig = function() {
		if ($scope.userInfo.watching) {
			var profileMethodRequest = jsonConcat(getUserMethodRequest(),{requestedUserId: $scope.config.destUserId})
			$http.post(settings.jsonLink + 'getCopyopWatchUserConfig', profileMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getCopyopWatchUserConfig')) {
						$scope.config.command = false;
						$scope.config.watchingPushNotification = (data.watchingPushNotification) ? 
							$scope.config.watchingPushNotificationObj[0].index : 
							$scope.config.watchingPushNotificationObj[1].index;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		}
	}
	
	$scope.action = function() {
		var watchUserConfigMethodRequest = jsonConcat(getUserMethodRequest(),{
			destUserId: $scope.config.destUserId,
			watchingPushNotification: $scope.config.watchingPushNotificationObj[$scope.config.watchingPushNotification].value,
			command: ($scope.config.command) ? 'UNWATCH' : 'WATCH'
		});
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.jsonLink + 'copyopWatchConfig', watchUserConfigMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'copyopWatchConfig')) {
						var state = !$scope.config.command;
						$rootScope.usersInfo[$scope.config.destUserId].watching = state;
						var inx = searchJsonKeyInArray($rootScope.copyopWatching, 'userId', $scope.config.destUserId);
						if (inx == -1 && state) {
							$rootScope.copyopWatching.push({userId: $scope.config.destUserId});
							$rootScope.copyopUser.copyopProfile.watching++;
							if (!isUndefined($rootScope.cUser)) {
								$rootScope.cUser.watchingCount++;
							}
						} else if (inx > -1 && !state) {
							$rootScope.copyopWatching.splice(inx, 1);
							$rootScope.copyopUser.copyopProfile.watching--;
							if (!isUndefined($rootScope.cUser)) {
								$rootScope.cUser.watchingCount--;
							}
						}
						if ($scope.config.command && !isUndefined($scope.config.obj)) {
							if (!isUndefined($scope.config.obj)) {
								$scope.config.obj.hide = true;
							}
						}
						$rootScope.closeCopyOpPopup();
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		}
	}
}]);

copyOpApp.controller('fullyCopiedCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = params.type;
		if (isUndefined($rootScope.popupInfo.destUserId)) {
			$rootScope.popupInfo.destUserId = params.obj.userId;
		}
		$scope.userInfo = $rootScope.usersInfo[$rootScope.popupInfo.destUserId];
		
		$scope.setFocus('submitBtn');
	}
}]);

copyOpApp.controller('infoPopupCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = 'info';
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		init();
		$scope.setFocus('submitBtn');
	}
	
	function init() {
		switch($scope.config.type) {
			case 'copy': 
				$scope.body = $rootScope.getMsgs('frozen.body');
				$scope.header = $rootScope.getMsgs('frozen.header');
				break;
			case 'watch': 
				$scope.body = $rootScope.getMsgs('popUp-watching.body');
				$scope.header = $rootScope.getMsgs('popUp-watching.header', {'nickname': $scope.userInfo.name});
				break;
		}
	}
	$scope.closePopUp = $scope.action = function() {
		$rootScope.closeCopyOpPopup();
	}
}]);

copyOpApp.controller('copyopWatching', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = params.type;
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		init();
		$scope.setFocus('submitBtn');
	}
	
	function init() {
		var watchListMethodRequest = jsonConcat(getUserMethodRequest(),{
			autoWatchRequired: true,
			requestedUserId: $scope.config.destUserId
		});
		$http.post(settings.jsonLink + 'getCopyopWatching', watchListMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getCopyopWatching')) {
					$scope.users = data.watchList;
					$scope.setWatchList(data);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
}]);

copyOpApp.controller('inviteFriends', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = params.type;
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		init();
	}
	
	function init() {
		$scope.users = $scope.myFacebookFriends;
	}
	
	$scope.inviteFriends = function() {
		FB.ui({
			method: 'apprequests',
			message: 'Invite Friends to copyOp'
		}, function(response){
			$rootScope.closeCopyOpPopup();
		});
	}
}]);

copyOpApp.controller('facebookConnect', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = params.type;
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
	}
}]);

// copyOpApp.controller('cashCopyOpCoins', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
function cashCopyOpCoins($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.passParams = function(params){
		$scope.config = params;
		$scope.popUpType = params.type;
		$scope.userInfo = $rootScope.usersInfo[params.destUserId];
		
		$scope.convertCoins = $scope.coins.balance;
		$scope.convertAmount = $scope.coins.convertAmount;
	}
	
	$scope.depositCopyopCoins = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.jsonLink + 'depositCopyopCoins', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'depositCopyopCoins')) {
						$scope.setCoins(data);
						$rootScope.closeCopyOpPopup();
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
	$scope.depositCopyopCoins();
};
// }]);

copyOpApp.controller('followNow', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.errorMsg = false;
	$rootScope.followData = {};
	$rootScope.defaultAmountValueTemp = $rootScope.copyopUser.user.defaultAmountValue;
	$scope.passParams = function(params){
		if (adjustFromUTCToLocal(new Date(params.data.timeEstClosing)).getTime() <= new Date().getTime()) {
			params.data.followBtnDisabled = true;
			setTimeout(function() {
				$rootScope.closeCopyOpPopup()
			}, 500);
			return;
		} else {
			$rootScope.followData.data = params.data;
			
			//$rootScope.followData.data.est_close_formated = $scope.formatDateNow(new Date(params.data.timeEstClosing));
			
			var schedule = (!isUndefined(params.data.scheduledId)) ? params.data.scheduledId : params.data.data.properties.cpop_scheduled;
			
			$rootScope.followData.marketId = params.data.data.properties.cpop_marketId;
			connectToLS_global({'group_follow':['aotps_'+schedule+'_' + params.data.data.properties[FMT.PROPERTY_KEY_MARKET_ID]]});
			startTimer();
		}
		
		$scope.popUpType = params.type;
		
		$scope.config = params;
		$scope.config.dir = params.data.data.properties[FMT.PROPERTY_KEY_DIRECTION];
		$scope.config.invId = params.data.data.properties[FMT.PROPERTY_KEY_INV_ID];
		$scope.config.asset = $scope.markets[params.data.data.properties[FMT.PROPERTY_KEY_MARKET_ID]].displayName;
		
		$rootScope.defaultAmountValueTemp = amountToFloat($rootScope.copyopUser.user.defaultAmountValue);
		$scope.config.amountDsp = formatAmount($rootScope.defaultAmountValueTemp, 0);
	}
	
	function startTimer() {
		var t = g('followTimer_1');
		if (t != null){
			try{
				var t2 = g('followTimer_2');
				var bgr = g('followTimer_bgr');
				var date_end = new Date().getTime() + settings.followTimeOut;
				var time_left = settings.followTimeOut;
				function doIt(){
					var min = Math.floor(time_left/1000);
					if(min < 10){min = '0'+min;}
					var sec = Math.floor((time_left-(min*1000))/10);
					if(sec < 10){sec = '0'+sec;}
					t.innerHTML = t2.innerHTML = min+":"+sec;
					bgr.style.width = 100-(time_left/settings.followTimeOut)*100+'%';
					
					if(time_left > 0){
						var interval = 20;
						time_left = date_end - new Date().getTime();
						if (time_left < 0) {
							time_left = 0;
						}
						$scope.timer = setTimeout(doIt,interval);
					}
					else{
						$scope.closePopup(true);
					}
				}
				doIt();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		} else {
			setTimeout(function(){startTimer()},50);
		}
	}
	
	$scope.closePopup = function(auto, params) {
		if ($rootScope.closeCopyOpPopup(params)) {
			clearTimeout($scope.timer);
			if (followls.isSubscribed()) {
				lsClient.unsubscribe(followls);
			}
			if (!isUndefined(auto) && auto) {
				$scope.$apply();
			}
		}
	}
	
	$scope.refreshPopUp = function(auto, params) {
		if ($rootScope.closeCopyOpPopup(params)) {
			clearTimeout($scope.timer);
			if (followls.isSubscribed()) {
				lsClient.unsubscribe(followls);
			}
			if (!isUndefined(auto) && auto) {
				$scope.$apply();
			}
			$rootScope.openCopyOpPopup({data: $scope.config.data, type: 'follow'});
		}
	}
	
	$scope.submited = false;
	$scope.error = false;
	$scope.copyopFollow = function(dev2) {
		if (isUndefined(dev2)) {dev2 = false;}
		if ($rootScope.copyopUser.user.balance <= $rootScope.defaultAmountValueTemp * 100) {
			clearTimeout($scope.timer);
			$rootScope.closeCopyOpPopup();
			$rootScope.openCopyOpPopup({type: 'noMoney'});
			return;
		}
		if (!isUndefined($rootScope.followData.ao_level_float)) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				clearTimeout($scope.timer);
				if (followls.isSubscribed()) {
					lsClient.unsubscribe(followls);
				}
				$scope.submited = true;
				var insertInvestmentMethodRequest = jsonConcat(getUserMethodRequest(), {
					copyopTypeId: 'FOLLOW',
					opportunityId: $rootScope.followData.ET_OPP_ID,
					requestAmount: $rootScope.defaultAmountValueTemp,
					pageLevel: $rootScope.followData.ao_level_float,
					pageOddsWin: $rootScope.followData.ET_ODDS_WIN,
					pageOddsLose: $rootScope.followData.ET_ODDS_LOSE,
					choice: $scope.config.dir,
					isFromGraph: false,
					ipAddress: '',
					apiExternalUserReference: '',
					defaultAmountValue: $rootScope.copyopUser.user.defaultAmountValue,
					copyopInvId: $scope.config.invId,
					dev2Second: dev2
				});
			
				$http.post(settings.jsonLink + 'copyopFollow', insertInvestmentMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						$scope.dev2 = false;
						if (data.errorCode == 209) {
							$rootScope.closeCopyOpPopup();
							$rootScope.openCopyOpPopup({type: 'noMoney'});
							clearTimeout($scope.timer);
						} else if (data.userMessages != null) {
							$rootScope.copyopUser.user.defaultAmountValue = $rootScope.defaultAmountValueTemp * Math.pow(10,settings.decimalPointDigits);
							data.stopClear = true;
							$scope.error = true;
							$scope.errorMsg = true;
							for (var i = 0; i < data.userMessages.length; i++) {
								if (data.userMessages[i].field == '') {
									data.userMessages[i].field = 'follow';
								}
							}
							if (data.errorCode == 213) {
								data.userMessages[0].message = $rootScope.getMsgs('CMS.tradeBox.text.cashDeposit',{cash_balance_parameter: formatAmount(data.userCashBalance, 0, true)});
							}
						}
						if (!handleErrors(data, 'copyopFollow')) {
							if (data.dev2Seconds > 0) {
								$scope.dev2 = true;
								setTimeout(function() {
									$scope.copyopFollow(true);
								}, data.dev2Seconds * 1000);
							} else {
								data.updateCUuser = false;
								$scope.setUser(data);
								$scope.success = true;
								data.investment.timeEstClosingDsp = formatDate(new Date(data.investment.timeEstClosing), false);
								var amount = amountToFloat(data.investment.amount);
								data.investment.if_correct = formatAmount(amount * data.investment.oddsWin, 0);
								data.investment.if_not_currect = formatAmount(amount * data.investment.oddsLose, 0);
								$scope.investRespons = data.investment;
							}
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		}
	}
	
	$scope.changeCount = function(minus) {
		if (minus) {
			$rootScope.defaultAmountValueTemp -= Math.ceil(amountToFloat($rootScope.copyopUser.user.defaultAmountValue) * settings.followAmoutIncProc / 100);
			if ($rootScope.defaultAmountValueTemp < 1) {
				$rootScope.defaultAmountValueTemp = 1
			}
		} else {
			$rootScope.defaultAmountValueTemp += Math.ceil(amountToFloat($rootScope.copyopUser.user.defaultAmountValue) * settings.followAmoutIncProc / 100);
		}
		$scope.config.amountDsp = formatAmount($rootScope.defaultAmountValueTemp, 0);
		$rootScope.followData.odds_win = formatAmount($rootScope.defaultAmountValueTemp * parseFloat($rootScope.followData.ET_ODDS_WIN), 0);
		$rootScope.followData.odds_lose = formatAmount($rootScope.defaultAmountValueTemp * parseFloat($rootScope.followData.ET_ODDS_LOSE), 0);
	}
}]);

copyOpApp.controller('infoPopup', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.ready = true;
	
	$scope.closePopUpDefaul = $scope.action = function() {
		$rootScope.closeCopyOpPopup();
	}

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		$scope.config.header = (!isUndefined($scope.config.header)) ? $scope.config.header : 'default-popup-header';
		$scope.header = $rootScope.getMsgs($scope.config.header);
		if (!isUndefined($scope.config.bodyTxt)) {
			$scope.body = $scope.config.bodyTxt;
		} else {
			$scope.body = $rootScope.getMsgs($scope.config.body, $scope.config.bodyParams);
		}
		
		if (!isUndefined($scope.config.redirect)) {
			$rootScope.$state.go($scope.config.redirect, {ln: $scope.skinLanguage});
		}
		
		if (!isUndefined($scope.config.onclose)) {
			$scope.closePopUpDefaul = function() {
				$rootScope.closeCopyOpPopup();
				$scope.config.onclose();
			}
		}
		
		if (!isUndefined($scope.config.onOk)) {
			$scope.action = function() {
				$rootScope.closeCopyOpPopup();
				$scope.config.onOk();
			}
		}
		
		if (isUndefined($scope.config.submitBtnTxt)) {
			$scope.submitBtnTxt = $rootScope.getMsgs('ok');
		} else {
			$scope.submitBtnTxt = $rootScope.getMsgs($scope.config.submitBtnTxt);
		}
		
		if (!isUndefined($scope.config.animate) && !isUndefined($scope.config.animateFuncton)) {
			$scope.closePopUp = function() {
				$scope.config.animateFuncton();
				
				$timeout(function() {
					$scope.closePopUpDefaul();
				}, $scope.config.animate * 1000);
			}
		} else {
			$scope.closePopUp = function() {
				$scope.closePopUpDefaul();
			}
		}
	}
}]);

copyOpApp.controller('infoImgPopup', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.ready = true;
	
	$scope.closePopUpDefaul = $scope.action = function(params) {
		$rootScope.closeCopyOpPopup(params);
	}

	$scope.passParams = function(params){
		$scope.config = params;
		
		$scope.imgClass = params.imgClass;
		
		if (!isUndefined($scope.config.buttonTxt)) {
			$scope.buttonTxt = $scope.config.buttonTxt;
		} else {
			$scope.buttonTxt = $rootScope.getMsgs('ok');
		}
		if (!isUndefined($scope.config.bodyTxt)) {
			$scope.body = $scope.config.bodyTxt;
		} else {
			$scope.body = $rootScope.getMsgs($scope.config.text, $scope.config.bodyParams);
		}
		
		if (!isUndefined($scope.config.onclose)) {
			$scope.closePopUpDefaul = function() {
				$rootScope.closeCopyOpPopup();
				$scope.config.onclose();
			}
		}
		
		if (!isUndefined($scope.config.onOk)) {
			$scope.action = function() {
				$rootScope.closeCopyOpPopup();
				$scope.config.onOk();
			}
		}
		
		if (!isUndefined($scope.config.animate) && !isUndefined($scope.config.animateFuncton)) {
			$scope.closePopUp = function() {
				$scope.config.animateFuncton();
				
				$timeout(function() {
					var index = searchJsonKeyInArray($scope.popUps, 'type', 'info-img');
					if (index > -1) {
						$scope.closePopUpDefaul({popupId: index});
					}
				}, $scope.config.animate * 1000);
			}
		} else {
			$scope.closePopUp = function() {
				$scope.closePopUpDefaul();
			}
		}
	}
}]);

function ls_updateFollow(updateInfo) {
	angular.element('html').scope().ls_updateFollowNg(updateInfo);
}

copyOpApp.controller('termsBonus', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.gUrl = '';
	var data = {};
	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		data = params.data;
		turnoverFactor = params.turnoverFactor;
		$scope.getCashBalance();
	}

	$scope.getCashBalance = function() {
		var DepositBonusBalanceMethodRequest = jsonClone(getUserMethodRequest(), {
			userId: $scope.copyopUser.userId,
			investmentId: 0,
			balanceCallType: balanceCallTypes.total
		});
		
		$http.post(settings.jsonLink + 'getUserDepositBonusBalance', DepositBonusBalanceMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserDepositBonusBalance')) {
					$scope.cashBalance = data.depositBonusBalanceBase.depositCashBalance;
					init();
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.texts = [];
	function init() {
		$scope.gUrl = "html/minisite/popUpBonusTermsIframe_" + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ".html";
		$scope.$apply();
		
		var mainKey = 'terms-bonus-' + data.typeId;
		var msgs = $scope.msgs[mainKey];
		for (var key in msgs) {
			var addKey = true;
			var addKeySpecial = false;
			if (data.typeId == 3) {
				addKey = false;
				if (data.numOfActions == 1) {
					var bonusStateIdArr = [1,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p1','p2','p3','p4'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
					bonusStateIdArr = [2,3,4];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p5','p6','p7','p8'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				} else {
					var bonusStateIdArr = [1,3,4,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p9','p10','p11','p12'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
					bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p13','p14','p15','p16'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				if (settings.skinId == 9) {
					var bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p22','p23','p24','p25','p26','p27','p28','p29'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p17','p18','p19','p20','p21'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			} else if (data.typeId == 4) {
				addKey = false;
				if (!data.isHasSteps) {
					if (data.numOfActions == 1) {
						var bonusStateIdArr = [1,3,4,5,6,7,10];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p1','p2','p4','p5'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
							if (data.bonusAmount != 0) {
								var keyArr = ['p3'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
						var bonusStateIdArr = [2];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p6','p7','p8','p9'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
						if (settings.skinId == 9) {
							var bonusStateIdArr = [2];
							if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
								var keyArr = ['p33','p34','p35','p36'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
					} else if (data.numOfActions > 1) {
						var bonusStateIdArr = [1,3,4,5,6,7,10];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							if (settings.skinId != 9) {
								var keyArr = ['p10','p11','p12','p13'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							} else {
								var keyArr = ['p37','p38','p39','p41'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
								if (data.bonusAmount != 0) {
									var keyArr = ['p40'];
									if (keyArr.indexOf(key) > -1) {
										addKey = true;
									}
								}
							}
						}
						var bonusStateIdArr = [2];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p6','p7','p8','p9'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
						if (settings.skinId != 9) {
							var bonusStateIdArr = [2];
							if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
								var keyArr = ['p42','p43','p44','p45','p46','p47','p48','p49'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
					}
				} else {
					var bonusStateIdArr = [1,3,4,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p18','p19','p21','p22'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
						var keyArr = ['p18_1'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
							addKeySpecial = true;
						}
						if (data.bonusAmount != 0) {
							var keyArr = ['p20'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
					}
					var bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p23','p24','p25','p26'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p27'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
				if (!data.isHasSteps || settings.skinId == 9) {
					var keyArr = ['p28'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				if (settings.skinId != 9 && settings.skinId != 10) {
					if (data.isHasSteps) {
						var keyArr = ['p29'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
						var keyArr = ['p29_1'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
							addKeySpecial = true;
						}
					}
					var keyArr = ['p30','p31','p32'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var keyArr = ['p50','p51'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			} else if (data.typeId == 5) {
				var bonusStateIdArr = [1,5,6,7,10];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p1','p2','p3'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
					if (data.bonusId != 320) {
						var keyArr = ['p4'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var bonusStateIdArr = [2,3,4];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p5','p6','p7'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
					if (data.bonusId != 320) {
						var keyArr = ['p8'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p9','p11','p14','p15'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
				if (data.bonusId != 320) {
					var keyArr = ['p10'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
			} else if (data.typeId == 6 || data.typeId == 7) {
				var bonusStateIdArr = [1,3,4,5,6,7,10];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p1','p2','p3','p4'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var bonusStateIdArr = [2];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p5','p6','p7','p8'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var keyArr = ['p9','p10','p11','p12','p13','p17','p18'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			}

			if (addKey) {
				if (!addKeySpecial) {
					$scope.texts.push($scope.msgsParam(msgs[key], {
						bonusAmount: formatAmount(data.bonusAmount, 0, true),
						sumInvQualify: formatAmount(data.sumInvQualify, 0, true),
						endDate: data.endDateTxt,
						endDates: data.endDateTxt,
						wageringParam: data.wageringParam,
						minDeposit: formatAmount(data.minDepositAmount, 0, true),
						numOfActions: data.numOfActions,
						bonusPercent: (data.bonusPercent * 100),
						formula: (data.wageringParam *  data.bonusPercent / 100),
						'TurnOverFactorPercent-3': turnoverFactor[2],
						'TurnOverFactorPercent-4': turnoverFactor[3],
						cash_balance_parameter: formatAmount($scope.cashBalance, 0, true)
					}));
				} else {
					for (var j = 0; j < data.bonusSteps.length; j++) {
						$scope.texts.push($scope.msgsParam(msgs[key], {
							minDepositAmount: formatAmount(data.bonusSteps[j].minDepositAmount, 0, true),
							maxDepositAmount: formatAmount(data.bonusSteps[j].maxDepositAmount, 0, true),
							num: j+1,
							bonusPercent: (data.bonusSteps[j].bonusPercent * 100),
							wageringParam_bonusPercent: data.bonusSteps[j].wageringParam * data.bonusSteps[j].bonusPercent
						}));
					}
				}
			}
		}
	}
	
	$scope.getParams = function() {
		var insertInvestmentMethodRequest = jsonConcat(getUserMethodRequest(), {});
		$http.post(settings.jsonLink + 'getParams', insertInvestmentMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getParams')) {
					$scope.setUser(data);
					$scope.closePopup(true);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.claimBonus = function(entry) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var bonusMethodRequest = jsonClone(getUserMethodRequest() ,{
				bonusId: entry.id,
				bonusStateId: 10
			});
			
			$http.post(settings.jsonLink + 'updateBonusStatus', bonusMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'updateBonusStatus')) {
						entry.bonusStateId = 1;
						$scope.getCopyopUser({onStateChange: true});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
}]);

copyOpApp.controller('noMoneyPopup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
	}
}]);

copyOpApp.controller('depositPopup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;

	$scope.depositTab = 1;
	$scope.changeTab = function(tab) {
		$scope.depositTab = tab;
	}

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
	}
	
	$scope.closePopup = function(params) {
		if ($rootScope.closeCopyOpPopup(params)) {
			$rootScope.checkForWoalktrough();
		}
	}
}]);

copyOpApp.controller('changeNickname', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
	}
}]);

copyOpApp.controller('openWalkthrough', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.stage = -1;
	
	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
	
		if ($scope.config.caller == 'header') {
			$scope.autoScroll = true;
			if ($rootScope.$state.is('ln.home.a')) {//home
				$scope.popupSet = 0;
				$scope.additionalObj_1 = [$rootScope.feedSwitch({
					"userId":130782,
					"queueType":1,
					"timeCreated":"Feb 12, 2015 9:27:38 AM",
					"timeCreatedUUID":"62c14051-b299-11e4-93cc-2f9a62d53bc4",
					"msgType":10,
					"properties":{
						"cpop_avatar":"https://www.copyop.com/avatar/m36.png",
						"cpop_watchers":"4",
						"cpop_copying":"3",
						"cpop_copiers":"2",
						"cpop_user_frozen":"false",
						"cpop_marketId":"601",
						"cpop_inherited_uuid":"629f1250-b299-11e4-93cc-2f9a62d53bc4",
						"cpop_watching":"8",
						"cpop_userId":"0",
						"cpop_amount_in_curr_2":"116.4379999999999992787991232034983113408088684082031250",
						"cpop_nickname":"Luigi Barzini",
						"cpop_amount_in_curr_1":"404.77",
						"cpop_amount_in_curr_4":"170.00",
						"cpop_amount_in_curr_3":"84.42",
						"cpop_amount_in_curr_6":"4084.36",
						"cpop_time_settled":"1423733427491",
						"cpop_amount_in_curr_5":"249.45",
						"cpop_amount_in_curr_8":"123383.53",
						"cpop_amount_in_curr_7":"722.72"
					}
				})];
				
				$scope.additionalObj_2 = [$rootScope.feedSwitch({
					"userId":130782,
					"queueType":1,
					"timeCreated":"Feb 12, 2015 8:59:28 AM",
					"timeCreatedUUID":"73b328f0-b295-11e4-93cc-2f9a62d53bc4",
					"msgType":7,
					"properties":{
						"cpop_avatar":"https://www.copyop.com/avatar/m36.png",
						"cpop_watchers":"4",
						"cpop_copying":"3",
						"cpop_copiers":"2",
						"cpop_user_frozen":"false",
						"cpop_marketId":"207",
						"cpop_inherited_uuid":"737dc110-b295-11e4-93cc-2f9a62d53bc4",
						"cpop_time_last_invest":"14237334000000",
						"cpop_watching":"8",
						"cpop_dir":"1",
						"cpop_push_currency_id":"2",
						"cpop_invId":"29069532",
						"cpop_userId":"132359",
						"cpop_amount_in_curr_2":"166.34",
						"cpop_nickname":"Luigi Barzini",
						"cpop_amount_in_curr_1":"578.24",
						"cpop_amount_in_curr_4":"100.00",
						"cpop_amount_in_curr_3":"120.59",
						"cpop_amount_in_curr_6":"5834.8",
						"cpop_amount_in_curr_5":"356.36",
						"cpop_level":"22378.062",
						"cpop_amount_in_curr_8":"176262.19",
						"cpop_push_skin_id":"2",
						"cpop_amount_in_curr_7":"1032.46"
					}
				})];
				
				$scope.popUpsMap = [
					{
						htmlId: 'feed-news',
						header: true,
						headerText: $rootScope.getMsgs('news'),
						body: $rootScope.getMsgs('walkthrough.txt1', {'different-color': 'red-color','text' : 'walkthrough-info'}),
						mainClass: '',
						additional: false,
						orientation: 'bc'//bottom center
					},{
						htmlId: 'feed-news',
						header: true,
						headerText: $rootScope.getMsgs('walkthrough.heading2'),
						body: $rootScope.getMsgs('walkthrough.txt2', {'different-color': 'red-color','text' : 'walkthrough-info'}),
						mainClass: '',
						additional: true,
						additionalObj: 1,
						orientation: 'bc'
					},{
						htmlId: 'feed-news',
						header: true,
						headerText: $rootScope.getMsgs('buy'),
						body: $rootScope.getMsgs('walkthrough.txt3'),
						mainClass: '',
						additional: true,
						additionalObj: 2,
						orientation: 'bc'
					},{
						htmlId: 'what-is-hot-header',
						header: true,
						headerText: $rootScope.getMsgs('hot'),
						body: $rootScope.getMsgs('walkthrough.txt4'),
						mainClass: 'small-box',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'what-is-hot-best-traders',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt5'),
						mainClass: 'smaller-box',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'asset-specialists',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt16'),
						mainClass: 'smaller-box Small',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'what-is-hot-best-trades',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt6'),
						mainClass: 'smaller-box',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'what-is-hot-best-copiers',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt7'),
						mainClass: 'smaller-box',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'header-button-trade-holder',
						header: true,
						headerText: $rootScope.getMsgs('walkthrough.heading9'),
						body: $rootScope.getMsgs('walkthrough.txt17'),
						mainClass: 'small-box long-header',
						additional: false,
						orientation: 'bc',
						topBar: '',
						padding: {
							top: '5',
							bottom: '5',
							left: '7',
							right: '7',
						}
					}
				];
			} else if ($rootScope.$state.includes('ln.in.a.user')) {//user
				$scope.popupSet = 1;
				$scope.popUpsMap = [
					{
						htmlId: 'side_user-stat-hit',
						header: true,
						headerText: $rootScope.getMsgs('hit-rate'),
						body: $rootScope.getMsgs('walkthrough.txt8'),
						mainClass: 'small-box',
						additional: false,
						orientation: 'bc'
					},{
						htmlId: 'side_user-stat-copy-watch',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt9'),
						mainClass: 'smaller-box',
						additional: false,
						orientation: 'bc'
					}
				];
			} else if ($rootScope.$state.includes('ln.in.a.asset')) {//trading area
				$scope.popupSet = 2;
				$scope.popUpsMap = [
					{
						htmlId: 'leftColumHeight',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt10', {'different-color': 'red-color','text' : 'walkthrough-info'}),
						mainClass: 'tr-page-small',
						additional: false,
						orientation: 'roc'//right offset center
					},{
						htmlId: 'tradeBox_left_0',
						header: false,
						body: $rootScope.getMsgs('walkthrough.txt11', {'different-color': 'red-color','text' : 'walkthrough-info'}),
						mainClass: 'tr-page',
						additional: false,
						orientation: 'cr'//right center
					}
				];
			}
		} else {
			$scope.popupSet = 3;//popup copy
			$scope.autoScroll = false;
			$scope.popUpsMap = [
				{
					htmlId: 'popup-copy-ul',
					header: false,
					body: $rootScope.getMsgs('walkthrough.txt15'),
					mainClass: 'copy-popup',
					additional: false,
					orientation: 'bc'//bottom center
				},{
					htmlId: 'popup-copy-how-many',
					header: false,
					body: $rootScope.getMsgs('walkthrough.txt12'),
					mainClass: 'copy-popup',
					additional: false,
					orientation: 'bc'//bottom center
				},{
					htmlId: 'popup-copy-amount',
					header: false,
					body: $rootScope.getMsgs('walkthrough.txt13'),
					mainClass: 'copy-popup',
					additional: false,
					orientation: 'bc'
				},{
					htmlId: 'popup-copy-assets',
					header: false,
					body: $rootScope.getMsgs('walkthrough.txt14'),
					mainClass: 'copy-popup',
					additional: false,
					orientation: 'bc'
				}
			];
		}
		
		$scope.changeSlide();
	}
	
	$scope.changeSlide = function(prev) {
		if((!prev && $scope.stage < $scope.popUpsMap.length - 1) || (prev && $scope.stage > 0)){
			if(!prev){
				$scope.stage++;
			}else{
				$scope.stage--;
			}
			$scope.info = $scope.popUpsMap[$scope.stage];
			var topBar = "header";
			if(!isUndefined($scope.info.topBar)){
				topBar = $scope.info.topBar;
			}
			var padding = {};
			if(!isUndefined($scope.info.padding)){
				padding = $scope.info.padding;
			}
			var elementPosition = addOutline($scope.info.htmlId, $scope.autoScroll, function(metrics){var res = $scope.positionPopup(metrics, $scope.info);return res;}, topBar, padding);
			setTimeout(function() {
				$scope.positionPopup(elementPosition, $scope.info);
			}, 50);
		}
	}
	
	$scope.positionPopup = function(position, slideInfo) {
		var walkthroughExists = false;
		for(var i = 0; i < $scope.popUps.length; i++){
			if($scope.popUps[i].type == 'walkthrough'){
				walkthroughExists = true;
			}
		}
		if (!walkthroughExists) {return false;}
		
		var popup = g('walkthrough');
		if (isUndefined(popup)) {
			setTimeout(function() {
				$scope.positionPopup(position, slideInfo);
			}, 50);
		} else {
			var parent = popup.parentNode;
			parent.style.height = $("body").height() + "px";
			parent.focus();
			if (slideInfo.orientation == 'bc') {
				if (slideInfo.additional) {
					newsFeedHome = $('.main-message')[0];
					var walkthroughHolder = g('walkthrough-feed-holder');
					walkthroughHolder.style.width = newsFeedHome.offsetWidth + 'px';
					
					var popupWidth = popup.offsetWidth;
					popup.style.top = position.bottom + 10 + 'px';
					popup.style.left = position.left + 10 + 'px';
				} else {
					var popupWidth = popup.offsetWidth;
					popup.style.top = position.bottom + 6 + 'px';
					popup.style.left = (position.left + (position.width / 2)) - (popupWidth / 2) + 'px';
				}
			} else if (slideInfo.orientation == 'cr') {
				var popupHeight = popup.offsetHeight;
				popup.style.top = (position.top + (position.height / 2)) - (popupHeight / 2) + 'px';
				popup.style.left = position.right + 6 + 'px';
			} else if (slideInfo.orientation == 'roc') {
				var popupHeight = popup.offsetHeight;
				popup.style.top = position.top + 40 + 'px';
				popup.style.left = position.right + 6 + 'px';
			}
		}
		return true;
	}
	
	$scope.handleKeydown = function(params) {
		if(params.event.type == 'keydown'){
			if(params.event.keyCode == 39){ //Right arrow key pressed
				$scope.changeSlide();
			}else if(params.event.keyCode == 37){ //Left arrow key pressed
				$scope.changeSlide(true);
			}else{
				$scope.closeSlide(params);
			}
		}
	}
	
	$scope.closeSlide = function(params) {
		if ($rootScope.closeCopyOpPopup(params)) {
			removeOutline();
		}
	}
}]);

copyOpApp.controller('affiliatesPopupCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		$scope.config = params;
		
		$scope.init();
	}
	
	$scope.init = function(){
		$scope.getCountryId();
	}
	
	$scope.$watch('copyopUser.user.country', function(){
		$scope.config.countryId = $rootScope.copyopUser.user.country;
	}, true);
	
	$scope.sendAffiliatesData = function(_form){
		if(_form.$valid){
			var messageBody = '';
			if(!isUndefined($scope.config.firstName) && ($scope.config.firstName != '')){messageBody += 'First name: ' + $scope.config.firstName + '\n';}
			if(!isUndefined($scope.config.lastName) && ($scope.config.lastName != '')){messageBody += 'Last name: ' + $scope.config.lastName + '\n';}
			messageBody += 'Email: ' + $scope.config.email + '\n';
			if(!isUndefined($scope.config.countryId) && !isUndefined($scope.config.countryId.phoneCode) && ($scope.config.countryId.phoneCode != '') && !isUndefined($scope.config.mobilePhone) && ($scope.config.mobilePhone != '')){messageBody += 'Phone: +' + $scope.config.countryId.phoneCode + ' ' + $scope.config.mobilePhone + '\n';}
			if(!isUndefined($scope.config.skype) && ($scope.config.skype != '')){messageBody += 'Skype: ' + $scope.config.skype + '\n';}
			
			var EmailMethodRequest = jsonClone(getUserMethodRequest(), {
				mailBody: messageBody,
				from: $scope.config.email
			});
			$http.post(settings.jsonLink + 'sendAffiliateEmail', EmailMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'sendAffiliateEmail')) {
						$rootScope.closeCopyOpPopup();
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}else{
			logIt({'type':3,'msg':_form.$error});
		}
	}
}]);

copyOpApp.controller('userSingleQuestionnaireCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.activeScreen = 0;
	$scope.screensComplete = [];
	$scope.hasError = true;

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		$scope.config = params;
		
		function waitForIt() {
			if (!$rootScope.ready) {
				setTimeout(function(){waitForIt()},50);
			} else {
				$scope.init();
			}
		}
		waitForIt();
	}
	
	$scope.regulationQuestNext = function() {
		$scope.checkStatus(true);
		if ($scope.screensComplete[$scope.activeScreen]) {
			$scope.checkStatus(false);
			$scope.activeScreen++
		}
	}
	
	$scope.isRegulationQuestionaireCalled = false;
	$scope.init = function() {
		if($scope.isRegulationQuestionaireCalled){
			return;
		}
		$scope.isRegulationQuestionaireCalled = true;
		$http.post(settings.jsonLink + 'getUserSingleQuestionnaireDynamic', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserSingleQuestionnaireDynamic')) {
					if (data.errorCode == errorCodeMap.regulation_suspended) {
						$scope.errorMsg = data.userMessages[0].message;
					} else {
						$scope.hasError = false;
						
						$scope.screens = [];
						$scope.userAnswers = data.userAnswers;
						
						for (var i = 0; i < data.questions.length; i++) {
							var screen = data.questions[i].screen - 1;
							if (isUndefined($scope.screens[screen])) {
								$scope.screens[screen] = [];
							}
							
							$scope.screensComplete[screen] = false;
							
							if (data.questions[i].questionType == 'DROPDOWN') {
								data.questions[i].answers.unshift({
									id: null, 
									translation: $rootScope.getMsgs('select')
								})
							} else if (data.questions[i].questionType == 'CHECKBOX') {
								if (data.questions[i].answers[0].name.search('no') > -1) {//if no is the first answer
									var el = data.questions[i].answers.pop();//remove the last one (yes)
									data.questions[i].answers.unshift(el);//and add it in the begining, before no
								}
							}
							
							var index = searchJsonKeyInArray(data.userAnswers, 'questionId', data.questions[i].id);
							data.questions[i].index = i;
							
							var indexAnswer = searchJsonKeyInArray(data.questions[i].answers, 'id', data.userAnswers[index].answerId);
							
							var indexAnswerReal = 0;
							if (index > -1 && data.userAnswers[index].answerId != null && indexAnswer > -1) {
								indexAnswerReal = indexAnswer;
							} else if (data.questions[i].defaultAnswerId == 0) {
								indexAnswerReal = 0;
							} else {
								indexAnswerReal = searchJsonKeyInArray(data.questions[i].answers, 'id', data.questions[i].defaultAnswerId);
							}
							
							if (data.questions[i].questionType == 'DROPDOWN') {
								data.questions[i].userAnswer = data.questions[i].answers[indexAnswerReal];
								data.questions[i].userAnswer.textAnswer = data.userAnswers[index].textAnswer;
							} else if (data.questions[i].questionType == 'CHECKBOX') {
								data.questions[i].translatation = data.questions[i].translatation.replace('link_risk', $scope.skinLanguage + '/general-terms#risk').replace('link_agreement', $scope.skinLanguage + '/agreement').replace('link_privacy', $scope.skinLanguage + '/privacy');
								
								data.questions[i].userAnswer = {
									id: data.questions[i].answers[indexAnswerReal].id
								}
							}
							
							$scope.screens[screen].push(data.questions[i]);
						}
						
						$scope.checkStatus(false);
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
	
	$scope.checkStatus = function(showErrors) {
		for (var i = 0; i < $scope.screens.length; i++) {
			var questionsAnswered = 0;
			var questionsTotal = 0;
			for (var n = 0; n < $scope.screens[i].length; n++) {
				if ($scope.screens[i][n].mandatory) {
					questionsTotal++;
				}
				if (
					$scope.screens[i][n].mandatory && (
						($scope.screens[i][n].questionType == 'DROPDOWN' && $scope.screens[i][n].userAnswer.id != null) || 
						($scope.screens[i][n].questionType == 'CHECKBOX' && $scope.screens[i][n].userAnswer.id == $scope.screens[i][n].answers[0].id)
					)
				) {
					questionsAnswered++;
					$scope.screens[i][n].hasError = false;
				} else if (showErrors && $scope.screens[i][n].mandatory) {
					$scope.screens[i][n].hasError = true;
				} else {
					$scope.screens[i][n].hasError = false;
				}
				
				var index = searchJsonKeyInArray($scope.userAnswers, 'questionId', $scope.screens[i][n].id);
				if (index > -1) {
					$scope.userAnswers[index].answerId = $scope.screens[i][n].userAnswer.id;
					$scope.userAnswers[index].textAnswer = $scope.screens[i][n].userAnswer.textAnswer;
				}
			}
			if (questionsTotal == questionsAnswered) {
				$scope.screensComplete[i] = true;
			} else {
				$scope.screensComplete[i] = false;
			}
		}
	}
	
	$scope.submitSQuestionnaire = function(submit, _form) {
		if (submit) {
			$scope.checkStatus(true);
			$scope.loading = true;
		} else {
			$scope.checkStatus(false);
		}
		
		
		var UpdateUserQuestionnaireAnswersRequest = jsonClone(getUserMethodRequest(), {
			userAnswers: $scope.userAnswers
		});
		
		$http.post(settings.jsonLink + 'updateUserQuestionnaireAnswers', UpdateUserQuestionnaireAnswersRequest)
			.success(function(data, status, headers, config) {
				$scope.loading = false;
				if (!handleErrors(data, 'updateUserQuestionnaireAnswers')) {
					if (submit && $scope.screensComplete[$scope.screensComplete.length-1]) {
						$scope.updateSingleQuestionnaireDone(_form);
					}
					//handleSuccess(data);
				}
			})
			.error(function(data, status, headers, config) {
				$scope.loading = false;
				handleNetworkError(data);
			})
	}
	
	$scope.updateSingleQuestionnaireDone = function(_form) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.jsonLink + 'updateSingleQuestionnaireDone', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					data.globalErrorField = "globalErrorField";
					$scope.setUser(data);
					if (!handleErrors(data, 'updateSingleQuestionnaireDone')) {
						$rootScope.copyopUser.userRegulation.regulationQuestionnaireDone = true;
						$rootScope.closeCopyOpPopup();
						$rootScope.$state.go('ln.in.a.asset', {ln: $scope.skinLanguage});
						$scope.stateMachine({});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		}
	}
}]);

copyOpApp.controller('withdrawPopup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.closePopUp = $scope.action = function() {
		$rootScope.closeCopyOpPopup();
	}

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		$scope.header = $scope.getMsgs('CMS.withdraw_popUp.text.txt1', {'first-name': $rootScope.copyopUser.user.firstName});
		$scope.footer = $scope.getMsgs('CMS.withdraw_popUp.text.txt2');
		$scope.body = $scope.getMsgs($scope.config.body, $scope.config.bodyParams);
		
		$scope.closePopUp = function() {
			$rootScope.closeCopyOpPopup();
		}
		
		$scope.action = function() {
			$rootScope.closeCopyOpPopup();
			$scope.config.onOk();
		}
	}
}]);

copyOpApp.controller('regulationStatusCtr', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.ready = true;
	$scope.isSubmitting = false;
	$scope.isUploaded = false;
	$scope.cnmvDocType = 38;
	
	$scope.doc = {
		fileTypeId: $scope.cnmvDocType,
		docTitle: ''
	};
	
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;

		$scope.className = $scope.config.txtStatus;
		
		function waitForIt() {
			if (!$rootScope.ready) {
				setTimeout(function(){waitForIt()},50);
			} else {
				$scope.init();
			}
		}
		waitForIt();
	}
	
	$scope.init = function() {
		if ($scope.config.errorCode == errorCodeMap.regulation_user_is_treshold_block) {
			$scope.btnTxt = $rootScope.getMsgs('activate-account');
		} else if ($scope.config.errorCode == errorCodeMap.regulation_suspended_cnmv_docs) {
			$scope.btnTxt = $rootScope.getMsgs('popUp-regulation-status.text.suspended-cnmv.docs.start-trading');
		} else {
			$scope.btnTxt = $rootScope.getMsgs('ok');
		}
	}
	
	$scope.closePopup = function(params) {
		if ($scope.className == 'restricted') {
			if ($rootScope.closeCopyOpPopup(params)) {
				// $scope.regAction();
			}
		} else if ($scope.className == 'restricted-no-thanks') {
			return false;//don't close the popup
		} else {
			$rootScope.closeCopyOpPopup(params);
		}
	}
	$scope.regAction = function(flag) {
		if ($rootScope.copyopUser.user.countryId == countries.spain && ($scope.config.errorCode == errorCodeMap.regulation_user_restricted || $scope.config.errorCode == errorCodeMap.reg_suspended_quest_incorrect)) {
			var IsKnowledgeQuestionMethodRequest = jsonConcat(getUserMethodRequest(),{
					userId: $rootScope.copyopUser.userId,
					isKnowledge: flag
				})
			$http.post(settings.jsonLink + 'updateIsKnowledgeQuestion', IsKnowledgeQuestionMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'updateIsKnowledgeQuestion')) {
						data.updateCUuser = false;
						$scope.setUser(data);
						if (flag) {
							$rootScope.closeCopyOpPopup();
							$scope.stateMachine({});
						} else {
							$scope.className = 'restricted-no-thanks';
						}
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		} else if ($scope.config.errorCode == errorCodeMap.regulation_suspended_cnmv_docs) {
			if(!$scope.isUploaded){
				return false;
			}
			$rootScope.closeCopyOpPopup();
			$scope.getCopyopUser({onStateChange: true});
		} else if (($scope.config.errorCode == errorCodeMap.regulation_user_restricted || $scope.config.errorCode == errorCodeMap.reg_suspended_quest_incorrect) && $scope.className != 'blocked-pep') {
			var IsKnowledgeQuestionMethodRequest = jsonConcat(getUserMethodRequest(),{
					userId: $rootScope.copyopUser.userId,
					isKnowledge: flag
				})
			$http.post(settings.jsonLink + 'updateIsKnowledgeQuestion', IsKnowledgeQuestionMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'updateIsKnowledgeQuestion')) {
						data.updateCUuser = false;
						$scope.setUser(data);
						if (flag) {
							$rootScope.closeCopyOpPopup();
							$scope.stateMachine({});
						} else {
							$scope.className = 'restricted-no-thanks';
						}
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		} else if ($scope.config.errorCode == errorCodeMap.regulation_user_is_treshold_block) {
			var UnblockUserMethodRequest = jsonConcat(getUserMethodRequest(),{
				userId: $rootScope.copyopUser.userId
			})
			$http.post(settings.jsonLink + 'unblockUserForThreshold', UnblockUserMethodRequest)									
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'unblockUserForThreshold')) {
						$rootScope.closeCopyOpPopup();
						$rootScope.copyopUser.userRegulation.scoreGroup = null;
						$rootScope.copyopUser.userRegulation.thresholdBlock = null;
						$scope.stateMachine({});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				})
		} else {
			$rootScope.closeCopyOpPopup();
		}
	}
	
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
	
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, docType, doc) {
		$scope.currentDocType = docType;
		$scope.ccId = 0;
		$scope.selectedDoc = doc;
		$scope.selectedDoc.loading = true;
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000  >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.selectedDoc.loading = false;
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		// closeElement({id:'popUp-avatar'});
		$scope.selectedFiles = [];
		$scope.progress = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							// if (index == 0) {
								// $rootScope.uploadedImage = $scope.dataUrls[index];
							// }
						});
					}
				}(fileReader, i);
			}
			if ($scope.fileReaderSupported) {
				$scope.uploadedFileName = $file.name;
			}
			$scope.progress[i] = -1;
			$scope.isSubmitting = true;
			$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			 var params = '?';
			 params += 'fileType=' + $scope.currentDocType;
			 params += '&fileName=' + $scope.selectedFiles[index].name;
			 params += '&ccId=' + $scope.ccId;
			 params += '&writerId=' + settings.writerId;
			 params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: settings.jsonImagegLink + 'UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileType: $scope.currentDocType,
					fileName: $scope.selectedFiles[index].name,
					ccId: $scope.ccId,
					writerId: settings.writerId
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response.data);
					$scope.uploadComplete(response.data);
				});
			}, function(response) {
				if (response.status > 0) $scope.$parent.errorMsg = $scope.errorMsg = response.status + ': ' + response.data;
				$scope.selectedDoc.loading = false;
				$scope.isSubmitting = false;
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				// $scope.selectedDoc.loading = false;
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			alert($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		$scope.selectedDoc.loading = false;
		$scope.isSubmitting = false;
		if (!handleErrors(data, 'UploadDocumentsService')) {
			$scope.isUploaded = true;
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
}]);

copyOpApp.controller('regulationDocuments', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.ready = true;
	$scope.currentDocType = 0;
	$scope.popupState = 0;//0 default, 1 blocked
	$scope.selects = {
		creditCard: {},
		proofOfId: {}
	}

	$scope.uploadComplateMsgs = false;
	
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
		$scope.popupState = params.popupState;
		
		function waitForIt() {
			if (!$rootScope.ready) {
				setTimeout(function(){waitForIt()},50);
			} else {
				$scope.init();
			}
		}
		waitForIt();
	}
	
	$scope.proofOfIdClicked = false;
	$scope.setProofOfIdClicked = function(val){
		$scope.proofOfIdClicked = val;
	}
	$scope.checkProofOfIdClicked = function(){
		return ($scope.proofOfIdClicked && $scope.selects.proofOfId.id == '');
	}

	$scope.init = function() {
		$scope.creditCards = [];
		$scope.proofOfId = [
			{id: '', docTitle: $rootScope.getMsgs('choose-type')}
		];
		
		$scope.selects.proofOfId = $scope.proofOfId[0];
		
		$scope.restOfDocs = [];
		
		var UserDocumentsMethodRequest = jsonClone(getUserMethodRequest(), {
			userId: $rootScope.copyopUser.userId
		})
		
		$http.post(settings.commonServiceLink + '/UserDocumentsServices/getUserDocuments', UserDocumentsMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserDocuments')) {
					$scope.userDocuments = data.userDocuments;
					$scope.documentUploadResult();
				}
			})
			.error(function(data, status, headers, config) {
				$scope.loading = false;
				handleNetworkError(data);
			})
	}

	$scope.documentUploadResult = function() {
		//document statuses
		$scope.userDocuments = $scope.userDocuments.map(function(current) {
			if (!current.hideField) {
				current.docStatus = $scope.switchStatus(current);
				return current;
			}
		});
		
		var proofOfIdList = [fileType.user_id_copy, fileType.passport];
		var creditCardsList = [fileType.cc_copy_front, fileType.cc_copy_back];
		for (var i = 0; i < $scope.userDocuments.length; i++) {
			switch($scope.userDocuments[i].fileTypeId) {
				case fileType.user_id_copy: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('id2');break;
				case fileType.passport: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('passport');break;
				case fileType.utility_bill: $scope.userDocuments[i].docTitle = $rootScope.getMsgs('utility-bill');break;
			}
			if (proofOfIdList.indexOf($scope.userDocuments[i].fileTypeId) > -1) {
				$scope.proofOfId.push($scope.userDocuments[i]);
				if ($scope.userDocuments[i].statusMsgId > 1) {
					$scope.selects.proofOfId = $scope.proofOfId[$scope.proofOfId.length - 1];
				}
			} else if (creditCardsList.indexOf($scope.userDocuments[i].fileTypeId) > -1) {
				var index = searchJsonKeyInArray($scope.creditCards, 'ccId', $scope.userDocuments[i].ccId);
				if (index == -1) {
					$scope.creditCards.push({
						ccId: $scope.userDocuments[i].ccId,
						ccName: $scope.userDocuments[i].ccName,
						ccType: $scope.userDocuments[i].ccType
					});
					index = $scope.creditCards.length-1;
				}
				
				if ($scope.userDocuments[i].fileTypeId == fileType.cc_copy_front) {
					$scope.creditCards[index].front = $scope.userDocuments[i];
				} else if ($scope.userDocuments[i].fileTypeId == fileType.cc_copy_back) {
					$scope.creditCards[index].back = $scope.userDocuments[i];
				}
			} else {
				$scope.restOfDocs.push($scope.userDocuments[i]);
			}
		}

		if ($scope.creditCards.length > 0) {
			$scope.selects.creditCard = $scope.creditCards[0];
		}
	}
	
	$scope.switchStatus = function(docData) {
		var rtn = {
			fileName: (!isUndefined(docData.fileNameForClient)) ? docData.fileNameForClient : $rootScope.getMsgs('no-file'),
			isLock: docData.isLock
		}
		
		switch (docData.statusMsgId) {
			case fileStatus.not_selected: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('select-type');
				break;
			case fileStatus.requested: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('missing');
				break;
			case fileStatus.in_progress: 
				rtn.className = 'info-message';
				rtn.txt = $rootScope.getMsgs('waiting-approval');
				break;
			case fileStatus.done: 
				rtn.className = 'ok-message icon';
				rtn.txt = $rootScope.getMsgs('approved');
				break;
			case fileStatus.invalid: 
				rtn.className = 'error-message icon';
				rtn.txt = $rootScope.getMsgs('invalid');
				break;
			case fileStatus.not_needed: 
				rtn.className = '';
				rtn.txt = '';
				break;
		}
		
		return rtn;
	}
	
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.fileTooBig = false;
	
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, docType, doc) {
		$scope.currentDocType = docType;
		$scope.ccId = 0;
		$scope.selectedDoc = doc;
		$scope.selectedDoc.loading = true;
		if (docType == fileType.cc_copy_front || docType == fileType.cc_copy_back) {
			$scope.ccId = $scope.selects.creditCard.ccId;
		}
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000  >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		// closeElement({id:'popUp-avatar'});
		$scope.selectedFiles = [];
		$scope.progress = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							// if (index == 0) {
								// $rootScope.uploadedImage = $scope.dataUrls[index];
							// }
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			 var params = '?';
			 params += 'fileType=' + $scope.currentDocType;
			 params += '&fileName=' + $scope.selectedFiles[index].name;
			 params += '&ccId=' + $scope.ccId;
			 params += '&writerId=' + settings.writerId;
			 params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: settings.jsonImagegLink + 'UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileType: $scope.currentDocType,
					fileName: $scope.selectedFiles[index].name,
					ccId: $scope.ccId,
					writerId: settings.writerId
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response.data);
					$scope.uploadComplate(response.data);
				});
			}, function(response) {
				if (response.status > 0) $scope.$parent.errorMsg = $scope.errorMsg = response.status + ': ' + response.data;
				$scope.selectedDoc.loading = false;
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				// $scope.selectedDoc.loading = false;
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			alert($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplate = function(data) {
		$scope.selectedDoc.loading = false;
		if (!handleErrors(data, 'UploadDocumentsService')) {
			var filenameFormated
			if ($scope.selectedFiles[0].name.length <= 13) {
				filenameFormated = $scope.selectedFiles[0].name;
			} else {
				var ext = $scope.selectedFiles[0].name.split('.');
				ext = '...' + ext[ext.length-1];
				filenameFormated = $scope.selectedFiles[0].name.substr(0, 9) + ext;
			}
			
			$scope.selectedDoc.docStatus = $scope.switchStatus({
				fileNameForClient: filenameFormated,
				statusMsgId: fileStatus.in_progress,
				isLock: $scope.selectedDoc.isLock
			});
			
			$scope.uploadComplateMsgs = true;
			$timeout(function() {
				$scope.uploadComplateMsgs = false;
			},3000);
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
}]);

copyOpApp.controller('bonusBalancePopupCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.inputs = {isKnowledge:false}
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
		
		$scope.depositCashBalance = formatAmount(params.data.depositBonusBalanceBase.depositCashBalance, 0, true);
		$scope.bonusBalance = formatAmount(params.data.depositBonusBalanceBase.bonusBalance, 0, true);
		$scope.bonusWinnings = formatAmount(params.data.depositBonusBalanceBase.bonusWinnings, 0, true);
		$scope.totalBalance = formatAmount(params.data.depositBonusBalanceBase.totalBalance, 0, true);
	}
	setTimeout(function(){
		//Position the tooltip
		var target = $scope.config.event.target;
		var topOffset = Math.sqrt(30*30*2)/2;
		$(".popUp-container-bonus-balance").css("left", ($(target).offset().left + $(target).outerWidth()/2 - $(".popUp-container-bonus-balance").outerWidth()/2) + "px");
		$(".popUp-container-bonus-balance").css("top", ($(target).offset().top + $(target).outerHeight() + topOffset) + "px");
		$(".popUp-container-bonus-balance").css("display", "block");
		
		//Set close events
		$(target).mouseleave(function() {
			setTimeout(function(){
				if($(".popUp-container-bonus-balance").attr('data-hovering') != 1){
					$rootScope.closeCopyOpPopup();
				}else{
					$(".popUp-container-bonus-balance").mouseleave(function(event){
						$(this).attr('data-hovering', '0');
						$rootScope.closeCopyOpPopup();
					});
				}
			}, 50);
		});
		$(".popUp-container-bonus-balance").mouseenter(function(event){
			$(this).attr('data-hovering', '1');
		}).mouseleave(function(event){
			$(this).attr('data-hovering', '0');
			$rootScope.closeCopyOpPopup();
		});
		
		//Show/hide relevant parts of the tooltip
		var $totalTxtHolder = $('#totalTxtHolder');
		$totalTxtHolder.removeClass('total amount return');
		if ($scope.config.data.balanceCallType == balanceCallTypes.total) {
			$totalTxtHolder.addClass('total');
		}else{
			if ($scope.config.data.balanceCallType == balanceCallTypes.open_amount || $scope.config.data.balanceCallType == balanceCallTypes.settled_amount) {
				$totalTxtHolder.addClass('amount');
			} else {
				$totalTxtHolder.addClass('return');
			}
		}
		$('#bonus-balance-info-more').on('click', function(event){
			$(this).css('display', 'none');
			$('#bonus-balance-info-less').css('display', 'block');
		});
		$('#bonus-balance-info-less').on('click', function(event){
			$(this).css('display', 'none');
			$('#bonus-balance-info-more').css('display', 'block');
		});
		$('#bonus-balance-info-less-close').on('click', function(event){
			$('#bonus-balance-info-more').css('display', 'block');
		});
		
		totalTxtHolder = null;
		target = null;
	}, 0);
	
}]);

copyOpApp.controller('lowBalancePopup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.closePopUp = $scope.action = function() {
		$rootScope.closeCopyOpPopup();
	}

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		$scope.header = $scope.getMsgs('CMS.lowBalance_popUp.text.heading');
		$scope.body = $scope.getMsgs('CMS.lowBalance_popUp.text.txt1');
		$scope.body += '<span class="b">' + params.minInvAmount + '</span>';
		
		$scope.closePopUp = function() {
			$rootScope.closeCopyOpPopup();
		}
		
		$scope.action = function() {
			$rootScope.closeCopyOpPopup();
			$rootScope.$state.go('ln.in.a.personal.my-account.banking', {ln: $scope.skinLanguage});
		}
	}
}]);

copyOpApp.controller('withdrawSurveyPopup', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.closePopUp = $scope.action = function() {
		$rootScope.closeCopyOpPopup();
	}

	$scope.passParams = function(params){
		$scope.popUpType = params.type;
		
		$scope.config = params;
		
		$scope.showSurvey = true;
		$scope.showSurveyConfirmation = false;
		
		$scope.survey = {};
		$scope.survey.answer = 0;
		$scope.survey.textAnswer = '';
		$scope.config.scope.userAnswerId = $scope.survey.answer;
		$scope.config.scope.textAnswer = $scope.survey.textAnswer;
		$scope.showError = false;
		
		$scope.checkShouldClose = function(event) {
			if($rootScope.shouldCloseCopyOpPopup(event)){
				$scope.submitWithdraw();
			}
		}
		
		$scope.submitSurvey = function() {
			if($scope.survey.answer == 0){
				$scope.showError = true;
			}else{
				$scope.config.scope.userAnswerId = $scope.survey.answer;
				$scope.config.scope.textAnswer = $scope.survey.textAnswer;
				if($scope.survey.answer != 405){
					$scope.config.scope.textAnswer = '';
				}
				$scope.showSurvey = false;
				$scope.showSurveyConfirmation = true;
			}
		}
		
		$scope.submitWithdraw = function() {
			$rootScope.closeCopyOpPopup();
			$scope.config.scope.submit($scope.config.form);
		}
	}
}]);

copyOpApp.controller('tooltipCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.inputs = {isKnowledge:false}
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
	}
	$scope.updatePos = function(){
		//Position the tooltip
		var target = $scope.config.event.target;
		var topOffset = Math.sqrt(30*30*2)/2;
		var defaultOffsetLeft = 65;
		var arrowOffsetLeft = 145;
		var offsetLeft = ($(target).offset().left + Math.min(defaultOffsetLeft, $(target).outerWidth()/2) - arrowOffsetLeft) + "px";
		if(parseInt(offsetLeft) < 0){
			offsetLeft = '5%';
		}
		$(".popUp-container-bonus-balance").css("left", offsetLeft);
		$(".popUp-container-bonus-balance").css("top", ($(target).offset().top + $(target).outerHeight() + topOffset) + "px");
		$(".popUp-container-bonus-balance").css("display", "block");
	}
	setTimeout(function(){
		$scope.updatePos();
		$scope.updatePosInterval = setInterval(function(){
			$scope.updatePos();
		}, 500);
	}, 0);
	
	$scope.closePopup = function(){
		clearInterval($scope.updatePosInterval);
		$rootScope.closeCopyOpPopup();
	}
}]);

copyOpApp.controller('shoppingBag', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	$scope.totalAmount = 0;
	
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
		$scope.addOrDeleteShoppingBagArr(params.info);
	}
	
	
	$scope.addOrDeleteShoppingBagArr = function (shopingBagObj) {
		if (shopingBagObj.command.toLowerCase() != "delete") {
			if (!$scope.isExistOppInSB(shopingBagObj.oppId)) {
				var popIndex = searchJsonKeyInArray($scope.popUps,'type','bonusBalance');
				if (popIndex > -1) {
					$rootScope.closeCopyOpPopup({popupId: popIndex});
				}
				
				$rootScope.shoppingBag.push(shopingBagObj);
				$scope.sortShoppingBagArr();
				
				$scope.totalAmountDsp = $rootScope.shoppingBag[$rootScope.shoppingBag.length - 1].amountDsp;
				$scope.timeClose = $rootScope.shoppingBag[$rootScope.shoppingBag.length - 1].timeClose;
			}
		} else {
			$scope.closePopUp();
		}
		
		// for (var i = 0; i < $rootScope.shoppingBag.length; i++) {
			// $scope.totalAmount += $rootScope.shoppingBag[i].amount;
		// }
	}
	
	$scope.isExistOppInSB = function (oppId) {
		for (var i = 0; i < $rootScope.shoppingBag.length; i++) {
			if($rootScope.shoppingBag[i].oppId === oppId) {
				return true;
			}
		}
		return false;
	}

	$scope.sortShoppingBagArr = function() {
		$rootScope.shoppingBag.sort(function(a, b) {
			var marketNameA = a.marketName.toLowerCase(), marketNameB=b.marketName.toLowerCase();
			var oppTypeA=a.oppType, oppTypeB=b.oppeTyp;	
			
			if(oppTypeA == 2){
				oppTypeA = 99;
			}
			
			if(oppTypeB == 2){
				oppTypeA = 99;
			}
						
			if (marketNameA > marketNameB) return  1;
			if (marketNameA < marketNameB) return -1;
			
			if (oppTypeA > oppTypeB) return  1;
			if (oppTypeA < oppTypeB) return -1;
		});
	}

	$scope.closePopUp = function() {
		$rootScope.closeCopyOpPopup();
		$rootScope.shoppingBag.length = 0;
	}
}]);

copyOpApp.controller('WithdrawUserDetailsController', ['$rootScope', '$scope', '$http','$upload', '$timeout', function($rootScope, $scope, $http, $upload, $timeout) {
	$scope.ready = true;
	
	$scope.passParams = function(params) {
		$scope.popUpType = params.type;
		$scope.config = params;
	}
	
	$scope.form = {
		type: 2
	}
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.getWithdrawUserDetails();
		}
	}
	
	$scope.getWithdrawUserDetails = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: $rootScope.copyopUser.user.id
			}
		});
			
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/getWithdrawUserDetails', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!handleErrors(data, 'getWithdrawUserDetails')) {
					$scope.updateId == null;
					if (data.withdrawUserDetails != null) {
						$scope.form = jsonClone($scope.form, data.withdrawUserDetails);
						$scope.updateId = data.withdrawUserDetails.id;
					}
				}
			})
			.catch(function(data) {
				handleNetworkError(data);
			})
	}
	
	$scope.insertUpdateWithdrawUserDetailas = function(_form) {
		if($scope.isSubmitting){
			return false;
		}
		$scope.isSubmitting = true;
		if (_form.$valid) {
			$scope.form.userId = $rootScope.copyopUser.user.id;
			var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
				withdrawUserDetails: $scope.form,
				withdrawUserDetailsType: $scope.form.type
			});
			
			var url = ($scope.updateId != null) ? 'updateWithdrawUserDetils' : 'insertWithdrawUserDetailas';
			$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/'+url, WithdrawUserDetailsMethodRequest)
				.then(function(data) {
					data = data.data;
					if (!handleErrors(data, url)) {
						if ($scope.form.type == 1) {
							if ($scope.selectedFiles.length) {
								$scope.send();
							} else {
								$scope.isSubmitting = false;
								$rootScope.closeCopyOpPopup();
							}
						} else {
							$scope.isSubmitting = false;
							$rootScope.closeCopyOpPopup();
						}
					}
				})
				.catch(function(data) {
					$scope.isSubmitting = false;
					handleNetworkError(data);
				})
		} else {
			$scope.isSubmitting = false;
			logIt({'type':3,'msg':$scope._form.$error});
		}
	}
	
	$scope.skipedWithdrawBankUserDetils = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: $rootScope.copyopUser.user.id
			}
		});
		
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/skipedWithdrawBankUserDetils', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!handleErrors(data, 'skipedWithdrawBankUserDetils')) {
					$scope.isSubmitting = false;
					$rootScope.closeCopyOpPopup();
				}
			})
			.catch(function(data) {
				$scope.isSubmitting = false;
				handleNetworkError(data);
			})
	}
	
	$scope.closePopUp = function(params) {
		// $rootScope.closeCopyOpPopup(params);
		// $scope.skipedWithdrawBankUserDetils();
	}
	
	waitForIt();

	$scope.shortenFileName = function(str){
		var maxLength = 8;
		var dotPos = str.lastIndexOf('.');
		if(dotPos != -1){
			var name = str.substring(0, dotPos);
			var extension = str.substring(dotPos);
			if(name.length > maxLength){
				name = name.substring(0, maxLength) + '...';
			}
			str = name + extension;
		}
		return str;
	}

	//Handle file upload
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

	$scope.selectedFiles = [];
	$scope.selectedSlideKey = null;
	$scope.progress = [];
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, slideKey) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.selectedSlideKey = slideKey;
		$scope.dataUrls = [];
		$scope.uploadedImage;
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				$scope.uploadedFileName = $file.name;
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileType=35&';
			params += 'writerId=' + settings.writerId + '&';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: settings.jsonImagegLink + '/UploadDocumentsService' + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileName: $scope.selectedFiles[index].name
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0){
					$scope.uploadedImage = null;
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			$scope.uploadedImage = null;
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		$scope.isSubmitting = false;
		$rootScope.closeCopyOpPopup();
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
	$scope.send = function(){
		for ( var i = 0; i < $scope.selectedFiles.length; i++) {
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	}
}]);