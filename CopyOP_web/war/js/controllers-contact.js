copyOpApp.controller('ContactCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.form = {};
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	waitForIt();
	
	function init() {
		$scope.contactForm = {contact: {}};
		$scope.getCountryId();
		function checkVar() {
			if (isUndefined($scope.detectedCountryId)) {
				setTimeout(function(){checkVar()},50);
			} else {
				$scope.contactForm.contact.countryId = $scope.detectedCountryId;
			}
		}
		checkVar();
	}

	$scope.submit = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var ContactMethodRequest  = jsonClone(getMethodRequest(), {
					comments: $scope.comments,
					contact: {
						firstName: (settings.loggedIn) ? $rootScope.copyopUser.user.firstName : $scope.form.firstName,
						lastName: (settings.loggedIn) ? $rootScope.copyopUser.user.lastName : $scope.form.lastName,
						name: (settings.loggedIn) ? $rootScope.copyopUser.user.name : $scope.form.firstName + ' ' + $scope.form.lastName,
						email: (settings.loggedIn) ? $rootScope.copyopUser.user.email : $scope.form.email,
						countryId: (settings.loggedIn) ? $rootScope.copyopUser.user.countryId : $rootScope.copyopUser.user.country.id,
						phone: (settings.loggedIn) ? $rootScope.copyopUser.user.mobilePhone : $scope.form.mobilePhone,
						userId:  $rootScope.copyopUser.user.id,
						countryName: $rootScope.copyopUser.user.country.displayName,
						mId: settings.mId,
						httpReferer: settings.httpReferer,
						combId: settings.combinationId,
						dynamicParameter: settings.dynamicParam,
						writerId: settings.writerId,
						skinId: settings.skinId,
						type: 2
					}
				});
				
				$http.post(settings.jsonLink + 'insertContact', ContactMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.loginNotCheck = true;
						if (!handleErrors(data, 'insertContact')) {
							$scope.form = {};
							_form.$setPristine();
							$rootScope.openCopyOpPopup({
								type: 'info', 
								body: 'contact.mail.success', 
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
}]);