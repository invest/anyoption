copyOpApp.controller('tradingCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.chartSettings = {
		boxId: 0,//starts from 0 init a new chart box
		boxType: 4,//1:AO small (HP, binary option), 2: AO profit line, 3: AO option plus, 4: copyOP main, 5: copyOP mini
		designType: 3//1:AO small, 2: AO big (option plus, profitline), 3: copyOp main, 4: copyOP mini
	}
	function waitForIt() {
		var box = g('tradeBox_main_0');
		if (!$scope.$parent.ready || isUndefined($scope.$parent.allBinaryMarkets) || isUndefined(returnRefund_all) || isUndefined(box)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	$scope.mainMarketId = 0;
	$scope.chartsCount = 0;
	$scope.historyCount = 0;
	$scope.charts = [];
	var charts = [];
	var group_tradeBox = [];
	var duplicateChart = null;
	
	function getChartData(box, request, duplicate) {
		$http.post(settings.jsonLink + 'getChartData', request)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getChartData')) {
					if (box > 0 || (duplicate)) {
						if (data.marketId == $rootScope.$stateParams.marketId) {
							duplicateChart = box;
							getChartData(0, jsonClone(getUserMethodRequest(),{box: 0, marketId: 0, opportunityId: 0, opportunityTypeId: 1}), true);
							return;
						} else if (duplicate) {
							defaultMarkets[duplicateChart] = data;
							box = duplicateChart;
							charts[duplicateChart] = {boxId: duplicateChart, boxType:5, designType:4, marketId: data.marketId};
						} else {
							defaultMarkets[box] = data;
							charts[box] = {boxId:box, boxType:5, designType:4, marketId: data.marketId};
						}
					} else {
						defaultMarkets[box] = data;
						$scope.mainMarketId = data.marketId;
						// $scope.chartSettings = {boxId:box, boxType:4, designType:3}
						$scope.marketName = marketsDisplayName[data.marketId];
						box_objects[box] = new box_obj({'box': box, 'invest':$rootScope.copyopUser.user.defaultAmountValue,'boxType': $scope.chartSettings.boxType});
						markestInBox[box] = box_objects[box].market = {'market_id':data.marketId,'schedule':1,'type':'aotps'};
						box_objects[box].marketId = data.marketId;
						$scope.chartsCount++;
						$scope.getPastExpiries(data.marketId);
					}
					group_tradeBox[box] = 'aotps_1_' + data.marketId;
					$scope.initLS(false);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
	
	function init() {
		for (var i = 0; i < 4; i++) {
			if (i == 0 && typeof $rootScope.$stateParams.marketId != 'undefined') {
				var chartDataMethodRequest = jsonClone(getUserMethodRequest(),{box: i, marketId: $rootScope.$stateParams.marketId, opportunityId: 0, opportunityTypeId: 0});
			} else {
				var chartDataMethodRequest = jsonClone(getUserMethodRequest(),{box: i, marketId: 0, opportunityId: 0, opportunityTypeId: 1})
			}
			getChartData(i, chartDataMethodRequest, false);
		}
		
		$scope.ready = true;
	}

	$scope.initLS = function(box) {
		if (box) {
			$scope.chartsCount++;
		} else {
			$scope.historyCount++;
			if ($scope.historyCount == 4) {
				$scope.charts = charts;
			}
		}
		if ($scope.chartsCount == 4 && $scope.historyCount == 4) {
			connectToLS_global({'group_tradeBox':group_tradeBox, 'fixedBox':true});
		}
	}
	
	$scope.current = settings.optionsLimit;
	$scope.rowsOnPage = settings.optionsLimit;
	$scope.showOptions = function() {
		if ($scope.current == settings.optionsLimit) {
			$scope.current = 100;
			$scope.rowsOnPage = 100;
		} else {
			$scope.current = settings.optionsLimit;
			$scope.rowsOnPage = settings.optionsLimit;
		}
		angular.element(g('optionOptionsTab')).scope().getInvestments($scope.current);
	}
	
	$scope.pastExpiries = [];
	$scope.getPastExpiries = function(marketId) {
		var PastExpiresMethodRequest = jsonClone(getUserMethodRequest(),{
			// date:
			marketId: marketId,
			// marketGroupId:
			startRow: 0,
			pageSize: 5
		});
		$http.post(settings.jsonLink + 'getPastExpiries', PastExpiresMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getPastExpiries')) {
					$scope.pastExpiries = data.opportunities;
					for (var i = 0; i < data.opportunities.length; i++) {
						$scope.pastExpiries[i].timeEstClosingDsp = adjustTimeToBrowser(data.opportunities[i].timeEstClosing, false);
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.marketInfo = '';
	
	$scope.loadMarketInfo = function(marketId){
		var arrayIndex = searchJsonKeyInArray($scope.assetIndexMarkets, 'id', marketId);
		if (arrayIndex != -1) {
			$scope.marketInfo = $scope.assetIndexMarkets[arrayIndex];
		}
	}
	
	$scope.showMarketInfo = function(){
		if(!$scope.isVisibleMarketInfo){
			$scope.isVisibleMarketInfo = true;
		}
	}
	
	$scope.hideMarketInfo = function(){
		if($scope.isVisibleMarketInfo){
			$scope.isVisibleMarketInfo = false;
		}
	}
	
	$scope.toggleMarketInfo = function(){
		if($scope.isVisibleMarketInfo){
			$scope.isVisibleMarketInfo = false;
		}else{
			$scope.isVisibleMarketInfo = true;
		}
	}
}]);

copyOpApp.controller('tradingInvestmentsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.total = 0;
	$scope.tblSettings = {
		tableClass: 'settle-table',
		buttons: true,
		totalRow: true,
		link: false// removed by issue CORE-3276
	}
	$scope.passParams = function(params){
		$scope.params = params;
		// $scope.tblSettings.buttons = !$scope.params.isSettled;// removed by issue CORE-3276
		$scope.tblSettings.buttons = false;
	}
	
	$scope.displayShowoff = true;
	$scope.displayExpiryColumn = true;
	
	$scope.sortAssetsParams = {
		openFirst: true,
		openSortField: 'timeCreated',
		openSortFieldType: 'date',
		openSortOrder: 'desc',
		settledSortField: 'timeCreated',
		settledSortFieldType: 'date',
		settledSortOrder: 'desc'
	}
	
	function waitForIt() {
		if (!$scope.$parent.ready || typeof $scope.params == 'undefined') {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		$scope.getInvestments(settings.optionsLimit);
		$scope.total = formatAmount(0, 0, true);
	}
	
	
	$scope.getInvestments = function(limit) {
		$http.post(settings.jsonLink + 'getUserInvestments', jsonClone(getUserMethodRequest(), {
				isSettled: $scope.params.isSettled,
				startRow: 0,
				// pageSize: 5
			}))
			.success(function(data, status, headers, config) {
				$rootScope.investmentsMaxShownCount = settings.optionsLimit;
				$scope.total = 0;
				if (!handleErrors(data, 'getUserInvestments')) {
					//$scope.setUser(data);
					$scope.investments = data.investments;
					if (!$scope.params.isSettled) {
						$scope.closingColumTxt = $rootScope.getMsgs('current');
						var c = 0;
						var j = 0;
						$rootScope.openOptions = [];
						for (var n = 0; n < $scope.investments.length; n++) {
							/*if (searchJsonKeyInArray($rootScope.openOptions, 'id', $scope.investments[n].id) == -1) {*/
								if (!$scope.params.isSettled && (data.investments[n].typeId == 6 || data.investments[n].typeId == 7)) {
									//skip open bubbles and dynamics
									continue;
								}
								data.investments[n].index = j;
								j++;
								data.investments[n].expiryLevel = data.investments[n].currentLevel;
								data.investments[n].timeCreated = adjustFromUTCToLocal(new Date(data.investments[n].timeCreated)).getTime() - settings.serverOffsetMillis;
								data.investments[n].timeEstClosing = adjustFromUTCToLocal(new Date(data.investments[n].timeEstClosing)).getTime();
								$rootScope.openOptions.push(data.investments[n]);
								
								for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
									var inx = searchJsonKeyInArray($scope.marketGroups.groups[i].markets, 'id', $scope.investments[n].marketId);
									if (inx > -1) {
										$scope.marketGroups.groups[i].markets[inx].topMarketForGroup = true;
									}
								}
								if ($rootScope.lsMarkets.indexOf('aotps_1_' + $scope.investments[n].marketId) == -1){
									$rootScope.lsMarkets.push('aotps_1_' + $scope.investments[n].marketId);
									c++;
								}
								$scope.total += data.investments[n].amount;
							/*}*/
						}
						if (c > 0) {
							setTimeout(function() {
								connectToLS_global({group_assets: $rootScope.lsMarkets});
							}, 1000);
						}
						$scope.investments = $rootScope.openOptions;
						$scope.$parent.investmentsOpenCount = $scope.investments.length;
					} else {
						$scope.closingColumTxt = $rootScope.getMsgs('closing');
						$scope.tblSettings.totalRow = false;
						for (var n = 0; n < $scope.investments.length; n++) {
							$scope.investments[n].index = n;
							
							if ($scope.investments[n].isCanceled) {
								$scope.investments[n].expiryLevel = $rootScope.getMsgs('canceled');
							} else if ($scope.investments[n].typeId == 7 && parseFloat($scope.investments[n].expiryLevel) == 0) {
								$scope.investments[n].expiryLevel = 'Dynamics';
							} else {
								$scope.investments[n].expiryLevel = ($scope.investments[n].isSettled == 0) ? $rootScope.getMsgs('open-options') : $scope.investments[n].expiryLevel;
							}
							
							$scope.investments[n].timeCreated = adjustFromUTCToLocal(new Date(data.investments[n].timeCreated)).getTime() - settings.serverOffsetMillis;
							$scope.investments[n].timeEstClosing = adjustFromUTCToLocal(new Date(data.investments[n].timeEstClosing)).getTime();
							if (($scope.investments[n].typeId == 1 && $scope.investments[n].currentLevel < $scope.investments[n].expiryLevel.replace(/,/g,'').replace(/ /g,'')) ||
								($scope.investments[n].typeId == 2 && $scope.investments[n].currentLevel > $scope.investments[n].expiryLevel.replace(/,/g,'').replace(/ /g,''))) {
								$scope.investments[n].win = true;
								$scope.investments[n].winAmount = formatAmount($scope.investments[n].oddsWin*$scope.investments[n].amount, 0, true);
							} else {
								$scope.investments[n].win = false;
							}
						}
						$scope.$parent.investmentsSettledCount = $scope.investments.length;
					}
				}
				$scope.total = formatAmount($scope.total, 2, true);
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.expectedReturn = function() {
		if (typeof $rootScope.openOptions != 'undefined') {
			var expectedReturnDsp = 0;
			for (var i = 0; i < $rootScope.openOptions.length; i++) {
				if (!isUndefined($rootScope.openOptions[i].amountReturn)) {
					expectedReturnDsp += $rootScope.openOptions[i].amountReturn;
				}
			}
			return formatAmount(expectedReturnDsp, 2, true);
		}
	}
	
	$scope.showCopiedFrom = function(index) {
		if ($scope.investments[index].copyopType == 'COPY') {
			clearTimeout($scope.tooltipTimer);
			for (var i = 0; i < $scope.investments.length; i++) {
				if (i != index) {
					$scope.investments[i].show = false;
				}
			}
			if (!$scope.investments[index].show) {
				$scope.investments[index].show = true;
				if (isUndefined($scope.investments[index].orgProfile)) {
					$http.post(settings.jsonLink + 'getCopyopOriginalInvProfile', jsonClone(getUserMethodRequest(), {
							copiedInvestmentId: $scope.investments[index].id
						}))
						.success(function(data, status, headers, config) {
							//if (!handleErrors(data, 'getCopyopOriginalInvProfile')) {
								$scope.investments[index].orgProfile = data;
							//}
						})
						.error(function(data, status, headers, config) {
							handleNetworkError(data);
						});
				}
			}
		}
	}
	
	$scope.hideCopiedFrom = function(index) {
		//$scope.investments[index].show = false;
	}

	waitForIt();
}]);

copyOpApp.controller('tradingChartCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.chartSettings = {
		boxId: -1,//styarts from 0 init a new chart box
		boxType: 5,//1:AO small (HP, binary option), 2: AO profit line, 3: AO option plus, 4: copyOP main, 5: copyOP mini
		designType: 4//1:AO small, 2: AO big (option plus, profitline), 3: copyOp main, 4: copyOP mini
	};
	$scope.passParams = function(params){
		$scope.chartSettings = params;
	}
	
	function waitForIt() {
		if (!$scope.$parent.ready || typeof $scope.chartSettings == 'undefined' || g('tradeBox_main_' + $scope.chartSettings.boxId) == null) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$scope.marketName = marketsDisplayName[$scope.chartSettings.marketId];
		box_objects[$scope.chartSettings.boxId] = new box_obj({'box': $scope.chartSettings.boxId, 'invest':$rootScope.copyopUser.user.defaultAmountValue ,'boxType': $scope.chartSettings.boxType});
		markestInBox[$scope.chartSettings.boxId] = box_objects[$scope.chartSettings.boxId].market = {'market_id':$scope.chartSettings.marketId,'schedule':1,'type':'aotps'};
		box_objects[$scope.chartSettings.boxId].marketId = $scope.chartSettings.marketId;
		$scope.ready = true;
		$scope.$parent.initLS(true);
	}
	
	$scope.nameFilters = {};
	$scope.marketGroupUl = {};
	
	$scope.getGroupOpenMarkets = function(markets){
		for(var i = 0; i < markets.length; i++){
			var isOpen = false;
			for(var j = 0; j < ls_marketStates.length; j++){
				if(markets[i].id == ls_marketStates[j]){
					isOpen = true;
					break;
				}
			}
			markets[i].isOpen = isOpen;
		}
		return markets;
	}
	
	$scope.showNameFilter = function(e, boxId){
		document.getElementById('nameFilter_'+boxId).style.display = "block";
		document.getElementById('nameFilter_'+boxId).focus();
	}
	
	$scope.selectMarket = function(marketId, boxId){
		window.txt_loading = $rootScope.getMsgs('loading');
		window.marketMenuError = $rootScope.getMsgs('market.menu.error');
		showMarketsMenu_box = boxId;
		var selectMarketBox = selectMarket(marketId);
		if(!isUndefined(selectMarketBox) && selectMarketBox >= 0){
			$rootScope.openCopyOpPopup({
				type: 'info',
				header: 'info-popup-header',
				body: 'market.menu.error',
				onclose: function(){
					document.getElementById('nameFilter_'+boxId).focus();
				}
			});
		}else{
			$scope.chartSettings.marketId = marketId;
			$('.panel').removeClass('active');
			$scope.nameFilters[boxId] = "";
			document.getElementById('nameFilter_'+boxId).style.display = "none";
		}
	}
}]);

copyOpApp.controller('tradingAssetsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}

	$scope.filterAll = {topMarketForGroup: 'true'};
	$rootScope.favMarkets = [];
	$rootScope.favMarketsOrder = [];
	$scope.changeTab = function(tab) {
		$scope.tabNum = tab;
		switch (tab) {
			case 1:
				$scope.order = 'displayName';
				$scope.showAll(false);
				break;
			case 2:
				$scope.order = 'est_close';
				$scope.showAll(true);
				break;
			case 3:
				$scope.order = 'showInFavOrder';
				$scope.getFavMarkets();
				break;
		}
	}
	
	$rootScope.lsMarkets = [];
	$scope.showAllState = true;
	$scope.showAll = function(forceAll) {
		$rootScope.lsMarkets = [];
		if (!isUndefined(forceAll)) {
			$scope.showAllState = forceAll;
			if ($scope.showAllState) {
				$scope.filterAll = {};
			} else {
				$scope.filterAll = {topMarketForGroup: 'true'};
			}
		} else {
			if ($scope.showAllState) {
				$scope.showAllState = false;
				$scope.filterAll = {topMarketForGroup: 'true'};
			} else {
				$scope.showAllState = true;
				$scope.filterAll = {};
			}
		}
		for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
			for (var j = 0; j < $scope.marketGroups.groups[i].markets.length; j++) {
				if ($scope.showAllState || (!$scope.showAllState && $scope.marketGroups.groups[i].markets[j].topMarketForGroup)) {
					$rootScope.lsMarkets.push('aotps_1_' + $scope.marketGroups.groups[i].markets[j].id);
				}
			}
		}
		connectToLS_global({group_assets: $rootScope.lsMarkets});
	}
	
	$scope.showFav = function() {
		$rootScope.lsMarkets = [];

		for (var i = 0; i < $rootScope.favMarkets.length; i++) {
			$rootScope.lsMarkets.push('aotps_1_' + $rootScope.favMarkets[i]);
		}
		connectToLS_global({group_assets: $rootScope.lsMarkets});
	}
	
	
	$scope.showInactive = function(index) {
		if ($scope.marketGroups.groups[index].showInactiveStates) {
			$scope.marketGroups.groups[index].showInactiveStates = false;
			$scope.marketGroups.groups[index].filterInactive = {active: 'true'};
		} else {
			$scope.marketGroups.groups[index].showInactiveStates = true;
			$scope.marketGroups.groups[index].filterInactive = {};
		}
	}
	
	$scope.getFavMarkets = function() {
		if ($rootScope.favMarkets.length == 0) {
			$http.post(settings.jsonLink + 'getFavMarkets', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					// data.favoriteMarkets = [];
					data.loginNotCheck = true;
					if (!handleErrors(data, 'getFavMarkets')) {
						$rootScope.favMarkets = data.favoriteMarkets;
						for (var i = 0; i < data.favoriteMarkets.length; i++) {
							$rootScope.favMarketsOrder.push(i);
						}
						
						$scope.showFav();
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.showFav();
		}
	}
	
	function init() {
		$scope.order = 'displayName';
		$scope.tabNum = 1;
		$scope.showAll(false);
	}
	
   $scope.assetFilter = function(obj) {
        var filter = [99,100];
        for (var i = 0; i < filter.length; i++) {
            if (obj.id == filter[i]) {
                return false;
            }
        }
        return true;
    };
	
	waitForIt();
}]);

function ls_updateAssets(updateInfo) {
	angular.element('html').scope().ls_updateAssetsNg(updateInfo);
}
