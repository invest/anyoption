var menuOverlayId = 'menu-overlay';
var menuOverlayClassName = 'menu-overlay';
$(function() {
	$(document).on('click', '.toggle', function() {
		if (!$(this).closest('.panel').is('.active')) {
			$('.panel').removeClass('active');
			$(this).closest('.panel').addClass('active');
		} else {
			$('.panel').removeClass('active');
		}
		removeOverlay(menuOverlayId, menuOverlayClassName, true);
	});
	$(document).on('click', '.dropdown', function(e) {
		$('.dropdown.active').not($(this)).removeClass('active');
		$(this).toggleClass('active');
		if(($(this).attr('id') == 'crunch-holder') && $(this).hasClass('active')){
			$(this).find("ul").css("height", getVisibleHeight('#body') - getVisibleHeight('#header') + "px");
			addOverlay(menuOverlayId, menuOverlayClassName, true);
		}else if($(this).attr('id') == 'crunch-holder'){
			removeOverlay(menuOverlayId, menuOverlayClassName, true);
		}else{
			removeOverlay(menuOverlayId, menuOverlayClassName, true);
		}
	});
	$(window).resize(function(){
		$('#crunch-holder.dropdown.active').find("ul").css("height", getVisibleHeight('#body') - getVisibleHeight('#header') + "px");
		if(document.getElementById(menuOverlayId)){
			document.getElementById(menuOverlayId).style.height = document.body.offsetHeight + "px";
			document.getElementById(menuOverlayId).style.width = document.body.offsetWidth + "px";
		}
	});
	$(document).on('click', function(e) {
		if (!$(e.target).closest('.dropdown').length) {
			$('.dropdown').removeClass('active');
			removeOverlay(menuOverlayId, menuOverlayClassName, true);
		}
	});
	$(document).on('click', '.inbox', function(e) {
		e.preventDefault();
	});
	$(document).on('click', '.change-amount li > div', function() {
		$('.checked').removeClass('checked');
		$(this).addClass('checked');
	});
});

