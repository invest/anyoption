copyOpApp.controller('howItWorksCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.hideMore = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	function init() {
		$scope.slider = new gd_slider({
			mainId: 'how-it-works',
			navId: 'how-it-works-navigation',
			slideType: 'slide',
			timing: [4, 4, 6, 4, 4, 4, 4, 0],
			autoStart: false,
			autoSlide: false,
			callBackOnChange: $scope.stateChange
		});
		var lis = g('how-it-works-navigation').getElementsByTagName('li');
		for (var i = 0; i < lis.length; i++) {
			var el = lis[i].getElementsByTagName('i')[0];
			if (!isUndefined(el)) {
				var h1 = ((lis[i].offsetHeight - 71) / 2);
				el.style.top = h1 + 80 + 'px';
				el.style.height = h1 + ((lis[i+1].offsetHeight - 100) / 2) + 30 + 'px';
			}
		}
		if (detectDevice() != '') {
			addEvent(window, 'resize', function() {
				$scope.slider.restart()
			});
			var ul = g('how-it-works').getElementsByTagName('ul')[0];
			ul.style.height = ul.offsetWidth * 1.086 + 'px';
		}
	}
	
	$scope.stateChange = function(state) {
		if (state < 5) {
			$scope.hideMore = false;
		} else {
			$scope.hideMore = true;
		}
		$scope.$apply();
	}
	
	$scope.sliderGoTo = function(slideId) {
		$scope.slider.play({startFrom: slideId});
	}
	waitForIt();
}]);