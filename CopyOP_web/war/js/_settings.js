// 'use strict';

var settings = {
	domain: window.location.host,
	msgsPath: 'msgs/',
	msgsFileName: 'MessageResources_',
	msgsEUsufix: '_EU',
	msgsExtension: '.json',
	protocol: window.location.href.split('/')[0],
	writerId: (detectDevice() != "" ? 24000 : 10000),
	skinId: 0,
	skinIdTexts: 2,
	skinLanguage: 'en',
	//donutChartColors: ['#00bc6f', '#35478c', '#ea2400', '#ffab4a'],
	donutChartColors: ['#ffab4a', '#ea2400', '#35478c', '#00bc6f'],
	timeFirstVisit: getCookie('timeFirstVisit'),
	avatarSizeLimit: 1,//MB
	documentSizeLimit: 0.7,//MB
	loggedIn: false,
	fbAppId: 832674160084280,
	fbAppScope: 'public_profile,email,user_friends',
	numOfMaleAvatars: 100,
	numOfFemaleAvatars: 50,
	serverTime: 0,
	serverOffsetMillis: 0,
	dstOffset: 0,
	maxCopying: 100,
	feedLimit: 50,
	optionsLimit: 4,
	resultsOnPage: 15,
	maxAssetSelected: 10,
	followTimeOut: 6000,//10 seconds
	followAmoutIncProc: 10,//10%
	shortYearFormat: new Date().getFullYear().toString().slice(-2),
	defaultLanguage: 16,
	timeToShowCoShopingBag: 8000,//8 seconds
	timeToShowShopingBag: 8000,//8 seconds
	loginDetected: false,
	url_params: window.location.search || (window.location.hash.indexOf('?') >= 0 ? window.location.hash.substring(window.location.hash.indexOf('?')) : ''),
	shareLink: 'https://www.copyop.com',
	sessionTimeout: (1000*60*31),//31 min
	appsflyerios: '//www.copyop.com/appsflyerios',
	appsflyerandroid: '//www.copyop.com/appsflyerandroid',
	recaptchaKey: '6Lf_jA0TAAAAANleUF7cUhAEcSCb3etKQg4ak5lU',
	showCnacelInv: true,
	showCnacelInv: true
}
settings.domain_parts = settings.domain.split('.');
settings.shortYear = Number(settings.shortYearFormat);
settings.shortYearFuture = settings.shortYear + 20;

if (settings.timeFirstVisit == null) {
	var m = new Date();
	var dateString = (m.getUTCMonth()+1) + ' ' + m.getUTCDate() + ',' + m.getUTCFullYear() + ' ' + m.getUTCHours() + ':' +m.getUTCMinutes() + ':' + m.getUTCSeconds();
	setCookie('timeFirstVisit', dateString, 365);
}

var utcOffset = new Date().getTimezoneOffset();
var jsonObjects = {}
jsonObjects.methodRequest = {
	utcOffset: utcOffset, 
	writerId: settings.writerId, 
	locale: ''
}
function getMethodRequest() {
	return jsonClone(jsonObjects.methodRequest, {skinId: settings.skinId});
}
try {
	jsonObjects.userMethodRequest = {
		encrypt: false, 
		isLogin: false, 
		afterDeposit: false,
		fingerPrint: new Fingerprint().get()
	}
} catch (e){}
function getUserMethodRequest() {
	return jsonClone(jsonObjects.userMethodRequest, getMethodRequest());
}
function getProfileMethodRequest(params) {
	return jsonClone(getUserMethodRequest(), {requestedUserId: params.userId, resetPaging: params.resetPaging});
}
try {
	jsonObjects.insertUserMethodRequest = {//combines ao and copyop InsertUserMethodRequest method
		register: {
			// timeFirstVisit: settings.timeFirstVisit,
			timeFirstVisit: 'Oct 28, 2014 9:46:38 AM',
			calcalistGame: false,
			skinId: 1,
			stateId: 0,
			languageId: 0,
			currencyId: 0,
			registerAttemptId: settings.registerAttemptId,
			contactId: 0,
			cityId: 0,
			contactByEmail: true,
			contactBySms: true,
			terms: true,
			deviceUniqueId: ''
		},
		marketingStaticPart: '',
		mId: '',
		etsMId: '',
		utmSource: '',
		avatar: '',
		avatarDsp: '',
		fbId: '',
		sendMail: false,
		sendSms: false,
		isBaidu: false,
		fingerPrint: new Fingerprint().get()
	}
} catch (e){}
function getInsertUserMethodRequest() {
	return jsonClone(jsonObjects.insertUserMethodRequest, getMethodRequest());
}
var FMT = {//FeedMessageTypes
	PROPERTY_KEY:		 						"key",
	PROPERTY_COMMAND:		 					"command",
	PROPERTY_KEY_USER_ID: 						"cpop_userId",
	PROPERTY_KEY_HIT_RATE: 						"cpop_hitRate",
	PROPERTY_KEY_PROFIT: 						"cpop_profit_",
	PROPERTY_KEY_ASSET_CREATE_DATE_TIME: 		"cpop_assetCreateDateTime",
	PROPERTY_KEY_ASSET_LAST_TRADING_HOURS:		"cpop_lastTradinghours",
	PROPERTY_KEY_COPIED_COUNT:					"cpop_copiedCount",
	PROPERTY_KEY_RANKED_IN_TOP: 				"cpop_ranked_in_top",
	PROPERTY_KEY_TOP_RANK: 						"cpop_top_ranke",
	PROPERTY_KEY_AMOUNT:						"cpop_amount",

	PROPERTY_KEY_ASSET_TREND:					"cpop_trend",
	PROPERTY_KEY_INVESTMENT_TYPE:				"cpop_investmentType",
	PROPERTY_KEY_OPPORTUNITY_EXPIRE:			"cpop_opportunityExpire",
	PROPERTY_KEY_OPPORTUNITY_ID:				"cpop_opportunityId",
	PROPERTY_KEY_SCHEDULED: 					"cpop_scheduled",
	PROPERTY_KEY_MARKET_ID:						"cpop_marketId",

	PROPERTY_KEY_INV_ID:						"cpop_invId",
	PROPERTY_KEY_LEVEL:							"cpop_level",
	PROPERTY_KEY_AMOUNT_IN_CURR:				"cpop_amount_in_curr_",
	PROPERTY_KEY_DIRECTION:						"cpop_dir",
	PROPERTY_KEY_DEST_USER_ID:					"cpop_destUserId",
	PROPERTY_KEY_DEST_NICKNAME:					"cpop_destNickname",
	PROPERTY_KEY_TIME_SETTLED:					"cpop_time_settled",
	PROPERTY_KEY_ODDS_WIN:						"cpop_oddsWin",
	PROPERTY_KEY_ODDS_LOSE:						"cpop_oddsLose",
	PROPERTY_KEY_TIME_CREATED:					"cpop_time_created",
	PROPERTY_KEY_COPIERS:						"cpop_copiers",
	PROPERTY_KEY_COPYING:						"cpop_copying",
	PROPERTY_KEY_WATCHERS:						"cpop_watchers",
	PROPERTY_KEY_WATCHING:						"cpop_watching",
	PROPERTY_KEY_NICKNAME:						"cpop_nickname",
	PROPERTY_KEY_OLD_NICKNAME:					"cpop_old_nickname",
	PROPERTY_KEY_AVATAR:						"cpop_avatar",
	PROPERTY_KEY_COINS_BALANCE:					"cpop_coins_balance", 
	PROPERTY_KEY_UPDATE_TYPE:					"cpop_update_type",
	PROPERTY_KEY_FIRST_NAME:					"cpop_first_name",
	PROPERTY_KEY_LAST_NAME:						"cpop_last_name",
	PROPERTY_KEY_SEATS_LEFT:					"cpop_seats_left",
	PROPERTY_KEY_PUSH_SKIN_ID:					"cpop_push_skin_id",
	PROPERTY_BONUS_TYPE_ID:						"cpop_bonus_type_id",
	PROPERTY_BONUS_DESCRIPTION:					"cpop_bonus_description",
	PROPERTY_KEY_USER_FROZEN:					"cpop_user_frozen",
	PROPERTY_KEY_DEST_USER_FROZEN:				"cpop_dest_user_frozen",
	PROPERTY_KEY_UUID:							"cpop_uuid",
	PROPERTY_KEY_INHERITED_UUID:				"cpop_inherited_uuid",
	PROPERTY_KEY_TIME_LAST_INVEST:				"cpop_time_last_invest",
	
	PROPERTY_KEY_SRC_USER_ID: "cpop_srcUserId",
	PROPERTY_KEY_SRC_NICKNAME: "cpop_srcNickname",
	PROPERTY_KEY_SRC_AVATAR: "cpop_srcAvatar"
}
var payment_type = {
	direct24: 2,//PAYMENT_TYPE_DIRECT24
	giropay: 3,//PAYMENT_TYPE_GIROPAY
	eps: 4//PAYMENT_TYPE_EPS
}
var oldTexts = {}//TODO refactor
var returnRefund_all = [];
var defaultMarkets = [];//TODO refactor!

var skinMap = {
	'1': {
		subdomain:'www',
		locale: 'iw',
		isRgulated: false,
		defaultSkinId: 1,
		languageId: 1,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Zא-ת ]+$"
	},
	'2': {
		subdomain:'www',
		locale: 'en',
		isRgulated: false,
		rgulatedSkinId: 16,
		defaultSkinId: 16,
		languageId: 2,
		isAllowed: false,
		skinSelector: false,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'3': {
		subdomain:'tr',
		locale: 'tr',
		isRgulated: false,
		defaultSkinId: 3,
		languageId: 3,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZİıÖöÜüÇçĞğŞş ]+$"
	},
	'4': {
		subdomain:'ar',
		locale: 'ar',
		isRgulated: false,
		defaultSkinId: 4,
		languageId: 4,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'5': {
		subdomain:'es',
		locale: 'es',
		isRgulated: false,
		rgulatedSkinId: 18,
		defaultSkinId: 18,
		languageId: 5,
		isAllowed: false,
		skinSelector: false,
		skinIdTexts: 5,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'8': {
		subdomain:'de',
		locale: 'de',
		isRgulated: false,
		rgulatedSkinId: 19,
		defaultSkinId: 19,
		languageId: 8,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 8,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$"
	},
	'9': {
		subdomain:'it',
		locale: 'it',
		isRgulated: false,
		rgulatedSkinId: 20,
		defaultSkinId: 20,
		languageId: 9,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 9,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$"
	},
	'10': {
		subdomain:'ru',
		locale: 'ru',
		isRgulated: false,
		defaultSkinId: 10,
		languageId: 10,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-ZА-Яа-яЁё ]+$"
	},
	'12': {
		subdomain:'fr',
		locale: 'fr',
		isRgulated: false,
		defaultSkinId: 21,
		languageId: 12,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 12,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$"
	},
	'13': {
		subdomain:'en-us',
		locale: 'en-us',
		isRgulated: false,
		defaultSkinId: 16,
		languageId: 2,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 13,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'14': {
		subdomain:'es-us',
		locale: 'es-us',
		isRgulated: false,
		defaultSkinId: 18,
		languageId: 5,
		isAllowed: true,
		skinSelector: false,
		skinIdTexts: 14,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'15': {
		subdomain:'sg',
		locale: 'sg',
		isRgulated: false,
		defaultSkinId: 15,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$"
	},
	'16': {
		subdomain:'www',
		locale: 'en',
		isRgulated: true,
		defaultSkinId: 16,
		languageId: 2,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 16,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'17': {
		subdomain:'kr',
		locale: 'kr',
		isRgulated: false,
		defaultSkinId: 17,
		languageId: 17,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z\u1100-\u11FF\uAC00-\uD7AF\u3130-\u318F ]+$"
	},
	'18': {
		subdomain:'es',
		locale: 'es',
		isRgulated: true,
		defaultSkinId: 18,
		languageId: 5,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 18,
		regEx_letters: "^['a-zA-ZÁáÉéÍíÑñÓóÚúÜü ]+$"
	},
	'19': {
		subdomain:'de',
		locale: 'de',
		isRgulated: true,
		defaultSkinId: 19,
		languageId: 8,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 19,
		regEx_letters: "^['a-zA-ZäÄöÖüÜß ]+$"
	},
	'20': {
		subdomain:'www1',
		locale: 'it',
		isRgulated: true,
		defaultSkinId: 20,
		languageId: 9,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 20,
		regEx_letters: "^['a-zA-ZÀàÁáÈèÉéÌìÍíÒòÓóÙùÚú ]+$"
	},
	'21': {
		subdomain:'fr',
		locale: 'fr',
		isRgulated: true,
		defaultSkinId: 21,
		languageId: 12,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 21,
		regEx_letters: "^['a-zA-Zéèàùâêîôûëïç ]+$"
	},
	'22': {
		subdomain:'hk',
		locale: 'hk',
		isRgulated: false,
		defaultSkinId: 22,
		languageId: 15,
		skinSelector: false,
		isAllowed: false,
		skinIdTexts: settings.defaultLanguage,
		regEx_letters: "^['a-zA-Z一-龠 ]+$"
	},
	'23': {
		subdomain:'nl',
		locale: 'nl',
		isRgulated: true,
		defaultSkinId: 23,
		languageId: 23,
		skinSelector: true,
		isAllowed: true,
		skinIdTexts: 23,
		regEx_letters: "^['a-zA-Zéëï ]+$"
	},
	'24': {
		subdomain:'se',
		locale: 'sv',
		isRgulated: true,
		defaultSkinId: 24,
		languageId: 24,
		skinSelector: true,
		isAllowed: true,
		skinIdTexts: 24,
		regEx_letters: "^['a-zA-ZÅÄÖåäö ]+$"
	},
	'25': {
		subdomain:'www',
		locale: 'en',
		isRgulated: false,
		rgulatedSkinId: 16,
		defaultSkinId: 16,
		languageId: 25,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 2,
		regEx_letters: "^['a-zA-Z ]+$"
	},
	'26': {
		subdomain:'es',
		locale: 'es',
		isRgulated: false,
		rgulatedSkinId: 18,
		defaultSkinId: 18,
		languageId: 26,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 5,
	},
	'27': {
		subdomain:'cs',
		locale: 'cs',
		isRgulated: true,
		defaultSkinId: 27,
		rgulatedSkinId: 27,
		languageId: 27,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 27,
	},
	'28': {
		subdomain:'pl',
		locale: 'pl',
		isRgulated: true,
		defaultSkinId: 28,
		rgulatedSkinId: 28,
		languageId: 28,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 28,
	},
	'29': {
		subdomain:'pt',
		locale: 'pt',
		isRgulated: true,
		defaultSkinId: 29,
		rgulatedSkinId: 29,
		languageId: 29,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 29,
	},
	'30': {
		subdomain:'ar',
		locale: 'ar',
		isRgulated: false,
		defaultSkinId: 30,
		rgulatedSkinId: 30,
		languageId: 30,
		isAllowed: true,
		skinSelector: true,
		skinIdTexts: 30,
	}
}
var currencyConst = {
	all: 0,//CURRENCY_ALL_ID
	ils: 1,//CURRENCY_ILS_ID
	usd: 2,//CURRENCY_USD_ID
	eur: 3,//CURRENCY_EUR_ID
	gbp: 4,//CURRENCY_GBP_ID
	'try': 5,//CURRENCY_TRY_ID
	rub: 6,//CURRENCY_RUB_ID
	cny: 7,//CURRENCY_CNY_ID
	kr: 8,//CURRENCY_KR_ID
	sek: 9,//CURRENCY_SEK_ID
	aud: 10,//CURRENCY_AUD_ID
	zar: 11,//CURRENCY_ZAR_ID
	czk: 12,
	pln: 13,
}
var currencyMap = {
	'0': {
		shortName: 'all',
	},
	'1': {
		shortName: 'ils',
	},
	'2': {
		shortName: 'usd',
	},
	'3': {
		shortName: 'eur',
	},
	'4': {
		shortName: 'gbp',
	},
	'5': {
		shortName: 'try',
	},
	'6': {
		shortName: 'rub',
	},
	'7': {
		shortName: 'krw',
	},
	'8': {
		shortName: 'cny',
	},
	'9': {
		shortName: 'sek',
	},
	'10': {
		shortName: 'aud',
	},
	'11': {
		shortName: 'zar',
	},
	'12': {
		shortName: 'czk',
	},
	'13': {
		shortName: 'pln',
	}
}
var countries = {
	spain: 192
}
var errorCodeMap = {
	success: 0,
	withdrawal_close: 110,
	invalid_input: 200,
	invalid_account: 201,
	dup_request: 202,
	missing_param: 203,
	login_failed: 205,
	inv_validation: 206,
	general_validation: 207,
	validation_without_field: 208,
	validation_low_balance: 209,
	transaction_failed: 210,
	cc_not_supported: 211,
	invalid_captch: 212,
	no_cash_for_niou: 213,
	regulation_missing_questionnaire: 300,
	regulation_suspended: 301,
	regulation_suspended_documents: 302,
	user_not_regulated: 303,
	regulation_user_restricted: 304,
	reg_suspended_quest_incorrect: 305,
	regulation_user_pep_prohibited: 307,
	regulation_user_is_treshold_block: 308,
	regulation_suspended_cnmv_docs: 313,

	questionnaire_not_full: 400,
	questionnaire_not_supported: 401,
	questionnaire_already_done: 402,
	session_expired: 419,
	opp_not_exists: 500,
	file_too_large: 600,
	user_removed: 800,
	user_blocked: 801,
	unknown: 999,

	et_regulation_additional_info: 700,
	not_accepted_terms: 701,
	et_regulation_knowledge_confirm: 702,
	et_regulation_capital_questionnaire_not_done: 703,
	et_regulation_suspended_low_score: 704,
	et_regulation_suspended_low_treshold: 705,
	et_regulation_suspended_medium_treshold: 706,

	copyop_max_copiers: 1001,

	bubbles_invalid_signature: 3000
}

var userRegulationBase = {
	regulation_user_registered: 0, 
	regulation_wc_approval: 1,
	regulation_first_deposit: 2,
	regulation_mandatory_questionnaire_done: 3,
	regulation_all_documents_received: 5,
	regulation_all_documents_support_approval: 6,
	regulation_control_approved_user: 7,
	
	et_regulation_knowledge_question_user: 103,
	et_capital_market_questionnaire: 104,
	et_regulation_done: 105,
	
	et_regulation_low_score_group_calc: 9,
	et_regulation_medium_score_group_calc: 17,
	
	et_regulation_unsuspended_score_group_id: 0,
	et_regulation_high_score_group_id: 1,
	et_regulation_medium_score_group_id: 2,
	et_regulation_low_score_group_id: 3,	
	
	ao_regulation_restricted_score_group_id: 4,
	ao_regulation_restricted_low_x_score_group_id: 5,
	ao_regulation_restricted_high_y_score_group_id: 6,
	ao_regulation_restricted_failed_q_score_group_id: 7,

	not_suspended: 0,
	suspended_by_world_check: 1,
	suspended_mandatory_questionnaire_not_filled: 2,
	suspended_due_documents: 3,
	suspended_manually: 4,
	suspended_mandatory_questionnaire_filled_incorrect: 5,
	suspended_due_cnmv: 15,

	non_prohibited: 0,
	auto_prohibited: 1,
	writer_prohibited: 2,
	approved_by_compliance: 3,
	false_classification: 4,

	et_suspend_low_score_group: 10,
	et_suspend_treshehold_low_score_group: 11,
	et_suspend_treshehold_medium_score_group: 12,
	
	regulation_user_version: 2, 

	days_from_qualified_time: 30
}

var fileStatus = {
	not_selected: 0,
	requested: 1,
	in_progress: 2,
	done: 3,
	invalid: 4,
	not_needed: 5
}
var fileType = {
	user_id_copy: 1,
	passport: 2,
	cc_copy: 4,
	bankwire_confirmation: 12,
	withdraw_form: 13,
	power_of_attorney: 16,
	account_closing: 17,
	cc_holder_id: 18,
	ssn: 20,
	driver_license: 21,
	utility_bill: 22,
	cc_copy_front: 23,
	cc_copy_back: 24
}

var balanceCallTypes = {
	total: 1,//CALL_TOTAL_AMOUNT_BALANCE
	open_amount: 2,//CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE
	settled_amount: 3,//CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE
	settled_return: 4//CALL_SETTLED_INVESTMENTS_RETURN_BALANCE
}

var optimoveTid = 'e6d329373bfb6eb36a22036097673bb059c4c348c2e8dd85dcd75de1071072a9';
var optimoveEvents = {
	login: 1,
	pageVisit: 3,
	loss: 5,
	win: 6,
	deposit: 7,
	withdraw: 8,
	trade: 9
}

settings.hpYoutubeVideo = {
	en: 'https://www.youtube.com/embed/QAadOMMFLU8?autoplay=1',
	es: 'https://www.youtube.com/embed/oueUpnklOew?autoplay=1',
	de: 'https://www.youtube.com/embed/6lkPdXAFKd8?autoplay=1',
	fr: 'https://www.youtube.com/embed/hP_cwdr1dYo?autoplay=1',
	it: 'https://www.youtube.com/embed/pnZuMxQ8-PY?autoplay=1',
	cz: 'https://youtu.be/uB4p2rySmq4?autoplay=1',
	hu: 'https://youtu.be/l9DNIshue0w?autoplay=1',
	nu: 'https://youtu.be/yP6Nxwmyr9c?autoplay=1',
	sv: 'https://youtu.be/bcwJn2Nq4MI?autoplay=1',
	po: 'https://youtu.be/W2yvaA-_fVY?autoplay=1',
}

//regExp
var regEx_lettersAndNumbers = /^[0-9a-zA-Z]+$/;
var regEx_nickname = /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/;
var regEx_nickname_reverse = /[^a-zA-Z0-9!@#$%\^:;"']/g;
var regEx_phone  = /^\d{7,20}$/;
var regEx_digits = /^\d+$/;
var regEx_digits_reverse  = /\D/g;
var regEx_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regEx_englishLettersOnly = /^['a-zA-Z ]+$/;
var regEx_lettersOnly = "^['\\p{L} ]+$";

var replaceParamsGT = {
	'${assets}': '<div ng-include="\'html/minisite/general-terms-asset-index.html\'" ng-controller="AssetIndexController"></div>',
	'${index_page}': '.',
	'${privacy_link}': './privacy',
	'${complaint_form}': './img/pdf/complaint-form/complaint-form-{locale}.pdf'
}

var bankWireInfo = [
	{
		//Regulated EUR 
		currencies: [3],
		accountName: 'Ouroboros Derivatives Trading Limited',
		accountNumber: 992970605,
		iban: 'DE62500100600992970605',
		swift: 'PBNKDEFFXXX',
		bankName: 'Postbank Frankfurt',
		address: 'Frankfurt am Main, 60288, Germany',
		limitations: 'bankwire-limitations-msg-eur'
	},{
		//Regulated USD 
		currencies: ['all'],
		accountName: 'Ouroboros Derivatives Trading Limited',
		accountNumber: 901660661,
		iban: 'DE45590100660901660661',
		swift: 'PBNKDEFF590',
		bankName: 'Postbank Frankfurt',
		address: 'Frankfurt am Main, 60288, Germany',
		limitations: 'bankwire-limitations-msg-usd'
	}
];