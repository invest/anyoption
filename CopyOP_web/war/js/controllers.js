copyOpApp.controller('copyOpCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile) {
	if (!settings.isLive) {
		$("body").attr('data-isdead','true');
	}
	$scope.changeHeader = function(login) {
		if (login) {
			if (detectDevice() == '') {
				$scope.template_url = 'html/templates/header.html';
			} else {
				$scope.template_url = 'html/minisite/header-login.html';
			}
		} else {
			$scope.template_url = 'html/minisite/header.html';
		}
	}
	$scope.changeHeader(false);

	$rootScope.ready = $scope.ready = false;//need to set ready when we have a user //TO DO make only $scope
	function waitForIt() {
		if (isUndefined($rootScope.$state.current.needLogin)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.defineCopyopWebSkin();
		}
	}
	waitForIt();
	
	//detect skin id and if logged
	$scope.defineCopyopWebSkin = function() {
		var urlLnId = parseInt(searchJsonKeyInJson(skinMap, 'locale', $rootScope.$state.params.ln));
		var skinIdCookie = getCookie('skinId');
		var locale = '';
		if (!isUndefined(skinIdCookie) && skinIdCookie != '') {
			skinId = skinIdCookie;
		} else {
			skinId = 0;
			if (urlLnId > -1) {
				locale = $rootScope.$state.params.ln;
				//skinId = skinMap[urlLnId].defaultSkinId;
			}
		}
		$http.post(settings.jsonLink + 'defineCopyopWebSkin', jsonConcat(getUserMethodRequest(),{
				skinId: skinId,
				locale: locale
			}))
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'defineCopyopWebSkin')) {
					//set params
					$rootScope.maxCopying = settings.maxCopying;
					settings.skinId = data.skinId;
					//TODO - hack for AR skin
//					settings.skinId = 30;
//					$rootScope.isRtl = true;
					
					settings.loginDetected = true;
					$scope.skinLanguage = settings.skinLanguage = skinMap[data.skinId].locale;
					$rootScope.isRegulated = settings.isRegulated = data.isRegulated;
					//TODO - hack for AR skin
//					$rootScope.isRegulated = settings.isRegulated = false;
					
					settings.loggedIn = data.loggedIn;
					settings.serverTime = data.currentTime;
					settings.serverOffsetMillis = data.currentTime - new Date().getTime();
					
					settings.skin_group_id = data.skinGroupId;
					SUBSCR_FLD.ao_level = settings.level_skin_group_id = 'LEVEL_' + settings.skin_group_id;
					SUBSCR_FLD.ao_clr = settings.level_color = 'CLR_' + settings.skin_group_id;
					settings.insurance_current_level = 'INS_CRR_LEVEL_' + settings.skin_group_id;
					settings.defaultCurrencyId = data.currencyId;
					settings.currencyLeftSymbol = data.currencyLeftSymbol;
					settings.currencySymbol = data.currencySymbol;
					// settings.ao_calls_trend = '#{applicationData.skinId == 1 ? "ET_CALLS_TREND" : "AO_CALLS_TREND"}';
					SUBSCR_FLD.ao_calls_trend = settings.ao_calls_trend = 'AO_CALLS_TREND';
					
					SUBSCR_FLD.ao_markets = 'AO_HP_MARKETS_' + settings.skinId;
					
					//
					
					replaceParamsGT['${complaint_form}'] = replaceParamsGT['${complaint_form}'].replace('{locale}', skinMap[skinMap[settings.skinId].skinIdTexts].locale);
					
					$scope.getTrackingParams();
					$scope.redirectLogin(data);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}

	$scope.redirectLogin = function(data) {
		setCookie('skinId', settings.skinId, 365);
		if (data.loggedIn) {
			var device = detectDevice();
			if (isUndefined($rootScope.copyopUser.errorCode)) {
				$scope.getCopyopUser({onStateChange: true});
			}
			if (device != '') {
				$rootScope.$state.go('ln.mobile-deposit', {ln: $scope.skinLanguage});
			} else {
				if ($rootScope.$state.current.needLogin < 1) {
					$rootScope.$state.go('ln.home.a', {ln: $scope.skinLanguage});
				}
				if (!isUndefined(settings.dpn) && settings.dpn != '') {
					$scope.getDeeplink();
				}
			}
		} else {
			$rootScope.copyopUser = {user: {}};
			$scope.skinLanguage = settings.skinLanguage = skinMap[settings.skinId].locale;
			
			if ($rootScope.$state.params.ln == 'tmp') {
				$rootScope.$state.go($rootScope.$state.current.name, {ln: $scope.skinLanguage});
			}
			if ($rootScope.$state.params.ln == 'check') {
				$rootScope.$state.go('ln.index', {ln: $scope.skinLanguage});
			} else if ($rootScope.$state.current.needLogin > 1 || isUndefined($rootScope.$state.current.needLogin)) {
				$rootScope.$state.go('ln.login', {ln: $scope.skinLanguage});
			}
			
			$scope.initRequests();//get all needed resources
			
			if (!isUndefined(settings.dpn) && settings.dpn != '') {
				$scope.getDeeplink();
			}
		}
	}
	
	//get logded user's info
	//login
	$scope.loginForm = getUserMethodRequest();
	$scope.getCopyopUser = function(params) {//onStateChange, _form, stateMachine
		if (params.onStateChange) {
			delete $scope.loginForm.password;
			delete $scope.loginForm.userName;
		}
		if (isUndefined($scope.loginForm.password) && isUndefined($scope.loginForm.userName)) {
			jsonObjects.userMethodRequest.encrypt = true;
			$scope.loginForm.encrypt = true;
		} else {
			jsonObjects.userMethodRequest.encrypt = false;
			$scope.loginForm.encrypt = false;
		}
		
		if (!params.onStateChange || (!isUndefined(params.forceInit) && params.forceInit)) {
			$scope.loginForm.login = {
				combinationId: settings.combinationId,
				dynamicParam: settings.dynamicParam,
				affSub1: settings.aff_sub1,
				affSub2: settings.aff_sub2,
				affSub3: settings.aff_sub3
			}
			$scope.loginForm.isLogin = true;
		}
		
		$scope.loginForm.skinId = settings.skinId;
		if (params.onStateChange || (!params.onStateChange && params._form.$valid)) {
			$scope.loading = true;
			$http.post(settings.jsonLink + 'getCopyopUser', $scope.loginForm)
				.success(function(data, status, headers, config) {
					$rootScope.afterDeposit = false;
					data.loginNotCheck = true;
					$scope.loading = false;
					if (!handleErrors(data, 'getCopyopUser')) {
						if (data.errorCode == 701) {
							$rootScope.copyopUser = data;
							settings.loggedIn = data.loggedIn = false;
							$scope.getCountries();
							$scope.$watch('countriesList.length', function() {
								if ($scope.countriesList.length > 0) {
									$scope.country = $scope.countriesList[searchJsonKeyInArray($scope.countriesList, 'id', data.user.countryId)].displayName;
								}
							});
							
							if (settings.skinId != data.user.skinId) {
								$scope.changeSkin(data.user.skinId);
							}
						} else {
							var device = detectDevice();
							
							settings.combinationId = data.user.combinationId;
							settings.showCnacelInv = data.user.showCancelInvestment;
							$rootScope.maxCopying = settings.maxCopying = data.maxCopiersPerAccount;
							settings.loggedIn = data.loggedIn = true;
							if (settings.skinId != data.user.skinId) {
								$scope.changeSkin(data.user.skinId);
							}
							settings.skinId = data.user.skinId;
							$scope.skinLanguage = settings.skinLanguage = data.skinLanguage =  skinMap[data.user.skinId].locale;
							
							$rootScope.safeMod = {
								isSafeMode: data.isSafeMode,
								maxTrades: data.safeModeMaxTrades,
								minCopyUsers: data.safeModeMinCopyUsers,
								period: data.safeModePeriod
							}
							
							
							if (!isUndefined(data.user.previousLoginTime)) {
								data.user.previousLoginTimeCopy = adjustFromUTCToLocal(new Date(data.user.previousLoginTime));
							} else {
								data.user.previousLoginTimeCopy = adjustFromUTCToLocal(new Date());
							}
							data.user.previousLoginTimeHeader = data.user.previousLoginTimeCopy.getDate() + '/' + (data.user.previousLoginTimeCopy.getMonth() + 1) + "/" + data.user.previousLoginTimeCopy.getFullYear();
							
							$rootScope.copyopUser = data;
							$scope.redirectLogin(data);
							
							var script = document.createElement('script');
							script.src = "https://www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded&hl="+settings.skinLanguage;
							document.head.appendChild(script);
						
							$rootScope.copyopUser.user.name = data.user.firstName + ' ' + data.user.lastName;
							$rootScope.copyopUser.userId = data.user.id;
							settings.userId = data.user.id;
							settings.userName = data.user.userName;
							settings.hashedPassword = data.user.hashedPassword;
							
							$scope.cUser = {
								userId: $rootScope.copyopUser.user.id,
								name: $rootScope.copyopUser.user.name,
								avatar: $rootScope.copyopUser.copyopProfile.avatar,
								copyingCount: $rootScope.copyopUser.copyopProfile.copying,
								copiersCount: $rootScope.copyopUser.copyopProfile.copiers,
								watchingCount: $rootScope.copyopUser.copyopProfile.watching,
								watchersCount: $rootScope.copyopUser.copyopProfile.watchers,
								hitRate: $rootScope.copyopUser.copyopProfile.hitRatePercentage,
								sameUser: true
							}
							
							settings.currencyLeftSymbol = data.user.currencyLeftSymbol;
							settings.currencySymbol = data.user.currencySymbol;
							settings.defaultCurrencyId = data.user.currencyId;
							
							if (data.user.currency != null) {
								settings.currencyCode = data.user.currency.code;
								settings.decimalPointDigits = data.user.currency.decimalPointDigits;
							}
							
							if (data.user.defaultAmountValue != null){
								settings.defaultAmountValue = data.user.defaultAmountValue;
								$rootScope.copyopUser.defaultAmountValueDsp = formatAmount(data.user.defaultAmountValue, 0, true);
							}
							
							if ($rootScope.copyopUser.userRegulation == null) {
								$rootScope.copyopUser.userRegulation = {};
								$rootScope.copyopUser.userRegulation.approvedRegulationStep = 0;
								$rootScope.copyopUser.userRegulation.optionalQuestionnaireDone = true;
								settings.isRegulated = false;
							} else {
								settings.isRegulated = true;
								$rootScope.copyopUser.userRegulation.thresholdBlockDsp = formatAmount(data.userRegulation.thresholdBlock, 2, true);
							}
							
							$rootScope.copyopUser.copyAmountsDsp = [];
							for (var i = 0; i < $rootScope.copyopUser.copyAmounts.length; i++) {
								$rootScope.copyopUser.copyAmountsDsp.push({
									amount: amountToFloat($rootScope.copyopUser.copyAmounts[i]),
									amountDsp: formatAmount($rootScope.copyopUser.copyAmounts[i],2, true),
								});
							}
							
							if (device == '') {
								$scope.getFacebookFriends();
							}
							
							$scope.changeHeader(true);
							
							//Show low balance popup if necessary
							if(data.minIvestmentAmount > 0){
								$scope.initWatch = $scope.$watch(function(){return $rootScope.ready;}, function(){
									if($rootScope.ready){
										$scope.initWatch();
										$rootScope.openCopyOpPopup({type: 'lowBalance', minInvAmount: formatAmount(data.minIvestmentAmount/100, 0)});
									}
								});
							}
							
							//update fb friends on login
							if (!params.onStateChange && $rootScope.copyopUser.copyopProfile.fbId != null) {
								$scope.updateFacebookFriends($rootScope.copyopUser.copyopProfile.fbId);
							}
							
							settings.skin_group_id = data.skinGroupId;
							SUBSCR_FLD.ao_level = settings.level_skin_group_id = 'LEVEL_' + settings.skin_group_id;
							SUBSCR_FLD.ao_clr = settings.level_color = 'CLR_' + settings.skin_group_id;
							settings.insurance_current_level = 'INS_CRR_LEVEL_' + settings.skin_group_id;
							
							SUBSCR_FLD.ao_markets = 'AO_HP_MARKETS_' + settings.skinId;
							
							if (!params.onStateChange || $rootScope.afterRefresh || (!isUndefined(params.forceInit) && params.forceInit)) {
								connectToLsCopyOp_global($rootScope, $scope);
								connectToLS_global({'group_shoppingBag':["shoppingbag_" + settings.userName.toLowerCase() + "_" + $rootScope.copyopUser.userId]});
								$scope.getLastLoginSummary();
								$scope.initRequests();//get all needed resources
								$scope.checkDirectDeposit();
								
								$scope.predefValues = [];
								if (data.user.predefValues != null) {
									for ( var i = 0; i < data.user.predefValues.length; i++) {
										$scope.predefValues.push({
											predefValue: data.user.predefValues[i].predefValue,
											predefValueDsp: formatAmount(data.user.predefValues[i].predefValue, 2, true)
										})
									}
								}
								
								if (!params.onStateChange) {
									$scope.getUserHolidayMessage();
								}
								
								$rootScope.afterRefresh = false;
							}
							
							//Check for login
							if (!params.onStateChange){
								params.fromLogin = true;
							}
								
							if ((isUndefined(params.stopStateMachine) || !stopStateMachine) && device == '') {
								$scope.stateMachine(params);
							}
							//some ugly shit
							function after3dSecure() {
								if (settings.action == 'Finish3dJson') {
									$rootScope.$state.go('ln.in.a.asset', {ln: $scope.skinLanguage});
									settings.action = ''
									setTimeout(function() {
										var errorCode  = getUrlValue('e');
										var transactionId = getUrlValue('t');
										var clearingProvider = getUrlValue('c');
										var isFirstDeposit = getUrlValue('fd');
										var balance = getUrlValue('b');
										var amount  = getUrlValue('a');
										var dollarAamount  = getUrlValue('da');
										
										var transaction = {
											transactionId: transactionId,
											amount: amount,
											dollarAmount: dollarAamount,
											balance: balance,
											clearingProvider: clearingProvider
										}
										
										if (errorCode == 0) {
											var onclose = null;
											var isFirstDeposit = 0;
											if (isFirstDeposit) {
												onclose = function() {$rootScope.openCopyOpPopup({type: 'autoWatch', destUserId: $rootScope.copyopUser.user.id})};
												$scope.addGTMPixel({pixelType: 'firstDeposit', transaction: transaction});
												$rootScope.copyopUser.user.firstDepositId = transaction.transactionId;
												isFirstDeposit = 1;
											} else {
												onclose = function() {
													$rootScope.afterDeposit = true;
													var checkStateMachine = !$scope.isQuestionairePEP();
													if (checkStateMachine) {
														checkStateMachine = !$scope.isTresholdBlock();
													}
													$scope.getCopyopUser({onStateChange: true, stateMachine: checkStateMachine});
												};
												$scope.addGTMPixel({pixelType: 'repeatDeposit', transaction: transaction});
											}
											optimoveReportGeneralEvent(optimoveEvents.deposit, {});
											
											$rootScope.openCopyOpPopup({
												type: 'info', 
												header: 'deposit-success-header', 
												body: 'deposit-success-body' + ((clearingProvider != '') ? '-provider' : ''), 
												onclose: onclose,
												animate: 1,
												animateFuncton: function() {
													animateBalanceUpdate()
												},
												bodyParams: {
													amount: '<b>' + formatAmount(amount, 2) + '</b>',
													transactionId: '<b>' + transaction.transactionId + '</b>',
													provider: '<b>' + transaction.clearingProvider + '</b>',
												}
											});
											
											$rootScope.copyopUser.user.balance = transaction.balance;
											$scope.balance = formatAmount(transaction.balance, 0);
											$rootScope.copyopUser.user.balanceWF = $scope.balance;
											
											if (settings.isLive) {
												//google tracking
												dataLayer.push({
													'is_deposit': '1',
													'is_first': isFirstDeposit,
													'deposit_sum': amount,
													'deposit_currency': $rootScope.copyopUser.user.currency.code
												});
											}
										} else {
											$('#globalErrorFieldPopup').html('');
											$rootScope.openCopyOpPopup({
												type: 'info-img', 
												header: $rootScope.getMsgs('unsuccessful-deposit'),
												buttonTxt: $rootScope.getMsgs('CMS.optionplus.popup.text.try.again'),
												imgClass: 'failed-deposit-popUp',
												bodyTxt: ''
											});
										}
									},2000)
								}
							}
							
							
							function checkMsgs() {
								if ($.isEmptyObject($scope.msgs)) {
									setTimeout(function() {
										checkMsgs();
									}, 500)
								} else {
									after3dSecure();
								}
							}
							checkMsgs();
							
							//send login event to optimove
							if(!params.onStateChange){
								optimoveReportGeneralEvent(optimoveEvents.login, {});
							}
							
							setTimeout(function() {
								$scope.loginForm.password = '';
								$scope.loginForm.userName = '';
							}, 10000);
						}
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		} else {
			logIt({'type':3,'msg':params._form.$error});
		}
	}
	
	$scope.setUser = function(data) {
		if (data.user != null) {
			var currency = $rootScope.copyopUser.user.currency;
			
			if (isUndefined(data.user.previousLoginTime)) {
				data.user.previousLoginTime = $rootScope.copyopUser.user.previousLoginTimeCopy;
			} else {
				data.user.previousLoginTime = adjustFromUTCToLocal(new Date(data.user.previousLoginTime));
			}
			data.user.previousLoginTimeCopy = data.user.previousLoginTime;
			data.user.previousLoginTimeHeader = data.user.previousLoginTime.getDate() + '/' + (data.user.previousLoginTime.getMonth() + 1) + "/" + data.user.previousLoginTime.getFullYear();
			
			$rootScope.copyopUser.user = data.user;
			if ($rootScope.copyopUser.user.currency == null) {
				$rootScope.copyopUser.user.currency = currency;
			}
			$rootScope.copyopUser.user.name = data.user.firstName + ' ' + data.user.lastName;
			$rootScope.copyopUser.user.balance = data.user.balance;
			$rootScope.copyopUser.user.balanceWF = formatAmount(data.user.balance, 0, true);
			settings.userName = data.user.userName;
			settings.hashedPassword = data.user.hashedPassword;
			settings.balance = data.user.balance;
			
			if ($rootScope.copyopUser.userRegulation == null) {
				$rootScope.copyopUser.userRegulation = {};
				$rootScope.copyopUser.userRegulation.approvedRegulationStep = 0;
				$rootScope.copyopUser.userRegulation.optionalQuestionnaireDone = true;
				settings.isRegulated = false;
			} else {
				settings.isRegulated = true;
				$rootScope.copyopUser.userRegulation = data.userRegulation;
			}
			
			if (isUndefined(data.updateCUuser)) {
				$rootScope.cUser = {
					userId: $rootScope.copyopUser.user.id,
					name: $rootScope.copyopUser.user.name,
					avatar: $rootScope.copyopUser.copyopProfile.avatar,
					copyingCount: $rootScope.copyopUser.copyopProfile.copying,
					copiersCount: $rootScope.copyopUser.copyopProfile.copiers,
					watchingCount: $rootScope.copyopUser.copyopProfile.watching,
					watchersCount: $rootScope.copyopUser.copyopProfile.watchers,
					hitRate: $rootScope.copyopUser.copyopProfile.hitRatePercentage,
					sameUser: true
				}
			}
		}
	}

	$scope.countriesList = [];
	$scope.getCountries = function(countryId) {
		if ($scope.countriesList.length == 0) {
			$http.post(settings.jsonLink + 'getSortedCountries', getMethodRequest())
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getSortedCountries')) {
						angular.forEach(data.countriesMap, function(value, key) {
							$scope.countriesList.push(jsonClone(value, {
								id: key
							}));
						});

						if (isUndefined(countryId) || countryId == 0) {
							if (isUndefined(data.countriesMap[data.ipDetectedCountryId])) {
								countryId = data.skinDefautlCointryId;
							} else if (data.ipDetectedCountryId != 0) {
								countryId = data.ipDetectedCountryId;
							} else if (data.skinDefautlCointryId != 0) {
								countryId = data.skinDefautlCointryId;
							}
							$scope.detectedCountryId = countryId;
						}
						
						if (!isUndefined(countryId)) {
							if (isUndefined($rootScope.copyopUser)) {
								setTimeout(function() {
									$scope.getCountries(countryId);
								},50);
							} else {
								var index = searchJsonKeyInArray($scope.countriesList, 'id', countryId);
								$rootScope.copyopUser.user.country = $scope.countriesList[index];
							}
						}
						logIt({'type':1,'msg':data});
						return $scope.detectedCountryId;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		} else {
			if (!isUndefined($scope.detectedCountryId) && isUndefined(countryId)) {
				countryId = $scope.detectedCountryId;
			}
			if (!isUndefined(countryId)) {
				if (isUndefined($rootScope.copyopUser)) {
					setTimeout(function() {
						$scope.getCountries(countryId);
					},50);
				} else {
					var index = searchJsonKeyInArray($scope.countriesList, 'id', countryId);
					$rootScope.copyopUser.user.country = $scope.countriesList[index];
				}
				return $scope.detectedCountryId;
			}
		}
	};
	
	$scope.getCountryId = function() {
		if (isUndefined($scope.detectedCountryId)) {
			$scope.getCountries();
		} else {
			$scope.getCountries();
			// return $scope.detectedCountryId;
		}
	}
	
	$scope.getAllBinaryMarkets = function() {
		if (isUndefined($scope.allBinaryMarkets)) {
			$scope.initRequestStatus.getAllBinaryMarkets = false;
			$http.post(settings.jsonLink + 'getAllBinaryMarkets', getMethodRequest())
				.success(function(data, status, headers, config) {
					data.loginNotCheck = true;
					if (!handleErrors(data, 'getAllBinaryMarkets')) {
						$scope.allBinaryMarkets = data;
						for (var i = 0; i < data.markets.length; i++){
							marketsDisplayName[data.markets[i].id] = data.markets[i].displayName;
						}
						$scope.initRequestStatus.getAllBinaryMarkets = true;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
	
	$scope.getMarketsNames = function() {
		if (isUndefined(settings.marketsNames)) {
			$scope.initRequestStatus.getMarketsNames = false;
			$http.post(settings.jsonLink + 'getMarketsNames', getMethodRequest())
				.success(function(data, status, headers, config) {
					data.loginNotCheck = true;
					if (!handleErrors(data, 'getMarketsNames')) {
						settings.marketsNames = data.marketsList;
						$scope.initRequestStatus.getMarketsNames = true;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
	
	$scope.getAllReturnOdds = function() {
		if (returnRefund_all.length == 0) {
			$scope.initRequestStatus.getAllReturnOdds = false;
			$http.post(settings.jsonLink + 'getAllReturnOdds', getMethodRequest())
				.success(function(data, status, headers, config) {
					data.loginNotCheck = true;
					if (!handleErrors(data, 'getAllReturnOdds')) {
						for (var i = 0; i < data.opportunityOddsPair.length; i++) {
							returnRefund_all[data.opportunityOddsPair[i].selectorID] = {profit: data.opportunityOddsPair[i].returnSelector,refund: data.opportunityOddsPair[i].refundSelector};
						}
						$scope.initRequestStatus.getAllReturnOdds = true;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
	
	//logout
	$scope.logout = function() {
		$http.post(settings.jsonLink + 'logoutUser', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'logout')) {
					settings.loggedIn = false;
					$scope.changeHeader(false);
					$rootScope.$state.go('ln.login', {ln: $scope.skinLanguage});
					$rootScope.initSettings();
				}
				var device = detectDevice();
				if (device == 'android'){
					$timeout(function(){window.location.href = '//www.copyop.com/appsflyerandroid?';},200);
				}else if(device == 'ios'){
					$timeout(function(){window.location.href = '//www.copyop.com/appsflyerios?';},200);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}

	//get list of numbers from one number to another
	$scope.Range = function(start, end) {
		var result = [];
		if (start < end) {
			for (var i = start; i <= end; i++) {
				if (i < 10) {
					result.push('0' + i);
				}
				else {
					result.push(i);
				}
			}
		} else {
			for (var i = start; i >= end; i--) {
				if (i < 10) {
					result.push('0' + i);
				}
				else {
					result.push(i);
				}
			}
		}
		return result;
	};

	$scope.showMandatoryQuestionnaire = function() {
		return (!isUndefined($rootScope.copyopUser.errorCode) && ($rootScope.copyopUser.userRegulation.regulationVersion == 1) &&
			($rootScope.copyopUser.userRegulation.approvedRegulationStep == 1 || $rootScope.copyopUser.userRegulation.approvedRegulationStep == 2));
	}
	
	$scope.showOptionalQuestionnaire = function() {
		return (!isUndefined($rootScope.copyopUser.errorCode) && ($rootScope.copyopUser.userRegulation.regulationVersion == 1) &&
			$rootScope.copyopUser.userRegulation.approvedRegulationStep == 3 && !$rootScope.copyopUser.userRegulation.optionalQuestionnaireDone);
	}
	
	$scope.showSingleQuestionnaire = function() {
		return (!isUndefined($rootScope.copyopUser.errorCode) && ($rootScope.copyopUser.userRegulation.regulationVersion == 2) && 
			($rootScope.copyopUser.userRegulation.approvedRegulationStep == 1 || $rootScope.copyopUser.userRegulation.approvedRegulationStep == 2));
	}
	
	//get translation from key
	$rootScope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($scope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $scope.msgsParam($scope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
					// return $scope.msgsParam($scope.msgs[key], params);
				} else {
					return $scope.msgs[key];
				}
			} else {
				return "?msgs[" + key + "]?";
			}
		} else {
			if ($scope.msgs.hasOwnProperty(key[0])) {
				if ($scope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key[0]][key[1]], params));
					} else {
						return $scope.msgs[key[0]][key[1]];
					}
				} else {
					return "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	//get msg with param
	$scope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}

	$scope.checkIfLogged = function() {
		return settings.loggedIn;
	}

	//conect with facebook
	$scope.loginWithFB = function() {
		FB.login(function(response) {
			logIt({'type':1,'msg':'fb login result'});
			logIt({'type':1,'msg':response});
			
			if (response.status === 'connected') {
				// Logged into your app and Facebook.
				FB.api('/me', function(responseInfo) {
					logIt({'type':1,'msg':'fb user info'});
					logIt({'type':1,'msg':responseInfo});
					$scope.fbUserInfo = responseInfo;
					var date = new Date().getTime();
					$scope.fbUserInfo.avatar = 'https://graph.facebook.com/'+responseInfo.id+'/picture?type=large&r' + date;
					$rootScope.$state.go('ln.finish-signup', {ln: $scope.skinLanguage});
				})
			} else if (response.status === 'not_authorized') {
				// The person is logged into Facebook, but not your app.
			} else {
				// The person is not logged into Facebook, so we're not sure if
				// they are logged into this app or not.
			}
		},
		{scope: settings.fbAppScope});
	}
	
	$scope.connectWithFB = function() {
		FB.login(function(response) {
			logIt({'type':1,'msg':'fb login result'});
			logIt({'type':1,'msg': response});
			
			if (response.status === 'connected') {
				$rootScope.copyopUser.copyopProfile.fbId
				$scope.updateFacebookFriends(response.authResponse.userID, true);
			} else if (response.status === 'not_authorized') {
				// The person is logged into Facebook, but not your app.
			} else {
				// The person is not logged into Facebook, so we're not sure if
				// they are logged into this app or not.
			}
		},
		{scope: settings.fbAppScope});
	}
	
	$scope.updateFacebookFriends = function(fbId, showPopup) {
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				getFriends();
			}
		});
		function getFriends() {
			FB.api(
				"/me/friends",
				function (response) {
					if (response && !response.error) {
						logIt({'type':1,'msg':'fb friends update'});
						logIt({'type':1,'msg':response});
						$scope.updateFbFriends(response);
					}
				}
			);
		}
		
		$scope.updateFbFriends = function(response) {
			var fbFriends = [];
			for (var i = 0; i < response.data.length; i++) {
				fbFriends.push(response.data[i].id);
			}
			
			var UpdateFacebookFriendsMethodRequest = jsonConcat(getUserMethodRequest(),{
				friends: fbFriends,
				facebookId: fbId
			})
			
			$http.post(settings.jsonLink + 'updateFacebookFriends', UpdateFacebookFriendsMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'updateFacebookFriends')) {
						if (showPopup) {
							$rootScope.closeCopyOpPopup();
							$rootScope.openCopyOpPopup({type: 'facebook'})
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		}
	}
	
	$scope.getFacebookFriends = function() {
		var pagingUserMethodRequest = jsonConcat(getUserMethodRequest(),{
			resetPaging: true
		});
		$http.post(settings.jsonLink + 'getFacebookFriends', pagingUserMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getFacebookFriends')) {
					$scope.myFacebookFriends = data.friends;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
	
	//GTM Data Layer pixel
	$scope.addGTMPixel = function(params){
		if (settings.isLive) {
			if (params.pixelType == 'afterRegister') {
				window.dataLayer = window.dataLayer || [];
				dataLayer.push({
					'userId': $rootScope.copyopUser.user.id,
					'event': 'Registration',
					'eventName': 'Registration',
				});
			} else if (params.pixelType == 'firstDeposit') {
				window.dataLayer = window.dataLayer || [];
				dataLayer.push({
					'userId': $rootScope.copyopUser.user.id,
					'event': 'FirstDeposit',
					'eventName': 'FirstDeposit',
					'transactionId': params.transaction.transactionId,
					'transactionTotal': params.transaction.amount,
					'transactionTotalUSD': params.transaction.dollarAmount,
					'transactionCurrency': $rootScope.copyopUser.user.currency.code,
					'transactionProducts': [{
						'sku': '1',
						'name': 'FTD',
						'price': params.transaction.amount,
						'quantity': '1',
					}]
				});
			} else if (params.pixelType == 'repeatDeposit') {
				window.dataLayer = window.dataLayer || [];
				dataLayer.push({
					'userId': $rootScope.copyopUser.user.id,
					'event': 'RepeatDeposit',
					'eventName': 'RepeatDeposit',
					'transactionId': params.transaction.transactionId,
					'transactionTotal': params.transaction.amount,
					'transactionTotalUSD': params.transaction.dollarAmount,
					'transactionCurrency': $rootScope.copyopUser.user.currency.code,
					'transactionProducts': [{
						'sku': '2',
						'name': 'Repeat',
						'price': params.transaction.amount,
						'quantity': '1',
					}]
				});
			}
		}
	}
	
	//tracking pixel
	$scope.getPixel = function(params) {//pixelType: firstNewCard - 1, register - 2
		if (settings.isLive) {
			var userId = $rootScope.copyopUser.user.id;
			var transactionId = params.transactionId;
			var pixelType = params.pixelType;
			var pixelMethodRequest = jsonClone(getMethodRequest(), {
				dynamicParam: settings.dynamicParam,
				combinationId: settings.combinationId,
				userId: userId,
				transactionId: transactionId,
				pixelType: pixelType
			})
			$http.post(settings.jsonLink + 'getTrackingPixel', pixelMethodRequest)
				.success(function(data, status, headers, config) {
					$('body').append($compile((data.pixelHttpsCode == '') ? data.pixelHttpCode : data.pixelHttpsCode)($scope));
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
	//tracking params
	$scope.getTrackingParams = function() {
		var combinationId = getUrlValue('combid');
		settings.combinationId = (combinationId == '') ? 0 : combinationId;
		settings.dynamicParam = getUrlValue('dp');
		settings.httpReferer = getCookie('hr');
		if (settings.httpReferer == null) {
			settings.httpReferer = document.referrer;
		}
		var mId = getCookie('smt')
		settings.mId = (mId == null) ? '' : mId;
		
		settings.utm_source = getUrlValue('utm_source');
		settings.campaginid = getUrlValue('utm_campaign'); 
		settings.utm_medium = getUrlValue('utm_medium');
		settings.utm_content = getUrlValue('utm_content');
		settings.aff_sub1 = getUrlValue('aff_sub1').substr(0, 20);
		settings.aff_sub2 = getUrlValue('aff_sub2').substr(0, 20);
		// settings.aff_sub3 = getUrlValue('aff_sub3').substr(0, 20);
		settings.aff_sub3 = '';
		if (getUrlValue('gclid') != '') {
			settings.aff_sub3 = 'gclid_' + getUrlValue('gclid').substr(0, 30);
		}
		settings.dpn = getUrlValue('dpn');
		
		//predefined register user data
		if (!$scope.firstName && getUrlValue('firstName') != '') {
			$scope.firstName = getUrlValue('firstName');
		}
		if (!$scope.lastName && getUrlValue('last_Name') != '') {
			$scope.lastName = getUrlValue('last_Name');
		}
		if (!$scope.email && getUrlValue('email') != '') {
			$scope.email = getUrlValue('email');
		}
		//3rd party actions
		settings.action = getUrlValue('action');
	}
	//inser contact
	$scope.insertRegisterAttempt = function() {
		$scope.registerAttemptMethodRequest = {
			email: g('email').value,
			firstName: g('firstName').value,
			lastName: g('lastName').value,
			mobilePhone: (g('mobilePhone').value != '') ? $rootScope.copyopUser.user.country.phoneCode + g('mobilePhone').value : '',
			countryId: $rootScope.copyopUser.user.country.id,
			marketingStaticPart: '',
			etsMId: '',
			mId: settings.mId,
			httpReferer: settings.httpReferer,
			combinationId: settings.combinationId,
			dynamicParameter: settings.dynamicParam
		}
		if (validateEmail($scope.registerAttemptMethodRequest.email) || $scope.registerAttemptMethodRequest.mobilePhone.length >= 7) {
			$http.post(settings.jsonLink + 'insertRegisterAttempt', jsonClone($scope.registerAttemptMethodRequest, getMethodRequest()))
				.success(function(data, status, headers, config) {
					data.loginNotCheck = true;
					if (!handleErrors(data, 'insertRegisterAttempt')) {
						settings.registerAttemptId = data.registerAttemptId;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		}
	}

	$scope.checkNoIndex = function() {
		if (typeof $rootScope.$state.current.robotIndex == 'undefined') {
			return true;
		} else {
			return $rootScope.$state.current.robotIndex;
		}
	}
	
	$scope.checkSameUser = function() {
		if (typeof $rootScope.cUser == 'undefined' || !$rootScope.cUser.sameUser) {
			return false;
		} else {
			return true;
		}
	}

	$scope.popUps = [];
	$rootScope.openCopyOpPopup = function(params) {
		$scope.setFocus('submitBtn');
		var setUp = {
			fnc: holyShit,
			config: params,
			type: params.type
		}
		switch(params.type) {
			case 'copy': 
				if ($rootScope.copyopUser.user.balance <= 0) {
					$rootScope.openCopyOpPopup({type: 'noMoney'});
				} else if (params.obj.copiers >= settings.maxCopying && !$rootScope.usersInfo[params.destUserId].copying) {
					$rootScope.popupInfo.destUserId = params.destUserId;
					$rootScope.openCopyOpPopup({type: 'fullyCopyied', obj: params.obj});
				} else {
					var _url,_ctr;
					if ($rootScope.usersInfo[params.destUserId].frozen && !$rootScope.usersInfo[params.destUserId].copying) {
						_url = 'html/templates/popUp-info.html';
						_ctr = 'infoPopupCtrl';
					} else {
						_url = 'html/templates/popUp-copy-watch.html';
						_ctr = 'copyPopupCtrl';
					}
					setUp.url = _url;
					setUp.ctr = _ctr;
					$scope.popUps.push(setUp);
				}
				break;
			case 'fullyCopyied': 
				setUp.url = 'html/templates/popUp-copy-watch.html';
				setUp.ctr = 'fullyCopiedCtrl';
				$scope.popUps.push(setUp);
				break;
			case 'watch': 
				if ($rootScope.copyopUser.user.balance <= 0) {
					$rootScope.openCopyOpPopup({type: 'noMoney'});
					break;
				} else {
					var _url,_ctr;
					if ($rootScope.usersInfo[params.destUserId].copying) {
						_url = 'html/templates/popUp-info.html';
						_ctr = 'infoPopupCtrl';
					} else {
						 _url = 'html/templates/popUp-copy-watch.html';
						_ctr = 'watchCtrl';
					}
					setUp.url = _url;
					setUp.ctr = _ctr;
					$scope.popUps.push(setUp);
				}
				break;
			case 'autoWatch': 
				setUp.url = 'html/templates/popUp-users-list.html';
				setUp.ctr = 'copyopWatching';
				$scope.popUps.push(setUp);
				break;
			case 'noFacebook': 
				setUp.url = 'html/templates/popUp-social.html';
				setUp.ctr = 'facebookConnect';
				$scope.popUps.push(setUp);
				break;
			case 'facebook': 
				setUp.url = 'html/templates/popUp-users-list.html';
				setUp.ctr = 'inviteFriends';
				$scope.popUps.push(setUp);
				break;
			case 'cashIt': 
				cashCopyOpCoins($rootScope, $scope, $http);
				break;
				setUp.url = 'html/templates/popUp-cash-it.html';
				setUp.ctr = 'cashCopyOpCoins';
				$scope.popUps.push(setUp);
				break;
			case 'follow': 
				if ($rootScope.copyopUser.user.balance <= 0) {
					$rootScope.openCopyOpPopup({type: 'noMoney'});
					break;
				} else {
//					var now = new Date().getTime()
//					if (params.data[FMT.PROPERTY_KEY_TIME_LAST_INVEST] <= now) {
//						params.data.followBtnDisabled = true;
//						return;
//					}
					setUp.url = 'html/templates/popUp-follow.html';
					setUp.ctr = 'followNow';
					$scope.popUps.push(setUp);
				}
				break;
			case 'termsBonus': 
				setUp.url = 'html/templates/popUp-terms-bonus.html';
				setUp.ctr = 'termsBonus';
				$scope.popUps.push(setUp);
				break;
			case 'info': 
				setUp.url = 'html/templates/popUp-info.html';
				setUp.ctr = 'infoPopup';
				$scope.popUps.push(setUp);
				break;
			case 'info-img': 
				setUp.url = 'html/templates/popUp-info-img.html';
				setUp.ctr = 'infoImgPopup';
				$scope.popUps.push(setUp);
				break;
			case 'confirm': 
				setUp.url = 'html/templates/popUp-info.html';
				setUp.ctr = 'infoPopup';
				$scope.popUps.push(setUp);
				break;
			case 'copyopShoppingBag': 
				setUp.url = 'html/templates/popUp-copyop-shopping-bag.html';
				setUp.ctr = 'infoPopup';
				$scope.popUps.push(setUp);
				break;
			case 'shoppingBag': 
				setUp.url = 'html/templates/popUp-shopping-bag.html';
				setUp.ctr = 'shoppingBag';
				$scope.popUps.push(setUp);
				break;
			case 'noMoney': 
				setUp.url = 'html/templates/popUp-no-money.html';
				setUp.ctr = 'noMoneyPopup';
				$scope.popUps.push(setUp);
				break;
			case 'deposit': 
				if ($rootScope.copyopUser.user.firstDepositId == 0) {
					$rootScope.closeCopyOpPopup({all: true});
					setUp.url = 'html/templates/popUp-first-deposit.html';
					setUp.ctr = 'depositPopup';
					$scope.popUps.push(setUp);
					break;
				} else {
					$rootScope.$state.go('ln.in.a.personal.my-account.banking', {ln: $scope.skinLanguage});
				}
				break;
			case 'otherFundingMethods': 
				$rootScope.closeCopyOpPopup({all:true});
				setUp.url = 'html/templates/popUp-bank-wire.html';
				setUp.ctr = 'depositPopup';
				$scope.popUps.push(setUp);
				break;
			case 'lastLoginSummary': 
				// $rootScope.closeCopyOpPopup();
				setUp.url = 'html/templates/popUp-summary-message.html';
				setUp.ctr = 'depositPopup';
				$scope.popUps.push(setUp);
				break;
			case 'changeNickname': 
				// $rootScope.closeCopyOpPopup();
				setUp.url = 'html/templates/popUp-change-nickname.html';
				setUp.ctr = 'changeNickname';
				$scope.popUps.push(setUp);
				break;
			case 'walkthrough': 
				setUp.url = 'html/templates/popUp-walkthrough.html';
				setUp.ctr = 'openWalkthrough';
				$scope.popUps.push(setUp);
				break;
			case 'affiliates': 
				setUp.url = 'html/templates/popUp-affiliates.html';
				setUp.ctr = 'affiliatesPopupCtr';
				$scope.popUps.push(setUp);
				break;
			case 'home-video': 
				setUp.url = 'html/minisite/popUp-home-video.html';
				setUp.ctr = 'infoPopup';
				setUp.config.hpYoutubeVideo = $sce.trustAsResourceUrl(settings.hpYoutubeVideo[$scope.skinLanguage]);
				$scope.popUps.push(setUp);
				break;
			case 'questionnaireRegulation': 
				setUp.url = 'html/templates/popUp-questionnaire-regulation.html';
				setUp.ctr = 'userSingleQuestionnaireCtr';
				$scope.popUps.push(setUp);
				break;
			case 'regulationStatus':
				setUp.url = 'html/templates/popUp-regulation-status.html';
				setUp.ctr = 'regulationStatusCtr';
				$scope.popUps.push(setUp);
				break;
			case 'regulationDocuments':
				setUp.url = 'html/templates/popUp-regulation-docs.html';
				setUp.ctr = 'regulationDocuments';
				$scope.popUps.push(setUp);
				break;
			case 'withdraw':
				setUp.url = 'html/templates/popUp-withdraw.html';
				setUp.ctr = 'withdrawPopup';
				$scope.popUps.push(setUp);
				break;
			case 'withdrawSuccess':
				setUp.url = 'html/templates/popUp-withdraw-accepted.html';
				setUp.ctr = 'infoPopup';
				$scope.popUps.push(setUp);
				break;
			case 'changePassword':
				setUp.url = 'html/templates/popUp-change-password.html';
				setUp.ctr = 'infoPopup';
				$scope.popUps.push(setUp);
				break;
			case 'bonusBalance':
				setUp.url = 'html/templates/popUp-bonus-balance.html';
				setUp.ctr = 'bonusBalancePopupCtr';
				$scope.popUps.push(setUp);
				break;
			case 'lowBalance': 
				setUp.url = 'html/templates/popUp-low-balance.html';
				setUp.ctr = 'lowBalancePopup';
				$scope.popUps.push(setUp);
				break;
			case 'withdrawSurvey': 
				setUp.url = 'html/templates/popUp-withdraw-survey.html';
				setUp.ctr = 'withdrawSurveyPopup';
				$scope.popUps.push(setUp);
				break;
			case 'tooltip': 
				setUp.url = 'html/templates/popUp-risk-warning.html';
				setUp.ctr = 'tooltipCtr';
				$scope.popUps.push(setUp);
				break;
			case 'withdrawUserDetails': 
				setUp.url = 'html/templates/popUp-withdrawUserDetails.html';
				setUp.ctr = 'WithdrawUserDetailsController';
				$scope.popUps.push(setUp);
				break;
			case 'generalTerms': 
				setUp.url = 'html/templates/popUp-generalTerms.html';
				setUp.ctr = 'generalTermsCtr';
				$scope.popUps.push(setUp);
				break;
		}
		
		var popupId = null;
		if(setUp.url){
			popupId = $scope.popUps.length - 1;
			var element = $("popups");
			var template = '<div data-popup id="' + setUp.ctr + '" ng-include="\'' + setUp.url + '\'" ng-controller="' + setUp.ctr + '" ng-init="passParams(popUps[' + ($scope.popUps.length - 1) + '].config)"></div>';
			var cTemplate = $compile(template)($scope);
			element.append(cTemplate);
		}
		
		if(!params.doNotHidePageScroll){
			if(!$rootScope.scrollbarHidden){
				if ($("body").height() > $(window).height()) { //Hide scrollbars only if there are scrollbars
					//Hide scrollbars
					$rootScope.scrollbarHidden = true;
					document.documentElement.style.height = "100%";
					document.documentElement.style.overflow = "hidden";
					var scrollWidth = getScrollBarWidth();
					$("body").css("padding-right", (parseFloat($("body").css("padding-right")) + scrollWidth) + "px");
					if($("#header").length){
						$("#header").css("transition-duration", "0s");
						$("#header > div > div").each(function(index, el){
							$(el).css("padding-right", parseFloat($(el).css("padding-right")) + scrollWidth + "px");
						});
					}
					if($("#footer").length){
						$("#footer").css("width", "calc(100% + " + scrollWidth + "px)");
						$("#footer").css("padding-right", parseFloat($("#footer").css("padding-right")) + scrollWidth + "px");
					}
				}
			}
		}
		
		return popupId;
	}
	
	$rootScope.shouldCloseCopyOpPopup = function(event){
		var doClose = true;
		
		if (event.type == 'mousedown' ) {
			var isOutsidePopup = true;
			var el = event.relatedTarget || event.toElement || event.fromElement || event.target;
			while (el) {
				if (el.hasAttribute && el.hasAttribute('data-is-popup-holder')) {
					isOutsidePopup = false;
					break;
				}
				el = el.parentNode;
			}
			if (!isOutsidePopup) {
				doClose = false; //Clicked inside the popup, do not close
			}else{
				event.preventDefault(); //Prevent a bug that selects the document on click outside the popup
			}
		} else if (event.type == 'keydown') {
			if (event.keyCode != 27) { //Only 'escape' closes the popup
				doClose = false;
			}
		}
		
		return doClose;
	}
	
	$rootScope.closeCopyOpPopup = function(params) {
		var doClose = true;
		
		if (params && params.event) {
			/*if (params.event.type == 'mousedown' ) {
				var isOutsidePopup = true;
				var el = params.event.relatedTarget || params.event.toElement || params.event.fromElement || params.event.target;
				while (el) {
					if (el.hasAttribute && el.hasAttribute('data-is-popup-holder')) {
						isOutsidePopup = false;
						break;
					}
					el = el.parentNode;
				}
				if (!isOutsidePopup) {
					doClose = false; //Clicked inside the popup, do not close
				}else{
					params.event.preventDefault(); //Prevent a bug that selects the document on click outside the popup
				}
			} else if (params.event.type == 'keydown') {
				if (params.event.keyCode != 27) { //Only 'escape' closes the popup
					doClose = false;
				}
			}*/
			doClose = $rootScope.shouldCloseCopyOpPopup(params.event);
		}
		
		if (doClose) {
			if (params && params.all) {
				$scope.popUps = [];
			} else {
				if(params && !isUndefined(params.popupId)){
					$scope.popUps.splice(params.popupId, 1);
				}else{
					$scope.popUps.pop();
				}
			}
			
			var childCount = 0;
			var element = $("popups");
			if(params && !isUndefined(params.popupId)){
				element.children().each(function(index, el){
					if(typeof $(el).attr("data-popup") != "undefined"){
						childCount++;
						if(params.popupId == (childCount - 1)){
							angular.element(el).scope().$destroy(); //Destroy the scope of the element being removed to prevent memory leaks
							angular.element(el).remove();
						}
					}
				});
			}else{
				element.children().each(function(index, el){
					if(typeof $(el).attr("data-popup") != "undefined"){
						childCount++;
						if(childCount > $scope.popUps.length){
							angular.element(el).scope().$destroy(); //Destroy the scope of the element being removed to prevent memory leaks
							angular.element(el).remove();
						}
					}
				});
			}
			if($scope.popUps.length == 0){
				element.empty();
			}
			
			if ($scope.popUps.length == 0) {
				if($rootScope.scrollbarHidden){
					//Show scrollbars
					$rootScope.scrollbarHidden = false;
					document.documentElement.style.height = "100%";
					document.documentElement.style.overflow = "auto";
					var scrollWidth = getScrollBarWidth();
					$("body").css("padding-right", (parseFloat($("body").css("padding-right")) - scrollWidth) + "px");
					if($("#header").length){
						$("#header > div > div").each(function(index, el){
							$(el).css("padding-right", parseFloat($(el).css("padding-right")) - scrollWidth + "px");
						});
						setTimeout(function(){$("#header").css("transition-duration", "");}, 0);
					}
					if($("#footer").length){
						$("#footer").css("width", "100%");
						$("#footer").css("padding-right", parseFloat($("#footer").css("padding-right")) - 17 + "px");
					}
				}
			}
			
			if (params && !params.all) {
				$rootScope.checkForWoalktrough();
			}
		}
		
		return doClose;
	}

	//get list of all users i'm copying (loged user)
	$scope.getCopyopCopying = function() {
		if (!isUndefined($rootScope.copyopUser.errorCode)) {
			if ($rootScope.copyopCopying.length == 0 && !$rootScope.getCopyopCopyingRequested) {
				$scope.initRequestStatus.getCopyopCopying = false;
				$rootScope.getCopyopCopyingRequested = true;
				$http.post(settings.jsonLink + 'getCopyopCopying', getProfileMethodRequest({userId: $rootScope.copyopUser.userId}))
					.success(function(data, status, headers, config) {
						if (!handleErrors(data, 'getCopyopCopying')) {
							$rootScope.copyopCopying = data.copyList;
							for (var i = 0; i < data.copyList.length; i++) {
								if (isUndefined($rootScope.usersInfo[data.copyList[i].userId])) {
									$rootScope.usersInfo[data.copyList[i].userId] = {
										name: data.copyList[i].nickname,
										frozen: data.copyList[i].frozen
									};
								}
								$rootScope.usersInfo[data.copyList[i].userId].copying = true;
							}
							$scope.initRequestStatus.getCopyopCopying = true;
						}
					})
					.error(function(data, status, headers, config) {
						handleNetworkError(data);
					});
			}
		} else {
			$scope.initRequestStatus.getCopyopCopying = false;
			setTimeout(function() {
				$scope.getCopyopCopying();
			},50);
		}
	}

	$scope.getCopyopWatching = function() {
		if (!isUndefined($rootScope.copyopUser.errorCode)) {
			if ($rootScope.copyopWatching.length == 0 && !$rootScope.getCopyopWatchingRequested) {
				$scope.initRequestStatus.getCopyopWatching = false;
				$rootScope.getCopyopWatchingRequested = true;
				$http.post(settings.jsonLink + 'getCopyopWatching', getProfileMethodRequest({userId: $rootScope.copyopUser.userId}))
					.success(function(data, status, headers, config) {
						if (!handleErrors(data, 'getCopyopWatching')) {
							$scope.setWatchList(data);
							
							$scope.initRequestStatus.getCopyopWatching = true;
						}
					})
					.error(function(data, status, headers, config) {
						handleNetworkError(data);
					});
			}
		} else {
			$scope.initRequestStatus.getCopyopWatching = false;
			setTimeout(function() {
				$scope.getCopyopWatching();
			},50);
		}
	}
	
	$scope.setWatchList = function(data) {
		$rootScope.copyopWatching = data.watchList;
		for (var i = 0; i < data.watchList.length; i++) {
			if (isUndefined($rootScope.usersInfo[data.watchList[i].userId])) {
				$rootScope.usersInfo[data.watchList[i].userId] = {
					name: data.watchList[i].nickname,
					frozen: data.watchList[i].frozen
				};
			}
			$rootScope.usersInfo[data.watchList[i].userId].watching = true;
		}
	}
	
	//news feed map
	$rootScope.feedSwitch = function(feed) {
		var rtn = {};
		var feed_p = feed.properties;
		var userId = feed_p[FMT.PROPERTY_KEY_USER_ID];
		
		if (userId != null) {
			$rootScope.usersInfo[userId] = {
				copying: ((searchJsonKeyInArray($rootScope.copyopCopying, 'userId', userId) > -1) ? true : false),
				watching: ((searchJsonKeyInArray($rootScope.copyopWatching, 'userId', userId) > -1) ? true : false),
				name: feed_p[FMT.PROPERTY_KEY_NICKNAME],
				frozen: feed_p.frozen
			}
		}
		
		var now = new Date();
		
		rtn.data = feed;
		rtn.msgType = feed.msgType;
		rtn.copiers = feed_p[FMT.PROPERTY_KEY_COPIERS];
		rtn.watchers = feed_p[FMT.PROPERTY_KEY_WATCHERS];
		rtn.userId = userId;
		rtn.avatar = feed_p[FMT.PROPERTY_KEY_AVATAR];
		rtn.title = '';
		rtn.action = feed_p[FMT.PROPERTY_KEY_NICKNAME];
		rtn.nickname = feed_p[FMT.PROPERTY_KEY_NICKNAME];
		rtn.frozen = feed_p.frozen;
		rtn.opportunityId =  feed_p[FMT.PROPERTY_KEY_OPPORTUNITY_ID];
		var date = parseInt(feed_p[FMT.PROPERTY_KEY_TIME_CREATED]);
		if (isNaN(date)) {
			date = adjustFromUTCToLocal(new Date(feed.timeCreated)).getTime() - settings.serverOffsetMillis;// Nov 20, 2014 9:00:46 AM
		} else {
			date = date;
		}
		rtn.timeCreated = $scope.formatDateNow(date);
		
		rtn.copyBtn = rtn.watchBtn = true;
		rtn.followBtn = rtn.depositBtn = rtn.coinsBtn = false;
		rtn.followBtnDisabled = false;
		if (typeof feed_p[FMT.PROPERTY_KEY_TIME_LAST_INVEST] != 'undefined') {
			rtn[FMT.PROPERTY_KEY_TIME_LAST_INVEST] = feed_p[FMT.PROPERTY_KEY_TIME_LAST_INVEST];
			if (feed_p[FMT.PROPERTY_KEY_TIME_LAST_INVEST] <= now.getTime()) {
				rtn.followBtnDisabled = true;
			}
		}
		
		var asset = '';
		if (typeof feed_p[FMT.PROPERTY_KEY_MARKET_ID] != 'undefined') {
			if (typeof $scope.markets[feed_p[FMT.PROPERTY_KEY_MARKET_ID]] != 'undefined') {
				asset = $scope.markets[feed_p[FMT.PROPERTY_KEY_MARKET_ID]].displayName;
			}
		}
		
		switch (parseInt(feed.msgType)) {
			case 1: //facebook New Friend
				rtn.action = feed_p[FMT.PROPERTY_KEY_FIRST_NAME] + " " + feed_p[FMT.PROPERTY_KEY_LAST_NAME];
				rtn.body = $rootScope.getMsgs('msgType_' + feed.msgType + '_body');
				break;
			case 2: //facebook's New Watching
			case 3: //facebook's New Copying
			case 12: //Copying New Copying
				var msgType = (feed.msgType == 2) ? 2 : 3;
				rtn.body = $rootScope.getMsgs('msgType_' + msgType + '_body', {
						destNickname: feed_p[FMT.PROPERTY_KEY_DEST_NICKNAME],
						destUserId: feed_p[FMT.PROPERTY_KEY_DEST_USER_ID],
						ln: '"' + $scope.skinLanguage + '"'
					});
				break;
			case 4: //facebook's New Inv
			case 7: //Watching New Inv
			case 19: //Any Trader New Inv
				rtn.body = $rootScope.getMsgs('msgType_4_body', {
						direction: ((feed_p[FMT.PROPERTY_KEY_DIRECTION] == 1) ? $rootScope.getMsgs('call') : $rootScope.getMsgs('put')),
						assetName: $scope.markets[feed_p[FMT.PROPERTY_KEY_MARKET_ID]].displayName,
						investmentAmount: formatAmount(feed_p[FMT.PROPERTY_KEY_AMOUNT_IN_CURR + settings.defaultCurrencyId], 2),
						level: feed_p[FMT.PROPERTY_KEY_LEVEL],
						marketId: feed_p[FMT.PROPERTY_KEY_MARKET_ID],
						userId: feed_p[FMT.PROPERTY_KEY_USER_ID],
						ln: '"' + $scope.skinLanguage + '"'
					});
					rtn.copyBtn = rtn.watchBtn = false;
					rtn.followBtn = true;
				break;
			case 5: //facebook's Show Off
			case 6: //facebook's any In money Expiry
			case 8: //Watching Show Off
			case 10: //Watching + $100 Expiry
			case 13: //Copying Show Off
			case 20: //Any Trader Show Off
			case 21: //Any Trader In-money Expiry
				var msgType = (feed.msgType == 5 || feed.msgType == 8 || feed.msgType == 13 || feed.msgType == 20) ? 5 : 6;
				rtn.action = $rootScope.getMsgs('msgType_' +  msgType  + '_action', {
						nickName: feed_p[FMT.PROPERTY_KEY_NICKNAME],
						class1: 'showing',
						class2: 'showoff icon'
					});
				rtn.body = $rootScope.getMsgs('msgType_' + msgType + '_body', {
						expiryAmount: formatAmount(feed_p[FMT.PROPERTY_KEY_AMOUNT_IN_CURR + settings.defaultCurrencyId], 2),
						asset: asset,
						expiryTimeStamp: $scope.formatDateNow(feed_p[FMT.PROPERTY_KEY_TIME_SETTLED]),
						marketId: feed_p[FMT.PROPERTY_KEY_MARKET_ID],
						userId: feed_p[FMT.PROPERTY_KEY_USER_ID],
						ln: '"' + $scope.skinLanguage + '"'
					});
				break;
			case 9: //Watching Nearly Full
			case 11: //Watching Now Available
			case 14: //Copied Froze
			case 15: //Last 24h Most Copied
			case 16: //Last 8h Highest hit%
			case 17: //Last 8h Highest Profit
			case 18: //Last 1h Trendiest Trends
			case 28: //I'm Top Profitable
				rtn.action = $rootScope.getMsgs('msgType_' + feed.msgType + '_title', {
						nickName: feed_p[FMT.PROPERTY_KEY_NICKNAME],//15
						hit: feed_p[FMT.PROPERTY_KEY_HIT_RATE],//16
						trend: feed_p[FMT.PROPERTY_KEY_ASSET_TREND]//18
					});
				rtn.body = $rootScope.getMsgs('msgType_' + feed.msgType + '_body', {
						direction: ((feed_p[FMT.PROPERTY_KEY_DIRECTION] == 1) ? $rootScope.getMsgs('call') : $rootScope.getMsgs('put')),
						seatsLeft: feed_p[FMT.PROPERTY_KEY_SEATS_LEFT],//9
						nickName: feed_p[FMT.PROPERTY_KEY_NICKNAME],//9,11,14
						copiedCount: feed_p[FMT.PROPERTY_KEY_COPIED_COUNT],//15
						lastTradinghours: feed_p[FMT.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS],//15,16,17
						hit: feed_p[FMT.PROPERTY_KEY_HIT_RATE],//16
						profit: formatAmount(feed_p[FMT.PROPERTY_KEY_PROFIT + settings.defaultCurrencyId], 2),//16,17,28
						trend: feed_p[FMT.PROPERTY_KEY_ASSET_TREND],//18
						expiryTimeStamp: $scope.formatDateNow(feed_p[FMT.PROPERTY_KEY_OPPORTUNITY_EXPIRE]),//18 
						ranked_in_top: feed_p[FMT.PROPERTY_KEY_RANKED_IN_TOP],//28
						asset: asset,
						marketId: feed_p[FMT.PROPERTY_KEY_MARKET_ID],
						userId: feed_p[FMT.PROPERTY_KEY_USER_ID],
						ln: '"' + $scope.skinLanguage + '"'
					});
				if (feed.msgType == 15 || feed.msgType == 16 || feed.msgType == 17 || feed.msgType == 18 || feed.msgType == 28) {
					rtn.copyBtn = rtn.watchBtn = false;
				}
				if(feed.msgType == 18) {
					rtn.avatar = "img/markets/58/micn"+feed_p[FMT.PROPERTY_KEY_MARKET_ID]+".png";
					if(feed_p[FMT.PROPERTY_KEY_MARKET_ID]){
						rtn.fallbackSrc = "img/markets/58/gicn"+feed_p[FMT.PROPERTY_KEY_MARKET_ID]+".png"
					}else{
						rtn.fallbackSrc = "";
					}
					rtn.followBtn = true;
				}
				break;
			case 23: //Today's Offer
				rtn.title = $rootScope.getMsgs('msgType_' + feed.msgType + '_title');
				rtn.body = feed_p[FMT.PROPERTY_BONUS_DESCRIPTION];
				rtn.copyBtn = rtn.watchBtn = false;
				break;
			case 27: //I'm Fully Copied
				rtn.title = $rootScope.getMsgs('msgType_' + feed.msgType + '_title');
				rtn.body = $rootScope.getMsgs('msgType_' + feed.msgType + '_body');
				rtn.copyBtn = rtn.watchBtn = false;
				break;
			//inbox
			case 22: //Copied \ Watched Changed nickname
			case 24: //My balance less minimum - Warning
			case 25: //My balance less minimum - and i have copier\s
				rtn.title = $rootScope.getMsgs('msgType_' + feed.msgType + '_title', {
					nickName: feed_p[FMT.PROPERTY_KEY_NICKNAME],
					amountOfCopiers: feed_p[FMT.PROPERTY_KEY_COPIERS]
				});
				rtn.copyBtn = rtn.watchBtn = false;
				if (feed.msgType == 24 || feed.msgType == 25) {
					rtn.depositBtn = true;
				}
				break;
			case 26: //My balance less minimum - and i'm a copying other traders
			case 33: //My balance less minimum - and i'm a copying other traders
				rtn.title = $rootScope.getMsgs('msgType_' + feed.msgType + '_title', {
					nickName: feed_p[FMT.PROPERTY_KEY_NICKNAME],
					coinsBalance: feed_p.cpop_coins_balance,
				});
				rtn.body = $rootScope.getMsgs('msgType_' + feed.msgType + '_body');
				rtn.copyBtn = rtn.watchBtn = false;
				if (feed.msgType == 33) {
					rtn.coinsBtn = true;
				}
				break;
				
			//hot
			case 61: //hot traders
			case 611: //hot traders
			case 612: //hot traders
			case 613: //hot traders
			case 62: //hot trades
			case 621: //hot trades
			case 622: //hot trades
			case 623: //hot trades
			case 63: //hot copiers
			case 631: //hot copiers
			case 632: //hot copiers
			case 633: //hot copiers
			case 64: //asset specialist
			case 641: //asset specialist
			case 642: //asset specialist
			case 643: //asset specialist
				rtn.profit = formatAmount(feed_p[FMT.PROPERTY_KEY_PROFIT + settings.defaultCurrencyId], 2, false, true);
				rtn.hitRate = $rootScope.getMsgs('procentFormat', {
						procent: feed_p[FMT.PROPERTY_KEY_HIT_RATE]
					});
				rtn.copiers = feed_p[FMT.PROPERTY_KEY_COPIERS];
				rtn.copying = feed_p[FMT.PROPERTY_KEY_COPYING];
				rtn.nickname = feed_p[FMT.PROPERTY_KEY_NICKNAME];
				rtn.lastTradinghours = feed_p[FMT.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS];
				if (typeof feed_p[FMT.PROPERTY_KEY_MARKET_ID] != 'undefined') {
					rtn.assetName = $scope.markets[feed_p[FMT.PROPERTY_KEY_MARKET_ID]].displayName;
					rtn.marketId = feed_p[FMT.PROPERTY_KEY_MARKET_ID];
				}
				
				if (typeof feed_p[FMT.PROPERTY_KEY_ASSET_CREATE_DATE_TIME] != 'undefined') {
					rtn.assetCreateDateTime = adjustTimeToBrowser(feed_p[FMT.PROPERTY_KEY_ASSET_CREATE_DATE_TIME]);
				}
				break;
		}
		
		if (rtn.userId == $rootScope.copyopUser.user.id) {
			rtn.copyBtn = rtn.watchBtn = false;
		}
		return rtn;
	}

	$rootScope.getSkinCurrencies = function(currencyId) {
		if ($rootScope.skinCurrencies.length == 0) {
			$http.post(settings.jsonLink + 'getSkinCurrencies', getMethodRequest())
				.success(function(data, status, headers, config) {
					data.loginNotCheck = true;
					if (!handleErrors(data, 'getSkinCurrencies')) {
						$rootScope.skinCurrencies = data.currency;
						$rootScope.predefinedDepositAmountMap = data.predefinedDepositAmountMap;
						//$rootScope.copyopUser.user.defaultAmountValue = $rootScope.predefinedDepositAmountMap[$rootScope.copyopUser.user.currency.id];
						var index = searchJsonKeyInArray($scope.skinCurrencies, 'id', currencyId);
						$rootScope.copyopUser.user.currency = $scope.skinCurrencies[index];
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			//$rootScope.copyopUser.user.defaultAmountValue = $rootScope.predefinedDepositAmountMap[$rootScope.copyopUser.user.currency.id];
			var index = searchJsonKeyInArray($scope.skinCurrencies, 'id', currencyId);
			$rootScope.copyopUser.user.currency = $scope.skinCurrencies[index];
		}
	}
	
	$rootScope.goSocial = function() {
		if ($rootScope.copyopUser.copyopProfile.fbId == null || $rootScope.copyopUser.copyopProfile.fbId == '') {
			$rootScope.openCopyOpPopup({type: 'noFacebook'});
		} else {
			$rootScope.openCopyOpPopup({type: 'facebook'});
		}
	}

	//feed tabs toggle
	$scope.feedTabs = function(tab) {
		$rootScope.unread.currentTab = tab;
		switch (tab) {
			case 1:
				$rootScope.unread.news = 0;
				break;
			case 2:
				break;
			case 3:
				$rootScope.unread.explore = 0;
				break;
			default:
				$rootScope.unread.currentTab = 1;
				$rootScope.unread.news = 0;
				break;
		}
	}

	$scope.setCoins = function(data) {
		$rootScope.coins = data;
		var time = Math.round(data.convertRemainingTime / 24);
		if (time < 0) {
			$rootScope.coins.convertRemainingTime = 0;
		} else {
			$rootScope.coins.convertRemainingTime = (time < 1) ? 1 : time;
		}
		$scope.coins.minInvAmountDsp = formatAmount(data.minInvAmount, 0, true);
		$scope.coins._rates = [];
		for (var key in data.rates) {
			$scope.coins._rates.push({
				amount: formatAmount(key, 2, true), 
				coins: data.rates[key]
			});
		}
		$scope.coins._coinsPerAmountInv = [];
		for (var key in data.coinsPerAmountInv) {
			$scope.coins._coinsPerAmountInv.push({
				amount: formatAmount(key, 2, true), 
				coins: data.coinsPerAmountInv[key]
			});
		}
	}

	$scope.ls_updateFollowNg = function(updateInfo) {
		var ET_STATE = updateInfo.getValue('ET_STATE');
		if (updateInfo.getValue('ET_OPP_ID') == $rootScope.followData.data.opportunityId) {
			if (ET_STATE == OPPORTUNITY_STATE.opened || ET_STATE == OPPORTUNITY_STATE.last_10_min) {
				for (i = 0; i < schemaFollow.length; i++) {
					var valueFromUpdate = updateInfo.getValue(schemaFollow[i]);
					if (schemaFollow[i] === SUBSCR_FLD.ao_level) {
						$rootScope.followData.ao_level = valueFromUpdate;
						$rootScope.followData.ao_level_float = parseFloat(valueFromUpdate.replace(/,/g,''));
					}
					else if(schemaFollow[i] === SUBSCR_FLD.ao_clr){
						$rootScope.followData.ao_clr_dsp = valueFromUpdate;
					}
					else if(schemaFollow[i] === SUBSCR_FLD.et_est_close){
						valueFromUpdate = adjustFromUTCToLocal(new Date(valueFromUpdate));
						$rootScope.followData.est_close_formated = $scope.formatDateNow(valueFromUpdate);
					}
					else if (schemaFollow[i] === SUBSCR_FLD.odds_group) {
						var odds_group = valueFromUpdate.trim().split(' ');
						var odds_group_selected = '';
						for (var n = 0; n < odds_group.length; n++) {
							if (odds_group[n].search('d') > -1) {
								odds_group[n] = odds_group[n].replace('d', '');
								odds_group_selected = odds_group[n];
							}
						}
						
						$rootScope.followData.ET_ODDS = returnRefund_all[odds_group_selected].profit + '%'; 
						$rootScope.followData.ET_ODDS_WIN = ((returnRefund_all[odds_group_selected].profit)/100)+1;
						$rootScope.followData.ET_ODDS_LOSE = ((returnRefund_all[odds_group_selected].refund)/100);
						if ($rootScope.defaultAmountValueTemp != null) {
							$rootScope.followData.odds_win = formatAmount($rootScope.defaultAmountValueTemp * parseFloat($rootScope.followData.ET_ODDS_WIN), 0);
							$rootScope.followData.odds_lose = formatAmount($rootScope.defaultAmountValueTemp * parseFloat($rootScope.followData.ET_ODDS_LOSE), 0);
						} else {
							logIt({'type':3,'msg':'Default amount is null'});
						}
					}
					
					$rootScope.followData[schemaFollow[i]] = valueFromUpdate;
				}
				$scope.$apply();
			} else {
				// $rootScope.followData.data
				// $rootScope.closeCopyOpPopup();
				if (followls.isSubscribed()) {
					lsClient.unsubscribe(followls);
				}
			}
		}
	}
	
	$scope.ls_updateAssetsNg = function(updateInfo) {
		var info = {};
		var ET_STATE = updateInfo.getValue('ET_STATE');
		if (ET_STATE == OPPORTUNITY_STATE.opened || ET_STATE == OPPORTUNITY_STATE.last_10_min || ET_STATE == OPPORTUNITY_STATE.closing_1_min || ET_STATE == OPPORTUNITY_STATE.closing) {
			info.active = true;
		} else {
			info.active = false;
		}
		for (i = 0; i < schemaAssets.length; i++) {
			var valueFromUpdate = updateInfo.getValue(schemaAssets[i]);
			if (schemaAssets[i] === SUBSCR_FLD.ao_level) {
				info.ao_level = valueFromUpdate;
				info.ao_level_float = parseFloat(valueFromUpdate.replace(/,/g,''));
			}
			else if(schemaAssets[i] === SUBSCR_FLD.ao_clr){
				info.ao_clr_dsp = valueFromUpdate;
			}
			else if(schemaAssets[i] === SUBSCR_FLD.last_invest){
				valueFromUpdate = new Date(valueFromUpdate);
			}
			info[schemaAssets[i]] = valueFromUpdate;
		}
		$scope.updateAssetInfo(info);
	}
	
	$scope.updateAssetInfo = function(info) {
		for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
			for (var j = 0; j < $scope.marketGroups.groups[i].markets.length; j++) {
				$arrIndex = $.inArray($scope.marketGroups.groups[i].markets[j].id, $rootScope.favMarkets);
				if ($arrIndex > -1) {
					$scope.marketGroups.groups[i].markets[j].showInFav = true;
					$scope.marketGroups.groups[i].markets[j].showInFavOrder = $rootScope.favMarketsOrder[$arrIndex];
				} else {
					$scope.marketGroups.groups[i].markets[j].showInFav = false;
				}
				if ($scope.marketGroups.groups[i].markets[j].id == info.ET_NAME) {
					$scope.marketGroups.groups[i].markets[j].active = info.active;
					
					
					if (info.ao_level_float > 0) {
						$scope.marketGroups.groups[i].markets[j].level = info.ao_level_float;
					}

					for (var n = 0; n < $rootScope.openOptions.length; n++) {
						if ($rootScope.openOptions[n].marketId == $scope.marketGroups.groups[i].markets[j].id) {
							if (($rootScope.openOptions[n].typeId == 1 && $rootScope.openOptions[n].level < info.ao_level_float) ||
								($rootScope.openOptions[n].typeId == 2 && $rootScope.openOptions[n].level > info.ao_level_float)) {
								$rootScope.openOptions[n].amountReturn = $rootScope.openOptions[n].amount * (1 + $rootScope.openOptions[n].oddsWin);
								$rootScope.openOptions[n].amountReturnWF = formatAmount($rootScope.openOptions[n].amountReturn, 0, true);
								$rootScope.openOptions[n].win = true;
							} else {
								$rootScope.openOptions[n].amountReturn = $rootScope.openOptions[n].amount * (1 - $rootScope.openOptions[n].oddsLose);
								$rootScope.openOptions[n].amountReturnWF = formatAmount($rootScope.openOptions[n].amountReturn, 0, true);
								$rootScope.openOptions[n].win = false;
							}
							$rootScope.openOptions[n].expiryLevel = info.ao_level_float;
						}
					}
					
					if (isUndefined($scope.marketGroups.groups[i].markets[j].est_close) || $scope.marketGroups.groups[i].markets[j].est_close > info.ET_LAST_INV.getTime()) {
						$scope.marketGroups.groups[i].markets[j].est_close = info.ET_LAST_INV.getTime();
						clearTimeout($scope.marketGroups.groups[i].markets[j].timer);
						$scope.marketGroups.groups[i].markets[j].timer = setInterval(function() {
							var date = new Date().getTime();
							var allInSec = parseInt((adjustFromUTCToLocal(new Date($scope.marketGroups.groups[i].markets[j].est_close)).getTime() - date - settings.serverOffsetMillis) / 1000);
							if (!$scope.marketGroups.groups[i].markets[j].active) {
								$scope.marketGroups.groups[i].markets[j].timeLeftDap = $rootScope.getMsgs('trading.box.available');
								$scope.marketGroups.groups[i].markets[j].est_close = adjustTimeToBrowser(info[SUBSCR_FLD.et_est_close]);
								$scope.marketGroups.groups[i].markets[j].showInTimeLeft = 2;
							} else if (allInSec > 0) {
								var m = parseInt(allInSec/60);
								var s = allInSec - (m*60);
								s = (s < 10) ? '0' + s : s;
								$scope.marketGroups.groups[i].markets[j].timeLeft = m + ":" + s;
								$scope.marketGroups.groups[i].markets[j].last10Color = (m < 10) ? 'last-10-Color' : '';
								$scope.marketGroups.groups[i].markets[j].waiting = false;
								$scope.marketGroups.groups[i].markets[j].timeLeftDap = $rootScope.getMsgs('time-left');
								$scope.marketGroups.groups[i].markets[j].showInTimeLeft = 1;
								$scope.$apply();
							} else {
								clearInterval($scope.marketGroups.groups[i].markets[j].timer);
								$scope.marketGroups.groups[i].markets[j].timeLeft = '';
								$scope.marketGroups.groups[i].markets[j].waiting = true;
								$scope.marketGroups.groups[i].markets[j].est_close = null;
								$scope.marketGroups.groups[i].markets[j].timeLeftDap = $rootScope.getMsgs('trading.box.waiting');
								$scope.marketGroups.groups[i].markets[j].showInTimeLeft = 3;
							}
						}, 1000);
					}
					$scope.marketGroups.groups[i].markets[j].color = switchColor(info.ao_clr_dsp);
					
					//Update assets in trading section
					var index = searchJsonKeyInArray(markestInBox, 'market_id', $scope.marketGroups.groups[i].markets[j].id);
					if(!info.active && $rootScope.$state.includes('ln.in.a.asset') && index > -1){
						angular.element(g('optionOptionsTab')).scope().getInvestments(settings.optionsLimit);
					}
					
					return;
				}
			}
		}
	}

	$scope.changeSkin = function (id) {
		$scope.countriesList.length = 0;
		
		var CheckUpdateMethodRequest = jsonConcat(getUserMethodRequest(),{
				skinId: id
			});
		
		$http.post(settings.jsonLink + 'skinUpdate', CheckUpdateMethodRequest)
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'skinUpdate')) {
					settings.skinId = id;
					$scope.skinLanguage = settings.skinLanguage = skinMap[id].locale;
					setCookie('skinId', settings.skinId, 365);
					getMsgsJson_first = true;
					$scope.getMsgsJson();
					$scope.getSkinSelector();
					if (!settings.loggedIn) {
						$rootScope.$state.go($rootScope.$state.current.name, {ln: $scope.skinLanguage});
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}

	$scope.formatDateNow = function(date) {
		var rtn = '';
		if (parseInt(date) == date) {
			date = new Date(parseInt(date));
		} else {
			date = new Date(date);
		}
		
		var date_h = date.getHours();
		date_h = (date_h < 10) ? '0' + date_h : date_h;
		
		var date_m = date.getMinutes();
		date_m = (date_m < 10) ? '0' + date_m : date_m;
		
		var today = new Date();
		var yesterday = new Date(today);
		yesterday.setDate(today.getDate() - 1);
		
		if (date.getUTCFullYear() == today.getUTCFullYear() 
			&& date.getUTCMonth() == today.getUTCMonth()
			&& date.getUTCDate() == today.getUTCDate()) {
			rtn = $rootScope.getMsgs('time_today', {time: date_h + ":" + date_m});
		} else if (yesterday.getUTCFullYear() == yesterday.getUTCFullYear() 
			&& date.getUTCMonth() == yesterday.getUTCMonth()
			&& date.getUTCDate() == yesterday.getUTCDate()) {
			rtn = $rootScope.getMsgs('yesterday_today', {time: date_h + ":" + date_m});
		} else {
			var tmp = '';
			var day = date.getDate();
			tmp += ((day < 10) ? '0' + day : day);
			
			var month = date.getMonth()+1;
			tmp += "/" + ((month < 10) ? '0' + month : month);
			
			if (date.getUTCFullYear() != today.getUTCFullYear()) {
				tmp += '/' + date.getYear();
			}
			
			tmp += " " + date_h + ":" + date_m;
			rtn = tmp;
		}
		return rtn;
	}

	$scope.shereCopyOp = function(entry) {
		if (entry.msgType == 23 && entry.data.properties.cpop_bonus_type_id == 0) {
			FB.ui({
				method: 'share',
				href: settings.domain,
			}, function(response){
				if (typeof response.error_code == 'undefined' || response.error_code == 0) {
					$http.post(settings.jsonLink + 'shareCopyop', getUserMethodRequest())
						.success(function(data, status, headers, config) {
							if (!handleErrors(data, 'shareCopyop')) {
								entry.hide = true;
							}
						})
						.error(function(data, status, headers, config) {
							handleNetworkError(data);
						})
				}
			});
		}
	}

	$scope.lsWebPopupClose = function(popupId) {
		$rootScope.closeCopyOpPopup({popupId: popupId});
		$rootScope.copyOpShoppingBag = [];
		clearTimeout(lsWebPopupUpdateTimer);
		lsWebPopupUpdateTimer = null;
		$rootScope.$apply();
	}
	
	//change languages
	$scope.skinSelector = [];
	$scope.getSkinSelector = function() {
		if ($scope.initRequestStatus.getMsgsJson) {
			if (isUndefined($rootScope.visibleSkins)) {
				$http.post(settings.jsonLink + 'getVisibleSkins', getMethodRequest())
					.success(function(data, status, headers, config) {
						$rootScope.visibleSkins = data.visibleSkins;
						
						var skinId = settings.skinId;
						if (!skinMap[skinId].skinSelector) {
							skinId = settings.defaultLanguage;
						}
						setTimeout(function() {
							$scope.selectedLanguge = $rootScope.getMsgs('header.' + skinMap[skinMap[skinId].skinIdTexts].locale);
							$scope.selectedLangugeFlag = skinMap[skinId].locale;
							$scope.$apply();
						}, 50)
						
						$scope.setSkinSelector();
					})
					.error(function(data, status, headers, config) {
						handleNetworkError(data);
					});
			} else {
				var skinId = settings.skinId;
				if (!skinMap[skinId].skinSelector) {
					skinId = settings.defaultLanguage;
				}
				setTimeout(function() {
					$scope.selectedLanguge = $rootScope.getMsgs('header.' + skinMap[skinMap[skinId].skinIdTexts].locale);
					$scope.selectedLangugeFlag = skinMap[skinId].locale;
					$scope.$apply();
				},50)
				
				$scope.setSkinSelector();
			}
		} else {
			setTimeout(function() {
				$scope.getSkinSelector();
			}, 50);
		}
	}
	
	$scope.setSkinSelector = function() {
		$scope.skinSelector = [];
		for (var i = 0; i < $rootScope.visibleSkins.length; i++) {
			if ($rootScope.visibleSkins[i].skinId != settings.skinId && skinMap[$rootScope.visibleSkins[i].skinId].skinSelector) {
				$scope.skinSelector.push({
					title: $rootScope.getMsgs('header.' + skinMap[$rootScope.visibleSkins[i].skinId].locale),
					flag: skinMap[$rootScope.visibleSkins[i].skinId].locale,
					id: $rootScope.visibleSkins[i].skinId
				})
			}else{
				$rootScope.supportPhone = $rootScope.visibleSkins[i].skinSupportPhone;
				$rootScope.supportEmail = $rootScope.visibleSkins[i].skinSupportEmail;
			}
		}
	}
	
	$scope.msgs = {};
	var getMsgsJson_first = true;
	//get all translations (bundle)
	$scope.getMsgsJson = function() {
		$scope.initRequestStatus.getMsgsJson = false;
		//skinMap[skinMap[settings.skinId].skinIdTexts].locale - seems legit :D
		var msgsUrl = settings.msgsPath + settings.msgsFileName + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ((getMsgsJson_first)?"":settings.msgsEUsufix) + settings.msgsExtension;
		$http.get(msgsUrl)
			.success(function(data, status) {
				if (typeof data == 'object') {
					if (getMsgsJson_first) {
						$scope.msgs = data;
					} else {
						$scope.msgs = jsonConcat($scope.msgs, data);
						if (!settings.loggedIn) {
							$scope.initRequestStatus.getMsgsJson = true;
						}
					}
					if (settings.isRegulated && getMsgsJson_first) {
						getMsgsJson_first = false;
						$scope.getMsgsJson();
					} else {
						$scope.initRequestStatus.getMsgsJson = true;
					}
					oldTexts.bundle_msg_today = $scope.msgs['investment.time.today'];//TO DO remove
					oldTexts.day_to_invest = $scope.msgs['day_to_invest'];
					oldTexts.days_to_invest = $scope.msgs['days_to_invest'];
					oldTexts.hours_to_invest = $scope.msgs['hours_to_invest'];
					oldTexts.min_to_invest = $scope.msgs['min_to_invest'];
					
					if (!isUndefined($rootScope.$state.current.metadataKey)) {
						$rootScope.metadataTitle = $rootScope.$state.current.metadataKey;
					} else {
						$rootScope.metadataTitle = 'index';
					}
				}
			})
			.error(function( data, status ) {
				handleNetworkError(data);
			});
	}

	//get all markets
	$scope.getMarketGroups = function() {
		// if (isUndefined($scope.markets)) {
			$scope.initRequestStatus.getMarketGroups = false;
			$http.post(settings.jsonLink + 'getMarketGroups', getMethodRequest())
				.success(function(data, status) {
					$scope.marketGroups = data;
					$scope.markets = [];
					$scope.marketsObj = [];
					for (var i = 0; i < $scope.marketGroups.groups.length; i++) {
						$scope.marketGroups.groups[i].index = i;
						$scope.marketGroups.groups[i].filterInactive = {active: true};
						$scope.marketGroups.groups[i].showInactiveStates = false;
						for (var j = 0; j < $scope.marketGroups.groups[i].markets.length; j++) {
							$scope.marketGroups.groups[i].markets[j].index = j;
							$scope.markets[$scope.marketGroups.groups[i].markets[j].id] = $scope.marketGroups.groups[i].markets[j];
							$scope.marketsObj.push($scope.marketGroups.groups[i].markets[j]);
						}
					}
					// $scope.$apply();
					$scope.initRequestStatus.getMarketGroups = true;
				})
				.error(function( data, status ) {
					handleNetworkError(data);
				});
		// }
	}
	
	//get info about each market
	$scope.assetIndexMarkets = [];
	$scope.getAssetIndexMarketsPerSkin = function() {
		if ($scope.assetIndexMarkets.length == 0) {
			$scope.lastConfig = {
				groupName: '',
				displayGroupNameKey: '',
				opportunityTypeId: ''
			};
			$scope.initRequestStatus.getAssetIndexMarketsPerSkin = false;
			$http.post(settings.commonServiceLink + 'AssetIndexServices/getAssetIndexMarketsPerSkin', getMethodRequest())
				.success(function(data, status) {
					$scope.assetIndexGeneral = {
						tooltipData: data.tooltipData,
						tooltipDataUnderTable: data.tooltipDataUnderTable,
						assetIndexFormulasBySkin: data.assetIndexFormulasBySkin
					}
					
					var i = 0;
					$.each(data.marketList, function(k, v) {
						if (v.opportunityTypeId == 1) {
							if ($scope.lastConfig.groupName != v.market.groupName) {
								if (!isUndefined(v.market.groupName)) {
									$scope.assetIndexMarkets.push({
										title: $rootScope.getMsgs(v.market.groupName),
										type: 1
									});
									$scope.lastConfig.groupName = v.market.groupName;
								}
							}
							
							if ($scope.lastConfig.displayGroupNameKey != v.market.displayGroupNameKey) {
								if (!isUndefined(v.market.displayGroupNameKey)) {
									$scope.assetIndexMarkets.push({
										title: $rootScope.getMsgs(v.market.displayGroupNameKey),
										type: 2
									});
									$scope.lastConfig.displayGroupNameKey = v.market.displayGroupNameKey;
								}
							}
						} else {
							if ($scope.lastConfig.opportunityTypeId != v.opportunityTypeId) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs('type-name-' + v.opportunityTypeId),
									type: 1
								});
								$scope.lastConfig.opportunityTypeId = v.opportunityTypeId;
							}
						}
						
						var temp = {
							type: 3,
							opportunityType: v.opportunityTypeId,
							id: v.market.id,
							title: v.market.displayName,
							description: v.marketAssetIndexInfo.marketDescription,
							additionalText: v.marketAssetIndexInfo.additionalText,
							symbol: v.market.feedName,
							tradingTime: '',
							newMarket: v.market.newMarket,
							is24h7: v.marketAssetIndexInfo.is24h7,
							formulas: []
						}

						var startDay = -1;
						var endDay = -1;
						for (var d = 0; d < 7; d++) {
							if (v.marketAssetIndexInfo.tradingDays[d] == 1 && startDay == -1) {
								startDay = d;
							}
							if (v.marketAssetIndexInfo.tradingDays[d] == 1) {
								endDay = d;
							}
						}
						
						var orgStartDate = new Date(v.marketAssetIndexInfo.startTime);
						var startDate = new Date(new Date(orgStartDate.setDate(orgStartDate.getDate() - orgStartDate.getDay() + startDay)).getTime() - (orgStartDate.getTimezoneOffset() * 60 * 1000));//crazy shit
						
						var orgEndDate = new Date(v.marketAssetIndexInfo.endTime);
						var endDate = new Date(new Date(orgEndDate.setDate(orgEndDate.getDate() - orgEndDate.getDay() + endDay)).getTime() - (orgEndDate.getTimezoneOffset() * 60 * 1000));//crazy shit
					
						temp.tradingTime += $rootScope.getMsgs('day-' + startDate.getDay()) + ' - ' + $rootScope.getMsgs('day-' + endDate.getDay());
						temp.tradingTime += ' ' + startDate.getHours() + ':' + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()) + '-' + endDate.getHours() + ':' + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());
						
						for (var t = 0; t < v.marketAssetIndexInfo.expiryFormulaCalculations.length; t++) {
							if (t == 0) {
								temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
								temp.formulas[temp.formulas.length - 1].sameNextRow = false;
								temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
							} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 2) {
								temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
								temp.formulas[temp.formulas.length - 1].sameNextRow = false;
								if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId == v.marketAssetIndexInfo.expiryFormulaCalculations[0].expiryFormulaId) {
									temp.formulas[temp.formulas.length - 1].sameNextRow = true;
									temp.formulas[0].sameNextRow = true;
								}
								temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
							} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 3) {
								temp.formulas[temp.formulas.length - 1].expiryTypeId = 3;
							}
						}
						
						$scope.assetIndexMarkets.push(temp);
						i++;
					});
					
					$scope.initRequestStatus.getAssetIndexMarketsPerSkin = true;
					
				})
				.error(function( data, status ) {
					$rootScope.handleNetworkError(data);
				});
		}
	}
	
	$scope.checkInitStatus = function() {
		for (var key in $scope.initRequestStatus) {
			if (!$scope.initRequestStatus[key]) {
				setTimeout(function() {
					$scope.checkInitStatus();
				}, 50);
				return;
			}
		}
		$rootScope.ready = $scope.ready = true;
		$rootScope.$apply();
	}
	
	$scope.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
	$scope.initRequestStatus = {}
	$scope.initRequests = function() {
		$rootScope.ready = false;
		$scope.getTrackingParams();
		$scope.getMsgsJson();
		$scope.getMarketGroups();
		$scope.getSkinSelector();
		
		if (settings.loggedIn) {
			$scope.getAllBinaryMarkets();
			$scope.getMarketsNames();
			$scope.getAllReturnOdds();
			$scope.getCopyopCopying();
			$scope.getCopyopWatching();
			$scope.getAssetIndexMarketsPerSkin();
		}
		startClock();
		
		$scope.checkInitStatus();
	}
	
	$rootScope.feedFollowCheck = function(feedName) {
		var now = new Date();
		for (var i = 0; i < $rootScope[feedName].length; i++) {
			if (!isUndefined($rootScope[feedName][i][FMT.PROPERTY_KEY_TIME_LAST_INVEST])) {
				if ($rootScope[feedName][i][FMT.PROPERTY_KEY_TIME_LAST_INVEST] <= now.getTime()) {
					$rootScope[feedName][i].followBtnDisabled = true;
				}
			}
		}
	}
	
	$scope.checkError = function(_form, field, error) {
		return (_form[field]) && ((_form.$submitted && _form[field].$error[error]) || (_form[field].$touched && _form[field].$dirty && _form[field].$error[error]));
	}
	$scope.checkRequired = function(_form, fields) {
		if (_form && fields) {
			for (var i = 0; i < fields.length; i++) {
				if (_form[fields[i]] && $scope.checkError(_form, fields[i], 'required')) {
					return true;
				}
			}
		}
		return false;
	}
	
	$rootScope.addErrorClass = function(_form, field) {
		return (_form[field]) && ((_form[field].$invalid && _form[field].$dirty && _form[field].$touched) || (_form.$submitted && _form[field].$invalid));
	}
	
	$scope.getLastLoginSummary = function() {
		$http.post(settings.jsonLink + 'getLastLoginSummary', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getLastLoginSummary')) {
					$scope.lastLoginSummary = {
						traders: []
					};
					if (!isUndefined(data.topLogTraders)) {
						$scope.lastLoginSummary.header = (data.topLogTraders[0].isAfterLastLogOff) ? $rootScope.getMsgs('popUp-summary-message.txt1') : $rootScope.getMsgs('popUp-summary-message.txt2');
						$scope.lastLoginSummary.header = $rootScope.getMsgs('popUp-summary-message.header',{time_period: $scope.lastLoginSummary.header});
						for (var i = 0; i < data.topLogTraders.length; i++) {
							if (data.topLogTraders[i].profit > 0) {
								data.topLogTraders[i].profitTxt = $rootScope.getMsgs('gain');
								data.topLogTraders[i].classType = 1;
								data.topLogTraders[i].profit = '+' + formatAmount(data.topLogTraders[i].profit, 0);
							} else {
								data.topLogTraders[i].profitTxt = $rootScope.getMsgs('lost');
								data.topLogTraders[i].profit = formatAmount(data.topLogTraders[i].profit, 0);
								data.topLogTraders[i].classType = 0;
								if (data.topLogTraders[i].profit == 0) {
									data.topLogTraders[i].classType = 2;
								}
							}
							if (data.topLogTraders[i].userId == 0) {
								$scope.lastLoginSummary.total = data.topLogTraders[i];
							} else if (data.topLogTraders[i].userId == 1) {
								$scope.lastLoginSummary.others = data.topLogTraders[i];
							} else {
								if (isUndefined($rootScope.usersInfo[data.topLogTraders[i].userId])) {
									$rootScope.usersInfo[data.topLogTraders[i].userId] = {
										copying: !data.topLogTraders[i].isFrozen,
										watching: false,
										name: data.topLogTraders[i].nickname,
										frozen: data.topLogTraders[i].isFrozen
									}
								}
								$scope.lastLoginSummary.traders.push(data.topLogTraders[i]);
							}
						}
						$rootScope.openCopyOpPopup({type: 'lastLoginSummary'});
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	var params = '?pid='+settings.utm_source+'&c='+settings.campaginid+'&af_siteid='+settings.combinationId+'&af_sub1='+settings.dynamicParam+'&af_sub2='+settings.utm_medium+'&af_sub3='+settings.utm_content;
	$scope.appsflyerios = settings.appsflyerios + params;
	$scope.appsflyerandroid = settings.appsflyerandroid + params;
	
	$scope.getMonths = function() {
		if (isUndefined($scope.monthsDsp)) {
			$scope.monthsDsp = [];
			for (var i = 1; i <= 12; i++) {
				$scope.monthsDsp.push({
					id: (i < 10) ? '0' + i : i,
					displayName: $rootScope.getMsgs('month-' + i)
				});
			}
		}
		return $scope.monthsDsp;
	}

	$scope.hasDirect24 = function() {
		var countryIds = [80,15,201,145];
		if (settings.skinId != 13 && settings.skinId != 14 && settings.isRegulated && $rootScope.copyopUser.userRegulation.isWcApproval
			&& settings.defaultCurrencyId == currencyConst.eur && countryIds.indexOf($rootScope.copyopUser.user.countryId) > -1) {
			return true;
		} else {
			return false;
		}
	}
	$scope.hasGiropay = function() {
		if (settings.skinId != 13 && settings.skinId != 14 
			&& settings.defaultCurrencyId == currencyConst.eur && $rootScope.copyopUser.user.countryId == 80) {
			return true;
		} else {
			return false;
		}
	}
	$scope.hasEps = function() {
		if (settings.skinId != 13 && settings.skinId != 14 && settings.isRegulated && $rootScope.copyopUser.userRegulation.isWcApproval
			&& settings.defaultCurrencyId == currencyConst.eur && $rootScope.copyopUser.user.countryId == 15) {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.openQuestionary = function() {
		switch ($rootScope.copyopUser.userRegulation.regulationVersion) {
			case 1: 
				$rootScope.showQuestionary = true;
				$rootScope.$state.go('ln.in.a.personal.my-account.personal-details', {ln: $scope.skinLanguage});
				break;
			case 2:
				var isOpened = false;
				for(var i = 0; i < $scope.popUps.length; i++){
					if($scope.popUps[i].config.type == "questionnaireRegulation"){
						isOpened = true;
					}
				}
				if(!isOpened){
					$rootScope.openCopyOpPopup({type: 'questionnaireRegulation'})
				}
				break;
		}
	}
	
	$scope.stateMachine = function(params) {//return true if no errors
		if($rootScope.copyopUser.user.isNeedChangePassword){
			$rootScope.openCopyOpPopup({type: 'changePassword'});
		}else if ($rootScope.copyopUser.user.firstDepositId == 0 && $rootScope.copyopUser.userRegulation.approvedRegulationStep < 2) {//if there is no first deposit
			setTimeout(function(){
				$rootScope.openCopyOpPopup({type: 'deposit'});
			},500);
			return false;
		} else if(settings.isRegulated && $rootScope.copyopUser.userRegulation.approvedRegulationStep < 3 && $rootScope.copyopUser.userRegulation.suspendedReasonId == 0) {//if no questionnaire is completed
			$scope.openQuestionary();
			return false;
		} else if($rootScope.copyopUser.user.countryId == countries.spain && !$rootScope.copyopUser.userRegulation.isKnowledgeQuestion && ($scope.isRestricted() || $scope.isQuestionaireIncorrect())) {//suspended due to cnmv
			$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.regulation_user_restricted, txtStatus: 'suspended-cnmv'});
			return false;
		} else if($scope.isSuspendedDueCnmv()) {//suspended due to cnmv - require documents
			$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.regulation_suspended_cnmv_docs, txtStatus: 'suspended-cnmv-docs'});
			return false;
		} else if ($scope.isRestricted() || $scope.isQuestionaireIncorrect()) {//if user did not answer correctly to all questions (304)
			$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.regulation_user_restricted, txtStatus: 'restricted'});
			return false;
//		} else if ($scope.isQuestionaireIncorrect()) {//if user failed questions Questionaire (305) - now acts like restricted
//			if ((!isUndefined(params.stateMachine) && params.stateMachine) || isUndefined(params.stateMachine)) {
//				$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.reg_suspended_quest_incorrect, txtStatus: 'blocked'});
//				return false;
//			}
//			return true;
		} else if (($scope.isQuestionairePEP() && isUndefined(params.errorCode)) || (!isUndefined(params.errorCode) && params.errorCode == errorCodeMap.regulation_user_pep_prohibited)) {//PEP (307)
			if ((!isUndefined(params.stateMachine) && params.stateMachine) || isUndefined(params.stateMachine)) {
				$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.reg_suspended_quest_incorrect, txtStatus: 'blocked-pep'});
				return false;
			}
		} else if (($scope.isTresholdBlock() && isUndefined(params.errorCode)) || (!isUndefined(params.errorCode) && params.errorCode == errorCodeMap.regulation_user_is_treshold_block)) {//REGULATION_USER_IS_TRESHOLD_BLOCK (308)
			if ((!isUndefined(params.stateMachine) && params.stateMachine) || isUndefined(params.stateMachine)) {
				$rootScope.openCopyOpPopup({type: 'regulationStatus', errorCode: errorCodeMap.regulation_user_is_treshold_block, txtStatus: 'restricted-loss'});
				return false;
			}
		} else if ($scope.isSuspendedDueDocuments()) {//if there is no documents uploaded or over 5000 (302)
			if ((!isUndefined(params.stateMachine) && params.stateMachine) || isUndefined(params.stateMachine)) {
				$rootScope.openCopyOpPopup({type: 'regulationDocuments', popupState: 1});
			}
			return false;
		} else if ($scope.showWarningDueDocuments()) {//if deposit is between 3000 and 5000
			if ((!isUndefined(params.stateMachine) && params.stateMachine) || isUndefined(params.stateMachine)) {
				$rootScope.openCopyOpPopup({type: 'regulationDocuments', popupState: 0});
			}
			return false;
		} else if (!isUndefined(params.fromLogin) && params.fromLogin) {
			$scope.showWithdrawUserDetails();
		}
		return true;
	}
	$scope.isRestricted = function() {
		return $rootScope.copyopUser.userRegulation.scoreGroup != null
			&& ($rootScope.copyopUser.userRegulation.scoreGroup == userRegulationBase.ao_regulation_restricted_score_group_id || 
				$rootScope.copyopUser.userRegulation.scoreGroup == userRegulationBase.ao_regulation_restricted_low_x_score_group_id ||
				$rootScope.copyopUser.userRegulation.scoreGroup == userRegulationBase.ao_regulation_restricted_high_y_score_group_id ||
				$rootScope.copyopUser.userRegulation.scoreGroup == userRegulationBase.ao_regulation_restricted_failed_q_score_group_id				) 
			&& !$rootScope.copyopUser.userRegulation.isKnowledgeQuestion;
	}
	$scope.isQuestionaireIncorrect = function() {
		return $rootScope.copyopUser.userRegulation.suspendedReasonId != null
			&& $rootScope.copyopUser.userRegulation.suspendedReasonId == userRegulationBase.suspended_mandatory_questionnaire_filled_incorrect;
	}
	$scope.isQuestionairePEP = function() {
		return $rootScope.copyopUser.userRegulation.pepState != null
			&& ($rootScope.copyopUser.userRegulation.pepState == userRegulationBase.auto_prohibited || $rootScope.copyopUser.userRegulation.pepState == userRegulationBase.writer_prohibited);
	}
	$scope.isTresholdBlock = function() {
		return ($rootScope.copyopUser.userRegulation.scoreGroup != null 
				&& $rootScope.copyopUser.userRegulation.thresholdBlock != null)
				&& ($rootScope.copyopUser.userRegulation.thresholdBlock > 0);
	}
	$scope.isSuspendedDueDocuments = function() {
		return !isUndefined($rootScope.copyopUser.userRegulation) && $rootScope.copyopUser.userRegulation.suspendedReasonId != null 
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep != null
				&& $rootScope.copyopUser.userRegulation.suspendedReasonId == userRegulationBase.suspended_due_documents
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep < userRegulationBase.regulation_control_approved_user;
	}
	$scope.showWarningDueDocuments = function() {
		return $rootScope.copyopUser.userRegulation.approvedRegulationStep != null
				&& !$rootScope.copyopUser.userRegulation.isQualified 
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep >= userRegulationBase.regulation_mandatory_questionnaire_done
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep < userRegulationBase.regulation_control_approved_user;
	}
	$scope.isSuspendedDueCnmv = function() {
		return !isUndefined($rootScope.copyopUser.userRegulation) && $rootScope.copyopUser.userRegulation.suspendedReasonId != null 
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep != null
				&& $rootScope.copyopUser.user.countryId == countries.spain
				&& $rootScope.copyopUser.userRegulation.suspendedReasonId == userRegulationBase.suspended_due_cnmv
				&& $rootScope.copyopUser.userRegulation.approvedRegulationStep < userRegulationBase.regulation_control_approved_user;
	}

	$scope.checkDirectDeposit = function() {
		var txid = getUrlValue('txid');
		var cs = getUrlValue('cs');
		if (txid != '' && cs != '') {
			var InatecDepositFinishMethodRequest = jsonClone(getUserMethodRequest(), {
				inatecTransactionId: txid,
				cs: cs
			});
			$http.post(settings.jsonLink + 'finishDirectDeposit', InatecDepositFinishMethodRequest)
				.success(function(data, status, headers, config) {
					if (data.errorCode) {
						$rootScope.openCopyOpPopup({
							type: 'info', 
							body: 'error-' + errorCodeMap.transaction_failed, 
						});
					} else if (data.success) {
						$rootScope.copyopUser.user.balance = data.balance;
						$scope.balance = formatAmount(data.balance, 0);
						$rootScope.copyopUser.user.balanceWF = $scope.balance;
						
						var onclose = null;
						var isFirstDeposit = 0;
						if (data.isFirstDeposit) {
							isFirstDeposit = 1;
							onclose = function(){$rootScope.openCopyOpPopup({type: 'autoWatch', destUserId: $rootScope.copyopUser.user.id})};
							$scope.getPixel({pixelType: 1, transactionId: data.transactionId});
							$scope.openQuestionary();
							$rootScope.copyopUser.user.firstDepositId = data.transactionId;
						}
						
						if (settings.isLive) {
							//google tracking
							dataLayer.push({
								'is_deposit': '1',
								'is_first': isFirstDeposit,
								'deposit_sum': data.dollarAmount,
								'deposit_currency': $rootScope.copyopUser.user.currency.code
							});
						}
						
						$rootScope.openCopyOpPopup({
							type: 'info', 
							header: 'deposit-success-header', 
							body: 'deposit-success-body', 
							onclose: onclose,
							bodyParams: {
								amount: formatAmount(data.amount, 2),
								transactionId: data.transactionId,
							}
						});
					} else {
						$rootScope.openCopyOpPopup({
							type: 'info', 
							body: 'direct-dposit-error', 
						});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}

	$scope.setFocus = function(id) {
		var counter = 20;
		function doIt() {
			var el = g(id);
			if (isUndefined(el)) {
				if (counter > 0) {
					setTimeout(function() {
						doIt();
					}, 200);
					counter--;
				}
			} else {
				el.focus();
			}
		}
		doIt();
	}
	
	$scope.showOff = function(event, investment){
		FB.ui({
			method: 'feed',
			name: $rootScope.getMsgs('show-off.title'),
			link: settings.shareLink,
			description: $rootScope.getMsgs('show-off.text.fb', {
				_noTrust:true, 
				amount: investment.winAmount,
				asset: investment.asset,
				'time-settled': investment.timeSettledDsp
			})
		}, function(response) {});
		//Use this to prevent closing of the popup on clicking "Show Off"
		// event.stopPropagation();
		
		var ShowOffMethodRequest = jsonClone(getUserMethodRequest(), {
			investmentId: investment.id
		});
		$http.post(settings.jsonLink + 'copyopShowOff', ShowOffMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'copyopShowOff')) {
					
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.changeNicknamePopup = function(nickname) {
		if(!$scope.copyopUser.copyopProfile.nicknameChanged) {
			$rootScope.openCopyOpPopup({
				type: 'changeNickname',
				nickname: nickname
			});
		}
	}
	
	$scope.changeNickname = function(newNickname) {
		if(!$scope.copyopUser.copyopProfile.nicknameChanged) {
			var ChangeNicknameMethodRequest = jsonClone(getUserMethodRequest(), {
				nickname: newNickname
			});
			$http.post(settings.jsonLink + 'changeCopyopNickname', ChangeNicknameMethodRequest)
				.success(function(data, status, headers, config) {
					data.globalErrorField = 'globalErrorFieldChangeNicknamePopup';
					if (!handleErrors(data, 'changeCopyopNickname')) {
						$scope.copyopUser.copyopProfile.nicknameChanged = true;
						$scope.copyopUser.copyopProfile.nickname = newNickname;
						$rootScope.closeCopyOpPopup();
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
	
	$scope.resetWalkthrough = function() {
		localStorage.setItem("walkthroughHome", 0); 
		localStorage.setItem("walkthroughUser", 0);
		localStorage.setItem("walkthroughAsset", 0);
		localStorage.setItem("walkthroughCopyPopup", 0);
	}
	
	$rootScope.checkForWoalktrough = function() {
		if ($scope.popUps.length == 0 && $rootScope.ready) {
			//walkthrough
			var walkthroughCookieHome = localStorage.getItem('walkthroughHome');
			var walkthroughCookieUser = localStorage.getItem('walkthroughUser');
			var walkthroughCookieAsset = localStorage.getItem('walkthroughAsset');
			//home
			if ($rootScope.$state.is('ln.home.a') && (isUndefined(walkthroughCookieHome) || walkthroughCookieHome == 0)) {
				$rootScope.openCopyOpPopup({type: 'walkthrough', caller: 'header', doNotHidePageScroll: true})
				localStorage.setItem('walkthroughHome', 1);
			}//user
			else if ($rootScope.$state.includes('ln.in.a.user') && (isUndefined(walkthroughCookieUser) || walkthroughCookieUser == 0)) {
				$rootScope.openCopyOpPopup({type: 'walkthrough', caller: 'header', doNotHidePageScroll: true})
				localStorage.setItem('walkthroughUser', 1);
			}//asset
			else if ($rootScope.$state.includes('ln.in.a.asset') && (isUndefined(walkthroughCookieAsset) || walkthroughCookieAsset == 0)) {
				$rootScope.openCopyOpPopup({type: 'walkthrough', caller: 'header', doNotHidePageScroll: true})
				localStorage.setItem('walkthroughAsset', 1);
			}
		}
	}
	
	$rootScope.openAffiliatesPopup = function(){
		$rootScope.openCopyOpPopup({type: 'affiliates'});
	}

	$scope.loginCheckTimer = null;
	$scope.loginCheck = function() {
		clearTimeout($scope.loginCheckTimer);
		
		$scope.loginCheckTimer = setTimeout(function() {
			$scope.getCopyopUser({onStateChange: true});
		}, settings.sessionTimeout);
	}
	
	$rootScope.openCacheBonusPopup = function(e, params){
		var invId = 0;
		var DepositBonusBalanceMethodRequest = jsonClone(getUserMethodRequest(), {
			userId: $scope.copyopUser.userId,
			investmentId: invId,
			balanceCallType: params.balanceCallType
		});
		$http.post(settings.jsonLink + 'getUserDepositBonusBalance', DepositBonusBalanceMethodRequest)
			.success(function(data, status, headers, config) {
				data.globalErrorField = 'globalErrorFieldUserDepositBonusBalance';
				data.balanceCallType = params.balanceCallType;
				if (!handleErrors(data, 'getUserDepositBonusBalance')) {
					var isOpened = false;
					for(var i = 0; i < $scope.popUps.length; i++){
						if($scope.popUps[i].config.type == "bonusBalance"){
							isOpened = true;
							$rootScope.closeCopyOpPopup({popupId: i});
						}
					}
					$rootScope.openCopyOpPopup({type: 'bonusBalance', doNotHidePageScroll: true, event: e, data: data});
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$rootScope.checkClickedTradeButton = function() {
		var neverBeenClicked = false;
		var clickedTradeButtonCookie = getCookie('clickedTradeButton');
		if ((isUndefined(clickedTradeButtonCookie) || clickedTradeButtonCookie == 0)) {
			neverBeenClicked = true;
			$(document).ready(function(){
				$('#header-button-trade').addClass("bounce");
			});
		}
		return neverBeenClicked;
	}
	
	$rootScope.setClickedTradeButton = function() {
		setCookie('clickedTradeButton', 1, 365*100);
		$('#header-button-trade').removeClass("bounce");
	}

	$scope.getUserHolidayMessage = function() {
		$http.post(settings.jsonLink + 'getUserHolidayMessage', getMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserHolidayMessage')) {
					if (!isUndefined(data.message)) {
						$rootScope.holidayMessage = data.message.text;
						$rootScope.openCopyOpPopup({type: 'info-img', bodyTxt: data.message.text, imgClass: 'holiday-popUp'});
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}

	$scope.showWithdrawUserDetails = function() {
		var WithdrawUserDetailsMethodRequest = jsonClone(getMethodRequest(), {
			withdrawUserDetails: {
				userId: $rootScope.copyopUser.user.id
			}
		});
			
		$http.post(settings.commonServiceLink  + '/WithdrawUserDetailsServices/showWithdrawUserDetails', WithdrawUserDetailsMethodRequest)
			.then(function(data) {
				data = data.data;
				if (!handleErrors(data, 'showWithdrawUserDetails')) {
					if (data.showPopup) {
						$rootScope.openCopyOpPopup({type: 'withdrawUserDetails'});
					}
					// $rootScope.openCopyOpPopup({type: 'withdrawUserDetails'});
				}
			})
			.catch(function(data) {
				handleNetworkError(data);
			})
	}

	$scope.fb_FTD_pixel = function(eventName, valueToSum, parameters) {
		 !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		 n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		 n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		 t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		 document,'script','https://connect.facebook.net/en_US/fbevents.js');
		 
		 fbq('init', '356800521349153', {
			 em: parameters.em
		 });
		 fbq('track', 'Purchase', {
			 value : '0.00',
			 currency : 'USD',
			 order_id : parameters.fb_order_id
		 });
	}

	$scope.getDeeplink = function() {
		var DeeplinkRequest = jsonClone(getMethodRequest(), {
			dpn: settings.dpn
		});

		$http.post(settings.jsonLink  + '/getDeeplink', DeeplinkRequest)
			.then(function(data) {
				data = data.data;
				if (!handleErrors(data, 'getDeeplink')) {
					if (data.deeplink && data.deeplink.copyopUrl) {
						var parts = data.deeplink.copyopUrl.split('/');
						if (parts[0] == '') {
							parts.splice(0, 1);
						}
						
						parts[0] = settings.skinLanguage;
						
						var link = (settings.isLive) ? '' : '#';
						// window.location.href = link + parts.join('/');
						history.pushState({}, '', link + parts.join('/'));
						settings.dpn = '';
					} else {
						$rootScope.$state.go('ln.home.a', {ln: $scope.skinLanguage});
					}
				}
			})
			.catch(function(data) {
				handleNetworkError(data);
			})
	}
	
	$scope.goToByScrollWeb = function(id, offset) {
		goToByScroll(id, offset);
	}
}]);

copyOpApp.controller('generalTermsCtr', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.TERMS_TYPE_AGREEMENT = 1;
	$scope.TERMS_TYPE_GENERAL_TERMS = 2;
	$scope.TERMS_TYPE_RISK_DISCLOSURE = 3;
	$scope.menuShownBeforeId = 0;
	$scope.showMenu = function(file){
		if($scope.menuShownBeforeId == 0 && file.typeId == $scope.TERMS_TYPE_GENERAL_TERMS){
			$scope.menuShownBeforeId = file.id;
		}
		if($scope.menuShownBeforeId == file.id){
			return true;
		}
		return false;
	}
	
	$scope.ready = false;
	$scope.termsContent = '';
	function waitForIt() {
		if (!$scope.$parent.ready) {
			$timeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	$scope.passParams = function(params){
		$scope.params = params;
		waitForIt();
	}
	
	function init() {
		$scope.loading = true;
		var MethodRequest = jsonClone(getUserMethodRequest(), {
			"platformIdFilter": 3,
			"skinIdFilter": settings.skinId,
			"termsType": $scope.params.termsType
		});
		
		function filterTitle(str){
			var result = "";
			if(str){
				result = str.replace(/\"/g, "");
			}
			return result;
		}
		
		$http.post(settings.jsonLink + 'getTermsFiles', MethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserDepositBonusBalance')) {
					data.filesList.sort(function(a, b) {
						return parseInt(a.orderNum) - parseInt(b.orderNum);
					});
					
					$scope.isRegulated = settings.isRegulated;
					$scope.filesList = data.filesList;
					
					for (var i = 0; i < $scope.filesList.length; i++) {
						$scope.filesList[i].html = multipleReplace(replaceParamsGT, $scope.filesList[i].html);
						
						$scope.filesList[i].anchorHref = "#" + ($scope.filesList[i].title ? filterTitle($scope.filesList[i].title) : $scope.filesList[i].id);
						var linkTitle = $scope.filesList[i].title;
						if (!linkTitle) {
							$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h2:eq(0)").each(function(index, el) {
								linkTitle = el.innerHTML;
							});
							if (!linkTitle) {
								$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h1:eq(0)").each(function(index, el) {
									linkTitle = el.innerHTML;
								});
							}
						}
						if (linkTitle) {
							$scope.filesList[i].menuTitle = linkTitle;
						}
					}
				}
				$scope.loading = false;
				repositionAnchors();
				if (!isUndefined($rootScope.$state.params.id)) {
					$timeout(function() {
						goToByScroll($rootScope.$state.params.id, -100);
					}, 1000)
				}
				if ($scope.params.scrollTo) {
					$timeout(function() {
						goToByScroll($scope.params.scrollTo, 0, '.scroll-content-gt');
					}, 1000)
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	$scope.goToByScrollWeb = function(id, offset) {
		goToByScroll(id, offset);
	}
	$scope.goToByScroll = function(id, offset) {
		goToByScroll(id, offset, '.scroll-content');
	}
}]);

copyOpApp.controller('AssetIndexController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.cols = [];
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.getAssetIndexMarketsPerSkin();
		}
	}
	
	$scope.assetIndexMarkets = [];//type 1: grpup, type 2: country, type 3: market
	$scope.lastConfig = {
		groupName: '',
		displayGroupNameKey: '',
		opportunityTypeId: ''
	};
	
	$scope.getAssetIndexMarketsPerSkin = function() {
		$http.post(settings.commonServiceLink + 'AssetIndexServices/getAssetIndexMarketsPerSkin', getMethodRequest())
			.success(function(data, status) {
				$scope.assetIndexGeneral = {
					tooltipData: data.tooltipData,
					tooltipDataUnderTable: data.tooltipDataUnderTable,
					assetIndexFormulasBySkin: data.assetIndexFormulasBySkin
				}
				
				var i = 0;
				$.each(data.marketList, function(k, v) {
					if (v.opportunityTypeId == 1) {
						if ($scope.lastConfig.groupName != v.market.groupName) {
							if (!isUndefined(v.market.groupName)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.groupName),
									type: 1
								});
								$scope.lastConfig.groupName = v.market.groupName;
							}
						}
						
						if ($scope.lastConfig.displayGroupNameKey != v.market.displayGroupNameKey) {
							if (!isUndefined(v.market.displayGroupNameKey)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.displayGroupNameKey),
									type: 2
								});
								$scope.lastConfig.displayGroupNameKey = v.market.displayGroupNameKey;
							}
						}
					} else {
						if ($scope.lastConfig.opportunityTypeId != v.opportunityTypeId) {
							$scope.assetIndexMarkets.push({
								title: $rootScope.getMsgs('type-name-' + v.opportunityTypeId),
								type: 1
							});
							$scope.lastConfig.opportunityTypeId = v.opportunityTypeId;
						}
					}
					
					var temp = {
						type: 3,
						opportunityType: v.opportunityTypeId,
						id: v.market.id,
						marketGroupId: v.marketGroupId,
						title: v.market.displayName,
						description: v.marketAssetIndexInfo.marketDescription,
						descriptionPage: v.marketAssetIndexInfo.marketDescriptionPage,
						additionalText: v.marketAssetIndexInfo.additionalText,
						symbol: v.market.feedName,
						tradingTime: '',
						newMarket: v.market.newMarket,
						is24h7: v.marketAssetIndexInfo.is24h7,
						formulas: []
					}

					var startDay = -1;
					var endDay = -1;
					for (var d = 0; d < 7; d++) {
						if (v.marketAssetIndexInfo.tradingDays[d] == 1 && startDay == -1) {
							startDay = d;
						}
						if (v.marketAssetIndexInfo.tradingDays[d] == 1) {
							endDay = d;
						}
					}
					
					var orgStartDate = new Date(v.marketAssetIndexInfo.startTime);
					var startDate = new Date(new Date(orgStartDate.setDate(orgStartDate.getDate() - orgStartDate.getDay() + startDay)).getTime() - (orgStartDate.getTimezoneOffset() * 60 * 1000));//crazy shit
					
					var orgEndDate = new Date(v.marketAssetIndexInfo.endTime);
					var endDate = new Date(new Date(orgEndDate.setDate(orgEndDate.getDate() - orgEndDate.getDay() + endDay)).getTime() - (orgEndDate.getTimezoneOffset() * 60 * 1000));//crazy shit
				
					temp.tradingTime += $rootScope.getMsgs('day-' + startDate.getDay()) + ' - ' + $rootScope.getMsgs('day-' + endDate.getDay());
					temp.tradingTime += ' ' + startDate.getHours() + ':' + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()) + '-' + endDate.getHours() + ':' + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());
					
					for (var t = 0; t < v.marketAssetIndexInfo.expiryFormulaCalculations.length; t++) {
						if (t == 0) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].sameNextRow = false;
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 2) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].sameNextRow = false;
							if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId == v.marketAssetIndexInfo.expiryFormulaCalculations[0].expiryFormulaId) {
								temp.formulas[temp.formulas.length - 1].sameNextRow = true;
								temp.formulas[0].sameNextRow = true;
							}
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 3) {
							temp.formulas[temp.formulas.length - 1].expiryTypeId = 3;
						}
					}
					
					$scope.assetIndexMarkets.push(temp);
					i++;
				});
				
				
				$scope.AssetIndexCols = Math.round($scope.assetIndexMarkets.length / 3);
				$scope.cols.push({markets:[]});
				
				for (var i = 0; i < $scope.assetIndexMarkets.length; i++) {
					if ($scope.cols[$scope.cols.length - 1].markets.length > $scope.AssetIndexCols) {
						$scope.cols.push({markets:[]});
					}
					
					$scope.cols[$scope.cols.length-1].markets.push($scope.assetIndexMarkets[i]);
				}
				
				if (!isUndefined(window.marketId)) {
					var arrayIndex = searchJsonKeyInArray($scope.assetIndexMarkets, 'id', marketId);
					if (arrayIndex != -1) {
						$scope.selectedMarket = $scope.assetIndexMarkets[arrayIndex];
					}
				}
			})
			.error(function( data, status ) {
				$rootScope.handleNetworkError(data);
			});
	}

	waitForIt();
}]);

copyOpApp.controller('PrivacyCtr', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.TERMS_TYPE_PRIVACY = 4;
	
	$scope.ready = false;
	$scope.privacyContent = '';
	function waitForIt() {
		if (!$scope.$parent.ready) {
			$timeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	waitForIt();
	
	function init() {
		$scope.loading = true;
		var MethodRequest = jsonClone(getUserMethodRequest(), {
			"platformIdFilter": 3,
			"skinIdFilter": settings.skinId,
			"termsType": $scope.TERMS_TYPE_PRIVACY
		});
		
		function filterTitle(str){
			var result = "";
			if(str){
				result = str.replace(/\"/g, "");
			}
			return result;
		}
		
		$http.post(settings.jsonLink + 'getTermsFiles', MethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserDepositBonusBalance')) {
					data.filesList.sort(function(a, b) {
						return parseInt(a.orderNum) - parseInt(b.orderNum);
					});
					
					$scope.isRegulated = settings.isRegulated;
					$scope.filesList = data.filesList;
					
					for (var i = 0; i < $scope.filesList.length; i++) {
						$scope.filesList[i].html = multipleReplace(replaceParamsGT, $scope.filesList[i].html);
						
						$scope.filesList[i].anchorHref = "#" + ($scope.filesList[i].title ? filterTitle($scope.filesList[i].title) : $scope.filesList[i].id);
						var linkTitle = $scope.filesList[i].title;
						if (!linkTitle) {
							$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h2:eq(0)").each(function(index, el) {
								linkTitle = el.innerHTML;
							});
							if (!linkTitle) {
								$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h1:eq(0)").each(function(index, el) {
									linkTitle = el.innerHTML;
								});
							}
						}
						if (linkTitle) {
							$scope.filesList[i].menuTitle = linkTitle;
						}
					}
				}
				$scope.loading = false;
				repositionAnchors();
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	$scope.goToByScrollWeb = function(id, offset) {
		goToByScroll(id, offset);
	}
	$scope.goToByScroll = function(id, offset) {
		goToByScroll(id, offset, '.scroll-content');
	}
}]);

copyOpApp.controller('MobileDepositController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.depositSuccessful = false;
	$scope.setSuccess = function(successMsg) {
		$scope.depositSuccessful = true;
		$scope.successMsg = successMsg; 
	}
	$scope.successMsg = '';

	$scope.depositTab = 1;
	$scope.changeTab = function(tab) {
		$scope.depositTab = tab;
	}
	
	
	function waitForIt() {
		if (!$scope.$parent || !$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			$scope.ready = true;
		}
	}
	waitForIt();
}]);

function repositionAnchors(){
	var topOffset = $(".main-header").outerHeight();
	$(".anchor").css("display", "block");
	$(".anchor").css("height", topOffset);
	$(".anchor").css("margin-top", (-1)*topOffset);
}
