//trading box
var livePage = false;//TODO what is the point of that
var OPPORTUNITY_STATE = {
	'created'			: 1,
	'opened'			: 2,//open
	'last_10_min'		: 3,//open
	'closing_1_min'		: 4,//w8ting for expiry
	'closing'			: 5,//w8ting for expiry
	'closed'			: 6,//expired
	'done'				: 7,//delete
	'paused'			: 8,
	'suspended'			: 9,
	'waiting_to_pause'  : 10
}
var SUBSCR_FLD = {
	'key'						: 'key',//1
	'command'					: 'command',//2
	'et_name'					: 'ET_NAME',//3
	'ao_level'					: settings.skin_group_id,//4
	'et_odds'					: 'ET_ODDS',//5
	'et_est_close'				: 'ET_EST_CLOSE',//6
	'et_opp_id'					: 'ET_OPP_ID',//7
	'et_odds_win'				: 'ET_ODDS_WIN',//8
	'et_odds_lose'				: 'ET_ODDS_LOSE',//9
	'et_state'					: 'ET_STATE',//10
	'ao_clr'					: settings.level_color,//11
	'ao_clr_dsp'				: 'ao_clr',//11
	'priority'					: 'ET_PRIORITY',//12
	'et_group_close'			: 'ET_GROUP_CLOSE',//13
	'et_scheduled'				: 'ET_SCHEDULED',//14
	'et_rnd_floor'				: 'ET_RND_FLOOR',//15
	'et_rnd_ceiling'			: 'ET_RND_CEILING',//16
	'et_group'					: 'ET_GROUP',//17
	'et_suspended_message'		: 'ET_SUSPENDED_MESSAGE',//18
	'ao_markets'				: 'AO_HP_MARKETS_' + settings.skinId,//19
	'ao_states'					: 'AO_STATES',//20
	'last_invest'				: 'ET_LAST_INV',//22
	'time_stamp'				: 'AO_TS',//23
	'ao_flags'					: 'AO_FLAGS',//24
	'ao_flags_dsp'				: 'ao_flags',
	'odds_group'				: 'ET_ODDS_GROUP',//25
	'ao_calls_trend'			: settings.ao_calls_trend,//26
	'ao_calls_trend_dep'		: 'ao_calls_trend',
	'mobile_market_list_index'	: "mmListIndex",
	'filter_new_scheduled'		: 'FILTER_NEW_SCHEDULED',
	'ask'						: 'ASK',
	'bid'						: 'BID',
	'last'						: 'LAST'
}

var marketsDisplayName = new Array();
var ls_marketStates = new Array();//marketStates
var ls_marketsTimestamp = 0;//marketsTimestamp
var ls_isNotLogInError = false;//isNotLogInError - TO DO
var ls_strHref = window.location.href;//strHref
var fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
var isLiveAOPage = false;

var markestInBox = new Array();//markets
function marketToBox(marketId){
	for(var i=0; i<box_objects.length; i++){
		if(box_objects[i] != null || box_objects[i] != undefined){
			if(box_objects[i].market.market_id == marketId){
				return i;
			}
		}
	}
	return -1;
}
var box_objects = new Array();
function box_obj(params){
	try{
		logIt({'type':1,'msg':'init box: '+params.box});
		var self = this;
		this.chart = null;
		this.box = params.box;
		this.boxType = params.boxType;
		new callPut_sw(this.box);
		
		this.boxFull = false;
		this.submited = false;
		this.inProgress = false;
		this.optionChanged = true;//if on update the oppId is the same
		this.oppId = 0;
		this.nh_oppId = 0;
		this.oppArrPos = 0;
		this.marketChanged = false;
		this.alternateOpps = [];
		
		this.market = {};//boxesMarkets
		this.boxOpportunities = [];//boxesOpportunities {'ET_OPP_ID':124323423,'ET_STATE':'DELETE'}
		
		this.if_correct = 0;
		this.if_not_currect = 0;
		this.invest = amountToFloat(params.invest);//amount to invest
		this.profit = 0;
		this.refund = 0;
		this.serverOffset = new Date().getTimezoneOffset()*60*1000*(-1);
		this.timeLastInv_mil = 0;
		this.timeClosing_mil = 0;
		this.chartPointsInv = [];
		this.chartPointsInv_last = {};
		this.lastSecondsToInvest = 600;//seconds to closing 10*60
		this.seconds_to_quot = 4000;//milliseconds to quoa 4*1000
		
		this.allPoints = [];
		this.allPointsInv = [];
		this.allInvestments = [];
		this.marketId = 0;
		this.marketIdManual = false;
		if (this.boxType == 5) {
			this.tradeBox_level_html = g('tradeBox_level_small_'+this.box);
		} else {
			this.tradeBox_level_html = g('tradeBox_level_'+this.box);
		}
		
		this.firstZoom = false;
		this.chartVissibleArea = {start:0,start_zoomed:0,end:0};
		this.stateReallyChanged = null;
		this.decimalPoint = 3;
		this.duplicatedTo = null;
		this.lastUpdateInfo = null;
		this.investmentId = null;
		this.delayChartHistory = false;
		this.chart_properties = {
			height:								165,
			yAxis_1:							160,
			band_bgr:							'chart_plotBand.png',
			band_bgr_width:						14,
			band_bgr_height:					6,
			popUp_fsize1:						'11px',
			popUp_fsize2:						'11px',
			popUp_fsize3:						'9px',
			chart_lineColor:					'#364b5e',
			chart_win:							'#16b865',
			chart_lose:							'#f7b431',
			//chart_noChange:						'#04618e',
			chart_noChange:						'#f7b431',
			chart_current:						'#093856',
			chart_current_with_inv:				'white',
			chart_plotBand_inTheMoney:			/*'#17714e'*//*'#36a779'*/'#29805d',
			chart_plotBand_inTheMoney_current:	'#37a67b',
			chart_plotBand_outTheMoney:			'#464646',
			chart_plotBand_outTheMoney_current:	'#939393',
			chart_marker:						'#7aab53',
			chart_marker_hover:					'#7aab53',
			chart_marker_with_inv:				'#7aab53',
			chart_marker_hover_with_inv:		'#7aab53',
			timer_bgrColor:						'#f5e9be',
			timer_timeElapsedColor:				'#00bc6f',
			timer_bgrLastTenColor:				/*'#1c8f52'*/'#aa3612',
			timer_timeLastTenElapsedColor:		'#e74614',
			plotLinesColor:						'#e74310',
			plotLinesColor_with_inv:			'#ea553a',//
			plotLinesColor_inv:					'#ea553a',//i think it's wrong color old #f2b702
			tooltip_borderColor:				/*'#156c96'*/'#999999',
			tooltip_bgrColor:					/*'#fafdff'*/'#f2f2f2',
			tooltip_timeTxtColor:				/*'#04618e'*/'#464646',
			tooltip_assetTxtColor:				'#e74310',
			tooltip_levelTxtColor:				/*'#04618e'*/'#464646',
			timeLineTxtColor:					/*'#04618e'*/'#464646',
			timeLineTxtPosition:				-10,
			plotBandMouseOver:					/*'rgba(78,162,198,0.2)'*//*'#EE2500'*/'rgba(179,100,88,0.2)'
		};
		if(this.boxType == 2 || this.boxType == 3){
			this.chart_properties.height							= 285;
			this.chart_properties.yAxis_1							= 275;
			this.chart_properties.band_bgr							= 'chart_plotBand_big.png';
			this.chart_properties.band_bgr_width					= 24;
			this.chart_properties.band_bgr_height					= 11;
			this.chart_properties.popUp_fsize1						= '14px';
			this.chart_properties.popUp_fsize2						= '18px';
			this.chart_properties.timeLineTxtPosition				= -15;
		} else if(this.boxType == 4) {
			this.chart_properties.height							= 240;
			this.chart_properties.yAxis_1							= 235;
			this.chart_properties.timer_bgrColor					= '#f5e9be';
			this.chart_properties.timer_timeElapsedColor			= '#00bc6f';
			this.chart_properties.timer_bgrLastTenColor				= '#e74614';
			this.chart_properties.timer_timeLastTenElapsedColor		= /*'#1c8f52'*/'#aa3612';
		} else if(this.boxType == 5) {
			this.chart_properties.height							= 130;
			this.chart_properties.yAxis_1							= 125;
			this.chart_properties.timer_bgrColor					= '#f5e9be';
			this.chart_properties.timer_timeElapsedColor			= '#00bc6f';
			this.chart_properties.timer_bgrLastTenColor				= '#e74614';
			this.chart_properties.timer_timeLastTenElapsedColor		= /*'#1c8f52'*/'#aa3612';
		}
		
		this.chart_point_properties = {
			enabled: true,
			symbol: 'circle',
			fillColor: this.chart_properties.chart_current,
			lineColor: this.chart_properties.chart_current,
			radius: 5
		};
		
		g('tradeBox_error_footer_close_'+this.box).onclick = function(){
			handleMessages({'type':'other','errorCode':''});
		}
		//reset box params
		function resetBox(cleanState){
			self.boxFull = false;
			self.submited = false;
			self.inProgress = false;
			self.optionChanged = true;
			self.chartPointsInv = [];
			
			self.profit = 0;
			self.refund = 0;
			self.timeLastInv_mil = 0;
			self.timeClosing_mil = 0;
			self.allPoints = [];
			self.allPointsInv = [];
			self.allInvestmentsForOpp = [];
			self.alternateOpps = [];
			self.quorterlyOpps = [];
			
			if (!isUndefined(defaultMarkets[self.box]) && !isUndefined(defaultMarkets[self.box].opportunityId) && (defaultMarkets[self.box].marketId == self.marketId || self.marketId == 0)) {
				self.oppId = defaultMarkets[self.box].opportunityId;
			} else {
				self.oppId = 0;
			}
			
			self.nh_oppId = 0;
			self.marketChanged = false;
			
			clearTimeout(self.timeLeftTimer_var);
			if(cleanState){
				changeStates({'state':''});//clean loading
				resetGraph()//reset graph
			}
		}
		this.resetBox = resetBox;
		
		function getNewMarket() {
			var oppType = (self.boxType == 1 || self.boxType == 4) ? self.boxOpportunities[self.oppArrPos].ET_OPP_ID : 0;
				
			var oppId = 0;
			if (self.market.schedule != 1 || !self.marketChanged) {
				oppId = self.boxOpportunities[self.oppArrPos].ET_OPP_ID;
			} else {
				oppId = oppType = 0;
			}

			var req = jsonClone(getUserMethodRequest(),{box: self.box, marketId: 0, opportunityId: 0, opportunityTypeId: 1})
			var ajaxCall = new aJaxCall_fn({
				'type': 'POST',
				'url': settings.jsonLink + "getChartData",
				'callback': getNewMarketResponse,
				'params': JSON.stringify(req)
			});
			ajaxCall.doGet();
		}
		this.getNewMarket = getNewMarket;
		
		function getNewMarketResponse(data) {
			try{
				defaultMarkets[self.box] = data;
				selectMarket(data.marketId);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//add default invest amount
		g('tradeBox_inv_amount_'+this.box).innerHTML = formatAmount(this.invest,3);

		//add onclick event to exp dropdown
		// this.addFnToExpDrop = addFnToExpDrop;
		function addFnToExpDrop(){
			try{
				// logIt({'type':1,'msg':'addFnToExpDrop box: '+self.box});
				var tradeBox_menu_exp = g('tradeBox_menu_exp_' + self.box);
				var lis = tradeBox_menu_exp.getElementsByTagName('li');
				for(var i=0; i<lis.length; i++){
					if(lis[i].getAttribute('data-value') != null){
						lis[i].onclick = function(){
							if (!self.inProgress) {
								$('#tradeBox_menu_exp_' + self.box + ' > li').removeClass('selected');
								$(this).addClass('selected');
								updateExpTimeOpp(this);
								// var type = '';
								// switch(this.getAttribute('data-value')) {
									// case '1': type = 'Nearest'; break;
									// case '2': type = 'EndDay'; break;
									// case '3': type = 'EndWeek'; break;
									// case '4': type = 'EndMnth'; break;
								// }
							}
						}
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		if(this.boxType == 1 || this.boxType == 4){
			addFnToExpDrop();
		}

		if(this.boxType == 3){
			var tradeBox_slipCommision = g('tradeBox_slipCommision_'+self.box);
			tradeBox_slipCommision.innerHTML = tradeBox_slipCommision.innerHTML.replace('{0}','<span id="tradeBox_slipCommision_in_'+self.box+'"></span>')
		}
		else{
			g('tradeBox_profit_drop_amount_' + self.box).onclick = function(){
				openElement({'span':this,'autoClose':true});
			}
		}
		var tradeBox_dev2_msg = g('tradeBox_dev2_msg_'+self.box);
		tradeBox_dev2_msg.innerHTML = tradeBox_dev2_msg.innerHTML.replace('{investLevel}','<span id="tradeBox_dev2_msg_in_'+self.box+'"></span>')

		//updateExpTimeOpp
		//this.updateExpTimeOpp = updateExpTimeOpp;
		function updateExpTimeOpp(el){
			try{
				closeElement({'last':true});
				var scheduled = el.getAttribute('data-value');
				var nh_oppId = el.getAttribute('data-opp-id');
				if (nh_oppId === null || self.market.schedule != scheduled) {
					handleMessages({'type':'popup','errorCode':4});
					group[self.box] = self.market.type+"_" + scheduled + "_" + self.marketId;

					self.boxOpportunities = [];
					self.market.schedule = scheduled;
					resetBox(true);
					
					if (!isUndefined(nh_oppId)) {
						self.oppId = nh_oppId;
					}
					
					lsClient.unsubscribe(tradingBox);
					tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
				} else {
					var forceStateWaiting = false;
					self.resetBox(true);
					self.oppId = nh_oppId;
					
					self.oppArrPos = searchJsonKeyInArray(self.boxOpportunities,'ET_OPP_ID',self.oppId);
					
					self.updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
					
					self.updateState(//changeBoxState
						{'oppId':self.oppId,
							'ao_level':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.ao_level],
							'et_state':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_state],
							'et_est_close':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_est_close],
							'last_invest':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.last_invest],
							'et_suspended_message':self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_suspended_message],
							'forceStateWaiting': forceStateWaiting
						}
					);
					
					// if(self.boxType != 1 || self.boxType != 4){
						// tradeBox_getBalace(self.box);
					// }
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//add onclick event to invest dropdpown
		// this.addFnToInvestDrop = addFnToInvestDrop;
		function addFnToInvestDrop(){
			try{
				logIt({'type':1,'msg':'addFnToInvestDrop box: '+self.box});
				var lis = g('tradeBox_invest_drop_ul_'+self.box).getElementsByTagName('li');
				var flag = true;
				for(var i=0; i<lis.length; i++){
					if(lis[i].getAttribute('data-amount') != null){
						var amount = parseFloat(lis[i].getAttribute('data-amount').replace(/,/g,''));
						lis[i].innerHTML = formatAmount(amount,2, true);
						lis[i].onclick = function(){
							updateInvest(this);
						}
						if(amount == self.invest){
							lis[i].className += " active";
							var flag = false;
						}
					}
				}
				//fill custom amount
				// if(flag){
					// var tradeBox_invest_drop_custom = g('tradeBox_invest_drop_custom_'+self.box);
					// tradeBox_invest_drop_custom.parentNode.className += " active";
					// tradeBox_invest_drop_custom.value = self.invest;
				// }
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		addFnToInvestDrop();
		
		//update invest amount
		// this.updateInvest = updateInvest;
		function updateInvest(el){
			try{
				logIt({'type':1,'msg':'updateInvest box: '+self.box});
				self.invest = amountToFloat(parseFloat(el.getAttribute('data-amount').replace(/,/g,'')));
				g('tradeBox_inv_amount_'+self.box).innerHTML = formatAmount(self.invest, 3);
				updateProfitAmount();
				clearActiveList(el.parentNode.getElementsByTagName('li'));
				el.className += " active";
				var tradeBox_invest_drop_custom = g('tradeBox_invest_drop_custom_'+self.box);
				tradeBox_invest_drop_custom.parentNode.className = tradeBox_invest_drop_custom.parentNode.className.replace(/ active/g,'');
				tradeBox_invest_drop_custom.value = tradeBox_invest_drop_custom.defaultValue;
				closeElement({'last':true});
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add onclick event to invest dropdpown
		// this.addFnToProfitDrop = addFnToProfitDrop;
		function addFnToProfitDrop(){
			try{
				logIt({'type':1,'msg':'addFnToProfitDrop box: '+self.box});
				var invest_lis = g('tradeBox_profit_drop_ul_'+self.box).getElementsByTagName('li');
				for(var i=0; i<invest_lis.length; i++){
					if(invest_lis[i].getAttribute('data-profit') != null){
						invest_lis[i].onclick = function(){
							updateProfit(this);
						}
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//update invest amount
		// this.updateProfit = updateProfit;
		function updateProfit(el){
			try{
				logIt({'type':1,'msg':'updateProfit box: '+self.box});
				self.profit = parseFloat(el.getAttribute('data-profit'));
				self.refund = parseFloat(el.getAttribute('data-refund'));
				g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
				updateProfitAmount();
				clearActiveList(el.parentNode.getElementsByTagName('li'));
				el.className += " active";
				closeElement({'last':true});
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//update if win and loose amount
		function updateProfitAmount(){
			try{
				logIt({'type':1,'msg':'updateProfitAmount box: '+self.box});
				self.if_correct = self.invest+(self.invest*self.profit/100);
				g('tradeBox_profit_amount_'+self.box).innerHTML = formatAmount(self.if_correct,1);
				self.if_not_currect = self.invest*self.refund/100;
				g('tradeBox_incorrect_amount_'+self.box).innerHTML = formatAmount(self.if_not_currect,1);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateProfitAmount = updateProfitAmount;
		
		//custom investment input
		g('tradeBox_invest_drop_custom_'+this.box).onkeyup = function(e){
			e = e || window.event;
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){
				closeElement({'last':true});
				return false;
			}
			if(settings.currencyLeftSymbol){
				this.value = settings.currencySymbol + checkValue(this.value);
			}
			else{
				// this.value = checkValue(this.value) + settings.currencySymbol;
				this.value = checkValue(this.value);
			}
			if(this.value == settings.currencySymbol){
				this.value = '';
			}
			
			if(this.value != "" && this.value != settings.currencySymbol){
				customInvAmount(this);
			}
		}
		g('tradeBox_invest_drop_custom_'+this.box).onclick = function() {
			this.value = '';
		}
		// this.customInvAmount = customInvAmount;
		function customInvAmount(el){
			try{
				logIt({'type':1,'msg':'customInvAmount box: '+self.box});
				self.invest = parseFloat(el.value.replace(settings.currencySymbol,''));
				g('tradeBox_inv_amount_'+self.box).innerHTML = formatAmount(self.invest,3);
				updateProfitAmount();
				clearActiveList(el.parentNode.parentNode.getElementsByTagName('li'));
				el.parentNode.className += " active";
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//popup errors
		this.currentErrors = {'pop':'hidden','other':'','chartHeader':'hidden'};
		//[type:popup]0:no error,1:not available,2:login message,3:deposit message,4:loading; 
		//[type:other]0:no error,1:invest error,2:wait for exp,3:invest confirm 
		//[type:chartHeader]1:time_invest,2:exp_level,3:waiting_for_exp
		function handleMessages(params){
			try{
				logIt({'type':1,'msg':{'msg':'handleMessages box: '+self.box,'params':params}});
				if(params.type == 'popup'){
					var holder = g('tradeBox_pop_holder_'+self.box);
					var errorsPop = 'hidden';
					switch(params.errorCode){
						case 1:
							errorsPop = 'notAv';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_pop_notAv_'+self.box).innerHTML = g('tradeBox_pop_notAv_'+self.box).innerHTML.replace('{expTime}',params.expTime);
							break;
						case 5:
							errorsPop = 'suspended';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_pop_suspended_'+self.box).innerHTML = params.errorMsg;
							break;
						case 2:
							errorsPop = 'login';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
						case 3:
							errorsPop = 'noDeposit';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
						case 4:
							errorsPop = 'loading';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							resetBox(false);
							break;
						case 6:
							errorsPop = 'dev2';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							g('tradeBox_dev2_msg_in_' + self.box).innerHTML = numberWithCommas(params.level);
							startDEV2Timer(params.time);
							break;
						case 7:
							errorsPop = 'cashDeposit';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							var cashDepositMsg = g('tradeBox_pop_cashDeposit_msg_' + self.box);
							cashDepositMsg.innerHTML = cashDepositMsg.getAttribute('data-msgs').replace('{cash_balance_parameter}', formatAmount(params.errorMsg, 2, true));
							self.currentErrors.pop = errorsPop;
							break;
						default:
							errorsPop = 'hidden';
							holder.className = holder.className.replace(self.currentErrors.pop,errorsPop);
							self.currentErrors.pop = errorsPop;
							break;
					}
				}
				else if(params.type == 'other'){
					if(self.currentErrors.other != ""){
						self.currentErrors.other.style.display = "none";
					}
					if (!isUndefined(self.currentErrors.jEl)) {
						self.currentErrors.jEl.removeClass(self.currentErrors.jCLass);
						self.currentErrors.jEl = null;
					}
					switch(params.errorCode){
						case 1:
							var tmp = g('tradeBox_error_footer_'+self.box);
							tmp.style.display = "block";
							g('tradeBox_error_footer_txt_'+self.box).innerHTML = params.errorMsg;
							self.currentErrors.other = tmp;
							clearTimeout(self.closeSlipTimer);
							self.closeSlipTimer = setTimeout(function(){handleMessages({'type':'other','errorCode':''});},5000);
							break;
						case 2:
							var tmp = g('tradeBox_waiting_for_exp_'+self.box);
							tmp.style.display = "block";
							self.currentErrors.other = tmp;
							break;
						case 3:
							var tmp = g('tradeBox_after_invest_'+self.box);
							tmp.style.display = "block";
							self.currentErrors.other = tmp;
							investConfirmFill(params.result);
							break;
						case 4:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv';
							cancelInvFill(params.result);
							break;
						case 5:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv-msg');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv-msg';
							break;
						case 6:
							var tmp = $('#chart-overlays_' + self.box);
							tmp.addClass('cancel-inv-error');
							self.currentErrors.jEl = tmp;
							self.currentErrors.jCLass = 'cancel-inv-error';
							break;
					}
				}
				else if(params.type == 'chartHeader'){
					var holder = g('tradeBox_chartHeader_'+self.box);
					if (holder != null) {
						var chartHeader = 'hidden';
						switch(params.errorCode){
							case 1:
								// if(self.timeLeftToInvest < 60*60){
									chartHeader = 'time_invest';
									holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
									self.currentErrors.chartHeader = chartHeader;
								// }
								break;
							case 2:
								chartHeader = 'exp_level';
								holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
								self.currentErrors.chartHeader = chartHeader;
								g('tradeBox_exp_level_value_'+self.box).innerHTML = params.ao_level;
								break;
							case 3:
								chartHeader = 'waiting_for_exp';
								holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
								self.currentErrors.chartHeader = chartHeader;
								break;
							default:
								chartHeader = 'hidden';
								holder.className = holder.className.replace(self.currentErrors.chartHeader,chartHeader);
								self.currentErrors.chartHeader = chartHeader;
								break;
						}
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.handleMessages = handleMessages;
		//loading by default
		handleMessages({'type':'popup','errorCode':4});
		//add close fnc
		g('tradeBox_pop_login_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		g('tradeBox_pop_noDeposit_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		g('tradeBox_pop_cashDeposit_close_'+this.box).onclick = function(){
			handleMessages({'type':'popup'});
		}
		
		//investConfirmFill
		this.closeSlipTimer = null;
		function investConfirmFill(result){
			try{
				logIt({'type':1,'msg':{'msg':'investConfirmFill box: '+self.box,'result':result}});
				var tradeBox_AI_type = g('tradeBox_AI_type_'+self.box);
				tradeBox_AI_type.innerHTML = (self.chartPointsInv_last.type == 1)?tradeBox_AI_type.getAttribute('data-call'):tradeBox_AI_type.getAttribute('data-put');
				switch(result.color){
					case '0':var color = " below";break;
					case '1':var color = " same";break;
					case '2':var color = " above";break;
				}
				g('tradeBox_AI_level_'+self.box).innerHTML = numberWithCommas(result.level);
				g('tradeBox_AI_level_'+self.box).className = color;
				g('tradeBox_AI_expires_'+self.box).innerHTML = formatDate(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE), false);
				g('tradeBox_AI_option_id_'+self.box).innerHTML = self.investmentId;
				if(self.boxType == 3){
					var totalInv = self.invest+commsionFee;
				}else{
					var totalInv = self.invest;
				}
				g('tradeBox_AI_amount_'+self.box).innerHTML = formatAmount(totalInv);
				g('tradeBox_AI_correct_'+self.box).innerHTML = formatAmount(self.if_correct,0);
				g('tradeBox_AI_incorrect_'+self.box).innerHTML = formatAmount(self.if_not_currect,0);
				g('tradeBox_sms_'+self.box).style.display = (result.sms == 0)?'none':'block';
				g('tradeBox_sms_inp_'+self.box).checked = false;
				g('tradeBox_sms_inp_'+self.box).disabled = false;
				var tradeBox_sms_txt = g('tradeBox_sms_txt_'+self.box);
				tradeBox_sms_txt.innerHTML = tradeBox_sms_txt.getAttribute('data-defaultMsg');
				g('tradeBox_sms_inp_'+self.box).onclick = function(){
					updateInvestSms();
				}
				if(self.boxType == 3){
					g('tradeBox_slipCommision_in_'+self.box).innerHTML = commsionFeeWithSign;
					g('tradeBox_sms_'+self.box).style.display = "none";
				}
				clearTimeout(self.closeSlipTimer);
				self.closeSlipTimer = setTimeout(function(){handleMessages({'type':'other'});},10000);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.investConfirmFill = investConfirmFill;
		
		//cancelInvFill
		g('submit-link_' + this.box).onclick = function() {
			cancelInv();
		}
		
		function cancelInvFill(result){
			try{
				logIt({'type':1,'msg':{'msg':'cancelInvFill box: '+self.box,'result':result}});
				
				startCancelTimeout();
				
				$('#cancel-inv-invest-arrow_' + self.box).removeClass('call');
				$('#cancel-inv-invest-arrow_' + self.box).removeClass('put');
				$('#cancel-inv-invest-arrow_' + self.box).addClass((self.chartPointsInv_last.type == 1) ? 'call' : 'put');
				
				$('#cancel-inv-level_' + self.box).html(numberWithCommas(result.level));
				
				if(self.boxType == 3){
					var totalInv = self.invest+commsionFee;
				}else{
					var totalInv = self.invest;
				}
				
				$('#cancel-inv-amount_' + self.box).html(formatAmount(totalInv));
				
				var comission = formatAmount(Math.floor((parseFloat(self.invest) * 0.1) * 100)/100, 2);
				$('#cancelInvText_' + self.box).html($('#cancelInvText_' + self.box).attr('data-text').replace('{amount}', comission));

			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.investConfirmFill = investConfirmFill;
		
		function startCancelTimeout() {
			var cancelTimeOut = self.cancelTimeOut * 1000;
			var end = new Date().getTime() + (cancelTimeOut);
			self.cancelTimeLeft = cancelTimeOut;
			function doIt(){
				self.cancelTimeLeftPercent = 100 - (self.cancelTimeLeft / cancelTimeOut) * 100;
				if (self.cancelTimeLeftPercent > 100) {self.cancelTimeLeftPercent = 100;}
				g('cancel-inv-time-left-percent_' + self.box).style.width = self.cancelTimeLeftPercent + '%';
				
				if (self.cancelTimeLeft > 0) {
					self.cancelTimeLeft = end - new Date().getTime();
					if (self.cancelTimeLeft < 0) {
						self.cancelTimeLeft = 0;
					}
					self.cancelInvTimerLoader = setTimeout(doIt, 10);
				}
				var seconds = Math.floor(self.cancelTimeLeft / 1000);
				if (seconds == 0) {
					seconds = '00';
				} else if (seconds < 10) {
					seconds = '0' + seconds;
				}
				var milSec = Math.round(self.cancelTimeLeft - (seconds * 1000));
				if (milSec == 0) {
					milSec = '000';
				} else if (milSec < 10) {
					milSec = '00' + milSec;
				} else if (milSec < 100) {
					milSec = '0' + milSec;
				}
				//g('cancel-inv-time-left-sec_' + self.box).innerHTML = '00:' + seconds + ':' + milSec;
				g('cancel-inv-time-left-sec_' + self.box).innerHTML = '00:' + seconds;
			}
			doIt();
		}
		this.startCancelTimeout = startCancelTimeout;
		
		this.cancelInvOneTime = false;
		
		function cancelInv() {
			if (!self.cancelInvOneTime) {
				self.cancelInvOneTime = true;
				
				clearTimeout(self.cancelInvTimer);
				clearTimeout(self.cancelInvTimerLoader);
	
				var InvestmentMethodResult = jsonClone(getUserMethodRequest(), {
					investmentId:self.investmentId,
					opportunityId: self.boxOpportunities[self.oppArrPos].ET_OPP_ID,
					amount: self.invest * Math.pow(10,settings.decimalPointDigits),
					pageLevel: self.chartPointsInv_last.level,
					oddWin: (1+(self.profit/100)),
					oddLose: (self.refund/100),
					choice: self.chartPointsInv_last.type,
					fromGraph: fromGraph
				});
				$.ajax({
					url     : settings.jsonLink + "cancelInvestment",
					type    : "POST",
					dataType: "json",
					data    : JSON.stringify(InvestmentMethodResult),
					contentType: "application/json;charset=utf-8",
					success : function(data) {
						//Successful call
						if (data.errorCode == 0) {
							changeStates({'state':'cancel-inv-msg'});
							angular.element($('html')).scope().getCopyopUser({onStateChange: true, stateMachine: false});
							self.cancelInvMsgsOverlayTimer = setTimeout(function() {
								hideCancelInvMsg();
							}, 3000);
							self.inProgress = false;
							self.cancelInvOneTime = false;
							angular.element(g('settledOptionsTab')).scope().getInvestments();
						} else {
							self.inProgress = false;
							self.cancelInvOneTime = false;
							
							changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
							
							addInvToChart();
						}
					},
					error   : function( xhr, err ) {
						//Failed call
						self.inProgress = false;
						self.cancelInvOneTime = false;
						
						changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
						
						addInvToChart();
					}
				});
			}
		}
		
		g('cancel-cool-close_' + this.box).onclick = function() {
			hideCancelInvMsg();
		}
		
		function hideCancelInvMsg() {
			handleMessages({'type':'other'})
		}
		this.hideCancelInvMsg = hideCancelInvMsg;

		g('hide-forever_' + this.box).onclick = function() {
			disableCanacelInv();
		}
		
		function disableCanacelInv() {
			$.ajax({
				url     : settings.jsonLink + "changeCancelInvestmentCheckboxState",
				type    : "POST",
				dataType: "json",
				data    : JSON.stringify(getMethodRequest()),
				contentType: "application/json;charset=utf-8",
				success : function(data) {
					//Successful call
					settings.showCnacelInv = false;
				},
				error   : function( xhr, err ) {
					//Failed call

				}
			});
		}
		this.disableCanacelInv = disableCanacelInv;
		
		
		//add onclick event to call and put
		// this.addDisableCallPutButn = addDisableCallPutButn;
		function addDisableCallPutButn(disable){
			try{
				logIt({'type':1,'msg':'addDisableCallPutButn box: '+self.box+' - disable: '+disable});
				if(!disable){
					g('tradeBox_call_btn_'+self.box).onclick = function(){
						changeStates({'state':'call_processing'});
					}
					g('tradeBox_put_btn_'+self.box).onclick = function(){
						changeStates({'state':'put_processing'});
					}
				}else{
					g('tradeBox_call_btn_'+self.box).onclick = function(){
						return false;
					}
					g('tradeBox_put_btn_'+self.box).onclick = function(){
						return false;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		addDisableCallPutButn();
		
		//change call put button
		// this.changeStatesBtn = changeStatesBtn;
		this.currentCallState = 'active';
		this.currentPutState = 'active';
		function changeStatesBtn(state){//state{active,call_processing,off,put_processing}
			try{
				logIt({'type':1,'msg':'changeStatesBtn box: '+self.box+' - state: '+state});
				var call = g('tradeBox_call_btn_'+self.box);
				var put = g('tradeBox_put_btn_'+self.box);
				if (!isUndefined(call)) {
					switch(state){
						case 'call_processing':
							call.className = call.className.replace(self.currentCallState,'proc_on');self.currentCallState = 'proc_on';
							put.className = put.className.replace(self.currentPutState,'proc_off');self.currentPutState = 'proc_off';
							addDisableCallPutButn(true);
							break;
						case 'put_processing':
							call.className = call.className.replace(self.currentCallState,'proc_off');self.currentCallState = 'proc_off';
							put.className = put.className.replace(self.currentPutState,'proc_on');self.currentPutState = 'proc_on';
							addDisableCallPutButn(true);
							break;
						case 'off':
							call.className = call.className.replace(self.currentCallState,'off');self.currentCallState = 'off';
							put.className = put.className.replace(self.currentPutState,'off');self.currentPutState = 'off';
							addDisableCallPutButn(true);
							break;
						default:
							call.className = call.className.replace(self.currentCallState,'active');self.currentCallState = 'active';
							put.className = put.className.replace(self.currentPutState,'active');self.currentPutState = 'active';
							addDisableCallPutButn(false);
							break;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//change state
		this.currentStates = {'state':'','errorCode':0,'errorType':''};
		function changeStates(params){//state{default,call_processing,put_processing,error,confirm,waiting_for_exp,expired}
			try{
				logIt({'type':1,'msg':{'msg':'changeStates box: '+self.box,'result':params.result}});
				switch(params.state){
					case 'call_processing'://press call button
						optimoveReportTradeEvent({from: "trade_call", box: self});
						changeStatesBtn(params.state);
						disableInvest(true);
						if((self.chart != null) || (self.boxOpportunities.length > 0 && self.boxOpportunities[self.oppArrPos].ET_SCHEDULED != '1')){
							doInvest(1,false);
							self.currentStates = params.state;
						}
						else{
							changeStates({'state':''});
						}
						break;
					case 'put_processing'://press put button
						optimoveReportTradeEvent({from: "trade_put", box: self});
						changeStatesBtn(params.state);
						disableInvest(true);
						if((self.chart != null) || (self.boxOpportunities.length > 0 && self.boxOpportunities[self.oppArrPos].ET_SCHEDULED != '1')){
							doInvest(2,false);
							self.currentStates = params.state;
						}
						else{
							changeStates({'state':''});
						}
						break;
					case 'error'://invest returned error
						handleMessages({'type':params.errorType,'errorCode':params.errorCode,'errorMsg':params.errorMsg});
						disableInvest(false);
						changeStatesBtn('active');
						self.currentStates = params.state;
						break;
					case 'confirm'://confirm invest
						changeStatesBtn('active');
						handleMessages({'type':'other','errorCode':3,'result':{'sms':params.sms,'level':params.level,'color':params.color}});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv'://cancel inv
						changeStatesBtn('active');
						handleMessages({'type':'other','errorCode':4,'result':{'level':params.level,'color':params.color}});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv-msg'://cancel inv msg
						changeStatesBtn('active');
						handleMessages({'type':'other','errorCode':5});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'cancel-inv-error'://cancel inv error
						changeStatesBtn('active');
						handleMessages({'type':'other','errorCode':6});
						disableInvest(false);
						self.currentStates = params.state;
						break;
					case 'waiting_for_exp'://waiting for expiry
						changeStatesBtn('off');
						disableInvest(true);
						handleMessages({'type':'chartHeader','errorCode':3});
						self.currentStates = params.state;
						break;
					case 'expired'://expired
						changeStatesBtn('off');
						disableInvest(true);
						handleMessages({'type':'chartHeader','errorCode':2,'ao_level':params.ao_level});
						self.currentStates = params.state;
						break;
					default:
						changeStatesBtn('active');
						disableInvest(false);
						handleMessages({'type':'popup'});
						handleMessages({'type':'other'});
						handleMessages({'type':'chartHeader','errorCode':1});
						self.currentStates = params.state;
						break;
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.changeStates = changeStates;
		
		// disable drop downs for invest and profit
		// this.disableInvest = disableInvest;
		this.disableInvestCurrent = 'active';
		function disableInvest(disable){
			try{
				logIt({'type':1,'msg':'disableInvest box: '+self.box+' - disable: '+disable});
				var state = '';
				var tradeBox_bottom = g('tradeBox_bottom_'+self.box);
				var tradeBox_incorrect = g('tradeBox_incorrect_'+self.box);
				if(disable){
					state = 'disabled';
				}
				else{
					state = 'active';
				}
				tradeBox_bottom.className = tradeBox_bottom.className.replace(self.disableInvestCurrent,state);
				tradeBox_incorrect.className = tradeBox_incorrect.className.replace(self.disableInvestCurrent,state);
				self.disableInvestCurrent = state;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//invest
		// this.doInvest = doInvest;
		this.doInvestSubmitParams = '';
		function doInvest(type,dev2){
			try{
				if (!settings.loggedIn) {//not logged in
					changeStates({'state':'error','errorType':'popup','errorCode':2});
				// } else if (settings.balance < self.invest) {//not enough money
					// changeStates({'state':'error','errorType':'popup','errorCode':3});
				} else {
					
					if(!dev2){
						self.submited = true;
						self.inProgress = true;
						clearTimeout(self.closeSlipTimer);
						handleMessages({'type':'other'});
						// if((self.market.schedule == '1') || (self.market.schedule == 2) || (self.boxType == 3)){
							var investLevel = self.boxOpportunities[self.oppArrPos].ao_level_float;
							var investColor = self.boxOpportunities[self.oppArrPos].ao_clr_dsp;
							self.chartPointsInv_last = {
								type: type,
								date: (self.chart != null) ? self.lastUpdate.x : '',
								level: investLevel,
								color: investColor,
								ask: self.boxOpportunities[self.oppArrPos].ASK,
								bid: self.boxOpportunities[self.oppArrPos].BID,
								last: self.boxOpportunities[self.oppArrPos].LAST
							};
						// }
						self.dev2Second = false;
					}else{
						self.dev2Second = true;
					}
					logIt({'type':1,'msg':'doInvest box: '+self.box+' - params: '+self.doInvestSubmitParams});
					var req = jsonClone(getUserMethodRequest(),{
							opportunityId: self.boxOpportunities[self.oppArrPos].ET_OPP_ID,
							requestAmount: self.invest,
							defaultAmountValue: settings.defaultAmountValue,
							pageLevel: self.chartPointsInv_last.level,
							pageOddsWin: (1+(self.profit/100)),
							pageOddsLose: (self.refund/100),
							choice: type,
							isFromGraph: fromGraph, 
							dev2Second: self.dev2Second,
							// apiExternalUserId:
							// copyopInvId:
						});
					var ajaxCall = new aJaxCall_fn({
						'type':'POST',
						'url':settings.jsonLink + 'insertInvestment',
						'params': JSON.stringify(req),
						'callback':doInvestCallback
					});
					ajaxCall.doGet();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//invest callback
		// this.doInvestCallback = doInvestCallback;
		// status:{'0':'sthing','1':'not login','2':'some error','3':'OK','4':'redirect'}
		function doInvestCallback(response){
			try{
				response = eval('('+response+')');
				logIt({'type':1,'msg':{'msg':'doInvestCallback box: '+self.box,'response':response}});
				angular.element('html').scope().setUser(response);
				if (response.dev2Seconds > 0) {
					self.investmentId = 0;
					var timeDEV2 = response.dev2Seconds;
					if (!settings.showCnacelInv) {
						handleMessages({'type':'popup','errorCode':6,'level':self.chartPointsInv_last.level,'time':timeDEV2});
						doInvest(self.chartPointsInv_last.type,true);
					} else {
						self.cancelTimeOut = (parseInt(response.cancelSeconds) > parseInt(timeDEV2)) ? response.cancelSeconds : timeDEV2;
						changeStates({'state':'cancel-inv','level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});

						self.cancelInvTimer = setTimeout(function() {
							doInvest(self.chartPointsInv_last.type,true);
						}, self.cancelTimeOut * 1000);
					}
				} else {
					handleMessages({'type':'popup'});
					self.submited = false;
					self.tradeBox_level_html.innerHTML = self.boxOpportunities[self.oppArrPos].ao_level;
					if (response.errorCode == 0) {
						self.cancelTimeOut = response.cancelSeconds;
						// refreshInvProfitBox();TODO
						self.smsEnable = 0;//TODO no sms
						// var investmentId = response.substring(2);
						// if (investmentId.indexOf("SMS=") > -1) {
							// investmentId = investmentId.substring(0, investmentId.length - 5);
							// if (response.substring(response.length-1) == "1") {
								// self.smsEnable = 1;
							// }
						// }

						clearTimeout(self.investOverlayTimer);
						
						self.investmentId = response.investment.id;
						
						if (!settings.showCnacelInv || self.dev2Second) {
							//changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
							
							addInvToChart();
						} else {
							changeStates({'state':'cancel-inv','level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});

							self.cancelInvTimer = setTimeout(function() {
								//changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
								
								addInvToChart()
								
							}, self.cancelTimeOut * 1000);
						}
					} else if (response.errorCode == errorCodeMap.regulation_missing_questionnaire || 
								response.errorCode == errorCodeMap.regulation_suspended_documents || 
								response.errorCode == errorCodeMap.regulation_user_restricted || 
								response.errorCode == errorCodeMap.reg_suspended_quest_incorrect ||
								response.errorCode == errorCodeMap.regulation_user_pep_prohibited ||
								response.errorCode == errorCodeMap.regulation_user_is_treshold_block || 
								response.errorCode == errorCodeMap.regulation_suspended_cnmv_docs) {
						try {
							changeStates({
								state : ""
							})
							angular.element('html').scope().closeCopyOpPopup();
							angular.element('html').scope().stateMachine({});
						} catch(e) {}
					} else if (response.errorCode == errorCodeMap.validation_low_balance) {
						changeStates({'state':'error','errorType':'popup','errorCode':3});
						self.inProgress = false;
					} else if (response.errorCode == errorCodeMap.no_cash_for_niou) {
						changeStates({'state':'error','errorType':'popup','errorCode':7, errorMsg: response.userCashBalance});
						self.inProgress = false;
					} else {
						var msg;
						if (response.userMessages != null && response.userMessages.length > 0) {
							if (self.dev2Second) {
								changeStates({'state':'cancel-inv-error'});
								setTimeout(function() {
									$('#chart-overlays_' + self.box).removeClass('cancel-inv-error');
								}, 2000);
							} else {
								msg = response.userMessages[0].message;
								changeStates({'state':'error','errorType':'other','errorCode':1,'errorMsg':msg});
							}
						} else {
							msg = 'Error code: ' + response.errorCode;
							changeStates({'state':'error','errorType':'other','errorCode':1,'errorMsg':msg});
						}
						self.inProgress = false;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function addInvToChart() {
			self.chartPointsInv_last.point = 'point_'+self.investmentId;
			addPointToChart(
				self.chartPointsInv_last.date,
				self.chartPointsInv_last.level,
				self.chartPointsInv_last.point,
				self.chartPointsInv_last.ask,
				self.chartPointsInv_last.bid,
				self.chartPointsInv_last.last,
				1
			);
			self.chartPointsInv.push(self.chartPointsInv_last);
			redrowChart();

			defaultMarkets[self.box] = null;
			chartGetHistory(1,true);

			setDefaultValueToAllBoxes();
			changeStates({'state':'confirm','sms':self.smsEnable,'level':self.chartPointsInv_last.level,'color':self.chartPointsInv_last.color});
			angular.element(g('optionOptionsTab')).scope().getInvestments();
			angular.element('html').scope().copyopUser.user.defaultAmountValue = self.invest * Math.pow(10,settings.decimalPointDigits);
			angular.element('html').scope().copyopUser.defaultAmountValueDsp = formatAmount(self.invest, 0);
			
			self.inProgress = false;
		}
		
		function startDEV2Timer(time){
			try{
				var seconds_to_wait = time * 1000;
				var bgr = g('tradeBox_dev2_progress_'+self.box);
				var date_end = new Date().getTime() + seconds_to_wait;
				var time_left = seconds_to_wait;
				function doIt(){
					bgr.style.width = 100-(time_left/seconds_to_wait)*100+'%';
					
					if(time_left > 0){
						var interval = 10;
						time_left = date_end - new Date().getTime();
						self.dev2Timer = setTimeout(doIt,interval);
					}
				}
				doIt();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function setDefaultValueToAllBoxes(){
			for(var box=0; box<group.length; box++){
				if(box != self.box){
					var lis = g('tradeBox_invest_drop_ul_'+box).getElementsByTagName('li');
					clearActiveList(lis);
					for(var i=0; i<lis.length; i++){
						if(lis[i].getAttribute('data-amount') != null){
							var amount = parseFloat(lis[i].getAttribute('data-amount').replace(/,/g,''));
							if(amount == self.invest){
								lis[i].className += " active";
								var flag = false;
							}
						}
					}
					g('tradeBox_inv_amount_'+box).innerHTML = formatAmount(self.invest,3);
					box_objects[box].invest = self.invest;
					box_objects[box].updateProfitAmount();
				}
			}
		}
		//time left
		this.time_invest_d = g('tradeBox_time_invest_d_'+this.box);
		this.time_invest_h = g('tradeBox_time_invest_'+this.box);
		// this.updateTimeLeft = updateTimeLeft;
		this.timeLeftTimer_var = null;
		function updateTimeLeft(){
			try{
				self.timeLastInv_mil = adjustFromUTCToLocal(new Date(self.boxOpportunities[self.oppArrPos].ET_LAST_INV)).getTime() - settings.serverOffsetMillis;
				self.timeClosing_mil = adjustFromUTCToLocal(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE)).getTime() - settings.serverOffsetMillis;
				
				if (self.optionChanged && self.chart) {
					self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = self.timeClosing_mil-(1000*60*60)+self.serverOffset;
					self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;
	
					setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
				}
				
				clearTimeout(self.timeLeftTimer_var);
				timeLeftTimer();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function timeLeftTimer(){
			try{
				//time left in miliseconds
				// self.timeLeftToInvest = (self.timeLastInv_mil - new Date().getTime() - settings.serverOffsetMillis)/1000;
				self.timeLeftToInvest = (self.timeLastInv_mil - new Date().getTime())/1000;
				if (self.timeLeftToInvest < 0) {
					self.timeLeftToInvest = 0;
				}
				
				//minutes
				var min = Math.floor(self.timeLeftToInvest / 60);
				//hours
				var hours = 0;
				if (min > 60) {
					hours = Math.floor(min / 60);
					min = min - (hours * 60);
				}
				//days
				var days = 0;
				if (hours > 23) {
					days = Math.ceil(hours / 24);
					hours = hours - (days * 24);
				}
				
				if (min < 10){
					var minDsp = '0'+min;
				} else {
					var minDsp = min;
				}
				
				//seconds
				var sec = Math.floor(self.timeLeftToInvest - (min * 60));
				var secInt = sec;
				if (sec < 10) {
					sec = '0' + sec;
				}

				if (days > 0) {
					var day_txt
					if (days == 1) {
						day_txt = oldTexts.day_to_invest;
					} else {
						day_txt = oldTexts.days_to_invest;
					}
					self.time_invest_d.innerHTML = '<span class="in">' + days + '</span>' + day_txt;
				} else if (hours > 0) {
					self.time_invest_d.innerHTML = '<span class="in">' + hours + ":" + minDsp + '</span>' + oldTexts.hours_to_invest;
				} else {
					self.time_invest_d.innerHTML = '<span class="in">' + minDsp + ":" + sec + '</span>' + oldTexts.min_to_invest;
				}
				self.time_invest_h.className = self.time_invest_h.className.replace(/ lastMin/g,'');
				
				if(self.boxOpportunities.length > 0){
					if((self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.opened) || (self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.last_10_min) || (self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.created)){
						if(((min == 0) && (secInt == 0)) || (min > 60)){
							handleMessages({'type':'chartHeader','errorCode':''});
						}
						else if(self.timeLeftToInvest <= self.lastSecondsToInvest){
							self.time_invest_h.className += " lastMin";
							handleMessages({'type':'chartHeader','errorCode':1});
						}
						if((min != 0) && (secInt != 0)){
							handleMessages({'type':'chartHeader','errorCode':1});
						}
					}
				}
				self.timeLeftTimer_var = setTimeout(timeLeftTimer,1000);
				updateChartTimeLeft();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//receive update from ls
		function updateState(params){
			try{
				if(params.oppId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){//make changes only if the update is for the current oppId
					logIt({'type':1,'msg':"state changed - id: " + params.oppId + " marketId: " + self.marketId + " state: " + params.et_state + " time: " + params.last_invest + " timeLastInvest: " + params.et_est_close + " suspendMessage: " + params.et_suspended_message});
				
					if(params.et_state == OPPORTUNITY_STATE.created || params.et_state == OPPORTUNITY_STATE.paused || params.et_state == OPPORTUNITY_STATE.waiting_to_pause
						|| self.boxOpportunities[self.oppArrPos].FILTER_NEW_SCHEDULED === '00000'){//state 1,8,10 or all opportunities are closed
						handleMessages({'type':'popup','errorCode':1,'expTime':formatDate(new Date(params.et_est_close), false)});
						self.marketIdManual = false;
					}
					else if(params.et_state >= OPPORTUNITY_STATE.opened && params.et_state <= OPPORTUNITY_STATE.closed){//between state 2 and 6
						if((params.et_state == OPPORTUNITY_STATE.opened) || (params.et_state == OPPORTUNITY_STATE.last_10_min)){//state 2,3 - open
							if(!self.submited){
								changeStates({'state':''});
							}
						}
						else if((params.et_state == OPPORTUNITY_STATE.closing_1_min) || (params.et_state == OPPORTUNITY_STATE.closing)){//state 4,5 - waiting for expiry
							if (!isUndefined(self.allInvestmentsForOpp) && self.allInvestmentsForOpp.length == 0 && self.alternateOpps.length > 0 && !params.forceStateWaiting) {
								setNextOpp();
							} else {
								self.stateDps = 'waiting';
								changeStates({'state':''});//clean loading
								changeStates({'state':'waiting_for_exp'});
							}
						} else {//state 6 - expired
							if (!isUndefined(self.allInvestmentsForOpp) && self.allInvestmentsForOpp.length == 0 && self.alternateOpps.length > 0 && !params.forceStateWaiting) {
								setNextOpp();
							} else {
								self.stateDps = 'inactive';
								playSound(3);
								changeStates({'state':''});//clean loading
								changeStates({'state':'expired','ao_level':params.ao_level});
							}
						}
					}
					else {//state 9,10
						if(params.et_suspended_message == null){
							params.et_suspended_message = self.boxOpportunities[self.oppArrPos][SUBSCR_FLD.et_suspended_message];
						}
						var sperator = "<br/>";
						var arr = params.et_suspended_message.split("s");
						var str = suspendMsgUpperLine[new Number(arr[0]) - 1] + sperator + suspendMsgLowerLine[new Number(arr[1]) - 1];
						if (arr.length == 3 && arr[2].length > 0) {
							str += " " + formatDate(new Date(arr[2]), false);
						}
						handleMessages({'type':'popup','errorCode':5,'errorMsg':str}); 
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateState = updateState;
		
		function setNextOpp() {
			self.stateDps = 'active';
			var index = searchJsonKeyInArray(self.alternateOpps, 'oppId', self.oppId);
			var nextOpp = 0;
			if (index > -1) {
				nextOpp = index + 1;
				if (nextOpp == self.alternateOpps.length) {nextOpp = self.alternateOpps.length-1;}
			}
			logIt({'type':1,'msg':'Alternate Hourly Option change: oppId = ' + self.alternateOpps[nextOpp].oppId});
			
			var tmp_oppId = self.alternateOpps[nextOpp].oppId;
			self.resetBox(true);
			self.oppId = tmp_oppId;
			self.oppArrPos = searchJsonKeyInArray(self.boxOpportunities,'ET_OPP_ID',self.oppId);
			
			self.updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
			
			// if(self.boxType != 1){
				// tradeBox_getBalace(self.box);
			// }
		}
		
		//update Profit/Refun Dropdown menu
		function updateProfitRefunDrop(){
			try{
				self.optionChanged = false;
				if (self.boxOpportunities[self.oppArrPos].ET_ODDS_GROUP != null) {
					var odds_group = self.boxOpportunities[self.oppArrPos].ET_ODDS_GROUP.trim().split(' ');
					self.odds_group_selected = '';
					for (var i = 0; i < odds_group.length; i++) {
						if (odds_group[i].search('d') > -1) {
							self.odds_group_selected = i;
							odds_group[i] = odds_group[i].replace('d', '');
						}
					}
					var tradeBox_profit_drop_amount = g('tradeBox_profit_drop_amount_' + self.box)
					if(odds_group.length <= 0){
						tradeBox_profit_drop_amount.onclick = function(){
							return false;
						}
						tradeBox_profit_drop_amount.className = tradeBox_profit_drop_amount.className.replace(/ disabled/g,'');
						tradeBox_profit_drop_amount.className += " disabled";
						return;
					}
					g('tradeBox_profit_drop_amount_' + self.box).onclick = function(){
						openElement({'span':this,'autoClose':true});
					}
					tradeBox_profit_drop_amount.className = tradeBox_profit_drop_amount.className.replace(/ disabled/g,'');
					var ul = g('tradeBox_profit_drop_ul_'+self.box);
					var lis = ul.getElementsByTagName('li');
					for(var i=1; i<lis.length; i++){
						if(i <= odds_group.length){
							var profit = returnRefund_all[odds_group[i-1]].profit;
							var refund = returnRefund_all[odds_group[i-1]].refund;
							lis[i].setAttribute('data-profit',profit);
							lis[i].setAttribute('data-refund',refund);
							var span = lis[i].getElementsByTagName('span');
							span[0].innerHTML = profit+'%';
							span[1].innerHTML = refund+'%';
							if (self.odds_group_selected != '') {
								if (self.odds_group_selected == (i - 1)) {
									lis[i].className = " active";
									self.profit = profit;
									self.refund = refund;
								} else {
									lis[i].className = lis[i].className.replace(/ active/,'');
								}
							} else {
								if (profit == self.profit && refund == self.refund) {
									lis[i].className = " active";
								} else {
									lis[i].className = lis[i].className.replace(/ active/,'');
								}
							}
						}
						else{
							ul.removeChild(lis[i]);
						}
					}
					for(i; i<=odds_group.length; i++){
						var profit = returnRefund_all[odds_group[i-1]].profit;
						var refund = returnRefund_all[odds_group[i-1]].refund;
						if (self.odds_group_selected != '') {
							if (self.odds_group_selected == (i - 1)) {
								var className = " active";
								self.profit = profit;
								self.refund = refund;
							} else {
								var className = "";
							}
						} else {
							var className = (profit == self.profit && refund == self.refund)?" active":"";
						}
						ul.innerHTML += '<li class="'+className+'" data-profit="'+profit+'" data-refund="'+refund+'"><span class="tradeBox_profit_drop_left tradeBox_profit_drop_left_'+skinMap[settings.skinId].locale+'">'+profit+'%</span><span class="tradeBox_profit_drop_right tradeBox_profit_drop_right_'+skinMap[settings.skinId].locale+'">'+refund+'%</span></li>';
					}
					addFnToProfitDrop();
					g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
					updateProfitAmount();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//update exp dropdown
		function updateExpDropAltHour(){
			try {
				var $ul = $('#tradeBox_menu_exp_' + self.box);
				var $lis = $ul.find('li');
				if (!isUndefined(self.boxOpportunities[self.oppArrPos].AO_NH) && self.boxOpportunities[self.oppArrPos].AO_NH != '') {
					self.alternateOpps = self.boxOpportunities[self.oppArrPos].AO_NH.split(';')
						.map(function(current) {
							var temp = current.split('|');
							return {
								oppId: temp[0],
								expTime: adjustFromUTCToLocal(new Date(temp[1]))
							}
						});

					var $altHours = $ul.find('._altHours');
					if (self.alternateOpps.length > $altHours.length) {
						for (var i = $altHours.length; i < self.alternateOpps.length; i++) {
							$('<li class="_altHours" data-standart="0" data-value="1"></li>').insertBefore($lis[1]);
						}
					} else if ($altHours.length > self.alternateOpps.length) {
						for (var i = $altHours.length - 1; i >= self.alternateOpps.length; i--) {
							$altHours[i].remove();
						}
					}
					
					var index = 0;
					$ul.find('._altHours')
						.each(function() {
							setProperties($(this), index);
							index++;
						});
						
					
					function setProperties($element, index) {
						var date = self.alternateOpps[index].expTime;
						var h = date.getHours();
						if (h < 9) {h = '0' + h;}
						var m = date.getMinutes();
						if (m < 9) {m = '0' + m;}
						
						$element.html($element.parent().attr('data-text').replace('{est_closing}', h + ':' + m));
						$element.attr('data-opp-id', self.alternateOpps[index].oppId);
						
						$element.unbind();
						$element.click(function() {
							if (!self.inProgress) {
								$('#tradeBox_menu_exp_' + self.box + ' > li').removeClass('selected');
								$(this).addClass('selected');
								updateExpTimeOpp(this);
							}
						})
					}
				}
				
				
				//quorterly
				self.quorterlyOpps = [];
				if (!isUndefined(self.boxOpportunities[self.oppArrPos].AO_NQ) && self.boxOpportunities[self.oppArrPos].AO_NQ != '') {
					self.quorterlyOpps = self.boxOpportunities[self.oppArrPos].AO_NQ.split(';')
						.map(function(current) {
							var temp = current.split('|');
							return {
								oppId: temp[0],
								expTime: adjustFromUTCToLocal(new Date(temp[1]))
							}
						});

					var $quorterly = $ul.find('._quorterly');
					if (self.quorterlyOpps.length > $quorterly.length) {
						for (var i = $quorterly.length; i < self.quorterlyOpps.length; i++) {
							$ul.append($('<li class="_quorterly" data-standart="0" data-value="6"></li>'));
						}
					} else if ($quorterly.length > self.quorterlyOpps.length) {
						for (var i = $quorterly.length - 1; i >= self.quorterlyOpps.length; i--) {
							$quorterly[i].remove();
						}
					}
					
					var index = 0;
					$ul.find('._quorterly')
						.each(function() {
							setPropertiesQuorterly($(this), index);
							index++;
						});
						
					
					function setPropertiesQuorterly($element, index) {
						var date = self.quorterlyOpps[index].expTime;
						var y = date.getFullYear();
						var m = date.getMonth();
						var q = Math.floor(m / 4) + 1;
						
						$element.html($element.parent().attr('data-text-quorterly') + ' Q' + q + ' ' + y);
						$element.attr('data-opp-id', self.quorterlyOpps[index].oppId);
						$element.attr('id', 'quorterly-' + self.quorterlyOpps[index].oppId);
						
						$element.unbind();
						$element.click(function() {
							if (!self.inProgress) {
								$('#tradeBox_menu_exp_' + self.box + ' > li').removeClass('selected');
								$(this).addClass('selected');
								updateExpTimeOpp(this);
							}
						})
					}
				} else {
					var $quorterly = $ul.find('._quorterly');
					for (var i = 0; i < $quorterly.length; i++) {
						$quorterly[i].remove();
					}
				}
				updateExpDrop();
			} catch(e) {
				logIt({'type':3,'msg':e});
			}
		}
		this.updateExpDropAltHour = updateExpDropAltHour;
		
		//update expiry dropdown
		function updateExpDrop(){
			try{
				if(self.boxType != 1 && self.boxType != 4){
					return;
				}
				var scheduled = self.boxOpportunities[self.oppArrPos].FILTER_NEW_SCHEDULED;
				var lis = g('tradeBox_menu_exp_'+self.box).getElementsByTagName('li');
				var index = scheduled.length;
				for (var i = 0; i < lis.length; i++) {
					var type = lis[i].getAttribute('data-value');
					if (type == 6) {type = 5;}
					
					if (scheduled[index - parseInt(type)] == '1') {
						lis[i].style.display = "block";
					} else {
						lis[i].style.display = "none";
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}

		//put value into the html
		function updateVisual(params){
			try{
				if(self.boxOpportunities.length > 0 && !isUndefined(self.boxOpportunities[self.oppArrPos])){
					self.boxFull = true;
					//update market name
					if(self.boxType == 1 || self.boxType == 4 || self.boxType == 5){
						var tradeBox_marketName = g('tradeBox_marketName_'+self.box);
						if (tradeBox_marketName != null) {
							tradeBox_marketName.innerHTML = self.boxOpportunities[self.oppArrPos].ET_NAME;
						}
						tradeBox_marketName = null;
					}
					//update current level
					if(self.boxOpportunities[self.oppArrPos].ao_level == 0){
						self.tradeBox_level_html.innerHTML = "wait...";
					}
					else{
						if(!self.submited){
							self.tradeBox_level_html.innerHTML = self.boxOpportunities[self.oppArrPos].ao_level;
						}
					}
					//update exp time
					if(self.boxType == 1 || self.boxType == 4){
						if(self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.created){
							var text = formatDate(new Date(self.boxOpportunities[self.oppArrPos].ET_EST_CLOSE), true);
							g('trade_box_exp_time_'+self.box).innerHTML = text;
							try{
								g('trade_box_exp_time2_'+self.box).innerHTML = text;
							} catch(e){}
						}
					}
					//update default profit refound
					if(self.boxType == 1 || self.boxType == 4){
						if(self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.created || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.waiting_to_pause || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.paused || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.suspended){
							g('tradeBox_menu_exp_h_'+self.box).style.display = "none";
						}
						else{
							g('tradeBox_menu_exp_h_'+self.box).style.display = "block";
						}
					}
					// if(self.profit === 0 || self.refund === 0){
						// self.profit = Math.round((parseFloat(self.boxOpportunities[self.oppArrPos].ET_ODDS_WIN)-1)*100);
						// self.refund = Math.round(parseFloat(self.boxOpportunities[self.oppArrPos].ET_ODDS_LOSE)*100);
					// }
					//update level colors
					if(self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.created && self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.paused && self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.waiting_to_pause && self.boxOpportunities[self.oppArrPos].ET_STATE != OPPORTUNITY_STATE.suspended){
						switch(self.boxOpportunities[self.oppArrPos].ao_clr_dsp){
							case '0':var color = " below";break;
							case '1':var color = " same";break;
							case '2':var color = " above";break;
						}
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ below/g,'');
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ above/g,'');
						self.tradeBox_level_html.className = self.tradeBox_level_html.className.replace(/ same/g,'');
						self.tradeBox_level_html.className += color;
					}
					//update flags (show/hide trends)
					if(this.boxType == 1 || this.boxType == 4){
						if (params.aoFlagsChanged || params.stateReallyChanged) {
							var tradeBox_trends = g('tradeBox_trends_'+self.box);
							if(self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.opened || self.boxOpportunities[self.oppArrPos].ET_STATE == OPPORTUNITY_STATE.last_10_min){
								if(((self.boxOpportunities[self.oppArrPos].AO_FLAGS) & 2) != 0) {
									tradeBox_trends.style.display = "block";
								}
								else{
									tradeBox_trends.style.display = "none";
								}
							}
							else {
								tradeBox_trends.style.display = "none";
							}
						}
					}
					//udpdate trend diagram
					if(this.boxType == 1 || this.boxType == 4){
						if(params.callsTrendChanged && self.boxOpportunities[self.oppArrPos].ao_calls_trend_dep != null) {
							var trends_call = Math.round(self.boxOpportunities[self.oppArrPos].ao_calls_trend_dep * 100);
							var trends_put = 100 - trends_call;
							
							var tradeBox_trends_graph_proc_call = g('tradeBox_trends_graph_proc_call_'+self.box);
							var tradeBox_trends_graph_sep = g('tradeBox_trends_graph_sep_'+self.box);
							if(trends_call === 0){
								tradeBox_trends_graph_proc_call.style.display = "none";
								tradeBox_trends_graph_sep.style.display = "none";
							}else{
								tradeBox_trends_graph_sep.style.display = "block";
								tradeBox_trends_graph_proc_call.style.display = "block";
								tradeBox_trends_graph_proc_call.innerHTML = trends_call+"%";
								tradeBox_trends_graph_proc_call.style.left = (trends_call/2)+"%";
							}
							var tradeBox_trends_graph_proc_put = g('tradeBox_trends_graph_proc_put_'+self.box);
							if(trends_put === 0){
								tradeBox_trends_graph_proc_put.style.display = "none";
							}else{
								tradeBox_trends_graph_proc_put.style.display = "block";
								tradeBox_trends_graph_proc_put.innerHTML = trends_put+"%";
								tradeBox_trends_graph_proc_put.style.right = (trends_put/2)+"%";					
							}
							
							g('tradeBox_trends_graph_put_'+self.box).style.width = trends_put+'%';
						}
					}
					
					//update current return
					updateCurrentReturn();
				
					updateTimeLeft();
					//update profit/refund dropdown
					if(self.optionChanged){
						if(this.boxType != 3){
							g('tradeBox_profit_procent_'+self.box).innerHTML = self.profit+'%';
							updateProfitRefunDrop();
						}
						else{
							g('tradeBox_profit_dsp_'+self.box).innerHTML = self.profit+"%";
							updateProfitAmount();
						}
						if(this.boxType == 1 ||this.boxType == 4){
							updateExpDrop();
						} else if (this.boxType == 4 || this.boxType == 5) {
							if (self.boxOpportunities[self.oppArrPos].ET_NH != null) {
								var opp_info = self.boxOpportunities[self.oppArrPos].ET_NH.split('|');
								self.nh_oppId = opp_info[0];
							}
						}
					}
					updateExpDropAltHour();
					if (self.chart == null) {
						initGraph();
					}
				}
				else{
					handleMessages({'type':'popup','errorCode':4});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateVisual = updateVisual;
		
		//init graph
		function initGraph(){
			if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 8.") != -1)) {
				var color = 'white';
			}
			else{
				var color = 'transparent';
			}
			try{
				self.chart = new Highcharts.StockChart({
					credits: {
						enabled: false
					},
					chart: {
						renderTo: 'tradeBox_chart_'+self.box,
						margin: 0,
						height: self.chart_properties.height,
						backgroundColor: color,
						panning: false
					},
					rangeSelector : {
						enabled : false,
						selected: 0
					},
					scrollbar : {
						enabled : false
					},
					navigator : {
						enabled : false
					},
					xAxis: [{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							x: 0,
							y: self.chart_properties.timeLineTxtPosition,
							style: {
								color: self.chart_properties.timeLineTxtColor
							},
							format: '{value:%H:%M}'
						},
						gridLineColor: color,
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled: false
						},
						lineWidth: 0,
						endOnTick: false
					},{
						ordinal: false,
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%H:%M'
						},
						min: self.timeClosing_mil-(1000*60*60*2)-(1000*60*10)+self.serverOffset,//2 hours
						max: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						labels: {
							enabled: false
						},
						lineWidth: 0,
						endOnTick: false
					}],
					yAxis: [{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0
					},{
						xAxis: 1,
						height:self.chart_properties.height-self.chart_properties.yAxis_1,
						top:self.chart_properties.yAxis_1
					},{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						gridLineColor: color,
						lineWidth: 0
					},{
						height: self.chart_properties.yAxis_1,
						labels: {
							enabled:false
						},
						lineWidth: 0
					}],
					series : [{
						type: 'line',
						lineWidth: 1,
						color: self.chart_properties.chart_lineColor,
						name: self.boxOpportunities[self.oppArrPos].ET_NAME,
						turboThreshold:0,
						data : [],
						zIndex: 12,
						dataGrouping: {
							enabled: false
						},
						states: {
							hover: {
								halo: {
									size: 0
								}
							}
						}
					},{
						yAxis: 1,
						xAxis: 1,
						type: 'line',
						lineWidth: 1,
						name: '',
						zIndex: 12,
						states: {
							hover: {
								halo: {
									size: 0
								}
							}
						}
					},{
						type: 'line',
						lineWidth: 0,
						turboThreshold:0,
						data : [],
						zIndex: 13,
						dataGrouping: {
							enabled:false
						},
						states: {
							hover: {
								halo: {
									size: 0
								}
							}
						}
					},{
						type: 'line',
						lineWidth: 0,
						turboThreshold:0,
						data : [],
						zIndex: 14,
						dataGrouping: {
							enabled:false
						},
						marker: self.chart_point_properties,
						states: {
							hover: {
								halo: {
									size: 0
								}
							}
						}
					}],
					tooltip: {
						hideDelay:0,
						backgroundColor: self.chart_properties.tooltip_bgrColor,
						borderColor: self.chart_properties.tooltip_borderColor,
						useHTML: true,
						style:{
							zIndex:10
						},
						zIndex:10,
						formatter: function() {
							var num = Math.pow(10,self.decimalPoint);
							var level = Math.round(this.y*(num))/num;
							
							var temp = '';
							if (settings.isRegulated) {
								var temp = (!isUndefined(this.points[0].point.ask) && this.points[0].point.ask != '') ? 'ASK:' + this.points[0].point.ask : '';
								temp += (!isUndefined(this.points[0].point.bid) && this.points[0].point.bid != '') ? ((temp != '') ? " | " : '') + 'BID:' + this.points[0].point.bid : '';
								temp += (!isUndefined(this.points[0].point.last) && this.points[0].point.last != '') ? ((temp != '') ? " | " : '') + 'LAST:' + this.points[0].point.last : '';
							}
							
							var str = '<span class="taL ffDef">';
							var date = (!isUndefined(this.points[0].point.org_x) && this.points[0].point.org_x != '') ? this.points[0].point.org_x : this.x;
							str += '<span style="display:block;color:'+self.chart_properties.tooltip_timeTxtColor+';font-size:'+self.chart_properties.popUp_fsize1+';">'+formatDate(new Date(date), false, 'back')+'</span>';
							str += '<span style="color:'+self.chart_properties.tooltip_assetTxtColor+';font-size:'+self.chart_properties.popUp_fsize1+';">'+self.boxOpportunities[self.oppArrPos].ET_NAME+': </span>';
							str += '<span style="color:'+self.chart_properties.tooltip_levelTxtColor+';font-size:'+self.chart_properties.popUp_fsize2+';font-weight:bold;">'+level+"</span>";
								str += '<span style="display:block;color:'+self.chart_properties.tooltip_timeTxtColor+';font-size:'+self.chart_properties.popUp_fsize3+';">';
								str += temp;
								str += '</span>';
							str += "</span>";
							return str;
						}
					}
				});
				self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = self.timeClosing_mil-(1000*60*60)+self.serverOffset + settings.serverOffsetMillis;
				self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;
				
				setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
				
				chartGetHistory(1,false);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.initGraph = initGraph;
		
		function setChartExtremes(start, end, flag, ani) {
			self.chart.xAxis[0].setExtremes(start, end, flag, ani);
			self.chart.xAxis[1].setExtremes(start, end, flag, ani);
			self.chart.xAxis[2].setExtremes(start, end, flag, ani);
			self.chart.xAxis[3].setExtremes(start, end, flag, ani);
			
			var format = '{value:%H:%M}';
			var tickInterval = 1000 * 60 * 20;
			switch(parseInt(self.market.schedule)) {
				case 1: format = '{value:%H:%M}';tickInterval = 1000 * 60 * 20;break;
				case 2: format = '{value:%H:%M}';tickInterval = 1000 * 60 * 60 * 2;break;
				case 3: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 2;break;
				case 4: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 5;break;
				case 6: format = '{value:%d/%m}';tickInterval = 1000 * 60 * 60 * 24 * 15;break;
			}
			
			self.chart.xAxis[0].update({
				labels:{
					format: format
				},
				//tickInterval: tickInterval
				tickPixelInterval: 60
			});
		}
		
		//get history
		function chartGetHistory(part, inv){
			try{
				var timeout = Math.floor(Math.random() * 2000);
				if (self.delayChartHistory) {
					timeout = Math.floor(Math.random() * 9000);
					self.delayChartHistory = false;
				}
				logIt({'type':1,'msg':'chart timeout: '+timeout});
				setTimeout(function() {
					self.showOnlyIvestments = inv;
					self.showHistoryPart = part;
					
					if (!isUndefined(defaultMarkets[self.box]) && defaultMarkets[self.box].marketId == self.marketId && part == 1 && defaultMarkets[self.box].opportunityId == self.oppId) {
						var historyIndex = searchJsonKeyInArray(defaultMarkets, 'marketId', self.marketId);
						addHistoryToChart(defaultMarkets[historyIndex]);
						defaultMarkets[historyIndex] = null;
					} else {
						var oppType = (self.boxType == 1 || self.boxType == 4) ? (self.boxOpportunities.length > 0) ? self.boxOpportunities[self.oppArrPos].ET_OPP_ID : 0: 0;
							
						var oppId = 0;
						if ((self.market.schedule != 1 || !self.marketChanged) && self.boxOpportunities.length > 0) {
							oppId = self.boxOpportunities[self.oppArrPos].ET_OPP_ID;
						} else {
							oppId = oppType = 0;
						}
						if (part == 2) {
							oppId = self.boxOpportunities[self.oppArrPos].ET_OPP_ID;;
						}

						var req = jsonClone(getUserMethodRequest(),{box: self.box, marketId: self.marketId, opportunityId: oppId, opportunityTypeId: oppType, scheduled: self.market.schedule})
						var ajaxCall = new aJaxCall_fn({
							'type': 'POST',
							'url': settings.jsonLink + (part == 1 ? "getChartData" : "getChartDataPreviousHour"),
							'callback': addHistoryToChart,
							'params': JSON.stringify(req)
						});
						ajaxCall.doGet();
					}
				}, timeout);
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.chartGetHistory = chartGetHistory;
		
		//add history to chart
		// this.addHistoryToChart = addHistoryToChart;
		function addHistoryToChart(response){
			try{
				if (typeof response == 'string') {
					var resp = eval('('+response+')');
				} else {
					var resp = response;
				}
				if(resp.marketId == self.marketId){
					if(!self.showOnlyIvestments){
						self.decimalPoint = resp.decimalPoint;
						if (self.oppId != resp.opportunityId) {
							self.oppId = resp.opportunityId;
						}
						self.marketChanged = true;
						if (resp.rates != null){
							if (self.market.schedule > 1 || self.boxType == 5) {
								$('#tradeBox_chart_zoom_h_'+self.box).hide();
							} else {
								$('#tradeBox_chart_zoom_h_'+self.box).show();
							}
							
							var timeFirstInvest = new Date(resp.timeFirstInvest).getTime();
							var orgPoint = '';
							if (resp.ratesTimes[0] < timeFirstInvest && self.market.schedule == 2) {//last day closing level
								orgPoint = resp.ratesTimes[0] + self.serverOffset;
								resp.ratesTimes[0] = timeFirstInvest;
								self.endOfDayLastDayLevel = true;
							} else {
								self.endOfDayLastDayLevel =  false;
							}
							
							for(var i=0; i<resp.rates.length; i++){
								addPointToChart(
									resp.ratesTimes[i] + self.serverOffset,
									resp.rates[i],
									'',
									(resp.asks != null) ? resp.asks[i] : '',
									(resp.bids != null) ? resp.bids[i] : '',
									(resp.lasts != null) ? resp.lasts[i] : '',
									0,
									(i == 0) ? orgPoint : ''
								);
							}
						}
						if(resp.rates != null && resp.rates.length > 0){
							if (self.market.schedule > 1) {
								var offset = 0;
								switch(parseInt(self.market.schedule)) {
									case 2: offset = (1000*60*60*0.1);break;
								}
								self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = resp.ratesTimes[0] + self.serverOffset - offset;
								self.chartVissibleArea.end = self.timeClosing_mil + self.serverOffset;
								
								setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
							} else {
								var lastPoint = resp.ratesTimes[resp.ratesTimes.length-1] + self.serverOffset;
								if (lastPoint < self.chartVissibleArea.start && self.showHistoryPart == '1') {
									self.chartVissibleArea.start = self.chartVissibleArea.start_zoomed = resp.ratesTimes[0] + self.serverOffset;
									self.chartVissibleArea.end = self.chartVissibleArea.start + (1000*60*70);
									
									setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
								} else if(self.showHistoryPart == '2') {//lastPoint < self.chartVissibleArea.start &&
									self.chartVissibleArea.start_zoomed = resp.ratesTimes[0] + self.serverOffset;
									
									setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
									updateChartTimeLeft();
								}
							}
						}
					}
					if(self.boxType == 2 || self.boxType == 3){
						var ul = g('tradeBox_profit_invs_'+self.box);
						ul.innerHTML = "";
					}
					if (self.showHistoryPart == 1) {
						self.chartPointsInv = [];
						self.allInvestments = resp.investments;
						self.allInvestmentsForOpp = [];
						var totalInv = 0;
					}
					if (resp.investments != null) {
						for(var i=0; i<resp.investments.length; i++){
							var time = new Date(resp.investments[i].timeCreated).getTime();
							if(self.boxType == 2 || self.boxType == 3){
								var li = document.createElement('li');
								li.className = "tradeBox_profit_invs_li";
								li.setAttribute('data-marketId',resp.investments[i].marketId);
								if(self.boxType == 2){
									li.onclick = function(){
										closeProfitLine_box();
										openProfitLine_box(this.getAttribute('data-marketId'));
									}
								}
								else if(self.boxType == 3){
									li.onclick = function(){
										selectMarket(this.getAttribute('data-marketId'));
									}
								}
								ul.appendChild(li);
								
								var span = document.createElement('span');
								span.className = "tradeBox_profit_invs_tooltip";
								span.innerHTML = optionPlus_openAsset;
								li.appendChild(span);
								
								var span = document.createElement('span');
								span.className = "tradeBox_profit_invs_c1 vaM";
								span.innerHTML = resp.investments[i].asset;
								li.appendChild(span);
								
								var arrow = document.createElement('i');
								switch(resp.investments[i].typeId){
									case 1:var cls = 'call_win';break;
									case 2:var cls = 'put_win';break;
								}
								arrow.className = "tradeBox_img tradeBox_profit_invs_c2 "+cls+" vaM";
								arrow.id = "tradeBox_invest_list_"+resp.investments[i].id;
								arrow.setAttribute('data-lastClass','');
								
								var levelTxt = document.createElement('span');
								levelTxt.className = "tradeBox_profit_invs_c3 vaM";
								levelTxt.innerHTML = resp.investments[i].currentLevelTxt;
								
								if(settings.skinId != '1'){
									li.appendChild(arrow);
									li.appendChild(levelTxt);
								}else{
									li.appendChild(levelTxt);
									li.appendChild(arrow);
								}
								
								var span = document.createElement('span');
								span.className = "tradeBox_profit_invs_c4 vaM";
								span.innerHTML = resp.investments[i].currentLevelTxt;
								li.appendChild(span);
								
								var span = document.createElement('span');
								span.className = "tradeBox_profit_invs_c5 vaM";
								span.innerHTML = formatDateHours(resp.investments[i].timeEstClosing, false);
								li.appendChild(span);
								if((self.boxType == 3) && (self.marketId == resp.investments[i].marketId)){
									var span = document.createElement('span');
									span.setAttribute('data-oppId',resp.investments[i].id);
									span.className = "tradeBox_profit_invs_c6 vaM cP "+skinMap[settings.skinId].locale;
									span.innerHTML = optionPlus_getQuote;
									span.onclick = function(){
										changeQuotState({'state':1,'oppId':this.getAttribute('data-oppId')});
									}
									li.appendChild(span);
								}
							}
							if(!self.showOnlyIvestments){
								if(self.boxOpportunities.length > 0 && resp.investments[i].opportunityId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){
									addPointToChart(
										time + (self.serverOffset*2),
										resp.investments[i].currentLevel,
										'point_' + resp.investments[i].id,
										'',
										'',
										'',
										1
									);//TODO this is crazy should not be like this *2
								}
							}
							if(self.boxOpportunities.length > 0 && resp.investments[i].opportunityId == self.boxOpportunities[self.oppArrPos].ET_OPP_ID){
								self.allInvestmentsForOpp.push(resp.investments[i]);
								self.chartPointsInv.push({
									'point':'point_'+resp.investments[i].id,
									'type':resp.investments[i].typeId,
									'copyopType':resp.investments[i].copyopType,
									'time':time,
									'level':resp.investments[i].currentLevel,
									'amount':resp.investments[i].amount,
									'oddsWin':resp.investments[i].oddsWin,
									'oddsLose':resp.investments[i].oddsLose,
									'marketId':resp.investments[i].marketId,
									'oppId':resp.investments[i].opportunityId
								});
								// addPointToChart(resp.investments[i].time,resp.investments[i].level,'point_'+resp.investments[i].id, 1);
								var num = Math.pow(10,settings.decimalPointDigits);
								totalInv += (resp.investments[i].amount/num);
								// if(self.boxType == 3){
									// totalInv -= commsionFee;
								// }
							}
						}
					}
					updateCurrentReturn();
					if (self.showHistoryPart == 1) {
						if(settings.decimalPointDigits == 0){
							totalInv = totalInv/100;
						}
						if(self.boxType == 2 || self.boxType == 3 || self.boxType == 4){
							g('profitLine_total_inv_val_' + self.box).innerHTML = formatAmount(totalInv,0);
						}
					}
					self.chartPointsInv = self.chartPointsInv.sort(function(a,b) {
						return a.level < b.level;
					});
					if(!self.showOnlyIvestments){
						redrowChart();
					}
					if(self.boxType != '1'){
						chartBandsIvestments();
						changeChartColors();
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function changeChartColors(){
			try{
				if (self.market.schedule == 1) {
					self.chart.series[0].update({
						lineWidth: 1,
						states: {
							hover: {
								lineWidth: 2
							}
						}
					})
				} else {
					self.chart.series[0].update({
						lineWidth: 2,
						states: {
							hover: {
								lineWidth: 3
							}
						}
					})
				}
				if(self.chartPointsInv.length > 0){
					self.chart.series[0].color = "white";
					self.chart.series[0].update({
						lineColor: 'white'
					})
					self.chart.xAxis[0].update({
							labels:{
								style:{
									color:'white'
								}
							}
						});
					self.chart_point_properties.fillColor = self.chart_properties.chart_current_with_inv;
					self.chart_point_properties.lineColor = self.chart_properties.chart_current_with_inv;
					self.chart.series[3].update({marker: self.chart_point_properties});
					$('#tradeBox_chart_zoom_plus_'+self.box).addClass("inv");
					$('#tradeBox_chart_zoom_minus_'+self.box).addClass("inv");
				}
				else{
					self.chart.series[0].color = self.chart_properties.chart_lineColor;
					self.chart.series[0].update({
						lineColor: self.chart_properties.chart_lineColor
					})
					self.chart.xAxis[0].update({
							labels:{
								style:{
									color:self.chart_properties.timeLineTxtColor
								}
							}
						});
					self.chart_point_properties.fillColor = self.chart_properties.chart_current;
					self.chart_point_properties.lineColor = self.chart_properties.chart_current;
					self.chart.series[3].update({marker: self.chart_point_properties});
					$('#tradeBox_chart_zoom_plus_'+self.box).removeClass("inv");
					$('#tradeBox_chart_zoom_minus_'+self.box).removeClass("inv");
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//in future if we realy do have all markets levels (not in use now)
		function updateInvest_list(){
			try{
				for(var i=0; i<self.allInvestments.length; i++){
					var class_part_1 = (type == '1')?'call_':'put_';
					var class_part_2 = (win)?'win':'loose';
					var class_part = class_part_1+class_part_2;
					var span = g('tradeBox_invest_list_'+self.allInvestments[i].id);
					span.className = span.className.replace(span.getAttribute('data-lastClass'),'');
					span.className += " "+class_part;
					span.setAttribute('data-lastClass'," "+class_part)
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//using all points redrow the chart after ordering the points
		function redrowChart(){
			try{
				if (self.chart != null) {
					self.allPoints = self.allPoints.sort(function(a,b){
						return a.x - b.x;
					});
					
					self.allPointsInv = self.allPointsInv.sort(function(a,b){
						return a.x - b.x;
					});
					
					self.chart.series[0].setData(self.allPoints, true);
					self.chart.series[2].setData(self.allPointsInv, true);
					self.chart.series[3].setData([{
						x : self.lastUpdate.x,
						y : +(self.lastUpdate.y).toFixed(self.decimalPoint),
					}], true);
					
					var marker = {}
					if (self.market.schedule == 1) {
						marker = {
							marker: {
								enabled: false
							}
						}
					} else {
						if (self.allPointsInv.length > 0) {
							marker = {
								marker: {
									enabled: true,
									fillColor: self.chart_properties.chart_marker_with_inv,
									lineColor: '#fff',
									lineWidth: 1,
									radius: 3,
									states: {
										hover: {
											fillColor: self.chart_properties.chart_marker_hover_with_inv
										}
									}
								}
							}
						} else {
							marker = {
								marker: {
									enabled: true,
									fillColor: self.chart_properties.chart_marker,
									lineColor: '#fff',
									lineWidth: 1,
									radius: 3,
									states: {
										hover: {
											fillColor: self.chart_properties.chart_marker_hover
										}
									}
								}
							}
						}
					}
					
					self.chart.series[0].update(marker);
					
					if (self.endOfDayLastDayLevel && self.market.schedule == 2) {
						var point = self.chart.series[0].data[0];
						if (point != null) {
							point.update({
								marker:{
									enabled: true,
									symbol: 'square',
									fillColor: '#0284d6',
									lineColor: '#0284d6',
									radius: 3
								}
							});
						}
					}
					
					updateChartInvArrow();
					changeChartColors();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		if(this.boxType == 3){
			g('tradeBox_offer_sell_btn_cancel_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_cancel2_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_cancel3_'+this.box).onclick = function(){
				changeQuotState({'state':0});
			}
			g('tradeBox_offer_sell_btn_sell_'+this.box).onclick = function(){
				changeQuotState({'state':6});
			}
			g('tradeBox_offer_sell_btn_qetNewQuot_'+this.box).onclick = function(){
				changeQuotState({'state':1,'oppId':self.getQuot_state.lastQuot_oppId});
			}
			g('tradeBox_offer_sell_btn_qetNewQuot2_'+this.box).onclick = function(){
				changeQuotState({'state':1,'oppId':self.getQuot_state.lastQuot_oppId});
			}
		}
		//change different stetes of quot popup
		this.getQuot_state = {'className':'hidden','lastQuot_oppId':0};
		this.quotTimer = null;
		function changeQuotState(params){
			try{
				clearTimeout(self.quotTimer);
				clearTimeout(self.hideQuotExpired);
				var popup = g('tradeBox_offer_'+self.box);
				switch(params.state){//0:hidden,1:loading get price,2:selling,3:expired,4:sold,5:error,6:loading selling
					case 1:
						if(self.boxOpportunities[self.oppArrPos].ET_STATE == '2' || self.boxOpportunities[self.oppArrPos].ET_STATE == '3'){
							var newClass = 'loading';
							popup.className = popup.className.replace(self.getQuot_state.className,newClass);
							self.getQuot_state.className = newClass;
							
							self.getQuot_state.lastQuot_oppId = params.oppId;
							var paramsAjax = "flashGetPrice=1" + "&investmentId=" + params.oppId;
							var ajaxCall = new aJaxCall_fn({'type':'POST','url':settings.jsonLink + '/jsp/system/ajax.jsf','params':paramsAjax,'callback':getQuot_Callback});
							ajaxCall.doGet();
						}else{
							changeQuotState({});
						}
						break;
					case 6: 
						var newClass = 'loading';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						
						var paramsAjax = "flashSell=1&investmentId=" + self.getQuot_state.lastQuot_oppId + "&price=" + self.getQuot_state.price;
						var ajaxCall = new aJaxCall_fn({'type':'POST','url':settings.jsonLink + '/jsp/system/ajax.jsf','params':paramsAjax,'callback':sellQuot_Callback});
						ajaxCall.doGet();
						break;
					case 2: 
						var newClass = 'sell';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						g('tradeBox_offer_sell_amount_'+self.box).innerHTML = formatAmount(params.quotPrice,1);
						self.getQuot_state.price = params.quotPrice;
						break;
					case 3: 
						var newClass = 'expired';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						self.hideQuotExpired = setTimeout(function(){changeQuotState({});},5000);
						break;
					case 4: 
						var newClass = 'sold';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						
						g('tradeBox_offer_sold_amount_'+self.box).innerHTML = formatAmount(self.getQuot_state.price,1);
						setTimeout(function(){changeQuotState({'state':0})},3000);
						break;
					case 5: 
						var newClass = 'error';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
						g('tradeBox_offer_error_msg_'+self.box).innerHTML = params.error.replace(/\+/g,' ');
						break;
					default:
						var newClass = 'hidden';
						popup.className = popup.className.replace(self.getQuot_state.className,newClass);
						self.getQuot_state.className = newClass;
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.changeQuotState = changeQuotState;
		
		//fnc after ajax call for quot price
		function getQuot_Callback(response){
			try{
				var resp = response.split('=');
				if(!isNaN(resp[1])){
					changeQuotState({'state':2,'quotPrice':resp[1]});
					startQuotTimer();
				}
				else{
					changeQuotState({'state':5,'error':resp[1]});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		// this.getQuot_Callback = getQuot_Callback;
		
		//fnc after ajax call for sell quot
		function sellQuot_Callback(response){
			try{
				var resp = response.split('=');
				if(resp[1] == 'OK'){
					// setTimeout(function(){tradeBox_getBalace(self.box);},500);//TODO user user object
					var point = self.chart.get('point_' + self.getQuot_state.lastQuot_oppId);
					if(point != null){
						point.remove(false,false);
					}
					defaultMarkets[self.box] = null;
					chartGetHistory(1,true);
					changeQuotState({'state':4});
				}
				else{
					changeQuotState({'state':5,'error':resp[1]});
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.sellQuot_Callback = sellQuot_Callback;
		
		function startQuotTimer(){
			try{
				var t = g('tradeBox_offer_sell_timer_time_'+self.box);
				var t2 = g('tradeBox_offer_sell_timer_time2_'+self.box);
				var bgr = g('tradeBox_offer_sell_timer_bgr_'+self.box);
				var popup = g('tradeBox_offer_'+self.box);
				var date_end = new Date().getTime() + self.seconds_to_quot;
				var time_left = self.seconds_to_quot;
				function doIt(){
					var min = Math.floor(time_left/1000);
					if(min < 10){min = '0'+min;}
					var sec = Math.floor((time_left-(min*1000))/10);
					if(sec < 10){sec = '0'+sec;}
					t.innerHTML = t2.innerHTML = min+":"+sec;
					bgr.style.width = 100-(time_left/self.seconds_to_quot)*100+'%';
					
					if(time_left > 0){
						var interval = 10;
						time_left = date_end - new Date().getTime();
						self.quotTimer = setTimeout(doIt,interval);
					}
					else{
						changeQuotState({'state':3});
					}
				}
				doIt();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add point to chart
		function addPointToChart(x_val, y_val, point_id, ask, bid, last, type, original_x){//type: 0-chart, 1-investment
			try{
				var pointInfo = {
					x : x_val,
					y : y_val,
					id: point_id,
					marker: {enabled: (self.market.schedule == 1) ? false : true},
					ask: ask,
					bid: bid,
					last: last,
					org_x: (!isUndefined(original_x) && original_x != '') ? original_x : ''
				};
				
				if (type == 0) {
					self.allPoints.push(pointInfo);
				} else {
					self.allPointsInv.push(pointInfo);
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		// function addSquareToLastPoint(){
			// try{
				// var l = self.allPoints.length;
				// if(l > 0){
					// if(l < 20){
						// var l2 = l;
					// }
					// else{
						// var l2 = 10;
					// }
					// for(var i=l-l2; i<l; i++){
						// self.allPoints[i].marker = {enabled: false};
						// if(self.allPoints[i].id == 'lastPoint'){
							// self.allPoints[i].id = '';
						// }
					// }
					// self.allPoints[l-1].marker = self.chart_point_properties;
					// if(self.allPoints[l-1].id == ""){
						// self.allPoints[l-1].id = 'lastPoint';
					// }
				// }
			// }catch(e){
				// logIt({'type':3,'msg':e});
			// }
		// }
		//update graph
		function updateGraph(){
			try{
				if(self.boxOpportunities[self.oppArrPos].ao_level_float != 0 && !self.submited){
					if(self.chart != null){
						logIt({'type':1,'msg':'UPDATE CHART box: '+self.box+' level:'+self.boxOpportunities[self.oppArrPos].ao_level_float});
						
						var time = parseInt(self.boxOpportunities[self.oppArrPos].AO_TS);
						var level = self.boxOpportunities[self.oppArrPos].ao_level_float;
						//check if the point is in graphsvissible area
						var update_time = time+self.serverOffset;
						if(update_time > self.timeClosing_mil + (1000*60*10) + self.serverOffset - (1000*60*60)){
							self.chartVissibleArea.start_zoom = self.chartVissibleArea.start;
							self.chartVissibleArea.start = self.timeClosing_mil-(1000*60*60)+self.serverOffset;
							self.chartVissibleArea.end = self.timeClosing_mil+(1000*60*10)+self.serverOffset;
						}
						else if(update_time > self.chartVissibleArea.end-(1000*60*10)){
							self.chartVissibleArea.start_zoom = self.chartVissibleArea.start;
							self.chartVissibleArea.start = update_time-(1000*60*5);
							self.chartVissibleArea.end = self.chartVissibleArea.start + (1000*60*60);
							
							setChartExtremes(self.chartVissibleArea.start, self.chartVissibleArea.end);
						}
						
						self.lastUpdate = {
							x: update_time,
							y: level
						}
						//add point to chart
						if (self.market.schedule == 1) {
							addPointToChart(
								self.lastUpdate.x,
								self.lastUpdate.y,
								'',
								(self.boxOpportunities[self.oppArrPos].ASK != null) ? self.boxOpportunities[self.oppArrPos].ASK : '',
								(self.boxOpportunities[self.oppArrPos].BID != null) ? self.boxOpportunities[self.oppArrPos].BID : '',
								(self.boxOpportunities[self.oppArrPos].LAST != null) ? self.boxOpportunities[self.oppArrPos].LAST : '',
								0
							);
						}
						redrowChart();
						
						self.chart.xAxis[0].removePlotLine('plot-line-x');
						self.chart.xAxis[0].addPlotLine({
							// value: time+self.serverOffset,
							value: self.lastUpdate.x,
							width: 1,
							color: (self.chartPointsInv.length == 0)?self.chart_properties.plotLinesColor:self.chart_properties.plotLinesColor_with_inv,
							dashStyle: 'ShortDash',
							zIndex:2,
							id: 'plot-line-x'
						});
						self.chart.yAxis[0].removePlotLine('plot-line-y');
						self.chart.yAxis[0].addPlotLine({
							value: self.lastUpdate.y,
							width: 1,
							color: (self.chartPointsInv.length == 0)?self.chart_properties.plotLinesColor:self.chart_properties.plotLinesColor_with_inv,
							dashStyle: 'ShortDash',
							zIndex:2,
							id: 'plot-line-y'
						});
						if(chartBands_last.btn != ""){
							chartBands(chartBands_last.btn,chartBands_last.add,true);
						}
						if(self.boxType != '1'){
							chartBandsIvestments();
						}
					}
					//TO DO in future
					// updateInvest_list();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.updateGraph = updateGraph;
		
		//check all invests and update points
		//this.updateChartInvArrow = updateChartInvArrow;
		function updateChartInvArrow(){
			try{
				for(var i=0; i<self.chartPointsInv.length; i++){
					var point = self.chart.get(self.chartPointsInv[i].point);
					if(point != null){
						if(self.chartPointsInv[i].type == 1){
							var type = 'triangle';
							if(self.chartPointsInv[i].level < self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_win;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/call-win.png)';
								}
							}
							else if(self.chartPointsInv[i].level > self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_lose;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/call-lose.png)';
								}
							}
							else{
								var color = self.chart_properties.chart_noChange;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/call-lose.png)';
								}
							}
						}
						else{
							var type = 'triangle-down';
							if(self.chartPointsInv[i].level > self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_win;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/put-win.png)';
								}
							}
							else if(self.chartPointsInv[i].level < self.boxOpportunities[self.oppArrPos].ao_level_float){
								var color = self.chart_properties.chart_lose;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/put-lose.png)';
								}
							}
							else{
								var color = self.chart_properties.chart_noChange;
								if(self.chartPointsInv[i].copyopType == 'COPY'){
									type = 'url(img/put-lose.png)';
								}
							}
						}
						
						point.update({
							marker:{
								enabled: true,
								symbol: type,
								fillColor: color,
								lineColor: 'white',
								radius: 5,
								states: {
									hover: {
										fillColor: 'black',
										radius: 5
									}
								}
							}
						});
					}
					else{
						logIt({'type':3,'msg':'Point id cannot be found'});
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//erase all data from the graph and get history
		function resetGraph(){
			try{
				if(self.chart != null){
					self.chart.destroy();
					self.chart = null;
				}
				self.firstZoom = false;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		this.resetGraph = resetGraph;
		
		//add mouseover/out event to call put button
		var chartBands_last = {'btn':'','add':''};
		g('tradeBox_call_btn_'+self.box).onmouseover = function(){
			chartBands(1,true,false);
		}
		g('tradeBox_call_btn_'+self.box).onmouseout = function(){
			chartBands(1,false,false);
		}
		g('tradeBox_put_btn_'+self.box).onmouseover = function(){
			chartBands(2,true,false);
		}
		g('tradeBox_put_btn_'+self.box).onmouseout = function(){
			chartBands(2,false,false);
		}
		
		//profit line button
		if(this.boxType == '1'){
			g('tradeBox_profitLine_'+self.box).onclick = function(){
				profitLine();
			}
		}
		if (this.boxType != 4) {
			g('tradeBox_print_'+self.box).onclick = function(){
				printSuccess(self.investmentId);
			}
		}
		//open profitline
		function profitLine(){
			openProfitLine_box(self.marketId);
		}
		
		//chartBands
		// this.chartBands = chartBands;
		this.hasPlotBand = false;
		function chartBands(btn,add,force){
			try{
				if(self.chart != null){
					self.chart.yAxis[0].removePlotBand('plot-band-1');
					self.chart.yAxis[0].removePlotBand('plot-band-2');

					if (add) {
						chartBands_last.btn = btn;
						chartBands_last.add = add;
						var from,to;
						if(btn == 1){
							from = self.chart.yAxis[0].max;
							to = self.lastUpdate.y;
						}else{
							from = self.lastUpdate.y;
							to = self.chart.yAxis[0].min;
						}
						self.chart.yAxis[0].addPlotBand({
							from: from,
							to: to,
							// color: {
								// pattern: image_context_path+'/'+self.chart_properties.band_bgr,
								// width: self.chart_properties.band_bgr_width,
								// height: self.chart_properties.band_bgr_height
							// },
							zIndex:1,
							color: self.chart_properties.plotBandMouseOver,
							id: 'plot-band-'+btn,
							zIndex:2
						});
					}
					else{
						chartBands_last.btn = '';
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		this.chartBandsNum = 0;
		this.soundType_last = 0;
		function chartBandsIvestments(){
			try{
				if(self.chartBandsNum > 0){
					for(var i=0; i<=self.chartBandsNum; i++){
						self.chart.yAxis[0].removePlotBand('plot-band-inv-'+i);
						if(i > 0){
							self.chart.yAxis[0].removePlotLine('plot-line-x-inv-'+i);
						}
					}
				}
				self.chartBandsNum = self.chartPointsInv.length;
				if(self.chartPointsInv.length > 0){
					var soundType = 1;//in
					for(var i=0; i<=self.chartPointsInv.length; i++){
						if(i == 0){
							var from = self.chart.yAxis[0].max;
							var to = self.chartPointsInv[i].level;
						}
						else if(i == self.chartPointsInv.length){
							var from = self.chartPointsInv[i-1].level;
							var to = self.chart.yAxis[0].min;
						}
						else{
							var from = self.chartPointsInv[i-1].level;
							var to = self.chartPointsInv[i].level;	
						}
						var color = (isProfitable(from,to))?
							(from > self.lastUpdate.y && self.lastUpdate.y > to)?self.chart_properties.chart_plotBand_inTheMoney_current:self.chart_properties.chart_plotBand_inTheMoney:
							(from > self.lastUpdate.y && self.lastUpdate.y > to)?self.chart_properties.chart_plotBand_outTheMoney_current:self.chart_properties.chart_plotBand_outTheMoney;
						;
						if(color == self.chart_properties.chart_plotBand_outTheMoney_current){
							soundType = 2;//out
						}
						self.chart.yAxis[0].addPlotBand({
							from: from,
							to: to,
							// color: {
								// pattern: image_context_path+'/'+self.chart_properties.band_bgr,
								// width: self.chart_properties.band_bgr_width,
								// height: self.chart_properties.band_bgr_height
							// },
							zIndex:1,
							color: color,
							id: 'plot-band-inv-'+i
						});
						if(i > 0){
							self.chart.yAxis[0].addPlotLine({
								value: from,
								width: 1,
								color: self.chart_properties.plotLinesColor_inv,
								dashStyle: 'ShortDash',
								zIndex:2,
								id: 'plot-line-x-inv-'+i
							});
						}
					}
					if(self.soundType_last != soundType){
						self.soundType_last = soundType;
						playSound(soundType);
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function isProfitable(bottomLevel,topLevel){
			try{
				var totalAmount = 0;
				var totalProfit = 0;
				var checkLevel = bottomLevel + (topLevel - bottomLevel) / 2;
				
				for (var i = 0; i < self.chartPointsInv.length; i++) {
					var amount = amountToFloat(self.chartPointsInv[i].amount);
					totalAmount += amount;
					if ((checkLevel < self.chartPointsInv[i].level && self.chartPointsInv[i].type == 1) || (self.chartPointsInv[i].level < checkLevel && self.chartPointsInv[i].type == 2)) {
						totalProfit += amount * (1 - self.chartPointsInv[i].oddsLose);
					} else {
						totalProfit += amount * (1 + self.chartPointsInv[i].oddsWin);
					}
				}
				return totalProfit > totalAmount;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function getColor(crrLvl){
			try{
				var current = false;
				if (bottomLevel < crrLvl && crrLvl < topLevel) {
					current = true;
				}
				if (profitable) {
					if (current) {
						return _root.CLR_WIN_CRR;
					} else {
						return _root.CLR_WIN;
					}
				} else {
					if (current) {
						return _root.CLR_LOSE_CRR;
					} else {
						return _root.CLR_LOSE;
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//play a sound on state change
		if(this.boxType == 2 || this.boxType == 3){
			g('tradeBox_audio').onclick = function(){
				tradeBox_mute(this)
			}
		}
		function playSound(soundType){
			if(self.boxType == '1' || self.boxType == '4' || self.boxType == '5'){return;}
			if(self.audioMute){return;}
			var fileName = '';
			switch(soundType){
				case 1: fileName = 'oneclick_in';break;
				case 2: fileName = 'oneclick_out';break;
				case 3: fileName = 'oneclick_close';break;
				default:return;
			}
			var player = g('tradeBox_audio_' + self.box);
			player.pause();
			g('tradeBox_audio_mp3_' + self.box).src = image_context_path_clean + '/audio/' + fileName + '.mp3';
			g('tradeBox_audio_ogg_' + self.box).src = image_context_path_clean + '/audio/' + fileName + '.ogg';
			player.load();
			player.play();
		}
		this.audioMute = false;
		function tradeBox_mute(el){
			if(!self.audioMute){
				self.audioMute = true;
				el.className += ' mute';
			}
			else{
				self.audioMute = false;
				el.className = el.className.replace(/ mute/g,'');
			}
		}
		//updateChartTimeLeft
		function updateChartTimeLeft(){
			try{
				if(self.chart != null && self.chart != undefined){
					self.chart.xAxis[1].removePlotBand('plot-band-timer-bgr');
					self.chart.xAxis[1].addPlotBand({
						from: self.chartVissibleArea.start_zoomed,
						to: self.timeClosing_mil+(1000*60*10)+self.serverOffset,
						color: self.chart_properties.timer_bgrColor,
						id: 'plot-band-timer-bgr',
						zIndex:1
					});
					self.chart.xAxis[1].removePlotBand('plot-band-timer-normal');
					self.chart.xAxis[1].addPlotBand({
						from: self.chartVissibleArea.start_zoomed,
						to: parseInt(self.boxOpportunities[self.oppArrPos].AO_TS)+self.serverOffset,
						color: self.chart_properties.timer_timeElapsedColor,
						id: 'plot-band-timer-bgr',
						zIndex:1
					});
					if(self.boxOpportunities[self.oppArrPos].AO_TS >= (self.timeLastInv_mil-(1000*self.lastSecondsToInvest))){
						self.chart.xAxis[1].removePlotBand('plot-band-timer-red_bgr');
						self.chart.xAxis[1].addPlotBand({
							from: self.timeLastInv_mil-(1000*self.lastSecondsToInvest)+self.serverOffset,
							to: self.timeLastInv_mil+self.serverOffset,
							color: self.chart_properties.timer_bgrLastTenColor,
							id: 'plot-band-timer-red_bgr',
							zIndex:1
						});
						self.chart.xAxis[1].removePlotBand('plot-band-timer-last');
						self.chart.xAxis[1].addPlotBand({
							from: self.timeLastInv_mil-(1000*self.lastSecondsToInvest)+self.serverOffset,
							to: parseInt(self.boxOpportunities[self.oppArrPos].AO_TS)+self.serverOffset,
							color: self.chart_properties.timer_timeLastTenElapsedColor,
							id: 'plot-band-timer-last',
							zIndex:1
						});
					}
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//add click event to zoom btns
		g('tradeBox_chart_zoom_plus_'+self.box).onclick = function(){
			chartZoom(false);
		}
		g('tradeBox_chart_zoom_minus_'+self.box).onclick = function(){
			chartZoom(true);
		}
		
		//chart zoom
		function chartZoom(plus){
			try{
				if(!self.firstZoom && plus){
					chartGetHistory(2,false);
					self.firstZoom = true;
				}
				else{
					if(plus){
						var min = self.chartVissibleArea.start_zoomed
					}
					else{
						var min = self.chartVissibleArea.start
					}
					var max = self.chartVissibleArea.end
					
					var ani = true;
					if(self.chartPointsInv.length > 0){
						ani = false;
					}

					setChartExtremes(min,max,true,ani);
					chartBandsIvestments();
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		//getBalace if optionPlus
		// if(this.boxType == 3){
			// tradeBox_getBalace(self.box);//TODO user user object
		// }
		
		if(this.boxType == 2 || this.boxType == 3){
			g('profitLine_marketDrop_title_'+this.box).onclick = function(){
				openElement({'span':this,'autoClose':true});
				getBigMarketsDropDown();
			}
		}
		
		//getBigMarketsDropDown profitline and optionplus
		function getBigMarketsDropDown(){
			try{
				var profitLine_marketDrop_title_txt = g('profitLine_marketDrop_title_txt_' + self.box);
				if(profitLine_marketDrop_title_txt.innerHTML != txt_loading){
					self.profitLine_marketDrop_title_txt_default = profitLine_marketDrop_title_txt.innerHTML;
				}
				profitLine_marketDrop_title_txt.innerHTML = txt_loading;
				var ul = g('profitLine_marketDrop_ul_'+self.box);
				ul.innerHTML = "";
				var typeId = (self.boxType == 3)?3:1;
				var url = settings.jsonLink + "/jsp/xml/profitLineAssetSelect.jsf?typeId="+typeId; 
				var ajaxCall = new aJaxCall_fn({'url':url,'callback':getBigMarketsDropDown_callBack});
				ajaxCall.doGet();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		function getBigMarketsDropDown_callBack(response){
			try{
				var resp = eval('('+response+')');
				var ul = g('profitLine_marketDrop_ul_'+self.box);
				ul.innerHTML = "";
				var group = "";
				var subgroup = "";
				ul.innerHTML = '<li class="marketsMenuFilter_li" id="marketsMenuFilter_li_'+self.box+'"><input id="marketsMenuFilter_'+self.box+'" class="funnel_drop_ul_search_inp_big" /></li>';
				g('marketsMenuFilter_'+self.box).onkeyup = function(){
					filterDropDowns(event,"getBigMarketsDropDown_reset("+self.box+")");
				}
				setTimeout(function(){g('marketsMenuFilter_'+self.box).focus();},100);
				
				for(var i=0; i<resp.length; i++){
					if(self.boxType == 2){
						if(group != resp[i].group){
							var li = document.createElement('li');
							li.className = 'profitLine_marketDrop_groupname';
							li.innerHTML = resp[i].group;
							ul.appendChild(li);
							group = resp[i].group;
						}
						if(subgroup != resp[i].subGroup){
							var li = document.createElement('li');
							li.className = 'profitLine_marketDrop_subgroupname';
							li.innerHTML = resp[i].subGroup;
							ul.appendChild(li);
							subgroup = resp[i].subGroup;
						}
					}
					var li = document.createElement('li');
					li.className = 'profitLine_marketDrop_element' + ((self.marketId == resp[i].marketId)?' active':'');
					li.setAttribute('data-marketId',resp[i].marketId);
					li.setAttribute('data-search','t');
					if(self.boxType == 2){
						li.onclick = function(){
							if (!self.inProgress) {
								closeProfitLine_box();
								g('profitLine_marketDrop_title_txt_'+self.box).innerHTML = ls_getMarketName(this.getAttribute('data-marketId'));
								self.tradeBox_level_html.innerHTML = txt_loading;
								openProfitLine_box(this.getAttribute('data-marketId'));
								closeElement({'last':true});
							}
						}
					}
					else if(self.boxType == 3){
						li.onclick = function(){
							selectMarket(this.getAttribute('data-marketId'));
							closeElement({'last':true});
						}
					}
					var span = document.createElement('span');
					span.className = 'fL';
					span.innerHTML = resp[i].market;
					li.appendChild(span);
					var span = document.createElement('span');
					span.className = 'fR mr10';
					span.innerHTML = resp[i].timeEstClosing;
					li.appendChild(span);
					
					ul.appendChild(li);
				}
				g('profitLine_marketDrop_title_txt_' + self.box).innerHTML = self.profitLine_marketDrop_title_txt_default;
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		// this.getBigMarketsDropDown = getBigMarketsDropDown;
		
		//send sms on expire
		function updateInvestSms(){
			try{
				var params = "turnSmsOn&invId=" + self.investmentId;
				logIt({'type':1,'msg':'updateInvestSms box: '+self.box});
				var ajaxCall = new aJaxCall_fn({'type':'POST','url':settings.jsonLink + '/ajax.jsf','params':params,'callback':updateInvestSmsCallback});
				ajaxCall.doGet();
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		//TO DO GET NORMAL JSON RESPONSE
		function updateInvestSmsCallback(response){
			try{
				if (response.substring(0, 2) == "ok") {
					g('tradeBox_sms_inp_'+self.box).disabled = true;
					g('tradeBox_sms_txt_'+self.box).innerHTML = acceptSmsMessage;
				} else {
					//do nothing we dont have text and not sure we want :)
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
		
		function updateCurrentReturn(){
			try{
				if(self.boxType > 1 && self.boxType <= 4){
					var totalProfit = 0;
					
					for (var i = 0; i < self.chartPointsInv.length; i++) {
						if(self.marketId == self.chartPointsInv[i].marketId){
							var num = Math.pow(10,settings.decimalPointDigits);
							if(settings.decimalPointDigits == 0){
								num = 100;
							}
							var amount = (self.chartPointsInv[i].amount/num);
							if(self.boxType == 3){
								amount -= commsionFee;
							}
							if ((self.boxOpportunities[self.oppArrPos].ao_level_float <= self.chartPointsInv[i].level && self.chartPointsInv[i].type == 1) || 
								(self.chartPointsInv[i].level <= self.boxOpportunities[self.oppArrPos].ao_level_float && self.chartPointsInv[i].type == 2)) {
								totalProfit += amount * (1 - self.chartPointsInv[i].oddsLose);
							} else {
								totalProfit += amount * (1 + self.chartPointsInv[i].oddsWin);
							}
						}
					}
					g('profitLine_curr_rtn_val_' + self.box).innerHTML = formatAmount(totalProfit,0);
				}
			}catch(e){
				logIt({'type':3,'msg':e});
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function callPut_sw(n){
	try{
		logIt({'type':1,'msg':'init call/put text switch for box '+n});
		this.n = n;
		this.call = g('tradeBox_call_btn_text_'+n);
		this.call_txt_1 = this.call.getAttribute('date-text_1');
		this.call_txt_2 = this.call.getAttribute('date-text_2');
		this.put = g('tradeBox_put_btn_text_'+n);
		this.put_txt_1 = this.put.getAttribute('date-text_1');
		this.put_txt_2 = this.put.getAttribute('date-text_2');
		this.flag = true;
		var self = this;
		
		function switchIt(){
			if(self.flag){
				self.call.innerHTML = self.call_txt_1;
				self.put.innerHTML = self.put_txt_1;
				self.flag = false;
			}
			else{
				self.flag = true;
				self.call.innerHTML = self.call_txt_2;
				self.put.innerHTML = self.put_txt_2;
			}
		}
		setInterval(switchIt,2000);
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function checkValue(val){
	try{
		var vals = val.split('.');
		var regD = /\D/g;
		vals[0] = vals[0].replace(regD,'');
		vals[0] = vals[0].substr(0,8);
		var rtn = vals[0];
		if(typeof vals[1] != 'undefined'){
			vals[1] = vals[1].replace(regD,'');
			vals[1] = vals[1].substr(0,2);
			
			rtn += '.';
			
			if(vals[1] >= 0){
				rtn += vals[1];
			}
		}
		return rtn;
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function checkChars(obj) {//from tradePage.js
	var validchars = "0123456789";
	var sc = ".";
	var scused = false;
	var newval = "";
	var currval;
	var allowed = 0;
	var allowed_before_dot = 0;

	currval = obj.value;

	for (var x = 0; x < currval.length; x++) {
        var currchar = currval.charAt(x);
        for (var y = 0; y < validchars.length; y++) {
            var c = validchars.charAt(y);
            if (currchar == c) {
                if (scused) {
                    allowed++;
                    if (allowed > 2) {
                        break;
                    } else {
                        newval += currchar;
                        break;
                    }
                } else {
                    newval += currchar;
                    break;
                }
            } else if (!scused) {
                if (currchar == sc) {
                    scused = true;
                    newval += currchar;
                }
            }
        }
    }
    obj.value = newval;
}
function clearActiveList(lis){
	for(var i=0; i<lis.length; i++){
		lis[i].className = lis[i].className.replace(/ active/g,'');
	}
}
function formatDate(est_date, isTitle, offset) {//from tradePage.js
	if (offset == undefined) {
		est_date = adjustFromUTCToLocal(est_date);
	} else if (offset){
		est_date = new Date(est_date.getTime()+new Date().getTimezoneOffset()*60*1000);
	} else if (offset == 'back'){
		est_date = new Date(est_date.getTime() - new Date().getTimezoneOffset()*60*1000);
	}

	var currdate = new Date();
	var min = est_date.getMinutes();
	var ruCharBox = 'в';
	if(ruCharBox != null){
		ruCharBox.innerHTML;
	}
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		if (settings.skinId == 10 && !isTitle) {
			est_date = ruCharBox + est_date.getHours() + ":" + min + ", " + oldTexts.bundle_msg_today;
		} else {
			est_date = est_date.getHours() + ":" + min + " " + oldTexts.bundle_msg_today;
		}

	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		var month = est_date.getMonth()+1;
		if (month < 10) {
			month = '0' + month;
		}
		if (settings.skinId == 10 && !isTitle) {
			est_date = est_date.getDate() + "." + month + "." + year.substring(2) + ", " + ruCharBox + est_date.getHours() + ":" + min;
		} else {
			est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + month + "." + year.substring(2);
		}
	}
	return est_date;
}
function ls_updateItem(updateInfo) {
	// if (g('markets_table') == null) {
		// lsClient.unsubscribe(assetsls);
		// lsClient.unsubscribe(tradingBox);
	// }
	var itemPos = updateInfo.getItemPos();
	var itemName = updateInfo.getItemName();
	var timeStamp = updateInfo.getValue(SUBSCR_FLD.time_stamp);

	// handle market states
	if(updateInfo.isValueChanged(SUBSCR_FLD.ao_states)){
		logIt({'type':1,'msg':'Market states changed: '+updateInfo.getValue(SUBSCR_FLD.ao_states)});
		if(timeStamp > ls_marketsTimestamp){
			logIt({'type':1,'msg':'Market states updated.'});
			var ms = new Array();
			var aoStates = updateInfo.getValue(SUBSCR_FLD.ao_states);
			if((aoStates != null) && (aoStates != '')){
				ls_marketStates = aoStates.split("|");
			}

			if(ls_marketStates.length === 0){//show off hour banner
				logIt({'type':1,'msg':'No open markets. Show offhours.'});
				ls_switchTable(0);
				if(livePage){
					ls_switchLiveTrends(0);
					ls_switchLiveTable(0);
				}
			}else{//show the boxes
				logIt({'type':1,'msg':'There are '+ls_marketStates.length+' open markets'});
				ls_switchTable(1);
				if(livePage){
					ls_switchLiveTrends(1);
					ls_switchLiveTable(1);
				}
			}
		}
	}

	// handle markets
	if((updateInfo.isValueChanged(SUBSCR_FLD.ao_markets)) || (markestInBox.length === 0)){
		logIt({'type':1,'msg':'Markets changed: ' + updateInfo.getValue(SUBSCR_FLD.ao_markets)});
		if (markestInBox.length === 0) {// in the first update init the two empty arrays
			for(i = 0; i < group.length; i++){
				var mv = group[i].split("_");//example:aotps_1_5
				if(mv[0] == "aotps"){
					markestInBox[i] = box_objects[i].market = {'market_id':mv[2],'schedule':mv[1],'type':mv[0]};
					box_objects[i].marketId = parseInt(mv[2]);
				}else{
					markestInBox[i] = box_objects[i].market = {'market_id':mv[1],'schedule':1,'type':mv[0]};
					box_objects[i].marketId = parseInt(mv[1]);
				}
				if(box_objects[i].duplicatedTo != null){
					box_objects[box_objects[i].duplicatedTo].market = {'market_id':box_objects[i].market.market_id,'schedule':box_objects[i].market.type};
					box_objects[box_objects[i].duplicatedTo].marketId = parseInt(box_objects[i].market.market_id);
				}
			}
		}
		logIt({'type':1,'msg':{'msg':'Markets in boxes','currentMarkestInBox':markestInBox}});

		var ms = updateInfo.getValue(SUBSCR_FLD.ao_markets).split("|");
		if(timeStamp > ls_marketsTimestamp){
			logIt({'type':1,'msg':'Markets updated'});
			for (i = 0; i < group.length; i++) {
				if(box_objects[i].boxType != 3 && box_objects[i].boxType != 5){
					if(!box_objects[i].marketIdManual){
						markestInBox[i] = {'market_id':ms[i],'schedule':1,'type':group[i].split('_')[0]};
						box_objects[i].marketId = parseInt(ms[i]);
						if(box_objects[i].duplicatedTo != null){
							box_objects[box_objects[i].duplicatedTo].marketId = parseInt(ms[i]);
						}
					}
				}
			}
			if(typeof box_objects[i] != 'undefined' && box_objects[i].boxType != 3){
				ls_scheduleStartBoxes();
			}
		}
	}

	if (timeStamp > ls_marketsTimestamp) {
		ls_marketsTimestamp = timeStamp;
	}

	// update the boxes
	for (i = 0; i < group.length; i++) {
		if (itemName === group[i]) {
			var num = i;
			box_objects[i].lastUpdateInfo = updateInfo;
			var updateValueChanges = {};
			for (var j = 0; j < schema.length; j++) {
				updateValueChanges[schema[j]] = updateInfo.isValueChanged(schema[j]);
			}
			ls_updateItemNonVisual(i, itemPos, updateInfo, updateValueChanges);
			if(box_objects[num].duplicatedTo != null){
				ls_updateItemNonVisual(box_objects[num].duplicatedTo, itemPos, updateInfo, updateValueChanges);
			}
			break;
		}
	}
}
function ls_updateItemNonVisual(boxNumber, itemPos, updateInfo, updateValueChanges) {//from tradePage.js
	try{
		var et_group_closeAllowed = [3,6,7,12,14,15];
		
		if(updateInfo.getValue(SUBSCR_FLD.et_scheduled) == box_objects[boxNumber].market.schedule || et_group_closeAllowed.indexOf(updateInfo.getValue(SUBSCR_FLD.et_group_close) != -1)){
			logIt({'type':1,'msg':'updateItemNonVisual box:'+boxNumber});
			var oppId = parseInt(updateInfo.getValue(SUBSCR_FLD.et_opp_id));
			var filter_new_scheduled = parseInt(updateInfo.getValue(SUBSCR_FLD.filter_new_scheduled));
			var oppState = updateInfo.getValue(SUBSCR_FLD.et_state);
			var oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',oppId);
			
			//if the opportunity is not active and state 'delete' comes - do nothing
			if(oppArrPos === -1 && updateInfo.getValue(SUBSCR_FLD.command) === "DELETE"){//state DELETE unset
				logIt({'type':1,'msg':'DELETE unset opp '+oppId});
				return;
			}
			
			//if we have the opportunity and delete state comes
			if(updateInfo.getValue(SUBSCR_FLD.command) === "DELETE"){//state DELETE
				logIt({'type':1, 'msg':'DELETE timer:' + oppId + 'time: ' + new Date()});
				//delay the delete in case it is a drop in the ls
				// self.deleteDelay = setTimeout(function() {
					logIt({'type':1,'msg':'DELETE '+oppId + 'time: ' + new Date()});
					box_objects[boxNumber].boxOpportunities.splice(oppArrPos,1);
					box_objects[boxNumber].resetBox(true);
					box_objects[boxNumber].updateVisual({'stateReallyChanged':true,'oppChanged':true,'callsTrendChanged':true,'aoFlagsChanged':true});//update with new oppId
					
					// if(box_objects[boxNumber].boxType != 1){//TODO removed use user object
						// tradeBox_getBalace(boxNumber);
					// }
					box_objects[boxNumber].delayChartHistory = true;
				// }, 30000);
				return;
			}
			
			var added = false;
			if (oppArrPos === -1) {//ADD
				logIt({'type':1,'msg':'ADD '+oppId});
				added = true;

				if(box_objects[boxNumber].boxOpportunities.length > 0){
					if(oppState > box_objects[boxNumber].boxOpportunities[0].ET_STATE || 
						new Date(updateInfo.getValue(SUBSCR_FLD.et_est_close)) < new Date(box_objects[boxNumber].boxOpportunities[0].ET_EST_CLOSE)){
						box_objects[boxNumber].boxOpportunities.unshift({'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled});
						box_objects[boxNumber].optionChanged = true;
					}
					else{//if new opp is with lower state then the one active one
						box_objects[boxNumber].boxOpportunities.push({'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled});
						box_objects[boxNumber].optionChanged = false;
					}
				}
				else{//if first opp for this market
					box_objects[boxNumber].boxOpportunities[0] = {'ET_OPP_ID':oppId,'ET_STATE':oppState,'FILTER_NEW_SCHEDULED': filter_new_scheduled};
				}
			} else {
				//if random ls reset comes to live - do not delete the opp
				// if (self.deleteDelay != null) {
					// logIt({'type':1, 'msg':'DELETE-add timer:' + oppId + 'time: ' + new Date()});
				// }
				// clearTimeout(self.deleteDelay);
				// self.deleteDelay = null;
			}
			
			oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',oppId);
			
			//update array index of the active opportunity
			var waitForCorrectOppId = false;
			if (box_objects[boxNumber].oppId != 0) {
				box_objects[boxNumber].oppArrPos = searchJsonKeyInArray(box_objects[boxNumber].boxOpportunities,'ET_OPP_ID',box_objects[boxNumber].oppId);
				if (box_objects[boxNumber].oppArrPos == -1) {
					box_objects[boxNumber].oppArrPos = 0;
					waitForCorrectOppId = true;
				}
			} else {
				box_objects[boxNumber].oppArrPos = 0;
			}
//i give up - this is impossible!! CORE-4399			
//			var ao_nh = updateInfo.getValue('AO_NH');
//			if (!isUndefined(ao_nh) && ao_nh != '') {
//				box_objects[boxNumber].alternateOpps = ao_nh.split(';')
//					.map(function(current) {
//						var temp = current.split('|');
//						return {
//							oppId: temp[0],
//							expTime: adjustFromUTCToLocal(new Date(temp[1]))
//						}
//					});
//			}
//			
//			var ao_nq = updateInfo.getValue('AO_NQ');
//			if (!isUndefined(ao_nq) && ao_nq != '') {
//				box_objects[boxNumber].quorterlyOpps = ao_nq.split(';')
//					.map(function(current) {
//						var temp = current.split('|');
//						return {
//							oppId: temp[0],
//							expTime: adjustFromUTCToLocal(new Date(temp[1]))
//						}
//					});
//			}
			
			var stateReallyChanged = updateValueChanges[SUBSCR_FLD.et_state];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.et_state] == updateInfo.getValue(SUBSCR_FLD.et_state)){
				// stateReallyChanged = false;
			// }
			var oppChanged = updateValueChanges[SUBSCR_FLD.et_opp_id];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.et_opp_id] == updateInfo.getValue(SUBSCR_FLD.et_opp_id)){
				// oppChanged = false;
			// }
			var callsTrendChanged = updateValueChanges[SUBSCR_FLD.ao_calls_trend];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.ao_calls_trend_dsp] == updateInfo.getValue(SUBSCR_FLD.ao_calls_trend)){
				// callsTrendChanged = false;
			// }
			var aoFlagsChanged = updateValueChanges[SUBSCR_FLD.ao_flags];
			// if(box_objects[boxNumber].boxOpportunities[oppArrPos][SUBSCR_FLD.ao_flags_dsp] == updateInfo.getValue(SUBSCR_FLD.ao_flags)){
				// aoFlagsChanged = false;
			// }
			
			if ((added || stateReallyChanged) && !waitForCorrectOppId) {
				box_objects[boxNumber].updateState(//changeBoxState
					{'oppId':oppId,
						'ao_level':updateInfo.getValue(SUBSCR_FLD.ao_level),
						'et_state':updateInfo.getValue(SUBSCR_FLD.et_state),
						'et_est_close':updateInfo.getValue(SUBSCR_FLD.et_est_close),
						'last_invest':updateInfo.getValue(SUBSCR_FLD.last_invest),
						'et_suspended_message':updateInfo.getValue(SUBSCR_FLD.et_suspended_message),
						'forceStateWaiting': false
					}
				);
			}

			// if(box_objects[boxNumber].submited){return;}
			
			for (i = 0; i < schema.length; i++) {
				var valueFromUpdate = updateInfo.getValue(schema[i]);
				if(schema[i] === SUBSCR_FLD.et_name){
					valueFromUpdate = ls_getMarketName(valueFromUpdate);
				}
				else if (schema[i] === SUBSCR_FLD.ao_level) {
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_level = valueFromUpdate;
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_level_float = parseFloat(valueFromUpdate.replace(/,/g,''));
				}
				if(schema[i] === SUBSCR_FLD.ao_clr){
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_clr_dsp = valueFromUpdate;
				}
				if (schema[i] === SUBSCR_FLD.ao_calls_trend) {//trends
					box_objects[boxNumber].boxOpportunities[oppArrPos].ao_calls_trend_dep = valueFromUpdate;
				}
				
				box_objects[boxNumber].boxOpportunities[oppArrPos][schema[i]] = valueFromUpdate;
			}

			if(box_objects[boxNumber].boxType == 2 || box_objects[boxNumber].boxType == 3){
				g('profitLine_marketDrop_title_txt_'+boxNumber).innerHTML = box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_NAME+' - '+formatDateHours(box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_EST_CLOSE,true);
			}
			if(box_objects[boxNumber].oppArrPos > -1 && oppId == box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].ET_OPP_ID && !waitForCorrectOppId){
				// handle force susspend of opportunity
				if (box_objects[boxNumber].boxOpportunities[box_objects[boxNumber].oppArrPos].AO_OPP_STATE == '1') {
					getNewMarket();
					return;
				}
				box_objects[boxNumber].updateVisual({'stateReallyChanged':stateReallyChanged,'oppChanged':oppChanged,'callsTrendChanged':callsTrendChanged,'aoFlagsChanged':aoFlagsChanged});
				box_objects[boxNumber].updateGraph();
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}
}
function ls_switchTable(flag) {//from tradePage.js
	try{
		var markets_table = g("markets_table");
		var off_hours_banner = g("off_hours_banner");
		// var howToHpBanner = g("howToHpBanner");//TO DO remove (only ZH)
		if(markets_table != null && off_hours_banner != null){
			var profitLineDiv = g("profitLineDiv"); //profit line open button
			if (flag === 1) {//show trade boxes
				markets_table.style.display = 'block';
				off_hours_banner.style.display = 'none';

				// howToHpBanner.style.display = 'block';
				if (profitLineDiv != null) {
					profitLineDiv.style.display = 'block';
				}
			}
			else {//show off hour banner
				markets_table.style.display = 'none';
				off_hours_banner.style.display = 'block';

				// howToHpBanner.style.display = 'block';
				if (profitLineDiv != null) {
					profitLineDiv.style.display = 'none';
				}
			}
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveTrends(flag) {//from live_page_trands.js
	try{
		var liveTradingTrends =  g("liveTradingTrends");
		var liveTradingTrendsOffHoursLogin = g("liveTradingTrendsOffHoursLogin");
		if (flag === 0) {
			liveTradingTrends.style.display = 'none';
			liveTradingTrendsOffHoursLogin.style.display = 'block';	
		} else {
			liveTradingTrends.style.display = 'block';
			liveTradingTrendsOffHoursLogin.style.display = 'none';
		}
		
		var trendsBox = g("trendsBox");
		if(trendsBox != null) {
			if(flag === 0) {
				trendsBox.style.height = "350px";
			} else {
				trendsBox.style.height = "220px";
			}
		}
		ls_switchLiveGlobe(flag);
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveGlobe(flag) {//from live_page_trands.js
	try{
		var globeLogout =  g("globeLogout");
		var globeLogoutOffHours = g("globeLogoutOffHours");
		if (flag === 0) {
			globeLogout.style.display = 'none';
			globeLogoutOffHours.style.display = 'block';	
		} else {
			globeLogout.style.display = 'block';
			globeLogoutOffHours.style.display = 'none';
		}
	}catch(e){
		logIt({'type':3,'msg':e});
	}

}
function ls_switchLiveTable(flag) {//from live.js
	var investmentsTable =  g("liveInvestmentsTable");
	var offHourTable = g("liveInvestmentsOffHours");
	if (flag == 0) {
		investmentsTable.style.display = 'none';
		offHourTable.style.display = 'block';
	} else {
		investmentsTable.style.display = 'block';
		offHourTable.style.display = 'none';
	}
	
}
var boxCleanTimeout = null;
function ls_scheduleStartBoxes() {//from tradePage.js
	if (boxCleanTimeout === null) {
		logIt({'type':1,'msg':'Schedule Start Boxes'});
		boxCleanTimeout = setTimeout(ls_cleanBoxes, 10000 + Math.floor(Math.random() * 30000));
	}
}
function ls_cleanBoxes() {//from tradePage.js
	boxCleanTimeout = null;
	logIt({'type':1,'msg':'Cleaning boxes'});
	if (markestInBox.length > 0) {
		try {
			var haveCleaned = false;
			for (var i = 0; i < group.length; i++) {
				logIt({'type':1,'msg':"box: " + i + " full: " + box_objects[i].boxFull + " currentMarket: " + box_objects[i].market.market_id + " market: " + markestInBox[i].market_id});
				if ((box_objects[i].boxFull && !box_objects[i].marketIdManual && box_objects[i].market.market_id != markestInBox[i].market_id)) {
					box_objects[i].resetBox(true);//reset the box
					box_objects[i].boxOpportunities = [];
					box_objects[i].handleMessages({'type':'popup','errorCode':4});//set the box in loading mode
					haveCleaned = true;
				}
				if (!box_objects[i].boxFull) {
					haveCleaned = true;
				}
			}
			if (haveCleaned) {
				lsClient.unsubscribe(tradingBox);
				ls_startBoxes();
			}
		} catch (err) {
			logIt({'type':3,'msg':'Problem in cleanBoxes: ' + err});
		}
	}
}
function ls_startBoxes() {//from tradePage.js
	logIt({'type':1,'msg':'Start boxes'});
	var j = 0;
	for (var i = 0; i < group.length; i++) {
		if (!box_objects[i].boxFull) {
			log("starting box: " + i + " market: " + markestInBox[i].market_id + " schedule: " + markestInBox[i].schedule);
			if(markestInBox[i].type == 'aotps'){
				group[i] = markestInBox[i].type + "_" + markestInBox[i].schedule + "_" + markestInBox[i].market_id;
			}else{
				group[i] = markestInBox[i].type + "_" + markestInBox[i].market_id;
			}
			box_objects[i].market = {'market_id':markestInBox[i].market_id,'schedule':markestInBox[i].schedule};
			box_objects[i].boxFull = true;
		}
	}
	tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
}
function searchJsonKeyInArray(arr,key,val){
	for(var i=0; i<arr.length; i++){
		if(arr[i][key] == val){
			return i;
		}
	}
	return -1;
}
function ls_getMarketName(marketId) {//from tradePage.js
	if (marketsDisplayName[marketId]) {
		return marketsDisplayName[marketId];
	}
	return "";
}
var showMarketsMenu_box = 0;
function showMarketsMenu(obj,box,evt) {//from menuMarketSelect.js
	g('marketsMenuFilter').value = g('marketsMenuFilter').defaultValue;
	showMarketsMenu_box = box;
	var treeM = g('treeM');
	selectActiveMarkets(treeM,box);
	absolutePosition(obj,treeM,'bl');
	treeM.style.width = obj.offsetWidth-2+"px";
	openElement({'id':'treeM','autoClose':true})
	g('marketsMenuFilter_li').style.position = "absolute";
	setTimeout(function(){g('marketsMenuFilter').focus();},100);
}
function selectActiveMarkets(el,box) {//from menuMarketSelect.js
	var ls_marketStates_rev = [];
	for(j=0;j<ls_marketStates.length;j++) {
		ls_marketStates_rev[ls_marketStates[j]] = 1;
	}
	var lis = el.getElementsByTagName("li");
	var marketId = box_objects[box].market.market_id;
	var last_group_close = null, last_subgroup_close = null, subGroup_els = [], subGroup_els_all = 0;

	for(var i=0; i<lis.length; i++){
		lis[i].style.display = "block";

		if(lis[i].getAttribute('data-close') != null){
			continue;
		}
		var group_el = lis[i].getAttribute('data-group');
		if(group_el != null){
			last_group_close = group_el;
			if(subGroup_els.length == subGroup_els_all){
				for(var n=0; n<subGroup_els.length; n++){
					if(subGroup_els[n] != ''){
						g(subGroup_els[n] + '_title').style.display = "none";
						g(subGroup_els[n]).style.display = "none";
					}
				}
			}
			subGroup_els = [];
			subGroup_els_all = 0;
			continue;
		}
		var subgroup_el = lis[i].getAttribute('data-subgroup');
		if(subgroup_el != null){
			last_subgroup_close = subgroup_el;
			var subGroup_els_last = subGroup_els.length;
			subGroup_els[subGroup_els.length] = last_subgroup_close;
			subGroup_els_all++;
			continue;
		}
		var optionMarketId = lis[i].getAttribute('data-marketId');
		if(optionMarketId != null){
			if(ls_marketStates_rev[optionMarketId] == null){
				lis[i].style.display = "none";
				lis[i].setAttribute('data-search', '');
			}
			else{
				subGroup_els.splice(subGroup_els_last,1);
				lis[i].setAttribute('data-search', 't');
				lis[i].className = lis[i].className.replace(/ active/g,'');
				if(marketId == optionMarketId) {
					lis[i].className += ' active';
				}
				g(last_group_close).style.display = "none";
				if(last_subgroup_close != null){
					g(last_subgroup_close).style.display = "none";
				}
			}
		}
	}
}
function selectMarket(marketId) {
	closeElement({'last':true});
	if(box_objects[showMarketsMenu_box].marketId != marketId && !box_objects[showMarketsMenu_box].inProgress){
		var boxId = marketToBox(marketId);
		if(boxId != -1){
			//alert(marketMenuError);
			return boxId;
		}
		boxId = showMarketsMenu_box;
		if(box_objects[boxId].boxType == '1' || box_objects[boxId].boxType == 4 || box_objects[boxId].boxType == 5){
			g('tradeBox_marketName_'+boxId).innerHTML = ls_getMarketName(marketId);
		}else{
			g('profitLine_marketDrop_title_txt_'+boxId).innerHTML = ls_getMarketName(marketId);
		}
		box_objects[boxId].tradeBox_level_html.innerHTML = txt_loading;
		
		g('tradeBox_chart_longTerm_'+boxId).style.display = "none";
		g('tradeBox_chart_main_'+boxId).style.display = "block";

		var schedule = 1;
		box_objects[boxId].market.market_id = marketId;
		box_objects[boxId].market.schedule = schedule;
		box_objects[boxId].marketIdManual = true;
		box_objects[boxId].marketId = marketId;
		box_objects[boxId].boxOpportunities = [];
		box_objects[boxId].resetBox(true);
		box_objects[boxId].marketChanged = true;
		
		if(box_objects[boxId].market.type == 'aotps'){
			group[boxId] = box_objects[boxId].market.type + "_"+schedule + "_" + marketId;
		}else{
			group[boxId] = box_objects[boxId].market.type + "_"+ marketId;
		}

		lsClient.unsubscribe(tradingBox);
		tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
		
		//when we change market - the default for the schedule will be the closest
		if (isLiveAOPage) {
			fromGraph = 2; //Live page - trade box.
		} else {
			fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
		}
	}
}
var profitLineBox = {'box':null,'duplicateOf':null};
function openProfitLine_box_callBack(response){
	var resp = eval('('+response+')');
	openProfitLine_box(resp.settings.marketId);
}
function openProfitLine_box(marketId){
	var profitLine_h = g('profitLine_h');
	if(profitLine_h != null){
		window.scrollTo(0,0);
		g('profitLine_h_bgr').style.display = "block";
		g('profitLine_h').style.display = "block";
		if(marketId == '0'){
			var url = settings.jsonLink + "/jsp/xml/profitLineData.jsf?marketId=0&oppId=0&typeId=1";
			var ajaxCall = new aJaxCall_fn({'url':url,'callback':openProfitLine_box_callBack});
			ajaxCall.doGet();
			return true;
		}
		var boxId = profitLineBox.box = profitLineBoxId;
		// tradeBox_getBalace(boxId);//TODO user user object
		var groupId = null;
		for(i = 0; i < group.length; i++){
			var market = group[i].split("_");//example:aotps_1_5
			if((market[1] == 1) && (market[2] == marketId)){
				groupId = i;
				break;
			}
		}	
		if(groupId == null){
			var type = 'aotps';
			profitLineBox.duplicateOf = null;
			box_objects[boxId].market = {'market_id':marketId,'schedule':1,'type':type};
			box_objects[boxId].marketIdManual = true;
			box_objects[boxId].marketId = marketId;
			box_objects[boxId].boxOpportunities = [];
			box_objects[boxId].resetBox(false);
			if(type == 'aotps'){
				var group_el = type + "_1_" + marketId;
			}else{
				var group_el = type + "_" + marketId;
			}
			
			group.push(group_el);
			if(!lsEngineReady){
				lsSubscribeParams.group_tradeBox = group;
				connectToLS_global(lsSubscribeParams)
			}
			else{
				lsClient.unsubscribe(tradingBox);
				tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
			}
		}
		else{
			profitLineBox.duplicateOf = groupId;
			box_objects[boxId].resetBox(false);
			box_objects[boxId].changeStates({'state':''});
			box_objects[boxId].market = cloneObj(box_objects[groupId].market);
			box_objects[boxId].marketIdManual = true;
			box_objects[groupId].marketIdManual = true;
			box_objects[groupId].duplicatedTo = boxId;
			box_objects[boxId].boxFull = true;
			box_objects[boxId].marketId = cloneObj(box_objects[groupId].marketId);
			box_objects[boxId].timeLastInv_mil = cloneObj(box_objects[groupId].timeLastInv_mil);
			box_objects[boxId].timeClosing_mil = cloneObj(box_objects[groupId].timeClosing_mil);
			
			box_objects[boxId].profit = cloneObj(box_objects[groupId].profit);
			box_objects[boxId].refund = cloneObj(box_objects[groupId].refund);
			box_objects[boxId].boxOpportunities = cloneObj(box_objects[groupId].boxOpportunities);
			if(box_objects[boxId].chart == null){
				box_objects[boxId].initGraph();
			}

			ls_updateItemNonVisual(boxId, '', box_objects[groupId].lastUpdateInfo);
			
			g('profitLine_marketDrop_title_txt_'+boxId).innerHTML = box_objects[boxId].boxOpportunities[box_objects[boxId].oppArrPos].ET_NAME+' - '+formatDateHours(box_objects[boxId].boxOpportunities[box_objects[boxId].oppArrPos].ET_EST_CLOSE,true);
		}
	}
	else{
		// window.location = context_path + tradePageN+"?profitLineMarketId="+marketId;
		function getProfitLine_calBack(response){
			var div = document.createElement('div');
			div.innerHTML = response;
			document.body.appendChild(div);
			
			profitLineBoxId = boxId;
			box_objects[boxId] = new box_obj({
				'box':boxId,
				'invest': defaultInvest,
				'boxType':2
			});
			openProfitLine_box(marketId);
		}
		
		addScriptToHead('highstock.js','highstock')
		var boxId = group.length;
		var url = settings.jsonLink + "/jsp/html_elements/profitLinePopUp.jsf?s=" + settings.skinId + "&boxId=" + boxId;
		var ajaxCall = new aJaxCall_fn({'url':url,'callback':getProfitLine_calBack});
		ajaxCall.doGet();
	}
}
function getBigMarketsDropDown_reset(box){
	var lis = g('profitLine_marketDrop_ul_' + box).getElementsByTagName('li');
	for(var i=1; i<lis.length; i++){
		lis[i].style.display = "block";
	}
}
function formatDateHours(date,addOffset){
	var date = new Date(date);
	if(addOffset){
		date = new Date(date.getTime()-new Date().getTimezoneOffset()*60*1000);
	}
	var min = date.getMinutes();
	if (new Number(date.getMinutes()) < new Number("10")) {
		min = "0" + date.getMinutes();
	}
	return date.getHours() + ":" + min;
}
function closeProfitLine_box(){
	if(profitLineBox.duplicateOf != null){
		box_objects[profitLineBox.duplicateOf].duplicatedTo = null;
		box_objects[profitLineBox.duplicateOf].marketIdManual = false;
	}
	else{
		group.pop();
		lsClient.unsubscribe(tradingBox);
		tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});
	}
	box_objects[profitLineBox.box].resetBox(true);
	// box_objects[profitLineBox.box].market.market_id = '';
	g('profitLine_h').style.display = "none";
	g('profitLine_h_bgr').style.display = "none";
}
var refreshHeader_timer = null;
function refreshHeader(){
	// g('header').contentWindow.location.reload(true);
	clearTimeout(refreshHeader_timer);
	refreshHeader_timer = setTimeout(doIt,1000);
	function doIt(){
		g('header').src = g('header').src;
	}
}
function refreshInvProfitBox(){
	var invBox = document.getElementById('right_side_investments');
	if (invBox != null) {
		invBox.contentWindow.location.reload();
	}
}
//print slip success
function printSuccess(invId) {
	window.open(context_path + "/jsp/single_pages/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}
//connect to ls
function configureLsConnection(){
	var lsClient = new LightstreamerClient(settings.lsServer, 'REUTERSJMS');
	lsClient.connectionOptions.setMaxBandwidth(20);
	lsClient.connectionOptions.setIdleMillis(30000);
	lsClient.connectionOptions.setPollingMillis(1000);
	lsClient.connectionOptions.setReconnectTimeout(10000);
	lsClient.connectionSharing.enableSharing("AOCommonConnection", "ATTACH", "CREATE");
	if (settings.loggedIn) {
		lsClient.connectionDetails.setUser("a" + settings.userName.toLowerCase() + "a");
		lsClient.connectionDetails.setPassword(settings.hashedPassword);
	}
	return lsClient;
}
var lsEngineReady = false,lsClient,subscriptionType,dataAdapterName,lsSubscribeParams = {},
	tradingBox,group,schema,
	followls,groupFollow,schemaFollow,
	assetsls,groupAssets,schemaAssets,
	lsShoppingBag,groupShoppigBag,schemaShoppigBag,
	lsOptionPlus,groupOptionPlus,schemaOptionPlus,
	lsTrends,groupTrends,schemaTrends;
function connectToLS(params){//['name':'','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName]
	logIt({'type':1,'msg':'connectToLS: '+params.name});
	var obj = new Subscription(params.subscriptionType,params.group,params.schema);
	obj.setDataAdapter(params.dataAdapterName);
	obj.setRequestedSnapshot("yes");
	obj.setRequestedMaxFrequency(1.0);
	obj.addListener({
		  onSubscription: function() {
			  logIt({'type':1,'msg':'SUBSCRIBED: '+params.name});
		  },
		  onUnsubscription: function() {
			  logIt({'type':1,'msg':'UNSUBSCRIBED: '+params.name});
		  },
		  onItemUpdate: function (updateObject) { 
			logIt({'type':1,'msg':'ItemUpdate: '+params.name});
			params.callBack(updateObject);
		  }
		});
	lsClient.subscribe(obj);
	return obj;
}
function connectToLS_global(params){
	lsSubscribeParams = params;
	if (isUndefined(lsClient)) {
		lsClient = configureLsConnection();
		lsClient.connect();
	}
	lsEngineReady = true;
	//trading boxes
	subscriptionType = "COMMAND";
	dataAdapterName = "JMS_ADAPTER";
		
	if(typeof params.group_tradeBox == 'object'){
		group = params.group_tradeBox;
		schema = ["key", "command", "ET_NAME", settings.level_skin_group_id, "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", settings.level_color, "ET_PRIORITY", 
			"ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_HP_MARKETS_" + settings.skinId, "AO_STATES", "ET_LAST_INV", 
			"AO_TS", "AO_FLAGS", "ET_ODDS_GROUP", settings.ao_calls_trend, "FILTER_NEW_SCHEDULED", "AO_OPP_STATE", "ASK", "BID", "LAST", "AO_NH", "AO_NQ"];
		var marketId = getUrlValue('marketId');
		if (typeof params.fixedBox != 'undefined' && params.fixedBox) {
			box_objects[0].marketIdManual = true;
		} else if(marketId != "" && group.indexOf('aotps_1_'+marketId) == -1){
			group[0] = 'aotps_1_'+marketId;
			box_objects[0].marketIdManual = true;
		}
		tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_updateItem});				
	}
	if((typeof params.group_shoppingBag == 'object')){
		groupShoppigBag = params.group_shoppingBag;
		schemaShoppigBag = ["key", "command", "SB_MARKET_ID", "SB_OPP_TYPE", "SB_LEVEL", "SB_AMOUNT", "SB_TIME_CLOSE"];
			
		lsShoppingBag = connectToLS({'name':'lsShoppingBag','subscriptionType':subscriptionType,'group':groupShoppigBag,'schema':schemaShoppigBag,'dataAdapterName':dataAdapterName,'callBack':ls_shoppingBag});				
	}
	if(typeof params.group_optionPlus == 'object'){
		group = params.group_optionPlus;
		schema = ["key", "command", "ET_NAME", settings.level_skin_group_id, "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", settings.level_color, "ET_PRIORITY", 
			"ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE", "AO_HP_MARKETS_" + settings.skinId, "AO_STATES", "ET_LAST_INV", 
			"AO_TS", "AO_FLAGS", "ET_ODDS_GROUP", settings.ao_calls_trend];
		if(group[0] == 'op_0'){
			g('markets_table').style.display = 'none';
			g('off_hours_banner').style.display = 'block';
		}
		else{
			var marketId = getUrlValue('marketId');
			if(marketId != ""){
				group[0] = 'op_'+marketId;
			}
			tradingBox = connectToLS({'name':'tradingBox','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName,'callBack':ls_optionPlus});				
		}
	}
	if(typeof params.group_follow == 'object'){
		groupFollow = params.group_follow;
		schemaFollow = ['command', "key", settings.level_skin_group_id, "ET_EST_CLOSE", "ET_OPP_ID", "ET_STATE", settings.level_color, 'ET_ODDS_GROUP'];
		followls = connectToLS({'name':'follow','subscriptionType':subscriptionType,'group':params.group_follow,'schema':schemaFollow,'dataAdapterName':dataAdapterName,'callBack':ls_updateFollow});				
	}
	if(typeof params.group_assets == 'object'){
		groupAssets = params.group_assets;
		schemaAssets = ["key", 'command', 'ET_NAME', settings.level_skin_group_id, "ET_LAST_INV", "ET_STATE", settings.level_color, "ET_EST_CLOSE"];
		if (!isUndefined(assetsls) && assetsls.isSubscribed()) {
			lsClient.unsubscribe(assetsls);
		}
		assetsls = connectToLS({'name':'assets','subscriptionType':subscriptionType,'group':params.group_assets,'schema':schemaAssets,'dataAdapterName':dataAdapterName,'callBack':ls_updateAssets});				
	}
}

function aJaxCall_fn(params){//url,params to post,type,callback,xml,async
	var type = (typeof params.type != 'undefined')?params.type:'GET';
	var post_params = (typeof params.params != 'undefined')?params.params:null;
	var async = (typeof params.async != 'undefined')?params.async:true;
	var xml = (typeof params.xml != 'undefined')?params.xml:false;
	
	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

    function processRequest () {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (params.callback) {
					if(xml){
						params.callback(req.responseXML);
					}else{
						params.callback(req.responseText);
					}
				}
			}
		}
    }

    this.doGet = function() {
		req.open(type, params.url, async);//type[post,get]
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(post_params);
   	}
}

//shopping bag
function formatHM(hourMin){
	var hm = hourMin;
	if (hm < 10) {
		hm = '0' + hm;
	} else {
		hm = hm + '';
	}
	return hm;
}
function getCloseTimeShopingBag(timeClose){
	var targetTime = new Date(timeClose);	
	var timeZoneOfSet = targetTime.getTimezoneOffset();
	targetTime.setMinutes(targetTime.getMinutes() + (timeZoneOfSet*-1));
	return formatHM(targetTime.getHours()) + ":" + formatHM(targetTime.getMinutes());
}
function getFormatShopingBagAmont(amount){
	var res = "";
	if(amount != 0){
		res = formatAmount(amount, 2, true);
	}	
	return  res;
}
function closeShopingBag(){
	g('shop_bag_popUp').style.top = -g('shop_bag').offsetHeight+'px';
	lsClient.sendMessage(groupShoppigBag);
}
function showShoppingBag(){
	if(shopingBagArr.length > 0){
		refreshHeader();
		var offset=(skinId == 15)?114:0;
		g('shop_bag_popUp').style.top = offset+'px';
		var  shop_bag_info_holder = g('shop_bag_info_holder');
		shop_bag_info_holder.innerHTML='';
		var oppTypeClass = '';
		for (var i = 0; i < shopingBagArr.length; i++) {
			switch (shopingBagArr[i].oppType) {
			  case "1":oppTypeClass = 'inv_binary';break;//BO
			  case "2":oppTypeClass = 'inv_1touch';break;//onetouch
			  case "3":oppTypeClass = 'inv_plus inv_plus_shop_bag';break;//optionPlus
			  case "4":oppTypeClass = 'inv_binary_0_100';break;//0100
			  case "5":oppTypeClass = 'inv_binary_0_100';break;//0100
			  case "7":oppTypeClass = 'inv_dynamics';break;//dynamics
			}
			shop_bag_info_holder.innerHTML += '<div class="shop_bag_info">\
				<a href="'+shopingBagArr[i].redirectUrl+'">\
				<span class="shop_bag_asset">'+shopingBagArr[i].marketName+'</span>\
				<span class="shop_bag_icon inv_p '+oppTypeClass+'"></span>\
				<span class="shop_bag_expiry_level">'+shopingBagArr[i].level+'</span>\
				</a></div>';
			
			if(shopingBagArr[i].amount === ''){
				g('shop_bag_amount_currency').style.display = "none";
			}else{
				g('shop_bag_amount_currency').style.display = "block";
				g('shop_bag_txt_amount').innerHTML = shopingBagArr[i].amount;
			}
			g('shop_bag_time_expiry').innerHTML = shopingBagArr[i].timeClose;
		}
	}
	else{
		g('shop_bag_popUp').style.top = -g('shop_bag').offsetHeight+'px';
	}
}
function ls_shoppingBag(updateInfo){
	angular.element('html').scope().getCopyopUser({onStateChange: true, stopStateMachine: true});
	var shopingBag = {
		oppId:  updateInfo.getValue(1),
		command:  updateInfo.getValue(2),
		marketId: updateInfo.getValue("SB_MARKET_ID"),
		marketName: ls_getMarketName(updateInfo.getValue("SB_MARKET_ID")),
		oppType: updateInfo.getValue("SB_OPP_TYPE"),
		level: updateInfo.getValue("SB_LEVEL"),
		amount: updateInfo.getValue("SB_AMOUNT"),
		amountDsp: getFormatShopingBagAmont(updateInfo.getValue("SB_AMOUNT")),
		timeClose: getCloseTimeShopingBag(updateInfo.getValue("SB_TIME_CLOSE"))
	}
	
	var popIndex = searchJsonKeyInArray(angular.element('html').scope().popUps,'ctr','shoppingBag');
	if (popIndex > -1) {
		angular.element($('#shoppingBag')).scope().addOrDeleteShoppingBagArr(shopingBag);
	} else if (shopingBag.command.toLowerCase() != "delete"){
		angular.element('html').scope().openCopyOpPopup({type: 'shoppingBag', doNotHidePageScroll: true, info: shopingBag});
	}

	return true;
}