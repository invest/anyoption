copyOpApp.controller('uploadServiceCtr', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.usingFlash = FileAPI && FileAPI.upload != null;
	$scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
	$scope.uploadRightAway = false;
	if($rootScope.$state.includes('ln.in.a.personal.my-account.personal-details')){
		$scope.uploadRightAway = true;
	}
	$scope.fileTooBig = false;
	
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	$scope.onFileSelect = function($files) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000  >= settings.avatarSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		closeElement({id:'popUp-avatar'});
		$scope.selectedFiles = [];
		$scope.progress = [];
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$rootScope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
			if ($scope.uploadRightAway) {
				$scope.start(i);
			}
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.avatarSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			$scope.upload[index] = $upload.upload({
				url: settings.jsonImagegLink + 'UploadService',
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile'
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response.data);
					$scope.$parent.uploadComplate(response.data);
				});
			}, function(response) {
				if (response.status > 0) $scope.$parent.errorMsg = $scope.errorMsg = response.status + ': ' + response.data;
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			alert($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.dragOverClass = function($event) {
		var items = $event.dataTransfer.items;
		var hasFile = false;
		if (items != null) {
			for (var i = 0 ; i < items.length; i++) {
				if (items[i].kind == 'file') {
					hasFile = true;
					break;
				}
			}
		} else {
			hasFile = true;
		}
		return hasFile ? "dragover" : "dragover-err";
	};
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
}]);