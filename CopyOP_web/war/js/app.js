var copyOpApp = angular.module('copyOpApp', [
	'ui.router',
	'angularFileUpload',
	'ngSanitize',
	'ngAnimate',
	'vcRecaptcha'
])
.run(['$rootScope', '$state', '$stateParams', '$interval', 
	function ($rootScope, $state, $stateParams, $interval) {
		// It's very handy to add references to $state and $stateParams to the $rootScope
		// so that you can access them from any scope within your applications.For example,
		// <li ng-class='{ active: $state.includes('contacts.list') }'> will set the <li>
		// to active whenever 'contacts.list' or one of its decendents is active.
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.getSettings = function(){
			return settings;
		}
		$rootScope.countries = countries;
		
		$rootScope.initSettings = function() {
			$rootScope.userProfile = '';//currently viewing user (myCopyopProfile or getCopyopProfile)
			$rootScope.copyopUser = {copyopProfile: {}, user: {}};//currently logged user (getCopyopUser)
			$rootScope.usersInfo = [];//info for other users (copying, watching etc)
			$rootScope.popupInfo = {};//info about currently opend popups if needed
			$rootScope.copyopCopying = [];//list of users i'm copying
			$rootScope.copyopWatching = [];//list of users i'm waching
			$rootScope.openOptions = [];//list of all open assets
			$rootScope.afterDeposit = false;
			
			$rootScope.newsFeed = [];//keeps the newsfeed trought all the states - so no need to get history more then once
			$rootScope.exploreFeed = [];//keeps the explorefeed trought all the states - so no need to get history more then once
			$rootScope.inbox = [];//keeps the inbox trought all the states - so no need to get history more then once
			$rootScope.unread = {
				news: 0,
				explore: 0,
				inbox: 0,
				currentTab: 1
			}
			$rootScope.hotTraders = [];//keeps the hotTraders trough all the states - so no need to get history more then once
			$rootScope.hotTrades = [];//keeps the hotTrades trough all the states - so no need to get history more then once
			$rootScope.hotCopiers = [];//keeps the hotCopiers trough all the states - so no need to get history more then once
			$rootScope.assetSpecialists = [];//keeps the asset specialists trough all the states - so no need to get history more then once
			
			if (angular.isDefined($rootScope.hotTradesTimer)) {
				$interval.cancel($rootScope.hotTradesTimer);
			}
			$rootScope.hotData = {
				hotTraders: {},
				hotTrades: {},
				hotCopiers: {},
				assetSpecialists: {}
			};//keeps the hotCopiers,hotTrades,hotTraders dropdawns trought all the states - so no need to get history more then once
			$rootScope.skinCurrencies = [];//array Currencies for current skin
			$rootScope.copyOpShoppingBag = [];//copyop shopping bag
			$rootScope.shoppingBag = [];//shopping bag
			$rootScope.shoppingBagTimer = null;
			
			$rootScope.followData = {};//data for open follow popup
			
			if (!isUndefined($rootScope.lsCoptyOp)) {
				if (!isUndefined($rootScope.lsCoptyOp.newsConnect) && $rootScope.lsCoptyOp.newsConnect.isSubscribed()) {
					$rootScope.lsCoptyOp.lsCpClient.unsubscribe($rootScope.lsCoptyOp.newsConnect);
				}
				if (!isUndefined($rootScope.lsCoptyOp.exploreConnect) && $rootScope.lsCoptyOp.exploreConnect.isSubscribed()) {
					$rootScope.lsCoptyOp.lsCpClient.unsubscribe($rootScope.lsCoptyOp.exploreConnect);
				}
				if (!isUndefined($rootScope.lsCoptyOp.indexConnect) && $rootScope.lsCoptyOp.indexConnect.isSubscribed()) {
					$rootScope.lsCoptyOp.lsCpClient.unsubscribe($rootScope.lsCoptyOp.indexConnect);
				}
				if (!isUndefined($rootScope.lsCoptyOp.shoppingConnect) && $rootScope.lsCoptyOp.shoppingConnect.isSubscribed()) {
					$rootScope.lsCoptyOp.lsCpClient.unsubscribe($rootScope.lsCoptyOp.shoppingConnect);
				}
				if (!isUndefined($rootScope.lsCoptyOp.lsCpClient)) {
					$rootScope.lsCoptyOp.lsCpClient.disconnect();
				}
			}
			
			if (!isUndefined(lsClient)) {
				if (!isUndefined(tradingBox) && tradingBox.isSubscribed()) {
					lsClient.unsubscribe(tradingBox);
				}
				if (!isUndefined(assetsls) && assetsls.isSubscribed()) {
					lsClient.unsubscribe(assetsls);
				}
				// if (!isUndefined(lsClient)) {
					// lsClient.disconnect();
				// }
			}

			
			$rootScope.lsCoptyOp = {
				lsCpEngineReady: false,
				subscriptionType: "COMMAND",
				dataAdapterName: "JMS_AMQ_ADAPTER"
			}
			$rootScope.mainColHeightOld = 0;
			$rootScope.screenSizeOld = 0;
			
			
			if (!isUndefined($rootScope.feedTimers)) {
				clearTimeout($rootScope.feedTimers.news);
				clearTimeout($rootScope.feedTimers.explore);
				clearTimeout($rootScope.feedTimers.inbox);
			}
			
			$rootScope.feedTimers = {
				news: null,
				explore: null,
				inbox: null
			}
			
			$rootScope.base = settings.base;
			
			$rootScope.recaptchaKey = settings.recaptchaKey;
			
			$rootScope.balanceCallTypes = balanceCallTypes;
			
			$rootScope.isMobile = (detectDevice() != "");
			
			$rootScope.safeMod = {
				isSafeMode: false,
				maxTrades: 0,
				minCopyUsers: 0,
				period: 0
			}
			
			$rootScope.getCopyopCopyingRequested = false;
			$rootScope.getCopyopWatchingRequested = false;
		}
		
		$rootScope.initSettings();
		
		setInterval(function() {
			var inContent = g('inContent');
			var size = screenDimensions();
			$rootScope.mainColHeight = (inContent != null) ? inContent.offsetHeight : 0;
			// if ($rootScope.mainColHeightOld != $rootScope.mainColHeight || size.h != $rootScope.screenSizeOld) {
				$rootScope.mainColHeightOld = $rootScope.mainColHeight;
				$rootScope.screenSizeOld = size.h;
				setColumsHeight();
			// }
		}, 1000);
		
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (fromState.url == '^' || fromState.url == '/') {
				$rootScope.afterRefresh = true;
			} else {
				$rootScope.afterRefresh = false;
			}
			if (toState.needLogin == 2 && !settings.loggedIn && settings.loginDetected) {
				if (detectDevice() == '') {
					$rootScope.$state.go('ln.login', {ln: $rootScope.$state.params.ln});
				} else {
					$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
				}
				return;
			}
			if (detectDevice() != '' && ((isUndefined(toState.noMobile) && settings.loggedIn && toState.needLogin == 2) || (!isUndefined(toState.noMobile) && toState.noMobile))) {
				$rootScope.$state.go('ln.index', {ln: $rootScope.$state.params.ln});
				return;
			}
			window.scrollTo(0, 0);
			//redirect links that should not be accessed
			if (!isUndefined($rootScope.$state.current.redirectTo)) {
				$rootScope.$state.go($rootScope.$state.current.redirectTo, {ln: $rootScope.$state.params.ln});
			}
			if (!$rootScope.preventWalkThrough) {
				$rootScope.walkthroughTimeout = setTimeout(function() {
					$rootScope.checkForWoalktrough();
				},1000);
			}
			$rootScope.preventWalkThrough = false;
			
			setTimeout(function() {
				var url = window.location.href.split('.com')[1].split('/');
				url.splice(0,2);
				var newUrl = '';
				for (var i = 0; i < url.length; i++) {
					newUrl += url[i] + '/';
				}
				$rootScope.altHref = newUrl; 
			}, 0)
		});
		
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams) {
				//change browser title
				if (!isUndefined(toState.metadataKey)) {
					$rootScope.metadataTitle = toState.metadataKey;
				} else {
					$rootScope.metadataTitle = 'index';
				}
				
				//close any open ppups
				 $rootScope.closeCopyOpPopup({all:true});
				
				//LS stuff that not working properly
				if (toState.name.search('ln.in.a.asset') == -1) {
					if (!isUndefined(assetsls) && assetsls.isSubscribed()) {
						lsClient.unsubscribe(assetsls);
					}
					if (!isUndefined(tradingBox) && tradingBox.isSubscribed()) {
						lsClient.unsubscribe(tradingBox);
						markestInBox = [];
						if (typeof group != 'undefined') {
							for (var i = 0; i < group.length; i++) {
								box_objects[i].resetBox(false);
							}
						}
						box_objects = [];
						group = [];
					}
				}
				error_stack = [];
				// if (settings.loggedIn && fromState.needLogin > 1) {
					// $scope.getCopyopUser({onStateChange: true});
				// }
			})
	}]
)
.config(['$httpProvider', '$stateProvider', '$urlRouterProvider','$locationProvider',
	function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
		//$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		delete $httpProvider.defaults.headers.post['Content-Type'];
		$httpProvider.defaults.withCredentials = true;
		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		$urlRouterProvider
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
		.when('/login', '/tmp/login')
		.when('/about-us', '/tmp/about-us')
		.when('/how-it-works', '/tmp/how-it-works')
		.when('/signup', '/tmp/signup')
		.when('/general-terms', '/tmp/general-terms')
		.when('/password', '/tmp/password')
		.when('/faq', '/tmp/faq')
		.when('/contact-us', '/tmp/contact-us')
		.when('/privacy', '/tmp/privacy')
		.when('/pages/inatecDepositResult.jsf', '/en/home/main')
		.otherwise('/check/');

		// Use $stateProvider to configure your states.
		$stateProvider
			.state('ln', {
				url: '/:ln',
				templateUrl: 'html/minisite/template.html',
				'abstract': true,
				needLogin: 1//0 - no, 1 - doesn't matter, 2-yes
			})
			//minisite
			.state('ln.index', {
				url: '/',
				views: {
					'main': {
						templateUrl: 'html/minisite/index.html'
					}
				},
				needLogin: 0,
				metadataKey: 'index'
			})
			.state('ln.about-us', {
				url: '/about-us',
				views: {
					'main': {
						templateUrl: 'html/minisite/about-us.html'
					}
				},
				needLogin: 1,
				metadataKey: 'about-us'
			})
			.state('ln.anyoption', {
				url: '/anyoption',
				views: {
					'main': {
						templateUrl: 'html/minisite/anyoption.html',
						controller: 'acceptedTermsAndConditionsCtrl'
					}
				},
				needLogin: 0,
				noMobile: true
			})
			.state('ln.contact-us', {
				url: '/contact-us',
				views: {
					'main': {
						templateUrl: 'html/minisite/contact-us.html',
						controller: 'ContactCtrl'
					}
				},
				needLogin: 1
			})
			.state('ln.faq', {
				url: '/faq',
				views: {
					'main': {
						templateUrl: 'html/minisite/faq.html'
					}
				},
				needLogin: 1,
				metadataKey: 'faq'
			})
			.state('ln.finish-signup', {
				url: '/finish-signup',
				views: {
					'main': {
						templateUrl: 'html/minisite/finish-signup.html',
						controller: 'registerCtrl'
					}
				},
				needLogin: 0,
				noMobile: true
			})
			.state('ln.general-terms', {
				url: '/general-terms',
				views: {
					'main': {
						templateUrl: 'html/minisite/general-terms.html'
					}
				},
				needLogin: 1
			})
			.state('ln.general-terms.id', {
				url: '/:id',
				views: {
					'main': {
						templateUrl: 'html/minisite/general-terms.html'
					}
				},
				needLogin: 1
			})
			.state('ln.agreement', {
				url: '/agreement',
				views: {
					'main': {
						templateUrl: 'html/minisite/agreement.html'
					}
				},
				needLogin: 1
			})
			.state('ln.agreement-new', {
				url: '/agreement-new',
				views: {
					'main': {
						templateUrl: 'html/minisite/agreement-new.html'
					}
				},
				needLogin: 1
			})
			.state('ln.new-terms', {
				url: '/new-terms',
				views: {
					'main': {
						templateUrl: 'html/minisite/new-terms.html'
					}
				},
				needLogin: 1
			})
			.state('ln.login', {
				url: '/login',
				views: {
					'main': {
						templateUrl: 'html/minisite/login.html'
					}
				},
				needLogin: 0,
				noMobile: true
			})
			.state('ln.password', {
				url: '/password',
				views: {
					'main': {
						templateUrl: 'html/minisite/password.html',
						controller: 'sendPasswordCtrl',
					}
				},
				needLogin: 0
			})
			.state('ln.privacy', {
				url: '/privacy',
				views: {
					'main': {
						templateUrl: 'html/minisite/privacy.html',
					}
				},
				needLogin: 1,
				robotIndex: false,
				metadataKey: 'privacy'
			})
			.state('ln.security', {
				url: '/security',
				views: {
					'main': {
						templateUrl: 'html/minisite/security.html',
					}
				},
				needLogin: 1,
				robotIndex: false,
				metadataKey: 'security'
			})
			.state('ln.signup', {
				url: '/signup',
				views: {
					'main': {
						templateUrl: 'html/minisite/signup.html',
						controller: 'registerCtrl',
					}
				},
				needLogin: 0,
				noMobile: false
			})
			.state('ln.how-it-works', {
				url: '/how-it-works',
				views: {
					'main': {
						templateUrl: 'html/minisite/how-it-works.html',
						controller: 'howItWorksCtr',
					}
				},
				needLogin: 0
			})
			.state('ln.mobile-deposit', {
				url: '/mobile-deposit',
				views: {
					'main': {
						templateUrl: 'html/minisite/deposit.html',
						controller: 'MobileDepositController'
					}
				},
				needLogin: 2,
				noMobile: false
			})
			//home template
			.state('ln.home', {
				url: '/home/main',
				views: {
					'main': {
						templateUrl: 'html/templates/template-home.html',
					}
				},
				'abstract':true,
				needLogin: 2
			})
			.state('ln.home.a', {
				url: '',
				views: {
					'home-header': {
						templateUrl: 'html/templates/header-home.html',
					},
					'home-first': {
						templateUrl: 'html/templates/side-user.html',
						controller: 'loggedUserCtr'
					},
					'home-main': {
						templateUrl: 'html/templates/home.html'
					},
					'home-last': {
						templateUrl: 'html/templates/side-hot.html'
					}
				},
				needLogin: 2
			})
			//main template
			.state('ln.in', {
				url: '/home',
				views: {
					'main': {
						templateUrl: 'html/templates/template.html',
					}
				},
				redirectTo: 'ln.home.a',
				'abstract':true
			})
			.state('ln.in.a', {
				url: '',
				views: {
					'in-last': {
						templateUrl: 'html/templates/news-feed.html'
					}
				},
				redirectTo: 'ln.home.a',
				needLogin: 2
			})
			//personal
			.state('ln.in.a.personal', {
				url: '/personal',
				views: {
					'in-first@ln.in': {
						templateUrl: 'html/templates/side-menu.html',
					}
				},
				redirectTo: 'ln.in.a.personal.my-account.personal-details',
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account', {
				url: '/my-account',
				redirectTo: 'ln.in.a.personal.my-account.personal-details',
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.personal-details', {
				url: '/personal-details',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/personal-details.html',
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking', {
				url: '/banking',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/deposit-existing-card.html',
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.deposit-new-card', {
				url: '/deposit-new-card',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/deposit-new-card.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.edit-card', {
				url: '/edit-card',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/edit-card.html',
						controller: 'EditCardCtrl'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.bank-wire', {
				url: '/bank-wire',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/bank-wire.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.direct24', {
				url: '/direct24',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/direct-deposit.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.giropay', {
				url: '/giropay',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/direct-deposit.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.eps', {
				url: '/eps',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/direct-deposit.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.banking.neteller', {
				url: '/neteller',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/deposit-neteller.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.bonuses', {
				url: '/bonuses',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/bonuses.html',
						controller: 'UserBonusesCtrl'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.my-options', {
				url: '/my-options',
				'abstract': true, 
				redirectTo: 'ln.in.a.personal.my-account.my-options.open-options',
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.my-options.open-options', {
				url: '/open-options',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/my-options.html',
						controller: 'UserInvestmentsCtrl',
					}
				},
				isSettiled: false,
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.my-options.settled-options', {
				url: '/settled-options',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/my-options.html',
						controller: 'UserInvestmentsCtrl',
					}
				},
				isSettiled: true,
				needLogin: 2
			})
			.state('ln.in.a.personal.my-account.documents', {
				url: '/documents',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/documents.html'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.copyop-coins', {
				url: '/copyop-coins',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/copyop-coins.html',
						controller: 'copyopProfileCoinsCtrl'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.personal.settings', {
				url: '/settings',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/settings.html',
						controller: 'settingsCtr'
					}
				},
				needLogin: 2
			})
			//trading
			.state('ln.in.a.asset', {
				url: '/asset',
				views: {
					'in-first@ln.in': {
						templateUrl: 'html/templates/side-assets.html',
						controller: 'tradingAssetsCtr'
					},
					'in-content@ln.in': {
						templateUrl: 'html/templates/trading-area.html',
						controller: 'tradingCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.asset.details', {
				url: '/:marketId',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/trading-area.html',
						controller: 'tradingCtr'
					}
				},
				needLogin: 2
			})
			//user
			.state('ln.in.a.user', {
				url: '/user/:userId',
				views: {
					'in-first@ln.in': {
						templateUrl: 'html/templates/side-user.html',
						controller: 'copyopProfileCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.overview', {
				url: '/overview',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-overview.html',
						controller: 'copyopProfileOverviewCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.stats', {
				url: '/stats',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-stats.html',
						controller: 'copyopProfileStatsCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.connections', {
				url: '/connections'
			})
			.state('ln.in.a.user.connections.my-copiers', {
				url: '/my-copiers',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-connections.html',
						controller: 'copyopConnectionsCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.connections.my-watchers', {
				url: '/my-watchers',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-connections.html',
						controller: 'copyopConnectionsCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.connections.im-copying', {
				url: '/im-copying',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-connections.html',
						controller: 'copyopConnectionsCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.connections.im-watching', {
				url: '/im-watching',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-connections.html',
						controller: 'copyopConnectionsCtr'
					}
				},
				needLogin: 2
			})
			.state('ln.in.a.user.connections.facebook', {
				url: '/facebook',
				views: {
					'in-content@ln.in': {
						templateUrl: 'html/templates/user-page-connections.html',
						controller: 'copyopConnectionsCtr'
					}
				},
				needLogin: 2
			})

			// enable html5Mode for pushstate ('#'-less URLs)
			if (typeof window.server == "undefined") {
				$locationProvider.html5Mode(true).hashPrefix('!');
			}
	}]);
	
copyOpApp.config(['$compileProvider', function ($compileProvider) {
	// $compileProvider.debugInfoEnabled(false);
}]);

//preload combined template file
copyOpApp.factory('$templateCache', ['$cacheFactory', '$http', '$injector', function($cacheFactory, $http, $injector) {
	var cache = $cacheFactory('templates');
	var allTplPromise;

	return {
		get: function(url) {
			var fromCache = cache.get(url);
			if (url.search('minisite') > 0) {
				return;
			}

			// already have required template in the cache
			if (fromCache) {
				return fromCache;
			}

			// first template request ever - get the all tpl file
			if (!allTplPromise) {
				allTplPromise = $http.get('html/templates/GeneralTemplate.html')
					.then(function(response) {
						// compile the response, which will put stuff into the cache
						$injector.get('$compile')(response.data);
						return response;
					});
			}

			// return the all-tpl promise to all template requests
			return allTplPromise
				.then(function(response) {
					return {
						status: response.status,
						data: cache.get(url),
						headers: response.headers
					};
				});
		},

		put: function(key, value) {
			cache.put(key, value);
		}
	};
}]);


var copyOpMobileApp = angular.module('copyOpMobileApp', ['ngSanitize']);

copyOpMobileApp.controller('copyOpMobile', ['$sce', '$rootScope', '$scope', '$http', '$timeout', '$compile', function($sce, $rootScope, $scope, $http, $timeout, $compile) {
	var skinId = getUrlValue('s');
	settings.skinId = (skinId == '') ? 16 : skinId;
	settings.isRegulated = skinMap[settings.skinId].isRgulated;
	
	$scope.msgs = {};
	var getMsgsJson_first = true;
	//get all translations (bundle)
	$scope.getMsgsJson = function() {
		//skinMap[skinMap[settings.skinId].skinIdTexts].locale - seems legit :D
		var msgsUrl = settings.msgsPath + settings.msgsFileName + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ((getMsgsJson_first)?"":settings.msgsEUsufix) + settings.msgsExtension;
		$http.get(msgsUrl)
			.success(function(data, status) {
				if (typeof data == 'object') {
					if (getMsgsJson_first) {
						$scope.msgs = data;
					} else {
						$scope.msgs = jsonConcat($scope.msgs, data);
					}
					if (settings.isRegulated && getMsgsJson_first) {
						getMsgsJson_first = false;
						$scope.getMsgsJson();
					}
				}
			})
			.error(function( data, status ) {
				handleNetworkError(data);
			});
	}
	
	$scope.getMsgsJson();
	
	//get translation from key
	$rootScope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($scope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $scope.msgsParam($scope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
					// return $scope.msgsParam($scope.msgs[key], params);
				} else {
					return $scope.msgs[key];
				}
			} else {
				return "?msgs[" + key + "]?";
			}
		} else {
			if ($scope.msgs.hasOwnProperty(key[0])) {
				if ($scope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key[0]][key[1]], params));
					} else {
						return $scope.msgs[key[0]][key[1]];
					}
				} else {
					return "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	//get msg with param
	$scope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}
}]);

copyOpMobileApp.controller('generalTermsCtr', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.TERMS_TYPE_AGREEMENT = 1;
	$scope.TERMS_TYPE_GENERAL_TERMS = 2;
	$scope.TERMS_TYPE_RISK_DISCLOSURE = 3;
	$scope.menuShownBeforeId = 0;
	$scope.showMenu = function(file){
		if($scope.menuShownBeforeId == 0 && file.typeId == $scope.TERMS_TYPE_GENERAL_TERMS){
			$scope.menuShownBeforeId = file.id;
		}
		if($scope.menuShownBeforeId == file.id){
			return true;
		}
		return false;
	}
	
	$scope.requestsStatus = {}//0 - requested, 1 - responded error 2 - success
	var skinId = getUrlValue('s');
	skinId = (skinId == '') ? 16 : skinId;
	
	$scope.loading = true;
	var request = {
		"platformIdFilter": 3,
		"skinIdFilter": skinId,
		"termsType": 1
	};
	
	function filterTitle(str){
		var result = "";
		if(str){
			result = str.replace(/\"/g, "");
		}
		return result;
	}
	
	$http.post(settings.jsonLink + 'getTermsFiles', request)
		.success(function(data, status, headers, config) {
			if (!handleErrors(data, 'getTermsFiles')) {
				data.filesList.sort(function(a, b) {
					return parseInt(a.orderNum) - parseInt(b.orderNum);
				});
				
				$scope.isRegulated = settings.isRegulated;
				$scope.filesList = data.filesList;
				
				for (var i = 0; i < $scope.filesList.length; i++) {
					$scope.filesList[i].html = multipleReplace(replaceParamsGT, $scope.filesList[i].html);
					
					$scope.filesList[i].anchorHref = "#" + ($scope.filesList[i].title ? filterTitle($scope.filesList[i].title) : $scope.filesList[i].id);
					var linkTitle = $scope.filesList[i].title;
					if (!linkTitle) {
						$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h1:eq(0)").each(function(index, el) {
							linkTitle = el.innerHTML;
						});
						
						if (!linkTitle) {
							$('<div>' + $scope.filesList[i].html + '</div>').find(".general_terms_h2:eq(0)").each(function(index, el) {
								linkTitle = el.innerHTML;
							});
						}
					}
					if (linkTitle) {
						$scope.filesList[i].menuTitle = linkTitle;
					}
				}
			}
			$scope.loading = false;
			repositionAnchors();
			var anchor = '';
			window.location.search.substring(1)
				.split('&')
				.map(function(current) {
					var split = current.split('=');
					if (split[0] == 'anchor') {
						anchor = split[1];
					}
				})
			if (anchor != "") {
				$timeout(function() {
					goToByScroll(anchor, 0);
				}, 1000)
			}
		})
		.error(function(data, status, headers, config) {
			handleNetworkError(data);
		});
}]);

copyOpMobileApp.controller('AssetIndexController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.cols = [];
	// function waitForIt() {
		// if (!$rootScope.ready) {
			// setTimeout(function(){waitForIt()},50);
		// } else {
			// $scope.getAssetIndexMarketsPerSkin();
		// }
	// }

	
	$scope.assetIndexMarkets = [];//type 1: grpup, type 2: country, type 3: market
	$scope.lastConfig = {
		groupName: '',
		displayGroupNameKey: '',
		opportunityTypeId: ''
	};
	
	$scope.getAssetIndexMarketsPerSkin = function() {
		var skinId = getUrlValue('s');
		skinId = (skinId == '') ? 16 : skinId;
		var request = {
			skinId: skinId,
			writerId: 10000
		}
		
		$http.post(settings.commonServiceLink + 'AssetIndexServices/getAssetIndexMarketsPerSkin', request)
			.success(function(data, status) {
				$scope.assetIndexGeneral = {
					tooltipData: data.tooltipData,
					tooltipDataUnderTable: data.tooltipDataUnderTable,
					assetIndexFormulasBySkin: data.assetIndexFormulasBySkin
				}
				
				var i = 0;
				$.each(data.marketList, function(k, v) {
					if (v.opportunityTypeId == 1) {
						if ($scope.lastConfig.groupName != v.market.groupName) {
							if (!isUndefined(v.market.groupName)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.groupName),
									type: 1
								});
								$scope.lastConfig.groupName = v.market.groupName;
							}
						}
						
						if ($scope.lastConfig.displayGroupNameKey != v.market.displayGroupNameKey) {
							if (!isUndefined(v.market.displayGroupNameKey)) {
								$scope.assetIndexMarkets.push({
									title: $rootScope.getMsgs(v.market.displayGroupNameKey),
									type: 2
								});
								$scope.lastConfig.displayGroupNameKey = v.market.displayGroupNameKey;
							}
						}
					} else {
						if ($scope.lastConfig.opportunityTypeId != v.opportunityTypeId) {
							$scope.assetIndexMarkets.push({
								title: $rootScope.getMsgs('type-name-' + v.opportunityTypeId),
								type: 1
							});
							$scope.lastConfig.opportunityTypeId = v.opportunityTypeId;
						}
					}
					
					var temp = {
						type: 3,
						opportunityType: v.opportunityTypeId,
						id: v.market.id,
						marketGroupId: v.marketGroupId,
						title: v.market.displayName,
						description: v.marketAssetIndexInfo.marketDescription,
						descriptionPage: v.marketAssetIndexInfo.marketDescriptionPage,
						additionalText: v.marketAssetIndexInfo.additionalText,
						symbol: v.market.feedName,
						tradingTime: '',
						newMarket: v.market.newMarket,
						is24h7: v.marketAssetIndexInfo.is24h7,
						formulas: []
					}

					var startDay = -1;
					var endDay = -1;
					for (var d = 0; d < 7; d++) {
						if (v.marketAssetIndexInfo.tradingDays[d] == 1 && startDay == -1) {
							startDay = d;
						}
						if (v.marketAssetIndexInfo.tradingDays[d] == 1) {
							endDay = d;
						}
					}
					
					var orgStartDate = new Date(v.marketAssetIndexInfo.startTime);
					var startDate = new Date(new Date(orgStartDate.setDate(orgStartDate.getDate() - orgStartDate.getDay() + startDay)).getTime() - (orgStartDate.getTimezoneOffset() * 60 * 1000));//crazy shit
					
					var orgEndDate = new Date(v.marketAssetIndexInfo.endTime);
					var endDate = new Date(new Date(orgEndDate.setDate(orgEndDate.getDate() - orgEndDate.getDay() + endDay)).getTime() - (orgEndDate.getTimezoneOffset() * 60 * 1000));//crazy shit
				
					temp.tradingTime += $rootScope.getMsgs('day-' + startDate.getDay()) + ' - ' + $rootScope.getMsgs('day-' + endDate.getDay());
					temp.tradingTime += ' ' + startDate.getHours() + ':' + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()) + '-' + endDate.getHours() + ':' + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());
					
					for (var t = 0; t < v.marketAssetIndexInfo.expiryFormulaCalculations.length; t++) {
						if (t == 0) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].sameNextRow = false;
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 2) {
							temp.formulas.push(v.marketAssetIndexInfo.expiryFormulaCalculations[t]);
							temp.formulas[temp.formulas.length - 1].sameNextRow = false;
							if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId == v.marketAssetIndexInfo.expiryFormulaCalculations[0].expiryFormulaId) {
								temp.formulas[temp.formulas.length - 1].sameNextRow = true;
								temp.formulas[0].sameNextRow = true;
							}
							temp.formulas[temp.formulas.length - 1].formulaIndex = searchJsonKeyInArray(data.assetIndexFormulasBySkin, 'formulaId', v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryFormulaId);
						} else if (v.marketAssetIndexInfo.expiryFormulaCalculations[t].expiryTypeId == 3) {
							temp.formulas[temp.formulas.length - 1].expiryTypeId = 3;
						}
					}
					
					$scope.assetIndexMarkets.push(temp);
					i++;
				});
				
				
				$scope.AssetIndexCols = Math.round($scope.assetIndexMarkets.length / 3);
				$scope.cols.push({markets:[]});
				
				for (var i = 0; i < $scope.assetIndexMarkets.length; i++) {
					if ($scope.cols[$scope.cols.length - 1].markets.length > $scope.AssetIndexCols) {
						$scope.cols.push({markets:[]});
					}
					
					$scope.cols[$scope.cols.length-1].markets.push($scope.assetIndexMarkets[i]);
				}
				
				if (!isUndefined(window.marketId)) {
					var arrayIndex = searchJsonKeyInArray($scope.assetIndexMarkets, 'id', marketId);
					if (arrayIndex != -1) {
						$scope.selectedMarket = $scope.assetIndexMarkets[arrayIndex];
					}
				}
			})
			.error(function( data, status ) {
				$rootScope.handleNetworkError(data);
			});
	}

	$scope.getAssetIndexMarketsPerSkin();
	// waitForIt();
}]);
