copyOpApp.controller('registerCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	function init() {
		$scope.getCountryId();
		function checkVar() {
			if (isUndefined($scope.detectedCountryId)) {
				setTimeout(function() {
					checkVar()
				},50);
			}
		}
		checkVar();
		
		if (!isUndefined($scope.$parent.fbUserInfo)) {
			$scope.fbId = $scope.$parent.fbUserInfo.id;
			$scope.fbName = $scope.$parent.fbUserInfo.name;
			$scope.firstName = $scope.$parent.fbUserInfo.first_name;
			$scope.lastName = $scope.$parent.fbUserInfo.last_name;
			$scope.email = $scope.$parent.fbUserInfo.email;
			$scope.avatar = $rootScope.uploadedImage = $scope.$parent.fbUserInfo.avatar;
		} else {
			$rootScope.randAvatar();
		}
	}

	
	$scope.submitRegister = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var tmp = getInsertUserMethodRequest();
				var InsertUserMethodRequest = jsonClone(tmp, {
					//InsertUserMethodRequest
					nickname: $scope.nickname,
					avatar: $scope.avatar,
					fbId: $scope.fbId,
					acceptedTermsAndConditions: (($scope.acceptedTermsAndConditions) ? true : false),
					//AO InsertUserMethodRequest
					register: jsonClone(tmp.register, {
						userName: $scope.email,
						password: $scope.password,
						password2: $scope.password2,
						email: $scope.email,
						firstName: $scope.firstName,
						lastName: $scope.lastName,
						countryId: $rootScope.copyopUser.user.country.id,
						mobilePhone: $scope.mobilePhone,
						skinId: settings.skinId,
						combinationId: settings.combinationId,
						dynamicParam: settings.dynamicParam,
						httpReferer: settings.httpReferer,
						platformId: 0,
						aff_sub1: settings.aff_sub1,
						aff_sub2: settings.aff_sub2,
						aff_sub3: settings.aff_sub3
					}),
					skinId: settings.skinId
				});
				$http.post(settings.jsonLink + 'insertCopyopProfile', InsertUserMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.loginNotCheck = true;
						data.rplElm = "{link}";
						data.rplWith = 'ln.password({ln:skinLanguage})';  
						data.rplWith = $rootScope.$state.href('ln.password', {ln:$scope.skinLanguage}, {absolute: true});
						if (!handleErrors(data, 'insertCopyopProfile')) {
							settings.combinationId = data.user.combinationId;
							$rootScope.copyopUser = data;
							$scope.getPixel({pixelType: 2});
							$scope.addGTMPixel({pixelType: 'afterRegister'});
							$rootScope.copyopUser.user.name = data.user.firstName + ' ' + data.user.lastName;
							if ($scope.avatar != $rootScope.uploadedImage) {
								angular.element(g('startUpload')).scope().start(0);
							} else {
								$scope.afterComplete();
							}
							settings.loggedIn = true;
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	//get a random avatar
	$rootScope.randAvatar = function(sex) {
		if (typeof sex == 'undefined') {
			sex = Math.floor((Math.random() * 2));
		}
		var avatar = '';
		switch (sex) {
			case 0: avatar = 'm' + Math.floor((Math.random() * settings.numOfMaleAvatars) + 1) + '.png'; break;
			case 1: avatar = 'f' + Math.floor((Math.random() * settings.numOfFemaleAvatars) + 1) + '.png'; break;
		}
		$scope.avatar = $rootScope.uploadedImage = settings.jsonImagegLink + 'avatar/' + avatar;
		closeElement({id:'popUp-avatar'});
	}
	
	$scope.uploadComplate = function(data) {
		$rootScope.copyopUser.copyopProfile.avatar = data.errorMessages[0].message;
		$scope.afterComplete();
	}
	
	$scope.afterComplete = function() {
		var device = detectDevice();
		if(device != ''){
			$rootScope.$state.go('ln.mobile-deposit', {ln: $scope.skinLanguage});
		}else{
			$rootScope.preventWalkThrough = true;
			$rootScope.$state.go('ln.home.a', {ln: $scope.skinLanguage});
			$scope.resetWalkthrough();
			$scope.getCopyopUser({onStateChange: true, forceInit: true});
		}
		$scope.changeHeader(true);
	}
	waitForIt();
}]);

copyOpApp.controller('acceptedTermsAndConditionsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.registerForm = {};
	
	$scope.submitAcceptTerms = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var AcceptedTermsAndConditions = jsonClone(getUserMethodRequest(), {
					userName: $scope.loginForm.userName,
					password: $scope.loginForm.password,
					nickname: $scope.nickname,
					acceptedTermsAndConditions: (($scope.acceptedTermsAndConditions) ? true : false),
					avatar: $scope.avatar
				});
				$http.post(settings.jsonLink + 'acceptedTermsAndConditions', AcceptedTermsAndConditions)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.loginNotCheck = true;
						if (!handleErrors(data, 'acceptedTermsAndConditions')) {
							$rootScope.copyopUser = data;
							$rootScope.copyopUser.user.name = data.user.firstName + ' ' + data.user.lastName;
							if ($scope.avatar != $rootScope.uploadedImage) {
								angular.element(g('startUpload')).scope().start(0);
							} else {
								$scope.afterComplete();
							}
							settings.loggedIn = true;
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$rootScope.randAvatar = function(sex) {
		if (typeof sex == 'undefined') {
			sex = Math.floor((Math.random() * 2));
		}
		var avatar = '';
		switch (sex) {
			case 0: avatar = 'm' + Math.floor((Math.random() * settings.numOfMaleAvatars) + 1) + '.png'; break;
			case 1: avatar = 'f' + Math.floor((Math.random() * settings.numOfFemaleAvatars) + 1) + '.png'; break;
		}
		$scope.avatar = $rootScope.uploadedImage = settings.jsonImagegLink + 'avatar/' + avatar;
		closeElement({id:'popUp-avatar'});
	}
	$rootScope.randAvatar();

	$scope.uploadComplate = function(data) {
		$rootScope.copyopUser.copyopProfile.avatar = data.errorMessages[0].message;
		$scope.afterComplete();
	}
	
	$scope.afterComplete = function() {
		$scope.changeHeader(true);
		$rootScope.$state.go('ln.home.a', {ln: $scope.skinLanguage});
		$scope.resetWalkthrough();
		$scope.getCopyopUser({onStateChange: true, forceInit: true});
	}
}]);
