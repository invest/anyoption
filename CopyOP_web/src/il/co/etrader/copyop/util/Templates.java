package il.co.etrader.copyop.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class Templates {
	
	public static void main(String[] args) {
		traversFilePath(args[0], args[1]);
	}
	
	public static void traversFilePath(String filesPath, String finalDestinationPath) {
		File path = new File(filesPath);
		FilenameFilter textFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.endsWith(".html")) {
					return true;
				} else {
					return false;
				}
			}
		};
		File[] files = path.listFiles(textFilter);
		File template = new File(finalDestinationPath + "/GeneralTemplate.html");
		if(template.exists()){
			template.delete();
		}
		for (File file : files) {
			concatenateHTMLTemplates(file.toString(), template);
		}
	}
	
	public static void concatenateHTMLTemplates(String path, File template) {
		File dir = new File(path);
		String fileName = dir.getName();
		InputStream ins = null;
		Reader reader = null; 
		BufferedReader br = null;
		PrintWriter out = null;
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(template, true);
		    bw = new BufferedWriter(fw);
		    out = new PrintWriter(bw);
			String s;
			ins = new FileInputStream(dir);
			reader = new InputStreamReader(ins, "UTF-8");
		    br = new BufferedReader(reader);
		    out.print("<script type=\"text/ng-template\" id=\"html/templates/" + fileName + "\">");
		    while ((s = br.readLine()) != null) {
		    	if(!(s.isEmpty() || s.trim().equals("") || s.trim().equals("\n"))) {
		    		out.print(s);
		    	}
		    }
		    out.print("</script>");
			ins.close();
			reader.close();
			br.close();
			out.close();
			bw.close();
			fw.close();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if(ins!=null){
					ins.close();
				}
				if(reader!=null) {
					reader.close();
				}
				if(br!=null){
					br.close();
				}
				if(out!=null){
					out.close();
				}
				if(bw!=null){
					bw.close();
				}
				if(fw!=null){
					fw.close();
				}
			} catch (IOException ioe) {
				
			}
		}
	}

}
