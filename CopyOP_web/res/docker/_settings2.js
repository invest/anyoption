settings.jsonLink = 'https://www.DOCKER_ENV_NAME.copyop.com/jsonService/AnyoptionService/';
settings.commonServiceLink = 'https://www.DOCKER_ENV_NAME.copyop.com/jsonService/CommonService/AnyoptionService/';
settings.lsServer = 'https://ls.DOCKER_ENV_NAME.copyop.com';
settings.jsonImagegLink = 'http://www.DOCKER_ENV_NAME.copyop.com/jsonService/';
settings.optimoveActive = false;

settings.isLive = false;
settings.logIt_type = 4;
settings.base = '/';
