package com.etrader.android.app;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.RegisterCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Register;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.requests.RegisterAttemptMethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.util.OptionItem;

public class RegisterActivity extends RegisterCommonActivity {
	
	@Override
	public void init() {
		MethodRequest request = new MethodRequest();
		Utils.requestService(this, responseH, "citiesCallBack", request, new OptionsMethodResult(), "getCitiesOptions");
		AnyoptionApplication ap = (AnyoptionApplication)context.getApplicationContext();
		OptionsMethodResult omr = new OptionsMethodResult();
		ArrayList<OptionItem> mobilePref = ap.getMobilePref();
		omr.setOptions(mobilePref.toArray(new OptionItem[mobilePref.size()]));
//		Spinner mobilePrefix = (Spinner) findViewById(R.id.spinnerMobilePrefix);
//		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.planets_array, R.layout.my_spinner_textview);
//		    adapter.setDropDownViewResource(R.layout.my_spinner_textview);
//		    mobilePrefix.setAdapter(adapter);
//		    mobilePrefix.setOnItemSelectedListener(new MyOnItemSelectedListener());
		fillSpinner((Spinner) findViewById(R.id.spinnerMobilePrefix), omr);
		
		Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
        dateOfBirth.setText(getString(R.string.register_birthdate));                                
        // add a click listener to the date of birth
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(Constants.DIALOG_DATE_ID);
            }
        });        
        //Set default date (01/01/1980)
        birthDay = 1;
        birthMonth = Calendar.JANUARY;
        birthYear = 1980;
        
        editTextEmail.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if(!hasFocus) {        			
    				String error = null;
    				error = Utils.validateEmailAndEmpty(context, editTextEmail.getText().toString(), Constants.EMPTY_STRING);  
    				if (null == error) {
    					insertRegisterAttempt();
    				} 
        		}
			}
		});
        
        editTextMobile.setOnFocusChangeListener(new OnFocusChangeListener() {			
        	public void onFocusChange(View v, boolean hasFocus) {
        		if(!hasFocus) {        			
    				String error = null;
    				error = Utils.validateDigitsOnlyAndEmpty(context, editTextMobile.getText().toString(), Constants.EMPTY_STRING);  
    				if (null == error) {
    					insertRegisterAttempt();
    				}
        		}        			
			}
		});
		 
	}
	
	@Override
	public void setUserUniqueDetalis(Register user) {
		EditText userName = (EditText)findViewById(R.id.editTextUserName);
		user.setUserName(userName.getText().toString());
		user.setContactBySms(true);
		user.setCountryId(1);
		user.setCurrencyId(1);
		user.setIdNum(((EditText) findViewById(R.id.editTextIdNum)).getText().toString());
		
    	user.setStreet(((EditText)findViewById(R.id.editTextStreet)).getText().toString());

    	EditText zipCode   = (EditText)findViewById(R.id.editTextZipCode);
    	RadioButton RadioGenderMale = (RadioButton)findViewById(R.id.radioMale); 
    	
    	user.setZipCode(zipCode.getText().toString());
    	user.setGender(RadioGenderMale.isChecked()? "M" : "F");

    	user.setBirthDay(String.valueOf(birthDay));
    	user.setBirthMonth(String.valueOf(birthMonth + 1));
    	user.setBirthYear(String.valueOf(birthYear));
	}
	
	@Override
	public HashMap<String, String> validateCityDetails() {
		HashMap<String, String> result = new HashMap<String, String>();
		String houseNumber = ((EditText) findViewById(R.id.editTextStreetNo)).getText().toString();
		String error = null;
		error = Utils.isFieldEmpty(context, houseNumber, context.getString(R.string.register_streetno));
		if (error != null) {
    		result.put(CITY_DETAILS_ERROR, error);
    		return result;
    	}
		error = Utils.validateStreetNo(context, houseNumber);
		if (error != null) {
    		result.put(CITY_DETAILS_ERROR, error);
    		return result;
    	}
		Spinner spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
		long cityId = ((OptionItem)spinnerCity.getAdapter().getItem(spinnerCity.getSelectedItemPosition())).getId();
		if (cityId == 0) {//no city is selected
			result.put(CITY_DETAILS_ERROR, Utils.isFieldEmpty(context, "", getString(R.string.register_city)));
			return result;
		}
		result.put(CITY_DETAILS_ERROR, null);
		result.put(CITY_DETAILS_CITY, String.valueOf(cityId));
		result.put(CITY_DETAILS_STREETNO, houseNumber);
		return result;
	}
	
	public void citiesCallBack(Object result) {
		Spinner spinner = (Spinner) findViewById(R.id.spinnerCity);
		fillSpinner(spinner, result);
	}
	
	@Override
	public void setInsertRegisterRequestAttempt (RegisterAttemptMethodRequest request) {
		request.setCountryId(1); //Israel country
		request.setContactBySMS(true);
	}

	@Override
	public String mobilePrefixValidate() {
		Spinner spinnerPrefix = (Spinner) findViewById(R.id.spinnerMobilePrefix);
		long prefixId = ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getId();
		Log.e("", "" + prefixId);
		if (prefixId == 0) {//no prefix is selected
			return String.format(context.getString(R.string.error_single_required), 
					getString(R.string.error_register_prefix));	
		}
		mobilePrefixText += ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getValue();
		return null;
	}

	@Override
	public String validateNumId() {
		return Utils.validateIdNum(context, ((EditText) findViewById(R.id.editTextIdNum)).getText().toString());
	}

	@Override
	public void validateDateOfBirth() {
		//Date of birth
		String error;
		Button dateOfBirth = (Button)findViewById(R.id.buttonDateOfBirth);
    	if (dateOfBirth.getText().toString().equalsIgnoreCase(getString(R.string.register_birthdate))){
    		error = String.format(context.getString(R.string.error_single_required), dateOfBirth.getText().toString());
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));    		    		  
    		return ;    	   
    	}
    	
    	//Check if under 18
    	Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(birthYear, birthMonth, birthDay);
        cal.add(Calendar.YEAR, 18);
        if (cal.after(Calendar.getInstance())) {
        	error = context.getString(R.string.error_register_birthdate);
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));    		    		  
    		return ;    	   
    	}
	}

	@Override
	public String validateStreet() {
		EditText street   = (EditText)findViewById(R.id.editTextStreet);
		return Utils.validateStreetAndEmpty(context, street.getText().toString(), getString(R.string.register_street));
	}

	@Override
	public String validateUserName() {
		EditText userName = (EditText)findViewById(R.id.editTextUserName);
		return Utils.validateUserName(context, userName.getText().toString());
	}
}
