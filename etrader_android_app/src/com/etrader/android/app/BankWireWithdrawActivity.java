/**
 * 
 */
package com.etrader.android.app;

import java.util.HashMap;

import android.widget.Spinner;
import com.anyoption.android.app.BankWireWithdrawCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.Wire;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.results.OptionsMethodResult;
import com.anyoption.json.util.OptionItem;
/**
 * @author IdanZ
 *
 */
public class BankWireWithdrawActivity extends BankWireWithdrawCommonActivity {
	
	boolean isGoodBranch = false;
	
	@Override
	public void init() {
		MethodRequest request = new MethodRequest();
		Utils.requestService(this, responseH, "banksCallBack", request, new OptionsMethodResult(), "getBanksET");
			
	}
	
	public void banksCallBack(Object result) {
		Spinner spinner = (Spinner) findViewById(R.id.spinnerBanks);
		Utils.fillSpinner(spinner, result, this);
	}
	
//	public void banksBranchesCallBack(Object result) {
//		errorBranches = result.is
//	}
	
	
	public HashMap<String, String> validateBanksDetails() {
		HashMap<String, String> result = new HashMap<String, String>();		
		Spinner spinnerBanks = (Spinner) findViewById(R.id.spinnerBanks);
		long bankId = ((OptionItem)spinnerBanks.getAdapter().getItem(spinnerBanks.getSelectedItemPosition())).getId();
		if (bankId == 0) {//no bank is selected
			result.put(BANK_DETAILS_ERROR, Utils.isFieldEmpty(this, "", getString(R.string.bank_wire_bank_name)));
			return result;
		}
		eTBankId = bankId;
		result.put(BANK_DETAILS_ERROR, null);
		result.put(BANK_DETAILS_CITY, String.valueOf("bankId"));
		return result;
	}
	
	@Override
	public String setParams(Wire wire, String error) {
		HashMap<String, String> bankDetails = validateBanksDetails();
		error = bankDetails.get(BANK_DETAILS_ERROR);
		if (error != null){
    		Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
    		return error;
    	}		
//		WireMethodRequest request = new WireMethodRequest();
//		Wire wire = new Wire();
//		wire.setBranch(branch)
//		request.setWire(wire)
//		Utils.requestService(this, responseH, "banksCallBack", request, new OptionsMethodResult(), "getBanksET");
		return error;		
	}
	
//	public String validateBankBranch() {
//		boolean isGoodbranchCode = false;
//        for (BankBranches bankBranch : ApplicationData.getBankBranches()) {
//            if (wire.getBankId().equalsIgnoreCase(bankBranch.getBankId().toString()) && //check that this bank and branch code are real
//                    wire.getBranch().equals(bankBranch.getBranchCode().toString())) {
//                isGoodbranchCode = true;
//                break;
//            }
//        }
//        if (!isGoodbranchCode) {
//            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.branch.code",null),null);
//            context.addMessage("wireForm:branch", fm);
//            return null;
//        }
//		return error;
//		 
//	}
}
