package com.etrader.android.app;

import java.util.HashMap;

import android.widget.TextView;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.ForgotPasswordCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.beans.base.Register;
import com.anyoption.common.beans.base.Country;

/**
 * @author IdanZ
 */
public class ForgotPasswordActivity extends ForgotPasswordCommonActivity{
	@Override
	public void init() {
        TextView mobilePhonePrefix = (TextView) findViewById(R.id.textViewMobilePhonePrefix);
        HashMap<Long, Country> cs = ((AnyoptionApplication) getApplication()).getCountries();
        Country c = cs.get(Constants.COUNTRY_ISRAEL_ID);
        mobilePhonePrefix.setText(c.getPhoneCode());
	}

	@Override
	public String setParams(Register user, String error) {		
		return error;
	}
}