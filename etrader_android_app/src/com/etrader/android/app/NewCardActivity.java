package com.etrader.android.app;


import android.widget.EditText;
import com.anyoption.android.app.NewCardCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.common.beans.base.CreditCard;

/**
 * @author KobiM
 *
 */
public class NewCardActivity extends NewCardCommonActivity {
            
    /** Called when the activity is first created. */

	@Override
	public String setHolderId(CreditCard card) {
		EditText holderID = (EditText)findViewById(R.id.editTextHolderId);
		//validate holder ID num
//		String error = null;
//		error = Utils.isFieldEmpty(context, ((EditText) findViewById(R.id.editTextHolderId)).getText().toString(), context.getString(R.string.register_id_num));
//		if (error != null){
//			Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));
//			return error;
//		}
		String error = Utils.validateIdNum(this, holderID.getText().toString()); 
    	if (null != error) {
    		Utils.showAlert(this, Constants.EMPTY_STRING, error, this.getString(R.string.alert_title));  
    		return error;
    	}
    	card.setHolderId(holderID.getText().toString());
    	return null;
	}
}