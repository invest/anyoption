/**
 * 
 */
package com.etrader.android.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anyoption.android.app.AnyoptionApplication;
import com.anyoption.android.app.ChangePersonalDetailsCommonActivity;
import com.anyoption.android.app.util.Constants;
import com.anyoption.android.app.util.Utils;
import com.anyoption.beans.base.City;
import com.anyoption.common.beans.base.User;
import com.anyoption.json.requests.MethodRequest;
import com.anyoption.json.util.OptionItem;

/**
 * @author IdanZ
 *
 */
public class ChangePersonalDetailsActivity extends ChangePersonalDetailsCommonActivity {
	
	protected static final String CITY_DETAILS_ERROR = "error";
	protected static final String CITY_DETAILS_CITY = "city";

	@Override
	public void init(User user) {
		((TextView) findViewById(R.id.personal_details_change_id)).setText(user.getIdNum());
		MethodRequest request = new MethodRequest();
		AnyoptionApplication ap = (AnyoptionApplication)context.getApplicationContext();
		Spinner spinnerCities = (Spinner) findViewById(R.id.spinnerCity);	     		 		               
        ArrayAdapter spinnerCitiesArrayAdapter = new ArrayAdapter(this,
              android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getCitiesArray());  
        spinnerCitiesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerCities.setAdapter(spinnerCitiesArrayAdapter);
        
        Spinner citiesSpinner = (Spinner) findViewById(R.id.spinnerCity);
        citiesSpinner.setOnItemSelectedListener(
    	    new AdapterView.OnItemSelectedListener() {
    	    	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {          	    	
    	    		ArrayAdapter<City> adapter = (ArrayAdapter<City>) parent.getAdapter();        	    		
    	    		City c = adapter.getItem(pos);      	            
				}
				public void onNothingSelected(AdapterView<?> arg0) {												
				}
    	    }
    	);     

		//set city spinner
		 int cityPosition = spinnerCitiesArrayAdapter.getPosition(((AnyoptionApplication)getApplication()).getCities().get(user.getCityId()));
		 spinnerCities.setSelection(cityPosition);
		 			
		//set mobile prefix spinner  
	    Spinner spinnerMobilePref = (Spinner) findViewById(R.id.spinnerMobilePrefix);
        ArrayAdapter spinnerMobilePrefArrayAdapter = new ArrayAdapter(this,
	              android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getMobilePref());  
        spinnerMobilePrefArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerMobilePref.setAdapter(spinnerMobilePrefArrayAdapter);
		
        ArrayList<OptionItem> mobilePrefList = ap.getMobilePref();
        int userMobilePrefId = 0;
        for (int i = 0 ; i < mobilePrefList.size() ; i++) {
        	if (user.getMobilePhonePrefix().equals(mobilePrefList.get(i).getValue())) {
        		userMobilePrefId = (int) mobilePrefList.get(i).getId();
        		Log.e(userMobilePrefId + " ", mobilePrefList.get(i).getValue());
        		
        	}
        }

		 if (user.getMobilePhonePrefix() != null) {		
			 int mobilePrefPosition = spinnerMobilePrefArrayAdapter.getPosition(mobilePrefList.get(userMobilePrefId));
			 spinnerMobilePref.setSelection(mobilePrefPosition);
		 }
		 	
		 
		//set landline prefix spinner
		 Spinner spinnerLandPref = (Spinner) findViewById(R.id.spinnerLandPrefix);
		 ArrayAdapter spinnerLandPrefArrayAdapter = new ArrayAdapter(this,
	              android.R.layout.simple_spinner_item, ((AnyoptionApplication)getApplication()).getEtLandPref());  
		 spinnerLandPrefArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		 spinnerLandPref.setAdapter(spinnerLandPrefArrayAdapter);
		 
		 if (null != user.getLandLinePhone()){
			 ArrayList<OptionItem> landPrefList = ap.getEtLandPref();
		        int userLandPrefId = 0;
		        for (int i = 0 ; i < landPrefList.size() ; i++) {
		        	if (user.getLandLinePhonePrefix().equals(landPrefList.get(i).getValue())) {
		        		userLandPrefId = (int) landPrefList.get(i).getId();
		        	}
		        }
			
			 if (user.getLandLinePhonePrefix() != null) {
				 int landPrefPosition = spinnerLandPrefArrayAdapter.getPosition(landPrefList.get(userLandPrefId));
				 spinnerLandPref.setSelection(landPrefPosition);
			 }
		 } 
	}
	
	@Override
	public String validateAndSet(String error, EditText phone) {
				
		Spinner spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
    	
    	
    	if (0 != ((City)spinnerCity.getAdapter().getItem(spinnerCity.getSelectedItemPosition())).getId()){
    		user.setCityId(((City)spinnerCity.getAdapter().getItem(spinnerCity.getSelectedItemPosition())).getId());
    		user.setCityName(((City)spinnerCity.getAdapter().getItem(spinnerCity.getSelectedItemPosition())).getName());
    	}
    	
    	//Mobile prefix for etrader
    	String mobilePrefixText = "";
    	error = mobilePrefixValidate(mobilePrefixText);//only implemented in etrader
    	if (error != null){
    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
    		return error;
    	}
    	
    	//Landline phone for etrader
    	if(!phone.getText().toString().equals("")) {
	    	String landPrefixText = "";
	    	error = landPrefixValidate(landPrefixText);//only implemented in etrader
	    	if (error != null){
	    		Utils.showAlert(context, Constants.EMPTY_STRING, error, context.getString(R.string.alert_title));  
	    		return error;
	    	}
    	}
    	
    	EditText street = (EditText)findViewById(R.id.personal_details_change_streetaddress);
    	//Street
    	error = Utils.validateStreetAndEmpty(context, street.getText().toString(), getString(R.string.register_street));
    	if (error!= null){
    		Utils.showAlert(context, "", error, context.getString(R.string.alert_title));  
    		return error;
    	}	
    	user.setStreet(street.getText().toString());
		return error;
	}
	
	public HashMap<String, String> validateCityDetails() {
		HashMap<String, String> result = new HashMap<String, String>();
		String houseNumber = ((EditText) findViewById(R.id.editTextStreetNo)).getText().toString();
		String error = Utils.validateStreetNo(context, houseNumber);
		if (error != null) {
    		result.put(CITY_DETAILS_ERROR, error);
    		return result;
    	}
		Spinner spinnerCountry = (Spinner) findViewById(R.id.spinnerCity);
		long cityId = ((OptionItem)spinnerCountry.getAdapter().getItem(spinnerCountry.getSelectedItemPosition())).getId();
		if (cityId == 0) {//no city is selected
			result.put(CITY_DETAILS_ERROR, Utils.isFieldEmpty(context, "", getString(R.string.register_city)));
			return result;
		}
		result.put(CITY_DETAILS_ERROR, null);
		result.put(CITY_DETAILS_CITY, String.valueOf(cityId));
		return result;
	}

	public String mobilePrefixValidate(String mobilePrefixText) {
		Spinner spinnerPrefix = (Spinner) findViewById(R.id.spinnerMobilePrefix);
		long prefixId = ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getId();
		Log.e("", "" + prefixId);
		if (prefixId == 0) {//no prefix is selected
			return String.format(context.getString(R.string.error_single_required), 
					getString(R.string.error_register_prefix));	
		}
		mobilePrefixText += ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getValue();
		user.setMobilePhonePrefix(mobilePrefixText);
		return null;
	}
	
	public String landPrefixValidate(String landPrefixText) {
		Spinner spinnerPrefix = (Spinner) findViewById(R.id.spinnerLandPrefix);
		long prefixId = ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getId();
		Log.e("", "" + prefixId);
		if (prefixId == 0) {//no prefix is selected
			return String.format(context.getString(R.string.error_single_required), 
					getString(R.string.error_register_prefix));	
		}
		landPrefixText += ((OptionItem)spinnerPrefix.getAdapter().getItem(spinnerPrefix.getSelectedItemPosition())).getValue();
		landPrefix = landPrefixText;
		//user.setLandLinePhonePrefix(landPrefixText);
		return null;
	}
}
