package com.copyop.service.beans;

public class InvestmentInfo {
	long investmentId;
	long amount;
	long currencyId;
	double rate;
	
	public long getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "InvestmentInfo [investmentId=" + investmentId + ", amount="
				+ amount + ", currencyId=" + currencyId + ", rate=" + rate
				+ "]";
	}

}
