/**
 *
 */
package com.copyop.service.beans;

/**
 * @author pavelhe
 *
 */
public class UserPushNotificationInfo {

	private long id;
	private String skinId;
	private String currencyId;
	private String offsetUTC;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the skinId
	 */
	public String getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(String skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the currencyId
	 */
	public String getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the offsetUTC
	 */
	public String getOffsetUTC() {
		return offsetUTC;
	}
	/**
	 * @param offsetUTC the offsetUTC to set
	 */
	public void setOffsetUTC(String offsetUTC) {
		this.offsetUTC = offsetUTC;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserPushNotificationInfo [id=").append(id)
				.append(", skinId=").append(skinId)
				.append(", currencyId=").append(currencyId)
				.append(", offsetUTC=").append(offsetUTC)
				.append("]");
		return builder.toString();
	}

}
