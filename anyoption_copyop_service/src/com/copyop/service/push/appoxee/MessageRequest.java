/**
 *
 */
package com.copyop.service.push.appoxee;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.copyop.common.enums.base.UpdateTypeEnum;
import com.google.gson.annotations.SerializedName;

/**
 * @author pavelhe
 *
 */
public class MessageRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final String SOUND_DEFAULT_NAME = "default";
	private static final int MESSAGE_TYPE_BROADCAST_ID = 0;
	private static final int CONTENT_TYPE_PUSH_ONLY_ID = 1;
	private static final int PUSH_BADGE_NUMBER = 1; // Not sure what this is
	private static final int SCHEDULE_TYPE_SEND_NOW_ID = 0;

	@SerializedName("campaign_id")
	private String campaignId;

	@SerializedName("application_id")
	private long applicationId;

	@SerializedName("name")
	private String messageName;

	@SerializedName("description")
	private String messageDescription;

	private String sound;

	@SerializedName("type")
	private int messageType;

	@SerializedName("content_type")
	private int contentType;

	@SerializedName("push_body")
	private String pushBody;

	@SerializedName("push_badge")
	private int pushBadge;

	@SerializedName("schedule_type")
	private int scheduleType;

	private List<SegmentIdWrapper> segments;

	private Map<String, String> payload;

	public MessageRequest(UpdateTypeEnum updateType,
							String campaignId,
							String messageText,
							Map<String, String> payload) {
		this.campaignId = campaignId;
		this.messageName = updateType.name() + "(" + updateType.getId() + ")";
		this.messageDescription = this.messageName;
		this.sound = SOUND_DEFAULT_NAME;
		this.messageType = MESSAGE_TYPE_BROADCAST_ID;
		this.contentType = CONTENT_TYPE_PUSH_ONLY_ID;
		this.pushBody = messageText;
		this.pushBadge = PUSH_BADGE_NUMBER;
		this.scheduleType = SCHEDULE_TYPE_SEND_NOW_ID;
		this.payload = payload;
	}

	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the applicationId
	 */
	public long getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the messageName
	 */
	public String getMessageName() {
		return messageName;
	}

	/**
	 * @param messageName the messageName to set
	 */
	public void setMessageName(String messageName) {
		this.messageName = messageName;
	}

	/**
	 * @return the messageDescription
	 */
	public String getMessageDescription() {
		return messageDescription;
	}

	/**
	 * @param messageDescription the messageDescription to set
	 */
	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}

	/**
	 * @return the sound
	 */
	public String getSound() {
		return sound;
	}

	/**
	 * @param sound the sound to set
	 */
	public void setSound(String sound) {
		this.sound = sound;
	}

	/**
	 * @return the messageType
	 */
	public int getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the contentType
	 */
	public int getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the pushBody
	 */
	public String getPushBody() {
		return pushBody;
	}

	/**
	 * @param pushBody the pushBody to set
	 */
	public void setPushBody(String pushBody) {
		this.pushBody = pushBody;
	}

	/**
	 * @return the pushBadge
	 */
	public int getPushBadge() {
		return pushBadge;
	}

	/**
	 * @param pushBadge the pushBadge to set
	 */
	public void setPushBadge(int pushBadge) {
		this.pushBadge = pushBadge;
	}

	/**
	 * @return the scheduleType
	 */
	public int getScheduleType() {
		return scheduleType;
	}

	/**
	 * @param scheduleType the scheduleType to set
	 */
	public void setScheduleType(int scheduleType) {
		this.scheduleType = scheduleType;
	}

	/**
	 * @return the segments
	 */
	public List<SegmentIdWrapper> getSegments() {
		return segments;
	}

	/**
	 * @param segments the segments to set
	 */
	public void setSegments(List<SegmentIdWrapper> segments) {
		this.segments = segments;
	}

	/**
	 * @return the payload
	 */
	public Map<String, String> getPayload() {
		return payload;
	}

	/**
	 * @param payload the payload to set
	 */
	public void setPayload(Map<String, String> payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageRequest [campaignId=").append(campaignId)
				.append(", applicationId=").append(applicationId)
				.append(", messageName=").append(messageName)
				.append(", messageDescription=").append(messageDescription)
				.append(", sound=").append(sound)
				.append(", messageType=").append(messageType)
				.append(", contentType=").append(contentType)
				.append(", pushBody=").append(pushBody)
				.append(", pushBadge=").append(pushBadge)
				.append(", scheduleType=").append(scheduleType)
				.append(", segments=").append(segments)
				.append(", payload=").append(payload)
				.append("]");
		return builder.toString();
	}

}
