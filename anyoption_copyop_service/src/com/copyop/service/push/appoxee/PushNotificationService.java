/**
 *
 */
package com.copyop.service.push.appoxee;

import java.util.Collections;
import java.util.Map;

import org.apache.log4j.Logger;

import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.service.manager.AppoxeeManager;
import com.copyop.service.push.PushNotificationServiceIfc;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author pavelhe
 *
 */
public class PushNotificationService implements PushNotificationServiceIfc {

	private static final Logger log = Logger.getLogger(PushNotificationService.class);

	private Gson gson;

	private String environmentPrefix;

	public PushNotificationService(String environmentPrefix) {
		this.environmentPrefix = environmentPrefix + "_";
	}

	@Override
	public void init() {
		log.info("Initializing...");

		gson = new GsonBuilder().disableHtmlEscaping().create();
		log.info("Initializing done");
	}

	@Override
	public void destroy() {
		log.info("Destroying...");
		/* Nothing to destroy */
		log.info("Destroying done");
	}

	@Override
	public void sendPushNotification(UpdateMessage uMsg, String textToSend, Map<String, String> payload) {
		log.info("Sending appoxee push notification with text: " + textToSend);

		Map<Long, Long> segmentsByAppId = AppoxeeManager.getUserSegments(uMsg.getUserId());

		if (segmentsByAppId != null && segmentsByAppId.size() > 0) {
			boolean userTemplateAPI = Boolean.parseBoolean(ConfigurationCopyopManager.getCopyopPropertiesValue(
																					"APPOXEE_USE_TEMPLATE_API",
																					"true"));
			if (userTemplateAPI) {
				sendPushNotificationWithTemplateAPI(uMsg, textToSend, payload, segmentsByAppId);
			} else {
				sendPushNotificationWithStandardAPI(uMsg, textToSend, payload, segmentsByAppId);
			}
		} else {
			log.info("No appoxee segments for recepient. Push notification will NOT be sent");
		}
	}

	/**
	 * @param uMsg
	 * @param textToSend
	 * @param payload
	 * @param segmentsByAppId
	 */
	private void sendPushNotificationWithStandardAPI(UpdateMessage uMsg,
														String textToSend,
														Map<String, String> payload,
														Map<Long, Long> segmentsByAppId) {
		String requestUrl = ConfigurationCopyopManager.getCopyopPropertiesValue(
															"APPOXEE_PUSH_URL",
															"http://saas.appoxee.com/api/v3/message?finalize=1");
		String campaignId = ConfigurationCopyopManager.getCopyopPropertiesValue("APPOXEE_USER_UPDATES_CAMPAIGN_ID", "167268");
		MessageRequest mr = new MessageRequest(uMsg.getUpdateType(), campaignId, textToSend, payload);
		for (Map.Entry<Long, Long> segmentByAppId : segmentsByAppId.entrySet()) {
			mr.setApplicationId(segmentByAppId.getKey());
			mr.setSegments(Collections.singletonList(new SegmentIdWrapper(segmentByAppId.getValue())));
			AppoxeeManager.sendPostRequest(requestUrl, gson.toJson(mr));
		}
	}

	/**
	 * @param uMsg
	 * @param textToSend
	 * @param payload
	 * @param segmentsByAppId
	 */
	private void sendPushNotificationWithTemplateAPI(UpdateMessage uMsg,
														String textToSend,
														Map<String, String> payload,
														Map<Long, Long> segmentsByAppId) {
		String requestUrl = ConfigurationCopyopManager.getCopyopPropertiesValue("APPOXEE_PUSH_TEMPLATE_URL",
																				"https://saas.appoxee.com/api/v3/message/direct");
		String templateId = null;
		MessageTemplateRequest mtr = new MessageTemplateRequest(textToSend,
																payload,
																environmentPrefix + uMsg.getUserId());
		for (Map.Entry<Long, Long> segmentByAppId : segmentsByAppId.entrySet()) {
			templateId = ConfigurationCopyopManager.getCopyopPropertiesValue(
														"APPOXEE_TEMPLATE_FOR_APP_" + segmentByAppId.getKey(), null);
			if (templateId != null) {
				mtr.setTemplateId(templateId);
				AppoxeeManager.sendPostRequest(requestUrl, gson.toJson(mtr));
			} else {
				log.error("No template ID for app [" + segmentByAppId.getKey() + "]. Message will not be sent");
			}
		}
	}

}
