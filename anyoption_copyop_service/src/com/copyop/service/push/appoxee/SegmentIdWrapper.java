/**
 *
 */
package com.copyop.service.push.appoxee;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * @author pavelhe
 *
 */
public class SegmentIdWrapper implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName("segment_id")
	private long segmentId;

	public SegmentIdWrapper(long segmentId) {
		this.segmentId = segmentId;
	}

	/**
	 * @return the segmentId
	 */
	public long getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId the segmentId to set
	 */
	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}

	@Override
	public String toString() {
		return "SegmentIdWrapper [segmentId=" + segmentId + "]";
	}

}
