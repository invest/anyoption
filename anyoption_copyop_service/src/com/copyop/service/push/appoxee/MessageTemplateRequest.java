/**
 *
 */
package com.copyop.service.push.appoxee;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.copyop.common.beans.AppoxeeMessagesData;
import com.google.gson.annotations.SerializedName;

/**
 * @author pavelhe
 *
 */
public class MessageTemplateRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName("template_id")
	private String templateId;

	@SerializedName("messages_data")
	private List<? extends AppoxeeMessagesData> messageData;

	public MessageTemplateRequest(String messageText, Map<String, String> payload, String userAlias)  {
		this.messageData = Collections.singletonList(new AppoxeeMessagesData(userAlias,messageText, payload)); 
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the userAlias
	 */
	public List<? extends AppoxeeMessagesData> getMessageData() {
		return messageData;
	}

	/**
	 * @param userAlias the userAlias to set
	 */
	public void setMessageData(List<? extends AppoxeeMessagesData> messageData) {
		this.messageData = messageData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessageRequest [templateId=").append(templateId)
				.append(", messageData=").append(messageData)
				.append("]");
		return builder.toString();
	}

}
