/**
 *
 */
package com.copyop.service.push;

import java.util.Map;

import com.copyop.common.jms.updates.UpdateMessage;

/**
 * @author pavelhe
 *
 */
public interface PushNotificationServiceIfc {

	void init();

	void destroy();

	void sendPushNotification(UpdateMessage uMsg, String textToSend, Map<String, String> payload);

}
