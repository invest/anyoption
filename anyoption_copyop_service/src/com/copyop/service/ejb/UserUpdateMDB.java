/**
 *
 */
package com.copyop.service.ejb;

import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Pool;
import org.jboss.ejb3.annotation.ResourceAdapter;

import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.service.processor.UserUpdateProcessor;

/**
 * @author pavelhe
 *
 */
@MessageDriven(activationConfig = {
  @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
  @ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.FEED_DESTINATION_NAME) /*,
//  This property is NOT supported by ActiveMQ
//  @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "16")*/})
@ResourceAdapter("activemq-ra.rar")
// Pool configuration in standalone.xml
@Pool("mdb-updates-strict-max-pool")
public class UserUpdateMDB extends AbstractMDB {

	private static final Logger log = Logger.getLogger(UserUpdateMDB.class);

	private UserUpdateProcessor userUpdateProcessor;

	@Override
	protected void initialize() {
		log.info("Initializing MDB: " + this);
		userUpdateProcessor = new UserUpdateProcessor(systemEJB.getPushNotificationService());
	}

	@Override
	public void onMessage(Message msg) {
		if (log.isTraceEnabled()) {
			log.trace("On msg: " + msg);
		}

		if (msg instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) msg;
			UpdateMessage uMsg = null;
			try {
				uMsg = (UpdateMessage) objMsg.getObject();
			} catch (JMSException e) {
				log.error("JMS could not provide the message", e);
				/* WARNING - Method exit point */
				return;
			}
			if (uMsg != null) {
				userUpdateProcessor.process(uMsg);
			} else {
				log.error("Received [null] message");
			}
		} else {
			log.warn("Unexpected non-object message: " + msg);
		}

	}

	@PreDestroy
	public void destroy() {
		// The object is destroyed when the server is being stopped.
		log.info("Destroying MDB: " + this);
	}

}
