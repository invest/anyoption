package com.copyop.service.ejb;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.InvSettleEvent;
import com.copyop.service.processor.InvestmentSettleProcessor;

/**
 * Message-Driven Bean implementation class
 *
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.INV_SETTLE_DESTINATION_NAME)})
@ResourceAdapter("activemq-ra.rar")
public class InvestmentSettleMDB extends AbstractMDB {
		private static final Logger logger = Logger.getLogger(InvestmentSettleMDB.class);

		@Resource
		MessageDrivenContext ctx;
		
		@Override
		protected void initialize() {
			logger.trace("initialize");
		}

		@Override
		public void onMessage(Message message) {
			try {
					ObjectMessage oMsg = (ObjectMessage)message;
					InvSettleEvent invSettle = (InvSettleEvent)oMsg.getObject();
					InvestmentSettleProcessor isp = new InvestmentSettleProcessor();
					logger.trace(invSettle);
					boolean processed = isp.process(invSettle); 
					if(!processed) {
						logger.trace("retry settle inv");
						ctx.setRollbackOnly();
					}
			} catch (ClassCastException e) {
				logger.error("Wrong object in queue", e);
			} catch (JMSException e) {
				logger.error("Internal error", e);
			} catch (IllegalStateException e) {
				logger.error("Rollback error", e);
			} 
		}
}
