package com.copyop.service.ejb;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.InvCreateEvent;
import com.copyop.service.processor.InvestmentCreateProcessor;

/**
 * Message-Driven Bean implementation class
 *
 */
@MessageDriven(activationConfig = {
	      @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
	      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
	      @ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.INV_CREATE_DESTINATION_NAME) })
@ResourceAdapter("activemq-ra.rar")
public class InvestmentCreateMDB extends AbstractMDB {
		private static final Logger logger = Logger.getLogger(InvestmentCreateMDB.class);

		@Resource
		MessageDrivenContext ctx;

		
		@Override
		protected void initialize() {
			logger.info("initialize");
		}

		@Override
		public void onMessage(Message message) {
			try {
				ObjectMessage oMsg = (ObjectMessage)message;
				InvCreateEvent invCreate = (InvCreateEvent)oMsg.getObject();
				logger.trace(invCreate);
				InvestmentCreateProcessor icp = new InvestmentCreateProcessor();
				boolean processed = icp.process(invCreate);
				if(!processed) {
					logger.trace("retry create inv");
					ctx.setRollbackOnly();
				}
			} catch (ClassCastException e){
				logger.debug("Wrong object in queue", e);
				return;
			} catch (JMSException e) {
				logger.debug("Internal error", e);
				return;
			}
		}
}
