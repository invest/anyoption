package com.copyop.service.ejb;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.NamingException;

import org.jboss.logging.Logger;

import com.anyoption.common.jmx.AmqJMXMonitor;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.ManagerBase;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.service.manager.MarketsManager;
import com.copyop.service.manager.UpdateTextsManager;
import com.copyop.service.push.PushNotificationServiceIfc;
import com.copyop.service.push.appoxee.PushNotificationService;

@Singleton
@Startup
public class SystemEJB {
	private static final Logger logger = Logger.getLogger(SystemEJB.class);

	private boolean init = false;

	private PushNotificationServiceIfc pushNotificationService;
	private Properties serverProperties;

    public boolean isInit() {
        return init;
    }

    @PostConstruct
    public void init() throws IOException, NamingException {
    	serverProperties = new Properties();
    	serverProperties.load(this.getClass().getResourceAsStream("/META-INF/server.properties"));
    	logger.info("Loaded server properties: " + serverProperties.toString());
        AmqJMXMonitor monitor = new AmqJMXMonitor("copyop");
        AmqJMXMonitor.initConnectionFactory();
    	monitor.registerJMX();

    	ManagerBase.init(serverProperties.getProperty("cassandra.contact.points").split(","), "copyop");
    	ManagerBase.getSession();
    	ConfigurationCopyopManager.loadCopyopCfg();
    	pushNotificationService = new PushNotificationService(serverProperties.getProperty("environment.name"));
    	pushNotificationService.init();
    	UpdateTextsManager.loadUpdateTexts();
    	MarketsManager.loadMarketNamesCache();
    	ProfilesOnlineManager.initOnlineUsersCache();
    	ProfileManager.clearProfilesLocks();
    	init = true;
    	logger.debug("Init end.");
    }

    @PreDestroy
    public void cleanUp() {
    	pushNotificationService.destroy();
    	ProfilesOnlineManager.clearOnlineUsersCache();
    	ManagerBase.closeSession();
    	ManagerBase.closeCluster();
    	logger.debug("CleanUp end.");
    }

	/**
	 * @return the pushNotificationService
	 */
	public PushNotificationServiceIfc getPushNotificationService() {
		return pushNotificationService;
	}

	/**
	 * @return the serverProperties
	 */
	public Properties getServerProperties() {
		return serverProperties;
	}

}
