package com.copyop.service.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import com.copyop.common.jms.CopyOpEventSender;

/**
 * Message-Driven Bean implementation class
 *
 */
@MessageDriven(activationConfig = {
	      @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
	      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
	      @ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.MONITOR_PING) })
@ResourceAdapter("activemq-ra.rar")
public class MonitorMDB extends AbstractMDB {
		private static final Logger logger = Logger.getLogger(MonitorMDB.class);

		
		@Override
		protected void initialize() {
			logger.info("initialize");
		}

		@Override
		public void onMessage(Message message) {
			String src = "";
			try {
				src = message.getStringProperty("src");
			} catch (JMSException e) {
				/* ignore */
			}
			logger.debug("Received ping " + src);
		}
}
