package com.copyop.service.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.service.processor.ProfileManageProcessor;


/**
 * Message-Driven Bean implementation class
 *
 */
@MessageDriven(activationConfig = {
	      @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
	      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
	      @ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME) })
@ResourceAdapter("activemq-ra.rar")
public class ProfileManageMDB extends AbstractMDB {
		private static final Logger logger = Logger.getLogger(ProfileManageMDB.class);

		@Override
		protected void initialize() {
			logger.info("initialize");
		}

		@Override
		public void onMessage(Message message) {
			try{
				ObjectMessage oMsg = (ObjectMessage)message;
				ProfileManageEvent linkManage = (ProfileManageEvent) oMsg.getObject();
				ProfileManageProcessor pmp = new ProfileManageProcessor();
				pmp.process(linkManage);
			} catch (ClassCastException e){
				logger.debug("Wrong object in queue", e);
				return;
			} catch (JMSException e) {
				logger.debug("Internal error", e);
				return;
			}
		}
}

