/**
 *
 */
package com.copyop.service.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;

/**
 * @author pavelhe
 *
 */
public abstract class AbstractMDB implements MessageListener {

	private static final Logger log = Logger.getLogger(AbstractMDB.class);

	@EJB
	protected SystemEJB systemEJB;

	@PostConstruct
	public void init() {
		if (!systemEJB.isInit()) {
            log.error("Unexpected uninitialized: " + systemEJB + " when initializing " + this);
        }
		initialize();
	}

	protected abstract void initialize();

}
