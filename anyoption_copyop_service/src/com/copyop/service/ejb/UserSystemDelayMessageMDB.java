package com.copyop.service.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.service.processor.UserSystemDelayMessageProcessor;


/**
 * Message-Driven Bean implementation class
 *
 */
@MessageDriven(activationConfig = {
	      @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
	      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
	      @ActivationConfigProperty(propertyName = "destination", propertyValue = CopyOpEventSender.USER_DELAY_SYSTEM_MSG) })
@ResourceAdapter("activemq-ra.rar")
public class UserSystemDelayMessageMDB extends AbstractMDB {
		private static final Logger logger = Logger.getLogger(UserSystemDelayMessageMDB.class);

		@Override
		protected void initialize() {
			logger.info("initialize");
		}

		@Override
		public void onMessage(Message message) {
			try{
				ObjectMessage oMsg = (ObjectMessage)message;
				UpdateMessage uMsg = (UpdateMessage)oMsg.getObject();
				UserSystemDelayMessageProcessor usdp = new UserSystemDelayMessageProcessor();
				usdp.process(uMsg);
			} catch (ClassCastException e){
				logger.debug("Wrong object in queue", e);
				return;
			} catch (JMSException e) {
				logger.debug("Internal error", e);
				return;
			}
		}
}
