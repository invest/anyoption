package com.copyop.service.tournament;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentUsers;
import com.copyop.common.jms.events.TournamentInvestmentEvent;
import com.copyop.service.manager.TournamentUsersManager;

public abstract class TournamentTypeHandler {
	
	private Tournament tournament;
	private TournamentInvestmentEvent tournamentInvestmentEvent;
	private static final Logger logger = Logger.getLogger(TournamentTypeHandler.class);
	
	public TournamentTypeHandler() {
		
	}
	
	public TournamentTypeHandler(Tournament tournament, TournamentInvestmentEvent tournamentInvestmentEvent) {
		super();
		this.tournament = tournament;
		this.tournamentInvestmentEvent = tournamentInvestmentEvent;
	}

	public boolean process(Connection conn) throws SQLException {
		if (check()) {
			double score = 1;
			if (tournament.getType().getId() == Tournament.TOURNAMENT_TYPE_BY_TRADE_VOLUME) {
				score = Math.round(tournamentInvestmentEvent.getBaseAmount() / tournament.getMinInvAmountCents());
			}

			TournamentUsersManager.insertUpdateUser(conn, new TournamentUsers(tournament.getId(), tournamentInvestmentEvent.getUserId(), score, tournamentInvestmentEvent.getBaseAmount()));
			return true;
		}
		return true;
	}
	
	public abstract boolean check();

	public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public TournamentInvestmentEvent getTournamentInvestmentEvent() {
		return tournamentInvestmentEvent;
	}

	public void setTournamentInvestmentEvent(TournamentInvestmentEvent tournamentInvestmentEvent) {
		this.tournamentInvestmentEvent = tournamentInvestmentEvent;
	}

}
