package com.copyop.service.tournament;

import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.Tournament;
import com.copyop.common.jms.events.TournamentInvestmentEvent;

public class TournamentTypeByTradeHandler extends TournamentTypeHandler {
	
	public TournamentTypeByTradeHandler(Tournament tournament, TournamentInvestmentEvent tournamentInvestmentEvent) {
		super(tournament, tournamentInvestmentEvent);
	}

	@Override
	public boolean check() {
		if (getTournamentInvestmentEvent().getOpportunity_product_type_id() == OpportunityType.PRODUCT_TYPE_BINARY &&
				getTournament().getSkins().contains(getTournamentInvestmentEvent().getUserSkin()) &&
					getTournamentInvestmentEvent().getBaseAmount() >= getTournament().getMinInvAmountCents()) {
			return true;
		}
		return false;
	}

}
