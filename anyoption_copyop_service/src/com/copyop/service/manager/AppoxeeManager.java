/**
 *
 */
package com.copyop.service.manager;

import java.util.Map;

import org.apache.log4j.Logger;

import com.copyop.service.dao.AppoxeeDAO;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class AppoxeeManager extends com.copyop.common.managers.AppoxeeManager {

	private static final Logger log = Logger.getLogger(AppoxeeManager.class);

	public static Map<Long, Long> getUserSegments(long userId) {
		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		Map<Long,Long> segmentsByApplication = AppoxeeDAO.getUserSegments(session, userId);
		if (log.isTraceEnabled()) {
			log.trace("TIME: getUserSegments " + (System.currentTimeMillis() - beginTime) + "ms");
		}
		return segmentsByApplication;
	}

}
