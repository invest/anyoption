/**
 *
 */
package com.copyop.service.manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BaseBLManager;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.service.beans.UserPushNotificationInfo;
import com.copyop.service.dao.UsersDAO;

/**
 * @author pavelhe
 *
 */
public class UsersManager extends BaseBLManager {

	protected static final Logger log = Logger.getLogger(UsersManager.class);

	public static String getSkinIdByUserId(long userId) {
		Connection conn = null;
        try {
        	conn = getConnection();
            return UsersDAO.getSkinIdByUserId(conn, userId);
        } catch (SQLException e) {
			log.error("Unable to get skin ID for user [" + userId + "]. Returning default skin [" + Skin.SKIN_ENGLISH + "]", e);
			return String.valueOf(Skin.SKIN_ENGLISH);
        } finally {
        	if (conn != null) {
	            closeConnection(conn);
        	}
        }
	}

	public static Long getCurrencyIdByUserId(long userId) {
		Connection conn = null;
        try {
        	conn = getConnection();
            return UsersDAO.getCurrencyIdByUserId(conn, userId);
        } catch (SQLException e) {
			log.error("Unable to get currency ID for user [" + userId + "]." , e);
			return null;
        } finally {
        	if (conn != null) {
	            closeConnection(conn);
        	}
        }
	}

	public static Map<Long, String> getPushNotificationSkinsByUser(Collection<Long> users) {
		Connection conn = null;
        try {
        	conn = getConnection();
            return UsersDAO.getPushNotificationSkinsByUser(conn, users);
        } catch (SQLException e) {
			log.error("Unable to get users skins for push notifications", e);
        } finally {
        	if (conn != null) {
	        	try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
	            closeConnection(conn);
        	}
        }
        return new HashMap<Long, String>();
    }

	public static Map<Long, UserPushNotificationInfo> getUserPushNotificationInfo(Collection<Long> users) {
		Connection conn = null;
        try {
        	conn = getConnection();
            return UsersDAO.getUserPushNotificationInfo(conn, users);
        } catch (SQLException e) {
			log.error("Unable to get users info for push notifications", e);
        } finally {
        	if (conn != null) {
	        	try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
        	}
        	closeConnection(conn);
        }
        return new HashMap<Long, UserPushNotificationInfo>();
	}

	public static void addPushNotificationSkinToMessage(UpdateMessage msg, Map<Long, String> skinsByUser) {
		Map<String, String> clonedProps = new HashMap<String, String>(msg.getProperties());
		String skinId = skinsByUser.get(msg.getUserId());
		if (skinId == null) {
			skinId = String.valueOf(Skin.SKIN_ENGLISH); // use English skin in case of error
		}
 		clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_SKIN_ID, skinId);
		msg.setProperties(clonedProps);
	}

	public static void addPushNotificationUserInfo(UpdateMessage msg,
													Map<Long, UserPushNotificationInfo> userInfo,
													boolean addUTCOffset) {
		Map<String, String> clonedProps = new HashMap<String, String>(msg.getProperties());
		UserPushNotificationInfo user = userInfo.get(msg.getUserId());
		if (user != null) {
			clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_SKIN_ID, user.getSkinId()); // use English skin in case of error
			clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_CURRENCY_ID, user.getCurrencyId());
			if (addUTCOffset) {
				clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_UTC_OFFSET, user.getOffsetUTC());
			}
		} else {
			clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_SKIN_ID, String.valueOf(Skin.SKIN_ENGLISH));
			clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_CURRENCY_ID, String.valueOf(Currency.CURRENCY_USD_ID));
			if (addUTCOffset) {
				clonedProps.put(FeedMessage.PROPERTY_KEY_PUSH_UTC_OFFSET, "UTC");
			}
		}
		msg.setProperties(clonedProps);
	}
}
