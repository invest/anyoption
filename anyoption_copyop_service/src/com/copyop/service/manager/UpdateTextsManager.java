/**
 *
 */
package com.copyop.service.manager;

import java.text.MessageFormat;
import java.util.Map;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.managers.ManagerBase;
import com.copyop.service.dao.UpdateTextsDAO;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class UpdateTextsManager extends ManagerBase {
	private static final Logger log = Logger.getLogger(UpdateTextsManager.class);

	private static Map<UpdateTypeEnum, Map<QueueTypeEnum, Map<Integer, String>>> updateTextsCacheMap;

	public static void loadUpdateTexts() {
		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}

		updateTextsCacheMap = UpdateTextsDAO.loadUpdateTexts(session);

		if (log.isTraceEnabled()) {
			log.trace("TIME: loadUpdateTexts " + (System.currentTimeMillis() - beginTime) + " ms");
		}
	}

	public static String getUpdateText(UpdateTypeEnum updateType,
										QueueTypeEnum queueType,
										int skinId,
										Object... params) {
		try {
			Map<QueueTypeEnum, Map<Integer, String>> queueTypeMap = updateTextsCacheMap.get(updateType);
			Map<Integer, String> skinMap = queueTypeMap.get(queueType);
			String text = skinMap.get(skinId);
			return MessageFormat.format(text, params);
		} catch (Exception e) {
			log.error("Unable to get/parse message", e);
			return null;
		}
	}

}
