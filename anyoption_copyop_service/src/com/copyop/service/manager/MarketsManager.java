/**
 *
 */
package com.copyop.service.manager;

import java.sql.Connection;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.managers.BaseBLManager;
import com.copyop.service.dao.MarketsDAO;

/**
 * @author pavelhe
 *
 */
public class MarketsManager extends BaseBLManager {

	protected static final Logger log = Logger.getLogger(MarketsManager.class);

	private static Map<Integer, Map<Integer, MarketNameBySkin>> marketsBySkin = null;

	public static void loadMarketNamesCache() {
		Connection conn = null;
		try {
			conn = getConnection();
			marketsBySkin = MarketsDAO.getMarketNamesBySkin(conn);
		} catch (Exception e) {
			log.error("Could not load market names cache", e);
		} finally {
			closeConnection(conn);
		}
	}

	public static String getMarketShortName(int marketId, int skinId) {
		return marketsBySkin.get(marketId).get(skinId).getShortName();
	}

	public static String getMarketName(int marketId, int skinId) {
		return marketsBySkin.get(marketId).get(skinId).getName();
	}
}
