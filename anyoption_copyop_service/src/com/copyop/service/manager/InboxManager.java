/**
 *
 */
package com.copyop.service.manager;

import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.dto.base.FeedMessage;
import com.copyop.service.dao.InboxDAO;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


/**
 * @author pavelhe
 *
 */
public class InboxManager extends com.copyop.common.managers.InboxManager {
	private static final Logger log = Logger.getLogger(InboxManager.class);

	/**
	 * TODO JAVA DOC
	 *
	 * @param inboxMsg
	 */
	public static void insertInboxMsg(FeedMessage inboxMsg) {
		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		UUID insertUUID = UUIDs.timeBased();
		log.info("Inserting message [" + inboxMsg.getTimeCreatedUUID() + "] with new UUID [" + insertUUID + "]");
		InboxDAO.insertInboxMsg(session, inboxMsg, insertUUID);
		if (log.isTraceEnabled()) {
			log.trace("TIME: insert inbox " + (System.currentTimeMillis() - beginTime) + "ms");
		}
	}
}
