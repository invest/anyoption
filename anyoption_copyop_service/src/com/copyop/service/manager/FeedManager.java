/**
 *
 */
package com.copyop.service.manager;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.service.dao.FeedDAO;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author pavelhe
 *
 */
public class FeedManager extends com.copyop.common.managers.FeedManager {

	private static final Logger log = Logger.getLogger(FeedManager.class);

	/**
	 * TODO JAVA DOC
	 *
	 * @param userId
	 * @param queueType
	 * @param limit
	 * @return
	 */
	public static List<Date> getPostAlgorithmFeed(long userId, QueueTypeEnum queueType, int limit) {
		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<Date> feed = FeedDAO.getPostAlgorithmFeed(session, userId, queueType, limit);
		if (log.isTraceEnabled()) {
			log.trace("TIME: getPostAlgorithmFeed " + (System.currentTimeMillis() - beginTime) + "ms");
		}
		return feed;
	}

	public static void insertBatchPostAlgorithmMessage(UpdateMessage um, QueueTypeEnum queueType) {
		Session session = getSession();
		long beginTime = 0;
		String strTTL = ConfigurationCopyopManager.getCopyopPropertiesValue("FEED_TABLE_TTL", null);
		Integer ttl = null;
		if (strTTL != null) {
			ttl = Integer.parseInt(strTTL) * ONE_DAY_IN_SEC;
		}

		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}

		UUID insertUUID = UUIDs.timeBased();
		log.info("Inserting message [" + um.getTimeCreatedUUID() + "] in batch with new UUID [" + insertUUID + "]");
		FeedDAO.insertBatchPostAlgorithmMessage(session, um, queueType, ttl, insertUUID);

		if (log.isTraceEnabled()) {
			log.trace("TIME: insertBatchPostAlgorithmMessage " + (System.currentTimeMillis() - beginTime) + "ms");
		}
	}

	public static void insertPostAlgorithmMessage(UpdateMessage um, QueueTypeEnum queueType) {
		Session session = getSession();
		long beginTime = 0;

		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}

		UUID insertUUID = UUIDs.timeBased();
		log.info("Inserting message [" + um.getTimeCreatedUUID() + "] in post algorithm with new UUID [" + insertUUID + "]");
		FeedDAO.insertPostAlgorithmMessage(session, um, queueType, insertUUID);

		if (log.isTraceEnabled()) {
			log.trace("TIME: insertPostAlgorithmMessage " + (System.currentTimeMillis() - beginTime) + "ms");
		}
	}

}
