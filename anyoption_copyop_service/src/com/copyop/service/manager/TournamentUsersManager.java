/**
 * 
 */
package com.copyop.service.manager;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentUsers;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.TournamentManagerBase;
import com.copyop.common.jms.events.TournamentInvestmentEvent;
import com.copyop.service.dao.TournamentUsersDAO;
import com.copyop.service.tournament.TournamentTypeHandler;



/**
 * @author EyalG
 *
 */
public class TournamentUsersManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(TournamentUsersManager.class);
	
	public static boolean insertUpdateUserForAllTournaments(TournamentInvestmentEvent tournamentInvestmentEvent) {
		ArrayList<Tournament> tournaments;
		try {
			tournaments = TournamentManagerBase.getAllTournamentsCache();
			Connection conn = getConnection();		
			try {
				conn.setAutoCommit(false);
				for (Tournament tournament : tournaments) {
					logger.debug("checking is open" + tournament);
					if (tournament.isOpen()) {
						Class<?> clazz = Class.forName(tournament.getType().getProccessClass());
						Constructor<?> constructor = clazz.getConstructor(Tournament.class, TournamentInvestmentEvent.class);
						TournamentTypeHandler tournamentTypeHandler = (TournamentTypeHandler) constructor.newInstance(tournament, tournamentInvestmentEvent);
						tournamentTypeHandler.process(conn);					
					}
				}
				conn.commit();
				return true;
			} catch (Exception e) {
				logger.error("cant insert investment for user id : " + tournamentInvestmentEvent.getUserId(), e);
				try {
					conn.rollback();
				} catch (SQLException ie) {
					logger.log(Level.ERROR, "Can't rollback.", ie);
				}
				return false;
			} finally {
				conn.setAutoCommit(true);
				closeConnection(conn);
			}
		} catch (SQLException e1) {
			logger.error("cant insert investment for user id : " + tournamentInvestmentEvent.getUserId(), e1);
			return false;
		}
		
	}
	
	public static void insertUpdateUser(Connection conn, TournamentUsers tournamentUsers) throws SQLException {
		logger.debug("going to insert or update " + tournamentUsers);
		boolean isInserted = TournamentUsersDAO.insertUser(conn, tournamentUsers);
		if (!isInserted) {
			TournamentUsersDAO.updateUser(conn, tournamentUsers);
		}
	}

	
}
