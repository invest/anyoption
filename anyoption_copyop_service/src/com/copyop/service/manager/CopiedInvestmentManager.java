package com.copyop.service.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.jms.events.InvCreateEvent;
import com.copyop.common.managers.CurrenciesCacheManager;
import com.copyop.service.beans.InvestmentInfo;
import com.copyop.service.dao.CopiedInvestmentDAO;
import com.copyop.service.dao.UsersDAO;


public class CopiedInvestmentManager extends BaseBLManager{
	private static final Logger logger = Logger.getLogger(CopiedInvestmentManager.class);

	public static long getUserBalance(long userId) throws SQLException {
		Connection conn = null;
		try{
			conn = getConnection();
			return UsersDAO.getUserBalance(conn, userId );
		} finally {
			conn.close();
		}
	}


	public static InvestmentInfo insertCopiedInvestment(InvCreateEvent invCreate, long userId, long amount, long minAmount, long writerId, double oddsWin, double oddsLose) throws SQLException {
		// profiles before introducing writer_id in profile_links will have default writer
		if(writerId == 0) {
			writerId = Writer.WRITER_ID_COPYOP_WEB;
		}
		InvestmentInfo investment = new InvestmentInfo();
		Connection conn = null;
        long isFreeTypeId = 0;
        long niouBuId = 0;
		try{
			conn = getConnection();
			BonusUsers niouBuIdAndType = BonusDAOBase.getNextInvestOnUsBonusUserIdAndType(conn, userId);
			conn.setAutoCommit(false);
			
	   		long balance = UsersDAO.getUserBalance(conn, userId );
	   		if(balance < amount) {
//   				if(balance > minAmount ) {
//   					amount = balance;
//   				} else {
   					investment.setInvestmentId(-1);
   					return investment;
//   				}
	   		}

	        UsersDAO.addToBalance(conn, userId, -amount);	        
	        investment.setAmount(amount);
        	User user = UsersDAOBase.getUserById(conn, userId);
	        long currencyId = user.getCurrencyId();
			Double rate = 1d;
			Double rateForDB = 1d;
			if(currencyId != Currency.CURRENCY_USD_ID) { 
				rate = CurrenciesCacheManager.getCurrenciesRatesToUSD().get(currencyId);
				rateForDB = new BigDecimal(1).divide(new BigDecimal(rate), 5, RoundingMode.CEILING).doubleValue();
			}
			//Bonus
            if (niouBuIdAndType != null){
            	isFreeTypeId = niouBuIdAndType.getTypeId();

                //all the next invest on us types should mark as 1
                if (isFreeTypeId == Bonus.TYPE_INSTANT_NEXT_INVEST_ON_US ||
                		isFreeTypeId == Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING ||
                		isFreeTypeId == Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS) {
    				isFreeTypeId = 1;
                }

                niouBuId = niouBuIdAndType.getId();
                HashMap<String, Long> freeInvValues = BonusDAOBase.getMinAndMaxFreeInv(conn, niouBuId);

            	boolean isFree = freeInvValues.get(ConstantsBase.FREE_INVEST_MIN).longValue() <= amount &&
			                freeInvValues.get(ConstantsBase.FREE_INVEST_MAX).longValue() >= amount;

                if (isFree) {
		            isFree = BonusDAOBase.useNextInvestOnUs(conn, niouBuId, amount) > 0;
		        }

                // check again that we realy update the rows in db if not or if its not good amount set the type to 0
                if (!isFree) {
                	isFreeTypeId = 0;
	            }
            }
			
	        long id = CopiedInvestmentDAO.insertInvestment(conn, 
											        		userId, 
											        		invCreate.getOppId(),
											        		amount, 
											        		invCreate.getLevel(), 
											        		rateForDB, 
											        		invCreate.getInvId(), 
											        		CopyOpInvTypeEnum.COPY, 
											        		writerId, 
											        		invCreate.getDir(),
											        		isFreeTypeId,
											        		niouBuId,
											        		user.getUtcOffset(),
											        		oddsWin,
											        		oddsLose
											        		);
	        investment.setInvestmentId(id);
	        UsersDAO.insertBalanceLog(conn, Writer.WRITER_ID_AUTO, userId, ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, "GMT+00:00");
	        investment.setRate(rate);
	        investment.setCurrencyId(currencyId);
	        
	        if(logger.isTraceEnabled()){
	        	logger.trace("Inserted copied investment:"+id+" for user:"+userId);
	        }
	        conn.commit();
	        
	        user = UsersManagerBase.getUserById(userId);
	        
	        try {

	        	InvestmentsManagerBase.afterInvestmentSuccess(user, user.getBalance(), id, amount, writerId, isFreeTypeId > 0, Opportunity.TYPE_REGULAR, 0, 0, 0);
			} catch (Exception e) {
				logger.error("When bonus logic run", e);
			}
	        
	        //to remove log
	        logger.debug("for user:"+userId+" balance: "+user.getBalance()+" amount: "+amount+" id: "+id);
	        
	        try {
	        	InvestmentsManagerBase.insertBonusInvestment(id, userId, user.getBalance(), amount);
	        } catch (Exception e) {
	        	logger.error("BMS, Problem processing bonus investments.", e);
	        }
	        
	        return investment;
		} catch (Exception e) {
	        	conn.rollback();
	        	logger.error("!!!ERROR when insertCopiedInvestment and rollback invCreate[" + invCreate + "] ", e);
	        	return null;
		} finally {
			conn.setAutoCommit(true);
			conn.close();
		}
	}

	public static long userIdForInvestment(long invId) throws SQLException {
		Connection conn = null;
		try{
			conn = getConnection();
			return CopiedInvestmentDAO.userIdForInvestment(conn, invId);
		} finally {
			conn.close();
		}
	}
/*
	public static Double convertToBaseAmount(long amount, long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return CopiedInvestmentDAO.convertToBaseAmount(con, amount, userId);
		} finally {
			closeConnection(con);
		}
	}
*/
}
