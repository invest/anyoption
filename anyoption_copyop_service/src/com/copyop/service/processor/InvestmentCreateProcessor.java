package com.copyop.service.processor;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.CommonUtil;
import com.copyop.common.Constants;
import com.copyop.common.dto.Frozen;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.LevelServiceSender;
import com.copyop.common.jms.events.InvCreateEvent;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.CoinsManager;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CurrenciesCacheManager;
import com.copyop.common.managers.FbManager;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.FrozenCopyopManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.service.beans.InvestmentInfo;
import com.copyop.service.beans.UserPushNotificationInfo;
import com.copyop.service.manager.CopiedInvestmentManager;
import com.copyop.service.manager.UsersManager;
import com.datastax.driver.core.utils.UUIDs;

public class InvestmentCreateProcessor {

	private static final Logger logger = Logger.getLogger(InvestmentCreateProcessor.class);
	/*
	 * list of last investment ids
	 */
	static final int LAST_INVESTMENTS_LIST_SIZE = 3;
	/*
	 * The size of the list where the each investment's "risk appetite" is stored
	 */
	static final int RISK_APPETITE_LIST_SIZE = 30;

	public boolean process(final InvCreateEvent invCreate) {
		long startTime = System.nanoTime();

		if(InvestmentsManagerBase.investmentIsCancelled(invCreate.getInvId())) {
			logger.info("Not processing cancelled investment:"+invCreate.getInvId());
			return true;
		}
		
		long userId = invCreate.getUserId();
		Profile profile = ProfileManager.getProfile(userId);
		if(profile == null ) {
			logger.debug("Profile not found for userId: "+userId);
			return true;
		}

		if(!ProfileManager.lockProfile(userId)) {
			// Can't lock profile - return for re-delivery
			logger.debug("Can't lock profile : "+userId +" - return for re-delivery" );
			return false;
		}
		try{
			if(logger.isTraceEnabled()) {
				logger.trace("IC processing :");
				logger.trace(invCreate);
			}

			// update last investments list
			ArrayList<Long> lastInvestments = new ArrayList<Long>(profile.getLastInvestments());
			lastInvestments.add(0, invCreate.getInvId());
			if(lastInvestments.size()>LAST_INVESTMENTS_LIST_SIZE) {
				lastInvestments.remove(LAST_INVESTMENTS_LIST_SIZE);
			}

			unFrozenStatus(profile, invCreate.getInvId(), invCreate.getWriterId());

			// update coin balance for the user the investment was followed from
			if(invCreate.getCopyopType()==CopyOpInvTypeEnum.FOLLOW && !profile.isTest()){
				String a = ConfigurationCopyopManager.getCopyopPropertiesValue("COINS_PER_FOLLOWED_INV", "1");
				CoinsManager.updateCoins(invCreate.getDestUserId(), Long.parseLong(a), CoinsActionTypeEnum.FOLLOWED,  UsersManager.getSkinIdByUserId(userId), invCreate.getWriterId(), "Copyop service increase for invId: " + invCreate.getInvId());
			}

			// update risk appetite list
			ArrayList<Float> riskAppetite = new ArrayList<Float>(profile.getRiskAppetite());
			riskAppetite.add(0, invCreate.getRiskAppetite());
			if(riskAppetite.size()>RISK_APPETITE_LIST_SIZE) {
				riskAppetite.remove(RISK_APPETITE_LIST_SIZE);
			}
			// update trades history
			String today = new SimpleDateFormat(Constants.TRADES_HISTORY_DATE_FORMAT).format(Calendar.getInstance().getTime());
			Map<String, Integer> tradesHistory = profile.getTradesHistory();
			int count = 0;
			if(tradesHistory != null){
				if(tradesHistory.get(today)!= null) {
					count = tradesHistory.get(today);
				}
			}
			// save updated values
			ProfileManager.updateProfile(lastInvestments,riskAppetite, today, (count+1), userId);

			// prepare parameters for messages
			HashMap<String, String> sys_props = new HashMap<String, String>();
			sys_props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
			sys_props.put(UpdateMessage.PROPERTY_KEY_DIRECTION, String.valueOf(invCreate.getDir()));//{direction}{purchase level} = CALL\PUT (according to purchase)
			sys_props.put(UpdateMessage.PROPERTY_KEY_LEVEL, String.valueOf(invCreate.getLevel()));
			sys_props.put(UpdateMessage.PROPERTY_KEY_MARKET_ID, String.valueOf(invCreate.getMarketId()));
			sys_props.put(UpdateMessage.PROPERTY_KEY_INV_ID, String.valueOf(invCreate.getInvId()));
			sys_props.put(UpdateMessage.PROPERTY_KEY_TIME_LAST_INVEST, String.valueOf(invCreate.getTimeLastInvest()));
			sys_props.put(UpdateMessage.PROPERTY_KEY_OPPORTUNITY_ID, String.valueOf(invCreate.getOppId()));
			sys_props.put(UpdateMessage.PROPERTY_KEY_SCHEDULED, String.valueOf(invCreate.getScheduled()));

			UUID inheritedUUID = UUIDs.timeBased();
			logger.info("InvestmentCreate: " + inheritedUUID.toString());
			sys_props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, inheritedUUID.toString());

			BigDecimal resultInUSD = new BigDecimal(invCreate.getAmount()).multiply(new BigDecimal(invCreate.getRate()));
			HashMap<String, String> resultInAllCurrencies = CurrenciesCacheManager.getConvertedAmounts(resultInUSD.doubleValue());
			// override the users currency - to avoid inaccuracy after double conversion currency/USD => USD/currency
			resultInAllCurrencies.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR+invCreate.getCurrencyId(), ""+new BigDecimal(invCreate.getAmount()).setScale(2, RoundingMode.CEILING));
			sys_props.putAll(resultInAllCurrencies);

			// send self message
			if(profile.getUserState() != UserStateEnum.STATE_REMOVED) {
				UpdateMessage updateSelfInv = new UpdateMessage(userId, UpdateTypeEnum.AB_I_SELF_INVESTED, sys_props);
				CopyOpEventSender.sendUpdate(updateSelfInv, CopyOpEventSender.FEED_DESTINATION_NAME);
			}

			/*
			 * SEND MESSAGES
			 * GRANT COINS
			 * skip for blocked and removed users
			 */
			if(profile.getUserState() == UserStateEnum.STATE_REGULAR) {
				ArrayList<Long> fbFriendsUserIds  = FbManager.getFbFriendsUserIds(userId);
				if(!profile.isTest()) {
					// send notifications to Facebook friends
					for(Long fbFriendUserId: fbFriendsUserIds) {
						// send notification
						CopyOpEventSender.sendUpdate(new UpdateMessage(fbFriendUserId, UpdateTypeEnum.FB_NEW_INVESTMENT, sys_props), CopyOpEventSender.FEED_DESTINATION_NAME);
					}
				}

				long coins = 0;
				ArrayList<ProfileLink> profileLinks =  ProfileLinksManager.getProfileLinks(userId);
				Set<Long> usersToPush = new LinkedHashSet<Long>();

				TreeMap <Long, Long> coinsPerStepByCur = ConfigurationCopyopManager.getCopyopStepsAndCoinsPerCurrency(invCreate.getCurrencyId());

				for (ProfileLink profileLink : profileLinks) {
				    int coinsPerStepByCurSize = coinsPerStepByCur.size();
					if (profileLink.getType() == ProfileLinkTypeEnum.COPIED_BY) {
						if (processCopy(invCreate, profileLink, profile.isTest())) {				    
							if (!ProfileManager.isTestProfile(profileLink.getDestUserId())) {
							    for (Map.Entry<Long, Long> entry : coinsPerStepByCur.entrySet()) {
								coinsPerStepByCurSize--;
								//Multiply amount by 100 as DB long value is doubles multiplied by 100 
								if ( !((Double.compare((invCreate.getAmount() * 100), (entry.getKey().doubleValue()))) > 0) ) {
								    coins += entry.getValue().longValue();
								    break;
								} else if ((Double.compare((invCreate.getAmount() * 100), (entry.getKey().doubleValue())) == 0)) {
								    coins += entry.getValue().longValue();
								    break;
								} else if (coinsPerStepByCurSize == 0) {
								    coins += entry.getValue().longValue();
								    break;
								}
							    }
							}
						}
					}
					if (profileLink.getType() == ProfileLinkTypeEnum.WATCHED_BY
							&& profileLink.isWatchingPushNotification()
							&& !fbFriendsUserIds.contains(profileLink.getDestUserId())) {
						usersToPush.add(profileLink.getDestUserId());
					}
				}
				Map<Long, UserPushNotificationInfo> usersPushInfo = UsersManager.getUserPushNotificationInfo(usersToPush);
				for (long userToPush : usersToPush) {
					// send notification if not sent already (see previous FOR cycle)
					UpdateMessage update = new UpdateMessage(userToPush, UpdateTypeEnum.W_NEW_INVESTMENT, sys_props);
					UsersManager.addPushNotificationUserInfo(update, usersPushInfo, false);
					CopyOpEventSender.sendUpdate(update, CopyOpEventSender.FEED_DESTINATION_NAME);
				}
				// grant each trader with virtual coins for every copied investment that was copied from him, as per DB COPYOP.CFG_COPYOP_COINS
				// exclude for investments made by test users
				if(logger.isDebugEnabled() || logger.isTraceEnabled()) {
				    logger.trace( "Coins to be added for copied investment: " + coins + "for userID: " + userId);
				    logger.debug( "Coins to be added copied investment: " + coins + "for userID: " + userId);
				}
				if(coins> 0) {
					CoinsManager.updateCoins(userId, coins, CoinsActionTypeEnum.COPIED, UsersManager.getSkinIdByUserId(userId), invCreate.getWriterId(), "Copyop service increase for invId: " + invCreate.getInvId());
				}

				/*
				 *  send SYS_ANY_NEW_INV
				 *  skip for blocked and removed users
				 */
				if(!profile.isFrozen() && !profile.isTest()) {
					int anyInvestmentsTreshold = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("ANY_INV_MSG_THRESHOLD", "50"));
					if((invCreate.getAmount()*invCreate.getRate())>anyInvestmentsTreshold) {
						//insert to Feed before sending system message
						FeedMessage fm = new FeedMessage();
						fm.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
						fm.setQueueType(QueueTypeEnum.SYSTEM.getId());
						fm.setMsgType(UpdateTypeEnum.SYS_ANY_NEW_INV.getId());
						fm.setProperties(sys_props);
						fm.setTimeCreatedUUID(inheritedUUID);
						fm.setTimeCreated(new Date(UUIDs.unixTimestamp(inheritedUUID)));
						FeedManager.insertFeedMessage(fm);

						// send system messages to online users
						Set<Long> userIdsOnline = ProfilesOnlineManager.getUserIdsOnline();
						for(Long userIdOnline: userIdsOnline) {
							// send to everyone online except fb/copy/watch/me
							if(!fbFriendsUserIds.contains(userIdOnline) && !contains(profileLinks, userIdOnline) && !userIdOnline.equals(userId)) {
								UpdateMessage updateAnyInvest = new UpdateMessage(userIdOnline, UpdateTypeEnum.SYS_ANY_NEW_INV, sys_props);
								CopyOpEventSender.sendUpdate(updateAnyInvest, CopyOpEventSender.FEED_DESTINATION_NAME);
							}
						}
					}
				}
			}

			if(logger.isTraceEnabled()) {
				long elapsedTimeNano = System.nanoTime()-startTime;
				long seconds = NANOSECONDS.toSeconds(elapsedTimeNano);
				long millis = NANOSECONDS.toMillis(elapsedTimeNano);
				logger.trace("IC Time elapsed:"+millis+" millis ("+seconds+" seconds)");
			}

			return true;
		} catch (Exception e) {
		    logger.error("Can't perform process because got the following error[ " + e + " ]", e);
		    return false;
		} finally {
			ProfileManager.unlockProfile(userId);
		}
	}

	private void unFrozenStatus(Profile profile, long invId, long invWriterId) {
		// update frozen status
		Date timeUserCreated = profile.getTimeUserCreated();
		if(timeUserCreated != null) {
			int hoursSinceRegistration = (int) MILLISECONDS.toHours((new Date()).getTime() - timeUserCreated.getTime());
			int initialHoursInFreezer = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("FROZEN_FRESH_USER_HOURS", "48"));
			int unfreezeInvNum = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("INVESTMENT_NUM_TO_PULL_OUT_OF_FREEZER", "3"));
			if(hoursSinceRegistration >= initialHoursInFreezer) {
				//get last self inv. count
				
				int lastSelfInvestmentsCount = 0;
				if(profile.getLastInvestments() != null){
					lastSelfInvestmentsCount = profile.getLastInvestments().size();
				}
				//Check (self + copied)  count
				if((lastSelfInvestmentsCount + profile.getCopiedInvestments()) >= unfreezeInvNum) {
					try{
						if(profile.isFrozen()) {
							logger.debug("start unfreezin " + profile.getUserId());
							Frozen fr = new Frozen(profile.getUserId(), false, 0l, invId, invWriterId);
							FrozenCopyopManager.updateNonFrozen(fr);
							// do not update cassandra in case of oracle failure
							profile.setFrozen(false);
							ProfileManager.updateProfile(false, profile.getUserId());
							logger.debug(profile.getUserId() + " is unfrozen");
						}
					}catch(SQLException ex) {
						logger.debug("Can't update Frozen status of:" + profile.getUserId(), ex);
					}
				}
			}
		} else {
			logger.error("Can't check frozen status");
			logger.error("Got null for timeUserCreated:" + profile.getUserId());
		}
	}

	private boolean processCopy(InvCreateEvent event, ProfileLink link, boolean userIsTest){
		String secondsAllowed = ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_SECONDS_AFTER_CLOSE", ""+0);
		long secondsAllowedAfterCloseTime = Long.parseLong(secondsAllowed);
		long timeSinceCreated = (new Date().getTime() - event.getTimeClose()); 
		if(TimeUnit.MILLISECONDS.toSeconds(timeSinceCreated) > secondsAllowedAfterCloseTime) {
			logger.info("Copied Investment EXPIRED before :" + timeSinceCreated + " seconds");
			return false;
		}
		
		Set<Long> assets = link.getAssets();
		long userId = link.getUserId();
		long destUserId = link.getDestUserId();

		ProfileLink check = ProfileLinksManager.getProfileLink(link.getUserId(), link.getDestUserId(), link.getType());
		if(check == null) {
			// link already deleted - do not copy
			logger.debug("non existing ProfileLink :" + link);
			return false;
		}
		if(check.getAmount() == 0 ) {
			logger.debug("Invalid ProfileLink zero amount ! deleting :" + check);
			boolean destUserIsTest = ProfileManager.isTestProfile(destUserId);
			ProfileLinksManager.deleteCopyProfileLinkBatch(destUserId, link.getUserId(), destUserIsTest, userIsTest);
			return false;
		}
		
		Profile destProfile = ProfileManager.getProfile(destUserId);
		if(destProfile == null ) {
			logger.debug("Profile not found for destUserId: " + destUserId);
			return false;
		}
		long copiedInvestmentId;
		long copiedInvestmentWriterId;
		// Markets - Copy all markets or specific.
		if(assets.size() == 0 || assets.contains(event.getMarketId())) {
			UUID eventId = UUIDs.timeBased();
			logger.info("processCopy [" + eventId+"] " + link.toString());
			// insert into oracle
			try {
					if( UserRegulationManager.isRegulationSuspended(destUserId)) {
						logger.info( "User is suspended - cannot copy !"+destUserId);
						// EXIT POINT
						return false;
					}
					String a = ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_MIN_AMOUNT", ""+10);
					long minInvestmentAmount = Long.parseLong(a);

					double[] odds = ConfigurationCopyopManager.getCopyopInvOdds(link.getTimeCreated());
					// deprecated
					//Double oddsWin = Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_ODDS_WIN", "0.6"));
					//Double oddsLose = Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_ODDS_LOSE", "0.9"));

					InvestmentInfo copiedInvestment = CopiedInvestmentManager.insertCopiedInvestment(event, destUserId, link.getAmount(), minInvestmentAmount, link.getWriterId(), odds[0], odds[1]);
					if(copiedInvestment == null){
						// TODO this is should happen only if copying is failed
						return false;
					}
					
					if(copiedInvestment.getInvestmentId() == -1L) {
						logger.debug("Can't insert inv - Low balance for user:"+destUserId);
						// schedule a message for 3 hours later
						int hours = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("LOW_BALANCE_FIRST_CHECK_HOURS", "3"));
						ProfileManageEvent scheduled = new ProfileManageEvent(ProfileLinkCommandEnum.LOWBALANCE_COPYING, destUserId, 1);
						long delay = TimeUnit.HOURS.toMillis(hours);
						logger.debug("Going to schedule "+ scheduled + " for delay "+delay);
						CopyOpEventSender.sendEvent(scheduled, CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME, delay);
						// send instant message to user
						Map<String, String> props = new HashMap<String, String>();
						props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(destUserId));
						props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, UsersManager.getSkinIdByUserId(destUserId));
						// TODO - send only one message
						CopyOpEventSender.sendUpdate(new UpdateMessage(destUserId,
																		UpdateTypeEnum.AB_BALANCE_LESS_MIN_WARN,
																		props,
																		eventId),
														CopyOpEventSender.FEED_DESTINATION_NAME);
						// EXIT POINT
						return false;
					}

					BigDecimal convertedAmount = new BigDecimal(copiedInvestment.getAmount()).divide(new BigDecimal(copiedInvestment.getRate()), 2, RoundingMode.CEILING);
					logger.trace("invID:"+copiedInvestment.getInvestmentId() + " converted amount:" +convertedAmount.doubleValue());
					// send notify for investment to level service
					LevelServiceSender.sendNotifyForInvestment(
							CommonUtil.buildNotifyForInvestment(
										event.getOppId(),	// opportunity ID
										copiedInvestment.getInvestmentId(),				// investment ID
										convertedAmount.doubleValue(),	// converted amount in USD ( * 100 )
										event.getDir(),	// direction, choice, type
										"0",		// amountForDisplay
										" ", 		// cityNameInEnglish
										0L,			// countryId
										event.getMarketId(),
										OpportunityType.PRODUCT_TYPE_BINARY,
										-1L,		// userSkinId TODO must be real only if Copyop is introduced to eTrader users
										event.getLevel(),
										0L,			// time
										"0",		// globeLat
										"0",		// globeLong
										0,			// aboveTotal - 0-100
										0,			// belowTotal - 0-100
										0,			// flooredAmount
										CopyOpInvTypeEnum.COPY));
					
					copiedInvestmentId = copiedInvestment.getInvestmentId();
					copiedInvestmentWriterId = link.getWriterId(); 
			} catch (SQLException e) {
				logger.error("Exception while inserting copied investment", e);
				// EXIT POINT
				return false;
			}
			// update profile links - number of investments copied by this link
			ProfileLinksManager.updateReachedCound((link.getCountReached()+1), link.getUserId(), link.getDestUserId(), link.getType());
			ProfileLinksManager.updateReachedCound((link.getCountReached()+1), link.getDestUserId(), link.getUserId(), ProfileLinkTypeEnum.COPY);
			link.setCountReached(link.getCountReached()+1);
			// send to inbox
			HashMap<String, String> props = new HashMap<String, String>();
				props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(destUserId));
				props.put(UpdateMessage.PROPERTY_KEY_SRC_USER_ID, String.valueOf(userId));
				props.put(UpdateMessage.PROPERTY_KEY_AMOUNT, String.valueOf(link.getAmount()));
				props.put(UpdateMessage.PROPERTY_KEY_MARKET_ID, String.valueOf(event.getMarketId()));
				props.put(UpdateMessage.PROPERTY_KEY_LEVEL, String.valueOf(event.getLevel()));
				props.put(UpdateMessage.PROPERTY_KEY_DIRECTION, String.valueOf(event.getDir()));
				props.put(UpdateMessage.PROPERTY_KEY_INV_ID, String.valueOf(event.getInvId()));
			UpdateMessage updateICpoiedInv = new UpdateMessage(destUserId,
																UpdateTypeEnum.AB_I_COPIED_INVESTMENT,
																props,
																eventId);
			CopyOpEventSender.sendUpdate(updateICpoiedInv, CopyOpEventSender.FEED_DESTINATION_NAME);

			if( link.getCount() != 0 ) { //( 0=Max copy all)
				if(link.getCountReached() == link.getCount()) { // How many investments to copy
					// stop copy move to watch
					logger.debug("Max copied count reached:"+link);
					boolean destUserIsTest = ProfileManager.isTestProfile(destUserId);
					ProfileLinksManager.deleteCopyProfileLinkBatch(destUserId, userId, destUserIsTest, userIsTest);
					ProfileLinksManager.insertWatchProfileLink(destUserId, userId, destUserIsTest, userIsTest);
				}
			}
			
			ProfileCountersManager.increaseCopiedInvestmntsCounters(destProfile.getUserId());
			unFrozenStatus(destProfile, copiedInvestmentId, copiedInvestmentWriterId);
			
			// EXIT POINT
			return true;
		} else {
			logger.debug("Not copied asset:"+link);
			// EXIT POINT
			return false;
		}

	}

	private boolean contains(ArrayList<ProfileLink> links, long userId ) {
		for(ProfileLink link: links){
			if(link.getDestUserId() == userId) {
				return true;
			}
		}
		return false;
	}
}
