
package com.copyop.service.processor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileInvResultCopy;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.InvSettleEvent;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CurrenciesCacheManager;
import com.copyop.common.managers.FbManager;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.HitManager;
import com.copyop.common.managers.InvestmentManager;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.service.beans.UserPushNotificationInfo;
import com.copyop.service.manager.CopiedInvestmentManager;
import com.copyop.service.manager.UsersManager;
import com.datastax.driver.core.utils.UUIDs;

public class InvestmentSettleProcessor {

	private static final Logger logger = Logger.getLogger(InvestmentSettleProcessor.class);

	public boolean process(final InvSettleEvent event) {
		long userId = event.getUserId();
		Profile profile = ProfileManager.getProfile(userId);
		if(profile == null ) {
			logger.debug("Profile not found for userId: "+userId);
			return true;
		}

		if(!ProfileManager.lockProfile(userId)) {
			// Can't lock profile - return for re-delivery
			logger.debug("Can't lock profile : "+userId +" - return for re-delivery" );
			return false;
		}

		try{

			boolean winningInvestment = (event.getWin()>event.getLose());
			long profit = event.getWin()-event.getLose();
			/*
			 *  START sending settlement messages
			 */
			if(winningInvestment) {
				if(!profile.isTest() && profile.getUserState() == UserStateEnum.STATE_REGULAR) {
					// display properties for message
					Map<String, String> props = new HashMap<String, String>();
						props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
						props.put(UpdateMessage.PROPERTY_KEY_TIME_SETTLED, String.valueOf(event.getSettlementTime().getTime()));
						props.put(UpdateMessage.PROPERTY_KEY_MARKET_ID, String.valueOf(event.getMarketId()));

					UUID eventId = UUIDs.timeBased();
					logger.info("InvSettleEvent: " + eventId.toString());

					props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, eventId.toString());

					BigDecimal resultInUSD = new BigDecimal(profit).divide(new BigDecimal(100L)).multiply(new BigDecimal(event.getRateToUSD()));
					HashMap<String, String> resultInAllCurrencies = CurrenciesCacheManager.getConvertedAmounts(resultInUSD.doubleValue());
					resultInAllCurrencies.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR + Currency.CURRENCY_USD_ID, "" + resultInUSD);
					// override the users currency - to avoid inaccuracy after double conversion currency/USD => USD/currency
					resultInAllCurrencies.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR+event.getCurrencyId(), ""+new BigDecimal(event.getResult()).divide(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
					props.putAll(resultInAllCurrencies);

					// My New Expiry in-money - replaced by shopping bag
					//UpdateMessage updateSelfInv = new UpdateMessage(userId, UpdateTypeEnum.AB_MY_EXPIRY, props);
					//CopyOpEventSender.sendUpdate(updateSelfInv, CopyOpEventSender.FEED_DESTINATION_NAME);
					ArrayList<Long> fbFriendsUserIds  = FbManager.getFbFriendsUserIds(userId);

					// send notifications to my facebook friend I had in-money expiry
					for(Long fbFriendUserId: fbFriendsUserIds) {
						CopyOpEventSender.sendUpdate(new UpdateMessage(fbFriendUserId, UpdateTypeEnum.FB_IN_MONEY_EXPIRY, props), CopyOpEventSender.FEED_DESTINATION_NAME);
					}

					// Trader I watch made +$100 profit
					Set<Long> watchers = null;
					Map<Long, Boolean> watchersInfo = ProfileLinksManager.getWatchersIds(userId);
					long watchMadeProfitThreshold = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("TRADER_I_WATCH_MADE_PROFIT", "10000"));
					long resultInUSDCents = resultInUSD.multiply(new BigDecimal(100L)).longValue();
					if(resultInUSDCents > watchMadeProfitThreshold) {
						watchers = watchersInfo.keySet();
						long watchMadeProfitPushThreshold = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("TRADER_I_WATCH_MADE_PROFIT_PUSH", "40000"));
						boolean sendPush = resultInUSDCents >= watchMadeProfitPushThreshold;
						logger.debug("Send push for ["+ UpdateTypeEnum.W_100_EXPIRY
								+ "] is [" + sendPush + "]. Inv amount [" + resultInUSDCents
								+ "], push trheshold [" + watchMadeProfitPushThreshold + "]");
						Map<Long, UserPushNotificationInfo> userInfo = null;
						if (sendPush) {
							userInfo = UsersManager.getUserPushNotificationInfo(watchers);
						}
						UpdateMessage updateMsg = null;
						for(Map.Entry<Long, Boolean> watcherInfo : watchersInfo.entrySet()) {
							updateMsg = new UpdateMessage(watcherInfo.getKey(), UpdateTypeEnum.W_100_EXPIRY, props);
							if (sendPush && watcherInfo.getValue()) { // user has subscribed for mobile notifications
								UsersManager.addPushNotificationUserInfo(updateMsg, userInfo, true);
							} else {
								updateMsg.getWherePosted().remove(QueueTypeEnum.MOBILE_NOTIFICATION);
							}
							// send notification
							CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
						}
					}

					// Any trader's expiry in-money (except frozen and me)
					if(!profile.isFrozen()) {
						double anyInvestmentsTreshold = Double.parseDouble(ConfigurationCopyopManager.getCopyopPropertiesValue("ANY_INV_MSG_THRESHOLD_STL", "50"));
						if(resultInUSD.doubleValue() > anyInvestmentsTreshold) {
							//insert to Feed before sending system message
							FeedMessage fm = new FeedMessage();
							fm.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
							fm.setQueueType(QueueTypeEnum.SYSTEM.getId());
							fm.setMsgType(UpdateTypeEnum.SYS_ANY_IN_MONEY_EXPIRY.getId());
							fm.setProperties(props);
							fm.setTimeCreatedUUID(eventId);
							fm.setTimeCreated(new Date(UUIDs.unixTimestamp(eventId)));
							FeedManager.insertFeedMessage(fm);

							Set<Long> userIdsOnline = ProfilesOnlineManager.getUserIdsOnline();
							for(Long userIdOnline: userIdsOnline) {
								if(!userIdOnline.equals(userId)){ // not me
									if(!fbFriendsUserIds.contains(userIdOnline) ) { // not a facebook friend
										if( watchers == null || (watchers != null && !watchers.contains(userIdOnline))) { // not a wacher ( if profit > 100 )
											UpdateMessage updateAnyInMoney = new UpdateMessage(userIdOnline, UpdateTypeEnum.SYS_ANY_IN_MONEY_EXPIRY, props);
											CopyOpEventSender.sendUpdate(updateAnyInMoney, CopyOpEventSender.FEED_DESTINATION_NAME);
										}
									}
								}
							}
						}
					}
				} else {
					logger.info("No settlement msgs for test user:"+userId);
				}
			}
			/*
			 *  END sending settlement messages
			 */

			/*
			 * START calculate statistics
			 */
			float selfHitRate = 0;
			float copyHitRate = 0;
			if(event.getType() == CopyOpInvTypeEnum.COPY) {
				// add copied investment result to profile statistics
				ProfileStatisticsManager.insertProfileInvResultCopy(winningInvestment, userId);

				// update copied investments profit
				try	{
						long copiedFromUserId = CopiedInvestmentManager.userIdForInvestment(event.getSrcInvestmentId());
						if(copiedFromUserId>0) {
							updateCopyInvestmetnStats(userId, event.getMarketId(), copiedFromUserId, profit);
						} else {
							logger.error("Can't update investment profit: Can't load userId for invId" + event.getSrcInvestmentId());
						}

						int copiedInvSuccessRateHistorySize = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INV_SUCCESS_RATE_HISTORY_SIZE", "200"));
						ProfileInvResultCopy invResultCopy = ProfileStatisticsManager.getProfileInvResultCopy(userId, copiedInvSuccessRateHistorySize);
						// calculate hit rate for profile
						int frequency = Collections.frequency(invResultCopy.getResults(), Boolean.TRUE);
						copyHitRate = ((float)frequency/invResultCopy.getResults().size());
				} catch (SQLException ex) {
					logger.error("Can't update copied investment stats", ex);
					logger.error(event);
				}
			} else { // SELF / FOLLOW
				// add SELF/FOLLOW investment result to profile statistics
				ProfileStatisticsManager.insertProfileInvResultSelf(winningInvestment, event.getMarketId(), userId);
				selfHitRate = updateSelfInvestmentStats(profile, event.getMarketId(), profit);
			}
			/*
			 * END calculate statistics
			 */

			try {
				int selfHitRatePercentage = Math.round(selfHitRate*100);
				int copyHitRatePercentage = Math.round(copyHitRate*100);
				logger.debug("Inserting hit rates :"+ selfHitRatePercentage +" self " + copyHitRatePercentage + " copy"+" for" + userId);
				HitManager.insertHitRate(userId, selfHitRatePercentage, copyHitRatePercentage);
			} catch (SQLException e) {
				logger.error("Can't update hit rates to oracle:", e);
				logger.error(event);
			}
			return true;
		} finally {
			ProfileManager.unlockProfile(userId);
		}

	}

	public static void updateCopyInvestmetnStats(long userId, long marketId, long copiedFromUserId, long result) {
//		boolean synced;
// LWT - replaced with profile lock
//		int retry = 0;
//		do {
//			synced = true;
//			if(retry > 0 ){
//				logger.debug("Retrying LWT:"+retry);
//			}
			// calculate copied investments statistics
			CopiedInvestment cp = InvestmentManager.getCopiedInvestment(userId, marketId, copiedFromUserId);
			if( cp == null){
				cp = new CopiedInvestment(userId, copiedFromUserId, marketId);
			}

			cp.setProfit(cp.getProfit()+result);
			cp.setCopiedCount(cp.getCopiedCount()+1);
			if(result >0) {
				cp.setCopiedCountSuccess(cp.getCopiedCountSuccess()+1);
			}
			float successRate =((float)cp.getCopiedCountSuccess()/cp.getCopiedCount());
			cp.setSuccessRate(successRate);
			// update copied investment statistics
			InvestmentManager.updateCopiedInvestment(cp, cp.getCopiedCount());
// LWT - replaced with profile lock
//			synced = synced && InvestmentManager.updateCopiedInvestment(cp, copiedCountLock);
//			retry++;
//		} while(!synced);
	}

	public static float updateSelfInvestmentStats(Profile profile, long marketId, long result) {
// LWT - replaced with profile lock
//		boolean synced;
//		int retry = 0;
		float newHitRate = 0;
//		do {
//			synced = true;
//			if(retry > 0 ){
//				logger.debug("Retrying LWT:"+retry);
//			}

			// get all assets results
			Map<Long, Long> assetResults = new HashMap<Long, Long>(profile.getAssetsResults());
			// get the result for this market id
			Long assetResult = assetResults.get(marketId);
			if(assetResult == null) {
				assetResult = 0l;
			}
			// calculate new asset result
			Long newAssetResult = assetResult + result;
			assetResults.put(marketId, newAssetResult);

			SortedSet<Map.Entry<Long, Long>> sortedAssetsResults = entriesSortedByValues(assetResults);
			// best asset
			Long newBestAsset = sortedAssetsResults.last().getKey();

			// calculate streak
			int newCurrentStreak = profile.getCurrentStreak();
			long newCurrentStreakAmount = profile.getCurrentStreakAmount();
			int newBestStreak = profile.getBestStreak();
			long newBestStreakAmount = profile.getBestStreakAmount();
			if(result>0) {
				newCurrentStreak = profile.getCurrentStreak() + 1;
				newCurrentStreakAmount = profile.getCurrentStreakAmount() + result;
				if(newCurrentStreak > profile.getBestStreak()) {
					newBestStreak = newCurrentStreak;
					newBestStreakAmount = newCurrentStreakAmount;
				}
			} else {
				newCurrentStreak = 0;
				newCurrentStreakAmount = 0;
			}
			newHitRate = profile.getHitRate();
			int invSuccessRateHistorySize = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("INV_SUCCESS_RATE_HISTORY_SIZE", "120"));
			ProfileInvResultSelf invResultSelf = ProfileStatisticsManager.getProfileInvResultSelf(	profile.getUserId(),
																									invSuccessRateHistorySize,
																									UserStateEnum.STATE_REGULAR);

			// calculate hit rate for profile
			int frequency = Collections.frequency(invResultSelf.getResults(), Boolean.TRUE);
			newHitRate = ((float)frequency/invResultSelf.getResults().size());
			ProfileManager.updateProfile(profile.getUserId(), newCurrentStreak, newCurrentStreakAmount,
					newBestStreak, newBestStreakAmount, newAssetResult, marketId, newBestAsset, newHitRate, profile.getLastTimeChanged());
// LWT replaced with profile lock
//			synced = synced && ProfileManager.updateProfile
//			retry++;
//		} while(!synced);
		return newHitRate;
	}

	   static <K,V extends Comparable<? super V>>
		   SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
		       SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
		           new Comparator<Map.Entry<K,V>>() {
		               @Override
		               public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
		                   return e1.getValue().compareTo(e2.getValue());
		               }
		           }
		       );
		   sortedEntries.addAll(map.entrySet());
	       return sortedEntries;
	   }

}
