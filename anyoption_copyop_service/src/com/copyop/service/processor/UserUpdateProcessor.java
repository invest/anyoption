/**
 *
 */
package com.copyop.service.processor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.copyop.common.Constants;
import com.copyop.common.enums.base.PriorityEnum;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateFamilyEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.service.manager.FeedManager;
import com.copyop.service.manager.InboxManager;
import com.copyop.service.manager.MarketsManager;
import com.copyop.service.manager.UpdateTextsManager;
import com.copyop.service.push.PushNotificationServiceIfc;


/**
 * TODO Java DOC
 *
 * @author pavelhe
 *
 */
public class UserUpdateProcessor {

	public static final String INVESTMENT_TYPE_CALL = "1";
    public static final String INVESTMENT_TYPE_PUT = "2";

	private final PushNotificationServiceIfc pushNotificationService;

	public UserUpdateProcessor(PushNotificationServiceIfc pushNotificationService) {
		this.pushNotificationService = pushNotificationService;
	}

	private static final Logger log = Logger.getLogger(UserUpdateProcessor.class);

	public void process(UpdateMessage uMsg) {
		log.debug("Handling: " + uMsg);

		Set<QueueTypeEnum> wherePostedSet = uMsg.getWherePosted();
		Iterator<QueueTypeEnum> iter = wherePostedSet.iterator();
		EnumSet<QueueTypeEnum> lsPosts = EnumSet.noneOf(QueueTypeEnum.class);
		Map<String, String> additionalProps = loadMsgAdditionalProps(uMsg);
		QueueTypeEnum wherePosted = null;
		while (iter.hasNext()) {
			wherePosted = iter.next();

			log.debug("Posting message in: " + wherePosted);
			switch (wherePosted) {
			case INBOX:
				InboxManager.insertInboxMsg(uMsg);
				iter.remove();
				lsPosts.add(wherePosted);
				break;
			case EXPLORE:
			case NEWS:
				if (tryToPostMsg(uMsg, wherePosted)) {
					iter.remove();
					lsPosts.add(wherePosted);
				}
				break;
			case IN_APP_STRIP:
			case WEB_POPUP:
				iter.remove();
				lsPosts.add(wherePosted);
				break;
			case MOBILE_NOTIFICATION:
				try {
					sendPushNotification(uMsg, additionalProps);
				} catch(Exception e) {
					log.error("Exception while sending push notification", e);
				}
				iter.remove();
				break;

			default:
				log.warn("Unknown queue type [" + wherePosted + "] skipping it");
				break;
			}
		}

		sendToLS(uMsg, lsPosts, additionalProps);

		if (wherePostedSet.size() > 0) {
			long ttl = uMsg.getUpdateType().getTtlMillis();
			long ttlLeft = Constants.NO_TTL;
			if (ttl != Constants.NO_TTL) {
				ttlLeft = uMsg.getUpdateType().getTtlMillis()
								- (System.currentTimeMillis()
									- uMsg.getTimeCreated().getTime());
			}
			long messageWaitPeriod = uMsg.getUpdateType().getPriority().getMessageWaitPeriod();
			if (ttl == Constants.NO_TTL || ttlLeft > messageWaitPeriod) {
				log.debug("Message [" + uMsg.getTimeCreatedUUID()
							+ "] for user [" + uMsg.getUserId()
							+ "] not sent to " + wherePostedSet
							+ ". Returning back to queue for [" + messageWaitPeriod + "] milliseconds");
				CopyOpEventSender.sendUpdate(uMsg,
												CopyOpEventSender.FEED_DESTINATION_NAME,
												messageWaitPeriod,
												ttlLeft);
			} else {
				log.debug("Message [" + uMsg.getTimeCreatedUUID()
							+ "] for user [" + uMsg.getUserId()
							+ "] not sent to " + wherePostedSet + " has expired and will be discarded");
			}
		}

		log.debug("Update handled");
	}

	private Map<String, String> loadMsgAdditionalProps(UpdateMessage uMsg) {
		Map<String, String> props = new HashMap<String, String>();
		String userId = uMsg.getProperties().get(UpdateMessage.PROPERTY_KEY_USER_ID);
		if (userId != null) {
			long longUserId = Long.parseLong(userId);
			ProfileManager.loadProfileDetailsForMessage(longUserId, props);
			ProfileCountersManager.loadAllProfileRelations(longUserId, props);
		}
		String destUserId = uMsg.getProperties().get(UpdateMessage.PROPERTY_KEY_DEST_USER_ID);
		if (destUserId != null) {
			long lDestUserId = Long.parseLong(destUserId);
			ProfileManager.loadDestProfileDetailsForMessage(lDestUserId, props);
		}
		String srcUserId = uMsg.getProperties().get(UpdateMessage.PROPERTY_KEY_SRC_USER_ID);
		if (srcUserId != null) {
			long lSrcUserId = Long.parseLong(srcUserId);
			ProfileManager.loadSrcProfileDetailsForMessage(lSrcUserId, props);
		}
		return props;
	}

	private void sendPushNotification(UpdateMessage uMsg, Map<String, String> additionalProps) {
		Map<String, String> payload = null;
		Map<String, String> msgProps = uMsg.getProperties();
		Object [] params = null;
		int skinId = Integer.valueOf(msgProps.get(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID));
		switch (uMsg.getUpdateType()) {
		case FB_NEW_FRIEND:
			params = new Object[] {msgProps.get(UpdateMessage.PROPERTY_KEY_FIRST_NAME),
									msgProps.get(UpdateMessage.PROPERTY_KEY_LAST_NAME)};
			break;
		case W_NEW_INVESTMENT: {
			String currencyId = msgProps.get(UpdateMessage.PROPERTY_KEY_PUSH_CURRENCY_ID);
			double amount = Double.valueOf(msgProps.get(UpdateMessage.PROPERTY_KEY_AMOUNT_IN_CURR + currencyId));
			Currency currency = CurrenciesManagerBase.getCurrency(Long.parseLong(currencyId));
			int marketId = Integer.parseInt(msgProps.get(UpdateMessage.PROPERTY_KEY_MARKET_ID));
			Market market = MarketsManagerBase.getMarket(marketId);
			String level = CommonUtil.formatLevel(Double.parseDouble(msgProps.get(UpdateMessage.PROPERTY_KEY_LEVEL)),
													market.getDecimalPoint());
			params = new Object[] {additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME),
									(INVESTMENT_TYPE_CALL.equals(msgProps.get(UpdateMessage.PROPERTY_KEY_DIRECTION)) ? "Call" : "Put"),
									MarketsManager.getMarketShortName(marketId, skinId),
									CommonUtil.formatCurrencyAmountAO(amount, true, currency),
									level};
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, "news_feed");
			payload.put(UpdateMessage.PROPERTY_KEY_UPDATE_TYPE, String.valueOf(uMsg.getUpdateType().getId()));
			payload.put(UpdateMessage.PROPERTY_KEY_MARKET_ID, msgProps.get(UpdateMessage.PROPERTY_KEY_MARKET_ID));
			payload.put(UpdateMessage.PROPERTY_KEY_INV_ID, msgProps.get(UpdateMessage.PROPERTY_KEY_INV_ID));
			payload.put(UpdateMessage.PROPERTY_KEY_DIRECTION, msgProps.get(UpdateMessage.PROPERTY_KEY_DIRECTION));
			payload.put(UpdateMessage.PROPERTY_KEY_AMOUNT, msgProps.get(UpdateMessage.PROPERTY_KEY_AMOUNT_IN_CURR + currencyId));
			break;
		}
		case W_NEARLY_FULL:
			params = new Object[] {msgProps.get(UpdateMessage.PROPERTY_KEY_SEATS_LEFT),
									additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME),
									};
			
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, UpdateMessage.PUSH_SCREEN_USER_PROFILE);
			payload.put(UpdateMessage.PUSH_USER_ID_DPL, additionalProps.get(UpdateMessage.PROPERTY_KEY_USER_ID));
			
			break;
		case W_100_EXPIRY: {
			Date timeSettled = new Date(Long.valueOf(msgProps.get(UpdateMessage.PROPERTY_KEY_TIME_SETTLED)));
			SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm");
			dateFormatter.setTimeZone(TimeZone.getTimeZone(msgProps.get(UpdateMessage.PROPERTY_KEY_PUSH_UTC_OFFSET)));
			String currencyId = msgProps.get(UpdateMessage.PROPERTY_KEY_PUSH_CURRENCY_ID);
			double amount = Double.valueOf(msgProps.get(UpdateMessage.PROPERTY_KEY_AMOUNT_IN_CURR + currencyId));
			Currency currency = CurrenciesManagerBase.getCurrency(Long.parseLong(currencyId));
			params = new Object[] {additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME),
									CommonUtil.formatCurrencyAmount(amount, true, currency),
									MarketsManager.getMarketShortName(
													Integer.parseInt(msgProps.get(UpdateMessage.PROPERTY_KEY_MARKET_ID)),
													skinId),
									dateFormatter.format(timeSettled)};
			
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, UpdateMessage.PUSH_SCREEN_USER_PROFILE);
			payload.put(UpdateMessage.PUSH_SCREEN_USER_PROFILE, additionalProps.get(UpdateMessage.PROPERTY_KEY_USER_ID));
			
			break;
		}
		case W_NOW_AVAILABLE:
			params = new Object[] {additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME)};
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, UpdateMessage.PUSH_SCREEN_USER_PROFILE);
			payload.put(UpdateMessage.PUSH_SCREEN_USER_PROFILE, additionalProps.get(UpdateMessage.PROPERTY_KEY_USER_ID));
			break;
		case CP_FROZE:
			params = new Object[] {additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME)};
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, UpdateMessage.PUSH_SCREEN_USER_PROFILE);
			payload.put(UpdateMessage.PUSH_SCREEN_USER_PROFILE, additionalProps.get(UpdateMessage.PROPERTY_KEY_USER_ID));
			break;
		case AB_BALANCE_LESS_MIN_WARN:
			String destNickname = additionalProps.get(UpdateMessage.PROPERTY_KEY_DEST_NICKNAME);
			if (destNickname != null) {
				params = new Object[] {destNickname};
				skinId += 100; // due to two different texts for one message
			}/* else {
				 Message without parameters - nothing to do.
			}*/
			break;
		case AB_BALANCE_LESS_MIN_COPIED:
			params = new Object[] {msgProps.get(UpdateMessage.PROPERTY_KEY_COPIERS)};
			break;
		case AB_BALANCE_LESS_MIN_COPYING:
			params = new Object[] {additionalProps.get(UpdateMessage.PROPERTY_KEY_NICKNAME)};
			break;
		case AB_IM_FULLY_COPIED:
			/* Nothing to do. */
			break;
		case AB_IM_TOP_PROFITABLE:
			params = new Object[] {msgProps.get(UpdateMessage.PROPERTY_KEY_RANKED_IN_TOP)};
			break;
		case AB_REACHED_COINS:
			params = new Object[] {msgProps.get(UpdateMessage.PROPERTY_KEY_COINS_BALANCE)};
			
			payload = new HashMap<String, String>();
			payload.put(UpdateMessage.PUSH_SCREEN_DPL, "copyop_coins");
			break;

		default:
			log.warn("Message not expected for push notifications");
			break;
		}

		String msgText = UpdateTextsManager.getUpdateText(uMsg.getUpdateType(),
															QueueTypeEnum.MOBILE_NOTIFICATION,
															skinId,
															params);

		if (msgText != null) {
			pushNotificationService.sendPushNotification(uMsg, msgText, payload);
		} else {
			log.error("Text not found for user message. Push notification will not be sent");
		}
	}

	/**
	 * TODO JAVA DOC
	 *
	 * @param uMsg
	 * @param lsPosts
	 * @param additionalProps
	 */
	private void sendToLS(UpdateMessage uMsg, EnumSet<QueueTypeEnum> lsPosts, Map<String, String> additionalProps) {
		if (lsPosts == null || lsPosts.size() == 0) {
			log.debug("No need to send message to LS");
			/* !!! WARNING !!! Method exit point! */
			return;
		}

		if (!ProfilesOnlineManager.isUserOnline(uMsg.getUserId())) {
			log.debug("User is [offline]. Message will NOT be sent to LS for: " + lsPosts);
			/* !!! WARNING !!! Method exit point! */
			return;
		}
		log.debug("Sending message to LS for " + lsPosts);

		UpdateMessage msgClone = new UpdateMessage(uMsg, lsPosts);
		msgClone.getProperties().putAll(additionalProps);
		CopyOpEventSender.sendUpdateToTopic(msgClone, CopyOpEventSender.LS_TOPIC_DESTINATION_NAME);
		log.debug("Message sent to LS");
	}

	/**
	 * TODO JAVA DOC
	 *
	 * @param uMsg
	 * @param queueType
	 * @param online
	 * @return
	 */
	public boolean tryToPostMsg(UpdateMessage uMsg, QueueTypeEnum queueType) {
		boolean posted = false;

		PriorityEnum category = uMsg.getUpdateType().getPriority();
		if (category == PriorityEnum.ALL_OTHERS) {
			log.error("Unexpected priority category [" + category + "]. Skipping message: " + uMsg);
			/* !!! WARNING !!! Method exit point! */
			return true;
		}

		List<Date> msgDates = FeedManager.getPostAlgorithmFeed(uMsg.getUserId(),
																queueType,
																category.getMessageCountLimit()); // Exclusive interval
		// Query result logging moved to DAO

		if (msgDates.size() - 1 < category.getMessageCountLimit()) { // Exclusive interval, keep in mind first element is "now"
			log.debug("Message count is less than maximum. Message will be posted");
			posted = true;
		} else {
			long now = msgDates.get(0).getTime();
			for (int idx = 1 /* skip first */; idx < msgDates.size(); idx++) {
				if (now - msgDates.get(idx).getTime() > category.getMessageTimeLimit()) {
					log.debug("Message [" + idx + "] is older than ["
								+ category.getMessageTimeLimit() + "] milliseconds. Message will be posted");
					posted = true;
					break;
				}
			}
		}

		if (posted) {
			if (uMsg.getUpdateType().getUpdateFamily() != UpdateFamilyEnum.SYSTEM) {
				FeedManager.insertBatchPostAlgorithmMessage(uMsg, queueType);
			} else {
				FeedManager.insertPostAlgorithmMessage(uMsg, queueType);
			}
			log.debug("Message posted");
		}

		return posted;
	}

}
