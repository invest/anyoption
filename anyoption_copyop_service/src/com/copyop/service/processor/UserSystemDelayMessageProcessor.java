package com.copyop.service.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.jboss.logging.Logger;

import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.ProfilesOnlineManager;

public class UserSystemDelayMessageProcessor {
	private static final Logger logger = Logger.getLogger(UserSystemDelayMessageProcessor.class);

	public void process(UpdateMessage event) {

		ArrayList<FeedMessage> feedList = new ArrayList<FeedMessage>();
		feedList.add(event);
		logger.debug("Insert to DB msg [" + event + "].");
		FeedManager.insertFeedMessage(feedList);

		logger.debug("Get Online Users");
		Set<Long> userIdsOnline = ProfilesOnlineManager.getUserIdsOnline();

		logger.debug("Begin to send to USERS " + CopyOpEventSender.FEED_DESTINATION_NAME + " with UpdateTypeEnum:" + event.getUpdateType().getId() + "....");
		Map<String, String> props = new HashMap<String, String>(event.getProperties());
		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, event.getTimeCreatedUUID().toString());
		for(Long userIdOnline: userIdsOnline) {
			UpdateMessage upadateUserMsg = new UpdateMessage(userIdOnline, event.getUpdateType(), props);
			CopyOpEventSender.sendUpdate(upadateUserMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
		}
		logger.debug("Finished send to USERS " + CopyOpEventSender.FEED_DESTINATION_NAME + " with UpdateTypeEnum:" + event.getUpdateType().getId() + "!");
	}
}
