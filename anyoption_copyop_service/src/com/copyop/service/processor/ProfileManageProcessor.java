package com.copyop.service.processor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.managers.UsersManagerBase;
import com.copyop.common.dto.CoinsBalanceHistory;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.CoinsManager;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CurrenciesCacheManager;
import com.copyop.common.managers.FbManager;
import com.copyop.common.managers.FeedManager;
import com.copyop.common.managers.ProfileCountersManager;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfilesOnlineManager;
import com.copyop.service.manager.CopiedInvestmentManager;
import com.copyop.service.manager.UsersManager;
import com.datastax.driver.core.utils.UUIDs;

public class ProfileManageProcessor {

	private static final Logger logger = Logger.getLogger(ProfileManageProcessor.class);

	public void process(ProfileManageEvent event) {
		if(event == null ) {
			logger.error("Got null object !");
			return;
		}
		if(event.getCommand()==ProfileLinkCommandEnum.PING) {
			logger.debug("Received Ping.");
			return;
		}

		if(event.getCommand()==ProfileLinkCommandEnum.COINSRESET){
				logger.debug("Coins reset :"+event);
				coinsReset(event.getUserId(), event.getTimeReset());
				return;
		}

		UserStateEnum userState = ProfileManager.getUserState(event.getUserId());
		if(userState != null && userState==UserStateEnum.STATE_REGULAR) {
			switch(event.getCommand()) {
				case COPY:
					logger.debug(event.getUserId()+"< copy >"+event.getDestUserId());
					copy(event.getUserId(), event.getDestUserId());
					break;
				case WATCH:
					logger.debug(event.getUserId()+"< watch >"+event.getDestUserId());
					watch(event.getUserId(), event.getDestUserId());
					break;
				case UNCOPY:
					logger.debug(event.getUserId()+"< UNcopy >"+event.getDestUserId());
					uncopy(event.getUserId(), event.getDestUserId());
					break;
				case UNWATCH:
					logger.debug(event.getUserId()+"< UNwatch >"+event.getDestUserId());
					unwatch(event.getUserId(), event.getDestUserId());
					break;
				case FREEZE:
					//When your heart's not open
					logger.debug("-> frozen <-");
					frozen();
					break;
				case SHOWOFF:
					logger.debug("Show off :"+event);
					showOff(event.getUserId(), event.getAmount(), event.getMarketId(), event.getCurrencyId(), event.getRate(), event.getTimeSettled());
					break;
				case LOWBALANCE_COPYING:
					long userId = event.getUserId();
					int count = event.getCount();
					logger.debug("Low balance copying :"+userId + " count :"+count);
					if(!ProfileManager.lockProfile(userId)) {
						logger.debug("Can't lock profile : "+userId);
						return;
					}
					try {
						lowBalanceCopying(userId, count);
					} finally {
						ProfileManager.unlockProfile(userId);
					}
					break;
				case LOWBALANCE_COPIERS:
					logger.debug("Low balance copyers :"+event);
					lowBalanceCopiers(event.getUserId(), event.getCount());
					break;
				case FB_NEW_FRIEND:
					logger.debug("FB new friend :"+event);
					fbNewFriend(event.getUserId());
					break;
				case NEW_NICKNAME:
					logger.debug(event.getUserId()+" new nickname "+event.getNewNickName()+" old nickname "+event.getOldNickName());
					newNickName(event.getUserId(), event.getNewNickName(), event.getOldNickName());
					break;
				default:
					logger.debug("Unsupported command:"+event.getCommand());
					break;
			}
		}
	}

	private void newNickName(long userId, String newNickName, String oldNickName) {
		// send messages
		if(newNickName == null || oldNickName == null) {
			logger.debug("Can't change nickname "+oldNickName+" to " +newNickName + " of " + userId);
			return;
		}

		if(ProfileManager.isFrozenProfile(userId)||ProfileManager.isTestProfile(userId)){
			logger.debug("Will not send change nickname message"+userId+" is frozen or test");
			return;
		}
	
		String strEventId = UUIDs.timeBased().toString();
		logger.info("newNickName: " + strEventId);

		List<Long> copiers = ProfileLinksManager.getCopiersIds(userId);
		List<Long> watchers = ProfileLinksManager.getListWatchersIds(userId);

		HashMap<String, String> props = new HashMap<String, String>();
		props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
		props.put(UpdateMessage.PROPERTY_KEY_NICKNAME, newNickName);
		props.put(UpdateMessage.PROPERTY_KEY_OLD_NICKNAME, oldNickName);
		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);
		for(Long copier : copiers) {
			CopyOpEventSender.sendUpdate(new UpdateMessage(copier, UpdateTypeEnum.SYS_NICKNAME_CHANGED, props),
														CopyOpEventSender.FEED_DESTINATION_NAME);
		}
		for(Long watcher : watchers){
			if(!copiers.contains(watcher)){
				CopyOpEventSender.sendUpdate(new UpdateMessage(watcher, UpdateTypeEnum.SYS_NICKNAME_CHANGED, props),
						CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}
	}

	private void fbNewFriend(long userId) {
		String strEventId = UUIDs.timeBased().toString();
		logger.info("fbNewFriend: " + strEventId);
		HashMap<Long, User> hm;
		try {
			ArrayList<Long> userIdInList = new ArrayList<Long>();
			userIdInList.add(userId);
			hm = UsersManagerBase.getUserFirstLastName(userIdInList);
		} catch (SQLException e) {
			logger.debug("Can't get User's first name/last name: "+ userId , e);
			return;
		}
		Profile profile = ProfileManager.getProfile(userId);
		if(profile == null ) {
			logger.debug("FB new friend : Can't load user profile");
			return;
		}
		HashMap<String, String> props = new HashMap<String, String>();
		props.put(UpdateMessage.PROPERTY_KEY_FIRST_NAME, hm.get(userId).getFirstName());
		props.put(UpdateMessage.PROPERTY_KEY_LAST_NAME, hm.get(userId).getLastName());
		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);
		props.put(UpdateMessage.PROPERTY_KEY_AVATAR, profile.getAvatar());
		props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
		props.put(UpdateMessage.PROPERTY_KEY_COPIERS, String.valueOf(profile.getCopiers()));
		props.put(UpdateMessage.PROPERTY_KEY_WATCHERS, String.valueOf(profile.getWatchers()));
		
		ArrayList<Long> fbFriends = FbManager.getFbFriendsUserIds(userId);
		Map<Long, String> skinsByUser = UsersManager.getPushNotificationSkinsByUser(fbFriends);
		for(Long fbFriend : fbFriends) {
			UpdateMessage updateMsg = new UpdateMessage(fbFriend, UpdateTypeEnum.FB_NEW_FRIEND, props);
			UsersManager.addPushNotificationSkinToMessage(updateMsg, skinsByUser);
			CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
		}
	}

	private void lowBalanceCopying(long userId, int count) {
		try {
			long balance = CopiedInvestmentManager.getUserBalance(userId);
			long minAmount = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("COPIED_INVESTMENT_MIN_AMOUNT", ""+10));
			if (balance < minAmount) {
				UUID eventId = UUIDs.timeBased();
				logger.info("lowBalanceCopying: " + eventId);
				// send warning message to user
				if(count == 1) {
					Map<String, String> props = new HashMap<String, String>();
					props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
					props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, UsersManager.getSkinIdByUserId(userId));

					CopyOpEventSender.sendUpdate(new UpdateMessage(userId,UpdateTypeEnum.AB_BALANCE_LESS_MIN_WARN,props, eventId),
							CopyOpEventSender.FEED_DESTINATION_NAME);
					// schedule a message for 3 hours later
					// TODO - trading hours
					int hours = Integer.parseInt(ConfigurationCopyopManager.getCopyopPropertiesValue("LOW_BALANCE_SECOND_CHECK_HOURS", "3"));
					ProfileManageEvent scheduled = new ProfileManageEvent(ProfileLinkCommandEnum.LOWBALANCE_COPYING, userId, 2);
					long delay = TimeUnit.HOURS.toMillis(hours);
					logger.debug("2Scheduled " + scheduled + " for " + delay);
					CopyOpEventSender.sendEvent(scheduled, CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME, delay);
				} else if (count == 2) {
					// after 6h in a row my balance is insufficient (3h after post #24 is sent ) and wasn't funded
					ArrayList<ProfileLink> copyLinks = (ArrayList<ProfileLink>) ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPY);
					// AND there's at least 1 trader I copy in my "Copying" list.
					if(copyLinks.size() > 0) {
						Map<String, String> props = new HashMap<String, String>();
						// user ID added here just because of the user name for push notification
						props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
						props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, UsersManager.getSkinIdByUserId(userId));
						CopyOpEventSender.sendUpdate(new UpdateMessage(userId,
																		UpdateTypeEnum.AB_BALANCE_LESS_MIN_COPYING,
																		props,
																		eventId),
														CopyOpEventSender.FEED_DESTINATION_NAME);
						boolean userIsTest = ProfileManager.isTestProfile(userId);						
						//the traders I copy will be moved to traders I watch
						for(ProfileLink copyLink:copyLinks) {
							boolean destUserIsTest = ProfileManager.isTestProfile(copyLink.getDestUserId());
							ProfileLinksManager.deleteCopyProfileLinkBatch(userId, copyLink.getDestUserId(), userIsTest, destUserIsTest);
							ProfileLinksManager.insertWatchProfileLink(userId, copyLink.getDestUserId(), userIsTest, destUserIsTest);
						}
					}
				}
			} else {
				logger.debug("The user "+userId+" have deposited. Balance: "+balance);
			}
		}catch (SQLException ex) {
			logger.debug("Can't get user balance: "+userId, ex);
		}
	}

	private void lowBalanceCopiers(long userId, int count) {
		// send msg 25 3 times
		List<Long> copiers = ProfileLinksManager.getCopiersIds(userId);
		if(copiers.size() > 0) {
			UUID eventId = UUIDs.timeBased();
			logger.info("lowBalanceCopiers: " + eventId);
			long balance = 0;
			long minSelfInvest = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("SELF_INVESTMENT_MIN_AMOUNT", "25"));
			try {
				balance = CopiedInvestmentManager.getUserBalance(userId);
				if(balance < minSelfInvest) {
					
					long delayHours = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("LOWBALANCE_COPYING_DELAY_HOURS", "24"));
					HashMap<String, String> props = new HashMap<String, String>();
					props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
					props.put(UpdateMessage.PROPERTY_KEY_COPIERS, String.valueOf(copiers.size()));
					props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, UsersManager.getSkinIdByUserId(userId));
					if(count != 1) {
							CopyOpEventSender.sendUpdate(new UpdateMessage(userId,
																			UpdateTypeEnum.AB_BALANCE_LESS_MIN_COPIED,
																			props,
																			eventId),
															CopyOpEventSender.FEED_DESTINATION_NAME);
					}

					// TODO trading hours
					if( count == 1 || count == 2) {
						ProfileManageEvent scheduled = new ProfileManageEvent(ProfileLinkCommandEnum.LOWBALANCE_COPIERS, userId, (count+1));
						logger.debug("1Scheduling "+scheduled +" for " +delayHours);
						CopyOpEventSender.sendEvent(scheduled, CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME, TimeUnit.HOURS.toMillis(delayHours));
					}
					
					if( count == 3) {
						// delete copiers
						List<ProfileLink> copiedList	= ProfileLinksManager.getProfileLinksByType(userId, ProfileLinkTypeEnum.COPIED_BY);
						for(ProfileLink copied:copiedList) {
							ProfileLinksManager.deleteCopyProfileLinkBatch(copied.getDestUserId(), copied.getUserId(), 
															ProfileManager.isTestProfile(copied.getDestUserId()),
															ProfileManager.isTestProfile(copied.getUserId()));
						}

					}
				}
			} catch (SQLException e) {
				logger.debug("LOWBALANCE_COPYING: Can't load user balance: "+userId);
			}
		} else {
			logger.debug("LOWBALANCE_COPYING: No copiers for userId:"+userId);
		}
	}

	private void coinsReset(long userId, UUID msgTimeuuid) {
		// reset profile coins
		Profile profile = ProfileManager.getProfile(userId);
		if(profile.getTimeResetCoins().equals(msgTimeuuid)) {
			logger.debug("Reseting coins for user:"+userId);
			long amount = ProfileCountersManager.resetProfileCoinsBalance(userId);
			CoinsBalanceHistory cbh = new CoinsBalanceHistory(userId, CoinsActionTypeEnum.RESET_COINS.getId(), true, amount, "", 0l);
			CoinsManager.insertCoinsBalanceHistory(cbh);
		} else {
			logger.debug("reset not executed for user:"+userId);
		}
	}

	private void showOff(long userId, long amount, long marketId, long currencyId, double rate, Date timeSettled) {
		Profile profile = ProfileManager.getProfile(userId);
		if(profile == null ) {
			logger.debug("Can't load profile for showOff:"+userId);
			return;
		}
		HashMap<String, String> props = new HashMap<String, String>();
			props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
			props.put(UpdateMessage.PROPERTY_KEY_MARKET_ID, String.valueOf(marketId));
			props.put(UpdateMessage.PROPERTY_KEY_TIME_SETTLED, String.valueOf(timeSettled.getTime()));
		BigDecimal resultInUSD = new BigDecimal(amount).divide(new BigDecimal(100)).multiply(new BigDecimal(rate));
		HashMap<String, String> resultInAllCurrencies = CurrenciesCacheManager.getConvertedAmounts(resultInUSD.doubleValue());
		// override the users currency - to avoid inaccuracy after double conversion currency/USD => USD/currency
		resultInAllCurrencies.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR+currencyId, ""+new BigDecimal(amount).divide(new BigDecimal(100)).setScale(2, RoundingMode.CEILING));
		props.putAll(resultInAllCurrencies);

		UUID inheritedUUID = UUIDs.timeBased();
		logger.info("showOff: " + inheritedUUID);
		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, inheritedUUID.toString());

		// send notifications to Facebook friends
		ArrayList<Long> fbFriendsUserIds  = FbManager.getFbFriendsUserIds(userId);
		for(Long fbFriendUserId: fbFriendsUserIds) {
			// send notification
			CopyOpEventSender.sendUpdate(new UpdateMessage(fbFriendUserId, UpdateTypeEnum.FB_SHOW_OFF, props), CopyOpEventSender.FEED_DESTINATION_NAME);
		}

		ArrayList<Long> profileCopiers =  (ArrayList<Long>) ProfileLinksManager.getCopiersIds(userId);
		for(Long copier:profileCopiers) {
			if(!fbFriendsUserIds.contains(copier)) {
				UpdateMessage update = new UpdateMessage(copier, UpdateTypeEnum.CP_SHOW_OFF, props);
				CopyOpEventSender.sendUpdate(update, CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}

		List<Long> watchers = ProfileLinksManager.getListWatchersIds(userId);
		for(Long watcherUserId : watchers) {
			if(!fbFriendsUserIds.contains(watcherUserId) && !profileCopiers.contains(watcherUserId)) {
				UpdateMessage update = new UpdateMessage(watcherUserId, UpdateTypeEnum.W_SHOW_OFF, props);
				CopyOpEventSender.sendUpdate(update, CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}

		// send system messages to online users
		if(!profile.isFrozen()&& !profile.isTest()) {
			//insert to Feed before sending system message
			FeedMessage fm = new FeedMessage();
			fm.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
			fm.setQueueType(QueueTypeEnum.SYSTEM.getId());
			fm.setMsgType(UpdateTypeEnum.SYS_ANY_SHOW_OFF.getId());
			fm.setProperties(props);
			fm.setTimeCreatedUUID(inheritedUUID);
			fm.setTimeCreated(new Date(UUIDs.unixTimestamp(inheritedUUID)));
			FeedManager.insertFeedMessage(fm);

			Set<Long> userIdsOnline = ProfilesOnlineManager.getUserIdsOnline();
			for(Long userIdOnline: userIdsOnline) {
				if(!fbFriendsUserIds.contains(userIdOnline) && !profileCopiers.contains(userIdOnline) && !watchers.contains(userIdOnline)&&userIdOnline != userId) {
					UpdateMessage updateAnyInvest = new UpdateMessage(userIdOnline, UpdateTypeEnum.SYS_ANY_SHOW_OFF, props);
					CopyOpEventSender.sendUpdate(updateAnyInvest, CopyOpEventSender.FEED_DESTINATION_NAME);
				}
			}
		}
	}

	private void frozen(){
		String strEventId = UUIDs.timeBased().toString();
		logger.info("frozen: " + strEventId);
		// load from cassandra
		ArrayList<Long> userIds = ProfileManager.getFrozenTraders();
		logger.debug("Sending message about "+userIds.size()+" users");
		// send notification to copiers
		for(Long userId : userIds){
			HashMap<String, String> props = new HashMap<String, String>();
				props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
				props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);
			List<Long> copiersIds = ProfileLinksManager.getUserIdLinksByType(userId, ProfileLinkTypeEnum.COPIED_BY);
			Map<Long, String> skinsByUser = UsersManager.getPushNotificationSkinsByUser(copiersIds);
			for(Long copierId : copiersIds) {
				UpdateMessage update = new UpdateMessage(copierId, UpdateTypeEnum.CP_FROZE, props);
				UsersManager.addPushNotificationSkinToMessage(update, skinsByUser);
				CopyOpEventSender.sendUpdate(update, CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}

		ProfileManager.deleteFrozenTraders();
	}

	private void copy(long userId, long destUserId) {
		// link create moved to json service
		//ProfileLinksManager.insertCopyProfileLinkBatch(userId, event.getDestUserId(), event.getAmount(), event.getCount(), event.getAssets());
		//ProfileCountersManager.increaseMutualCountsCopy(userId, event.getDestUserId());

		String strEventId = UUIDs.timeBased().toString();
		logger.info("copy: " + strEventId);

		// display properties for message
		HashMap<String, String> props = new HashMap<String, String>();
		props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
		props.put(UpdateMessage.PROPERTY_KEY_DEST_USER_ID, String.valueOf(destUserId));
		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);

		// send notifications
		ArrayList<Long> fbFriendsUserIds  = FbManager.getFbFriendsUserIds(userId);
		for(Long fbFriendUserId: fbFriendsUserIds) {
			// send notification
			CopyOpEventSender.sendUpdate(new UpdateMessage(fbFriendUserId, UpdateTypeEnum.FB_NEW_COPYING, props), CopyOpEventSender.FEED_DESTINATION_NAME);
		}
		// notify the destination that someone started copying him
		if(!fbFriendsUserIds.contains(destUserId)) {
			CopyOpEventSender.sendUpdate(new UpdateMessage(destUserId, UpdateTypeEnum.CP_NEW_COPYING, props), CopyOpEventSender.FEED_DESTINATION_NAME);
		}
		// notify the copiers of the copier that he has started copying someone
		List<Long> copiersUsersIds = ProfileLinksManager.getCopiersIds(userId);
		for(Long copierUserId: copiersUsersIds) {
			if(!fbFriendsUserIds.contains(copierUserId)) {
				// send notification
				CopyOpEventSender.sendUpdate(new UpdateMessage(copierUserId, UpdateTypeEnum.CP_NEW_COPYING, props), CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}
		long copiersCount = ProfileCountersManager.getProfileCopiers(destUserId);
		int maxCopiersPerAccount = Integer.valueOf(ConfigurationCopyopManager.getCopyopPropertiesValue("MAX_COPIERS_PER_ACCOUNT", "" + 100));
		if(copiersCount == maxCopiersPerAccount) {
			Date date = ProfileManager.getLastTimeMostCopied(destUserId);
			if(date == null || !isInCurrentWeek(date)) {
				Map<String, String> ab_props = new HashMap<String, String>();
				// user ID added here just because of the user name for push notification
				ab_props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(destUserId));
				ab_props.put(UpdateMessage.PROPERTY_KEY_PUSH_SKIN_ID, UsersManager.getSkinIdByUserId(destUserId));
				ab_props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);
				CopyOpEventSender.sendUpdate(new UpdateMessage(destUserId, UpdateTypeEnum.AB_IM_FULLY_COPIED, ab_props),
												CopyOpEventSender.FEED_DESTINATION_NAME);
				ProfileManager.updateLastTimeMostCopied(destUserId);
			}
		} else if (copiersCount == (maxCopiersPerAccount - 1) || copiersCount == (maxCopiersPerAccount - 5)) {
			Map<String, String> nearlyFullProps = new HashMap<String, String>();
			nearlyFullProps.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(destUserId));
			nearlyFullProps.put(UpdateMessage.PROPERTY_KEY_SEATS_LEFT, String.valueOf((maxCopiersPerAccount - copiersCount)));
			nearlyFullProps.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);
			Map<Long, Boolean> watchersInfo = ProfileLinksManager.getWatchersIds(destUserId);
			Map<Long, String> skinsByUser = UsersManager.getPushNotificationSkinsByUser(watchersInfo.keySet());
			UpdateMessage updateMsg = null;
			for(Map.Entry<Long, Boolean> watcherInfo : watchersInfo.entrySet()) {
				updateMsg = new UpdateMessage(watcherInfo.getKey(), UpdateTypeEnum.W_NEARLY_FULL, nearlyFullProps);
				if (watcherInfo.getValue()) { // user has subscribed for mobile notifications
					UsersManager.addPushNotificationSkinToMessage(updateMsg, skinsByUser);
				} else {
					updateMsg.getWherePosted().remove(QueueTypeEnum.MOBILE_NOTIFICATION);
				}
				// send notification
				CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}
	}

	private void watch(long userId, long destUserId){
		// link create moved to json service
		//ProfileLinksManager.insertWatchProfileLink(userId, destUserId);

		// display properties for message
		HashMap<String, String> props = new HashMap<String, String>();
			props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(userId));
			props.put(UpdateMessage.PROPERTY_KEY_DEST_USER_ID, String.valueOf(destUserId));

		String strEventId = UUIDs.timeBased().toString();
		logger.info("watch: " + strEventId);

		props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);

		// send notifications
		ArrayList<Long> fbFriendsUserIds  = FbManager.getFbFriendsUserIds(userId);
		for(Long fbFriendUserId: fbFriendsUserIds) {
			// send notification
			CopyOpEventSender.sendUpdate(new UpdateMessage(fbFriendUserId, UpdateTypeEnum.FB_NEW_WATCHING, props),
											CopyOpEventSender.FEED_DESTINATION_NAME);
		}
	}

	private static void uncopy(long userId, long destUserId) {
		// save profile_link history
//		ProfileLink link = ProfileLinksManager.getProfileLink(userId, destUserId, ProfileLinkTypeEnum.COPY);
//		ProfileLinksManager.insertProfileLinkHistory(link, reason);
//		ProfileLink revlink = ProfileLinksManager.getProfileLink(destUserId, userId, ProfileLinkTypeEnum.COPIED_BY);
//		ProfileLinksManager.insertProfileLinkHistory(revlink, reason);
//		// delete both links ( copy <-> copied )
//		ProfileLinksManager.deleteCopyProfileLinkBatch(userId, destUserId);
//		// update profile - copiers count, copying count
//		ProfileCountersManager.decreaseMutualCountsCopy(userId, destUserId);

		//Send notification if new copy slot is available
		long copiersCount = ProfileCountersManager.getProfileCopiers(destUserId);
		int maxCopiersPerAccount = Integer.valueOf(ConfigurationCopyopManager.getCopyopPropertiesValue("MAX_COPIERS_PER_ACCOUNT", "" + 100));
		if((copiersCount+1) == maxCopiersPerAccount) {
			// send notification to watchers
			Map<Long, Boolean> watchersInfo = ProfileLinksManager.getWatchersIds(destUserId);
			Map<Long, String> skinsByUser = UsersManager.getPushNotificationSkinsByUser(watchersInfo.keySet());
			Map<String, String> props = new HashMap<String, String>();
			props.put(UpdateMessage.PROPERTY_KEY_USER_ID, String.valueOf(destUserId));

			String strEventId = UUIDs.timeBased().toString();
			logger.info("uncopy: " + strEventId);

			props.put(UpdateMessage.PROPERTY_KEY_INHERITED_UUID, strEventId);

			UpdateMessage updateMsg = null;
			for(Map.Entry<Long, Boolean> watcherInfo : watchersInfo.entrySet()) {
				updateMsg = new UpdateMessage(watcherInfo.getKey(), UpdateTypeEnum.W_NOW_AVAILABLE, props);
				if (watcherInfo.getValue()) { // user has subscribed for mobile notifications
					UsersManager.addPushNotificationSkinToMessage(updateMsg, skinsByUser);
				} else {
					updateMsg.getWherePosted().remove(QueueTypeEnum.MOBILE_NOTIFICATION);
				}
				CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
			}
		}
	}

	private static void unwatch(long userId, long destUserId){
//		// save profile_link history
//		ProfileLink link = ProfileLinksManager.getProfileLink(userId, destUserId, ProfileLinkTypeEnum.WATCH);
//		ProfileLinksManager.insertProfileLinkHistory(link, ProfileLinkChangeReasonEnum.STOP_WATCH);
//		ProfileLink revlink = ProfileLinksManager.getProfileLink(destUserId, userId, ProfileLinkTypeEnum.WATCHED_BY);
//		ProfileLinksManager.insertProfileLinkHistory(revlink, ProfileLinkChangeReasonEnum.STOP_WATCH);
//
//		// delete both links ( watch <-> watched )
//		ProfileLinksManager.insertDeleteWatchProfileLink(userId, destUserId);
	}
	
	public static boolean isInCurrentWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setTime(new Date()); // Now use today date.
		c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK) + 1);
		return date.compareTo(c.getTime())>=0;
	}
}


