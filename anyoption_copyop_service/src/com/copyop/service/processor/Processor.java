package com.copyop.service.processor;

import com.copyop.common.jms.events.CopyOpEvent;

public interface Processor {
	public void process(CopyOpEvent event);
}
