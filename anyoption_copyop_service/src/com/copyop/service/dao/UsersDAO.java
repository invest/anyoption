package com.copyop.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.copyop.service.beans.UserPushNotificationInfo;



public class UsersDAO extends DAOBase {

	protected static final Logger log = Logger.getLogger(UsersDAO.class);

    public static void insertBalanceLog(Connection con, long writerId, long userId, String table, long key, int command, String utcoffset) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "INSERT INTO balance_history " +
                    "(id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset) " +
                "VALUES " +
                    "(seq_balance_history.nextval, sysdate, ?, " +
                        "(select balance from users where id = ?), " +
                        "(select tax_balance from users where id = ?), ?, ?, ?, ?, (SELECT utc_offset FROM users WHERE ID = ?))";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, userId);
            pstmt.setLong(3, userId);
            pstmt.setLong(4, writerId);
            pstmt.setString(5, table);
            pstmt.setLong(6, key);
            pstmt.setInt(7, command);
            pstmt.setLong(8, userId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

	public static void addToBalance(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql =
				" UPDATE" +
							" users " +
				" SET " +
							" balance = balance + ?," +
							" time_modified = SYSDATE, " +
							" utc_offset_modified = utc_offset " +
				" WHERE " +
							" id = ?";

			ps =  con.prepareStatement(sql);
			ps.setLong(1,amount);
			ps.setLong(2,id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static long getUserBalance(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        long balance = 0;

        try {
            String sql =
            	"SELECT " +
            			" u.balance " +
            	"FROM " +
            			" users u " +
            	"WHERE " +
            			" u.id = ? ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
            	balance = rs.getLong("balance");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
       return balance;
	}

	public static Map<Long, String> getPushNotificationSkinsByUser(Connection conn,
																	Collection<Long> users) throws SQLException {
		Map<Long, String> skinsByUser = new HashMap<Long, String>(users.size());
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        	conn.setAutoCommit(false);
        	insertUsersInTmpTable(conn, users);

            pstmt = conn.prepareStatement(" SELECT id, skin_id FROM users WHERE id IN (SELECT user_id FROM TEMP_USERS_PUSH_NOTIFICATIONS) ");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                skinsByUser.put(rs.getLong("id"), rs.getString("skin_id"));
            }

            conn.commit();
        } catch (SQLException e) {
        	if (!conn.getAutoCommit()) {
	        	log.error("Unable to get push notification users skins. Rolling-back transaction", e);
				conn.rollback();
        	}
			throw e;
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
		return skinsByUser;
	}

	public static Map<Long, UserPushNotificationInfo> getUserPushNotificationInfo(
														Connection conn, Collection<Long> users) throws SQLException {
		Map<Long, UserPushNotificationInfo> userInfo = new HashMap<Long, UserPushNotificationInfo>(users.size());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn.setAutoCommit(false);
			insertUsersInTmpTable(conn, users);

			pstmt = conn.prepareStatement(" SELECT id, skin_id, currency_id, utc_offset "
										+ " FROM users "
										+ " WHERE id IN (SELECT user_id FROM TEMP_USERS_PUSH_NOTIFICATIONS) ");
			rs = pstmt.executeQuery();

			UserPushNotificationInfo user = null;
			while (rs.next()) {
				user = new UserPushNotificationInfo();
				user.setId(rs.getLong("id"));
				user.setSkinId(rs.getString("skin_id"));
				user.setCurrencyId(rs.getString("currency_id"));
				user.setOffsetUTC(rs.getString("utc_offset"));
				userInfo.put(user.getId(), user);
			}

			conn.commit();
		} catch (SQLException e) {
			if (!conn.getAutoCommit()) {
				log.error("Unable to get push notification users. Rolling-back transaction", e);
				conn.rollback();
			}
			throw e;
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return userInfo;
	}

	private static void insertUsersInTmpTable(Connection conn, Collection<Long> users) throws SQLException {
    	PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(" INSERT INTO TEMP_USERS_PUSH_NOTIFICATIONS (user_id) VALUES (?) ");
            for (long userId : users) {
            	pstmt.setLong(1, userId);
				pstmt.addBatch();
			}
            pstmt.executeBatch();
        } finally {
            closeStatement(pstmt);
        }
    }

	public static String getSkinIdByUserId(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(" select skin_id from users where id = ? ");
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getString("skin_id");
            } else {
            	log.warn("User [" + userId + "] not found. Returning default skin [" + Skin.SKIN_ENGLISH + "]");
            	return String.valueOf(Skin.SKIN_ENGLISH);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
	}

	public static Long getCurrencyIdByUserId(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(" SELECT currency_id FROM users WHERE id = ? ");
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("currency_id");
            } else {
            	log.warn("User [" + userId + "] not found.");
            	return null;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
	}

//	public static long validateUsersBalance(Connection conn, long userId, long amount) throws SQLException {
//
//		long balance = UsersDAO.getUserBalance(conn, userId);
//		if( balance < amount ) {
//			// take from properties in DB
//			if(balance > 25 ) {
//				return balance;
//			} else {
//				return -1;
//			}
//		} else {
//			return amount;
//		}
//	}
}