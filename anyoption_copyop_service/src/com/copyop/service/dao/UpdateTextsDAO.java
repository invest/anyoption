/**
 *
 */
package com.copyop.service.dao;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.copyop.common.dao.DAOBase;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class UpdateTextsDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(UpdateTextsDAO.class);

	public static Map<UpdateTypeEnum, Map<QueueTypeEnum, Map<Integer, String>>> loadUpdateTexts(Session session) {
		Map<UpdateTypeEnum, Map<QueueTypeEnum, Map<Integer, String>>> updateTypeMap
						= new EnumMap<UpdateTypeEnum, Map<QueueTypeEnum,Map<Integer,String>>>(UpdateTypeEnum.class);

		PreparedStatement ps = methodsPreparedStatement.get("loadUpdateTexts");
		if (ps == null) {
			String cql = " select * from copyop.update_texts ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("loadUpdateTexts", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind());
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		Row row = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			UpdateTypeEnum updateType = UpdateTypeEnum.getById(row.getInt("update_id"));
			Map<QueueTypeEnum, Map<Integer, String>> queueTypeMap = updateTypeMap.get(updateType);
			if (queueTypeMap == null) {
				queueTypeMap = new EnumMap<QueueTypeEnum, Map<Integer,String>>(QueueTypeEnum.class);
				updateTypeMap.put(updateType, queueTypeMap);
			}

			QueueTypeEnum queueType = QueueTypeEnum.getById(row.getInt("queue_id"));
			Map<Integer, String> skinMap = queueTypeMap.get(queueType);
			if (skinMap == null) {
				skinMap = new HashMap<Integer, String>();
				queueTypeMap.put(queueType, skinMap);
			}
			skinMap.put(row.getInt("skin_id"), row.getString("message_text"));
		}

		return updateTypeMap;
	}

}
