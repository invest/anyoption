/**
 *
 */
package com.copyop.service.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.Constants;
import com.copyop.common.dto.base.FeedMessage;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


/**
 * @author pavelhe
 *
 */
public class InboxDAO extends com.copyop.common.dao.InboxDAO {

	private static final Logger log = Logger.getLogger(InboxDAO.class);

	/**
	 * TODO JAVA DOC
	 *
	 * @param session
	 * @param inboxMsg
	 */
	public static void insertInboxMsg(Session session, FeedMessage inboxMsg, UUID insertUUID) {
		PreparedStatement ps = methodsPreparedStatement.get("insertInbox");
		if (ps == null) {
			String cql = " INSERT INTO inbox (user_id, period, time_created, msg_type, properties)"
									+ " VALUES (?, ?, ?, ?, ?) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertInbox", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}

		SimpleDateFormat formatter = new SimpleDateFormat(Constants.INBOX_PERIOD_FORMAT);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

		ResultSet rs = session.execute(boundStatement.bind(inboxMsg.getUserId(),
															formatter.format(new Date(UUIDs.unixTimestamp(insertUUID))),
															insertUUID,
															inboxMsg.getMsgType(),
															inboxMsg.getProperties()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}
}
