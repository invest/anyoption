/**
 *
 */
package com.copyop.service.dao;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class AppoxeeDAO extends com.copyop.common.dao.AppoxeeDAO {

	private static final Logger log = Logger.getLogger(AppoxeeDAO.class);

	public static Map<Long, Long> getUserSegments(Session session, long userId) {
		Map<Long, Long> result = new LinkedHashMap<Long, Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getUserSegments");
		if (ps == null) {
			ps = session.prepare("SELECT * FROM copyop.appoxee_user_segments WHERE user_id = ?");
			methodsPreparedStatement.put("getUserSegments", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		Row row = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			result.put(row.getLong("application_id"), row.getLong("segment_id"));
		}
		return result;
	}

}
