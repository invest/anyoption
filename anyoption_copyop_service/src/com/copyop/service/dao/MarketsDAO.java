/**
 *
 */
package com.copyop.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.daos.DAOBase;

/**
 * @author pavelhe
 *
 */
public class MarketsDAO extends DAOBase {

	protected static final Logger log = Logger.getLogger(MarketsDAO.class);

	public static Map<Integer, Map<Integer, MarketNameBySkin>> getMarketNamesBySkin(
																				Connection conn) throws SQLException {
		Map<Integer, Map<Integer, MarketNameBySkin>> result = new HashMap<Integer, Map<Integer,MarketNameBySkin>>();
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT * FROM market_name_skin ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            MarketNameBySkin market = null;
            while (rs.next()) {
            	market = new MarketNameBySkin();
            	market.setMarketId(rs.getInt("market_id"));
            	market.setSkinId(rs.getInt("skin_id"));
            	market.setName(rs.getString("name"));
            	market.setShortName(rs.getString("short_name"));
            	Map<Integer, MarketNameBySkin> marketNamesBySkin = result.get(market.getMarketId());
            	if (marketNamesBySkin == null) {
            		marketNamesBySkin = new HashMap<Integer, MarketNameBySkin>();
            		result.put(market.getMarketId(), marketNamesBySkin);
            	}
            	marketNamesBySkin.put(market.getSkinId(), market);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }

		return result;
	}

}
