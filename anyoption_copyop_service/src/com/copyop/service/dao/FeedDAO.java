/**
 *
 */
package com.copyop.service.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.jms.updates.UpdateMessage;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class FeedDAO extends com.copyop.common.dao.FeedDAO {

	private static final Logger log = Logger.getLogger(FeedDAO.class);

	/**
	 * TODO JAVA DOC
	 *
	 * @param session
	 * @param userId
	 * @param queueType
	 * @param limit
	 * @return
	 */
	public static List<Date> getPostAlgorithmFeed(Session session,
															long userId,
															QueueTypeEnum queueType,
															int limit) {
		PreparedStatement ps = methodsPreparedStatement.get("getPostAlgorithmFeed");
		if (ps == null) {
			String cql = " SELECT dateOf(now()) as now, dateOf(time_created) as date_created, time_created "
						+ " FROM feed_post_algorithm_data "
						+ " WHERE user_id = ? AND queue_type = ? LIMIT ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getPostAlgorithmFeed", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, queueType.getId(), limit));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		List<Date> result = new ArrayList<Date>();
		boolean firstPassed = false;
		List<Row> rows = rs.all();
		StringBuilder sb = new StringBuilder();
		sb.append("Extracted current date and create dates for last [")
			.append(limit)
			.append("] messages - ");
		if (rows.size() > 0) {
			for (Row row : rows) {
				if (!firstPassed) {
					Date now = row.getDate("now");
					result.add(now);
					sb.append("now: ").append(now).append(", ");
					firstPassed = true;
				}
				Date dateCreated = row.getDate("date_created");
				result.add(dateCreated);
				sb.append("[")
					.append(dateCreated)
					.append(", ")
					.append(row.getUUID("time_created").toString())
					.append("], ");
			}
			sb.replace(sb.length() - 2, sb.length(), ""); // remove last 2 symbols
		} else {
			sb.append("none");
		}
		log.debug(sb.toString());
		return result;
	}

	public static PreparedStatement getInsertPostAlgorithmDataPS(Session session) {
		PreparedStatement insertPostAlgorithmData = methodsPreparedStatement.get("insertPostAlgorithmData");
		if (insertPostAlgorithmData == null) {
			String cql =
				"INSERT INTO feed_post_algorithm_data (user_id, queue_type, time_created, msg_type) VALUES (?, ?, ?, ?);";

			insertPostAlgorithmData = session.prepare(cql);
			methodsPreparedStatement.put("insertPostAlgorithmData", insertPostAlgorithmData);
		}

		return insertPostAlgorithmData;
	}

	public static void insertBatchPostAlgorithmMessage(Session session,
													UpdateMessage um,
													QueueTypeEnum queueType,
													Integer ttl,
													UUID insertUUID) {
		PreparedStatement insertFeedStmt = null;
		if (ttl != null) {
			insertFeedStmt = getInsertFeedMessageWithTTLPS(session);
		} else {
			insertFeedStmt = getInsertFeedMessagePS(session);
		}
		PreparedStatement insertPostAlgorithmData = getInsertPostAlgorithmDataPS(session);
		BatchStatement batch = new BatchStatement();
		if (ttl != null) {
			batch.add(insertFeedStmt.bind(um.getUserId(),
											queueType.getId(),
											insertUUID,
											um.getUpdateType().getId(),
											um.getProperties(),
											ttl));
		} else {
			batch.add(insertFeedStmt.bind(um.getUserId(),
											queueType.getId(),
											insertUUID,
											um.getUpdateType().getId(),
											um.getProperties()));
		}
		batch.add(insertPostAlgorithmData.bind(um.getUserId(),
												queueType.getId(),
												insertUUID,
												um.getUpdateType().getId()));

		if (log.isTraceEnabled()) {
			batch.enableTracing();
		}
		ResultSet rs = session.execute(batch);
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static void insertPostAlgorithmMessage(Session session,
													UpdateMessage um,
													QueueTypeEnum queueType,
													UUID insertUUID) {
		BoundStatement boundStatement = new BoundStatement(getInsertPostAlgorithmDataPS(session));
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(um.getUserId(),
															queueType.getId(),
															insertUUID,
															um.getUpdateType().getId()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}
}
