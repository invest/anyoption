package com.copyop.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;

import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.copyop.common.managers.ConfigurationCopyopManager;

public class CopiedInvestmentDAO  extends DAOBase {

    public static long insertInvestment(Connection conn, 
    									long userId, 
    									long oppId, 
    									double amount, 
    									double level, 
    									double rate, 
    									long srcInvId, 
    									CopyOpInvTypeEnum invType, 
    									long writerId, 
    									int dir,
    									long bonusOddsChangeTypeId,
    									long bonusUsersId,
    									String utcOffsetCreated,
    									double oddsWin,
    									double oddsLose
    									) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long id = -1;
        try {
            String sql =
                    "INSERT INTO " +
                				"investments " +
                    				"(" +
                    					"id, " +
	                    				"user_id, " + 
	                    				"opportunity_id, " + 
	                    				"type_id, " +
	                    				"amount, " +
	                    				"current_level, " + 
	                    				"odds_win, " +
	                    				"odds_lose, " +
	                    				"writer_id, " +
	                    				"ip, " +
	                    				"time_created, " + 
	                    				"rate,  " +
	                    				"copyop_type_id, " +
	                    				"copyop_inv_id, " +
	                    				"win, " + 
	                    				"lose, " + 
	                    				"house_result, " +
	                    				"is_settled, " + 
	                    				"is_canceled, " + 
	                    				"is_void, " +
	                    				"is_like_hourly, " +
	                    				"bonus_odds_change_type_id," +
	                    				"bonus_user_id,   " +
	                    				"utc_offset_created " +
                    				") " +
                				"VALUES " +
                        			"(" +
	                        			"SEQ_INVESTMENTS.NEXTVAL, " + 
	                        			"?, " + // 1 - userId
	                        			"?, " +	// 2 - oppId
	                        			"?, " + // 3 - OppType
	                        			"?, " + // 4 - amount
	                        			"?, " + // 5 - level
	                        			"?, " + // 6 - odds win
	                        			"?, " + // 7 - odds lose
	                        			"?, " + // 8 - writerId
	                        			"?, " + // 9 - ip
	                        			"?, " + // 10 - timestamp
	                        			"?, " +	// 11 - rate
	                        			"?, " + // 12 - copyop type 
	                        			"?, " +  // 13 -src inv id
	                        			"0, " + // win
	                        			"0, " + // lose
	                        			"0, " + // house
	                        			"0, " + // settled
	                        			"0, " + // canceled
	                        			"0, " + // void
	                        			"1," +  // all copied investments are "like" hourly
	                        			"?," + //14 bonus_odds_change_type_id
	                        			"?,  " + //15 bonus_user_id
	                        			"? " + // 16 utc offset created
                        			")";

            pstmt = conn.prepareStatement(sql);

			pstmt.setLong(1, userId);  
			pstmt.setLong(2, oppId);  
			pstmt.setLong(3, dir);
			pstmt.setDouble(4, amount);
			pstmt.setDouble(5, level);
			pstmt.setDouble(6, oddsWin); // odds win
			pstmt.setDouble(7, oddsLose);  // odds lose
			pstmt.setLong(8, writerId);
			pstmt.setString(9, "0.0.0.0");
			pstmt.setTimestamp(10, new Timestamp(new Date().getTime()));
			pstmt.setDouble(11, rate);
			pstmt.setInt(12, invType.getCode());// copyop_inv_type.COPY/FOLLOW/SELF
			pstmt.setLong(13, srcInvId);// src investment id
			pstmt.setLong(14, bonusOddsChangeTypeId);
            if (bonusOddsChangeTypeId > 0 && bonusUsersId > 0) {
                pstmt.setLong(15, bonusUsersId);
            } else {
                pstmt.setNull(15, Types.NUMERIC);
            }
            pstmt.setString(16, utcOffsetCreated);
			rs = pstmt.executeQuery();
			
			id = getSeqCurValue(conn, "SEQ_INVESTMENTS");
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }
    
    public static long userIdForInvestment(Connection conn, long invId) throws SQLException {
    	PreparedStatement pstmt = null;
        ResultSet rs = null;
        long id = -1;
        try {
            String sql = "SELECT user_id FROM investments WHERE id = ?";
            pstmt = conn.prepareStatement(sql);
   			pstmt.setLong(1, invId);  
			rs = pstmt.executeQuery();
			if (rs.next()) {
				 id = rs.getLong(1);
			}
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }
/*
	public static Double convertToBaseAmount(Connection con, long amount, long userId) throws SQLException {
		PreparedStatement ps=null;
		ResultSet rs=null;
		Double res = new Double(0);
		
		try {
			String sql="select CONVERT_AMOUNT_TO_USD_USER_ID(?,?) base_amount from dual";
			ps = con.prepareStatement(sql);
			
			ps.setLong(1, amount);
			ps.setLong(2, userId);
			
			rs=ps.executeQuery();
			
			if (rs.next()) {
				res = rs.getDouble("base_amount");
			} 
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return res;
	}
*/    

}
