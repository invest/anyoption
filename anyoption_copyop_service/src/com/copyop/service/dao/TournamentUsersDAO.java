package com.copyop.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.TournamentUsers;
import com.anyoption.common.daos.DAOBase;

public class TournamentUsersDAO extends DAOBase {

	protected static final Logger log = Logger.getLogger(TournamentUsersDAO.class);

	public static boolean insertUser(Connection conn, TournamentUsers tournamentUsers) throws SQLException {
        PreparedStatement pstmt = null;
        boolean isInsert = false;
        try {
            String sql =
            				"INSERT " +
							 "INTO tournament_users " + 
							 "( " +
							      "user_id, " + 
							      "tournament_id, " +
							      "score, " + 
							      "score_2, " +
							      "time_created, " +
							      "id, " +
							      "time_updated " +
							 ") " +
			                 "SELECT " +
			                 	"?, " +
			                 	"?, " +
			                 	"?, " +
			                 	"?, " +
			                 	"sysdate, " +
			                 	"seq_tournament_users.nextVal, " +
			                 	"sysdate " +
			                 "FROM " +
			                 	"DUAL " +
			                 "WHERE " +
			                 	"NOT EXISTS (" +
			                 					"SELECT " + 
			                 						"1 " + 
                                                "FROM " +
                                                	"tournament_users tu " +
                                                "WHERE " +
                                                	"tu.user_id = ? AND " +
                                                	"tu.tournament_id = ? " +
                                 ") ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, tournamentUsers.getUserId());
            pstmt.setLong(2, tournamentUsers.getTournamentId());
            pstmt.setDouble(3, tournamentUsers.getScore());
            pstmt.setDouble(4, tournamentUsers.getScore2());
            pstmt.setLong(5, tournamentUsers.getUserId());
            pstmt.setLong(6, tournamentUsers.getTournamentId());
            isInsert = pstmt.executeUpdate() > 0 ? true : false;
        } finally {
            closeStatement(pstmt);
        }
		return isInsert;
    }

	public static void updateUser(Connection conn, TournamentUsers tournamentUsers) throws SQLException {
		
		PreparedStatement ps = null;		
		int index = 1;
		try	{

		String sql = " UPDATE " +
					 	" tournament_users " +
					 " SET " +
					 	" score	= score + ?, " +
					 	" score_2 = score_2 + ?, " +
					 	" time_updated = sysdate " +
					 " WHERE " +
					 	" user_id = ? AND "
					  + " tournament_id = ? ";

		ps = conn.prepareStatement(sql);
		ps.setDouble(index++, tournamentUsers.getScore());
		ps.setDouble(index++, tournamentUsers.getScore2());
		ps.setLong(index++, tournamentUsers.getUserId());
		ps.setLong(index++, tournamentUsers.getTournamentId());

		ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
		
	}
}