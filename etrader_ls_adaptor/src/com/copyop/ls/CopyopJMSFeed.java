/**
 *
 */
package com.copyop.ls;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ConnectionLoop;
import com.anyoption.common.jms.JMSHandler;
import com.anyoption.common.jms.ifc.ExtendedMessageListener;
import com.copyop.common.jms.updates.UpdateMessage;

/**
 * @author pavelhe
 *
 */
public class CopyopJMSFeed implements ExtendedMessageListener {
	private static final Logger log = Logger.getLogger(CopyopJMSFeed.class);

	protected JMSHandler jmsHandler;
	protected ConnectionLoop connector;
	protected int recoveryPause;
	CopyopFeedListenerIfc listener;

	public CopyopJMSFeed(String initialContextFactory,
								String providerURL,
								String connectionFactoryName,
								String topicName,
								CopyopFeedListenerIfc listener) {
		this.recoveryPause = 2000;
		this.listener = listener;
		jmsHandler = new JMSHandler(CopyopJMSFeed.class.getSimpleName(),
										initialContextFactory,
										providerURL,
										null,
										null,
										connectionFactoryName,
										topicName);
		jmsHandler.setListener(this);

		connector = new CopyopFeedConnectionLoop(jmsHandler, recoveryPause);
		connector.start();

		log.info("JMS feed ready.");
	}

	@Override
	public void onMessage(Message msg) {
		if (log.isTraceEnabled()) {
			log.trace("On msg: " + msg);
		}

		if (msg instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) msg;
			UpdateMessage uMsg = null;
			try {
				uMsg = (UpdateMessage) objMsg.getObject();
			} catch (JMSException e) {
				log.error("JMS could not provide the message", e);
				/* WARNING - Method exit point */
				return;
			}
			if (uMsg != null) {
				listener.onMessage(uMsg);
			} else {
				log.error("Received [null] message");
			}
		} else {
			log.warn("Unexpected non-object message: " + msg);
		}
	}

	@Override
	public void onException(JMSException e) {
		log.error("JMS exception occurred. Resetting connection", e);

		jmsHandler.reset();
		connector = new CopyopFeedConnectionLoop(jmsHandler, recoveryPause);
        connector.start();
	}

	/**
     * Disconnects from the levels service and free all resources used by the cache.
     */
    public void close() {
        if (null != connector) {
            connector.abort();
        }
        if (null != jmsHandler) {
            jmsHandler.close();
        }
    }

	 /**
     * This thread keeps on trying to connect to JMS until succeeds. When connected
     * calls the onConnection method
     */
    private class CopyopFeedConnectionLoop extends ConnectionLoop {

        public CopyopFeedConnectionLoop(JMSHandler jmsHandler, int recoveryPause) {
            super(jmsHandler, recoveryPause);
            this.threadName = CopyopFeedConnectionLoop.class.getSimpleName();
        }

        @Override
		protected void onConnectionCall() {
            // Let the connector go. It did its job.
            connector = null;
        }

        @Override
		protected void connectionCall() throws JMSException, NamingException {
            // Initialize TopicSubscriber
            jmsHandler.initTopicSubscriber();
        }
    }

}
