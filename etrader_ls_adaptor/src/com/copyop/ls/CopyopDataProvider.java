/**
 *
 */
package com.copyop.ls;

import il.co.etrader.ls.AdapterUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.lightstreamer.interfaces.data.DataProvider;
import com.lightstreamer.interfaces.data.DataProviderException;
import com.lightstreamer.interfaces.data.FailureException;
import com.lightstreamer.interfaces.data.ItemEventListener;
import com.lightstreamer.interfaces.data.SubscriptionException;

/**
 * @author pavelhe
 *
 */
public class CopyopDataProvider implements DataProvider, CopyopFeedListenerIfc {

	private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm";

	private Logger log;

	private CopyopJMSFeed copyopFeed;

	private ItemEventListener listener;

	private Map<QueueTypeEnum, Map<Long, String>> usersByQueueType;

	@Override
	public void init(@SuppressWarnings("rawtypes") Map params, File configDir) throws DataProviderException {
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> parameters = params;

			//load configuration
	        String logConfig = parameters.get("log_config");
	        if (logConfig != null) {
	            File logConfigFile = new File(configDir, logConfig);
	            String logRefresh = parameters.get("log_config_refresh_seconds");
	            if (logRefresh != null) {
	                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
	            } else {
	                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
	            }
	        }
	        log = Logger.getLogger(CopyopDataProvider.class);

	        log.info("Initializing " + CopyopDataProvider.class.getSimpleName());

	        Runtime.getRuntime().addShutdownHook(new CleanupHook());

	        copyopFeed =  new CopyopJMSFeed(parameters.get("initialContextFactory"),
							        		parameters.get("providerURL"),
							        		parameters.get("connectionFactory"),
							        		CopyOpEventSender.LS_TOPIC_DESTINATION_NAME,
							        		this);

	        usersByQueueType = new EnumMap<QueueTypeEnum, Map<Long, String>>(QueueTypeEnum.class);
	        for (QueueTypeEnum queueType :  QueueTypeEnum.values()) {
	        	usersByQueueType.put(queueType, new ConcurrentHashMap<Long, String>());
			}
	        log.info("Initializing " + CopyopDataProvider.class.getSimpleName() + " done");
		} catch (Throwable t) {
			log.fatal("Unable to initialize data provider", t);
			// In case of Error log4j does not work an Lighstreamer logger does not print the stack trace.
			t.printStackTrace();
			throw new DataProviderException(t.getClass().getName() + " " + t.getMessage());
		}
	}

	private class CleanupHook extends Thread {
        @Override
		public void run() {
            log.info("CleanupHook start.");
            copyopFeed.close();
            log.info("CleanupHook end.");
        }
    }

	@Override
	public boolean isSnapshotAvailable(String itemName) throws SubscriptionException {
		return false; // No snapshots will be cached for Copyop.
	}

	@Override
	public void setListener(ItemEventListener listener) {
		// This listener will pass updates to Lightstreamer Kernel
        this.listener = listener;
	}

	@Override
	public void subscribe(String itemName, boolean needsIterator) throws SubscriptionException, FailureException {
		if (log.isInfoEnabled()) {
            log.info("Subscribing to " + itemName);
        }
		try {
			String[] itemInfo = itemName.split("_");
			itemInfo = AdapterUtils.patchForUnderscoreInUserName(itemInfo);
			QueueTypeEnum queueType = QueueTypeEnum.getById(Integer.valueOf(itemInfo[0]));
			long userId = Long.valueOf(itemInfo[itemInfo.length - 1]);
			String prevItem = usersByQueueType.get(queueType).put(userId, itemName);
			if (prevItem != null) {
				log.warn("User is already subscribed with item [" + itemName + "]");
			} else if (log.isDebugEnabled()) {
				log.debug("Subscribed user [" + itemInfo[1] + ":" + itemInfo[itemInfo.length - 1] + "] to [" + queueType + "]");
			}
		} catch (Exception e) {
			log.error("Unable to subscribe for item [" + itemName + "]", e);
			throw new SubscriptionException(e.getClass().getSimpleName() + ":" + e.getMessage()
												+ " while trying to subscribe for item [" + itemName + "]");
		}
	}

	@Override
	public void unsubscribe(String itemName) throws SubscriptionException, FailureException {
		if (log.isInfoEnabled()) {
            log.info("Unsubscribing from " + itemName);
        }
		try {
			String[] itemInfo = itemName.split("_");
			itemInfo = AdapterUtils.patchForUnderscoreInUserName(itemInfo);
			QueueTypeEnum queueType = QueueTypeEnum.getById(Integer.valueOf(itemInfo[0]));
			long userId = Long.valueOf(itemInfo[itemInfo.length - 1]);
			String prevItem = usersByQueueType.get(queueType).remove(userId);
			if (prevItem == null) {
				log.warn("Unsubscribing user that has not been subscribed");
			} else if (log.isDebugEnabled()) {
				log.debug("Unsubscribed user [" + itemInfo[1] + ":" + itemInfo[itemInfo.length - 1] + "] from [" + queueType + "]");
			}
		} catch (Exception e) {
			log.error("Unable to unsubscribe from item [" + itemName + "]", e);
			throw new SubscriptionException(e.getClass().getSimpleName() + ":" + e.getMessage()
												+ " while trying to unsubscribe from item [" + itemName + "]");
		}
	}

	@Override
	public void onMessage(UpdateMessage msg) {
		log.info("Received message: " + msg);

		Set<QueueTypeEnum> wherePosted = msg.getWherePosted();
		for (QueueTypeEnum queueType : wherePosted) {
			String subscription = usersByQueueType.get(queueType).get(msg.getUserId());
			if (subscription != null) {
				sendToKernel(msg, subscription, queueType);
			} else {
				if (log.isTraceEnabled()) {
					log.trace("Message will not be sent. User not subscribed for [" + queueType + "]");
				}
			}
		}

	}

	private void sendToKernel(UpdateMessage msg, String subscription, QueueTypeEnum queueType) {
		Map<String, String> msgMap = new HashMap<String, String>(msg.getProperties());
		msgMap.put("key", String.valueOf(queueType.getId()));
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
		dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		msgMap.put(UpdateMessage.PROPERTY_KEY_UPDATE_TYPE, String.valueOf(msg.getUpdateType().getId()));
		msgMap.put(UpdateMessage.PROPERTY_KEY_TIME_CREATED, String.valueOf(msg.getTimeCreated().getTime()));
		msgMap.put(UpdateMessage.PROPERTY_KEY_UUID, msg.getTimeCreatedUUID().toString());
		listener.update(subscription, msgMap, false);
		log.debug("Message sent to [" + subscription + "]");
	}

}
