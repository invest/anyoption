/**
 *
 */
package com.copyop.ls;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.ls.AdapterUtils;
import il.co.etrader.ls.DBUtil;
import il.co.etrader.ls.bl_managers.UsersManager;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.UnixUtil;
import com.copyop.common.managers.ManagerBase;
import com.copyop.ls.managers.ProfilesOnlineManager;
import com.lightstreamer.interfaces.metadata.AccessException;
import com.lightstreamer.interfaces.metadata.CreditsException;
import com.lightstreamer.interfaces.metadata.ItemsException;
import com.lightstreamer.interfaces.metadata.MetadataProviderAdapter;
import com.lightstreamer.interfaces.metadata.MetadataProviderException;
import com.lightstreamer.interfaces.metadata.NotificationException;
import com.lightstreamer.interfaces.metadata.SchemaException;
import com.lightstreamer.interfaces.metadata.TableInfo;

/**
 * @author pavelhe
 *
 */
public class CopyopMetadataProvider extends MetadataProviderAdapter {

	private Logger log;

	private static String NOPARAM = " is missing.\nProcess exits";
    private static String USEDEFAULT = " is missing. Using default.";

    private Map<String, UserBase> qualifiedUsersByName;
    private Map<String, UserBase> qualifiedUsersBySession;

    private String serverId;

    public CopyopMetadataProvider() {
    	qualifiedUsersByName = new ConcurrentHashMap<String, UserBase>();
        qualifiedUsersBySession = new ConcurrentHashMap<String, UserBase>();
    }

	@Override
	public void init(@SuppressWarnings("rawtypes") Map params, File configDir) throws MetadataProviderException {
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> parameters = params;

			String logConfig = parameters.get("log_config");
	        if (logConfig != null) {
	            File logConfigFile = new File(configDir, logConfig);
	            String logRefresh = parameters.get("log_config_refresh_seconds");
	            if (logRefresh != null) {
	                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
	            } else {
	                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
	            }
	        }
	        log = Logger.getLogger(CopyopMetadataProvider.class);

	        log.info("Initializing " + CopyopMetadataProvider.class.getSimpleName());

	        log.info("Initializing Cassandra...");
	        String contactPointsStr = parameters.get("cassandra_contact_points");
			String[] contactPoints = contactPointsStr.split(",");
			String keyspace = parameters.get("cassandra_keyspace");
			log.info("cassandra_contact_points: " + contactPointsStr);
			log.info("cassandra_keyspace: " + keyspace);
			ManagerBase.init(contactPoints, keyspace);
			ManagerBase.getSession();
			log.info("Initializing Cassandra done");

			Runtime.getRuntime().addShutdownHook(new CleanupHook());

			HashMap<String,String> dbMap = new HashMap<String,String>();
	        dbMap.put("dburl", getParam(parameters, "dburl", true, null));
	        dbMap.put("dbuser", getParam(parameters, "dbuser", true, null));
	        dbMap.put("dbpass", getParam(parameters, "dbpass", true, null));
	        dbMap.put("dbpoolname", getParam(parameters, "dbpoolname", true, null));
	        DBUtil.init(dbMap);
	        log.debug("Pool initialized.");

	        this.serverId = getServerId();

	        ProfilesOnlineManager.closeServerSessions(serverId);
	        log.info("Initializing " + CopyopMetadataProvider.class.getSimpleName() + " done");
		} catch (Throwable t) {
			log.fatal("Unable to initialize metadata provider", t);
			// In case of Error log4j does not work an Lighstreamer logger does not print the stack trace.
			t.printStackTrace();
			throw new MetadataProviderException(t.getClass().getName() + " " + t.getMessage());
		}
	}

	private class CleanupHook extends Thread {
        @Override
		public void run() {
            log.info("CleanupHook start.");
            ProfilesOnlineManager.closeServerSessions(serverId);
            ManagerBase.closeCluster();
            log.info("CleanupHook end.");
        }
    }

	@Override
	public String[] getItems(String user, String sessionID, String group) throws ItemsException {
		log.debug("getItems - id: " + group);
        return tokenize(group);
	}

	@Override
	public String[] getSchema(String user,
								String sessionID,
								String group,
								String schema) throws ItemsException, SchemaException {
		log.debug("getSchema - id: " + group + " schema: " + schema);
        return tokenize(schema);
	}

	@Override
	public void notifyUser(String user,
							String password,
							@SuppressWarnings("rawtypes") Map httpHeaders) throws AccessException, CreditsException {
		user = unwrapUser(user);
        if (log.isTraceEnabled()) {
            log.trace("notifyUser - user: " + user + " password: " + password);
        }
        try {
			for (Object header : httpHeaders.keySet()) {
				if (null != header) {
					log.info("notifyUser httpheaders " + header.toString() + " = " + httpHeaders.get(header));
				}
			}
		} catch (RuntimeException e) {
			log.info("notifyUser error", e);
		}

        if (user != null) {
        	UserBase objUser = null;
        	try {
				objUser = UsersManager.getUserByName(user);
			} catch (SQLException e) {
				log.error("Cannot get user [" + user + "] from DB", e);
				/* !!! WARNING !!! Method exit point! */
				return;
			}

        	if (UsersManager.isUserQualified(objUser, password)) {
        		log.debug("qualified user: " + user);
                qualifiedUsersByName.put(user, objUser);
        	}
        }
	}

	@Override
	public void notifyNewSession(
					String user,
					String sessionID,
					@SuppressWarnings("rawtypes") Map clientContext) throws CreditsException, NotificationException {
		user = unwrapUser(user);
        try {
	        log.info("notifyNewSession clientContext " + clientContext);
        } catch (Exception e) {
        	log.info("notifyNewSession error", e);
		}
        try {
	        for (Object clientC : clientContext.values()) {
	        	if (null != clientC) {
	        		log.info("notifyNewSession loop clientContext " + clientC.toString());
	        	}
			}
        } catch (Exception e) {
        	log.info("notifyNewSession loop error", e);
		}

        if (user != null) {
        	UserBase objUser = qualifiedUsersByName.remove(user);
        	if (objUser != null) {
        		log.debug("qualified user: " + user + " session: " + sessionID);
        		qualifiedUsersBySession.put(sessionID, objUser);
        		ProfilesOnlineManager.insertProfileOnlineStatus(objUser.getId(), serverId, sessionID, true);
        	} else {
        		log.warn("User [" + user + "] with session [" + sessionID + "] not qualified by user name");
        	}
        }
	}

	@Override
	public boolean wantsTablesNotification(String user) {
		return true; // Because of the next method.
	}

	@Override
	public void notifyNewTables(String user,
								String sessionID,
								TableInfo[] tables) throws CreditsException, NotificationException {
		user = unwrapUser(user);
        String itemName = null;
        for (int i = 0; i < tables.length; i++) {
        	itemName = tables[i].getId();
            log.debug("notifyNewTables - user: " + user + " session: " + sessionID + " table: " + itemName);
            validateUserTable(user, sessionID, itemName);
        }
	}

	 /**
     * This method checks whether a table that can be used only by logged-in
     * users is used by such user.
     *
     * @param user
     * @param sessionID
     * @param itemName
     * @throws CreditsException
     */
    private void validateUserTable(String user,
    								String sessionID,
    								String itemName) throws CreditsException {
    	String [] itemNameArray = itemName.split("_");
    	itemNameArray = AdapterUtils.patchForUnderscoreInUserName(itemNameArray);
    	if (null == user || null == sessionID || null == qualifiedUsersBySession.get(sessionID)) {
            log.warn(itemNameArray[0] + " table request from not qualified user: " + user);
            throw new CreditsException(0, "No permissions.");
        } else {
        	if (itemNameArray.length < 1) {
        		log.warn(itemNameArray[0] + " table item name without username");
        		throw new CreditsException(0, "No permissions. Table item name without username");
        	} else if (!itemNameArray[1].equals(user)) {
        		log.warn(itemNameArray[0] + " table username [" + itemNameArray[1] + "] does not match session username");
        		throw new CreditsException(0, "No permissions. Table username does not match session username");
        	} else {
        		log.debug(itemNameArray[0] + " table subscribed for user: " + user);
        	}
        }
    }

	@Override
	public void notifySessionClose(String sessionID) throws NotificationException {
		log.debug("session closed: " + sessionID);
		if (null != sessionID) {
			UserBase objUser = qualifiedUsersBySession.remove(sessionID);
			if (objUser != null) {
				ProfilesOnlineManager.insertProfileOnlineStatus(objUser.getId(), serverId, sessionID, false);
			} else {
				log.warn("Session [" + sessionID + "] closed for unqualified user");
			}
		} else {
			log.warn("Closed session is [null]");
		}
	}

	private String getParam(Map<String, String> params,
								String toGet,
								boolean required,
								String def) throws MetadataProviderException {
        String res = params.get(toGet);
        if (res == null) {
            if (required) {
                throw new MetadataProviderException(toGet + NOPARAM);
            } else {
                if (log != null) {
                    log.warn(toGet + USEDEFAULT);
                }
                res = def;
            }
        }
        return res;
    }

    private String[] tokenize(String str) {
        StringTokenizer source = new StringTokenizer(str, " ");
        int dim = source.countTokens();
        String[] ret = new String[dim];

        for (int i = 0; i < dim; i++) {
            ret[i] = source.nextToken();
        }
        return ret;
    }

    /**
     * Because of some strange bug a user starting with 0 lose it until it gets to the MetadataAdapter.
     * To go around that problem we wrap the user with "a"s.
     *
     * @param user
     * @return The user without the wrapping "a"s.
     */
    private String unwrapUser(String user) {
        if (null == user || user.trim().equals("") || user.length() < 3) {
            return user;
        }
        return user.substring(1, user.length() - 1);
    }

    private String getServerId() throws MetadataProviderException {
    	log.info("Getting server ID from hostname");
		String unixRes = UnixUtil.runUnixCommand(ConstantsBase.UNIX_HOSTNAME_COMMAND);
		if (unixRes != null && unixRes.trim().length() > 0) {
			try {
				unixRes = unixRes.substring(0, unixRes.indexOf("."));
			} catch (Exception e) {
				log.warn("Unable to parse server ID. Using the whole hostname");
			}
		} else {
			log.fatal("Unable to get hostname");
			throw new MetadataProviderException("Unable to get server ID");
		}
		log.info("Server ID is [" + unixRes + "]");
		return unixRes;
    }

}
