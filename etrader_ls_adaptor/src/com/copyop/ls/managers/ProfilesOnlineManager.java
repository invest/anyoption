/**
 *
 */
package com.copyop.ls.managers;

import org.apache.log4j.Logger;

import com.copyop.ls.dao.ProfilesOnlineDAO;
import com.datastax.driver.core.Session;


/**
 * @author pavelhe
 *
 */
public class ProfilesOnlineManager extends com.copyop.common.managers.ProfilesOnlineManager {

	private static final Logger log = Logger.getLogger(ProfilesOnlineManager.class);

	public static void insertProfileOnlineStatus(long userId,
													String serverId,
													String sessionId,
													boolean startSession) {
		Session session = getSession();
		long beginTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		ProfilesOnlineDAO.insertProfileOnlineStatus(session, userId, serverId, sessionId, startSession);
		if (log.isTraceEnabled()) {
			log.trace("TIME: insertProfileOnlineStatus " + (System.currentTimeMillis() - beginTime) + " ms");
		}
	}

	public static void closeServerSessions(String serverId) {
		Session session = getSession();
		long beginTime = System.currentTimeMillis();
		ProfilesOnlineDAO.closeServerSessions(session, serverId);
		log.info("TIME: closeServerSessions " + (System.currentTimeMillis() - beginTime) + " ms");
	}

}
