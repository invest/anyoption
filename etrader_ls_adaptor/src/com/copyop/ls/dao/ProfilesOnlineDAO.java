/**
 *
 */
package com.copyop.ls.dao;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;


/**
 * @author pavelhe
 *
 */
public class ProfilesOnlineDAO extends com.copyop.common.dao.ProfilesOnlineDAO {

	private static final Logger log = Logger.getLogger(ProfilesOnlineDAO.class);
	
	private static final Long PROFILES_ONLINE_TTL_PERIOD = 604800L; // 1 week in seconds

	public static void insertProfileOnlineStatus(Session session,
													long userId,
													String serverId,
													String sessionId,
													boolean startSession) {
		PreparedStatement psPerUser = methodsPreparedStatement.get("insertProfileOnlinePerUser");
		if (psPerUser == null) {
			String cql =
					"INSERT INTO profiles_online_per_user "
						+ " (user_id, time_created, server_id, session_id, start_session) "
						+ " VALUES (?, now(), ?, ?, ?) USING TTL " + PROFILES_ONLINE_TTL_PERIOD + ";";

			psPerUser = session.prepare(cql);
			methodsPreparedStatement.put("insertProfileOnlinePerUser", psPerUser);
		}

		PreparedStatement psPerServer = methodsPreparedStatement.get("insertProfileOnlinePerServer");
		if (psPerServer == null) {
			String cql =
					"INSERT INTO profiles_online_per_server "
						+ " (user_id, time_created, server_id, session_id, start_session) "
						+ " VALUES (?, now(), ?, ?, ?) USING TTL " + PROFILES_ONLINE_TTL_PERIOD + ";";

			psPerServer = session.prepare(cql);
			methodsPreparedStatement.put("insertProfileOnlinePerServer", psPerServer);
		}
		BatchStatement batch = new BatchStatement();
		batch.add(psPerUser.bind(userId, serverId, sessionId, startSession));
		batch.add(psPerServer.bind(userId, serverId, sessionId, startSession));

		if (log.isTraceEnabled()) {
			batch.enableTracing();
		}
		ResultSet rs = session.execute(batch);
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static void closeServerSessions(Session session, String serverId) {
		BoundStatement boundStatement = new BoundStatement(getServerSessionLogPS(session));
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(serverId, getCurrentLimitDate()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}

		Set<String> closedSessions = new HashSet<String>();
		Row row = null;
		while (!rs.isExhausted()) {
			row = rs.one();
			String sessionId = row.getString("session_id");
			if (row.getBool("start_session")) {
				if (!closedSessions.remove(sessionId)) {
					insertProfileOnlineStatus(session, row.getLong("user_id"), serverId, sessionId, false);
				}
			} else {
				closedSessions.add(sessionId);
			}
		}
	}

}
