/**
 *
 */
package com.copyop.ls;

import com.copyop.common.jms.updates.UpdateMessage;

/**
 * @author pavelhe
 *
 */
public interface CopyopFeedListenerIfc {


	void onMessage(UpdateMessage msg);

}
