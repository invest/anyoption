//package il.co.etrader.ls;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.net.InetAddress;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Random;
//import java.util.StringTokenizer;
//import java.util.Vector;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Opportunity;
//import com.lightstreamer.interfaces.data.DataProvider;
//import com.lightstreamer.interfaces.data.DataProviderException;
//import com.lightstreamer.interfaces.data.FailureException;
//import com.lightstreamer.interfaces.data.ItemEventListener;
//import com.lightstreamer.interfaces.data.SubscriptionException;
//import com.reuters.rfa.common.Client;
//import com.reuters.rfa.common.Context;
//import com.reuters.rfa.common.DispatchException;
//import com.reuters.rfa.common.Event;
//import com.reuters.rfa.common.EventQueue;
//import com.reuters.rfa.common.Handle;
//import com.reuters.rfa.common.QualityOfServiceRequest;
//import com.reuters.rfa.common.StandardPrincipalIdentity;
//import com.reuters.rfa.common.TokenizedPrincipalIdentity;
//import com.reuters.rfa.session.MarketDataItemSub;
//import com.reuters.rfa.session.MarketDataSubscriber;
//import com.reuters.rfa.session.MarketDataSubscriberInterestSpec;
//import com.reuters.rfa.session.Session;
//import com.reuters.rfa.session.event.ConnectionEvent;
//import com.reuters.rfa.session.event.EntitlementsAuthenticationEvent;
//import com.reuters.rfa.session.event.MarketDataItemEvent;
//import com.reuters.rfa.session.event.MarketDataSvcEvent;
//import com.reuters.tibmsg.TibException;
//import com.reuters.tibmsg.TibField;
//import com.reuters.tibmsg.TibMsg;
//
//public class ReutersCommandDataAdapter implements DataProvider, Runnable {
//    private static final Logger log = Logger.getLogger(ReutersCommandDataAdapter.class);
//
//    private ItemEventListener listener;
//
//    // Subscription states
//    private final HashMap subscribedItems = new HashMap();
//
//    // Config
//    String sessionName;
//    String serviceName;
//    String userName;
//    ArrayList itemsSupported;
//    
//    //RFA objects
//    protected StandardPrincipalIdentity _standardPI;
//    protected TokenizedPrincipalIdentity _tokenizedPI;
//    protected Session session;
//    protected EventQueue eventQueue;
//    protected MarketDataSubscriber marketDataSubscriber;
//    protected Handle mdsClientHandle;
////    protected Handle itemHandle;
//    protected ETraderClient client = new ETraderClient();
//
//    // ETrader
//    private double dailyAverageInvestmentAmount;
//    
//    public void init(Map params, File configDir) throws DataProviderException {
//        sessionName = (String) params.get("sessionName");
//        serviceName = (String) params.get("serviceName");
//        userName = (String) params.get("userName");
//        StringTokenizer st = new StringTokenizer((String) params.get("itemsList"), ",");
//        itemsSupported = new ArrayList();
//        while (st.hasMoreTokens()) {
//            itemsSupported.add(st.nextToken().trim());
//        }
//        DBUtil.init(params);
//        
//        session = Session.acquire(sessionName);
//        if (null == session) {
//            throw new DataProviderException("Could not acquire session.");
//        }
//
//        //Create an object to receive event callbacks
////        ETraderClient client = new ETraderClient();
//
//        //Create an Event Queue
//        eventQueue = EventQueue.create("etraderEventQueue");
//
//        String defaultPosition = "1.1.1.1/net";
//        try {
//            defaultPosition = InetAddress.getLocalHost().getHostAddress() + "/" + InetAddress.getLocalHost().getHostName();
//        } catch(Exception e) {
//            throw new DataProviderException("Can't prepare position: " + e.toString());
//        }
//        
//        // Create pricipal
//        StandardPrincipalIdentity standardPI = new StandardPrincipalIdentity();
//        standardPI.setName(userName);
//        standardPI.setPosition(defaultPosition);
//        standardPI.setAppName("256");
//        
//        //Create a MarketDataSubscriber event source
//        marketDataSubscriber = session.createMarketDataSubscriber("etraderMarketDataSubscriber", true, standardPI);
//
//        //Register for service and connection status
//        MarketDataSubscriberInterestSpec marketDataSubscriberInterestSpec = new MarketDataSubscriberInterestSpec();
//        marketDataSubscriberInterestSpec.setMarketDataSvcInterest(true);
//        marketDataSubscriberInterestSpec.setConnectionInterest(true);
//        marketDataSubscriberInterestSpec.setEntitlementsInterest(true);
//        mdsClientHandle = marketDataSubscriber.registerClient(eventQueue, marketDataSubscriberInterestSpec, client, null);
//        
//        Thread dispatcher = new Thread(this);
//        dispatcher.setDaemon(true);
//        dispatcher.start();
//    }
//
//    public void setListener(ItemEventListener listener) {
//        this.listener = listener;
//    }
//
//    public void subscribe(String itemName, boolean needsIterator) throws SubscriptionException, FailureException {
//        if (!itemsSupported.contains(itemName)) {
//            throw new SubscriptionException("Unsupported item: " + itemName);
//        }
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Subscribing for: " + itemName);
//        }
//        if (needsIterator) {
//            // OK, as we will always send HashMap objects
//        }
//        new Thread(new SubscribeHelper(itemName)).start();
//    }
//
//    public boolean isSnapshotAvailable(String itemName) throws SubscriptionException {
//        return true;
//    }
//
//    public void unsubscribe(String itemName) throws SubscriptionException, FailureException {
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Unsubscribing from " + itemName);
//        }
//        new Thread(new UnsubscribeHelper(itemName)).start();
//    }
//
//    public void run() {
//        //Dispatch item events
//        while (true) {
//            try {
//                eventQueue.dispatch(100);
//            } catch (DispatchException de) {
//                log.log(Level.ERROR, "Queue deactivated", de);
//                break;
//            }
//        }
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.WARN, Context.string());
//            log.log(Level.WARN, "Daemon " + getClass().toString() + " exiting");
//        }
//    }
//    
//    public void cleanup() {
//        marketDataSubscriber.unsubscribeAll();
//        marketDataSubscriber.unregisterClient(mdsClientHandle);
//        marketDataSubscriber.destroy();
//        eventQueue.deactivate();
//        session.release();
//        Context.uninitialize();
//    }
//
//    /**
//     * Check if the current tiem has BID/ASK fields or has TRDPRC_1 field.
//     * 
//     * @param itemName the item name to check for
//     * @return <code>true</code> if for this item we have BID/ASK fields else <code>false</code>.
//     */
//    private boolean isBidAsk(String itemName) {
//        if (itemName.startsWith(".") || itemName.equals("TEVA.TA") || itemName.equals("POLI.TA") || itemName.equals("AFIL01.TA")) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//    
//    class ETraderClient implements Client {
//        private DecimalFormat numberFormat2 = new DecimalFormat("#,##0.00");
//        private DecimalFormat numberFormat3 = new DecimalFormat("#,##0.000");
//        private DecimalFormat numberFormat5 = new DecimalFormat("#,##0.00000");
//        private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
//        private DecimalFormat percentFormat = new DecimalFormat("#0%");
//        
//        private Random random = new Random();
//        
//        public void processEvent(Event event) {
//            switch (event.getType()) {
//            case Event.MARKET_DATA_ITEM_EVENT:
//                processMarketDataItemEvent((MarketDataItemEvent) event);
//                break;
//            case Event.MARKET_DATA_SVC_EVENT:
//                processMarketDataSvcEvent((MarketDataSvcEvent) event);
//                break;
//            case Event.CONNECTION_EVENT:
//                processConnectionEvent((ConnectionEvent) event);
//                break;
//            case Event.COMPLETION_EVENT:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Received COMPLETION_EVENT for handle " + event.getHandle());
//                }
//                break;
//            case Event.ENTITLEMENTS_AUTHENTICATION_EVENT:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Received ENTITLEMENTS_AUTHENTICATION_EVENT: " + ((EntitlementsAuthenticationEvent)event).getStatus());
//                }
//                break;
//            default:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Unhandled event type: " + event.getType());
//                }
//                break;
//            }
//        }
//
//        protected void processMarketDataSvcEvent(MarketDataSvcEvent marketDataSvcEvent) {
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Received MARKET_DATA_SVC_EVENT: " + marketDataSvcEvent);
//            }
//        }
//
//        protected void processConnectionEvent(ConnectionEvent connectionEvent) {
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Received CONNECTION_EVENT: " + connectionEvent.getConnectionName() + " " + connectionEvent.getConnectionStatus().toString());
//            }
//        }
//
//        protected void processMarketDataItemEvent(MarketDataItemEvent marketDataItemEvent) {
//            String itemName = marketDataItemEvent.getItemName();
//            if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.IMAGE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.UNSOLICITED_IMAGE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.UPDATE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.CORRECTION ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.CLOSING_RUN) {
//                byte[] data = marketDataItemEvent.getData();
//                int length = (data != null ? data.length : 0);
//
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (data), service = " +
//                            marketDataItemEvent.getServiceName() + ", dataFormat = " +
//                            marketDataItemEvent.getDataFormat()+ ", item = " +
//                            itemName + ", status = " +
//                            marketDataItemEvent.getStatus() + ", datasize = " +
//                            length);
//                }
//                if (length == 0) {
//                    return;
//                }
//                synchronized (subscribedItems) {
//                    if (subscribedItems.containsKey(itemName)) {
//                        TibMsg tibMsg = new TibMsg();
//                        try {
//                            tibMsg.UnPack(data);
//                            if (log.isEnabledFor(Level.DEBUG)) {
//                                String msg = null;
//                                try {
//                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                    OutputStreamWriter osw = new OutputStreamWriter(baos);
//                                    PrintWriter pw = new PrintWriter(osw);
//                                    tibMsg.Print(pw);
//                                    pw.flush();
//                                    msg = new String(baos.toByteArray());
//                                    pw.close();
//                                    osw.close();
//                                    baos.close();
//                                } catch (Exception e) {
//                                    log.log(Level.ERROR, "Can't get the message.", e);
//                                }
//                                log.log(Level.DEBUG, msg);
//                            }
//                            HashMap fields = new HashMap();
//                            TibField field = new TibField();
//                            int err = field.First(tibMsg);
//                            while (err == TibMsg.TIBMSG_OK) {
//                                fields.put(field.Name(), field.Data());
//                                err = field.Next();
//                            }
////                            if (log.isEnabledFor(Level.DEBUG)) {
////                                String ls = System.getProperty("line.separator");
////                                StringBuffer sb = new StringBuffer(ls);
////                                for (Iterator i = fields.keySet().iterator(); i.hasNext();) {
////                                    String key = (String) i.next();
////                                    sb.append(key).append(" ").append(fields.get(key)).append(ls);
////                                }
////                                log.log(Level.DEBUG, sb.toString());
////                                for (Iterator i = fields.keySet().iterator(); i.hasNext();) {
////                                    String key = (String) i.next();
////                                    log.log(Level.DEBUG, key + " " + fields.get(key));
////                                }
////                            }
//                            if (hasLevel(itemName, fields)) {
//                                ItemHelper helper = (ItemHelper) subscribedItems.get(itemName);
//                                boolean isSnapshot = !helper.isSnapshotReceived();
//                                for (int i = 0; i < helper.getOppsCount(); i++) {
//                                    HashMap flds = new HashMap();
//                                    OpportunityWrapper w = helper.getOpp(i);
//                                    if (isSnapshot) {
//                                        helper.setSnapshotReceived(true);
//                                        flds.put("ET_NAME", w.getOpportunity().getMarket().getDisplayName());
//                                        flds.put("ET_ODDS", formatOdds(w.getOpportunity().getOddsType().getOverOddsWin()));
//                                        flds.put("ET_EST_CLOSE", formatDate(w.getOpportunity().getTimeEstClosing()));
//                                        flds.put("ET_OPP_ID", String.valueOf(w.getOpportunity().getId()));
//                                        flds.put("ET_ODDS_WIN", formatFloat(1 + w.getOpportunity().getOddsType().getOverOddsWin()));
//                                        flds.put("ET_ODDS_LOSE", formatFloat(1 - w.getOpportunity().getOddsType().getOverOddsLose()));
//                                        flds.put("command", "ADD");
//                                    } else {
//                                        flds.put("command", "UPDATE");
//                                    }
//                                    flds.put("key", "opp" + w.getOpportunity().getId());
//                                    flds.put("ET_LEVEL", level(itemName, w.getOpportunity(), fields));
//                                    if (w.isHome()) {
//                                        listener.update("home", flds, false);
//                                    }
//                                    if (w.isInvest()) {
//                                        listener.update("invest", flds, false);
//                                    }
//                                }
//                            }
//                        } catch (TibException te) {
//                            log.log(Level.ERROR, "Unable to unpack data with TibMsg. Displaying hex.");
//                        }
//                    }
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.STATUS) {
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (status), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            itemName + ", status = " +
//                            marketDataItemEvent.getStatus().toString());
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.GROUP_CHANGE) {
//                // GROUP_CHANGE
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT( group_merge ), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            itemName + ", new group ID = " +
//                            marketDataItemEvent.getGroupID());
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.RENAME) {
//                // RENAME
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT( rename ), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            itemName + ", new item name = " +
//                            marketDataItemEvent.getNewItemName());
//                }
//            } else {
//                //PERMISSION_DATA,
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (misc), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            itemName);
//                }
//            }
//        }
//        
//        private String formatValue(String itemName, Object obj) {
//            if (obj instanceof String) {
//                return (String) obj;
//            } else if (obj instanceof Double) {
//                if (itemName.equals("TEVA.TA") || itemName.equals("POLI.TA") || itemName.equals("AFIL01.TA")) {
//                    return numberFormat2.format(((Double) obj).doubleValue());
//                } else if (itemName.endsWith("=") && !itemName.equals("JPY=")) {
//                    return numberFormat5.format(((Double) obj).doubleValue());
//                } else {
//                    return numberFormat3.format(((Double) obj).doubleValue());
//                }
//            } else {
//                return obj.toString();
//            }
//        }
//        
//        private String formatFloat(float val) {
//            return numberFormat2.format(val);
//        }
//        
//        private String formatDate(Date val) {
//            return dateFormat.format(val);
//        }
//        
//        private String formatOdds(float val) {
//            return percentFormat.format(val);
//        }
//        
//        private boolean hasLevel(String itemName, HashMap fields) {
//            if (isBidAsk(itemName)) {
//                return null != fields.get("BID");
//            } else {
//                return null != fields.get("TRDPRC_1");
//            }
//        }
//        
//        private String level(String itemName, Opportunity opp, HashMap fields) {
//            Double formulaResult = (Double) formula(itemName, fields);
//            Double exposureResult = exposure(itemName, opp, formulaResult);
//            return formatValue(itemName, exposureResult);
//        }
//        
//        private Object formula(String itemName, HashMap fields) {
//            if (itemName.equals(".TA25")) {
//                return fields.get("TRDPRC_1");
//            } else if (itemName.startsWith(".") || itemName.equals("TEVA.TA") || itemName.equals("POLI.TA") || itemName.equals("AFIL01.TA")) {
//                return formulaRandomChange(((Double) fields.get("TRDPRC_1")).doubleValue(), 0.0001, 0.0007);
//            } else if (itemName.equals("ILS=")) {
//                return fields.get("BID");
//            } else if (itemName.endsWith("=")) {
//                Double avg = formulaCurrencies(((Double) fields.get("BID")).doubleValue());
//                return formulaRandomChange(avg.doubleValue(), -0.00025, 0.00025);
//            } else {
//                // only the oil left
//                return fields.get("BID");
//            }
////            if (itemName.startsWith(".")) {
////                return fields.get("TRDPRC_1");
////            } else {
////                return fields.get("BID");
////            }
//        }
//        
//        /**
//         * Randomly modify the level with % in specified interval. For example
//         * if you want to increase the level with 0.01% to 0.07% call with
//         * level, 0.0001 and 0.0007.
//         * 
//         * @param level the level to change
//         * @param min the change interval start value
//         * @param max the change interval end value
//         * @return
//         */
//        private Double formulaRandomChange(double level, double min, double max) {
//            // randomly choose a value in the spec interval to change the level with
//            double rnd = random.nextDouble() * (max - min) + min;
//            return new Double(level * (1 + rnd));
//        }
//        
//        private Double formulaCurrencies(double level) {
//            return new Double(level);
//        }
//        
//        private Double exposure(String itemName, Opportunity opp, Double formulaResult) {
//            return null;
//        }
//    }
//
//    /**
//     * Helper class to keep the item snapshot state and handle and opportunities.
//     * 
//     * @author Tony
//     */
//    class ItemHelper {
//        private boolean snapshotReceived;
//        private Handle handle;
//        private ArrayList opportunities;
//        
//        public ItemHelper(boolean snapshotReceived, Handle handle) {
//            this.snapshotReceived = snapshotReceived;
//            this.handle = handle;
//            opportunities = new ArrayList();
//        }
//        
//        /**
//         * @return the snapshotReceived
//         */
//        public boolean isSnapshotReceived() {
//            return snapshotReceived;
//        }
//        
//        /**
//         * @param snapshotReceived the snapshotReceived to set
//         */
//        public void setSnapshotReceived(boolean snapshotReceived) {
//            this.snapshotReceived = snapshotReceived;
//        }
//
//        /**
//         * @return the handle
//         */
//        public Handle getHandle() {
//            return handle;
//        }
//
//        /**
//         * @param handle the handle to set
//         */
//        public void setHandle(Handle handle) {
//            this.handle = handle;
//        }
//        
//        /**
//         * Add opportunity to the RICs list. If the opp already exist
//         * modify its flags for which "COMMAND" items it is subscribed
//         * (home/invest).
//         * 
//         * @param opp
//         * @param home
//         */
//        public void addOpportunity(Opportunity opp, boolean home) {
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Adding opp: " + opp.getMarket().getDisplayName() + " Time: " + opp.getTimeEstClosing() + " Item: " + home);
//            }
//            OpportunityWrapper w = getOppById(opp.getId());
//            if (null != w) {
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Existing");
//                }
//                if (home) {
//                    w.setHome(true);
//                } else {
//                    w.setInvest(true);
//                }
//            } else {
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "New");
//                }
//                opportunities.add(new OpportunityWrapper(opp, home, !home));
//            }
//        }
//        
//        /**
//         * Get opportunity with specified id from the opps list.
//         * 
//         * @param id the id to look for
//         * @return <code>OpportunityWrapper</code> of the opp with specified id or null if not in the list.
//         */
//        private OpportunityWrapper getOppById(long id) {
//            OpportunityWrapper w = null;
//            for (int i = 0; i < opportunities.size(); i++) {
//                w = (OpportunityWrapper) opportunities.get(i);
//                if (w.getOpportunity().getId() == id) {
//                    return w;
//                }
//            }
//            return null;
//        }
//        
//        /**
//         * @return The count of opps connected to this item.
//         */
//        private int getOppsCount() {
//            return opportunities.size();
//        }
//        
//        /**
//         * The i-th opp connected to this item.
//         * 
//         * @param i the index of the opp to return
//         * @return <code>OpportunityWrapper</code> of the i-th opp of this item.
//         */
//        private OpportunityWrapper getOpp(int i) {
//            return (OpportunityWrapper) opportunities.get(i);
//        }
//        
//        /**
//         * Remove the opportunities for this tiem from specified subscribtion (home/invest).
//         * 
//         * @param home the type of subscribtion to remove (home/invest)
//         */
//        public void removeOpportunities(boolean home) {
//            OpportunityWrapper w = null;
//            int i = 0;
//            while (i < opportunities.size()) {
//                w = (OpportunityWrapper) opportunities.get(i);
//                if ((home && !w.isInvest()) || (!home && !w.isHome())) {
//                    opportunities.remove(i);
//                } else {
//                    if (home) {
//                        w.setHome(false);
//                    } else {
//                        w.setInvest(false);
//                    }
//                    i++;
//                }
//            }
//        }
//        
//        /**
//         * @return If the item has any opportunities left.
//         */
//        public boolean hasOpportunities() {
//            return opportunities.size() > 0;
//        }
//    }
//    
//    class OpportunityWrapper {
//        private boolean home;
//        private boolean invest;
//        private Opportunity opportunity;
//        
//        public OpportunityWrapper(Opportunity opp, boolean home, boolean invest) {
//            this.opportunity = opp;
//            this.home = home;
//            this.invest = invest;
//        }
//
//        /**
//         * @return the home
//         */
//        public boolean isHome() {
//            return home;
//        }
//
//        /**
//         * @param home the home to set
//         */
//        public void setHome(boolean home) {
//            this.home = home;
//        }
//
//        /**
//         * @return the invest
//         */
//        public boolean isInvest() {
//            return invest;
//        }
//
//        /**
//         * @param invest the invest to set
//         */
//        public void setInvest(boolean invest) {
//            this.invest = invest;
//        }
//
//        /**
//         * @return the opportunity
//         */
//        public Opportunity getOpportunity() {
//            return opportunity;
//        }
//
//        /**
//         * @param opportunity the opportunity to set
//         */
//        public void setOpportunity(Opportunity opportunity) {
//            this.opportunity = opportunity;
//        }
//    }
//    
//    class SubscribeHelper implements Runnable {
//        private String itemName;
//        private QualityOfServiceRequest qosr;
//        
//        private Vector trdPrc = null;
//        private Vector bidAsk = null;
//        
//        public SubscribeHelper(String itemName) {
//            this.itemName = itemName;
//
//            // Set the QOS request information
//            qosr = new QualityOfServiceRequest();
//            qosr.setBestRate(0);
//            qosr.setBestTimeliness(0);
//            qosr.setWorstRate(2147483647);
//            qosr.setWorstTimeliness(2147483647);
////            if (dynamicStream) {
////                 qosr.setStreamProperty(QualityOfServiceRequest.DYNAMIC_STREAM);
////            }
//            
//            trdPrc = new Vector();
//            trdPrc.add("TRDPRC_1");
//            trdPrc.add("HST_CLOSE");
//            
//            bidAsk = new Vector();
//            bidAsk.add("BID");
//            bidAsk.add("ASK");
//        }
//        
//        public void run() {
//            if (log.isEnabledFor(Level.INFO)) {
//                log.log(Level.INFO, "Subscribing to " + itemName);
//            }
//            boolean home = itemName.equals("home");
//            synchronized (subscribedItems) {
//                try {
////                    OpportunitiesManager.getJustFun();
//                    ArrayList opps = null;
//                    if (home) {
////                        opps = OpportunitiesManager.getMostPopular();
//                    } else {
////                        opps = OpportunitiesManager.getInvestment();
//                    }
//                    
//                    for (int i = 0; i < opps.size(); i++) {
//                        Opportunity opp = (Opportunity) opps.get(i);
//                        if (log.isEnabledFor(Level.DEBUG)) {
//                            log.log(Level.DEBUG, "Subscribtion helper processing: " + opp.getMarket().getFeedName());
//                        }
//                        ItemHelper ih = (ItemHelper) subscribedItems.get(opp.getMarket().getFeedName());
//                        if (null == ih) { // no subscribtion to Reuters for this RIC yet
//                            if (log.isEnabledFor(Level.DEBUG)) {
//                                log.log(Level.DEBUG, "Connecting to reuters: " + opp.getMarket().getFeedName());
//                            }
//                            MarketDataItemSub marketDataItemSub = new MarketDataItemSub();
//                            marketDataItemSub.setItemName(opp.getMarket().getFeedName());
//                            marketDataItemSub.setServiceName(serviceName);
//                            marketDataItemSub.setRequestedQualityOfService(qosr);
//                            marketDataItemSub.setFieldNames(getFieldNames(opp.getMarket().getFeedName()));
//                
//                            Handle itemHandle = marketDataSubscriber.subscribe(eventQueue, marketDataItemSub, client, null);
//                            ih = new ItemHelper(false, itemHandle);
//                            subscribedItems.put(opp.getMarket().getFeedName(), ih);
//                        }
//                        ih.addOpportunity(opp, home);
//                    }
//                } catch (Throwable t) {
//                    log.log(Level.FATAL, "Can't process subscribtion.", t);
//                }
//            }
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Subscribtion done");
//            }
//        }
//        
//        private Vector getFieldNames(String itemName) {
//            if (isBidAsk(itemName)) {
//                return bidAsk;
//            } else {
//                return trdPrc;
//            }
//        }
//    }
//    
//    class UnsubscribeHelper implements Runnable {
//        private String itemName;
//        
//        public UnsubscribeHelper(String itemName) {
//            this.itemName = itemName;
//        }
//        
//        public void run() {
//            ItemHelper helper = null;
//            boolean home = itemName.equals("home");
//            synchronized (subscribedItems) {
//                for (Iterator i = subscribedItems.keySet().iterator(); i.hasNext();) {
//                    Object key = i.next();
//                    try {
//                        helper = (ItemHelper) subscribedItems.get(key);
//                        helper.removeOpportunities(home);
//                        if (!helper.hasOpportunities()) {
//                            if (log.isEnabledFor(Level.DEBUG)) {
//                                log.log(Level.DEBUG, "Unsubscribe helper removing: " + key);
//                            }
//                            marketDataSubscriber.unsubscribe(helper.getHandle());
//                            i.remove();
//                        }
//                    } catch (Throwable t) {
//                        log.log(Level.ERROR, "Problem.", t);
//                    }
//                }
//            }
//        }
//    }
//}