package il.co.etrader.ls.bl_managers;

import il.co.etrader.dao_managers.UrlsDAOBase;

import java.sql.Connection;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.ApiDAOBase;
import com.anyoption.common.util.Sha1;

/**
 * @author EyalG
 *
 */
public class ApiManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(ApiManager.class);

    /**
     * check if api user and password are correct
     * @param userName
     * @param password
     * @throws Exception
     */
    public static boolean validateAPIUser(String userName, String password) {
    	Connection con = null;
        try {
        	con = getConnection();
        	String pass = ApiDAOBase.validateAPIUser(con, userName, password);
        	log.info("LS pass = " + password + " db pass = " + pass + " db sha1 pass = " + Sha1.encode(pass));
        	if (null != pass) {
        		return Sha1.encode(pass).equals(password);
            }
        } catch (Exception e) {
            log.error("Can't check user for qualification.", e);
        } finally {
            closeConnection(con);
        }
        return false;
    }

    public static ArrayList<String> getOurDomains() {
    	ArrayList<String> domains = null;
    	Connection con = null;
        try {
        	con = getConnection();
        	domains = UrlsDAOBase.getOurDomains(con);
        } catch (Exception e) {
            log.error("Can't get our domains.", e);
        } finally {
            closeConnection(con);
        }
        if (null == domains || domains.isEmpty()) {
        	log.warn("Can't get our domains. useing default: etrader, anyoption, 168qiquan");
        	domains = new ArrayList<String>();
        	domains.add("anyoption");
        	domains.add("etrader");
        	domains.add("168qiquan");
        }
        return domains;
    }


}