package il.co.etrader.ls.bl_managers;

import il.co.etrader.ls.DBUtil;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public abstract class BaseBLManager {
    private static final Logger logger = Logger.getLogger(BaseBLManager.class);

    protected static Connection getConnection() throws SQLException{
        DataSource ds = DBUtil.getDataSource();
        if (ds == null) {
            logger.error("************************************* ");
            logger.error(" GOT NULL in data source!!!!!!!!      ");
            logger.error("************************************* ");
        }

        Connection conn = ds.getConnection();
        if (conn == null) {
            logger.error("************************************* ");
            logger.error(" GOT NULL in Connection!!!!!!!!       ");
            logger.error("************************************* ");
        }
        return conn;
    }

    protected static void closeConnection(final Connection c) {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException ex) {
                logger.error(c, ex);
            }
        }
    }
}