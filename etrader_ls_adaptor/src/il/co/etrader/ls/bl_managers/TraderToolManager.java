//package il.co.etrader.ls.bl_managers;
//
//import il.co.etrader.ls.dao_managers.TraderToolDAO;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
///**
// * @author Eyal
// */
//public class TraderToolManager extends BaseBLManager {
//	public static ArrayList<Integer> loadPublishedOpportunities() throws SQLException {
//		Connection con = getConnection();
//		try {
//			return TraderToolDAO.getPublishedOpportunities(con);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//	public static OpportunitiesData getOppDataById(int oppId) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return TraderToolDAO.getOppDataById(con, oppId);
//		} finally {
//			closeConnection(con);
//		}
//	}
//}