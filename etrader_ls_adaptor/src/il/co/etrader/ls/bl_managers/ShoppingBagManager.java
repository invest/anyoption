package il.co.etrader.ls.bl_managers;

import il.co.etrader.ls.dao_managers.ShoppingBagDAO;
import il.co.etrader.ls.shopping_bag.user.UserOpportunityWrapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

public class ShoppingBagManager extends BaseBLManager {
    public static final Logger log = Logger.getLogger(ShoppingBagManager.class);

    /**
     * Checks if the given users have done investments in all opportunities with
     * the given opportunity closing date. If so, a set of opportunity IDs is
     * created for each user.
     *
     * @param users
     * @param timeEstClosing
     * @return a map with key user ID and value - a set of opportunity IDs in
     * which each user has done investments.
     * @throws SQLException
     */
    public static Map<Long, Set<Long>> getUserInvestmentsOpp(List<Long> users, Date timeEstClosing) throws SQLException {
        Connection conn = getConnection();
        try {
            return  ShoppingBagDAO.getUserInvestmentsOpp(conn, users, timeEstClosing);
        } finally {
        	conn.setAutoCommit(true);
            closeConnection(conn);
        }
    }

    /**
     * Collects all users opportunities amounts of return in
     * <code>shoppingBag</code> parameter.
     *
     * @param users
     * @param opportunities
     * @param shoppingBag
     * @throws SQLException
     */
    public static void getUserShopingBagData(Collection<Long> users, Collection<Long> opportunities, Map<Long, Map<Long, UserOpportunityWrapper>> shoppingBag) throws SQLException{
        Connection conn = getConnection();
        try {
            ShoppingBagDAO.getUserShopingBagData(conn, users, opportunities, shoppingBag);
        } finally {
        	conn.setAutoCommit(true);
            closeConnection(conn);
        }
    }
}