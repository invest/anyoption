package il.co.etrader.ls.bl_managers;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.UsersDAOBase;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.util.Sha1;

public class UsersManager extends BaseBLManager {
    public static final Logger log = Logger.getLogger(UsersManager.class);

    public static UserBase getUserByName(String userName) throws SQLException {
        UserBase user = new UserBase();
        Connection conn = getConnection();
        try {
            UsersDAOBase.getByUserName(conn, userName, user, true);
        } finally {
            closeConnection(conn);
        }
        return user;
    }

    public static boolean isUserQualified(String userName, String password) {
        boolean qualified = false;
        try {
            UserBase u = getUserByName(userName);
            qualified = isUserQualified(u, password);
        } catch (Exception e) {
            log.error("Can't check user for qualification.", e);
        }
        return qualified;
    }


    public static boolean isUserQualified(UserBase user, String password) {
        boolean qualified = false;
        try {
            if (null != user && null != user.getPassword()) {
                qualified = Sha1.encode(user.getPassword()).equals(password);
                if (!qualified) {
            		log.debug("User [" + user.getUserName() + "] password does not match");
                }
            } else {
            	log.info("Can't find user for qualification. userName " + user.getUserName() + " password " + password);
            }
        } catch (Exception e) {
            log.error("Can't check user for qualification.", e);
        }
        return qualified;
    }
}