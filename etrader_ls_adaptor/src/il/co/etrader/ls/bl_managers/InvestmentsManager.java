package il.co.etrader.ls.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.common.beans.Investment;


import il.co.etrader.ls.dao_managers.InvestmentsDAO;

public class InvestmentsManager extends BaseBLManager {
    public static ArrayList<Investment> getUserHourInvestments(String userName, int goldenMinStart, int goldenMinEnd) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = getConnection();
        try {
            l = InvestmentsDAO.getUserHourInvestments(conn, userName, goldenMinStart, goldenMinEnd);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static HashMap<String, Integer> getInsuranceTime() throws SQLException {
        HashMap<String, Integer> l = null;
        Connection conn = getConnection();
        try {
            l = InvestmentsDAO.getInsuranceTime(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static boolean updateInvSeen(long invId) throws SQLException {
        boolean isDone = false;
        Connection conn = getConnection();
        try {
            isDone = InvestmentsDAO.updateInvSeen(conn, invId);
        } finally {
            closeConnection(conn);
        }
        return isDone;
    }
}