package il.co.etrader.ls;

import java.io.File;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.lightstreamer.interfaces.metadata.CreditsException;
import com.lightstreamer.interfaces.metadata.ItemsException;
import com.lightstreamer.interfaces.metadata.MetadataProviderAdapter;
import com.lightstreamer.interfaces.metadata.MetadataProviderException;
import com.lightstreamer.interfaces.metadata.NotificationException;
import com.lightstreamer.interfaces.metadata.SchemaException;

public class TraderToolMetadataAdapter extends MetadataProviderAdapter {
    private Logger log;



    /**
     * Reads configuration settings for user and resource constraints.
     * If some setting is missing, the corresponding constraint is not set.
     *
     * @param  params  Can contain the configuration settings.
     * @param  dir  Not used.
     * @throws MetadataProviderException in case of configuration errors.
     */
    public void init(Map params, File dir) throws MetadataProviderException {
        String logConfig = (String) params.get("log_config");
        if (logConfig != null) {
            File logConfigFile = new File(dir, logConfig);
            String logRefresh = (String) params.get("log_config_refresh_seconds");
            if (logRefresh != null) {
                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
            } else {
                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
            }
        }
        log = Logger.getLogger(TraderToolMetadataAdapter.class);


        log.debug("Pool initialized.");
    }


    public void notifyUserMessage(String user, String session, String message) throws CreditsException, NotificationException {

        log.debug("notifyUserMessage - user: " + user + " session: " + session + " message: " + message);
        if (null != message) {
        	String[] messageArr = message.split("_");
        	if (messageArr.length > 1) {
        		TraderToolAdapter.getInstance().sendDeleteCommand(Long.valueOf(messageArr[0]), messageArr[1]);
        	}
        }
    }


    private String[] tokenize(String str) {
        StringTokenizer source = new StringTokenizer(str, " ");
        int dim = source.countTokens();
        String[] ret = new String[dim];

        for (int i = 0; i < dim; i++) {
            ret[i] = source.nextToken();
        }
        return ret;
    }

    /**
     * Resolves a Group name supplied in a Request. The names of the Items
     * in the Group are returned.
     *
     * @param user A User name.
     * @param session A Session name. Not used.
     * @param id A Group name.
     * @return An array with the names of the Items in the Group.
     * @throws ItemsException
     */
    public String[] getItems(String user, String session, String id) throws ItemsException {
        log.debug("getItems - id: " + id);
        return tokenize(id);
    }

    /**
     * Resolves a Schema name supplied in a Request. The names of the Fields
     * in the Schema are returned.
     *
     * @param user A User name.
     * @param session A Session name. Not used.
     * @param id The name of the Group whose Items the Schema is to be applied
     * to.
     * @param schema A Schema name.
     * @return An array with the names of the Fields in the Schema.
     * @throws SchemaException
     */
    public String[] getSchema(String user, String session, String id, String schema) throws SchemaException {
        log.debug("getSchema - id: " + id + " schema: " + schema);
        return tokenize(schema);
    }

}