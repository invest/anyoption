package il.co.etrader.ls;

import java.io.File;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.LiveJMSMessage;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.ifc.LevelsCacheListener;
import com.anyoption.common.jms.msgs.SettledOpportunitiesMessage;
import com.anyoption.common.jmx.DbJMXMonitor;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;
import com.lightstreamer.interfaces.data.DataProvider;
import com.lightstreamer.interfaces.data.DataProviderException;
import com.lightstreamer.interfaces.data.FailureException;
import com.lightstreamer.interfaces.data.ItemEventListener;
import com.lightstreamer.interfaces.data.SubscriptionException;

import il.co.etrader.ls.bl_managers.InvestmentsManager;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.ShoppingBagCacheListener;
import il.co.etrader.util.ConstantsBase;

public class JMSCommandDataAdapter implements DataProvider, LevelsCacheListener, ShoppingBagCacheListener {
    protected Logger log;

    private static final int ITEM_TYPE_INVALID = 0;
    private static final int ITEM_TYPE_HOME = 1;
    private static final int ITEM_TYPE_GROUP = 2;
    private static final int ITEM_TYPE_AOTP = 3;
    private static final int ITEM_TYPE_AOCTRL = 4;
    private static final int ITEM_TYPE_AOSTATE = 5;
    private static final int ITEM_TYPE_INSURANCES = 6;
    private static final int ITEM_TYPE_OPTION_PLUS = 7;
    private static final int ITEM_TYPE_BINARY_ZERO_ONE_HUNDRED = 8;
    private static final int ITEM_TYPE_LIVE_GLOBE = 9;
    private static final int ITEM_TYPE_LIVE_INVESTMENTS = 10;
    private static final int ITEM_TYPE_LIVE_TRENDS = 11;
    private static final int ITEM_TYPE_SHOPPING_BAG = 12;

    private ItemEventListener listener;

    private HashMap<Integer, Boolean> home;
    private HashMap<Integer, HashMap<Integer, Boolean>> groups;
    private HashMap<String, Boolean> aoSubscriptions;
    private boolean aoState;
    private HashMap<String, Boolean> optionPlusSubscriptions;
    private HashMap<String, Boolean> binaryZeroOneHundredSubscriptions;
    private boolean liveGlobeSubscribed;
    private Map<String, Boolean> liveInvestmentsSubscriptions;
    private Map<String, Boolean> liveTrendsSubscriptions;

    protected List<String> users;
    protected List<String> usersQueue;
    // standard golden minutes
    private int goldenMinStart;
    private int goldenMinEnd;
    // additinal RF
    private int goldenMin2Start;
    private int goldenMin2End;
    
    private Hashtable<String, TreeSet<Investment>> userInvestments;
    private Hashtable<Long, List<String>> opportunityUsers;
    private static InvestmentsInTheMoneyComparator invComparator = new InvestmentsInTheMoneyComparator();
    private GoldenMinutesTicker gmTicker;
    private HashMap<Long, Double> oppsGMStartCurrentLevel;
    protected ShoppingBagCache shoppingBagCache;

    private LevelsCache levelsCache;
    private static JMSCommandDataAdapter instance;
    
    private DbJMXMonitor dbMon;

    public JMSCommandDataAdapter() {
        instance = this;
        users = new LinkedList<String>();
        usersQueue = new LinkedList<String>();
        userInvestments = new Hashtable<String, TreeSet<Investment>>();
        opportunityUsers = new Hashtable<Long, List<String>>();
        oppsGMStartCurrentLevel = new HashMap<Long, Double>();
        shoppingBagCache = new ShoppingBagCache(this);
    }

    public static JMSCommandDataAdapter getInstance() {
        return instance;
    }

    /*
     * called by Lightstreamer Kernel on start
     */
    @Override
	public void init(Map params, File dir) throws DataProviderException {
        //load configuration
        String logConfig = (String) params.get("log_config");
        if (logConfig != null) {
            File logConfigFile = new File(dir, logConfig);
            String logRefresh = (String) params.get("log_config_refresh_seconds");
            if (logRefresh != null) {
                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
            } else {
                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
            }
        }
        log = Logger.getLogger(JMSCommandDataAdapter.class);
        log.info("Init anyoption data adapter");
        
        //load JMS connections parameters
        String providerURL = getParam(params, "jmsUrl", true, null);
        String initialContextFactory = getParam(params, "initialContextFactory", true, null);
        String connectionFactory = getParam(params, "connectionFactory", true, null);
        String topic = getParam(params, "topicName", true, null);
        String queue = getParam(params, "queueName", true, null);
        //the size of the message pool
        int msgPoolSize = getParam(params, "msgPoolSize", false, 15);
        //in case of disconnection/failed_connection from/to JMS this is
        //the pause between each reconnection attempt
        int recoveryPause = getParam(params, "recoveryPauseMillis", false, 2000);
        String jmxName = getParam(params, "jmxName", true, null);

        try {
            HashMap<String, Integer> a = InvestmentsManager.getInsuranceTime();
            goldenMinStart	= a.get("insurance_period_start_time");
            goldenMinEnd	= a.get("insurance_period_end_time");
            goldenMin2Start	= a.get("insurance_period_2_start_time");
            goldenMin2End	= a.get("insurance_period_2_end_time");
            
            log.info("loaded start and end time for standard insurance. start " + goldenMinStart + " , end " + goldenMinEnd);
            log.info("loaded start and end time for additional insurance. start " + goldenMin2Start + " , end " + goldenMin2End);
        } catch (SQLException e) {
            log.info("cant load start and end time for insurance. setting defaults to start 30, end 25", e);
            goldenMinStart = 30;
            goldenMinEnd = 15;
        }
        log.debug("Configuration read.");

        home = new HashMap<Integer, Boolean>();
        // the current group ids are 2 to 5 so pad the array with 0 and 1 so we can use
        // the ids as array index
        groups = new HashMap<Integer, HashMap<Integer, Boolean>>();

        aoSubscriptions = new HashMap<String, Boolean>();
        optionPlusSubscriptions = new HashMap<String, Boolean>();

        binaryZeroOneHundredSubscriptions = new HashMap<String, Boolean>();

        levelsCache = new LevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, this);
        levelsCache.registerJMX(jmxName);
        Runtime.getRuntime().addShutdownHook(new CleanupHook());

        gmTicker = new GoldenMinutesTicker();
        gmTicker.start();

        liveGlobeSubscribed = false;
        liveInvestmentsSubscriptions = new HashMap<String, Boolean>();
        liveTrendsSubscriptions = new HashMap<String, Boolean>();

        log.info("JMSCommandDataAdapter ready.");
        
		try {
			dbMon = new DbJMXMonitor(DBUtil.getDataSource(), "LightStreamerAO");
			dbMon.registerJMX();
		} catch (SQLException e) {
			log.error("Can't register db monitor", e);
		}
    }

    @Override
	public void setListener(ItemEventListener listener) {
        //this listener will pass updates to Lightstreamer Kernel
        this.listener = listener;
    }

    @Override
	public void update(String itemName, HashMap<String, String> update) {
    	String[] itemNameArray = itemName.split("_"); // array of the item name 0 = home/group; 1 = skinId; 2 = groupid(if its group)
        boolean b = itemNameArray[0].equals("home");
        int groupId = 0;
        if (!b) {
            try {
                // the names of the groups are "groupX" where X is the group id
                groupId = Integer.parseInt(itemNameArray[2]);
            } catch (Throwable t) {
                log.error("Can't parse group id.", t);
            }
        }
        int skinId = -1;
        try {
            // the names of the home/groups are "x_skinId" where skinId is the group/home skinId
            skinId = Integer.parseInt(itemNameArray[1]);
        } catch (Throwable t) {
            log.error("Can't parse skin id.", t);
        }

        if (log.isTraceEnabled()) {
            String ls = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer();
            sb.append(ls).append("home: ").append(home.get(skinId)).append(" b: ").append(b).append(" groupId: ").append(groupId).append(" skinId: ").append(skinId).append(ls);
//            sb.append("itemName: ").append(itemName).append(ls);
//            String keys = null;
//            for (Iterator<String> i = update.keySet().iterator(); i.hasNext();) {
//                keys = i.next();
//                sb.append(keys).append(": ").append(update.get(keys)).append(ls);
//            }
            log.trace(sb.toString());
        }
        if (b && home.containsKey(skinId) ? home.get(skinId) : false) {
            listener.update(itemName, update, false);
            log.trace("sent update to home! skin id: " + skinId);
        }

        // if there is someone that subscribe to this list and to this groupid then send update
        log.trace("sending update to group! skin id: " + skinId + " group " + groupId + " have this skin in groups? " + groups.containsKey(skinId));
        if (groups.containsKey(skinId)) {
        	log.trace("have this skin looking for group? " + groups.get(skinId).containsKey(groupId));
        	if (groups.get(skinId).containsKey(groupId) ? groups.get(skinId).get(groupId) : false) {
	            listener.update(itemName, update, false);
	            log.trace("sent update to group! skin id: " + skinId + " group " + groupId);
        	}
        }
    }

    /**
     * Notify about "COMMAND" mode subscription event (command).
     *
     * @param marketId the id of the market for which this update is for
     * @param scheduled the scheduled for which this updates is for
     * @param update the update
     */
    @Override
	public void aoTPSUpdate(long marketId, int scheduled, HashMap<String, String> update) {
        Boolean aos = aoSubscriptions.get(marketId + "_" + scheduled);
        if (log.isTraceEnabled()) {
            log.trace("aotps - marketId: " + marketId + " scheduled: " + scheduled + " aos: " + aos);
        }
        if (null != aos && aos) {
            if (log.isTraceEnabled()) {
                log.trace("sending update for marketId: " + marketId + " scheduled: " + scheduled);
            }
            listener.update("aotps_" + scheduled + "_" + marketId, update, false);
        }
    }

    /**
     * Notify about "COMMAND" mode subscription event (command).
     *
     * @param skinId the id of the skin for which control connection this update is for
     * @param update the update
     */
    @Override
	public void aoCtrlUpdate(long skinId, HashMap<String, String> update) {
        Boolean aos = aoSubscriptions.get(String.valueOf(skinId));
        if (log.isTraceEnabled()) {
            log.trace("aoctrl - skinId: " + skinId + " aos: " + aos);
        }
        if (null != aos && aos) {
            if (update.get("command").equals("UPDATE")) {
                String ls = System.getProperty("line.separator");
                log.warn("UPDATE command in Ctrl subscription!" + ls + update);
            }
            if (log.isTraceEnabled()) {
                log.trace("sending update for aoctrl skinId: " + skinId + " cmd: " + update.get("command") + " update: " + update);
            }
            listener.update("aoctrl_" + skinId, update, false);
        }
    }

    /**
     * Notify about ao state subscription event (market open/close).
     *
     * @param update
     */
    @Override
	public void aoStateUpdate(HashMap<String, String> update, long marketId) {
        if (aoState) {
            log.info("aostate: " + update);
            listener.update("aostate", update, false);
        }
    }

    /**
     * Notify personal insurances subscriptions about opportunity update. The
     * key entry in the update keep the opportunity id. The update must be
     * cloned before passed to Lightstreamer as Lightstreamer engine add
     * internal fields to the update that prevent it from beeing used in another
     * notification to the engine.
     *
     * @param opportunityId
     * @param update
     */
    @Override
	public void insurancesUpdate(long opportunityId, HashMap<String, String> update, double oppCurrentLevel) {
        List<String> usrs = opportunityUsers.get(opportunityId);
        if (null != usrs) {
            String usr = null;
            synchronized (usrs) {
                for (Iterator<String> i = usrs.iterator(); i.hasNext();) {
                    usr = i.next();

                    Map<SkinGroup, String> insuranceCurrentLevels = new EnumMap<SkinGroup, String>(SkinGroup.class);
                    String currentLevel = null;
                    for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
                    	currentLevel = update.get(skinGroup.getLevelUpdateKey());
						insuranceCurrentLevels.put(skinGroup, currentLevel != null ? currentLevel : "0");
					}
                    updateInsurancesPage(usr, opportunityId, 0, update.get("command"), false, insuranceCurrentLevels);
                }
            }
        }
    }

    /**
     * Option Plus update.
     *
     * @param marketId
     * @param update
     */
    @Override
	public void optionPlusUpdate(long marketId, HashMap<String, String> update) {
        Boolean ops = optionPlusSubscriptions.get(String.valueOf(marketId));
        if (log.isTraceEnabled()) {
            log.trace("optionPlus - marketId: " + marketId + " ops: " + ops);
        }
        if (null != ops && ops) {
            int groupClose = Integer.parseInt(update.get("ET_GROUP_CLOSE"));
            int state = Integer.parseInt(update.get("ET_STATE"));
            if ((groupClose & Opportunity.CLOSING_FILTER_FLAG_CLOSEST) != 0
            		|| state == Opportunity.STATE_SUSPENDED
            		|| state == Opportunity.STATE_CREATED) {
                if (log.isTraceEnabled()) {
                    log.trace("Option Plus update marketId: " + marketId);
                }
                listener.update("op_" + marketId, update, false);
            }
        }
    }

    /**
     * binary Zero One Hundred update.
     *
     * @param marketId
     * @param opportunityTypeId
     * @param update
     */
    @Override
	public void binaryZeroOneHundredUpdate(long marketId, long opportunityTypeId, HashMap<String, String> update) {
        Boolean bzs = binaryZeroOneHundredSubscriptions.get(marketId + "_" + opportunityTypeId);
        if (log.isTraceEnabled()) {
            log.trace("binary Zero One Hundred - marketId: " + marketId + " opportunityTypeId: " + opportunityTypeId + " bzs: " + bzs + " update " + update);
        }
        if (null != bzs && bzs) {
        	listener.update("bz_" + marketId + "_" + opportunityTypeId, update, false);
        }
    }

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    @Override
	public void liveGlobeUpdate(String itemName, Map<String, String> update) {
    	if (log.isTraceEnabled()) {
            log.trace("Received [" + itemName + "] jms message [" + update + "]. LiveGlobeSubscribed [" + liveGlobeSubscribed + "]");
        }
    	if (liveGlobeSubscribed) {
    		listener.update(itemName, update, false);
    	}
    }

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    @Override
	public void liveInvestmentsUpdate(String itemName, Map<String, String> update) {
    	Boolean lis = liveInvestmentsSubscriptions.get(itemName);
    	if (log.isTraceEnabled()) {
            log.trace("Received [" + itemName + "] jms message [" + update + "]. Subscribed for invesment stream [" + lis + "]");
        }
    	if (null != lis && lis) {
    		listener.update(LiveJMSMessage.KEY_INVESTMENTS_PREFIX + "_" + itemName, update, false);
    	}
    }

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    @Override
	public void liveTrendsUpdate(String itemName, Map<String, String> update) {
    	Boolean lis = liveTrendsSubscriptions.get(itemName);
    	if (log.isTraceEnabled()) {
            log.trace("Received [" + itemName + "] jms message [" + update + "]. LiveTrendsSubscribed [" + lis + "]");
        }
    	if (null != lis && lis) {
    		listener.update(itemName, update, false);
    	}
    }

    private void updateInsurancesPage(String usr,
    									long opportunityId,
    									long investmentId,
    									String command,
    									boolean forseRemove,
    									Map<SkinGroup, String> insuranceCurrentLevels) {
        Investment inv = null;
        TreeSet<Investment> ts = userInvestments.get(usr);
        if (forseRemove) {
            log.debug("usr: " + usr + " opportunityId: " + opportunityId + " investmentId: " + investmentId + " command: " + command + " forseRemove: " + forseRemove + " ts.size: " + ts.size());
        }
        long oppId = opportunityId; // in the remove investment case store the opp id
        boolean deleted = false;
        int invPosition = 0;
        for (Iterator<Investment> tsi = ts.iterator(); tsi.hasNext();) {
            inv = tsi.next();
            if ((opportunityId > 0 && inv.getOpportunityId() == opportunityId) ||
                    (investmentId > 0 && inv.getId() == investmentId)) {
  /*              if (!forseRemove) {
                    updateInvestmentCurrentValues(inv);
                }
                if (inv.getCurrentInsuranceQualifyPercent() < inv.getInsuranceQualifyPercent() || forseRemove) { */
                if (forseRemove) {
                    tsi.remove();
                    sendInsurancesUpdate(usr, inv, "DELETE", gmTicker.currentGMType.name());
                    deleted = true;
                    log.debug("updateInsurancesPage - remove - usr: " + usr + " opportunityId : " + opportunityId + " command: " + command + " forseRemove: " + forseRemove);
                    log.debug("investments left after remove: " + ts.size());
                    oppId = inv.getOpportunityId();
                    invPosition = inv.getInsurancePosition(); //if we have more then 5 invesment the 5 investment will get this position.
                } else {
                    inv.setInsuranceCurrentLevels(insuranceCurrentLevels);
                    sendInsurancesUpdate(usr, inv, command, gmTicker.currentGMType.name());
                }
            } else if (deleted && !inv.isShowInsurance()) {
                //TODO should be same position of the 1 that was deleted
                inv.setInsurancePosition(invPosition);
                inv.setShowInsurance(true);
                sendInsurancesUpdate(usr, inv, "ADD", gmTicker.currentGMType.name());
                break;
            }
        }
        if (deleted) {
            boolean noMoreInvOnThisOpp = true;
            for (Iterator<Investment> tsi = ts.iterator(); tsi.hasNext();) {
                inv = tsi.next();
                if (inv.getOpportunityId() == opportunityId) {
                    noMoreInvOnThisOpp = false;
                    break;
                }
            }
            if (noMoreInvOnThisOpp) {
                List<String> usrs = opportunityUsers.get(oppId);
                log.debug("remove user from this opp users");
                if (null != usrs) {
                    synchronized (usrs) {
                        usrs.remove(usr);
                    }
                } else {
                    log.warn("no users for opportunityId: " + oppId);
                }
            }
        }
    }

    private int getItemType(String itemName) {
        if (itemName.matches("home_.+")) {
            return ITEM_TYPE_HOME;
        } else if (itemName.matches("group_.+")) {
            return ITEM_TYPE_GROUP;
        } else if (itemName.startsWith("aotps_")) {
            return ITEM_TYPE_AOTP;
        } else if (itemName.matches("aoctrl_[0-9]+")) {
            return ITEM_TYPE_AOCTRL;
        } else if (itemName.equals("aostate")) {
            return ITEM_TYPE_AOSTATE;
        } else if (itemName.matches("insurances_.+")) {
            return ITEM_TYPE_INSURANCES;
        } else if (itemName.startsWith("op_")) {
            return ITEM_TYPE_OPTION_PLUS;
        } else if (itemName.startsWith("bz_")) {
            return ITEM_TYPE_BINARY_ZERO_ONE_HUNDRED;
        } else if (itemName.equals(LiveJMSMessage.KEY_GLOBE)) {
        	return ITEM_TYPE_LIVE_GLOBE;
        } else if (itemName.startsWith(LiveJMSMessage.KEY_INVESTMENTS_PREFIX)) { // TODO: validate against table names
        	return ITEM_TYPE_LIVE_INVESTMENTS;
        } else if (itemName.startsWith(LiveJMSMessage.KEY_TRENDS)) {
        	return ITEM_TYPE_LIVE_TRENDS;
        } else if (itemName.matches(ShoppingBagCache.ITEM_NAME_MATCH)) {
        	return ITEM_TYPE_SHOPPING_BAG;
        }
        return ITEM_TYPE_INVALID;
    }

    /*
     * called by Lightstreamer Kernel on item subscription
     */
    @Override
	public void subscribe(String itemName, boolean needsIterator) throws SubscriptionException, FailureException {
        if (log.isInfoEnabled()) {
            log.info("Subscribing to " + itemName);
        }
        int itemType = getItemType(itemName);
        if (itemType == ITEM_TYPE_INVALID) {
            log.warn("Subscribe request for invalid subscription - " + itemName);
            throw new SubscriptionException("(Subscribing) Unexpected item: " + itemName);
        }

        if (log.isDebugEnabled()) {
            log.debug("(Subscribing) Valid item: " + itemName);
        }
        String[] itemNameArray = itemName.split("_");
        if (itemType == ITEM_TYPE_HOME || itemType == ITEM_TYPE_GROUP) {
            boolean homePage = itemNameArray[0].equals("home");
            int groupId = 0;
            if (!homePage) {
                try {
                    // the names of the groups are "groupX" where X is the group id
                    groupId = Integer.parseInt(itemNameArray[2]);
                } catch (Throwable t) {
                    log.error("Can't parse group id.", t);
                }
            }

            int skinId = -1;
            try {
                // the names of the home/groups are "x_skinId" where skinId is the group/home skinId
                skinId = Integer.parseInt(itemNameArray[1]);
            } catch (Throwable t) {
                log.error("Can't parse skin id.", t);
            }

            if (homePage) {
                home.put(skinId, true);
            } else {
                if (null == groups.get(skinId)) { //if first time subscribe from this skin id
                	groups.put(skinId, new HashMap<Integer, Boolean>());
                }
                groups.get(skinId).put(groupId, true);
            }

            levelsCache.sendSnapshot(homePage, groupId, skinId);

            log.info("sent snap shot to skin id: " + skinId + " homepage: " + homePage + " groupId " + groupId);
        } else if (itemType == ITEM_TYPE_AOTP){
            subUnsubAO(itemNameArray, true);
        } else if (itemType == ITEM_TYPE_AOCTRL) {
            subUnsubAOCtrl(itemNameArray, true);
        } else if (itemType == ITEM_TYPE_AOSTATE) {
            this.aoState = true;
            levelsCache.sendAOStateSnapshot();
        } else if (itemType == ITEM_TYPE_INSURANCES) {
        	itemNameArray = AdapterUtils.patchForUnderscoreInUserName(itemNameArray);
            synchronized (usersQueue) {
                usersQueue.add(itemNameArray[1]);
            }
        } else if (itemType == ITEM_TYPE_OPTION_PLUS) {
            optionPlusSubscriptions.put(itemNameArray[1], true);
            levelsCache.sendOptionPlusSnapshot();
        } else if (itemType == ITEM_TYPE_BINARY_ZERO_ONE_HUNDRED) {
        	log.trace("subscribe to ITEM_TYPE_BINARY_ZERO_ONE_HUNDRED itemNameArray[1]: " + itemNameArray[1] + " itemNameArray[2]: " + itemNameArray[2]);
            binaryZeroOneHundredSubscriptions.put(itemNameArray[1] + "_" + itemNameArray[2], true);
            levelsCache.sendBinaryZeroOneHundredSnapshot();
        } else if (itemType == ITEM_TYPE_LIVE_GLOBE) {
        	liveGlobeSubscribed = true;
        	levelsCache.sendLiveGlobeSnapshot();
        } else if (itemType == ITEM_TYPE_LIVE_INVESTMENTS) {
        	liveInvestmentsSubscriptions.put(itemNameArray[1], true);
        	levelsCache.sendLiveInvestmentsSnapshot(itemNameArray[1]);
        } else if (itemType == ITEM_TYPE_LIVE_TRENDS) {
        	liveTrendsSubscriptions.put(itemName, true);
        	levelsCache.sendLiveTrendsSnapshot(itemName);
        } else if (itemType == ITEM_TYPE_SHOPPING_BAG) {
        	shoppingBagCache.subscribe(itemNameArray, itemName);
        }
    }

    /*
     * called by Lightstreamer Kernel on item unsubscription
     */
    @Override
	public void unsubscribe(String itemName) throws SubscriptionException, FailureException {
        log.info("Unsubscribing from " + itemName);

        int itemType = getItemType(itemName);
        if (itemType == ITEM_TYPE_INVALID) {
            log.warn("Unsubscribe request for invalid subscription - " + itemName);
            throw new SubscriptionException("(Unsubscribing) Unexpected item: " + itemName);
        }

        String[] itemNameArray = itemName.split("_");
        if (itemType == ITEM_TYPE_HOME || itemType == ITEM_TYPE_GROUP) {
            boolean unsubHome = itemNameArray[0].equals("home");
            int groupId = 0;
            if (!unsubHome) {
                try {
                    // the names of the groups are "groupX" where X is the group id
                    groupId = Integer.parseInt(itemNameArray[2]);
                } catch (Throwable t) {
                    log.error("Can't parse group id.", t);
                }
            }
            int skinId = -1;
            try {
                // the names of the home/groups are "x_skinId" where skinId is the group/home skinId
                skinId = Integer.parseInt(itemNameArray[1]);
            } catch (Throwable t) {
                log.error("Can't parse skin id.", t);
            }
            if (unsubHome && home.containsKey(skinId)?home.get(skinId):false) {
            	home.put(skinId, false);
            } else if (groups.containsKey(skinId)) {
        		if (groups.get(skinId).containsKey(groupId)?groups.get(skinId).get(groupId):false) {
        			groups.get(skinId).put(groupId, false);
        		}
            } else {
            	throw new SubscriptionException("(Unsubscribing) Unexpected item to unsubscribe: " + itemName);
            }
        } else if (itemType == ITEM_TYPE_AOTP) {
            subUnsubAO(itemNameArray, false);
        } else if (itemType == ITEM_TYPE_AOCTRL) {
            subUnsubAOCtrl(itemNameArray, false);
        } else if (itemType == ITEM_TYPE_AOSTATE) {
            this.aoState = false;
        } else if (itemType == ITEM_TYPE_INSURANCES) {
        	itemNameArray = AdapterUtils.patchForUnderscoreInUserName(itemNameArray);
            synchronized (usersQueue) {
                usersQueue.remove(itemNameArray[1]);
            }
            synchronized (users) {
                users.remove(itemNameArray[1]);
            }
            TreeSet<Investment> ts = userInvestments.remove(itemNameArray[1]);
            clrearInsuranceTs(ts, itemNameArray[1]);
        } else if (itemType == ITEM_TYPE_OPTION_PLUS) {
            optionPlusSubscriptions.put(itemNameArray[1], false);
        } else if (itemType == ITEM_TYPE_BINARY_ZERO_ONE_HUNDRED) {
            binaryZeroOneHundredSubscriptions.put(itemNameArray[1] + "_" + itemNameArray[2], false);
        } else if (itemType == ITEM_TYPE_LIVE_GLOBE) {
        	liveGlobeSubscribed = false;
        } else if (itemType == ITEM_TYPE_LIVE_INVESTMENTS) {
        	liveInvestmentsSubscriptions.put(itemNameArray[1], false);
        } else if (itemType == ITEM_TYPE_LIVE_TRENDS) {
        	liveTrendsSubscriptions.put(itemName, false);
        } else if (itemType == ITEM_TYPE_SHOPPING_BAG) {
        	shoppingBagCache.unsubscribe(itemNameArray, itemName);
        }
    }

    /**
     * Update the flag of any options trading page subscription
     *
     * @param itemNameArray the splitted subscription name
     * @param subscribe <code>true</code> for subscribe <code>false</code> for unsub
     */
    private void subUnsubAO(String[] itemNameArray, boolean subscribe) {
        long marketId = 0;
        try {
            marketId = Long.parseLong(itemNameArray[2]);
        } catch (Throwable t) {
            log.error("Can't parse market id.", t);
        }
        int scheduled = 0;
        try {
            scheduled = Integer.parseInt(itemNameArray[1]);
        } catch (Throwable t) {
            log.error("Can't parse schedule.", t);
        }
        if (log.isInfoEnabled()) {
            log.info("Adding flag for aotps marketId: " + marketId + " scheduled: " + scheduled);
        }
        aoSubscriptions.put(marketId + "_" + scheduled, subscribe);
        if (subscribe) {
            levelsCache.sendSnapshot(marketId, scheduled);
        }
    }

    /**
     * Update the flag of any options trading page control subscription
     *
     * @param itemNameArray the splitted subscription name
     * @param subscribe <code>true</code> for subscribe <code>false</code> for unsub
     */
    private void subUnsubAOCtrl(String[] itemNameArray, boolean subscribe) {
        int skinId = 0;
        try {
            skinId = Integer.parseInt(itemNameArray[1]);
        } catch (Throwable t) {
            log.error("Can't parse skin id.", t);
        }
        if (log.isInfoEnabled()) {
            log.info("Adding flag for aoctrl skniId: " + skinId);
        }
        aoSubscriptions.put(itemNameArray[1], subscribe);
        if (subscribe) {
            levelsCache.sendSnapshot(skinId);
        }
    }

    /*
     * called by Lightstreamer Kernel to know if the snapshot is available for an item
     */
    @Override
	public boolean isSnapshotAvailable(String itemName) throws SubscriptionException {
        //we generate the updates so we can make snapshots always available (even if JMS is down
        //we generate locally an "inactive" update)
        return true;
    }

    public void notifyUserMessage(String user, String message) {
        try {
            log.debug("user: " + user + " message: " + message);
            String[] messageArray = message.split("_");
            if (messageArray[0].equalsIgnoreCase("invId")) {
                updateInsurancesPage(user, 0, Long.parseLong(messageArray[1]), null, true, null);
            } else if (messageArray[0].equalsIgnoreCase("viewedInvId")) {
                updateInvSeen(user, Long.parseLong(messageArray[1]));
            } else if (messageArray[0].equalsIgnoreCase(ShoppingBagCache.ITEM_PREFIX)) {
            	shoppingBagCache.notifyCloseShoppingBag(user, messageArray, message);
            }
        } catch (Exception e) {
            log.error("Failed to process user: " + user + " message: " + message, e);
        }
    }

    public void updateInvSeen(String user, long invId) {
        Investment inv = null;
        TreeSet<Investment> ts = userInvestments.get(user);
        for (Iterator<Investment> tsi = ts.iterator(); tsi.hasNext();) {
            inv = tsi.next();
            if (inv.getId() == invId) {
                if (!inv.isSeen()) { //if it wasnt mark as seen already
                    try {
                        if (InvestmentsManager.updateInvSeen(invId)) {
                            inv.setSeen(true);
                        } else {
                            log.error("Cant insert investment " + invId + "  into golden minutes viewing table");
                        }
                    } catch (Exception e) {
                        log.error("Cant insert investment " + invId + " into golden minutes viewing table", e);
                    }
                }
                break;
            }
        }
    }

///////////////// Utils
    private static String noParam = " is missing.\nProcess exits";
    private static String useDefault = " is missing. Using default.";
    private static String isNaN = " must be a number but it isn't. Using default.";

    private int getParam(Map params, String toGet, boolean required, int def) throws DataProviderException {
        int resInt;
        String res = (String) params.get(toGet);
        if (res == null) {
            if (required) {
                throw new DataProviderException(toGet + noParam);
            } else {
                if (log != null) {
                    log.warn(toGet + useDefault);
                }
                resInt = def;
            }
        } else {
            try {
                resInt = Integer.parseInt(res);
            } catch (NumberFormatException nfe) {
                if (log != null) {
                    log.error(toGet + isNaN);
                }
                resInt = def;
            }
        }

        if (log != null) {
            log.debug(toGet+ ": " + resInt);
        }
        return resInt;
    }

    private String getParam(Map params, String toGet, boolean required, String def) throws DataProviderException {
        String res = (String) params.get(toGet);
        if (res == null) {
            if (required) {
                throw new DataProviderException(toGet + noParam);
            } else {
                if (log != null) {
                    log.warn(toGet + useDefault);
                }
                res = def;
            }
        }
        if (log != null) {
            log.debug(toGet+ ": " + res);
        }
        return res;
    }
///////////////// Utils end
    /*
     * check if the user have golden or roll up investments
     *
     * @parm userName the user name to load his investments
     * return true if the user have investment with insurance flag Null. False if all the investments arent null
     */
    private boolean startUser(String userName, GoldenMinuteType gmType) throws SQLException {
        log.debug("starting user: " + userName + " gmType:" +gmType.name());
        
        ArrayList<Investment> l = null;
        if(gmType == GoldenMinuteType.STANDARD) {
        	l = InvestmentsManager.getUserHourInvestments(userName, goldenMinStart, goldenMinEnd);
        } if(gmType == GoldenMinuteType.ADDITIONAL) {
        	l = InvestmentsManager.getUserHourInvestments(userName, goldenMin2Start, goldenMin2End);
        }

        Investment inv = null;

        for (int j = 0; j < l.size(); j++) {
            inv = l.get(j);
            if(gmType == GoldenMinuteType.STANDARD && null == inv.getInsuranceFlag() ) {
            	return true;
            }
            
            if (gmType == GoldenMinuteType.ADDITIONAL && null == inv.getInsuranceFlagAdditional()) {
            	return true;
            }
        }

        TreeSet<Investment> ts = new TreeSet<Investment>(invComparator);
        TreeSet<Investment> tsRU = new TreeSet<Investment>(invComparator);
        for (int j = 0; j < l.size(); j++) {
            inv = l.get(j);
            int insuranceFlag = -1;
            if(gmType == GoldenMinuteType.STANDARD) {
            	insuranceFlag = inv.getInsuranceFlag();
            } else if( gmType == GoldenMinuteType.ADDITIONAL) {
            	insuranceFlag = inv.getInsuranceFlagAdditional();
            }
            	
            if (insuranceFlag == ConstantsBase.INSURANCE_GOLDEN_MINUTES ) {
                ts = addNewInvestment(userName, inv, ts, ConstantsBase.INSURANCE_GOLDEN_MINUTES);
            } else if (insuranceFlag == ConstantsBase.INSURANCE_ROLL_UP && !inv.isSeen()) {
                tsRU = addNewInvestment(userName, inv, tsRU, ConstantsBase.INSURANCE_ROLL_UP);
            }
        }

        if (ts.size() == 0) { //if no GM, use The RU
            ts = tsRU;
        }

        log.debug("investments loaded: " + l.size() + " qualified: " + ts.size());

        int i = 0;
        for (Iterator<Investment> itr = ts.iterator(); itr.hasNext(); i++) {
            inv = itr.next();
            inv.setInsurancePosition(i);
            if (i < 4) {
                inv.setShowInsurance(true);
                inv.setInsuranceCurrentLevels(levelsCache.getCurrentLevels(inv.getOpportunityId()));
                sendInsurancesUpdate(userName, inv, "ADD", gmTicker.currentGMType.name());
            }
        }

        // leave the current user investments out until the snapshot is sent
        userInvestments.put(userName, ts);
        return false;
    }

    /**
     * if the investment is GM or RU add the investment also to the opps users list for opp updates
     * @param userName
     * @param inv
     */
    private TreeSet<Investment> addNewInvestment(String userName, Investment inv, TreeSet<Investment> ts, int type) {
        List<String> usrs = null;

        inv.setShowInsurance(false);
        inv.setInsuranceType(type);
        ts.add(inv);

        usrs = opportunityUsers.get(inv.getOpportunityId());
        if (null == usrs) {
            usrs = new LinkedList<String>();
            usrs.add(userName);
            opportunityUsers.put(inv.getOpportunityId(), usrs);
            log.debug("added users for opp " + inv.getOpportunityId());
        } else {
            synchronized (usrs) {
                if (!usrs.contains(userName)) {
                    usrs.add(userName);
                    log.debug("added user to existing users for opp " + inv.getOpportunityId());
                }
            }
        }

        return ts;
    }

    /*not in use any more SP do it called from service
    private void updateInvestmentCurrentValues(Investment inv) {
        double crrAssetLevel = oppsGMStartCurrentLevel.get(inv.getOpportunityId());
        log.debug("investment id: " + inv.getId() + " investment level " + inv.getCurrentLevel() + " opp current level: " + crrAssetLevel);
        double levelSpread = 0;
        if (inv.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_CALL) {
            levelSpread = crrAssetLevel - inv.getCurrentLevelValue();
        } else {
            levelSpread = inv.getCurrentLevelValue() - crrAssetLevel;
        }
        inv.setInsuranceCurrentLevel(crrAssetLevel);
        inv.setCurrentInsuranceQualifyPercent(levelSpread / inv.getCurrentLevelValue());
    }*/

    private void sendInsurancesUpdate(String userName, Investment i, String command, String gmType) {
        if (i.isShowInsurance()) {
            if (log.isTraceEnabled()) {
                log.trace("sending update to user: " + userName + " for investment: " + i.getId() + " command: " + command);
            }
            Hashtable<String, String> update = new Hashtable<String, String>();
            update.put("key", String.valueOf(i.getId()));
            update.put("command", command);
            update.put("INS_MARKET", String.valueOf(i.getMarketId()));
            update.put("INS_MARKET_NAME", i.getMarketName());
            update.put("INS_LEVEL", formatLevel(i.getCurrentLevel(), i.getDecimalPoint()));

            for (Map.Entry<SkinGroup, String> entry : i.getInsuranceCurrentLevels().entrySet()) {
            	update.put(entry.getKey().getInsCurrentLevelUpdateKey(), entry.getValue());
			}
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
            update.put("INS_AMOUNT", formatAmount(InvestmentsManagerBase.getAmountWithoutFees(i)));
            update.put("INS_WIN", formatAmount(InvestmentsManagerBase.getAmountWithoutFees(i) + (InvestmentsManagerBase.getAmountWithoutFees(i) * i.getOddsWin())));
            double x = Math.round(InvestmentsManagerBase.getAmountWithoutFees(i) * (i.getInsuranceType() == ConstantsBase.INSURANCE_GOLDEN_MINUTES ? i.getInsurancePremiaPercent() : i.getRollUpPremiaPercent()));
            update.put("INS_INS", ""+(x)/100);
            update.put("INS_TYPE", String.valueOf(i.getTypeId()));
            update.put("INS_POSITION", String.valueOf(i.getInsurancePosition()));
            update.put("INS_INS_TYPE", String.valueOf(i.getInsuranceType()));
            update.put("INS_LEVEL_CLR", String.valueOf(i.getCurrentLevel() < i.getMarketLastDayClosingLevel() ? "0" : i.getCurrentLevel() == i.getMarketLastDayClosingLevel() ? "1" : "2"));
            update.put("INS_CLOSE_TIME", String.valueOf(gmTicker.getCurrentClosingTime()));
            update.put("INS_IS_OPEN_GRAPH", String.valueOf(i.isOpenGraph()));
            update.put("INS_OPP_ID", String.valueOf(i.getOpportunityId()));
            update.put("INS_TIME_CREATED", String.valueOf(i.getTimeCreated().getTime()));
            update.put("INS_TIME_EST_CLOSE", String.valueOf(i.getTimeEstClosing().getTime()));
            update.put("INS_PTYPE", gmType); // roll forward standard(1) or additional(2)
            listener.update("insurances_" + userName, update, false);
        }
    }

    protected static String formatAmount(double amount) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        return df.format(amount / 100);
    }

    protected static String formatLevel(double level, long decimalPoint) {
        String pattern = "#,##0.0";
        for (int i = 2; i <= decimalPoint; i++) {
            pattern = pattern + "0";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(level);
    }

    private void clrearInsuranceTs(TreeSet<Investment> ts, String userName) {
    	if (null == ts) {
    		return;
    	}
        Investment inv = null;
        List<String> usrs = null;
        for (Iterator<Investment> i = ts.iterator(); i.hasNext();) {
            inv = i.next();
            usrs = opportunityUsers.get(inv.getOpportunityId());
            synchronized (usrs) {
                usrs.remove(userName);
            }
        }
    }

    class GoldenMinutesTicker extends Thread {
        private boolean goldenMinutesOpen;
        private boolean running;
        private long currentOpeningTime;
        private long currentClosingTime;
        GoldenMinuteType currentGMType = GoldenMinuteType.STANDARD;

        public GoldenMinutesTicker() {
            log.debug("GoldenMinutesTicker create");
            goldenMinutesOpen = false;
            currentOpeningTime = Long.MAX_VALUE;
            currentClosingTime = Long.MAX_VALUE;
        }

        @Override
		public void run() {
            Thread.currentThread().setName("GoldenMinutesTicker");
            log.info("GoldenMinutesTicker start.");
            running = true;
            while (running) {
                try {
                    Calendar currentTime = Calendar.getInstance();
                    if(!goldenMinutesOpen) {
                    	if (currentTime.getTimeInMillis() >= currentOpeningTime) {
                    		goldenMinutesStart();
                    	}
                    } else {
	                    if (currentTime.getTimeInMillis() >= currentClosingTime) {
	                    	goldenMinutesStop();
	                    }
                    }
                	
                    // move the usersQueue to users first
                    List<String> tmpQueue = null;
                    synchronized (usersQueue) {
                        if (goldenMinutesOpen) {
                            tmpQueue = new LinkedList<String>();
                            tmpQueue.addAll(usersQueue);
                            usersQueue.clear();
                        } else {
                            synchronized(users) {
                                users.addAll(usersQueue);
                            }
                            usersQueue.clear();
                        }
                    }
                    if (null != tmpQueue) {
                        String u = null;
                        for (Iterator<String> i = tmpQueue.iterator(); i.hasNext();) {
                            u = i.next();
                            startUser(u, currentGMType);
                            synchronized(users) {
                                users.add(u);
                            }
                        }
                        tmpQueue = null;
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    log.error("Problem in GoldenMinutesTicker.", e);
                }
            }
            log.info("GoldenMinutesTicker end.");
        }

        private void calculateCurrentClosingTime(int expiryMinutes, GoldenMinuteType gmType) {
        	currentGMType = gmType;
            Calendar c = Calendar.getInstance();
            if (c.get(Calendar.MINUTE) > expiryMinutes) {
                c.add(Calendar.HOUR_OF_DAY, 1);
            }
            c.set(Calendar.MINUTE, expiryMinutes);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            
        	if(gmType == GoldenMinuteType.STANDARD) {
        		c.add(Calendar.MINUTE, -goldenMinEnd);
        	} else {
        		c.add(Calendar.MINUTE, -goldenMin2End);
        	}
        	currentClosingTime = c.getTimeInMillis();
        	
        	if(gmType == GoldenMinuteType.STANDARD) {
        		c.add(Calendar.MINUTE, -(goldenMinStart - goldenMinEnd));
        	} else {
        		c.add(Calendar.MINUTE, -(goldenMin2Start - goldenMin2End));
        	}
        	currentOpeningTime = c.getTimeInMillis();
       		
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
       		log.info("Current opening time: " + format.format(new Date(currentOpeningTime)) + " closing time: " + format.format(new Date(currentClosingTime)));
        }

        
        public long getCurrentClosingTime() {
            return currentClosingTime;
        }

        public void stopTicker() {
            running = false;
        }

        public void goldenMinutesStart() throws SQLException {
            log.info("Starting golden minutes");
            List<String> u = new LinkedList<String>();
            List<String> temp = null;
            boolean hasNullInv = false;

            synchronized (users) {
                u.addAll(users);
            }
            while (u.size() > 0) {
                temp = new LinkedList<String>();
                for (Iterator<String> i = u.iterator(); i.hasNext();) {
                    String userName = i.next();
                    hasNullInv = startUser(userName, currentGMType);
                    if (hasNullInv) {
                        log.debug("user " + userName + " has null investments added him to retry list");
                        temp.add(userName);
                    }
                }
                u = temp;
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    log.debug("cant send thread to sleep ;) ", e);
                }
            }

            goldenMinutesOpen = true;
            currentOpeningTime = Long.MAX_VALUE;
            log.info("Started golden minutes");
        }

        private void goldenMinutesStop() {
            log.info("Stopping golden minutes");
            currentClosingTime = Long.MAX_VALUE;
            goldenMinutesOpen = false;

            String usr = null;
            TreeSet<Investment> ts = null;
            Investment inv = null;
            List<String> usrs = null;
            for (Iterator<String> i = userInvestments.keySet().iterator(); i.hasNext();) {
                usr = i.next();
                ts = userInvestments.get(usr);
                i.remove();
                for (Iterator<Investment> tsi = ts.iterator(); tsi.hasNext();) {
                    inv = tsi.next();
                    sendInsurancesUpdate(usr, inv, "DELETE", gmTicker.currentGMType.name());
                    usrs = opportunityUsers.get(inv.getOpportunityId());
                    synchronized (usrs) {
                        usrs.remove(usr);
                    }
                }
            }
            userInvestments.clear();
            opportunityUsers.clear();
            oppsGMStartCurrentLevel.clear();
            log.info("Stopped golden minutes");
        }
    }

    class CleanupHook extends Thread {
        @Override
		public void run() {
            if (log.isInfoEnabled()) {
                log.info("CleanupHook start.");
            }
            levelsCache.unregisterJMX();
            levelsCache.close();

            dbMon.unregisterJMX();

            if (null != gmTicker) {
                gmTicker.stopTicker();
            }

            if (shoppingBagCache != null) {
            	shoppingBagCache.shutdown();
            }
            if (log.isInfoEnabled()) {
                log.info("CleanupHook end.");
            }
        }
    }

    @Override
    public void settledOpportunitiesUpdate(SettledOpportunitiesMessage msg) {
    	shoppingBagCache.handleSettledOpportunitiesUpdate(msg);
    }

	@Override
	public void ttBinary0100Update(long opportunityId, double autoShiftParameter, double calls, double puts, String feedName, long decimalPoint, boolean isDisabled, boolean isAutoDisabled, HashMap<String, String> update, double contractsBought, double contractsSold, Object parameters, double level, boolean notClosed, Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
		// do nothing
	}

	@Override
	public void ttUpdate(long opportunityId, double autoShiftParameter, double calls, double puts, String feedName, long decimalPoint, boolean isDisabled, boolean isAutoDisabled, HashMap<String, String> update, boolean notClosed, Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
		// do nothing
	}

	@Override
	public void shoppingBagUpdate(String itemName, Map<String, String> update) {
		if (log.isTraceEnabled()) {
            log.trace("Sending [" + itemName + "] message [" + update + "] to lightstreamer");
        }
		listener.update(itemName, update, false);
	}

	@Override
	public void notifyInsurancesStart(long id) {
		int expiryMinutes = (int) (id >> 32);
		GoldenMinuteType type = GoldenMinuteType.getById((int) id);
		log.info("notify insurance start for :" + expiryMinutes +" type :"+ type);
//		try {
			if (gmTicker.goldenMinutesOpen) {
				log.error("golden minutes is currently running !");
			} else {
				// calculate current gm end
				gmTicker.calculateCurrentClosingTime(expiryMinutes, type);
				// start gm
//				gmTicker.goldenMinutesStart();
			}
//		} catch (SQLException e) {
//			log.error("Cannot START Golden Minute:", e);
//		}
	}
	
	public static void main(String args[]) {
		double d = 100.20;
		
		System.out.println(d /100 );
	}
}