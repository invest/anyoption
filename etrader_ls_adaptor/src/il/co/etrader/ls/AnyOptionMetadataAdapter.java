package il.co.etrader.ls;

import il.co.etrader.ls.bl_managers.ApiManager;
import il.co.etrader.ls.bl_managers.UsersManager;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.lightstreamer.interfaces.metadata.AccessException;
import com.lightstreamer.interfaces.metadata.CreditsException;
import com.lightstreamer.interfaces.metadata.ItemsException;
import com.lightstreamer.interfaces.metadata.MetadataProviderAdapter;
import com.lightstreamer.interfaces.metadata.MetadataProviderException;
import com.lightstreamer.interfaces.metadata.NotificationException;
import com.lightstreamer.interfaces.metadata.SchemaException;
import com.lightstreamer.interfaces.metadata.TableInfo;

public class AnyOptionMetadataAdapter extends MetadataProviderAdapter {
    private Logger log;

    private static String NOPARAM = " is missing.\nProcess exits";
    private static String USEDEFAULT = " is missing. Using default.";

    private Hashtable<String, String> qualifiedUsersByName;
    private Hashtable<String, String> qualifiedUsersBySession;

    private Hashtable<String, String> qualifiedUsersByNameAOTPS;
    private Hashtable<String, Boolean> qualifiedUsersBySessionAOTPS;
    private ArrayList<String> ourDomains;

    public AnyOptionMetadataAdapter() {
        qualifiedUsersByName = new Hashtable<String, String>();
        qualifiedUsersBySession = new Hashtable<String, String>();
        qualifiedUsersByNameAOTPS = new Hashtable<String, String>();
        qualifiedUsersBySessionAOTPS = new Hashtable<String, Boolean>();

    }

    /**
     * Reads configuration settings for user and resource constraints.
     * If some setting is missing, the corresponding constraint is not set.
     *
     * @param  params  Can contain the configuration settings.
     * @param  dir  Not used.
     * @throws MetadataProviderException in case of configuration errors.
     */
    @Override
	public void init(Map params, File dir) throws MetadataProviderException {
        String logConfig = (String) params.get("log_config");
        if (logConfig != null) {
            File logConfigFile = new File(dir, logConfig);
            String logRefresh = (String) params.get("log_config_refresh_seconds");
            if (logRefresh != null) {
                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
            } else {
                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
            }
        }
        log = Logger.getLogger(AnyOptionMetadataAdapter.class);
        log.info("Init anyoption metadata adapter");
        HashMap<String,String> dbMap = new HashMap<String,String>();
        dbMap.put("dburl", getParam(params, "dburl", true, null));
        dbMap.put("dbuser", getParam(params, "dbuser", true, null));
        dbMap.put("dbpass", getParam(params, "dbpass", true, null));
        dbMap.put("dbpoolname", getParam(params, "dbpoolname", true, null));
        DBUtil.init(dbMap);
        log.debug("Pool initialized.");

        log.debug("loading our domains list");

        ourDomains = ApiManager.getOurDomains();

        log.debug("loading our domains list done");
    }

    private String getParam(Map params, String toGet, boolean required, String def) throws MetadataProviderException {
        String res = (String) params.get(toGet);
        if (res == null) {
            if (required) {
                throw new MetadataProviderException(toGet + NOPARAM);
            } else {
                if (log != null) {
                    log.warn(toGet + USEDEFAULT);
                }
                res = def;
            }
        }
        return res;
    }

    private String[] tokenize(String str) {
        StringTokenizer source = new StringTokenizer(str, " ");
        int dim = source.countTokens();
        String[] ret = new String[dim];

        for (int i = 0; i < dim; i++) {
            ret[i] = source.nextToken();
        }
        return ret;
    }

    /**
     * Resolves a Group name supplied in a Request. The names of the Items
     * in the Group are returned.
     *
     * @param user A User name.
     * @param session A Session name. Not used.
     * @param id A Group name.
     * @return An array with the names of the Items in the Group.
     * @throws ItemsException
     */
    @Override
	public String[] getItems(String user, String session, String id) throws ItemsException {
        log.debug("getItems - id: " + id);
        return tokenize(id);
    }

    /**
     * Resolves a Schema name supplied in a Request. The names of the Fields
     * in the Schema are returned.
     *
     * @param user A User name.
     * @param session A Session name. Not used.
     * @param id The name of the Group whose Items the Schema is to be applied
     * to.
     * @param schema A Schema name.
     * @return An array with the names of the Fields in the Schema.
     * @throws SchemaException
     */
    @Override
	public String[] getSchema(String user, String session, String id, String schema) throws SchemaException {
        log.debug("getSchema - id: " + id + " schema: " + schema);
        return tokenize(schema);
    }

    @Override
	public void notifyUser(String user, String password, Map httpHeaders) throws AccessException, CreditsException {
        user = unwrapUser(user);
        if (log.isTraceEnabled()) {
            log.trace("notifyUser - user: " + user + " password: " + password);
        }
        try {
			for (Object header : httpHeaders.keySet()) {
				if (null != header) {
					log.info("notifyUser httpheaders " + header.toString() + " = " + httpHeaders.get(header));
				}
			}
		} catch (RuntimeException e) {
			log.info("notifyUser error", e);
		}

		if (!isOurDomain(httpHeaders) && null != user && ApiManager.validateAPIUser(user, password)) {
			qualifiedUsersByNameAOTPS.put(user, user);
		}
        if (null != user && UsersManager.isUserQualified(user, password)) {
            log.debug("qualified user: " + user);
            qualifiedUsersByName.put(user, user);
        }
    }

    @Override
	public void notifyNewSession(String user, String session, Map clientContext) throws CreditsException, NotificationException {
        user = unwrapUser(user);
        try {
	        log.info("notifyNewSession clientContext " + clientContext);
        } catch (Exception e) {
        	log.info("notifyNewSession error", e);
		}
        try {
	        for (Object clientC : clientContext.values()) {
	        	if (null != clientC) {
	        		log.info("notifyNewSession loop clientContext " + clientC.toString());
	        	}
			}
        } catch (Exception e) {
        	log.info("notifyNewSession loop error", e);
		}

        if (null != user && null != qualifiedUsersByNameAOTPS.get(user)) {
        	log.debug("qualified user AOTPS: " + user + " session: " + session);
        	qualifiedUsersByNameAOTPS.remove(user);
            qualifiedUsersBySessionAOTPS.put(session, true);
        } else {
        	Map httpHeaders = null != clientContext ? (Map)clientContext.get("HTTP_HEADERS") : null;
            if (isOurDomain(httpHeaders)) {
            	qualifiedUsersBySessionAOTPS.put(session, true);
            }
        }

        if (null != user && null != qualifiedUsersByName.get(user)) {
            log.debug("qualified user: " + user + " session: " + session);
            qualifiedUsersByName.remove(user);
            qualifiedUsersBySession.put(session, user);
        }
    }

    @Override
	public boolean wantsTablesNotification(java.lang.String user) {
        return true;
    }

    @Override
	public void notifyNewTables(String user, String session, TableInfo[] tables) throws CreditsException, NotificationException {
        user = unwrapUser(user);
        String itemName = null;
        for (int i = 0; i < tables.length; i++) {
        	itemName = tables[i].getId();
            log.debug("notifyNewTables - user: " + user + " session: " + session + " table: " + itemName);
            if (itemName.startsWith("insurances")
            		|| itemName.startsWith(ShoppingBagCache.ITEM_PREFIX)) {
                validateUserTable(user, session, itemName);
            } else if (itemName.startsWith("aotps")) {
            	if (null == session || null == qualifiedUsersBySessionAOTPS.get(session)) {
            		log.warn("aotps table request from not qualified user: " + user + " session: " + session);
                    throw new CreditsException(0, "No permissions.");
            	} else {
                    log.debug("aotps table subscribed for user: " + user + " session: " + session);
                }
            }
        }
    }

    /**
     * This method checks whether a table that can be used only by logged-in
     * users is used by such user.
     *
     * @param user
     * @param session
     * @param itemName
     * @throws CreditsException
     */
    private void validateUserTable(String user,
    								String session,
    								String itemName) throws CreditsException {
    	String [] itemNameArray = itemName.split("_");
    	itemNameArray = AdapterUtils.patchForUnderscoreInUserName(itemNameArray);
    	if (null == user || null == session || null == qualifiedUsersBySession.get(session)) {
            log.warn(itemNameArray[0] + " table request from not qualified user: " + user);
            throw new CreditsException(0, "No permissions.");
        } else {
        	if (itemNameArray.length < 1) {
        		log.warn(itemNameArray[0] + " table item name without username");
        		throw new CreditsException(0, "No permissions. Table item name without username");
        	} else if (!itemNameArray[1].equals(user)) {
        		log.warn(itemNameArray[0] + " table username [" + itemNameArray[1] + "] does not match session username");
        		throw new CreditsException(0, "No permissions. Table username does not match session username");
        	} else {
        		log.debug(itemNameArray[0] + " table subscribed for user: " + user);
        	}
        }
    }

    @Override
	public void notifySessionClose(java.lang.String session) throws NotificationException {
        log.debug("session closed: " + session);
        if (null != session) {
            qualifiedUsersBySession.remove(session);
            qualifiedUsersBySessionAOTPS.remove(session);
        }
    }

    @Override
	public void notifyUserMessage(String user, String session, String message) throws CreditsException, NotificationException {
        user = unwrapUser(user);
        log.debug("notifyUserMessage - user: " + user + " session: " + session + " message: " + message);
        if (null != user && null != message) {
            JMSCommandDataAdapter.getInstance().notifyUserMessage(user, message);
        }
    }

    /**
     * Because of some strange bug a user starting with 0 lose it until it gets to the MetadataAdapter.
     * To go around that problem we wrap the user with "a"s.
     *
     * @param user
     * @return The user without the wrapping "a"s.
     */
    private String unwrapUser(String user) {
        if (null == user || user.trim().equals("") || user.length() < 3) {
            return user;
        }
        return user.substring(1, user.length() - 1);
    }

    private boolean isOurDomain(Map httpHeaders) {
    	String host = null != httpHeaders.get("host") ? httpHeaders.get("host").toString() : null;
		if (null != host) {
			for (String domain : ourDomains) {
				if (host.toLowerCase().contains(domain)) {
					return true;
				}
			}
		}
		return false;
    }
}