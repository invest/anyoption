package il.co.etrader.ls;

import il.co.etrader.ls.shopping_bag.ShoppingBagCache;

import java.util.Arrays;

public class AdapterUtils {
    /**
     * The insurances and shopping back subscriptions have format:
     * insurances_XXXXX
     * shoppingbag_XXXXX_YYYYY
     * 
     * copyop subscriptions have format A_XXXXX_YYYYY
     * 
     * where:
     * XXXXX is the user name
     * YYYYY is the user id
     * 
     * Problem occurs when the user name (which is an email) conains _ which is used as separator. So patch the array to fix this case.
     * 
     * @param itemNameArray
     * @return
     */
    public static String[] patchForUnderscoreInUserName(String[] itemNameArray) {
    	if (null != itemNameArray && itemNameArray.length > 1) {
        	if (itemNameArray[0].equals("insurances") && itemNameArray.length > 2) {
        		itemNameArray[1] = String.join("_", Arrays.copyOfRange(itemNameArray, 1, itemNameArray.length));
        	}
        	if ((itemNameArray[0].equals(ShoppingBagCache.ITEM_PREFIX)
        			|| itemNameArray[0].length() == 1) // for now copyop queue type IDs are 1 digit long. if it gets longer maybe we should check if the first tooken is a number
        			&& itemNameArray.length > 3) {
        		itemNameArray[1] = String.join("_", Arrays.copyOfRange(itemNameArray, 1, itemNameArray.length - 1));
        	}
    	}
    	return itemNameArray;
    }
}