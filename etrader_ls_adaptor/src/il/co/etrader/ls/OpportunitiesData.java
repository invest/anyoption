//package il.co.etrader.ls;
//
//
///**
// * @author Eyal
// */
//public class OpportunitiesData {
//	private static final Logger log = Logger.getLogger(OpportunitiesData.class);
//
//	//sum of investes in money(centes)(put,calls,call-put,call+put)
//	private double callsAmount;
//	private double putsAmount;
//	private double callsAmountSubPutsAmount;
//	private double callsAmountAddPutsAmount;
//	//number of investes (put and call)
//	private int calls;
//	private int puts;
//
//	// opp last time invest and est closing time
//	private Date timeLastInvest;
//	private Date timeEstClosing;
//
//	//opp shift parameter by trader to ET
//	private BigDecimal shiftParameter;
////	opp shift parameter by trader to AO
//	private BigDecimal shiftParameterAO;
//	//opp shift parameter by server
//	private double autoShiftParameter;
//	//opp disable by trader
//	private boolean isDisabledTrader;
//	//opp disable
//	private boolean isDisabled;
//	//opp scheduled (1 hourly 2 daily 3 weekly 4 monthly)
//	private int scheduled;
//	//opp type (1 defulet 2 one touch)
//	private int oppType;
//	/**
//	 * opp state
//	 * state 2: waiting for expiery
//	 * state 3: disable or suspend
//	 * state 1: else
//	 */
//	private int state;
//	// opp max exposure
//	private long maxExposure;
//
//	//market feed name
//	private String feedName;
//	//market suspended
//	private boolean isSuspended;
//	//market suspend message
//	private String suspendedMessage;
//	//market id
//	private int marketId;
//	//market group id (from table MARKETS)
//	private int marketGroupId;
//	// market Decimal Point
//	private int decimalPoint;
//
//
//	/**
//	 * @return the calls
//	 */
//	public int getCalls() {
//		return calls;
//	}
//	/**
//	 * @param calls the calls to set
//	 */
//	public void setCalls(int calls) {
//		this.calls = calls;
//	}
//	/**
//	 * @return the callsAmount
//	 */
//	public double getCallsAmount() {
//		return callsAmount;
//	}
//	/**
//	 * @param callsAmount the callsAmount to set
//	 */
//	public void setCallsAmount(double callsAmount) {
//		this.callsAmount = callsAmount;
//	}
//	/**
//	 * @return the callsAmountAddPutsAmount
//	 */
//	public double getCallsAmountAddPutsAmount() {
//		return callsAmountAddPutsAmount;
//	}
//	/**
//	 * @param callsAmountAddPutsAmount the callsAmountAddPutsAmount to set
//	 */
//	public void setCallsAmountAddPutsAmount(double callsAmountAddPutsAmount) {
//		this.callsAmountAddPutsAmount = callsAmountAddPutsAmount;
//	}
//	/**
//	 * @return the callsAmountSubPutsAmount
//	 */
//	public double getCallsAmountSubPutsAmount() {
//		return callsAmountSubPutsAmount;
//	}
//	/**
//	 * @param callsAmountSubPutsAmount the callsAmountSubPutsAmount to set
//	 */
//	public void setCallsAmountSubPutsAmount(double callsAmountSubPutsAmount) {
//		this.callsAmountSubPutsAmount = callsAmountSubPutsAmount;
//	}
//
//	/**
//	 * @return the puts
//	 */
//	public int getPuts() {
//		return puts;
//	}
//	/**
//	 * @param puts the puts to set
//	 */
//	public void setPuts(int puts) {
//		this.puts = puts;
//	}
//	/**
//	 * @return the putsAmount
//	 */
//	public double getPutsAmount() {
//		return putsAmount;
//	}
//	/**
//	 * @param putsAmount the putsAmount to set
//	 */
//	public void setPutsAmount(double putsAmount) {
//		this.putsAmount = putsAmount;
//	}
//
//	/**
//	 * @return the marketGroupId
//	 */
//	public int getMarketGroupId() {
//		return marketGroupId;
//	}
//	/**
//	 * @param marketGroupId the marketGroupId to set
//	 */
//	public void setMarketGroupId(int marketGroupId) {
//		this.marketGroupId = marketGroupId;
//	}
//	/**
//	 * @return the marketId
//	 */
//	public int getMarketId() {
//		return marketId;
//	}
//	/**
//	 * @param marketId the marketId to set
//	 */
//	public void setMarketId(int marketId) {
//		this.marketId = marketId;
//	}
//	/**
//	 * @return the oppType
//	 */
//	public int getOppType() {
//		return oppType;
//	}
//	/**
//	 * @param oppType the oppType to set
//	 */
//	public void setOppType(int oppType) {
//		this.oppType = oppType;
//	}
//	/**
//	 * @return the scheduled
//	 */
//	public int getScheduled() {
//		return scheduled;
//	}
//	/**
//	 * @param scheduled the scheduled to set
//	 */
//	public void setScheduled(int scheduled) {
//		this.scheduled = scheduled;
//	}
//	/**
//	 * this method calculate the needed data from the opp investments.
//	 * @param list the investments for this opp
//	 */
//	public void calculateData(ArrayList<Investment> list) {
//		for ( Investment inv : list ) {
//			if (inv.getTypeId() == 1) {
//				this.calls++;
//				this.callsAmount += inv.getBaseAmount();
//			} else {
//				this.puts++;
//				this.putsAmount += inv.getBaseAmount();
//			}
//		}
//		this.callsAmountAddPutsAmount = callsAmount + putsAmount;
//		this.callsAmountSubPutsAmount = callsAmount - putsAmount;
//	}
//
//	/**
//	 * build hashmap with data to send to LS
//	 * @return hashmap with the data
//	 */
//	public HashMap<String, String> dataToSend(int oppId , String command) {
//		HashMap<String, String> data = new HashMap<String, String>();
//		data.put("key", String.valueOf(oppId));
//		data.put("command", String.valueOf(command));
//		data.put("callsAmount", String.valueOf(callsAmount/100));
//		data.put("putsAmount", String.valueOf(putsAmount/100));
//		data.put("callsAmountSubPutsAmount", String.valueOf(callsAmountSubPutsAmount/100));
//		data.put("callsAmountAddPutsAmount", String.valueOf(callsAmountAddPutsAmount/100));
//		data.put("calls", String.valueOf(calls));
//		data.put("puts", String.valueOf(puts));
//		data.put("timeLastInvest", getTimeLastInvestTxt());
//		data.put("marketId", String.valueOf(marketId));
//		data.put("marketGroupId", String.valueOf(marketGroupId));
//		data.put("scheduled", String.valueOf(scheduled));
//		data.put("oppType", String.valueOf(oppType));
//		data.put("shiftParameter", null != shiftParameter ? shiftParameter.multiply(new BigDecimal("100")).stripTrailingZeros().toString(): "0.0");
//		data.put("shiftParameterAO", null != shiftParameterAO ? shiftParameterAO.multiply(new BigDecimal("100")).stripTrailingZeros().toString(): "0.0");
//		data.put("autoShiftParameter", String.valueOf(autoShiftParameter*100));
//		data.put("isDisabledTrader", isDisabledTrader?"enable":"disable");
//		data.put("feedName", String.valueOf(feedName));
//		data.put("isSuspended", isSuspended?"resume":"suspend");
//		data.put("suspendedMessage", String.valueOf(suspendedMessage));
//		data.put("state", String.valueOf(state));
//		data.put("timeEstClosing", getTimeEstClosingTxt());
//		data.put("maxExposure", String.valueOf(maxExposure));
//		data.put("decimalPoint", String.valueOf(decimalPoint));
//		/*if (log.isDebugEnabled()) {
//			log.debug(data.toString());
//		}*/
//		return data;
//	}
//
//	/**
//	 * compare 2 Data of the same oppid(the 1 we sent last time with the new 1)
//	 * to check if need to send update or not
//	 * if we got diff numbers of puts/calls or
//	 * diff amount for put/calls mean we need to send update.
//	 * @param oppdata the new data from db
//	 * @return true need update , false no need update
//	 */
//	public boolean compareData(OpportunitiesData oppdata) {
//		if (calls != oppdata.calls || puts != oppdata.puts ||
//			callsAmount != oppdata.callsAmount || putsAmount != oppdata.putsAmount ||
//			!timeLastInvest.equals(oppdata.timeLastInvest) || !timeEstClosing.equals(oppdata.timeEstClosing) ||
//			(null != shiftParameter && null != oppdata.shiftParameter && shiftParameter.doubleValue() != oppdata.shiftParameter.doubleValue()) ||
//			(null != shiftParameterAO && null != oppdata.shiftParameterAO && shiftParameterAO.doubleValue() != oppdata.shiftParameterAO.doubleValue()) ||
//			autoShiftParameter != oppdata.autoShiftParameter ||
//			isDisabledTrader != oppdata.isDisabledTrader || isDisabled != oppdata.isDisabled ||
//			isSuspended != oppdata.isSuspended || maxExposure != oppdata.maxExposure ||
//			suspendedMessage != oppdata.suspendedMessage || state != oppdata.state || decimalPoint != oppdata.decimalPoint) {
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * @return the timeFirstInvest as String
//	 */
//	public String getTimeLastInvestTxt() {
//		return formatDate(timeLastInvest, false);
//	}
//
//	/**
//     * Used to format the est close time (format dd/MM/yy HH:mm) gmt 0.
//     *
//     * @param val the time to format
//     * @return
//     */
//    protected static String formatDate(Date val, boolean sort) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        if (sort) {
//        	dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
//        }
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
//        return dateFormat.format(val);
//    }
//	/**
//	 * @return the timeLastInvest
//	 */
//	public Date getTimeLastInvest() {
//		return timeLastInvest;
//	}
//	/**
//	 * @param timeLastInvest the timeLastInvest to set
//	 */
//	public void setTimeLastInvest(Date timeLastInvest) {
//		this.timeLastInvest = timeLastInvest;
//	}
//	/**
//	 * @return the autoShiftParameter
//	 */
//	public double getAutoShiftParameter() {
//		return autoShiftParameter;
//	}
//	/**
//	 * @param autoShiftParameter the autoShiftParameter to set
//	 */
//	public void setAutoShiftParameter(double autoShiftParameter) {
//		this.autoShiftParameter = autoShiftParameter;
//	}
//	/**
//	 * @return the feedName
//	 */
//	public String getFeedName() {
//		return feedName;
//	}
//	/**
//	 * @param feedName the feedName to set
//	 */
//	public void setFeedName(String feedName) {
//		this.feedName = feedName;
//	}
//	/**
//	 * @return the isDisabled
//	 */
//	public boolean isDisabled() {
//		return isDisabled;
//	}
//	/**
//	 * @param isDisabled the isDisabled to set
//	 */
//	public void setDisabled(boolean isDisabled) {
//		this.isDisabled = isDisabled;
//	}
//	/**
//	 * @return the isDisabledTrader
//	 */
//	public boolean isDisabledTrader() {
//		return isDisabledTrader;
//	}
//	/**
//	 * @param isDisabledTrader the isDisabledTrader to set
//	 */
//	public void setDisabledTrader(boolean isDisabledTrader) {
//		this.isDisabledTrader = isDisabledTrader;
//	}
//	/**
//	 * @return the isSuspended
//	 */
//	public boolean isSuspended() {
//		return isSuspended;
//	}
//	/**
//	 * @param isSuspended the isSuspended to set
//	 */
//	public void setSuspended(boolean isSuspended) {
//		this.isSuspended = isSuspended;
//	}
//	/**
//	 * @return the shiftParameter
//	 */
//	public BigDecimal getShiftParameter() {
//		return shiftParameter;
//	}
//	/**
//	 * @param shiftParameter the shiftParameter to set
//	 */
//	public void setShiftParameter(BigDecimal shiftParameter) {
//		this.shiftParameter = shiftParameter;
//	}
//
//	/**
//	 * @return the shiftParameterAO
//	 */
//	public BigDecimal getShiftParameterAO() {
//		return shiftParameterAO;
//	}
//	/**
//	 * @param shiftParameterAO the shiftParameterAO to set
//	 */
//	public void setShiftParameterAO(BigDecimal shiftParameterAO) {
//		this.shiftParameterAO = shiftParameterAO;
//	}
//
//	/**
//	 * @return the suspendedMessage
//	 */
//	public String getSuspendedMessage() {
//		return suspendedMessage;
//	}
//	/**
//	 * @param suspendedMessage the suspendedMessage to set
//	 */
//	public void setSuspendedMessage(String suspendedMessage) {
//		this.suspendedMessage = suspendedMessage;
//	}
//	/**
//	 * @return the timeEstClosing
//	 */
//	public Date getTimeEstClosing() {
//		return timeEstClosing;
//	}
//	/**
//	 * @return the timeEstClosing as String
//	 */
//	public String getTimeEstClosingTxt() {
//		return formatDate(timeEstClosing,true);
//	}
//	/**
//	 * @param timeEstClosing the timeEstClosing to set
//	 */
//	public void setTimeEstClosing(Date timeEstClosing) {
//		this.timeEstClosing = timeEstClosing;
//	}
//	/**
//	 * @return the state
//	 */
//	public int getState() {
//		return state;
//	}
//	/**
//	 * @param state the state to set
//	 */
//	public void setState() {
//		Date now = new Date();
//		if (this.isDisabled) {
//			this.state = 4;
//		} else if (this.isSuspended) {
//			this.state = 3;
//		} else if (now.after(this.timeLastInvest)) {
//			this.state = 2;
//		} else {
//			state = 1;
//		}
//	}
//
//	/**
//	 * @return the maxExposure
//	 */
//	public long getMaxExposure() {
//		return maxExposure;
//	}
//
//	/**
//	 * @param maxExposure the maxExposure to set
//	 */
//	public void setMaxExposure(long maxExposure) {
//		this.maxExposure = maxExposure;
//	}
//	/**
//	 * @return the decimalPoint
//	 */
//	public int getDecimalPoint() {
//		return decimalPoint;
//	}
//	/**
//	 * @param decimalPoint the decimalPoint to set
//	 */
//	public void setDecimalPoint(int decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//	
//	
//}
