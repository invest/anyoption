package il.co.etrader.ls;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.ifc.LevelsCacheListener;
import com.anyoption.common.jms.msgs.SettledOpportunitiesMessage;
import com.lightstreamer.interfaces.data.DataProvider;
import com.lightstreamer.interfaces.data.DataProviderException;
import com.lightstreamer.interfaces.data.FailureException;
import com.lightstreamer.interfaces.data.ItemEventListener;
import com.lightstreamer.interfaces.data.SubscriptionException;

/**
 * @author Eyal
 */
public class TraderToolAdapter implements DataProvider, LevelsCacheListener {
	private Logger log;

	private ItemEventListener listener;
	private boolean traderTool;
	private boolean traderToolBinary0100;
//	private LoadOppJob job = null;
//	private String aoSubscriptionPattern;
	private LevelsCache levelsCache;
//	protected HashMap<Integer, OpportunitiesData> oppsListData;
//	protected HashMap<Integer, OpportunitiesData> oppsListDataBinary0100;

    private static String NOPARAM = " is missing.\nProcess exits";
    private static String USEDEFAULT = " is missing. Using default.";


    private static TraderToolAdapter instance;

    public TraderToolAdapter() {

        instance = this;

    }

    public static TraderToolAdapter getInstance() {
        return instance;
    }

	/*
     * called by Lightstreamer Kernel on start
     */
    public void init(Map params, File dir) throws DataProviderException {
        //load configuration
        String logConfig = (String) params.get("log_config");
        if (logConfig != null) {
            File logConfigFile = new File(dir, logConfig);
            String logRefresh = (String) params.get("log_config_refresh_seconds");
            if (logRefresh != null) {
                PropertyConfigurator.configureAndWatch(logConfigFile.getAbsolutePath(), Integer.parseInt(logRefresh) * 1000);
            } else {
                PropertyConfigurator.configure(logConfigFile.getAbsolutePath());
            }
        }
        log = Logger.getLogger(TraderToolAdapter.class);

//        HashMap<String,String> dbMap = new HashMap<String,String>();
//        dbMap.put("dburl", getParam(params, "dburl", true, null));
//        dbMap.put("dbuser", getParam(params, "dbuser", true, null));
//        dbMap.put("dbpass", getParam(params, "dbpass", true, null));
//        dbMap.put("dbpoolname", getParam(params, "dbpoolname", true, null));
//        DBUtil.init(dbMap);

//      load JMS connections parameters
        String providerURL = getParam(params, "jmsUrl", true, null);
        String initialContextFactory = getParam(params, "initialContextFactory", true, null);
        String connectionFactory = getParam(params, "connectionFactory", true, null);
        String topic = getParam(params, "topicName", true, null);
        String queue = getParam(params, "queueName", true, null);
        //the size of the message pool
        int msgPoolSize = Integer.parseInt(getParam(params, "msgPoolSize", false, "15"));
        //in case of disconnection/failed_connection from/to JMS this is
        //the pause between each reconnection attempt
        int recoveryPause = Integer.parseInt(getParam(params, "recoveryPauseMillis", false, "2000"));
        String jmxName = getParam(params, "jmxName", true, null);

//        aoSubscriptionPattern = getParam(params, "aoSubscriptionPattern", true, null);
    //    oppsListData = null;
        log.debug("Configuration read.");

        levelsCache = new LevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, this);
        levelsCache.registerJMX(jmxName);
        Runtime.getRuntime().addShutdownHook(new CleanupHook());

        log.info("TraderToolAdapter ready.");
    }

	public void setListener(ItemEventListener listener) {
		//this listener will pass updates to Lightstreamer Kernel
		this.listener = listener;
	}

    public boolean isSnapshotAvailable(String arg0) throws SubscriptionException {
		return true;
	}

	public void subscribe(String itemName, boolean arg1) throws SubscriptionException, FailureException {
        if (log.isDebugEnabled()) {
            log.debug("new user subscribe with itemName = " + itemName);
        }
		if (itemName.equals("traderTool")) {
			this.traderTool = true;
			levelsCache.sendTTSnapshot(7);
//			oppsListData = new HashMap<Integer, OpportunitiesData>();
		} else if (itemName.equals("traderToolBinary0100")) {
			this.traderToolBinary0100 = true;
			levelsCache.sendTTSnapshot(8);
	//		oppsListDataBinary0100 = new HashMap<Integer, OpportunitiesData>();
		} else {
			throw new SubscriptionException("(Subscribing) Unexpected item: " + itemName);
		}


//		if (log.isDebugEnabled()) {
//			log.debug("starting job to load opp from db.");
//		}
//		job = new LoadOppJob();
//		job.start();
//		if (log.isDebugEnabled()) {
//			log.debug("job was started.");
//		}
	}

	public void unsubscribe(String itemName) throws SubscriptionException, FailureException {
		if (itemName.equals("traderTool")) {
			this.traderTool = false;
//			this.oppsListData = null;
			log.debug("user unsubscribe with itemName = " + itemName);
		} else if (itemName.equals("traderToolBinary0100")) {
			log.debug("user unsubscribe with itemName = " + itemName);
			this.traderToolBinary0100 = false;
//			this.oppsListDataBinary0100 = null;
		} else {
			throw new SubscriptionException("(Unsubscribing) Unexpected item: " + itemName);
		}

	}

    private String getParam(Map params, String toGet, boolean required, String def) throws DataProviderException {
        String res = (String) params.get(toGet);
        if (res == null) {
            if (required) {
                throw new DataProviderException(toGet + NOPARAM);
            } else {
                if (log != null) {
                    log.warn(toGet + USEDEFAULT);
                }
                res = def;
            }
        }
        if (log != null) {
            log.debug(toGet+ ": " + res);
        }
        return res;
    }

    public void insurancesUpdate(long opportunityId, HashMap<String, String> update, double level) {
    	// do nothing
	}

	public void update(String itemName, HashMap<String, String> update) {
    	// do nothing
	}

	public void aoCtrlUpdate(long skinId, HashMap<String, String> update) {
    	// do nothing
	}

	public void aoStateUpdate(HashMap<String, String> update, long marketId) {
    	// do nothing
	}

	//we want to send all the updates only with the level as command UPDATE
	public void aoTPSUpdate(long marketId, int scheduled, HashMap<String, String> update) {
    	// do nothing
	}

    /**
     * Option Plus update.
     *
     * @param marketId
     * @param update
     */
    public void optionPlusUpdate(long marketId, HashMap<String, String> update) {
    	// do nothing
    }

    public void binaryZeroOneHundredUpdate(long marketId, long opportunityTypeId, HashMap<String, String> update) {
    	// do nothing
	}

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    public void liveGlobeUpdate(String itemName, Map<String, String> update) {
    	// do nothing
    }

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    public void liveInvestmentsUpdate(String itemName, Map<String, String> update) {
    	// do nothing
    }

    /**
     * Live Globe update.
     *
     * @param itemName
     * @param update
     */
    public void liveTrendsUpdate(String itemName, Map<String, String> update) {
    	// do nothing
    }

    public void ttBinary0100Update(long opportunityId,
    								double autoShiftParameter,
    								double callsAmount,
    								double putsAmount,
    								String feedName,
						    		long decimalPoint,
						    		boolean isDisabled,
						    		boolean isAutoDisabled,
						    		HashMap<String, String> update,
						    		double contractsBought,
						    		double contractsSold,
						    		Object parameters,
						    		double level,
						    		boolean notClosed,
						    		Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
    	if (traderToolBinary0100) {
            HashMap<String, String> tempData = proccessLevelUpdate(
		            										update,
		            										opportunityId,
		            										autoShiftParameter, 
		            										callsAmount,
		            										putsAmount,
		            										feedName,
		            										decimalPoint,
		            										isDisabled,
		            										isAutoDisabled,
		            										true, 
		            										notClosed,
		            										skinGroupMapping);
            BigDecimal bd = new BigDecimal(Double.toString(level));
            tempData.put("etLevel", bd.setScale((int)decimalPoint, BigDecimal.ROUND_HALF_UP).toString());
            bd = new BigDecimal(update.get("BZ_EVENT"));
            tempData.put("BZ_EVENT", bd.setScale((int)decimalPoint, BigDecimal.ROUND_HALF_UP).toString());
            tempData.put("calls", String.valueOf(contractsBought));
            tempData.put("puts", String.valueOf(contractsSold));
//            tempData.put("callsSubPuts", String.valueOf(contractsBought - contractsSold));
            tempData.put("callsSubPuts", String.valueOf(Math.max(contractsBought, contractsSold)));
            tempData.put("BZ_BID", update.get("BZ_BID"));
            tempData.put("BZ_OFFER", update.get("BZ_OFFER"));

            MarketDynamicsQuoteParams qp = (MarketDynamicsQuoteParams) parameters;
            tempData.put("parameter_sp", String.valueOf(qp.getSp()));
            tempData.put("parameter_t", String.valueOf(qp.getT()));
            tempData.put("parameters", parameters.toString());

            listener.update("traderToolBinary0100", tempData, false);
        }
	}

	public void ttUpdate(long opportunityId,
							double autoShiftParameter,
							double callsAmount,
							double putsAmount,	
							String feedName,
							long decimalPoint,
							boolean isDisabled,
							boolean isAutoDisabled,
							HashMap<String, String> update,
							boolean notClosed,
							Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
		if (traderTool) {
            HashMap<String, String> tempData = proccessLevelUpdate(
            										update,
            										opportunityId,
            										autoShiftParameter, 
            										callsAmount,
            										putsAmount,
            										feedName,
            										decimalPoint,
            										isDisabled,
            										isAutoDisabled,
            										false,
            										notClosed,
            										skinGroupMapping);
            listener.update("traderTool", tempData, false);
        }
	}
	
	/**
     * Used to format the est close time (format dd/MM/yy HH:mm) gmt 0.
     *
     * @param val the time to format
     * @return
     */
    protected static String formatDate(Date val, boolean sort) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        if (sort) {
        	dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        }
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        return dateFormat.format(val);
    }

    /**
     * get levels updates for opps from types 3 and 1 and proccess it for the TT
     * @param update the update data from the service
     * @return update data to send to the TT
     */
    public HashMap<String, String> proccessLevelUpdate(
									HashMap<String, String> update,
									long opportunityId,
									double autoShiftParameter,
									double callsAmount,
									double putsAmount,
									String feedName,
									long decimalPoint,
									boolean isDisabledTrader,
									boolean isAutoDisabled,
									boolean isBinary0100,
									boolean notClosed,
									Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
    	String fieldStart = "ET";
    	if (isBinary0100) {
    		fieldStart = "BZ";
    	}
    	HashMap<String, String> data = new HashMap<String, String>();
    	data.put("key", String.valueOf(opportunityId));
        int stateId = Integer.parseInt(update.get(fieldStart + "_STATE"));
        log.trace("got TT update oppId: " + opportunityId);
        String command = update.get("command");

        if ((stateId > 1 && stateId < 10 && stateId != 7 && stateId != 8 && !command.equalsIgnoreCase("delete")) || (stateId == 12 && !command.equalsIgnoreCase("delete"))) { //send update only to opps with data
            data.put("command", "UPDATE");
            if (isBinary0100) {
        		data.put("marketId", update.get("BZ_MARKET_ID"));
        		data.put("scheduled", "1");
            } else {
            	for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
            		data.put(skinGroup.getTTLevelUpdateKey(), update.get(skinGroup.getLevelUpdateKey()));
				}
        		data.put("marketGroupId", update.get("ET_GROUP"));
        		data.put("marketId", update.get("ET_MARKET"));
        		data.put("scheduled", update.get("ET_SCHEDULED"));
            }
            BigDecimal bd = new BigDecimal(String.valueOf(callsAmount/100));
    		data.put("callsAmount", bd.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
    		bd = new BigDecimal(String.valueOf(putsAmount/100));
    		data.put("putsAmount", bd.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
    		bd = new BigDecimal(String.valueOf((callsAmount - putsAmount)/100));
    		data.put("callsAmountSubPutsAmount", bd.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
    		bd = new BigDecimal(String.valueOf((callsAmount + putsAmount)/100));
    		data.put("callsAmountAddPutsAmount", bd.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
    		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    		try {
				data.put("timeLastInvest", formatDate(dateFormat.parse(update.get(fieldStart + "_LAST_INV")), true));
			} catch (ParseException e) {
				log.warn("cant parse time last invest " + update.get(fieldStart + "_LAST_INV"), e);
			}
    		data.put("oppType", update.get("AO_TYPE"));
    		
    		for (Map.Entry<SkinGroup, OpportunitySkinGroupMap> entry : skinGroupMapping.entrySet()) {
				SkinGroup skinGroup = entry.getKey();
				OpportunitySkinGroupMap mapping = entry.getValue();
				data.put(skinGroup.getTTShiftParameterUpdateKey(),
							new BigDecimal(String.valueOf(mapping.getShiftParameter()))
									.multiply(new BigDecimal("100")).stripTrailingZeros().toString());
	    		data.put(skinGroup.getTTMaxExposureUpdateKey(), String.valueOf(mapping.getMaxExposure()));
			}
    		
    		data.put("autoShiftParameter", String.valueOf(autoShiftParameter*100));
    		data.put("isDisabledTrader", isDisabledTrader ? "enable" : "disable");
    		data.put("feedName", String.valueOf(feedName));
    		data.put("isSuspended", stateId == Opportunity.STATE_SUSPENDED ? "resume" : "suspend");
    		data.put("suspendedMessage", update.get(fieldStart + "_SUSPENDED_MESSAGE"));
    		String state = "1";
    		if (isAutoDisabled || isDisabledTrader) {
    			state = "4";
    		} else if (stateId == Opportunity.STATE_SUSPENDED) { // Note the comment on Opportunity.STATE_SUSPENDED doc
    			state = "3";
    		} else if (stateId >= Opportunity.STATE_CLOSING_1_MIN && stateId != Opportunity.STATE_BINARY0100_UPDATING_PRICES) {
    			state = "2";
    		}

			data.put("state", state); // The state suspended is referred to market suspended, not for the new feature opportunity suspend
    		data.put("oppOnlySuspended", update.get("AO_OPP_STATE"));
    		try {
				data.put("timeEstClosing", formatDate(dateFormat.parse(update.get(fieldStart + "_EST_CLOSE")), true));
			} catch (ParseException e) {
				log.warn("cant parse time est close " + update.get(fieldStart + "_EST_CLOSE"), e);
			}
    		data.put("decimalPoint", String.valueOf(decimalPoint));
        } else {
        	data.put("command", String.valueOf("DELETE"));
        }
        return data;
    }

    /**
     * when we settle option we want to remove it from the TT this msg come from the client
     * @param oppId the opp id to remove
     * @param subscribeName the subscribe to send the update to
     */
    public void sendDeleteCommand(long oppId, String subscribeName) {
    	log.info("oppId: " + oppId + " subscribeName " + subscribeName);
    	HashMap<String, String> data = new HashMap<String, String>();
    	data.put("key", String.valueOf(oppId));
    	data.put("command", String.valueOf("DELETE"));
    	listener.update(subscribeName, data, false);
    }

//	class LoadOppJob extends Thread {
//		public void run() {
//			log.debug("run job with traderTool = " + traderTool);
//			boolean isSendSnapShot = true;
//			ArrayList<Integer> oppsList = null;
//			HashMap<Integer, OpportunitiesData> tmpOppsListData = null;
//	        OpportunitiesData oppData = null;
//			int key;
//		    while(traderTool) {
//				tmpOppsListData = new HashMap<Integer, OpportunitiesData>();
//		    	try {
//		    		oppsList = TraderToolManager.loadPublishedOpportunities();
//		    	} catch (SQLException excep) {
//		    		log.error("can't load Published Opportunities", excep);
//		    		sleep();
//		    		continue;
//		    	}
//
////		      	if its not in the new update list mean we need to delete it.
//				for (Iterator<Integer> i = oppsListData.keySet().iterator(); i.hasNext();) {
//                    key = i.next();
//                    if (!oppsList.contains(key)) {
//                    	oppData = oppsListData.get(key);
//                    	sendData(key, oppData, "DELETE");
//                    }
//				}
//
//				if (oppsList.size() > 0) {
//					for (int ind = 0; ind < oppsList.size(); ind++) {
//						final int oppId = oppsList.get(ind);
//						try {
//							oppData = TraderToolManager.getOppDataById(oppId);
//				    	} catch (SQLException excep) {
//				    		log.error("can't get data for opp id: " + oppId + " continue for next opp", excep);
//				    		continue;
//				    	}
//						if (null != oppsListData && oppsListData.containsKey(oppId)) {
//							if (oppData.compareData(oppsListData.get(oppId))) {
//								//need to send data with command update
//								sendData(oppId, oppData, "UPDATE");
//							}
//							//mean we found it in the last update sent we need to keep it. i will remove it bcoz later
//							//i will delete all keys that left in this list and loop over this list to send delete command
//							//log.debug("removing opp id: " + oppId + "from oppsListData");
//							//oppsListData.remove(oppId);
//						} else { //not in the last hashmap mean its new need to send with command update
//							sendData(oppId, oppData, "ADD");
//						}
//						//add it to the current update hashmap
//						tmpOppsListData.put(oppId, oppData);
//					}
//
//					// save the current list as last update list.
//					oppsListData = tmpOppsListData;
//					// send snapShot to all markets if its the first time we run.
//					if (isSendSnapShot) {
//						levelsCache.sendSnapshot(0, 0);
//						isSendSnapShot = false;
//					}
//			    }
//			    sleep();
//		    }
//		}
//
//		private void sleep() {
//			try {
//				log.trace("sleeping for 1 sec");
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//            	log.debug("cant start sleep time",e);
//            }
//		}
//
//		private void sendData(int oppId, OpportunitiesData oppData, String command ) {
//			try {
//                log.trace("sending data to oppId: " + oppId + " command: " + command + " state: " + oppData.getState() + " ao shift " + oppData.getShiftParameterAO());
//				listener.update("traderTool" , (HashMap<String, String>)oppData.dataToSend(oppId, command).clone(), false);
//            } catch (Exception e) {
//            	log.debug("cant send data with oppid " + oppId + " command: " + command, e);
//            }
//		}
//    }

	class CleanupHook extends Thread {
        public void run() {
            if (log.isInfoEnabled()) {
                log.info("CleanupHook start.");
            }
            levelsCache.unregisterJMX();
            levelsCache.close();

            if (log.isInfoEnabled()) {
                log.info("CleanupHook end.");
            }
        }
    }

	@Override
	public void settledOpportunitiesUpdate(SettledOpportunitiesMessage msg) {
		// nothing to do
	}

	@Override
	public void notifyInsurancesStart(long id) {
		// do nothing
		
	}
}