package il.co.etrader.ls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

/**
 * DB utils.
 *
 * @author Tony
 */
public class DBUtil {
    private static Logger log = Logger.getLogger(DBUtil.class);

    private static Object lock = new Object();
    private static DataSource dataSource;
    private static String dbUrl;
    private static String dbUser;
    private static String dbPass;
    private static String dbPoolName;

    /**
     * Init the db connection parameters (dburl, dbuser, dbpass).
     *
     * @param params the <code>Map</code> to take the params from
     */
    public static void init(Map params) {
        dbUrl = (String) params.get("dburl");
        dbUser = (String) params.get("dbuser");
        dbPass = (String) params.get("dbpass");
        dbPoolName = (String) params.get("dbpoolname");
        if (log.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            log.log(Level.DEBUG, ls +
                    "dbUrl: " + dbUrl + ls +
                    "dbUser: liorS " + ls +
                    "dbPass: ********************@:) " + ls +
                    "dbPoolName: " + dbPoolName + ls);
        }
    }

    /**
     * @return The <code>DataSource</code> to obtain db connection from. Create
     *      it if not created.
     * @throws SQLException
     */
    public static DataSource getDataSource() throws SQLException {
        if (null != dataSource) {
            return dataSource;
        }

//        OracleDataSource ods = new OracleDataSource();
//        ods.setURL(dbUrl);
//        ods.setUser(dbUser);
//        ods.setPassword(dbPass);
//        dataSource = ods;
//
//      runATest();

        synchronized (lock) {
            if (null == dataSource) {
                PoolDataSource pool = PoolDataSourceFactory.getPoolDataSource();
                pool.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
                pool.setURL(dbUrl);
                pool.setUser(dbUser);
                pool.setPassword(dbPass);
                pool.setConnectionPoolName(dbPoolName);
                pool.setConnectionWaitTimeout(30);
                pool.setInitialPoolSize(2);
                pool.setMaxPoolSize(10);
                pool.setMaxStatements(20);
                pool.setMinPoolSize(2);
                pool.setInactiveConnectionTimeout(180);
                pool.setTimeToLiveConnectionTimeout(180);
                pool.setAbandonedConnectionTimeout(30);
                pool.setPropertyCycle(30);
                pool.setValidateConnectionOnBorrow(true);
                pool.setSQLForValidateConnection("select 1 from dual");
                dataSource = pool;
            }
        }

        return dataSource;
    }

//    private static void runATest() throws SQLException {
//        Connection conn = null;
//        Statement stmt = null;
//        ResultSet rs = null;
//        try {
//            conn = dataSource.getConnection();
//            stmt = conn.createStatement();
//            rs = stmt.executeQuery("SELECT 7 FROM dual");
//            if (rs.next()) {
//                int rez = rs.getInt(1);
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "DB Connection working. rez = " + rez);
//                }
//            }
//        } finally {
//            closeResultset(rs);
//            closeStatement(stmt);
//            closeConnection(conn);
//        }
//    }

    /**
     * Close db Resultset.
     *
     * @param rs the rs to close
     */
    public static void closeResultset(ResultSet rs) {
        if (null != rs) {
            try {
                rs.close();
            } catch (SQLException sqle) {
                log.log(Level.ERROR, "Can't close Resultset.", sqle);
            }
        }
    }

    /**
     * Close db stmt.
     *
     * @param stmt the db stmt to close
     */
    public static void closeStatement(Statement stmt) {
        if (null != stmt) {
            try {
                stmt.close();
            } catch (SQLException sqle) {
                log.log(Level.ERROR, "Can't close Statement.", sqle);
            }
        }
    }

    /**
     * Close db connection.
     *
     * @param conn the db conn to close
     */
    public static void closeConnection(Connection conn) {
        if (null != conn) {
            try {
                conn.close();
            } catch (SQLException sqle) {
                log.log(Level.ERROR, "Can't close connection.", sqle);
            }
        }
    }
}