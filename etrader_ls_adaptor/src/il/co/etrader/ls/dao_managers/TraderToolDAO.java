///**
// *
// */
//package il.co.etrader.ls.dao_managers;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
///**
// * @author Eyal
// *
// */
//public class TraderToolDAO {
//	private static final Logger log = Logger.getLogger(TraderToolDAO.class);
//
//
//	public static ArrayList<Integer> getPublishedOpportunities(Connection conn) {
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		ArrayList<Integer> list = new ArrayList<Integer>();
//		try {
//			String sql = "SELECT o.id FROM opportunities o WHERE o.is_published = 1 AND o.opportunity_type_id in (1, 3)";
//			pstmt = conn.prepareStatement(sql);
//			rs = pstmt.executeQuery();
//			while(rs.next()) {
//				list.add(rs.getInt("id"));
//			}
//		} catch (SQLException sqle) {
//			log.log(Level.ERROR, "problem load published oppertounties for trader tool", sqle);
//		} catch (Throwable t) {
//			log.log(Level.ERROR, "problem load published oppertounties for trader tool", t);
//		} finally {
//			closeResultSet(rs);
//			closeStatement(pstmt);
//		}
//		return list;
//
//	}
//		
//		public static OpportunitiesData getOppDataById(Connection con,long id) throws SQLException {
//			  ArrayList<Investment> list = new ArrayList<Investment>();
//			  PreparedStatement ps = null;
//			  ResultSet rs = null;
//			  OpportunitiesData oppData = null;
//			  try {
//				    String sql = "SELECT " +
//                                    "i.time_created, " +
//                                    "i.TYPE_ID, " +
//                                    "i.AMOUNT + NVL(t.amount, 0) as amount, " +
//                                    "i.CURRENT_LEVEL, " +
//                                    "i.RATE " +
//    				    		" FROM " +
//                                    "investments i " +
//                                        "LEFT JOIN transactions t ON t.bonus_user_id = i.bonus_user_id AND i.bonus_odds_change_type_id > 1, " +
//                                    "users u " +
//    				    		" WHERE " +
//                                    "i.opportunity_id = ? AND " +
//                                    "i.user_id = u.id AND " +
//                                    "u.class_id > 0 AND " +
//                                    "i.is_canceled = 0 AND " +
//                                    "i.is_settled = 0 " +
//    				    		" ORDER BY " +
//                                    "i.time_created desc";
//
//				    ps = con.prepareStatement(sql);
//					ps.setLong(1, id);
//					rs = ps.executeQuery();
//					oppData = new OpportunitiesData();
//
//					while (rs.next()) {
//						Investment i = new Investment();
//						i.setAmount(rs.getLong("amount"));
//						i.setTimeCreated(rs.getDate("time_created"));
//						i.setOpportunityId(id);
//						i.setTypeId(rs.getLong("type_id"));
//						i.setCurrentLevel(rs.getDouble("current_level"));
//						i.setBaseAmount(Math.round(i.getAmount()*rs.getDouble("rate")));
//						list.add(i);
//					}
//					if (list.size() > 0) {
//						oppData.calculateData(list);
//					}
//
//
//					sql = " SELECT " +
//								"m.id, " +
//								"m.MARKET_GROUP_ID, " +
//								"to_char(o.TIME_LAST_INVEST, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS TIME_LAST_INVEST, " +
//								"o.SCHEDULED, " +
//								"o.OPPORTUNITY_TYPE_ID, " +
//								"to_char(o.TIME_EST_CLOSING, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS TIME_EST_CLOSING, " +
//								"o.SHIFT_PARAMETER, " +
//								"o.SHIFT_PARAMETER_AO, " +
//								"o.AUTO_SHIFT_PARAMETER, " +
//								"o.IS_DISABLED, " +
//								"o.IS_DISABLED_TRADER, " +
//								"o.MAX_EXPOSURE, " +
//								"m.FEED_NAME, " +
//								"m.IS_SUSPENDED, " +
//								"m.SUSPENDED_MESSAGE, " +
//								"(m.DECIMAL_POINT - m.DECIMAL_POINT_SUBTRACT_DIGITS) as DECIMAL_POINT " +
//							" FROM " +
//				    			"markets m, " +
//				    			"opportunities o " +
//			    			" WHERE " +
//				    			"o.id = ? AND " +
//				    			"m.id = o.market_id";
//
//					ps = con.prepareStatement(sql);
//					ps.setLong(1, id);
//					rs = ps.executeQuery();
//					if (rs.next()) {
//
//						oppData.setMarketGroupId(rs.getInt("MARKET_GROUP_ID"));
//						oppData.setMarketId(rs.getInt("id"));
//						String timeLastinvest = rs.getString("TIME_LAST_INVEST");
//						String timeEstClosing = rs.getString("TIME_EST_CLOSING");
//						SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
//						try {
//							oppData.setTimeLastInvest(localDateFormat.parse(timeLastinvest));
//							oppData.setTimeEstClosing(localDateFormat.parse(timeEstClosing));
//						} catch (ParseException e) {
//							log.debug("cant pare time last ivest from db for opp id: " + id,e);
//						}
//						oppData.setScheduled(rs.getInt("SCHEDULED"));
//						oppData.setOppType(rs.getInt("OPPORTUNITY_TYPE_ID"));
//						oppData.setShiftParameter(rs.getBigDecimal("shift_parameter"));
//						oppData.setAutoShiftParameter(rs.getDouble("auto_shift_parameter"));
//						oppData.setDisabledTrader(rs.getBoolean("IS_DISABLED_TRADER"));
//						oppData.setDisabled(rs.getBoolean("is_disabled"));
//						oppData.setFeedName(rs.getString("feed_name"));
//						oppData.setSuspended(rs.getBoolean("is_suspended"));
//						oppData.setSuspendedMessage(rs.getString("suspended_message"));
//						oppData.setMaxExposure(rs.getLong("max_exposure"));
//						oppData.setDecimalPoint(rs.getInt("DECIMAL_POINT"));
//						oppData.setShiftParameterAO(rs.getBigDecimal("shift_parameter_ao"));
//						oppData.setState();
//					}
//
//
//				} finally {
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//				return oppData;
//
//		  }
//
//		public static void closeStatement(final Statement stmt) {
//	        if (stmt != null) {
//	            try {
//	                stmt.close();
//	            } catch (SQLException ex) {
//	            	log.error(stmt, ex);
//	            }
//	        }
//	    }
//
//	    public static void closeResultSet(final ResultSet rs) {
//	        if (rs != null) {
//	            try {
//	                rs.close();
//	            } catch (SQLException ex) {
//	            	log.error(rs, ex);
//	            }
//	        }
//	    }
//}
