package il.co.etrader.ls.dao_managers;

import il.co.etrader.ls.shopping_bag.user.UserOpportunityWrapper;
import oracle.jdbc.OracleTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

public class ShoppingBagDAO extends DAOBase {
	protected static Logger log = Logger.getLogger(ShoppingBagDAO.class);

    private static final String SHOPPING_BAG_USER_OPPS_SQL =
		" SELECT DISTINCT i.user_id, op.id AS opp_id " +
		" FROM investments i " +
			" ,opportunities op " +
		" WHERE i.opportunity_id = op.id " +
			" AND i.is_canceled != 1 " +
			" AND i.user_id IN ( " +
						" SELECT user_id " +
						" FROM temp_users_shoping_bag) " +
			" AND nvl(from_tz(CAST(i.time_settled AS TIMESTAMP), 'UTC'), op.time_est_closing) >= op.time_est_closing " +
			" AND op.time_est_closing = ? ";
    /**
     * Checks if the given users have done investments in all opportunities with
     * the given opportunity closing date. If so, a set of opportunity IDs is
     * created for each user.
     *
     * @param conn
     * @param users
     * @param timeEstClosing
     * @return a map with key user ID and value - a set of opportunity IDs in
     * which each user has done investments.
     * @throws SQLException
     */
    public static Map<Long, Set<Long>> getUserInvestmentsOpp(
    										Connection conn,
    										List<Long> users,
    										Date timeEstClosing) throws SQLException {
        LinkedHashMap<Long, Set<Long>> userOpp = new LinkedHashMap<Long, Set<Long>>();
        CallableStatement stmt = null;
        ResultSet rs = null;
        try {
        	
        	String sql = "{call pkg_shoppingbag.get_users_opportunities(?, ?, ?)}";
        	
            stmt = conn.prepareCall(sql);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.setArray(2, getPreparedSqlArray(conn, users));
            stmt.setTimestamp(3, convertToTimestamp(timeEstClosing));
            
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(1);
            Set<Long> opportunities = null;
            while (rs.next()) {
                long userId = rs.getLong("user_id");
                long oppId = rs.getLong("opp_id");

                opportunities = userOpp.get(userId);
                if (opportunities == null) {
                	opportunities = new LinkedHashSet<Long>();
                	userOpp.put(userId, opportunities);
                }
                opportunities.add(oppId);
            }

        } catch (SQLException e) {
        	if (!conn.getAutoCommit()) {
	        	log.error("Unable to get shopping bag users. Rolling-back transaction", e);
				conn.rollback();
        	}
			throw e;
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
        }
        return userOpp;
    }

    private static final String SHOPPING_BAG_DATA_SQL =
		" SELECT i.user_id " +
				" ,i.opportunity_id " +
				" ,sum(i.amount) AS invest_amount " +
				" ,sum(i.amount + i.win - i.lose) AS return_amount " +
				" ,COUNT(i.id) AS inv_count " +
		" FROM investments i " +
			" ,opportunities op " +
		" WHERE i.opportunity_id = op.id " +
			" AND i.is_canceled != 1 " +
			" AND i.user_id IN ( " +
					" SELECT user_id " +
					" FROM temp_users_shoping_bag) " +
			" AND i.opportunity_id IN (%s) " +
			" AND from_tz(CAST(i.time_settled AS TIMESTAMP), 'UTC') >= op.time_est_closing " +
		" GROUP BY i.user_id " +
			" ,i.opportunity_id ";
    /**
     * Collects all users opportunities amounts of return in
     * <code>shoppingBag</code> parameter.
     *
     * @param conn
     * @param users
     * @param opportunities
     * @param shoppingBag
     * @throws SQLException
     */
    public static void getUserShopingBagData(
							Connection conn,
							Collection<Long> users,
							Collection<Long> opportunities,
							Map<Long, Map<Long, UserOpportunityWrapper>> shoppingBag) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sbOpps = new StringBuilder("0");
        try {
        	conn.setAutoCommit(false);
        	insertUsersInTmpTable(conn, users);

        	Iterator<Long> iter = opportunities.iterator();
        	while(iter.hasNext()) {
        		iter.next();
        		sbOpps.append(",?");
        	}

            String sql = String.format(SHOPPING_BAG_DATA_SQL, sbOpps.toString());
            pstmt = conn.prepareStatement(sql);

            int idx = 1;
            for (Long id : opportunities) {
            	pstmt.setLong(idx++, id);
            }

            rs = pstmt.executeQuery();

            long userId;
            long opportunityId;
            long investAmount;
            long returnAmount;
            String invCount;
            while (rs.next()) {
                userId = rs.getLong("user_id");
                opportunityId = rs.getLong("opportunity_id");
                investAmount = rs.getLong("invest_amount");
                returnAmount = rs.getLong("return_amount");
                invCount = rs.getString("inv_count");

                Map<Long, UserOpportunityWrapper> userBag = shoppingBag.get(userId);
                if (userBag != null) {
                	UserOpportunityWrapper userOpp = userBag.get(opportunityId);
                	if (userOpp != null) {
                		userOpp.setAmountOfInestments(investAmount);
                		userOpp.setAmountOfReturn(returnAmount);
                		userOpp.setWinning(returnAmount > investAmount);
                		userOpp.setInvestmentCount(invCount);
                	}
                }
            }
            conn.commit();
        } catch (SQLException e) {
        	if (!conn.getAutoCommit()) {
	        	log.error("Unable to get shopping bag data. Rolling-back transaction", e);
				conn.rollback();
        	}
			throw e;
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }

    /**
     * Inserts shopping bag users IDs into a temporary table in order to avoid Oracle SQL "IN" clause restriction
     * for 1000 maximum comma separated entries ORA-01795.
     *
     * @param conn connection that must not be in auto commit mode.
     * @param users user IDs to insert.
     * @throws SQLException
     */
    private static void insertUsersInTmpTable(Connection conn, Collection<Long> users) throws SQLException {
    	PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(" INSERT INTO temp_users_shoping_bag (user_id) VALUES (?) ");
            for (long userId : users) {
            	pstmt.setLong(1, userId);
				pstmt.addBatch();
			}
            pstmt.executeBatch();
        } finally {
            closeStatement(pstmt);
        }
    }
}