package il.co.etrader.ls.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;

import il.co.etrader.dao_managers.InvestmentsDAOBase;

public class InvestmentsDAO extends InvestmentsDAOBase {
	private static final Logger logger = Logger.getLogger(InvestmentsDAOBase.class);
    public static ArrayList<Investment> getUserHourInvestments(Connection conn, String userName, int goldenMinStart, int goldenMinEnd) throws SQLException {
        ArrayList<Investment> l = new ArrayList<Investment>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                "SELECT " +
                    "A.*, " +
                    "C.market_id, " +
                    "sys_extract_utc(C.time_est_closing) AS time_est_closing, " +
                    "D.insurance_qualify_percent, " +
                    "D.insurance_premia_percent, " +
                    "D.decimal_point, " +
                    "D.roll_up_qualify_percent, " +
                    "D.roll_up_Premia_Percent, " +
                    "D.is_roll_up, " +
                    "D.is_golden_minutes, " +
                    "E.closing_level," +
                    "CASE WHEN A.type_id = 1 THEN C.gm_level - A.current_level ELSE A.current_level - C.gm_level END as current_ins_qualifyP, " +
                    "gmv.id as is_seen, " +
                    "F.name AS market_name, " +
                    "C.is_open_graph " +
                "FROM " +
                    "investments A " +
                        "LEFT JOIN golden_minutes_viewing gmv ON gmv.investment_id = A.id, " +
                    "users B, " +
                    "opportunities C, " +
                    "market_name_skin F, " +
                    "markets D LEFT JOIN" +
                    "(SELECT " +
                        "DISTINCT M.id, " +
                        "O.closing_level " +
                    "FROM " +
                        "markets M, " +
                        "opportunities O " +
                    "WHERE " +
                        "M.id = O.market_id AND " +
                        "NOT O.closing_level IS NULL AND " +
                        "NOT O.time_act_closing IS NULL AND " +
                        "O.time_act_closing = " +
                            "(SELECT " +
                                "MAX(x.time_act_closing) " +
                            "FROM " +
                                "opportunities X " +
                            "WHERE " +
                                "X.opportunity_type_id = 1 AND " +
                                "X.market_id = O.market_id AND " +
                                "NOT X.closing_level IS NULL AND " +
                                "NOT X.time_act_closing IS NULL AND " +
                                "NOT X.scheduled = " + Opportunity.SCHEDULED_HOURLY + ")) E ON D.id = E.id " +
                "WHERE " +
                    "A.user_id = B.id AND " +
                    "A.opportunity_id = C.id AND " +
                    "C.market_id = D.id AND " +
                    "B.user_name = UPPER(?) AND " +
                    "C.scheduled = 1 AND " +
                    "A.is_settled = 0 AND " +
                    "A.time_created <= C.time_est_closing - to_dsinterval('0 00:' || ? || ':00') AND " +
                    "A.bonus_odds_change_type_id = 0 AND " +
                    "C.opportunity_type_id = 1 AND " +
                    "current_timestamp >= C.time_est_closing - to_dsinterval('0 00:' || ? || ':00') AND " +
                    "current_timestamp <= C.time_est_closing - to_dsinterval('0 00:' || ? || ':00') AND " +
                    "F.market_id = C.market_id AND " +
                    "F.skin_id = B.skin_id";
//                    "to_char(C.time_est_closing, 'MI') = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, userName.toUpperCase());
            pstmt.setString(2, goldenMinStart < 10 ? "0" + goldenMinStart : String.valueOf(goldenMinStart));
            pstmt.setString(3, goldenMinStart < 10 ? "0" + goldenMinStart : String.valueOf(goldenMinStart));
            pstmt.setString(4, goldenMinEnd < 10 ? "0" + goldenMinEnd : String.valueOf(goldenMinEnd));
            rs = pstmt.executeQuery();
            Investment i = null;
            while (rs.next()) {
                i = getVO(rs);
                i.setInsuranceQualifyPercent(rs.getDouble("insurance_qualify_percent"));
                i.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
                i.setMarketId(rs.getLong("market_id"));
                i.setMarketName(rs.getString("market_name"));
                i.setDecimalPoint(rs.getLong("decimal_point"));
                i.setRollUpQualifyPercent(rs.getDouble("roll_up_qualify_percent"));
                i.setRollUp(rs.getInt("is_roll_up") == 1 ? true : false);
                i.setGoldenMinutes((rs.getInt("is_golden_minutes")) == 1 ? true : false);
                i.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
                i.setMarketLastDayClosingLevel(rs.getDouble("closing_level"));
                i.setCurrentInsuranceQualifyPercent(rs.getDouble("current_ins_qualifyP"));
                i.setSeen(rs.getLong("is_seen") > 0);
                i.setOpenGraph(rs.getInt("is_open_graph") == 1 ? true : false);
                i.setOpportunityId(rs.getLong("opportunity_id"));
   				i.setTimeEstClosing((rs.getTimestamp("time_est_closing")));
                l.add(i);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * insert into golden_minutes_viewing table the investment (that mean that the user saw it)
     * @param conn
     * @param invId the investment id the user saw
     * @return true if it was inserted else false
     */
    public static boolean updateInvSeen(Connection conn, long invId) throws SQLException {
        boolean isDone = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "INSERT INTO golden_minutes_viewing " +
                    "( " +
                        "id, " +
                        "user_id, " +
                        "investment_id, " +
                        "type_id, " +
                        "time_created " +
                    ") " +
                "VALUES " +
                    "( " +
                        "seq_golden_minutes_viewing.NEXTVAL, " +
                        "(SELECT user_id FROM investments WHERE id = ?), " +
                        "?, " +
                        "2, " +
                        "sysdate " +
                    ") ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, invId);
            pstmt.setLong(2, invId);
            pstmt.executeUpdate();
            isDone = true;
        } finally {
            closeStatement(pstmt);
        }
        return isDone;
    }
}