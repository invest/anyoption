/**
 * 
 */
package il.co.etrader.ls.shopping_bag;

import java.util.Map;


/**
 * The implementor of this interface are eligible to send shopping bag updates
 * to users.
 * 
 * @author pavelhe
 *
 */
public interface ShoppingBagCacheListener {

	/**
	 * Handles the shopping bag update that should be send.
	 *
	 * @param itemName
	 * @param update
	 */
	public void shoppingBagUpdate(String itemName, Map<String, String> update);

}
