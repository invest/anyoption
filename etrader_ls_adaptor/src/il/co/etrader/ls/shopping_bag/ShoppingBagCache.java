/**
 * 
 */
package il.co.etrader.ls.shopping_bag;

import il.co.etrader.ls.JMSCommandDataAdapter;
import il.co.etrader.ls.shopping_bag.tasks.SettledOpportunitiesTask;
import il.co.etrader.ls.shopping_bag.tasks.SubscriptionHandlerTask;
import il.co.etrader.ls.shopping_bag.tasks.TaskParams;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.msgs.SettledOpportunitiesMessage;
import com.lightstreamer.interfaces.data.DataProvider;


/**
 * The main class of the shopping bag (expiry note) functionality. It is
 * instantiated by {@link JMSCommandDataAdapter} and handles all input and
 * output points.
 * 
 * @author pavelhe
 *
 */
public class ShoppingBagCache {
	
	private static final Logger log = Logger.getLogger(ShoppingBagCache.class);
	
	public static final String ITEM_PREFIX = "shoppingbag";
	public static final String ITEM_NAME_MATCH = ITEM_PREFIX + "_.+";
	
	public static final long FIRST_EXECUTION_DELAY_MILLIS = 30 * 1000L;
	public static final long SETTLED_OPP_FIRST_TIMEOUT_MILLIS = 45L * 1000L;
	public static final long SETTLED_OPP_SECOND_TIMEOUT_MILLIS = 90L * 1000L;
	public static final long POPUP_EXPIRATION_MOBILE_MILLIS = 5L * 1000L;
	public static final long POPUP_EXPIRATION_WEB_MILLIS = 15L * 1000L;
	public static final int CACHE_THREAD_NUMBER = 4;
	
	private final TaskParams taskParams;
	
	private final ScheduledExecutorService executorService;
	
	/**
	 * Constructor
	 * 
	 * @param listener a listener that is interested in all shopping bag output events.
	 */
	public ShoppingBagCache(ShoppingBagCacheListener listener) {
		executorService = Executors.newScheduledThreadPool(CACHE_THREAD_NUMBER, new CacheTreadFactory());
		
		taskParams = new TaskParams(listener, executorService);
		
		executorService.scheduleWithFixedDelay(
							new SubscriptionHandlerTask(taskParams),
							FIRST_EXECUTION_DELAY_MILLIS,
							SubscriptionHandlerTask.EXECUTION_DELAY_MILLIS,
							TimeUnit.MILLISECONDS);
		
		executorService.scheduleWithFixedDelay(
							new SettledOpportunitiesTask(taskParams),
							FIRST_EXECUTION_DELAY_MILLIS,
							SettledOpportunitiesTask.EXECUTION_DELAY_MILLIS,
							TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Handles settled opportunities that come from level service via JMS.
	 * 
	 * @param msg a JMS message containing settled opportunities.
	 */
	public void handleSettledOpportunitiesUpdate(SettledOpportunitiesMessage msg) {
		log.info("Handling settled opportunities [" + msg + "]");
		
		synchronized (taskParams.getJustSettledOpps()) {
			taskParams.getJustSettledOpps().addAll(msg.getSettledOpps());
		}
	}
	
	/**
	 * Handles user subscription to shopping bag.
	 * <p>
	 * Taken from {@link DataProvider#subscribe(String, boolean)}:
	 * <i>
	 * The method should perform as fast as possible. If the implementation 
	 * is slow because of complex subscription activation operations, it
	 * might delay a subsequent unsubscription and resubscription of the
	 * same item. In that case, configuring a dedicated thread pool for this
	 * Data Adapter is recommended, in order not to block operations for
	 * different Data Adapters.
	 * </i>
	 * 
	 * @param itemNameArray
	 * @param itemName
	 * 
	 * @see DataProvider#subscribe(String, boolean)
	 */
	public void subscribe(String [] itemNameArray, String itemName) {
		ShoppingBagUser user = new ShoppingBagUser(itemNameArray, itemName);
		log.debug("Subscribing " + user);
		
		synchronized (taskParams.getSubscriptionMonitor()) {
			taskParams.getJustSubscribedUsers().add(user);
			taskParams.getJustUnsubscribedUsers().remove(user.getId());
		}
	}
	
	/**
	 * Handles user unsubscribe to shopping bag.
	 * <p>
	 * Taken from {@link DataProvider#unsubscribe(String)}:
	 * <i>
	 * The method should perform fast. If the implementation is slow because
	 * of complex housekeeping operations, it might delay a subsequent
	 * subscription of the same item. In that case, configuring a dedicated
	 * thread pool for this Data Adapter is recommended, in order not to
	 * block operations for different Data Adapters.
	 * </i>
	 * 
	 * @param itemNameArray
	 * @param itemName
	 * 
	 * @see DataProvider#unsubscribe(String)
	 */
	public void unsubscribe(String [] itemNameArray, String itemName) {
		ShoppingBagUser user = new ShoppingBagUser(itemNameArray, itemName);
		log.debug("Unsubscribing " + user);
		
		synchronized (taskParams.getSubscriptionMonitor()) {
			if(!taskParams.getJustSubscribedUsers().remove(user)) {
				taskParams.getJustUnsubscribedUsers().add(user.getId());
			}
		}
	}
	
	/**
	 * Handles message sent by a user when shopping bag is closed manually.
	 * 
	 * @param userName taken from lightsreamer session
	 * @param messageArray
	 * @param message
	 */
	public void notifyCloseShoppingBag(String userName, String[] messageArray, String message) {
		log.debug("Close shopping bag notification from user: " + userName);
		
		ShoppingBagUser user = new ShoppingBagUser(messageArray, message);
		if (!user.getUserName().equals(userName)) {
			log.warn("User [" + userName + "] is trying to close shopping bag of antoher " + user);
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		synchronized (taskParams.getTaskMonitor()) {
			if (taskParams.getSubscribedUsers().containsKey(user.getId())) {
				UserShoppingBagData bagData = taskParams.getUserBags().get(user.getId());
				if (bagData != null) {
					UserShoppingBag bag = bagData.getLastSentBag();
					if (bag != null) {
						bagData.setLastSentBag(null);
						if (!bag.isClosed()) {
							bag.sendDeleteSnapshot(taskParams.getListener());
							log.debug("Closed " + bag);
						} else {
							log.warn("Received shopping bag close message for bag that is already closed " + bag);
						}
					} else {
						log.warn("Received shopping bag close message for user that has no bag");
					}
				} else {
					log.warn("Received shopping bag close message for user with missing bag data");
				}
			} else {
				log.warn("Received shopping bag close message for unsubscribed user");
			}
		}
	}
	
	/**
	 * Called when lightstreamer is being shut down, this method stops the
	 * shopping bag task executor.
	 */
	public void shutdown() {
		log.info("Shutting down " + this.getClass().getSimpleName());
		if (executorService != null) {
			executorService.shutdownNow();
		}
		log.info("Done");
	}

}
