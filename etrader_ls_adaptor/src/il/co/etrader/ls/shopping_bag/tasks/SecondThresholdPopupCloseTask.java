/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Responsible for closing all bags shown by
 * {@link SecondThresholdPopupOpenTask}. It also clears all user shopping bag
 * data in cache for the given opportunity closing date.
 * 
 * @author pavelhe
 *
 */
public class SecondThresholdPopupCloseTask extends AbstractTask {

	private final Collection<UserShoppingBag> shownBags;
	private final Date timeEstClosing;
	
	/**
	 * Constructor
	 * 
	 * @param params
	 * @param shownBags
	 * @param timeEstClosing
	 */
	public SecondThresholdPopupCloseTask(TaskParams params, Collection<UserShoppingBag> shownBags, Date timeEstClosing) {
		super(params);
		this.shownBags = shownBags;
		this.timeEstClosing = timeEstClosing;
	}

	@Override
	protected void handle() {
		
		StringBuilder usersWithRemovedBagData = new StringBuilder();
		StringBuilder usersRemovedFromBagCache = new StringBuilder();
		
		synchronized (params.getTaskMonitor()) {
			log.debug("Cleaning bag data and closing user bags: " + shownBags);
			Map<Long, ShoppingBagUser> subscribedUsers = params.getSubscribedUsers();
			Map<Long, UserShoppingBagData> userBags = params.getUserBags();
			ShoppingBagUser user = null;
			for (UserShoppingBag bag : shownBags) {
				user = bag.getUser();
				UserShoppingBagData bagData = userBags.get(user.getId());
				if (bagData != null) {
					if (bag == bagData.getLastSentBag()) {
						bagData.setLastSentBag(null);
					}
					if (!bag.isClosed() && subscribedUsers.containsKey(user.getId())) {
						bag.sendDeleteSnapshot(params.getListener());
					}
					
					if (bagData.getBags().remove(timeEstClosing) != null) {
						if (log.isDebugEnabled()) {
							usersWithRemovedBagData.append(user.getId()).append(",");
						}
					}
					
					if (bagData.getBags().size() == 0) {
						userBags.remove(user.getId());
						if (log.isDebugEnabled()) {
							usersRemovedFromBagCache.append(user.getId()).append(",");
						}
					}
				}
			}
		}
		
		if (log.isDebugEnabled()) {
			if (usersWithRemovedBagData.length() > 0) {
				usersWithRemovedBagData.replace(usersWithRemovedBagData.length() - 1, usersWithRemovedBagData.length(), "");
				log.debug("Removed [" + timeEstClosing + "] bag data for users: " + usersWithRemovedBagData);
			} else {
				log.debug("No user bag data removed for [" + timeEstClosing + "]");
			}
			
			if (usersRemovedFromBagCache.length() > 0) {
				usersRemovedFromBagCache.replace(usersRemovedFromBagCache.length() - 1, usersRemovedFromBagCache.length(), "");
				log.debug("Removed users from bag cache: " + usersRemovedFromBagCache);
			}
		}
	}

}
