/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.ShoppingBagCacheListener;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ScheduledExecutorService;

import com.anyoption.common.beans.OpportunitySettle;

/**
 * An instance of this class is an immutable object that holds references to all
 * shopping bag cache data structures and executor service that are used by
 * the tasks.
 * 
 * @author pavelhe
 *
 */
public class TaskParams {
	private final Object taskMonitor; 
	private final Object subscriptionMonitor;
	private final Set<ShoppingBagUser> justSubscribedUsers;
	private final Set<Long> justUnsubscribedUsers;
	private final Map<Long, ShoppingBagUser> subscribedUsers;
	private final ShoppingBagCacheListener listener;
	private final List<OpportunitySettle> justSettledOpps;
	private final TreeMap<Date, OpportunityGroup> opportunityGroups;
	private final Map<Long, UserShoppingBagData> userBags;
	private final ScheduledExecutorService executorService;
	
	/**
	 * Constructor
	 * 
	 * @param listener
	 * @param executorService
	 */
	public TaskParams(ShoppingBagCacheListener listener, ScheduledExecutorService executorService) {
		this.listener = listener;
		taskMonitor = new Object();
		subscriptionMonitor = new Object();
		justSettledOpps = new LinkedList<OpportunitySettle>();
		opportunityGroups = new TreeMap<Date, OpportunityGroup>();
		justSubscribedUsers = new LinkedHashSet<ShoppingBagUser>();
		justUnsubscribedUsers = new LinkedHashSet<Long>();
		subscribedUsers = new LinkedHashMap<Long, ShoppingBagUser>();
		userBags = new LinkedHashMap<Long, UserShoppingBagData>();
		this.executorService = executorService;
	}

	/**
	 * @return the subscriptionMonitor
	 */
	public Object getSubscriptionMonitor() {
		return subscriptionMonitor;
	}

	/**
	 * @return the justSubscribedUsers
	 */
	public Set<ShoppingBagUser> getJustSubscribedUsers() {
		return justSubscribedUsers;
	}

	/**
	 * @return the justUnsubscribedUsers
	 */
	public Set<Long> getJustUnsubscribedUsers() {
		return justUnsubscribedUsers;
	}

	/**
	 * @return the subscribedUsers
	 */
	public Map<Long, ShoppingBagUser> getSubscribedUsers() {
		return subscribedUsers;
	}

	/**
	 * @return the listener
	 */
	public ShoppingBagCacheListener getListener() {
		return listener;
	}

	/**
	 * @return the opportunityGroups
	 */
	public TreeMap<Date, OpportunityGroup> getOpportunityGroups() {
		return opportunityGroups;
	}

	/**
	 * @return the userBags
	 */
	public Map<Long, UserShoppingBagData> getUserBags() {
		return userBags;
	}

	/**
	 * @return the taskMonitor
	 */
	public Object getTaskMonitor() {
		return taskMonitor;
	}

	/**
	 * @return the justSettledOpps
	 */
	public List<OpportunitySettle> getJustSettledOpps() {
		return justSettledOpps;
	}

	/**
	 * @return the executorService
	 */
	public ScheduledExecutorService getExecutorService() {
		return executorService;
	}
	
}
