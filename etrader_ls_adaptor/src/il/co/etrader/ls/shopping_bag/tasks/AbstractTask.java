/**
 *
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.bl_managers.ShoppingBagManager;
import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserOpportunityWrapper;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagGroup;

import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * This abstract class holds all the logic for a shopping bag task
 *
 * @author pavelhe
 *
 */
public abstract class AbstractTask implements Runnable {

	protected final Logger log;

	protected final TaskParams params;

	/**
	 * Constructor
	 *
	 * @param params
	 */
	public AbstractTask(TaskParams params) {
		log = Logger.getLogger(this.getClass());
		this.params = params;
	}

	@Override
	public void run() {
		log.trace("Starting");
		try {
			handle();
		} catch (Exception e) {
			log.error("Exception while task execution", e);
		}
		log.trace("Finished");
	}

	/**
	 * This method is called when a task instance is executed. Put your main
	 * task logic here.
	 */
	protected abstract void handle();


   /**
	 * Loads shopping bags for the given users and opportunities and sends them
	 * the users.
	 *
	 * @param oppGroup
	 * @param users
	 * @param loadInvestmentsIntersection if <code>true</code> the created
	 * shopping bags will contain the currently settled user opportunities for
	 * the specific closing date. If <code>false</code> a shopping bag will be
	 * created only if all user opportunities  for the specific closing date
	 * are settled.
	 * @return the shopping bags sent to users
	 */
	protected Set<UserShoppingBag> loadUsersOpportunityData(OpportunityGroup oppGroup,
															Collection<ShoppingBagUser> users,
															boolean loadInvestmentsIntersection) {
		log.debug("Loading [" + oppGroup.getTimeEstimatedClosing() + "] user shopping bags for settled opps: " + oppGroup.getSettledOpps().keySet());
		List<Long> newUsers = new LinkedList<Long>();
		Set<Long> usersToLoad = new LinkedHashSet<Long>();
		Set<Long> oppsToLoad = new LinkedHashSet<Long>();
		Set<UserShoppingBag> shownBags = new LinkedHashSet<UserShoppingBag>();
		Map<Long, Map<Long, UserOpportunityWrapper>> shoppingBagsToLoad = new LinkedHashMap<Long, Map<Long, UserOpportunityWrapper>>();
		Map<Long, UserShoppingBagData> userBags = params.getUserBags();

		UserShoppingBagData userBagData = null;
		for (ShoppingBagUser user : users) {
			userBagData = userBags.get(user.getId());
			if (userBagData != null) {
				UserShoppingBagGroup userGroup = userBagData.getBags().get(oppGroup.getTimeEstimatedClosing());
				if (userGroup != null) {
					constructUserBagsToLoad(user.getId(),
												oppGroup,
												userGroup,
												usersToLoad,
												oppsToLoad,
												shoppingBagsToLoad,
												loadInvestmentsIntersection);
				} else {
					newUsers.add(user.getId());
				}
			} else {
				newUsers.add(user.getId());
			}
		}

		if (newUsers.size() > 0) {
			try {
				Map<Long, Set<Long>> usersOpps = ShoppingBagManager.getUserInvestmentsOpp(newUsers, oppGroup.getTimeEstimatedClosing());
				for (Map.Entry<Long, Set<Long>> userOpp : usersOpps.entrySet()) {
					UserShoppingBagData userShoppingBagData = userBags.get(userOpp.getKey());
					if (userShoppingBagData == null) {
						userShoppingBagData = new UserShoppingBagData();
						userBags.put(userOpp.getKey(), userShoppingBagData);
					}
					UserShoppingBagGroup userSBGroup = userShoppingBagData.getBags().get(oppGroup.getTimeEstimatedClosing());
					if (userSBGroup == null) {
						// Should always enter here.
						userSBGroup = new UserShoppingBagGroup(
											params.getSubscribedUsers().get(userOpp.getKey()),
											userOpp.getValue(),
											oppGroup.getFormattedClosingDate());
						userShoppingBagData.getBags().put(oppGroup.getTimeEstimatedClosing(), userSBGroup);
					} else {
						log.warn("New user has shopping bag group [" + userSBGroup + "] for [" + oppGroup.getTimeEstimatedClosing() + "]");
					}

					constructUserBagsToLoad(userOpp.getKey(),
											oppGroup,
											userSBGroup,
											usersToLoad,
											oppsToLoad,
											shoppingBagsToLoad,
											loadInvestmentsIntersection);
				}
			} catch (SQLException e) {
				log.error("Could not load user [" + newUsers + "] opportunities for closing time [" + oppGroup.getTimeEstimatedClosing() + "]", e);
			}
		}

		if (usersToLoad.size() > 0 && oppsToLoad.size() > 0) {
			try {
				ShoppingBagManager.getUserShopingBagData(usersToLoad, oppsToLoad, shoppingBagsToLoad);
				for (Map.Entry<Long, Map<Long, UserOpportunityWrapper>> bagsEntry : shoppingBagsToLoad.entrySet()) {
					UserShoppingBagData userSBData = userBags.get(bagsEntry.getKey());
					UserShoppingBagGroup userSBGroup = userSBData.getBags().get(oppGroup.getTimeEstimatedClosing());
					UserShoppingBag userBag = userSBGroup.addUserOpportunities(bagsEntry.getValue().values());
					if (userBag != null) {
						UserShoppingBag lastSent = userSBData.getLastSentBag();

						if (lastSent != null && !lastSent.isClosed()) {
							lastSent.sendDeleteSnapshot(params.getListener());
						}

						userSBData.setLastSentBag(userBag);
						userBag.sendUpdateSnapshot(params.getListener());
						shownBags.add(userBag);
					} else {
						log.warn("Could not add opportunities to a shopping bag ["
									+ bagsEntry.getValue().values()
									+ "]. They should be already in a shopping bag");
					}
				}
			} catch (SQLException e) {
				log.error("Could not load user opps amount of return data for users [" + usersToLoad + "], opportunities [" + oppsToLoad + "]", e);
			}
		}

		if (shownBags.size() > 0) {
			log.debug("Shown shopping bags : " + shownBags);
		} else {
			log.debug("No shopping bags shown");
		}
		return shownBags;
	}

	/**
	 * Constructs a map of user shopping bag data beans that need to be loaded
	 * from DB before bags are sent to users.
	 *
	 * @param userId
	 * @param oppGroup
	 * @param userGroup
	 * @param usersToLoad
	 * @param oppsToLoad
	 * @param shoppingBagsToLoad the map that is about to be added user bag data beans
	 * @param loadInvestmentsIntersection If <code>false</code> the map will be
	 * added user bags data to load only if all of the currently not sent user
	 * opportunities are settled. If <code>true</code> the map will be added all
	 * not sent user opportunities that are currently settled.
	 */
	protected void constructUserBagsToLoad(long userId,
											OpportunityGroup oppGroup,
											UserShoppingBagGroup userGroup,
											Set<Long> usersToLoad,
											Set<Long> oppsToLoad,
											Map<Long, Map<Long, UserOpportunityWrapper>> shoppingBagsToLoad,
											boolean loadInvestmentsIntersection) {
		if (userGroup.getInvestedOppsNotSent().size() > 0) {
			Set<Long> retainedOpps = new LinkedHashSet<Long>(oppGroup.getSettledOpps().keySet());
			retainedOpps.retainAll(userGroup.getInvestedOppsNotSent());

			if (retainedOpps.size() == userGroup.getInvestedOppsNotSent().size()
					|| (loadInvestmentsIntersection && retainedOpps.size() > 0)) {
				usersToLoad.add(userId);
				oppsToLoad.addAll(retainedOpps);

				Map<Long, UserOpportunityWrapper> userBagToLoad = new LinkedHashMap<Long, UserOpportunityWrapper>();
				shoppingBagsToLoad.put(userId, userBagToLoad);

				for (Long userOppToLoad : retainedOpps) {
					userBagToLoad.put(userOppToLoad, new UserOpportunityWrapper(oppGroup.getSettledOpps().get(userOppToLoad)));
				}
			}
		}
	}
}
