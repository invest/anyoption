/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagGroup;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Responsible for showing shopping bags for users which opportunities are
 * still partially settled even after the first threshold
 * ({@link FirstThresholdPopupOpenTask}). If there are no not shown settled
 * opportunities this task cleans all shopping bag data from cache for the
 * given closing time. This task is executed
 * {@link ShoppingBagCache#SETTLED_OPP_SECOND_TIMEOUT_MILLIS} milliseconds
 * after opportunity closing time.
 * 
 * @author pavelhe
 *
 */
public class SecondThresholdPopupOpenTask extends AbstractTask {

	private final Date timeEstClosing;
	
	/**
	 * Constructor
	 * 
	 * @param params
	 * @param timeEstClosing
	 */
	public SecondThresholdPopupOpenTask(TaskParams params, Date timeEstClosing) {
		super(params);
		this.timeEstClosing = timeEstClosing;
	}

	@Override
	protected void handle() {
		Set<UserShoppingBag> shownBags = null;
		
		boolean haveBags = false;
		StringBuilder usersWithRemovedBagData = new StringBuilder();
		StringBuilder usersRemovedFromBagCache = new StringBuilder();
		
		synchronized (params.getTaskMonitor()) {
			OpportunityGroup oppGroup = params.getOpportunityGroups().remove(timeEstClosing);
			
			if (oppGroup == null) {
				log.error("Opportunity group for [" + timeEstClosing + "] is missing");
				/* !!! WARNING - METHOD EXIT POINT !!! */
				return;
			}
			
			shownBags = loadUsersOpportunityData(oppGroup,
													params.getSubscribedUsers().values(),
													true);
			
			haveBags = shownBags != null && shownBags.size() > 0;
			
			if (!haveBags) {
				log.debug("No shown bags after second threshold for closing date [" + timeEstClosing + "] Cleaning bag data");
				Map<Long, UserShoppingBagData> userBags = params.getUserBags();
				Iterator<Entry<Long, UserShoppingBagData>> iterator = userBags.entrySet().iterator();
				Map<Date, UserShoppingBagGroup> bagsByDate = null;
				Entry<Long, UserShoppingBagData> userBagsEntry = null;
				while (iterator.hasNext()) {
					userBagsEntry = iterator.next();
					bagsByDate = userBagsEntry.getValue().getBags();
					
					if (bagsByDate.remove(timeEstClosing) != null) {
						if (log.isDebugEnabled()) {
							usersWithRemovedBagData.append(userBagsEntry.getKey()).append(",");
						}
					}
					
					if (bagsByDate.size() == 0) {
						iterator.remove();
						if (log.isDebugEnabled()) {
							usersRemovedFromBagCache.append(userBagsEntry.getKey()).append(",");
						}
					}
				}
			}
		}
		
		if (haveBags) {
			params.getExecutorService().schedule(
								new HideSBInMobileTask(params, shownBags),
								ShoppingBagCache.POPUP_EXPIRATION_MOBILE_MILLIS,
								TimeUnit.MILLISECONDS);
			
			params.getExecutorService().schedule(
								new SecondThresholdPopupCloseTask(params, shownBags, timeEstClosing),
								ShoppingBagCache.POPUP_EXPIRATION_WEB_MILLIS,
								TimeUnit.MILLISECONDS);
		} else {
			if (log.isDebugEnabled()) {
				if (usersWithRemovedBagData.length() > 0) {
					usersWithRemovedBagData.replace(usersWithRemovedBagData.length() - 1, usersWithRemovedBagData.length(), "");
					log.debug("Removed [" + timeEstClosing + "] bag data for users: " + usersWithRemovedBagData);
				} else {
					log.debug("No user bag data removed for [" + timeEstClosing + "]");
				}
				
				if (usersRemovedFromBagCache.length() > 0) {
					usersRemovedFromBagCache.replace(usersRemovedFromBagCache.length() - 1, usersRemovedFromBagCache.length(), "");
					log.debug("Removed users from bag cache: " + usersRemovedFromBagCache);
				}
			}
		}
		
	}

}
