/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySettle;

/**
 * Responsible for handling all newly settled opportunities. It checks if
 * currently subscribed users have done investments to the opportunities and
 * sends shopping bags if needed. This is a periodic task with execution delay
 * {@link SettledOpportunitiesTask#EXECUTION_DELAY_MILLIS} milliseconds.
 * 
 * @author pavelhe
 *
 */
public class SettledOpportunitiesTask extends AbstractTask {
	public static final long EXECUTION_DELAY_MILLIS = 1000L;

	/**
	 * Constructor
	 * 
	 * @param params
	 */
	public SettledOpportunitiesTask(TaskParams params) {
		super(params);
	}

	@Override
	protected void handle() {
		List<OpportunitySettle> justSettledOppsSnapshot;
		List<OpportunitySettle> justSettledOpps = params.getJustSettledOpps();
		synchronized (justSettledOpps) {
			justSettledOppsSnapshot = new ArrayList<OpportunitySettle>(justSettledOpps);
			justSettledOpps.clear();
		}
		
		if (justSettledOppsSnapshot.size() <= 0) {
			log.trace("No settled opps in current task run");
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		log.debug("Handling settled opportunities");
		
		Set<UserShoppingBag> shownBags = null;
		
		synchronized (params.getTaskMonitor()) {
			TreeMap<Date, OpportunityGroup> oppGroups = params.getOpportunityGroups();
			
			List<OpportunityGroup> updatedGroups = new LinkedList<OpportunityGroup>();
			for (OpportunitySettle settledOpp : justSettledOppsSnapshot) {
				if (settledOpp.getTimeEstimatedClosing().getTime()
							+ ShoppingBagCache.SETTLED_OPP_SECOND_TIMEOUT_MILLIS
							< System.currentTimeMillis()
						&& settledOpp.getTypeId() != Opportunity.TYPE_ONE_TOUCH) {
					log.warn(settledOpp + " expired. Skipping it");
					continue;
				} else if (settledOpp.getTimeEstimatedClosing().getTime() > System.currentTimeMillis()) {
					log.warn(settledOpp + " settled before its closing time. Skipping it");
					continue;
				}
				
				Date closingTime = settledOpp.getTimeEstimatedClosing();
				
				OpportunityGroup oppGroup = oppGroups.get(closingTime);
				if (oppGroup == null) {
					oppGroup = new OpportunityGroup(settledOpp);
					oppGroups.put(closingTime, oppGroup);
					
					long firstThresholdDelay;
					long secondThresholdDelay;
					if (settledOpp.getTypeId() != Opportunity.TYPE_ONE_TOUCH) {
						long now = System.currentTimeMillis();
						firstThresholdDelay = closingTime.getTime()
												+ ShoppingBagCache.SETTLED_OPP_FIRST_TIMEOUT_MILLIS
												- now;
						secondThresholdDelay = closingTime.getTime()
												+ ShoppingBagCache.SETTLED_OPP_SECOND_TIMEOUT_MILLIS
												- now;
					} else {
						firstThresholdDelay = ShoppingBagCache.SETTLED_OPP_FIRST_TIMEOUT_MILLIS;
						secondThresholdDelay = ShoppingBagCache.SETTLED_OPP_SECOND_TIMEOUT_MILLIS;
					}
					
					if (firstThresholdDelay >= 0) {
						params.getExecutorService().schedule(
													new  FirstThresholdPopupOpenTask(params, closingTime),
													firstThresholdDelay,
													TimeUnit.MILLISECONDS);
						log.info(FirstThresholdPopupOpenTask.class.getSimpleName() + " for closing time [" + closingTime
											+ "] scheduled for execution after [" + firstThresholdDelay + "] milliseconds");
					} else {
						log.warn("Opportunities with close date [" + closingTime + "] settled too late. Skipping first threshold bags");
					}
					params.getExecutorService().schedule(
												new  SecondThresholdPopupOpenTask(params, closingTime),
												secondThresholdDelay,
												TimeUnit.MILLISECONDS);
					
					log.info(SecondThresholdPopupOpenTask.class.getSimpleName() + " for closing time [" + closingTime
										+ "] scheduled for execution after [" + secondThresholdDelay + "] milliseconds");
					
					
				} else {
					oppGroup.addSettledOpp(settledOpp);
				}
				updatedGroups.add(oppGroup);
			}
			
			Map.Entry<Date, OpportunityGroup> lastEntry = oppGroups.lastEntry();
			if (lastEntry == null) {
				log.warn("Opportunity groups map is empty. Will skip subscribed users check");
				/* !!! WARNING - METHOD EXIT POINT !!! */
				return;
			}
			
			OpportunityGroup latestGroup = lastEntry.getValue();
			if (!updatedGroups.contains(latestGroup)) {
				log.warn("Latest group with closing date [" + latestGroup.getTimeEstimatedClosing() + "] not updated. Will skip subscribed users check");
				/* !!! WARNING - METHOD EXIT POINT !!! */
				return;
			}
			
			shownBags = loadUsersOpportunityData(latestGroup, params.getSubscribedUsers().values(), false);
		}
		
		if (shownBags != null && shownBags.size() > 0) {
			params.getExecutorService().schedule(
								new HideSBInMobileTask(params, shownBags),
								ShoppingBagCache.POPUP_EXPIRATION_MOBILE_MILLIS,
								TimeUnit.MILLISECONDS);
			
			params.getExecutorService().schedule(
								new PopupCloseTask(params, shownBags),
								ShoppingBagCache.POPUP_EXPIRATION_WEB_MILLIS,
								TimeUnit.MILLISECONDS);
		}
	}
}
