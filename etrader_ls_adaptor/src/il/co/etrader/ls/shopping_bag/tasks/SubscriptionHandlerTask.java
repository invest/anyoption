/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Responsible for newly subscribed/unsubscribed users. This is a periodic task
 * with execution delay {@link SubscriptionHandlerTask#EXECUTION_DELAY_MILLIS}
 * milliseconds. It removes unsubscibed users from cache and sends newly created
 * users shopping bags if needed.
 * 
 * @author pavelhe
 *
 */
public class SubscriptionHandlerTask extends AbstractTask {
	public static final long EXECUTION_DELAY_MILLIS = 1000L;
	
	/**
	 * Constructor
	 * 
	 * @param params
	 */
	public SubscriptionHandlerTask(TaskParams params) {
		super(params);
	}
	
	@Override
	protected void handle() {
		List<ShoppingBagUser> justSubscribedUsersSnapshot = null;
		List<Long> justUnsubscribedUsersSnapshot = null;
		synchronized (params.getSubscriptionMonitor()) {
			justSubscribedUsersSnapshot = new ArrayList<ShoppingBagUser>(params.getJustSubscribedUsers());
			params.getJustSubscribedUsers().clear();
			justUnsubscribedUsersSnapshot = new ArrayList<Long>(params.getJustUnsubscribedUsers());
			params.getJustUnsubscribedUsers().clear();
		}
		
		Map<Long, ShoppingBagUser> subscribedUsers = params.getSubscribedUsers();
		Set<UserShoppingBag> shownBags = null;
		synchronized (params.getTaskMonitor()) {
			if (justUnsubscribedUsersSnapshot.size() > 0) {
				log.debug("Removing unsubscribed users from cache: " + justUnsubscribedUsersSnapshot);
				subscribedUsers.keySet().removeAll(justUnsubscribedUsersSnapshot);
			}
			
			if (justSubscribedUsersSnapshot.size() > 0) {
				log.debug("Adding subscribed users to cache: " + justSubscribedUsersSnapshot);
				
				Map<Long, UserShoppingBagData> userBags = params.getUserBags();
				UserShoppingBagData userBagData = null;
				StringBuilder previouslyShownBags = new StringBuilder();
				for (ShoppingBagUser user : justSubscribedUsersSnapshot) {
					subscribedUsers.put(user.getId(), user);
					
					userBagData = userBags.get(user.getId());
					if (userBagData != null) {
						UserShoppingBag lastBag = userBagData.getLastSentBag();
						if (lastBag != null && !lastBag.isClosed()) {
							lastBag.sendUpdateSnapshot(params.getListener());
							if (log.isDebugEnabled()) {
								previouslyShownBags.append(lastBag).append(",");
							}
						}
					}
				}
				if (previouslyShownBags.length() > 0) {
					previouslyShownBags.replace(previouslyShownBags.length() - 1, previouslyShownBags.length(), "");
					log.debug("Shown previously shown shopping bags: " + previouslyShownBags);
				}
				
				Entry<Date, OpportunityGroup> latestGroupEntry = params.getOpportunityGroups().lastEntry();
				OpportunityGroup latestGroup = null;
				if (latestGroupEntry != null) {
					latestGroup = latestGroupEntry.getValue();
					shownBags = loadUsersOpportunityData(latestGroup, justSubscribedUsersSnapshot, false);
				} else {
					// No recently settled opportunities
					/* !!! WARNING - METHOD EXIT POINT */
					return;
				}
			} else {
				// No subscribed users 
				/* !!! WARNING - METHOD EXIT POINT */
				return;
			}
		}
		
		if (shownBags != null && shownBags.size() > 0) {
			params.getExecutorService().schedule(
									new HideSBInMobileTask(params, shownBags),
									ShoppingBagCache.POPUP_EXPIRATION_MOBILE_MILLIS,
									TimeUnit.MILLISECONDS);
			
			params.getExecutorService().schedule(
									new PopupCloseTask(params, shownBags),
									ShoppingBagCache.POPUP_EXPIRATION_WEB_MILLIS,
									TimeUnit.MILLISECONDS);
		}
		
	}

}
