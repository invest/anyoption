/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.OpportunityGroup;
import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * This task is responsible for opening shopping bags for all users which not
 * all of their opportunities are settled. It takes only the currently settled
 * opportunities and sends them as shopping bags to users. This task is executed
 * {@link ShoppingBagCache#SETTLED_OPP_FIRST_TIMEOUT_MILLIS} milliseconds after
 * closing time.
 * 
 * @author pavelhe
 *
 */
public class FirstThresholdPopupOpenTask extends AbstractTask {
	
	private final Date timeEstClosing;

	/**
	 * Constructor
	 * 
	 * @param params
	 * @param timeEstClosing
	 */
	public FirstThresholdPopupOpenTask(TaskParams params, Date timeEstClosing) {
		super(params);
		this.timeEstClosing = timeEstClosing;
	}

	@Override
	protected void handle() {
		Set<UserShoppingBag> shownBags = null;
		
		synchronized (params.getTaskMonitor()) {
			OpportunityGroup oppGroup = params.getOpportunityGroups().get(timeEstClosing);
			
			if (oppGroup == null) {
				log.error("Opportunity group for [" + timeEstClosing + "] is missing");
				/* !!! WARNING - METHOD EXIT POINT !!! */
				return;
			}
			
			shownBags = loadUsersOpportunityData(oppGroup,
													params.getSubscribedUsers().values(),
													true);
		}
		
		if (shownBags != null && shownBags.size() > 0) {
			params.getExecutorService().schedule(
								new HideSBInMobileTask(params, shownBags),
								ShoppingBagCache.POPUP_EXPIRATION_MOBILE_MILLIS,
								TimeUnit.MILLISECONDS);
			
			params.getExecutorService().schedule(
								new PopupCloseTask(params, shownBags),
								ShoppingBagCache.POPUP_EXPIRATION_WEB_MILLIS,
								TimeUnit.MILLISECONDS);
		}
	}

}
