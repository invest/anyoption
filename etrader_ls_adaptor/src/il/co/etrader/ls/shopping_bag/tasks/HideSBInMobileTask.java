/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;

import java.util.Map;
import java.util.Set;

/**
 * Responsible for sending mobile close update for shopping bags
 * that have been already sent to users and are not manually closed. This task
 * is executed {@link ShoppingBagCache#POPUP_EXPIRATION_MOBILE_MILLIS} after
 * the shopping bags are shown.
 * 
 * @author pavelhe
 *
 */
public class HideSBInMobileTask extends AbstractTask {
	
	private final Set<UserShoppingBag> bagsToHide;

	/**
	 * Constructor
	 * 
	 * @param params
	 * @param bagsToHide
	 */
	public HideSBInMobileTask(TaskParams params, Set<UserShoppingBag> bagsToHide) {
		super(params);
		this.bagsToHide = bagsToHide;
	}

	@Override
	protected void handle() {
		Map<Long, ShoppingBagUser> subscribedUsers = params.getSubscribedUsers();
		synchronized (params.getTaskMonitor()) {
			log.debug("Hiding user bags in mobile : " + bagsToHide);
			for (UserShoppingBag bag : bagsToHide) {
				if (!bag.isClosed()) {
					bag.markClosedInMobile();
					if (subscribedUsers.containsKey(bag.getUser().getId())) {
						bag.sendUpdateMobileSnapshot(params.getListener());
					}
				}
			}
		}
	}

}
