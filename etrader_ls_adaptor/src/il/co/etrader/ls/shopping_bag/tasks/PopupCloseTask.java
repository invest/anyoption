/**
 * 
 */
package il.co.etrader.ls.shopping_bag.tasks;

import il.co.etrader.ls.shopping_bag.ShoppingBagCache;
import il.co.etrader.ls.shopping_bag.user.ShoppingBagUser;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBag;
import il.co.etrader.ls.shopping_bag.user.UserShoppingBagData;

import java.util.Collection;
import java.util.Map;

/**
 * Responsible for closing all shown shopping bags that are not manually closed.
 * This task is executed {@link ShoppingBagCache#POPUP_EXPIRATION_WEB_MILLIS}
 * after shopping bags are shown.
 * 
 * @author pavelhe
 *
 */
public class PopupCloseTask extends AbstractTask {
	
	private final Collection<UserShoppingBag> shownBags;

	/**
	 * Constructor
	 * 
	 * @param params
	 * @param shownBags
	 */
	public PopupCloseTask(TaskParams params, Collection<UserShoppingBag> shownBags) {
		super(params);
		this.shownBags = shownBags;
	}

	@Override
	protected void handle() {
		synchronized (params.getTaskMonitor()) {
			log.debug("Closing user bags: " + shownBags);
			Map<Long, ShoppingBagUser> subscribedUsers = params.getSubscribedUsers();
			Map<Long, UserShoppingBagData> userBags = params.getUserBags();
			for (UserShoppingBag bag : shownBags) {
				UserShoppingBagData bagData = userBags.get(bag.getUser().getId());
				if (bagData != null && bag == bagData.getLastSentBag()) {
					bagData.setLastSentBag(null);
				}
				if (!bag.isClosed() && subscribedUsers.containsKey(bag.getUser().getId())) {
					bag.sendDeleteSnapshot(params.getListener());
				}
			}
		}
	}

}
