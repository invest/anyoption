/**
 * 
 */
package il.co.etrader.ls.shopping_bag.user;

import com.anyoption.common.beans.OpportunitySettle;

/**
 * An {@link OpportunitySettle} wrapper that holds the amount of return for the
 * wrapped opportunity for a specific user.
 * 
 * @author pavelhe
 *
 */
public class UserOpportunityWrapper {
	
	private final OpportunitySettle settledOpp;
	private long amountOfInestments;
	private long amountOfReturn;
	private boolean winning;
	private String investmentCount;
	
	/**
	 * Constructor
	 * 
	 * @param opp
	 */
	public UserOpportunityWrapper(OpportunitySettle opp) {
		settledOpp = opp;
	}

	/**
	 * @return the amountOfReturn
	 */
	public long getAmountOfReturn() {
		return amountOfReturn;
	}

	/**
	 * @param amountOfReturn the amountOfReturn to set
	 */
	public void setAmountOfReturn(long amountOfReturn) {
		this.amountOfReturn = amountOfReturn;
	}

	/**
	 * @return the investmentCount
	 */
	public String getInvestmentCount() {
		return investmentCount;
	}

	/**
	 * @param investmentCount the investmentCount to set
	 */
	public void setInvestmentCount(String investmentCount) {
		this.investmentCount = investmentCount;
	}

	/**
	 * @return the settledOpp
	 */
	public OpportunitySettle getSettledOpp() {
		return settledOpp;
	}

	/**
	 * @return the amountOfInestments
	 */
	public long getAmountOfInestments() {
		return amountOfInestments;
	}

	/**
	 * @param amountOfInestments the amountOfInestments to set
	 */
	public void setAmountOfInestments(long amountOfInestments) {
		this.amountOfInestments = amountOfInestments;
	}

	/**
	 * @return the winning
	 */
	public boolean isWinning() {
		return winning;
	}

	/**
	 * @param winning the winning to set
	 */
	public void setWinning(boolean winning) {
		this.winning = winning;
	}

}
