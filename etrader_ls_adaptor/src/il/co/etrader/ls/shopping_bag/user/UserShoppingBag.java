/**
 * 
 */
package il.co.etrader.ls.shopping_bag.user;

import il.co.etrader.ls.shopping_bag.ShoppingBagCacheListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * An entity that holds all user specific shopping bag data
 * 
 * @author pavelhe
 *
 */
public class UserShoppingBag {
	private static final Logger log = Logger.getLogger(UserShoppingBag.class);
	
	private final List<UserOpportunityWrapper> userOpportunites;
	private final String formattedClosingDate;
	private final ShoppingBagUser user;
	private long totalAmountOfReturn;
	private String strTotalAmountOfReturn;
	private boolean sent;
	private boolean closedInMobile;
	private boolean closed;
	
	/**
	 * Constructor
	 * 
	 * @param user
	 * @param formattedClosingDate
	 * @param userOpps
	 * @param amountOfReturn
	 */
	public UserShoppingBag(ShoppingBagUser user,
							String formattedClosingDate,
							List<UserOpportunityWrapper> userOpps,
							long amountOfReturn) {
		this.user = user;
		this.formattedClosingDate = formattedClosingDate;
		userOpportunites = userOpps;
		totalAmountOfReturn = amountOfReturn;
		strTotalAmountOfReturn = String.valueOf(totalAmountOfReturn);
		sent = false;
		closed = false;
		closedInMobile = false;
	}
	
	/**
	 * Adds settled opportunities to the shopping bag with a calculated amount
	 * of return for the given opportunities.
	 * 
	 * @param userOpps
	 * @param amountOfReturn amount of return for the given opportunities
	 */
	public void addUserOpportunities(List<UserOpportunityWrapper> userOpps, long amountOfReturn) {
		if (sent) {
			log.error("addUserOpportunities : Shopping bag for user [" + user + "] with opportunities [" + userOpportunites + "] already sent");
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		userOpportunites.addAll(userOpps);
		totalAmountOfReturn += amountOfReturn;
		strTotalAmountOfReturn = String.valueOf(totalAmountOfReturn);
	}

	/**
	 * @return the sent
	 */
	public boolean isSent() {
		return sent;
	}

	/**
	 * @return the closed
	 */
	public boolean isClosed() {
		return closed;
	}

	/**
	 * @return the user
	 */
	public ShoppingBagUser getUser() {
		return user;
	}
	
	/**
	 * Marks the shopping bag as closed in mobile. Used when lightstreamer
	 * update is constructed.
	 */
	public void markClosedInMobile() {
		closedInMobile = true;
	}

	/**
	 * Constructs and sends full shopping bag add/update message to the given listener.
	 * 
	 * @param listener the listener to whom the message will be sent.
	 */
	public void sendUpdateSnapshot(ShoppingBagCacheListener listener) {
		if (closed) {
			log.error("sendUpdateSnapshot : Shopping bag for user [" + user + "] with opportunities [" + userOpportunites + "] already closed");
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		Map<String, String> update;
		for (UserOpportunityWrapper opp : userOpportunites) {
			update = new HashMap<String, String>();
			update.put("key", String.valueOf(opp.getSettledOpp().getId()));
			update.put("command", sent ? "UPDATE" : "ADD");
			update.put("SB_MARKET_ID", String.valueOf(opp.getSettledOpp().getMarketId()));
			update.put("SB_OPP_TYPE", String.valueOf(opp.getSettledOpp().getTypeId()));
			update.put("SB_LEVEL", opp.getSettledOpp().getClosingLevel());
			update.put("SB_AMOUNT", strTotalAmountOfReturn);
			update.put("SB_TIME_CLOSE", formattedClosingDate);
			update.put("SB_CLOSE_MOBILE", closedInMobile ? "1" : "0");
			update.put("SB_INV_COUNT", opp.getInvestmentCount());
			update.put("SB_MARKET_RETURN_AMOUNT", String.valueOf(opp.getAmountOfReturn()));
			update.put("SB_MARKET_WINNING", opp.isWinning() ? "1" : "0");
			
			listener.shoppingBagUpdate(user.getItemName(), update);
		}
		sent = true;
	}
	
	/**
	 * Constructs and sends mobile application update message to the given listener.
	 * 
	 * @param listener the listener to whom the message will be sent.
	 */
	public void sendUpdateMobileSnapshot(ShoppingBagCacheListener listener) {
		if (closed) {
			log.error("sendUpdateMobileSnapshot : Shopping bag for user [" + user + "] with opportunities [" + userOpportunites + "] already closed");
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		if (!sent) {
			log.error("Trying to send close mobile snapshot for [" + this + "] before sending the full snapshot. Will send the full snapshot");
			sendUpdateMobileSnapshot(listener);
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		Map<String, String> update;
		for (UserOpportunityWrapper opp : userOpportunites) {
			update = new HashMap<String, String>();
			update.put("key", String.valueOf(opp.getSettledOpp().getId()));
			update.put("command", "UPDATE");
			update.put("SB_CLOSE_MOBILE", closedInMobile ? "1" : "0");
			
			listener.shoppingBagUpdate(user.getItemName(), update);
		}
	}
	
	/**
	 * Constructs and sends delete message to the given listener.
	 * 
	 * @param listener the listener to whom the message will be sent.
	 */
	public void sendDeleteSnapshot(ShoppingBagCacheListener listener) {
		if (closed) {
			log.error("sendDeleteSnapshot : Shopping bag for user [" + user + "] with opportunities [" + userOpportunites + "] already closed");
			/* !!! WARNING - METHOD EXIT POINT !!! */
			return;
		}
		
		Map<String, String> update;
		for (UserOpportunityWrapper opp : userOpportunites) {
			update = new HashMap<String, String>();
			update.put("key", String.valueOf(opp.getSettledOpp().getId()));
			update.put("command", "DELETE");
			listener.shoppingBagUpdate(user.getItemName(), update);
		}
		closed = true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ShoppingBag[opportunites=");
		builder.append("[");
		if (userOpportunites.size() > 0) {
			for (UserOpportunityWrapper opp : userOpportunites) {
				builder.append(opp.getSettledOpp().getId()).append(",");
			}
			builder.replace(builder.length() - 1, builder.length(), "");
		}
		builder.append("]");
		builder.append(", closingDate=");
		builder.append(formattedClosingDate);
		builder.append(", user=");
		builder.append(user.getId());
		builder.append(", amountOfReturn=");
		builder.append(strTotalAmountOfReturn);
		builder.append(", sent=");
		builder.append(sent);
		builder.append(", closedInMobile=");
		builder.append(closedInMobile);
		builder.append(", closed=");
		builder.append(closed);
		builder.append("]");
		return builder.toString();
	}

}
