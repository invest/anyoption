/**
 * 
 */
package il.co.etrader.ls.shopping_bag.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * Holds a collection of user's shopping bags for a specific closing date.
 * Also, contains a list of user's opportunities that have not been added to a
 * shopping bag yet.
 * 
 * @author pavelhe
 *
 */
public class UserShoppingBagGroup {
	
	private static final Logger log = Logger.getLogger(UserShoppingBagGroup.class);

	private final List<UserShoppingBag> shoppingBags;
	private final String formattedClosingDate;
	private final Set<Long> investedOppsNotSent;
	private final ShoppingBagUser user;
	
	/**
	 * Constructor
	 * 
	 * @param user
	 * @param investedOpps
	 * @param formattedClosingDate
	 */
	public UserShoppingBagGroup(ShoppingBagUser user,
								Set<Long> investedOpps,
								String formattedClosingDate) {
		this.user = user;
		this.shoppingBags = new ArrayList<UserShoppingBag>();
		this.formattedClosingDate = formattedClosingDate;
		this.investedOppsNotSent = investedOpps;
	}

	/**
	 * Adds the user opportunities to an already existing or a newly created
	 * shopping bag. It also calculates the total amount of return for the added
	 * opportunities.
	 * 
	 * @param userOpps
	 * @return the shopping bag that holds the added opportunities or
	 * <code>null</code> if the opportunities could not be added.
	 */
	public UserShoppingBag addUserOpportunities(Collection<UserOpportunityWrapper> userOpps) {
		List<UserOpportunityWrapper> oppsLeft = new LinkedList<UserOpportunityWrapper>();
		long amountOfReturn = 0L;
		for (UserOpportunityWrapper userOpportunityWrapper : userOpps) {
			if (investedOppsNotSent.remove(userOpportunityWrapper.getSettledOpp().getId())) {
				oppsLeft.add(userOpportunityWrapper);
				amountOfReturn += userOpportunityWrapper.getAmountOfReturn();
			}
		}
		if (oppsLeft.size() > 0) {
			for (UserShoppingBag bag : shoppingBags) {
				if (!bag.isSent()) {
					// Should never enter here
					log.warn("Adding [" + oppsLeft + "] to existing " + bag);
					bag.addUserOpportunities(oppsLeft, amountOfReturn);
					return bag;
				}
			}
			UserShoppingBag bag = new UserShoppingBag(user, formattedClosingDate, oppsLeft, amountOfReturn);
			shoppingBags.add(bag);
			return bag;
		}
		
		return null;
	}
	
	/**
	 * @return the investedOppsNotSent
	 */
	public Set<Long> getInvestedOppsNotSent() {
		return investedOppsNotSent;
	}
	
}
