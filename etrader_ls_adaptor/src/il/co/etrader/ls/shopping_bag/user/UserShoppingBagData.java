/**
 * 
 */
package il.co.etrader.ls.shopping_bag.user;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Holds a map of user's shopping bags split by closing date and a reference to
 * the last sent shopping bag.
 * 
 * @author pavelhe
 *
 */
public class UserShoppingBagData {
	
	private final Map<Date, UserShoppingBagGroup> bags;
	private UserShoppingBag lastSentBag;
	
	/**
	 * Constructor
	 */
	public UserShoppingBagData() {
		bags = new LinkedHashMap<Date, UserShoppingBagGroup>();
	}
	
	/**
	 * @return the lastSentBag
	 */
	public UserShoppingBag getLastSentBag() {
		return lastSentBag;
	}
	/**
	 * @param lastSentBag the lastSentBag to set
	 */
	public void setLastSentBag(UserShoppingBag lastSentBag) {
		this.lastSentBag = lastSentBag;
	}
	/**
	 * @return the bags
	 */
	public Map<Date, UserShoppingBagGroup> getBags() {
		return bags;
	}

}
