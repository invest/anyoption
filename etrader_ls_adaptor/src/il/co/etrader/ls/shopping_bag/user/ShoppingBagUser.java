/**
 * 
 */
package il.co.etrader.ls.shopping_bag.user;

import java.util.Arrays;

/**
 * An immutable representation of a shopping bag user.
 * 
 * @author pavelhe
 *
 */
public class ShoppingBagUser {
	
	private final Long id;
	private final String userName;
	private final String itemName;
	private final String stringRep;
	
	/**
	 * Constructor
	 * 
	 * @param itemNameArray
	 * @param itemName
	 */
	public ShoppingBagUser(String [] itemNameArray, String itemName) {
		if (itemNameArray.length > 3) {
			this.userName = String.join("_", Arrays.copyOfRange(itemNameArray, 1, itemNameArray.length - 1));
		} else {
			this.userName = itemNameArray[1];
		}
		this.id = Long.parseLong(itemNameArray[itemNameArray.length - 1]);
		this.itemName = itemName;
		
		StringBuilder sb = new StringBuilder();
		stringRep = sb.append("User[")
						.append(itemName.substring(itemName.indexOf("_") + 1, itemName.length()))
						.append("]")
						.toString();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return id.equals(obj);
	}

	@Override
	public String toString() {
		return stringRep;
	}
		
}
