/**
 * 
 */
package il.co.etrader.ls.shopping_bag;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import com.anyoption.common.beans.OpportunitySettle;

/**
 * Holds all settled opportunities for a specific closing date.
 * 
 * @author pavelhe
 *
 */
public class OpportunityGroup {
	
	private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm";
	private static final SimpleDateFormat DATE_FORMATTER;
	
	static {
		DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);
		DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	
	private final Map<Long, OpportunitySettle> settledOpps;
	
	private final Date timeEstimatedClosing;
	private final String formattedClosingDate;
	
	/**
	 * Constructor
	 * 
	 * @param settledOpp
	 */
	public OpportunityGroup(OpportunitySettle settledOpp) {
		timeEstimatedClosing = settledOpp.getTimeEstimatedClosing();
		formattedClosingDate = DATE_FORMATTER.format(timeEstimatedClosing);
		settledOpps = new LinkedHashMap<Long, OpportunitySettle>();
		settledOpps.put(settledOpp.getId(), settledOpp);
	}
	
	/**
	 * @param oppToAdd
	 */
	public void addSettledOpp(OpportunitySettle oppToAdd) {
		this.settledOpps.put(oppToAdd.getId(), oppToAdd);
	}
	
	/**
	 * @return the settledOpps
	 */
	public Map<Long, OpportunitySettle> getSettledOpps() {
		return settledOpps;
	}

	/**
	 * @return the timeEstimatedClosing
	 */
	public Date getTimeEstimatedClosing() {
		return timeEstimatedClosing;
	}

	/**
	 * @return the formattedClosingDate
	 */
	public String getFormattedClosingDate() {
		return formattedClosingDate;
	}

}
