/**
 * 
 */
package il.co.etrader.ls.shopping_bag;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Thread factoru for the shopping bag scheduled executor service.
 * 
 * @author pavelhe
 *
 */
public class CacheTreadFactory implements ThreadFactory {

	private static final AtomicInteger THREAD_NUM = new AtomicInteger(0);
	private static final String THREAD_PREFIX = "SHOPPING-BAG-";

	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r);
		thread.setName(THREAD_PREFIX + THREAD_NUM.getAndIncrement());
		return thread;
	}

}
