package il.co.etrader.ls;

import java.util.Comparator;

import com.anyoption.common.beans.Investment;

import il.co.etrader.util.ConstantsBase;

public class InvestmentsInTheMoneyComparator implements Comparator<Investment> {
    public int compare(Investment i1, Investment i2) {
        if (i1.getInsuranceType() == ConstantsBase.INSURANCE_GOLDEN_MINUTES) {
            if (i1.getCurrentInsuranceQualifyPercent() > i2.getCurrentInsuranceQualifyPercent()) {
                return -1;
            }
            if (i1.getCurrentInsuranceQualifyPercent() < i2.getCurrentInsuranceQualifyPercent()) {
                return 1;
            }
        } else {
            if (i1.getTimeCreated().before(i2.getTimeCreated())) {
                return -1;
            }
            if (i1.getTimeCreated().after(i2.getTimeCreated())) {
                return 1;
            }
        }
        return (i1.getId() - i2.getId()) > 0 ? 1 : -1;
    }
}