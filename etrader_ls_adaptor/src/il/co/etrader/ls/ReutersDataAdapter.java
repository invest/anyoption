//package il.co.etrader.ls;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.net.InetAddress;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.StringTokenizer;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.lightstreamer.interfaces.data.DataProvider;
//import com.lightstreamer.interfaces.data.DataProviderException;
//import com.lightstreamer.interfaces.data.FailureException;
//import com.lightstreamer.interfaces.data.ItemEventListener;
//import com.lightstreamer.interfaces.data.SubscriptionException;
//import com.reuters.rfa.common.Client;
//import com.reuters.rfa.common.Context;
//import com.reuters.rfa.common.DispatchException;
//import com.reuters.rfa.common.Event;
//import com.reuters.rfa.common.EventQueue;
//import com.reuters.rfa.common.Handle;
//import com.reuters.rfa.common.QualityOfServiceRequest;
//import com.reuters.rfa.common.StandardPrincipalIdentity;
//import com.reuters.rfa.common.TokenizedPrincipalIdentity;
//import com.reuters.rfa.session.MarketDataItemSub;
//import com.reuters.rfa.session.MarketDataSubscriber;
//import com.reuters.rfa.session.MarketDataSubscriberInterestSpec;
//import com.reuters.rfa.session.Session;
//import com.reuters.rfa.session.event.ConnectionEvent;
//import com.reuters.rfa.session.event.EntitlementsAuthenticationEvent;
//import com.reuters.rfa.session.event.MarketDataItemEvent;
//import com.reuters.rfa.session.event.MarketDataSvcEvent;
//import com.reuters.tibmsg.TibException;
//import com.reuters.tibmsg.TibField;
//import com.reuters.tibmsg.TibMsg;
//
//public class ReutersDataAdapter implements DataProvider, Runnable {
//    private static final Logger log = Logger.getLogger(ReutersDataAdapter.class);
//
//    private ItemEventListener listener;
//
//    private final HashMap subscribedItems = new HashMap();
//
//    // Config
//    String sessionName;
//    String serviceName;
//    String userName;
//    ArrayList itemsSupported;
//    
//    //RFA objects
//    protected StandardPrincipalIdentity _standardPI;
//    protected TokenizedPrincipalIdentity _tokenizedPI;
//    protected Session session;
//    protected EventQueue eventQueue;
//    protected MarketDataSubscriber marketDataSubscriber;
//    protected Handle mdsClientHandle;
////    protected Handle itemHandle;
//
//    private int opportunityId;
//    private int opportunityValue;
//    private long opportunityStartTime;
//    private long opportunityLastTick;
//
//    public void init(Map params, File configDir) throws DataProviderException {
//        sessionName = (String) params.get("sessionName");
//        serviceName = (String) params.get("serviceName");
//        userName = (String) params.get("userName");
//        StringTokenizer st = new StringTokenizer((String) params.get("itemsList"), ",");
//        itemsSupported = new ArrayList();
//        while (st.hasMoreTokens()) {
//            itemsSupported.add(st.nextToken().trim());
//        }
//        
//        session = Session.acquire(sessionName);
//        if (null == session) {
//            throw new DataProviderException("Could not acquire session.");
//        }
//
//        //Create an object to receive event callbacks
////        ETraderClient client = new ETraderClient();
//
//        //Create an Event Queue
//        eventQueue = EventQueue.create("etraderEventQueue");
//
//        String defaultPosition = "1.1.1.1/net";
//        try {
//            defaultPosition = InetAddress.getLocalHost().getHostAddress() + "/" + InetAddress.getLocalHost().getHostName();
//        } catch(Exception e) {
//            throw new DataProviderException("Can't prepare position: " + e.toString());
//        }
//        
//        // Create pricipal
//        StandardPrincipalIdentity standardPI = new StandardPrincipalIdentity();
//        standardPI.setName(userName);
//        standardPI.setPosition(defaultPosition);
//        standardPI.setAppName("256");
//        
//        //Create a MarketDataSubscriber event source
//        marketDataSubscriber = session.createMarketDataSubscriber("etraderMarketDataSubscriber", true, standardPI);
//
//        //Register for service and connection status
//        MarketDataSubscriberInterestSpec marketDataSubscriberInterestSpec = new MarketDataSubscriberInterestSpec();
//        marketDataSubscriberInterestSpec.setMarketDataSvcInterest(true);
//        marketDataSubscriberInterestSpec.setConnectionInterest(true);
//        marketDataSubscriberInterestSpec.setEntitlementsInterest(true);
//        mdsClientHandle = marketDataSubscriber.registerClient(eventQueue, marketDataSubscriberInterestSpec, new ETraderClient(), null);
//        
//        Thread dispatcher = new Thread(this);
//        dispatcher.setDaemon(true);
//        dispatcher.start();
//    }
//
//    public void setListener(ItemEventListener listener) {
//        this.listener = listener;
//    }
//
//    public void subscribe(String itemName, boolean needsIterator) throws SubscriptionException, FailureException {
//        if (!itemsSupported.contains(itemName)) {
//            throw new SubscriptionException("Unsupported item: " + itemName);
//        }
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Subscribing for: " + itemName);
//        }
//        if (needsIterator) {
//            // OK, as we will always send HashMap objects
//        }
//
//        Handle itemHandle = null;
//        if (!itemName.equalsIgnoreCase("opportunities")) {
//            // Set the QOS request information
//            QualityOfServiceRequest qosr = new QualityOfServiceRequest();
//            qosr.setBestRate(0);
//            qosr.setBestTimeliness(0);
//            qosr.setWorstRate(2147483647);
//            qosr.setWorstTimeliness(2147483647);
//    //        if (dynamicStream) {
//    //             qosr.setStreamProperty(QualityOfServiceRequest.DYNAMIC_STREAM);
//    //        }
//    
//    
//            if (log.isEnabledFor(Level.INFO)) {
//                log.log(Level.INFO, "Subscribing to " + itemName);
//            }
//            MarketDataItemSub marketDataItemSub = new MarketDataItemSub();
//            marketDataItemSub.setItemName(itemName);
//            marketDataItemSub.setServiceName(serviceName);
//            marketDataItemSub.setRequestedQualityOfService(qosr);
//            //check for any specified field names to set
//    //        if(!fieldNames.isEmpty()) {
//    //          marketDataItemSub.setFieldNames(fieldNames);
//    //        }
//    
//            itemHandle = marketDataSubscriber.subscribe(eventQueue, marketDataItemSub, new ETraderClient(), null);
//        } else {
//            opportunityId = 0;
//            opportunityValue = 0;
//            opportunityStartTime = 0;
//            opportunityLastTick = 0;
//        }
//
//        synchronized (subscribedItems) {
//            subscribedItems.put(itemName, new ItemHelper(false, itemHandle));
//        }
//    }
//
//    public boolean isSnapshotAvailable(String itemName) throws SubscriptionException {
//        return true;
//    }
//
//    public void unsubscribe(String itemName) throws SubscriptionException, FailureException {
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Unsubscribing from " + itemName);
//        }
//        ItemHelper helper = null;
//        synchronized (subscribedItems) {
//            helper = (ItemHelper) subscribedItems.remove(itemName);
//        }
//        if (null != helper && null != helper.getHandle()) {
//            marketDataSubscriber.unsubscribe(helper.getHandle());
//        }
//    }
//
//    public void run() {
//        //Dispatch item events
//        while (true) {
//            try {
//                eventQueue.dispatch(100);
//            } catch (DispatchException de) {
//                log.log(Level.ERROR, "Queue deactivated", de);
//                break;
//            }
//            
//            synchronized(subscribedItems) {
//                if (subscribedItems.containsKey("opportunities")) {
//                    // command mode test
//                    if (System.currentTimeMillis() - opportunityStartTime > 60000) { // 1 min
//                        if (log.isEnabledFor(Level.DEBUG)) {
//                            log.log(Level.DEBUG, "tack");
//                        }
//                        if (opportunityId > 0) {
//                            HashMap delCmd = new HashMap();
//                            delCmd.put("key", "opportunity" + opportunityId);
//                            delCmd.put("command", "DELETE");
//
//                            listener.update("opportunities", delCmd, false);
//                        }
//                        opportunityId++;
//                        opportunityValue = 1;
//                        opportunityStartTime = System.currentTimeMillis();
//                        opportunityLastTick = opportunityStartTime;
//                        
//                        HashMap addCmd = new HashMap();
//                        addCmd.put("key", "opportunity" + opportunityId);
//                        addCmd.put("command", "ADD");
//                        addCmd.put("name", "opportunity" + opportunityId);
//                        addCmd.put("value", String.valueOf(opportunityValue));
//                        listener.update("opportunities", addCmd, false);
//                    } else if (System.currentTimeMillis() - opportunityLastTick > 5000) { // 5 sec
//                        if (log.isEnabledFor(Level.DEBUG)) {
//                            log.log(Level.DEBUG, "tick");
//                        }
//                        opportunityValue++;
//                        opportunityLastTick = System.currentTimeMillis();
//                        
//                        HashMap updateCmd = new HashMap();
//                        updateCmd.put("key", "opportunity" + opportunityId);
//                        updateCmd.put("command", "UPDATE");
//                        updateCmd.put("name", "opportunity" + opportunityId);
//                        updateCmd.put("value", String.valueOf(opportunityValue));
//                        listener.update("opportunities", updateCmd, false);
//                    }
//                }
//            }
//        }
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.WARN, Context.string());
//            log.log(Level.WARN, "Daemon " + getClass().toString() + " exiting");
//        }
//    }
//    
//    public void cleanup() {
//        marketDataSubscriber.unsubscribeAll();
//        marketDataSubscriber.unregisterClient(mdsClientHandle);
//        marketDataSubscriber.destroy();
//        eventQueue.deactivate();
//        session.release();
//        Context.uninitialize();
//    }
//
//    class ETraderClient implements Client {
//        private DecimalFormat numberFormat = new DecimalFormat("#,##0.000000");
//        
//        public void processEvent(Event event) {
//            switch (event.getType()) {
//            case Event.MARKET_DATA_ITEM_EVENT:
//                processMarketDataItemEvent((MarketDataItemEvent) event);
//                break;
//            case Event.MARKET_DATA_SVC_EVENT:
//                processMarketDataSvcEvent((MarketDataSvcEvent) event);
//                break;
//            case Event.CONNECTION_EVENT:
//                processConnectionEvent((ConnectionEvent) event);
//                break;
//            case Event.COMPLETION_EVENT:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Received COMPLETION_EVENT for handle " + event.getHandle());
//                }
//                break;
//            case Event.ENTITLEMENTS_AUTHENTICATION_EVENT:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Received ENTITLEMENTS_AUTHENTICATION_EVENT: " + ((EntitlementsAuthenticationEvent)event).getStatus());
//                }
//                break;
//            default:
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.INFO, "Unhandled event type: " + event.getType());
//                }
//                break;
//            }
//        }
//
//        protected void processMarketDataSvcEvent(MarketDataSvcEvent marketDataSvcEvent) {
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Received MARKET_DATA_SVC_EVENT: " + marketDataSvcEvent);
//            }
//        }
//
//        protected void processConnectionEvent(ConnectionEvent connectionEvent) {
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Received CONNECTION_EVENT: " + connectionEvent.getConnectionName() + " " + connectionEvent.getConnectionStatus().toString());
//            }
//        }
//
//        protected void processMarketDataItemEvent(MarketDataItemEvent marketDataItemEvent) {
//            if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.IMAGE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.UNSOLICITED_IMAGE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.UPDATE ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.CORRECTION ||
//                    marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.CLOSING_RUN) {
//                byte[] data = marketDataItemEvent.getData();
//                int length = (data != null ? data.length : 0);
//
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (data), service = " +
//                            marketDataItemEvent.getServiceName() + ", dataFormat = " +
//                            marketDataItemEvent.getDataFormat()+ ", item = " +
//                            marketDataItemEvent.getItemName() + ", status = " +
//                            marketDataItemEvent.getStatus() + ", datasize = " +
//                            length);
//                }
//                if (length == 0) {
//                    return;
//                }
//
//                synchronized (subscribedItems) {
//                    if (subscribedItems.containsKey(marketDataItemEvent.getItemName())) {
//                        TibMsg tibMsg = new TibMsg();
//                        try {
//                            tibMsg.UnPack(data);
//                            if (log.isEnabledFor(Level.DEBUG)) {
//                                String msg = null;
//                                try {
//                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                    OutputStreamWriter osw = new OutputStreamWriter(baos);
//                                    PrintWriter pw = new PrintWriter(osw);
//                                    tibMsg.Print(pw);
//                                    pw.flush();
//                                    msg = new String(baos.toByteArray());
//                                    pw.close();
//                                    osw.close();
//                                    baos.close();
//                                } catch (Exception e) {
//                                    log.log(Level.ERROR, "Can't get the message.", e);
//                                }
//                                log.log(Level.DEBUG, msg);
//                            }
//                            HashMap fields = new HashMap();
//                            TibField field = new TibField();
//                            int err = field.First(tibMsg);
//                            while (err == TibMsg.TIBMSG_OK) {
//                                fields.put(field.Name(), formatValue(field.Data()));
//                                err = field.Next();
//                            }
//                            if (log.isEnabledFor(Level.DEBUG)) {
////                                String ls = System.getProperty("line.separator");
////                                StringBuffer sb = new StringBuffer(ls);
////                                for (Iterator i = fields.keySet().iterator(); i.hasNext();) {
////                                    String key = (String) i.next();
////                                    sb.append(key).append(" ").append(fields.get(key)).append(ls);
////                                }
////                                log.log(Level.DEBUG, sb.toString());
//                                for (Iterator i = fields.keySet().iterator(); i.hasNext();) {
//                                    String key = (String) i.next();
//                                    log.log(Level.DEBUG, key + " " + fields.get(key));
//                                }
//                            }
//                            ItemHelper helper = (ItemHelper) subscribedItems.get(marketDataItemEvent.getItemName());
//                            
//                            listener.update(marketDataItemEvent.getItemName(), fields, !helper.isSnapshotReceived());
//                            if (!helper.isSnapshotReceived()) {
//                                helper.setSnapshotReceived(true);
//                            }
//                        } catch (TibException te) {
//                            log.log(Level.ERROR, "Unable to unpack data with TibMsg. Displaying hex.");
//                        }
//                    }
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.STATUS) {
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (status), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            marketDataItemEvent.getItemName() + ", status = " +
//                            marketDataItemEvent.getStatus().toString());
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.GROUP_CHANGE) {
//                // GROUP_CHANGE
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT( group_merge ), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            marketDataItemEvent.getItemName() + ", new group ID = " +
//                            marketDataItemEvent.getGroupID());
//                }
//            } else if (marketDataItemEvent.getMarketDataMsgType() == MarketDataItemEvent.RENAME) {
//                // RENAME
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT( rename ), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            marketDataItemEvent.getItemName() + ", new item name = " +
//                            marketDataItemEvent.getNewItemName());
//                }
//            } else {
//                //PERMISSION_DATA,
//                if (log.isEnabledFor(Level.DEBUG)) {
//                    log.log(Level.DEBUG, "Received MARKET_DATA_ITEM_EVENT (misc), service = " +
//                            marketDataItemEvent.getServiceName() + ", item = " +
//                            marketDataItemEvent.getItemName());
//                }
//            }
//        }
//        
//        private String formatValue(Object obj) {
//            if (obj instanceof String) {
//                return (String) obj;
//            } else if (obj instanceof Double) {
//                return numberFormat.format(((Double) obj).doubleValue());
//            } else {
//                return obj.toString();
//            }
//        }
//    }
//
//    /**
//     * Helper class to keep the item snapshot state and handle.
//     * 
//     * @author Tony
//     */
//    class ItemHelper {
//        private boolean snapshotReceived;
//        private Handle handle;
//        
//        public ItemHelper(boolean snapshotReceived, Handle handle) {
//            this.snapshotReceived = snapshotReceived;
//            this.handle = handle;
//        }
//        
//        /**
//         * @return the snapshotReceived
//         */
//        public boolean isSnapshotReceived() {
//            return snapshotReceived;
//        }
//        
//        /**
//         * @param snapshotReceived the snapshotReceived to set
//         */
//        public void setSnapshotReceived(boolean snapshotReceived) {
//            this.snapshotReceived = snapshotReceived;
//        }
//
//        /**
//         * @return the handle
//         */
//        public Handle getHandle() {
//            return handle;
//        }
//
//        /**
//         * @param handle the handle to set
//         */
//        public void setHandle(Handle handle) {
//            this.handle = handle;
//        }
//    }
//}