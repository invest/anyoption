<%@page import="javax.servlet.http.Cookie" %>
<%
	String chatId = request.getParameter("lpChatId");
	if (null != chatId) {
		response.addHeader("P3P","CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\""); // solution for: IE Blocking thirdParty iFrame Cookies
		Cookie cookie = new Cookie("lpChatId", chatId);
		cookie.setPath("/");
		cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
		response.addCookie(cookie);
	}
%>