/*********** popupLandingPage.js ************/
		function openErrorBox(stat){
			var t_box = document.getElementById("box1");
			var b_box = document.getElementById("box2");
			var t_box_n = document.getElementById("box1_number");
			var b_box_n = document.getElementById("box2_number");

			if (stat == 1){
				t_box.innerHTML  = "Exp Above:";
				//t_box_n.innerHTML  = "$175.00";
				b_box.innerHTML  = "Exp Below:";
				//b_box_n.innerHTML  = "$16.00";
				//t_box_n.className="number above";
				//b_box_n.className="number below";
			} else {
				t_box.innerHTML  = "Exp Below:";
				//t_box_n.innerHTML  = "$16.00";
				b_box.innerHTML  = "Exp Above:";
				//b_box_n.innerHTML  = "$175.00";
				//t_box_n.className="number below";
				//b_box_n.className="number above";
			}
			var box = document.getElementById("lightbox");
			var overlay = document.getElementById("Overlay");
			box.style.display="block";
			openOverlay();
			// move box to the center
			box.style.left = ((getWidth()/2)-(218 /2 ))+"px";


		}

		function openOverlay(){
			var overlay = document.getElementById("Overlay");
			overlay.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)+"px";
			overlay.style.display="block";
		}

		function closeLightBox(){
			var box = document.getElementById("lightbox");
			var overlay = document.getElementById("Overlay");
			var box2 = document.getElementById("lightform");
			box.style.display="none";
			box2.style.display="none";
			overlay.style.display="none";
		}

		function openFormBox(fname, lname, email, mphone, agreeUpdates){
			document.getElementById("contactForm:firstName").value = fname;
			document.getElementById("contactForm:lastName").value = lname;
			document.getElementById("contactForm:email").value = email;
			document.getElementById("contactForm:mobilePhone").value = mphone;
			document.getElementById("contactForm:agreeUpdatesLB").checked = agreeUpdates;

			document.getElementById("contactForm:savelink").click();
			markFirstErrorFieldWithPopUp()

			document.getElementById("firstName").value = document.getElementById("contactForm:firstName").value;
			document.getElementById("lastName").value = document.getElementById("contactForm:lastName").value;
			document.getElementById("email").value = document.getElementById("contactForm:email").value;
			document.getElementById("mobilePhone").value = document.getElementById("contactForm:mobilePhone").value;
			document.getElementById("agree").checked = document.getElementById("contactForm:agreeUpdatesLB").checked;
		}

		function openFormBox2 () {
			var old_box = document.getElementById("lightbox");
			var box = document.getElementById("lightform");

			//alert("*" + fname + "");
			openOverlay();
			old_box.style.display = "none";
			box.style.display = "block";
			box.style.left = ((getWidth()/2)-(303 /2 ))+"px";

			//document.forms[1][0].focus();
		}

		function getWidth() {
			w = self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
			if (w < 1200) w = 1200;
			return w;
		}

		function chackForm() {
			openformbox();
			return false;
		}

		function load() {
			document.forms[0][0].focus();
		}
