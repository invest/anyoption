var INS_TYPE_GOLDEN = 1;

var openTable = false ;
//count how much invest we have
var countGoldInvest = 0;

// the class attribute name. there us diff btw IE (className) and FF (class)
var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';
// in IE 8.0 it is like in FireFox
classAtr = navigator.appVersion.toLowerCase().indexOf("msie 8.0") != -1 ? 'class' : classAtr;

//when chose "select all". mark all the select boxs as selected or unselect
function goldenSelectAll(box) {
	//var tbody = box.parentNode.parentNode.parentNode; //should count number of childes and loop from 2 to length - 1
	for (var i = 1; i < 5; i++) {
		var tr = document.getElementById("investmentInfo" + i);
		if (tr.style.display != 'none') {
			var checkbox = document.getElementById("buyCheckBox" + i);
			checkbox.checked = box.checked;

		}
		sumAllInv();
	}
}

//sending ajax request and recive the answer for buying insurance
var insuranceSubmitted = 1;
function submitInsuranceToServer(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType) {
	var subIns = new SubIns(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType);
	function SubIns(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType) {
		this.invId = invId;
		this.insuranceAmount = insuranceAmount;
		this.winAmount = winAmount;
		this.investmentRowNumber = investmentRowNumber;
		this.invInsType = invInsType;
		SubIns.ticker = function() {
			var xmlHttpIns = getXMLHttp();
			if (null == xmlHttpIns) {
				return false;
			}
			xmlHttpIns.onreadystatechange = function() {
				if (xmlHttpIns.readyState == 4) {
					canSubmitGM = true;
					var winAmountArray = subIns.winAmount.split("_");
					var insuranceAmountArray = subIns.insuranceAmount.split("_");
					var invIdArray = subIns.invId.split("_");
					var rowNumberArray = subIns.investmentRowNumber.split("_");
					var invInsType = subIns.invInsType.split("_");
					var responseTextArray = xmlHttpIns.responseText.split("_");
//					alert("!@# " + responseTextArray);
					for (var i = 0; i < rowNumberArray.length ; i++) {
						if (responseTextArray[i].length > 2) {
							if (responseTextArray[i].substring(0, 2) == "OK") {
								successFromInsurance(rowNumberArray[i], invIdArray[i], winAmountArray[i], responseTextArray[i].substring(2));
							} else if (responseTextArray[i].substring(0, 1) == "1") {
								errorInInsuranceNotLogin(responseTextArray[i].substring(1));
								// refresh the header to see he is not logged in
								document.getElementById('header').contentWindow.location.reload();
								break;
							} else {
							//	errorInSlip(subIns.invId, subIns.winAmount, xmlHttpIns.responseText);
							//	unfreezeInSlip(subIns.invId);
								errorInInsurance(rowNumberArray[i], invIdArray[i], responseTextArray[i]);
							}
						} else {
						//	errorInInsurance(subIns.invId, subIns.winAmount, xmlHttpIns.responseText);
						//	unfreezeInInsurance(subIns.invId);
							errorInInsurance(rowNumberArray[i], invIdArray[i], responseTextArray[i]);
						}
					}
					sumAllInv();
					var imgBuy = document.getElementById("goldenBuy");
					imgBuy.style.display = 'block';
					imgBuy = document.getElementById("goldenBuyWait");
					imgBuy.style.display = 'none';
				}
			}
			xmlHttpIns.open("POST", "ajax.jsf", true);
			xmlHttpIns.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xmlHttpIns.send("command=submitInsurance&invId=" + subIns.invId +
						 	"&insuranceAmount=" + subIns.insuranceAmount +
						 	"&insuranceType=" + subIns.invInsType);
		}
	}
	setTimeout(SubIns.ticker, insuranceSubmitted * 1000);
	insuranceSubmitted++;
}

//submit the insurance when clicking on the buy button
var canSubmitGM = true;
function submitInsurances(buttonBuy) {
	buttonBuy.style.display = 'none';
	var img = getChildOfType(buttonBuy.parentNode, "IMG", 2);
	img.style.display = 'block';
	var checkbox;
	var tr;
	var td;
	var winAmount = "";
	var insuranceAmount = "";
	var invId = "";
	var invInsType = "";
	var rowNumber = "";
	var needToSend = false;
	var myregexp;
	var match;
	for (var i = 1; i < 5; i++) {
		checkbox = document.getElementById("buyCheckBox" + i);
		if (checkbox.checked) { //only if check box is selected submit
			tr = document.getElementById("investmentInfo" + i);
			td = getChildOfType(tr, "TD", 7);
			myregexp = /\D*([\d,]+\.\d{2})\D*/;
			match = myregexp.exec(td.innerHTML);
			if (match != null) {
				result = match[1];
			} else {
				result = td.innerHTML.substring(1);
			}
			winAmount += "_" + result;
			td = getChildOfType(tr, "TD", 12); //was 8
			insuranceAmount += "_" + td.innerHTML;
			td = getChildOfType(tr, "TD", 10);
			invId += "_" + td.innerHTML;
			td = getChildOfType(tr, "TD", 11);
			invInsType += "_" + td.innerHTML;
			rowNumber += "_" + i;
			needToSend = true;
//			alert("id = " + invId.substring(1) + " insAmount = " + insuranceAmount.substring(1) + " winAmount = " + winAmount.substring(1) + " rowNumber = " + rowNumber.substring(1));
		}
	}
	if (needToSend && canSubmitGM) {
		canSubmitGM = false;
		var errorTable = document.getElementById("mainError");
		errorTable.style.display = 'none';
		submitInsuranceToServer(invId.substring(1), insuranceAmount.substring(1), winAmount.substring(1), rowNumber.substring(1), invInsType.substring(1));
	}
	if (!needToSend) {
		var	link = document.getElementById("generalTermsLink");
		if (link.href.indexOf("goldenMinutes") > -1) {
			errorInInsuranceNotLogin(noInvSelectedGM);
		} else {
			errorInInsuranceNotLogin(noInvSelectedRU);
		}
		var imgBuy = document.getElementById("goldenBuy");
		imgBuy.style.display = 'block';
		imgBuy = document.getElementById("goldenBuyWait");
		imgBuy.style.display = 'none';
	}
}


//show the recipt when got confirm from server
function successFromInsurance(investmentRowNumber, invId, winAmount, responseText) {
	//send msg back to LS so he will stop sending updates
	lsEngRef.sendMessage("invId_" + invId);

	//hide the invest info
	var tr = document.getElementById("investmentInfo" + investmentRowNumber);
	tr.style.display = "none";
	var td = getChildOfType(tr, "TD", 10); //INS_id
	td.innerHTML = "";

	td = getChildOfType(tr, "TD", 11); //INS_type (gm/ru)
	var successType = td.innerHTML;

	//hide the invest error msg
	tr = document.getElementById("investmentError" + investmentRowNumber);
	tr.style.display = "none";

	//show the invest success msg
	tr = document.getElementById("investmentSuccess" + investmentRowNumber);
	tr.style.display = '';
	
	td = getChildOfType(tr, "TD", 1)
	var table = getChildOfType(td, "TABLE", 1);
	var tbody = getChildOfType(table, "TBODY", 1);
	tr = getChildOfType(tbody, "TR", 1);
	td = getChildOfType(tr, "TD", 2);
	if (successType == INS_TYPE_GOLDEN) {
		if (currencySymbol == "TL" || 4 == skinId) {
			td.innerHTML = reciptTxt1 + " " + winAmount + currencySymbol + " " + reciptTxt2 + "&#160;&#160;&#160;" + reciptNumber + " " + invId;
		} else {
			td.innerHTML = reciptTxt1 + " " + currencySymbol + winAmount + " " + reciptTxt2 + "&#160;&#160;&#160;" + reciptNumber + " " + invId;
		}
	} else {
		td.innerHTML = reciptTxt1_RU + "&#160;&#160;&#160" + reciptNumber + " " + invId;
	}

	//if its none visable will not tring to submit it again
	var checkbox = document.getElementById("buyCheckBox" + investmentRowNumber);
	checkbox.checked = false;


	document.getElementById('header').contentWindow.location.reload();
	var invBox = document.getElementById('right_side_investments');
	if (invBox != null) {
		invBox.contentWindow.location.reload();
	}

	// remove the investment from the profit line and if its the last 1 close it.
	var PLIframe = document.getElementById("TB_iframeContent");
	if (null != PLIframe) {
		if (PLIframe.contentWindow.removeInvestment(invId)) {
			stopThickBox();
		}
	}
}

//hide the recipt when clicking on the X
function closeGoldenRecipt(button, investmentRowNumber) {
	//hide the success
	var tr = document.getElementById("investmentSuccess" + investmentRowNumber);
	tr.style.display = "none";
	//clean the success
	tr = button.parentNode;
	var td = getChildOfType(tr, "TD", 2);
	td.innerHTML = "";
	td = getChildOfType(tr, "TD", 3);
	td.innerHTML = "";

	//decide if to show the row again (bcoz it have new investment) or to hide the show
	tr = document.getElementById("investmentInfo" + investmentRowNumber);
	td = getChildOfType(tr, "TD", 10); //INS_id
	if (td.innerHTML.length > 0) {
		tr.style.display = '';
	}
	if (countGoldInvest == 0) {
		toggleDetailsTable(false);
		toggleGMBanner(false);
	}
}

//if not log in and try to submit show the erro msg
function errorInInsuranceNotLogin(responseText) {
//	alert("errorInInsuranceNotLogin responseText: " + responseText);
	var table = document.getElementById("mainError");
	var tbody = getChildOfType(table, "TBODY", 1);
	var tr = getChildOfType(tbody, "TR", 1);
	var td = getChildOfType(tr, "TD", 1);
	td.innerHTML = responseText;
	//show the error msg
	table.style.display = '';
}

//show error msg in the row that got error
function errorInInsurance(investmentRowNumber, id, responseText) {
//	alert("errorInInsuranceNotLogin responseText: " + responseText);
	var tr = document.getElementById("investmentError" + investmentRowNumber);
	var td = getChildOfType(tr, "TD", 1);
	td.innerHTML = responseText;
	//show the invest error msg
	tr.style.display = '';

}

//when getting update from LS put it in the right row.
function updateItemGM(itemPos, updateInfo) {
	var investmentRowNumber = new Number(updateInfo.getNewValue("INS_POSITION")) + 1;
	var tr;
	var table;
	var cmd = updateInfo.getNewValue("command");
	if ((cmd == "ADD") || (cmd == "UPDATE")) {
		// get display name
		if (cmd == "ADD") {
			if (countGoldInvest == 0) {
				getGoldenTimeLeft(updateInfo.getNewValue("key"), updateInfo.getNewValue("INS_INS_TYPE"));
				document.getElementById("sumTitle").style.visibility = "visible";
				document.getElementById("insuranceBuyButton").style.visibility = "visible";
			}
			countGoldInvest++;
			if (openTable) {
				lsEngRef.sendMessage("viewedInvId_" + updateInfo.getNewValue("key"));
			}
		}
		var market_id = updateInfo.getNewValue("INS_MARKET");
		var name = "";
		var img;
		for (var i = 0; i < marketsDiaplayNameGM.length; i++) {
			if (marketsDiaplayNameGM[i][0] == market_id) {
				name = marketsDiaplayNameGM[i][1];
				break;
			}
		}

		tr = document.getElementById("investmentInfo" + investmentRowNumber);
		var td = getChildOfType(tr, "TD", 2); // market name
		td.innerHTML = name;
		td = getChildOfType(tr, "TD", 3); //INS_LEVEL
		if (updateInfo.getNewValue("INS_TYPE") == 1) {
			img = "arrowTable_Call.png";
		} else {
			img = "arrowTable_Put.png";
		}
		td.innerHTML = updateInfo.getNewValue("INS_LEVEL");

		td = getChildOfType(tr, "TD", 4); //inv type gif

		td.innerHTML = "<img src=\"" + context_pathGM_img + "/" + img + "\" alt=\"\" border=\"0\" />"

		td = getChildOfType(tr, "TD", 5); //INS_CRR_LEVEL_AO

		td.innerHTML = updateInfo.getNewValue("INS_CRR_LEVEL_AO");
		if (updateInfo.getNewValue("INS_TYPE") == 1) {
			td.style.color = "#2AA600";
		} else {
			td.style.color = "#CA0004";
		}
		var clrChange = updateInfo.getNewValue("INS_LEVEL_CLR");

		var clrToSet = "invest_level_up";
		if (clrChange == 0) {
			clrToSet = "invest_level_down";
		} else if (clrChange == 1) {
			clrToSet = "invest_level_close";
		}
		td.setAttribute(classAtr, "clrToSet");
//		setClass(td, clrToSet);
		td = getChildOfType(tr, "TD", 6); //INS_AMOUNT
		var amountTxt;
		var winTxt;
		var insTxt;
		if (currencySymbol == "TL") {
			amountTxt = updateInfo.getNewValue("INS_AMOUNT") + currencySymbol;
			winTxt = updateInfo.getNewValue("INS_WIN") + currencySymbol;
			insTxt = updateInfo.getNewValue("INS_INS") + currencySymbol;
		}
		else {
			amountTxt = currencySymbol + updateInfo.getNewValue("INS_AMOUNT");
			winTxt = currencySymbol + updateInfo.getNewValue("INS_WIN");
			insTxt = currencySymbol + updateInfo.getNewValue("INS_INS");
		}
		td.innerHTML = amountTxt;
		td = getChildOfType(tr, "TD", 7); //INS_WIN
		td.innerHTML = winTxt;
		td = getChildOfType(tr, "TD", 8); //INS_INS premia amount
		td.innerHTML = insTxt;
		td = getChildOfType(tr, "TD", 10); //INS_id
		td.innerHTML = updateInfo.getNewValue("key");
		td = getChildOfType(tr, "TD", 11); //INS_INS_TYPE
		td.innerHTML = updateInfo.getNewValue("INS_INS_TYPE");

		td = getChildOfType(tr, "TD", 12); //ins premia amount not formated
		td.innerHTML = updateInfo.getNewValue("INS_INS").replace(/\,/g,'');

		td = getChildOfType(tr, "TD", 13); //Arrow image to profit line.	
		if (updateInfo.getNewValue("INS_IS_OPEN_GRAPH") == "true") { //if the opp has graph and the market have gm than display the image tag.										
			td.innerHTML = "<img src=\"" + context_pathGM_img + "/bttn_openevent_over.png\" style=\"cursor:pointer\" onclick=\"openProfitLine(" + market_id + "," + updateInfo.getNewValue("INS_OPP_ID") + ");\" class=\"btnOpen_nd\" />";
		}
		
		table = document.getElementById("investmentInfo" + investmentRowNumber);
		table.style.display = '';
		tr.style.display = '';
/*		//if there is no recipt open. show this line;
		var trRecipt = document.getElementById("investmentSuccess" + investmentRowNumber);
		if (trRecipt.style.display = "none") {*/
			
//		}
	}

	if (cmd == "DELETE") {
		countGoldInvest--;
		var trSuccess = document.getElementById("investmentSuccess" + investmentRowNumber);
		if (countGoldInvest == 0) {
			if (trSuccess.style.display == "none") {
				toggleDetailsTable(false);
				toggleGMBanner(false);
			} else {
				document.getElementById("sumTitle").style.visibility = "hidden";
				document.getElementById("insuranceBuyButton").style.visibility = "hidden";
			}
		}
		//clean the tr when u got delete command
		tr = document.getElementById("investmentInfo" + investmentRowNumber);
		tr.style.display = "none";

		//hide the error tr
		tr = document.getElementById("investmentError" + investmentRowNumber);
		tr.style.display = "none";

		td = getChildOfType(tr, "TD", 10); //INS_id
		td.innerHTML = "";
		var checkBox = document.getElementById("buyCheckBox" + investmentRowNumber);
		checkbox.checked = false;
		if (trSuccess.style.display == "none") {
			tri = document.getElementById("investmentInfo" + investmentRowNumber);
			tri.style.display = "none";
		}
	}
}

//sum all the investment that are checked
function sumAllInv() {
		var sum = 0;
		var td;
		var tr
		var checkbox
		for (var i = 1; i < 5; i++) {
			tr = document.getElementById("investmentInfo" + i);
			checkbox = document.getElementById("buyCheckBox" + i);
			if (tr.style.display != 'none' && checkbox.checked) {
				td = getChildOfType(tr, "TD", 12).innerHTML;
				sum += new Number(td);
			}
		}
		td = document.getElementById("sumInv");
		sum = sum.toFixed(2);
		if (sum > 0) {
			if (currencySymbol == "TL") {
				td.innerHTML = sum + currencySymbol;
			}
			else {
				td.innerHTML = currencySymbol + sum;
			}
		} else {
			td.innerHTML = "";
		}
}

//add or substract invest prim to sum
function addInvToSum(checkBox, number) {
	var tr = document.getElementById("investmentInfo" + number);
	var tdInvRow = new Number(getChildOfType(tr, "TD", 12).innerHTML);
	var tdSum = document.getElementById("sumInv");
	var tdSumValue = 0;
	if (tdSum.innerHTML.length > 0) {
		var myregexp = /\D*([\d,]+\.\d{2})\D*/;
		var match = myregexp.exec(tdSum.innerHTML);
		if (match != null && match.length > 1) {
			tdSumValue = new Number(match[1]);
		} else {
			tdSumValue  = new Number(0);
		}
	}
	if (checkBox.checked) {
		tdSumValue += tdInvRow;
	} else {
		tdSumValue -= tdInvRow;
	}
	setSelectAllStatus();
	tdSumValue = tdSumValue.toFixed(2);
	if (tdSumValue > 0) {
		if (currencySymbol == "TL") {
			tdSum.innerHTML = tdSumValue + currencySymbol;
		}
		else {
			tdSum.innerHTML = currencySymbol + tdSumValue;
		}
	} else {
		tdSum.innerHTML = "";
	}
}

// if all ivestments checked, check also "select all"
function setSelectAllStatus() {
	for (var i = 1; i < 5; i++) {
		var tr = document.getElementById("investmentInfo" + i);
		if (tr.style.display != 'none' && !document.getElementById("buyCheckBox" + i).checked) {
			document.getElementById("buyAll").checked = false;
			return;
		}
	}
	document.getElementById("buyAll").checked = true;
}

//put the golden div in the right position on the page345
//! useing tigra hint!
function positionGoldenDiv() {
	var mTd = document.getElementById('languagesBox_nd');
	var conDiv = document.getElementById('goldenDiv');
	if (null != mTd && null != conDiv) {
		if (4 == skinId) {
			$(conDiv).css("left", f_getPosition(mTd, 'Left') + 238);
		} 
		else if(15 == skinId) {
			$(conDiv).css("left", f_getPosition(mTd, 'Left') - 430);
		}
		else {
			$(conDiv).css("left", f_getPosition(mTd, 'Left') - 820);
		}
		$(conDiv).css("top", f_getPosition(mTd, 'Top') + 110);
	}
}

var timesToGetIns = 1;
//get the time left for the user to buy insurance (called on first update)
//when its got the time its will position the div and show the banner
function getGoldenTimeLeft(invId, insType) {
	var timeGetterIns = new TimeGetterIns(invId, insType);
	function TimeGetterIns(invId, insType) {
		this.invId = invId;
		this.insType = insType;
		this.xmlHttp = null;
		this.ticker = function() {
//			log("Tick. invId: " + TimeGetterIns.invId);
			timeGetterIns.xmlHttp = getXMLHttp();
			if (null == timeGetterIns.xmlHttp) {
				return false;
			}
			timeGetterIns.xmlHttp.onreadystatechange = function() {
				if (timeGetterIns.xmlHttp.readyState == 4) {
					setInsuranceClockAndTxt(timeGetterIns.xmlHttp.responseText + "_" + timeGetterIns.insType)
				}
			}
			timeGetterIns.xmlHttp.open("POST", "ajax.jsf", true);
			timeGetterIns.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			timeGetterIns.xmlHttp.send("opp_time_exp=" + timeGetterIns.invId);
		}
	}
	setTimeout(timeGetterIns.ticker, 1000 * timesToGetIns);
	timesToGetIns++;
}


//when clicking on the text "details" or "close"
function openDetailsNoImage() {
	var strHref = window.top.location.href;
	if (!strEndsWith(strHref, homePageN) && (strHref.indexOf(tradePageN) == -1)) {
  		window.location.replace(context_pathGM + tradePageN + "?openDetails=true");
  		return;
  	}
	var txt = document.getElementById("goldenOpenImg");
	var arrow = document.getElementById("goldenOpenImg2");	
	if (txt != null && arrow != null) {
		if (!openTable) {	
			txt.textContent = details_close;
			arrow.src = context_pathGM_img + "/close.png";
			toggleDetailsTable(true);
			markInvestmetsViewed();
		} else {
			txt.textContent = details_open;
			arrow.src = context_pathGM_img + "/open.png";
			toggleDetailsTable(false);
		}
	}
}

//when clicking on the img "details" or "close"
function openDetails(tImage) {
	var strHref = window.top.location.href;
	if (!strEndsWith(strHref, homePageN) && (strHref.indexOf(tradePageN) == -1)) {
  		window.location.replace(context_pathGM + tradePageN + "?openDetails=true");
  		return;
  	}
	var image = tImage;
	if (!openTable) {
		image.src = context_pathGM_skin_img + "/close.gif";
		toggleDetailsTable(true);
		markInvestmetsViewed();
	} else {
		image.src = context_pathGM_skin_img + "/details.gif";
		toggleDetailsTable(false);
	}
}

//check if str end with suffix
function strEndsWith(str, suffix) {
	return str.match(suffix + "$") == suffix;
}

//hide the details table
//flag = true to show, else hide.
function toggleDetailsTable(flag) {
	var invTr = document.getElementById("goldenInvestmentsTR");
	var table = document.getElementById("goldenInvestmentsTable");
	var tr = document.getElementById("goldenDetails");
	var tr1 = document.getElementById("goldenDetails1");
	var tr2 = document.getElementById("goldenDetails2");
	var selectBox = document.getElementById("select_asset");
	if (flag) {
		openTable = true;
		if (selectBox) {
			selectBox.style.visibility = "hidden";
		}
		  tr.style.display="";
		  tr1.style.display="";
		  tr2.style.display="";
		  table.style.display="";
		  invTr.style.display="";		
	} else {
		openTable = false;
		tr.style.display = "none";
		tr1.style.display = "none";
		tr2.style.display = "none";
		table.style.display = "none";
		invTr.style.display = "none";
		if (selectBox) {
			selectBox.style.visibility = "visible";
		}
	}
}

//toggle the banner hide|show
//flag = true show, false hide
function toggleGMBanner(flag) {
	var goldDiv = document.getElementById("goldenDiv");
	if (flag) {
		goldDiv.style.display = 'inline';
		insertToDB();
	} else {
		goldDiv.style.display = 'none';
		clearInsuranceBanner();
	}
}

//position the banner in the right place on the page
function f_getPositionGM (e_elemRef, s_coord) {
//	log("f_getPosition e_elemRef: " + e_elemRef + " s_coord: " + s_coord);

	var n_pos = 0, n_offset,
		e_elem = e_elemRef;

	while (e_elem) {
		n_offset = e_elem["offset" + s_coord];
		n_pos += n_offset;
		e_elem = e_elem.offsetParent;
	}

	// margin correction in some browsers (not supported for GM)
	if (false) {
		n_pos += parseInt(document.body[s_coord.toLowerCase() + 'Margin']);
	}

	e_elem = e_elemRef;
	while (e_elem != document.body) {
		n_offset = e_elem["scroll" + s_coord];
		if (n_offset && e_elem.style.overflow == 'scroll')
			n_pos -= n_offset;
		e_elem = e_elem.parentNode;
	}

//	log("f_getPosition end.");
	return n_pos;
}

//if the ajax call return values mean we need to show the banner
function clockAndTxtCallback(responseXML) {
	responseTxt = responseXML.getElementsByTagName("time")[0].firstChild.nodeValue;
	if (null != responseTxt) {
		setInsuranceClockAndTxt(responseTxt);
	}
}

//check if need to open the banner in nonetradeing pages
function getClockAndTxt() {
	var url = "ajax.jsf?getClockAndTxt=true";
	var ajaxGM = new AJAXInteraction(url, clockAndTxtCallback);
	ajaxGM.doGet();
}

//set the insurance details, images and clock with times and images of RU or GM.
function setInsuranceClockAndTxt(responseTxt) {
	var rez = responseTxt.split("_");
	var id = rez[0];
	var min = rez[1];
	var sec = rez[2];
	var est_close_date = new Date(rez[3]);
	var est_next_close_date = new Date(rez[4]);
	var insType = new Number(rez[5]);
	var s = document.getElementById("goldenTime");
	var titleTxt = document.getElementById("goldenTitle");
	var formatedDate = formatDateGM(est_close_date);
	var formatedNextDate = formatDateGM(est_next_close_date);
	var img;
	var imgPath;
	var	link = document.getElementById("generalTermsLink");
	if (insType == INS_TYPE_GOLDEN) { //GM
		titleTxt.innerHTML = headerTxt1 + formatedDate.substring(formatedDate.indexOf(" ") + 1) + " " + headerTxt2;
		imgPath = "takeProfit.png";
		generalLink += "GML";
	} else { //RU
		titleTxt.innerHTML = headerTxtRU1 + formatedDate.substring(formatedDate.indexOf(" ") + 1) + " " + headerTxtRU2 + formatedNextDate.substring(formatedNextDate.indexOf(" ") + 1) + " " + headerTxtRU3;
		imgPath = "rollForward.png";
		link.href += "#rollUp"
		generalLink += "RUL";
	}
	img = document.getElementById("goldenBuy");
	img.src = context_pathGM_skin_img + "/buy_b.png";
	img = document.getElementById("goldenTitleImg");
	img.src = context_pathGM_skin_img + "/title_" + imgPath;
	//img = document.getElementById("goldenOpenImg");
	//img.src = context_pathGM_skin_img + "/details.gif";
	document.getElementById("goldenOpenImg").textContent = details_open;;
	var arrow = document.getElementById("goldenOpenImg2").src = context_pathGM_img + "/open.png";
	document.getElementById("generalTermsLink").href = generalLink;
	img = document.getElementById("goldenOpenImg");
	if (min > 0 || sec > 0) {
		if (s) {
			startCountDownGM(s, min, sec);
		}
		timesToGetIns--;
		//now show the banner

		positionGoldenDiv();
		toggleGMBanner(true);
		if (openDetailsFlag) {
			openDetailsNoImage();
		}
	}
}

//format the date to show it good to the user
function formatDateGM(est_date) {
	/*	//est_date = adjustFromUTCToLocalGM(est_date);
		est_date.setHours(est_date.getHours() + utcOffsetHour);
		est_date.setMinutes(est_date.getMinutes() + utcOffsetMin);

		var currdate = new Date();
		var min = est_date.getMinutes();
		if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			est_date = bundle_msg_todayGM + " " + est_date.getHours() + ":" + min;
		} else {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			var year = new String(est_date.getFullYear());
			est_date = est_date.getDate() + "/" + (est_date.getMonth()+1) + "/" + year.substring(2) + " " + est_date.getHours() + ":" + min;
		}
		return est_date;*/

		est_date = adjustFromUTCToLocalGM(est_date);
		var currdate = new Date();
		var min = est_date.getMinutes();
		if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			est_date = bundle_msg_todayGM + " " + est_date.getHours() + ":" + min;
		} else {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			var year = new String(est_date.getFullYear());
			est_date = est_date.getDate() + "/" + (est_date.getMonth()+1) + "/" + year.substring(2) + " " + est_date.getHours() + ":" + min;
		}
		return est_date;
	}

	//gm clock count down
	function startCountDownGM(display, min, sec) {
		var countDown = new CountDown(display, min, sec);
		function CountDown(display, min, sec) {
			this.display = display;
			this.min = min;
			this.sec = sec;
			CountDown.ticker = function() {
				if (countDown.sec > 0) {
					countDown.sec = countDown.sec - 1;
				} else {
					if (countDown.min > 0) {
						countDown.min = countDown.min - 1;
						countDown.sec = 59;
					}
				}
				countDown.display.innerHTML = padToTwoDigGM(countDown.min) + ":" + padToTwoDigGM(countDown.sec);
				if (countDown.min > 0 || countDown.sec > 0) {
					setTimeout(CountDown.ticker, 1000);
				} else {
					toggleDetailsTable(false);
					toggleGMBanner(false);
				}
			}
		}
		setTimeout(CountDown.ticker, 1000);
	}

	function padToTwoDigGM(val) {
		if (val < 10) {
			return "0" + val;
		}
		return val;
	}

	//when time is finish or no more insurance to buy clear the banner
	function clearInsuranceBanner() {
		var tr;
		var trRecipt;
		var table;
		var td;
		for (var i = 1; i < 5; i++) {
			//hide any open investments and delete the investment id;
			tr = document.getElementById("investmentInfo" + i);
			tr.style.display = 'none';
			td = getChildOfType(tr, "TD", 10); //INS_id
			td.innerHTML = "";

			//hide any recipt
			trRecipt = document.getElementById("investmentSuccess" + i);
			trRecipt.style.display = 'none';

			//delete all errors
			tr = document.getElementById("investmentError" + i);
			tr.style.display = 'none';

		}
		table = document.getElementById("mainError");
		tr = getChildOfType(tr, "TR", 1);
		td = getChildOfType(tr, "TD", 1);
		td.innerHTML = "";

		//delete the sum
		td = document.getElementById("sumInv");
		td.innerHTML = "";
	}

	function adjustFromUTCToLocalGM(toAdj) {
		var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
		var adjH = 0;
		if (adjM >= 0) {
			adjH = Math.floor(adjM / 60);
			adjM = adjM % 60;
		} else {
			adjH = Math.floor(Math.abs(adjM) / 60);
			if (Math.abs(adjM) % 60 != 0) {
				adjH += 1;
				adjM = 60 - Math.abs(adjM) % 60;
			} else {
				adjM = Math.abs(adjM) % 60;
			}
			adjH = -adjH;
		}
		toAdj.setHours(toAdj.getHours() + adjH);
		toAdj.setMinutes(adjM);
		return toAdj;
	}

	//insert to db when user see the banner first time
	function insertToDB() {
	    var url = "ajax.jsf?insuranceBanner=1";
	    var ajax = new AJAXInteraction(url);
	    ajax.doGet();
	}

	//tell to the LS to mark this investments as viewed
	function markInvestmetsViewed() {
		var invId;
		var tr;
		var td;
		for (var i = 1; i < 5; i++) {
			tr = document.getElementById("investmentInfo" + i);
			td = getChildOfType(tr, "TD", 10);
			invId = td.innerHTML;
			if (invId != "") {
				lsEngRef.sendMessage("viewedInvId_" + invId);
			}
		}
	}
	function openProfitLine(marketId, oppId) {
		if (window.top.location.href.indexOf("https") > -1) {
			window.top.location.href = "#{applicationData.homePageUrl}jsp/index.jsf?tickBoxMarketId=" + marketId + "&amp;tickBoxOppId=" + oppId;
		} else {
			window.top.runThickBox(marketId, oppId);
		}
		return false;
	}