var alignC = 'tlbr';
	if (skinId == 4 || skinId == 11) {   // arabic skin
		alignC = 'trbl';
}
var UTC_HINTS_CFG = {
	'smart'      : true, // don't go off screen, don't overlap the object in the document
	'margin'     : 0, // minimum allowed distance between the hint and the window edge (negative values accepted)
	'gap'        : 0, // minimum allowed distance between the hint and the origin (negative values accepted)
	'align'      : alignC, // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
	'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
	'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
	'hide_delay' : 1, // a delay between closing event (mouseout for example) and hint disappearing
	'follow'     : false, // hint follows the mouse as it moves
	'z-index'    : 100, // a z-index for all hint layers
	'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
	'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
	'opacity'    : 85 // opacity of the hint in %%
};

var ROLLUP_HINT = [
			'<div id="insuranceBought"></div>'
		];
var rollUpHint = new THints(ROLLUP_HINT, UTC_HINTS_CFG);

function showRollupBoughtHint(invAmount, insuranceAmount) {
	document.getElementById('insuranceBought').innerHTML = rollUpBoughtMsg1 + " " + invAmount + " " + rollUpBoughtMsg2 + " " + insuranceAmount;
	rollUpHint.show('0');
}

function showGMBoughtHint(invAmount, insuranceAmount) {
	document.getElementById('insuranceBought').innerHTML = GMBoughtMsg1 + " " + invAmount + " " + GMBoughtMsg2 + " " + insuranceAmount + " " + GMBoughtMsg3;
	rollUpHint.show('0');
}

function showOptionPlusHint(price) {
	document.getElementById('insuranceBought').innerHTML = optionPlusTip1 + " " + price + " " + optionPlusTip2;
	rollUpHint.show('0');
}

function showOptionPlusSettledHint() {
	document.getElementById('insuranceBought').innerHTML = optionPlusSettledTip;
	rollUpHint.show('0');
}

// configuration variable for the hint object, these setting will be shared among all hints created by this object
var HINTS_CFG = {
	'smart'      : true, // don't go off screen, don't overlap the object in the document
	'margin'     : 10, // minimum allowed distance between the hint and the window edge (negative values accepted)
	'gap'        : 20, // minimum allowed distance between the hint and the origin (negative values accepted)
	'align'      : 'blbr', // align of the hint and the origin (by first letters origin's top|middle|bottom left|center|right to hint's top|middle|bottom left|center|right)
	'css'        : 'hintsClass', // a style class name for all hints, applied to DIV element (see style section in the header of the document)
	'show_delay' : 0, // a delay between initiating event (mouseover for example) and hint appearing
	'hide_delay' : 200, // a delay between closing event (mouseout for example) and hint disappearing
	'follow'     : true, // hint follows the mouse as it moves
	'z-index'    : 100, // a z-index for all hint layers
	'IEfix'      : false, // fix IE problem with windowed controls visible through hints (activate if select boxes are visible through the hints)
	'IEtrans'    : ['blendTrans(DURATION=.3)', null], // [show transition, hide transition] - nice transition effects, only work in IE5+
	'opacity'    : 90 // opacity of the hint in %%
};

var HINTS_ITEMS_LOSE = ['<div id="bonusMsg"></div>'];

var myHintLose = new THints (HINTS_ITEMS_LOSE, HINTS_CFG);

function showBonusToolTip(bonusName) {
	var divBonusMsg = document.getElementById('bonusMsg');
	divBonusMsg.innerHTML = bonusToolTip + " " + bonusName;
	myHintLose.show(0);
}

function hideBonusToolTip() {
	myHintLose.hide();
}