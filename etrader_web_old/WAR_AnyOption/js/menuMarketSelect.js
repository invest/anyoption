function nodeClick(marketId, oppId) {
	var boxId = getBoxId();
	for (var i=1;i<=4;i++) {
		if ((boxesMarkets[i] == marketId) && (boxId != i - 1)) {
			if (isMobileParm) {
				lastSelectedMarketlist.options[lastSelectedMarket.innerHTML].selected = true;
				if (isAndroidApp) {
					window.AlertInterface.showAlert(marketMenuError);
				} else {
					customAlert("", marketMenuError);
				}
			} else {
				alert(marketMenuError);
			}
			return;
		}
	}
	removeBoxTable(boxId);
	var schedule=1;
	boxesMarkets[boxId + 1] = marketId;
	boxesFixed[boxId + 1] = true;
	boxesFull[boxId + 1] = true;
	boxesFixedScheduled[boxId + 1] = schedule;
	boxesFixedMarkets[boxId + 1] = marketId;
//    addBoxTable(market, "1", true, boxId);
	group[boxId] = "aotps_1_" + marketId;
	aoTable = new NonVisualTable(group, schema, "COMMAND");
	aoTable.setDataAdapter("JMS_ADAPTER");
	aoTable.setSnapshotRequired(true);
	aoTable.setRequestedMaxFrequency(1.0);
	aoTable.onItemUpdate = updateItemAO;
	lsPage.addTable(aoTable, "aoTable");
    //when we change market - the default for the schedule will be the closest
    //createCookie("box"+boxId+1,marketId+"_1",360);
    if (isLiveAOPage) {
    	fromGraph = 2; //Live page - trade box.
    } else {
    	fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
    }
}

function scheduleSelect(scheduled){

	//var box = getBox(boxId);
	var marketId = TigraBoxMarketId;
	var boxId = getBoxId();
	removeBoxTable(boxId);
	boxesFixed[boxId + 1] = true;
	boxesFull[boxId + 1] = true;
	boxesFixedScheduled[boxId + 1] = scheduled;
	boxesFixedMarkets[boxId + 1] = TigraBoxMarketId;
//    addBoxTable(marketId, scheduled, true, boxId);
	group[boxId] = "aotps_" + scheduled + "_" + marketId;
	aoTable = new NonVisualTable(group, schema, "COMMAND");
	aoTable.setDataAdapter("JMS_ADAPTER");
	aoTable.setSnapshotRequired(true);
	aoTable.setRequestedMaxFrequency(1.0);
	aoTable.onItemUpdate = updateItemAO;
	lsPage.addTable(aoTable, "aoTable");
    //createCookie("box" + boxId, marketId + "_" + scheduled, 360);
}

function returnSelector(returnS,refundS){
	var oppId = document.getElementById("returnSelectOppId").innerHTML;
	var choice = document.getElementById(oppId+"_choice").innerHTML;
	document.getElementById(oppId + "_5").innerHTML = selectorRetunrPct.replace("{0}", returnS);
	var tmpReturnS = (returnS/100) + 1;
	document.getElementById(oppId + "_returnS").innerHTML = tmpReturnS;
	var tmpRefundS = (refundS)/100;
	document.getElementById(oppId + "_refundS").innerHTML = tmpRefundS;	
	document.getElementById(oppId + "_above").innerHTML = selectorAboveBelowPct.replace("{0}", returnS);
	document.getElementById(oppId + "_below").innerHTML = selectorAboveBelowPct.replace("{0}", refundS);
   	updateAboveBelow(oppId);
 	if (choice != "0") {
      var tmpAmount = document.getElementById(oppId + "_amount");
      var amt = new Number(tmpAmount.value);
      updateWinLoseInt(tmpAmount, oppId);
	}
}

function updateAboveBelow(opp){
	var tmpchoice= document.getElementById(opp+"_choice").innerHTML;
	if(tmpchoice == 1){ 
		document.getElementById(opp+"_aboveU").childNodes[1].innerHTML = document.getElementById(opp + "_above").innerHTML;
		document.getElementById(opp+"_aboveD").childNodes[1].innerHTML = document.getElementById(opp + "_below").innerHTML;
	}else if (tmpchoice == 2){
		document.getElementById(opp+"_aboveU2").childNodes[1].innerHTML = document.getElementById(opp + "_above").innerHTML;
		document.getElementById(opp+"_aboveD2").childNodes[1].innerHTML = document.getElementById(opp + "_below").innerHTML;
	}
} 

function getBoxId() {
	for (var i = 0; i < boxesMarkets.length; i++) {
		if (boxesMarkets[i] == TigraBoxMarketId) {
			return i - 1
		}
	}
}

var prevColor;
var prevBG;
function changeColors(element) {
	prevBG = element.style.backgroundColor;
	element.style.backgroundColor = '#0284d6';
	if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
		prevColor = element.children[0].getElementsByTagName("label")[0].style.color;
		element.children[0].getElementsByTagName("label")[0].style.color = "white";
	} else {
		prevColor = element.childNodes[1].getElementsByTagName("label")[0].style.color;
		element.childNodes[1].getElementsByTagName("label")[0].style.color = "white";
	}
}

function changeColorsBack(element) {
	element.style.backgroundColor = prevBG;
	if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
		element.children[0].getElementsByTagName("label")[0].style.color = prevColor;
	}else {
		element.childNodes[1].getElementsByTagName("label")[0].style.color = prevColor;
	}
}


var prevBGSelector;
function changeColorsSelector(element) {
	prevBG = element.style.backgroundColor;
	element.style.backgroundColor = '#d7ebf8';
	element.style.color = "#0284d6"
}

function changeColorsBackSelector(element) {
	element.style.backgroundColor = prevBG;
	element.style.color="#093755";
}
