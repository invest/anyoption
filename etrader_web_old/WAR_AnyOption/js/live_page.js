//Get from lightstreamer
var FLD_KEY	= 1;
var FLD_COMMAND	= 2;
var LI_CITY = 3 //City
var LI_COUNTRY = 4 // Country
var LI_ASSET = 5 // Asset
var LI_PROD_TYPE = 6 // binary/+/100
var LI_INV_TYPE = 7 // call/put/buy/sell
var LI_LEVEL = 8 // 1.12345
var LI_AMOUNT = 9 // formatted for display investment amount in user currency
var LI_TIME = 10 // number of millis since epoch
var LI_INV_KEY = 11
var LI_OPP_ID = 12 // opportunity id
var LI_CONVERTED_AMOUNT = 13 //amount of the investment in USD
//Created by updateItem_live_investments function
var LI_ASSET_DISPLAY_NAME = 14;

var lineNum = 0;
function engineStatusChange(chngStatus) {	

}

//@
function formatValue_live_investments (item, itemUpdate) {
	var tdImageFlag = getChildOfType(item, "TD", 1);
	var countryName = getCountryName(itemUpdate.getServerValue(LI_COUNTRY));	
	var city = itemUpdate.getServerValue(LI_CITY);
	
	if(city.length > 23) {
		city = city.substring(0, 20) + "...";
	}
	tdImageFlag.innerHTML = "<div><img title='" + countryName + "' src = '" + getFlagImage(itemUpdate.getServerValue(LI_COUNTRY)) + "' /> " + city + "</div>";	
	
	var tdImageType = getChildOfType(item, "TD", 2);
	tdImageType.innerHTML = "<center><img src = '" + getProdTypeImage(itemUpdate.getServerValue(LI_PROD_TYPE)) + "' /></center>";
	
	var tdInvType = getChildOfType(item, "TD", 4);
	tdInvType.innerHTML = "<div><img src = '" + getInvTypeImage(itemUpdate.getServerValue(LI_INV_TYPE)) + "' /> " + itemUpdate.getServerValue(LI_LEVEL) + "</div><span style='display:none;'>" + itemUpdate.getServerValue(LI_INV_TYPE) + "</span>";
		
	if(itemUpdate.getServerValue(LI_PROD_TYPE) != 1) {
		var tdBinaryOption = getChildOfType(item, "TD", 6).style.visibility="hidden";
	}
	
	var table = document.getElementById("investments_table_live");
	var tableRow = table.getElementsByTagName("TR");
	
	for (var i = 1; i < tableRow.length; i++) {
		if(i % 2 == 0) {
			tableRow[i].style.background = "#ebebeb";
		} else {
			tableRow[i].style.background = "";
		}
	}
	
}//formatValue_live_investments

//@@
function updateItem_live_investments(item, updateInfo) {
	//add field to asset display name
	if (updateInfo.isValueChanged(LI_ASSET)) {
		var marketName = getMarketName(updateInfo.getNewValue(LI_ASSET));
		
		if(updateInfo.getNewValue(LI_ASSET) == 15) {
			marketName = "EUR/USD";
		}
		
		if(updateInfo.getNewValue(LI_ASSET) == 3
				|| updateInfo.getNewValue(LI_ASSET) == 12
				|| updateInfo.getNewValue(LI_ASSET) == 13
				|| updateInfo.getNewValue(LI_ASSET) == 15
				|| updateInfo.getNewValue(LI_ASSET) == 22
				|| updateInfo.getNewValue(LI_ASSET) == 23
				|| updateInfo.getNewValue(LI_ASSET) == 24
				|| updateInfo.getNewValue(LI_ASSET) == 25
				|| updateInfo.getNewValue(LI_ASSET) == 61
				|| updateInfo.getNewValue(LI_ASSET) == 350
				|| updateInfo.getNewValue(LI_ASSET) == 435
				|| updateInfo.getNewValue(LI_ASSET) == 436
				|| updateInfo.getNewValue(LI_ASSET) == 562
				|| updateInfo.getNewValue(LI_ASSET) == 567) {
			marketName = "Tel Aviv 25";
		}
			
		updateInfo.addField(LI_ASSET_DISPLAY_NAME, marketName);
		updateInfo.addField(LI_MARKET_DECIMAL, getMarketsDecimalPoint(updateInfo.getNewValue(LI_ASSET)));
	}
	
	if(updateInfo.getNewValue(FLD_COMMAND) == "DELETE") {
		lineNum--;
	} else {
		lineNum++;
	}
	
	if (checkAllClose()) {
		//show off hour banner 0
		switchLiveTable(0);
	} else {
		//show the market table 1
		switchLiveTable(1);
	}
	
}//updateItem_live_investments


function getMarketName(marketId) {
	for (var i = 0; i < marketsDisplayName.length; i++) {
		if (marketsDisplayName[i][0] == marketId) {
			return marketsDisplayName[i][1];
		}
	}
	return "";
}

function getCountryName(countryId) {
	for (var i = 0; i < countriesDisplayName.length; i++) {
		if (countriesDisplayName[i][0] == countryId) {
			return countriesDisplayName[i][1];
		}
	}
	return "";
}

function getFlagImage(countryId) {
	var flagUrl = imagePath + "/flags/" + countryId + ".png";
	return flagUrl;
}
		 		 
function getProdTypeImage(typeId) {
	var TypeUrl = "";
	if(typeId == 1) {
		 TypeUrl = imagePath + "/icon_BO.png";
	}
	if(typeId == 3) {
		TypeUrl = imagePath + "/optionPlus_icon_sm.png";
	}
	if(typeId == 4 || typeId == 5) {
		TypeUrl = imagePath + "/0100_icon_sm.png";
	}
	
	return TypeUrl;
}

function getInvTypeImage(typeId) {
	var TypeUrl = "";
	if(typeId == 1 || typeId == 4) {
		 TypeUrl = imagePath + "/arrowTable_Call.png";
	}
	if(typeId == 2 || typeId == 5) {
		TypeUrl = imagePath + "/arrowTable_Put.png";
	}	
	
	return TypeUrl;
}

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

function showPurchaseElement(isShow, infoId, devNum) {
    var msg = getChildOfType(infoId, "DIV", devNum);
	if (isShow == 'true') {
	    msg.style.display = "block";
	} else {
    	msg.style.display = "none";
	}
}
var intreval;
function sendToTradeBox(info){
	var flag = false;
	var td = info.parentNode;
	var tr = td.parentNode;
	var assetTD = getChildOfType(tr, "TD", 3);
	var assetId = getChildOfType(assetTD, "SPAN", 1).innerHTML;
	var oppId = getChildOfType(assetTD, "SPAN", 2).innerHTML;
	var invTypeTD = getChildOfType(tr, "TD", 4);
	var invTypeId = getChildOfType(invTypeTD, "SPAN", 1).innerHTML;
	fromGraph = 4; //Live Trades page.
	tickerClickLive(assetId,'1','1');
	
	//get the call or put button from the box 

	intreval = setInterval(function (){simulatePutOrCall(tr, invTypeId, assetId)}, 1000);
}

function simulatePutOrCall(tr, invTypeId, assetId) {
	try {
		var boxTemp = document.getElementById('box0');
		var index;
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 2);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "DIV", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		boxTemp = getChildOfType(boxTemp, "TR", 1);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		boxTemp = getChildOfType(boxTemp, "TABLE", 1);
		boxTemp = getChildOfType(boxTemp, "TBODY", 1);
		if (invTypeId == 1) {
			index = 3;
		} else {
			index = 5;
		}
		boxTemp = getChildOfType(boxTemp, "TR", index);
		boxTemp = getChildOfType(boxTemp, "TD", 1);
		var img = getChildOfType(boxTemp, "IMG", 1);
		var imgSrc = img.src.toString();
		
		if(imgSrc.indexOf('call.gif') > -1 || imgSrc.indexOf('put.gif') > -1) {
			clearInterval(intreval);
			if (typeof img.onclick == "function") {
	    		img.onclick.apply(boxTemp);
			}
			var amountTextBox = document.getElementById('box0').getElementsByTagName("input")[0];
			
			var amount = getChildOfType(tr, "TD", 5);
			var args = "amountForLive=" + getChildOfType(amount, "SPAN", 1).textContent
							+ "&marketId=" + assetId;
			
			var request = $.ajax({
			   type: "POST",
			   url: "ajax.jsf",
			   data: args,
			   success: function(msg) {
				   if (msg != null && msg != "-1") {
						amountTextBox.value = msg;
						if (typeof amountTextBox.onkeyup == "function") {
							var amountTextBoxId = $(amountTextBox).attr('id');
							amountTextBoxId = amountTextBoxId.substring(0, amountTextBoxId.indexOf('_'));
							updateWinLoseInt(amountTextBox, amountTextBoxId);
						}
				   }
			   }
			});
			var tradeBox = document.getElementById('box0');
			window.scroll(0,findPos(tradeBox));
		}
	} catch(e) {
		clearInterval(intreval);
	}
}

function findPos(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		do {
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	return [curtop];
	}
}

function switchLiveTable(flag) {
	var investmentsTable =  document.getElementById("liveInvestmentsTable");
	var offHourTable = document.getElementById("liveInvestmentsOffHours");
	var trade = document.getElementById("tradeBox");
	var marketsTable = document.getElementById("markets_table");
	var graphOffHours = document.getElementById("tradeOffHours");
	
	if (flag == 0) {
		investmentsTable.style.display = 'none';
		offHourTable.style.display = 'block';
		trade.style.display = 'none';
		markets_table.style.display = 'none';
		graphOffHours.style.display = 'block';		
	} else {
		investmentsTable.style.display = 'block';
		offHourTable.style.display = 'none';
		trade.style.display = 'block';
		markets_table.style.display = 'block';
		graphOffHours.style.display = 'none';
	}
	
}
