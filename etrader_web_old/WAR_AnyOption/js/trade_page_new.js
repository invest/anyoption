//function updateItemNonVisual1(itemPos, updateInfo) {
//	updateItemNonVisual(1, itemPos, updateInfo);
//}
//
//function updateItemNonVisual2(itemPos, updateInfo) {
//	updateItemNonVisual(2, itemPos, updateInfo);
//}
//
//function updateItemNonVisual3(itemPos, updateInfo) {
//	updateItemNonVisual(3, itemPos, updateInfo);
//}
//
//function updateItemNonVisual4(itemPos, updateInfo) {
//	updateItemNonVisual(4, itemPos, updateInfo);
//}

function updateItemAO(itemPos, updateInfo, itemName) {
	// handle market states
	if (updateInfo.isValueChanged(20)) {
		log("market states changed: " + updateInfo.getNewValue(20));
		var ms = updateInfo.getNewValue(20).split("|");
		var newMarketStates = new Array(ms.length);
		for (i = 0; i < ms.length; i++) {
			var cms = new Array(2);
			cms[0] = ms[i];
			cms[1] = 1;
			newMarketStates[i] = cms;
		}
		marketStates = newMarketStates;

		if (checkAllClose()) {
			//show off hour banner 0
			switchTable(0);
		} else {
			//show the market table 1
			switchTable(1);
		}
	}

	// handle home page markets
	if (updateInfo.isValueChanged(19)) {
		log("home page markets changed: " + updateInfo.getNewValue(19));
		if (null == markets) { // in the first update init the two empty arrays
			var newMarkets = new Array(4);
			for (i = 0; i < 4; i++) {
				var mv = group[i].split("_");
				var m = new Array(3);
				m[0] = mv[2];
				m[1] = i;
				m[2] = mv[1];
				newMarkets[i] = m;
			}
			for (i = 0; i < 4; i++) {
				boxesMarkets[i + 1] = newMarkets[i][0];
			}
			markets = newMarkets;
		}
		
		var ms = updateInfo.getNewValue(19).split("|");
		var newMarkets = new Array(4);
		for (i = 0; i < 4; i++) {
			var m = new Array(3);
			m[0] = ms[i];
			m[1] = i;
			m[2] = 1;
			newMarkets[i] = m;
		}
		log("oldMarkets: " + markets + " newMarkets: " + newMarkets);
		markets = newMarkets;
		scheduleStartBoxes();
	}

	// update the boxes
	for (i = 0; i < 4; i++) {
		if (itemName == group[i]) {
			updateItemNonVisual(i + 1, itemPos, updateInfo);
		}
	}
}

function updateItemNonVisual(boxNumber, itemPos, updateInfo) {
	var stateChanged = updateInfo.isValueChanged(SUBSCR_FLD_ET_STATE);
	var oldState = updateInfo.getOldValue(SUBSCR_FLD_ET_STATE);
	var state = updateInfo.getNewValue(SUBSCR_FLD_ET_STATE);

	var colorChanged = updateInfo.isValueChanged(SUBSCR_FLD_ET_CLR);
	var color = updateInfo.getNewValue(SUBSCR_FLD_ET_CLR);

	/* save the time to close/open */
	var est_date = new Date(updateInfo.getNewValue(SUBSCR_FLD_ET_EST_CLOSE));

/*	est_date.setHours(est_date.getHours() + getUTCOffset());

	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = "TODAY " + est_date.getHours() + ":" + min;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2) + " " + est_date.getHours() + ":" + min;
	}
*/
	est_date = formatDate(est_date);
	// get display name
	var name = "";
//	if (updateInfo.isValueChanged(SUBSCR_FLD_ET_NAME)) {
		for (var i = 0; i < marketsDisplayName.length; i++) {
			if (marketsDisplayName[i][0] == updateInfo.getNewValue(SUBSCR_FLD_ET_NAME)) {
				name = marketsDisplayName[i][1];
				break;
			}
		}
//	}

	var box = getBox(boxNumber, 0);
	var oppId = updateInfo.getNewValue(SUBSCR_FLD_ET_OPP_ID);
	var item = null;
	var cmd = updateInfo.getNewValue(SUBSCR_FLD_COMMAND);
	log("cmd: " + cmd +
		" stateChanged: " + stateChanged +
		" oldState: " + oldState +
		" state: " + state +
		" colorChanged: " + colorChanged +
		" color: " + color +
		" date: " + est_date +
		" name: " + name +
		" oppId: " + oppId);

//	if (cmd == "UPDATE" || cmd == "DELETE") {
		var tr = null;
		var i = 3;
		do {
			tr = getChildOfType(box, "TR", i);
			if (null != tr && getLSValue(getOppTRSF(tr, 1, 5, null, 2)) == oppId) {
				item = tr;
				break;
			}
			i++;
		} while (null != tr);
//	}
	// as strange as it can be we can get "ADD" command after we have already got
	// "ADD" or "UPDATE" for certain opportunity...
	if (cmd == "ADD" && null != item) {
		log("ADD after we already have that opp");
	}
//	if ((cmd == "ADD" && null == item) || null == item) {
	if (null == item) {
		// make sure we don't have more than 1 row
//		clearBoxRows(box);
		if (cmd == "UPDATE" && null == item) {
			log("UPDATE without ADD");
		}
		var loading = getChildOfType(box, "TR", 1);
		loading.style.display = "none";
		var template = getChildOfType(box, "TR", 3);
		template = getChildOfType(template, "TD", 1);
		// copy the template
		// do it this way so we go around the "Unknown runtime error" in IE when editing the innerHTML of a table
		var newEntry = document.createElement("tr");
		var newEntryTd = document.createElement("td");
		newEntry.appendChild(newEntryTd);
		newEntryTd.innerHTML = template.innerHTML;
		item = newEntry;
		fillNewRow(item, updateInfo, est_date, name, boxNumber);
		var banner = getChildOfType(box, "TR", 2);
		var bannerShowing = banner.style.display != "none";
		if (bannerShowing || null != getChildOfType(box, "TR", 4)) {
			item.style.display = "none";
		}
		box.appendChild(item);
		stateChanged = true;
	}

	// if its suspend msg then show the format the msg and put it in the td
	if (state == OPPORTUNITY_STATE_SUSPENDED) {
		//alert(updateInfo.getNewValue(SUBSCR_FLD_ET_SUSPENDED_MESSAGE));
		var suspendMsg = updateInfo.getNewValue(SUBSCR_FLD_ET_SUSPENDED_MESSAGE).split("s");
		var suspendMsgString =  suspendMsgUpperLine[new Number(suspendMsg[0]) - 1] + "<br/>" + suspendMsgLowerLine[new Number(suspendMsg[1]) - 1];
		if (suspendMsg.length == 3 && suspendMsg[2].length > 0) {
			var suspendDate = new Date(suspendMsg[2]);
	//		alert(suspendDate.getDate());
			suspendMsgString += " " + formatDate(suspendDate);
		}
		getOppTRSF(item, 1, 3, 2, 7).innerHTML = suspendMsgString;
	}


	// copy the level to the slip if not submitting
	if (getOppTRSF(item, 1, 5, null, 7).innerHTML == "0") {
		getOppTRSF(item, 2, 2, null, 2).innerHTML = updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL);
	}
	//for bug 2029 when ur login update the showing level all the time or when drop chart is open
	var chartIframe = document.getElementById("chart" + updateInfo.getNewValue(SUBSCR_FLD_ET_OPP_ID));
	if (updatesFrequencyFlag == 1 || null != chartIframe) {
		setOppLevelAnd15MinLevel(item, updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL), 0, 2);
	}
	if (stateChanged) {
		setLSValue(getOppTRSF(item, 1, 5, null, 5), updateInfo.getNewValue(SUBSCR_FLD_ET_STATE));
		//for bug 2029 when ur logout and state change then update the showing level
		if (updatesFrequencyFlag != 1) {
			setOppLevelAnd15MinLevel(item, updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL));
		}
	}

	formatValuesNonVisual(item, oppId, stateChanged, oldState, state, colorChanged, color, est_date, name);

	//disable the drop down chart for weekly and monthly if they r not daily
	var closeAnd1 = updateInfo.getNewValue(SUBSCR_FLD_ET_GROUP_CLOSE) & 1;
	if (getLSValue(getOppTRSF(item, 1, 5, null, 9)) > 2 && closeAnd1 == 0) {
		hideElm(getOppTRSF(item, 1, 2, 1, 3));
		showElm(getOppTRSF(item, 1, 2, 1, 4), ELEMENT_TYPE_CELL);
	}

	if (cmd == "DELETE") {
		box.removeChild(item);
		var banner = getChildOfType(box, "TR", 2);
		var bannerShowing = banner.style.display != "none";
		if (!bannerShowing) {
			var nextOpp = getChildOfType(box, "TR", 4);
			if (null != nextOpp) {
				showElm(nextOpp, ELEMENT_TYPE_ROW);
			} else {
				var loading = getChildOfType(box, "TR", 1);
				showElm(loading, ELEMENT_TYPE_ROW);
			}
		}
	}
}

function setLSValue(td, value, hn) {
	var hnn = 1;
	if (null != hn) {
		hnn = hn;
	}
	var holder = getChildOfType(td, "DIV", hnn);
	if (null == holder) {
		holder = getChildOfType(td, "SPAN", hnn);
	}
	holder.innerHTML = value;
}

function fillNewRow(item, updateInfo, date, name, boxNumber) {
	setLSValue(getOppTRSF(item, 1, 1, 1, 2), name + getDownArrows());
	setLSValue(getOppTRSF(item, 1, 1, 1, 6), "<strong>" + trading_box_expires + "</strong> " + date + getDownArrows());
	var odds = updateInfo.getNewValue(SUBSCR_FLD_ET_ODDS);
	if (skinId == 3 || skinId == 4) {
		odds = '%' + odds.substring(0,odds.length-1);
	}
	setLSValue(getOppTRSF(item, 1, 2, 1, 2), odds /*updateInfo.getNewValue(SUBSCR_FLD_ET_ODDS)*/);
	setLSValue(getOppTRSF(item, 1, 5, null, 12), updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL));
	setLSValue(getOppTRSF(item, 1, 5, null, 13), updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL));
	setLSValue(getOppTRSF(item, 1, 3, 2, 6), date);
	setLSValue(getOppTRSF(item, 1, 5, null, 2), updateInfo.getNewValue(SUBSCR_FLD_ET_OPP_ID));
	setLSValue(getOppTRSF(item, 1, 5, null, 3), updateInfo.getNewValue(SUBSCR_FLD_ET_ODDS_WIN));
	setLSValue(getOppTRSF(item, 1, 5, null, 4), updateInfo.getNewValue(SUBSCR_FLD_ET_ODDS_LOSE));
	setLSValue(getOppTRSF(item, 1, 5, null, 5), updateInfo.getNewValue(SUBSCR_FLD_ET_STATE));
	setLSValue(getOppTRSF(item, 1, 5, null, 8), updateInfo.getNewValue(SUBSCR_FLD_ET_NAME));
	setLSValue(getOppTRSF(item, 1, 5, null, 9), updateInfo.getNewValue(SUBSCR_FLD_ET_SCHEDULED));
	setLSValue(getOppTRSF(item, 1, 5, null, 10), updateInfo.getNewValue(SUBSCR_FLD_ET_RND_FLOOR));
	setLSValue(getOppTRSF(item, 1, 5, null, 11), updateInfo.getNewValue(SUBSCR_FLD_ET_RND_CEILING));
	setLSValue(getOppTRSF(item, 2, 2, null, 1), name);
	setLSValue(getOppTRSF(item, 2, 5, null, 2), date);

	if (slipTypes[boxNumber] != 0) {
		hideElm(getOppTRSF(item, 1));
		showElm(getOppTRSF(item, 2), ELEMENT_TYPE_TABLE);

		getOppTRSF(item, 2, 9, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 1).innerHTML;
		getOppTRSF(item, 2, 10, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 2).innerHTML

		if (slipTypes[boxNumber] == 2) {
			hideElm(getOppTRSF(item, 2, 4, null, 2));
			showElm(getOppTRSF(item, 2, 4, null, 3), ELEMENT_TYPE_ROW);
		getOppTRSF(item, 2, 9, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 2).innerHTML;
		getOppTRSF(item, 2, 10, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 1).innerHTML;
		}

		setOppSlipAmount(item, slipAmounts[boxNumber]);
		slipTypes[boxNumber] = 0;
		updateWinLoseInt(item);
	}

	//fill the chart table
	setLSValue(getOppTRSF(item, 4, 1, 1, 2), name + getDownArrows());
	setLSValue(getOppTRSF(item, 4, 1, 1, 6), "<strong>" + trading_box_expires + "</strong> " + date + getDownArrows());
	setLSValue(getOppTRSF(item, 4, 3, 1, 2), odds);
	setChartTableOppLevel(item, updateInfo.getNewValue(SUBSCR_FLD_ET_LEVEL));
}

function formatValuesNonVisual(item, oppId, stateChanged, oldState, state, colorChanged, color, date, name) {
	if (stateChanged) {
		log("state for market " + name + " changed from " + oldState + " to " + state);
		if (state == OPPORTUNITY_STATE_CREATED || state == OPPORTUNITY_STATE_WAITING_TO_PAUSE ||
				state == OPPORTUNITY_STATE_PAUSED || state == OPPORTUNITY_STATE_SUSPENDED) {
			formatValuesNotOpenedNonVisual(item, state, date);
		} else if (state == OPPORTUNITY_STATE_OPENED && null != oldState && oldState != state) {
			formatValuesOpenedNonVisual(item);
		} else if (state == OPPORTUNITY_STATE_LAST_10_MIN) {
			if (oldState == OPPORTUNITY_STATE_CREATED || oldState == OPPORTUNITY_STATE_PAUSED || oldState == OPPORTUNITY_STATE_SUSPENDED) {
				formatValuesOpenedNonVisual(item);
			}
			formatValues10MinNonVisual(item, oppId);
		} else if (state == OPPORTUNITY_STATE_CLOSING_1_MIN) {
			if (oldState == OPPORTUNITY_STATE_CREATED || oldState == OPPORTUNITY_STATE_PAUSED || oldState == OPPORTUNITY_STATE_SUSPENDED) {
				formatValuesOpenedNonVisual(item);
			}
			formatValuesClosingNonVisual(item);
		} else if (state == OPPORTUNITY_STATE_CLOSING) {
			if (oldState == OPPORTUNITY_STATE_CREATED || oldState == OPPORTUNITY_STATE_PAUSED || oldState == OPPORTUNITY_STATE_SUSPENDED) {
				formatValuesOpenedNonVisual(item);
				formatValuesClosingNonVisual(item);
			}
			formatValuesClosingNonVisual(item);
		} else if (state == OPPORTUNITY_STATE_CLOSED) {
			if (oldState == OPPORTUNITY_STATE_CREATED || oldState == OPPORTUNITY_STATE_PAUSED || oldState == OPPORTUNITY_STATE_SUSPENDED) {
				formatValuesOpenedNonVisual(item);
			}
			formatValuesClosedNonVisual(item);
		}
	}
	formatValuesChangeColorsNonVisual(item, state, colorChanged, color);
}

function formatValuesNotOpenedNonVisual(item, state, date) {
	setClassOff(getOppTRSF(item, 1, 2, 1, 2));
	setClassOff(getOppTRSF(item, 1, 2, 2, 2));

	if (state == OPPORTUNITY_STATE_WAITING_TO_PAUSE) {
		setLSValue(getOppTRSF(item, 1, 3, 2, 6), date);
	}

	hideElm(getOppTRSF(item, 1, 2, 1, 3));
	showElm(getOppTRSF(item, 1, 2, 1, 4), ELEMENT_TYPE_CELL);

	hideElm(getOppTRSF(item, 1, 3, 1, 3));
	showElm(getOppTRSF(item, 1, 3, 1, 4), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 2, 2));
	hideElm(getOppTRSF(item, 1, 3, 2, 4));
	hideElm(getOppTRSF(item, 1, 3, 2, 5));

	showElm(getOppTRSF(item, 1, 3, 2, state == OPPORTUNITY_STATE_SUSPENDED ? 7 : 6), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 3, 3));
	showElm(getOppTRSF(item, 1, 3, 3, 4), ELEMENT_TYPE_CELL);

	//hide call/put in chart box, show call/put off
	showElm(getChartTableOppLevelTd(item, 1 , 2));
	showElm(getChartTableOppLevelTd(item, 3 , 2));
	hideElm(getChartTableOppLevelTd(item, 1 , 1));
	hideElm(getChartTableOppLevelTd(item, 3 , 1));

	//hide scheduled selection
	hideElm(getOppTRSF(item, 1, 1, 1, 6));

}

function formatValuesOpenedNonVisual(item) {
	setClassOn(getOppTRSF(item, 1, 2, 1, 2));
	setClassOn(getOppTRSF(item, 1, 2, 2, 2));

	hideElm(getOppTRSF(item, 1, 2, 1, 4));
	showElm(getOppTRSF(item, 1, 2, 1, 3), ELEMENT_TYPE_CELL);

	hideElm(getOppTRSF(item, 1, 3, 1, 4));
	showElm(getOppTRSF(item, 1, 3, 1, 3), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 2, 6));
	hideElm(getOppTRSF(item, 1, 3, 2, 7));
	showElm(getOppTRSF(item, 1, 3, 2, 2), ELEMENT_TYPE_CELL);
	showElm(getOppTRSF(item, 1, 3, 2, 4), ELEMENT_TYPE_CELL);
	showElm(getOppTRSF(item, 1, 3, 2, 5), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 3, 4));
	showElm(getOppTRSF(item, 1, 3, 3, 3), ELEMENT_TYPE_CELL);

	showElm(getOppTRSF(item, 1, 4, 1), ELEMENT_TYPE_ROW);
	hideElm(getOppTRSF(item, 1, 4, 2));
	hideElm(getOppTRSF(item, 1, 4, 3));
	hideElm(getOppTRSF(item, 1, 4, 4));

	//showElm scheduled selection
	showElm(getOppTRSF(item, 1, 1, 1, 6));

	//chart trade box hide the 4 tr where there is msgs sometimes
	showElm(getOppTRSF(item, 4, 4, null, 1), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 4, 4, null, 2));
	hideElm(getOppTRSF(item, 4, 4, null, 3));
	hideElm(getOppTRSF(item, 4, 4, null, 4));

	//show call/put in chart box, hide call/put off
	hideElm(getChartTableOppLevelTd(item, 1 , 2));
	hideElm(getChartTableOppLevelTd(item, 3 , 2));
	showElm(getChartTableOppLevelTd(item, 1 , 1));
	showElm(getChartTableOppLevelTd(item, 3 , 1));
}

function formatValues10MinNonVisual(item, oppId) {
	hideElm(getOppTRSF(item, 1, 4, 1));
	showElm(getOppTRSF(item, 1, 4, 2), ELEMENT_TYPE_ROW);
	var progressTD = getOppTRSF(item, 1, 4, 3);
	showElm(progressTD, ELEMENT_TYPE_ROW);
	var td = getChildOfType(progressTD, "TD", 1);
	var tdtbl = getChildOfType(td, "TABLE", 1);
	getTimeLeftAO(oppId, tdtbl);
	//show the td of the clock for time left
	hideElm(getOppTRSF(item, 4, 4, null, 1));
	showElm(getOppTRSF(item, 4, 4, null, 2), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 4, 4, null, 3));
	hideElm(getOppTRSF(item, 4, 4, null, 4));

	//show call/put in chart box, hide call/put off
	hideElm(getChartTableOppLevelTd(item, 1 , 2));
	hideElm(getChartTableOppLevelTd(item, 3 , 2));
	showElm(getChartTableOppLevelTd(item, 1 , 1));
	showElm(getChartTableOppLevelTd(item, 3 , 1));

	var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
	if (chartIframe != null) {
		chartIframe.contentWindow.showLast10Min();
	}
}

function formatValuesClosingNonVisual(item) {
	hideElm(getOppTRSF(item, 1, 3, 1, 3));
	showElm(getOppTRSF(item, 1, 3, 1, 4), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 3, 3));
	showElm(getOppTRSF(item, 1, 3, 3, 4), ELEMENT_TYPE_CELL);

	hideElm(getOppTRSF(item, 1, 4, 1));
	hideElm(getOppTRSF(item, 1, 4, 2));
	hideElm(getOppTRSF(item, 1, 4, 3));
	showElm(getOppTRSF(item, 1, 4, 4), ELEMENT_TYPE_ROW);

	//chart box waiting for expiry
	hideElm(getOppTRSF(item, 4, 4, null, 1));
	hideElm(getOppTRSF(item, 4, 4, null, 2));
	showElm(getOppTRSF(item, 4, 4, null, 3), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 4, 4, null, 4));

	//hide call/put in chart box, show call/put off
	showElm(getChartTableOppLevelTd(item, 1 , 2));
	showElm(getChartTableOppLevelTd(item, 3 , 2));
	hideElm(getChartTableOppLevelTd(item, 1 , 1));
	hideElm(getChartTableOppLevelTd(item, 3 , 1));

	var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
	if (chartIframe != null) {
		chartIframe.contentWindow.showWaitingForExpiry();
	}
}

function formatValuesClosedNonVisual(item) {
	setClassOff(getOppTRSF(item, 1, 2, 1, 2));
	setClassOff(getOppTRSF(item, 1, 2, 2, 2));
	// show put and call off
	hideElm(getOppTRSF(item, 1, 3, 1, 3));
	showElm(getOppTRSF(item, 1, 3, 1, 4), ELEMENT_TYPE_CELL);
	hideElm(getOppTRSF(item, 1, 3, 3, 3));
	showElm(getOppTRSF(item, 1, 3, 3, 4), ELEMENT_TYPE_CELL);

	hideElm(getOppTRSF(item, 1, 4, 4));
	showElm(getOppTRSF(item, 1, 4, 1), ELEMENT_TYPE_ROW);

	hideElm(getOppTRSF(item, 1, 3, 2, 2));
	showElm(getOppTRSF(item, 1, 3, 2, 3), ELEMENT_TYPE_CELL);

	//chart box expiry level
	hideElm(getOppTRSF(item, 4, 4, null, 1));
	hideElm(getOppTRSF(item, 4, 4, null, 2));
	hideElm(getOppTRSF(item, 4, 4, null, 3));
	showElm(getOppTRSF(item, 4, 4, null, 4), ELEMENT_TYPE_CELL);

	//hide call/put in chart box, show call/put off
	showElm(getChartTableOppLevelTd(item, 1 , 2));
	showElm(getChartTableOppLevelTd(item, 3 , 2));
	hideElm(getChartTableOppLevelTd(item, 1 , 1));
	hideElm(getChartTableOppLevelTd(item, 3 , 1));

	var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
	if (chartIframe != null) {
		chartIframe.contentWindow.showWaitingForExpiry();
	}

	//disable the chart drop down
	hideElm(getOppTRSF(item, 1, 2, 1, 3));
	showElm(getOppTRSF(item, 1, 2, 1, 4), ELEMENT_TYPE_CELL);
}

function formatValuesChangeColorsNonVisual(item, state, colorChanged, color) {
	if (colorChanged) {
		if (state == OPPORTUNITY_STATE_CREATED || state == OPPORTUNITY_STATE_PAUSED || state == OPPORTUNITY_STATE_SUSPENDED) {
			return;
		}
		var td = getOppTRSF(item, 1, 3, 2, 4);
		var cls = "trade_level_green";
		var cls1 = "slip_head_green";
		if (color == 0) {
			cls = "trade_level_red";
			cls1 = "slip_head_red";
		} else if (color == 1) {
			cls = "trade_level_expired";
			cls1 = "slip_head";
		}
		setElmClass(td, cls);
		setElmClass(getOppTRSF(item, 2, 2, null, 2), cls1);
		//color for the chart level
		setElmClass(getChartTableOppLevelTd(item, 2, 1), cls + "_chart");
	}
}

function formatDate(est_date) {
	est_date = adjustFromUTCToLocal(est_date);

	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
	}
	return est_date;
}

function getDownArrows() {
	return document.getElementById("template_down").innerHTML;
}


// functions for homepage movie (Daniella)
function checkMovie() {
	if (firstTimeVisit == "true") {
		document.getElementById('movie_cell').style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : '';
		document.getElementById('markets_cell').style.display="none";
	} else {
		document.getElementById('movie_cell').style.display = "none";
		document.getElementById('markets_cell').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : '';
	}
}

function closeFlash() {
	document.getElementById('movie_cell').style.display = "none";
	document.getElementById('markets_cell').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : '';
}

var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
// Handle all the FSCommand messages in a Flash movie.
function ao_fc_DoFSCommand(command, args) {
	var testObj = isInternetExplorer ? document.all.ao_fc : document.ao_fc;

	document.getElementById('movie_cell').style.display = "none";
	document.getElementById('markets_cell').style.display=navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : '';

}
// Hook for Internet Explorer.
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub ao_fc_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call ao_fc_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');
}