/*********** skinsMenuChange.js ************/
	
	/*
			This is done just to FF, to solve the problem that in FF the onclick
			event pressed just from the lable and not the whole td
	*/
	function changeSkin(element) {
		document.getElementById(element).onclick();
	}
	function changeLinkColor(element, type) {
		element.style.backgroundColor='#2d4759';
		if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) {  //IE
			prevColor = element.children[0].getElementsByTagName(type)[0].style.color;
			element.children[0].getElementsByTagName(type)[0].style.color="white";
		} else {
				prevColor = element.childNodes[1].getElementsByTagName(type)[0].style.color;
				element.childNodes[1].getElementsByTagName(type)[0].style.color="white";
		}
	}
	
	function changeColorBack(element, type) {
		element.style.backgroundColor='#011b2c';
		if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
			element.children[0].getElementsByTagName(type)[0].style.color=prevColor;
		} else {
				element.childNodes[1].getElementsByTagName(type)[0].style.color=prevColor;
		}
	
	}