			//bugId 2029 start
			//start random update if not log in
			if (updatesFrequencyFlag == 0) {
				var rndUpdtTimer = setInterval ( "updateLevelRandomly()", 5000 );
				var rndCopyTimer = setInterval ( "copyRealLevel()", 900000 ); //900000
			}

			//loop over all the box and change the box and slip level randomly
			function updateLevelRandomly() {
				var chartIframe;
				var i = 1;
				for (i=1; i<5 ; i++) {
					var boxRow = getBox(i);
					if (null != boxRow ) {
						chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(boxRow, 1, 5, null, 2)));
						var state = getLSValue(getOppTRSF(boxRow, 1, 5, null, 5));
						if (state != OPPORTUNITY_STATE_CREATED && state != OPPORTUNITY_STATE_PAUSED && state != OPPORTUNITY_STATE_SUSPENDED && state != OPPORTUNITY_STATE_CLOSED && chartIframe == null) {
							setOppLevelRandomChange(boxRow, getRandom(boxRow));
							updateSlipLevel(boxRow);
						}
					} else {
						break;
					}
				}
			}

			function setOppLevelRandomChange(opp, random) {

				var levelsTd = getOppTRSF(opp, 1, 3, 2, 4);

				var div1 = getChildOfType(levelsTd, "DIV", 1); // the real level to show

	//			var div2 = getChildOfType(levelsTd, "DIV", 3); // the 15 min level to copy from
				var div2 = getOppTRSF(opp, 1, 5, null, 13);

	//			alert("Td: " + levelsTd.innerHTML + " div1: " + div1.innerHTML + " div3: " + div2.innerHTML);
				if (null == div1 || null == div2) {
					return;
				}

				if (div2.innerHTML.indexOf("img") == -1) {
					var level = addCommas(addRandomValue(div2.innerHTML, parseFloat(random)), div2.innerHTML);
					div1.innerHTML = level;
					setChartTableOppLevel(opp, level);
				}
			}

			//add random value to the last level
			function addRandomValue(str, value) {
				//delete commas ","
				var iStr = str;
				var index = iStr.indexOf(",");
			//	alert("b4 loop: " + iStr);
				while (index != -1) {
					var start = iStr.substring(0, index);
					var end = iStr.substring(index + 1);
					iStr = start + end;
					index = iStr.indexOf(",");
				}
				//count digits after zero before adding random value
				iStr += '';
		//		alert("Str after loop: " + iStr);
				var c2Length = 0;
				var x = iStr.split('.');
				var x2 = x[1];
				var x2Length = x2.length;
				//adding the random value
//				var result = parseFloat(iStr) + parseFloat(value);
				var result = parseFloat(iStr) + parseFloat(iStr) * parseFloat(value); // Tony - the random value is a fraction not absolute value

				//add missing zero
				result = result.toFixed(x2Length);
				return result;
			}

			// add the commas and round the number
			//parm nStr: the new level
			//parm oldLevel: the old level to comper the round format
			function addCommas(nStr, oldLevel) {
			//	alert("b4 nStr " + nStr + " old level " + oldLevel);
				//nStr = round(nStr, oldLevel);
				//alert("nStr " + nStr + " old level " + oldLevel);
				nStr += '';
				x = nStr.split('.');
				x1 = x[0];
				x2 = x.length > 1 ? '.' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}

			//the random change to the level when user not login
			//parm row: the row of the opp
			function getRandom(row){
				var rnd = Math.random();
		//		alert(" random " + rnd + "  random with cell and floor:  " + (rnd * (0.0002 - (-0.0002)) + (-0.0002)) + " 10 + randomCF  " + 10 + parseFloat(rnd * (0.0002 - (-0.0002)) + (-0.0002)) );
				var rndFloor = parseFloat(getLSValue(getOppTRSF(row, 1, 5, null, 10)));
		//		alert("111: " + getLSValue(getOppTRSF(row, 1, 5, null, 10)));
				var rndCeiling = parseFloat(getLSValue(getOppTRSF(row, 1, 5, null, 11)));
				if ( rndCeiling == 0 && rndFloor == 0 ) {
					rndFloor = -0.0001;
					rndCeiling = 0.0001;
				}
				var fnRnd = rnd * (rndCeiling - (rndFloor)) + (rndFloor);
			//	alert("floor: " + rndFloor + " cell: " + rndCeiling + "rnd " + rnd + " frand " + fnRnd );
			//	return rnd * (0.0002 - (-0.0002)) + (-0.0002);
			//	alert(fnRnd);
				return fnRnd;
			}

			//update the sleep with the random level every change
			//parm: oppId: the opp id to copy
			//parm: oppLevel the new level to write in the sleep
			function updateSlipLevel(item) {

				var levelsTd = getOppTRSF(item, 1, 3, 2, 4);
				var oppLevel = getChildOfType(levelsTd, "DIV", 1);

				// copy the level to the slip if not submitting
				if (getOppTRSF(item, 1, 5, null, 7).innerHTML == "0") {
					getOppTRSF(item, 2, 2, null, 2).innerHTML = oppLevel.innerHTML;
				}
			}

			//set opp level and 15 min level and update the slip
			//parm opp: the row of the opp in the table
			//parm value: the value for the new level
			function setOppLevelAnd15MinLevel(opp, value) {

				var levelsTd = getOppTRSF(opp, 1, 3, 2, 4);

				var div1 = getChildOfType(levelsTd, "DIV", 1); // the real level to show

//				var div2 = getChildOfType(levelsTd, "DIV", 3); // the 15 min level to copy from
				var div2 = getOppTRSF(opp, 1, 5, null, 13);
				if (null == div1 || null == div2) {
					return;
				}
				var chartValue = value;
				if (value == 0) {
					value = "<img src='../images/wait.gif' border='0' />";
					chartValue = "<img src='../images/small_wait.gif' border='0' />";
				}
				div1.innerHTML = value;
				div2.innerHTML = value;
	//			alert("Td: " + levelsTd.innerHTML + " div1: " + div1.innerHTML + " div3: " + div2.innerHTML + " value " + value);
				setChartTableOppLevel(opp, chartValue);
				updateSlipLevel(opp);
			}

			//copy real level from hidden TD to shown TD
			//useing in the timer every 15 min
			function copyRealLevel(){
				var chartIframe;
				var i = 1;
				for (i=1; i<5 ; i++) {
					var boxRow = getBox(i);
					if (null != boxRow ) {
						chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(boxRow, 1, 5, null, 2)));
						var state = getLSValue(getOppTRSF(boxRow, 1, 5, null, 5));
						if (state != OPPORTUNITY_STATE_CREATED  &&  state != OPPORTUNITY_STATE_PAUSED && state != OPPORTUNITY_STATE_SUSPENDED && state != OPPORTUNITY_STATE_CLOSED && chartIframe == null) {
							setOppLevelRandomChange(boxRow, getHiddenOppLevel(boxRow));
							updateSlipLevel(boxRow);
						}
					} else {
						break;
					}
				}
			}

			//return the value of the hidden TD with the lightstreamer level value
			//parm opp: row
			function getHiddenOppLevel(opp) {
				var levelsTd = getOppTRSF(opp, 1, 3, 2, 4);
		//		var div2 = getChildOfType(levelsTd, "DIV", 2); // the lightstreamer level
				var div2 = getOppTRSF(opp, 1, 5, null, 12);
				return div2.innerHTML;
			}