var cX = 0; var cY = 0; var rX = 0; var rY = 0;
function UpdateCursorPositionSec(e){ cX = e.pageX; cY = e.pageY;}
function UpdateCursorPositionDocAllSec(e){ cX = event.clientX; cY = event.clientY;}
if(document.all) { document.onmousemove = UpdateCursorPositionDocAllSec; }
else { document.onmousemove = UpdateCursorPositionSec; }
function AssignPositionSec(d) {
	if (skinId == 15) {
		d.style.top = "202px";
	} else {
		d.style.top = "220px";
	}
	if (skinId == 4 || skinId == 11) {
		d.style.left = "168px";
	}
	else {
		d.style.left = "810px";
	}
}

function AssignPositionSecReg(d) {
	d.style.top = "220px";
	if (skinId == 4 || skinId == 11) {
		d.style.left = "210px";
	}
	else {
		d.style.left = "810px";
	}
}

function HideContentSec(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}

function ShowContentSec(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPositionSec(dd);
dd.style.display = "block";
}

function ShowContentSecReg(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPositionSecReg(dd);
dd.style.display = "block";
}

function ReverseContentDisplaySec(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPositionSec(dd);
if(dd.style.display == "none") { dd.style.display = "block"; }
else { dd.style.display = "none"; }
}