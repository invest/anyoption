function validate1() {
	var errormsg = null;
	//errormsg = validateUserName(document.getElementById("loginForm:userName").value);
	if (errormsg == null) {
		errormsg = validatePassword(document.getElementById("loginForm:j_password").value);
	}
	if (errormsg == null) {
		return myfaces.oam.submitForm('loginForm','loginForm:loginHiddenbtn');
	} else {
		var serverErrorMsg = document.getElementById('loginForm:messages');
		//alert(serverErrorMsg.parentNode.innerHTML);
		serverErrorMsg.style.display = 'none';
		document.getElementById('loginForm:errorlogin').innerHTML = errormsg;
	}
}

function validateUserName(username) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (username == "") {
        error = emptyfield;
    } else if (username.length < 6) {
        error = usernameSmall;
    } else if (illegalChars.test(username)) {
	    error = lettersandnumbers.replace("{0} ", "");

    }

   return error;
}

function validatePassword(password) {
    var error = null;
    var illegalChars = /[\W_]/; // allow only letters and numbers

    if (password == "") {
        error = emptyfield;
    } else if (password.length < 6) {
        error = passwordSmall;
    } else if (illegalChars.test(password)) {
	    if (lettersandnumbers.indexOf("{0}") > -1 ) {
		    error = lettersandnumbers;
		}
    }

   return error;
}