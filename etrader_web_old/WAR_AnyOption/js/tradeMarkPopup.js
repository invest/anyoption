function closeTradeMarkPopup() {
	var	elem = document.getElementById('trademark_image');
    elem.parentNode.removeChild(elem);
	$('#mask_tm').hide();
	$('.window_tm').hide();
	if(document.getElementById('FlashID')){
		document.getElementById('FlashID').style.display = "block";
	}
}
   
function showTradeMarkPopup(css){
	if(document.getElementById('FlashID')){
		document.getElementById('FlashID').style.display = "none";
	}
	
	var id = '#dialog_tm';
	var html = '<div  id="trademark_image" class="container_trademark" >'+
                 '<div   id="lightbox_close" onclick="closeTradeMarkPopup()"></div></div> ';
	//Get the screen height and width
	var maskHeight = "100%";
	var maskWidth = "100%";
	

	//Set heigth and width to mask to fill up the whole screen
	$('#mask_tm').css({'width':maskWidth,'height':maskHeight});
	
	//transition effect		
	$('#mask_tm').fadeIn(500);	
	$('#mask_tm').fadeTo("slow",0.8);	

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();

	//load Template in dialog div
	$(id).append(html);
     if(css != undefined){
     	document.getElementById('trademark_image').className = css;
     } 
	
	//transition effect
	$(id).fadeIn(500); 
			
	$(document).ready(function(){
    $(this).scrollTop(0);
	});	

	//if close button is clicked
	$('.window_tm').click(function (e) {
	//Cancel the link behavior
	e.preventDefault();
	
	//var	elem = document.getElementById('trademark_image');
	//elem.parentNode.removeChild(elem);
	//id.removeChild('.container_trademark');
	//$('#mask_tm').hide();
	//$('.window_tm').hide();
	});		     
}