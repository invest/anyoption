function showAgreemen(){
	window.open(general_terms_link.toString()+general_terms_link_cid.toString(),'','scrollbars=yes,height=600,width=970,resizable=yes,toolbar=yes,screenX=0,left=1024,screenY=0,top=600');
}
function checkCountryId(){
	var iframe = document.createElement("iframe");
	iframe.src="http://cdn."+document.domain+"/getIp.php?dom="+document.domain;
	iframe.id="getIp_iframe";
	iframe.name="getIp_iframe";
	iframe.title="getIp_iframe";
	iframe.style.display="none";
	document.body.appendChild(iframe);
	// register_country_select(25,true);
}
function register_country_select(countryId,fromUrl){
	if(fromUrl){
		countryIdByIp = countryId;
		if(skinId != 1) {
			if(isBlockedCountry(countryIdByIp)){
				countryId = firstCountryId;
			}
		}
		if(typeof getQueryVariables_array['funnel_countryId'] != 'undefined'){
			countryId = getQueryVariables_array['funnel_countryId'];
		}
		general_terms_link_cid = "?refresh=true&cid=1";
	}
	if((skinId == 1)&&(!fromUrl)){
		g('funnel_phonePrefix').value = countryId;
		g('funnel_countryId').value = 1;
		g('funnel_drop_countryId').innerHTML = '0'+countryId;
	}
	if(skinId != 1) {
		g("funnel_drop_h").className = "funnel_drop_h";
		g('funnel_countryId').value = countryId;
		g('funnel_drop_country').innerHTML = countryMiniMap[countryId].a2;
		g('funnel_drop_countryId').innerHTML = "+" + countryMiniMap[countryId].phoneCode;
		general_terms_link_cid = "?refresh=true&cid="+countryId;
	}
	var reg_drop_ul = g('funnel_drop_ul').getElementsByTagName('li');
	for(var i=0; i<reg_drop_ul.length; i++){
		reg_drop_ul[i].className = reg_drop_ul[i].className.replace(/active/gi,'');
	}
	g('funnel_drop_li_'+countryId).className += " active";
	funnel_drop_close();
}
var isAllowIp;
function allowIp(flag){
	isAllowIp = flag;
 	return isAllowIp;
}
function isBlockedCountry(countryId){
	var res = false;
	switch (countryId) {
			case 1:
			case 97:
			case 99:
			case 220:
				res = true;
				break;
			}
	return res;			
}
function getQueryVariables() {
	var rtn = new Array();
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		rtn[pair[0]] = decodeURIComponent(pair[1]);
    }
    return rtn;
}
function getQueryVariables() {
	var fields_from_url = ['email','firstName','last_Name','funnel_countryId','mobilePhone'];
	var rtn = new Array();
	rtn[0] = 0;
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
		if(fields_from_url.indexOf(pair[0]) > -1){
			rtn[pair[0]] = decodeURIComponent(pair[1]);
			rtn[0]++;
		}
    }
    return rtn;
}
var getQueryVariables_array = getQueryVariables();
function get_funnel_html(){
	getQueryVariables()
	if(!g('funnel_firstName')){
		$.ajax({
			url: context_path_ajax+"/jsp/register_content.jsf?s="+skinId,
			success:function(result){
				g(funnel_div_id).innerHTML = result;
				checkCountryId();
				if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
				g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
				funnel_fill_inputs();
			}
		});
	}else{
		checkCountryId();
		if(typeof isShortForm == "undefined"){var isShortForm = 'true';}
		g('register_from_short_reg').value = (isShortForm === 'false')?"false":"true";
		funnel_fill_inputs();
		if(getQueryVariables_array[0] > 1){
			funnel_submit();
			try {
				g('full_reg_top_text_default').style.display='none';
				g('full_reg_top_text_alt').style.display='block';
			} catch(e) {;}
		}
	}
}
function funnel_fill_inputs(){
	if(typeof getQueryVariables_array['firstName'] != 'undefined'){
		g('funnel_firstName').value = getQueryVariables_array['firstName'];
	}
	if(typeof getQueryVariables_array['last_Name'] != 'undefined'){
		g('funnel_lastName').value = getQueryVariables_array['last_Name'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['email'] != 'undefined'){
		g('funnel_email').value = getQueryVariables_array['email'];
	}
	if(typeof getQueryVariables_array['mobilePhone'] != 'undefined'){
		var sub = '';
		if(typeof getQueryVariables_array['mobilePhoneCode'] != 'undefined'){
			getQueryVariables_array['mobilePhoneCode'] = getQueryVariables_array['mobilePhoneCode'].toString().replace('+','');
			if(getQueryVariables_array['mobilePhoneCode'].length > 3){
				sub = getQueryVariables_array['mobilePhoneCode'].substr(1,10);
			}
		}
		
		g('funnel_mobilePhone').value = sub+getQueryVariables_array['mobilePhone'].toString();
	}
}
/* insert into register attempts AJAX call */
function insertUpdateRegisterAttempts() {
	var isMarketingTracking = (typeof getQueryVariables_array['marketingTracking'] != 'undefined')?getQueryVariables_array['marketingTracking']:false;
	
	var firstName = g("funnel_firstName");
	var lastName = g("funnel_lastName");
	var email = g("funnel_email");
	var countryId = g("funnel_countryId");
	var landMobilePhone = g("funnel_mobilePhone");

	if((emailValidationRegEx.test(email.value))||(landMobilePhone.value.length > 6)){
		var dataString = '';
		if(typeof isShortForm == undefined){var isShortForm = 'true';}
		dataString += (isShortForm === 'false')?"emailRegisterAttempts":"quickStartEmailRegisterAttempts";

		dataString += "=" + encodeURIComponent((email.value !== email.defaultValue)?email.value:'')
			+ "&firstNameRegisterAttempts="	+ encodeURIComponent((firstName.value !== firstName.defaultValue)?firstName.value:'')
			+ "&lastNameRegisterAttempts=" + encodeURIComponent((lastName.value !== lastName.defaultValue)?lastName.value:'')
			+ "&mobilePhoneRegisterAttempts=" + encodeURIComponent((landMobilePhone.value !== landMobilePhone.defaultValue)?landMobilePhone.value:'')
			+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value)
			+ "&isShortForm=" + isShortForm
			+ "&marketingTracking=" + isMarketingTracking;
		$.ajax({
			type: "POST",
			url: context_path_ajax+"/jsp/ajax.jsf",
			data: dataString
		});
	}
}
function funnel_submit(){
	var btn_open_account = g('btn_open_account');
	btn_open_account.className += " disabled";
	btn_open_account.onclick = function(){return false;}
	
	var selectedCountryId = parseInt(g('funnel_countryId').value);
	if(((isBlockedCountry(countryIdByIp) && !isAllowIp) || isBlockedCountry(selectedCountryId)) && skinId != 1) {	    
		showErrorsBlocked(isBlockedCountry(selectedCountryId));
		return;					
	}
	isShowErrors = false;
	isAsync = false;
	validateCheckbox();
	validatePasswordConfirm();
	validatePassword();
	validatePhoneNumberTag();
	validateEmail();
	validateLastName();
	validateFirstName();
	isAsync = true;
	isShowErrors = true;
	if(typeof register_page == 'undefined'){register_page = false;}
	if((errorMap.size() <= 2)||(register_page)){
		showErrors();
	}
	if (errorMap.size() == 0) {
		g('funnel_form').submit();
	}
	else{
		handle_error_count(errorMap.size());
	}
}
function funnel_results(jsonRes){
	var count = 0;
	for (var key in jsonRes) {
		if (jsonRes.hasOwnProperty(key)) {
			if(key == "forbiddenRegisterRedirect"){
				window.location = jsonRes[key];
				return;
			}else{
				var field = g(key);
				field.className = field.className.replace(/correct/g,'');
				field.className += " error";
				var error_msg_holder = g(key+"_error_txt");
				error_msg_holder.innerHTML = jsonRes[key];
				error_msg_holder = g(key+"_error_txt_in");
				error_msg_holder.innerHTML = jsonRes[key];
				g(key+"_error").className = "funnel_error_bad";

				count++;
			}
		}
	}
	handle_error_count(count);
}
function handle_error_count(count){
	if(typeof register_page == 'undefined'){register_page = false;}
	if(count === 0){//you are registered and logged -> redirecting to deposit
		window.location = deposit_page_url;
	}else if(count < 3){//you are not so bad, stay on page and fill correctly
		var btn_open_account = g('btn_open_account');
		btn_open_account.className = btn_open_account.className.replace(" disabled","");
		btn_open_account.onclick = function(){funnel_submit();return false;}
		return false;
	}else{//you cannot be more wrong go to full reg
		if(!register_page){//if short reg
			var firstName = g("funnel_firstName");
			var lastName = g("funnel_lastName");
			var email = g("funnel_email");
			var countryId = g("funnel_countryId");
			var landMobilePhone = g("funnel_mobilePhone");
			
			var url = '';
			url += ((email.value !== "")&&(email.value !== email.defaultValue))?((url === '')?"?":"&")+"email=" + encodeURIComponent(email.value):'';
			url += ((firstName.value !== "")&&(firstName.value !== firstName.defaultValue))?((url === '')?"?":"&")+"firstName=" + encodeURIComponent(firstName.value):'';
			url += ((lastName.value !== "")&&(lastName.value !== lastName.defaultValue))?((url === '')?"?":"&")+"last_Name=" + encodeURIComponent(lastName.value):'';
			url += (countryId.value != "")?((url === '')?"?":"&")+"funnel_countryId=" + encodeURIComponent(countryId.value):'';
			url += ((landMobilePhone.value !== "")&&(landMobilePhone.value !== landMobilePhone.defaultValue))?((url === '')?"?":"&")+"mobilePhone=" + encodeURIComponent(landMobilePhone.value):'';
			window.location = register_page_url+url;
		}
		else{
			var btn_open_account = g('btn_open_account');
			btn_open_account.className = btn_open_account.className.replace(" disabled","");
			btn_open_account.onclick = function(){funnel_submit();return false;}
		}
	}
}
function newCard_submit(full){
	var btn = g('btn_newCard');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateHolderName();
	validateExpYear();
	validateExpMonth();
	validateCCPass();
	validateCCNumber();
	validateCCType();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('newCardForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('newCardForm','newCardForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_newCard');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){newCard_submit(full);}
	}
}
function direct24_submit(full, paymentType){
	var btn = g('btn_direct24_'+paymentType);
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	validateBenefiName();
	validateSortCode();
	validateDirectAccNum();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('direct24_'+paymentType).onsubmit = function(){return true;}
		return myfaces.oam.submitForm('direct24_'+paymentType,'direct24_'+paymentType+':hiddenDeposit');
	}
	else{
		var btn = g('btn_direct24_'+paymentType);
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){direct24_submit(full, paymentType);}
	}
}
function eps_submit(full){
	var btn = g('btn_epsForm');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	validateBenefiName();
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('epsForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('epsForm','epsForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_epsForm');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){eps_submit(full);}
	}
}
function baropay_submit(full){
	var btn = g('btn_baropay');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateBaroLandLinePhone();
	validateSenderName();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('baroPayDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('baroPayDepositForm','baroPayDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_baropay');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){baropay_submit(full);}
	}
}
function ukash_submit(full){
	var btn = g('btn_ukashFrom');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateUkashVoucherNumber();
	validateDepositAmount(g('ukashFrom:voucherValue'));
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('ukashFrom').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('ukashFrom','ukashFrom:hiddenDeposit');
	}
	else{
		var btn = g('btn_ukashFrom');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){ukash_submit(full);}
	}
}
function webmoney_submit(full){
	var btn = g('btn_webmoney');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('webMoneyDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('webMoneyDepositForm','webMoneyDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_webmoney');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){webmoney_submit(full);}
	}
}
function envoyDeposit_submit(full){
	var btn = g('btn_envoyDeposit');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('directDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('directDepositForm','directDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_envoyDeposit');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){envoyDeposit_submit(full);}
	}
}
function moneybookers_submit(full){
	var btn = g('btn_moneybookers');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('moneybookersDepositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('moneybookersDepositForm','moneybookersDepositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_moneybookers');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){moneybookers_submit(full);}
	}
}
function deltapay_submit(full){
	var btn = g('btn_deltapay');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	if(full){
		validateZipCode();
		validateCityName();
		validateStreet();
	}
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('deltaPayForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('deltaPayForm','deltaPayForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_deltapay');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){deltapay_submit(full);}
	}
}
function oldCard_submit(full){
	var btn = g('btn_oldCard');
	btn.className += " disabled";
	btn.onclick = function(){return false;}
	isShowErrors = false;
	isAsync = false;
	validateCCSelect();
	validateDepositAmount(null);
	isAsync = true;
	isShowErrors = true;
	showErrors();
	if (errorMap.size() == 0) {
		g('depositForm').onsubmit = function(){return true;}
		return myfaces.oam.submitForm('depositForm','depositForm:hiddenDeposit');
	}
	else{
		var btn = g('btn_oldCard');
		btn.className = btn.className.replace(" disabled",'');
		btn.onclick = function(){oldCard_submit(full);}
	}
}
//function used to clear the form after clicking back button
function reColorFields(){
	try{
		inpFocusShort(g('funnel_firstName'),'t');
		inpFocusShort(g('funnel_lastName'),'t');
		inpFocusShort(g('funnel_email'),'t');
		inpFocusShort(g('funnel_mobilePhone'),'t');
		inpFocusShort(g('funnel_password'),'t');
		inpFocusShort(g('funnel_passwordConfirm'),'t');
		if(g('funnel_terms').checked){
			g('funnel_terms').parentNode.className += " checked";
		}
	}catch(e){;}
}
function funnel_drop(th){
	th.parentNode.className += " active";
}
var funnel_drop_timer = null;
function funnel_drop_close(){
	clearTimeout(funnel_drop_timer);
	function doIt(){
		var el = g('funnel_drop_h');
		el.className = el.className.replace(/active/g,'');
	}
	funnel_drop_timer = setTimeout(doIt,100);
}
function funnel_drop_keep_open(){
	clearTimeout(funnel_drop_timer);
}
addScriptToHead('map.js','initRegForm_hashmap');