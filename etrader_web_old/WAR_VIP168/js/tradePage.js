var OPPORTUNITY_STATE_CREATED			= 1;
var OPPORTUNITY_STATE_OPENED			= 2;
var OPPORTUNITY_STATE_LAST_10_MIN		= 3;
var OPPORTUNITY_STATE_CLOSING_1_MIN		= 4;
var OPPORTUNITY_STATE_CLOSING			= 5;
var OPPORTUNITY_STATE_CLOSED			= 6;
var OPPORTUNITY_STATE_DONE				= 7;
var OPPORTUNITY_STATE_PAUSED			= 8;
var OPPORTUNITY_STATE_SUSPENDED			= 9;
var OPPORTUNITY_STATE_WAITING_TO_PAUSE  = 10;

var SUBSCR_FLD_KEY						= 1;
var SUBSCR_FLD_COMMAND					= 2;
var SUBSCR_FLD_ET_NAME					= 3;
var SUBSCR_FLD_AO_LEVEL					= 4;
var SUBSCR_FLD_ET_ODDS					= 5;
var SUBSCR_FLD_ET_EST_CLOSE				= 6;
var SUBSCR_FLD_ET_OPP_ID				= 7;
var SUBSCR_FLD_ET_ODDS_WIN				= 8;
var SUBSCR_FLD_ET_ODDS_LOSE				= 9;
var SUBSCR_FLD_ET_STATE					= 10;
var SUBSCR_FLD_AO_CLR					= 11;
var SUBSCR_FLD_PRIORITY					= 12;
var SUBSCR_FLD_ET_GROUP_CLOSE			= 13;
var SUBSCR_FLD_ET_SCHEDULED				= 14;
var SUBSCR_FLD_ET_RND_FLOOR				= 15;
var SUBSCR_FLD_ET_RND_CEILING			= 16;
var SUBSCR_FLD_ET_GROUP					= 17;
var SUBSCR_FLD_ET_SUSPENDED_MESSAGE		= 18;
var SUBSCR_FLD_AO_MARKETS				= 19;
var SUBSCR_FLD_AO_STATES				= 20;
var SUBSCR_FLD_FILTER_SCHEDULED			= 21;
var SUBSCR_FLD_LAST_INVEST				= 22;
var SUBSCR_FLD_TIME_STAMP				= 23;
var SUBSCR_FLD_AO_FLAGS					= 24;
var SUBSCR_FLD_ODDS_GROUP				= 25;
var SUBSCR_FLD_AO_CALLS_TREND			= 26;
var SUBSCR_FLD_MOBILE_MARKET_LIST_INDEX = "mmListIndex";

var ELEMENT_TYPE_TABLE					= 0;
var ELEMENT_TYPE_ROW					= 1;
var ELEMENT_TYPE_CELL					= 2;

var markets = null;
var marketStates = null;

var marketsTimestamp = 0;

var isNotLogInError = false;

var strHref = window.location.href;

var fromGraph = 0; //No from LIVE-AO, profitline, option + or binary 0-100
var isLiveAOPage = false;

function updateItemAO(itemPos, updateInfo, itemName) {
	try {
		var logEntry = itemName + "<br />";
		for (i = 1; i <= updateInfo.getNumFields(); i++) {
			logEntry = logEntry + i +
					"<br />isChanged: " + updateInfo.isValueChanged(i) +
			 		"<br />oldValue: " + updateInfo.getOldValue(i) +
			 		"<br />newValue: " + updateInfo.getNewValue(i) +
			 		"<br />";
		}
		log(logEntry);


		var timeStamp = updateInfo.getNewValue(SUBSCR_FLD_TIME_STAMP);
		var shouldUpdateMobileSelects = false;

		// handle market states
		if (updateInfo.isValueChanged(SUBSCR_FLD_AO_STATES)) {
			log("market states changed: " + updateInfo.getNewValue(SUBSCR_FLD_AO_STATES));
			var ms = new Array(0);
			var aoStates = updateInfo.getNewValue(SUBSCR_FLD_AO_STATES);
			if (null != aoStates) {
				ms = updateInfo.getNewValue(SUBSCR_FLD_AO_STATES).split("|");
			}

			if (timeStamp > marketsTimestamp) {
				log("market states updated.");
				if (ms.length > 0 && ms[0].length > 0) {
					var newMarketStates = new Array(ms.length);
					for (i = 0; i < ms.length; i++) {
						var cms = new Array(2);
						cms[0] = ms[i];
						cms[1] = 1;
						newMarketStates[i] = cms;
					}
					marketStates = newMarketStates;
				} else {
					marketStates = new Array(0);
				}

				if (checkAllClose()) {
					//show off hour banner 0
					switchTable(0);
					try {
						switchLiveTrends(0);
					} catch(e) {
					}
					try {
						switchLiveTable(0);
					} catch(e) {
					}
				} else {
					//show the market table 1
					switchTable(1);
					try {
						switchLiveTrends(1);
					} catch(e) {
					}
					try {
						switchLiveTable(1);
					} catch(e) {
					}

				}
			}
			shouldUpdateMobileSelects = true;
		}

		// handle home page markets
		if (updateInfo.isValueChanged(SUBSCR_FLD_AO_MARKETS) || null == markets) {
			log("home page markets changed: " + updateInfo.getNewValue(SUBSCR_FLD_AO_MARKETS));
			if (null == markets) { // in the first update init the two empty arrays
				var newMarkets = new Array(4);
				for (i = 0; i < group.length; i++) {
					var mv = group[i].split("_");
					var m = new Array(3);
					m[0] = mv[2];
					m[1] = i;
					m[2] = mv[1];
					newMarkets[i] = m;
				}
				for (i = 0; i < group.length; i++) {
					boxesMarkets[i + 1] = newMarkets[i][0];
				}
				markets = newMarkets;
			}

			var ms = updateInfo.getNewValue(SUBSCR_FLD_AO_MARKETS).split("|");
			if (timeStamp > marketsTimestamp) {
				log("markets updated.");
				var newMarkets = new Array(4);
				for (i = 0; i < 4; i++) {
					var m = new Array(3);
					m[0] = ms[i];
					m[1] = i;
					m[2] = 1;
					newMarkets[i] = m;
				}
				log("oldMarkets: " + markets + " newMarkets: " + newMarkets);
				markets = newMarkets;
				scheduleStartBoxes();
			}
		}

		if (timeStamp > marketsTimestamp) {
			marketsTimestamp = timeStamp;
		}

		// update the boxes
		for (i = 0; i < group.length; i++) {
			if (itemName == group[i]) {
				updateItemNonVisual(i, itemPos, updateInfo);
			}
		}

		if (shouldUpdateMobileSelects) {
			// in the mobile update the markets selects according to the market states
			log("Updating mobile selects");
			var i;
			for (i = 0; i < 2; i++) {
				var box = document.getElementById("box" + i);
				if (null != box) {
					log("Box i = " + i + " id: " + box.id);
					var boxOpp = null;
					var j = 1;
					while ((boxOpp = getChildOfType(box, "DIV", j)) != null) {
						log("Box i = " + i + " oppId: " + boxOpp.id);
						var boxOppSel = document.getElementById(boxOpp.id + "_markets");
						if (null != boxOppSel) {
							markClosedMarket(boxOppSel, document.getElementById(boxOpp.id + "_marketId").innerHTML);
						}
						j++;
					}

				}
			}
		}
	} catch (e) {
		log(e);
	}
}

//switch the markets table to "off hours banner"
//parm flag: 1 to show markets table, 0 to show off hours banner
function switchTable(flag) {
	var mTbl = document.getElementById("markets_table");
	var oHB = document.getElementById("off_hours_banner");
	var notl = document.getElementById("noOneTouchLinkDiv");
	var otl = document.getElementById("oneTouchLinkDiv");
	var hoHP = document.getElementById("howToHpBanner");
	var otHP = document.getElementById("oneTouchHpBanner");
	var profitLineBtn = document.getElementById("profitLineDiv"); //profit line open button
	if (flag == 1) {
		mTbl.style.display = 'block';
		oHB.style.display = 'none';
		if (otl != null) {
			notl.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
			otl.style.display = 'none';
		}
		if (otHP != null) {
			hoHP.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
			otHP.style.display = 'none';
		}
		if (profitLineBtn != null) {
			profitLineBtn.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}

	} else {
		mTbl.style.display = 'none';
		if (homePage_check) {
			var imageArray  = oHB.getElementsByTagName("IMG");
			imageArray[0].src = image_context_path + "/hp_offhours.jpg";
		}
		oHB.style.display = 'block';
		if (otl != null) {
			notl.style.display = 'none';
			otl.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}
		if (otHP != null) {
			hoHP.style.display = 'none';
			otHP.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}
		if (profitLineBtn != null) {
			profitLineBtn.style.display = 'none';
		}
	}
}

//check if all opps on home page are closed ( state 1 created)
//return 1 if all close else 0
function checkAllClose() {
	if (skinId == 9) {
		return 1;
	}
	for (j = 0; j < marketStates.length; j++) {
		if (marketStates[j][1] > 0 && getMarketName(marketStates[j][0]) != "") {
			return 0;
		}
	}
	return 1;
}

function setMobileSelectsCurrentValues(id, boxMarketId, scheduled, filterScheduled, timeEstClose) {
	//select the market in the select box
	var marketsSelectBox = document.getElementById(id + "_markets");
//	var boxMarketId = updateInfo.getNewValue(SUBSCR_FLD_ET_NAME); //SUBSCR_FLD_ET_NAME = market id
	for (var i = 0; i < marketsSelectBox.length; i++) {
		if (marketsSelectBox.options[i].value == boxMarketId) {
			marketsSelectBox.selectedIndex = i;
			document.getElementById(id + "_" + SUBSCR_FLD_MOBILE_MARKET_LIST_INDEX).innerHTML = i;
			break;
		}
	}

	//select the scheduled in the select box
	var scheduledSelectBox = document.getElementById(id + "_scheduledSelect");
//	var Scheduled = updateInfo.getNewValue(SUBSCR_FLD_ET_SCHEDULED);
	scheduledSelectBox.options[scheduled - 1].selected = true;
//	scheduledSelectBox.options[scheduled - 1].text = formatDate(new Date(updateInfo.getNewValue(SUBSCR_FLD_ET_EST_CLOSE)));
	scheduledSelectBox.options[scheduled - 1].text = timeEstClose;
//	var filterScheduled = updateInfo.getNewValue(SUBSCR_FLD_FILTER_SCHEDULED);
	if (scheduledSelectBox.length > filterScheduled) {
		switch(filterScheduled) {
			case "1":
				scheduledSelectBox.remove(3);
				scheduledSelectBox.remove(2);
				scheduledSelectBox.remove(1);
			  	break;
			case "2":
				scheduledSelectBox.remove(3);
				scheduledSelectBox.remove(2);
			  	break;
			case "3":
				scheduledSelectBox.remove(3);
			  	break;
			case "4":
			  	break;
			default:
		}
	}
}

function updateItemNonVisual(boxNumber, itemPos, updateInfo) {
	var box = document.getElementById("box" + boxNumber);
	var id = updateInfo.getNewValue(SUBSCR_FLD_KEY).substring(3);
	var opp = document.getElementById(id);
	if (null == opp && updateInfo.getNewValue(SUBSCR_FLD_COMMAND) == "DELETE") {
		return;
	}
	if (updateInfo.getNewValue(SUBSCR_FLD_COMMAND) == "DELETE") {
		log("DELETE " + id);
		box.removeChild(opp);

		if (boxesOpportunities[boxNumber].length > 1) {
			var boxOpportunities = boxesOpportunities[boxNumber];
			boxesOpportunities[boxNumber] = new Array(1);
			boxesOpportunities[boxNumber][0] = boxOpportunities[1];

			var otherOpp = document.getElementById(boxOpportunities[1]);
			showElm(otherOpp, ELEMENT_TYPE_TABLE);
			if (isMobileParm) {
				resize();
			}
		} else {
			boxesOpportunities[boxNumber] = new Array(0);
			document.getElementById("box" + boxNumber).innerHTML = document.getElementById("template_loading").innerHTML;
		}

		return;
	}
	var added = false;
	if (null == opp) {
		log("ADD " + id);
		added = true;
		var template = document.getElementById("template_opportunity" + (portrait ? "_portrait" : ""));
		if (!hasFlash) {
			template = document.getElementById("template_opportunity_nf");
		}
		var str = template.innerHTML.replace(/'oppId'/g, id);
		str = str.replace(/'marketId'/g, updateInfo.getNewValue(SUBSCR_FLD_ET_NAME));
		var tmpDiv = document.getElementById("tmpdiv");
		tmpDiv.innerHTML = str;
		if (isMobileParm) {
			setMobileSelectsCurrentValues(id,
					updateInfo.getNewValue(SUBSCR_FLD_ET_NAME),
					updateInfo.getNewValue(SUBSCR_FLD_ET_SCHEDULED),
					updateInfo.getNewValue(SUBSCR_FLD_FILTER_SCHEDULED),
					formatDate(new Date(updateInfo.getNewValue(SUBSCR_FLD_ET_EST_CLOSE)), false));
		}

		opp = document.getElementById(id);
		var boxOpportunities = boxesOpportunities[boxNumber];
		if (boxOpportunities.length > 0) {
			var otherOppState = document.getElementById(boxOpportunities[0] + "_" + SUBSCR_FLD_ET_STATE).innerHTML;
			if (updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) > otherOppState) {
				var otherOpp = document.getElementById(boxOpportunities[0]);
				otherOpp.style.display = "none";
				box.insertBefore(opp, otherOpp);
				boxesOpportunities[boxNumber] = new Array(2);
				boxesOpportunities[boxNumber][0] = id;
				boxesOpportunities[boxNumber][1] = boxOpportunities[0];
			} else {
				opp.style.display = "none";
				box.appendChild(opp);
				boxesOpportunities[boxNumber] = new Array(2);
				boxesOpportunities[boxNumber][0] = boxOpportunities[0];
				boxesOpportunities[boxNumber][1] = id;
			}
		} else {
			box.innerHTML = "";
			boxesOpportunities[boxNumber] = new Array(1);
			boxesOpportunities[boxNumber][0] = id;
			box.appendChild(opp);
		}
		tmpDiv.innerHTML = "";

		if (isMobileParm) {
			try {
				boxCharts[boxNumber] = new BoxChart(updateInfo.getNewValue(SUBSCR_FLD_ET_NAME), id, id + "_chart");
				boxCharts[boxNumber].init();
			} catch (e) {
				log(e);
			}
		}
	}

	var stateReallyChanged = updateInfo.isValueChanged(SUBSCR_FLD_ET_STATE);
	if (document.getElementById(id + "_" + SUBSCR_FLD_ET_STATE).innerHTML == updateInfo.getNewValue(SUBSCR_FLD_ET_STATE)) {
		stateReallyChanged = false;
	}
	var oppChanged = updateInfo.isValueChanged(SUBSCR_FLD_ET_OPP_ID);
	if (document.getElementById(id + "_" + SUBSCR_FLD_ET_OPP_ID).innerHTML == updateInfo.getNewValue(SUBSCR_FLD_ET_OPP_ID)) {
		oppChanged = false;
	}

	var callsTrendChanged = false;
	if (!isMobileParm) {
		if (document.getElementById(id + "_" + SUBSCR_FLD_AO_CALLS_TREND).innerHTML == updateInfo.getNewValue(SUBSCR_FLD_AO_CALLS_TREND)) {
			callsTrendChanged = false;
		} else {
			callsTrendChanged = true;
		}
	}
	
	var aoFlagsChanged;
	if (document.getElementById(id + "_" + SUBSCR_FLD_AO_FLAGS).innerHTML == updateInfo.getNewValue(SUBSCR_FLD_AO_FLAGS)) {
		aoFlagsChanged = false;
	} else {
		aoFlagsChanged = true;
	} 
	
	
	if (added || stateReallyChanged) {
		changeBoxState(
				id,
				updateInfo.getNewValue(SUBSCR_FLD_ET_NAME),
				updateInfo.getNewValue(SUBSCR_FLD_ET_STATE),
				updateInfo.getNewValue(SUBSCR_FLD_ET_EST_CLOSE),
				updateInfo.getNewValue(SUBSCR_FLD_LAST_INVEST),
				updateInfo.getNewValue(SUBSCR_FLD_ET_SUSPENDED_MESSAGE));
	}

	if (document.getElementById(id + "_submit").innerHTML == 1) {
		return;
	}

	for (i = 1; i <= schema.length; i++) {
		if (added
				|| (updateInfo.isValueChanged(i) && !updateInfo.isSnapshot())
				|| (updateInfo.isSnapshot() && oppChanged)
				|| (i == SUBSCR_FLD_AO_CLR && stateReallyChanged)
				|| (i == SUBSCR_FLD_AO_CALLS_TREND && callsTrendChanged)
				|| (i == SUBSCR_FLD_AO_FLAGS && aoFlagsChanged)) {
			var tag = document.getElementById(id + "_" + i);
			if (null != tag) {
				if (i == SUBSCR_FLD_ET_NAME) {
					tag.innerHTML = getMarketName(updateInfo.getNewValue(i));
				} else if (i == SUBSCR_FLD_AO_LEVEL) {
					if (updateInfo.getNewValue(i) == 0) {
						tag.innerHTML = "<img src='"+image_path+"/wait.gif' border='0' />";
					} else {
						tag.innerHTML = updateInfo.getNewValue(i);
					}
				} else if (i == SUBSCR_FLD_ET_EST_CLOSE && updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) != OPPORTUNITY_STATE_CREATED) {
					tag.innerHTML = formatDate(new Date(updateInfo.getNewValue(i)), true);
				} else if (i == SUBSCR_FLD_ET_STATE && (updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_CREATED ||
														updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_PAUSED ||
														updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) == OPPORTUNITY_STATE_SUSPENDED)) {
//					document.getElementById(id + "_return").style.display = "none";
					document.getElementById(id + "_scheduled").style.display = "none";
					if (!isMobileParm) {
						showElm(document.getElementById(id + "_empty"), ELEMENT_TYPE_CELL);
					} else {
//						document.getElementById(id + "_bottom_line").style.display = "none";
					}
					tag.innerHTML = updateInfo.getNewValue(i);
					if (i == 8  && !isMobileParm){
						document.getElementById(id + "_returnS").innerHTML = tag.innerHTML;
					}else if (i == 9  && !isMobileParm){
						document.getElementById(id + "_refundS").innerHTML = tag.innerHTML;
					}
				} else {
					if (i == SUBSCR_FLD_ET_ODDS && (skinId == 4 || skinId == 3)) {
						var myregexp = /[^%]*/;
						var match = myregexp.exec(updateInfo.getNewValue(i));
						if (match != null) {
							tag.innerHTML = '%' + match[0];
						} else {
							tag.innerHTML = updateInfo.getNewValue(i);
						}
					} else {
						tag.innerHTML = updateInfo.getNewValue(i);
						if (i == 8 && !isMobileParm){
							document.getElementById(id + "_returnS").innerHTML = tag.innerHTML;
							var tmpAbv = (tag.innerHTML*100)-100;
							document.getElementById(id + "_above").innerHTML = "("+tmpAbv+"%):"
						}else if (i == 9 && !isMobileParm){
							document.getElementById(id + "_refundS").innerHTML = tag.innerHTML;
							var tmpBlw = tag.innerHTML*100;
							document.getElementById(id + "_below").innerHTML = "("+tmpBlw+"%):"
						}
					}
				}
			}
			if (i == SUBSCR_FLD_AO_CLR &&
					updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) != OPPORTUNITY_STATE_CREATED &&
					updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) != OPPORTUNITY_STATE_PAUSED &&
					updateInfo.getNewValue(SUBSCR_FLD_ET_STATE) != OPPORTUNITY_STATE_SUSPENDED) {
				log("clr changed: " + updateInfo.getNewValue(i));
				var cls = "level_green";
				if (updateInfo.getNewValue(i) == "0") {
					cls = "level_red";
				}
				var level_tag = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
				if (null != level_tag) {
					var oldLevelClass = level_tag.attributes["class"].value;
					if (oldLevelClass.indexOf("slip") > -1) {
						cls = "slip_" + cls;
					}
					level_tag.attributes["class"].value = cls;
				}
			}
			
			if (!isMobileParm) {								
				if ((i == SUBSCR_FLD_AO_FLAGS && aoFlagsChanged)
						|| (i == SUBSCR_FLD_ET_STATE && stateReallyChanged)) {
					var state = updateInfo.getNewValue(SUBSCR_FLD_ET_STATE);
					if (state == OPPORTUNITY_STATE_OPENED || state == OPPORTUNITY_STATE_LAST_10_MIN) {
						if ((updateInfo.getNewValue(SUBSCR_FLD_AO_FLAGS) & 2) != 0) {
							document.getElementById(id + "_trend_pie").style.display = "";
						} else {
							document.getElementById(id + "_trend_pie").style.display = "none";
							closeTrendPopup(id + "_tradingbx_trend_msg");
						}
					} else {
						document.getElementById(id + "_trend_pie").style.display = "none";
						closeTrendPopup(id + "_tradingbx_trend_msg");
					}
				}
				
				if (i == SUBSCR_FLD_AO_CALLS_TREND) {
					if (callsTrendChanged
							&& updateInfo.getNewValue(SUBSCR_FLD_AO_CALLS_TREND) != null) {
						var trends_call = Math.round(updateInfo.getNewValue(SUBSCR_FLD_AO_CALLS_TREND) * 100);
						var trends_put = Math.round(100 - trends_call);
						var trends_call_percent = trends_call + "%";
						var trends_put_percent = trends_put + "%";
						var trendsCallWidth;
						var trendsPutWidth;
						if (trends_call != "0") {
							if (trends_call == 100) {
								trendsCallWidth = trends_call + "%";
							} else {
								trendsCallWidth = (trends_call - 1) + "%";
							}
						} else {
							trendsCallWidth = "0%";
							trends_call_percent = '';
						}
						if (trends_put != "0") {
							if (trends_put == 100) {
								trendsPutWidth = trends_put + "%";
							} else {
								trendsPutWidth = (trends_put - 1) + "%";
							}
						} else {
							trendsPutWidth = "0%";
							trends_put_percent = '';
						}
						
						document.getElementById(id + "_tradingbx_chart_call").style.width = trendsCallWidth;
						document.getElementById(id + "_tradingbx_chart_call_label").innerHTML = trends_call_percent;
						document.getElementById(id + "_tradingbx_chart_put").style.width = trendsPutWidth;
						document.getElementById(id + "_tradingbx_chart_put_label").innerHTML = trends_put_percent;
						
						if (trendsCallWidth == "0%" || trendsPutWidth == "0%") {
							document.getElementById(id + "_tradingbx_chart_separator").style.display = 'none';
						} else {
							document.getElementById(id + "_tradingbx_chart_separator").style.display = 'block';
						}
					}
				}
			}
			
		}
	}
	if (id == boxesOpportunities[boxNumber][0]) {
		try {
			if (isMobileParm) {
				boxCharts[boxNumber].updateItem(
						updateInfo.getNewValue(SUBSCR_FLD_AO_LEVEL),
						updateInfo.getNewValue(SUBSCR_FLD_ET_STATE),
						updateInfo.getNewValue(SUBSCR_FLD_TIME_STAMP));
			} else {
				var chart = getMovie(id + "_chart");
				chart.updateItem(
						updateInfo.getNewValue(SUBSCR_FLD_AO_LEVEL),
						updateInfo.getNewValue(SUBSCR_FLD_ET_STATE),
						updateInfo.getNewValue(SUBSCR_FLD_TIME_STAMP));
			}
		} catch (e) {
//			alert(e);
		}
	}
}

function changeBoxState(id, marketId, state, time, timeLastInvest, suspendMessage) {
	log("state changed - id: " + id + " marketId: " + marketId + " state: " + state + " time: " + time + " timeLastInvest: " + timeLastInvest + " suspendMessage: " + suspendMessage);
	var oldLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
	var oldLvlVal = null;
	var oldLvlCls = null;
	if (null != oldLvl) {
		oldLvlVal = oldLvl.innerHTML;
		if (oldLvl.attributes.getNamedItem("class").value.indexOf("green") > -1) {
			oldLvlCls = "level_green";
		} else {
			oldLvlCls = "level_red";
		}
	}

	var slip = document.getElementById(id + "_slip");
	var template = document.getElementById("template_state_" + state + (portrait ? "_portrait" : ""));
	slip.innerHTML = template.innerHTML.replace(/'oppId'/g, id);

	if (null != oldLvlVal && state != OPPORTUNITY_STATE_SUSPENDED) {
		var newLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
		newLvl.innerHTML = oldLvlVal;
		newLvl.attributes.getNamedItem("class").value = oldLvlCls;
	}

	if (state == OPPORTUNITY_STATE_CREATED || state == OPPORTUNITY_STATE_PAUSED) {
		var str;
		if (hasFlash) {
			str = document.getElementById("template_chart_not_opened").innerHTML;
		} else {
			str = document.getElementById("template_closed").innerHTML;
		}
		if (null == time) {
			time = document.getElementById(id + "_" + SUBSCR_FLD_ET_EST_CLOSE).innerHTML;
		}
		str = str.replace(/{expTime}/g, formatDate(new Date(time), false));
		document.getElementById(id + "_chart_holder").innerHTML = str;

	} else if (state >= OPPORTUNITY_STATE_OPENED && state <= OPPORTUNITY_STATE_CLOSED) {
		var scheduled = getScheduled(getMarketId(id));
		if (scheduled == 1 || scheduled == 2) {
			if (hasFlash) {
				var chart = document.getElementById(id + "_chart");
				if (null == chart) {
					var str = document.getElementById("template_chart").innerHTML;
					str = str.replace(/'oppId'/g, id);
					str = str.replace(/'marketId'/g, marketId);
					str = str.replace(/{state}/g, state);
					document.getElementById(id + "_chart_holder").innerHTML = str;
				}
			}
			if (state == OPPORTUNITY_STATE_LAST_10_MIN) {
				var timeLeft = adjustFromUTCToLocal(new Date(timeLastInvest)).getTime() - new Date().getTime() - serverOffsetMillis;
				if (timeLeft < 0) {
					tiimeLeft = 0;
				}
				var min = Math.floor(timeLeft / 60000);
				timeLeft = timeLeft - min * 60000;
				var sec = Math.floor(timeLeft / 1000);
				startCountDownAO(id, min, sec);
			}
			if (hasFlash) {
				if (state >= OPPORTUNITY_STATE_CLOSING_1_MIN && state <= OPPORTUNITY_STATE_CLOSED) {
					try {
						chart = getMovie(id + "_chart");
						chart.showWaitingForExpiry();
					} catch (e) {
						// do nothing
					}
				}
			}
			if (isMobileParm && state >= OPPORTUNITY_STATE_CLOSING_1_MIN && state <= OPPORTUNITY_STATE_CLOSED) {
				for (i = 0; i < boxesOpportunities.length; i++) {
					if (boxCharts[i].oppId == id) {
						boxCharts[i].showWaitingForExpiry();
					}
				}
			}
		} else {
			if (hasFlash) {
				document.getElementById(id + "_chart_holder").innerHTML = document.getElementById("template_long_term").innerHTML;
			}
		}
//		showElm(document.getElementById(id + "_return"), ELEMENT_TYPE_CELL);
//		showElm(document.getElementById(id + "_scheduled"), ELEMENT_TYPE_CELL);
		if (!isMobileParm) {
			document.getElementById(id + "_empty").style.display = "none";
		} else {
//			showElm(document.getElementById(id + "_bottom_line"), ELEMENT_TYPE_CELL);
		}
	} else {
		if (null == suspendMessage) {
			suspendMessage = document.getElementById(id + "_" + SUBSCR_FLD_ET_SUSPENDED_MESSAGE).innerHTML;
		}
		var sperator = "<br/>";
		if (isMobileParm) {
			sperator = " ";
		}
		var arr = suspendMessage.split("s");
		var str = suspendMsgUpperLine[new Number(arr[0]) - 1] + sperator + suspendMsgLowerLine[new Number(arr[1]) - 1];
		if (arr.length == 3 && arr[2].length > 0) {
			str += " " + formatDate(new Date(arr[2]), false);
		}
		var template = document.getElementById("template_suspended").innerHTML;
		template = template.replace(/{msg}/g, str);
		document.getElementById(id + "_chart_holder").innerHTML = template;
	}
}

function getScheduled(marketId) {
	for (var i = 0; i < group.length; i++) {
		var arr = group[i].split("_");
		if (arr[2] == marketId) {
			return arr[1];
		}
	}
	return 1;
}

function getMarketId(id) {
	for (var i = 0; i < boxesOpportunities.length; i++) {
		for (var j = 0; j < boxesOpportunities[i].length; j++) {
			if (boxesOpportunities[i][j] == id) {
				return group[i].split("_")[2];
			}
		}
	}
	return 0;
}

/*function getMovie1(movieName) {
	if (navigator.appName.indexOf("Microsoft") != -1) {
		return window[movieName];
	} else {
		return document[movieName];
	}
}*/

function getMovie(movieName) {
  if (window.document[movieName]) {
      return window.document[movieName];
  }
  if (navigator.appName.indexOf("Microsoft Internet") == -1 && document.embeds && document.embeds[movieName]) {
      return document.embeds[movieName];
  } else { // if (navigator.appName.indexOf("Microsoft Internet")!=-1)
    return document.getElementById(movieName);
  }
}

function getMarketName(marketId) {
	for (var i = 0; i < marketsDisplayName.length; i++) {
		if (marketsDisplayName[i][0] == marketId) {
			return marketsDisplayName[i][1];
		}
	}
	return "";
}

function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}

function formatDate(est_date, isTitle) {
	est_date = adjustFromUTCToLocal(est_date);

	var currdate = new Date();
	var min = est_date.getMinutes();
	var ruCharBox = document.getElementById('russionCharBox').innerHTML;
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		if (skinId == 10 && !isTitle) {
			est_date = ruCharBox + est_date.getHours() + ":" + min + ", " + bundle_msg_today;
		} else {
			est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
		}

	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		if (skinId == 10 && !isTitle) {
			est_date = est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2) + ", " + ruCharBox + est_date.getHours() + ":" + min;
		} else {
			est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
		}
	}
	return est_date;
}


function engineStatusChange(chngStatus) {
	log("ENGINE state changed " + chngStatus);
	if (chngStatus != "STREAMING") { // DISCONNECTED
		log("ENGINE DISCONNECTED");
//		for (var i = 1; i <= 4; i++) {
//			var b = getBox(i, 0);
//			clearBoxRows(b);
//			if (getChildOfType(b, "TR", 2).style.display == "none") {
//				var loading = getChildOfType(b, "TR", 1);
//				showElm(loading, ELEMENT_TYPE_ROW);
//			}
//		}
	}
}

function openSlip(id, type) {
	var oldLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
	var oldLvlVal = null;
	var oldLvlCls = null;
	if (null != oldLvl) {
		oldLvlVal = oldLvl.innerHTML;
		if (oldLvl.attributes.getNamedItem("class").value.indexOf("green") > -1) {
			oldLvlCls = "slip_level_green";
		} else {
			oldLvlCls = "slip_level_red";
		}
	}

//	if (isMobileParm) {
//		document.getElementById(id + "_return").style.display = "none";
//	}
	var template = document.getElementById("template_" + type + (portrait ? "_portrait" : "")).innerHTML;
	template = template.replace(/'oppId'/g, id);
	template = template.replace(/{marketName}/g, document.getElementById(id + "_" + SUBSCR_FLD_ET_NAME).innerHTML);
	try {
	template = template.replace(/{expTime}/g, document.getElementById(id + "_" + SUBSCR_FLD_ET_EST_CLOSE).innerHTML);
	} catch (e) {
	}
	var slip = document.getElementById(id + "_slip");
	slip.innerHTML = template;
	if (null != oldLvlVal) {
		var newLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
		newLvl.innerHTML = oldLvlVal;
		newLvl.attributes.getNamedItem("class").value = oldLvlCls;
	}

	document.getElementById(id + "_choice").innerHTML = (type == "call" ? "1" : "2");

	if (!isMobileParm) {
		updateAboveBelow(id);
	}

	if (navigator.userAgent.toLowerCase().indexOf("msie") > -1) {
		setTimeout("document.getElementById('"+ id +"_amount').focus();", 50);
	} else {
		document.getElementById(id + "_amount").focus();
	}

	if (isMobileParm) {
		for (i = 0; i < boxesOpportunities.length; i++) {
			if (boxCharts[i].oppId == id) {
				if (type == "call") {
					boxCharts[i].showCall();
				} else {
					boxCharts[i].showPut();
				}
				boxCharts[i].repaint();
			}
		}
	}
}

function closeSlip(id) {
//	if (isMobileParm) {
//		showElm(document.getElementById(id + "_return"), ELEMENT_TYPE_CELL);
//	}
	changeBoxState(
			id,
			document.getElementById(id + "_" + SUBSCR_FLD_ET_NAME).innerHTML,
			document.getElementById(id + "_" + SUBSCR_FLD_ET_STATE).innerHTML,
			null,
			document.getElementById(id + "_" + SUBSCR_FLD_LAST_INVEST).innerHTML,
			null);
	document.getElementById(id + "_choice").innerHTML = "0";
	try {

		if (isMobileParm) {
			for (i = 0; i < boxesOpportunities.length; i++) {
				if (boxCharts[i].oppId == id) {
					boxCharts[i].hideCall();
					boxCharts[i].hidePut();
					boxCharts[i].repaint();
				}
			}
		} else {
			var chart = getMovie(id + "_chart");
			if (null != chart) {
				chart.hidePut();
				chart.hideCall();
			}
		}
	} catch (e) {
		// do nothing
	}
	
	if (isLiveAOPage) {
    	fromGraph = 2;
    } else {
    	fromGraph = 0;
    }
}

function twoDecimals(inputObj) {
	var newValue = decRound(inputObj.value, currencyDecimalPoint);
	inputObj.value = newValue;
}

function decRound(num, places) {
	//Round decimal places
    var x = num * Math.pow(10, places);
    x = x.toFixed(1);
    x = Math.round(x);
    x = x / Math.pow(10, places);
	var a = x.toString();
    if (places > 0) {
		//Pad with zero's
		var b = (a.indexOf(".") == -1) ? 0 : (a.length - a.indexOf(".") - 1);
		a += ((b == 0) ? "." : "");
		var c = places - b;
		if (c > 0) {
			for (var i = 0; i < c; i++) {
				a += "0";
			}
		}
	}
    return a;
}

function checkChars(obj) {
	var validchars = "0123456789";
	var sc = ".";
	var scused = false;
	var newval = "";
	var currval;
	var allowed = 0;

	currval = obj.value;

	for (var x = 0; x < currval.length; x++) {
        var currchar = currval.charAt(x);
        for (var y = 0; y < validchars.length; y++) {
            var c = validchars.charAt(y);
            if (currchar == c) {
                if (scused) {
                    allowed++;
                    if (allowed > currencyDecimalPoint) {
                        break;
                    } else {
                        newval += currchar;
                        break;
                    }
                } else {
                    newval += currchar;
                    break;
                }
            } else if (!scused && currencyDecimalPoint > 0) {
                if (currchar == sc) {
                    scused = true;
                    newval += currchar;
                }
            }
        }
    }
    obj.value = newval;
}

function updateWinLose(amount, event, id) {
	if (event.keyCode == 13) {
		submitSlip(id);
		return;
	}

	if (((event.keyCode != 9) && (event.keyCode != 16) &&
			(event.keyCode != 37) && (event.keyCode != 38) &&
			(event.keyCode != 39) && (event.keyCode != 40)) || isMobileParm) { //if not arrows/shift/tab
		checkChars(amount);
	}
	updateWinLoseInt(amount, id);
}

function updateWinLoseInt(amount, id) {
	var amt = new Number(amount.value);

	if (!isMobileParm) {
		var winOdds = document.getElementById(id + "_" + "returnS").innerHTML;
		var loseOdds = document.getElementById(id +"_" + "refundS").innerHTML;
		log("winOdds: " + winOdds + " loseOdds: " + loseOdds + " amount: " + amount.value);
	}else{
		var winOdds = new Number(document.getElementById(id + "_" + SUBSCR_FLD_ET_ODDS_WIN).innerHTML);
		var loseOdds = new Number(document.getElementById(id + "_" + SUBSCR_FLD_ET_ODDS_LOSE).innerHTML);
		log("winOdds: " + winOdds + " loseOdds: " + loseOdds + " amount: " + amount.value);
	}

	if (skinId == 4 || !isLeftCurrencySymbol) {
		document.getElementById(id + "_win").innerHTML = decRound(amt * winOdds, currencyDecimalPoint) + currencySymbol;
		document.getElementById(id + "_lose").innerHTML = decRound(amt * loseOdds, currencyDecimalPoint) + currencySymbol;
	} else {
		document.getElementById(id + "_win").innerHTML = currencySymbol + decRound(amt * winOdds, currencyDecimalPoint);
		document.getElementById(id + "_lose").innerHTML = currencySymbol + decRound(amt * loseOdds, currencyDecimalPoint);
	}
}

var boxCleanTimeout = null;
function scheduleStartBoxes() {
//	if (null != boxCleanTimeout) {
//		clearTimeout(boxCleanTimeout);
//		boxCleanTimeout = null;
//	}
	if (null == boxCleanTimeout) {
		boxCleanTimeout = setTimeout('cleanBoxes();', 10000 + Math.floor(Math.random() * 30000));
	}
}

function cleanBoxes() {
	boxCleanTimeout = null;
	log("Cleaning boxes");
	if (null != markets && markets.length > 0) {
		try {
			var haveCleaned = false;
			var j = 0;
			for (var i = 0; i < group.length; i++) {
				var reallyFixed = boxesFixed[i + 1] && (isMarketOpened(boxesFixedMarkets[i + 1]) || isOnHomepage(boxesFixedMarkets[i + 1]));
				if (!reallyFixed) {
					// find market that's not among the fixed
					while (j < markets.length && isFixedMarket(markets[j][0])) {
						j++;
					}
				}
				log("box " + i + " full: " + boxesFull[i + 1] + " fixed: " + boxesFixed[i + 1] + " market: " + boxesMarkets[i + 1] + " reallyFixed: " + reallyFixed + " j: " + j + " markets: " + markets[j][0]);
				if ((boxesFull[i + 1] && !reallyFixed && boxesMarkets[i + 1] != markets[j][0]) ||
					(boxesFull[i + 1] && reallyFixed && boxesMarkets[i + 1] != boxesFixedMarkets[i + 1])) {
					removeBoxTable(i);
					haveCleaned = true;
				}
				if (!boxesFull[i + 1]) {
					haveCleaned = true;
				}
				if (!reallyFixed) {
					j++;
				}
			}
			if (haveCleaned) {
				lsPage.removeTable("aoTable");
				startBoxes();
			}
		} catch (err) {
			log("Problem in cleanBoxes: " + err);
		}
	}
}

function removeBoxTable(box) {
	log("Removing box " + box);
	boxesFull[box + 1] = false;
	document.getElementById("box" + box).innerHTML = document.getElementById("template_loading").innerHTML;
	boxesOpportunities[box] = new Array(0);
}

function startBoxes() {
	log("startBoxes");
	var j = 0;
	for (var i = 0; i < group.length; i++) {
		var reallyFixed = boxesFixed[i + 1] && (isMarketOpened(boxesFixedMarkets[i + 1]) || isOnHomepage(boxesFixedMarkets[i + 1]));
		if (!reallyFixed) {
			// find market that's not among the fixed
			while (j < markets.length && isFixedMarket(markets[j][0])) {
				j++;
			}
		}
		if (!boxesFull[i + 1]) {
			if (!reallyFixed) {
				log("starting box: " + i + " market: " + markets[j][0] + " schedule: " + markets[j][2]);
				group[i] = "aotps_" + markets[j][2] + "_" + markets[j][0];
				boxesMarkets[i + 1] = markets[j][0];
				boxesFull[i + 1] = true;
			} else {
				log("starting fixed box: " + i + " market: " + boxesFixedMarkets[i + 1] + " schedule: " + boxesFixedScheduled[i + 1]);
				group[i] = "aotps_" + boxesFixedScheduled[i + 1] + "_" + boxesFixedMarkets[i + 1];
				boxesMarkets[i + 1] = boxesFixedMarkets[i + 1];
				boxesFull[i + 1] = true;
			}
		}
		if (!reallyFixed) {
			j++;
		}
	}
	aoTable = new NonVisualTable(group, schema, "COMMAND");
	aoTable.setDataAdapter("JMS_ADAPTER");
	aoTable.setSnapshotRequired(true);
	aoTable.setRequestedMaxFrequency(1.0);
	aoTable.onItemUpdate = updateItemAO;
	lsPage.addTable(aoTable, "aoTable");
}

function isMarketOpened(marketId) {
	for (var i = 0; i < marketStates.length; i++) {
		if (marketStates[i][0] == marketId) {
			return marketStates[i][1] > 0;
		}
	}
	return false;
}

function isOnHomepage(marketId) {
	for (var i = 0; i < markets.length; i++) {
		if (markets[i][0] == marketId) {
			return true;
		}
	}
	return false;
}

function isFixedMarket(marketId) {
	for (var i = 1; i <= 4; i++) {
		if (boxesFixedMarkets[i] == marketId) {
			return true;
		}
	}
	return false;
}

var canSubmit = true;
function submitSlip(id) {
	if (canSubmit) {
		canSubmit = false;

		// mark as submitting. no more change to slip level
		document.getElementById(id + "_submit").innerHTML = 1;

		try {
			//freez the chart
			var chart = getMovie(id + "_chart");
			if (null != chart) {
				chart.freezeChart();
			}
		} catch (e) {
			// do nothing
		}

		var oppLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL).innerHTML;
		if (!isMobileParm) {
			var oppOddsWin = document.getElementById(id + "_returnS").innerHTML;
			var oppOddsLose = document.getElementById(id +"_refundS").innerHTML;
		}else{
			var oppOddsWin = document.getElementById(id + "_" + SUBSCR_FLD_ET_ODDS_WIN).innerHTML;
			var oppOddsLose = document.getElementById(id + "_" + SUBSCR_FLD_ET_ODDS_LOSE).innerHTML;
		}
		var choice = document.getElementById(id + "_choice").innerHTML;
		var amount = document.getElementById(id + "_amount").value;

		// clear errors
		if (!isMobileParm) {
			document.getElementById(id + "_error").innerHTML = "";
			document.getElementById(id + "_errorMsgDiv").style.display = "none";
		}

		document.getElementById(id + "_img").src = submitBtnSubmittingImg;
		document.getElementById(id + "_x_img").style.display = "none";
		submitSlipToServer(id, oppLvl, oppOddsWin, oppOddsLose, choice, amount, fromGraph);
	}
}

function submitSlipToServer(id, lvl, oddWin, oddLose, choice, amount, fromGraph) {
	var subSTS = new SubSTS(id, lvl, oddWin, oddLose, choice, amount, fromGraph);
	function SubSTS(id, lvl, oddWin, oddLose, choice, amount, fromGraph) {
		this.id = id;
		this.lvl = lvl;
		this.oddWin = oddWin;
		this.oddLose = oddLose;
		this.choice = choice;
		this.amount = amount;
		this.xmlHttp = null;
		this.fromGraph = fromGraph;

		this.ticker = function() {
			subSTS.xmlHttp = getXMLHttp();
			if (null == subSTS.xmlHttp) {
				return false;
			}
			subSTS.xmlHttp.onreadystatechange = function() {
				if (subSTS.xmlHttp.readyState == 4) {
					canSubmit = true;
					if (subSTS.xmlHttp.responseText == "redirectToTerms") {
						window.location = context_path + "/jsp/terms.jsf?from=tradePage";
					} else {
						if (subSTS.xmlHttp.responseText.length > 2) {

							if (subSTS.xmlHttp.responseText.substring(0, 2) == "OK") {
								submitSlipUnfreeze(subSTS.id);
								var smsEnable = 0;
								var investmentId = subSTS.xmlHttp.responseText.substring(2);
								if (investmentId.indexOf("SMS=") > -1) {
									investmentId = investmentId.substring(0, investmentId.length - 5);
									// check sms field
									if (subSTS.xmlHttp.responseText.substring(subSTS.xmlHttp.responseText.length-1) == "1") {
										smsEnable = 1;
									}
								}
								submitSlipSuccess(subSTS.id, subSTS.choice, investmentId, smsEnable);
							} else if (subSTS.xmlHttp.responseText.substring(0, 1) == "1") {
								submitSlipErrorNotLogin(subSTS.id);
								//submitSlipError(subSTS.id, subSTS.xmlHttp.responseText.substring(1));
								submitSlipUnfreeze(subSTS.id);
							} else {
								submitSlipError(subSTS.id, subSTS.xmlHttp.responseText);
								submitSlipUnfreeze(subSTS.id);
							}
						} else {
							submitSlipError(subSTS.id, subSTS.xmlHttp.responseText);
							submitSlipUnfreeze(subSTS.id);
						}
					}
				}
			}
			subSTS.xmlHttp.open("POST", "ajax.jsf", true);
			subSTS.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			subSTS.xmlHttp.send(
					"command=submit" +
					"&slipEntryId=" + subSTS.id +
					"&slipEntryLevel=" + subSTS.lvl +
					"&slipEntryOddWin=" + subSTS.oddWin +
					"&slipEntryOddLose=" + subSTS.oddLose +
					"&choice=" + subSTS.choice +
					"&all=1" +
					"&amount=" + subSTS.amount +
					"&utcOffset=" + new Date().getTimezoneOffset() +
					"&fromGraph=" + subSTS.fromGraph);
		}
	}
	subSTS.ticker();
}

function submitSlipSuccess(id, choice, invId, smsEnable) {
	log("successSlip - id: " + id + " choice: " + choice + " invId: " + invId + " smsEnable:" + smsEnable);
	var oldLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
	var oldLvlVal = null;
	var oldLvlCls = null;
	if (null != oldLvl) {
		oldLvlVal = oldLvl.innerHTML;
		if (oldLvl.attributes.getNamedItem("class").value.indexOf("green") > -1) {
			oldLvlCls = "slip_level_green";
		} else {
			oldLvlCls = "slip_level_red";
		}
	}

	var profitLineStr = "";
	try {
		var groupClose = new Number(document.getElementById(id + "_" + SUBSCR_FLD_ET_GROUP_CLOSE).innerHTML);
		var aoFlags = new Number(document.getElementById(id + "_" + SUBSCR_FLD_AO_FLAGS).innerHTML);
		if ((groupClose & 1) != 0 && (aoFlags & 3) != 0) {
			profitLineStr = document.getElementById("template_success_profitline").innerHTML;
		}
	} catch (e) {
		log("Error handle Profit Line button: " + e);
	}

	var template = document.getElementById("template_" + (choice == 1 ? "call" : "put") + "_success" + (portrait ? "_portrait" : "")).innerHTML;
	template = template.replace(/{profitLine}/g, profitLineStr);
	template = template.replace(/'oppId'/g, id);
	template = template.replace(/'marketId'/g, getMarketId(id));
	template = template.replace(/{marketName}/g, document.getElementById(id + "_" + SUBSCR_FLD_ET_NAME).innerHTML);
	template = template.replace(/{level_clr}/g, oldLvl.attributes.getNamedItem("class").value);
	template = template.replace(/{level}/g, oldLvl.innerHTML);
	template = template.replace(/{expTime}/g, document.getElementById(id + "_" + SUBSCR_FLD_ET_EST_CLOSE).innerHTML);
	template = template.replace(/{amount}/g, document.getElementById(id + "_amount").value);
	template = template.replace(/{lose}/g, document.getElementById(id + "_lose").innerHTML);
	template = template.replace(/{win}/g, document.getElementById(id + "_win").innerHTML);
	template = template.replace(/{confirm}/g, invId);

	var slip = document.getElementById(id + "_slip");
	slip.innerHTML = template;

	if (null != oldLvlVal) {
		var newLvl = document.getElementById(id + "_" + SUBSCR_FLD_AO_LEVEL);
		newLvl.innerHTML = oldLvlVal;
		newLvl.attributes.getNamedItem("class").value = oldLvlCls;
	}

	//reset and show the sms check box
	if (smsEnable) {
		showElm(document.getElementById(id + "_sms"), ELEMENT_TYPE_ROW);
	}

	var profitIframe = document.getElementById('right_side_investments');
	if (null != profitIframe) {
		profitIframe.src = profitIframe.contentWindow.location.href;
	}

	// refresh balance
	var headerIfram = document.getElementById('header');
	if (null != headerIfram) {
		headerIfram.contentWindow.location.reload();
	}
	// refresh banners page
	//document.getElementById('banners_page').contentWindow.location.reload();

	// refresh mobile balance
	var balanceIfram = document.getElementById("balanceIframe");
	if (null != balanceIfram) {
		balanceIfram.contentWindow.location.reload()
	}
}

function submitSlipError(id, errMsg) {
	var leftPos = 118;
	if (skinId == 4) {
		leftPos = 138;
	}
	showErrorDiv(20, leftPos, id, errMsg);
}

function submitSlipErrorNotLogin(id) {
	if (!isMobileParm) {
		var leftPos = 120;
		if (skinId == 4) {
			leftPos = 138;
		}
	}
	showErrorDiv(37, leftPos, id, null);
}

function submitSlipUnfreeze(id) {
	document.getElementById(id + "_submit").innerHTML = 0;
	document.getElementById(id + "_img").src = submitBtnNormalImg;
	document.getElementById(id + "_x_img").style.display = "";
	try {
		var chart = getMovie(id + "_chart");
		if (null != chart) {
			chart.unfreezeChart();
		}
	} catch (e) {
		// do nothing
	}
}

function showElm(elm, elmType) {
	if (isMobileParm) {
		elm.style.display = 'block';
	} else if (elmType == ELEMENT_TYPE_TABLE) {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
	} else if (elmType == ELEMENT_TYPE_ROW) {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	} else {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-cell';
	}
}

function showErrorDiv(top, left, id, errMsg) {
	if (isMobileParm) {
		errorMsg = null;
		if (errMsg != null) {
			errorMsg = errMsg;
		} else {
			isNotLogInError = true; //in use only in mobile
			errorMsg = document.getElementById(id + "_loginError").innerHTML;
		}
		if (isAndroidApp && !isNotLogInError) {
			window.AlertInterface.showAlert(errorMsg);
		} else if (isIPhoneApp && !isNotLogInError && appVer > '1.0') {
			window.location = "alertinterface://" + encodeURI(errorMsg);
		} else {
			customAlert("", errorMsg);
		}
	} else {
		var mTd = document.getElementById(id + "_errorPosTD");
		var conDiv = document.getElementById(id + "_errorMsgDiv");
		var positionLeft = left;
		var positionTop = top;
		var live_login = document.getElementById("live_login");
		var live_logout = document.getElementById("live_logout");
		/*
		if (live_login != null) {
			positionLeft = positionLeft - left - 348;
			positionTop = positionTop - top - 113;
		}
		if (live_logout != null) {
			positionLeft = positionLeft - left - 345;
			positionTop = positionTop - top - 415;
		}
		if (null != mTd && null != conDiv) {
			$(conDiv).css("left", positionLeft); //4
			$(conDiv).css("top", positionTop); //28
		}
		*/
		conDiv.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		if (errMsg != null) {
			g(id + "_error").innerHTML = errMsg;
			g(id+'_loginError').style.display = "none";
		} else {  // login error
			var errTd = g(id + "_error");
			errTd.innerHTML = "";
			errTd.style.display = "none";   // errorMsg div
			g(id+'_loginError').style.display = "block";//show the login error
		}
	}
}

function closeErrorDiv(id) {
	document.getElementById(id + "_errorMsgDiv").style.display = "none";
}

//print slip success
function printSuccess(invId) {
	window.open(context_path + "/jsp/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}

//show chart call or put image
//oppId = oppId u was on with mouse
//flag = true for show, false for hide
//isCall = true for call, false for put
function chartShowCallPut(oppId, flag, isCall) {
	try {
		var chart = getMovie(oppId + "_chart");
		if (chart != null) {
			if (flag) {
				if (isCall) {
					chart.hidePut();
					chart.showCall();
				} else {
					chart.hideCall();
					chart.showPut();
				}
			} else {
				var slipChoice = 0;
				if (document.getElementById(oppId + "_choice").innerHTML != "0") {
					slipChoice = document.getElementById(oppId + "_choice").innerHTML == "2" ? 2 : 1;
					if (isCall) {
						if (slipChoice != 1) {
							chart.hideCall();
						}
					} else if (slipChoice != 2) {
						chart.hidePut();
					}
				}
				if (slipChoice != 0) {
					if (slipChoice == 1) {
						chart.showCall();
					} else {
						chart.showPut();
					}
				} else {
					chart.hidePut();
					chart.hideCall();
				}
			}
		}
	} catch (e) {
		// do nothing
	}
}

function padToTwoDig(val) {
	if (val >= 0 && val < 10) {
		return "0" + val;
	}
	return val;
}

function updateProgress(id, min, sec) {
	document.getElementById(id + "_cd_time").innerHTML = padToTwoDig(min) + ":" + padToTwoDig(sec);
	var width = Math.round((1 - (min * 60 + sec) / 600) * 111);
	if (width < 0) {
		width = 0;
	}
	document.getElementById(id + "_cd_prg").style.width = width + 'px';
}

function startCountDownAO(id, min, sec) {
	var countDown = new CountDown(id, min, sec);
	function CountDown(progressTable, min, sec) {
		this.id = id;
		this.min = min;
		this.sec = sec;

		log("Start count down - id: " + id + " min: " + min + " sec: " + sec);
		this.ticker = function() {
			try {
				if (countDown.sec > 0) {
					countDown.sec = countDown.sec - 1;
				} else {
					if (countDown.min > 0) {
						countDown.min = countDown.min - 1;
						countDown.sec = 59;
					}
				}
				updateProgress(countDown.id, countDown.min, countDown.sec);
				if (countDown.min > 0 || countDown.sec > 0) {
					setTimeout(countDown.ticker, 1000);
				}
			} catch (e) {
				log("CountDown for " + countDown.id + " broke - " + e);
			}
		}
	}
	setTimeout(countDown.ticker, 1000);
}

function isIsraelMarket(marketId) {
	for (i = 0; i < israelMarkets.length; i++) {
		if (israelMarkets[i] == marketId) {
			return true;
		}
	}
	return false;
}

// Update is_accepted_sms to investment for getting expiration message
function updateInvestSms(element, id) {
	var xmlHttp = getXMLHttp();
	if (null == xmlHttp) {
		return false;
	}
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			if (xmlHttp.responseText.length >= 2) {
				if (xmlHttp.responseText.substring(0, 2) == "ok") {
					var smsTxt = document.getElementById(id + "_sms_txt");
					smsTxt.innerHTML = acceptSmsMessage;
					//element.style.display = 'none';
					element.disabled = true;
				} else {
					//do nothing we dont have text and not sure we want :)
				}
			}
		}
	}
	var invId = document.getElementById(id + "_confirm").innerHTML.substring(2);

	xmlHttp.open("POST", "ajax.jsf", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.send("turnSmsOn&invId=" + invId);
}

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

//switch picture
function ChangeTrendPie(elementObj, picPath, toolTipShow) {
	elementObj.src = picPath;
	var tooltip = document.getElementById(elementObj.getAttribute("data-oppid") + '_tradingbx_info_msg_trend');
	
	if (toolTipShow) {
		tooltip.style.display = 'block';
	} else {
		tooltip.style.display = 'none';
	}
}

function openTrendPopup(id) {
	var div = document.getElementById(id);
	div.style.display = "";
	if (document.getElementById('trend_popup_background')) {
		var s = document.getElementById('trend_popup_background');
		s.setAttribute('data-id', id);
		s.style.display = 'block';
		s.onclick=getMousePos;
	} else {
		var s = document.createElement('div');
		s.id = 'trend_popup_background';
		s.className='trend_popup_background';
		s.setAttribute('data-id', id);
		s.onclick=getMousePos;
		document.getElementsByTagName('body')[0].appendChild(s);
	}
}

function closeTrendPop_new(th){
	th.parentNode.style.display = "none";
	l(th)
}
function closeTrendPopup() {
	var h = document.getElementById('trend_popup_background');
	if (h != null) {
		var div = document.getElementById(h.getAttribute('data-id'));
		div.style.display = "none";
		h.style.display = 'none';
	}
}

function getMousePos(e){
	closeTrendPopup();
	if(!e){
		var e = window.event||window.Event;
	}
	mouseX = e.clientX;
	mouseY = e.clientY;
	if(mouseX < 0){mouseX = 0;}
	if(mouseY < 0){mouseY = 0;}
	var el = document.elementFromPoint(mouseX,mouseY);
	el.click();
	// console.log(el);
}

/********************** ticker start **********************/
var blinkTimer = null;
var tmpbox = null;
function tickerClick(marketId, schedule, boxId, isBankOption){
	boxId = boxId - 1; // box starts from 0
  	if (strHref.indexOf(tradePageN) > -1) {

		for (var i=1;i<=4;i++) {
			if ((boxesMarkets[i] == marketId) && (boxId != i - 1)) {
				alert(marketMenuError);
				return;
			}
		}
		removeBoxTable(boxId);
		var schedule=1;
		boxesMarkets[boxId + 1] = marketId;
		boxesFixed[boxId + 1] = true;
		boxesFull[boxId + 1] = true;
		boxesFixedScheduled[boxId + 1] = schedule;
		boxesFixedMarkets[boxId + 1] = marketId;

		group[boxId] = "aotps_1_" + marketId;
		aoTable = new NonVisualTable(group, schema, "COMMAND");
		aoTable.setDataAdapter("JMS_ADAPTER");
		aoTable.setSnapshotRequired(true);
		aoTable.setRequestedMaxFrequency(1.0);
		aoTable.onItemUpdate = updateItemAO;
		lsPage.addTable(aoTable, "aoTable");

		//if its from bank option move up
	    if (isBankOption) {
			scroll(0, 0);
		}

		if (null != blinkTimer){
			clearInterval(blinkTimer);
			blinkCounter=0;
			blinkTimer=null;
		}
		setTimeout("startBlink("+ marketId +")",100);

	} else if (strHref.substring(strHref.length - homePageN.length) == homePageN) {
		window.top.location.href = context_path + tradePageN;
	}
}

function tickerClickLive(marketId, schedule, boxId){
	boxId = boxId - 1; // box starts from 0
	removeBoxTable(boxId);
	var schedule=1;
	boxesMarkets[boxId + 1] = marketId;
	boxesFixed[boxId + 1] = true;
	boxesFull[boxId + 1] = true;
	boxesFixedScheduled[boxId + 1] = schedule;
	boxesFixedMarkets[boxId + 1] = marketId;

	group[boxId] = "aotps_1_" + marketId;
	aoTable = new NonVisualTable(group, schema, "COMMAND");
	aoTable.setDataAdapter("JMS_ADAPTER");
	aoTable.setSnapshotRequired(true);
	aoTable.setRequestedMaxFrequency(1.0);
	aoTable.onItemUpdate = updateItemAO;
	lsPage.addTable(aoTable, "aoTable");

	//move down
	window.scroll(0, 100);


	if (null != blinkTimer){
		clearInterval(blinkTimer);
		blinkCounter=0;
		blinkTimer=null;
	}
	setTimeout("startBlink("+ marketId +")",100);
}

function startBlink(marketId) {
	blinkTimer = setInterval ( "divblink("+ marketId +")", 250 );
}

var blinkCounter = 0;
function divblink(marketId){
	try {
		var TDToBlink = document.getElementById("blinkTD_" + marketId);
		var blinkTB = getChildOfType(TDToBlink, "TABLE", 1);
		blinkTB = getChildOfType(blinkTB, "TBODY", 1);
		blinkTB = getChildOfType(blinkTB, "TR", 1);
		var tmptdToBlinkAsset = getChildOfType(blinkTB, "TD", 2);
		var tmptdToBlinkExp = getChildOfType(blinkTB, "TD", 4);

		if(blinkCounter%2==0){
			if (skinId != 11){
				TDToBlink.setAttribute("background" , "../images/1x27.jpg");
			} else {
				TDToBlink.setAttribute("background" , "../images/iw_11/1x27.jpg");
			}

			tmptdToBlinkExp.style.visibility = "visible";
			tmptdToBlinkAsset.style.visibility = "visible";
		} else {
			TDToBlink.setAttribute("background" , "");
			tmptdToBlinkExp.style.visibility = "hidden";
			tmptdToBlinkAsset.style.visibility = "hidden";
		}
		blinkCounter++;
		if (blinkCounter==11){
			clearInterval(blinkTimer);
			blinkCounter=0;
			blinkTimer=null;
		}
	} catch(e) {
		clearInterval(blinkTimer);
		blinkCounter=0;
		blinkTimer=null;
	}
}
/********************** ticker end **********************/
