//	submit form by enter
function submitForm(e) {
	var keynum;
	if(window.event) { // IE
		keynum = e.keyCode;
	}
	else if(e.which) {  // Netscape/Firefox/Opera
		keynum = e.which;
    }
	if (keynum == 13) {  // enter key
		document.login.submit();
  	}
}
//check if str end with suffix
function strEndsWith(str, suffix) {
	return str.match(suffix + "$") == suffix;
}
function changeHeaderWidth(){
	var pageStr =  document.location+'' ;
	if(pageStr.indexOf('investments.jsf') > 0 ||pageStr.indexOf('deposit.jsf') > 0 ||
	   pageStr.indexOf('newCard.jsf') > 0 || pageStr.indexOf('cards.jsf') > 0 ||
	   pageStr.indexOf('wireDeposit.jsf') > 0 || pageStr.indexOf('wire.jsf') > 0 ||
	   pageStr.indexOf('reverseWithdraw.jsf') > 0 || pageStr.indexOf('transactions.jsf') > 0 ||
	   pageStr.indexOf('personal.jsf') > 0 || pageStr.indexOf('changePass.jsf') > 0 || pageStr.indexOf('contactus.jsf') > 0
	   || pageStr.indexOf('cc_delete_approve.jsf') > 0 || pageStr.indexOf('receipt.jsf') > 0
	   || pageStr.indexOf('bankWire_approve.jsf') > 0 || pageStr.indexOf('withdraw_approve.jsf') > 0
	   || pageStr.indexOf('myaccount_empty_withdrawal.jsf') > 0 || pageStr.indexOf('creditCard_withdraw.jsf') > 0
	   || pageStr.indexOf('myaccount_empty_pass.jsf') > 0 || pageStr.indexOf('termsNextInvest.jsf') > 0
	   || pageStr.indexOf('editCard.jsf') > 0 || pageStr.indexOf('personal.jsf') > 0 || pageStr.indexOf('firstNewCard.jsf') > 0
	   || pageStr.indexOf('envoyDeposit.jsf') > 0 ){
				document.getElementById('secure_table').style.display="inline";
			}
}

function SetEnd(TB) {
	if (TB.createTextRange) {
		var FieldRange = TB.createTextRange();
		FieldRange.moveStart('character', TB.value.length);
		FieldRange.collapse();
		FieldRange.select();
	}
}
function changePhoneCodeByCountry() {
    var countryId = document.getElementById("contactUsForm:countriesList").value;
    var url = "ajax.jsf?countryId=" + countryId;
    var ajax = new AJAXInteraction(url, changePhoneCodeByCountryCallback);
    ajax.doGet();
}
function changePhoneCodeByCountryCallback(responseXML) {
   var codeValue = responseXML.getElementsByTagName("phoneCode")[0].firstChild.nodeValue;
   var phoneCodeElm = document.getElementById("contactUsForm:mobilePhoneCode");
   phoneCodeElm.disabled = false;
   phoneCodeElm.value = codeValue;
   phoneCodeElm.disabled = true;
}
function gowait(elementId,className) {
   	g(elementId).className += " "+className;

	window.setTimeout( function() {
		var wg = g(elementId);
		wg.src = wg.src; }, 50);

	g(elementId).disabled = true; // disable click on commandButton
}
function validateEmail() {
    var target = document.getElementById("contactUsForm:email");
	if (target.value.length > 0){
	    var url = "ajax.jsf?emailValidation=" + encodeURIComponent(target.value);
	    var ajax = new AJAXInteraction(url, validateEmailCallback);
	    ajax.doGet();
    }
}
function validateEmailCallback(responseXML) {
   var msg = responseXML.getElementsByTagName("emailErrormsg")[0].firstChild.nodeValue;
   var check = document.getElementById("contactUsForm:emailError");
   if (check != null) {
      check.innerHTML = '';
   }
   var mdiv = document.getElementById("emailMessage");
   // set the style on the div to invalid
   mdiv.className = "error_messagess";
   mdiv.innerHTML = msg;
}
/*
	insert into register attempts AJAX call
*/
function insertUpdateRegisterAttempts() {
	var email = document.getElementById("contactUsForm:email");
	var firstName = document.getElementById("contactUsForm:firstName");
	var lastName = document.getElementById("contactUsForm:lastName");
	var countryId = document.getElementById("contactUsForm:countriesList");
	var landMobilePhoneCode = document.getElementById("contactUsForm:landMobilePhoneCode");
	var landMobilePhone = document.getElementById("contactUsForm:landMobilePhone");

	if (email.value.length > 0 ){
		var dataa = "quickStartEmailRegisterAttempts=" + encodeURIComponent(email.value)
					+ "&firstNameRegisterAttempts="	+ encodeURIComponent(firstName.value)
					+ "&lastNameRegisterAttempts=" + encodeURIComponent(lastName.value)
					+ "&countryRegisterAttempts=" + encodeURIComponent(countryId.value);
					if(landMobilePhoneCode != null){
						dataa += "&mobilePhoneRegisterAttempts=" + encodeURIComponent(landMobilePhoneCode.value + landMobilePhone.value) ;
					}
		$.ajax({
			type: "POST",
			url: "ajax.jsf",
			data: dataa
		});
    }
}