/*********** logging.js ************/

var logging = false;
function log(msg) {
	if (!logging) {
		return;
	}
	try {
//		var log = document.getElementById("log");
//		log.innerHTML = log.innerHTML + getCurrentTimeFormatted() + " " + msg + "<br />";
		console.log(getCurrentTimeFormatted() + " " + msg);
	} catch (e) {
		// do nothing
	}
}

function getCurrentTimeFormatted() {
	var tf = "";
	try {
		var d = new Date();
		tf = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "." + d.getMilliseconds();
	} catch (e) {
		// do nothing
	}
	return tf;
}

function clearLog() {
	if (!logging) {
		return;
	}
	try {
		var log = document.getElementById("log");
		log.innerHTML = "";
	} catch (e) {
		// do nothing
	}
}