var slideTime = 300;
var closedHeight = 36;
// 2D array of open div height for each feature. [skinId][divNum]
var maxHeight = new Array(12);
//				[p.h., live, choose profit, trend, takeProfit, rollForward, sms, profitLine, showOff, option+]
maxHeight[0] = []; // place holder
maxHeight[1] = []; // place holder
maxHeight[2] = [0, 400, 400, 420, 400, 400, 130, 840, 110, 400]; // EN skin
maxHeight[3] = [0, 400, 400, 420, 400, 400, 130, 820, 110, 400]; // TR skin
maxHeight[4] = [0, 0, 390, 420, 400, 400, 130, 820, 110]; // AR skin
maxHeight[5] = [0, 400, 400, 420, 400, 400, 130, 900, 110, 400]; // ES skin
maxHeight[6] = []; // place holder
maxHeight[7] = []; // place holder
maxHeight[8] = [0, 400, 450, 420, 400, 400, 130, 970, 110, 400]; // DE skin
maxHeight[9] = [0, 400, 420, 420, 400, 400, 130, 880, 110, 400]; // IT skin
maxHeight[10] = [0, 410, 420, 420, 400, 400, 130, 900, 110, 400]; // RU skin
maxHeight[11] = []; // place holder
maxHeight[12] = [0, 400, 400, 420, 400, 400, 130, 870, 110]; // FR skin
maxHeight[13] = [0, 0, 400, 420, 400, 400, 130, 820, 110, 400]; // EN_US skin
maxHeight[14] = [0, 0, 400, 420, 400, 400, 130, 820, 110, 400]; // ES_US skin
maxHeight[15] = [0, 340, 380, 420, 400, 400, 130, 520, 110, 400]; // ZH skin
maxHeight[16] = [0, 400, 400, 420, 400, 400, 130, 840, 110, 400]; // EN reg skin
maxHeight[17] = [0, 400, 400, 420, 400, 400, 130, 840, 110, 400]; // KR skin
maxHeight[18] = [0, 400, 400, 420, 400, 400, 130, 900, 110, 400]; // ES REG skin
maxHeight[19] = [0, 400, 450, 420, 400, 400, 130, 970, 110, 400]; // DE REG skin
maxHeight[20] = [0, 400, 420, 420, 400, 400, 130, 880, 110, 400]; // IT REG skin
maxHeight[21] = [0, 400, 400, 420, 400, 400, 130, 870, 110, 400]; // FR REG skin
maxHeight[22] = [0, 340, 380, 420, 400, 400, 130, 520, 110, 400]; // VIP168 skin

// when click "open", close all other divs and open the selected div
function openDiv(divNum) {
	var startTime = (new Date()).getTime();

	// hide 'open' link and show 'close' link
	document.getElementById('div' + divNum + 'open').style.display = 'none';
	document.getElementById('div' + divNum + 'close').style.display = 'inline';

	//close all other divs
	var i=0;
	for (i=1;i<=9;i++) {
		if (i != divNum) {
			closeDiv(i);
		}
	}
	// switch between snippet text and full text
	document.getElementById('text' + divNum).innerHTML = document.getElementById('divText' + divNum).innerHTML;
	// starting the slide
	openDivRec(divNum, startTime);
}

// slide to open specific div
function openDivRec(divNum, startTime) {
	var div1 = document.getElementById('div' + divNum);
	var elapsed = (new Date()).getTime() - startTime;

	// if the slide time didn't finish, calculate the current height
	if (elapsed < slideTime) {
		var curHeight = closedHeight + Math.round((elapsed / slideTime) * (maxHeight[skinId][divNum] - closedHeight));
		div1.style.height = curHeight + "px";
		// call this function again after 5 ms.
		var nextFcn = "openDivRec('" + divNum + "', " + startTime + ")";
		setTimeout(nextFcn, 5);
	} else {
		// when slide time over, set the div to its open mode height
		div1.style.height = maxHeight[skinId][divNum] + 'px';
	}
}

// when click "close", close the selected div
function closeDiv(divNum) {
	var startTime = (new Date()).getTime();
	var div1 = document.getElementById('div' + divNum);
	if (div1 != null){
		var currHeight = div1.style.height;
		var regexp = /(\d*)\D*/;
		var match = regexp.exec(currHeight);
		// if the current height > the height of close mode, the current div is open, and can be closed
		if (match[1] > closedHeight) {
			// hide 'close' link and show 'open' link
			document.getElementById('div' + divNum + 'close').style.display = 'none';
			document.getElementById('div' + divNum + 'open').style.display = 'inline';
			// starting the slide
			closeDivRec(divNum, startTime);
		}
	}
}

// slide to close specific div
function closeDivRec(divNum, startTime) {
	var div1 = document.getElementById('div' + divNum);
	var elapsed = (new Date()).getTime() - startTime;
	// if the slide time didn't finish, calculate the current height
	if (elapsed < slideTime) {
		var curHeight = maxHeight[skinId][divNum] - Math.round((elapsed / slideTime) * (maxHeight[skinId][divNum] - closedHeight));
		div1.style.height = curHeight + "px";
		// call this function again after 5 ms.
		var nextFcn = "closeDivRec('" + divNum + "', " + startTime + ")";
		setTimeout(nextFcn, 5);
	} else {
		// when slide time over, set the div to its close mode height
		div1.style.height = closedHeight + "px";
		// switch between full text and snippet text
		document.getElementById('text' + divNum).innerHTML = document.getElementById('divSnippet' + divNum).innerHTML;
	}
}

if (window.location.hash == newFeaturesOpenLive) {
	openDiv(1);
} else if (window.location.hash == newFeaturesOpenChooseProfit) {
	openDiv(2);
} else if (window.location.hash == newFeaturesOpenTrendProfit) {
	openDiv(3);	
} else if (window.location.hash == newFeaturesOpenReutersExpiry) {
	openDiv(9);
} else if (window.location.hash == newFeaturesOpenTakeProfit) {
	openDiv(4);
} else if (window.location.hash == newFeaturesOpenProfitLine) {
	openDiv(7);
} else if (window.location.hash == newFeaturesOpenRollForward) {
	openDiv(5);
} else if (window.location.hash == newFeaturesOpenSMS) {
	openDiv(6);
} else if (window.location.hash == newFeaturesOpenShowOff) {
	openDiv(8);
}