////////////////////////////////////////////////////////////////////
// Stuff common with eTrader trade_page.js - start
////////////////////////////////////////////////////////////////////
var OPPORTUNITY_STATE_CREATED			= 1;
var OPPORTUNITY_STATE_OPENED			= 2;
var OPPORTUNITY_STATE_LAST_10_MIN		= 3;
var OPPORTUNITY_STATE_CLOSING_1_MIN		= 4;
var OPPORTUNITY_STATE_CLOSING			= 5;
var OPPORTUNITY_STATE_CLOSED			= 6;
var OPPORTUNITY_STATE_DONE				= 7;
var OPPORTUNITY_STATE_PAUSED			= 8;
var OPPORTUNITY_STATE_SUSPENDED			= 9;
var OPPORTUNITY_STATE_WAITING_TO_PAUSE  = 10;

var SUBSCR_FLD_KEY						= 1;
var SUBSCR_FLD_COMMAND					= 2;
var SUBSCR_FLD_ET_NAME					= 3;
var SUBSCR_FLD_ET_LEVEL					= 4;
var SUBSCR_FLD_ET_ODDS					= 5;
var SUBSCR_FLD_ET_EST_CLOSE				= 6;
var SUBSCR_FLD_ET_OPP_ID				= 7;
var SUBSCR_FLD_ET_ODDS_WIN				= 8;
var SUBSCR_FLD_ET_ODDS_LOSE				= 9;
var SUBSCR_FLD_ET_STATE					= 10;
var SUBSCR_FLD_ET_CLR					= 11;
var SUBSCR_FLD_PRIORITY					= 12;
var SUBSCR_FLD_ET_GROUP_CLOSE			= 13;
var SUBSCR_FLD_ET_SCHEDULED				= 14;
var SUBSCR_FLD_ET_RND_FLOOR				= 15;
var SUBSCR_FLD_ET_RND_CEILING			= 16;
var SUBSCR_FLD_ET_GROUP					= 17;
var SUBSCR_FLD_ET_SUSPENDED_MESSAGE		= 18;
var SUBSCR_FLD_ET_EST_CLOSE_COPY		= 19;
var SUBSCR_FLD_OLD_STATE				= 20;
var SUBSCR_FLD_GROUP_CLOSE_CHANGED		= 21;
var SUBSCR_FLD_STATE_CHANGED			= 22;
var SUBSCR_FLD_COLOR_CHANGED			= 23;
var SUBSCR_FLD_CURRENT_LEVEL			= 24;
var SUBSCR_FLD_ET_DISPLAY_NAME			= 25; //Disaply name to show in the table;
var SUBSCR_FLD_ET_DISPLAY_NAME1			= 26;
var SUBSCR_FLD_ET_EST_CLOSE_COPY1		= 27;
var SUBSCR_FLD_CLOSE_TIME				= 28;

var ELEMENT_TYPE_TABLE					= 0;
var ELEMENT_TYPE_ROW					= 1;
var ELEMENT_TYPE_CELL					= 2;

//var noLongTermMarkets 					= ['137','138','21','20','136','26','17','18','294','435','437','477','538'];

var errLogInDisplayed;

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

function setElmClass(elm, newClass) {
	var atts = elm.attributes;
	var cls = atts.getNamedItem("class");
	cls.value = newClass;
}

function padToTwoDig(val) {
	if (val < 10) {
		return "0" + val;
	}
	return val;
}

function checkChars(obj) {
	var validchars = "0123456789";
	var sc = ".";
	var scused = false;
	var newval = "";
	var currval;
	var allowed = 0;

	currval = obj.value;

	for (var x = 0; x < currval.length; x++) {
        var currchar = currval.charAt(x);
        for (var y = 0; y < validchars.length; y++) {
            var c = validchars.charAt(y);
            if (currchar == c) {
                if (scused) {
                    allowed++;
                    if (allowed > 2) {
                        break;
                    } else {
                        newval += currchar;
                        break;
                    }
                } else {
                    newval += currchar;
                    break;
                }
            } else if (!scused) {
                if (currchar == sc) {
                    scused = true;
                    newval += currchar;
                }
            }
        }
    }
    obj.value = newval;
}

function twoDecimals(inputObj) {
	var newValue = decRound(inputObj.value, 2);
	inputObj.value = newValue;
}

function decRound(num, places) {
	//Round decimal places
    var x = num * Math.pow(10, places);
    x = x.toFixed(1);
    x = Math.round(x);
    x = x / Math.pow(10, places);
	//Pad with zero's
	var a = x.toString();
	var b = (a.indexOf(".") == -1) ? 0 : (a.length - a.indexOf(".") - 1);
	a += ((b == 0) ? "." : "");
	var c = places - b;
	if (c > 0) {
		for (var i = 0; i < c; i++) {
			a += "0";
		}
	}
    return a;
}

////////////////////////////////////////////////////////////////////
// Stuff common with eTrader trade_page.js - end
////////////////////////////////////////////////////////////////////

function updateProgress(progressTable, min, sec) {
	var tmp = getChildOfType(progressTable, "TBODY", 1);
	tmp = getChildOfType(tmp, "TR", 1);
	var td1 = getChildOfType(tmp, "TD", 1);
	td1.innerHTML = min + ":" + padToTwoDig(sec);
	var td2 = getChildOfType(tmp, "TD", 2);
	var div = getChildOfType(td2, "DIV", 1);
	var div2 = getChildOfType(div, "DIV", 1);
	div2.style.width = Math.round((1 - (min * 60 + sec) / 600) * 292) + 'px';
	//chart box count down
	var item = progressTable.parentNode;
	for (i = 0; i < 9; i++) {
		item = item.parentNode;
	}
	var countTd = getOppTRSF(item, 4, 4, null, 2);
	var spanTime = getChildOfType(countTd, "DIV", 2);
	spanTime.innerHTML = td1.innerHTML;
}

function startCountDownAO(progressTable, min, sec) {
	var countDown = new CountDown(progressTable, min, sec);
	function CountDown(progressTable, min, sec) {
		this.progressTable = progressTable;
		this.min = min;
		this.sec = sec;

		log("Start count down - progressTable: " + progressTable + " min: " + min + " sec: " + sec);
		this.ticker = function() {
			if (countDown.sec > 0) {
				countDown.sec = countDown.sec - 1;
			} else {
				if (countDown.min > 0) {
					countDown.min = countDown.min - 1;
					countDown.sec = 59;
				}
			}
			updateProgress(countDown.progressTable, countDown.min, countDown.sec);
			if (countDown.min > 0 || countDown.sec > 0) {
				setTimeout(countDown.ticker, 1000);
//			} else {
//				if (!updatesFrequencyFlag) {
//					try {
//						if (HOME_PAGE) {
//							setOppValue(countDown.display.parentNode.parentNode, 14, OPPORTUNITY_STATE_CLOSING_1_MIN);
//							formatValuesClosing1Min(countDown.display.parentNode.parentNode);
//						} else {
//							setOppValue(countDown.display.parentNode.parentNode, 14, OPPORTUNITY_STATE_CLOSING);
//							formatValuesClosing(countDown.display.parentNode.parentNode);
//						}
//					} catch (e) {
//						// do nothing. most likely the state changed by ls
//					}
//				}
			}
		}
	}
	setTimeout(countDown.ticker, 1000);
}

var timesToGet = 1;
function getTimeLeftAO(oppId, progressTable) {
	var timeGetter = new TimeGetter(oppId, progressTable);
	function TimeGetter(oppId, progressTable) {
		this.oppId = oppId;
		this.progressTable = progressTable;
		this.xmlHttp = null;

		log("Get time left - oppId: " + oppId + " progressTable: " + progressTable);
		this.ticker = function() {
			log("Tick. oppId: " + timeGetter.oppId);
			timeGetter.xmlHttp = getXMLHttp();
			if (null == timeGetter.xmlHttp) {
				return false;
			}
			timeGetter.xmlHttp.onreadystatechange = function() {
				if (timeGetter.xmlHttp.readyState == 4) {
					var rez = timeGetter.xmlHttp.responseText;
					var id = rez.substring(0, rez.indexOf("_"));
					rez = rez.substring(rez.indexOf("_") + 1);
					var min = rez.substring(0, rez.indexOf("_"));
					var sec = rez.substring(rez.indexOf("_") + 1);
					startCountDownAO(timeGetter.progressTable, min, sec);
					timesToGet--;
				}
			}
			log("Request the time for oppId: " + timeGetter.oppId);
			timeGetter.xmlHttp.open("POST", "ajax.jsf", true);
			timeGetter.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			timeGetter.xmlHttp.send("command=time&slipEntryId=" + timeGetter.oppId);
		}
	}
	setTimeout(timeGetter.ticker, 1000 * timesToGet);
	timesToGet++;
}

//function addBoxTable(marketId, scheduled, fixed, box) {
////	boxesFixed[box] = fixed;
//	boxesFull[box] = true;
//	boxesMarkets[box] = marketId;
//	if (fixed) {
//		boxesFixedMarkets[box] = marketId;
//		boxesFixedScheduled[box] = scheduled;
//	}
//
//	var item = getBox(box, 3);
//	item.id = "box" + box + "_" + marketId;
//
//	var group = ["aotps_" + scheduled + "_" + marketId];
//	var schema = ["key", "command", "ET_NAME", "ET_LEVEL", "ET_ODDS", "ET_EST_CLOSE", "ET_OPP_ID", "ET_ODDS_WIN", "ET_ODDS_LOSE", "ET_STATE", "ET_CLR", "ET_PRIORITY", "ET_GROUP_CLOSE", "ET_SCHEDULED", "ET_RND_FLOOR", "ET_RND_CEILING", "ET_GROUP", "ET_SUSPENDED_MESSAGE"];
//	var newTable = new NonVisualTable(group, schema, "COMMAND");
//	newTable.setDataAdapter("JMS_ADAPTER");
//	newTable.setSnapshotRequired(true);
//	if (updatesFrequencyFlag == 1) {
//		newTable.setRequestedMaxFrequency(updatesFrequencyIn);
//	} else {
//		newTable.setRequestedMaxFrequency(updatesFrequencyNotIn);
//	}
//	if (box == 1) {
//		newTable.onItemUpdate = updateItemNonVisual1;
//	} else if (box == 2) {
//		newTable.onItemUpdate = updateItemNonVisual2;
//	} else if (box == 3) {
//		newTable.onItemUpdate = updateItemNonVisual3;
//	} else {
//		newTable.onItemUpdate = updateItemNonVisual4;
//	}
//	lsPage.addTable(newTable, "box" + box + "_" + marketId);
//}

function removeBoxTable(box) {
	log("Removing box " + box);
	boxesFull[box] = false;

	// clean out the box
	var item = getBox(box, 3);
	var tbody = item.parentNode;
	var btr = null;
	do {
		btr = getChildOfType(tbody, "TR", 4);
		if (null != btr) {
			tbody.removeChild(btr);
		}
	} while (null != btr);
	var banner = getChildOfType(tbody, "TR", 2);
	var bannerShowing = banner.style.display != "none";
	if (!bannerShowing) {
		var loading = getChildOfType(tbody, "TR", 1);
		showElm(loading, ELEMENT_TYPE_ROW);
	}
}

function setClassOn(elm) {
	var atts = elm.attributes;
	var cls = atts.getNamedItem("class");
	if (cls.value.length > 3 && cls.value.substring(cls.value.length - 3) == "off") {
		cls.value = cls.value.substring(0, cls.value.length - 4);
	}
}

function setClassOff(elm) {
	var atts = elm.attributes;
	var cls = atts.getNamedItem("class");
	if (cls.value.length > 3 && !(cls.value.substring(cls.value.length - 3) == "off")) {
		cls.value = cls.value + "_off";
	}
}

function showElm(elm, elmType) {
	if (elmType == ELEMENT_TYPE_TABLE) {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
	} else if (elmType == ELEMENT_TYPE_ROW) {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	} else {
		elm.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-cell';
	}
}

function hideElm(elm) {
	elm.style.display = "none";
}

function getOppTRSF(opp, table, row, subRow, field) {
	var tmp = getChildOfType(opp, "TD", 1);
	tmp = getChildOfType(tmp, "TABLE", table);
	if (null == row) {
		return tmp;
	}
	tmp = getChildOfType(tmp, "TBODY", 1);
	tmp = getChildOfType(tmp, "TR", row);
	if (null != subRow) {
		tmp = getChildOfType(tmp, "TD", 1);
		tmp = getChildOfType(tmp, "TABLE", 1);
		tmp = getChildOfType(tmp, "TBODY", 1);
		tmp = getChildOfType(tmp, "TR", subRow);
	}
	if (null == field) {
		return tmp;
	}
	return getChildOfType(tmp, "TD", field);
}

function getLSValue(td) {
	return getChildOfType(td, "DIV", 1).innerHTML;
}

function getOppSlipAmount(item) {
	var td = getOppTRSF(item, 2, 6, null, 2);
	var inp = getChildOfType(td, "INPUT", 1);
	return inp.value;
}

function getOppSlipChartAmount(item) {
	var slipTmp = getChildOfType(item, "TD", 1);
	var slipDiv = getChildOfType(slipTmp, "DIV", 1);
	var inp = slipDiv.getElementsByTagName("INPUT")[0];
	return inp.value;
}

function setOppSlipAmount(item, amount) {
	var td = getOppTRSF(item, 2, 6, null, 2);
	var inp = getChildOfType(td, "INPUT", 1);
	inp.value = amount;
}

function setOppSlipChartAmount(item, amount) {
	var slipTmp = getChildOfType(item, "TD", 1);
	var slipDiv = getChildOfType(slipTmp, "DIV", 1);
	var inp = slipDiv.getElementsByTagName("INPUT")[0];
	inp.value = amount;
}

function getElmClass(elm) {
	var atts = elm.attributes;
	var cls = atts.getNamedItem("class");
	return cls.value;
}

function openSlip(link, type) {
	var item = link;
	for (i = 0; i < 10 ; i++) {
		item = item.parentNode;
	}

	if (type == 1) {
		hideElm(getOppTRSF(item, 2, 4, null, 3));
		showElm(getOppTRSF(item, 2, 4, null, 2), ELEMENT_TYPE_ROW);
		getOppTRSF(item, 2, 9, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 1).innerHTML;
		getOppTRSF(item, 2, 10, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 2).innerHTML;

		//chart trade box
		//call or put
		hideElm(getOppChartDivValue(item, 1, 2, 3, false));
		showElm(getOppChartDivValue(item, 1, 2, 2, false));
		//win
		getOppChartDivValue(item, 1, 6, 1, 1).innerHTML = getOppChartDivValue(item, 1, 9, 1, 1).innerHTML;
		//lose
		getOppChartDivValue(item, 1, 7, 1, 1).innerHTML = getOppChartDivValue(item, 1, 9, 2, 1).innerHTML;


	}

	if (type == 2) {
		hideElm(getOppTRSF(item, 2, 4, null, 2));
		showElm(getOppTRSF(item, 2, 4, null, 3), ELEMENT_TYPE_ROW);
		getOppTRSF(item, 2, 9, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 2).innerHTML;
		getOppTRSF(item, 2, 10, null, 1).innerHTML = getOppTRSF(item, 2, 13, null, 1).innerHTML;
		//chart trade box
		//call or put
		hideElm(getOppChartDivValue(item, 1, 2, 2, false));
		showElm(getOppChartDivValue(item, 1, 2, 3, false));
		//win
		getOppChartDivValue(item, 1, 6, 1, 1).innerHTML = getOppChartDivValue(item, 1, 9, 2, 1).innerHTML;
		//lose
		getOppChartDivValue(item, 1, 7, 1, 1).innerHTML = getOppChartDivValue(item, 1, 9, 1, 1).innerHTML;

	}
	//clear win and lose amount
	getOppTRSF(item, 2, 9, null, 2).innerHTML = "";
	getOppTRSF(item, 2, 10, null, 2).innerHTML = "";
	getOppChartDivValue(item, 1, 6, 1, 2).innerHTML = "";
	getOppChartDivValue(item, 1, 7, 1, 2).innerHTML = "";
	//clear slip amount
	setOppSlipAmount(item, "");
	setOppSlipChartAmount(item, "");
	//clear error msg
	getOppTRSF(item, 2, 11, null, 1).innerHTML = "";

	//hide error msg and success in chart box
	hideChartDiv(item, 2);
	hideChartDiv(item, 3);

	var oppId = getLSValue(getOppTRSF(item, 1, 5, null, 2));
	var chartIframe = document.getElementById("chart" + oppId);
	var inp;
	if (chartIframe != null) {
		//show chart box slip
		var leftPos = 4;
		if (skinId == 4) {
			leftPos = 215;
		}
		showChartDiv(item, 1, getOppTRSF(item, 4, 1, 1), 28, leftPos);

		var spanInput = getOppChartDivValue(item, 1, 4, 1, 2);
		inp = getChildOfType(spanInput, "INPUT", 1);
	} else {
		//show slip
		hideElm(getOppTRSF(item, 1));
		showElm(getOppTRSF(item, 2), ELEMENT_TYPE_TABLE);

		var td = getOppTRSF(item, 2, 6, null, 2);
		inp = getChildOfType(td, "INPUT", 1);
	}
	inp.focus();
}

function updateWinLose(amount, event) {
	if (event.keyCode == 13) {
		submitSlip(amount);
		return;
	}
	if ((event.keyCode != 9) && (event.keyCode != 16) &&
			(event.keyCode != 37) && (event.keyCode != 38) &&
			(event.keyCode != 39) && (event.keyCode != 40)) {
		checkChars(amount);
	}
	var item = amount;
	for (var i = 0; i < 6; i++) {
		item = item.parentNode;
	}
	updateWinLoseInt(item);
}

function updateWinLoseInt(item) {
	var oppId = getLSValue(getOppTRSF(item, 1, 5, null, 2));
	var chartIframe = document.getElementById("chart" + oppId);
	var crr = getOppTRSF(item, 1, 5, null, 6).innerHTML;
	var call = getOppTRSF(item, 2, 4, null, 2).style.display != 'none';
	var winOdds = getLSValue(getOppTRSF(item, 1, 5, null, 3));
	var loseOdds = getLSValue(getOppTRSF(item, 1, 5, null, 4));
	var win = getOppTRSF(item, 2, 9 , null, 2);
	var lose = getOppTRSF(item, 2, 10, null, 2);
	var chartBoxWin = getOppChartDivValue(item, 1, 6, 1, 2);
	var chartBoxlose = getOppChartDivValue(item, 1, 7, 1, 2);
	var amount;
	if (chartIframe) {
		amount = getOppSlipChartAmount(item);
	} else {
		amount = getOppSlipAmount(item);
	}
	win.innerHTML = crr + decRound(new Number(amount) * new Number(winOdds), 2);
	lose.innerHTML = crr + decRound(new Number(amount) * new Number(loseOdds), 2);
	if (skinId == 4) {
		chartBoxWin.innerHTML = decRound(new Number(amount) * new Number(winOdds), 2) + crr;
		chartBoxlose.innerHTML = decRound(new Number(amount) * new Number(loseOdds), 2) + crr;
	} else {
		chartBoxWin.innerHTML = crr + decRound(new Number(amount) * new Number(winOdds), 2);
		chartBoxlose.innerHTML = crr + decRound(new Number(amount) * new Number(loseOdds), 2);
	}


}

var canSubmit = true;
function submitSlip(button) {
	if (canSubmit) {
		canSubmit = false;

		errLogInDisplayed = false;
		var item = button;
		for (var i = 0; i < 6; i++) {
			item = item.parentNode;
		}

		getOppTRSF(item, 1, 5, null, 7).innerHTML = 1; // mark as submitting. no more change to slip level

		var oppId = getLSValue(getOppTRSF(item, 1, 5, null, 2));
		var chartIframe = document.getElementById("chart" + oppId);
		if (chartIframe) {
			chartIframe.contentWindow.freezeChart();
		}
		var oppLvl = getOppTRSF(item, 2, 2, null, 2).innerHTML;
		var oppOddsWin = getLSValue(getOppTRSF(item, 1, 5, null, 3));
		var oppOddsLose = getLSValue(getOppTRSF(item, 1, 5, null, 4));
		var choice = getOppTRSF(item, 2, 4, null, 2).style.display == 'none' ? 2 : 1;
		var amount;
		if (chartIframe) {
			amount = getOppSlipChartAmount(item);
		} else {
			amount = getOppSlipAmount(item);
		}
		// clear error
		getOppTRSF(item, 2, 11, null, 1).innerHTML = "";

		button.src = submitBtnSubmittingImg;

		entSubmitted = 1;
		submitSlipToServer(item, oppId, oppLvl, oppOddsWin, oppOddsLose, choice, 0, amount);
		entSubmitted = 1;
	}
}

var entSubmitted = 1;
function submitSlipToServer(item, id, lvl, oddWin, oddLose, choice, all, amount) {
	var subSTS = new SubSTS(item, id, lvl, oddWin, oddLose, choice, all, amount);
	function SubSTS(item, id, lvl, oddWin, oddLose, choice, all, amount) {
		this.item = item;
		this.id = id;
		this.lvl = lvl;
		this.oddWin = oddWin;
		this.oddLose = oddLose;
		this.choice = choice;
		this.all = all;
		this.amount = amount;
		this.xmlHttp = null;

		this.ticker = function() {
			subSTS.xmlHttp = getXMLHttp();
			if (null == subSTS.xmlHttp) {
				return false;
			}
			subSTS.xmlHttp.onreadystatechange = function() {
				if (subSTS.xmlHttp.readyState == 4) {
					canSubmit = true;
					if (subSTS.xmlHttp.responseText.length > 2) {
						if (subSTS.xmlHttp.responseText.substring(0, 2) == "OK") {
							submitSlipUnfreeze(subSTS.item);
							var smsEnable = 0;
							var investmentId = subSTS.xmlHttp.responseText.substring(2);
							if (investmentId.indexOf("SMS=")> -1) {
								investmentId = investmentId.substring(0,investmentId.length-5);
								// check sms field
								if (subSTS.xmlHttp.responseText.substring(subSTS.xmlHttp.responseText.length-1) == "1" && isSmsAvaliableForUser) {
									smsEnable = 1;
								}
							}
							submitSlipSuccess(subSTS.item, investmentId, smsEnable);
						} else if (subSTS.xmlHttp.responseText.substring(0, 1) == "1") {
							if (!errLogInDisplayed) {
								errLogInDisplayed = true;
								//alert(subSTS.xmlHttp.responseText.substring(1));
								submitSlipErrorNotLogin(subSTS.item);
								submitSlipUnfreeze(subSTS.item);
								// refresh the header to see he is not logged in
//								document.getElementById('header').contentWindow.location.reload();
							}
						} else {
							submitSlipError(subSTS.item, subSTS.xmlHttp.responseText);
							submitSlipUnfreeze(subSTS.item);
						}
					} else {
						submitSlipError(subSTS.item, subSTS.xmlHttp.responseText);
						submitSlipUnfreeze(subSTS.item);
					}
				}
			}
			subSTS.xmlHttp.open("POST", "ajax.jsf", true);
			subSTS.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			subSTS.xmlHttp.send(
					"command=submit" +
					"&slipEntryId=" + subSTS.id +
					"&slipEntryLevel=" + subSTS.lvl +
					"&slipEntryOddWin=" + subSTS.oddWin +
					"&slipEntryOddLose=" + subSTS.oddLose +
					"&choice=" + subSTS.choice +
					"&all=" + subSTS.all +
					"&amount=" + subSTS.amount +
					"&utcOffset=" + new Date().getTimezoneOffset());
		}
	}
	setTimeout(subSTS.ticker, entSubmitted * 1000);
	entSubmitted++;
}

function submitSlipSuccess(item, invId, smsEnable) {
	getOppTRSF(item, 3, 2, null, 1).innerHTML = getLSValue(getOppTRSF(item, 2, 2, null, 1)); //market name
	getOppTRSF(item, 3, 2, null, 2).innerHTML = getOppTRSF(item, 2, 2, null, 2).innerHTML; //slip level
	getOppTRSF(item, 3, 4, null, 2).style.display = getOppTRSF(item, 2, 4, null, 2).style.display; //type
	getOppTRSF(item, 3, 4, null, 3).style.display = getOppTRSF(item, 2, 4, null, 3).style.display; //type
	getOppTRSF(item, 3, 5, null, 2).innerHTML = getLSValue(getOppTRSF(item, 2, 5, null, 2)); //close time
	//put the amount with 2 decimal point
	var samount;
	if (document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)))) {
		samount = getOppSlipChartAmount(item);
	} else {
		samount = getOppSlipAmount(item);
	}
	samount = decRound(samount, 2);
	getOppTRSF(item, 3, 6, null, 2).innerHTML = getOppTRSF(item, 1, 5, null, 6).innerHTML + samount; //sign + amount
	getOppTRSF(item, 3, 9, null, 2).innerHTML = getOppTRSF(item, 2, 9, null, 2).innerHTML; //win
	getOppTRSF(item, 3, 10, null, 2).innerHTML = getOppTRSF(item, 2, 10, null, 2).innerHTML; //lose
	getOppTRSF(item, 3, 11, null, 2).innerHTML = "#" + invId;
	getOppTRSF(item, 3, 9, null, 1).innerHTML = getOppTRSF(item, 2, 9, null, 1).innerHTML; //txt first row label (above or below)
	getOppTRSF(item, 3, 10, null, 1).innerHTML = getOppTRSF(item, 2, 10, null, 1).innerHTML; //txt second row label (above or below)
//fill chart box recipt
	getOppChartDivValue(item, 2, 2, 1, 1).innerHTML = getLSValue(getOppTRSF(item, 2, 2, null, 1));
	getOppChartDivValue(item, 2, 2, 1, 2).innerHTML = getOppTRSF(item, 2, 2, null, 2).innerHTML;
	getOppChartDivValue(item, 2, 3, 1).style.display = getOppChartDivValue(item, 1, 2, 2, false).style.display;
	getOppChartDivValue(item, 2, 3, 2).style.display = getOppChartDivValue(item, 1, 2, 3, false).style.display;
	getOppChartDivValue(item, 2, 4, 1, 2).innerHTML = getLSValue(getOppTRSF(item, 2, 5, null, 2));
	if (skinId == 4) {
		getOppChartDivValue(item, 2, 5, 1, 2).innerHTML = samount + getOppTRSF(item, 1, 5, null, 6).innerHTML;
	} else {
		getOppChartDivValue(item, 2, 5, 1, 2).innerHTML = getOppTRSF(item, 1, 5, null, 6).innerHTML + samount;
	}
	getOppChartDivValue(item, 2, 7, 1, 2).innerHTML = getOppTRSF(item, 2, 9, null, 2).innerHTML;
	getOppChartDivValue(item, 2, 8, 1, 2).innerHTML = getOppTRSF(item, 2, 10, null, 2).innerHTML;
	getOppChartDivValue(item, 2, 7, 1, 1).innerHTML = getOppChartDivValue(item, 1, 6, 1, 1).innerHTML;
	getOppChartDivValue(item, 2, 8, 1, 1).innerHTML = getOppChartDivValue(item, 1, 7, 1, 1).innerHTML;
	if (skinId == 4) {
		getOppChartDivValue(item, 2, 9, 1, 2).innerHTML = invId + "#";
	} else {
		getOppChartDivValue(item, 2, 9, 1, 2).innerHTML = "#" + invId;
	}
	setOppSlipAmount(item, "");
	setOppSlipChartAmount(item, "");

	hideElm(getOppTRSF(item, 1));
	hideElm(getOppTRSF(item, 2));
	hideChartDiv(item, 1);
	hideChartDiv(item, 3);

	//reset and show the sms check box
	if (smsEnable) {
		resetSmsCheckBox(getOppTRSF(item, 3, 13, null, 1), sendSmsMessage);
		showElm(getOppTRSF(item, 3, 13, null, null), ELEMENT_TYPE_ROW);
		resetSmsCheckBox(getOppChartDivValue(item, 2, 11, 1, null), sendSmsMessageShort);
		showElm(getOppChartDivValue(item, 2, 11, null, null), ELEMENT_TYPE_ROW);
	}

	if (document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)))) {
		var leftPos = 4;
		if (skinId == 4) {
			leftPos = 197;
		}
		showChartDiv(item, 2, getOppTRSF(item, 4, 1, 1), 28, leftPos);
	} else {
		showElm(getOppTRSF(item, 3), ELEMENT_TYPE_TABLE);
	}
	// refresh balance
	document.getElementById('header').contentWindow.location.reload();
	// refresh banners page
	document.getElementById('banners_page').contentWindow.location.reload();

}

//enable the check box and replace the success msg with send msg
function resetSmsCheckBox(smsTd, msg) {
	var smsTable = getChildOfType(smsTd, "TABLE", 1);
	var smsTbody = getChildOfType(smsTable, "TBODY", 1);
	var smsTr = getChildOfType(smsTbody, "TR", 1);
	var smsTdd = getChildOfType(smsTr, "TD", 1);
	var smsCheckBox = getChildOfType(smsTdd, "INPUT", 1);
	smsTdd = getChildOfType(smsTr, "TD", 2);
	smsTdd.innerHTML = msg;
	smsCheckBox.checked = false;
	smsCheckBox.disabled = false;
}

function submitSlipError(item, errMsg) {
	getOppTRSF(item, 2, 11, null, 1).innerHTML = errMsg;
	if (document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)))) {
		getOppChartDivValue(item, 3, 1, 1).innerHTML = errMsg;
		var leftPos = 118;
		if (skinId == 4) {
			leftPos = -200;
		}
		showChartDiv(item, 3, getOppChartDivValue(item, 1, 6, 1), 0, leftPos);
	}
}

function submitSlipErrorNotLogin(item) {
	hideElm(getOppTRSF(item, 2, 12, null, 1));
	getOppTRSF(item, 2, 11, null, 1).innerHTML = getOppTRSF(item, 2, 14, null, 1).innerHTML
	if (document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)))) {
		getOppChartDivValue(item, 3, 1, 1).innerHTML = getOppChartDivValue(item, 3, 2, 1).innerHTML;
		var leftPos = 118;
		if (skinId == 4) {
			leftPos = -200;
		}
		showChartDiv(item, 3, getOppChartDivValue(item, 1, 6, 1), 0, leftPos);
	}
}

function submitSlipUnfreeze(item) {
	getOppTRSF(item, 1, 5, null, 7).innerHTML = 0;
	var cls = getElmClass(getOppTRSF(item, 1, 3, 2, 4));
	var cls1 = "slip_head";
	if (cls.indexOf("red") != -1) {
		cls1 = "slip_head_red";
	} else if (cls.indexOf("green") != -1) {
		cls1 = "slip_head_green";
	}
	setElmClass(getOppTRSF(item, 2, 2, null, 2), cls1);
	var img = getOppTRSF(item, 2, 12, null, 1);
	img = getChildOfType(img, "IMG", 1);
	img.src = submitBtnNormalImg;
	//after we stop the level we need to update it after we got response from server
	setChartTableOppLevel(item, getHiddenOppLevel(item));
	var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
	if (chartIframe) {
		chartIframe.contentWindow.unfreezeChart();
	}
}

function closeSlip(button) {
	var item = button;
	for (var i = 0; i < 10; i++) {
		item = item.parentNode;
	}

	hideElm(getOppTRSF(item, 2));
	hideElm(getOppTRSF(item, 3));
	showElm(getOppTRSF(item, 1), ELEMENT_TYPE_TABLE);
	showElm(getOppTRSF(item, 2, 12, null, 1));
}

function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}

function getUTCOffset() {
	var d = new Date();
	var localHour = d.getHours();
	var utcHour = d.getUTCHours();
	var offset = localHour - utcHour;
	if (offset < -12) {
		offset += 24;
	}
	if (offset > 12) {
		offset -= 24;
	}
	return offset;
}

function getBox(boxNumber, row) {
	var tmp = document.getElementById("boxHolder" + boxNumber);
	tmp = getChildOfType(tmp, "TABLE", 1);
	tmp = getChildOfType(tmp, "TBODY", 1);
	if (null != row) {
		if (row == 0) {
			return tmp;
		}
		return getChildOfType(tmp, "TR", row);
	}
	return getChildOfType(tmp, "TR", 4);
}

//print slip success
function printSuccess(link) {
	var entry = link;
	for (var i = 0; i < 3; i++) {
		entry = entry.parentNode;
	}
	var tr = getChildOfType(entry, "TR", 11);
	var td = getChildOfType(tr, "TD", 2);
	var tmpInvId = td.innerHTML;
	var invId = tmpInvId.substring(1);

//	printFromServerSlip(invId);
//	window.open(context_path + "/jsp/waiting.html", "invSuccessPrint", "width=403,height=427,menubar=1,toolbar=0,scrollbars=0");
//	setTimeout('window.open("' + context_path + '/jsp/invSuccessPrint.jsf", "invSuccessPrint", "width=403,height=427,menubar=1,toolbar=0,scrollbars=0");', 300);
	window.open(context_path + "/jsp/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}

//print from server the slip success
function printFromServerSlip(id) {
	xmlHttp = getXMLHttp();
	if (null == xmlHttp) {
		return;
	}
	xmlHttp.open("POST", "ajax.jsf", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.send("command=print&slipEntryId=" + id);
}

var markets = null;
//function updateItemCtrl(itemPos, updateInfo) {
//	var cmd = updateInfo.getNewValue(2);
//	if (cmd == "ADD") {
//		var addMarket = updateInfo.getNewValue(3);
//		var addPriority = new Number(updateInfo.getNewValue(4));
//		var addScheduled = updateInfo.getNewValue(5);
//		log("ADD market: " + addMarket + " priority: " + addPriority + " scheduled: " + addScheduled + " key: " + updateInfo.getNewValue(1));
//
//		var replace = false;
//		if (null != markets) {
//			for (var i = 0; i < markets.length; i++) {
//				if (markets[i][0] == addMarket) {
//					log("replace");
//					replace = true;
//					markets[i][1] = addPriority;
//					markets[i][2] = addScheduled;
//				}
//			}
//		}
//
//		if (!replace) {
//			var marketsSize = (null == markets ? 1 : markets.length + 1);
//			log("marketsSize: " + marketsSize);
//			var newMarkets = new Array(marketsSize);
//			if (null != markets) {
//				for (var i = 0; i < markets.length; i++) {
//					newMarkets[i] = markets[i];
//				}
//			}
//			var newMarket = new Array(3);
//			newMarket[0] = updateInfo.getNewValue(3);
//			newMarket[1] = new Number(updateInfo.getNewValue(4));
//			newMarket[2] = updateInfo.getNewValue(5);
//			newMarkets[newMarkets.length - 1] = newMarket;
//			markets = newMarkets;
//			log("markets: " + markets);
//		}
//
//		scheduleStartBoxes();
//	}
//	if (cmd == "DELETE") {
//		var deleteMarket = updateInfo.getNewValue(3);
//		log("DELETE market: " + deleteMarket);
//		var newMarkets = new Array(markets.length - 1);
//		var j = 0;
//		for (var i = 0; i < markets.length; i++) {
//			if (markets[i][0] != deleteMarket) {
//				newMarkets[j] = markets[i];
//				j++;
//			} else { // delete (skip the copy)
//				if (i < 5) {
//					scheduleStartBoxes();
//				}
//			}
//		}
//		markets = newMarkets;
//		log("markets: " + markets);
//	}
//	if (cmd != "ADD" && cmd != "DELETE") {
//		log("unexpected command: " + cmd);
//	}
//}

var boxCleanTimeout = null;
function scheduleStartBoxes() {
	if (null != boxCleanTimeout) {
		clearTimeout(boxCleanTimeout);
		boxCleanTimeout = null;
	}
//	if (null != markets && markets.length == 45) {
//		cleanBoxes();
//	} else {
		boxCleanTimeout = setTimeout('cleanBoxes();', 10000);
//	}
}

var boxFillTimeout = null;
function cleanBoxes() {
	log("Cleaning boxes");
	if (null != markets && markets.length > 0) {
		try {
//			sortMarkets(markets);
//			log("Sorted markets: " + markets);
			var haveCleaned = false;
			var j = 0;
			for (var i = 0; i < 4; i++) {
				var reallyFixed = boxesFixed[i + 1] && (isMarketOpened(boxesFixedMarkets[i + 1]) || isOnHomepage(boxesFixedMarkets[i + 1]));
				if (!reallyFixed) {
					// find market that's not among the fixed
					while (j < markets.length && isFixedMarket(markets[j][0])) {
						j++;
					}
				}
				log("box " + (i + 1) + " full: " + boxesFull[i + 1] + " fixed: " + boxesFixed[i + 1] + " market: " + boxesMarkets[i + 1] + " reallyFixed: " + reallyFixed + " j: " + j + " markets: " + markets[j][0]);
				if ((boxesFull[i + 1] && !reallyFixed && boxesMarkets[i + 1] != markets[j][0]) ||
					(boxesFull[i + 1] && reallyFixed && boxesMarkets[i + 1] != boxesFixedMarkets[i + 1])) {
					removeBoxTable(i + 1);
					haveCleaned = true;
				}
				if (!boxesFull[i + 1]) {
					haveCleaned = true;
				}
				if (!reallyFixed) {
					j++;
				}
			}
			if (haveCleaned) {
				lsPage.removeTable("aoTable");
				if (null != boxFillTimeout) {
					clearTimeout(boxFillTimeout);
					boxFillTimeout = null;
				}
				startBoxes();
			}
		} catch (err) {
			log("Problem in cleanBoxes: " + err);
		}
	}
}

function startBoxes() {
	var j = 0;
	for (var i = 0; i < 4; i++) {
		var reallyFixed = boxesFixed[i + 1] && (isMarketOpened(boxesFixedMarkets[i + 1]) || isOnHomepage(boxesFixedMarkets[i + 1]));
		if (!reallyFixed) {
			// find market that's not among the fixed
			while (j < markets.length && isFixedMarket(markets[j][0])) {
				j++;
			}
		}
		if (!boxesFull[i + 1]) {
			if (!reallyFixed) {
				log("starting box: " + (i + 1) + " market: " + markets[j][0] + " schedule: " + markets[j][2]);
//				addBoxTable(markets[j][0], markets[j][2], false, i + 1);
				group[i] = "aotps_" + markets[j][2] + "_" + markets[j][0];
				boxesMarkets[i + 1] = markets[j][0];
				boxesFull[i + 1] = true;
			} else {
				log("starting fixed box: " + (i + 1) + " market: " + boxesFixedMarkets[i + 1] + " schedule: " + boxesFixedScheduled[i + 1]);
//				addBoxTable(boxesFixedMarkets[i + 1], boxesFixedScheduled[i + 1], boxesFixed[i + 1], i + 1);
				group[i] = "aotps_" + boxesFixedScheduled[i + 1] + "_" + boxesFixedMarkets[i + 1];
				boxesMarkets[i + 1] = boxesFixedMarkets[i + 1];
				boxesFull[i + 1] = true;
			}
		}
		if (!reallyFixed) {
			j++;
		}
	}
	aoTable = new NonVisualTable(group, schema, "COMMAND");
	aoTable.setDataAdapter("JMS_ADAPTER");
	aoTable.setSnapshotRequired(true);
	aoTable.setRequestedMaxFrequency(1.0);
	aoTable.onItemUpdate = updateItemAO;
	lsPage.addTable(aoTable, "aoTable");
}

//function sortMarkets(markets) {
//	var hasSwapped = false;
//	do {
//		hasSwapped = false;
//		for (var i = 0; i < markets.length - 1; i++) {
//			if (markets[i][1] > markets[i + 1][1]) {
//				var tmp = markets[i];
//				markets[i] = markets[i + 1];
//				markets[i + 1] = tmp;
//				hasSwapped = true;
//			}
//		}
//	} while (hasSwapped);
//}

var marketStates = null;
//function updateItemState(itemPos, updateInfo) {
//	var updateCmd = updateInfo.getNewValue(2);
//	var updateMarket = updateInfo.getNewValue(1);
//	var updateState = updateInfo.getNewValue(3);
//	log("state cmd: " + updateCmd + " market: " + updateMarket + " state: " + updateState);
//
//	if (updateCmd == "ADD" || updateCmd == "UPDATE") {
//		var replace = false;
//		if (null != marketStates) {
//			for (var i = 0; i < marketStates.length; i++) {
//				if (marketStates[i][0] == updateMarket) {
//					log("state replace");
//					replace = true;
//					marketStates[i][1] = updateState;
//				}
//			}
//		}
//
//		if (!replace) {
//			var marketsSize = (null == marketStates ? 1 : marketStates.length + 1);
//			log("marketsSize: " + marketsSize);
//			var newMarketStates = new Array(marketsSize);
//			if (null != marketStates) {
//				for (var i = 0; i < marketStates.length; i++) {
//					newMarketStates[i] = marketStates[i];
//				}
//			}
//			var newMarket = new Array(2);
//			newMarket[0] = updateMarket;
//			newMarket[1] = updateState;
//			newMarketStates[newMarketStates.length - 1] = newMarket;
//			marketStates = newMarketStates;
//			log("marketStates: " + marketStates);
//		}
//	}
//	if (updateCmd == "DELETE") {
//		if (null != marketStates) {
//			var newMarketsStates = new Array(marketsStates.length - 1);
//			var j = 0;
//			for (var i = 0; i < marketsStates.length; i++) {
//				if (marketsStates[i][0] != updateMarket) {
//					newMarketsStates[j] = marketsStates[i];
//					j++;
//				}
//			}
//			marketsStates = newMarketsStates;
//			log("marketStates: " + marketStates);
//		}
//	}
//	if (isFixedMarket(updateMarket)) { // an open/close of a fixed market
//		log("Open/close of a fixed market. Schedule start boxes.");
//		scheduleStartBoxes();
//	}
//	if (updateState == 0) {
//		if (checkAllClose()) {
//			//show off hour banner 0
//			switchTable(0);
//		}
//	} else {
//		//show the market table 1
//		switchTable(1);
//	}
//}

function isMarketOpened(marketId) {
	for (var i = 0; i < marketStates.length; i++) {
		if (marketStates[i][0] == marketId) {
			return marketStates[i][1] > 0;
		}
	}
	return false;
}

function isOnHomepage(marketId) {
	for (var i = 0; i < markets.length; i++) {
		if (markets[i][0] == marketId) {
			return true;
		}
	}
	return false;
}

function isFixedMarket(marketId) {
	for (var i = 1; i <= 4; i++) {
		if (boxesFixedMarkets[i] == marketId) {
			return true;
		}
	}
	return false;
}

function dumpUpdate(itemUpdate) {
	for (var i = 1; i <= 27; i++) {
		log("dump " + i + ": " + itemUpdate.getServerValue(i));
	}
}

function engineStatusChange(chngStatus) {
	log("ENGINE state changed " + chngStatus);
	if (chngStatus != "STREAMING") { // DISCONNECTED
		log("ENGINE DISCONNECTED");
		for (var i = 1; i <= 4; i++) {
			var b = getBox(i, 0);
			clearBoxRows(b);
			if (getChildOfType(b, "TR", 2).style.display == "none") {
				var loading = getChildOfType(b, "TR", 1);
				showElm(loading, ELEMENT_TYPE_ROW);
			}
		}
	}
}

function clearBoxRows(box) {
	var tr = null;
	do {
		tr = getChildOfType(box, "TR", 4);
		if (null != tr) {
			box.removeChild(tr);
		}
	} while (null != tr);
}

function storeSlipStateToCookie() {
	var val = "";
	for (var i = 1; i <= 4; i++) {
		var box = getBox(i);
		var type = 0;
		var amount = "0";
		if (null != box) {
			var tmp = getOppTRSF(box, 2);
			type = tmp.style.display == 'none' ? 0 : 1;
			if (type == 1) {
				type = getOppTRSF(box, 2, 4, null, 2).style.display == 'none' ? 2 : 1;
				amount = getOppSlipAmount(box);
			}
		}
		amount = amount.length == 0 ? 0 : amount;
		val += type + "_" + amount;
		if (i != 4) {
			val += "_";
		}
	}
	createCookie("slip", val);
}

function clearSlipStateFromCookie() {
	eraseCookie("slip");
}

////////////// off hour banner //////////////

//switch the markets table to "off hours banner"
//parm flag: 1 to show markets table, 0 to show off hours banner
function switchTable(flag) {
	var mTbl = document.getElementById("markets_table");
	var oHB = document.getElementById("off_hours_banner");
	if (flag == 1) {
		mTbl.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		oHB.style.display = 'none';
	} else {
		mTbl.style.display = 'none';
		oHB.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
	}
}

//check if all opps on home page are closed ( state 1 created)
//return 1 if all close else 0
function checkAllClose() {
	for (j = 0; j < marketStates.length; j++) {
		if (marketStates[j][1] > 0 && !isIsraelMarket(marketStates[j][0])) {
			return 0;
		}
	}
	return 1;
}

function isIsraelMarket(marketId) {
	for (i = 0; i < israelMarkets.length; i++) {
		if (israelMarkets[i] == marketId) {
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////
var blinkTimer = null;
var tmpbox = null;
function tickerClick(market_id, schedule, boxId, isBankOption){
	var strHref = window.location.href;
  	if (strHref.indexOf(homePageN) > -1) {

  		for (var i=1;i<=4;i++) {
			if ((boxesMarkets[i] == market_id) && (boxId != i)) {
				return;
			}
		}

		removeBoxTable(boxId);
		boxesFixed[boxId] = true;
	    addBoxTable(market_id, schedule, true, boxId);

	    //createCookie("box" + boxId, market_id + "_" + schedule, 360);

		//if its from bank option move up
	    if (isBankOption) {
			scroll(0, 0);
		}

	    tmpbox = document.getElementById("boxHolder" + boxId);
		if (null != blinkTimer){
			clearInterval(blinkTimer);
			blinkCounter=0;
			blinkTimer=null;
		}
		blinkTimer = setInterval ( "divblink()", 250 );
	}
}

var blinkCounter=0;
function divblink(){
	try {
	    var tmptable = getChildOfType(tmpbox, "TABLE", 1);
	    var tmpbody = getChildOfType(tmptable, "TBODY", 1);
	    var tmptr = getChildOfType(tmpbody, "TR", 4);
	    var tmptd = getChildOfType(tmptr, "TD", 1);
	    tmptable = getChildOfType(tmptd, "TABLE", 1);
	    tmpbody = getChildOfType(tmptable, "TBODY", 1);
	    tmptr = getChildOfType(tmpbody, "TR", 1);
	    tmptd = getChildOfType(tmptr, "TD", 1);
		var TableToBlink = getChildOfType(tmptd, "TABLE", 1);
		tmpbody = getChildOfType(TableToBlink, "TBODY", 1);
	    tmptr = getChildOfType(tmpbody, "TR", 1);
	    tmptdToBlinkAsset = getChildOfType(tmptr, "TD", 2);
   	    tmptdToBlinkExp = getChildOfType(tmptr, "TD", 6);



		if(blinkCounter%2==0){
			TableToBlink.setAttribute("background" , "../images/" +imgPath + "/1px.jpg");
			showElm(tmptdToBlinkAsset);
			showElm(tmptdToBlinkExp);
		} else {
			TableToBlink.setAttribute("background" , "");
			hideElm(tmptdToBlinkExp);
			hideElm(tmptdToBlinkAsset);
		}
		blinkCounter++;
		if (blinkCounter==11){
			clearInterval(blinkTimer);
			blinkCounter=0;
			blinkTimer=null;
		}
	} catch(e) {
		clearInterval(blinkTimer);
		blinkCounter=0;
		blinkTimer=null;
	}
}

function isBoxSubmitting(img) {
	var item = img;
	for (var i = 0; i < 6; i++) {
		item = item.parentNode;
	}

	if (getOppTRSF(item, 1, 5, null, 7).innerHTML == 1) {
		return true;
	}
	return false;
}

function closeBoxBanner(xImg) {
	var box = xImg.parentNode;
	for (var i = 0; i < 6; i++) {
		box = box.parentNode;
	}

	var banner = document.getElementById("bannerHolder");
	banner.style.display = "none";


}

function closeBtn(){
	var box = document.getElementById('bannerHolder');
	box = box.parentNode;
	var newBox = getChildOfType(box, "TR", 4);
	if (null == newBox) {
	 	newBox = getChildOfType(box, "TR", 1);
	}
	showElm(newBox, ELEMENT_TYPE_ROW);

	var banner = document.getElementById("bannerHolder");
	hideElm(banner);
}

function startBtn(){
	window.top.location.href = context_path + "/jsp/register.jsf";
}

function printSuccessOneTouch(invId, oneTUpDown) {
	window.open(context_path + "/jsp/invSuccessPrintOneTouch.jsf?investmentId=" + invId + "&typeIn=" + oneTUpDown, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}

/*################### daniela flash movie commands ################*/
var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
// Handle all the FSCommand messages in a Flash movie.
function welcom_DoFSCommand(command, args) {
	var welcomObj = isInternetExplorer ? document.all.welcom : document.welcom;
	// no need to check which command its bcoz we have only 1 command
	closeBtn();
	//
}
// Hook for Internet Explorer.
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub welcom_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call welcom_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');
}
/*################### end daniela flash movie commands ################*/

function openChart(link) {
	var item = link;
	for (i = 0; i < 10; i++) {
		item = item.parentNode;
	}
	var marketId = getLSValue(getOppTRSF(item, 1, 5, null, 8));
	var oppId = getLSValue(getOppTRSF(item, 1, 5, null, 2));
	var oppState = getLSValue(getOppTRSF(item, 1, 5, null, 5));
	var tdChart = getOppTRSF(item, 4, 5, null, 1);

	tdChart.innerHTML = "<iframe id='chart" + oppId + "' src='" + context_path + "/jsp/chartPopup.jsf?marketId=" + marketId + "&oppId=" + oppId + "&command=" + oppState + "' width='351' height='150' frameborder='0'></iframe>";

	hideElm(getOppTRSF(item, 1));
	showElm(getOppTRSF(item, 4), ELEMENT_TYPE_TABLE);

	return false;
}

function closeChart(link) {
	var item = link;
	for (i = 0; i < 10; i++) {
		item = item.parentNode;
	}

	var tdChart = getOppTRSF(item, 4, 5, null, 1);
	tdChart.innerHTML = "";

	hideElm(getOppTRSF(item, 4));
	hideChartDiv(item, 1);
	hideChartDiv(item, 2);
	hideChartDiv(item, 3);
	showElm(getOppTRSF(item, 1), ELEMENT_TYPE_TABLE);

	return false;
}

//get chart box opp level td
function getChartTableOppLevelTd(item, tr, td) {
	var levelTd = getOppTRSF(item, 4, 3, 1, 3);
	var levelTable = getChildOfType(levelTd, "TABLE", 1);
	var levelTbody = getChildOfType(levelTable, "TBODY", 1);
	var levelTr = getChildOfType(levelTbody, "TR", tr);
	return getChildOfType(levelTr, "TD", td);
}

//update the level in the chart table so when u open it u will c level.
function setChartTableOppLevel(item, level) {
	if (getOppTRSF(item, 1, 5, null, 7).innerHTML == "0") { //if not submiting
		var levelTd = getChartTableOppLevelTd(item, 2 ,1);
		levelTd.innerHTML = level;
	}
}

//show slip in the chart trade box
//div 1 - slip, 2 - recipt, 3 - errorMsg
//elementPos eflement to start the pos form
//top px to move up or down
//left px to move left or right
function showChartDiv(item, div, elementPos, top, left) {
	var mTd = elementPos;
	var conTD = getChildOfType(item, "TD", 1);
	var conDiv = getChildOfType(conTD, "DIV", div);
	if (null != mTd && null != conDiv) {
		$(conDiv).css("left", f_getPosition(mTd, 'Left') + left); //4
		$(conDiv).css("top", f_getPosition(mTd, 'Top') + top); //28
	}

	conDiv.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
}


function hideChartDiv(item, div) {
	var conTD = getChildOfType(item, "TD", 1);
	var conDiv = getChildOfType(conTD, "DIV", div);
	conDiv.style.display = "none";

	if(div == 1) {//if we close the slip
		var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
		if (chartIframe != null) {
			chartIframe.contentWindow.hidePut();
			chartIframe.contentWindow.hideCall();
		}
	}
}

function closeChartDiv(button, divNumber) {
	for (i = 0; i < 11 ; i++) {
		button = button.parentNode;
	}
	hideChartDiv(button, divNumber);
	if (divNumber == 1) {
		hideChartDiv(button, 3); //hide error msg if there was.
	}
}
	//get the trade box chart span with the value
	//row the TR number
	//field the TD number
	//span the SPAN number
	function getOppChartDivValue(item, div, row, field, span) {
		var slipTmp = getChildOfType(item, "TD", 1);
		var slipDiv = getChildOfType(slipTmp, "DIV", div);
		if (null == row) {
			return slipDiv;
		}
		slipTmp = getChildOfType(slipDiv, "TABLE", 1);
		slipTmp = getChildOfType(slipTmp, "TBODY", 1);
		slipTmp = getChildOfType(slipTmp, "TR", 2);
		slipTmp = getChildOfType(slipTmp, "TD", 2);
		slipTmp = getChildOfType(slipTmp, "TABLE", 1);
		var slipTbody = getChildOfType(slipTmp, "TBODY", 1);
		var slipTr = getChildOfType(slipTbody, "TR", row);
		if (null == field) {
			return slipTr;
		}
		var slipTd = getChildOfType(slipTr, "TD", field);
		if (span) {
			if (getChildOfType(slipTd, "SPAN", span)) {
				return getChildOfType(slipTd, "SPAN", span);
			} else {
				return getChildOfType(slipTd, "DIV", span);
			}
		} else {
			return slipTd;
		}
	}

	//open slip when chart trade box open
	function openSlipFromChartBox(item, type) {
		for (i = 0; i < 4 ; i++) {
			item = item.parentNode;
		}
		openSlip(item, type);
	}


	function updateWinLoseFromChartBox(amount, event) {
		if (event.keyCode == 13) {
			submitSlipChartBox(amount.parentNode);
			return;
		}

		if ((event.keyCode != 9) && (event.keyCode != 16) &&
				(event.keyCode != 37) && (event.keyCode != 38) &&
				(event.keyCode != 39) && (event.keyCode != 40)) {
			checkChars(amount);
		}

		var item = amount;
		for (var i = 0; i < 12; i++) {
			item = item.parentNode;
		}
		updateWinLoseInt(item);
	}

	function submitSlipChartBox(button) {
		for (var i = 0; i < 5; i++) {
			button = button.parentNode;
		}
		submitSlip(button);
	}

//show chart call or put image
//image = image u was on with mouse
//flag = true for show, false for hide
//isCall = true for call, false for put
function chartShowCallPut(image, flag, isCall) {
	var item = image;
	for (var i = 0; i < 14; i++) {
		item = item.parentNode;
	}

	var chartIframe = document.getElementById("chart" + getLSValue(getOppTRSF(item, 1, 5, null, 2)));
	if (chartIframe != null) {
		if (flag) {
			if (isCall) {
				chartIframe.contentWindow.hidePut();
				chartIframe.contentWindow.showCall();
			} else {
				chartIframe.contentWindow.hideCall();
				chartIframe.contentWindow.showPut();
			}
		} else {
			var slipChoice = 0;
			if (getOppChartDivValue(item, 1).style.display != 'none') {
				slipChoice = getOppChartDivValue(item, 1, 2, 2).style.display == 'none' ? 2 : 1;
				if (isCall) {
					if (slipChoice != 1) {
						chartIframe.contentWindow.hideCall();
					}
				} else if (slipChoice != 2) {
					chartIframe.contentWindow.hidePut();
				}
			}
			if (slipChoice != 0) {
				if (slipChoice == 1) {
					chartIframe.contentWindow.showCall();
				} else {
					chartIframe.contentWindow.showPut();
				}
			} else {
				chartIframe.contentWindow.hidePut();
				chartIframe.contentWindow.hideCall();
			}
		}
	}
}

//print slip success
function printSuccessFromChartBox(link) {
	var entry = link;
	for (var i = 0; i < 3; i++) {
		entry = entry.parentNode;
	}
	var tr = getChildOfType(entry, "TR", 9);
	var td = getChildOfType(tr, "TD", 1);
	var tmpInvId = getChildOfType(td, "SPAN", 2).innerHTML;
	var invId = tmpInvId.substring(1);

	window.open(context_path + "/jsp/invSuccessPrint.jsf?investmentId=" + invId, "invSuccessPrint", "width=498,height=427,menubar=1,toolbar=0,scrollbars=0");
}

function isBoxSubmittingChartBox(img) {
	for (var i = 0; i < 5; i++) {
		img = img.parentNode;
	}
	return isBoxSubmitting(img);
}

// Update is_accepted_sms to investment for getting expiration message
function updateInvestSms(element, invId) {
	if (null == invId) {
		var tBody = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
		var trInv = getChildOfType(tBody, "TR", 11);
		var tdInv = getChildOfType(trInv, "TD", 2);
		invId = tdInv.innerHTML;
	}

	var xmlHttp = getXMLHttp();
	if (null == xmlHttp) {
		return false;
	}
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			if (xmlHttp.responseText.length >= 2) {
				if (xmlHttp.responseText.substring(0, 2) == "ok") {
					var tr = element.parentNode.parentNode;
					var td = getChildOfType(tr, "TD", 2);
					td.innerHTML = acceptSmsMessage;
					//element.style.display = 'none';
					element.disabled = true;
				} else {
					//do nothing we dont have text and not sure we want :)
				}
			}
		}
	}
	if (invId.indexOf("#") == 0) {
		invId = invId.substring(1);
	} else {
		invId = invId.substring(0, invId.length - 1);
	}
	xmlHttp.open("POST", "ajax.jsf", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.send("turnSmsOn&invId=" + invId);
}

function updateInvestSmsFromChartBox(element) {
	var tBody = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
	var trInv = getChildOfType(tBody, "TR", 9);
	var tdInv = getChildOfType(trInv, "TD", 1);
	var spandInv = getChildOfType(tdInv, "SPAN", 2);
	updateInvestSms(element, spandInv.innerHTML);
}