/*small function that could be used in site*/
/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}
/*submit a form by id*/
function submit_form(id){g(id).submit();}
/*console.log() shorted*/
function l(){console.log(arguments);}
/*input on focus delete the value*/
function inpFocus(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	def = def.toUpperCase();
	var pass = (field.getAttribute('data-p') != undefined)?true:false;
	var tooltip = (field.getAttribute('data-tooltip') != undefined)?g(field.getAttribute('data-tooltip')):'';
	if(field.value == ""){
		field.value = def;
		if(pass){field.type = 'text';}
		field.className = field.className.replace(/active/g,'');
	}
	else if(field.value.toUpperCase() == def){
		field.value = "";
		if(pass){field.type = 'password';}
		field.className += " active";
		field.className = field.className.replace(/correct/g,'');
	}
	else{
		field.className += " active";
		field.className = field.className.replace(/correct/g,'');
	}
	if(field.getAttribute('data-tooltip_show') == 't'){
		if(tooltip != ""){
			tooltip.style.display = "none";
			field.setAttribute('data-tooltip_show','');
		}	
	}
	else{
		if(tooltip != ""){
			tooltip.style.display = "block";
			tooltip.style.top = -(tooltip.offsetHeight/2)+(field.offsetHeight/2)+'px';
			field.setAttribute('data-tooltip_show','t');
		}	
	}
	if(noblur == undefined){
		field.onblur = function(){
			inpFocus(field,noblur);
			eval(field.getAttribute('onblur'));
		}
	}
}
function inpFocusShort(field,noblur){
	var def = (field.getAttribute('data-def') != undefined)?field.getAttribute('data-def'):(field.alt != '')?field.alt:field.defaultValue;
	field.value = def;
}
function checkBox(th){
	var inp = th.getElementsByTagName('input')[0];
	if(inp.checked){
		inp.checked = false;
		th.className = th.className.replace(/ checked/g,'');
	}
	else{
		inp.checked = true;
		th.className += " checked";
	}
}
/*Pop up for documents. Gets parameter of the class with the image*/
function pop_doc(cls){
	if(g('pop_doc_bgr') == undefined){
		var bgr = document.createElement('div');
		bgr.className = "pop_doc_bgr";
		bgr.id = "pop_doc_bgr";
		bgr.onclick = function(){pop_doc_close();}
		window.document.body.appendChild(bgr);
		
		var holder = document.createElement('div');
		holder.id = "pop_doc_holder";
		window.document.body.appendChild(holder);
		
		var ss = document.createElement('span');
		ss.className = "pop_doc_close";
		ss.onclick = function(){pop_doc_close();}
		holder.appendChild(ss);
	}
	else{
		var holder = g('pop_doc_holder');
	}
	holder.className = "pop_doc_holder "+cls;
	window.scrollTo(0,0);
	gd_fade('pop_doc_bgr',0,100);
	gd_fade('pop_doc_holder',0,100);
}
function pop_doc_close(){
	gd_fade('pop_doc_bgr',100,0);
	gd_fade('pop_doc_holder',100,0);
}
/*fade*/
var gd_fade_t = new Array();
function gd_fade(id,from,to){
	var el = g(id);
	clearTimeout(gd_fade_t[id]);
	if(from > to){var dir = "down";}
	else{var dir = "up";el.style.display = "block";}
	var left = Math.abs(from-to);
	function doIt(){
		if(left > 0){
			if(dir == "up"){
				from += 10;
			}
			else{
				from -= 10;
			}
			el.style.opacity = from/100;
			el.style.filter = "alpha(opacity="+from+")";
			left -= 10;
			gd_fade_t[id] = setTimeout(doIt,30);
		}
		else{
			if(dir == "down"){
				el.style.display = "none";
			}
		}
	}
	doIt();
}
/*fade END*/
function addScriptToHead(filename,scr_id){
	if(!g(scr_id)){
	  var js = document.createElement('script');
	  js.id = scr_id;
	  js.src = context_path+"/js/"+filename;
	  document.getElementsByTagName('head')[0].appendChild(js);
	}
}
var ua = navigator.userAgent.toLowerCase();
var funnel_div_id = '';
var register_lp = false;
function initRegForm(id,skinId){
	funnel_div_id = id;
	addScriptToHead('regForm_all.js','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
		
	//Add json lib. for IE7
	if (ua.indexOf('msie 7.0') >= 0){  
		addScriptToHead('json2.js','initRegFormJson');
	}
}
var global_deposit_prefix = '';
var add_to_single_errors_map_out = [];
function initDeposit(deposit_prefix){
	global_deposit_prefix = deposit_prefix;
	addScriptToHead('regForm_all.js','initRegForm');
	//regForm_all.js loads map.js
	//map.js loads validation_reg_funnel.js
	//validation_reg_funnel.js loads regForm_map.jsf
}
function cont_us_show(show){
	if(show){
		g('cont_us_phones').style.display = 'block';
		g('cont_us_right_banner_opend').style.display = 'block';
		g('cont_us_right_banner_closed').style.display = 'none';
	}else{
		g('cont_us_phones').style.display = 'none';
		g('cont_us_right_banner_opend').style.display = 'none';
		g('cont_us_right_banner_closed').style.display = 'block';
	}
}
function submitWait(th){
	th.onclick = function (){return false;}
	th.className += " disabled";
}
var paymentMethod_menu = '';
function open_deposit(th,deposit_prefix){
	errorMap.clear();
	global_deposit_prefix = deposit_prefix;
	try{
		idPrefix = deposit_prefix;
	}catch(e){}
	var el = th.parentNode.getElementsByTagName('li')[1];
	if(el.style.display == "none"){
		el.style.display = "block";
		var title = th.getElementsByTagName('span')[0];
		title.className += " paymentMethods_title_selected";
	}else{
		el.style.display = "none";
		var title = th.getElementsByTagName('span')[0];
		title.className = "paymentMethods_title";
	}
	if((paymentMethod_menu != '')&&(paymentMethod_menu != th)){
		var el = paymentMethod_menu.parentNode.getElementsByTagName('li')[1];
		el.style.display = "none";
		var title = paymentMethod_menu.getElementsByTagName('span')[0];
		title.className = "paymentMethods_title";
	}
	paymentMethod_menu = th;
}
function set_deposit_prefix(deposit_prefix){
	global_deposit_prefix = deposit_prefix;
	try{
		idPrefix = deposit_prefix;
	}catch(e){}
}
// This part is used only for qiquan only war, when the qiquan war is merged with ao this will be unnecessary
//Lightstreamer fields
var level_skin_group_id = 'LEVEL_3';
var level_color = 'CLR_3';
var insurance_current_level = 'INS_CRR_LEVEL_3';
// End of lightsreamer fields

/*start*/
var closeOnClickOut_current = null;
var closeOnClickOut_callBack = null;
function closeOnClickOut(id,callBack){
	closeOnClickOut_current = id;
	closeOnClickOut_callBack = callBack;
	addEvent(window.document.body,'click',closeOnClickOutMain);
}
function closeOnClickOutMain(e){
	var t = e.target || e.srcElement;
	while(t){
		if(t.id == closeOnClickOut_current){
			return false;
		}
		else{
			t=t.parentNode;
		}
	}
	if(typeof closeOnClickOut_callBack != "undefined"){
		eval(closeOnClickOut_callBack);
	}
	else{
		g(closeOnClickOut_current).style.display = "none";
	}
	remEvent(window.document.body,'click',closeOnClickOutMain);
}
/*end*/
function getTopLevelById(el,id){
	while(el){
		if(el.id == id){
			return el;
		}
		el = el.parentNode;
	}
}
function closeErrorPop(el,top_id){
	if(typeof top_id != "undefined"){
		getTopLevelById(el,top_id).style.display = "none";
	}
	else{
		el.parentNode.style.display = "none";
	}
}
function addEvent(el,e,fn){
	if(el.addEventListener){
		el.addEventListener(e, fn, false);
	}else{
		el.attachEvent('on'+e, fn);
	}
}
function remEvent(el,e,fn){
	if(el.removeEventListener){
		el.removeEventListener(e, fn, false);
	}else{
		el.detachEvent('on'+e, fn);
	}
}

function changeAcc() {
	eraseCookie("AOETUsername");
	//location.reload(); - causes firefox to display alert
	window.location.href = window.location.href;
}

function do_we_know_you() {
	var iKnowYouCookie = getCookie("AOETUsername");
	iKnowYouCookie = iKnowYouCookie.replace(/"/g, "");
	var changeAccount = document.getElementById("changeAccount");
	var rememberMeCheckBox = document.getElementById("login:rememberMeCheckBox");
	var userNameInput = document.getElementById("login:j_username");
	var userNameOutput = document.getElementById("login:j_username2");
	
	var userNameLabel = document.getElementById("login:userNameLabel");
	
	if (iKnowYouCookie != null && iKnowYouCookie != "") {
		userNameOutput.innerHTML = iKnowYouCookie;
		userNameOutput.style.display = "";
		
		userNameInput.value = iKnowYouCookie;		
		userNameInput.style.display = "none";
		
		userNameLabel.innerHTML = "";
		
		changeAccount.style.display = "";
		rememberMeCheckBox.checked = true;
	} else {
		userNameOutput.innerHTML = "";
		userNameOutput.style.display = "none";
		
		userNameInput.value = "";
		userNameInput.style.display = "";
		userNameInput.className="login_input vaM ml10";
		
		changeAccount.style.display = "none";
		rememberMeCheckBox.checked = false;
	}
}