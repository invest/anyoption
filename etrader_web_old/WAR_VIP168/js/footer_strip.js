/*By Georgi Diamandiev*/
function cloneId(id,where){
	var el = g(id);
	var w = g(where);
	var clone = el.cloneNode(true);
	w.appendChild(clone);
}
function slider2(id){
	var d = g(id);
	var img = [];
	img[0] = g(id+'_1');
	var imgs = img[0].getElementsByTagName('img');
	var sum = 0;
	for(var i=0;i<imgs.length;i++){
		sum += imgs[i].offsetWidth;
	}
	img[0].style.width = sum+"px";
	cloneId(id+'_1',id);
	d.getElementsByTagName('div')[1].id = id+'_2';
	img[1] = d.getElementsByTagName('div')[1];
	img[0].style.left = img[0].offsetWidth+"px";
	
	img[1].getElementsByTagName('img')[0].onload = function(){
		var c = 0;
		var w = img[0].offsetWidth;
		var hw = d.offsetWidth;
		var inc = 1;
		var curr = img[1];
		var curr2 = img[0];
		var st = true;
		var doSetTimeout = function(){
			// console.log((Math.abs(curr.offsetLeft)+hw) +'/'+ w);
			if(((Math.abs(curr.offsetLeft)+hw+inc) > w)&&(st)){
				curr2.style.left = hw+"px";
				st = false;
			}
			if(!st){
				curr2.style.left = curr2.offsetLeft-inc+"px";
			}
			curr.style.left = curr.offsetLeft-inc+"px";
			if(Math.abs(curr.offsetLeft) >= w){
				var curr3 = curr;
				curr = curr2;
				curr2 = curr3;
				st = true;
			}
			setTimeout(doSetTimeout,40);
		}
		doSetTimeout();
	}
}