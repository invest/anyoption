
/* iphone scroll to top */
addEventListener("load", function()	{
	    setTimeout(updateLayout, 0);
	}, false);

var currentOrient = -1; // ( 0/180: portrait | 90/-90: landscape | -1: my initial value)

//redirectIPhoneToHomescreen();

function orientationChangeHP() {
	if (document.getElementById('instBackDiv') != null) {
		orientationUpdate();
	}
	resize();
}

function updateLayout() {
    if (window.orientation != currentOrient) {
        currentOrient = window.orientation;
        var orient = Math.abs(currentOrient) == 90 ? "landscape" : "portrait";
        document.body.setAttribute("orient", orient);
        setTimeout(function() {
            window.scrollTo(0, 1);
        }, 100);
        // handle message div
        orientationChange();
    }
}

setInterval(updateLayout, 100);  // for update orientation change
// Note: for ipone we can use onorientationchange eventListener

/*
	Handle on orientation change event
*/
function orientationChange() {
	var mesDiv = document.getElementById("modalContainer");
	if (null != mesDiv) {
		var sWidth = window.innerWidth;
		var sHight = window.innerHeight;
		mesDiv.style.left = String(Math.round((sWidth-290)/2))+"px";
		mesDiv.style.top = String(Math.round((sHight-188)/2) + getScrollXY()[1]) +"px";
	}
	var mesConDiv = document.getElementById("containerDiv");
	if (null != mesConDiv) {
		mesConDiv.style.width = window.innerWidth + "px";
		mesConDiv.style.height = getMaxHeightByOrientation();
	}
	var expiryTodayIfram = document.getElementById("right_side_investments");
	if (null != expiryTodayIfram) {
		expiryTodayIfram.contentWindow.location.reload()
	}
	firstNameByOrientation();
}

/*
	Prevent default events
*/
function preventTouch(event) {
	event.preventDefault();
}

/*
	Handle on touch event when message alert is available
*/
function touch(event) {
	var touch = event.touches[0];
	var tochedElm = touch.target;
	var msgBtn = false;
	if (null != tochedElm) {
		if (tochedElm.id == 'closeBtn' || tochedElm.id == 'investLoginErrLink') {
			msgBtn = true;
		}
	}
	if (msgBtn) { // close message and remove event handlers
		enableTouch();
	} else {  // disable onTouch
		event.preventDefault();
	}
}

/*
	Disable screen touch
*/
function disableTouch() {
	// touch events
	document.body.setAttribute("ontouchstart", "touch(event);");
	document.body.setAttribute("ontouchmove", "preventTouch(event);");
	document.body.setAttribute("ontouchend", "preventTouch(event);");
	document.body.setAttribute("ontouchcancel", "preventTouch(event);");
	// forms events
	document.body.setAttribute("onmousemove", "preventTouch(event);");
	document.body.setAttribute("onmousedown", "preventTouch(event);");
	document.body.setAttribute("onmouseup", "preventTouch(event);");
	document.body.setAttribute("onclick", "preventTouch(event);");
}

/*
	Enable screen touch
*/
function enableTouch() {
	document.body.removeAttribute("ontouchstart");
	document.body.removeAttribute("ontouchmove");
	document.body.removeAttribute("ontouchend");
	document.body.removeAttribute("ontouchcancel");
	document.body.removeAttribute("onmousemove");
	document.body.removeAttribute("onmousedown");
	document.body.removeAttribute("onmouseup");
	document.body.removeAttribute("onclick");
}

/* Save the current page in the sessionStorage */
function menuNavigate() {
	var currPage = window.location.href;
	sessionStorage.setItem("menuBack", currPage);
	window.location.href = context_path + "/mobile/mainMenu.jsf";
}

/* Save the current page in the sessionStorage */
/*function saveBackPage() {
	var hist = sessionStorage.getItem("historyArr");
	if (null != hist) {  // history session attribute exists
		//alert(hist);
		hist += "," + window.location.href;
		sessionStorage.setItem("historyArr", hist);
	} else { // first time
		sessionStorage.setItem("historyArr", window.location.href);
	}
	alert(sessionStorage.getItem("historyArr"));
}*/

/* Save thelast page from the sessionStorage history */
/*function removePage() {
	var hist = sessionStorage.getItem("historyArr");
	if (null != hist) {
		//alert(hist);
		var lastIdx = hist.lastIndexOf(",");
		//alert("idx:" + lastIdx);
		if (lastIdx > 1) {
			hist = hist.substring(0, lastIdx-1);
		} else {
			hist = null;
		}
		alert(hist);
		sessionStorage.setItem("historyArr", hist);
	} else {
		// do nothing
	}
}*/

/* get the last page that saved */
/*function getLastPage() {
	var hist = sessionStorage.getItem("historyArr");
	if (null != hist) {
		//alert(hist);
		var pages = hist.split(",");
		//alert(pages.length-1);
		var page = pages[pages.length-1];
		alert(page);
		return page;
	} else {
		// do nothing
		return "";
	}
}*/

/* display errors alert
   Note: need to have the following in the page:
   		 <span style="display:none;"><t:messages layout="table" id="errorMessages" replaceIdWithLabel="true" /></span>
 */
function displayErrors() {
	var tableMsg = document.getElementById("errorMessages");
	if (null != tableMsg) {
		var msgs = tableMsg.getElementsByTagName("SPAN");
		var alertMsg = "";
		for (var i = 0; i < msgs.length; i++ ) {
			alertMsg += msgs[i].innerHTML + "\n\n";
		}
		alert(alertMsg);
	}
}

/* return the first error in the page with the correct title
   Note: need to have the following in the page:
   		 <span style="display:none;"><t:messages layout="table" id="errorMessages" replaceIdWithLabel="true" errorClass="error_messages" infoClass="info_messages"/></span>
 */
function returnFirstError() {
	var alertMsg = "";
	var titleMsg = null;
	var tableMsg = document.getElementById("errorMessages");
	if (null != tableMsg) {
		var msgs = tableMsg.getElementsByTagName("SPAN");
		for (var i = 0; i < msgs.length; i++ ) {
			alertMsg = msgs[i].innerHTML;
			var cls = msgs[i].getAttribute("class");  // display title by class (error/info)
			if (null != cls && cls != "") {
				if (cls == "error_messages") {
					titleMsg = errorMsg;
				} else if (cls == "info_messages") {
					titleMsg = infoMsg;
				}
			}
			break;
		}
	}
	if (null != titleMsg) {
	 	return (alertMsg + "|title|" + titleMsg);
	}
	return alertMsg;
}

function displayFirstErrorDiv(title, buttonTxt) {
	setTimeout("displayFirstErrorDivRun()",1500);
}

/*
	display error msg as custom alert
*/
function displayFirstErrorDivRun(title, buttonTxt) {
	var alertMsg = returnFirstError();
	var title = title;
	if (title == null && alertMsg.indexOf("|title|") > -1 ) {  // get title
		var arr = alertMsg.split("|title|");
		alertMsg = arr[0];
		title = arr[1];
	}
	if (title == null) {
		title = "";
	}
	if (null != alertMsg && alertMsg != "") {
		if (isAndroidApp) {
			window.AlertInterface.showAlert(alertMsg);
		} else if (isIPhoneApp && appVer > '1.0') {
			window.location = "alertinterface://" + encodeURI(alertMsg);
		} else {
			window.scrollTo(0, 1);
			customAlert(title, alertMsg, buttonTxt);
		}
	}
}

/*
	display error msg as default alert
*/
function displayFirstError() {
	var alertMsg = returnFirstError();
	if (null != alertMsg && alertMsg != "") {
		alert(alertMsg);
	}
}

/*
	return true if the userAgent is iphone / android
*/
function isMobile() {
	if (isIPhone() || isAndroid()) {
		return true;
	}
	return false;
}

/*
	return true if the userAgent is android
*/
function isAndroid() {
	return (navigator.userAgent.toLowerCase().indexOf("android") > -1);

}

/*
	return true if the userAgent is iPhone / iPod
*/
function isIPhone() {
	return (navigator.userAgent.toLowerCase().indexOf('iphone') > -1 ||
			navigator.userAgent.toLowerCase().indexOf('ipod') > -1);
}

/*
	return true if the userAgent is iPhone / iPod, and the client is in home screen mode (standalone)
*/
function isIPhoneStandalone() {
	return (isIPhone() && (navigator.standalone || isIPhoneApp));
}

/*
	return true if the userAgent is iPhone / iPod, and the client is not in home screen mode
*/
function isIPhoneNotStandalone() {
	return (isIPhone() && !(navigator.standalone || isIPhoneApp));
}

function redirectIPhoneToHomescreen() {
	if (doRediredt && isIPhoneNotStandalone() &&
			window.location.href.indexOf('/index.jsf') == -1 &&
			window.location.href.indexOf('/errorPage.jsf') == -1) {
		window.location = context_path + '/mobile/index.jsf';
	}
}


/*
	Get max document height.
	used when embedding the background div of the custom alert message
*/
function getMaxHeightByOrientation() {
	var height;
	if (document.body.getAttribute("orient") == "portrait") {
		height = 500;
	} else { // landscape
		height = 600;
	}
	return getScrollXY()[1] + height + "px";
}

//obtain the scrolling offsets.
function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}

//show the select box in the mobile
//setting focus in the mobile make the select box to display like u click on it.
//@listName the name of the select box u want to show
function showList(listName) {
	var listSelectL = document.getElementById(listName);
	listSelectL.style.display = "block";
	listSelectL.focus();
	listSelectL.style.display = "none";
}

function firstNameByOrientation() {
	var orientation = Math.abs(currentOrient) == 90 ? "landscape" : "portrait";
	var divNameToDisplay;
	if (orientation == 'landscape') {
		divNameToDisplay = document.getElementById('firstNameLong');
	} else {
		divNameToDisplay = document.getElementById('firstNameShort');
	}
	if (divNameToDisplay != null) {
		document.getElementById('userFirstNameDiv').innerHTML = divNameToDisplay.innerHTML;
	}
}

/*
Open date picker dialog
For android - use application Android date picker
For iphone - user JS
*/
function showDatePicker(isAndroid ,value, lable, year, month, day) {
	if (isAndroid) {
		window.DateInterface.pickDate(value, lable, document.getElementById(lable).innerHTML);
	} else {
		openBirthDate(value, lable, year, month, day);
	}
}