<%@page import="il.co.etrader.web.bl_managers.ApplicationData"%>
<%@page import="il.co.etrader.web.mbeans.PageNames"%>
<%@page import="il.co.etrader.web.util.Constants" %>

<%@page import="il.co.etrader.util.CommonUtil" %>


<%
	String redirectSessionparam2	= (String) session.getAttribute(Constants.FROM_REDIRECT);
	String fromLive 				= (String) request.getSession().getAttribute(Constants.FROM_LIVE);
	String redirectedToReg			= (String) request.getSession().getAttribute(Constants.REDIRECTED_TO_REGISTER);
	String utcOffset				= (String) request.getSession().getAttribute(Constants.UTC_OFFSET);
	String skin						= (String) request.getSession().getAttribute(Constants.SKIN_ID);
	ApplicationData ap = (ApplicationData)application.getAttribute("applicationData");
	ap.updateLogout(session);

	String url = PageNames.getPageNameStatic(ap.getLinkLocalePrefix(session, request, response), "/jsp/index.jsf");
	String prefix = ap.getLinkLocalePrefix(session, request, response);
	System.out.println("fromLive: " + fromLive);
	System.out.println("url: " + url);
	System.out.println("prefix: " + prefix);
	System.out.println("skin: " + skin);
	request.getSession().invalidate();

	if ( null != redirectSessionparam2 && redirectSessionparam2.equals("israel") ) {
		url += "?from=israel";
	}

	if (null != redirectedToReg) {
		request.getSession().setAttribute(Constants.REDIRECTED_TO_REGISTER, redirectedToReg);
	}
	request.getSession().setAttribute(Constants.UTC_OFFSET, utcOffset);
	request.getSession().setAttribute(Constants.SKIN_ID, skin);
	if(fromLive != null && fromLive.equals("pending")) {
		request.getSession().setAttribute(Constants.FROM_LIVE, "true");
	} else {
		request.getSession().setAttribute(Constants.FROM_LIVE, "false");
	}
	
	System.out.println("id: "+request.getSession().getId());
	response.sendRedirect(ap.getSubDomain(prefix) + url);

%>