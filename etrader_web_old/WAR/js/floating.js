$(document).ready(function(){
//alert("q " + $(".floatingLeft").offset({ scroll: false }).left);

	$(window).scroll(function(){
		fixSlipPosition();
		$("#profitLineButton").css("top", $(window).scrollTop()+155);
	});

});

function fixSlipPosition() {
	if (slipCount > 0 && window.screen.width > 904) {

		var fLeft = $(".floatingLeft").offset({ scroll: false }).left;
		if (navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
			fLeft += 1;
		}

		var fTop = $(".invest_header").offset({ scroll: false }).top;
		var fTopStop = $("#stopFloating").offset({ scroll: false }).top - 457;
		if  ($(window).scrollTop() > fTop && $(window).scrollTop() <= fTopStop) {
		   $("#slip").css("position", "absolute");
		   $("#slip").css("top", $(window).scrollTop());
		   $("#slip").css("left", fLeft);
		}

		if ($(window).scrollTop() > fTopStop) {
			$("#slip").css("position", "absolute");
			$("#slip").css("top", fTopStop);
			$("#slip").css("left", fLeft);
		}

		if  ($(window).scrollTop() <= fTop) {
   		   $("#slip").css("position", "absolute");
		   $("#slip").css("top", fTop - 2);
		   $("#slip").css("left", fLeft);
		}
	}
}