
/*
   Timer Clock
   including days, hours, minutes and seconds
*/
function startCountDownHours(display, day, hour, min, sec) {
	var countDownClock = new CountDownClock(display, day, hour, min, sec);
	function CountDownClock(display, day, hour, min, sec) {
		this.display = display;
		this.day = day;
		this.hour = hour;
		this.min = min;
		this.sec = sec;
		CountDownClock.ticker = function() {
			if (countDownClock.sec > 0) {
				countDownClock.sec = countDownClock.sec - 1;
			}  else if (countDownClock.min > 0) {
					countDownClock.min = countDownClock.min - 1;
					countDownClock.sec = 59;
			} else if (countDownClock.hour > 0) {
					countDownClock.hour = countDownClock.hour - 1;
					countDownClock.min = 59;
					countDownClock.sec = 59;
			} else {
				if (countDownClock.day > 0) {
					countDownClock.day = countDownClock.day - 1;
					countDownClock.hour = 23;
					countDownClock.min = 59;
					countDownClock.sec = 59;
				}
			}
			countDownClock.display.innerHTML = padToTwoDig(countDownClock.day) + ":" + padToTwoDig(countDownClock.hour) + ":" + padToTwoDig(countDownClock.min) + ":" + padToTwoDig(countDownClock.sec);
			if (countDownClock.day > 0 || countDownClock.hour > 0 || countDownClock.min > 0 || countDownClock.sec > 0) {
				setTimeout(CountDownClock.ticker, 1000);
			} else {
			    if (window.location.href.indexOf("index.jsf") == -1) {
					var trS = document.getElementById("gameStartedS");
					var trNS = document.getElementById("gameNotStartedS");
					trNS.style.display = 'none';
					trS.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				} else { // index, reload page
					window.location.reload();
				}
			}
		}
	}
	setTimeout(CountDownClock.ticker, 1000);
}

function padToTwoDig(val) {
	if (val < 10) {
		return "0" + val;
	}
	return val;
}