var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
// Handle all the FSCommand messages in a Flash movie.
function graphBanner_DoFSCommand(command, args) {
	var graphBannerObj = isInternetExplorer ? document.all.graphBanner : document.graphBanner;
	//alert(command);
	if(command == 'movie'){
		toggleGraphBanner(false);
	}
}
// Hook for Internet Explorer.
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub graphBanner_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call graphBanner_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');
}

function toggleGraphBanner(flag) {
	var graphInfoDiv = document.getElementById("graphInfoDiv");
	if (flag) {
		graphInfoDiv.style.display = 'inline';
	} else {
		var select_asset = document.getElementById("select_asset");
		if (null != select_asset) {
			select_asset.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}
		var selectMarket = document.getElementById("select_market");
		if (null != selectMarket) {
			selectMarket.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}
		var selectScheduled = document.getElementById("select_time");
		if (null != selectScheduled) {
			selectScheduled.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'inline';
		}
		graphInfoDiv.style.display = 'none';
	}
}

function startBtn() {
	window.location.href = context_path + "/jsp/profitline.jsf";
}

//put the golden div in the right position on the page
//! useing tigra hint!
function positionGraphBannerDiv() {
	var mTd = document.getElementById('messageTd');
	var conDiv = document.getElementById('graphInfoDiv');
	if (null != mTd && null != conDiv) {
		conDiv.style.left = f_getPositionGB(mTd, 'Left')-38 ; //28 = (GMDiv - TradeTable) / 2
		conDiv.style.top = f_getPositionGB(mTd, 'Top') ;
	}
}

function f_getPositionGB (e_elemRef, s_coord) {
//	log("f_getPosition e_elemRef: " + e_elemRef + " s_coord: " + s_coord);

	var n_pos = 0, n_offset,
		e_elem = e_elemRef;

	while (e_elem) {
		n_offset = e_elem["offset" + s_coord];
		n_pos += n_offset;
		e_elem = e_elem.offsetParent;
	}

	// margin correction in some browsers (not supported for GM)
	if (false) {
		n_pos += parseInt(document.body[s_coord.toLowerCase() + 'Margin']);
	}

	e_elem = e_elemRef;
	while (e_elem != document.body) {
		n_offset = e_elem["scroll" + s_coord];
		if (n_offset && e_elem.style.overflow == 'scroll')
			n_pos -= n_offset;
		e_elem = e_elem.parentNode;
	}

//	log("f_getPosition end.");
	return n_pos;
}