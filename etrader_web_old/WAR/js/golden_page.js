var INS_TYPE_GOLDEN = 1;
//if the user see the investments or not
var openTable = false ;
//count how much invest we have
var countGoldInvest = 0;

// the class attribute name. there us diff btw IE (className) and FF (class)
var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';
// in IE 8.0 it is like in FireFox
classAtr = navigator.appVersion.toLowerCase().indexOf("msie 8.0") != -1 ? 'class' : classAtr;

//when chose "select all". mark all the select boxs as selected or unselect
function goldenSelectAll(box) {
	//var tbody = box.parentNode.parentNode.parentNode; //should count number of childes and loop from 2 to length - 1
	for (var i = 1; i < 5; i++) {
		var tr = document.getElementById("investmentInfo" + i);
		if (tr.style.display != 'none') {
			var checkbox = document.getElementById("buyCheckBox" + i);
			checkbox.checked = box.checked;

		}
		sumAllInv();
	}
}

//sending ajax request and recive the answer for buying insurance
var insuranceSubmitted = 1;
function submitInsuranceToServer(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType) {
	var subIns = new SubIns(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType);
	function SubIns(invId, insuranceAmount, winAmount, investmentRowNumber, invInsType) {
		this.invId = invId;
		this.insuranceAmount = insuranceAmount;
		this.winAmount = winAmount;
		this.investmentRowNumber = investmentRowNumber;
		this.invInsType = invInsType;
		SubIns.ticker = function() {
			var xmlHttpIns = getXMLHttp();
			if (null == xmlHttpIns) {
				return false;
			}
			xmlHttpIns.onreadystatechange = function() {
				if (xmlHttpIns.readyState == 4) {
					canSubmitGM = true;
					var winAmountArray = subIns.winAmount.split("_");
					var insuranceAmountArray = subIns.insuranceAmount.split("_");
					var invIdArray = subIns.invId.split("_");
					var rowNumberArray = subIns.investmentRowNumber.split("_");
					var invInsType = subIns.invInsType.split("_");
					var responseTextArray = xmlHttpIns.responseText.split("_");
//					alert("!@# " + responseTextArray);
					for (var i = 0; i < rowNumberArray.length ; i++) {
						if (responseTextArray[i].length > 2) {
							if (responseTextArray[i].substring(0, 2) == "OK") {
								successFromInsurance(rowNumberArray[i], invIdArray[i], winAmountArray[i], responseTextArray[i].substring(2));
							} else if (responseTextArray[i].substring(0, 1) == "1") {
								errorInInsuranceNotLogin(responseTextArray[i].substring(1));
								// refresh the header to see he is not logged in
								document.getElementById('header').contentWindow.location.reload();
								break;
							} else {
							//	errorInSlip(subIns.invId, subIns.winAmount, xmlHttpIns.responseText);
							//	unfreezeInSlip(subIns.invId);
								errorInInsurance(rowNumberArray[i], invIdArray[i], responseTextArray[i]);
							}
						} else {
						//	errorInInsurance(subIns.invId, subIns.winAmount, xmlHttpIns.responseText);
						//	unfreezeInInsurance(subIns.invId);
							errorInInsurance(rowNumberArray[i], invIdArray[i], responseTextArray[i]);
						}
					}
					sumAllInv();
					var imgBuy = document.getElementById("goldenBuy");
					imgBuy.style.display = 'block';
					imgBuy = document.getElementById("goldenBuyWait");
					imgBuy.style.display = 'none';
				}
			}
			xmlHttpIns.open("POST", "ajax.jsf", true);
			xmlHttpIns.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xmlHttpIns.send("command=submitInsurance&invId=" + subIns.invId +
						 	"&insuranceAmount=" + subIns.insuranceAmount +
						 	"&insuranceType=" + subIns.invInsType);
		}
	}
	setTimeout(SubIns.ticker, insuranceSubmitted * 1000);
	insuranceSubmitted++;
}

//submit the insurance when clicking on the buy button
var canSubmitGM = true;
function submitInsurances(buttonBuy) {
	buttonBuy.style.display = 'none';
	var img = getChildOfType(buttonBuy.parentNode, "IMG", 2);
	img.style.display = 'block';
	var checkbox;
	var tr;
	var td;
	var winAmount = "";
	var insuranceAmount = "";
	var invId = "";
	var invInsType = "";
	var rowNumber = "";
	var needToSend = false;
	for (var i = 1; i < 5; i++) {
		checkbox = document.getElementById("buyCheckBox" + i);
		if (checkbox.checked) { //only if check box is selected submit
			tr = document.getElementById("investmentInfo" + i);
			td = getChildOfType(tr, "TD", 7);
			winAmount += "_" + td.innerHTML.substring(1);
			td = getChildOfType(tr, "TD", 12); //was 8
			insuranceAmount += "_" + td.innerHTML;
			td = getChildOfType(tr, "TD", 10);
			invId += "_" + td.innerHTML;
			td = getChildOfType(tr, "TD", 11);
			invInsType += "_" + td.innerHTML;
			rowNumber += "_" + i;
			needToSend = true;
//			alert("id = " + invId.substring(1) + " insAmount = " + insuranceAmount.substring(1) + " winAmount = " + winAmount.substring(1) + " rowNumber = " + rowNumber.substring(1));
		}
	}
	if (needToSend && canSubmitGM) {
		canSubmitGM = false;
		var errorTable = document.getElementById("mainError");
		errorTable.style.display = 'none';
		submitInsuranceToServer(invId.substring(1), insuranceAmount.substring(1), winAmount.substring(1), rowNumber.substring(1), invInsType.substring(1));
	}
	if (!needToSend) {
		var	link = document.getElementById("generalTermsLink");
		if (link.href.indexOf("goldenMinutes") > -1) {
			errorInInsuranceNotLogin(noInvSelectedGM);
		} else {
			errorInInsuranceNotLogin(noInvSelectedRU);
		}
		var imgBuy = document.getElementById("goldenBuy");
		imgBuy.style.display = 'block';
		imgBuy = document.getElementById("goldenBuyWait");
		imgBuy.style.display = 'none';
	}
}


//show the recipt when got confirm from server
function successFromInsurance(investmentRowNumber, invId, winAmount, responseText) {
	//send msg back to LS so he will stop sending updates
	lsEngRef.sendMessage("invId_" + invId);

	//hide the invest info
	var tr = document.getElementById("investmentInfo" + investmentRowNumber);
	tr.style.display = "none";
	var td = getChildOfType(tr, "TD", 10); //INS_id
	td.innerHTML = "";

	td = getChildOfType(tr, "TD", 11); //INS_type (gm/ru)
	var successType = td.innerHTML;

	//hide the invest error msg
	tr = document.getElementById("investmentError" + investmentRowNumber);
	tr.style.display = "none";

	//show the invest success msg
	tr = document.getElementById("investmentSuccess" + investmentRowNumber);
	tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';

	td = getChildOfType(tr, "TD", 2)
	var table = getChildOfType(td, "TABLE", 1);
	var tbody = getChildOfType(table, "TBODY", 1);
	tr = getChildOfType(tbody, "TR", 1);
	td = getChildOfType(tr, "TD", 2);
	if (successType == INS_TYPE_GOLDEN) {
		td.innerHTML = reciptTxt1 + " " + winAmount + " " + reciptTxt2;
	} else {
		td.innerHTML = reciptTxt1_RU;
	}
	td = getChildOfType(tr, "TD", 3);
	td.innerHTML = reciptNumber + " " + invId;

	//if its none visable will not tring to submit it again
	var checkbox = document.getElementById("buyCheckBox" + investmentRowNumber);
	checkbox.checked = false;


	document.getElementById('header').contentWindow.location.reload();

	// remove the investment from the profit line and if its the last 1 close it.
	var PLIframe = document.getElementById("TB_iframeContent");
	if (null != PLIframe) {
		if (PLIframe.contentWindow.removeInvestment(invId)) {
			stopThickBox();
		}
	}
}

//hide the recipt when clicking on the X
function closeGoldenRecipt(button, investmentRowNumber) {
	//hide the success
	var tr = document.getElementById("investmentSuccess" + investmentRowNumber);
	tr.style.display = "none";
	//clean the success
	tr = button.parentNode;
	var td = getChildOfType(tr, "TD", 2);
	td.innerHTML = "";
	td = getChildOfType(tr, "TD", 3);
	td.innerHTML = "";

	//decide if to show the row again (bcoz it have new investment) or to hide the show
	tr = document.getElementById("investmentInfo" + investmentRowNumber);
	td = getChildOfType(tr, "TD", 10); //INS_id
	if (td.innerHTML.length > 0) {
		tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	}
	if (countGoldInvest == 0) {
		toggleDetailsTable(false);
		toggleGMBanner(false);
	}
}

//if not log in and try to submit show the erro msg
function errorInInsuranceNotLogin(responseText) {
//	alert("errorInInsuranceNotLogin responseText: " + responseText);
	var table = document.getElementById("mainError");
	var tbody = getChildOfType(table, "TBODY", 1);
	var tr = getChildOfType(tbody, "TR", 1);
	var td = getChildOfType(tr, "TD", 1);
	td.innerHTML = responseText;
	//show the error msg
	table.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
}

//show error msg in the row that got error
function errorInInsurance(investmentRowNumber, id, responseText) {
//	alert("errorInInsuranceNotLogin responseText: " + responseText);
	var tr = document.getElementById("investmentError" + investmentRowNumber);
	var td = getChildOfType(tr, "TD", 1);
	td.innerHTML = responseText;
	//show the invest error msg
	tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';

}

//when getting update from LS put it in the right row.
function updateItemGM(itemPos, updateInfo) {
	var investmentRowNumber = new Number(updateInfo.getNewValue("INS_POSITION")) + 1;
	var tr;
	var table;
	var cmd = updateInfo.getNewValue("command");
	if ((cmd == "ADD") || (cmd == "UPDATE")) {
		// get display name
		if (cmd == "ADD") {
			if (countGoldInvest == 0) {
				getGoldenTimeLeft(updateInfo.getNewValue("key"), updateInfo.getNewValue("INS_INS_TYPE"));
			}
			countGoldInvest++;
			if (openTable) {
				lsEngRef.sendMessage("viewedInvId_" + updateInfo.getNewValue("key"));
			}
		}
		var market_id = updateInfo.getNewValue("INS_MARKET");
		var name = "";
		var img;
		for (var i = 0; i < marketsDiaplayNameGM.length; i++) {
			if (marketsDiaplayNameGM[i][0] == market_id) {
				name = marketsDiaplayNameGM[i][1];
				break;
			}
		}

		tr = document.getElementById("investmentInfo" + investmentRowNumber);
		var td = getChildOfType(tr, "TD", 2); // market name
		td.innerHTML = name;
		td = getChildOfType(tr, "TD", 3); //INS_LEVEL
		if (updateInfo.getNewValue("INS_TYPE") == 1) {
			img = "ar_up.gif";
		} else {
			img = "ar_down.gif";
		}
		td.innerHTML = updateInfo.getNewValue("INS_LEVEL");

		td = getChildOfType(tr, "TD", 4); //inv type gif

		td.innerHTML = "<img src=\"" + context_pathGM + "/images/" + img + "\" alt=\"\" border=\"0\" />"

		td = getChildOfType(tr, "TD", 5); //INS_CRR_LEVEL

		td.innerHTML = updateInfo.getNewValue("INS_CRR_LEVEL");

		var clrChange = updateInfo.getNewValue("INS_LEVEL_CLR");

		var clrToSet = "invest_level_up";
		if (clrChange == 0) {
			clrToSet = "invest_level_down";
		} else if (clrChange == 1) {
			clrToSet = "invest_level_close";
		}
		td.setAttribute(classAtr, "clrToSet");
//		setClass(td, clrToSet);
		td = getChildOfType(tr, "TD", 6); //INS_AMOUNT
		td.innerHTML = currencySymbol + updateInfo.getNewValue("INS_AMOUNT");
		td = getChildOfType(tr, "TD", 7); //INS_WIN
		td.innerHTML = currencySymbol + updateInfo.getNewValue("INS_WIN");
		td = getChildOfType(tr, "TD", 8); //INS_INS premia amount
		td.innerHTML = currencySymbol + updateInfo.getNewValue("INS_INS");
		td = getChildOfType(tr, "TD", 10); //INS_id
		td.innerHTML = updateInfo.getNewValue("key");
		td = getChildOfType(tr, "TD", 11); //INS_INS_TYPE
		td.innerHTML = updateInfo.getNewValue("INS_INS_TYPE");

		td = getChildOfType(tr, "TD", 12); //ins premia amount not formated
		td.innerHTML = updateInfo.getNewValue("INS_INS").replace(/\,/g,'');

		table = document.getElementById("investmentRow" + investmentRowNumber);
		table.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';

/*		//if there is no recipt open. show this line;
		var trRecipt = document.getElementById("investmentSuccess" + investmentRowNumber);
		if (trRecipt.style.display = "none") {*/
			tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
//		}
	}

	if (cmd == "DELETE") {
		countGoldInvest--;
		var trSuccess = document.getElementById("investmentSuccess" + investmentRowNumber);
		if (countGoldInvest == 0) {
			if (trSuccess.style.display == "none") {
				toggleDetailsTable(false);
				toggleGMBanner(false);
			}
		}
		//clean the tr when u got delete command
		tr = document.getElementById("investmentInfo" + investmentRowNumber);
		tr.style.display = "none";

		//hide the error tr
		tr = document.getElementById("investmentError" + investmentRowNumber);
		tr.style.display = "none";

		td = getChildOfType(tr, "TD", 10); //INS_id
		td.innerHTML = "";
		var checkBox = document.getElementById("buyCheckBox" + investmentRowNumber);
		checkbox.checked = false;
		if (trSuccess.style.display == "none") {
			table = document.getElementById("investmentRow" + investmentRowNumber);
			table.style.display = "none";
		}
	}
}

//sum all the investment that are checked
function sumAllInv() {
		var sum = 0;
		var td;
		var tr
		var checkbox
		for (var i = 1; i < 5; i++) {
			tr = document.getElementById("investmentInfo" + i);
			checkbox = document.getElementById("buyCheckBox" + i);
			if (tr.style.display != 'none' && checkbox.checked) {
				td = getChildOfType(tr, "TD", 12).innerHTML;
				sum += new Number(td);
			}
		}
		td = document.getElementById("sumInv");
		sum = sum.toFixed(2);
		if (sum > 0) {
			td.innerHTML = currencySymbol + sum;
		} else {
			td.innerHTML = "";
		}
}

//add or substract invest prim to sum
function addInvToSum(checkBox, number) {
	var tr = document.getElementById("investmentInfo" + number);
	var tdInvRow = new Number(getChildOfType(tr, "TD", 12).innerHTML);
	var tdSum = document.getElementById("sumInv");
	var tdSumValue = 0;
	if (tdSum.innerHTML.length > 0) {
		tdSumValue = new Number(tdSum.innerHTML.substring(1));
	}
	if (checkBox.checked) {
		tdSumValue += tdInvRow;
	} else {
		tdSumValue -= tdInvRow;
	}
	tdSumValue = tdSumValue.toFixed(2);
	if (tdSumValue > 0) {
		tdSum.innerHTML = currencySymbol + tdSumValue;
	} else {
		tdSum.innerHTML = "";
	}
}

//put the golden div in the right position on the page
//! useing tigra hint!
function positionGoldenDiv() {
	var mTd = document.getElementById('messageTd');
	var conDiv = document.getElementById('goldenDiv');
	if (null != mTd && null != conDiv) {
		conDiv.style.left = f_getPositionGM(mTd, 'Left')-28 ; //28 = (GMDiv - TradeTable) / 2
		conDiv.style.top = f_getPositionGM(mTd, 'Top') ;
	}
}

var timesToGetIns = 1;
//get the time left for the user to buy insurance (called on first update)
//when its got the time its will position the div and show the banner
function getGoldenTimeLeft(invId, insType) {
	var timeGetterIns = new TimeGetterIns(invId, insType);
	function TimeGetterIns(invId, insType) {
		this.invId = invId;
		this.insType = insType;
		this.xmlHttp = null;
		this.ticker = function() {
			log("Tick. invId: " + TimeGetterIns.invId);
			timeGetterIns.xmlHttp = getXMLHttp();
			if (null == timeGetterIns.xmlHttp) {
				return false;
			}
			timeGetterIns.xmlHttp.onreadystatechange = function() {
				if (timeGetterIns.xmlHttp.readyState == 4) {
					setInsuranceClockAndTxt(timeGetterIns.xmlHttp.responseText + "_" + timeGetterIns.insType)
				}
			}
			timeGetterIns.xmlHttp.open("POST", "ajax.jsf", true);
			timeGetterIns.xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			timeGetterIns.xmlHttp.send("opp_time_exp=" + timeGetterIns.invId);
		}
	}
	setTimeout(timeGetterIns.ticker, 1000 * timesToGetIns);
	timesToGetIns++;
}


//when clicking on the img "details" or "close"
function openDetails(tImage) {
	var strHref = window.top.location.href;
	if ((strHref.indexOf("index.jsf") == -1) && (strHref.indexOf("indices.jsf") == -1) && (strHref.indexOf("stocks.jsf") == -1) && (strHref.indexOf("currencies.jsf") == -1) && (strHref.indexOf("commodities.jsf") == -1) && (strHref.indexOf("foreignstocks.jsf") == -1) && (strHref.indexOf("etraderIndices.jsf") == -1)) {
  		window.location.replace(context_pathGM + "/jsp/index.jsf?openDetails=true");
  		return;
  	}
	var image = tImage;
	var type = image.src.substring(image.src.length - 6);
	if (!openTable) {
		image.src = context_pathGM + "/images/x_" + type; //gm.gif";
		toggleDetailsTable(true);
		markInvestmetsViewed();
	} else {
		image.src = context_pathGM + "/images/open_" + type;//gm.gif";
		toggleDetailsTable(false);
	}
}

//hide the details table
//flag = true to show, else hide.
function toggleDetailsTable(flag) {
	var invTr = document.getElementById("goldenInvestmentsTR");
	var table = document.getElementById("goldenInvestmentsTable");
	var tr = document.getElementById("goldenDetails");
	var selectBox = document.getElementById("select_asset");
	var trGapDown = document.getElementById("goldenDownGap");
	if (flag) {
		openTable = true;
		if (selectBox) {
			selectBox.style.visibility = "hidden";
		}
		tr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
		table.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table';
		invTr.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
		trGapDown.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	} else {
		openTable = false;
		tr.style.display = "none";
		table.style.display = "none";
		invTr.style.display = "none";
		trGapDown.style.display = "none";
		if (selectBox) {
			selectBox.style.visibility = "visible";
		}
	}
}

//toggle the banner hide|show
//flag = true show, false hide
function toggleGMBanner(flag) {
	var goldDiv = document.getElementById("goldenDiv");
	if (flag) {
		goldDiv.style.display = 'inline';
		insertToDB();
	} else {
		goldDiv.style.display = 'none';
		clearInsuranceBanner();
	}
}

//position the banner in the right place on the page
function f_getPositionGM (e_elemRef, s_coord) {
//	log("f_getPosition e_elemRef: " + e_elemRef + " s_coord: " + s_coord);

	var n_pos = 0, n_offset,
		e_elem = e_elemRef;

	while (e_elem) {
		n_offset = e_elem["offset" + s_coord];
		n_pos += n_offset;
		e_elem = e_elem.offsetParent;
	}

	// margin correction in some browsers (not supported for GM)
	if (false) {
		n_pos += parseInt(document.body[s_coord.toLowerCase() + 'Margin']);
	}

	e_elem = e_elemRef;
	while (e_elem != document.body) {
		n_offset = e_elem["scroll" + s_coord];
		if (n_offset && e_elem.style.overflow == 'scroll')
			n_pos -= n_offset;
		e_elem = e_elem.parentNode;
	}

//	log("f_getPosition end.");
	return n_pos;
}

//if the ajax call return values mean we need to show the banner
function clockAndTxtCallback(responseXML) {
	responseTxt = responseXML.getElementsByTagName("time")[0].firstChild.nodeValue;
	if (null != responseTxt) {
		setInsuranceClockAndTxt(responseTxt);
	}
}

//check if need to open the banner in nonetradeing pages
function getClockAndTxt() {
	var url = "ajax.jsf?getClockAndTxt=true";
	var ajaxGM = new AJAXInteraction(url, clockAndTxtCallback);
	ajaxGM.doGet();
}

//set the insurance details, images and clock with times and images of RU or GM.
function setInsuranceClockAndTxt(responseTxt) {
	var rez = responseTxt.split("_");
	var id = rez[0];
	var min = rez[1];
	var sec = rez[2];
	var est_close_date = new Date(rez[3]);
	var est_next_close_date = new Date(rez[4]);
	var insType = new Number(rez[5]);
	var s = document.getElementById("goldenTime");
	var titleTxt = document.getElementById("goldenTitle");
	var formatedDate = formatDateGM(est_close_date);
	var formatedNextDate = formatDateGM(est_next_close_date);
	var img;
	var imgPath;
	var	link = document.getElementById("generalTermsLink");
	var linkExtra = link.href.lastIndexOf('#');
	if (linkExtra > -1) {
		link.href = link.href.substring(0,linkExtra);
	}
	if (insType == INS_TYPE_GOLDEN) { //GM
		titleTxt.innerHTML = headerTxt1 + formatedDate.substring(formatedDate.indexOf(" ") + 1) + " " + headerTxt2;
		imgPath = "gm.gif";

		link.href += "#goldenMinutes"
	} else { //RU
		titleTxt.innerHTML = headerTxtRU1 + formatedDate.substring(formatedDate.indexOf(" ") + 1) + " " + headerTxtRU2 + formatedNextDate.substring(formatedNextDate.indexOf(" ") + 1) + headerTxtRU3;
		imgPath = "ru.gif";
		link.href += "#rollUp"
	}
	img = document.getElementById("goldenBuy");
	img.src = context_pathGM + "/images/buy_" + imgPath;
	img = document.getElementById("goldenTitleImg");
	img.src = context_pathGM + "/images/title_" + imgPath;
	img = document.getElementById("goldenOpenImg");
	img.src = context_pathGM + "/images/open_" + imgPath;
	if (min > 0 || sec > 0) {
		if (s) {
			startCountDownGM(s, min, sec);
		}
		timesToGetIns--;
		//now show the banner

		positionGoldenDiv();
		toggleGMBanner(true);
		if (openDetailsFlag) {
			openDetails(img);
		}
	}
}

//format the date to show it good to the user
function formatDateGM(est_date) {
		//est_date = adjustFromUTCToLocal(est_date);
		est_date.setHours(est_date.getHours() + utcOffsetHour);
		est_date.setMinutes(est_date.getMinutes() + utcOffsetMin);

		var currdate = new Date();
		var min = est_date.getMinutes();
		if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			est_date = bundle_msg_todayGM + " " + est_date.getHours() + ":" + min;
		} else {
			if (new Number(est_date.getMinutes()) < new Number("10")) {
				min = "0" + est_date.getMinutes();
			}
			var year = new String(est_date.getFullYear());
			est_date = est_date.getDate() + "/" + (est_date.getMonth()+1) + "/" + year.substring(2) + " " + est_date.getHours() + ":" + min;
		}
		return est_date;
	}

	//gm clock count down
	function startCountDownGM(display, min, sec) {
		var countDown = new CountDown(display, min, sec);
		function CountDown(display, min, sec) {
			this.display = display;
			this.min = min;
			this.sec = sec;
			CountDown.ticker = function() {
				if (countDown.sec > 0) {
					countDown.sec = countDown.sec - 1;
				} else {
					if (countDown.min > 0) {
						countDown.min = countDown.min - 1;
						countDown.sec = 59;
					}
				}
				countDown.display.innerHTML = padToTwoDigGM(countDown.min) + ":" + padToTwoDigGM(countDown.sec);
				if (countDown.min > 0 || countDown.sec > 0) {
					setTimeout(CountDown.ticker, 1000);
				} else {
					toggleDetailsTable(false);
					toggleGMBanner(false);
				}
			}
		}
		setTimeout(CountDown.ticker, 1000);
	}

	function padToTwoDigGM(val) {
		if (val < 10) {
			return "0" + val;
		}
		return val;
	}

	//when time is finish or no more insurance to buy clear the banner
	function clearInsuranceBanner() {
		var tr;
		var trRecipt;
		var table;
		var td;
		for (var i = 1; i < 5; i++) {
			//hide any open investments and delete the investment id;
			tr = document.getElementById("investmentInfo" + i);
			tr.style.display = 'none';
			td = getChildOfType(tr, "TD", 10); //INS_id
			td.innerHTML = "";

			//hide any recipt
			trRecipt = document.getElementById("investmentSuccess" + i);
			trRecipt.style.display = 'none';

			//delete all errors
			tr = document.getElementById("investmentError" + i);
			tr.style.display = 'none';

		}
		table = document.getElementById("mainError");
		tr = getChildOfType(tr, "TR", 1);
		td = getChildOfType(tr, "TD", 1);
		td.innerHTML = "";

		//delete the sum
		td = document.getElementById("sumInv");
		td.innerHTML = "";
	}

	//insert to db when user see the banner first time
	function insertToDB() {
	    var url = "ajax.jsf?insuranceBanner=1";
	    var ajax = new AJAXInteraction(url);
	    ajax.doGet();
	}

	//tell to the LS to mark this investments as viewed
	function markInvestmetsViewed() {
		var invId;
		var tr;
		var td;
		for (var i = 1; i < 5; i++) {
			tr = document.getElementById("investmentInfo" + i);
			td = getChildOfType(tr, "TD", 10);
			invId = td.innerHTML;
			if (invId != "") {
				lsEngRef.sendMessage("viewedInvId_" + invId);
			}
		}
	}