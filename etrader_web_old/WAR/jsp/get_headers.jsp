<%@page import="javax.servlet.http.Cookie" %>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Calendar" %>
<%@page import="org.apache.log4j.*" %>
<%@page import="java.util.ResourceBundle" %>
<%@page import="java.util.MissingResourceException" %>


<html>
	<body>

	<%
		// take serverId
		ResourceBundle messages = ResourceBundle.getBundle("messages");
		String text = "";
		String key = "server.id";
		try {
			if ( null != messages) {
				text = messages.getString(key);
			}
		} catch (MissingResourceException e) {
			text = "?" + key + "?";
		}

		out.println("<h3 align=center><font color=green>Etrader, " + text + " </font></h3>");

		Logger logger = Logger.getLogger("TEST");
		long time = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		logger.info("-------------");
		logger.info(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR));

		out.println("-------------"+"<br/>");
		out.println(cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR)+" ");


		int minutes = cal.get(Calendar.MINUTE);
		String minutesString = null;
		if (minutes < 10)
		{
			minutesString = "0" + minutes;
		}
		else
		{
			minutesString = String.valueOf(minutes);
		}
		int seconds = cal.get(Calendar.SECOND);
		String secondsString = null;
		if (seconds < 10)
		{
			secondsString = "0" + seconds;
		}
		else
		{
			secondsString = String.valueOf(seconds);
		}
		//out.println("<br/>");
		logger.info(" - " + cal.get(Calendar.HOUR_OF_DAY) + ":" + minutesString + ":" + secondsString + "." + cal.get(Calendar.MILLISECOND));
		out.println(" - " + cal.get(Calendar.HOUR_OF_DAY) + ":" + minutesString + ":" + secondsString + "." + cal.get(Calendar.MILLISECOND)+"<br/>");

		out.println("<br/>-------------<br/>");
		out.println("Header:<br/>");
		Enumeration e = request.getHeaderNames();
		while (e.hasMoreElements())
		{
			String header = (String)e.nextElement();
			logger.info(header + ": " + request.getHeader(header));
			out.println(header + ": " + request.getHeader(header)+"<br/>");

		}
		Cookie[] cookies = request.getCookies();

		out.println("<br/>");
		logger.info("Cookies: ");
		out.println("<br/>-------------<br/>");
		out.println("Cookies:<br/>");

		for (int i=0; i < cookies.length; i++)
		{
			logger.info(cookies[i].getName() + ": " + cookies[i].getValue());
			out.println(cookies[i].getName() + ": " + cookies[i].getValue()+"<br/>");

		}


		out.println("<br/>");
		out.println("<br/>-------------<br/>");
		out.println("Request: <br/>");
		out.println("request.getAuthType():" + request.getAuthType()+"<br/>");
		out.println("request.getLocalAddr():" + request.getLocalAddr()+"<br/>");
 		out.println("request.getLocalPort():" + request.getLocalPort()+"<br/>");
 		out.println("request.getProtocol():" + request.getProtocol()+"<br/>");
		out.println("request.getRemoteAddr():" + request.getRemoteAddr()+"<br/>");
 		out.println("request.getRemoteHost():" + request.getRemoteHost()+"<br/>");
 		out.println("request.getRemotePort():" + request.getRemotePort()+"<br/>");
 		out.println("request.getScheme():" + request.getScheme()+"<br/>");
		out.println("request.getServerName():" + request.getServerName()+"<br/>");
 		out.println("request.getServerPort():" + request.getServerPort()+"<br/>");
 		out.println("request.getPathInfo():" + request.getPathInfo()+"<br/>");
 		out.println("request.getRemoteUser():" + request.getRemoteUser()+"<br/>");
		out.println("request.getRequestURL():" + request.getRequestURL()+"<br/>");
 		out.println("request.getRequestURI():" + request.getRequestURI()+"<br/>");





	%>

	</body>
</html>