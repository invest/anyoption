<%@page import="il.co.etrader.web.bl_managers.ApplicationData"%>
<%
	ApplicationData ap = (ApplicationData)application.getAttribute("applicationData");
	ap.updateLogout(session);
	request.getSession().invalidate();
	response.sendRedirect(request.getContextPath() + "/jsp/index.jsf");
%>