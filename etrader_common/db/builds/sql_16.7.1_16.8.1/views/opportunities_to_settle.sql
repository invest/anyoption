
create or replace view opportunities_to_settle 
as
select id, time_est_closing, opportunity_type_id, market_id, closing_level, closing_level_decimal_point
from   (select a.id
              ,to_char(a.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') time_est_closing
              ,a.opportunity_type_id
              ,a.market_id
              ,a.closing_level
              ,(m.decimal_point - m.decimal_point_subtract_digits) closing_level_decimal_point
              ,(select count(*)
                from   investments b
                where  b.opportunity_id = a.id
                and    b.is_settled = 0
                and    b.is_canceled = 0
                and    b.is_void = 0
                and    rownum <= 1) has_investments
        from   opportunities a
        join   markets m
        on     m.id = a.market_id
        where  a.is_settled = 0
        and    a.time_act_closing is not null)
where  has_investments = 0
order  by time_est_closing;
