create or replace package transactions_manager is

  function velocity_check(p_usr_id number) return number;

  function is_cc_forward_documents(p_usr_id number) return number;

  function velociy_check_is_active(p_usr_id number) return number;
  
  procedure maintenance_fee(p_user_id in number default null);
  
  function maintenance_fee_issue_64(p_user_id in number) return number;
  
  function maintenance_fee_issue_65(p_user_id in number) return number;
  
  function is_black_list_bin(p_bin_id number) return number;
  
end transactions_manager;
/
create or replace package body transactions_manager is

FUNCTION VELOCITY_CHECK(p_usr_id number)
  return number is

CHECK_HOURS CONSTANT NUMBER :=6;
HAVE_DEPOSIT_DOCUMENTS CONSTANT NUMBER :=1;
ET_CUSTOMER CONSTANT NUMBER :=1;

AO_NO_DOC_DEPOSITS NUMBER :=1;
AO_HAVE_DOC_DEPOSITS NUMBER :=2;
ET_NO_DOC_DEPOSITS NUMBER :=3;
ET_HAVE_DOC_DEPOSITS NUMBER :=4;

v_deposits_num number:=-1;
v_id_doc_verify number :=-1;
v_skin_id number :=-1;

cursor c_get_data is
select count(*) deposits_num , u.skin_id
from
transactions tr,
users u
where
tr.user_id = u.id
and tr.type_id = 1
and tr.status_id in (2,7)
and  ((sysdate - tr.time_created)*24)<= CHECK_HOURS
and u.id=p_usr_id
group by id_doc_verify, u.skin_id;

cursor c_get_data_after_activate(pi_actv_date date) is
select count(*) deposits_num , u.skin_id
from
transactions tr,
users u
where
tr.user_id = u.id
and tr.type_id = 1
and tr.status_id in (2,7)
and  ((sysdate - tr.time_created)*24)<= CHECK_HOURS
and pi_actv_date <=tr.time_created
and u.id=p_usr_id
group by id_doc_verify, u.skin_id;

v_resault number :=0;


cursor c_check_rec_vc is
select vc.issue_actions_id,vc.is_active, vc.time_created
from
velocity_check vc
where vc.user_id = p_usr_id
order by vc.id desc;

v_check_rec_vc number:=0;
vc_is_active number :=-1;
v_activate_date date :=sysdate;

BEGIN

IF VELOCIY_CHECK_IS_ACTIVE(p_usr_id) = 1 THEN



open c_check_rec_vc;
fetch c_check_rec_vc into v_check_rec_vc,vc_is_active,v_activate_date;
close c_check_rec_vc;

if (vc_is_active>0) then
 open c_get_data_after_activate(v_activate_date);
fetch c_get_data_after_activate into v_deposits_num,v_skin_id;
close c_get_data_after_activate; 
 else 
   open c_get_data;
fetch c_get_data into v_deposits_num,v_skin_id;
close c_get_data;

end if;

if v_deposits_num in (20) or (v_check_rec_vc>0 and v_deposits_num>=20)  then
v_id_doc_verify:=IS_CC_FORWARD_DOCUMENTS(p_usr_id);
end if;

IF (v_skin_id!=ET_CUSTOMER and v_id_doc_verify != HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1  and v_deposits_num >= 20) THEN

   v_resault :=AO_NO_DOC_DEPOSITS;

ELSIF (v_skin_id!=ET_CUSTOMER and v_id_doc_verify = HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=AO_HAVE_DOC_DEPOSITS;

ELSIF (v_skin_id = ET_CUSTOMER and v_id_doc_verify != HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=ET_NO_DOC_DEPOSITS;

ELSIF (v_skin_id = ET_CUSTOMER and v_id_doc_verify = HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=ET_HAVE_DOC_DEPOSITS;
ELSE
     v_resault:=0;

END IF;

END IF;
return v_resault;

END;
-----------------------------------------------------------------------------
--
-----------------------------------------------------------------------------

FUNCTION IS_CC_FORWARD_DOCUMENTS(p_usr_id number)
  return number is

cursor c_get_sent_cc_copy is
select sum(cop.CC_COPY)
from (
select case when nvl(sent_cc_copy.cc_id,-1) = -1 then 1 else 0 end as CC_COPY
from
(select
distinct tr.credit_card_id, tr.user_id
from
transactions tr
where
 tr.type_id = 1
and tr.status_id in (2,7)
and months_between(sysdate,tr.time_created)<=6
and tr.user_id=p_usr_id) past_cc,
(select fl.cc_id,fl.user_id
from files fl
where
fl.is_approved=1
and fl.file_status_id =3
and fl.file_type_id in (4,23)
and fl.user_id=p_usr_id) sent_cc_copy
where
past_cc.credit_card_id=sent_cc_copy.cc_id(+)
and past_cc.user_id=sent_cc_copy.user_id(+)
) cop;

v_sent_cc_copy NUMBER:=0;

--
cursor c_get_user_sent_id is
select f.id
  from files f
 where f.IS_APPROVED = 1
   and f.FILE_STATUS_ID = 3
   and f.FILE_TYPE_ID in (1,2,21)
   and f.user_id = p_usr_id;

v_user_sent_id NUMBER:=0;


BEGIN
  open c_get_sent_cc_copy;
  fetch c_get_sent_cc_copy into v_sent_cc_copy;
  close c_get_sent_cc_copy;

  open c_get_user_sent_id;
  fetch c_get_user_sent_id into v_user_sent_id;
  close c_get_user_sent_id;


   if v_sent_cc_copy = 0 and v_user_sent_id > 0 then
       return 1;
    else
       return 0;
    end if;
END;

-----------------------------------------------------------------------------
--
-----------------------------------------------------------------------------

FUNCTION VELOCIY_CHECK_IS_ACTIVE(p_usr_id number)
  return number is

  cursor c_get_vel_ch is
  select vc.is_active
    from velocity_check vc
   where vc.user_id = p_usr_id
   order by vc.id desc;

   res number:= 1;

  BEGIN
    open c_get_vel_ch;
    fetch c_get_vel_ch  into res;
    close c_get_vel_ch;
    return res;
  END;
  
  procedure maintenance_fee(p_user_id in number default null) is
    l_issue_check date;
    l_bonus_check number;
    l_sysdate     date := sysdate;
    l_trn         transactions%rowtype;
    l_balance     number;
  begin
  
    for i in (select m.id, m.amount, m.tax_balance, m.currency_id, m.utc_offset
              from   m_fee m
              where  (p_user_id is null or m.id = p_user_id)
              and    not exists (select 1
                      from   investments a
                      where  a.time_created >= add_months(l_sysdate, -6)
                      and    a.user_id = m.id)
              and    not exists (select 1
                      from   transactions t
                      where  t.time_created >= add_months(l_sysdate, -1)
                      and    t.user_id = m.id
                      and    t.type_id = 47)
              and    not exists (select 1
                      from   transactions t
                      join   transaction_types tt
                      on     tt.id = t.type_id
                      where  t.user_id = m.id
                      and    ((tt.class_type = 2 and t.status_id = 4) or
                            (tt.class_type = 1 and t.status_id in (2, 7) and t.time_created > l_sysdate - 7))))
    loop
    
      select issue65, bonus_cnt
      into   l_issue_check, l_bonus_check
      from   (select max(s.time_created) issue65
              from   issues s
              where  s.user_id = i.id
              and    s.subject_id = 65)
            ,(select count(*) bonus_cnt
             from   bonus_users b
             where  b.bonus_state_id in (1, 2, 3)
             and    b.user_id = i.id
             and    b.time_created >= add_months(l_sysdate, -6));
    
      if l_bonus_check = 0
      then
      
        if l_issue_check < l_sysdate - 7
        then
        
          l_trn.id                     := seq_transactions.nextval;
          l_trn.user_id                := i.id;
          l_trn.type_id                := 47;
          l_trn.time_created           := l_sysdate;
          l_trn.status_id              := 2;
          l_trn.description            := 'transactions.maintenance.fee';
          l_trn.writer_id              := 0;
          l_trn.ip                     := 'IP NOT FOUND!';
          l_trn.time_settled           := l_sysdate;
          l_trn.comments               := 'Maintenance fee';
          l_trn.processed_writer_id    := 0;
          l_trn.fee_cancel             := 0;
          l_trn.is_accounting_approved := 0;
          l_trn.utc_offset_settled     := 'GMT+02:00';
          l_trn.rate                   := trunc(convert_amount_to_usd(1, i.currency_id, l_sysdate), 5);
          l_trn.clearing_provider_id   := 0;
          l_trn.deposit_reference_id   := 0;
          l_trn.splitted_reference_id  := 0;
          l_trn.payment_type_id        := 0;
        
          select balance into l_balance from users where id = i.id for update;
        
          if i.amount > l_balance
          then
            l_trn.amount := l_balance;
          else 
            l_trn.amount := i.amount;
          end if;
        
          update users u set u.balance = u.balance - l_trn.amount where u.id = i.id;
        
          insert into transactions
            (id
            ,user_id
            ,type_id
            ,time_created
            ,amount
            ,status_id
            ,description
            ,writer_id
            ,ip
            ,time_settled
            ,comments
            ,processed_writer_id
            ,fee_cancel
            ,is_accounting_approved
            ,utc_offset_settled
            ,rate
            ,clearing_provider_id
            ,deposit_reference_id
            ,splitted_reference_id
            ,payment_type_id)
          values
            (l_trn.id
            ,l_trn.user_id
            ,l_trn.type_id
            ,l_trn.time_created
            ,l_trn.amount
            ,l_trn.status_id
            ,l_trn.description
            ,l_trn.writer_id
            ,l_trn.ip
            ,l_trn.time_settled
            ,l_trn.comments
            ,l_trn.processed_writer_id
            ,l_trn.fee_cancel
            ,l_trn.is_accounting_approved
            ,l_trn.utc_offset_settled
            ,l_trn.rate
            ,l_trn.clearing_provider_id
            ,l_trn.deposit_reference_id
            ,l_trn.splitted_reference_id
            ,l_trn.payment_type_id);
        
          insert into balance_history
            (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
          values
            (seq_balance_history.nextval
            ,i.id
            ,l_balance - l_trn.amount
            ,i.tax_balance
            ,0
            ,l_trn.time_created
            ,'transactions'
            ,l_trn.id
            ,'45'
            ,i.utc_offset);
        
          commit;
        
        end if;
      
      end if;
    end loop;
  
  end maintenance_fee;

  function maintenance_fee_issue_64(p_user_id in number) return number as
    l_flag          number;
    l_sysdate       date := sysdate;
    l_balance_check date;
  begin
    for i in (select max(issue_time) issue_time, max(inv_time) inv_time, max(lastfee_time) lastfee_time
              from   (select null issue_time, max(a.time_created) inv_time, null lastfee_time -- time of the last investment (activity)
                      from   investments a
                      where  a.user_id = p_user_id
                      union all
                      select max(s.time_created), null, null -- time of the last issue 64
                      from   issues s
                      where  s.user_id = p_user_id
                      and    s.subject_id = 64
                      union all
                      select null, null, max(t.time_created) -- time when the last fee was collected
                      from   transactions t
                      where  t.user_id = p_user_id
                      and    t.type_id = 47))
    loop
    
      if i.inv_time is null
         and i.issue_time is null
      then
        select min(time_created) into l_balance_check from balance_history bh where bh.user_id = p_user_id;
      
        if l_balance_check < add_months(l_sysdate, -5)
        then
          l_flag := 1;
        else
          l_flag := 0;
        end if;
      
      elsif i.lastfee_time is null
      then
      
        if i.inv_time is null
           and i.issue_time < add_months(l_sysdate, -6)
        then
          l_flag := 1;
        elsif i.inv_time < add_months(l_sysdate, -5)
              and i.issue_time is null
        then
          l_flag := 1;
        elsif i.inv_time > i.issue_time
              and i.inv_time < add_months(l_sysdate, -5)
        then
          l_flag := 1;
        elsif i.inv_time < i.issue_time
              and i.issue_time < add_months(l_sysdate, -6)
        then
          l_flag := 1;
        else
          l_flag := 0;
        end if;
      
      else
      
        if i.inv_time > i.lastfee_time
           and i.inv_time < add_months(l_sysdate, -5)
           and (i.issue_time is null or (i.issue_time < i.inv_time and i.issue_time < add_months(l_sysdate, -6)))
        then
          l_flag := 1;
        else
          l_flag := 0;
        end if;
      
      end if;
    
    end loop;
  
    return l_flag;
  end maintenance_fee_issue_64;

  function maintenance_fee_issue_65(p_user_id in number) return number as
    l_flag      number;
    l_sysdate   date := sysdate;
  begin
    for i in (select max(issue64) issue64, max(issue65) issue65, max(inv_time) inv_time, max(lastfee_time) lastfee_time
              from   (select null issue64, null issue65, max(a.time_created) inv_time, null lastfee_time -- time of the last investment (activity)
                      from   investments a
                      where  a.user_id = p_user_id
                      union all
                      select max(case
                                   when s.subject_id = 64 then
                                    s.time_created
                                 end)
                            ,max(case
                                   when s.subject_id = 65 then
                                    s.time_created
                                 end)
                            ,null
                            ,null -- time of the last issue 64 and 65
                      from   issues s
                      where  s.user_id = p_user_id
                      and    s.subject_id in (64, 65)
                      union all
                      select null, null, null, max(t.time_created) -- time when the last fee was collected
                      from   transactions t
                      where  t.user_id = p_user_id
                      and    t.type_id = 47))
    loop
    
      if i.lastfee_time is null
      then
        if (i.issue64 > i.issue65 or i.issue65 is null)
           and (i.issue65 < add_months(l_sysdate, -6) or i.issue65 is null)
           and i.issue64 between add_months(l_sysdate, -5) and add_months(l_sysdate, -1) + 7
        then
          l_flag := 1;
        else
          l_flag := 0;
        end if;
      else
        if i.lastfee_time < add_months(l_sysdate, -1) + 7
           and (i.issue65 is null or i.issue65 < i.lastfee_time)
           and i.issue64 is not null
           and (i.inv_time is null or i.inv_time < i.lastfee_time)
        then
          l_flag := 1;
        else
          l_flag := 0;
        end if;
      end if;
    end loop;
  
    return l_flag;
  end maintenance_fee_issue_65;

  -- return 0 - not black listed or 1 - black listed
  function is_black_list_bin(p_bin_id number) return number is
    lres number;
  begin
  
    select count(*)
    into   lres
    from   bins
    where  (country_id in (22, 220) -- Belgium, USA
      or bank = 'CANADIAN IMPERIAL BANK OF COMMERCE')
    and    from_bin = p_bin_id
    and    rownum <= 1;
  
    return lres;
  end is_black_list_bin;

end transactions_manager;
/
