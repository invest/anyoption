create or replace package pkg_credit_card is

  -- Author  : PAVEL.TABAKOV
  -- Created : 23-Aug-16 09:54:22
  -- Purpose : CreditCardsDAO

  procedure get_cc_holder_name_hist
  (
    o_cc_holder_name_hist out sys_refcursor
   ,i_user_id             in number
   ,i_cc_number           in varchar2
  );

end pkg_credit_card;
/
create or replace package body pkg_credit_card is

  procedure get_cc_holder_name_hist
  (
    o_cc_holder_name_hist out sys_refcursor
   ,i_user_id             in number
   ,i_cc_number           in varchar2
  ) is
  begin
    open o_cc_holder_name_hist for
      select chh.holder_name, chh.time_created, chh.operation_id
      from   card_holder_history chh
      where  chh.user_id = i_user_id
      and    chh.operation_id = 1
      and    chh.cc_number = i_cc_number;
  
  end get_cc_holder_name_hist;
  
end pkg_credit_card;
/
