create or replace package pkg_promotion_management is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-07 15:18:51
  -- Purpose : 

  function get_slider_images
  (
    i_slider_id   in number
   ,i_language_id in number
  ) return sys_refcursor;

  procedure get_all_banners(o_banners out sys_refcursor);

  procedure get_banner_sliders_by_place
  (
    o_data         out sys_refcursor
   ,i_banner_place in varchar2
   ,i_language_id  in number
  );

  procedure get_all_slider_link_types(o_linktypes out sys_refcursor);

  procedure get_banner_sliders
  (
    o_data      out sys_refcursor
   ,i_banner_id in number
   ,i_is_active in number
  );

  procedure get_slider_images
  (
    o_data        out sys_refcursor
   ,i_slider_id   in number
   ,i_language_id in number default null
  );

  procedure create_banner_slider
  (
    o_slider_id    out number
   ,i_banner_id    in number
   ,i_writer_id    in number
   ,i_link_type_id in number
  );

  procedure update_slider_image
  (
    i_slider_image_id in number
   ,i_link_url        in varchar2
   ,i_image_name      in varchar2
   ,i_writer_id       in number
  );

  procedure update_slider_type
  (
    i_banner_slider_id in number
   ,i_link_type_id     in number
  );

  procedure create_banner_slider_image
  (
    i_banner_slider_id in number
   ,i_language_id      in number
   ,i_link_url         in varchar2
   ,i_image_name       in varchar2
   ,i_writer_id        in number
  );

  procedure enable_banner_slider(i_slider_id in number);

  procedure disable_banner_slider(i_slider_id in number);

  procedure delete_banner_slider(i_slider_id in number);

  procedure change_slider_pos
  (
    i_slider_id    in number
   ,i_new_position in number
   ,i_writer_id    in number
  );
end pkg_promotion_management;
/
create or replace package body pkg_promotion_management is

  procedure get_all_banners(o_banners out sys_refcursor) is
  begin
    open o_banners for
      select id, name, width_pixels, height_pixels from banners;
  end get_all_banners;

  procedure get_all_slider_link_types(o_linktypes out sys_refcursor) is
  begin
    open o_linktypes for
      select id, name from banner_slider_link_types;
  end get_all_slider_link_types;

  procedure get_banner_sliders
  (
    o_data      out sys_refcursor
   ,i_banner_id in number
   ,i_is_active in number
  ) is
  begin
    open o_data for
      select s.id slider_id, s.position, s.is_active, si.link_url, si.image_name, si.language_id, s.link_type_id
      from   banner_sliders s
      left   join banner_slider_images si
      on     si.banner_slider_id = s.id
      and    si.language_id = 2
      where  s.banner_id = i_banner_id
      and    s.is_deleted = 0
      and    (i_is_active is null or s.is_active = i_is_active)
      order  by case
                  when s.is_active = 0 then
                   1
                  else
                   0
                end
               ,s.position
               ,s.id;
  
  end get_banner_sliders;

  procedure get_banner_sliders_by_place
  (
    o_data         out sys_refcursor
   ,i_banner_place in varchar2
   ,i_language_id  in number
  ) is
  begin
    open o_data for
      select bs.id, bs.position, bs.link_type_id, get_slider_images(bs.id, i_language_id) sl_images
      from   banner_sliders bs
      join   banners b
      on     b.id = bs.banner_id
      where  b.banner_place = i_banner_place
      and    bs.is_active = 1
      order  by bs.position;
  end get_banner_sliders_by_place;

  procedure get_slider_images
  (
    o_data        out sys_refcursor
   ,i_slider_id   in number
   ,i_language_id in number default null
  ) is
  begin
    o_data := get_slider_images(i_slider_id, i_language_id);
  end get_slider_images;

  function get_slider_images
  (
    i_slider_id   in number
   ,i_language_id in number
  ) return sys_refcursor is
    l_out sys_refcursor;
  begin
    open l_out for
      select si.id, si.link_url, si.image_name, si.language_id, s.link_type_id, s.banner_id
      from   banner_sliders s
      left   join banner_slider_images si
      on     si.banner_slider_id = s.id
      and    (i_language_id is null or si.language_id = i_language_id)
      where  s.id = i_slider_id
      and    s.is_deleted = 0
      order  by si.id;
  
    return l_out;
  end get_slider_images;

  procedure update_slider_image
  (
    i_slider_image_id in number
   ,i_link_url        in varchar2
   ,i_image_name      in varchar2
   ,i_writer_id       in number
  ) is
  begin
    update banner_slider_images
    set    link_url = i_link_url, image_name = i_image_name, writer_id = i_writer_id, time_updated = sysdate
    where  id = i_slider_image_id;
  end update_slider_image;

  procedure update_slider_type
  (
    i_banner_slider_id in number
   ,i_link_type_id     in number
  ) is
  begin
    update banner_sliders set link_type_id = i_link_type_id, time_updated = sysdate where id = i_banner_slider_id;
  end update_slider_type;

  procedure create_banner_slider
  (
    o_slider_id    out number
   ,i_banner_id    in number
   ,i_writer_id    in number
   ,i_link_type_id in number
  ) is
  begin
    insert into banner_sliders
      (id, banner_id, position, is_active, time_created, time_updated, writer_id, link_type_id, is_deleted)
    values
      (seq_banners_conf.nextval
      ,i_banner_id
      ,(select nvl(max(position), 0) + 1 from banner_sliders)
      ,1
      ,sysdate
      ,null
      ,i_writer_id
      ,i_link_type_id
      ,0)
    returning id into o_slider_id;
  end create_banner_slider;

  procedure create_banner_slider_image
  (
    i_banner_slider_id in number
   ,i_language_id      in number
   ,i_link_url         in varchar2
   ,i_image_name       in varchar2
   ,i_writer_id        in number
  ) is
  begin
    insert into banner_slider_images
      (id, banner_slider_id, language_id, link_url, image_name, time_created, time_updated, writer_id)
    values
      (seq_banners_conf.nextval, i_banner_slider_id, i_language_id, i_link_url, i_image_name, sysdate, null, i_writer_id);
  end create_banner_slider_image;

  procedure enable_banner_slider(i_slider_id in number) is
  begin
    update banner_sliders
    set    position    =
           (select max(position) + 1 from banner_sliders)
          ,is_active    = 1
          ,time_updated = sysdate
    where  id = i_slider_id;
  end enable_banner_slider;

  procedure disable_banner_slider(i_slider_id in number) is
  begin
    update banner_sliders set position = 0, is_active = 0, time_updated = sysdate where id = i_slider_id;
  end disable_banner_slider;

  procedure delete_banner_slider(i_slider_id in number) is
  begin
    update banner_sliders set position = 0, is_active = 0, is_deleted = 1, time_updated = sysdate where id = i_slider_id;
  end delete_banner_slider;

  procedure change_slider_pos
  (
    i_slider_id    in number
   ,i_new_position in number
   ,i_writer_id    in number
  ) is
  begin
    update banner_sliders
    set    position = i_new_position, writer_id = i_writer_id, time_updated = sysdate
    where  is_active = 1
    and    is_deleted = 0
    and    id = i_slider_id;
  end change_slider_pos;

end pkg_promotion_management;
/
