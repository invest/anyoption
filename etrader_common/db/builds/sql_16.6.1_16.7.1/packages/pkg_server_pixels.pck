create or replace package pkg_server_pixels is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-26 09:55:51
  -- Purpose : 

  procedure get_deposit_info
  (
    o_deposits  out sys_refcursor
   ,i_from_date in date
   ,i_to_date   in date
  );

  procedure get_register_info
  (
    o_register  out sys_refcursor
   ,i_from_date in date
   ,i_to_date   in date
  );

end pkg_server_pixels;
/
create or replace package body pkg_server_pixels is

  --grant execute on pkg_server_pixels to etrader_web_connect;
  --create or replace public synonym pkg_server_pixels for pkg_server_pixels;

  procedure get_deposit_info
  (
    o_deposits  out sys_refcursor
   ,i_from_date in date
   ,i_to_date   in date
  ) is
  begin
  
    open o_deposits for
      select u.id, u.platform_id, u.first_deposit_id as tran_id, duis.idfa, duis.advertising_id
      from   users u
      join   device_unique_ids_skin duis --- this join will cause duplicates sometimes as duis.device_unique_id is not unique
      on     u.device_unique_id = duis.device_unique_id
      join   marketing_combinations m_comb
      on     u.combination_id = m_comb.id
      join   marketing_campaigns m_camp
      on     m_comb.campaign_id = m_camp.id
      join   marketing_sources ms
      on     m_camp.source_id = ms.id
      where  ms.name = 'Adquant'
      and    exists
       (select 1
              from   transactions t
              join   transaction_types tt
              on     t.type_id = tt.id
              where  tt.class_type = 1
              and    ((t.time_created >= i_from_date - 1 / 48 and t.time_created < i_to_date - 1 / 48 and t.status_id in (7, 8)) or
                    ((t.time_settled >= i_from_date - 1 / 48 and t.time_settled < i_to_date - 1 / 48 and t.status_id = 2)))
              and    t.id = u.first_deposit_id);
  
  end get_deposit_info;

  procedure get_register_info
  (
    o_register  out sys_refcursor
   ,i_from_date in date
   ,i_to_date   in date
  ) is
  begin
  
    open o_register for
      select u.id, u.platform_id, duis.idfa, duis.advertising_id
      from   users u
      join   device_unique_ids_skin duis
      on     u.device_unique_id = duis.device_unique_id
      join   marketing_combinations m_comb
      on     u.combination_id = m_comb.id
      join   marketing_campaigns m_camp
      on     m_comb.campaign_id = m_camp.id
      join   marketing_sources ms
      on     m_camp.source_id = ms.id
      where  u.time_created >= i_from_date - 1 / 48
      and    u.time_created < i_to_date - 1 / 48
      and    ms.name = 'Adquant';
  
  end get_register_info;

end pkg_server_pixels;
/
