CREATE OR REPLACE PACKAGE "WRITERS_COMMISSION" is
  
  FUNCTION DEPOSIT_HANDLER(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER)
    RETURN NUMBER;
    
  FUNCTION DEPOSIT_HANDLER_CHECK(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN NUMBER;

  FUNCTION IS_EXISTS_IN_WCD(P_transaction_id NUMBER, P_is_valid NUMBER)
    RETURN NUMBER;

  FUNCTION UPDATE_DB_PARAMETERS(P_db_parameter_id NUMBER, P_start_time DATE)
    RETURN NUMBER;

  FUNCTION GET_DATE_FROM_DB_PARAMETERS(P_db_parameter_id NUMBER)
    RETURN DATE;
    
  FUNCTION GET_MAX_DEP_DURING_CALL_DAYS
    RETURN NUMBER;
    
  function get_last_assigned_writer
  (
    p_population_users_id in number
   ,p_to_date             in date
   ,p_writer_id           in number
  ) return peh_nt;
    
  function get_commission_deposits
  (
    p_last_run                     in date
   ,p_start_time                   in date
   ,p_max_deposit_during_call_days in number
   ,p_transaction_id               in number
  ) return sys_refcursor;
    
  FUNCTION WITHDRAW_HANDLER(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER)
    RETURN NUMBER;
    
  FUNCTION WITHDRAW_HANDLER_CHECK(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN NUMBER;
    
  FUNCTION IS_EXISTS_WCD_IN_TIMEFRAME(P_user_id NUMBER, P_writer_id NUMBER, P_to_date DATE)
    RETURN NUMBER;
    
  FUNCTION GET_LAST_WCD(P_user_id NUMBER, P_to_date DATE, P_writer_id NUMBER)
    RETURN SYS_REFCURSOR;
    
  FUNCTION GET_FIRST_ASSIGNED_UNASSIGNED(P_population_users_id NUMBER, P_from_date DATE, P_to_date DATE)
    RETURN peh_nt;
    
  FUNCTION GET_COMMISSION_WITHDRAWALS(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN SYS_REFCURSOR;
    
  FUNCTION IS_EXISTS_DEPOSIT(P_user_id NUMBER, P_from_date DATE, P_to_date DATE)
    RETURN NUMBER;
    
  FUNCTION GET_NET_DEPOSITS(P_user_id NUMBER, P_writer_id NUMBER, P_to_date DATE, P_trans_amount NUMBER)
    RETURN NUMBER;
  
End WRITERS_COMMISSION;
/
CREATE OR REPLACE PACKAGE BODY "WRITERS_COMMISSION" is
  
  FUNCTION DEPOSIT_HANDLER(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_count_commission_deposits NUMBER := 0;
    F_cursor SYS_REFCURSOR;
    F_info_tran_id transactions.id %TYPE;
    F_info_user_id transactions.user_id %TYPE;
    F_info_assign_writer_id NUMBER;
    F_transaction_id NUMBER := 0;
    F_last_issue_action_id number;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('DEPOSIT_HANDLER START');
    F_cursor := WRITERS_COMMISSION.GET_COMMISSION_DEPOSITS(P_last_run, P_start_time, P_max_deposit_during_call_days, F_transaction_id);
    
    -- Loop through the resulting data
    IF (F_cursor IS NOT NULL)
    THEN
        LOOP
            FETCH F_cursor INTO F_info_tran_id, F_info_user_id, F_info_assign_writer_id, F_last_issue_action_id;
            EXIT WHEN F_cursor%NOTFOUND;
            -- INSERT INTO writers_commission_dep
            DBMS_OUTPUT.PUT_LINE('About to insert into writers_commission_dep, User id: '||F_info_user_id||', Transaction id: '||F_info_tran_id||', assigned writer id: '||F_info_assign_writer_id||', last issue action id: '||F_last_issue_action_id);
            INSERT INTO writers_commission_dep(id, transaction_id, writer_id ,time_created, last_issue_action_id)
              VALUES (SEQ_WRITERS_COMMISSION_DEP.nextval, F_info_tran_id ,F_info_assign_writer_id, sysdate, F_last_issue_action_id);
            -- count commission deposits
            F_count_commission_deposits := F_count_commission_deposits + 1;
        END LOOP;
        CLOSE F_cursor;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('DEPOSIT_HANDLER END');
    RETURN F_count_commission_deposits;
  END DEPOSIT_HANDLER;
  
  
  FUNCTION DEPOSIT_HANDLER_CHECK(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_count_commission_deposits NUMBER := 0;
    F_cursor SYS_REFCURSOR;
    F_info_tran_id transactions.id %TYPE;
    F_info_user_id transactions.user_id %TYPE;
    F_info_assign_writer_id NUMBER;
    F_is_exists_in_wcd NUMBER;
    F_data_found BOOLEAN := FALSE;
    F_is_valid_only NUMBER := 1;
    F_last_issue_action_id number;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('DEPOSIT_HANDLER_CHECK START');
    F_is_exists_in_wcd := WRITERS_COMMISSION.IS_EXISTS_IN_WCD(P_transaction_id, F_is_valid_only);
    IF F_is_exists_in_wcd > 0 THEN 
      DBMS_OUTPUT.PUT_LINE('The transaction: '||P_transaction_id||' exists in the table writers_commission_dep');
      F_count_commission_deposits := F_count_commission_deposits + 1;
      RETURN F_count_commission_deposits;
    END IF;
    
    F_cursor := WRITERS_COMMISSION.GET_COMMISSION_DEPOSITS(P_last_run, P_start_time, P_max_deposit_during_call_days, P_transaction_id);    
    
    -- Loop through the resulting data
    IF (F_cursor IS NOT NULL)
    THEN
        LOOP
            FETCH F_cursor INTO F_info_tran_id, F_info_user_id, F_info_assign_writer_id, F_last_issue_action_id;
            EXIT WHEN F_cursor%NOTFOUND;
            -- The info which supposed to insert into writers_commission_dep
            DBMS_OUTPUT.PUT_LINE('The info which supposed to insert into writers_commission_dep:');
            DBMS_OUTPUT.PUT_LINE('User id: '||F_info_user_id||', Transaction id: '||F_info_tran_id||', assigned writer id: '||F_info_assign_writer_id||', last issue action id: '||F_last_issue_action_id);
            F_data_found := TRUE;
            -- count commission deposits
            F_count_commission_deposits := F_count_commission_deposits + 1;
        END LOOP;
        CLOSE F_cursor;
    END IF;
    
    IF NOT F_data_found THEN
      DBMS_OUTPUT.PUT_LINE('No data found which supposed to insert into writers_commission_dep.');
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('DEPOSIT_HANDLER_CHECK END');
    RETURN F_count_commission_deposits;
  END DEPOSIT_HANDLER_CHECK;
  
  
  FUNCTION IS_EXISTS_IN_WCD(P_transaction_id NUMBER, P_is_valid NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_is_exists NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_IN_WCD START');
    select
      1
    into
      F_is_exists
    from
      writers_commission_dep wcd
    where
      wcd.TRANSACTION_ID = P_transaction_id
      and wcd.IS_VALID = DECODE(P_is_valid, -1, wcd.IS_VALID, P_is_valid);
    
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_IN_WCD END');
    RETURN F_is_exists;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_EXISTS_IN_WCD END');
      RETURN F_is_exists;
  END IS_EXISTS_IN_WCD;
  
  
  FUNCTION UPDATE_DB_PARAMETERS(P_db_parameter_id NUMBER, P_start_time DATE)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_updated NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('UPDATE_DB_PARAMETERS START');
    update 
      db_parameters
    set 
      date_value = P_start_time
    where 
      id = P_db_parameter_id;
    
    DBMS_OUTPUT.PUT_LINE('UPDATE_DB_PARAMETERS END');
    F_updated := 1;
    RETURN F_updated;
  END UPDATE_DB_PARAMETERS;
  
  
  FUNCTION GET_DATE_FROM_DB_PARAMETERS(P_db_parameter_id NUMBER)
    RETURN DATE
  AS
    -- *** Declaration_section ***
    F_date_value DATE;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_DATE_FROM_DB_PARAMETERS START');
    SELECT 
      date_value 
    into 
      F_date_value
    from 
      db_parameters 
    where 
      id = P_db_parameter_id;
    
    DBMS_OUTPUT.PUT_LINE('GET_DATE_FROM_DB_PARAMETERS END');
    RETURN F_date_value;
  END GET_DATE_FROM_DB_PARAMETERS;
  
  
  FUNCTION GET_MAX_DEP_DURING_CALL_DAYS
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_max_dep_during_call_days NUMBER;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_MAX_DEP_DURING_CALL_DAYS START');
    select 
      max(deposit_during_call_days)
    into 
      F_max_dep_during_call_days
    from 
      skins;
    
    DBMS_OUTPUT.PUT_LINE('GET_MAX_DEP_DURING_CALL_DAYS END');
    RETURN F_max_dep_during_call_days;
  END GET_MAX_DEP_DURING_CALL_DAYS;
  
  function get_last_assigned_writer
  (
    p_population_users_id in number
   ,p_to_date             in date
   ,p_writer_id           in number
  ) return peh_nt as
    -- *** Declaration_section ***
    f_peh_status_assign    number := 2;
    f_assigned_writer_id   number := -1;
    f_assigned_writer_time date := sysdate;
    f_assigned_peh_id      number := -1;
    f_val                  peh_nt;
  begin
    -- *** Execution_section ***
    select peh_t(p.assigned_writer_id, p.time_created, p.id)
    bulk   collect
    into   f_val
    from   (select peh.*
            from   population_entries pe
            join   population_entries_hist peh
            on     peh.population_entry_id = pe.id
            where  pe.population_users_id = p_population_users_id
            and    peh.time_created <= p_to_date
            and    peh.status_id = f_peh_status_assign
            and    peh.assigned_writer_id = decode(p_writer_id, 0, peh.assigned_writer_id, p_writer_id)
            order  by peh.id desc) p
    where  rownum <= 1;
  
    if f_val.count = 0
    then
      f_val.extend;
      f_val(f_val.last) := peh_t(f_assigned_writer_id, f_assigned_writer_time, f_assigned_peh_id);
    end if;
  
    return f_val;  
  end get_last_assigned_writer;
  
  
    FUNCTION GET_COMMISSION_DEPOSITS(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** Declaration_section ***
    F_cursor SYS_REFCURSOR;
    F_TRANS_STATUS_SUCCEED NUMBER := 2;
    F_TRANS_STATUS_PENDING NUMBER := 7;
    F_TRANS_STATUS_CANCELED_ET NUMBER := 8;
    F_trans_class_deposit NUMBER := 1;
    F_ISSUE_CHANNEL_CALL NUMBER := 1;
    F_ISSUE_REACHED_STATUS_REACHED NUMBER := 2;
    F_ISSUE_DEPOSIT_SEARCH_TYPE NUMBER := 2;
    F_ISSUE_DEPOSIT_DURING_CALL NUMBER := 31;
    F_INTERVAL_DAYS_FOR_ACTIONS NUMBER := 30;
    F_pehs_id NUMBER := 6;
    F_pehs_is_cancel_assign NUMBER := 1;
    F_is_valid NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_COMMISSION_DEPOSITS START');
    OPEN 
      F_cursor 
    for
    select * from (
    SELECT 
      t.id as tran_id,
      t.user_id,
      (SELECT 
        la.T_assigned_writer_id
      FROM 
        TABLE(WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(pu.id, t.TIME_CREATED, 0)) la)
      ,(SELECT max(ia.id)
                  FROM
                    issues i,
                    issue_actions ia,
                    issue_action_types iat,
                    TABLE(WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(pu.id, t.TIME_CREATED, 0)) last_assign
                  WHERE
                    i.id = ia.issue_id
                    and ia.issue_action_type_id = iat.id
                    and i.user_id = u.ID
                    and iat.channel_id = F_ISSUE_CHANNEL_CALL
                    and iat.reached_status_id = F_ISSUE_REACHED_STATUS_REACHED
                    -- Get only issue actions which have one of the following cases:
                    -- 1. The issue action is positive and made in the 'INTERVAL_DAYS_FOR_ACTIONS' days prior to the transaction's time created.
                    -- 2. The issue action is deposit during call and made in the 'max_deposit_during_call_days' minutes before or after transaction's time created.
                    -- 3. The issue action is deposit during call, It's made in the 'INTERVAL_DAYS_FOR_ACTIONS' days prior to the transaction's time created and user deposited 'max_deposit_during_call_days' minutes before or after this issue.
                    and ((iat.DEPOSIT_SEARCH_TYPE = F_ISSUE_DEPOSIT_SEARCH_TYPE
                          and ia.action_time between (t.TIME_CREATED - F_INTERVAL_DAYS_FOR_ACTIONS) and t.TIME_CREATED)
                        or 
                          (iat.id = F_ISSUE_DEPOSIT_DURING_CALL
                          and t.TIME_CREATED between (ia.action_time - P_max_deposit_during_call_days) and (ia.action_time + P_max_deposit_during_call_days)
                          and t.TIME_CREATED >= last_assign.T_assigned_writer_time)
                        or
                          (iat.id = F_ISSUE_DEPOSIT_DURING_CALL
                          and ia.action_time between (t.TIME_CREATED - F_INTERVAL_DAYS_FOR_ACTIONS) and t.TIME_CREATED
                          and exists (select 
                                        1
                                      from
                                        writers_commission_dep ddc_wcd,
                                        transactions ddc_t
                                      where
                                        ddc_wcd.TRANSACTION_ID = ddc_t.id
                                        and ddc_t.USER_ID = u.ID
                                        and ddc_wcd.writer_id = last_assign.T_assigned_writer_id
                                        and ddc_t.TIME_CREATED between (ia.action_time - P_max_deposit_during_call_days) and (ia.action_time + P_max_deposit_during_call_days)
                                        and ddc_t.TIME_CREATED >= last_assign.T_assigned_writer_time
                                        and ddc_wcd.IS_VALID = F_is_valid)))
                    -- Get only issue actions which made by the last assign writer
                    and ia.writer_id = last_assign.T_assigned_writer_id
                    -- Get only issue action which doesn't have cancel assign between issue action's time created to transaction's time created.
                    and NOT EXISTS (select 
                                      1
                                    from 
                                      population_entries pe, 
                                      population_entries_hist peh,
                                      population_entries_hist_status pehs
                                    where 
                                      pe.id = peh.population_entry_id
                                      and peh.status_id = pehs.id
                                      and pe.population_users_id = pu.ID
                                      and peh.time_created <= t.TIME_CREATED
                                      and peh.time_created >= ia.ACTION_TIME
                                      and (pehs.id = F_pehs_id OR pehs.is_cancel_assign = F_pehs_is_cancel_assign))) max_issue_action_id  
    FROM 
      transactions t,
      transaction_types tt,
      users u,
      population_users pu
    WHERE  
      t.type_id = tt.id
      and t.user_id = u.id
      and u.id = pu.user_id
      and u.first_deposit_id <> t.id 
      and tt.class_type = F_trans_class_deposit
      and t.id = DECODE(P_transaction_id, 0, t.id, P_transaction_id)
      and (((t.time_created + P_max_deposit_during_call_days) >  P_last_run
            and (t.time_created + P_max_deposit_during_call_days) <= P_start_time
            and t.status_id in (F_TRANS_STATUS_PENDING,F_TRANS_STATUS_CANCELED_ET))
           or
            ((t.time_settled + P_max_deposit_during_call_days) >  P_last_run
            and (t.time_settled + P_max_deposit_during_call_days) <= P_start_time
            and t.status_id = F_TRANS_STATUS_SUCCEED))
      -- Get only transactions which doesn't exists in the table 'writers_commission_dep'.
      and not exists (select
                        1
                      from
                        writers_commission_dep wcd
                      where
                        wcd.TRANSACTION_ID = t.id
                        and wcd.IS_VALID = F_is_valid)
      -- Get only transactions which have issue actions with some conditions (The conditions will describe below)
      ) where max_issue_action_id is not null;
      
    DBMS_OUTPUT.PUT_LINE('GET_COMMISSION_DEPOSITS END');
    RETURN F_cursor;
  END GET_COMMISSION_DEPOSITS;
  
  
  FUNCTION WITHDRAW_HANDLER(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_count_commission_withdrawals NUMBER := 0;
    F_cursor SYS_REFCURSOR;
    F_info_tran_id transactions.id %TYPE;
    F_info_user_id transactions.user_id %TYPE;
    F_info_assign_writer_id NUMBER;
    F_info_amount NUMBER;
    T_info_assigned_peh_id NUMBER;
    T_info_unassigned_peh_id NUMBER;
    T_info_net_deposits NUMBER;
    T_info_last_attributed_tran_id NUMBER;
    F_transaction_id NUMBER := 0;
    
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('WITHDRAW_HANDLER START');
    F_cursor := WRITERS_COMMISSION.GET_COMMISSION_WITHDRAWALS(P_last_run, P_start_time, P_max_deposit_during_call_days, F_transaction_id);
    
    -- Loop through the resulting data
    IF (F_cursor IS NOT NULL)
    THEN
        LOOP
            FETCH F_cursor INTO F_info_tran_id, F_info_user_id, F_info_assign_writer_id, F_info_amount, T_info_assigned_peh_id, T_info_unassigned_peh_id, T_info_net_deposits, T_info_last_attributed_tran_id;
            EXIT WHEN F_cursor%NOTFOUND;
            -- INSERT INTO writers_commission_dep
            DBMS_OUTPUT.PUT_LINE('About to insert into writers_commission_dep, User id: '||F_info_user_id||', Transaction id: '||F_info_tran_id||', assigned writer id: '||F_info_assign_writer_id||', amount: '||F_info_amount|| ',T_info_assigned_peh_id: '||T_info_assigned_peh_id|| ',T_info_unassigned_peh_id: '||T_info_unassigned_peh_id|| ',T_info_net_deposits: '||T_info_net_deposits|| ',T_info_last_attributed_tran_id: '||T_info_last_attributed_tran_id);
            INSERT INTO writers_commission_dep(id, transaction_id, writer_id ,time_created, AMOUNT, ASSIGN_POP_ENTRIES_HIST_ID, UNASSIGN_POP_ENTRIES_HIST_ID, NET_DEPOSITS, LAST_ATTRIBUTED_TRAN_ID)
              VALUES (SEQ_WRITERS_COMMISSION_DEP.nextval, F_info_tran_id ,F_info_assign_writer_id, sysdate, F_info_amount, T_info_assigned_peh_id, T_info_unassigned_peh_id, T_info_net_deposits, T_info_last_attributed_tran_id);
            -- count commission withdrawals
            F_count_commission_withdrawals := F_count_commission_withdrawals + 1;
        END LOOP;
        CLOSE F_cursor;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('WITHDRAW_HANDLER END');
    RETURN F_count_commission_withdrawals;
  END WITHDRAW_HANDLER;
  
  
  FUNCTION WITHDRAW_HANDLER_CHECK(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_count_commission_withdrawals NUMBER := 0;
    F_cursor SYS_REFCURSOR;
    F_info_tran_id transactions.id %TYPE;
    F_info_user_id transactions.user_id %TYPE;
    F_info_assign_writer_id NUMBER;
    F_info_amount NUMBER;
    T_info_assigned_peh_id NUMBER;
    T_info_unassigned_peh_id NUMBER;
    T_info_net_deposits NUMBER;
    T_info_last_attributed_tran_id NUMBER;
    F_is_exists_in_wcd NUMBER;
    F_data_found BOOLEAN := FALSE;
    F_is_valid_only NUMBER := 1;
    
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('WITHDRAW_HANDLER_CHECK START');
    F_is_exists_in_wcd := WRITERS_COMMISSION.IS_EXISTS_IN_WCD(P_transaction_id, F_is_valid_only);
    IF F_is_exists_in_wcd > 0 THEN 
      DBMS_OUTPUT.PUT_LINE('The transaction: '||P_transaction_id||' exists in the table writers_commission_dep');
      F_count_commission_withdrawals := F_count_commission_withdrawals + 1;
      RETURN F_count_commission_withdrawals;
    END IF;
    
    F_cursor := WRITERS_COMMISSION.GET_COMMISSION_WITHDRAWALS(P_last_run, P_start_time, P_max_deposit_during_call_days, P_transaction_id);    
    
    -- Loop through the resulting data
    IF (F_cursor IS NOT NULL)
    THEN
        -- The info which supposed to insert into writers_commission_dep
        DBMS_OUTPUT.PUT_LINE('The info which supposed to insert into writers_commission_dep:');
        LOOP
            FETCH F_cursor INTO F_info_tran_id, F_info_user_id, F_info_assign_writer_id, F_info_amount, T_info_assigned_peh_id, T_info_unassigned_peh_id, T_info_net_deposits, T_info_last_attributed_tran_id;
            EXIT WHEN F_cursor%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE('User id: '||F_info_user_id||', Transaction id: '||F_info_tran_id||', assigned writer id: '||F_info_assign_writer_id|| ',F_info_amount: '||F_info_amount|| ',T_info_assigned_peh_id: '||T_info_assigned_peh_id|| ',T_info_unassigned_peh_id: '||T_info_unassigned_peh_id|| ',T_info_net_deposits: '||T_info_net_deposits|| ',T_info_last_attributed_tran_id: '||T_info_last_attributed_tran_id);
            F_data_found := TRUE;
            -- count commission withdrawals
            F_count_commission_withdrawals := F_count_commission_withdrawals + 1;
        END LOOP;
        CLOSE F_cursor;
    END IF;
    
    IF NOT F_data_found THEN
      DBMS_OUTPUT.PUT_LINE('No data found which supposed to insert into writers_commission_dep.');
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('WITHDRAW_HANDLER_CHECK END');
    RETURN F_count_commission_withdrawals;
  END WITHDRAW_HANDLER_CHECK;
  
  
  FUNCTION IS_EXISTS_WCD_IN_TIMEFRAME(P_user_id NUMBER, P_writer_id NUMBER, P_to_date DATE)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_is_exists NUMBER := 0;
    F_trans_class_deposit NUMBER := 1;
    F_is_valid NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_WCD_IN_TIMEFRAME START');
    select
      1
    into
      F_is_exists
    from
      writers_commission_dep wcd,
      transactions t,
      transaction_types tt
    where
      wcd.TRANSACTION_ID = t.ID
      and t.TYPE_ID = tt.id
      and t.USER_ID = P_user_id
      and t.TIME_CREATED <= P_to_date
      and wcd.WRITER_ID = P_writer_id
      and tt.class_type = F_trans_class_deposit
      and wcd.IS_VALID = F_is_valid;
    
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_WCD_IN_TIMEFRAME END');
    RETURN F_is_exists;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_EXISTS_IN_WCD END');
      RETURN F_is_exists;
  END IS_EXISTS_WCD_IN_TIMEFRAME;
  
  
  FUNCTION GET_LAST_WCD(P_user_id NUMBER, P_to_date DATE, P_writer_id NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** Declaration_section ***
    F_cursor SYS_REFCURSOR;
    F_writer_id NUMBER := -1;
    F_wcd_time DATE := sysdate;
    F_trans_class_deposit NUMBER := 1;
    F_is_valid NUMBER := 1;
    F_tran_id NUMBER := -1;
  BEGIN
    -- *** Execution_section ***
    OPEN 
      F_cursor 
    FOR
    select
      wcd_sorted.WRITER_ID,
      wcd_sorted.TIME_CREATED,
      wcd_sorted.tran_id
    into
      F_writer_id,
      F_wcd_time,
      F_tran_id
    from
      (select
        wcd.*,
        t.id as tran_id
      from 
        WRITERS_COMMISSION_DEP wcd,
        TRANSACTIONS t,
        transaction_types tt
      where
        wcd.TRANSACTION_ID = t.ID
        and t.TYPE_ID = tt.id
        and t.time_created <= P_to_date
        and t.USER_ID = P_user_id
        and tt.class_type = F_trans_class_deposit
        and wcd.IS_VALID = F_is_valid
        and wcd.WRITER_ID = DECODE(P_writer_id, 0, wcd.WRITER_ID, P_writer_id)
      ORDER BY wcd.id desc
      ) wcd_sorted
    where
      ROWNUM = 1;
      
    RETURN F_cursor;
  END GET_LAST_WCD;
  
  
  FUNCTION GET_FIRST_ASSIGNED_UNASSIGNED(P_population_users_id NUMBER, P_from_date DATE, P_to_date DATE)
    RETURN peh_nt
  AS
    -- *** Declaration_section ***
    F_peh_status_assign NUMBER := 2;
    F_assigned_writer_id NUMBER := -1;
    F_assigned_writer_time DATE := sysdate;
    F_assigned_peh_id NUMBER := -1;
    F_val peh_nt := peh_nt();
    F_pehs_id NUMBER := 6;
    F_pehs_is_cancel_assign NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    select
      peh_sorted.ASSIGNED_WRITER_ID,
      peh_sorted.TIME_CREATED,
      peh_sorted.id
    into
      F_assigned_writer_id,
      F_assigned_writer_time,
      F_assigned_peh_id
    from
      (select
        peh.*
      from 
        population_entries pe, 
        population_entries_hist peh,
        population_entries_hist_status pehs
      where
        pe.id = peh.population_entry_id
        and peh.status_id = pehs.id
        and pe.population_users_id = P_population_users_id
        and peh.time_created > P_from_date
        and peh.time_created <= P_to_date 
        and (pehs.id = F_pehs_id OR pehs.is_cancel_assign = F_pehs_is_cancel_assign OR peh.status_id = F_peh_status_assign)
      ORDER BY peh.id asc
      ) peh_sorted
    where
      ROWNUM = 1;
    
    F_val.EXTEND;
    F_val (F_val.LAST) := peh_t (F_assigned_writer_id, F_assigned_writer_time, F_assigned_peh_id);
    RETURN F_val;
    
    EXCEPTION
      WHEN OTHERS THEN
      F_val.EXTEND;
      F_val (F_val.LAST) := peh_t (F_assigned_writer_id, F_assigned_writer_time, F_assigned_peh_id);
      RETURN F_val;
  END GET_FIRST_ASSIGNED_UNASSIGNED;
  
  
  FUNCTION GET_COMMISSION_WITHDRAWALS(P_last_run DATE, P_start_time DATE, P_max_deposit_during_call_days NUMBER, P_transaction_id NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** Declaration_section ***
    F_cursor SYS_REFCURSOR;
    F_cursor_last_wcd SYS_REFCURSOR;
    F_TRANS_CLASS_WITHDRAW NUMBER := 2;
    F_TRANS_STATUS_SUCCEED NUMBER := 2;
    F_last_rep peh_nt := peh_nt();
    F_last_rep_unassigned peh_nt := peh_nt();
    F_last_rep_wcd peh_nt := peh_nt();
    F_last_rep_wcd_unassigned peh_nt := peh_nt();
    F_wcd_nt wcd_nt := wcd_nt();
    F_is_exists_wcd_in_timeframe NUMBER;
    F_is_exists_deposit NUMBER;
    F_writer_id NUMBER;
    F_wcd_time DATE;
    F_amount_for_attribution NUMBER;
    F_is_need_check_second_case NUMBER;
    F_is_exists_in_wcd NUMBER;
    F_is_valid_all NUMBER := -1;
    F_net_deposits NUMBER;
    F_last_attributed_tran_id NUMBER;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_COMMISSION_WITHDRAWALS START');
    FOR 
      v_tran 
    IN 
      ( SELECT 
          t.id as tran_id,
          t.user_id as user_id,
          t.time_created as tran_time,
          t.amount,
          pu.id as pop_user_id
        FROM 
          transactions t,
          transaction_types tt,
          users u,
          population_users pu
        WHERE 
          u.id = t.user_id 
          and t.type_id = tt.id
          and u.id = pu.user_id
          and tt.class_type = F_TRANS_CLASS_WITHDRAW
          and t.id = DECODE(P_transaction_id, 0, t.id, P_transaction_id)
          and (t.time_settled + P_max_deposit_during_call_days) >  P_last_run 
          and (t.time_settled + P_max_deposit_during_call_days) <= P_start_time
          and t.status_id = F_TRANS_STATUS_SUCCEED
      ) 
    LOOP
      DBMS_OUTPUT.PUT_LINE('-----');
      DBMS_OUTPUT.PUT_LINE('CHECK, User id: '||v_tran.user_id||', Transaction id: '||v_tran.tran_id||', Transaction time created: '||v_tran.tran_time||', Amount: '||v_tran.amount||', pop_user_id: '||v_tran.pop_user_id);
      F_is_need_check_second_case := 1;
      
      -- IS_EXISTS_IN_WCD
      F_is_exists_in_wcd := WRITERS_COMMISSION.IS_EXISTS_IN_WCD(v_tran.tran_id, F_is_valid_all);
      IF F_is_exists_in_wcd > 0 THEN 
        DBMS_OUTPUT.PUT_LINE('The transaction: '||v_tran.tran_id||' exists in the table writers_commission_dep');
        GOTO END_LOOP;
      END IF;
      
      -- GET_LAST_ASSIGNED_WRITER
      F_last_rep := WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(v_tran.pop_user_id, v_tran.tran_time, 0);
      DBMS_OUTPUT.PUT_LINE('Last writer assigned: ' || F_last_rep(1).T_assigned_writer_id || ' in: ' || F_last_rep(1).T_assigned_writer_time || ', population_entries_hist_id: ' || F_last_rep(1).T_assigned_peh_id);
      IF F_last_rep(1).T_assigned_writer_id = -1 THEN 
        DBMS_OUTPUT.PUT_LINE('Transaction will not attributed, The user did not assigned before transaction settled.');
        GOTO END_LOOP;
      END IF;
      
      -- GET_FIRST_ASSIGNED_UNASSIGNED (after last assigned)
      F_last_rep_unassigned := WRITERS_COMMISSION.GET_FIRST_ASSIGNED_UNASSIGNED(v_tran.pop_user_id, F_last_rep(1).T_assigned_writer_time, v_tran.tran_time);      
      
      -- First case
      IF F_last_rep_unassigned(1).T_assigned_writer_id = -1 THEN
        DBMS_OUTPUT.PUT_LINE('The user was assigned when withdraw settled.');
        DBMS_OUTPUT.PUT_LINE('First case - START.');
        -- Check if the customer deposit is attributed (ever) to the representative [X] prior to the withdraw request
        F_cursor_last_wcd := WRITERS_COMMISSION.GET_LAST_WCD(v_tran.user_id, v_tran.tran_time, F_last_rep(1).T_assigned_writer_id);
        FETCH F_cursor_last_wcd INTO F_writer_id, F_wcd_time, F_last_attributed_tran_id;
        IF F_cursor_last_wcd%FOUND THEN
          DBMS_OUTPUT.PUT_LINE('The user '||v_tran.user_id||' has deposit(ever) which attributed to the representative '||F_last_rep(1).T_assigned_writer_id||' prior to the withdraw request.');
          DBMS_OUTPUT.PUT_LINE('About to add into the list, User id: '||v_tran.user_id||', Transaction id: '||v_tran.tran_id||', assigned writer id: '||F_last_rep(1).T_assigned_writer_id||', amount: '||v_tran.amount);
          F_wcd_nt.EXTEND;
          F_wcd_nt (F_wcd_nt.LAST) := wcd_t (v_tran.tran_id, v_tran.user_id, F_last_rep(1).T_assigned_writer_id, v_tran.amount, F_last_rep(1).T_assigned_peh_id, 0, '', F_last_attributed_tran_id);
          F_is_need_check_second_case := 0;
        ELSE
          DBMS_OUTPUT.PUT_LINE('The user '||v_tran.user_id||' is not have deposit(ever) which attributed to the representative '||F_last_rep(1).T_assigned_writer_id||' prior to the withdraw request.');
        END IF;
        DBMS_OUTPUT.PUT_LINE('First case - END.');
      ELSE 
        DBMS_OUTPUT.PUT_LINE('Unassigned at: ' || F_last_rep_unassigned(1).T_assigned_writer_time);
      END IF;
      
      -- Second case
      IF F_is_need_check_second_case = 1 THEN
        DBMS_OUTPUT.PUT_LINE('Second case - START.');
        
        -- GET_LAST_WCD
        F_cursor_last_wcd := WRITERS_COMMISSION.GET_LAST_WCD(v_tran.user_id, v_tran.tran_time, 0);
        FETCH F_cursor_last_wcd INTO F_writer_id, F_wcd_time, F_last_attributed_tran_id;
        IF F_cursor_last_wcd%NOTFOUND THEN
          DBMS_OUTPUT.PUT_LINE('Not found deposit attributed.');
          GOTO END_LOOP;
        END IF;
        DBMS_OUTPUT.PUT_LINE('Last deposit attributed, F_writer_id: '||F_writer_id||', F_wcd_time: '||F_wcd_time);
        
        -- Get the last time assigned by the writer which has deposit attributed
        F_last_rep_wcd := WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(v_tran.pop_user_id, F_wcd_time, F_writer_id);
        DBMS_OUTPUT.PUT_LINE('Last writer assigned which has deposit attributed: ' || F_last_rep_wcd(1).T_assigned_writer_id || ' in: ' || F_last_rep_wcd(1).T_assigned_writer_time);
--        IF F_last_rep_wcd(1).T_assigned_writer_id = -1 THEN 
--          DBMS_OUTPUT.PUT_LINE('Transaction will not attributed, Not found writer assigned which has deposit attributed before transaction settled.');
--          GOTO END_LOOP;
--        END IF;
        
        -- Get the first time unassigned (after assignment time) by the writer which has deposit attributed
        F_last_rep_wcd_unassigned := WRITERS_COMMISSION.GET_FIRST_ASSIGNED_UNASSIGNED(v_tran.pop_user_id, F_last_rep_wcd(1).T_assigned_writer_time, v_tran.tran_time);
        DBMS_OUTPUT.PUT_LINE('Unassigned at: ' || F_last_rep_wcd_unassigned(1).T_assigned_writer_time);
        
        -- IS_EXISTS_DEPOSIT (Check if deposit was made from when customer was unassigned from the selected representative up to the withdraw request created timestamp)
        F_is_exists_deposit := WRITERS_COMMISSION.IS_EXISTS_DEPOSIT(v_tran.user_id, F_last_rep_wcd_unassigned(1).T_assigned_writer_time, v_tran.tran_time);
        IF F_is_exists_deposit = 0 THEN 
          DBMS_OUTPUT.PUT_LINE('Calculate amount for withdraw commission');
          
          F_amount_for_attribution := v_tran.amount;
          F_net_deposits := WRITERS_COMMISSION.GET_NET_DEPOSITS(v_tran.user_id, F_last_rep_wcd(1).T_assigned_writer_id, v_tran.tran_time, v_tran.amount);
          IF (F_net_deposits < F_amount_for_attribution) THEN
            F_amount_for_attribution := F_net_deposits;
          END IF;
          
          IF (F_amount_for_attribution > 0) THEN
            DBMS_OUTPUT.PUT_LINE('About to add into the list, User id: '||v_tran.user_id||', Transaction id: '||v_tran.tran_id||', assigned writer id: '||F_last_rep_wcd(1).T_assigned_writer_id||', amount: '||F_amount_for_attribution);
            F_wcd_nt.EXTEND;
            F_wcd_nt (F_wcd_nt.LAST) := wcd_t (v_tran.tran_id, v_tran.user_id, F_last_rep_wcd(1).T_assigned_writer_id, F_amount_for_attribution, F_last_rep_wcd(1).T_assigned_peh_id, F_last_rep_wcd_unassigned(1).T_assigned_peh_id, F_net_deposits, F_last_attributed_tran_id);
          ELSE
            DBMS_OUTPUT.PUT_LINE('Transaction will not attributed, the amount for attribution must be higher than zero. ');
          END IF;
        ELSE
          DBMS_OUTPUT.PUT_LINE('Transaction will not attributed, the deposit was made from when customer was unassigned from the selected representative up to the withdraw request created timestamp');
        END IF;
        
        DBMS_OUTPUT.PUT_LINE('Second case - END.');
      END IF;
      
      <<END_LOOP>> NULL;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('-----');
    
    -- Get all data which supposed to be inserted into DB
    OPEN
      F_cursor
    FOR
      SELECT 
        wcd_info.*
      FROM 
        TABLE(F_wcd_nt) wcd_info;
    
    DBMS_OUTPUT.PUT_LINE('GET_COMMISSION_WITHDRAWALS END');
    RETURN F_cursor;
  END GET_COMMISSION_WITHDRAWALS;
  
  
  FUNCTION IS_EXISTS_DEPOSIT(P_user_id NUMBER, P_from_date DATE, P_to_date DATE)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_is_exists NUMBER := 0;
    F_TRANS_STATUS_SUCCEED NUMBER := 2;
    F_TRANS_STATUS_PENDING NUMBER := 7;
    F_TRANS_STATUS_CANCELED_ET NUMBER := 8;
    F_trans_class_deposit NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_DEPOSIT START');
    select
      1
    into
      F_is_exists
    FROM 
      transactions t,
      transaction_types tt
    WHERE  
      t.type_id = tt.id
      and t.user_id = P_user_id
      and tt.class_type = F_trans_class_deposit
      and ((t.time_created >  P_from_date
            and t.time_created <= P_to_date
            and t.status_id in (F_TRANS_STATUS_PENDING,F_TRANS_STATUS_CANCELED_ET))
           or
            (t.time_settled >  P_from_date
            and t.time_settled <= P_to_date
            and t.status_id = F_TRANS_STATUS_SUCCEED));
    
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_DEPOSIT END');
    RETURN F_is_exists;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_EXISTS_DEPOSIT END');
      RETURN F_is_exists;
  END IS_EXISTS_DEPOSIT;
  
  
  FUNCTION GET_NET_DEPOSITS(P_user_id NUMBER, P_writer_id NUMBER, P_to_date DATE, P_trans_amount NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_trans_class_deposit NUMBER := 1;
    F_trans_class_withdraw NUMBER := 2;
    F_sum_deposit NUMBER := 0;
    F_sum_withdraw NUMBER := 0;
    F_difference_deposit_withdraw NUMBER := 0;
    F_is_valid NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_NET_DEPOSITS START');
    -- Amount to be attributed = MIN {New Withdrawal request amount, SUM(qualifying deposits that came before this withdrawal request) вЂ“ SUM(qualifying withdrawals that came before this withdrawal request)}
    SELECT
      SUM(CASE WHEN (tt.class_type = 1) THEN t.amount END) as sum_deposit,
      SUM(CASE WHEN (tt.class_type = 2) THEN wcd.amount END) as sum_withdraw
    INTO
      F_sum_deposit,
      F_sum_withdraw
    FROM 
      WRITERS_COMMISSION_DEP wcd,
      TRANSACTIONS t,
      transaction_types tt
    WHERE
      wcd.TRANSACTION_ID = t.ID
      and t.TYPE_ID = tt.id
      and t.time_created < P_to_date
      and wcd.WRITER_ID = P_writer_id
      and t.USER_ID = P_user_id
      and wcd.IS_VALID = F_is_valid
    ORDER BY wcd.id desc;
    
    F_difference_deposit_withdraw := F_sum_deposit - F_sum_withdraw;
    
    DBMS_OUTPUT.PUT_LINE('Sum deposits: '||F_sum_deposit);
    DBMS_OUTPUT.PUT_LINE('Sum withdrawals: '||F_sum_withdraw);
    DBMS_OUTPUT.PUT_LINE('GET_NET_DEPOSITS END');
    RETURN F_difference_deposit_withdraw;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('GET_NET_DEPOSITS END');
      RETURN F_difference_deposit_withdraw;
  END GET_NET_DEPOSITS;
  
END WRITERS_COMMISSION;
/
