CREATE OR REPLACE TRIGGER AFTER_INVESTMENTS
before INSERT ON INVESTMENTS
REFERENCING NEW AS new
FOR EACH ROW
declare

cursor email_alert_c is
select ua.user_id, ua.alert_email_subject, :new.rate*:new.amount amount, ua.alert_email_recepients, u.user_name
from users_alerts ua, users u
where ua.user_id = :new.user_id
  and :new.rate*:new.amount between ua.alert_from_amount and ua.alert_to_amount
  and ua.user_id=u.id
  and ua.alert_type=8;

cursor c_check1 is
select 'user_id: '||u.id||' investment_id: '||:new.id||' alert: different investment country and registration country' txt
from users u
where u.id=:new.user_id
  and u.country_id!=:new.country_id;

cursor c_check2 is
select 'user_id: '||u.id||' investment_id: '||:new.id||' alert: Etrader user with investment from abroad' txt
from users u
where u.skin_id=1
  and u.id=:new.user_id
  and 1!=:new.country_id;

cursor c_check3 is
select 'user_id: '||u.id||' investment_id: '||:new.id||' alert: Anyoption user with investment from Israel' txt
from users u
where u.skin_id!=1
  and u.id=:new.user_id
  and 1=:new.country_id;

cursor c_check4 is
select 'user_id: '||:new.user_id||' investment_id: '||:new.id||' alert: investment with equal IP accross different accounts' txt
from users_alerts_temp i
where i.ip=:new.ip
  and i.user_id!=:new.user_id;

cursor c_check5 is
select 'user_id: '||:new.user_id||' investment_id: '||:new.id||' alert: investment with different IP in the same accounts' txt
from users_alerts_temp i
where i.ip!=:new.ip
  and i.user_id=:new.user_id
  and i.time_created>=:new.time_created-5/1440;

cursor c_conf is
select ALERT_EMAIL_SUBJECT, ALERT_EMAIL_RECEPIENTS
from users_alerts
where user_id=-1
  and alert_type=9;

cursor c_cfg_invest_check is
select sec_between_investments, sec_between_investments_mob
from markets m, opportunities o
where o.market_id=m.id
  and o.id=:new.opportunity_id;

r_cfg_invest number := 0;
r_cfg_invest_mob number := 0;
r_invest number:=0;

msg_text varchar2(100);
msg_subject varchar2(100):='failed configuration dev-244';
msg_recepients varchar2(4000):='simeonps@anyoption.com';

user_id  number;
alert_email_subject varchar2(100);
alert_amount number;
alert_email_recepients varchar2(4000);
user_name varchar2(100);

BEGIN

  open c_cfg_invest_check;
  fetch c_cfg_invest_check into r_cfg_invest, r_cfg_invest_mob;
  close c_cfg_invest_check;

  if (:new.writer_id=1 or :new.writer_id=10000) then
    r_invest:=r_cfg_invest;
  elsif (:new.writer_id=200 or :new.writer_id=868 or :new.writer_id=20000) then
    r_invest:=r_cfg_invest_mob;
  end if;

  if r_invest!=0 and :new.copyop_type_id!=1 and nvl(:new.reference_investment_id,0) = 0 then
    if utils.check_invest(:new.type_id,:new.opportunity_id,:new.user_id,:new.time_created,r_invest)='x' then
      raise_application_error(-20001, 'Investment failed due to time constraint between investments of: '||r_invest||' sec!');
    end if;
  end if;

  open email_alert_c;
  fetch email_alert_c into user_id, alert_email_subject, alert_amount, alert_email_recepients, user_name;
  if email_alert_c%FOUND then
    null;
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '8', user_name||': '||alert_email_subject, 'amount: '||alert_amount, alert_email_recepients, 0, null, systimestamp);
  end if;
  close email_alert_c;

  /*open c_conf;
  fetch c_conf into msg_subject, msg_recepients;
  close c_conf;

  msg_text:=null;
  open c_check1;
  fetch c_check1 into msg_text;
  if c_check1%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '9', msg_subject, msg_text, msg_recepients, 0, null, systimestamp);
  end if;
  close c_check1;

  msg_text:=null;
  open c_check2;
  fetch c_check2 into msg_text;
  if c_check2%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '9', msg_subject, msg_text, msg_recepients, 0, null, systimestamp);
  end if;
  close c_check2;

  msg_text:=null;
  open c_check3;
  fetch c_check3 into msg_text;
  if c_check3%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '9', msg_subject, msg_text, msg_recepients, 0, null, systimestamp);
  end if;
  close c_check3;

  msg_text:=null;
  open c_check4;
  fetch c_check4 into msg_text;
  if c_check4%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '9', msg_subject, msg_text, msg_recepients, 0, null, systimestamp);
  end if;
  close c_check4;

  msg_text:=null;
  open c_check5;
  fetch c_check5 into msg_text;
  if c_check5%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '9', msg_subject, msg_text, msg_recepients, 0, null, systimestamp);
  end if;
  close c_check5;

  insert into users_alerts_temp values(:new.id, :new.ip, :new.user_id, :new.time_created);*/

END;
/
