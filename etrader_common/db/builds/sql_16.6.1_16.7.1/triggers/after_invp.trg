create or replace
TRIGGER AFTER_INVP
BEFORE INSERT OR UPDATE OF IS_CANCELED
ON INVESTMENTS 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE


BEGIN

IF INSERTING THEN
    IF :NEW.IS_CANCELED = 0 THEN
    
  UPDATE silverpop_users su 
    set su.INV_SUM_A = nvl(su.INV_SUM_A,0)+:NEW.AMOUNT, 
        su.INV_CNT_A = nvl(su.INV_CNT_A,0)+1, 
        su.INV_FIRST_DATE = nvl(su.INV_FIRST_DATE,:NEW.TIME_CREATED), 
        su.INV_LAST_DATE = :NEW.TIME_CREATED
    WHERE su.user_id = :NEW.user_id;
    
    UPDATE 
		users_active_data uad 
	SET 
		uad.lifetime_volume = NVL(uad.lifetime_volume,0) + (:NEW.amount - nvl(:NEW.option_plus_fee,0) - NVL(:NEW.insurance_amount_ru,0)) * :NEW.rate
	WHERE 
		uad.user_id = :NEW.user_id;
  
    ELSE NULL;
END IF;

END IF;

IF UPDATING THEN

    IF :NEW.IS_CANCELED = 1 AND :OLD.IS_CANCELED = 0 THEN
    
        UPDATE silverpop_users su 
           set su.INV_SUM_A = nvl(su.INV_SUM_A,0)-:NEW.AMOUNT, 
               su.INV_CNT_A = nvl(su.INV_CNT_A,0)-1, 
               su.INV_FIRST_DATE = utils.RETURN_INV_FIRST_DATE(:NEW.user_id,:NEW.id), 
               su.INV_LAST_DATE = utils.RETURN_INV_LAST_DATE(:NEW.user_id,:NEW.id)              
         WHERE su.user_id = :NEW.user_id;
         
         UPDATE 
            users_active_data uad 
          SET 
            uad.lifetime_volume = NVL(uad.lifetime_volume,0) - (:NEW.amount - nvl(:NEW.option_plus_fee,0) - NVL(:NEW.insurance_amount_ru,0)) * :NEW.rate
          WHERE 
            uad.user_id = :NEW.user_id;
        
    ELSIF :NEW.IS_CANCELED = 0 AND :OLD.IS_CANCELED = 1 THEN
        
        UPDATE silverpop_users su 
           set su.INV_SUM_A = nvl(su.INV_SUM_A,0)+:NEW.AMOUNT, 
               su.INV_CNT_A = nvl(su.INV_CNT_A,0)+1, 
               su.INV_FIRST_DATE = utils.RETURN_INV_FIRST_DATE(:NEW.user_id,:NEW.id), 
               su.INV_LAST_DATE = utils.RETURN_INV_LAST_DATE(:NEW.user_id,:NEW.id)  
        WHERE su.user_id = :NEW.user_id;
            
    END IF;
    

END IF;

IF :NEW.IS_CANCELED = 0 AND :NEW.TYPE_ID IN (1,2) AND :NEW.COPYOP_TYPE_ID != 1 THEN


update users_active_data uad set uad.inv_cnt = nvl(uad.inv_cnt,0)+1, uad.inv_sum = nvl(uad.inv_sum,0)+:NEW.AMOUNT-:NEW.OPTION_PLUS_FEE, uad.last_inv_id = :NEW.ID, uad.def_inv_amount = :NEW.AMOUNT-:NEW.OPTION_PLUS_FEE  where uad.user_id = :NEW.user_id;

END IF;

END AFTER_INVP;
/
