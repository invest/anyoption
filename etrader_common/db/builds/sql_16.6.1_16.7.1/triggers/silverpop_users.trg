create or replace trigger silverpop_users
  after insert on users
  for each row
declare
  l_has_dynamics number;
  l_has_bubbles  number;
begin
  insert into silverpop_users (id, user_id) values (seq_silverpop_users.nextval, :new.id);

  l_has_dynamics := case
                      when :new.skin_id in (3, 10, 16, 18, 19, 20, 21, 23, 24) then
                       1
                      else
                       0
                    end;

  l_has_bubbles := case
                     when :new.skin_id in (1) then
                      0
                     else
                      1
                   end;

  insert into users_active_data
    (id, user_id, has_bubbles, has_dynamics)
  values
    (seq_users_active_data.nextval, :new.id, l_has_bubbles, l_has_dynamics);

  marketing.insert_marketing_att(marketing_attribution_obj(user_id                    => :new.id
                                                          ,type_id                    => 1
                                                          ,combination_id             => :new.combination_id
                                                          ,dynamic_param              => :new.dynamic_param
                                                          ,aff_sub1                   => null
                                                          ,aff_sub2                   => null
                                                          ,aff_sub3                   => null
                                                          ,affiliate_key              => :new.affiliate_key
                                                          ,special_code               => :new.special_code
                                                          ,affiliate_system           => :new.affiliate_system
                                                          ,affiliate_system_user_time => :new.affiliate_system_user_time));
end;
/
