----------------------------------------
-- Update GBG files. BAC-3293, BAC-3364.
--

-------
-- Creation of a temporary teable to log the modified GBG files.
-- Check if the table exists.
--
CREATE TABLE ETRADER.GBG_FILES_UPDATED
  (RUN_ID            NUMBER    NOT NULL
  ,STEP              NUMBER    NOT NULL
  ,ACTION_TIME       DATE      NOT NULL
  ,FILE_ID           NUMBER    NOT NULL
  ,USER_ID           NUMBER    NOT NULL
  ,OLD_FILE_TYPE_ID  NUMBER    NOT NULL
  ,NEW_FILE_TYPE_ID  NUMBER    NOT NULL
  ,REFERENCE         VARCHAR2(40 CHAR) NOT NULL
  );

-- Check the log of the mofified GBG files.
select run_id
      ,step
      ,to_char(action_time, 'yyyy-mm-dd hh24:mi:ss') action_time
      ,file_id
      ,user_id
      ,old_file_type_id
      ,new_file_type_id
      ,reference
  from etrader.gbg_files_updated
 order by run_id
         ,step;

----------
-- Some SQL*Plus settings.
--
set lines 220 pages 49999
set timing on time on
set verify off
set serveroutput on size 1000000
DEFINE RUN_ID = "1"

----------
-- Update of the GBG files.
--
DECLARE

  TYPE t_files IS TABLE OF ETRADER.FILES%ROWTYPE;

  c_OLD_FILE_TYPE_ID CONSTANT NUMBER := 22;
  c_NEW_FILE_TYPE_ID CONSTANT NUMBER := 36;

  lt_Files         t_files;
  lr_Prev_File     ETRADER.FILES%ROWTYPE;

  ln_Result        NUMBER(1,0) := 0;
  ln_Step          NUMBER      := 0;

  CURSOR c_GBG_Files IS
    SELECT f.*
      FROM etrader.files f
     WHERE f.file_type_id = c_OLD_FILE_TYPE_ID
       AND UPPER(f.reference) LIKE '%GBG%'
     ORDER BY f.user_id;

BEGIN
  lr_Prev_File.user_id      := 0;
  lr_Prev_File.file_type_id := 0;

  --
  OPEN c_GBG_Files;
  LOOP
    FETCH c_GBG_Files
    BULK COLLECT INTO lt_Files LIMIT 10000;
    EXIT WHEN lt_Files.count = 0;

    FOR i IN lt_Files.first .. lt_Files.last LOOP

      ln_Step := ln_Step + 1;

      -- If the cuurent user is different than the previous,
      -- then re-calculate the current document for the previous user.
      IF (    (lr_Prev_File.USER_ID <> 0)
          AND (lr_Prev_File.USER_ID <> lt_Files(i).USER_ID)
         ) THEN
        BEGIN
          dbms_output.put_line('Executing ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS('||lr_Prev_File.USER_ID||','||c_OLD_FILE_TYPE_ID||','||lr_Prev_File.CC_ID||')');
          dbms_output.put_line('Executing ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS('||lr_Prev_File.USER_ID||','||c_NEW_FILE_TYPE_ID||','||lr_Prev_File.CC_ID||')');
          ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS(lr_Prev_File.USER_ID
                                                        ,c_OLD_FILE_TYPE_ID
                                                        ,lr_Prev_File.CC_ID
                                                        );

          ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS(lr_Prev_File.USER_ID
                                                        ,c_NEW_FILE_TYPE_ID
                                                        ,lr_Prev_File.CC_ID
                                                        );
        END;
      END IF;

      dbms_output.put_line('---');
      dbms_output.put_line('Step: ' || ln_Step || ', Processing User_ID: ' || lt_Files(i).user_id || ', File_ID: ' || lt_Files(i).id);
      insert into gbg_files_updated
        (run_id
        ,step
        ,action_time
        ,file_id
        ,user_id
        ,old_file_type_id
        ,new_file_type_id
        ,reference
        )
      values
        (&RUN_ID
        ,ln_Step
        ,SYSDATE
        ,lt_Files(i).id
        ,lt_Files(i).user_id
        ,lt_Files(i).file_type_id
        ,c_NEW_FILE_TYPE_ID
        ,lt_Files(i).reference
        );

      update etrader.files f
      set    f.file_type_id = c_NEW_FILE_TYPE_ID
      where  f.id = lt_Files(i).id;

      dbms_output.put_line('File_id: ' || lt_Files(i).id || ': ' || SQL%ROWCOUNT || ' records updated.');

      commit;

      lr_Prev_File := lt_Files(i);

    END LOOP;

  END LOOP;
  CLOSE c_GBG_Files;

  IF ((lr_Prev_File.USER_ID <> 0)
     ) THEN
    BEGIN
      dbms_output.put_line('Executing ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS('||lr_Prev_File.USER_ID||','||c_OLD_FILE_TYPE_ID||','||lr_Prev_File.CC_ID||')');
      dbms_output.put_line('Executing ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS('||lr_Prev_File.USER_ID||','||c_NEW_FILE_TYPE_ID||','||lr_Prev_File.CC_ID||')');
      ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS(lr_Prev_File.USER_ID
                                                    ,c_OLD_FILE_TYPE_ID
                                                    ,lr_Prev_File.CC_ID
                                                    );

      ETRADER.PKG_USER_DOCUMENTS.SET_IS_CURRENT_DOCS(lr_Prev_File.USER_ID
                                                    ,c_NEW_FILE_TYPE_ID
                                                    ,lr_Prev_File.CC_ID
                                                    );

      commit;
    END;
  END IF;

END;
/
