CREATE OR REPLACE TRIGGER TRG_FIRST_DEP_USERS
  after update of first_deposit_id ON USERS for each row
DECLARE


cursor c_rec_mt is
select * from
marketing_tracking mt
where mt.user_id=:new.id
order by 1 desc;

cursor c_check_activ is
select count(*) from
marketing_tracking
where user_id = :new.id
and marketing_tracking_activity_id=5;

v_activ number:=0;

v_first_dep marketing_tracking%rowtype;

begin

open c_check_activ;
fetch c_check_activ into v_activ;
close c_check_activ;

If v_activ=0 then
    open c_rec_mt;
    fetch  c_rec_mt into v_first_dep;
    if c_rec_mt%found then

    insert into marketing_tracking
      (id, marketing_tracking_static_id, combination_id_dynamic, time_dynamic, http_referer_dynamic, dynamic_param_dynamic, contact_id, user_id, marketing_tracking_activity_id)
    values
      (SEQ_MARKETING_TRACKING.NEXTVAL, v_first_dep.marketing_tracking_static_id, v_first_dep.combination_id_dynamic, v_first_dep.time_dynamic, v_first_dep.http_referer_dynamic, v_first_dep.dynamic_param_dynamic, v_first_dep.contact_id, v_first_dep.user_id, 5);
    end if;
    close c_rec_mt;
End if;
/*
insert into marketing_tracking_ets_fd
  (id, user_id, first_deposit_id)
values
  (SEQ_MARKETING_TRACKING_ETS_FD.NEXTVAL, :new.id, :new.first_deposit_id);
 */ 

    if :old.first_deposit_id is null
       and :new.first_deposit_id is not null
    then
    
      update users_regulation
      set    approved_regulation_step = 2
      where  user_id = :new.id
      and    approved_regulation_step < 2;
    
     
    end if;

end TRG_FIRST_DEP_USERS;
/
