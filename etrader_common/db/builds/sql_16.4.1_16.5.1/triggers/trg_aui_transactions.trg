create or replace trigger trg_aui_transactions
  after insert or update of status_id on transactions
  for each row
begin
  pkg_deposit.update_first_deposit_id(:new.id, :new.user_id, :new.status_id, :old.status_id, :new.type_id);
end trg_aui_transactions;
/
