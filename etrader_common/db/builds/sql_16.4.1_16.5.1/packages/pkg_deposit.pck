create or replace package pkg_deposit is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-06-05 10:41:22
  -- Purpose : 

  procedure update_first_deposit_id
  (
    i_transaction_id in number
   ,i_user_id        in number
   ,i_new_status_id  in number
   ,i_old_status_id  in number
   ,i_type_id        in number
   ,i_updating       boolean default false
   ,i_inserting      boolean default false
  );

end pkg_deposit;
/
create or replace package body pkg_deposit is

  procedure find_new_fdi
  (
    o_transaction_id out number
   ,i_user_id        in number
   ,i_transaction_id in number
  ) is
    pragma autonomous_transaction;
  begin
    select min(t.id)
    into   o_transaction_id
    from   transactions t
    where  t.user_id = i_user_id
    and    exists (select 1
            from   transaction_types tt
            where  tt.class_type = 1
            and    tt.id = t.type_id)
    and    t.status_id in (2, 7, 8)
    and    t.id != i_transaction_id;
  end find_new_fdi;

  procedure update_first_deposit_id
  (
    i_transaction_id in number
   ,i_user_id        in number
   ,i_new_status_id  in number
   ,i_old_status_id  in number
   ,i_type_id        in number
   ,i_updating       boolean default false
   ,i_inserting      boolean default false
  ) is
    l_new_transaction_id number;
  begin
    if (inserting or i_inserting)
       and i_new_status_id in (2, 7)
    then
    
      update users u
      set    u.first_deposit_id = i_transaction_id
      where  u.id = i_user_id
      and    u.first_deposit_id is null
      and    exists (select 1
              from   transaction_types tt
              where  tt.class_type = 1
              and    tt.id = i_type_id);
    
    elsif (updating or i_updating)
    then
    
      if i_new_status_id in (2, 7, 8)
         and i_old_status_id not in (2, 7, 8)
      then
      
        update users u
        set    u.first_deposit_id = i_transaction_id
        where  u.id = i_user_id
        and    (u.first_deposit_id is null or u.first_deposit_id > i_transaction_id)
        and    exists (select 1
                from   transaction_types tt
                where  tt.class_type = 1
                and    tt.id = i_type_id);
      
      elsif i_new_status_id not in (2, 7, 8)
            and i_old_status_id in (2, 7, 8)
      then
        --find_new_fdi(l_new_transaction_id, i_user_id, i_transaction_id);
      
        update users u
        set    u.first_deposit_id = l_new_transaction_id
        where  u.id = i_user_id
        and    u.first_deposit_id = i_transaction_id;
      
      end if;
    
    end if;
  end update_first_deposit_id;

end pkg_deposit;
/
