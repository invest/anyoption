create or replace package pkg_gcontext is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-06-03 17:38:47
  -- Purpose : Global context

  procedure set_value
  (
    i_name  in varchar2
   ,i_value in varchar2
  );

  function get_value(i_name in varchar2) return varchar2;

end pkg_gcontext;
/
create or replace package body pkg_gcontext is

  procedure set_value
  (
    i_name  in varchar2
   ,i_value in varchar2
  ) is
  begin
    dbms_session.set_context('AO_GCONTEXT', i_name, i_value);
  end set_value;

  function get_value(i_name in varchar2) return varchar2 is
  begin
    return sys_context('AO_GCONTEXT', i_name);
  end get_value;

end pkg_gcontext;
/
