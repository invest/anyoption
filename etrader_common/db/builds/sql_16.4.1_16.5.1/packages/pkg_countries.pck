create or replace package pkg_countries is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-25 11:48:54
  -- Purpose : 

  procedure load_countries(o_countries out sys_refcursor);

end pkg_countries;
/
create or replace package body pkg_countries is

  procedure load_countries(o_countries out sys_refcursor) is
  begin
  
    open o_countries for
      select c.id
            ,c.country_name
            ,c.a2
            ,c.a3
            ,c.phone_code
            ,c.display_name
            ,c.support_phone
            ,c.support_fax
            ,c.gmt_offset
            ,c.is_sms_available
            ,c.is_have_ukash_site
            ,c.is_display_pop_call_me
            ,c.is_display_pop_rnd
            ,c.is_display_pop_short_reg
            ,c.is_display_pop_reg_attempt
            ,c.is_alphanumeric_sender
            ,c.is_netrefer_reg_block
            ,c.countries_group_netrefer_id
            ,c.capital_city
            ,c.capital_city_latitude
            ,c.capital_city_longitude
            ,c.is_regulated
            ,c.is_blocked
      from   countries c
      where  c.a2 not in ('KP')
      order  by country_name;
  
  end load_countries;

end pkg_countries;
/
