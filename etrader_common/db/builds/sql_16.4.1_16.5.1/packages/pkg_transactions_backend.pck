create or replace package pkg_transactions_backend is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-03 12:25:41
  -- Purpose : Procedures used by backend

  -- %param o_transactions        
  -- %param i_fromdate               Filter by transactions.time_created or transactions.time_settled, depends on i_datetype
  -- %param i_todate                 Filter by transactions.time_created or transactions.time_settled, depends on i_datetype
  -- %param i_datetype               Filter by time_created(1) or time_settled(2)
  -- %param i_transaction_id 
  -- %param i_user_id 
  -- %param i_user_classes 
  -- %param i_business_cases 
  -- %param i_countries 
  -- %param i_currencies 
  -- %param i_compaigns 
  -- %param i_transaction_statuses 
  -- %param i_system_writers 
  -- %param i_has_balance            Returns only users with balance=0 (i_has_balance=0) or balance>0 (i_has_balance=1)
  -- %param i_clearing_providers 
  -- %param i_skins 
  -- %param i_page_number            Page number to be returned
  -- %param i_rows_per_page          Number of rows per page
  -- %param i_transaction_types 
  -- %param i_gateway  
  -- %param i_cc_bin 
  -- %param i_cc_last4               TODO; Last 4 digits of the credit card number. Must add new column to credit_cards or decrypt card number in DB.
  -- %param i_cc_number              Credit card number (encrypted)
  -- %param i_epg_id  
  -- %param i_trans_class_types      Combining Direct banking transaction types and payment methods ({17,2},{17,3},{17,4})
  -- %param i_receipt_number 
  -- %param i_j4postponed  
  procedure get_transactions
  (
    o_transactions         out sys_refcursor
   ,i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_user_classes         in number_table
   ,i_business_cases       in number_table
   ,i_countries            in number_table
   ,i_currencies           in number_table
   ,i_compaigns            in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_has_balance          in number
   ,i_clearing_providers   in number_table
   ,i_skins                in number_table
   ,i_page_number          in number
   ,i_rows_per_page        in number
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_cc_number            in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_types    in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
  );

  -- All cursors should return two columns with names ID, VALUE
  -- %param o_departments 
  -- %param o_platforms 
  -- %param o_skin_business_cases 
  -- %param o_user_classes 
  -- %param o_countries 
  -- %param o_currencies 
  -- %param o_transaction_statuses 
  -- %param o_transaction_class_types 
  -- %param o_clearing_providers 
  -- %param o_transaction_types 
  -- %param o_transaction_type_class 
  -- %param o_skins 
  -- %param o_compaigns 
  procedure load_filters
  (
    o_platforms               out sys_refcursor
   ,o_skin_business_cases     out sys_refcursor
   ,o_user_classes            out sys_refcursor
   ,o_countries               out sys_refcursor
   ,o_transaction_statuses    out sys_refcursor
   ,o_transaction_class_types out sys_refcursor
   ,o_clearing_providers      out sys_refcursor
   ,o_transaction_types       out sys_refcursor
   ,o_transaction_type_class  out sys_refcursor
   ,o_compaigns               out sys_refcursor
   ,o_system_writers          out sys_refcursor
  );

  -- %param i_transaction_id 
  -- %param i_writer_id 
  procedure cancel_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
   ,i_status_id      in number
  );

  procedure approve_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
  );

  -- %param i_writer_id 
  -- %param i_status_id 
  -- %param i_time_created 
  -- %param i_description 
  -- %param i_utc_offset_created 
  -- %param i_issue_id 
  -- %param i_arn 
  -- %param i_time_pos 
  -- %param i_time_chb 
  -- %param i_time_rep 
  -- %param i_time_settled 
  -- %param i_state 
  -- %param i_transaction_id 
  procedure create_chargeback
  (
    i_writer_id          in number
   ,i_status_id          in number
   ,i_time_created       in date
   ,i_description        in varchar2
   ,i_utc_offset_created in varchar2
   ,i_issue_id           in number
   ,i_arn                in varchar2
   ,i_time_pos           in date
   ,i_time_chb           in date
   ,i_time_rep           in date
   ,i_time_settled       in date
   ,i_state              in number
   ,i_transaction_id     in number
  );

  procedure manage_trans_postponed
  (
    i_ppn_id          in number
   ,i_ppn_num_of_days in number
   ,i_ppn_trans_id    in number
   ,i_writer_id       in number
  );
end pkg_transactions_backend;
/
create or replace package body pkg_transactions_backend is

  g_deposit_trns  number_table;
  g_withdraw_trns number_table;

  procedure get_transactions_final
  (
    o_transactions   out sys_refcursor
   ,i_user_classes   in number_table
   ,i_business_cases in number_table
   ,i_countries      in number_table
   ,i_currencies     in number_table
   ,i_compaigns      in number
   ,i_has_balance    in number
   ,i_skins          in number_table
   ,i_page_number    in number
   ,i_rows_per_page  in number
   ,i_cc_last4       in number
  ) is
  begin
    open o_transactions for
      select v.total_count
            ,v.sum_deposits
            ,v.sum_withdraws
            ,v.count_deposits
            ,v.count_withdraws
            ,v.transaction_id
            ,v.user_id
            ,v.credit_card_id
            ,v.type_id
            ,v.time_created
            ,v.amount
            ,v.status_id
            ,v.description
            ,v.writer_id
            ,v.ip
            ,v.time_settled
            ,v.comments
            ,v.processed_writer_id
            ,v.cheque_id
            ,v.reference_id
            ,v.wire_id
            ,v.charge_back_id
            ,v.receipt_num
            ,v.auth_number
            ,v.xor_id_authorize
            ,v.xor_id_capture
            ,v.fee_cancel
            ,v.is_accounting_approved
            ,v.utc_offset_settled
            ,v.utc_offset_created
            ,v.rate
            ,v.clearing_provider_id
            ,v.fee_cancel_by_admin
            ,v.credit_amount
            ,v.deposit_reference_id
            ,v.splitted_reference_id
            ,v.is_credit_withdrawal
            ,v.payment_type_id
            ,v.bonus_user_id
            ,v.paypal_email
            ,v.temp_bonus_user_id
            ,v.envoy_account_num
            ,v.internals_amount
            ,v.pixel_run_time
            ,v.moneybookers_email
            ,v.webmoney_purse
            ,v.update_by_admin
            ,v.acquirer_response_id
            ,v.is_rerouted
            ,v.rerouting_transaction_id
            ,v.is_manual_routing
            ,v.selector_id
            ,v.login_id
            ,v.wd_fee_exempt_writer_id
            ,v.rn
            ,w.user_name writer_name
            ,v.user_name
            ,v.user_country_id
            ,cu.country_name
            ,v.cc_number
            ,case
               when v.type_id member of g_deposit_trns then
                1
               when v.type_id member of g_withdraw_trns then
                2
               else
                0
             end deposit_or_withdraw
            ,ct.name cc_name
            ,ct.description cc_type_by_bin
            ,cn.country_name cc_country_name
            ,v.cc_is_allowed
            ,v.cc_utc_offset_created
            ,v.cc_utc_offset_modified
            ,v.cc_exp_year
            ,v.cc_holder_name
            ,v.cc_holder_id_num
            ,v.cc_time_modified
            ,v.cc_time_created
            ,v.cc_exp_month
            ,v.cc_type_id_by_bin
            ,v.first_name
            ,v.last_name
            ,bc.description business_case_desc
            ,v.user_skin_name
            ,v.user_currency_id
            ,po.id ppn_id
            ,po.number_of_days ppn_number_of_days
            ,po.writer_id ppn_writer_id
            ,po.time_updated ppn_time_updated
      from   (select row_number() over(order by t.time_created, t.id) rn
                    ,count(*) over() total_count
                    ,nvl(sum(case
                               when t.type_id member of g_deposit_trns then
                                t.amount * t.rate
                             end) over()
                        ,0) sum_deposits
                    ,nvl(sum(case
                               when t.type_id member of g_withdraw_trns then
                                t.amount * t.rate
                             end) over()
                        ,0) sum_withdraws
                    ,count(case
                             when t.type_id member of g_deposit_trns then
                              1
                           end) over() count_deposits
                    ,count(case
                             when t.type_id member of g_withdraw_trns then
                              1
                           end) over() count_withdraws
                    ,u.user_name
                    ,u.country_id user_country_id
                    ,d.cc_number
                    ,d.type_id cc_type_id
                    ,d.country_id cc_country_id
                    ,d.is_allowed cc_is_allowed
                    ,d.utc_offset_created cc_utc_offset_created
                    ,d.utc_offset_modified cc_utc_offset_modified
                    ,d.exp_year cc_exp_year
                    ,d.holder_name cc_holder_name
                    ,d.holder_id_num cc_holder_id_num
                    ,d.time_modified cc_time_modified
                    ,d.time_created cc_time_created
                    ,d.exp_month cc_exp_month
                    ,d.type_id_by_bin cc_type_id_by_bin
                    ,t.*
                    ,t.id transaction_id
                    ,u.first_name
                    ,u.last_name
                    ,s.business_case_id
                    ,s.name user_skin_name
                    ,u.currency_id user_currency_id
              from   gt_transactions t
              join   users u
              on     u.id = t.user_id
              join   skins s
              on     s.id = u.skin_id
              left   join credit_cards d
              on     d.id = t.credit_card_id
              where  (i_user_classes is null or u.class_id member of i_user_classes)
                    
              and    (i_business_cases is null or s.business_case_id member of i_business_cases)
                    
              and    (i_countries is null or u.country_id member of i_countries)
                    
              and    (i_currencies is null or u.currency_id member of i_currencies)
                    
              and    (i_compaigns is null or exists (select 1
                                                     from   marketing_combinations co
                                                     join   marketing_campaigns a
                                                     on     a.id = co.campaign_id
                                                     where  co.id = u.combination_id
                                                     and    a.id = i_compaigns))
                    
              and    (i_skins is null or u.skin_id member of i_skins)
                    
              and    (i_has_balance is null or ((i_has_balance = 1 and u.balance > 0) or (i_has_balance = 0 and u.balance = 0)))
              
              ) v
      join   writers w
      on     w.id = v.writer_id
      left   join credit_card_types ct
      on     ct.id = v.cc_type_id
      left   join countries cn
      on     cn.id = v.cc_country_id
      join   countries cu
      on     cu.id = v.user_country_id
      join   skin_business_cases bc
      on     bc.id = v.business_case_id
      left   join transaction_postponed po
      on     po.transaction_id = v.transaction_id
      where  v.rn > (i_page_number - 1) * i_rows_per_page
      and    v.rn <= i_page_number * i_rows_per_page
      order  by v.time_created, v.transaction_id;
  
  end get_transactions_final;

  procedure get_transactions_by_trnid_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_cc_number            in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.id = i_transaction_id
            
      and    (i_user_id is null or t.user_id = i_user_id)
            
      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate + 1) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate + 1))
            
      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_transaction_types is null or t.type_id member of i_transaction_types)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_cc_number is null or exists (select 1 from credit_cards where cc_number = i_cc_number))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_trnid_tmp;

  procedure get_transactions_by_userid_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_user_id              in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_cc_number            in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.user_id = i_user_id
            
      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate + 1) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate + 1))
            
      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_transaction_types is null or t.type_id member of i_transaction_types)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_cc_number is null or exists (select 1 from credit_cards where cc_number = i_cc_number))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_userid_tmp;

  procedure get_transactions_by_ccnum_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_cc_number            in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.credit_card_id in (select id from credit_cards where cc_number = i_cc_number)
            
      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate + 1) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate + 1))
            
      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_transaction_types is null or t.type_id member of i_transaction_types)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_ccnum_tmp;

  procedure get_transactions_by_status_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.status_id in (select /*+ cardinality(a,1) */
                              column_value
                             from   table(i_transaction_statuses) a)
            
      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate + 1) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate + 1))
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_transaction_types is null or t.type_id member of i_transaction_types)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_status_tmp;

  procedure get_transactions_by_type_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.type_id in (select /*+ cardinality(a,1) */
                            column_value
                           from   table(i_transaction_types) a)
            
      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate + 1) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate + 1))
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_type_tmp;

  procedure get_transactions_by_timec_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.time_created >= i_fromdate
      and    t.time_created < i_todate + 1
            
      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_transaction_types is null or t.type_id member of i_transaction_types)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_timec_tmp;

  procedure get_transactions_by_times_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_bits          in number
  ) is
  begin
  
    insert into gt_transactions
      select t.*
      from   (select m.*
              from   transactions m
              where  m.time_settled >= i_fromdate
              and    m.time_settled < i_todate + 1
              and    m.time_created >= i_fromdate - 20
              and    m.time_created < i_todate + 1
              and    m.time_settled - m.time_created <= 20
              union all
              select n.*
              from   transactions n
              where  case
                       when n.time_settled - n.time_created > 20 then
                        n.time_settled
                     end >= i_fromdate
              and    case
                      when n.time_settled - n.time_created > 20 then
                       n.time_settled
                    end < i_todate + 1) t
      
      where  (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)
            
      and    (i_system_writers is null or t.writer_id member of i_system_writers)
            
      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)
            
      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))
            
      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)
            
      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)
            
      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)
            
      and    (i_j4postponed is null
            
            or ((i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id))
            
            or (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id))))
            
      and    (i_search_bits = 0 or ((bitand(i_search_bits, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_bits, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_bits, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));
  
  end get_transactions_by_times_tmp;

  procedure get_transactions
  (
    o_transactions         out sys_refcursor
   ,i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_user_classes         in number_table
   ,i_business_cases       in number_table
   ,i_countries            in number_table
   ,i_currencies           in number_table
   ,i_compaigns            in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_has_balance          in number
   ,i_clearing_providers   in number_table
   ,i_skins                in number_table
   ,i_page_number          in number
   ,i_rows_per_page        in number
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in number
   ,i_cc_number            in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_types    in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
  ) is
    l_noidx_trn_types      number_table := number_table(1, 13, 10, 12, 16, 47, 6, 17, 38, 14, 32, 2, 18, 33); -- if trns.type_id is in this list, do not use index
    l_noidx_trn_statuses   number_table := number_table(2, 3, 6); -- if trns.status_id is in this list, do not use index
    l_tmp_types            number_table;
    l_tmp_statuses         number_table;
    l_trans_class_internal number_table;
    l_transaction_types    number_table;
    l_search_bits          number := 0;
    l_tmp                  number;
  begin
    select count(*) into l_tmp from gt_transactions where rownum <= 1;
    if l_tmp > 0
    then
      raise_application_error(-20010, 'Global temporary table not empty.');
    end if;
  
    if i_trans_class_types is not null
    then
      select id bulk collect into l_trans_class_internal from transaction_types where class_type member of i_trans_class_types;
    end if;
  
    if i_transaction_types is not null
    then
      l_transaction_types := number_table();
    
      -- this is to extract fake negative transaction types. This hack was introduced to be able to filter
      -- types "Direct 24", "Giropay", "Direct EPS"      
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=2 PAYMENT_TYPE_DIRECT24 from PAYMENT_METHODS
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=3 PAYMENT_TYPE_GIROPAY from PAYMENT_METHODS
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=4 PAYMENT_TYPE_EPS from PAYMENT_METHODS    
      for i in 1 .. i_transaction_types.count
      loop
        if i_transaction_types(i) > 0
        then
          if l_transaction_types is null
          then
            l_transaction_types := number_table(i_transaction_types(i));
          else
            l_transaction_types.extend;
            l_transaction_types(l_transaction_types.last) := i_transaction_types(i);
          end if;
        else
          case i_transaction_types(i)
            when -172 then
              l_search_bits := l_search_bits + 1;
            when -173 then
              l_search_bits := l_search_bits + 2;
            when -174 then
              l_search_bits := l_search_bits + 4;
          end case;
        end if;
      end loop;
    end if;
  
    l_tmp_types    := l_transaction_types multiset intersect l_noidx_trn_types;
    l_tmp_statuses := i_transaction_statuses multiset intersect l_noidx_trn_statuses;
  
    if i_transaction_id is not null
    then
      -- will access transactions table by its PK
      get_transactions_by_trnid_tmp(i_fromdate
                                   ,i_todate
                                   ,i_datetype
                                   ,i_transaction_id
                                   ,i_user_id
                                   ,i_transaction_statuses
                                   ,i_system_writers
                                   ,i_clearing_providers
                                   ,i_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_cc_number
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_bits);
    elsif i_user_id is not null
    then
      -- will access transactions table by index on user_id
      get_transactions_by_userid_tmp(i_fromdate
                                    ,i_todate
                                    ,i_datetype
                                    ,i_user_id
                                    ,i_transaction_statuses
                                    ,i_system_writers
                                    ,i_clearing_providers
                                    ,i_transaction_types
                                    ,i_gateway
                                    ,i_cc_bin
                                    ,i_cc_last4
                                    ,i_cc_number
                                    ,i_epg_id
                                    ,l_trans_class_internal
                                    ,i_receipt_number
                                    ,i_j4postponed
                                    ,l_search_bits);
    elsif i_cc_number is not null
    then
      -- will access transactions table by index on credit_card_id
      get_transactions_by_ccnum_tmp(i_fromdate
                                   ,i_todate
                                   ,i_datetype
                                   ,i_transaction_statuses
                                   ,i_system_writers
                                   ,i_clearing_providers
                                   ,i_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_cc_number
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_bits);
    elsif l_tmp_statuses is not null
          and l_tmp_statuses.count = 0
    then
      -- will access transactions table by index on status_id
      get_transactions_by_status_tmp(i_fromdate
                                    ,i_todate
                                    ,i_datetype
                                    ,i_transaction_statuses
                                    ,i_system_writers
                                    ,i_clearing_providers
                                    ,i_transaction_types
                                    ,i_gateway
                                    ,i_cc_bin
                                    ,i_cc_last4
                                    ,i_epg_id
                                    ,l_trans_class_internal
                                    ,i_receipt_number
                                    ,i_j4postponed
                                    ,l_search_bits);
    elsif l_tmp_types is not null
          and l_tmp_types.count = 0
    then
      -- will access transactions table by index on type_id
      get_transactions_by_type_tmp(i_fromdate
                                  ,i_todate
                                  ,i_datetype
                                  ,i_system_writers
                                  ,i_clearing_providers
                                  ,i_transaction_types
                                  ,i_gateway
                                  ,i_cc_bin
                                  ,i_cc_last4
                                  ,i_epg_id
                                  ,l_trans_class_internal
                                  ,i_receipt_number
                                  ,i_j4postponed
                                  ,l_search_bits);
    elsif i_datetype = 1
    then
      -- will access transactions table by partition key on time_created
      get_transactions_by_timec_tmp(i_fromdate
                                   ,i_todate
                                   ,i_transaction_statuses
                                   ,i_system_writers
                                   ,i_clearing_providers
                                   ,i_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_bits);
    else
      -- will access transactions table by partition key on time_created + index on time_settled
      get_transactions_by_times_tmp(i_fromdate
                                   ,i_todate
                                   ,i_transaction_statuses
                                   ,i_system_writers
                                   ,i_clearing_providers
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_bits);
    end if;
  
    get_transactions_final(o_transactions
                          ,i_user_classes
                          ,i_business_cases
                          ,i_countries
                          ,i_currencies
                          ,i_compaigns
                          ,i_has_balance
                          ,i_skins
                          ,i_page_number
                          ,i_rows_per_page
                          ,i_cc_last4);
  
  end get_transactions;

  procedure load_filters
  (
    o_platforms               out sys_refcursor
   ,o_skin_business_cases     out sys_refcursor
   ,o_user_classes            out sys_refcursor
   ,o_countries               out sys_refcursor
   ,o_transaction_statuses    out sys_refcursor
   ,o_transaction_class_types out sys_refcursor
   ,o_clearing_providers      out sys_refcursor
   ,o_transaction_types       out sys_refcursor
   ,o_transaction_type_class  out sys_refcursor
   ,o_compaigns               out sys_refcursor
   ,o_system_writers          out sys_refcursor
  ) is
  begin
    open o_platforms for
      select p.id, p.name value from platforms p order by p.id;
  
    open o_skin_business_cases for
      select c.id, c.description value from skin_business_cases c order by c.id;
  
    open o_user_classes for
      select c.id, c.name value from user_classes c order by c.id;
  
    open o_countries for
      select c.id, c.display_name value from countries c order by c.id;
  
    open o_transaction_statuses for
      select ts.id, ts.description value from transaction_statuses ts order by ts.id;
  
    open o_transaction_class_types for
      select tct.id, tct.name value from transaction_class_types tct order by tct.id;
  
    open o_clearing_providers for
      select p.id, p.name value
      from   clearing_providers p
      where  p.is_active = 1
      and    p.provider_class not in ('com.anyoption.common.clearing.EPGClearingProvider')
      order  by p.name;
  
    open o_transaction_types for
      select t.id, t.description value from transaction_types t order by t.id;
  
    open o_transaction_type_class for
      select t.id, t.class_type value from transaction_types t order by t.id;
  
    open o_compaigns for
      select m.id, m.name value from marketing_campaigns m order by m.id;
  
    open o_system_writers for
      select w.id, w.user_name value
      from   writers w
      where  w.dept_id = 9
      and    w.user_name not in ('AUTO', 'KEESING')
      order  by w.id;
  end load_filters;

  procedure cancel_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
   ,i_status_id      in number
  ) is
    l_transaction_id     transactions.id%type;
    l_transaction_amount transactions.amount%type;
    l_balance            users.balance%type;
    l_tax_balance        users.tax_balance%type;
    l_user_id            users.id%type;
    l_status_id          transactions.status_id%type;
    l_utc_offset         users.utc_offset%type;
    l_sysdate            date := sysdate;
  begin
    --  will check if the status of the transaction is not final
    --  (2, 3, 5, 6, 8, 12, 13, 14, 15, 16) are final statuses. 
    --  We are not supposed to change those once they are set.   
  
    update transactions t
    set    t.status_id           = i_status_id
          ,t.time_settled        = l_sysdate
          ,t.utc_offset_settled =
           (select u.utc_offset from users u where u.id = t.user_id)
          ,t.processed_writer_id = i_writer_id
    where  t.id = i_transaction_id
    and    t.status_id not in (2, 3, 5, 6, 8, 12, 13, 14, 15, 16)
    returning t.amount, t.id, t.user_id into l_transaction_amount, l_transaction_id, l_user_id;
  
    if sql%found
    then
      update users u
      set    u.balance = u.balance + l_transaction_amount, u.time_modified = l_sysdate, u.utc_offset_modified = u.utc_offset
      where  u.id = l_user_id
      returning u.balance, u.tax_balance, u.utc_offset into l_balance, l_tax_balance, l_utc_offset;
    
      insert into balance_history
        (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
      values
        (seq_balance_history.nextval
        ,l_sysdate
        ,l_user_id
        ,l_balance
        ,l_tax_balance
        ,i_writer_id
        ,'transactions'
        ,l_transaction_id
        ,2
        ,l_utc_offset);
    else
      -- transaction was not updated
      select t.status_id into l_status_id from transactions t where t.id = i_transaction_id;
    
      raise_application_error(-20001, 'Transaction status is final(?): ' || l_status_id);
    end if;
  
  exception
    when no_data_found then
      raise_application_error(-20000, 'No such transaction: ' || i_transaction_id);
  end cancel_deposit;

  -- called to approve wired transactions (?)
  procedure approve_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
  ) is
    l_sysdate   date := sysdate;
    l_status_id transactions.status_id%type;
  begin
    update transactions t
    set    t.status_id           = 2
          ,t.time_settled        = l_sysdate
          ,t.utc_offset_settled =
           (select u.utc_offset from users u where u.id = t.user_id)
          ,t.processed_writer_id = i_writer_id
    where  t.id = i_transaction_id
    and    t.status_id not in (2, 3, 5, 6, 8, 12, 13, 14, 15, 16);
  
    if sql%notfound
    then
      -- transaction was not updated
      select t.status_id into l_status_id from transactions t where t.id = i_transaction_id;
    
      raise_application_error(-20003, 'Transaction status is final(?): ' || l_status_id);
    end if;
  
  exception
    when no_data_found then
      raise_application_error(-20002, 'No such transaction: ' || i_transaction_id);
  end approve_deposit;

  procedure create_chargeback
  (
    i_writer_id          in number
   ,i_status_id          in number
   ,i_time_created       in date
   ,i_description        in varchar2
   ,i_utc_offset_created in varchar2
   ,i_issue_id           in number
   ,i_arn                in varchar2
   ,i_time_pos           in date
   ,i_time_chb           in date
   ,i_time_rep           in date
   ,i_time_settled       in date
   ,i_state              in number
   ,i_transaction_id     in number
  ) is
    l_charge_back_id   number;
    l_user_id          number;
    l_sysdate          date := sysdate;
    l_amount_to_return number;
    l_user_balance     number;
    l_user_tax_balance number;
    l_user_utc_offset  users.utc_offset%type;
  begin
    -- in legacy code the 4 date fields were inserted without time part
    insert into charge_backs
      (id
      ,writer_id
      ,status_id
      ,time_created
      ,description
      ,utc_offset_created
      ,issue_id
      ,arn
      ,time_pos
      ,time_chb
      ,time_rep
      ,time_settled
      ,state)
    values
      (seq_charge_backs.nextval
      ,i_writer_id
      ,i_status_id
      ,i_time_created
      ,i_description
      ,i_utc_offset_created
      ,i_issue_id
      ,i_arn
      ,trunc(i_time_pos)
      ,trunc(i_time_chb)
      ,trunc(i_time_rep)
      ,trunc(i_time_settled)
      ,i_state)
    returning id into l_charge_back_id;
  
    update transactions
    set    charge_back_id = l_charge_back_id
          ,status_id     =
           (select id from transaction_statuses where code = 'TRANS_STATUS_CHARGE_BACK')
    where  id = i_transaction_id
    returning user_id, amount into l_user_id, l_amount_to_return;
  
    if sql%found
    then
      if i_state = 2
      then
        update users
        set    balance = balance - l_amount_to_return, time_modified = l_sysdate, utc_offset_modified = utc_offset
        where  id = l_user_id
        returning balance, tax_balance, utc_offset into l_user_balance, l_user_tax_balance, l_user_utc_offset;
      
        insert into balance_history
          (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
        values
          (seq_balance_history.nextval
          ,l_sysdate
          ,l_user_id
          ,l_user_balance
          ,l_user_tax_balance
          ,i_writer_id
          ,'transactions'
          ,i_transaction_id
          ,(select to_number(code)
           from   enumerators
           where  enumerator = 'log_balance'
           and    value = 'log.balance.charge.back')
          ,l_user_utc_offset);
      end if;
    else
      raise_application_error(-20005, 'Transaction does not exists: ' || i_transaction_id);
    end if;
  end create_chargeback;

  procedure manage_trans_postponed
  (
    i_ppn_id          in number
   ,i_ppn_num_of_days in number
   ,i_ppn_trans_id    in number
   ,i_writer_id       in number
  ) is
    l_sysdate date := sysdate;
  begin
  
    if i_ppn_id is null
    then
      insert into transaction_postponed
        (id, time_created, number_of_days, writer_id, time_updated, transaction_id)
      values
        (seq_transaction_postponed.nextval, l_sysdate, i_ppn_num_of_days, i_writer_id, l_sysdate, i_ppn_trans_id);
    
    else
    
      update transaction_postponed
      set    number_of_days = i_ppn_num_of_days, writer_id = i_writer_id, time_updated = l_sysdate
      where  id = i_ppn_id;
    
      if sql%notfound
      then
        raise_application_error(-20007, 'Record to be updated does not exists: ' || i_ppn_id);
      end if;
    end if;
  
  end manage_trans_postponed;

begin

  select t.id
  bulk   collect
  into   g_deposit_trns
  from   transaction_types t
  join   transaction_class_types c
  on     c.id = t.class_type
  where  c.code in ('C_TYPE_REAL_DEPOSITS')
  and    t.code not in ('TRANS_TYPE_REVERSE_WITHDRAW');

  select t.id
  bulk   collect
  into   g_withdraw_trns
  from   transaction_types t
  join   transaction_class_types c
  on     c.id = t.class_type
  where  (c.code in ('C_TYPE_REAL_WITHDRAWALS', 'C_TYPE_ADMIN_WITHDRAWALS') or
         (c.code in ('C_TYPE_FIX_BALANCE') and t.code in ('TRANS_TYPE_FIX_BALANCE_WITHDRAW')));

end pkg_transactions_backend;
/
