
create or replace PACKAGE "PKG_WRITERS_COMMISSION_INV" is
        
  PROCEDURE PRC_WRITERS_COMMISSION_INV;
    
  PROCEDURE PRC_WRITER_COMMISSION_INV_EXAM(i_last_run in DATE, i_start_time in DATE, i_investment_id in NUMBER);
  
End PKG_WRITERS_COMMISSION_INV;
/

create or replace PACKAGE BODY "PKG_WRITERS_COMMISSION_INV" is

  FUNCTION FUN_IS_EXISTS_IN_WCI(i_investment_id NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    l_is_exists NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('FUN_IS_EXISTS_IN_WCI START');
    select
      1
    into
      l_is_exists
    from
      writers_commission_inv wci
    where
      wci.INVESTMENTS_ID = i_investment_id;
    
    DBMS_OUTPUT.PUT_LINE('FUN_IS_EXISTS_IN_WCI END');
    RETURN l_is_exists;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('FUN_IS_EXISTS_IN_WCI END');
      RETURN l_is_exists;
  END FUN_IS_EXISTS_IN_WCI;
  
  FUNCTION FUN_GET_COMMISSION_INVESTMENTS(i_last_run DATE, i_start_time DATE, i_investment_id NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** Declaration_section ***
    c_data SYS_REFCURSOR;
    l_ISSUE_CHANNEL_CALL NUMBER := 1;
    l_ISSUE_REACHED_STATUS_REACHED NUMBER := 2;
    l_IAT_DST_ONLINE NUMBER := 1;
    l_IAT_DST_TRACKING NUMBER := 2;
    l_INTERVAL_DAYS_FOR_ACTIONS NUMBER := 30;
    l_pehs_id NUMBER := 6;
    l_pehs_is_cancel_assign NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('FUN_GET_COMMISSION_INVESTMENTS START');
    
    OPEN 
      c_data 
    FOR
      SELECT /*+ no_expand full(inv) cardinality(inv, 100) */ 
        inv.id as inv_id,
        inv.user_id as user_id,
        (SELECT 
          la.T_assigned_writer_id
        FROM 
          TABLE(WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(pu.id, inv.TIME_CREATED, 0)) la),
        rtf.FACTOR
      FROM 
        investments inv,
        users u,
        population_users pu,
        opportunities opt,
        RETENTION_TURNOVER_FACTOR rtf
      WHERE
        inv.user_id = u.id
        and u.id = pu.user_id
        and inv.OPPORTUNITY_ID = opt.id
        and opt.OPPORTUNITY_TYPE_ID = rtf.OPPORTUNITY_TYPE_ID
        and inv.IS_CANCELED = 0
        and (i_investment_id = 0 or inv.id = i_investment_id)
        and inv.time_created > i_last_run - 1/48
        and inv.time_created <= i_start_time - 1/48
        -- Get only investments which have issue actions with some conditions (The conditions will describe below)
        and exists (SELECT 
                      1 
                    FROM
                      issues i,
                      issue_actions ia,
                      issue_action_types iat,
                      TABLE(WRITERS_COMMISSION.GET_LAST_ASSIGNED_WRITER(pu.id, inv.TIME_CREATED, 0)) last_assign
                    WHERE
                      i.id = ia.issue_id
                      and ia.issue_action_type_id = iat.id
                      and i.user_id = u.ID
                      and iat.channel_id = l_ISSUE_CHANNEL_CALL
                      and iat.reached_status_id = l_ISSUE_REACHED_STATUS_REACHED
                      -- Get only issue actions which is positive and made in the 'INTERVAL_DAYS_FOR_ACTIONS' days prior to the investment's time created.
                      and iat.DEPOSIT_SEARCH_TYPE in (l_IAT_DST_ONLINE, l_IAT_DST_TRACKING)
                      and ia.action_time between (inv.TIME_CREATED - l_INTERVAL_DAYS_FOR_ACTIONS) and inv.TIME_CREATED
                      -- Get only issue actions which made by the last assign writer
                      and ia.writer_id = last_assign.T_assigned_writer_id
                      -- Get only issue action which doesn't have cancel assign between issue action's time created to investment's time created.
                      and NOT EXISTS (select 
                                        1
                                      from 
                                        population_entries pe, 
                                        population_entries_hist peh,
                                        population_entries_hist_status pehs
                                      where 
                                        pe.id = peh.population_entry_id
                                        and peh.status_id = pehs.id
                                        and pe.population_users_id = pu.ID
                                        and peh.time_created <= inv.TIME_CREATED
                                        and peh.time_created >= ia.ACTION_TIME
                                        and (pehs.id = l_pehs_id OR pehs.is_cancel_assign = l_pehs_is_cancel_assign)))
        -- Only investments which their amount include cash money will get into attribution logic.
        and (inv.amount - nvl((select sum(bi.bonus_amount)
                             from   bonus_investments bi
                             where  bi.investments_id = inv.id
                             and    bi.type_id = 1)
                            ,0) - nvl(inv.insurance_amount_gm, 0) - nvl(inv.insurance_amount_ru, 0) -
             nvl(inv.option_plus_fee, 0)) > 0;
        
    DBMS_OUTPUT.PUT_LINE('FUN_GET_COMMISSION_INVESTMENTS END');
    RETURN c_data;
  END FUN_GET_COMMISSION_INVESTMENTS;
    
  FUNCTION FUN_INVESTMENT_HANDLER(i_last_run DATE, i_start_time DATE)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    l_count_commission_investments NUMBER := 0;
    c_data SYS_REFCURSOR;
    l_info_inv_id investments.id %TYPE;
    l_info_user_id investments.user_id %TYPE;
    l_info_assign_writer_id NUMBER;
    l_investment_id NUMBER := 0;
    l_factor NUMBER := 0;
    
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('FUN_INVESTMENT_HANDLER START');
    c_data := FUN_GET_COMMISSION_INVESTMENTS(i_last_run, i_start_time, l_investment_id);
    
    -- Loop through the resulting data
    IF (c_data IS NOT NULL)
    THEN
        LOOP
            FETCH c_data INTO l_info_inv_id, l_info_user_id, l_info_assign_writer_id, l_factor;
            EXIT WHEN c_data%NOTFOUND;
            -- INSERT INTO writers_commission_inv
            DBMS_OUTPUT.PUT_LINE('About to insert into writers_commission_inv, User id: '||l_info_user_id||', Investment id: '||l_info_inv_id||', assigned writer id: '||l_info_assign_writer_id||', factor: '|| l_factor);
            INSERT INTO writers_commission_inv(id, INVESTMENTS_ID, writer_id , FACTOR, time_created)
              VALUES (SEQ_WRITERS_COMMISSION_INV.nextval, l_info_inv_id ,l_info_assign_writer_id, l_factor, sysdate);
            -- count commission investments
            l_count_commission_investments := l_count_commission_investments + 1;
        END LOOP;
        CLOSE c_data;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('FUN_INVESTMENT_HANDLER END');
    RETURN l_count_commission_investments;
  END FUN_INVESTMENT_HANDLER;
  
  
  FUNCTION FUN_INVESTMENT_HANDLER_CHECK(i_last_run DATE, i_start_time DATE, i_investment_id NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    l_count_commission_investments NUMBER := 0;
    c_data SYS_REFCURSOR;
    l_info_inv_id investments.id %TYPE;
    l_info_user_id investments.user_id %TYPE;
    l_info_assign_writer_id NUMBER;
    l_is_exists_in_wci NUMBER;
    l_data_found BOOLEAN := FALSE;
    l_factor NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('FUN_INVESTMENT_HANDLER_CHECK START');
    l_is_exists_in_wci := FUN_IS_EXISTS_IN_WCI(i_investment_id);
    IF l_is_exists_in_wci > 0 THEN 
      DBMS_OUTPUT.PUT_LINE('The investment: '||i_investment_id||' exists in the table writers_commission_inv');
      l_count_commission_investments := l_count_commission_investments + 1;
      RETURN l_count_commission_investments;
    END IF;
    
    c_data := FUN_GET_COMMISSION_INVESTMENTS(i_last_run, i_start_time, i_investment_id);    
    
    -- Loop through the resulting data
    IF (c_data IS NOT NULL)
    THEN
        LOOP
            FETCH c_data INTO l_info_inv_id, l_info_user_id, l_info_assign_writer_id, l_factor;
            EXIT WHEN c_data%NOTFOUND;
            -- The info which supposed to insert into writers_commission_inv
            DBMS_OUTPUT.PUT_LINE('The info which supposed to insert into writers_commission_inv:');
            DBMS_OUTPUT.PUT_LINE('User id: '||l_info_user_id||', Investment id: '||l_info_inv_id||', assigned writer id: '||l_info_assign_writer_id||', factor: '||l_factor);
            l_data_found := TRUE;
            -- count commission investments
            l_count_commission_investments := l_count_commission_investments + 1;
        END LOOP;
        CLOSE c_data;
    END IF;
    
    IF NOT l_data_found THEN
      DBMS_OUTPUT.PUT_LINE('No data found which supposed to insert into writers_commission_inv.');
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('FUN_INVESTMENT_HANDLER_CHECK END');
    RETURN l_count_commission_investments;
  END FUN_INVESTMENT_HANDLER_CHECK;

  PROCEDURE PRC_WRITERS_COMMISSION_INV AS
    l_last_run DATE;
    l_start_time DATE;
    l_db_parameter_id NUMBER := 13;
    l_count_investments NUMBER;
    l_db_parameter_updated NUMBER;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    DBMS_OUTPUT.PUT_LINE('PRC_WRITERS_COMMISSION_INV started at: ' || to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    
    -- Get last time which procedure was running.
    l_last_run := WRITERS_COMMISSION.GET_DATE_FROM_DB_PARAMETERS(l_db_parameter_id);
    DBMS_OUTPUT.PUT_LINE('The last time which the procedure was running, l_last_run: ' || to_char(l_last_run, 'yyyy-mm-dd hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('---');
    
    -- Get current time.
    l_start_time := sysdate;
    DBMS_OUTPUT.PUT_LINE('Current time, l_start_time: ' || to_char(l_start_time, 'yyyy-mm-dd hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('---');

    l_count_investments := FUN_INVESTMENT_HANDLER(l_last_run, l_start_time);
    DBMS_OUTPUT.PUT_LINE('Count number of commission investments which added to the table WRITERS_COMMISSION_INV, l_count_investments: ' || l_count_investments);
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- Update DB_PARAMETERS with procedure's start time.
    l_db_parameter_updated := WRITERS_COMMISSION.UPDATE_DB_PARAMETERS(l_db_parameter_id, l_start_time);

    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    DBMS_OUTPUT.PUT_LINE('PRC_WRITERS_COMMISSION_INV finished at: ' || to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  END PRC_WRITERS_COMMISSION_INV;

  PROCEDURE PRC_WRITER_COMMISSION_INV_EXAM(i_last_run in DATE, i_start_time in DATE, i_investment_id in NUMBER) AS
    l_count_investments NUMBER;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    DBMS_OUTPUT.PUT_LINE('PRC_WRITER_COMMISSION_INV_EXAM started at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    
    -- FUN_INVESTMENT_HANDLER_CHECK
    l_count_investments := FUN_INVESTMENT_HANDLER_CHECK(i_last_run, i_start_time, i_investment_id);
    DBMS_OUTPUT.PUT_LINE('Count number of commission investments which supposed to add into the table WRITERS_COMMISSION_INV, l_count_investments: ' || l_count_investments);
    DBMS_OUTPUT.PUT_LINE(' ');

    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
    DBMS_OUTPUT.PUT_LINE('PRC_WRITER_COMMISSION_INV_EXAM finished at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  END PRC_WRITER_COMMISSION_INV_EXAM;
  
END PKG_WRITERS_COMMISSION_INV;
/
