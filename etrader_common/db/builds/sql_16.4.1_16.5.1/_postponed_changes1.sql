
whenever sqlerror exit

spool _postponed_changes1.log

----------------------------------------------------------------
--  AR-1741 - BE - Display investments on the sales deposit screen
--  Victor Slavov
----------------------------------------------------------------

begin
  pkg_release.ddl_helper('drop index WRITERS_COM_SUP', -1418);
end;
/
  
begin
  pkg_release.ddl_helper('drop index WRITERS_COMMISSION_INV_TMCR', -1418);
end;
/

begin
  pkg_release.ddl_helper('drop index WRITERS_COMMISSION_INV_DT', -1418);
end;
/

begin
  dbms_redefinition.can_redef_table(user, 'WRITERS_COMMISSION_INV');
end;
/

declare
  l_mindate date;
begin
  select add_months(nvl(trunc(min(time_created), 'mm'), trunc(sysdate, 'mm')), 1) into l_mindate from writers_commission_inv;

  pkg_release.ddl_helper('
    create table writers_commission_inv_i
    (
       id             number not null
      ,writer_id      number not null
      ,investments_id number not null
      ,time_created   date   not null
      ,factor         number
    ) partition by range (time_created)
    interval (numtoyminterval(1, ''MONTH''))
    (partition p_wcmi_first values less than (to_date(' || to_char(l_mindate, 'yyyymm') || ', ''yyyymm'')))');
end;
/

                       
begin
  dbms_redefinition.start_redef_table(user
                                     ,'WRITERS_COMMISSION_INV'
                                     ,'WRITERS_COMMISSION_INV_I'
                                     ,col_mapping => '
  ID                ID,
  INVESTMENTS_ID    INVESTMENTS_ID,
  WRITER_ID         WRITER_ID,
  FACTOR            FACTOR,
  TIME_CREATED      TIME_CREATED  
  ');
end;
/

declare
  l_errs number;
begin
  dbms_redefinition.copy_table_dependents(user
                                         ,'WRITERS_COMMISSION_INV'
                                         ,'WRITERS_COMMISSION_INV_I'
                                         ,copy_indexes              => 0
                                         ,copy_triggers             => false
                                         ,copy_constraints          => false
                                         ,copy_privileges           => true
                                         ,copy_statistics           => false
                                         ,num_errors                => l_errs);
end;
/

alter table writers_commission_inv_i add constraint pk_writers_commission_inv primary key (id);

begin
  dbms_stats.set_table_prefs(user, 'WRITERS_COMMISSION_INV_I', 'incremental', 'true');
  dbms_stats.gather_table_stats(user, 'WRITERS_COMMISSION_INV_I', degree => 4);
end;
/

begin
  dbms_redefinition.finish_redef_table(user, 'WRITERS_COMMISSION_INV', 'WRITERS_COMMISSION_INV_I');
end;
/

drop table writers_commission_inv_i;

create index idx_fk_wcmi_invs on writers_commission_inv (investments_id) online;

alter table writers_commission_inv add constraint fk_wcmi_invs foreign key (investments_id) references investments(id) enable novalidate;

begin
  pkg_release.ddl_helper('alter table writers_commission_inv modify constraint fk_wcmi_invs validate', -2298);
end;
/

comment on table writers_commission_inv is '[WCMI] mapping between writers and investments';

alter table bonus_investments modify (bonus_amount constraint nn_binv_amount not null enable novalidate);

begin
  execute immediate 'create index idx_fk_binv_invs on bonus_investments (investments_id) online compress';
exception
  when others then
    if sqlcode = -1408
    then
      execute immediate 'alter index idx_bonusinv_invid rename to idx_fk_binv_invs';
    else
      raise;
    end if;
end;
/

begin
  execute immediate 'alter table bonus_investments add constraint fk_binv_invs foreign key (investments_id) references investments (id) enable novalidate';
exception
  when others then
    if sqlcode = -2275
    then
      execute immediate 'alter table bonus_investments rename constraint investment_id_fkp to fk_binv_invs';
    else
      raise;
    end if;
end;
/

@packages/pkg_writers_comm_reports.pck

grant execute on pkg_writers_comm_reports to etrader_web_connect;
create or replace public synonym pkg_writers_comm_reports for pkg_writers_comm_reports;

alter table bonus_investments modify constraint fk_binv_invs validate;
alter table bonus_investments modify constraint nn_binv_amount validate;

----------------------------------------------------------------
--  END AR-1741 - BE - Display investments on the sales deposit screen
--  Victor Slavov
----------------------------------------------------------------

-----------------------------------------------------------------------
-- AR-1850 [server] - BE - Investment attribution logics
-- Eyal O
-----------------------------------------------------------------------

-- drop old logic
drop procedure WRITERS_COMMISSION_INV_HANDLER;
drop public synonym WRITERS_COMMISSION_INV_HANDLER;
drop procedure WRITERS_COMMISSION_INV_CHECK;
drop public synonym WRITERS_COMMISSION_INV_CHECK;

@packages/pkg_writers_commission_inv.pck

-- Grant to the new logic
grant EXECUTE on PKG_WRITERS_COMMISSION_INV to ETRADER_WEB;
create public synonym PKG_WRITERS_COMMISSION_INV for PKG_WRITERS_COMMISSION_INV;

-----------------------------------------------------------------------
-- AR-1850 [server] - BE - Investment attribution logics
-- Eyal O
-----------------------------------------------------------------------

--- AR-1851 Investment attribution logics; Victor
create table temp_vic_bkp_roles 
as 
select * from roles;

delete from roles
where  id in (select id from (select t.*, row_number() over(partition by user_name, role order by id desc) rn from roles t) where rn > 1);

commit;

alter table roles add constraint uk_roles unique (user_name, role);
alter table roles modify (user_name not null, role not null);

@packages/pkg_writers_legacy.pck

grant execute on pkg_writers_legacy to etrader_web_connect;
create or replace public synonym pkg_writers_legacy for pkg_writers_legacy;

--- END; AR-1851 Investment attribution logics; Victor

spool off
