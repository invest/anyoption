----------------------------------------------------------------
--  for tracking release of DB changes
--  Victor Slavov
----------------------------------------------------------------

create table release_db_properties
(
   key        varchar2(50)  not null
  ,value      varchar2(200) not null
  ,constraint pk_release_db_properties primary key (key)
  ,constraint cc_rdbp_key check (key = lower(key))
);

comment on table release_db_properties is '[RDBP] System table to keep various properties of the database';

insert into release_db_properties (key, value) values ('db_version', '16.3.1');
commit;

create table release_db_version_history
(
   curr_version     varchar2(16)  not null
  ,prev_version     varchar2(16)  not null
  ,upload_date      date          not null
  ,constraint pk_db_version_history primary key (curr_version)
);

comment on table release_db_version_history is '[RDVH] System table to keep version history of the database';

insert into release_db_version_history (curr_version, prev_version, upload_date) values ('16.3.1', '16.2.1', sysdate - 60);
commit;

create table release_execution_log 
( 
   execute_time     timestamp(6) not null, 
   sql_statement    clob         not null, 
   ignore_exception number
);

comment on table release_execution_log is '[RELG] Logs executions of release scripts';

@packages/pkg_release.pck

----------------------------------------------------------------
--  END for tracking release of DB changes
--  Victor Slavov
----------------------------------------------------------------
