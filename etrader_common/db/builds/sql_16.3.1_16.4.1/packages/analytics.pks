-------- Create package for ANALYTICS --------
create or replace PACKAGE "ANALYTICS" is     
	FUNCTION GET_ANALYTICS_DEPOSITS    
	RETURN ANALYTICS_DEPOSITS_TBL;     
End ANALYTICS;

-------- Create package BODY for ANALYTICS --------
create or replace PACKAGE BODY         "ANALYTICS" is

FUNCTION GET_ANALYTICS_DEPOSITS 
RETURN ANALYTICS_DEPOSITS_TBL
AS
    v_return  ANALYTICS_DEPOSITS_TBL;
    F_USER_CLASS_TEST NUMBER := 0;
    F_USER_SKIN_ETRADER NUMBER := 1;
    F_TRANS_CLASS_DEPOSIT NUMBER := 1;
    F_TRANS_STATUS_SUCCEED NUMBER := 2;
    F_TRANS_STATUS_PENDING NUMBER := 7;
    F_TRANS_STATUS_CANCELED_ET NUMBER := 8;
    F_TRANS_TYPE_CC_DEPOSIT NUMBER := 1;
    F_PLATFORM_ID_ET NUMBER := 1;
    F_PLATFORM_ID_AO NUMBER := 2;
    F_PLATFORM_ID_COPYOP NUMBER := 3;
    F_WRITER_ID_WEB NUMBER := 1;
    F_WRITER_ID_MOBILE NUMBER := 200;
    F_WRITER_ID_COPYOP_WEB NUMBER := 10000;
    F_WRITER_ID_COPYOP_MOBILE NUMBER := 20000;
BEGIN
     SELECT ANALYTICS_DEPOSITS_OBJ(platform_id, platform_name, count_ftds, deposits_amount, time_data_fetched) 
     BULK COLLECT INTO v_return
     FROM
          (
          SELECT
                p.id as platform_id,
                p.name as platform_name,
                sum(analytics_dep.count_ftd) as count_ftds,
                sum(analytics_dep.total_deposits_amount) as deposits_amount,
                sysdate as time_data_fetched
              FROM
                platforms p 
                LEFT JOIN (SELECT 
                            (CASE -- WHEN (analytics.register_platform_id = F_PLATFORM_ID_ET) THEN F_PLATFORM_ID_ET -- ET
                                  WHEN ((analytics.writer_id in (F_WRITER_ID_WEB, F_WRITER_ID_MOBILE) AND analytics.register_platform_id <> F_PLATFORM_ID_ET) OR (analytics.writer_id not in (F_WRITER_ID_WEB, F_WRITER_ID_MOBILE, F_WRITER_ID_COPYOP_WEB, F_WRITER_ID_COPYOP_MOBILE) AND analytics.register_platform_id = F_PLATFORM_ID_AO)) THEN F_PLATFORM_ID_AO -- AO
                                  WHEN (analytics.writer_id in (F_WRITER_ID_COPYOP_WEB, F_WRITER_ID_COPYOP_MOBILE) OR (analytics.writer_id not in (F_WRITER_ID_WEB, F_WRITER_ID_MOBILE, F_WRITER_ID_COPYOP_WEB, F_WRITER_ID_COPYOP_MOBILE) AND analytics.register_platform_id = F_PLATFORM_ID_COPYOP)) THEN F_PLATFORM_ID_COPYOP END) as analytics_platform_id, -- Copyop
                            analytics.count_ftd,
                            analytics.total_deposits_amount
                          FROM
                            (SELECT 
                              t.writer_id,
                              s.business_case_id,
                              u.platform_id as register_platform_id,
                              count(CASE WHEN 
                                      t.id = u.first_deposit_id 
                                    THEN t.id END) as count_ftd,
                              sum(t.amount * t.rate) as total_deposits_amount 
                            FROM  
                              transactions t,  
                              transaction_types tt,  
                              users u,
                              skins s
                            WHERE
                              t.type_id = tt.id
                              AND t.user_id = u.id
                              AND u.skin_id = s.id
                              AND u.skin_id <> F_USER_SKIN_ETRADER
                              AND u.platform_id <> F_PLATFORM_ID_ET
                              AND u.class_id <> F_USER_CLASS_TEST  
                              AND tt.class_type = F_TRANS_CLASS_DEPOSIT  
                              AND t.status_id in ( F_TRANS_STATUS_SUCCEED, F_TRANS_STATUS_PENDING, F_TRANS_STATUS_CANCELED_ET)  
                              AND t.time_created >= trunc(sysdate,'DD') 
                              AND t.time_created <= sysdate  
                            GROUP BY 
                              t.writer_id,
                              s.business_case_id,
                              u.platform_id) analytics) analytics_dep on p.id = analytics_dep.analytics_platform_id
                   WHERE
                   	 p.id <> F_PLATFORM_ID_ET
                   GROUP BY
                     p.id,
                     p.name      
          );
RETURN v_return;
END GET_ANALYTICS_DEPOSITS;
END ANALYTICS;