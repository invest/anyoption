CREATE OR REPLACE PACKAGE REGULATION AS
 /*
 FUNCTION is_suspended_due_documents
          (p_user_id IN number)
 RETURN NUMBER; 
 */
 
FIRST_LIMIT_DEPOSIT_DOC constant number := 300000;
SECOND_BLOCK_LIMIT_DEPOSIT_DOC constant number := 1000000;

FUNCTION GET_FIRST_LIMIT_DEP_DOC RETURN NUMBER;
FUNCTION GET_SECOND_BLOCK_LIMIT_DEP_DOC RETURN NUMBER;
  
PROCEDURE SUSPEND_ACCOUNTS;

FUNCTION GET_ISSID_QUEST_REACHED (p_user_id IN NUMBER) 
  RETURN NUMBER;
FUNCTION DEPOSIT_CHECK_SUM (p_user_id NUMBER) 
  RETURN NUMBER;
  
PROCEDURE UPDATE_WRNG_INCRSE_FIRST_LIMIT;

PROCEDURE CALCULATE_PENDING_DOCS_STATE(p_file_id IN NUMBER, p_issue_id IN NUMBER);  
  
END REGULATION;
/


CREATE OR REPLACE PACKAGE BODY REGULATION AS

FUNCTION GET_ISSID_QUEST_REACHED (p_user_id IN NUMBER) RETURN NUMBER AS
  res NUMBER:=-1;
  cursor c_get_last_issue is
  select iss.id from
    issues iss
  where iss.user_id = p_user_id
  and iss.subject_id = 63
  order by 1 desc;

  v_last_issue number:=-1 ;

  cursor c_get_last_issue_action(p_issue_id number) is
  select ia.id from
  issue_actions ia
  where ia.issue_id = p_issue_id
  order by 1 desc;

  v_last_issue_action_id number :=-1;

BEGIN
  open c_get_last_issue;
  fetch c_get_last_issue into v_last_issue;
  close c_get_last_issue;

  open c_get_last_issue_action(v_last_issue);
  fetch c_get_last_issue_action into v_last_issue_action_id;
  close c_get_last_issue_action;
   res := v_last_issue_action_id;
  return(res);
END;

/*
FUNCTION is_suspended_due_documents
          (p_user_id IN number)
 RETURN NUMBER AS

  v_ret_val BOOLEAN;
 v_check_hist BOOLEAN;
 v_step_id NUMBER;
 v_usr NUMBER;
 v_result NUMBER ;
 v_s_reason varchar2(50) := 'User did not send documents after deposit.';
 v_suspended NUMBER;
v_histrow_days NUMBER;
 regrow users_regulation%ROWTYPE;

 cursor get_step_regulation is
 select * from users_regulation
 where user_id = p_user_id;

 cursor get_step_hist(p_step_id NUMBER) is
 select (sysdate - time_created) as days
 from users_regulation_history
 where user_id = p_user_id
 AND approved_regulation_step = p_step_id;



 BEGIN

   v_ret_val := FALSE;
   v_check_hist := FALSE;
   v_result :=0;
   v_suspended :=0;


 open get_step_regulation;
  fetch  get_step_regulation into regrow;
     IF regrow.approved_regulation_step >=5 THEN
       v_ret_val := FALSE;
       v_check_hist :=false;
    ELSIF regrow.is_suspended =0 and regrow.approved_regulation_step =4 THEN
       v_check_hist := TRUE;
       v_step_id :=2;
    ELSIF regrow.is_suspended =0 and regrow.approved_regulation_step =3 THEN
       v_check_hist := TRUE;
       v_step_id :=2;
    ELSIF regrow.is_suspended =0 and regrow.approved_regulation_step =2 THEN
          v_check_hist := FALSE;
          if(sysdate - regrow.time_created) > 30 then
          v_ret_val :=TRUE;
          else v_ret_val :=FALSE;
          end if;
    ELSIF regrow.is_suspended =1 and regrow.approved_regulation_step =4 THEN
       v_check_hist := TRUE;
       v_step_id :=2;
    ELSIF regrow.is_suspended =1 and regrow.approved_regulation_step =3 THEN
       v_check_hist := TRUE;
       v_step_id :=2;
    ELSIF regrow.is_suspended =1 and regrow.approved_regulation_step =2 THEN
          v_check_hist := FALSE;
          if(sysdate - regrow.time_created) > 30 then
          v_ret_val :=TRUE;
          else v_ret_val :=FALSE;
          end if;
    end if;
    v_suspended := regrow.is_suspended;
 close get_step_regulation;

   IF v_check_hist THEN

   open get_step_hist(v_step_id);
   fetch get_step_hist into v_histrow_days;
    IF v_histrow_days >= 30 THEN
       v_ret_val := TRUE;
   close get_step_hist;
   END IF;
   END IF;


  IF v_ret_val THEN
   v_result :=1;
   IF  v_suspended != 1 THEN
    Update users_regulation set is_suspended = 1, suspended_reason =  v_s_reason  where user_id=p_user_id;
   -- v_usr :=1;
   END IF;
  END if;
return v_result;

v_result :=0;
return v_result;
END is_suspended_due_documents;
*/

PROCEDURE SUSPEND_ACCOUNTS IS

/*cursor c_main is
select ur.user_id user_id, ur.approved_regulation_step step, decode(tu.id,null,0,1) as not_suspend
  from users u, transactions t, users_regulation ur left join TRANSFERRED_USERS tu on ur.user_id=tu.user_id and tu.is_first_dep_made=1
 where ur.SUSPENDED_REASON_ID = 0
   and u.id = ur.user_id
   and u.first_deposit_id = t.id
   and ur.approved_regulation_step between 2 and 6
   and sysdate - t.time_created >= 90;
*/
cursor c_check is
select ur.user_id from users_regulation ur where ur.qualified = 1 
    and ur.qualified_time is not null  
    and ur.qualified_time <= sysdate-30
    and ur.suspended_reason_id = 0
    and ur.approved_regulation_step not in (3,7)
    and not exists (select * from users_regulation_history urh where ur.user_id = urh.user_id and urh.approved_regulation_step = 3);

begin
/*
for tmp in c_main loop
  if tmp.not_suspend = 1 then
    goto END_LOOP;
  elsif tmp.step = 2  then
    update users_regulation ur set ur.SUSPENDED_REASON_ID = 2,ur.Time_Suspended_Regulation=sysdate,ur.writer_id=0 where user_id=tmp.user_id;
  else
    update users_regulation ur set ur.SUSPENDED_REASON_ID = 3,ur.Time_Suspended_Regulation=sysdate,ur.writer_id=0 where user_id=tmp.user_id;
  end if;

    <<END_LOOP>>
      null;
end loop;
commit;
*/
/*for tmp1 in c_check loop
    update users_regulation ur set ur.SUSPENDED_REASON_ID = 2,ur.Time_Suspended_Regulation=sysdate,ur.writer_id=0 where user_id=tmp1.user_id;
end loop;
commit;*/
null;
end SUSPEND_ACCOUNTS;

FUNCTION DEPOSIT_CHECK_SUM (p_user_id NUMBER) RETURN NUMBER IS
     PRAGMA AUTONOMOUS_TRANSACTION;
     calculated_sum NUMBER;
     calculated_sum_transferred NUMBER;
     date_transferred DATE;
     
     cursor cmain is 
     select sum(round(t.amount / 100  * t.rate)) as check_sum
     from transactions t, transaction_types tt, users u, users_regulation ur
    where t.status_id in (2,7)
        and tt.class_type = 1
        --and u.currency_id in (2,3,4)
        and t.type_id = tt.id
        and u.id = t.user_id
        and t.user_id = p_user_id
        and ur.user_id = t.user_id
        and u.SKIN_ID != 1;
        
     cursor cmain_transfered(p_date_in in date) is
     select sum(round(t.amount / 100 * t.rate)) as check_sum
     from transactions t, transaction_types tt, users u, users_regulation ur
    where t.status_id in (2,7)
        and tt.class_type = 1
        --and u.currency_id in (2,3,4)
        and t.type_id = tt.id
        and u.id = t.user_id
        and t.user_id = p_user_id
        and ur.user_id = t.user_id
        and p_date_in <= t.TIME_CREATED
        and u.SKIN_ID != 1;
        
     cursor is_transfered is
     select TIME_CREATED 
     from transferred_users tr
    where tr.user_id = p_user_id
       and tr.TIME_CREATED >= to_date('01-02-2015','dd-mm-yyyy');
   
   BEGIN
        open is_transfered;
        fetch is_transfered into date_transferred;
        close is_transfered;
        
        if date_transferred is not null then
   
            open cmain_transfered(date_transferred);
            fetch cmain_transfered into calculated_sum_transferred;
            close cmain_transfered;
        
            IF calculated_sum_transferred IS NULL THEN
            calculated_sum_transferred:= 0;   
            END IF;
            
            RETURN calculated_sum_transferred;
            
        else    
     
            open cmain;
            fetch cmain into calculated_sum;
            close cmain;
        
            IF calculated_sum IS NULL THEN
            calculated_sum:= 0;
            END IF;
            
            RETURN calculated_sum;
            
       end if; 
       
END DEPOSIT_CHECK_SUM;

FUNCTION GET_FIRST_LIMIT_DEP_DOC RETURN NUMBER AS
BEGIN
return FIRST_LIMIT_DEPOSIT_DOC;
END;

FUNCTION GET_SECOND_BLOCK_LIMIT_DEP_DOC RETURN NUMBER AS
BEGIN
return SECOND_BLOCK_LIMIT_DEPOSIT_DOC;
END;


PROCEDURE UPDATE_WRNG_INCRSE_FIRST_LIMIT IS
  BEGIN
    update USERS_REGULATION a set
           a.qualified_amount = null,
           a.comments = 'The first limit was increase and remove the warning msg'
   where a.user_id in (
         select user_id 
          from 
               (select 
                   u.id as user_id , 
                   regulation.DEPOSIT_CHECK_SUM(ur.user_id) as dep_sum,
                   regulation.GET_FIRST_LIMIT_DEP_DOC()/100 as first_limit
                 from users_regulation ur,
                      users u
                 where ur.approved_regulation_step < 7
                  and nvl(ur.qualified_amount,0) != 0
                  and ur.qualified = 0
                  and ur.user_id =  u.id
                  and u.skin_id != 1)
                  where dep_sum < first_limit
                     );
  END;
  
PROCEDURE CALCULATE_PENDING_DOCS_STATE(p_file_id IN NUMBER, p_issue_id IN NUMBER) IS
  
  cursor c_get_data is
  SELECT 
         iss.id as issue_id, 
         SUM(
         --*********Pending DOCS*****************
           --1. Case when is not uploaded files
           case when nvl(ff.file_name,'x')='x' 
             then 1 
           --2. Case when is not touched  
           when  (nvl(ff.is_approved,0) + nvl(ff.is_support_rejected,0) + nvl(ff.is_approved_control,0) + nvl(ff.is_control_rejected,0)) = 0
             then 1
           --3. Case when is suppoert reject  
           when nvl(ff.is_support_rejected,0) = 1 
                  and  nvl(ff.is_approved,0) = 0
                  and  nvl(ff.is_approved_control,0) = 0 
             then 1     
           --4. Case when is control reject
           when nvl(ff.is_control_rejected,0) = 1 
                and  nvl(ff.is_approved_control,0) = 0      
             then 1 
          --5. Case when is not MAIN issue and control is not approved
           when nvl(ff.is_approved_control,0) != 1 
                and  nvl(d.is_main_regulation_issue,0) = 0      
             then 1     
               else 0 end) as pending_docs,
         --*********Uploaded FILES*****************
         --When file name is not null the the file is upload      
         SUM(case when nvl(ff.file_name,'x')='x' then 0 else 1 end) as upload_docs,
         --*********Support is Rejected*****************
         --When support is rejected and the Support/Control are not approved then the file is Reject from Support
         SUM(case when nvl(ff.is_support_rejected,0) = 1 
                  and  nvl(ff.is_approved,0) = 0
                  and  nvl(ff.is_approved_control,0) = 0 then 1 else 0  end ) reject_support,
         --*********Control is Rejected*****************
         --When Control is rejected and Control is not approved then the file is Reject from Control
         SUM(case when nvl(ff.is_control_rejected,0) = 1 
                  and  nvl(ff.is_approved_control,0) = 0 then 1 else 0  end) reject_control,
         --When Control is NOT approved then the file is Reject from Control
         SUM(case when nvl(ff.is_approved_control,0) = 1  then 0 else 1  end) is_not_approved_control,
         count(*) as TOTAL_FILES_COUNT         
  FROM
         files ff,
         issues iss,
         users_pending_docs d,
         (select ia.issue_id, f.issue_action_id from 
                 files f,
                 issue_actions ia
                 where f.id = decode(p_file_id,-1,f.id,p_file_id)
                 and ia.issue_id = decode(p_issue_id,-1,ia.issue_id,p_issue_id)
                 and ia.id = f.issue_action_id
           group by ia.issue_id, f.issue_action_id      
          ) i
 WHERE
   ff.issue_action_id = i.issue_action_id
   and iss.id = i.issue_id
   and iss.subject_id = 56
   and ff.file_status_id != 5
   and iss.id = d.issue_id
   group by iss.id;
   
   v_get_data c_get_data%ROWTYPE;
  
  BEGIN
      open c_get_data;
      fetch c_get_data into  v_get_data;
       IF c_get_data%FOUND THEN
         update users_pending_docs
            set 
                pending_docs   = v_get_data.pending_docs,
                uploaded_docs  = v_get_data.upload_docs,
                reject_support = v_get_data.reject_support,
                reject_control = v_get_data.reject_control,
                total_files_count = v_get_data.total_files_count,
                pending_docs_control = v_get_data.is_not_approved_control
          where issue_id = v_get_data.issue_id;         
       END IF;
      close c_get_data; 
    
  
  END;

END REGULATION;
/
