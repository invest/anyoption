create or replace PACKAGE "MARKETING" is


PROCEDURE INSERT_MARKETING_TRACKING 
  (p_mid varchar2, 
   p_combination_id number,  
   p_time_static date, 
   p_http_referer varchar2,
   p_dynamic_param NVARCHAR2,  
   p_utm_source varchar2,
   p_combination_id_dynamic number, 
   p_time_dynamic date, 
   p_http_referer_dynamic varchar2,
   p_dynamic_param_dynamic NVARCHAR2,
   p_utm_source_dynmic varchar2,
   p_contact_id number, 
   p_user_id number, 
   p_activity_id number,
   p_aff_sub1 NVARCHAR2 default NULL,
   p_aff_sub2 NVARCHAR2 default NULL,
   p_aff_sub3 nvarchar2 default NULL
   );
   
   
   
PROCEDURE INSERT_MARKETING_TRACKING 
  (p_mid varchar2, 
   p_combination_id number,  
   p_time_static date, 
   p_http_referer varchar2,
   p_dynamic_param NVARCHAR2,  
   p_combination_id_dynamic number, 
   p_time_dynamic date, 
   p_http_referer_dynamic varchar2,
   p_dynamic_param_dynamic NVARCHAR2,
   p_contact_id number, 
   p_user_id number, 
   p_activity_id number,
   p_aff_sub1 NVARCHAR2 default NULL,
   p_aff_sub2 NVARCHAR2 default NULL,
   p_aff_sub3 nvarchar2 default NULL
   );
   

FUNCTION GET_TRACK_DATA_BY_PRIORITY(p_static_id number, p_static_date date)
  return marketing_tracking%rowtype;
   
FUNCTION GET_MID_BY_USER_ID(p_usr_id number)
  return varchar2;
  
     
FUNCTION GET_ACTIVITY_BY_MID(p_mid varchar2)
  return number;
  
FUNCTION GET_MID_BY_CONTACT_DETAILS_BE(p_email varchar2, p_mobile_phone varchar2, p_land_line_phone varchar2, p_contact_id number, p_user_id number)
  return varchar2; 
  
FUNCTION GET_MID_BY_CONTACT_DETAILS_EP(p_email varchar2, p_phone varchar2)
  return varchar2;
  
FUNCTION GET_ETS_MID_BY_CONTACT_DETAILS(p_email varchar2, p_phone varchar2)
  return varchar2;
  
FUNCTION GET_ETS_MID_CNTCT_DETAILS_BE(p_email varchar2, p_mobile_phone varchar2, p_land_line_phone varchar2, p_contact_id number, p_user_id number)
  return varchar2;
  
FUNCTION GET_CONTACT_BY_PHONE_OR_EMAIL(p_phone varchar2, p_email varchar2, p_country_id number)
  return number;  
  
FUNCTION GET_FREE_CONTACT_DETAILS(p_country_id number, p_land_line_phone varchar2, p_mobile_phone varchar2, p_email varchar2, p_skin_id number)
  return number;
  
  PROCEDURE INSERT_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ);
  
  PROCEDURE UPDATE_MARKETING_ATT(P_marketing_att_old MARKETING_ATTRIBUTION_OBJ, P_marketing_att_new MARKETING_ATTRIBUTION_OBJ);
  
  PROCEDURE UPDATE_MARKETING_ATT_AFF(P_marketing_att_old MARKETING_ATTRIBUTION_OBJ, P_marketing_att_new MARKETING_ATTRIBUTION_OBJ);
  
  FUNCTION GET_MARKETING_ATT_TYPE_ID(P_user_id number)
    RETURN NUMBER;
    
  PROCEDURE TRANSACTIONS_HANDLER(P_user_id number, P_transaction_id number, P_tran_time_created date, P_login_id number);
  
  FUNCTION GET_MARKETING_ATT_BY_LOGIN(P_login_id number, P_user_id number)
    RETURN MARKETING_ATTRIBUTION_OBJ;
    
  PROCEDURE MERGE_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ);
  
  PROCEDURE UPDATE_USERS(P_marketing_att MARKETING_ATTRIBUTION_OBJ);
  
  PROCEDURE UPDATE_UAD(P_marketing_att MARKETING_ATTRIBUTION_OBJ);
  
  FUNCTION GET_SPECIAL_CODE(P_dynamic_param varchar2, P_comb_id number)
    RETURN varchar2;
    
  FUNCTION IS_EXISTS_MARKETING_ATT(P_user_id NUMBER, P_marketing_att_type_id NUMBER)
    RETURN NUMBER;
    
  FUNCTION IS_AFFILATE_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
    RETURN NUMBER;
    
  FUNCTION GET_AFFILATE_KEY(P_dynamic_param varchar2, P_comb_id number)
    RETURN number;

End MARKETING;

/


create or replace PACKAGE BODY "MARKETING" is


PROCEDURE INSERT_MARKETING_TRACKING
  (p_mid varchar2,
   p_combination_id number,
   p_time_static date,
   p_http_referer varchar2,
   p_dynamic_param NVARCHAR2,
   p_utm_source varchar2,
   p_combination_id_dynamic number,
   p_time_dynamic date,
   p_http_referer_dynamic varchar2,
   p_dynamic_param_dynamic NVARCHAR2,
   p_utm_source_dynmic varchar2,
   p_contact_id number,
   p_user_id number,
   p_activity_id number,
   p_aff_sub1 NVARCHAR2 default NULL,
   p_aff_sub2 NVARCHAR2 default NULL,
   p_aff_sub3 nvarchar2 default NULL
   )
   IS

cursor c_check_activ_fd is
select count(*)
from marketing_tracking mt,
marketing_tracking_static st
where mt.marketing_tracking_static_id=st.id
and st.mid=p_mid
and mt.marketing_tracking_activity_id=5;

v_activ_check number :=0;
--
cursor c_chek_fd_by_mid is
select  count(*)
from users u,
marketing_tracking mt,
marketing_tracking_static ms
where ms.id=mt.marketing_tracking_static_id
and mt.user_id=u.id
and nvl(u.first_deposit_id,0)!=0
and ms.mid=p_mid;

v_chek_fd_by_mid number :=0;
--
cursor c_chek_fd_by_uid is
select count(*)
from users u
where u.id=p_user_id
and nvl(u.first_deposit_id,0)!=0;

v_chek_fd_by_uid number :=0;
--
cursor c_get_static_by_mid is
select * from
marketing_tracking_static ms
where ms.mid=p_mid;

cursor c_get_static_date is
select st.time_static
from marketing_tracking_static st
where st.mid=p_mid;
v_time_static date;



cursor c_contact_activity_exist(pi_static_id number) is
select cm.id from
marketing_tracking cm
where cm.contact_id = p_contact_id
and cm.marketing_tracking_static_id = pi_static_id
and nvl(cm.combination_id_dynamic,0)= nvl(p_combination_id_dynamic,0)
and nvl(cm.http_referer_dynamic,'x')=nvl(p_http_referer_dynamic,'x')
and nvl(cm.dynamic_param_dynamic,'x') = nvl(p_dynamic_param_dynamic,'x')
and cm.marketing_tracking_activity_id=3;

v_contact_activity_exist number :=0;

v_lock_data marketing_tracking%rowtype;

v_static_data marketing_tracking_static%rowtype;

BEGIN

open c_check_activ_fd;
fetch c_check_activ_fd into v_activ_check;
close c_check_activ_fd;

open c_chek_fd_by_mid;
fetch c_chek_fd_by_mid into v_chek_fd_by_mid;
close c_chek_fd_by_mid;

open c_chek_fd_by_uid;
fetch c_chek_fd_by_uid into v_chek_fd_by_uid;
close c_chek_fd_by_uid;
--
v_static_data.id :=0;
open c_get_static_by_mid;
fetch c_get_static_by_mid into v_static_data;
close c_get_static_by_mid;

If (v_activ_check=0 and v_chek_fd_by_mid=0 and v_chek_fd_by_uid=0) or (v_chek_fd_by_uid=0 and nvl(p_user_id,0)>0) then

     if nvl(v_static_data.id,0)=0 then
       v_static_data.id:=SEQ_MARKETING_TRACKING_STATIC.NEXTVAL;
      insert into marketing_tracking_static
        (id, mid, combination_id, time_static, http_referer, dynamic_param, utm_source, aff_sub1, aff_sub2, aff_sub3)
      values
        (v_static_data.id, p_mid, p_combination_id, p_time_static, p_http_referer, p_dynamic_param, p_utm_source, p_aff_sub1, p_aff_sub2, p_aff_sub3);

        --Add first click
            insert into marketing_tracking
       (id, marketing_tracking_static_id, combination_id_dynamic, time_dynamic, http_referer_dynamic, dynamic_param_dynamic, contact_id, user_id, marketing_tracking_activity_id, utm_source, aff_sub1, aff_sub2, aff_sub3)
     values
       (SEQ_MARKETING_TRACKING.NEXTVAL, v_static_data.id, p_combination_id, p_time_static, p_http_referer, p_dynamic_param, p_contact_id, p_user_id, 1, p_utm_source, p_aff_sub1, p_aff_sub2, p_aff_sub3);


     end if;

          --check if exist record for contatc
        if  p_activity_id = 3 then
          open c_contact_activity_exist(v_static_data.id);
          fetch c_contact_activity_exist into v_contact_activity_exist;
          close c_contact_activity_exist;
        end if;

     if p_activity_id>0 and v_contact_activity_exist = 0 then
     open  c_get_static_date;
     fetch c_get_static_date into v_time_static;
     close c_get_static_date;
     
       if p_activity_id > 1 then
         v_lock_data := GET_TRACK_DATA_BY_PRIORITY(v_static_data.id, v_time_static);         
         insert into marketing_tracking
           (id, marketing_tracking_static_id, combination_id_dynamic, time_dynamic, http_referer_dynamic, dynamic_param_dynamic, contact_id, user_id, marketing_tracking_activity_id, utm_source, aff_sub1, aff_sub2, aff_sub3)
         values
           (SEQ_MARKETING_TRACKING.NEXTVAL, v_static_data.id, v_lock_data.combination_id_dynamic, p_time_dynamic, v_lock_data.http_referer_dynamic, v_lock_data.dynamic_param_dynamic, p_contact_id, p_user_id, p_activity_id, v_lock_data.utm_source, p_aff_sub1, p_aff_sub2, p_aff_sub3);
           
          --Update Lock_ID 
         update marketing_tracking_static c
          set 
               lock_id = SEQ_MARKETING_TRACKING.CURRVAL
           where id =  v_static_data.id;
           
       else         
         insert into marketing_tracking
           (id, marketing_tracking_static_id, combination_id_dynamic, time_dynamic, http_referer_dynamic, dynamic_param_dynamic, contact_id, user_id, marketing_tracking_activity_id, utm_source, aff_sub1, aff_sub2, aff_sub3)
         values
           (SEQ_MARKETING_TRACKING.NEXTVAL, v_static_data.id, p_combination_id_dynamic, p_time_dynamic, p_http_referer_dynamic, p_dynamic_param_dynamic, p_contact_id, p_user_id, p_activity_id, p_utm_source_dynmic, p_aff_sub1, p_aff_sub2, p_aff_sub3);
       end if;
       
       --Update STATCI with LAST CLICKS
      
     if p_activity_id = 1 and nvl(v_static_data.lock_id,0) = 0  then
            update marketing_tracking_static
         set 
             combination_id = p_combination_id_dynamic,
             time_static = p_time_dynamic,
             http_referer = p_http_referer_dynamic,
             dynamic_param = p_dynamic_param_dynamic,
             time_created = sysdate,
             utm_source = p_utm_source_dynmic,
             aff_sub1 = p_aff_sub1,
             aff_sub2 = p_aff_sub2,
             aff_sub3 = p_aff_sub3
       where id = v_static_data.id;
    end if;
       
       
     end if;

     --Update Static After 45 day and if not have Activity with last CLick params
     if(p_activity_id = 1 and GET_ACTIVITY_BY_MID(p_mid) = 0 and (p_time_dynamic-v_time_static) >= 45) then
        update marketing_tracking_static
           set
               combination_id = p_combination_id_dynamic,
               time_static = p_time_dynamic,
               http_referer = p_http_referer_dynamic,
               dynamic_param = p_dynamic_param_dynamic,
               utm_source = p_utm_source_dynmic,
	           aff_sub1 = p_aff_sub1,
    	       aff_sub2 = p_aff_sub2,
        	   aff_sub3 = p_aff_sub3
  
         where mid = p_mid;
     end if;

End if;

END;


PROCEDURE INSERT_MARKETING_TRACKING
  (p_mid varchar2,
   p_combination_id number,
   p_time_static date,
   p_http_referer varchar2,
   p_dynamic_param NVARCHAR2,
   p_combination_id_dynamic number,
   p_time_dynamic date,
   p_http_referer_dynamic varchar2,
   p_dynamic_param_dynamic NVARCHAR2,
   p_contact_id number,
   p_user_id number,
   p_activity_id number,
   p_aff_sub1 NVARCHAR2 default NULL,
   p_aff_sub2 NVARCHAR2 default NULL,
   p_aff_sub3 nvarchar2 default NULL
   )
   IS
   Begin
     INSERT_MARKETING_TRACKING ( p_mid ,
                                 p_combination_id ,
                                 p_time_static ,
                                 p_http_referer ,
                                 p_dynamic_param ,
                                 '',
                                 p_combination_id_dynamic ,
                                 p_time_dynamic ,
                                 p_http_referer_dynamic ,
                                 p_dynamic_param_dynamic ,
                                 '',
                                 p_contact_id ,
                                 p_user_id ,
                                 p_activity_id ,
                                 p_aff_sub1 ,
                                 p_aff_sub2 ,
                                 p_aff_sub3
                                 );
   End;
---------------------------------------------------------------------------------------------------------------------------------------
--Return Marketing DATA by campaign priority
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_TRACK_DATA_BY_PRIORITY(p_static_id number, p_static_date date)
  return marketing_tracking%rowtype is
  
cursor c_get_locked_data is
select mt.* from
marketing_tracking mt,
marketing_tracking_static mts
where mts.lock_id = mt.id
and mts.id = p_static_id;

cursor c_get_lock_data is
select mt.* from 
marketing_combinations comb,
marketing_campaigns camp,
marketing_tracking mt,
marketing_channel chn
where 
mt.combination_id_dynamic = comb.id
and camp.id = comb.campaign_id
and mt.marketing_tracking_activity_id = 1
and mt.marketing_tracking_static_id = p_static_id
and mt.time_dynamic >= p_static_date
and chn.id = camp.marketing_channel_priority_id
and chn.priority is not null
order by decode(mt.utm_source,'N/A',1,chn.priority), mt.time_dynamic; 

cursor c_get_old_locked_date is
select distinct mts.* from 
marketing_tracking_static mts,
marketing_tracking mt
where mts.id = mt.marketing_tracking_static_id
and mt.marketing_tracking_activity_id > 1
and trunc(mts.time_created) <= to_date('11-05-2014','dd-mm-yyyy')
and mts.id = p_static_id;

v_marketing_tracking  marketing_tracking%rowtype:=null;
v_marketing_tracking_static  marketing_tracking_static%rowtype:=null;
v_found_data boolean:= false;
v_found_old_data boolean:= false;

--WHITOUT ORDER AND GET DATA FROM STATIC TABLE
cursor c_wihtout_order is
select * from
marketing_tracking_static mts
where mts.id = p_static_id;
--
BEGIN 
  --WHITOUT ORDER AND GET DATA FROM STATIC TABLE
  open c_wihtout_order;
  fetch c_wihtout_order into v_marketing_tracking_static;
  close c_wihtout_order;
  
  v_marketing_tracking.combination_id_dynamic := v_marketing_tracking_static.combination_id;
  v_marketing_tracking.http_referer_dynamic := v_marketing_tracking_static.http_referer;
  v_marketing_tracking.dynamic_param_dynamic := v_marketing_tracking_static.dynamic_param;
  v_marketing_tracking.utm_source := v_marketing_tracking_static.utm_source;
  return v_marketing_tracking;
  --END WHITOUT ORDER      
  --search for locked data                                           
    open c_get_locked_data;
    fetch c_get_locked_data into v_marketing_tracking;
    IF c_get_locked_data%found THEN
       v_found_data := true;
    END IF;          
    close c_get_locked_data;
    
    IF not v_found_data THEN
      
       --check if old data before new logic
        open c_get_old_locked_date;
        fetch c_get_old_locked_date into v_marketing_tracking_static;
        IF c_get_old_locked_date%found THEN
          v_found_old_data :=true;
        END IF;
        close c_get_old_locked_date;   
        
        IF v_found_old_data THEN
          v_marketing_tracking.combination_id_dynamic := v_marketing_tracking_static.combination_id;
          v_marketing_tracking.http_referer_dynamic := v_marketing_tracking_static.http_referer;
          v_marketing_tracking.dynamic_param_dynamic := v_marketing_tracking_static.dynamic_param;

        ELSE          
            open c_get_lock_data;
            fetch c_get_lock_data into v_marketing_tracking;
            close c_get_lock_data;
            
            update marketing_tracking_static
               set lock_id = v_marketing_tracking.id,
                   combination_id = v_marketing_tracking.combination_id_dynamic,
                   http_referer = v_marketing_tracking.http_referer_dynamic,
                   dynamic_param = v_marketing_tracking.dynamic_param_dynamic
             where id = p_static_id;        
        END IF;     
    END IF;                

    return v_marketing_tracking;
END;

---------------------------------------------------------------------------------------------------------------------------------------
--Return MID by USER_ID, if null then return USER_ID for MID
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MID_BY_USER_ID(p_usr_id number)
  return varchar2 is

cursor c_get_mid is
select distinct MID from
marketing_tracking t,
marketing_tracking_static st
where t.marketing_tracking_static_id=st.id
and t.user_id = p_usr_id;

v_MID varchar2(400) :=null;

  BEGIN
    open c_get_mid;
    fetch c_get_mid into v_MID;
    If c_get_mid%notfound then
      v_MID:=to_char(p_usr_id);
    End if;
    close c_get_mid;

    return v_MID;
  END;

---------------------------------------------------------------------------------------------------------------------------------------
--Return Activity by MID, if 0  then NOT EXIST Activity
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ACTIVITY_BY_MID(p_mid varchar2)
  return number is

cursor c_get_activity is
select count(*)
from marketing_tracking mt,
marketing_tracking_static mts
where mt.marketing_tracking_static_id = mts.id
and mts.mid = p_mid
and mt.marketing_tracking_activity_id > 1;

v_activity number:=0;

  BEGIN
    open c_get_activity;
    fetch c_get_activity into v_activity;
    close c_get_activity;

    return v_activity;
  END;

  ---------------------------------------------------------------------------------------------------------------------------------------
--Return MID by Contact Details (ID,Email,Phones), if null then return none for MID
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MID_BY_CONTACT_DETAILS_BE(p_email varchar2, p_mobile_phone varchar2, p_land_line_phone varchar2, p_contact_id number, p_user_id number)
  return varchar2 is

cursor c_get_mid_by_contatc_id is
select mts.mid from
marketing_tracking mt,
marketing_tracking_static mts,
contacts c
where mt.marketing_tracking_static_id = mts.id
and mt.contact_id = p_contact_id
and mt.contact_id = c.id
and c.user_id in (0,p_user_id)
order by mts.id;
cursor c_get_mid_by_contact_details is
select mts.mid from
marketing_tracking mt,
marketing_tracking_static mts,
 (SELECT id
   FROM contacts
  WHERE user_id in  (0,p_user_id)
    AND ((type not in (5, 6) AND (phone = p_land_line_phone or phone = p_mobile_phone or email = p_email)) OR
        (type in (5, 6) AND
        (land_line_phone = p_land_line_phone or mobile_phone = p_mobile_phone or email = p_email)))
)contact
Where
mt.marketing_tracking_static_id = mts.id
and mt.contact_id = contact.id
order by mts.id;

v_MID varchar2(400) :='none';

  BEGIN

    /*IF  p_contact_id > 0  THEN
      open c_get_mid_by_contatc_id;
      fetch c_get_mid_by_contatc_id into v_MID;
      close c_get_mid_by_contatc_id;
    END IF;

    IF v_MID = 'none'  THEN
      open c_get_mid_by_contact_details;
      fetch c_get_mid_by_contact_details into v_MID;
      close c_get_mid_by_contact_details;
    END IF;*/
    return v_MID;
  END;
---------------------------------------------------------------------------------------------------------------------------------------
--Return MID by Contact Details (Email,Phones), if null then return none for MID
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MID_BY_CONTACT_DETAILS_EP(p_email varchar2, p_phone varchar2)
  return varchar2 is

cursor c_get_mid_by_contact_details is
select mts.mid from
marketing_tracking mt,
marketing_tracking_static mts,
 (
    SELECT
       id
     FROM
         contacts
     WHERE
        /*phone = p_phone
        OR mobile_phone = p_phone
        OR land_line_phone = p_phone
        OR*/ email = p_email
)contact
Where
mt.marketing_tracking_static_id = mts.id
and mt.contact_id = contact.id
order by mts.id;

cursor c_get_mid_by_cont_from_users is
select mts.mid from
marketing_tracking mt,
marketing_tracking_static mts,
 (
    SELECT
       id
     FROM
         users
     WHERE
        /*mobile_phone = p_phone
        OR land_line_phone = p_phone
        OR*/ email = p_email
) usrs
Where
mt.marketing_tracking_static_id = mts.id
and mt.user_id = usrs.id
order by mts.id;

v_MID varchar2(400) :='none';

BEGIN

    open c_get_mid_by_contact_details;
    fetch c_get_mid_by_contact_details into v_MID;
    close c_get_mid_by_contact_details;

    if (v_MID = 'none') then
      open c_get_mid_by_cont_from_users;
      fetch c_get_mid_by_cont_from_users into v_MID;
      close c_get_mid_by_cont_from_users;
    end if;

    return v_MID;

End;

FUNCTION GET_ETS_MID_BY_CONTACT_DETAILS(p_email varchar2, p_phone varchar2)
  return varchar2 is

cursor c_get_mid_by_contact_details is
select mid 
from contacts 
where email = p_email
order by id ;

cursor c_get_mid_by_cont_from_users is
select mid 
from users --!!!!USER!!!!
where email = p_email
order by id ;

v_MID varchar2(400) :=null;

BEGIN

    open c_get_mid_by_contact_details;
    fetch c_get_mid_by_contact_details into v_MID;
    close c_get_mid_by_contact_details;

    if (v_MID = null) then
      open c_get_mid_by_cont_from_users;
      fetch c_get_mid_by_cont_from_users into v_MID;
      close c_get_mid_by_cont_from_users;
    end if;

    return nvl(v_MID,'none');

End;


FUNCTION GET_ETS_MID_CNTCT_DETAILS_BE(p_email varchar2, p_mobile_phone varchar2, p_land_line_phone varchar2, p_contact_id number, p_user_id number)
  return varchar2 is

cursor c_get_mid_by_contatc_id is
select mid from
contacts c
where c.id = p_contact_id
and c.user_id in (0,p_user_id)
order by c.id;

cursor c_get_mid_by_contact_details is


 SELECT mid
   FROM contacts
  WHERE user_id in  (0,p_user_id)
    AND ((type not in (5, 6) AND (phone = p_land_line_phone or phone = p_mobile_phone or email = p_email)) OR
        (type in (5, 6) AND
        (land_line_phone = p_land_line_phone or mobile_phone = p_mobile_phone or email = p_email)))
order by id;

v_MID varchar2(400) :=null;

  BEGIN

    IF  p_contact_id > 0  THEN
      open c_get_mid_by_contatc_id;
      fetch c_get_mid_by_contatc_id into v_MID;
      close c_get_mid_by_contatc_id;
    END IF;

    IF (v_MID = null)  THEN
      open c_get_mid_by_contact_details;
      fetch c_get_mid_by_contact_details into v_MID;
      close c_get_mid_by_contact_details;
    END IF;
    return nvl(v_MID,'none');
  END;
  
FUNCTION GET_CONTACT_BY_PHONE_OR_EMAIL(p_phone varchar2, p_email varchar2, p_country_id number)
  return number is
  
	cursor c_get_contact is
	SELECT id
	FROM contacts 
	WHERE user_id = 0 
	    AND country_id = p_country_id 
	    AND (phone=p_phone or email=p_email); 

	r_contact number := 0;

BEGIN
	open c_get_contact;
	fetch c_get_contact into r_contact;
	close c_get_contact;
	
	return r_contact;

END; 

FUNCTION GET_FREE_CONTACT_DETAILS(p_country_id number, p_land_line_phone varchar2, p_mobile_phone varchar2, p_email varchar2, p_skin_id number)
  return number is

cursor c_get_free_contact_details is
  SELECT id
    FROM contacts
   WHERE user_id = 0
     AND country_id = p_country_id
     AND (mobile_phone = p_mobile_phone or email = p_email)
   ORDER BY id;
   
/*cursor c_get_free_contact_details_ET is
  SELECT id
    FROM contacts
   WHERE user_id = 0
     AND country_id = p_country_id
     AND (
           (type not in (5, 6) AND
           (phone = p_land_line_phone or phone = p_mobile_phone)) 
       OR  
          (type in (5, 6) AND
           (land_line_phone = p_land_line_phone or mobile_phone = p_mobile_phone))
         )
   ORDER BY id;   */

v_contact_id number :=0;

BEGIN
--insert into trace_contact_search values(p_country_id, p_land_line_phone, p_mobile_phone, p_email, systimestamp, 'start');
/*IF p_skin_id = 1 THEN
    open c_get_free_contact_details_ET;
    fetch c_get_free_contact_details_ET into v_contact_id;
    close c_get_free_contact_details_ET;  
ELSE */
    open c_get_free_contact_details;
    --insert into trace_contact_search values(p_country_id, p_land_line_phone, p_mobile_phone, p_email, systimestamp, 'step 1');
    fetch c_get_free_contact_details into v_contact_id;
    --insert into trace_contact_search values(p_country_id, p_land_line_phone, p_mobile_phone, p_email, systimestamp, 'step 2');
    close c_get_free_contact_details;
    --insert into trace_contact_search values(p_country_id, p_land_line_phone, p_mobile_phone, p_email, systimestamp, 'step 3');
/*END IF;*/

    return v_contact_id;

End;

  PROCEDURE INSERT_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
  IS
  BEGIN
    INSERT INTO MARKETING_ATTRIBUTION
      (
        ID,
        USER_ID,
        TYPE_ID,
        COMBINATION_ID,
        DYNAMIC_PARAM,
        AFF_SUB1,
        AFF_SUB2,
        AFF_SUB3,
        AFFILIATE_KEY,
        SPECIAL_CODE,
        AFFILIATE_SYSTEM,
        AFFILIATE_SYSTEM_USER_TIME,
        TIME_CREATED,
        TIME_UPDATED
      )
      VALUES
      (
        SEQ_MARKETING_ATTRIBUTION.nextval ,
        P_marketing_att.USER_ID ,
        P_marketing_att.TYPE_ID ,
        P_marketing_att.COMBINATION_ID ,
        P_marketing_att.DYNAMIC_PARAM ,
        P_marketing_att.AFF_SUB1 ,
        P_marketing_att.AFF_SUB2 ,
        P_marketing_att.AFF_SUB3 ,
        P_marketing_att.AFFILIATE_KEY ,
        P_marketing_att.SPECIAL_CODE ,
        P_marketing_att.AFFILIATE_SYSTEM ,
        P_marketing_att.AFFILIATE_SYSTEM_USER_TIME ,
        sysdate ,
        null
      );
  END;
  
  PROCEDURE UPDATE_MARKETING_ATT(P_marketing_att_old MARKETING_ATTRIBUTION_OBJ, P_marketing_att_new MARKETING_ATTRIBUTION_OBJ)
  IS
  BEGIN
    IF 
      nvl(P_marketing_att_new.DYNAMIC_PARAM,'-1') != nvl(P_marketing_att_old.DYNAMIC_PARAM,'-1') OR
      nvl(P_marketing_att_new.COMBINATION_ID,'-1') != nvl(P_marketing_att_old.COMBINATION_ID,'-1') OR
      nvl(P_marketing_att_new.AFFILIATE_KEY,'-1') != nvl(P_marketing_att_old.AFFILIATE_KEY,'-1') OR
      nvl(P_marketing_att_new.SPECIAL_CODE,'-1') != nvl(P_marketing_att_old.SPECIAL_CODE,'-1') OR
      nvl(P_marketing_att_new.AFFILIATE_SYSTEM,'-1') != nvl(P_marketing_att_old.AFFILIATE_SYSTEM,'-1') OR
      nvl(P_marketing_att_new.AFFILIATE_SYSTEM_USER_TIME,to_date('2400','YYYY')) != nvl(P_marketing_att_old.AFFILIATE_SYSTEM_USER_TIME, to_date('2400','YYYY'))

    THEN
      UPDATE 
        MARKETING_ATTRIBUTION ma
      SET 
        ma.DYNAMIC_PARAM = P_marketing_att_new.DYNAMIC_PARAM,
        ma.COMBINATION_ID = P_marketing_att_new.COMBINATION_ID,
        ma.AFFILIATE_KEY = P_marketing_att_new.AFFILIATE_KEY,
        ma.SPECIAL_CODE = P_marketing_att_new.SPECIAL_CODE,
        ma.AFFILIATE_SYSTEM = P_marketing_att_new.AFFILIATE_SYSTEM,
        ma.AFFILIATE_SYSTEM_USER_TIME = P_marketing_att_new.AFFILIATE_SYSTEM_USER_TIME,
        ma.TIME_UPDATED = sysdate
      WHERE 
        ma.USER_ID = P_marketing_att_new.USER_ID
        and ma.TYPE_ID = P_marketing_att_new.TYPE_ID;
    END IF; 
  END;
  
  PROCEDURE UPDATE_MARKETING_ATT_AFF(P_marketing_att_old MARKETING_ATTRIBUTION_OBJ, P_marketing_att_new MARKETING_ATTRIBUTION_OBJ)
  IS
    -- *** Declaration_section ***
    F_marketing_att_type_id NUMBER;
    T_marketing_att_type_reg NUMBER := 1;
  BEGIN
    F_marketing_att_type_id := MARKETING.GET_MARKETING_ATT_TYPE_ID(P_marketing_att_new.USER_ID);
    IF (F_marketing_att_type_id = T_marketing_att_type_reg
        AND (nvl(P_marketing_att_new.AFF_SUB1,'-1') != nvl(P_marketing_att_old.AFF_SUB1,'-1') or
             nvl(P_marketing_att_new.AFF_SUB2,'-1') != nvl(P_marketing_att_old.AFF_SUB2,'-1') or
             nvl(P_marketing_att_new.AFF_SUB3,'-1') != nvl(P_marketing_att_old.AFF_SUB3,'-1')))
    THEN
      UPDATE 
        MARKETING_ATTRIBUTION ma
      SET 
        ma.AFF_SUB1 = P_marketing_att_new.AFF_SUB1,
        ma.AFF_SUB2 = P_marketing_att_new.AFF_SUB2,
        ma.AFF_SUB3 = P_marketing_att_new.AFF_SUB3,
        ma.TIME_UPDATED = sysdate
      WHERE 
        ma.USER_ID = P_marketing_att_new.USER_ID
        and ma.TYPE_ID = F_marketing_att_type_id;
    END IF; 
  END;
  
  FUNCTION GET_MARKETING_ATT_TYPE_ID(P_user_id number)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_marketing_att_type_id NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    select 
      u.MARKETING_ATTRIBUTION_TYPE_ID
    into 
      F_marketing_att_type_id
    from 
      users u
    where
      u.id = P_user_id;
    RETURN F_marketing_att_type_id;
    
    EXCEPTION
      WHEN OTHERS THEN
      RETURN F_marketing_att_type_id;
  END GET_MARKETING_ATT_TYPE_ID;
  
  PROCEDURE TRANSACTIONS_HANDLER(P_user_id number, P_transaction_id number, P_tran_time_created date, P_login_id number)
  AS
    -- *** Declaration_section ***
    F_marketing_att_type_ftd NUMBER := 2;
    F_marketing_att_type_last_dep NUMBER := 3;
    F_marketing_att MARKETING_ATTRIBUTION_OBJ;
    F_user_obj USER_OBJ;
    F_ftd_attribution VARCHAR2(2000);
    F_is_change_attribution NUMBER := 1;
  BEGIN
    -- *** Execution_section ***
    -- Last deposit
    F_marketing_att := MARKETING.GET_MARKETING_ATT_BY_LOGIN(P_login_id, P_user_id);
    F_marketing_att.TYPE_ID := F_marketing_att_type_last_dep;
    F_marketing_att.USER_ID := P_user_id;
    F_marketing_att.AFFILIATE_KEY := MARKETING.GET_AFFILATE_KEY(F_marketing_att.DYNAMIC_PARAM, F_marketing_att.COMBINATION_ID);
    MARKETING.MERGE_MARKETING_ATT(F_marketing_att);
    -- First time deposit
    F_user_obj := UTILS.GET_USER(P_user_id);
    IF (F_user_obj.FIRST_DEPOSIT_ID = P_transaction_id OR F_user_obj.FIRST_DEPOSIT_ID is null)
        AND IS_EXISTS_MARKETING_ATT(P_user_id, F_marketing_att_type_ftd) = 0 
    THEN
      IF F_user_obj.MARKETING_ATT.AFFILIATE_KEY is not null and F_user_obj.MARKETING_ATT.AFFILIATE_KEY > 0
      THEN
        F_is_change_attribution := IS_AFFILATE_MARKETING_ATT(F_marketing_att);
      END IF;
      
      F_ftd_attribution := UTILS.GET_ENUM_VALUE('retargeting','ftd_attribution');
      IF (P_tran_time_created - F_user_obj.TIME_CREATED) > TO_NUMBER(F_ftd_attribution) 
          AND F_is_change_attribution = 1
      THEN
        -- Main marketing parameters
        F_marketing_att.TYPE_ID := F_marketing_att_type_ftd;
        
        -- !!! Comment out for staging deploy !!!
        --UPDATE_USERS(F_marketing_att);
        --UPDATE_UAD(F_marketing_att);
        -- !!! Comment out for staging deploy !!!
      ELSE
        -- get marketing attribution by user
        F_marketing_att := F_user_obj.MARKETING_ATT;        
      END IF;
      -- MERGE_MARKETING_ATT for ftd
      F_marketing_att.TYPE_ID := F_marketing_att_type_ftd;
      MARKETING.MERGE_MARKETING_ATT(F_marketing_att);
    END IF;
    
  END TRANSACTIONS_HANDLER;
  
  FUNCTION GET_MARKETING_ATT_BY_LOGIN(P_login_id number, P_user_id number)
    RETURN MARKETING_ATTRIBUTION_OBJ
  AS
    P_marketing_att MARKETING_ATTRIBUTION_OBJ;
  BEGIN
    select
      MARKETING_ATTRIBUTION_OBJ(P_user_id, 0, l.COMBINATION_ID, l.DYNAMIC_PARAM, l.AFF_SUB1, l.AFF_SUB2, l.AFF_SUB3, 0, 0, 0, null)
    into
      P_marketing_att
    from
      logins l
    where
      l.id = P_login_id;
    
    -- Get special code
    P_marketing_att.SPECIAL_CODE := MARKETING.GET_SPECIAL_CODE(P_marketing_att.DYNAMIC_PARAM, P_marketing_att.COMBINATION_ID);
    RETURN P_marketing_att;
    
    EXCEPTION
      WHEN OTHERS THEN
        SELECT                        
          MARKETING_ATTRIBUTION_OBJ(0, 0, s.DEFAULT_COMBINATION_ID, null, null, null, null, 0, 0, 0, null)
        into
          P_marketing_att
        FROM 
          skins s, 
          users u 
        WHERE 
          s.ID = u.SKIN_ID
          and u.id = P_user_id;
      RETURN P_marketing_att;
  END GET_MARKETING_ATT_BY_LOGIN;
  
  PROCEDURE MERGE_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
  IS
    -- *** Declaration_section ***
  BEGIN
    -- *** Execution_section ***
    MERGE INTO MARKETING_ATTRIBUTION USING dual ON ( "USER_ID"=P_marketing_att.USER_ID and "TYPE_ID"=P_marketing_att.TYPE_ID )
      WHEN MATCHED THEN 
        UPDATE SET 
          "COMBINATION_ID"=P_marketing_att.COMBINATION_ID,
          "DYNAMIC_PARAM"=P_marketing_att.DYNAMIC_PARAM,
          "AFF_SUB1"=P_marketing_att.AFF_SUB1,
          "AFF_SUB2"=P_marketing_att.AFF_SUB2,
          "AFF_SUB3"=P_marketing_att.AFF_SUB3,
          "AFFILIATE_KEY"=P_marketing_att.AFFILIATE_KEY,
          "SPECIAL_CODE"=P_marketing_att.SPECIAL_CODE,
          "AFFILIATE_SYSTEM"=P_marketing_att.AFFILIATE_SYSTEM,
          "AFFILIATE_SYSTEM_USER_TIME"=P_marketing_att.AFFILIATE_SYSTEM_USER_TIME,
          "TIME_UPDATED"=sysdate
      WHEN NOT MATCHED THEN 
        INSERT
        (
          "ID",
          "USER_ID",
          "TYPE_ID",
          "COMBINATION_ID",
          "DYNAMIC_PARAM",
          "AFF_SUB1",
          "AFF_SUB2",
          "AFF_SUB3",
          "AFFILIATE_KEY",
          "SPECIAL_CODE",
          "AFFILIATE_SYSTEM",
          "AFFILIATE_SYSTEM_USER_TIME",
          "TIME_CREATED",
          "TIME_UPDATED"
        )
        VALUES
        (
          SEQ_MARKETING_ATTRIBUTION.nextval ,
          P_marketing_att.USER_ID ,
          P_marketing_att.TYPE_ID ,
          P_marketing_att.COMBINATION_ID ,
          P_marketing_att.DYNAMIC_PARAM ,
          P_marketing_att.AFF_SUB1 ,
          P_marketing_att.AFF_SUB2 ,
          P_marketing_att.AFF_SUB3 ,
          P_marketing_att.AFFILIATE_KEY ,
          P_marketing_att.SPECIAL_CODE ,
          P_marketing_att.AFFILIATE_SYSTEM ,
          P_marketing_att.AFFILIATE_SYSTEM_USER_TIME ,
          sysdate ,
          null
        );
  END;
  
  PROCEDURE UPDATE_USERS(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
  IS
  BEGIN
    UPDATE 
      USERS u
    SET 
      u.DYNAMIC_PARAM = P_marketing_att.DYNAMIC_PARAM,
      u.COMBINATION_ID = P_marketing_att.COMBINATION_ID,
      u.AFFILIATE_KEY = P_marketing_att.AFFILIATE_KEY,
      u.SPECIAL_CODE = P_marketing_att.SPECIAL_CODE,
      u.AFFILIATE_SYSTEM = P_marketing_att.AFFILIATE_SYSTEM,
      u.MARKETING_ATTRIBUTION_TYPE_ID = P_marketing_att.TYPE_ID,
      u.AFFILIATE_SYSTEM_USER_TIME =  nvl(P_marketing_att.AFFILIATE_SYSTEM_USER_TIME, null)
    WHERE 
      u.ID = P_marketing_att.USER_ID;
  END;
  
  PROCEDURE UPDATE_UAD(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
  IS
  BEGIN
    UPDATE 
      USERS_ACTIVE_DATA uad
    SET
      uad.AFF_SUB1 = P_marketing_att.AFF_SUB1,
      uad.AFF_SUB2 = P_marketing_att.AFF_SUB2,
      uad.AFF_SUB3 = P_marketing_att.AFF_SUB3
    WHERE 
      uad.USER_ID = P_marketing_att.USER_ID;
  END;
  
  FUNCTION GET_SPECIAL_CODE(P_dynamic_param varchar2, P_comb_id number)
    RETURN varchar2
  AS
    F_special_code NVARCHAR2(15) := '0';
    F_netrefer_combination NUMBER;
  BEGIN
    -- for API we are reciving only this format API_XXXXXX_YYYYYY_specialCode_OtherParams
    -- XXXXXX - sub affiliate key
    -- YYYYYY - affiliate key
    -- SpecialCode - offline code on Netrefer
    -- OtherParams - other tracking params
    if P_dynamic_param is not null AND 
         (REGEXP_LIKE(P_dynamic_param, '^API_\d{6}_\d{6}_.*')  OR REGEXP_LIKE(P_dynamic_param, '^BR_\d{6}_\d{6}_.*') )          
    then  
      begin
        select
          1 
        into 
          F_netrefer_combination 
        from
          marketing_combinations mc
        where
          mc.id = P_comb_id
          and mc.campaign_id = 365;
             
        EXCEPTION WHEN no_data_found THEN
          F_netrefer_combination := 0;                         
      end;                                    
      if F_netrefer_combination = 1 then               
        -- Convert delimited string to array                                           
        F_special_code := regexp_substr(P_dynamic_param,'[^_]+',1,4);
      end if;                                                         
    end if;
    RETURN F_special_code;
  END GET_SPECIAL_CODE;
  
  FUNCTION IS_EXISTS_MARKETING_ATT(P_user_id NUMBER, P_marketing_att_type_id NUMBER)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_is_exists NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_MARKETING_ATT START');
    select
      1
    into
      F_is_exists
    from
      MARKETING_ATTRIBUTION ma
    where
      ma.USER_ID = P_user_id
      and ma.TYPE_ID = P_marketing_att_type_id;
    
    DBMS_OUTPUT.PUT_LINE('IS_EXISTS_MARKETING_ATT END');
    RETURN F_is_exists;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_EXISTS_MARKETING_ATT END');
      RETURN F_is_exists;
  END IS_EXISTS_MARKETING_ATT;
  
  FUNCTION IS_AFFILATE_MARKETING_ATT(P_marketing_att MARKETING_ATTRIBUTION_OBJ)
    RETURN NUMBER
  AS
    -- *** Declaration_section ***
    F_is_affilate NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_AFFILATE_MARKETING_ATT START');
    SELECT 
      1
    INTO
      F_is_affilate 
    FROM 
      DUAL
    WHERE
      (
        P_marketing_att.COMBINATION_ID in (select 
                                            id 
                                          from 
                                              marketing_combinations mcom
                                          where 
                                              mcom.campaign_id = 365) 
        and (
              LENGTH(SUBSTR(P_marketing_att.DYNAMIC_PARAM,INSTR(P_marketing_att.DYNAMIC_PARAM,'_')+1)) = 32 
              OR ( -- AFFILIATE API USERS 
                    LENGTH(SUBSTR(P_marketing_att.DYNAMIC_PARAM,INSTR(P_marketing_att.DYNAMIC_PARAM,'_')+1)) = 3
                    AND P_marketing_att.DYNAMIC_PARAM like '%_API'
                  )
            )
      )
      OR
      (
        P_marketing_att.SPECIAL_CODE is not null
        and exists (
                      select 
                        mac.affiliate_key 
                      from 
                        marketing_affilates_code mac 
                      where 
                        mac.SPECIAL_CODE = P_marketing_att.SPECIAL_CODE
                    )
      )
      OR
      (       
        (
          LENGTH(SUBSTR(P_marketing_att.DYNAMIC_PARAM,INSTR(P_marketing_att.DYNAMIC_PARAM,'_')+1)) = 32
          OR 
          ( -- AFFILIATE API USERS 
            LENGTH(SUBSTR(P_marketing_att.DYNAMIC_PARAM,INSTR(P_marketing_att.DYNAMIC_PARAM,'_')+1)) = 3
            AND P_marketing_att.DYNAMIC_PARAM like '%_API'
          )
        )
        and REGEXP_LIKE(SUBSTR(P_marketing_att.DYNAMIC_PARAM,0,INSTR(P_marketing_att.DYNAMIC_PARAM,'_')-1), '^[0-9]') 
        and (
              P_marketing_att.DYNAMIC_PARAM not like 'RPOA_%' 
              OR 
              P_marketing_att.COMBINATION_ID not in (
                                                      select 
                                                        id 
                                                      from 
                                                        marketing_combinations mcom
                                                      where 
                                                        mcom.campaign_id = 1535
                                                    )
            )
        )
        OR
        (      
          P_marketing_att.COMBINATION_ID in (
                                              select 
                                                id 
                                              from 
                                                marketing_combinations mcom
                                              where 
                                                mcom.campaign_id = 365
                                            ) 
          and P_marketing_att.DYNAMIC_PARAM is null
        );
    
    DBMS_OUTPUT.PUT_LINE('IS_AFFILATE_MARKETING_ATT END');
    RETURN F_is_affilate;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_AFFILATE_MARKETING_ATT END');
      RETURN F_is_affilate;
  END IS_AFFILATE_MARKETING_ATT;
  
  FUNCTION GET_AFFILATE_KEY(P_dynamic_param varchar2, P_comb_id number)
    RETURN number
  AS
    F_affiliate_key NVARCHAR2(15) := '0';
    F_netrefer_combination NUMBER;
  BEGIN
    if P_dynamic_param is not null AND 
         (REGEXP_LIKE(P_dynamic_param, '^\d{6}_.*'))
    then  
      begin
        select
          1 
        into 
          F_netrefer_combination 
        from
          marketing_combinations mc
        where
          mc.id = P_comb_id
          and mc.campaign_id = 365;
             
        EXCEPTION WHEN no_data_found THEN
          F_netrefer_combination := 0;                         
      end;                                    
      if F_netrefer_combination = 1 then               
        -- Convert delimited string to array                                           
        F_affiliate_key := regexp_substr(P_dynamic_param,'\d{6}');
      end if;                                                         
    end if;
    RETURN F_affiliate_key;
  END GET_AFFILATE_KEY;

End MARKETING;

/