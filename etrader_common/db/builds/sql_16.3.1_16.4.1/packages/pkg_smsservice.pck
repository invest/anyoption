CREATE OR REPLACE PACKAGE etrader.pkg_smsservice IS

  -- Author  : VICTORS
  -- Created : 2016-03-28 11:08:48
  -- Purpose : 

  PROCEDURE load_messages
  (
    plist       OUT SYS_REFCURSOR,
    ploadedlist IN numbers_table,
    plimit      IN NUMBER
  );

END pkg_smsservice;
/
CREATE OR REPLACE PACKAGE BODY etrader.pkg_smsservice IS

  PROCEDURE load_messages
  (
    plist       OUT SYS_REFCURSOR,
    ploadedlist IN numbers_table,
    plimit      IN NUMBER
  ) IS
  BEGIN
    IF ploadedlist IS NULL THEN
      raise_application_error(-20000, 'ploadedlist IS NULL');
    END IF;
  
    OPEN plist FOR
      SELECT *
        FROM (SELECT id,
                     sms_type_id,
                     sender,
                     phone,
                     message,
                     wap_url,
                     dst_port,
                     sms_provider_id,
                     retries,
                     scheduled_time,
                     time_sent,
                     sms_status_id,
                     reference,
                     error,
                     key_value,
                     key_type,
                     provider_id,
                     sender_number,
                     sms_description_id
                FROM sms
               WHERE sms_status_id = 1
                 AND scheduled_time <= SYSDATE
                 AND id NOT MEMBER OF ploadedlist
               ORDER BY scheduled_time, id)
       WHERE ROWNUM < plimit;
  END load_messages;

END pkg_smsservice;
/
