﻿/*
begin
  for i in (select * from dba_sys_privs where grantee = 'DBA')
  loop
    execute immediate 'grant ' || i.privilege || ' to victor with admin option';
  end loop;
end;
/
*/

create or replace package pkg_cleanup_data is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-04-24 13:25:26
  -- Purpose : clean data from database

  procedure truncate_table
  (
    i_owner     in varchar2
   ,i_tablename in varchar2
  );

  procedure truncate_part_table
  (
    i_owner     in varchar2
   ,i_tablename in varchar2
   ,i_mindate   in date default null
  );

end pkg_cleanup_data;
/
create or replace package body pkg_cleanup_data is
  type t_sqls is table of varchar2(4000) index by pls_integer;

  procedure truncate_part_table
  (
    i_owner     in varchar2
   ,i_tablename in varchar2
   ,i_mindate   in date default null
  ) is
    l_enable t_sqls;
    l_date   date;
  begin
    for i in (select rownum rn, c.*
              from   dba_constraints c
              where  c.r_owner = i_owner
              and    c.r_constraint_name = (select constraint_name
                                            from   dba_constraints
                                            where  owner = i_owner
                                            and    table_name = i_tablename
                                            and    constraint_type = 'P')
              and    c.status = 'ENABLED')
    loop
      execute immediate 'alter table ' || i.owner || '.' || i.table_name || ' modify constraint ' || i.constraint_name || ' disable';
    
      l_enable(i.rn) := 'alter table ' || i.owner || '.' || i.table_name || ' modify constraint ' || i.constraint_name ||
                        ' enable novalidate';
    end loop;
  
    for i in (select *
              from   dba_tab_partitions
              where  table_name = i_tablename
              and    table_owner = i_owner
              order  by partition_position)
    loop
      execute immediate 'select ' || i.high_value || ' from dual'
        into l_date;
      if (i_mindate is null or l_date <= i_mindate)
      then
        if i.partition_position > 1
        then
          execute immediate 'alter table ' || i.table_owner || '.' || i.table_name || '  drop partition ' || i.partition_name;
        else
          execute immediate 'alter table ' || i.table_owner || '.' || i.table_name || '  truncate partition ' || i.partition_name;
        end if;
      end if;
    end loop;
  
    for i in 1 .. l_enable.count
    loop
      execute immediate l_enable(i);
    end loop;
  
    for i in (select *
              from   dba_indexes
              where  status = 'UNUSABLE'
              and    table_name = i_tablename
              and    owner = i_owner)
    loop
      execute immediate 'alter index ' || i.owner || '.' || i.index_name || ' rebuild';
    end loop;
  end truncate_part_table;

  procedure truncate_table
  (
    i_owner     in varchar2
   ,i_tablename in varchar2
  ) is
    l_enable t_sqls;
  begin
    for i in (select rownum rn, c.*
              from   dba_constraints c
              where  c.r_owner = i_owner
              and    c.r_constraint_name = (select constraint_name
                                            from   dba_constraints
                                            where  owner = i_owner
                                            and    table_name = i_tablename
                                            and    constraint_type = 'P')
              and    c.status = 'ENABLED')
    loop
      execute immediate 'alter table ' || i.owner || '.' || i.table_name || ' modify constraint ' || i.constraint_name || ' disable';
    
      l_enable(i.rn) := 'alter table ' || i.owner || '.' || i.table_name || ' modify constraint ' || i.constraint_name ||
                        ' enable novalidate';
    end loop;
  
    begin
      execute immediate 'truncate table ' || i_owner || '.' || i_tablename;
    exception
      when others then
        dbms_output.put_line('Unable to truncate table ' || i_owner || '.' || i_tablename);
        dbms_output.put_line(dbms_utility.format_error_stack);
    end;
  
    for i in 1 .. l_enable.count
    loop
      execute immediate l_enable(i);
    end loop;
  
    for i in (select *
              from   dba_indexes
              where  status = 'UNUSABLE'
              and    table_name = i_tablename
              and    owner = i_owner)
    loop
      execute immediate 'alter index ' || i.owner || '.' || i.index_name || ' rebuild';
    end loop;
  
  end truncate_table;

end pkg_cleanup_data;
/
