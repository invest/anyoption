create or replace package pkg_writers is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-04-21 10:16:15
  -- Purpose : 

  procedure create_permissions
  (
    i_space_name       in writer_screen_permissions.spacename%type
   ,i_screen_name      in writer_screen_permissions.screenname%type
   ,i_permission_name  in writer_screen_permissions.permission%type
   ,i_permission_order in number
  );

  procedure get_writer_screens
  (
    o_screens   out sys_refcursor
   ,i_writer_id in writers.id%type
  );

  procedure get_writer_screen_perms
  (
    o_screens     out sys_refcursor
   ,i_writer_id   in writers.id%type
   ,i_screen_name in writer_screen_permissions.screenname%type
  );

  procedure get_department_writers
  (
    o_writers out sys_refcursor
   ,i_role_id in writer_roles.id%type default null
  );

  procedure get_roles(o_roles out sys_refcursor);

  procedure get_role_permissions
  (
    o_permissions out sys_refcursor
   ,i_role_id     in writer_roles.id%type default null
  );

  procedure update_role_permissions
  (
    i_screen_names in string_table
   ,i_perm_names   in string_table
   ,i_role_id      in writer_roles.id%type
  );

  procedure update_role
  (
    i_role_id   in writer_roles.id%type
   ,i_role_name in writer_roles.rolename%type
  );

  procedure create_role
  (
    o_role_id   out writer_roles.id%type
   ,i_role_name in writer_roles.rolename%type
  );

  procedure set_writers_role
  (
    o_unset_writers out sys_refcursor
   ,i_writers       in number_table
   ,i_role_id       in writer_roles.id%type
  );

  procedure unset_writers_role(i_writers in number_table);

  procedure get_writers
  (
    o_writers       out sys_refcursor
   ,i_page_number   in number default 1
   ,i_rows_per_page in number default 20
   ,i_departments   in number_table default null
   ,i_roles         in number_table default null
   ,i_skins         in number_table default null
   ,i_text_filter   in varchar2 default null
   ,i_is_active     in number default null
   ,i_writer_id     in number default null
  );

  procedure create_writer
  (
    o_writer_id       out writers.id%type
   ,i_user_name       in writers.user_name%type
   ,i_first_name      in writers.first_name%type
   ,i_last_name       in writers.last_name%type
   ,i_email           in writers.email%type
   ,i_mobile_phone    in writers.mobile_phone%type
   ,i_dept_id         in writers.dept_id%type
   ,i_nick_name_first in writers.nick_name_first%type
   ,i_nick_name_last  in writers.nick_name_last%type
   ,i_emp_id          in writers.emp_id%type
   ,i_role_id         in writers.role_id%type
   ,i_password        in writers.password%type
   ,i_skins           in number_table
  );

  procedure update_writer
  (
    i_writer_id       in writers.id%type
   ,i_first_name      in writers.first_name%type
   ,i_last_name       in writers.last_name%type
   ,i_email           in writers.email%type
   ,i_mobile_phone    in writers.mobile_phone%type
   ,i_nick_name_first in writers.nick_name_first%type
   ,i_nick_name_last  in writers.nick_name_last%type
   ,i_emp_id          in writers.emp_id%type
   ,i_dept_id         in writers.dept_id%type
   ,i_role_id         in writers.role_id%type
   ,i_is_active       in writers.is_active%type
   ,i_skins           in number_table
  );

  procedure get_existing_usernames
  (
    o_usernames     out sys_refcursor
   ,i_lookup_string in varchar2
  );

  procedure load_filters(o_departments out sys_refcursor);
end pkg_writers;
/
create or replace package body pkg_writers is

  procedure create_permissions
  (
    i_space_name       in writer_screen_permissions.spacename%type
   ,i_screen_name      in writer_screen_permissions.screenname%type
   ,i_permission_name  in writer_screen_permissions.permission%type
   ,i_permission_order in number
  ) is
    l_next_id number;
  begin
    insert into writer_screen_permissions
      (id, spacename, screenname, permission, permorder)
    values
      (writer_screen_permissions_seq.nextval, i_space_name, i_screen_name, i_permission_name, i_permission_order)
    returning id into l_next_id;
  
  end create_permissions;

  procedure get_writer_screens
  (
    o_screens   out sys_refcursor
   ,i_writer_id in writers.id%type
  ) is
  begin
  
    open o_screens for
      select wsp.spacename, wsp.screenname, wsp.permission
      from   writers w
      join   writer_roles r
      on     r.id = w.role_id
      join   writer_role_permissions wrp
      on     wrp.role_id = r.id
      join   writer_screen_permissions wsp
      on     wsp.id = wrp.permission_id
      where  w.id = i_writer_id
      and    wsp.permission = 'view'
      order  by wsp.permorder;
  
  end get_writer_screens;

  procedure get_writer_screen_perms
  (
    o_screens     out sys_refcursor
   ,i_writer_id   in writers.id%type
   ,i_screen_name in writer_screen_permissions.screenname%type
  ) is
  begin
  
    open o_screens for
      select wsp.spacename, wsp.screenname, wsp.permission
      from   writers w
      join   writer_roles r
      on     r.id = w.role_id
      join   writer_role_permissions wrp
      on     wrp.role_id = r.id
      join   writer_screen_permissions wsp
      on     wsp.id = wrp.permission_id
      where  w.id = i_writer_id
      and    (wsp.spacename, wsp.screenname) in (select wsp1.spacename, wsp1.screenname
                                                 from   writers w1
                                                 join   writer_roles r1
                                                 on     r1.id = w1.role_id
                                                 join   writer_role_permissions wrp1
                                                 on     wrp1.role_id = r1.id
                                                 join   writer_screen_permissions wsp1
                                                 on     wsp1.id = wrp1.permission_id
                                                 where  w1.id = i_writer_id
                                                 and    wsp1.permission = 'view'
                                                 and    wsp1.screenname = i_screen_name)
      order  by wsp.permorder;
  
  end get_writer_screen_perms;

  procedure get_department_writers
  (
    o_writers out sys_refcursor
   ,i_role_id in writer_roles.id%type default null
  ) is
  begin
  
    open o_writers for
      select d.dept_name
            ,d.id dept_id
            ,ww.writer_id
            ,ww.user_name
            ,case
               when ww.cnt_dep > ww.cnt_dep_role then
                1
               else
                0
             end multitude
            ,ww.rolename
            ,ww.role_id
      from   departments d
      left   join (select *
                   from   (select w.id writer_id
                                 ,w.dept_id
                                 ,r.rolename
                                 ,w.role_id
                                 ,w.user_name
                                 ,count(*) over(partition by w.dept_id, w.role_id) cnt_dep_role
                                 ,count(*) over(partition by w.dept_id) cnt_dep
                           from   writers w
                           left   join etrader.writer_roles r
                           on     r.id = w.role_id
                           where  w.is_active = 1)
                   where  (i_role_id is null or role_id = i_role_id)) ww
      on     ww.dept_id = d.id
      where  d.id != 9 -- remove System department
      order  by dept_name, user_name;
  
  end get_department_writers;

  procedure get_roles(o_roles out sys_refcursor) is
  begin
  
    open o_roles for
      select w.id, w.rolename from writer_roles w order by w.rolename;
  
  end get_roles;

  procedure get_role_permissions
  (
    o_permissions out sys_refcursor
   ,i_role_id     in writer_roles.id%type default null
  ) is
  begin
  
    open o_permissions for
      select wrp.role_id, wsp.spacename, wsp.screenname, wsp.permission
      from   writer_role_permissions wrp
      join   writer_screen_permissions wsp
      on     wsp.id = wrp.permission_id
      where  (i_role_id is null or wrp.role_id = i_role_id);
  
  end get_role_permissions;

  procedure update_role_permissions
  (
    i_screen_names in string_table
   ,i_perm_names   in string_table
   ,i_role_id      in writer_roles.id%type
  ) is
    l_perms number_table := number_table();
  begin
    l_perms.extend(i_screen_names.count);
  
    for i in 1 .. i_screen_names.count
    loop
      select wsp.id
      into   l_perms(i)
      from   writer_screen_permissions wsp
      where  upper(wsp.screenname) = upper(i_screen_names(i))
      and    upper(wsp.permission) = upper(i_perm_names(i));
    end loop;
  
    insert into writer_role_permissions
      (role_id, permission_id)
      select /*+ cardinality(t, 1) */
       i_role_id, t.column_value
      from   table(l_perms) t
      where  t.column_value not in (select permission_id from writer_role_permissions where role_id = i_role_id);
  
    delete from writer_role_permissions
    where  permission_id not member of l_perms
    and    role_id = i_role_id;
  end update_role_permissions;

  procedure update_role
  (
    i_role_id   in writer_roles.id%type
   ,i_role_name in writer_roles.rolename%type
  ) is
  begin
    update writer_roles r
    set    r.rolename = i_role_name
    where  r.id = i_role_id
    and    r.rolename <> i_role_name;
  exception
    when dup_val_on_index then
      raise_application_error(-20000, 'Role name is already in use (update): ' || i_role_name, false);
  end update_role;

  procedure create_role
  (
    o_role_id   out writer_roles.id%type
   ,i_role_name in writer_roles.rolename%type
  ) is
  begin
    insert into writer_roles (id, rolename) values (writer_roles_seq.nextval, i_role_name) returning id into o_role_id;
  exception
    when dup_val_on_index then
      raise_application_error(-20000, 'Role name is already in use (insert): ' || i_role_name, false);
  end create_role;

  procedure set_writers_role
  (
    o_unset_writers out sys_refcursor
   ,i_writers       in number_table
   ,i_role_id       in writer_roles.id%type
  ) is
    l_role_id       number;
    l_unset_writers number_table := number_table();
  begin
    if i_writers is not null
    then
      for i in 1 .. i_writers.count
      loop
        update writers w
        set    w.role_id = case
                             when w.role_id is not null then
                              w.role_id
                             else
                              i_role_id
                           end
        where  w.id = i_writers(i)
        returning w.role_id into l_role_id;
      
        if l_role_id != i_role_id
        then
          l_unset_writers.extend;
          l_unset_writers(l_unset_writers.last) := i_writers(i);
        end if;
      end loop;
    end if;
  
    open o_unset_writers for
      select w.user_name, d.dept_name, r.rolename
      from   departments d
      join   writers w
      on     w.dept_id = d.id
      join   writer_roles r
      on     r.id = w.role_id
      where  w.id in (select /*+ cardinality(t,1) */
                       column_value
                      from   table(l_unset_writers) t);
  end set_writers_role;

  procedure unset_writers_role(i_writers in number_table) is
  begin
    if i_writers is not null
    then
      forall i in 1 .. i_writers.count
        update writers w
        set    w.role_id = null
        where  w.id = i_writers(i)
        and    w.role_id is not null;
    end if;
  end unset_writers_role;

  procedure get_writers
  (
    o_writers       out sys_refcursor
   ,i_page_number   in number default 1
   ,i_rows_per_page in number default 20
   ,i_departments   in number_table default null
   ,i_roles         in number_table default null
   ,i_skins         in number_table default null
   ,i_text_filter   in varchar2 default null
   ,i_is_active     in number default null
   ,i_writer_id     in number default null
  ) is
    l_text_filter varchar2(100) := '%' || upper(replace(replace(replace(i_text_filter, '%', '\%'), '_', '\_'), '\', '\\')) || '%';
  begin
  
    open o_writers for
      select a.*, cursor (select s.skin_id id from writers_skin s where s.writer_id = a.writer_id) skins
      from   (select count(*) over() total_count
                    ,row_number() over(order by w.time_created desc) rn
                    ,w.id writer_id
                    ,w.user_name
                    ,w.first_name
                    ,w.last_name
                    ,d.dept_name
                    ,r.rolename
                    ,w.email
                    ,w.mobile_phone
                    ,w.is_active
                    ,w.nick_name_first
                    ,w.nick_name_last
                    ,w.time_created
                    ,w.role_id
                    ,w.dept_id
              from   writers w
              join   departments d
              on     d.id = w.dept_id
              left   join writer_roles r
              on     r.id = w.role_id
              where  (i_writer_id is null or w.id = i_writer_id)
              and    (i_is_active is null or w.is_active = i_is_active)
              and    (i_text_filter is null or
                    (upper(w.user_name) like l_text_filter escape '\' or upper(w.first_name) like l_text_filter escape
                     '\' or upper(w.last_name) like l_text_filter escape '\' or upper(w.email) like l_text_filter escape
                     '\' or upper(w.nick_name_first) like l_text_filter escape '\' or upper(w.nick_name_last) like l_text_filter escape '\'))
              and    (i_skins is null or exists (select 1 from writers_skin where skin_id member of i_skins))
              and    (i_roles is null or exists (select 1 from writer_roles where role_id member of i_roles))
              and    (i_departments is null or w.dept_id member of i_departments)) a
      where  rn > (i_page_number - 1) * i_rows_per_page
      and    rn <= i_page_number * i_rows_per_page
      order  by time_created desc;
  
  end get_writers;

  procedure create_writer
  (
    o_writer_id       out writers.id%type
   ,i_user_name       in writers.user_name%type
   ,i_first_name      in writers.first_name%type
   ,i_last_name       in writers.last_name%type
   ,i_email           in writers.email%type
   ,i_mobile_phone    in writers.mobile_phone%type
   ,i_dept_id         in writers.dept_id%type
   ,i_nick_name_first in writers.nick_name_first%type
   ,i_nick_name_last  in writers.nick_name_last%type
   ,i_emp_id          in writers.emp_id%type
   ,i_role_id         in writers.role_id%type
   ,i_password        in writers.password%type
   ,i_skins           in number_table
  ) is
  begin
    insert into writers
      (id
      ,user_name
      ,first_name
      ,last_name
      ,email
      ,mobile_phone
      ,is_active
      ,dept_id
      ,nick_name_first
      ,nick_name_last
      ,emp_id
      ,role_id
      ,password
      ,time_created)
    values
      (seq_writers.nextval
      ,i_user_name
      ,i_first_name
      ,i_last_name
      ,i_email
      ,i_mobile_phone
      ,1
      ,i_dept_id
      ,i_nick_name_first
      ,i_nick_name_last
      ,i_emp_id
      ,i_role_id
      ,i_password
      ,sysdate)
    returning id into o_writer_id;
  
    if i_skins is not null
    then
      forall i in 1 .. i_skins.count
        insert into writers_skin (id, skin_id, writer_id) values (seq_writers_skin.nextval, i_skins(i), o_writer_id);
    end if;
  
    --insert into roles (id, user_name, role) values (seq_roles.nextval, 'GEORGIDO', 'tv');
  
    --insert into issue_action_writer_permission (writer_id, issue_action_type_id, is_active) values (635, 75, 1);  
  end create_writer;

  procedure update_writer
  (
    i_writer_id       in writers.id%type
   ,i_first_name      in writers.first_name%type
   ,i_last_name       in writers.last_name%type
   ,i_email           in writers.email%type
   ,i_mobile_phone    in writers.mobile_phone%type
   ,i_nick_name_first in writers.nick_name_first%type
   ,i_nick_name_last  in writers.nick_name_last%type
   ,i_emp_id          in writers.emp_id%type
   ,i_dept_id         in writers.dept_id%type
   ,i_role_id         in writers.role_id%type
   ,i_is_active       in writers.is_active%type
   ,i_skins           in number_table
  ) is
  begin
    update writers
    set    first_name      = i_first_name
          ,last_name       = i_last_name
          ,email           = i_email
          ,mobile_phone    = i_mobile_phone
          ,nick_name_first = i_nick_name_first
          ,nick_name_last  = i_nick_name_last
          ,emp_id          = i_emp_id
          ,dept_id         = i_dept_id
          ,role_id         = i_role_id
          ,is_active       = i_is_active
    where  id = i_writer_id;
  
    insert into writers_skin
      (id, skin_id, writer_id, assign_limit)
      select /*+ cardinality(t,1) */
       seq_writers_skin.nextval, t.column_value, i_writer_id, 0
      from   table(i_skins) t
      where  t.column_value not in (select skin_id from writers_skin where writer_id = i_writer_id);
  
    delete from writers_skin
    where  writer_id = i_writer_id
    and    skin_id not member of i_skins;
  end update_writer;

  procedure get_existing_usernames
  (
    o_usernames     out sys_refcursor
   ,i_lookup_string in varchar2
  ) is
  begin
    open o_usernames for
      select user_name from writers where upper(user_name) like upper(i_lookup_string) || '%' order by upper(user_name);
  end get_existing_usernames;

  procedure load_filters(o_departments out sys_refcursor) is
  begin
    open o_departments for
      select d.id, d.dept_name value from departments d order by d.dept_name;
  
  end load_filters;

end pkg_writers;
/
