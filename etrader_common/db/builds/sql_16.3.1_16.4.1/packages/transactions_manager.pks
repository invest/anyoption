CREATE OR REPLACE
PACKAGE "TRANSACTIONS_MANAGER"
IS
FUNCTION VELOCITY_CHECK(p_usr_id number)
  return number;
  
FUNCTION IS_CC_FORWARD_DOCUMENTS(p_usr_id number)
  return number;
  
FUNCTION VELOCIY_CHECK_IS_ACTIVE(p_usr_id number)
  return number;
PROCEDURE MAINTENANCE_FEE;
FUNCTION MAINTENANCE_FEE_ISSUE_64(p_user_id IN NUMBER) 
  RETURN  NUMBER;
FUNCTION MAINTENANCE_FEE_ISSUE_65(p_user_id IN NUMBER) 
  RETURN  NUMBER;    
FUNCTION IS_BLACK_LIST_BIN(p_bin_id number)
  return number;
End TRANSACTIONS_MANAGER;
/


create or replace PACKAGE BODY         "TRANSACTIONS_MANAGER" is


FUNCTION VELOCITY_CHECK(p_usr_id number)
  return number is

CHECK_HOURS CONSTANT NUMBER :=6;
HAVE_DEPOSIT_DOCUMENTS CONSTANT NUMBER :=1;
ET_CUSTOMER CONSTANT NUMBER :=1;

AO_NO_DOC_DEPOSITS NUMBER :=1;
AO_HAVE_DOC_DEPOSITS NUMBER :=2;
ET_NO_DOC_DEPOSITS NUMBER :=3;
ET_HAVE_DOC_DEPOSITS NUMBER :=4;

v_deposits_num number:=-1;
v_id_doc_verify number :=-1;
v_skin_id number :=-1;

cursor c_get_data is
select count(*) deposits_num , u.skin_id
from
transactions tr,
users u
where
tr.user_id = u.id
and tr.type_id = 1
and tr.status_id in (2,7)
and  ((sysdate - tr.time_created)*24)<= CHECK_HOURS
and u.id=p_usr_id
group by id_doc_verify, u.skin_id;

cursor c_get_data_after_activate(pi_actv_date date) is
select count(*) deposits_num , u.skin_id
from
transactions tr,
users u
where
tr.user_id = u.id
and tr.type_id = 1
and tr.status_id in (2,7)
and  ((sysdate - tr.time_created)*24)<= CHECK_HOURS
and pi_actv_date <=tr.time_created
and u.id=p_usr_id
group by id_doc_verify, u.skin_id;

v_resault number :=0;


cursor c_check_rec_vc is
select vc.issue_actions_id,vc.is_active, vc.time_created
from
velocity_check vc
where vc.user_id = p_usr_id
order by vc.id desc;

v_check_rec_vc number:=0;
vc_is_active number :=-1;
v_activate_date date :=sysdate;

BEGIN

IF VELOCIY_CHECK_IS_ACTIVE(p_usr_id) = 1 THEN



open c_check_rec_vc;
fetch c_check_rec_vc into v_check_rec_vc,vc_is_active,v_activate_date;
close c_check_rec_vc;

if (vc_is_active>0) then
 open c_get_data_after_activate(v_activate_date);
fetch c_get_data_after_activate into v_deposits_num,v_skin_id;
close c_get_data_after_activate; 
 else 
   open c_get_data;
fetch c_get_data into v_deposits_num,v_skin_id;
close c_get_data;

end if;

if v_deposits_num in (20) or (v_check_rec_vc>0 and v_deposits_num>=20)  then
v_id_doc_verify:=IS_CC_FORWARD_DOCUMENTS(p_usr_id);
end if;

IF (v_skin_id!=ET_CUSTOMER and v_id_doc_verify != HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1  and v_deposits_num >= 20) THEN

   v_resault :=AO_NO_DOC_DEPOSITS;

ELSIF (v_skin_id!=ET_CUSTOMER and v_id_doc_verify = HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=AO_HAVE_DOC_DEPOSITS;

ELSIF (v_skin_id = ET_CUSTOMER and v_id_doc_verify != HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=ET_NO_DOC_DEPOSITS;

ELSIF (v_skin_id = ET_CUSTOMER and v_id_doc_verify = HAVE_DEPOSIT_DOCUMENTS and v_id_doc_verify!=-1 and v_deposits_num >= 20) THEN

     v_resault :=ET_HAVE_DOC_DEPOSITS;
ELSE
     v_resault:=0;

END IF;

END IF;
return v_resault;

END;
-----------------------------------------------------------------------------
--
-----------------------------------------------------------------------------

FUNCTION IS_CC_FORWARD_DOCUMENTS(p_usr_id number)
  return number is

cursor c_get_sent_cc_copy is
select sum(cop.CC_COPY)
from (
select case when nvl(sent_cc_copy.cc_id,-1) = -1 then 1 else 0 end as CC_COPY
from
(select
distinct tr.credit_card_id, tr.user_id
from
transactions tr
where
 tr.type_id = 1
and tr.status_id in (2,7)
and months_between(sysdate,tr.time_created)<=6
and tr.user_id=p_usr_id) past_cc,
(select fl.cc_id,fl.user_id
from files fl
where
fl.is_approved=1
and fl.file_status_id =3
and fl.file_type_id in (4,23)
and fl.user_id=p_usr_id) sent_cc_copy
where
past_cc.credit_card_id=sent_cc_copy.cc_id(+)
and past_cc.user_id=sent_cc_copy.user_id(+)
) cop;

v_sent_cc_copy NUMBER:=0;

--
cursor c_get_user_sent_id is
select f.id
  from files f
 where f.IS_APPROVED = 1
   and f.FILE_STATUS_ID = 3
   and f.FILE_TYPE_ID in (1,2,21)
   and f.user_id = p_usr_id;

v_user_sent_id NUMBER:=0;


BEGIN
  open c_get_sent_cc_copy;
  fetch c_get_sent_cc_copy into v_sent_cc_copy;
  close c_get_sent_cc_copy;

  open c_get_user_sent_id;
  fetch c_get_user_sent_id into v_user_sent_id;
  close c_get_user_sent_id;


   if v_sent_cc_copy = 0 and v_user_sent_id > 0 then
       return 1;
    else
       return 0;
    end if;
END;

-----------------------------------------------------------------------------
--
-----------------------------------------------------------------------------

FUNCTION VELOCIY_CHECK_IS_ACTIVE(p_usr_id number)
  return number is

  cursor c_get_vel_ch is
  select vc.is_active
    from velocity_check vc
   where vc.user_id = p_usr_id
   order by vc.id desc;

   res number:= 1;

  BEGIN
    open c_get_vel_ch;
    fetch c_get_vel_ch  into res;
    close c_get_vel_ch;
    return res;
  END;
  
procedure maintenance_fee is

cursor cmain is
select * from M_FEE m
WHERE     NOT EXISTS
                  (SELECT 1
                     FROM investments i
                    WHERE     i.time_created > TRUNC (SYSDATE - 183)
                          AND m.id = i.user_id)
       AND NOT EXISTS
                  (SELECT 1
                     FROM transactions t
                    WHERE     t.time_created > TRUNC (SYSDATE - 183)
                          AND m.id = t.user_id
                          AND t.type_id = 47)
       AND NOT EXISTS
                  (SELECT 1
                     FROM transactions t, transaction_types tt
                    WHERE     t.type_id = tt.id
                          AND tt.class_type = 2
                          AND t.status_id = 4
                          AND m.id = t.user_id)
       AND NOT EXISTS
                  (SELECT 1
                     FROM transactions t, transaction_types tt
                    WHERE     t.type_id = tt.id
                          AND tt.class_type = 1
                          AND t.status_id IN (2, 7)
                          AND t.time_created > SYSDATE - 7
                          AND m.id = t.user_id);

                                     
cursor issue_check (p_user_id NUMBER) is
select max(iss.time_created) from issues iss where ISS.USER_ID = p_user_id and iss.subject_id = 65;

cursor bonus_check (bu_user_id NUMBER) is
select count(*) from bonus_users bu where bu.bonus_state_id in (1,2,3) and bu.user_id = bu_user_id and bu.time_created > TRUNC (SYSDATE - 183); 

v_issue_check DATE;
trans_id NUMBER;
ttime DATE;
v_bonus_check NUMBER;
                                     
begin

for tmp in cmain loop

    open issue_check (tmp.id);
    fetch issue_check into v_issue_check;
    close issue_check;
    
    open bonus_check (tmp.id);
    fetch bonus_check into v_bonus_check;
    close bonus_check;
    
if v_bonus_check = 0 then
    
if v_issue_check < sysdate-7 then
    if tmp.amount > tmp.balance then
    update users u set u.balance = 0 where u.id = tmp.id;
    Insert into TRANSACTIONS
   (ID, USER_ID, TYPE_ID, TIME_CREATED, AMOUNT, STATUS_ID, DESCRIPTION, WRITER_ID, IP, TIME_SETTLED, COMMENTS, PROCESSED_WRITER_ID, FEE_CANCEL, 
   IS_ACCOUNTING_APPROVED, UTC_OFFSET_SETTLED, UTC_OFFSET_CREATED, RATE, CLEARING_PROVIDER_ID, FEE_CANCEL_BY_ADMIN, CREDIT_AMOUNT, 
   DEPOSIT_REFERENCE_ID, SPLITTED_REFERENCE_ID, IS_CREDIT_WITHDRAWAL, PAYMENT_TYPE_ID, 
   INTERNALS_AMOUNT, UPDATE_BY_ADMIN, IS_REROUTED, REROUTING_TRANSACTION_ID, IS_MANUAL_ROUTING, SELECTOR_ID)
 Values
   (seq_transactions.nextval, tmp.id, 47, sysdate, 
    tmp.balance, 2, 'transactions.maintenance.fee', 0, 'IP NOT FOUND!', 
    sysdate, 'Maintenance fee', 0, 
    0, 0, 'GMT+02:00', 'GMT+02:00', 
    trunc(CONVERT_AMOUNT_TO_USD(1,tmp.currency_id,sysdate),5) , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1) RETURNING ID,TIME_CREATED INTO trans_id,ttime; 
Insert into BALANCE_HISTORY
   (ID, USER_ID, BALANCE, TAX_BALANCE, WRITER_ID, TIME_CREATED, TABLE_NAME, KEY_VALUE, COMMAND, UTC_OFFSET)
 Values
   (seq_balance_history.nextval, tmp.id, 0, tmp.tax_balance, 0, 
    ttime, 'transactions', trans_id, '45', tmp.utc_offset);
    else
    Insert into TRANSACTIONS
   (ID, USER_ID, TYPE_ID, TIME_CREATED, AMOUNT, STATUS_ID, DESCRIPTION, WRITER_ID, IP, TIME_SETTLED, COMMENTS, PROCESSED_WRITER_ID, FEE_CANCEL, 
   IS_ACCOUNTING_APPROVED, UTC_OFFSET_SETTLED, UTC_OFFSET_CREATED, RATE, CLEARING_PROVIDER_ID, FEE_CANCEL_BY_ADMIN, CREDIT_AMOUNT, 
   DEPOSIT_REFERENCE_ID, SPLITTED_REFERENCE_ID, IS_CREDIT_WITHDRAWAL, PAYMENT_TYPE_ID, 
   INTERNALS_AMOUNT, UPDATE_BY_ADMIN, IS_REROUTED, REROUTING_TRANSACTION_ID, IS_MANUAL_ROUTING, SELECTOR_ID)
 Values
   (seq_transactions.nextval, tmp.id, 47, sysdate, 
    tmp.amount, 2, 'transactions.maintenance.fee', 0, 'IP NOT FOUND!', 
    sysdate, 'Maintenance fee', 0, 
    0, 0, 'GMT+02:00', 'GMT+02:00', 
    trunc(CONVERT_AMOUNT_TO_USD(1,tmp.currency_id,sysdate),5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1) RETURNING ID,TIME_CREATED INTO trans_id,ttime;
    Insert into BALANCE_HISTORY
   (ID, USER_ID, BALANCE, TAX_BALANCE, WRITER_ID, TIME_CREATED, TABLE_NAME, KEY_VALUE, COMMAND, UTC_OFFSET)
 Values
   (seq_balance_history.nextval, tmp.id, tmp.balance - tmp.amount, tmp.tax_balance, 0, 
    ttime, 'transactions', trans_id, '45', tmp.utc_offset);    
    update users u set u.balance = u.balance - tmp.amount where u.id = tmp.id;
    end if;
end if;
commit;
else null;
end if;
end loop;
--commit;
null;
end maintenance_fee;

function maintenance_fee_issue_64(p_user_id IN NUMBER) RETURN  NUMBER as
flag NUMBER;

cursor cmain is
select max(issue_time)issue_time,max(inv_time)inv_time from (
select null as issue_time,i.time_created inv_time from investments i where i.user_id = p_user_id
union all
select iss.time_created,null from issues iss where iss.user_id = p_user_id and iss.subject_id = 64);

cursor balance is
select min(time_created) from balance_history bh where bh.user_id = p_user_id;

v_balance_check DATE;

begin
for tmp in cmain loop
flag := null;



 if tmp.inv_time is null and tmp.issue_time is null then
 open balance;
fetch balance into v_balance_check;
close balance;
 if v_balance_check < sysdate - 155 then
    flag := 1;
    end if;
    GOTO end_check;
 elsif tmp.inv_time is null and tmp.issue_time < sysdate - 183 then
    flag := 1;
       GOTO end_check;
 elsif tmp.issue_time is null and tmp.inv_time < sysdate - 155 then 
    flag := 1;
       GOTO end_check;
 elsif tmp.inv_time >tmp.issue_time and tmp.inv_time < sysdate - 155 then
    flag := 1;
       GOTO end_check;
 elsif tmp.inv_time < tmp.issue_time and tmp.issue_time < sysdate - 183 then
    flag := 1;
       GOTO end_check;
    else flag := 0;
 end if;
<<end_check>>
NULL;
end loop;
return flag;
end maintenance_fee_issue_64;

function maintenance_fee_issue_65(p_user_id IN NUMBER) RETURN  NUMBER as
flag NUMBER;

cursor cmain is
select max(issue64)issue64,max(issue65)issue65 from (
select iss.time_created as issue64,null as issue65 from issues iss where iss.user_id = p_user_id and iss.subject_id = 64
union all
select null,iss.time_created from issues iss where iss.user_id = p_user_id and iss.subject_id = 65);


begin
for tmp in cmain loop
flag := null;

 if tmp.issue64 > nvl(tmp.issue65, tmp.issue64-1) and  (tmp.issue65 < sysdate - 183 or tmp.issue65 is null) and tmp.issue64 between sysdate - 183 and sysdate - 21 then
    flag := 1;
    else flag := 0;
 end if;

end loop;
return flag;
end maintenance_fee_issue_65;

FUNCTION IS_BLACK_LIST_BIN(p_bin_id number)
  return number is
  USA_COUNTRY_ID CONSTANT number := 220;
  cursor c_check_country is
     select 'x' 
     from bins b
     where 
          b.country_id = USA_COUNTRY_ID
          and b.from_bin = p_bin_id;

   res number:= 0;
   v_ch varchar2(1);
  BEGIN
    open c_check_country;
    fetch c_check_country  into v_ch;
    if c_check_country%found then
      res:= 1;
    end if;    
    close c_check_country;
    return res;
  END;


End TRANSACTIONS_MANAGER;


/
