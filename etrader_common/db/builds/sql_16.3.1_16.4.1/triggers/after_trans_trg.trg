CREATE OR REPLACE TRIGGER AFTER_TRANS_TRG
BEFORE INSERT OR UPDATE
OF STATUS_ID
ON TRANSACTIONS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE


CURSOR c_first_dep is
select first_deposit_id from
    users where id=:new.user_id;

CURSOR c_bonus_amount is
select bonus_amount from
   bonus_users where id=:new.bonus_user_id;

cursor email_alert_c is
select ua.user_id, ua.alert_email_subject, :new.rate*:new.amount amount, ua.alert_email_recepients, u.user_name, ua.alert_type
from users_alerts ua, users u
where ua.user_id = :new.user_id
  and (select class_type from transaction_types where id= :new.type_id) = ua.alert_class_type
  and :new.status_id like ua.alert_status_id
  and ua.user_id=u.id
  and ua.alert_type in (3,4,5,6,7);

cursor trans_type is
select count(*) cnt  from transaction_types tt   where tt.class_type = 1 and tt.id = :new.type_id;

cursor c_conf is
Select Ua.Alert_Email_Subject, Ua.Alert_Email_Recepients
  , 'user_id: '||id||' phone: '||nvl(U.Mobile_Phone, U.Land_Line_Phone)||' email: '||email||' skin: '||skin_id||' name: '||u.first_name||' '||U.Last_Name||' deposit time: '||to_char(:New.time_created, 'dd-mm-yyyy hh24:mi:ss') text
From Users_Alerts Ua, Users U
Where Ua.User_Id=-1
  And Ua.Alert_Type=10
  And U.Id=:New.User_Id;
  
cursor get_skin_id is
select skin_id  from users   where id = :new.user_id;

user_id  number;
alert_email_subject varchar2(100);
alert_amount number;
alert_email_recepients varchar2(4000);
alert_type varchar2(100);
user_name varchar2(100);

v_first_dep NUMBER;
v_check NUMBER;
v_bonus_amount NUMBER;
V_Cnt Number;
v_skin_id Number;

Msg_Subject Varchar2(100);
Msg_Recepients Varchar2(4000);
msg_text Varchar2(4000);
msg_send NUMBER:=0;

BEGIN

    open c_first_dep;
    fetch c_first_dep into v_first_dep;
    close c_first_dep;

    open c_bonus_amount;
    fetch c_bonus_amount into v_bonus_amount;
    close c_bonus_amount;

    open trans_type;
    fetch trans_type into v_cnt;
    close trans_type;
    
    open get_skin_id;
    fetch get_skin_id into v_skin_id;
    close get_skin_id;

IF INSERTING THEN
  IF (:new.STATUS_ID in (2,7)) THEN

    -- BMS (Bonuses management system), update bonus amount  amount when needed (problematic bonus type example: next invest on us).
    IF (:new.TYPE_ID in (12) and v_bonus_amount = 0)  THEN
        update bonus_users
        set bonus_amount=  (:new.amount), adjusted_amount= (:new.amount)
       where id=:new.bonus_user_id;
     END IF;

    If V_First_Dep Is Null Then
      msg_send := 1;

      update users_regulation
        set approved_regulation_step=2
        where user_id=:new.user_id
        and exists (select *
                   from transaction_types tt
                   where tt.class_type = 1
                     and tt.id = :new.type_id);
    END IF;
    -- check if  total amount is more than 5000 USD/EUR/GBP
/*    IF v_cnt > 0 AND v_skin_id != 1 THEN
     IF REGULATION.DEPOSIT_CHECK_SUM(:NEW.USER_ID) + :new.amount / 100  >= 5000 THEN
        update users_regulation
        set qualified = 1, qualified_time = sysdate
        where user_id=:new.user_id;
        ELSE null;
     END IF;
     END IF;*/
     -- update user's first deposit id id necessary
     update users u
     set u.first_deposit_id = :new.id
     where u.id = :new.user_id
       and u.first_deposit_id is null
       and exists (select *
                   from transaction_types tt
                   where tt.class_type = 1
                     and tt.id = :new.type_id);

  END IF;

  open email_alert_c;
  fetch email_alert_c into user_id, alert_email_subject, alert_amount, alert_email_recepients, user_name, alert_type;
  if email_alert_c%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, alert_type, user_name||': '||alert_email_subject, user_name||': '||alert_email_subject||' amount: '||alert_amount, alert_email_recepients, 0, null, systimestamp);
  end if;
  close email_alert_c;

END IF;

IF UPDATING THEN
        IF (:new.status_id in (2,7,8) and :old.status_id not in (2,7,8)) THEN

    If V_First_Dep Is Null Then
      msg_send := 1;

      update users_regulation
        set approved_regulation_step=2
        where user_id=:new.user_id
        and exists (select *
                   from transaction_types tt
                   where tt.class_type = 1
                     and tt.id = :new.type_id);
    END IF;

    -- check if  total amount is more than 5000 USD/EUR/GBP
/*    IF v_cnt > 0 and v_skin_id != 1 THEN
     IF REGULATION.DEPOSIT_CHECK_SUM(:NEW.USER_ID) + :new.amount / 100  >= 5000 THEN
        update users_regulation
        set qualified = 1, qualified_time = sysdate
        where user_id=:new.user_id;
        ELSE null;
     END IF;

     END IF;*/
     -- update user's first deposit id id necessary
			IF v_cnt > 0 THEN
	             update users u
	             	set u.first_deposit_id = :new.id
	             where 
	             	u.id = :new.user_id
	                and u.first_deposit_id is null;
	                
			END IF;
        ELSIF (:new.status_id not in (2,7,8) and :old.status_id in (2,7,8) and v_cnt > 0) THEN
        	update users u
            	set u.first_deposit_id = null
          	where 
          		u.id = :new.user_id
            	and u.first_deposit_id = :new.id;
        END IF;
END IF;

If Msg_Send > 0 Then
    open c_conf;
    fetch c_conf into Msg_Subject, Msg_Recepients, msg_text;
    If c_conf%Found Then
      insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, 10, Msg_Subject, msg_text, Msg_Recepients, 0, null, systimestamp);
    end if;
    close c_conf;
END IF;

END;
/

-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--LioR SoLoMoN
--25.11.13
--dev 2471 - Bonus Management System - Bonus Cleanup Job
--NOTICE: make sure there is no such id=43,63 in dblive.
