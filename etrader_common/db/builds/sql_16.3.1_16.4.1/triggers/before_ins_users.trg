create or replace trigger BEFORE_INS_USERS
     before insert on USERS 
     REFERENCING OLD AS old NEW AS new
     for each row
          declare
               v_dynamic_param varchar2(2000);             
               netrefer_combination NUMBER;
     begin                
                    
          v_dynamic_param :=  :new.dynamic_param;
          -- for API we are reciving only this format API_XXXXXX_YYYYYY_specialCode_OtherParams
          -- XXXXXX - sub affiliate key
          -- YYYYYY - affiliate key
          -- SpecialCode - offline code on Netrefer
          -- OtherParams - other tracking params
          if v_dynamic_param is not null AND 
               (REGEXP_LIKE(v_dynamic_param, '^API_\d{6}_\d{6}_.*')  OR REGEXP_LIKE(v_dynamic_param, '^BR_\d{6}_\d{6}_.*') )          
          then  
               begin
                    select
                         1 into netrefer_combination 
                     from
                         marketing_combinations mc
                    where
                         mc.id = :new.combination_id
                         and mc.campaign_id = 365;
                         
                    EXCEPTION                  
                      WHEN no_data_found THEN
                            netrefer_combination := 0;                         
                end;                                    
               if netrefer_combination = 1
                    then               
                        -- Convert delimited string to array                                           
                           :new.special_code := regexp_substr(v_dynamic_param,'[^_]+',1,4);
                           :new.affiliate_key := regexp_substr(v_dynamic_param,'[^_]+',1,3);
               end if;                                                         
          end if;  
end;
/
