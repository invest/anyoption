create or replace PROCEDURE "TRANSACTIONS_ISSUES_CYCLE_FILL" AS 
  last_transaction_issues transactions_issues%ROWTYPE;
  cycle_start_time date;
  cycle_last_investment_id number;
  investment_time_created date;
  
  p_cycle_reached number;
  p_last_investment_id number;
  p_time_settled date;
  p_num_inv_reached number;
  
  count_updated_transactions number := 0;
  count_settled_transactions number := 0;
  is_update_transaction number;
  is_handle_transaction number;
  
  inv_delay_hours number;
  num_inv_qualification number;
  to_time_date date;
BEGIN 
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------------------------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_CYCLE_FILL started at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');

  -- Get the hours delay from db - delay that suppued to take only investments that happned before it in order to avoid getting RF investments
  -- and to get real amount of Take Profit investments
  SELECT 
    num_value 
  INTO 
    inv_delay_hours
  FROM 
    db_parameters
  WHERE 
    id = 3;

  -- A Sales deposit will be deemed "qualified" if after the deposit the customer inserts X investments (num_inv_qualification).
  SELECT 
    enum.value 
  INTO 
    num_inv_qualification
  FROM 
    enumerators enum
  WHERE 
    enum.enumerator LIKE 'num_inv'
    AND enum.code LIKE 'qualified_after_num_inv';

  -- Find all transactions issues records which hasn't been settled.
  FOR transaction1 IN 
    (SELECT 
        t.id, 
        t.time_created, 
        t.amount, 
        t.user_id, 
        ti.cycle_reached, 
        ti.time_settled, 
        ti.last_investment_id, 
        ti.last_investment_remainder, 
        ti.num_inv 
     FROM transactions_issues ti, 
        transactions t 
     WHERE 
        t.id = ti.transaction_id 
        AND ti.time_settled IS NULL 
        AND t.time_created <= sysdate - inv_delay_hours / 24 
        AND EXISTS 
         (SELECT 
            i.id 
          FROM 
            investments i 
          WHERE 
            i.user_id = t.user_id 
            AND i.time_created >= t.time_created 
            AND i.time_created < sysdate - inv_delay_hours / 24 
            AND i.is_canceled = 0) 
--        and t.user_id  =1011929
     ORDER BY -- need to order by user and then by investment id, to take last transaction that started to count investments
       t.user_id, 
       ti.last_investment_id DESC nulls LAST, 
       t.time_created) 
  LOOP 
    DBMS_OUTPUT.PUT_LINE('-----');
    DBMS_OUTPUT.PUT_LINE('CHECK User id: ' ||transaction1.user_id||', Transaction id: '||transaction1.id);
    -- Initialize
    cycle_start_time := NULL;
    cycle_last_investment_id := 0;
    investment_time_created := NULL;
    p_cycle_reached := 0;
    p_time_settled := NULL;
    p_last_investment_id := 0;
    p_num_inv_reached := 0;
    is_handle_transaction := 0;
    is_update_transaction := 0;
    BEGIN 
      -- If last_investment_id of current transaction_issues is greater than 0 it means we'va already started handeling this transaction
      -- Else we'll look for the last transaction in this table for the current user.
      IF (transaction1.last_investment_id > 0) THEN -- Set parameters from current transaction.
        cycle_last_investment_id := transaction1.last_investment_id;
        p_cycle_reached := transaction1.cycle_reached;
        p_num_inv_reached := transaction1.num_inv;
        is_handle_transaction := 1;
      ELSE BEGIN 
        -- Find the last user's transaction_issues entry that was qualified for that user. 
        SELECT 
          * 
        INTO 
          last_transaction_issues 
        FROM 
          (SELECT 
              ti.* 
           FROM 
              transactions_issues ti, 
              transactions t 
           WHERE 
              ti.transaction_id = t.id 
              AND t.user_id = transaction1.user_id 
              AND ti.last_investment_id > 0 -- and t.time_created <= transaction1.time_created
              AND t.id != transaction1.id -- order by desc investment id, so we'll take the last investment that was we used for qualification
           ORDER BY 
              ti.last_investment_id DESC)
        WHERE rownum = 1;

        DBMS_OUTPUT.PUT_LINE('The last transaction_issues entry that was qualified for that user, Transatction id: ' ||last_transaction_issues.transaction_id);
        -- If prior transaction was settled, get parameters from it.
        IF (last_transaction_issues.time_settled IS NOT NULL) THEN 
          cycle_last_investment_id := last_transaction_issues.last_investment_id;
          is_handle_transaction := 1;
        ELSE -- If prior transaction wasn't settled, stop handling this one.
          is_handle_transaction := 0;
          DBMS_OUTPUT.PUT_LINE('Found transatction id: ' ||last_transaction_issues.transaction_id|| ' that was not settled, stop handling this transaction: '|| transaction1.id);
        END IF;

        -- If we couldn't find prior transaction set parameters to default.
        EXCEPTION WHEN no_data_found THEN 
          p_cycle_reached := 0;
          cycle_last_investment_id := 0;
          p_num_inv_reached := 0;
          is_handle_transaction := 1;
        END;
      END IF; -- IF (transaction1.last_investment_id > 0) THEN

      -- If last_investment_id greater than 0, set cycle_start_time as investment time,
      -- Else set cycle_start_time as transaction time.
      IF (cycle_last_investment_id > 0) THEN
        SELECT 
          i.time_created 
        INTO 
          cycle_start_time
        FROM 
          investments i
        WHERE 
          i.id = cycle_last_investment_id;

        -- If the last investment was created before transaction set cycle_start_time as transaction time.
        IF (cycle_start_time < transaction1.time_created) THEN 
          cycle_start_time := transaction1.time_created;
        END IF;
      ELSE 
        cycle_start_time := transaction1.time_created;
        cycle_last_investment_id := 0;
      END IF; -- IF (cycle_last_investment_id > 0) THEN

      EXCEPTION WHEN no_data_found THEN 
        cycle_start_time := NULL;
        is_handle_transaction := 0;
        DBMS_OUTPUT.PUT_LINE('ERROR - problem with transaction: ' || last_transaction_issues.transaction_id || ' on processing transaction: ' || transaction1.id);
      END;

      IF (is_handle_transaction = 1) THEN 
        -- Find all investments after the cycle_start_time.
        SELECT
          sysdate - inv_delay_hours / 24
        INTO
          to_time_date
        FROM
          dual;
        DBMS_OUTPUT.PUT_LINE('Find all investments between ' ||to_char(cycle_start_time, 'dd-mm-yyyy hh24:mi:ss')|| ' to '|| to_char(to_time_date, 'dd-mm-yyyy hh24:mi:ss')|| ' (without cancel investments and without investment id: ' ||cycle_last_investment_id|| ').');
        FOR investment IN
          (SELECT /*+ INDEX(i IDX_OPT_INV)*/ i.*
           FROM 
              investments i
           WHERE 
              i.time_created >= cycle_start_time
              AND i.time_created < to_time_date
              AND i.is_canceled = 0
              AND i.user_id = transaction1.user_id
              AND i.id != cycle_last_investment_id
           ORDER BY time_created) 
        LOOP 
          IF (is_update_transaction = 0) THEN 
            is_update_transaction := 1;
          END IF;
          p_last_investment_id := investment.id;
          p_cycle_reached := p_cycle_reached + investment.amount;
          investment_time_created := investment.time_created;
          p_num_inv_reached := p_num_inv_reached + 1;
          EXIT WHEN (p_num_inv_reached >= num_inv_qualification) ;
        END LOOP;

        -- If we had any relevant investments, update transaction_issues table.
        IF (is_update_transaction = 1) THEN -- If transaction was settles set the relevant parameters.
          IF (p_num_inv_reached >= num_inv_qualification) THEN 
            p_time_settled := investment_time_created;
            count_settled_transactions := count_settled_transactions + 1;
          END IF; -- IF (p_num_inv_reached >= num_inv_qualification) THEN
          
          DBMS_OUTPUT.PUT_LINE('UPDATE, Transatction id: ' || transaction1.id|| ', cycle_reached: ' ||p_cycle_reached|| ', time_settled: ' ||p_time_settled|| ', last_investment_id: ' ||p_last_investment_id|| ', number of investments: ' ||p_num_inv_reached);
          UPDATE 
            transactions_issues ti
          SET 
            ti.cycle_reached = p_cycle_reached,
            ti.time_settled = p_time_settled,
            ti.last_investment_id = p_last_investment_id,
            ti.num_inv = p_num_inv_reached
          WHERE 
            ti.transaction_id = transaction1.id;

          count_updated_transactions := count_updated_transactions + 1;
          COMMIT;
        ELSE
          DBMS_OUTPUT.PUT_LINE('No relevant investments, no need to update transaction_issues table.');
        END IF; -- IF (is_update_transaction = 1) THEN
      END IF; -- IF  (is_handle_transaction = 1) THEN
    END LOOP;

    UPDATE 
      etrader.db_parameters
    SET 
      date_value = sysdate
    WHERE 
      id = 4; -- last cycle fill run

    DBMS_OUTPUT.PUT_LINE('-----');
    DBMS_OUTPUT.PUT_LINE('Number of updated transactions : ' || count_updated_transactions);
    DBMS_OUTPUT.PUT_LINE('Number of settled transactions : ' || count_settled_transactions);
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
    DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_CYCLE_FILL ended at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
    NULL;
END TRANSACTIONS_ISSUES_CYCLE_FILL;