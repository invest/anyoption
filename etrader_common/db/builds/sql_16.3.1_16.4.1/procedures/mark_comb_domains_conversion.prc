create or replace PROCEDURE MARK_COMB_DOMAINS_CONVERSION AS 
     MARKETING_DOMAINS_ID NUMBER;
     counter NUMBER :=0;
BEGIN
DBMS_OUTPUT.PUT_LINE('START AT ' || sysdate);
FOR v_mar_comb IN (select
                     mc.*,
                     mlp.LANDING_PAGE_TYPE
                  from 
                    MARKETING_COMBINATIONS mc,
                    MARKETING_LANDING_PAGES mlp
                  WHERE                    
                    mlp.id = mc.LANDING_PAGE_ID
--                    AND mc.LANDING_PAGE_ID = 250  -- run on specific landing page
                    and mc.id in (21972) -- run on specific combid
                    ) LOOP
   
   MARKETING_DOMAINS_ID := 0;   
   
-- search for relevant domain   
SELECT  
   domains_rules.id  INTO MARKETING_DOMAINS_ID
 FROM  
   (SELECT  
     mdr.id  
   FROM  
     MARKETING_DOMAINS_RULES mdr  
   WHERE  
     (mdr.SKIN_ID is null or mdr.SKIN_ID = v_mar_comb.skin_id)  
     AND (mdr.MARKETING_URL_SOURCE_TYPE_ID is null or mdr.MARKETING_URL_SOURCE_TYPE_ID = NVL(v_mar_comb.MARKETING_URL_SOURCE_TYPE_ID,1))  
     AND (mdr.MARKETING_LANDING_PAGE_TYPE_ID is null or mdr.MARKETING_LANDING_PAGE_TYPE_ID = (v_mar_comb.LANDING_PAGE_TYPE))  
     AND (mdr.STATIC_LANDING_PAGE_PATH_ID is null or mdr.STATIC_LANDING_PAGE_PATH_ID in (1, 2))  
   ORDER BY  
     mdr.MARKETING_URL_SOURCE_TYPE_ID,  
     mdr.MARKETING_LANDING_PAGE_TYPE_ID,  
     mdr.STATIC_LANDING_PAGE_PATH_ID,  
     mdr.SKIN_ID) domains_rules  
   WHERE  
     ROWNUM = 1;

-- insert --
--insert into  MARKETING_COMBINATION_DOMAINS (id, COMBINATION_ID, DOMAIN_ID) 
--values (SEQ_MARKETING_COMB_DOMAINS.nextval, v_mar_comb.id, MARKETING_DOMAINS_ID);
--  COMMIT;

-- update
--UPDATE
--  MARKETING_COMBINATION_DOMAINS mcd
--SET
--  mcd.DOMAIN_ID = MARKETING_DOMAINS_ID
--WHERE
--  mcd.COMBINATION_ID = v_mar_comb.id;
  
counter := counter +1;
                    
--DBMS_OUTPUT.PUT_LINE('comb_id=' || v_mar_comb.id || ' skin_id=' || v_mar_comb.skin_id ||
--                     ' url_source_id=' || v_mar_comb.MARKETING_URL_SOURCE_TYPE_ID || ' landing_type_id=' || v_mar_comb.LANDING_PAGE_TYPE ||
--                     ' MARKETING_DOMAINS_ID=' || MARKETING_DOMAINS_ID
--                     );                    

END LOOP;
 DBMS_OUTPUT.PUT_LINE('counter: ' || counter);
 DBMS_OUTPUT.PUT_LINE('END ' || sysdate);
END MARK_COMB_DOMAINS_CONVERSION;