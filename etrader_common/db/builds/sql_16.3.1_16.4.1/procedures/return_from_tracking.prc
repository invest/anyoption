CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:58 (QP5 v5.149.1003.31008) */
PROCEDURE RETURN_FROM_TRACKING
AS
   v_peh                          population_entries_hist%ROWTYPE;
   tracking_entry_time            DATE;
   entries_return_from_tracking   NUMBER := 0;

   NUMBER_OF_DAYS                 NUMBER := 7;
   POP_HIS_STATUS_TRACKING        NUMBER := 200;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('RETURN_FROM_TRACKING started at: ' || SYSDATE);

   FOR v_user
      IN (SELECT u.id AS user_id,
                 pu.curr_population_entry_id AS population_entry_id,
                 pu.id AS population_user_id,
                 pu.curr_assigned_writer_id AS writer_id
            FROM users u,
                 population_users pu,
                 population_entry_types pet,
                 population_entries pe
           WHERE     u.id = pu.user_id
                 AND u.first_deposit_id IS NOT NULL
                 AND pu.entry_type_id = pet.id
                 AND pe.id = pu.curr_population_entry_id
                 AND pet.id = 2
                 AND pu.delay_id IS NULL
                 AND u.is_active = 1
                 AND u.class_id <> 0)
   LOOP
      SELECT tracking_entry_time
        INTO tracking_entry_time
        FROM (  SELECT peh.time_created AS tracking_entry_time
                  FROM population_entries_hist peh,
                       population_entries pe,
                       population_users pu
                 WHERE     peh.population_entry_id = pe.id
                       AND pu.id = pe.population_users_id
                       AND pu.user_id = v_user.user_id
                       AND peh.status_id IN (200, 201, 215)
              ORDER BY peh.time_created DESC)
       WHERE ROWNUM = 1;

      IF (tracking_entry_time < SYSDATE - NUMBER_OF_DAYS)
      THEN
         --init
         v_peh := NULL;

         -- Insert a new population_entry_history
         SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

         v_peh.population_entry_id := v_user.population_entry_id;
         v_peh.status_id := 202;               -- remove from tracking by time
         v_peh.writer_id := 0;
         v_peh.assigned_writer_id := v_user.writer_id;
         v_peh.time_created := SYSDATE;
         v_peh.issue_action_id := NULL;

         INSERT INTO population_entries_hist
              VALUES v_peh;

         -- Update user entry in population_users table
         UPDATE population_users pu
            SET pu.entry_type_id = 1          -- population.enrty.type.general
          WHERE pu.id = v_user.population_user_id;

         entries_return_from_tracking := entries_return_from_tracking + 1;
      END IF;
   END LOOP;

   DBMS_OUTPUT.
    PUT_LINE (
      'entries return from tracking  = ' || entries_return_from_tracking);
   DBMS_OUTPUT.PUT_LINE ('RETURN_FROM_TRACKING finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
END RETURN_FROM_TRACKING;
/
