CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:59 (QP5 v5.149.1003.31008) */
PROCEDURE "STRTOK" (tok IN OUT VARCHAR, s IN OUT VARCHAR, ct IN VARCHAR)
AS
   i             PLS_INTEGER;
   p             PLS_INTEGER;
   len           PLS_INTEGER;
   token_start   PLS_INTEGER;
   intoken       BOOLEAN := FALSE;
BEGIN
   IF (s IS NOT NULL)
   THEN
      len := LENGTH (s);
      i := 1;

      WHILE (i <= len)
      LOOP
         p := INSTR (ct, SUBSTR (s, i, 1));

         IF ( (i = len) OR (p > 0))
         THEN
            IF (intoken)
            THEN
               IF (p > 0)
               THEN
                  tok := SUBSTR (s, token_start, i - token_start);
                  s := SUBSTR (s, i + 1);
               ELSE
                  tok := SUBSTR (s, token_start, i - token_start + 1);
                  s := '';
               END IF;

               EXIT WHEN TRUE;
            END IF;
         ELSIF (NOT intoken)
         THEN
            intoken := TRUE;
            token_start := i;
         END IF;

         i := i + 1;
      END LOOP;
   END IF;
END;
/
