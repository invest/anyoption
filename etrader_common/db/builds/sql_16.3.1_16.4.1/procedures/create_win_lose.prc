CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:54 (QP5 v5.149.1003.31008) */
PROCEDURE "CREATE_WIN_LOSE"
IS
   c_win_lose   NUMBER;
   c_count      NUMBER;
BEGIN
   c_count := 0;

   FOR item IN (SELECT id
                  FROM users
                 WHERE skin_id = 1)
   LOOP
      SELECT SUM (i.win) - SUM (i.lose) + SUM (NVL (t.amount, 0))
        INTO c_win_lose
        FROM    investments i
             LEFT JOIN
                transactions t
             ON t.bonus_user_id = i.bonus_user_id
                AND i.bonus_odds_change_type_id > 1
       WHERE     i.user_id = item.id
             AND i.time_settled_year = 2010
             AND i.is_settled = 1
             AND i.is_canceled = 0
             AND NOT (i.bonus_odds_change_type_id = 1 AND i.lose > 0);

      UPDATE users
         SET win_lose = NVL (c_win_lose, 0)
       WHERE id = item.id;

      c_count := c_count + 1;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN                                                  -- handles all errors
      ROLLBACK;
END;
/
