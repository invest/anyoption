CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:02 (QP5 v5.149.1003.31008) */
PROCEDURE "UPDATE_MARKETING_GOOGLE_INIT" (start_date   IN DATE,
                                          days_num     IN NUMBER)
AS
   -- The day of the updated data
   i   NUMBER;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');

   FOR i IN 0 .. days_num - 1
   LOOP
      update_marketing_google_day (start_date + i);
   END LOOP;
END UPDATE_MARKETING_GOOGLE_INIT;
/
