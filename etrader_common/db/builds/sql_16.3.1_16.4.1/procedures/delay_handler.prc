CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:54 (QP5 v5.149.1003.31008) */
PROCEDURE "DELAY_HANDLER"
AS
   v_peh                      population_entries_hist%ROWTYPE;
   delayTime                  NUMBER;
   isCancelDelayBySwitchPop   NUMBER;
BEGIN
   -- Find all users that are in delay entry_type_id (6)
   FOR v_pop_user
      IN (SELECT pu.id,
                 pu.delay_id,
                 pu.curr_population_entry_id,
                 pt.is_delay_enable,
                 pt.id curr_pop_type_id,
                 pe.qualification_time curr_qualification_time,
                 p.dept_id curr_pop_dept,
                 d_ia.action_time delay_start_time,
                 d_pe.id delay_pop_entry_id,
                 d_pdt.delay_days_same_pop,
                 d_pdt.delay_days_other_pop,
                 d_p.population_type_id delay_pop_type_id,
                 d_pd.delay_type_id,
                 d_pd.delay_count,
                 (CASE
                     WHEN pe.id IS NOT NULL
                          AND pe.qualification_time > d_pe.qualification_time
                     THEN
                        1
                     ELSE
                        0
                  END)
                    is_new_pop_entry,
                 (CASE
                     WHEN pe.id IS NOT NULL
                          AND pe.qualification_time > d_ia.action_time
                     THEN
                        1
                     ELSE
                        0
                  END)
                    is_qualified_after_delay
            FROM                      population_users pu
                                   LEFT JOIN
                                      users u
                                   ON pu.user_id = u.id
                                LEFT JOIN
                                   contacts c
                                ON pu.contact_id = c.id
                             LEFT JOIN
                                population_entries pe
                             ON pu.curr_population_entry_id = pe.id
                          LEFT JOIN
                             populations p
                          ON pe.population_id = p.id
                       LEFT JOIN
                          population_types pt
                       ON p.population_type_id = pt.id
                    LEFT JOIN
                       population_delays d_pd
                    ON pu.delay_id = d_pd.id
                 LEFT JOIN
                    population_delay_types d_pdt
                 ON d_pd.delay_type_id = d_pdt.id,
                 issue_actions d_ia,
                 issues d_i,
                 population_entries d_pe,
                 populations d_p
           WHERE     pu.entry_type_id = 6                             -- Delay
                 AND pu.lock_history_id IS NULL
                 AND pu.entry_type_action_id = d_ia.id
                 AND d_ia.issue_id = d_i.id
                 AND d_i.population_entry_id = d_pe.id
                 AND d_pe.population_id = d_p.id)
   LOOP
      -- Init params
      v_peh := NULL;
      delayTime := 0;
      isCancelDelayBySwitchPop := 0;

      -- First, If the delay was activated from DAA - remove that user from it's population
      IF (v_pop_user.curr_pop_dept = 2
          AND v_pop_user.curr_population_entry_id =
                 v_pop_user.delay_pop_entry_id
          AND v_pop_user.is_qualified_after_delay = 0)
      THEN
         -- Insert a new population_entry_history
         SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

         v_peh.status_id := 602;       -- remove from population in delay type
         v_peh.population_entry_id := v_pop_user.curr_population_entry_id;
         v_peh.writer_id := 0;
         v_peh.assigned_writer_id := NULL;
         v_peh.time_created := SYSDATE;
         v_peh.issue_action_id := NULL;

         INSERT INTO population_entries_hist
              VALUES v_peh;

         -- Update user entry in population_users table
         UPDATE population_users pu
            SET pu.curr_population_entry_id = NULL,
                pu.curr_assigned_writer_id = NULL
          WHERE pu.id = v_pop_user.id;

         -- Init v_peh
         v_peh := NULL;
      END IF;

      -- Check Delay
      IF (v_pop_user.delay_id IS NULL)
      THEN
         v_peh.status_id := 653;                  -- remove from delay by call
      ELSE
         -- Check If population has changed since the delay.
         IF (v_pop_user.delay_pop_type_id != v_pop_user.curr_pop_type_id
             AND v_pop_user.is_new_pop_entry = 1)
         THEN
            IF (v_pop_user.is_delay_enable = 0)
            THEN
               isCancelDelayBySwitchPop := 1;
               v_peh.status_id := 650;            -- remove from delay by  pop
            ELSE
               -- If user'pop changed to a different one with a qualification time after delay, check delay with delay_days_other_pop param
               IF (v_pop_user.delay_type_id = 1)
               THEN
                  delayTime := v_pop_user.delay_days_other_pop;
               ELSIF (v_pop_user.delay_type_id = 2)
               THEN
                  delayTime := v_pop_user.delay_days_other_pop;
               END IF;

               v_peh.status_id := 651; -- remove from delay by time switch pop
            END IF;
         ELSE
            -- If it's the same population type, but a new population entry and it's an delay disable type, cancel delay
            IF (v_pop_user.is_qualified_after_delay = 1
                AND v_pop_user.is_delay_enable = 0)
            THEN
               isCancelDelayBySwitchPop := 1;
               v_peh.status_id := 650;             -- remove from delay by pop
            ELSE
               -- If user is in the same pop, or in no pop or in a different one with a qualification time before delay (old one),
               -- check delay with delay_days_same_pop param
               IF (v_pop_user.delay_type_id = 1)
               THEN
                  delayTime :=
                     v_pop_user.delay_days_same_pop * v_pop_user.delay_count;
               ELSIF (v_pop_user.delay_type_id = 2)
               THEN
                  delayTime := v_pop_user.delay_days_same_pop;
               END IF;

               v_peh.status_id := 652;   -- remove from delay by time same pop
            END IF;
         END IF;
      END IF;

      IF (   v_pop_user.delay_id IS NULL
          OR SYSDATE > v_pop_user.delay_start_time + delayTime
          OR isCancelDelayBySwitchPop = 1)
      THEN
         -- Insert a new population_entry_history
         SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

         v_peh.population_entry_id := v_pop_user.delay_pop_entry_id;
         v_peh.writer_id := 0;
         v_peh.assigned_writer_id := NULL;
         v_peh.time_created := SYSDATE;
         v_peh.issue_action_id := NULL;

         INSERT INTO population_entries_hist
              VALUES v_peh;

         -- Update user entry in population_users table
         UPDATE population_users pu
            SET pu.entry_type_id = 1,         -- population.enrty.type.general
                pu.entry_type_action_id = NULL,
                pu.curr_assigned_writer_id = NULL
          WHERE pu.id = v_pop_user.id;
      END IF;

      COMMIT;
   END LOOP;

   NULL;
END DELAY_HANDLER;
/
