CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:01 (QP5 v5.149.1003.31008) */
PROCEDURE UPDATE_BUSSINESS_TRAN_BINS
AS
   counter                         NUMBER := 0;
   last_bin                        NUMBER := -1;
   last_time_last_failed_reroute   DATE := NULL;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.
    PUT_LINE ('UPDATE_BUSSINESS_TRANSACTIONS_BINS started at: ' || SYSDATE);

   FOR v_data
      IN (  SELECT tb.from_bin,
                   s.business_case_id,
                   MAX (t.time_created) AS time_created,
                   MAX (tb.time_last_failed_reroute)
                      AS time_last_failed_reroute
              FROM transactions_bins tb,
                   credit_cards cc,
                   transactions t,
                   users u,
                   skins s
             WHERE     tb.from_bin = cc.bin
                   AND cc.id = t.credit_card_id
                   AND t.user_id = u.id
                   AND u.skin_id = s.id
                   AND t.status_id IN (2, 7, 8)
                   AND t.type_id = 1
          GROUP BY tb.from_bin, s.business_case_id
          ORDER BY tb.from_bin, time_created DESC)
   LOOP
      IF (last_bin != v_data.from_bin)
      THEN
         UPDATE transactions_bins tran_b
            SET tran_b.business_case_id = v_data.business_case_id
          WHERE tran_b.from_bin = v_data.from_bin;

         IF (v_data.business_case_id = 1)
         THEN
            UPDATE transactions_bins tran_b
               SET tran_b.time_last_failed_reroute = NULL
             WHERE tran_b.from_bin = v_data.from_bin;
         END IF;
      ELSE
         IF (v_data.business_case_id = 1)
         THEN
            last_time_last_failed_reroute := NULL;
         END IF;

         INSERT INTO TRANSACTIONS_BINS (id,
                                        from_bin,
                                        time_last_success,
                                        time_last_failed_reroute,
                                        updated_by_rerouting,
                                        business_case_id)
              VALUES (SEQ_TRANSACTIONS_BINS.NEXTVAL,
                      v_data.from_bin,
                      v_data.time_created,
                      last_time_last_failed_reroute,
                      1,
                      v_data.business_case_id);
      END IF;

      last_bin := v_data.from_bin;
      last_time_last_failed_reroute := v_data.time_last_failed_reroute;
      counter := counter + 1;
   END LOOP;

   FOR v_data_2
      IN (  SELECT tb.from_bin,
                   s.business_case_id,
                   MAX (t.time_created) AS time_created
              FROM transactions_bins tb,
                   credit_cards cc,
                   transactions t,
                   users u,
                   skins s
             WHERE     tb.from_bin = cc.bin
                   AND cc.id = t.credit_card_id
                   AND t.user_id = u.id
                   AND u.skin_id = s.id
                   AND t.status_id NOT IN (2, 7, 8)
                   AND t.type_id = 1
                   AND tb.business_case_id IS NULL
          GROUP BY tb.from_bin, s.business_case_id
          ORDER BY tb.from_bin, time_created)
   LOOP
      UPDATE transactions_bins tran_b
         SET tran_b.business_case_id = v_data_2.business_case_id
       WHERE tran_b.from_bin = v_data_2.from_bin;

      counter := counter + 1;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('counter  = ' || counter);
   DBMS_OUTPUT.
    PUT_LINE ('UPDATE_BUSSINESS_TRANSACTIONS_BINS finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
END UPDATE_BUSSINESS_TRAN_BINS;
/
