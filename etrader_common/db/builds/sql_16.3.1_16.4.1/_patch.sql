
CREATE OR REPLACE TYPE STRING_TABLE AS TABLE OF VARCHAR2(4000 CHAR);
/
CREATE OR REPLACE PUBLIC SYNONYM STRING_TABLE FOR STRING_TABLE;
GRANT EXECUTE ON STRING_TABLE TO ETRADER_WEB_CONNECT;

CREATE OR REPLACE TYPE NUMBER_TABLE AS TABLE OF NUMBER;
/
CREATE OR REPLACE PUBLIC SYNONYM NUMBER_TABLE FOR NUMBER_TABLE;
GRANT EXECUTE ON NUMBER_TABLE TO ETRADER_WEB_CONNECT;

CREATE OR REPLACE TYPE DATE_TABLE AS TABLE OF DATE;
/
CREATE OR REPLACE PUBLIC SYNONYM DATE_TABLE FOR DATE_TABLE;
GRANT EXECUTE ON DATE_TABLE TO ETRADER_WEB_CONNECT;

----------------------------------------------------------------

------------------------------------------------------------------------
-- Kristina AR-1726 BE - Add affiliate site link field to the affiliate creation and edit
-----------------------------------------------------------------------

ALTER TABLE marketing_affilates ADD site_link varchar2(100 char);

------------------------------------------------------------------------
--End AR-1726
-----------------------------------------------------------------------

------------------------------------------------------------------------
--BAC-53 Neteller banking page info update
--Simeon
-----------------------------------------------------------------------
insert into payment_methods_countries
(select seq_p_m_countries.nextval, s.* from
(select id, 58, 1 excluded_ind from countries
where id in (38,94,1,120,184,212,2,5,26,44,46,54,61,87,106,110,116,128,134,143,158,159,168,204,213,216,222,231,141,52,49,66,89,99,100,115,154,188,196,202,235)
union
select id, 58, 0 excluded_ind from countries
where id not in (38,94,1,120,184,212,2,5,26,44,46,54,61,87,106,110,116,128,134,143,158,159,168,204,213,216,222,231,141,52,49,66,89,99,100,115,154,188,196,202,235)) s);
------------------------------------------------------------------------
--end
--BAC-53 Neteller banking page info update
-----------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-2792
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
update turnover_factor set factor = 0.2 where id = 6;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End CORE-2792
-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-- BAC-1642
-- Mariya
-------------------------------------------------------------------------------------------------------------------------------------------
update mailbox_templates set subject = 'anyoption™: Volmacht' where type_id = 21 and skin_id = 23;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End BAC-1642
-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-- BAC-1639
-- Mariya
-------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
vString mailbox_templates.template%type;
BEGIN
vString := '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="Text/html; charset=utf-8" />
</head>
<body>
    <table style="border: 1px solid #e0e0e0; border-collapse: collapse;" align="center"
        cellpadding="0" cellspacing="0" width="532">
        <tr>
            <td align="center" bgcolor="#ffffff">
                <a href="http://www.anyoption.com/jsp/index.jsf" style="font-family: Verdana, Tahoma,  Geneva, sans-serif;
                    color: #093755; font-size: 26px; font-weight: bold; text-align: center" target="_blank">
                    <img src="http://picbase.anyoption.com/images/customer_email_header.png" alt="anyoption"
                        width="532" height="99" border="0" style="display: block" /></a>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" valign="top">
                <table align="center" cellpadding="0" cellspacing="0" width="500">
                    <tr>
                        <td width="100%" align="left" valign="top" bgcolor="#FFFFFF" style="font-family: Verdana, Tahoma,  Geneva, sans-serif; font-size: 12px; color: #555555; text-align: left; line-height: 16px;">                              ' || chr(38) || 'nbsp;<br /><span style="font-size: 14px;"><strong>Beste ${userFirstName},</strong></span><br /><br /> In overeenstemming met reglementaire vereisten van de EU vragen we u ons zo spoedig mogelijk de volgende documenten te sturen:<br /> <br />                             <ol>
                            <li>Een kopie van de creditkaart eindigend op ${transactionPan} die u hebt gebruikt om geld te storten op uw account bij ons op ${date}.<br />' || chr(38) || 'nbsp;  </li>                             <strong>LET OP:</strong> bedek om veiligheidsredenen het getal voorop de kaart, en laat alleen de laatste 4 getallen zichtbaar. Bedek ook het CVV-nummer op de achterkant, maar zorg er wel voor dat de handtekening zichtbaar is.<br />' || chr(38) || 'nbsp;                              <li>Een kopie van uw ID/ paspoort en het ID/paspoort van de eigenaar van de creditcard.<br />' || chr(38) || 'nbsp;  </li>
                            <li>Een kopie van de bijgevoegde volmacht, <a href="" style="color:#555555; font-weight: bold; ">ondertekend door de eigenaar van de creditcard</a>, die u machtigt de genoemde creditcard te gebruiken.<br />' || chr(38) || 'nbsp;  </li>
                            <li>Het telefoonnummer van de eigenaar van de creditcard. Wij zullen contact met hem/haar opnemen voor verdere toestemming en bevestiging. Let op: voor het proces is het mogelijk noodzakelijk dat de eigenaar van de creditcard ons meer documentatie  verschaft.</li>
                      </ol>                             U kunt de documenten en de gegevens via e-mail naar ons sturen: <a href="mailto:documents@anyoption.com" style="color:#0284d6;"> documents@anyoption.com</a> of  deze doorfaxen naar: <strong>${supportFax}.</strong><br /><br /> De klantenservice van anyoption kan al uw eventuele vragen beantwoorden. Mail ons, maak gebruik van onze live chatservice of bel: <strong>${supportPhone}.' || chr(38) || 'nbsp;</strong> <br />' || chr(38) || 'nbsp;<br />                           </td>
                    </tr>
                    <tr>
                    	<td width="100%" align="left" valign="top" bgcolor="#FFFFFF" style="font-family: Verdana, Tahoma,  Geneva, sans-serif; font-size: 12px; color: #555555; text-align: left; line-height: 16px;">                         	<strong>Met vriendelijke groet,<br /> anyoption' || chr(38) || 'trade; </strong><br /> ' || chr(38) || 'nbsp;<br />' || chr(38) || 'nbsp;<br />                         </td>
                    </tr>
                </table>
            </td>
        </tr>        
        <tr>
            <td style="text-align: center; color: #898989; font-family: Verdana,Arial,Helvetica,sans-serif;
                font-weight: normal; font-size: 10px;" bgcolor="#e5e5e5" height="25" valign="middle">                 anyoption 2008 ' || chr(38) || 'copy; | <a href="http://www.anyoption.com/jsp/index.jsf" target="_blank"
 style="color: #898989;">www.anyoption.com</a> | <a style="color: #898989;" href="mailto:support@anyoption.com"> support@anyoption.com</a>             </td>
        </tr>
    </table>
</body>
</html>';
update mailbox_templates set template = vString where type_id = 21 and skin_id = 23;

vString := '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <table style="border: 1px solid #e0e0e0; border-collapse: collapse;" align="center"
        cellpadding="0" cellspacing="0" width="532">
        <tr>
            <td align="center" bgcolor="#ffffff">
                <a href="http://www.anyoption.com/jsp/index.jsf" style="font-family: Verdana, Tahoma,  Geneva, sans-serif;
                    color: #093755; font-size: 26px; font-weight: bold; text-align: center" target="_blank">
                    <img src="http://picbase.anyoption.com/images/customer_email_header.png" alt="anyoptions"
                        width="532" height="99" border="0" style="display: block" /></a>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" valign="top">
                <table align="center" cellpadding="0" cellspacing="0" width="500">
                    <tr>
                        <td width="100%" align="left" valign="top" bgcolor="#FFFFFF" style="font-family: Verdana, Tahoma,  Geneva, sans-serif; font-size: 12px; color: #555555; text-align: left; line-height: 16px;">                              ' || chr(38) || 'nbsp;<br /><span style="font-size: 14px;"><strong>Kära ${userFirstName},</strong></span><br /><br /> I enlighet med EU-regelkraven ber vi dig skicka följande dokument så snart som möjligt:<br /> <br />                             <ol>
                            <li>En kopia på kreditkortet som slutar med ${transactionPan} som du har använt för att sätta in medel på ditt konto hos oss ${date}.<br />' || chr(38) || 'nbsp;</li>                             <strong>OBS:</strong> Av säkerhetsskäl ber vi dig att täcka över siffrorna på framsidan av kortet så att endast de 4 sista siffrorna syns. Täck även över CVV-numret på baksidan och se till att namnteckningen är synlig.<br />' || chr(38) || 'nbsp;                             <li>En kopia på ditt ID-kort/pass och kreditkortsinnehavarens ID-kort/pass.<br />' || chr(38) || 'nbsp;</li>
                            <li>En kopia på den bifogade fullmakten, <a href="" style="color:#555555; font-weight: bold; ">undertecknad av kreditkortsinnehavaren</a>, som godkänner att du använder nämnda kreditkort.<br />' || chr(38) || 'nbsp;</li>
                            <li>Kreditkortsinnehavarens telefonnummer. Vi kommer att kontakta honom/henne för ytterligare bekräftelse och konfirmering. Observera att processen kan kräva att ytterligare dokumentation tillhandahålles av kreditkortsinnehavaren.</li>
                            </ol>                             Du kan skicka dokumenten och uppgifterna via e-post: <a href="mailto:documents@anyoption.com" style="color:#0284d6;"> documents@anyoption.com</a> eller genom att faxa dem till: <strong>${supportFax}.</strong><br /><br /> Hos anyoptions kundtjänst kan vi besvara ytterligare frågor du har. Skicka e-post till oss, använd vår livechatt eller ring: <strong>${supportPhone}.' || chr(38) || 'nbsp;</strong> <br />' || chr(38) || 'nbsp;<br />                         </td>
                    </tr>
                    <tr>
                    	<td width="100%" align="left" valign="top" bgcolor="#FFFFFF" style="font-family: Verdana, Tahoma,  Geneva, sans-serif; font-size: 12px; color: #555555; text-align: left; line-height: 16px;">                         	<strong>Bästa hälsningar,<br /> anyoption' || chr(38) || 'trade; </strong><br /> ' || chr(38) || 'nbsp;<br />' || chr(38) || 'nbsp;<br />                         </td>
                    </tr>
                </table>
            </td>
        </tr>        
        <tr>
            <td style="text-align: center; color: #898989; font-family: Verdana,Arial,Helvetica,sans-serif;
                font-weight: normal; font-size: 10px;" bgcolor="#e5e5e5" height="25" valign="middle">                 anyoption 2008 ' || chr(38) || 'copy; | <a href="http://www.anyoption.com/jsp/index.jsf" target="_blank"
 style="color: #898989;">www.anyoption.com</a> | <a style="color: #898989;" href="mailto:support@anyoption.com"> support@anyoption.com</a>             </td>
        </tr>
    </table>
</body>
</html>';
Insert into MAILBOX_TEMPLATES
   (ID, NAME, SUBJECT, TYPE_ID, TIME_CREATED, WRITER_ID, SENDER_ID, TEMPLATE, IS_HIGH_PRIORITY, SKIN_ID, LANGUAGE_ID, POPUP_TYPE_ID)
Values
   (SEQ_MAILBOX_TEMPLATES.nextval, 'POA', 'anyoption™: Fullmakt', 21, SYSDATE, 1090, 2, vString, 0, 24, 13, 1);
end;
/

commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End BAC-1639
-------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- Eyal G - AR-1887 - [DB] - Remarketing - Attribute user FTD to retargeting campaign combid
----------------------------------------------------------------------------------

@triggers/after_trans_trg.trg

----------------------------------------------------------------
-- [AR-1887] - [DB] - Remarketing - Attribute user FTD to retargeting campaign combid
-- Eyal O
----------------------------------------------------------------

-- MARKETING_ATTRIBUTION_TYPES
CREATE TABLE MARKETING_ATTRIBUTION_TYPES 
(
  ID NUMBER NOT NULL 
, NAME VARCHAR2(100) 
, CONSTRAINT MARKETING_ATTRIBUTION_TYPE_PK PRIMARY KEY  (ID) 
);

COMMENT ON COLUMN MARKETING_ATTRIBUTION_TYPES.ID IS 'Type''s identify';
COMMENT ON COLUMN MARKETING_ATTRIBUTION_TYPES.NAME IS 'Type''s name';

CREATE public synonym MARKETING_ATTRIBUTION_TYPES for MARKETING_ATTRIBUTION_TYPES;
GRANT select, update, insert on MARKETING_ATTRIBUTION_TYPES to ETRADER_WEB_CONNECT;

INSERT INTO MARKETING_ATTRIBUTION_TYPES(ID, NAME) VALUES(1, 'Register');
INSERT INTO MARKETING_ATTRIBUTION_TYPES(ID, NAME) VALUES(2, 'FTD (First Time Deposit)');
INSERT INTO MARKETING_ATTRIBUTION_TYPES(ID, NAME) VALUES(3, 'Last deposit');

-- MARKETING_ATTRIBUTION
CREATE TABLE MARKETING_ATTRIBUTION 
(
  ID NUMBER NOT NULL 
, USER_ID NUMBER NOT NULL 
, TYPE_ID NUMBER NOT NULL 
, COMBINATION_ID NUMBER 
, DYNAMIC_PARAM NVARCHAR2(1000) 
, AFF_SUB1 NVARCHAR2(4000) 
, AFF_SUB2 NVARCHAR2(4000) 
, AFF_SUB3 NVARCHAR2(4000) 
, AFFILIATE_KEY NUMBER 
, SPECIAL_CODE NVARCHAR2(15) 
, AFFILIATE_SYSTEM NUMBER 
, AFFILIATE_SYSTEM_USER_TIME DATE 
, TIME_CREATED DATE NOT NULL 
, TIME_UPDATED DATE
, CONSTRAINT MARKETING_ATTRIBUTION_PK PRIMARY KEY (ID)
, CONSTRAINT UK_MARKETING_ATTRIBUTION UNIQUE (user_id, type_id)
);

ALTER TABLE MARKETING_ATTRIBUTION
ADD CONSTRAINT MARKETING_ATTRIBUTION_FK1 FOREIGN KEY (TYPE_ID)
  REFERENCES MARKETING_ATTRIBUTION_TYPES (ID);

ALTER TABLE MARKETING_ATTRIBUTION
ADD CONSTRAINT MARKETING_ATTRIBUTION_FK3 FOREIGN KEY (USER_ID)
  REFERENCES USERS (ID);

COMMENT ON COLUMN MARKETING_ATTRIBUTION.ID IS 'Record''s identify';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.USER_ID IS 'Foreign key to the table USERS';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.TYPE_ID IS 'Foreign key to the table MARKETING_ATTRIBUTION_TYPES ';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.COMBINATION_ID IS 'Foreign key to the table MARKETING_COMBINATIONS';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.AFFILIATE_SYSTEM_USER_TIME IS 'The time which the user marked in the ''affiliate system users''';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.TIME_CREATED IS 'The time which the record created';
COMMENT ON COLUMN MARKETING_ATTRIBUTION.TIME_UPDATED IS 'The time which the record updated';

CREATE public synonym MARKETING_ATTRIBUTION for MARKETING_ATTRIBUTION;
GRANT select, update, insert on MARKETING_ATTRIBUTION to ETRADER_WEB_CONNECT;
CREATE SEQUENCE SEQ_MARKETING_ATTRIBUTION ;
CREATE public synonym SEQ_MARKETING_ATTRIBUTION for SEQ_MARKETING_ATTRIBUTION;

-- USERS
ALTER TABLE USERS 
ADD (MARKETING_ATTRIBUTION_TYPE_ID NUMBER DEFAULT 1 NOT NULL);

ALTER TABLE USERS ADD CONSTRAINT USERS_FK1 FOREIGN KEY
  (MARKETING_ATTRIBUTION_TYPE_ID) REFERENCES MARKETING_ATTRIBUTION_TYPES (ID) ENABLE NOVALIDATE;
ALTER TABLE USERS MODIFY CONSTRAINT USERS_FK1 VALIDATE;

COMMENT ON COLUMN USERS.MARKETING_ATTRIBUTION_TYPE_ID IS 'Foreign key to the table MARKETING_ATTRIBUTION_TYPES';

-- ENUMERATORS
INSERT INTO ENUMERATORS (ID, ENUMERATOR, CODE, VALUE, DESCRIPTION) 
VALUES (SEQ_ENUMERATORS.nextval, 'retargeting', 'ftd_attribution', '45', 'Attribute customer FTD to a different campaign when FTD created X days after register.');

@types/marketing_attribution_obj.tps
@types/user_obj.tps

@packages/utils.pks
@packages/marketing.pks

@triggers/after_uad_marketing.trg
@triggers/after_users_market_trc.trg
@triggers/silverpop_users.trg
@triggers/trg_ins_upd_transaction_status.trg


-- Migration for register
INSERT INTO MARKETING_ATTRIBUTION
(
  ID,
  USER_ID,
  TYPE_ID,
  COMBINATION_ID,
  DYNAMIC_PARAM,
  AFF_SUB1,
  AFF_SUB2,
  AFF_SUB3,
  AFFILIATE_KEY,
  SPECIAL_CODE,
  AFFILIATE_SYSTEM,
  AFFILIATE_SYSTEM_USER_TIME,
  TIME_CREATED,
  TIME_UPDATED
)
SELECT 
  SEQ_MARKETING_ATTRIBUTION.nextval,
  u.id,
  1,
  u.COMBINATION_ID,
  u.DYNAMIC_PARAM,
  uad.AFF_SUB1,
  uad.AFF_SUB2,
  uad.AFF_SUB3,
  u.AFFILIATE_KEY,
  u.SPECIAL_CODE,
  u.AFFILIATE_SYSTEM,
  u.AFFILIATE_SYSTEM_USER_TIME,
  sysdate,
  NULL
FROM
  users u,
  USERS_ACTIVE_DATA uad
where
  u.id = uad.USER_ID
  and u.id not in (select user_id from MARKETING_ATTRIBUTION where type_id = 1);
  
-- Migration for FTD
INSERT INTO MARKETING_ATTRIBUTION
(
  ID,
  USER_ID,
  TYPE_ID,
  COMBINATION_ID,
  DYNAMIC_PARAM,
  AFF_SUB1,
  AFF_SUB2,
  AFF_SUB3,
  AFFILIATE_KEY,
  SPECIAL_CODE,
  AFFILIATE_SYSTEM,
  AFFILIATE_SYSTEM_USER_TIME,
  TIME_CREATED,
  TIME_UPDATED
)
SELECT 
  SEQ_MARKETING_ATTRIBUTION.nextval,
  u.id,
  2,
  u.COMBINATION_ID,
  u.DYNAMIC_PARAM,
  uad.AFF_SUB1,
  uad.AFF_SUB2,
  uad.AFF_SUB3,
  u.AFFILIATE_KEY,
  u.SPECIAL_CODE,
  u.AFFILIATE_SYSTEM,
  u.AFFILIATE_SYSTEM_USER_TIME,
  sysdate,
  NULL
FROM
  users u,
  USERS_ACTIVE_DATA uad
where
  u.id = uad.USER_ID
  and u.FIRST_DEPOSIT_ID is not null
  and u.id not in (select user_id from MARKETING_ATTRIBUTION where type_id = 2);

commit;
  
----------------------------------------------------------------
-- END
-- [AR-1887] - [DB] - Remarketing - Attribute user FTD to retargeting campaign combid
-- Eyal O
----------------------------------------------------------------

----------------------------------------------------------------
--  https://anyoption.atlassian.net/browse/BAC-1923
--  Victor Slavov
----------------------------------------------------------------
  
create table writer_roles
(
   id       number        not null,
   rolename varchar2(255) not null,
   constraint pk_writer_roles primary key (id)
);

comment on table writer_roles is '[WROL] All available writer roles';
create unique index idx_wrol_unique on writer_roles (upper(rolename));

create table writer_screen_permissions
(
   id         number        not null,
   spacename  varchar2(255) not null,
   screenname varchar2(255) not null,
   permission varchar2(255) not null,
   permorder  number        not null,
   constraint pk_writer_screen_permissions primary key (id)
);

comment on table writer_screen_permissions is '[WSCP] All screen permissions';
create unique index idx_wscp_unique on writer_screen_permissions (upper(screenname), upper(permission));

create table writer_role_permissions
(
   role_id       number not null,
   permission_id number not null,
   constraint pk_writer_role_permissions primary key (role_id, permission_id)
);

comment on table writer_role_permissions is '[WRLP] mapping between writer_roles and writer_screen_permissions';
alter table writer_role_permissions add constraint fk_WRLP_WROL foreign key (role_id) references writer_roles(id);
alter table writer_role_permissions add constraint fk_WRLP_WSCP foreign key (permission_id) references writer_screen_permissions(id);

create sequence writer_screen_permissions_seq;
create sequence writer_roles_seq;

alter table writers add (
      role_id      number,
      time_created date
);

comment on table writers is '[WTER] Writers (backend users) in the system';
alter table writers add constraint fk_WTER_wrol foreign key (role_id) references writer_roles(id);

comment on column writers.role_id is 'Current role of the writer';
comment on column writers.time_created is 'Auto-insert when the record is created. For existing users just copy the value from Time_birth_date';

alter table writers modify (time_created default sysdate);
update writers set time_created = time_birth_date where time_created is null;
commit;
alter table writers modify (time_created not null);

alter table writers modify (
      street                null,
      city_id               null,
      --zip_code              null,
      mobile_phone          null,
      street_no             null,
      time_birth_date       null
);

alter table writers rename constraint writers_pk to pk_writers;
alter index writers_pk rename to pk_writers;

insert into departments (id, dept_name) values (0, 'NoDepartment');
insert into departments (id, dept_name) values (9, 'System');
insert into departments (id, dept_name) values (10, 'Board');
insert into departments (id, dept_name) values (11, 'Product');
insert into departments (id, dept_name) values (12, 'Conversion');
insert into departments (id, dept_name) values (13, 'Risk');
insert into departments (id, dept_name) values (14, 'Payments');
insert into departments (id, dept_name) values (15, 'Compliance');
insert into departments (id, dept_name) values (16, 'Legal');
insert into departments (id, dept_name) values (17, 'Back office');
insert into departments (id, dept_name) values (18, 'Affiliate managers');
insert into departments (id, dept_name) values (19, 'Media');
insert into departments (id, dept_name) values (20, 'Internal Marketing');
insert into departments (id, dept_name) values (21, 'PPC');
insert into departments (id, dept_name) values (22, 'SEO');

update writers set dept_id = 9 where id in (1, 200, 20000, 10000);
update writers set dept_id = 0 where dept_id is null;
commit;
alter table writers modify(dept_id not null);
alter table writers add constraint fk_WTER_DEPT foreign key (dept_id) references departments(id);

alter table skins add is_active number(1);
alter table skins modify (is_active default 1);
update skins set is_active=0 where id in (4, 6, 13, 14, 21);
update skins set is_active=1 where is_active is null;
commit;
alter table skins modify (is_active not null);

@packages/pkg_writers.pck

grant execute on pkg_writers to etrader_web_connect;
create or replace public synonym pkg_writers for pkg_writers;

--- rename non-unique writers  
begin
  for i in (select *
            from   (select w.id
                          ,w.user_name
                          ,w.is_active
                          ,row_number() over(partition by upper(w.user_name) order by w.id) rn
                          ,count(*) over(partition by upper(w.user_name)) cnt
                    from   writers w)
            where  cnt > 1)
  loop
    if i.rn > 1
    then
      update writers set user_name = i.user_name || i.rn where id = i.id;
    end if;
  end loop;
end;
/

commit;

alter table writers modify (user_name varchar2(40 char));

create unique index uk_writers_username on writers (upper(user_name));  

-- Remove duplicate "writer_id, skin_id" combinations from writers_skin table. Leave only the most recent one.
-- update ref table writer_rank_distribution
begin
  for i in (select writer_id, skin_id from writers_skin group by writer_id, skin_id having count(*) > 1)
  loop
    update writer_rank_distribution
    set    writer_skin_id =
           (select id
            from   (select id, row_number() over(partition by writer_id, skin_id order by id desc) rn
                    from   writers_skin
                    where  writer_id = i.writer_id
                    and    skin_id = i.skin_id)
            where  rn = 1)
    where  writer_skin_id in (select id
                              from   (select id, row_number() over(partition by writer_id, skin_id order by id desc) rn
                                      from   writers_skin
                                      where  writer_id = i.writer_id
                                      and    skin_id = i.skin_id)
                              where  rn > 1);
  end loop;
end;
/

delete from writers_skin
where  id in
       (select id from (select id, row_number() over(partition by writer_id, skin_id order by id desc) rn from writers_skin) where rn > 1);
commit;

drop index WRITERS_SKIN_WRI_ID_UQ;
drop index IDX$$_420150006;
alter table writers_skin add constraint uk_writers_skin unique (writer_id, skin_id);

-- fix FK constraint names for writers_skin
begin
  for i in (select *
            from   all_constraints ac
            where  ac.owner = 'ETRADER'
            and    ac.table_name = 'WRITERS_SKIN'
            and    ac.constraint_type = 'R')
  loop
    execute immediate 'alter table ' || i.owner || '.' || i.table_name || ' drop constraint ' || i.constraint_name;
  end loop;
end;
/

alter table writers_skin add constraint fk_wrsk_skin foreign key(skin_id) references skins(id);
alter table writers_skin add constraint fk_wrsk_wter foreign key(writer_id) references writers(id);

begin
  pkg_writers.create_permissions('Administrator', 'writers', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'writers', 'addWriter', 20);
  pkg_writers.create_permissions('Administrator', 'writers', 'editWriter', 30);

  pkg_writers.create_permissions('Administrator', 'permissions', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'permissions', 'addRole', 20);
  pkg_writers.create_permissions('Administrator', 'permissions', 'editRole', 30);
  pkg_writers.create_permissions('Content', 'terms', 'view', 10);
end;
/

insert into writer_roles (id, rolename) values (1, 'Viewer');

insert into writer_role_permissions
  (role_id, permission_id)
  select 1, id from writer_screen_permissions where permission = 'view';

commit;  

----------------------------------------------------------------
--  END
--  https://anyoption.atlassian.net/browse/BAC-1923
--  Victor Slavov
----------------------------------------------------------------


----------------------------------------------------------------
--  last level fix
--  Victor Slavov
----------------------------------------------------------------

begin
  pkg_release.ddl_helper('create index idx_oppt_estclose on opportunities (market_id, time_est_closing) compress 1 online'
                        ,i_ignore_exception => -955);
end;
/

begin
  pkg_release.ddl_helper('drop index idx_opp_market_id', i_ignore_exception => -1418);
end;
/

-- view
@views/last_levels.sql

@functions/get_last_closing_level.fnc

begin
  pkg_release.ddl_helper('alter table opportunities add constraint fk_oppt_mrkt foreign key (market_id) references markets (id) enable novalidate'
                        ,i_ignore_exception => -2275);
end;
/
  
----------------------------------------------------------------
--  END
--  last level fix
--  Victor Slavov
----------------------------------------------------------------

  
------------------------------------------------------------------------
-- Kristina AR-1903 BE - Add an "Internal" channel to the campaign creation page
-----------------------------------------------------------------------

insert into marketing_channel (ID, NAME, PRIORITY, DESCRIPTION)
values (5, 'Internal', 5, 'marketing.channel.internal');

------------------------------------------------------------------------
--End AR-1903
-----------------------------------------------------------------------
------------------------------------------------------------------------
-- CORE-3077 [DB/ Server Dynamics] Missing/ incorrect 'turnover_factor' used in bonus wagering
--'0.3' percentage of investment counted as wagering (same as previously defined for 0-100 investments )
-- Pavlin
------------------------------------------------------------------------
insert into turnover_factor values ( 7, 7, 0.3, sysdate, sysdate);
------------------------------------------------------------------------
-- END CORE-3077
-----------------------------------------------------------------------

----------------------------------------------------------------------------------
-- Kristina - AR-1725 BE - Affiliate key to show immediately on the user details when user registers through API
----------------------------------------------------------------------------------

@triggers/before_ins_users.trg

----------------------------------------------------------------
-- END AR-1725
----------------------------------------------------------------



----------------------------------------------------------------------------------
-- Victor - BAC-1747 - Ability to edit the general terms pages from Backend
----------------------------------------------------------------------------------

drop index SKIN_CURRENCIES_IDX1;
drop index SKIN_CURRENCIES_IDX2;

alter table skin_currencies add constraint uk_skin_currencies unique (skin_id, currency_id);

@packages/pkg_terms.pck

create or replace public synonym pkg_terms for pkg_terms;
grant execute on pkg_terms to etrader_web_connect;

----------------------------------------------------------------------------------
-- END BAC-1747 - Ability to edit the general terms pages from Backend
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- Jamal - BAC-1747 17.05.2016	
-- Ability to edit the general terms pages from Backend 
----------------------------------------------------------------------------------

-- Create table
create table TERMS_PARTS
(
  id         NUMBER,
  parts_name VARCHAR2(20),
  order_num  NUMBER,
  type       NUMBER,
   constraint pk_TERMS_PARTS primary key (ID)
);

-- Add comments to the columns 
comment on column TERMS_PARTS.type is '1-AGREEMENT; 2-GENERAL_TERMS; 3-RISK_DISCLOSURS';
-- Grant/Revoke object privileges 
grant select, insert, update on TERMS_PARTS to ETRADER_WEB_CONNECT;
CREATE public synonym TERMS_PARTS for TERMS_PARTS;

-- Create sequence 
create sequence SEQ_TERMS_PARTS;
CREATE public synonym SEQ_TERMS_PARTS for SEQ_TERMS_PARTS;

-- Create table
create table TERMS_FILES
(
  id         NUMBER not null,
  file_name  VARCHAR2(128),
  title      VARCHAR2(40),
  constraint TERMS_FILES_ID_PK primary key (ID)
);
-- Create/Recreate primary, unique and foreign key constraints 

-- Grant/Revoke object privileges 
grant select, insert, update on TERMS_FILES to ETRADER_WEB;
grant select, insert, update on TERMS_FILES to ETRADER_WEB_CONNECT;
CREATE public synonym TERMS_FILES for TERMS_FILES;

-- Create sequence 
create sequence SEQ_TERMS_FILES;
CREATE public synonym SEQ_TERMS_FILES for SEQ_TERMS_FILES;


-- Create table
create table TERMS
(
  id          NUMBER not null,
  platform_id number,
  skin_id     NUMBER,
  part_id     NUMBER,
  file_id     NUMBER,
  constraint TERMS_PK primary key (ID)
);

alter table TERMS
  add constraint TERMS_PLATFORM_FK foreign key (platform_id)
  references platforms (ID);
alter table TERMS
  add constraint TERMS_FILE_FK foreign key (FILE_ID)
  references TERMS_FILES (ID);
alter table TERMS
  add constraint TERMS_PART_FK foreign key (PART_ID)
  references TERMS_PARTS (ID);
alter table TERMS
  add constraint TERMS_SKIN_FK foreign key (SKIN_ID)
  references SKINS (ID);
-- Grant/Revoke object privileges 
grant select, insert, update on TERMS to ETRADER_WEB_CONNECT;

CREATE public synonym TERMS for TERMS;

create sequence SEQ_TERMS;
CREATE public synonym SEQ_TERMS for SEQ_TERMS;


-- Create table
create table TERMS_PARAMS
(
  KEY          VARCHAR2(50) not null,
  VALUE        VARCHAR2(100) not null,
  constraint TERMS_PARAMS_PK primary key (KEY)
);
-- Create/Recreate primary, unique and foreign key constraints 
  
grant select, insert, update on TERMS_PARAMS to ETRADER_WEB_CONNECT;
CREATE public synonym TERMS_PARAMS for TERMS_PARAMS;

----------------------------------------------------------------------------------
-- END - BAC-1747 Ability to edit the general terms pages from Backend 
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- BAC-1827 Victor
----------------------------------------------------------------------------------

begin
  pkg_release.ddl_helper('drop index transactions_idwr', -1418);
end;
/

-- will take more time on production!
begin
  pkg_release.ddl_helper('create index idx_trns_writer_id on transactions (writer_id) online compress', -955);
end;
/

begin
  pkg_release.ddl_helper('create index idx_trns_longtrns on transactions
 (case
    when time_created - time_settled > 1 then
      time_settled
  end) online', -955);
end;
/

begin
  pkg_release.ddl_helper('create index idx_trns_nosetltime on transactions
 (case
    when time_settled is null then
      1
  end) online compress', -955);
end;
/
  
begin
  pkg_release.ddl_helper('alter table transaction_types add (is_active number(1) default 1 not null)', -1430);
end;
/

comment on column transaction_types.is_active is '0 - not active; 1 - active. No known relation to is_displayed flag';
comment on table transaction_types is '[TRNT]';

update transaction_types set is_active = 0 where id in (3, 18, 19, 21, 28, 29, 30, 34, 35, 44, 45);
commit;

begin
  pkg_release.ddl_helper('drop index IDX$$_420150005', -1418);
end;
/

begin
  pkg_release.ddl_helper('alter table transaction_types add constraint fk_trnt_trct foreign key (class_type) references transaction_class_types (id)', -2275);
end;
/

comment on table transaction_class_types is '[TRCT]';

begin
  pkg_release.ddl_helper('alter table skins add constraint fk_skns_skbc foreign key (business_case_id) references skin_business_cases (id)', -2275);
end;
/

begin
  for i in (select constraint_name
            from   user_constraints
            where  table_name = 'SKINS'
            and    r_constraint_name = (select constraint_name
                                        from   user_constraints
                                        where  table_name = 'LANGUAGES'
                                        and    constraint_type = 'P'))
  loop
    pkg_release.ddl_helper('alter table skins rename constraint ' || i.constraint_name || ' to fk_skns_lang', -2264);
  end loop;
end;
/

comment on table skin_business_cases is '[SKBC]';
comment on table skins is '[SKNS] definition of different language/regulation configuration of the client';
comment on table languages is '[LANG]';
comment on table clearing_providers is '[CLRP]';

@packages/pkg_transactions_backend.pck

create or replace public synonym pkg_transactions_backend for pkg_transactions_backend;
grant execute on pkg_transactions_backend to etrader_web;

begin
  pkg_writers.create_permissions('Transaction', 'transactions', 'view', 10);
  pkg_writers.create_permissions('Transaction', 'transactions', 'ccNum', 20);
  pkg_writers.create_permissions('Transaction', 'transactions', 'cancelPendingDeposit', 30);
  pkg_writers.create_permissions('Transaction', 'transactions', 'fraudCancelPendingDeposit', 40);
  pkg_writers.create_permissions('Transaction', 'transactions', 'approveWireDeposit', 50);
  pkg_writers.create_permissions('Transaction', 'transactions', 'cancelBankWireMistake', 60);
  pkg_writers.create_permissions('Transaction', 'transactions', 'exportExcel', 70);
  pkg_writers.create_permissions('Transaction', 'transactions', 'postponeJ4', 80);
end;
/

commit;

----------------------------------------------------------------------------------
-- END BAC-1827 Victor
----------------------------------------------------------------------------------


