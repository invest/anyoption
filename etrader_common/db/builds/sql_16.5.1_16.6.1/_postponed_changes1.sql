
whenever sqlerror exit

spool _postponed_changes1.log

-------------------------------------------------------------------------------------------------------------------------------------------
-- AR-2071 DB Field Additions - WRITERS_COMMISION_DEP, WRITERS_COMMISION_INV
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------
-- add columns to the table WRITERS_COMMISSION_DEP
ALTER TABLE WRITERS_COMMISSION_DEP ADD 
(
   ASSIGN_POP_ENTRIES_HIST_ID   NUMBER, 
   UNASSIGN_POP_ENTRIES_HIST_ID NUMBER, 
   NET_DEPOSITS                 NUMBER, 
   LAST_ATTRIBUTED_TRAN_ID      NUMBER
);

COMMENT ON COLUMN WRITERS_COMMISSION_DEP.ASSIGN_POP_ENTRIES_HIST_ID IS 'The id related to the table ''POPULATION_ENTRIES_HIST'' which represent the last representative assign attributed.';
COMMENT ON COLUMN WRITERS_COMMISSION_DEP.UNASSIGN_POP_ENTRIES_HIST_ID IS 'The id related to the table ''POPULATION_ENTRIES_HIST'' which represent the last representative unassign attributed.';
COMMENT ON COLUMN WRITERS_COMMISSION_DEP.NET_DEPOSITS IS 'Sum deposits minus sum withdrawals which valid for commission prior to the withdrawal requested.';
COMMENT ON COLUMN WRITERS_COMMISSION_DEP.LAST_ATTRIBUTED_TRAN_ID IS 'The last valid deposit attributed prior to the withdrawal created';

-- Add members to objects 
-- ************************************
-- Please pay attention !!!
-- Run the sripts only between running time of PROCEDURE WRITERS_COMMISSION_DEP_HANDLER !!!
-- ************************************
-- peh_t and peh_nt
drop type PEH_NT;

CREATE OR REPLACE TYPE peh_t IS OBJECT (
   T_assigned_writer_id NUMBER,
   T_assigned_writer_time DATE,
   T_assigned_peh_id NUMBER);
   
CREATE TYPE peh_nt IS TABLE OF peh_t;

-- wcd_t and wcd_nt
drop type wcd_nt;

CREATE OR REPLACE TYPE wcd_t IS OBJECT (
   T_transaction_id NUMBER,
   T_user_id NUMBER,
   T_writer_id NUMBER,
   T_amount NUMBER,
   T_assigned_peh_id NUMBER,
   T_unassigned_peh_id NUMBER,
   T_net_deposits NUMBER,
   T_last_attributed_tran_id NUMBER);
   
CREATE TYPE wcd_nt IS TABLE OF wcd_t;

@packages/writers_commission.pks

-------------------------------------------------------------------------------------------------------------------------------------------
-- AR-2071 DB Field Additions - WRITERS_COMMISION_DEP, WRITERS_COMMISION_INV
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------

spool off
