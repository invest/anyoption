create or replace package pkg_ccalert is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-06-30 12:05:51
  -- Purpose : 

  procedure check_card_country
  (
    o_result         out number
   ,i_credit_card_id in number
   ,i_transaction_id in number
  );

  procedure get_user_alert_countries
  (
    o_countries     out varchar2
   ,o_cc_number     out varchar2
   ,o_last_trn_date out date
   ,i_user_id       in number
  );

end pkg_ccalert;
/
create or replace package body pkg_ccalert is

  procedure check_card_country
  (
    o_result         out number
   ,i_credit_card_id in number
   ,i_transaction_id in number
  ) is
    l_cc_country_id   number;
    l_user_country_id number;
    l_time_created    date;
    l_user_id         number;
    l_sysdate         date := sysdate;
  begin
    select c.country_id, u.country_id, a.time_created, c.user_id
    into   l_cc_country_id, l_user_country_id, l_time_created, l_user_id
    from   credit_cards c
    join   users u
    on     u.id = c.user_id
    left   join cc_country_alerts a
    on     a.credit_card_id = c.id
    and    a.user_id = u.id
    where  c.id = i_credit_card_id;
  
    o_result := 0;
    if l_time_created is null
       and l_cc_country_id <> l_user_country_id
    then
    
      insert into cc_country_alerts
        (user_id, credit_card_id, cc_country_id, user_country_id, time_created, transaction_id)
      values
        (l_user_id, i_credit_card_id, l_cc_country_id, l_user_country_id, l_sysdate, i_transaction_id);
    
      for i in (select cc_country_id, lag(cc_country_id) over(order by time_created, credit_card_id) prev_cc_country_id
                from   cc_country_alerts
                where  user_id = l_user_id
                order  by time_created, credit_card_id)
      loop
        if i.cc_country_id <> i.prev_cc_country_id
        then
          o_result := 1;
          exit;
        end if;
      end loop;
    
    end if;
  end check_card_country;

  procedure get_user_alert_countries
  (
    o_countries     out varchar2
   ,o_cc_number     out varchar2
   ,o_last_trn_date out date
   ,i_user_id       in number
  ) is
    l_user_time_cr date;
  begin
    select stringagg(country_name), max(time_created) -- MAX to avoid no_data_found
    into   o_countries, l_user_time_cr
    from   (select c.country_name, u.time_created, row_number() over(partition by a.cc_country_id order by a.time_created) rn
            from   cc_country_alerts a
            join   countries c
            on     c.id = a.cc_country_id
            join   users u
            on     u.id = a.user_id
            where  a.user_id = i_user_id
            order  by a.time_created)
    where  rn = 1;
  
    select m.time_created, (select max(c.cc_number) from credit_cards c where c.id = m.credit_card_id)
    into   o_last_trn_date, o_cc_number
    from   (select t.credit_card_id, t.time_created
            from   transactions t
            where  t.type_id = (select id from transaction_types where code = 'TRANS_TYPE_CC_DEPOSIT')
            and    t.status_id in (2, 7, 8)
            and    t.user_id = i_user_id
            and    t.time_created >= l_user_time_cr
            order  by t.time_created desc) m
    where  rownum <= 1;
  
  exception
    when no_data_found then
      null;
  end get_user_alert_countries;

end pkg_ccalert;
/
