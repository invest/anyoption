create or replace trigger after_users_market_trc
  after update of combination_id, dynamic_param, http_referer, mobile_phone, land_line_phone, affiliate_key, special_code, affiliate_system, affiliate_system_user_time on users
  for each row
declare
  t_marketing_att_obj_old marketing_attribution_obj;
  t_marketing_att_obj_new marketing_attribution_obj;
begin
  if pkg_audit.dif(:new.http_referer, :old.http_referer)
     or pkg_audit.dif(:new.dynamic_param, :old.dynamic_param)
     or pkg_audit.dif(:new.combination_id, :old.combination_id)
     or pkg_audit.dif(:new.user_name, :old.user_name)
     or pkg_audit.dif(:new.email, :old.email)
  then
    insert into users_history
      (id, user_id, http_referer, dynamic_param, comb_id, user_name, email)
    values
      (seq_users_history.nextval, :old.id, :old.http_referer, :old.dynamic_param, :old.combination_id, :old.user_name, :old.email);
  end if;

  t_marketing_att_obj_old := marketing_attribution_obj(:old.id
                                                      ,:old.marketing_attribution_type_id
                                                      ,:old.combination_id
                                                      ,:old.dynamic_param
                                                      ,null
                                                      ,null
                                                      ,null
                                                      ,:old.affiliate_key
                                                      ,:old.special_code
                                                      ,:old.affiliate_system
                                                      ,:old.affiliate_system_user_time);

  t_marketing_att_obj_new := marketing_attribution_obj(:new.id
                                                      ,:new.marketing_attribution_type_id
                                                      ,:new.combination_id
                                                      ,:new.dynamic_param
                                                      ,null
                                                      ,null
                                                      ,null
                                                      ,:new.affiliate_key
                                                      ,:new.special_code
                                                      ,:new.affiliate_system
                                                      ,:new.affiliate_system_user_time);

  marketing.update_marketing_att(t_marketing_att_obj_old, t_marketing_att_obj_new);
end;
/
