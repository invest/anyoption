create or replace trigger after_users_wlb
  after insert or update of winlose_balance on users
  for each row
begin
  if :new.winlose_balance != :old.winlose_balance
  then
    update users_regulation a set threshold = :new.winlose_balance, a.threshold_curr = :new.currency_id where user_id = :old.id;
  end if;
end;
/
