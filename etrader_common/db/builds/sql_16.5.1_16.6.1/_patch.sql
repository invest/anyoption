
---------- victor; first deposit id fix on transactions status update
-- these must be executed with privileged user!
--grant aq_administrator_role to etrader;
--grant aq_user_role to etrader;
--grant execute on dbms_aqadm to etrader;
--grant execute on dbms_aq to etrader;
--grant execute on dbms_aqin to etrader;

begin
  pkg_release.ddl_helper('
create type t_first_deposit is object
(
  user_id        number,
  transaction_id number,
  time_created   date
)' ,-955);
end;
/

begin
  dbms_aqadm.create_queue_table(queue_table        => 'QTAB_FIRST_DEPOSIT'
                               ,queue_payload_type => 'T_FIRST_DEPOSIT'
                               ,multiple_consumers => false
                               ,compatible         => '10.0'
                               ,primary_instance   => 1
                               ,secondary_instance => 0
                               ,comment            => 'To update users.first_deposit_id on event');
exception
  when others then
    if sqlcode = -24001
    then
      null;
    else
      raise;
    end if;
end;
/

begin
  dbms_aqadm.create_queue(queue_name     => 'Q_FIRST_DEPOSIT'
                         ,queue_table    => 'QTAB_FIRST_DEPOSIT'
                         ,max_retries    => 0
                         ,retention_time => 600
                         ,comment        => 'AQ is used to update users.first_deposit_id by callback procedure');
exception
  when others then
    if sqlcode = -24006
    then
      null;
    else
      raise;
    end if;                         
end;
/

begin
  dbms_aqadm.start_queue(queue_name => 'Q_FIRST_DEPOSIT');
end;
/

@packages/pkg_deposit.pck

declare
  l_aglist sys.aq$_reg_info_list;
begin
  l_aglist := sys.aq$_reg_info_list(sys.aq$_reg_info('Q_FIRST_DEPOSIT'
                                                    ,dbms_aq.namespace_aq
                                                    ,lower('plsql://pkg_deposit.user_fdi_callback')
                                                    ,hextoraw('FF')
                                                    ,dbms_aq.ntfn_qos_reliable
                                                    ,0));

  dbms_aq.register(l_aglist, 1);
end;
/

---------- END; victor; first deposit id fix on transactions status update

-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3258 [DB] 'Gold/ Silver' market updates have incorrect data in 'markets' table
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
update markets set feed_name = 'XAG=' where id = 137;
update markets set feed_name = 'XAU=ALL' where id = 20;
update markets set feed_name = 'XAG=.Option+' where id = 653;
update markets set feed_name = 'XAU=ALL.Option+' where id = 606;
update markets set feed_name = 'XAU=ALL.Dynamicssss' where id = 732;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End CORE-3258 [DB] 'Gold/ Silver' market updates have incorrect data in 'markets' table
-------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------
--LioR SoLoMoN
--BAC-2090 - [EPG implementation] - CSS for checkout
 
update CLEARING_PROVIDERS cp set cp.PROPS = cp.PROPS || 'CSS_URL=css/EPG/styles.css;'
 where cp.id = 57;
 
commit;
      
-- victor; modified after complain from Chen S.
alter table files modify (reference varchar2(40 char));


-- victor; fixed data in BONUS_AUTO_PARMETERS table and added constriants in production DB
delete from bonus_auto_parmeters where rowid in (select rid
from   (select a.rowid rid
              ,a.*
              ,count(*) over(partition by bonus_id, combination_id, campaign_id, skin_id, business_skin_id, country_id, affiliate_key, is_active, time_start, time_end, type) cnt
              ,row_number() over(partition by bonus_id, combination_id, campaign_id, skin_id, business_skin_id, country_id, affiliate_key, is_active, time_start, time_end, type order by id) rn
        from   bonus_auto_parmeters a)
where  rn > 1);
commit;

declare
  l_max_id number;
begin
  select nvl(max(id), 1) into l_max_id from bonus_auto_parmeters;
  pkg_release.ddl_helper('create sequence seq_bonus_auto_parmeters start with ' || l_max_id, -955);

  for i in (select *
            from   (select a.rowid rid, a.*, row_number() over(partition by a.id order by a.id) rn from bonus_auto_parmeters a)
            where  rn > 1)
  loop
    update bonus_auto_parmeters a set id = seq_bonus_auto_parmeters.nextval where rowid = i.rid;
  end loop;
  commit;
end;
/

begin
  pkg_release.ddl_helper('alter table bonus_auto_parmeters add constraint pk_bonus_auto_parmeters primary key (id)', -2260);
end;
/
  
begin
  pkg_release.ddl_helper('alter table bonus_auto_parmeters modify (type not null)', -1442);
end;
/

begin
  pkg_release.ddl_helper('alter table bonus_auto_parmeters add constraint uk_bonus_auto_parmeters unique (bonus_id
      ,combination_id,campaign_id,skin_id,business_skin_id,country_id,affiliate_key,is_active,time_start,time_end,type)', -2261);
end;
/

begin
  pkg_release.ddl_helper('alter table bonus_auto_parmeters add constraint fk_bnap_bonus foreign key (bonus_id) references bonus (id)', -2275);
end;
/

begin
  pkg_release.ddl_helper('create table bonus_auto_parmeter_types
(
    id          number       not null
   ,description varchar2(30) not null
   ,constraint pk_bonus_auto_parmeter_types primary key(id)
)', -955);
end;
/

begin
  insert into bonus_auto_parmeter_types (id, description) values (1, 'register');
  insert into bonus_auto_parmeter_types (id, description) values (2, 'remarketing');
  insert into bonus_auto_parmeter_types (id, description) values (3, 'login');
exception
  when dup_val_on_index then
    null;
end;
/

begin
  pkg_release.ddl_helper('alter table bonus_auto_parmeters add constraint fk_bnap_bapt foreign key (type) references bonus_auto_parmeter_types(id)', -2275);
end;
/

comment on table  bonus_auto_parmeters is '[BNAP] ';

grant select on seq_bonus_auto_parmeters to etrader_web_connect;
create or replace public synonym seq_bonus_auto_parmeters for seq_bonus_auto_parmeters;

grant select on bonus_auto_parmeter_types to etrader_web_connect;
create or replace public synonym bonus_auto_parmeter_types for bonus_auto_parmeter_types;

-- END; victor; fixed data in BONUS_AUTO_PARMETERS 

-------------------------------------------------------------------------------------------------------------------------------------------
-- AR-1836 - AO - Mobile minisite deposit page (Web)
-- Kristina
-------------------------------------------------------------------------------------------------------------------------------------------
insert into writers
(id,user_name,password,first_name,last_name,street,city_id,zip_code,email,comments,time_birth_date,mobile_phone,land_line_phone,is_active,street_no,utc_offset,group_id,is_support_enable,dept_id,sales_type,nick_name_first,nick_name_last,auto_assign,cms_login,last_failed_time,failed_count,sales_type_dept_id,emp_id,role_id,time_created)
values
(23000,'AO_MINISITE','ao_minisite$$$','ao_minisite','ao_minisite',null,null,null,'na',null,sysdate,null,null,1,null,'GMT+02:00',0,1,9,null,null,null,0,null,null,0,null,null,null,sysdate);

insert into writers
(id,user_name,password,first_name,last_name,street,city_id,zip_code,email,comments,time_birth_date,mobile_phone,land_line_phone,is_active,street_no,utc_offset,group_id,is_support_enable,dept_id,sales_type,nick_name_first,nick_name_last,auto_assign,cms_login,last_failed_time,failed_count,sales_type_dept_id,emp_id,role_id,time_created)
values
(24000,'CO_MINISITE','co_minisite$$$','co_minisite','co_minisite',null,null,null,'na',null,sysdate,null,null,1,null,'GMT+02:00',0,1,9,null,null,null,0,null,null,0,null,null,null,sysdate);
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End AR-1836 - AO - Mobile minisite deposit page (Web)
-------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------
-- BAC-139 Create a Risk email alert for deposit attempts with stolen cards
-- Jamal
-------------------------------------------------------------------------------------------------------------------------------------------
-- Create table
create table STOLEN_CC_ERR_CODE
(
  provider_type INTEGER not null,
  code          VARCHAR2(10) not null,
  description   VARCHAR2(200)
);
-- Add comments to the columns 
comment on column STOLEN_CC_ERR_CODE.provider_type is '1-From Issueres; 2-From Wirecard; 3-From Credit Guard;';
-- Grant/Revoke object privileges 

grant select on STOLEN_CC_ERR_CODE to etrader_web_connect;
create or replace public synonym STOLEN_CC_ERR_CODE for STOLEN_CC_ERR_CODE;

@tables_data/stolen_cc_err_code.sql
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
--END  BAC-139 Create a Risk email alert for deposit attempts with stolen cards
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
--BAC-1699 New automatic risk Alert - Multiple CCs from Different countries
-- Jamal
-------------------------------------------------------------------------------------------------------------------------------------------
insert into risk_alerts_types
  (id, code, description, class_type_id)
values
  (19, 'RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES', 'risk.type.deposit.cc.from.different.countries', 1);
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END BAC-1699 New automatic risk Alert - Multiple CCs from Different countries
-------------------------------------------------------------------------------------------------------------------------------------------


------ victor; CORE-2520; Dynamics server side and database 

@triggers/after_users_market_trc.trg
@triggers/after_users_wlb.trg
@triggers/before_ins_users.trg
@triggers/before_users_password.trg
@triggers/silverpop_users.trg
@triggers/trg_first_dep_users.trg

begin
  for i in (select ua.user_id
            from   users u
            join   users_active_data ua
            on     ua.user_id = u.id
            where  u.skin_id in (18, 20)
            and    ua.has_dynamics = 0)
  loop
    update users_active_data
    set    has_dynamics = 1
    where  has_dynamics = 0
    and    user_id = i.user_id;
    commit;
  end loop;
end;
/

begin
  pkg_release.ddl_helper('alter table users modify (winlose_balance constraint nn_usr_winlose_balance not null enable novalidate)', -1442);
end;
/

begin
  pkg_release.ddl_helper('alter table users modify constraint nn_usr_winlose_balance validate', -2296);
end;
/

begin
  pkg_release.ddl_helper('alter table users_active_data add constraint fk_usrs_usad foreign key (user_id) references users(id) enable novalidate', -2275);
end;
/

begin
  pkg_release.ddl_helper('alter table users_active_data modify constraint fk_usrs_usad validate', -2298);
end;
/

begin
  pkg_release.ddl_helper('alter table users_active_data rename constraint unique_uad_user_id to uk_users_active_data', -23292);
end;
/

begin
  pkg_release.ddl_helper('alter index IDX_UAD_USER_ID rename to uk_users_active_data', -1418);
end;
/

------ END; victor; CORE-2520; Dynamics server side and database
--------------------------------------------------------------------------
--  Pavlin CORE-3185 Frozen traders not being removed from my copiers list
--------------------------------------------------------------------------
alter table copyop.copyop_frozen add (unlinked number(1) default 0 not null);
INSERT INTO copyop.cfg_properties (key, value, description) VALUES ('FROZEN_USERS_UNLINK_HOURS', 6, 'unlink frozen users after this period (hours). default 6');
commit;

---- depends on server fix CORE-3424
---- should be executed after the fix is deployed, otherwise data will continue to be populated incorrectly.
-- will update about 900 records in prod
update investments
set    house_result = lose
where  is_canceled = 1
and    is_settled = 1
and    lose > 0
and    time_created >= to_date('2016-05', 'yyyy-mm');

commit;
--------------------------------------------------------------------------
--  Pavlin CORE-3185 Frozen traders not being removed from my copiers list
--------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- add STATIC_LANDING_PAGE_PATH_COPYOP_REGULATED
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------

-- add new columns
ALTER TABLE STATIC_LANDING_PAGE_PATH
ADD (IS_REGULATED NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN STATIC_LANDING_PAGE_PATH.IS_REGULATED IS 'Mark record as regulated or not, 1 - REGULATED, 0 - NOT REGULATED';

UPDATE 
  STATIC_LANDING_PAGE_PATH slpp
SET 
  slpp.IS_REGULATED = 1
WHERE 
  slpp.id = 3;

-------------------------------------------------------------------------------------------------------------------------------------------
-- add STATIC_LANDING_PAGE_PATH_COPYOP_REGULATED
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------
-- START Ivan Petkov CORE-3478 [Bubbles] Duplicate setup for bubbles in skin_market_group_markets
-------------------------------------------------------------------------------------------------------------------------------------------


delete from skin_market_group_markets
where  id in
       (select id
        from   (select id, skin_market_group_id, market_id, row_number() over(partition by skin_market_group_id, market_id order by id) rn
                from   skin_market_group_markets)
        where  rn > 1);
commit;

begin
  pkg_release.ddl_helper('drop index skin_market_idx_group_id', -1418);
end;
/

begin
  pkg_release.ddl_helper('alter table skin_market_group_markets add constraint uk_skin_market_group_markets unique (skin_market_group_id, market_id)', -2261);
end;
/

-------------------------------------------------------------------------------------------------------------------------------------------
-- END Ivan Petkov CORE-3478
-------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------
--  Pavlin CORE-
--------------------------------------------------------------------------
Insert into enumerators (ID,ENUMERATOR,CODE,VALUE,DESCRIPTION) values (246,'insurance_period_2_end_time','insurance_period_2_end_time','4','golden minites end time for quarter expiry');
INSERT INTO enumerators (ID,ENUMERATOR,CODE,VALUE,DESCRIPTION) VALUES (245,'insurance_period_2_start_time','insurance_period_2_start_time','5','golden minites start time for quarter expiry');
commit;

ALTER TABLE investments ADD INSURANCE_FLAG_ADD NUMBER;
comment on column investments.INSURANCE_FLAG_ADD is 'Additional (second) Roll-Forward flag. null - not passed qualification, 2 - Roll forward';

ALTER table opportunities ADD (GM_LEVEL_ADD NUMBER);
comment on column opportunities.GM_LEVEL_ADD is 'Additional (second) Roll-Forward level. updated by level service before second qualification';

@procedures/qualify_inv_for_add_insurances.prc

create or replace public synonym qualify_inv_for_add_insurances for qualify_inv_for_add_insurances;
grant execute on qualify_inv_for_add_insurances to etrader_service;

insert into jobs (id, server, running, run_interval, description, job_class) values ( 35, 'run later', 1, 10, 'remove frozen users from copy/copied lists', 'il.co.etrader.jobs.copyop.CopyopFrozenUnlinkJob');
commit;


----- victor; add constraints on exchanges
alter table exchanges modify (name varchar2(60));
alter table exchanges modify (half_day_close_time varchar2(5));
alter table exchanges add constraint uk_exchanges unique (name);

delete from exchange_holidays
where  id in (select id
              from   (select a.*, row_number() over(partition by exchange_id, holiday order by id) rn from exchange_holidays a)
              where  rn > 1);
commit;

alter table exchange_holidays add constraint uk_exchange_holidays unique (exchange_id, holiday);
----- END victor; add constraints on exchanges



--------- BAC-1699; victor
create table cc_country_alerts
(
   user_id         number not null
  ,credit_card_id  number not null
  ,cc_country_id   number not null
  ,user_country_id number not null
  ,time_created    date   not null
  ,transaction_id  number not null
  ,constraint pk_cc_country_alerts primary key (user_id, credit_card_id)
);  

alter table cc_country_alerts add constraint fk_ccca_users foreign key (user_id) references users(id);
alter table cc_country_alerts add constraint fk_ccca_ccards foreign key (credit_card_id) references credit_cards(id);
alter table cc_country_alerts add constraint fk_ccca_trans foreign key (transaction_id) references transactions(id);

comment on table cc_country_alerts is '[CCCA] Alerts for transactions that has different country_id for the user and for the credit card. Only first occurance is recorded';

update credit_cards c set country_id = (select country_id from bins b where b.from_bin=c.bin) where c.country_id is null;
update credit_cards c set country_id = (select country_id from users u where u.id=c.user_id) where c.country_id is null;
commit;

begin
  pkg_release.ddl_helper('alter table transaction_types add constraint uk_transaction_types unique (code)', -2261);
end;
/

begin
  pkg_release.ddl_helper('alter table credit_cards modify (country_id not null)', -1442);
end;
/

begin
  pkg_release.ddl_helper('alter table credit_cards add constraint fk_ccards_cntr foreign key (country_id) references countries(id) enable novalidate', -2275);
end;
/

begin
  pkg_release.ddl_helper('alter table credit_cards modify constraint fk_ccards_cntr validate', -2298);
end;
/

begin
  pkg_release.ddl_helper('drop index idx_bins_bins_range', -1418);
end;
/

begin
  pkg_release.ddl_helper('alter table bins drop constraint uk_bins_from_bin drop index', -2443);
end;
/

-- remove duplicated invalid BINS from DB
delete from bins
where  from_bin in (975221, 975222)
and    country_name = 'SWEDEN';
commit;

begin
  pkg_release.ddl_helper('create unique index idx_bins_range on bins (from_bin, to_bin)', -955);
end;
/

@packages/pkg_ccalert.pck

create or replace public synonym pkg_ccalert for pkg_ccalert;
grant execute on pkg_ccalert to etrader_web_connect;

--------- END; BAC-1699; victor

--------- AR-2221 and AR-2202; victor
@packages/pkg_writers_comm_reports.pck

---- victor; remove global variables
@packages/pkg_transactions_backend.pck

---- victor; fix for sales_type
@packages/pkg_writers.pck

