#!/bin/bash
#
#
# Script Variables (change them to suit your needs)
export TNS_ADMIN=/opt/oracle-client/11g
export NLS_LANG=AMERICAN_AMERICA.UTF8
export PATH=/opt/oracle-client/11g:/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin

#-- FTP properties --#
HOST='anyoption.data.netrefer.com'
USER='anyoption_data'
PASSWD='HyQtddN'
FTPLOG='/home/oracle/Scripts/ftplog.log'
FTPTEMPLOG='/tmp/NetRefer/ftptemplog.log'

cd /tmp/CUP

# ------- Daily ----------

sqlplus -s " etrader_web/w3tr4d3r@DBLIVE " <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='YYYYMMDD HH24:MI' ;
set pages
SET MARKUP HTML ON
spool spread_daily.html
@/home/oracle/Scripts/SQL/spread_daily.sql
CLEAR BUFFER
spool off
exit
eof1

FILE1="spread_daily.html"

# ------- Weekly ----------

sqlplus -s " etrader_web/w3tr4d3r@DBLIVE " <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='YYYYMMDD HH24:MI' ;
set pages
SET MARKUP HTML ON
spool spread_weekly.html
@/home/oracle/Scripts/SQL/spread_weekly.sql
CLEAR BUFFER
spool off
exit
eof1

FILE2="spread_weekly.html"

# ------- Monthly ----------

sqlplus -s " etrader_web/w3tr4d3r@DBLIVE " <<eof1
ALTER SESSION SET NLS_DATE_FORMAT='YYYYMMDD HH24:MI' ;
set pages
SET MARKUP HTML ON
spool spread_monthly.html
@/home/oracle/Scripts/SQL/spread_monthly.sql
CLEAR BUFFER
spool off
exit
eof1

FILE3="spread_monthly.html"

## create email
zip spread.zip spread_daily.html spread_weekly.html spread_monthly.html

echo "Report for CUP deposits" |mutt -a spread.zip -s "Successful CUP deposits" simeonps@anyoption.com

rm -vf /tmp/CUP/*
