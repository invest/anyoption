--Referpartner. dp or campaign
update 
      users u
set
      u.affiliate_system_user_time = trunc(sysdate-1/24),
      u.affiliate_system = 2,
      u.affiliate_key = REGEXP_SUBSTR(nvl(u.dynamic_param,0),'[0-9]+',1 ,1)
where
      u.affiliate_system_user_time is null
      AND (u.dynamic_param like 'RPOA_%'  OR u.combination_id in
          (select 
                  id 
          from 
                  marketing_combinations mcom
          where 
                  mcom.campaign_id = 1535))
      AND (trunc(u.time_first_authorized) = trunc(sysdate-1/24) OR u.id in
          (select 
                t.user_id 
          from 
                transactions t, users_first_deposit ufd
          where 
                t.id = ufd.first_deposit_id AND ufd.user_id = t.user_id
                AND trunc(t.time_created) = trunc(sysdate-1/24)));  
