grant select, update, insert on ETRADER.FRAUD_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.FRAUDS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.WIRES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.BANKS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.USERS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CITIES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ADS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ISSUES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CURRENCIES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ISSUE_SUBJECTS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ISSUE_STATUSES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CREDIT_CARD_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CREDIT_CARDS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CHARGE_BACKS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CHARGE_BACK_STATUSES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.WRITERS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.TRANSACTION_STATUSES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.TRANSACTION_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.TRANSACTIONS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.MARKETS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_CHANGES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_ODDS_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.INVESTMENTS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.INVESTMENT_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.LIMITS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.FILE_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.TEMPLATES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_TEMPLATES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITIES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ROLES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.SERVICE_CONFIG to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ISSUE_CHANNELS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ISSUE_PRIORITIES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CALLS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CALL_SUBJECTS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CALL_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CLICKS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.ENUMERATORS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CC_BLACK_LIST to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.EXCHANGES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.EXCHANGE_HOLIDAYS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_TEMPLATES_OLD to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.CHEQUES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_SHIFTINGS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.INVESTMENT_LIMITS_GROUPS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.INVESTMENT_LIMITS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.INVESTMENT_LIMIT_GROUP to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.FILES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.MARKET_GROUPS to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.OPPORTUNITY_TYPES to ETRADER_WEB_CONNECT;
grant select, update, insert on ETRADER.USER_CLASSES to ETRADER_WEB_CONNECT;


create public synonym FRAUD_TYPES for ETRADER.FRAUD_TYPES;
create public synonym FRAUDS for ETRADER.FRAUDS;
create public synonym WIRES for ETRADER.WIRES;
create public synonym BANKS for ETRADER.BANKS;
create public synonym USERS for ETRADER.USERS;
create public synonym CITIES for ETRADER.CITIES;
create public synonym ADS for ETRADER.ADS;
create public synonym ISSUES for ETRADER.ISSUES;
create public synonym CURRENCIES for ETRADER.CURRENCIES;
create public synonym ISSUE_SUBJECTS for ETRADER.ISSUE_SUBJECTS;
create public synonym ISSUE_STATUSES for ETRADER.ISSUE_STATUSES;
create public synonym CREDIT_CARD_TYPES for ETRADER.CREDIT_CARD_TYPES;
create public synonym CREDIT_CARDS for ETRADER.CREDIT_CARDS;
create public synonym CHARGE_BACKS for ETRADER.CHARGE_BACKS;
create public synonym CHARGE_BACK_STATUSES for ETRADER.CHARGE_BACK_STATUSES;
create public synonym WRITERS for ETRADER.WRITERS;
create public synonym TRANSACTION_STATUSES for ETRADER.TRANSACTION_STATUSES;
create public synonym TRANSACTION_TYPES for ETRADER.TRANSACTION_TYPES;
create public synonym TRANSACTIONS for ETRADER.TRANSACTIONS;
create public synonym MARKETS for ETRADER.MARKETS;
create public synonym OPPORTUNITY_CHANGES for ETRADER.OPPORTUNITY_CHANGES;
create public synonym OPPORTUNITY_ODDS_TYPES for ETRADER.OPPORTUNITY_ODDS_TYPES;
create public synonym INVESTMENTS for ETRADER.INVESTMENTS;
create public synonym INVESTMENT_TYPES for ETRADER.INVESTMENT_TYPES;
create public synonym LIMITS for ETRADER.LIMITS;
create public synonym FILE_TYPES for ETRADER.FILE_TYPES;
create public synonym TEMPLATES for ETRADER.TEMPLATES;
create public synonym OPPORTUNITY_TEMPLATES for ETRADER.OPPORTUNITY_TEMPLATES;
create public synonym OPPORTUNITIES for ETRADER.OPPORTUNITIES;
create public synonym ROLES for ETRADER.ROLES;
create public synonym SERVICE_CONFIG for ETRADER.SERVICE_CONFIG;
create public synonym ISSUE_CHANNELS for ETRADER.ISSUE_CHANNELS;
create public synonym ISSUE_PRIORITIES for ETRADER.ISSUE_PRIORITIES;
create public synonym CALLS for ETRADER.CALLS;
create public synonym CALL_SUBJECTS for ETRADER.CALL_SUBJECTS;
create public synonym CALL_TYPES for ETRADER.CALL_TYPES;
create public synonym CLICKS for ETRADER.CLICKS;
create public synonym ENUMERATORS for ETRADER.ENUMERATORS;
create public synonym CC_BLACK_LIST for ETRADER.CC_BLACK_LIST;
create public synonym EXCHANGES for ETRADER.EXCHANGES;
create public synonym EXCHANGE_HOLIDAYS for ETRADER.EXCHANGE_HOLIDAYS;
create public synonym OPPORTUNITY_TEMPLATES_OLD for ETRADER.OPPORTUNITY_TEMPLATES_OLD;
create public synonym CHEQUES for ETRADER.CHEQUES;
create public synonym OPPORTUNITY_SHIFTINGS for ETRADER.OPPORTUNITY_SHIFTINGS;
create public synonym INVESTMENT_LIMITS_GROUPS for ETRADER.INVESTMENT_LIMITS_GROUPS;
create public synonym INVESTMENT_LIMITS for ETRADER.INVESTMENT_LIMITS;
create public synonym INVESTMENT_LIMIT_GROUP for ETRADER.INVESTMENT_LIMIT_GROUP;
create public synonym FILES for ETRADER.FILES;
create public synonym MARKET_GROUPS for ETRADER.MARKET_GROUPS;
create public synonym OPPORTUNITY_TYPES for ETRADER.OPPORTUNITY_TYPES;
create public synonym USER_CLASSES for ETRADER.USER_CLASSES;
create public synonym CREATE_MONTH_OPPORTUNITIES for ETRADER.CREATE_MONTH_OPPORTUNITIES;

grant execute any procedure to ETRADER_WEB_CONNECT;


create public synonym SEQ_ADS for ETRADER.SEQ_ADS;
create public synonym SEQ_CALLS for ETRADER.SEQ_CALLS;
create public synonym SEQ_CC_BLACK_LIST for ETRADER.SEQ_CC_BLACK_LIST;
create public synonym SEQ_CHARGE_BACKS for ETRADER.SEQ_CHARGE_BACKS;
create public synonym SEQ_CHEQUES for ETRADER.SEQ_CHEQUES;
create public synonym SEQ_CLICKS for ETRADER.SEQ_CLICKS;
create public synonym SEQ_CREDIT_CARDS for ETRADER.SEQ_CREDIT_CARDS;
create public synonym SEQ_EXCHANGE_HOLIDAYS for ETRADER.SEQ_EXCHANGE_HOLIDAYS;
create public synonym SEQ_FILES for ETRADER.SEQ_FILES;
create public synonym SEQ_FRAUDS for ETRADER.SEQ_FRAUDS;
create public synonym SEQ_INVESTMENTS for ETRADER.SEQ_INVESTMENTS;
create public synonym SEQ_INV_LIMITS for ETRADER.SEQ_INV_LIMITS;
create public synonym SEQ_INV_LIMITS_GROUPS for ETRADER.SEQ_INV_LIMITS_GROUPS;
create public synonym SEQ_ISSUES for ETRADER.SEQ_ISSUES;
create public synonym SEQ_LIMITS for ETRADER.SEQ_LIMITS;
create public synonym SEQ_MARKETS for ETRADER.SEQ_MARKETS;
create public synonym SEQ_MARKET_GROUPS for ETRADER.SEQ_MARKET_GROUPS;
create public synonym SEQ_OPPORTUNITIES for ETRADER.SEQ_OPPORTUNITIES;
create public synonym SEQ_OPPORTUNITIES_CHANGES for ETRADER.SEQ_OPPORTUNITIES_CHANGES;
create public synonym SEQ_OPPORTUNITY_ODDS_TYPES for ETRADER.SEQ_OPPORTUNITY_ODDS_TYPES;
create public synonym SEQ_OPPORTUNITY_SHIFTINGS for ETRADER.SEQ_OPPORTUNITY_SHIFTINGS;
create public synonym SEQ_OPPORTUNITY_TEMPLATES for ETRADER.SEQ_OPPORTUNITY_TEMPLATES;
create public synonym SEQ_RECEIPT for ETRADER.SEQ_RECEIPT;
create public synonym SEQ_ROLES for ETRADER.SEQ_ROLES;
create public synonym SEQ_STRESS for ETRADER.SEQ_STRESS;
create public synonym SEQ_TEMPLATES for ETRADER.SEQ_TEMPLATES;
create public synonym SEQ_TRANSACTIONS for ETRADER.SEQ_TRANSACTIONS;
create public synonym SEQ_USERS for ETRADER.SEQ_USERS;
create public synonym SEQ_WIRES for ETRADER.SEQ_WIRES;
create public synonym SEQ_WITHDRAWS for ETRADER.SEQ_WITHDRAWS;
create public synonym SEQ_WRITERS for ETRADER.SEQ_WRITERS;


grant select, update, insert on ETRADER.OPTIONS_EXPIRATIONS to ETRADER_WEB_CONNECT;
create public synonym OPTIONS_EXPIRATIONS for ETRADER.OPTIONS_EXPIRATIONS;
create public synonym SEQ_WRITERS for ETRADER.SEQ_OPTIONS_EXPIRATIONS;

---
create public synonym DYNAMIC_BANNERS for ETRADER.DYNAMIC_BANNERS; -- deployed on 11/5/08
grant select on ETRADER.DYNAMIC_BANNERS to ETRADER_WEB_CONNECT; -- deployed on 11/5/08

-- oneTouch
grant select, update, insert on ETRADER.OPPORTUNITY_ONE_TOUCH to ETRADER_WEB_CONNECT; -- deployed on 28/6/08
create public synonym OPPORTUNITY_ONE_TOUCH  for ETRADER.OPPORTUNITY_ONE_TOUCH; -- deployed on 28/6/08


--TAX-- deployed 30/06/08
create public synonym TAX_HISTORY for ETRADER.TAX_HISTORY;
create public synonym SEQ_TAX_HISTORY for ETRADER.SEQ_TAX_HISTORY;
grant select, insert on ETRADER.TAX_HISTORY to ETRADER_WEB_CONNECT;

--BIN_BLACK_LIST deployed 13/7/08
create public synonym BIN_BLACK_LIST for ETRADER.BIN_BLACK_LIST;
create public synonym SEQ_BIN_BLACK_LIST for ETRADER.SEQ_BIN_BLACK_LIST;
grant select, insert on ETRADER.BIN_BLACK_LIST to ETRADER_WEB_CONNECT;

-- opportunity one touch table deployed 7/8/08
create public synonym SEQ_OPPORTUNITY_ONE_TOUCH for ETRADER.SEQ_TAX_HISTORY;

-- kobi add for MESSAGES table deployed 14/8/8
grant select, insert,update on ETRADER.MESSAGES to ETRADER_WEB_CONNECT;
create public synonym MESSAGES  for ETRADER.MESSAGES;

-- markets history
grant select, insert, update on ETRADER.MARKET_RATES to ETRADER_WEB_CONNECT;
create public synonym MARKET_RATES for ETRADER.MARKET_RATES;

-- user market disable   done!
grant select, insert, update on ETRADER.USER_MARKET_DISABLE to ETRADER_WEB_CONNECT;
create public synonym USER_MARKET_DISABLE for ETRADER.USER_MARKET_DISABLE;
create public synonym SEQ_USER_MARKET_DISABLE for ETRADER.SEQ_USER_MARKET_DISABLE;

-- clearing done!

grant select, insert, update on ETRADER.CLEARING_PROVIDERS to ETRADER_WEB_CONNECT;
create public synonym CLEARING_PROVIDERS for ETRADER.CLEARING_PROVIDERS;

grant select, insert, update on ETRADER.CLEARING_BIN_ROUTES to ETRADER_WEB_CONNECT;
create public synonym CLEARING_BIN_ROUTES for ETRADER.CLEARING_BIN_ROUTES;
create public synonym SEQ_CLEARING_BIN_ROUTES for ETRADER.SEQ_CLEARING_BIN_ROUTES;

grant select, insert, update on ETRADER.CLEARING_CURRENCY_ROUTES to ETRADER_WEB_CONNECT;
create public synonym CLEARING_CURRENCY_ROUTES for ETRADER.CLEARING_CURRENCY_ROUTES;
create public synonym SEQ_CLEARING_CURRENCY_ROUTES for ETRADER.SEQ_CLEARING_CURRENCY_ROUTES;

--grant execute on ETRADER.GET_CLEARING_ROUTE to ETRADER_WEB_CONNECT;
create public synonym GET_CLEARING_ROUTE for ETRADER.GET_CLEARING_ROUTE;



-- 10/03/09 Kobi marketing development done!
grant select, insert, update on ETRADER.MARKETING_PAYMENT_TYPES to ETRADER_WEB_CONNECT;
create public synonym MARKETING_PAYMENT_TYPES for ETRADER.MARKETING_PAYMENT_TYPES;

grant select, insert, update on ETRADER.MARKETING_PAYMENT_RECIPIENTS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_PAYMENT_RECIPIENTS for ETRADER.MARKETING_PAYMENT_RECIPIENTS;

grant select, insert, update on ETRADER.MARKETING_CAMPAIGNS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_CAMPAIGNS for ETRADER.MARKETING_CAMPAIGNS;

grant select, insert, update on ETRADER.MARKETING_CONTENTS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_CONTENTS for ETRADER.MARKETING_CONTENTS;

grant select, insert, update on ETRADER.MARKETING_LANDING_PAGES to ETRADER_WEB_CONNECT;
create public synonym MARKETING_LANDING_PAGES for ETRADER.MARKETING_LANDING_PAGES;

grant select, insert, update on ETRADER.MARKETING_LOCATIONS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_LOCATIONS for ETRADER.MARKETING_LOCATIONS;

grant select, insert, update on ETRADER.MARKETING_MEDIUMS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_MEDIUMS for ETRADER.MARKETING_MEDIUMS;

grant select, insert, update on ETRADER.MARKETING_SIZES to ETRADER_WEB_CONNECT;
create public synonym MARKETING_SIZES for ETRADER.MARKETING_SIZES;

grant select, insert, update on ETRADER.MARKETING_SOURCES to ETRADER_WEB_CONNECT;
create public synonym MARKETING_SOURCES for ETRADER.MARKETING_SOURCES;

grant select, insert, update on ETRADER.MARKETING_TYPES to ETRADER_WEB_CONNECT;
create public synonym MARKETING_TYPES for ETRADER.MARKETING_TYPES;

grant select, insert, update on ETRADER.MARKETING_COMBINATIONS to ETRADER_WEB_CONNECT;
create public synonym MARKETING_COMBINATIONS for ETRADER.MARKETING_COMBINATIONS;

create public synonym SEQ_MAR_CAMPAIGNS for ETRADER.SEQ_MAR_CAMPAIGNS;
create public synonym SEQ_MAR_CONTENTS for ETRADER.SEQ_MAR_CONTENTS;
create public synonym SEQ_MAR_LANDING_PAGES for ETRADER.SEQ_MAR_LANDING_PAGES;
create public synonym SEQ_MAR_LOCATIONS for ETRADER.SEQ_MAR_LOCATIONS;
create public synonym SEQ_MAR_MEDIUMS for ETRADER.SEQ_MAR_MEDIUMS;
create public synonym SEQ_MAR_PAYMENT_RECIPIENTS for ETRADER.SEQ_MAR_PAYMENT_RECIPIENTS;
create public synonym SEQ_MAR_PAYMENT_TYPES for ETRADER.SEQ_MAR_PAYMENT_TYPES;
create public synonym SEQ_MAR_SIZES for ETRADER.SEQ_MAR_SIZES;
create public synonym SEQ_MAR_SOURCES for ETRADER.SEQ_MAR_SOURCES;
create public synonym SEQ_MAR_TYPES for ETRADER.SEQ_MAR_TYPES;
create public synonym SEQ_MAR_COMBINATIONS for ETRADER.SEQ_MAR_COMBINATIONS;

-- 3D Secure  done!

grant select, insert, update on ETRADER.CREDIT_CARD_3D_SECURE to ETRADER_WEB_CONNECT;
create public synonym CREDIT_CARD_3D_SECURE for ETRADER.CREDIT_CARD_3D_SECURE;
create public synonym SEQ_CREDIT_CARD_3D_SECURE for ETRADER.SEQ_CREDIT_CARD_3D_SECURE;

-- Clearing country routing   done!

grant select, insert, update on ETRADER.CLEARING_COUNTRY_ROUTES to ETRADER_WEB_CONNECT;
create public synonym CLEARING_COUNTRY_ROUTES for ETRADER.CLEARING_COUNTRY_ROUTES;
create public synonym SEQ_CLEARING_COUNTRY_ROUTES for ETRADER.SEQ_CLEARING_COUNTRY_ROUTES;


-- 19/03/09 Kobi marketing done!
grant select, insert, update on ETRADER.AD_COMBINATION_MAP to ETRADER_WEB_CONNECT;
create public synonym AD_COMBINATION_MAP for ETRADER.AD_COMBINATION_MAP;

-- 30/03/09 Eliran 30/03/09 done!
grant select, insert, update on ETRADER.issue_reached_statuses to ETRADER_WEB_CONNECT;
create public synonym issue_reached_statuses for ETRADER.issue_reached_statuses;

grant select, insert, update on ETRADER.issue_reactions to ETRADER_WEB_CONNECT;
create public synonym issue_reactions for ETRADER.issue_reactions;

