create or replace package pkg_deposit is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-06-05 10:41:22
  -- Purpose : 

  procedure update_first_deposit_id
  (
    i_transaction_id in number
   ,i_user_id        in number
   ,i_new_status_id  in number
   ,i_old_status_id  in number
   ,i_type_id        in number
   ,i_updating       boolean default false
   ,i_inserting      boolean default false
  );

  procedure notify_user_fdi
  (
    i_user_id        in number
   ,i_transaction_id in number
  );

  procedure user_fdi_callback
  (
    context  in raw
   ,reginfo  in sys.aq$_reg_info
   ,descr    in sys.aq$_descriptor
   ,payload  in varchar2
   ,payloadl in number
  );
end pkg_deposit;
/
create or replace package body pkg_deposit is

  procedure find_new_fdi
  (
    i_user_id        in number
   ,i_transaction_id in number
  ) is
  begin
  
    update users u
    set    u.first_deposit_id =
           (select min(t.id)
            from   transactions t
            where  t.user_id = i_user_id
            and    exists (select 1
                    from   transaction_types tt
                    where  tt.class_type = 1
                    and    tt.id = t.type_id)
            and    t.status_id in (2, 7, 8))
    where  u.id = i_user_id;
  
  end find_new_fdi;

  procedure notify_user_fdi
  (
    i_user_id        in number
   ,i_transaction_id in number
  ) is
    l_enq_options    dbms_aq.enqueue_options_t;
    l_msg_properites dbms_aq.message_properties_t;
    l_msgid          raw(16);
  begin
    -- activate asynchronously
    l_msg_properites.delay       := dbms_aq.no_delay;
    l_msg_properites.correlation := to_char(i_user_id);
  
    dbms_aq.enqueue(queue_name         => 'ETRADER.Q_FIRST_DEPOSIT'
                   ,enqueue_options    => l_enq_options
                   ,message_properties => l_msg_properites
                   ,payload            => t_first_deposit(i_user_id, i_transaction_id, null)
                   ,msgid              => l_msgid);
  end notify_user_fdi;

  procedure update_first_deposit_id
  (
    i_transaction_id in number
   ,i_user_id        in number
   ,i_new_status_id  in number
   ,i_old_status_id  in number
   ,i_type_id        in number
   ,i_updating       boolean default false
   ,i_inserting      boolean default false
  ) is
  begin
    if (inserting or i_inserting)
       and i_new_status_id in (2, 7)
    then
    
      update users u
      set    u.first_deposit_id = i_transaction_id
      where  u.id = i_user_id
      and    u.first_deposit_id is null
      and    exists (select 1
              from   transaction_types tt
              where  tt.class_type = 1
              and    tt.id = i_type_id);
    
    elsif (updating or i_updating)
    then
    
      if i_new_status_id in (2, 7, 8)
         and i_old_status_id not in (2, 7, 8)
      then
      
        update users u
        set    u.first_deposit_id = i_transaction_id
        where  u.id = i_user_id
        and    (u.first_deposit_id is null or u.first_deposit_id > i_transaction_id)
        and    exists (select 1
                from   transaction_types tt
                where  tt.class_type = 1
                and    tt.id = i_type_id);
      
      elsif i_new_status_id not in (2, 7, 8)
            and i_old_status_id in (2, 7, 8)
      then
        for i in (select 1
                  from   users u
                  where  u.id = i_user_id
                  and    u.first_deposit_id = i_transaction_id)
        loop
          notify_user_fdi(i_user_id, i_transaction_id);
        end loop;
      end if;
    
    end if;
  end update_first_deposit_id;

  procedure user_fdi_callback
  (
    context  in raw
   ,reginfo  in sys.aq$_reg_info
   ,descr    in sys.aq$_descriptor
   ,payload  in varchar2
   ,payloadl in number
  ) is
    l_deq_options    dbms_aq.dequeue_options_t;
    l_msg_properties dbms_aq.message_properties_t;
    l_msgid          raw(16);
    l_message        t_first_deposit;
  
    e_nomsg exception;
    pragma exception_init(e_nomsg, -25228);
    e_nomsgid exception;
    pragma exception_init(e_nomsgid, -25263);
  begin
    l_deq_options.msgid := descr.msg_id;
    l_deq_options.wait  := dbms_aq.no_wait;
  
    loop
      dbms_aq.dequeue(queue_name         => descr.queue_name
                     ,dequeue_options    => l_deq_options
                     ,message_properties => l_msg_properties
                     ,payload            => l_message
                     ,msgid              => l_msgid);
    
      find_new_fdi(l_message.user_id, l_message.transaction_id);
      commit;
    
      l_deq_options.msgid := null;
    end loop;
  exception
    when e_nomsg then
      null;
    when e_nomsgid then
      null;
    when others then
      raise;
  end user_fdi_callback;

end pkg_deposit;
/
