create or replace package pkg_risk is

  procedure get_chargeback_providers
  (
    o_providers               out sys_refcursor
  );
  
  procedure get_all_reason_codes
  (
   o_reason_codes             out sys_refcursor
  );
  
  procedure get_chargebacks
  (
    o_chargebacks             out sys_refcursor
    ,i_from_date              in date
    ,i_to_date                in date
    ,i_transaction_id         in number
    ,i_user_id                in number
    ,i_arn                    in varchar2
    ,i_last_four_digits       in varchar2
    ,i_user_classes           in number
    ,i_business_cases         in number_table
    ,i_skins                  in number_table
    ,i_countries              in number_table  
    ,i_currencies             in number_table
    ,i_statuses               in number_table
    ,i_providers              in number_table
    ,i_rc                     in number_table
    ,i_page                   in number
    ,i_rows_per_page          in number
    ,i_other                  in number
  );
  
  procedure get_chargeback_details
  (
    o_chargeback_details            out sys_refcursor
    ,i_trans_id                     in number
  );
  
  procedure insert_chargeback
  (
    i_transaction_id          in number
		, i_issue_id              in number
		, i_chb_date              in date
		, i_status                in number
		, i_case_id               in varchar2
		, i_arn                   in varchar2
		, i_rc                    in number
		, i_rc_new                in number
		, i_reason_code_new       in varchar2
		, i_dispute_date          in date
		, i_comments              in varchar2
		, i_writer_id             in number
		, i_state                 in number
  );
  
  procedure update_chargeback
  (
    i_id                    in number
		,i_updated_by           in number
		,i_chb_date             in number
		,i_status               in number
		,i_case_id              in varchar2
		,i_arn                  in varchar2
		,i_rc                   in number
		,i_rc_new               in number
		,i_reason_code_new      in varchar2
		,i_dispute_date         in number
		,i_comments             in varchar2
  );
  
end pkg_risk;
/
create or replace package body pkg_risk is

  procedure get_chargeback_providers
  (
    o_providers               out sys_refcursor
  ) is

  begin

    open o_providers for
      select p.id
            ,p.name
        from clearing_providers p
       where p.is_active = 1
         and p.cc_available = 1
          or p.id = 34
       order by upper(p.name);

  end get_chargeback_providers;

  procedure get_all_reason_codes
  (
    o_reason_codes            out sys_refcursor
  ) is

  begin

    open o_reason_codes for
      SELECT rc
            ,REASON_CODE
            ,type
        FROM chargeback_reason_codes
       where type in (1, 2)
       order by type
               ,REASON_CODE
               ,RC;

  end get_all_reason_codes;

    procedure get_chargebacks
    (
        o_chargebacks           out sys_refcursor
        ,i_from_date            in date
        ,i_to_date              in date
        ,i_transaction_id       in number
        ,i_user_id              in number
        ,i_arn                  in varchar2
        ,i_last_four_digits     in varchar2
        ,i_user_classes         in number
        ,i_business_cases       in number_table
        ,i_skins                in number_table
        ,i_countries            in number_table
        ,i_currencies           in number_table
        ,i_statuses             in number_table
        ,i_providers            in number_table
        ,i_rc                   in number_table
        ,i_page                 in number
        ,i_rows_per_page        in number
        ,i_other                in number
  ) is

    begin

    open o_chargebacks for
    select 
        *
    from 
        (select
            v.*
            ,rownum rn
        from
            (select
                count(*) over() total_count,
                cb.id,
                t.user_id,
                cbs.name status,
                t.amount amount,
                t.id transaction_id,
                u.currency_id,
                cb.time_chb date_chb,
                cb.case_id,
                cb.rc reason_code,
                w.user_name updated_by_writer,
                cb.dispute_deadline,
                cp.name provider_name,
                t.is_3d
            from
                transactions t
                join charge_backs cb on t.charge_back_id = cb.id
                join charge_back_statuses cbs on cbs.id = cb.status_id
                join users u on t.user_id = u.id
                join skins s on u.skin_id = s.id
                left join writers w on cb.updated_by = w.id
                left join clearing_providers cp on t.clearing_provider_id = cp.id
                left join CHARGEBACK_REASON_CODES crc on cb.rc = crc.rc
                left join credit_cards cc on t.CREDIT_CARD_ID = cc.id
            where
                t.charge_back_id is not null
                and cb.transaction_id is not null
                and cb.TIME_CHB >= i_from_date
                and cb.TIME_CHB < i_to_date
                and (i_transaction_id is null or t.id = i_transaction_id)
                and (i_user_id is null or u.id = i_user_id)
                and (i_arn is null or cb.arn = i_arn)
                and (i_last_four_digits is null or cc.CC_NUMBER_LAST_4_DIGITS = i_last_four_digits)
                and (i_user_classes is null
                    or ((i_user_classes =  12) and (u.class_id in (1, 2)))
                    or ((i_user_classes <> 12) and (u.class_id = i_user_classes))
                    )
                and (i_business_cases is null or s.business_case_id member of i_business_cases)
                and (u.skin_id member of i_skins)
                and (i_countries is null or u.country_id member of i_countries)
                and (i_currencies is null or u.currency_id member of i_currencies)
                and (i_statuses is null or cb.status_id member of i_statuses)
                and (i_providers is null or t.clearing_provider_id member of i_providers)
                and (   (i_rc is null and i_other = 0)
                    or (i_rc is null and i_other = 1 and crc.type = 3)
                    or (i_rc is not null and i_other = 0 and cb.rc member of i_rc)
                    or (i_rc is not null and i_other = 1 and cb.rc member of i_rc and crc.type = 3)
                    )
            order by cb.time_created desc
            ) v
        where
            rownum <= i_page * i_rows_per_page
        )
    where 
        rn > (i_page - 1) * i_rows_per_page;
    end get_chargebacks;

	procedure get_chargeback_details
	(
		o_chargeback_details		out sys_refcursor
		,i_trans_id					in number
	) is

		begin
		open o_chargeback_details for
		select
				cb.id
				,u.user_name
				,u.first_name
				,u.last_name
				,u.id user_id
				,t.id transaction_id
				,u.currency_id
				,t.amount
				,u.affiliate_key
				,w1.user_name account_manager
				,t.amount*t.rate amount_usd
        ,t.type_id transaction_type_id
				,case
					when t.type_id = 32 then 'Skrill'
					else cp.name
				end as clearing_provider
				,t.charge_back_id chargeback_id
				,cb.issue_id
				,cb.time_created
        ,cb.time_chb
				,cb.updated_at time_updated
        ,cb.status_id
				,w.user_name updating_writer
				,cct.description cc_type_by_bin
        ,cc.type_id_by_bin cc_type_id_by_bin
				,cct.name cc_name
        ,cb.case_id
        ,cb.arn
        ,cb.rc
        ,crc.rc new_rc
        ,crc.reason_code
        ,cb.dispute_deadline
        ,cb.description comments
        ,t.is_3d
		from transactions t
		join users u on t.user_id = u.id
		left join charge_backs cb on cb.transaction_id = t.id
    left join chargeback_reason_codes crc on crc.rc = cb.rc
		left join writers w on w.id = cb.updated_by
		left join population_users pu on pu.user_id = u.id
		left join writers w1 on w1.id = pu.CURR_ASSIGNED_WRITER_ID
		left join clearing_providers cp on cp.id = t.clearing_provider_id
		left join credit_cards cc on t.credit_card_id = cc.id
		left join credit_card_types cct on cct.id = cc.type_id
		where
			t.id = i_trans_id;
	end get_chargeback_details;

  procedure insert_chargeback
  (
     i_transaction_id         in number
    ,i_issue_id               in number
    ,i_chb_date               in date
    ,i_status                 in number
    ,i_case_id                in varchar2
    ,i_arn                    in varchar2
    ,i_rc                     in number
    ,i_rc_new                 in number
    ,i_reason_code_new        in varchar2
    ,i_dispute_date           in date
    ,i_comments               in varchar2
    ,i_writer_id              in number
    ,i_state                  in number
  ) is

  l_rc number := 0;

  begin
    if i_rc_new is not null then
      l_rc := i_rc_new;
    else
      l_rc := i_rc;
    end if;

    insert into charge_backs
    (  id
      ,writer_id
      ,status_id
      ,time_created
      ,description
      ,issue_id
      ,arn
      ,state
      ,transaction_id
      ,time_chb
      ,rc
      ,dispute_deadline
      ,case_id
    )
    values
    (  SEQ_CHARGE_BACKS.nextval
      ,i_writer_id
      ,i_status
      ,sysdate
      ,i_comments
      ,i_issue_id
      ,i_arn
      ,i_state
      ,i_transaction_id
      ,i_chb_date
      ,l_rc
      ,i_dispute_date
      ,i_case_id
    );

    if (i_rc_new is not null and i_reason_code_new is not null) then
      insert into CHARGEBACK_REASON_CODES
      (  id
        ,RC
        ,REASON_CODE
        ,TYPE
      )
      values
      (  SEQ_CHARGEBACK_REASON_CODES.nextval
        ,i_rc_new
        ,i_reason_code_new
        ,3
      );
    end if;
  end insert_chargeback;

  procedure update_chargeback
  (
     i_id                     in number
    ,i_updated_by             in number
    ,i_chb_date               in number
    ,i_status                 in number
    ,i_case_id                in varchar2
    ,i_arn                    in varchar2
    ,i_rc                     in number
    ,i_rc_new                 in number
    ,i_reason_code_new        in varchar2
    ,i_dispute_date           in number
    ,i_comments               in varchar2
  ) is

  r_chb_date DATE := DATE '1970-01-01' + ( 1 / 24 / 60 / 60 / 1000) * i_chb_date;
  r_dispute_date DATE := DATE '1970-01-01' + ( 1 / 24 / 60 / 60 / 1000) * i_dispute_date;
  l_rc number := 0;

  begin
      if i_rc_new is not null then
        l_rc := i_rc_new;
      else
        l_rc := i_rc;
      end if;

      update charge_backs
         set updated_by = i_updated_by
            ,updated_at = sysdate
            ,time_chb = r_chb_date
            ,status_id = i_status
            ,case_id = i_case_id
            ,arn = i_arn
            ,rc = l_rc
            ,dispute_deadline =  r_dispute_date
            ,description = i_comments
      where id = i_id;

      if (i_rc_new is not null and i_reason_code_new is not null) then
          insert into CHARGEBACK_REASON_CODES
          (  id
            ,RC
            ,REASON_CODE
            ,TYPE
          )
          values
          (  SEQ_CHARGEBACK_REASON_CODES.nextval
            ,i_rc_new
            ,i_reason_code_new
            ,3
          );
      end if;

  end update_chargeback;
end pkg_risk;
/