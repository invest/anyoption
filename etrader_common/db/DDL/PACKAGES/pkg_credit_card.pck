create or replace package pkg_credit_card is

  -- Author  : PAVEL.TABAKOV
  -- Created : 23-Aug-16 09:54:22
  -- Purpose : CreditCardsDAO

  procedure get_cc_holder_name_hist
  (
    o_cc_holder_name_hist out sys_refcursor
   ,i_user_id             in number
   ,i_cc_number           in varchar2
  );
  
  PROCEDURE UPDATE_CC_DOCUMENT_SENT(O_ROWS_UPDATED OUT NUMBER,
                                    I_USER_ID      IN NUMBER,
                                    I_FILE_TYPE_ID IN NUMBER,
                                    I_CC_ID        IN NUMBER);

end pkg_credit_card;
/
CREATE OR REPLACE PACKAGE BODY PKG_CREDIT_CARD IS

  PROCEDURE GET_CC_HOLDER_NAME_HIST(O_CC_HOLDER_NAME_HIST OUT SYS_REFCURSOR,
                                    I_USER_ID             IN NUMBER,
                                    I_CC_NUMBER           IN VARCHAR2) IS
  BEGIN
    OPEN O_CC_HOLDER_NAME_HIST FOR
      SELECT *
        FROM (SELECT CHH.HOLDER_NAME, CHH.TIME_CREATED, CHH.OPERATION_ID
                FROM CARD_HOLDER_HISTORY CHH
               WHERE CHH.USER_ID = I_USER_ID
                 AND CHH.CC_NUMBER = I_CC_NUMBER
                 AND CHH.OPERATION_ID = 1
               ORDER BY CHH.TIME_CREATED DESC)
       WHERE ROWNUM <= 3;
  
  END GET_CC_HOLDER_NAME_HIST;

  PROCEDURE UPDATE_CC_DOCUMENT_SENT(O_ROWS_UPDATED OUT NUMBER,
                                    I_USER_ID      IN NUMBER,
                                    I_FILE_TYPE_ID IN NUMBER,
                                    I_CC_ID        IN NUMBER) IS
  
  BEGIN
  
    UPDATE CREDIT_CARDS CCC
       SET CCC.IS_DOCUMENTS_SENT = 1
     WHERE CCC.ID = I_CC_ID
       AND CCC.USER_ID IN
           (SELECT DISTINCT F.USER_ID
              FROM FILES F
             WHERE F.CC_ID = I_CC_ID
               AND F.FILE_TYPE_ID = I_FILE_TYPE_ID
               AND F.IS_APPROVED = 1
               AND F.USER_ID = I_USER_ID
               AND F.IS_CURRENT = 1
               AND F.FILE_STATUS_ID NOT IN (4, 5));
    O_ROWS_UPDATED := SQL%ROWCOUNT; -- get number of rows updated
    --COMMIT; not a good practice, Client(Server) is responsible for commiting transaction
  
  END UPDATE_CC_DOCUMENT_SENT;

END PKG_CREDIT_CARD;
/
