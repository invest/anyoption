create or replace package pkg_pending_withdrawals is

  procedure get_pending_withdrawals
  (
    o_pending_withdraws out sys_refcursor
   ,i_from_date           in number
   ,i_to_date             in number
   ,i_transaction_id      in number
   ,i_user_id             in number
   ,i_amount_from         in number
   ,i_amount_to           in number
   ,i_business_cases      in number_table
   ,i_skins               in number_table
   ,i_countries           in number_table
   ,i_user_classes        in number
   ,i_currencies          in number_table
   ,i_withdraw_types      in number_table
   ,i_invested            in number
   ,i_past_withdraws      in number
   ,i_flag_subject        in number_table
   ,i_status              in number
   ,i_page_number         in number
   ,i_rows_per_page       in number
   ,i_screen              in number
   ,i_flag_accounting_ok  in number default null
  );

  procedure get_details
  (
    o_data          out sys_refcursor
   ,i_transation_id in number
  );
  
  procedure get_details_user
  (
   o_data_user     out sys_refcursor
   ,i_transation_id in number
  );
  
  procedure get_details_deposit
  (
   o_data_deposit  out sys_refcursor
   ,i_transation_id in number
   ,i_user_id IN NUMBER DEFAULT 0
  );
  
  procedure get_flag_approve_change
  (
    o_data                  out sys_refcursor
   ,i_withdrawal_changes_id in number
  );
  
  procedure update_flag_approve_change
  (
     i_transaction_id           in number
    ,i_flag_change_date         in date
    ,i_flag_writer_id           in number
    ,i_approve_change_date      in date
    ,i_approve_writer_id        in number
    ,i_flag                     in number
    ,i_flag_subject_id          in number
	,i_flag_accounting_ok       in number default 0
	,i_accounting_ok_writer_id  in number default null
	,i_accounting_ok_date       in date default null
  );

  procedure update_traders_reviewed
  (
   i_traders_reviewed_flag     in number
  ,i_writer_id                 in number
  ,i_user_id                   in number
  );

  procedure get_flag_subjects
  (
    o_flag_subjects            out sys_refcursor
  );
end pkg_pending_withdrawals;
/
create or replace package body pkg_pending_withdrawals is

  procedure get_pending_withdrawals
  (
    o_pending_withdraws out sys_refcursor
   ,i_from_date           in number
   ,i_to_date             in number
   ,i_transaction_id      in number
   ,i_user_id             in number
   ,i_amount_from         in number
   ,i_amount_to           in number
   ,i_business_cases      in number_table
   ,i_skins               in number_table
   ,i_countries           in number_table
   ,i_user_classes        in number
   ,i_currencies          in number_table
   ,i_withdraw_types      in number_table
   ,i_invested            in number
   ,i_past_withdraws      in number
   ,i_flag_subject        in number_table
   ,i_status              in number
   ,i_page_number         in number
   ,i_rows_per_page       in number
   ,i_screen              in number
   ,i_flag_accounting_ok  in number default null
  ) is

  r_from_date DATE := DATE '1970-01-01' + ( 1 / 24 / 60 / 60 / 1000) * i_from_date;
  r_to_date DATE := DATE '1970-01-01' + ( 1 / 24 / 60 / 60 / 1000) * i_to_date;

  begin
    open o_pending_withdraws for
      select *
      from   (select v.*
                    ,case
                       when inv_count > 0 then
                        1
                       else
                        0
                     end as invested
                    ,case
                       when wc > 0 then
                        1
                       else
                        0
                     end as past_withdraws
                    ,row_number() over(order by time_created asc, transactionid asc) rn
                    ,count(*) over() total_count
              from   (select
                            t.id transactionid
                                          ,nvl(pwc.id, 0) transaction_flagged
                                          ,wfs.name flag_subject_name
                                          ,t.status_id
                                          ,t.type_id
                            ,t.clearing_provider_id clearing_provider              
                            ,u.id userid
                            ,u.user_name
                            ,u.currency_id
                            ,s.name skinname
                            ,c.country_name
                            ,t.time_created
                            ,tt.description || '.clean' transactiontype
                            ,t.amount
                            ,t.amount * t.rate base_amount
                            ,pwc.flag_accounting_ok
                            ,(select count(*)
                              from   investments
                              where  is_canceled = 0
                              and    user_id = u.id) inv_count
                            ,(select count(*) suc_with_count
                              from   transactions t
                              where  t.type_id in (5, 3, 10, 14, 35, 36, 37, 39, 45, 59, 60, 61, 19)
                              and    t.status_id in (2)
                              and    t.user_id = u.id) wc
                      from   transactions t
                      join   users u
                      on     t.user_id = u.id
                      join   skins s
                      on     u.skin_id = s.id
                      join   countries c
                      on     u.country_id = c.id
                      join   transaction_types tt
                      on     t.type_id = tt.id
                      left   join pending_withdrawal_changes pwc
                      on     pwc.id = t.withdrawal_changes_id
                                  left   join withdrawal_flag_subjects wfs
                                  on     wfs.id = pwc.flag_subject_id
                      where  t.status_id in (9, 4)
                      and    t.time_created between sysdate - 35 and sysdate
                      and    t.time_created >= r_from_date
                      and    t.time_created < r_to_date
                      and    (i_transaction_id is null or t.id = i_transaction_id)
                      and    (i_user_id is null or u.id = i_user_id)
                      and    (i_amount_from is null or t.amount * t.rate >= i_amount_from)
                      and    (i_amount_to is null or t.amount * t.rate <= i_amount_to)
                      and    (i_business_cases is null or s.business_case_id member of i_business_cases)
                      and    (u.skin_id member of i_skins)
                      and    (i_countries is null or u.country_id member of i_countries)
                      and    (i_user_classes is null or (i_user_classes = 12 and u.class_id in (1, 2)) or
                            (i_user_classes <> 12 and u.class_id = i_user_classes))
                      and    (i_currencies is null or u.currency_id member of i_currencies)
                      and    (i_withdraw_types is null or t.type_id member of i_withdraw_types)
                      and    (i_screen = 1 and T.STATUS_ID = 4 and not exists (select 1 from pending_withdrawal_changes where t.withdrawal_changes_id = id and withdraw_support_flag = 1) or
                                         (i_screen = 2 and T.STATUS_ID = 9 and not exists (select 1 from pending_withdrawal_changes where t.withdrawal_changes_id = id and withdraw_support_flag = 1)) or
                                         (i_screen = 3 and
                                                  exists (select 1 from pending_withdrawal_changes where
                                                                t.withdrawal_changes_id = id and withdraw_support_flag = 1
                                                                and (i_flag_subject is null or flag_subject_id member of i_flag_subject))
                                                   and (i_status is null or t.status_id = i_status)))
                     and     (i_flag_accounting_ok is null or pwc.flag_accounting_ok = i_flag_accounting_ok)
                  ) v
							where  (i_invested is null or (i_invested = 0 and inv_count = 0) or (i_invested = 1 and inv_count > 0))
							and    (i_past_withdraws is null or (i_past_withdraws = 0 and wc = 0) or (i_past_withdraws = 1 and wc > 0))
              order  by v.time_created asc, v.transactionid asc
							)
			where  rn > (i_page_number - 1) * i_rows_per_page
			and    rn <= i_page_number * i_rows_per_page;

	end get_pending_withdrawals;

   procedure get_details
  (
    o_data          out sys_refcursor
   ,i_transation_id in number
  ) is
  begin
    open o_data for
      select u.user_name
            ,u.currency_id
            ,u.is_active
            ,u.first_name
            ,u.last_name
            ,u.street
            ,u.street_no
            ,u.zip_code
            ,u.city_name
            ,c.country_name
                  ,c.id country_id
            ,u.mobile_phone
            ,u.land_line_phone
            ,cc.cc_number_last_4_digits
            ,cc.bin
            ,wi.beneficiary_name
            ,wi.swift
            ,wi.iban
            ,wi.account_num
            ,wi.bank_name
            ,wi.branch_address
            ,wi.branch branch_number
            ,t.moneybookers_email skrill_account
            ,t.paypal_email
            ,wr.user_name src
            ,w1.user_name first_approve_writer
            ,pwc.approve_change first_approve_time
            ,t.amount
            ,t.amount * t.rate amount_in_usd
            ,t.clearing_provider_id
            ,pwc.flag_change flag_date
            ,w2.user_name flag_writer
                  ,pwc.withdraw_support_flag flag
            ,w3.user_name accounting_ok_writer
            ,w3.id accounting_ok_writer_id
            ,pwc.flag_subject_id
            ,pwc.flag_accounting_ok
            ,pwc.accounting_ok_date
            ,t.comments
                  ,t.*
                  ,nvl(t.fee_cancel, 2) fee_cancel
                  ,nvl(ch.fee_cancel, 2) cheque_fee_cancel
      from   transactions t
      join   users u
      on     u.id = t.user_id
      join   skins s
      on     s.id = u.skin_id
      join   countries c
      on     c.id = u.country_id
      join   writers wr
      on     wr.id = t.writer_id
      left   join credit_cards cc
      on     cc.id = t.credit_card_id
      left   join wires wi
      on     wi.id = t.wire_id
      left   join pending_withdrawal_changes pwc
      on     pwc.id = t.withdrawal_changes_id
      left   join writers w1
      on     w1.id = pwc.approve_writer_id
      left   join writers w2
      on     w2.id = PWC.FLAG_WRITER_ID
      left   join writers w3
      on     w3.id = pwc.accounting_ok_writer_id
      left   join cheques ch
      on     ch.id = T.CHEQUE_ID
      where  t.id = i_transation_id;

  end get_details;
  
  procedure get_details_user
  (
   o_data_user     out sys_refcursor
   ,i_transation_id in number
  ) is
  begin
  open o_data_user for
      select
                  u.balance,
                  nvl(ur.user_id, 0) user_regulated,
            u.winlose_balance,
            case when ur.approved_regulation_step = 7 then 1 else 0 end control_approved,
            ur.control_approved_date,
            invc.investments_count,
            bonu.used_bonuses,
            bonr.received_bonuses,
            utils.GET_USER_BONUSES_BE_STATE(u.id) as b_state,
            case when bw.bonuses_waived > 0 then 1 else 0 end bonuses_waived,
                  uad.reviewed_by_traders,
                  uad.traders_review_date,
                  uad.traders_review_writer_id,
                  w.user_name traders_review_writer,
            wc.suc_with_count,
            wc.withdraw_amount,
                  ff.last_file,
                  ff.file_name,
                  nvl((SELECT 1
                      from files fid
                      where
                          fid.user_id = u.id
                          and fid.file_type_id in (2,9,21)
                          and (case when nvl(ur.user_id, 0) <> 0 then fid.is_approved_control else fid.is_approved end) = 1
                          and rownum = 1)
                  , 0) id_approve,
                  nvl((SELECT 1
                       from files fub
                       where
                          fub.user_id = u.id
                          and fub.file_type_id = 22
                          and (case when nvl(ur.user_id, 0) <> 0 then fub.is_approved_control else fub.is_approved end)= 1
                          and rownum = 1)
                  , 0) ub_approve,
                  case
                    when (SELECT 1
                             from files fcf
                             where
                                  fcf.user_id = u.id
                                  and fcf.file_type_id = 23
                                  and (case when nvl(ur.user_id, 0) <> 0 then fcf.is_approved_control else fcf.is_approved end) = 1
                                  and rownum = 1) = 1
                     and (SELECT 1
                          from files fcb
                          where
                              fcb.user_id = u.id
                              and fcb.file_type_id = 24
                              and (case when nvl(ur.user_id, 0) <> 0 then fcb.is_approved_control else fcb.is_approved end) = 1
                              and rownum = 1) = 1
                    then 1
                    else 0
                  end cc_approve,
                  case when (select
                              nvl(count(*), 0)
                        from
                              files fdcc
                        where
                              fdcc.user_id = u.id and fdcc.file_type_id in (23,24)) > 0 then 1 else 0 end display_cc,
            dc.deposit_amount,
            dc.deposit_amount_usd
            from transactions t
            join users u on t.user_id = u.id and t.id = i_transation_id
                  join users_active_data uad on uad.USER_ID = u.id
                  left join writers w on w.id = uad.traders_review_writer_id
            left join users_regulation ur on u.id = UR.user_id
            left join (select
                    count(*) investments_count,
                    user_id
                   from
                      investments
                   where
                      is_canceled = 0
                   group by user_id) invc on invc.user_id = u.id
            left join (select
                    count(*) suc_with_count,
                    sum(amount) withdraw_amount,
                    user_id
                   from
                      transactions t
                   where
                      t.type_id in (5, 3, 10, 14, 35, 36, 37, 39, 45, 59, 60, 61, 19)
                    and    t.status_id in (2)
                   group by user_id) wc on wc.user_id = u.id
            left join (select
                    sum(amount) deposit_amount,
                    ceil(sum(t.amount*t.rate)) deposit_amount_usd,
                    user_id
                   from
                      transactions t
                   where
                      t.type_id in (1,17,21,18,2,34,4,25,28,29,30,32,44,33,38,9,54,58)
                    and    t.status_id in (2,7)
                   group by user_id) dc on dc.user_id = u.id
            left join (select
                    count(*) bonuses_waived,
                    bu.user_id
                   from
                      bonus b,
                      bonus_users bu,
                      bonus_states bs
                   where
                      b.id=bu.bonus_id
                    and bs.id = bu.bonus_state_id
                    and bu.bonus_state_id = 9
                   group by bu.user_id) bw on bw.user_id = u.id
            left join (select
                    count(*) used_bonuses,
                    bu.user_id
                   from
                      bonus b,
                      bonus_users bu,
                      bonus_states bs
                   where
                      b.id=bu.bonus_id
                    and bs.id = bu.bonus_state_id
                    and bs.id in (3,4)
                   group by bu.user_id) bonu on bonu.user_id = u.id
            left join (select
                    count(*) received_bonuses,
                    bu.user_id
                   from
                      bonus b,
                      bonus_users bu,
                      bonus_states bs
                   where
                      b.id=bu.bonus_id
                    and bs.id = bu.bonus_state_id
                   group by bu.user_id) bonr on bonr.user_id = u.id
                  left join (select
                           *
                         from
                            (select
                                f.user_id,
                                f.time_created last_file,
                                f.file_name,
                                      row_number() over (partition by user_id order by time_created desc ) rn
                                   from
                                      files f
                                   where
                                      file_type_id = 13)
                             where rn = 1) ff on ff.user_id = u.id;

  end get_details_user;
      
  procedure get_details_deposit
  (
   o_data_deposit     out sys_refcursor
   ,i_transation_id in NUMBER
   ,i_user_id IN NUMBER DEFAULT 0
  ) is
  begin
  open o_data_deposit for
    SELECT tp.id type_id,
      tp.description,
      CASE
        WHEN tr.type_id = 1
        THEN TO_CHAR(cct.name)
        ELSE ''
      END AS card_details,
      cc.cc_number_last_4_digits,
      decode(grouping(cp.name), 1, null, nvl(cp.name, 'no provider')) name,
      COUNT(*)       AS COUNT,
      SUM(tr.amount) AS amount,
      sum(tr.amount-tr.credit_amount) AS amount_to_credit,
      u.currency_id,
      MAX(tr.time_created) AS max_date,
          MAX(nvl(CC.EXP_MONTH,'00')) exp_month,
          MAX(nvl(CC.EXP_YEAR,'0000')) exp_year
    FROM transactions tr,
      transaction_types tp,
      credit_cards cc,
      credit_card_types cct,
      users u,
      clearing_providers cp
    WHERE tr.user_id      = u.id
    AND tr.time_created   > u.time_created
    AND tr.type_id        = tp.id
    AND tp.class_type    IN (1,5)
    AND tr.status_id     IN (2,7)
    AND tr.credit_card_id = cc.id(+)
    AND cc.type_id        = cct.id(+)
    and cp.id(+) = tr.clearing_provider_id
    AND u.id              = CASE WHEN i_user_id > 0 THEN i_user_id ELSE (select user_id from transactions where id = i_transation_id) END
    GROUP BY
      rollup (tp.id,
      tp.description,
      CASE
        WHEN tr.type_id = 1
        THEN TO_CHAR(cct.name)
        ELSE ''
      END,
      u.currency_id,
      cc.cc_number_last_4_digits,
      cp.name
      )
    having (grouping(tp.id)=0
            and grouping(tp.description)=0
            and grouping(CASE WHEN tr.type_id = 1 THEN TO_CHAR(cct.name) ELSE '' END)=0
            and grouping(u.currency_id)=0
            and grouping(cc.CC_NUMBER_LAST_4_DIGITS)=0
            )
    order by 1, 2, 3, 4, 5 nulls first;

  end get_details_deposit;

  procedure get_flag_approve_change
  (
  o_data                  out sys_refcursor
  ,i_withdrawal_changes_id in number
  ) is
  begin
    open o_data for
      SELECT pwc.* FROM pending_withdrawal_changes pwc WHERE id = i_withdrawal_changes_id;
  end get_flag_approve_change;

  procedure update_flag_approve_change
  (
   i_transaction_id           in number
  ,i_flag_change_date         in date
  ,i_flag_writer_id           in number
  ,i_approve_change_date      in date
  ,i_approve_writer_id        in number
  ,i_flag                     in number
  ,i_flag_subject_id          in number
  ,i_flag_accounting_ok       in number default 0
  ,i_accounting_ok_writer_id  in number default null
  ,i_accounting_ok_date       in date default null
  ) is

  begin
    insert into pending_withdrawal_changes
      (id, transaction_id, flag_change, flag_writer_id, approve_change, approve_writer_id, withdraw_support_flag, flag_subject_id, flag_accounting_ok, accounting_ok_writer_id, accounting_ok_date)
    values
      (SEQ_PENDING_WITHDRAWAL_CHANGES.nextval, i_transaction_id, i_flag_change_date, i_flag_writer_id, i_approve_change_date, i_approve_writer_id, i_flag, i_flag_subject_id, i_flag_accounting_ok, i_accounting_ok_writer_id, i_accounting_ok_date);

    update transactions set withdrawal_changes_id = SEQ_PENDING_WITHDRAWAL_CHANGES.currval where id = i_transaction_id;

  end update_flag_approve_change;

  procedure update_traders_reviewed
  (
   i_traders_reviewed_flag     in number
  ,i_writer_id                 in number
  ,i_user_id                   in number
  ) is
  l_traders_flag               number;
  begin
    select uad.reviewed_by_traders into l_traders_flag from users_active_data uad where uad.user_id = i_user_id;
    if i_traders_reviewed_flag <> l_traders_flag
    then
      update users_active_data set reviewed_by_traders = i_traders_reviewed_flag, traders_review_date = sysdate, traders_review_writer_id = i_writer_id where user_id = i_user_id;
    end if;
  end update_traders_reviewed;

  procedure get_flag_subjects
  (
    o_flag_subjects            out sys_refcursor
  ) is

  begin
  open o_flag_subjects for
    select id, name from withdrawal_flag_subjects
    where id != 11;
  end get_flag_subjects;

end pkg_pending_withdrawals;