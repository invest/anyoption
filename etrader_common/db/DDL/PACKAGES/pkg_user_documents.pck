CREATE OR REPLACE PACKAGE PKG_USER_DOCUMENTS IS

  PROCEDURE GET_USER_DOCS_BY_ID
    (
     O_DATA                      OUT SYS_REFCURSOR
    ,I_USER_ID                   IN NUMBER
    );
    
  PROCEDURE UPLOAD_DOCS
  (
    O_FILE_ID                     OUT NUMBER
   ,I_USER_ID                     IN NUMBER
   ,I_FILE_TYPE_ID                IN NUMBER
   ,I_WRITER_ID                   IN NUMBER
   ,I_FILE_NAME                   IN VARCHAR2
   ,I_CC_ID                       IN NUMBER
  );
  
  FUNCTION GET_FILE_STATUS_ID 
   (
    I_FILE_NAME                  IN VARCHAR2
   ,I_IS_APPROVED                IN NUMBER
   ,I_IS_SUPPORT_REJECTED        IN NUMBER
   ) RETURN NUMBER;
  
  PROCEDURE SET_IS_CURRENT_DOCS
  (
    I_USER_ID                     IN NUMBER
   ,I_FILE_TYPE_ID                IN NUMBER
   ,I_CC_ID                       IN NUMBER
  ); 
  
    PROCEDURE SET_IS_CURRENT_DOCS_MANUAL
  (
    I_FILE_ID                      IN NUMBER
  ); 
  
  PROCEDURE GET_CALL_CENTER_SCREENS
  (
    O_DATA                        OUT SYS_REFCURSOR
   ,I_FTD_DATE_FROM               IN DATE 
   ,I_FTD_DATE_TO                 IN DATE   
   ,I_FE_DATE_FROM                IN DATE default null
   ,I_FE_DATE_TO                  IN DATE default null
   ,I_USER_ID                     IN NUMBER
   ,I_USERN_NAME                  IN VARCHAR2
   ,I_USER_CLASSES                IN NUMBER
   ,I_SKINS                       IN NUMBER_TABLE
   ,I_COUNTRIES                   IN NUMBER_TABLE
   ,I_IS_HAVE_PW                  IN NUMBER
   ,I_DAYS_LEFT                   IN NUMBER_TABLE
   ,I_IS_SPENDED                  IN NUMBER
   ,I_IS_USER_ACTIVE              IN NUMBER
   ,I_IS_COMPLIANCE_APPROVED      IN NUMBER
   ,I_COMPL_DOCUMENTS_UPLOADED    IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_DOCUMENTS_UPLOADED          IN NUMBER
   ,I_FROM_TOTAL_DEPOSIT          IN NUMBER
   ,I_TO_TOTAL_DEPOSIT            IN NUMBER
   ,I_CALL_ATTEMPS                IN NUMBER
   ,I_RATE                        IN NUMBER  
   ,I_ORDER_BY_ID                 IN NUMBER
   ,I_ASC_DESC_PARAM              IN NUMBER
   ,I_PAGE_NUMBER                 IN NUMBER 
   ,I_ROWS_PER_PAGE               IN NUMBER
   ,I_DOC_TYPE_1                  IN NUMBER default -1
   ,I_RESOLUTION_1                IN NUMBER_TABLE default null
   ,I_DOC_TYPE_2                  IN NUMBER default -1
   ,I_RESOLUTION_2                IN NUMBER_TABLE default null
   ,I_EXPIRE_DOC                  IN NUMBER default null
   ,I_BUSINESS_CASES              IN NUMBER_TABLE DEFAULT NULL
  );
  
  PROCEDURE GET_BACK_OFFICE_SCREENS
  (
    O_DATA                        OUT SYS_REFCURSOR
   ,I_FE_DATE_FROM                IN DATE default null
   ,I_FE_DATE_TO                  IN DATE default null
   ,I_USER_ID                     IN NUMBER
   ,I_USERN_NAME                  IN VARCHAR2
   ,I_USER_CLASSES                IN NUMBER
   ,I_SKINS                       IN NUMBER_TABLE
   ,I_COUNTRIES                   IN NUMBER_TABLE
   ,I_IS_HAVE_PW                  IN NUMBER
   ,I_DAYS_LEFT                   IN NUMBER_TABLE
   ,I_IS_SPENDED                  IN NUMBER
   ,I_IS_USER_ACTIVE              IN NUMBER
   ,I_IS_COMPLIANCE_APPROVED      IN NUMBER
   ,I_COMPL_DOCUMENTS_UPLOADED    IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_FROM_TOTAL_DEPOSIT          IN NUMBER
   ,I_TO_TOTAL_DEPOSIT            IN NUMBER
   ,I_RATE                        IN NUMBER  
   ,I_DOCUMENTS_TYPE              IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_ORDER_BY_ID                 IN NUMBER
   ,I_ASC_DESC_PARAM              IN NUMBER
   ,I_PAGE_NUMBER                 IN NUMBER
   ,I_ROWS_PER_PAGE               IN NUMBER
   ,I_DOC_TYPE_1                  IN NUMBER default -1
   ,I_RESOLUTION_1                IN NUMBER_TABLE default null
   ,I_DOC_TYPE_2                  IN NUMBER default -1
   ,I_RESOLUTION_2                IN NUMBER_TABLE default null
   ,I_BUSINESS_CASES              IN NUMBER_TABLE DEFAULT NULL
  );
  
    
PROCEDURE GET_USER_DATA_BY_ID
  (
     O_DATA                      OUT SYS_REFCURSOR
    ,O_FILE_DATA                 OUT SYS_REFCURSOR
    ,O_USER_FILE_COMMENTS        OUT SYS_REFCURSOR
    ,O_IS_ENABLE_REG_APPROVE     OUT NUMBER
    ,O_IS_ENABLE_REG_APPROVE_MAN OUT NUMBER
    ,I_USER_ID                   IN NUMBER
  );
  
  PROCEDURE GET_USER_DATA_BY_ID_TMP
  (
     O_DATA                      OUT SYS_REFCURSOR
    ,O_FILE_DATA                 OUT SYS_REFCURSOR
    ,O_USER_FILE_COMMENTS        OUT SYS_REFCURSOR
    ,O_IS_ENABLE_REG_APPROVE     OUT NUMBER
    ,O_IS_ENABLE_REG_APPROVE_MAN OUT NUMBER
    ,I_USER_ID                   IN NUMBER
  );
  
  PROCEDURE GET_USER_REG_FILE_STATE
  (
     O_POI_APPROVED             OUT NUMBER
    ,O_POR_APPROVED             OUT NUMBER
    ,O_APPROVED_REGULATION_STEP OUT NUMBER
    ,O_CNMV_APPROVED            OUT NUMBER
    ,I_USER_ID                  IN NUMBER
  );
  
  PROCEDURE IS_CAN_REGULATION_APPROVE
  (
   O_IS_CAN_APPROVE  OUT NUMBER
  ,I_USER_ID         IN NUMBER
  );
  
  PROCEDURE GET_DOCUMENT_STATUS
  (
   O_DOCUMENT_STATUS            OUT NUMBER
  ,I_USER_ID                    IN NUMBER
  );
  
PROCEDURE LOCK_USER_DOC
  (
   O_LOCKED_WRITER_NAME  OUT VARCHAR2
  ,I_SESSION_ID          IN VARCHAR2
  ,I_WRITER_ID           IN NUMBER
  ,I_USER_ID             IN NUMBER
  ,I_SERVER_ID           IN VARCHAR2
  ,I_SCREEN_ID			     IN NUMBER DEFAULT 2
  );
  
  PROCEDURE UNLOCK_USER_DOC
  (
   I_SESSION_ID          IN VARCHAR2
  ,I_WRITER_ID           IN NUMBER
  ,I_SERVER_ID           IN VARCHAR2
  ,I_SCREEN_ID           IN NUMBER DEFAULT 2
  );
  
  PROCEDURE MIGRATE_CURRNT_STATUS
  (
   I_FROM_ID           IN NUMBER
  ,I_TO_ID             IN NUMBER
  );
  
  PROCEDURE CALC_USERS_PENDING_DOC_CALL
  (
   I_USER_ID           IN NUMBER
  );
  
  PROCEDURE GET_USER_POP_DOCUMENTS
  (
    O_DATA    OUT SYS_REFCURSOR
   ,I_USER_ID IN NUMBER
  );
         
  PROCEDURE GET_DEFAULT_FILES
  (
      O_DEFAULT_FILES            OUT SYS_REFCURSOR
     ,I_MATCH_FROM               IN DATE
     ,I_MATCH_TO                 IN DATE
     ,I_USER_ID                  IN NUMBER
     ,I_USER_NAME                IN VARCHAR2
     ,I_USER_CLASS               IN NUMBER
     ,I_SKINS                    IN NUMBER_TABLE
     ,I_COUNTRIES                IN NUMBER_TABLE
     ,I_PAGE                     IN NUMBER
     ,I_ROWS_PER_PAGE            IN NUMBER
  );
  
END PKG_USER_DOCUMENTS;
/

CREATE OR REPLACE PACKAGE BODY PKG_USER_DOCUMENTS IS

ID_FILE_TYPE constant number := 1;
PASSPORT_FILE_TYPE constant number := 2;
UB_FILE_TYPE constant number := 22;
CC_FRONT_FILE_TYPE constant number := 23;
CC_BACK_FILE_TYPE constant number := 24;
GBG_FILE_TYPE constant number := 36;
DEFAULT_FILE_TYPE constant number := 37;

STATUS_REQUESTED constant number := 1;
STATUS_IN_PROGRESS constant number := 2;
STATUS_DONE constant number := 3;
STATUS_REJECT constant number := 4;

PROCEDURE GET_USER_DOCS_BY_ID
  (
    O_DATA    OUT SYS_REFCURSOR
   ,I_USER_ID IN NUMBER
  ) IS     

  BEGIN  
  --Get File LIST
    OPEN O_DATA FOR
      SELECT  f.id
             ,f.file_name 
             ,0 as hide_file
             ,f.is_approved
             ,cc.Id as cc_id
             ,cc.cc_number
             ,cct.name cc_type_name
             ,PKG_USER_DOCUMENTS.GET_FILE_STATUS_ID(f.file_name, f.is_approved, f.is_support_rejected) as status
             ,f.file_type_id
        FROM FILES f,             
             CREDIT_CARDS cc,
             CREDIT_CARD_TYPES cct,
             USERS_ACTIVE_DATA uad                          
       WHERE f.USER_ID = I_USER_ID    
             and f.file_type_id in (ID_FILE_TYPE,PASSPORT_FILE_TYPE,UB_FILE_TYPE,CC_FRONT_FILE_TYPE)
             and f.is_current = 1
             and f.user_id = uad.user_id
             -- Expire docs
             and  0 = (case when f.file_type_id in (1,2,22) 
                                  and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                      and (trunc(f.exp_date) - trunc(sysdate)) between 1 and 14 then 1
                                  when  f.file_type_id in (1,2,22) 
                                      and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                          and (trunc(f.exp_date) - trunc(sysdate)) <=0 then 2 
                                  else 0 end )   
             and f.cc_id = cc.id(+)
             and cc.type_id = cct.id(+)
       ORDER BY f.file_type_id, f.time_updated;

  END GET_USER_DOCS_BY_ID;
  
FUNCTION GET_FILE_STATUS_ID 
  (
    I_FILE_NAME           IN VARCHAR2
   ,I_IS_APPROVED         IN NUMBER
   ,I_IS_SUPPORT_REJECTED IN NUMBER
   )
RETURN NUMBER AS
 BEGIN
  --not uploded
   if nvl(I_FILE_NAME, 'x') = 'x' then
     return  STATUS_REQUESTED;
   end if;
   
   if nvl(I_IS_SUPPORT_REJECTED, 0)> 0 then
     return  STATUS_IN_PROGRESS;
   end if;    
  
     -- uploded  and (approved or not approved )
   if nvl(I_IS_APPROVED, 0) > 0 then
     return  STATUS_DONE;
   else 
       return  STATUS_IN_PROGRESS;
   end if;      
  return STATUS_REQUESTED;
 END;

PROCEDURE UPLOAD_DOCS
  (
    O_FILE_ID                     OUT NUMBER
   ,I_USER_ID                     IN NUMBER
   ,I_FILE_TYPE_ID                IN NUMBER
   ,I_WRITER_ID                   IN NUMBER
   ,I_FILE_NAME                   IN VARCHAR2
   ,I_CC_ID                       IN NUMBER
  ) is

cursor c_get_exist_cc_file is
  select id from files 
where 
  user_id = i_user_id
  and file_type_id = i_file_type_id
  and cc_id = i_cc_id
  and file_name is null;
v_file_id number;  
   BEGIN
     --Check exist created empty file and then update (only CC)
    v_file_id :=0;      
    if(i_file_type_id in (CC_FRONT_FILE_TYPE, CC_BACK_FILE_TYPE)) then
          open c_get_exist_cc_file;
          fetch c_get_exist_cc_file into v_file_id;
          close c_get_exist_cc_file;
          if(v_file_id > 0) then
           update files f
            set
              f.file_name = i_file_name,
              f.uploader_id = i_writer_id,
              f.time_uploaded = sysdate
            where f.id = v_file_id;           
          end if;
    end if; 
    --Create new file
    if(v_file_id = 0) then
       pkg_userfiles.create_file_return_id
       (
            o_file_id         => v_file_id
           ,i_user_id         => i_user_id
           ,i_file_type_id    => i_file_type_id
           ,i_writer_id       => i_writer_id
           ,i_file_name       => i_file_name
           ,i_file_status_id  => 2
           ,i_uploader_id     => i_writer_id
           ,i_cc_id           => i_cc_id
       );
    end if;
    o_file_id:= v_file_id;
    
    --Set is_current_flag
    SET_IS_CURRENT_DOCS(i_user_id, i_file_type_id, i_cc_id);
END UPLOAD_DOCS;
 
PROCEDURE SET_IS_CURRENT_DOCS
  (
   I_USER_ID                      IN NUMBER
   ,I_FILE_TYPE_ID                IN NUMBER
   ,I_CC_ID                       IN NUMBER
  ) IS

cursor c_get_current_file is
select f.id 
from 
  files f
where 
  user_id = i_user_id
  and f.file_type_id = i_file_type_id
  and f.file_type_id <> DEFAULT_FILE_TYPE
  and (i_cc_id is null or f.cc_id = i_cc_id)
order by 
nvl(time_uploaded, to_date('01-01-1970', 'dd-mm-yyyy')) desc,
time_created desc;

v_file_id number;  
   BEGIN
   open c_get_current_file;
   fetch c_get_current_file into v_file_id;
   if c_get_current_file%found then
       -- Set all = 0
     update files f 
     set   f.is_current = 0
     where f.user_id = i_user_id
           and f.file_type_id =  i_file_type_id     
           and (i_cc_id is null or f.cc_id = i_cc_id);
       -- Set current = 1
     update files f 
     set   f.is_current = 1
     where  f.id =  v_file_id;    
   end if;
   close c_get_current_file;      
  END SET_IS_CURRENT_DOCS; 
  
  PROCEDURE SET_IS_CURRENT_DOCS_MANUAL
  (
    I_FILE_ID                      IN NUMBER
  ) IS
   BEGIN
       -- Set manual is_current = 1
     update files f 
     set   f.is_current = decode(f.id,I_FILE_ID,1,0)
     where  f.id in (select fo.id 
                      from 
                          files f,
                          files fo 
                      where f.id = I_FILE_ID
                            and f.file_type_id =  fo.file_type_id
                            and f.user_id = fo.user_id
                            and length(fo.file_name)>0
                            and nvl(f.cc_id,0) = nvl(fo.cc_id,0)
                     );
     
  END SET_IS_CURRENT_DOCS_MANUAL; 
 ----BE----
  
PROCEDURE GET_SCREENS_DATA
  (
    O_DATA                        OUT SYS_REFCURSOR
   ,I_SCREEN_ID                   IN NUMBER
   ,I_FTD_DATE_FROM               IN DATE
   ,I_FTD_DATE_TO                 IN DATE
   ,I_FE_DATE_FROM                IN DATE
   ,I_FE_DATE_TO                  IN DATE
   ,I_USER_ID                     IN NUMBER
   ,I_USERN_NAME                  IN VARCHAR2
   ,I_USER_CLASSES                IN NUMBER
   ,I_SKINS                       IN NUMBER_TABLE
   ,I_COUNTRIES                   IN NUMBER_TABLE
   ,I_IS_HAVE_PW                  IN NUMBER
   ,I_DAYS_LEFT                   IN NUMBER_TABLE
   ,I_IS_SPENDED                  IN NUMBER
   ,I_IS_USER_ACTIVE              IN NUMBER
   ,I_IS_COMPLIANCE_APPROVED      IN NUMBER
   ,I_DOCUMENTS_UPLOADED          IN NUMBER
   ,I_FROM_TOTAL_DEPOSIT          IN NUMBER
   ,I_TO_TOTAL_DEPOSIT            IN NUMBER
   ,I_RATE                        IN NUMBER  
   ,I_CALL_ATTEMPS                IN NUMBER
   ,I_ORDER_BY_ID                 IN NUMBER
   ,I_ASC_DESC_PARAM              IN NUMBER
   ,I_PAGE_NUMBER                 IN NUMBER
   ,I_ROWS_PER_PAGE               IN NUMBER
   ,I_DOC_TYPE_1                  IN NUMBER
   ,I_RESOLUTION_1                IN NUMBER_TABLE
   ,I_DOC_TYPE_2                  IN NUMBER
   ,I_RESOLUTION_2                IN NUMBER_TABLE
   ,I_EXPIRE_DOC                  IN NUMBER
   ,I_BUSINESS_CASES             IN NUMBER_TABLE
  ) IS

  BEGIN 
   
 open o_data for
 SELECT * FROM
 ( 
   select
     rownum rn
     ,count(*) over() total_count 

     ,dt.* 
   from 
    (
     select 
            u.id as user_id
           ,u.first_name
           ,u.last_name
           ,c.country_name ||', '|| to_char(CAST((FROM_TZ(CAST(sysdate AS TIMESTAMP),'+00:00') AT TIME ZONE c.time_zone) AS DATE),'dd-mm-yyyy hh24:mi:ss') country_name
           ,c.gmt_offset
           ,s.name skin_name
           ,s.id skin_id
           ,decode(ur.approved_regulation_step, 7,1,0)  as is_regulation_approved
           ,case when (user_docs.poi_upload +  user_docs.POR_UPLOAD) = 0 then 0 
                 when (user_docs.poi_upload > 0 and  user_docs.POR_UPLOAD = 0) or (user_docs.poi_upload = 0 and  user_docs.POR_UPLOAD > 0)  then 1  
                 when  user_docs.poi_upload > 0 and user_docs.POR_UPLOAD  > 0 then 2
                 else 0 end as compl_docs_upload
            ,ceil(ua.sum_deposits*i_rate) as sum_deposits
            ,ur.days_left        
            ,nvl(updc.call_attempts,0) call_attemps
            ,updc.last_call_time
            ,updc.last_reached_time
     from 
             users u
            ,skins s
            ,countries c
            ,transactions t
            ,users_active_data ua
            ,users_pending_docs_call_info updc
            ,(select case when 0 > (14 - (trunc(sysdate) - trunc(rr.questionnaire_done_date))) then 0 else
                  14 - (trunc(sysdate) - trunc(rr.questionnaire_done_date)) end as days_left,  rr.*  from users_regulation rr) ur
           ,(
            select f.user_id,
                   --POI
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POI_UPLOAD,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 and f.expire_doc = 0 then 1 else 0 end) as POI_NO_RESOLUTION,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POI_REJECT,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_approved,0) = 1 and f.expire_doc = 0 then 1 else 0 end) as POI_APPROVE,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192)) then 1 else 0 end) as POI_DOCS,
                   sum(case when  ft.id = 38 and uf.country_id = 192  and f.file_name is not null and nvl(f.is_approved,0) = 1 then 1 else 0 end) as CNMV_APPROVE,
                   --POR
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POR_UPLOAD,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 and f.expire_doc = 0 then 1 else 0 end) as POR_NO_RESOLUTION,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POR_REJECT,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_approved,0) = 1 and f.expire_doc = 0 then 1 else 0 end) as POR_APPROVE,
                   sum(case when ft.file_group_id = 2 then 1 else 0 end) as POR_DOCS,
                   --POP
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POP_UPLOAD,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 then 1 else 0 end) as POP_NO_RESOLUTION,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POP_REJECT,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_approved,0) = 1 then 1 else 0 end) as POP_APPROVE,
                   sum(case when ft.file_group_id = 3  and f.file_name is null then 1 else 0 end) as POP_NEED_UPLOAD,
                   sum(case when ft.file_group_id = 3 then 1 else 0 end) as POP_DOCS,
                   --Expire docs.                   
                   sum(case when f.expire_doc = 1 then 1 else 0 end) as ABOUT_TO_EXPIRE,
                   sum(case when f.expire_doc = 2 then 1 else 0 end) as EXPIRED
            from
                   file_types ft, 
                   users uf,
                   (select f1.*
                           , case when f1.file_type_id in (1,2,22) 
                                  and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                      and (trunc(f1.exp_date) - trunc(sysdate)) between 1 and 14 then 1
                                  when  f1.file_type_id in (1,2,22) 
                                      and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                          and (trunc(f1.exp_date) - trunc(sysdate)) <=0 then 2 
                                  else 0 end as expire_doc   
                  from 
                    files f1,
                    users_active_data uad
                 where 
                    f1.user_id = uad.user_id 
                  )f  
            where 
                   (ft.file_group_id in (1,2,3) or ft.id = 38)  --38 CNMV File for Spain
                   and ft.id = f.file_type_id
                   and f.user_id = uf.id
                   and f.is_current = 1 
           group by 
                   f.user_id
           ) user_docs
     where 
           u.skin_id = s.id
           and u.country_id = c.id
           --and ((s.is_regulated = 1 and ur.approved_regulation_step >= 3) or s.is_regulated = 0)
           and ur.approved_regulation_step >= 3
           and s.id != 1
           and u.id = user_docs.user_id(+)
           and t.id = u.first_deposit_id
           and ua.user_id = u.id
           and u.id = ur.user_id
           and u.id = updc.user_id(+)
           ---
           and (i_screen_id = 
                 (case 
                 --user is REG and have file ONE of POI approved and missing POR
                  when (nvl(ur.approved_regulation_step,0) >= 3 
                                    and (nvl(user_docs.POI_APPROVE,0)> 0 and  (nvl(user_docs.POR_APPROVE,0) + nvl(user_docs.POR_NO_RESOLUTION,0) = 0)) 
                                        and nvl(ur.approved_regulation_step,0) < 7) then 1                                
                 --user is REG and have file ONE of POR approved and missing POI
                  when (nvl(ur.approved_regulation_step,0) >= 3 
                                    and (nvl(user_docs.POR_APPROVE,0)> 0 and  (nvl(user_docs.POI_APPROVE,0) + nvl(user_docs.POI_NO_RESOLUTION,0) = 0)) 
                                        and nvl(ur.approved_regulation_step,0) < 7) then 1                                                                        
                 --user is REG and have file POI or POR with NO_RESOLUITON
                  when (nvl(ur.approved_regulation_step,0) >= 3 
                                    and (nvl(user_docs.POI_NO_RESOLUTION,0) + nvl(user_docs.POR_NO_RESOLUTION,0)> 0) 
                                        and nvl(ur.approved_regulation_step,0) < 7) then 2                  
                 --user is REG and have resolution for POI and POR and not Final Approve for non Spain
                  when (nvl(ur.approved_regulation_step,0) >= 3 
                                    and nvl(user_docs.POI_APPROVE,0) > 0 and nvl(user_docs.POR_APPROVE,0)> 0 
                                        and nvl(ur.approved_regulation_step,0) < 7 and u.country_id != 192) then 2
                 --user is REG from Spain and have resolution for POI and POR and not Final Approve
                  when (nvl(ur.approved_regulation_step,0) >= 3 
                                    and nvl(user_docs.POI_APPROVE,0) > 0 and nvl(user_docs.POR_APPROVE,0)> 0 and nvl(user_docs.CNMV_APPROVE,0) > 0
                                        and nvl(ur.approved_regulation_step,0) < 7 and u.country_id = 192) then 2                                        
                  --After Approved and have Expire POI or POR docs and we have without resolution
                  when ((POI_APPROVE = 0 and POI_NO_RESOLUTION > 0) or (POR_APPROVE = 0 and POI_NO_RESOLUTION > 0) ) and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)  then  2                                        
                  --After Approved and have Expire POI or POR docs
                  when (POI_APPROVE = 0 or POR_APPROVE = 0 ) and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)  then  1                                        
                  --user  have NO resolution for POP
                  when (nvl(user_docs.POP_NO_RESOLUTION,0) > 0 and nvl(ur.approved_regulation_step,7) >= 7) then 2
                  --No files and quest. is not done
                  when (nvl(user_docs.user_id,0) = 0 and nvl(ur.approved_regulation_step,0) < 2) then  -1
                  --After Approved and have Expire POI or POR docs
                  when (POI_APPROVE = 0 or POR_APPROVE = 0 ) and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)  then  1
                 --After Approved and have additonal POI or POR docs. upload
                  when (nvl(user_docs.POP_NEED_UPLOAD,0) = 0 and nvl(user_docs.POP_REJECT,0) = 0 and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)) 
                    and (nvl(user_docs.POI_NO_RESOLUTION,0) + nvl(user_docs.POR_NO_RESOLUTION,0)> 0)  then  2                                        
                 -- APPROVED: After All docs are approved return 3 : 1.When not need upload POP and if regulated is step 7
                  when (nvl(user_docs.POP_NEED_UPLOAD,0) = 0 and nvl(user_docs.POP_REJECT,0) = 0 and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)) then  3
                 else 1 end
                 )
               )
            ---           
 -----FILTERS---------           
    and (i_ftd_date_from is null or (t.time_created >= i_ftd_date_from))
    and (i_ftd_date_to is null or (t.time_created >= i_ftd_date_to))
    and (i_fe_date_from is null or (ur.questionnaire_done_date >= i_fe_date_from))
    and (i_fe_date_to is null or (ur.questionnaire_done_date < i_fe_date_to))
    and (i_user_id is null or (u.id = i_user_id))
    and (i_usern_name is null or(upper(u.user_name) = upper(i_usern_name)))
    and (i_user_classes is null or (i_user_classes = 12 and u.class_id in (1, 2)) or
             (i_user_classes <> 12 and u.class_id = i_user_classes))
    and (i_skins is null or u.skin_id member of i_skins)
    and (i_countries is null or u.country_id member of i_countries)
    and (i_is_have_pw is null or
             (i_is_have_pw = 0 and not exists (select 1 from transactions where status_id in (9, 4) and user_id = u.id)) or
             (i_is_have_pw = 1 and exists (select 1 from transactions where status_id in (9, 4) and user_id = u.id)))               
    and (i_is_spended is null or 
             (i_is_spended = 0 and nvl(ur.suspended_reason_id,0) = 0) or
             (i_is_spended = 1 and nvl(ur.suspended_reason_id,0) > 0))
    and (i_is_user_active is null or 
             (i_is_user_active = 0 and u.is_active = 0) or
             (i_is_user_active = 1 and u.is_active = 1))
    and (i_is_compliance_approved is null or 
             (i_is_compliance_approved = 0 and nvl(ur.approved_regulation_step,0) != 7) or
             (i_is_compliance_approved = 1 and nvl(ur.approved_regulation_step,0) = 7))
    
    and (i_documents_uploaded is null or 
             (i_documents_uploaded = 0 and( nvl(user_docs.POI_UPLOAD,0) 
                                     +  nvl(user_docs.POR_UPLOAD,0)
                                     +  nvl(user_docs.POP_UPLOAD,0)
                                     +  nvl(user_docs.POI_REJECT,0)
                                     +  nvl(user_docs.POP_REJECT,0)
                                     +  nvl(user_docs.POR_REJECT,0) = 0)) or
              (i_documents_uploaded = 1 and( nvl(user_docs.POI_UPLOAD,0) 
                                     +  nvl(user_docs.POR_UPLOAD,0)
                                     +  nvl(user_docs.POP_UPLOAD,0)
                                     +  nvl(user_docs.POI_REJECT,0)
                                     +  nvl(user_docs.POP_REJECT,0)
                                     +  nvl(user_docs.POR_REJECT,0) > 0)))
    and    (i_from_total_deposit is null or ceil(ua.sum_deposits * i_rate) >= i_from_total_deposit)
    and    (i_to_total_deposit is null or   ceil(ua.sum_deposits * i_rate) <= i_to_total_deposit)                              
    
    and (i_days_left is null or  ur.days_left member of i_days_left)   
    and (i_call_attemps is null or 
             (i_call_attemps  = 6  and nvl(updc.call_attempts,0) >= i_call_attemps) or
             (i_call_attemps != 6  and nvl(updc.call_attempts,0)  = i_call_attemps))             
          
    and (i_resolution_1 is null OR
              (
               -- RESOLUTION MISSING
                 (1  member of i_resolution_1 and
                    (
                      (decode (i_doc_type_1,0,POI_DOCS,1,POI_DOCS,-1) = 0) OR 
                      (decode (i_doc_type_1,0,POR_DOCS,2,POR_DOCS,-1) = 0) OR 
                      (decode (i_doc_type_1,0,POP_NEED_UPLOAD,3,POP_NEED_UPLOAD, 0) > 0) 
                    )
                 )                 
                 or                 
                 -- RESOLUTION PENDING
                 (2  member of i_resolution_1 and
                    (
                      (decode (i_doc_type_1, 0, POI_NO_RESOLUTION, 1, POI_NO_RESOLUTION, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POR_NO_RESOLUTION, 2, POR_NO_RESOLUTION, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POP_NO_RESOLUTION, 3, POP_NO_RESOLUTION, -1) > 0) 
                    )
                 )
                 or                 
                 -- RESOLUTION APPROVED
                 (3  member of i_resolution_1 and
                    (
                      (decode (i_doc_type_1, 0, POI_APPROVE, 1, POI_APPROVE, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POI_APPROVE, 2, POI_APPROVE, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POP_APPROVE, 3, POP_APPROVE, -1) > 0) 
                    )
                 )
                 or                 
                 -- RESOLUTION REJECTED
                 (4  member of i_resolution_1 and
                    (
                      (decode (i_doc_type_1, 0, POI_REJECT, 1, POI_REJECT, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POR_REJECT, 2, POR_REJECT, -1) > 0) OR 
                      (decode (i_doc_type_1, 0, POP_REJECT, 3, POP_REJECT, -1) > 0) 
                    )
                 )
          )
      )
      
      and (i_resolution_2 is null OR
              (
               -- RESOLUTION MISSING
                 (1  member of i_resolution_2 and
                    (
                      (decode (i_doc_type_2,0,POI_DOCS,1,POI_DOCS,-1) = 0) OR 
                      (decode (i_doc_type_2,0,POR_DOCS,2,POR_DOCS,-1) = 0) OR 
                      (decode (i_doc_type_2,0,POP_NEED_UPLOAD,3,POP_NEED_UPLOAD, 0) > 0) 
                    )
                 )                 
                 or                 
                 -- RESOLUTION PENDING
                 (2  member of i_resolution_2 and
                    (
                      (decode (i_doc_type_2, 0, POI_NO_RESOLUTION, 1, POI_NO_RESOLUTION, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POR_NO_RESOLUTION, 2, POR_NO_RESOLUTION, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POP_NO_RESOLUTION, 3, POP_NO_RESOLUTION, -1) > 0) 
                    )
                 )
                 or                 
                 -- RESOLUTION APPROVED
                 (3  member of i_resolution_2 and
                    (
                      (decode (i_doc_type_2, 0, POI_APPROVE, 1, POI_APPROVE, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POI_APPROVE, 2, POI_APPROVE, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POP_APPROVE, 3, POP_APPROVE, -1) > 0) 
                    )
                 )
                 or                 
                 -- RESOLUTION REJECTED
                 (4  member of i_resolution_2 and
                    (
                      (decode (i_doc_type_2, 0, POI_REJECT, 1, POI_REJECT, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POR_REJECT, 2, POR_REJECT, -1) > 0) OR 
                      (decode (i_doc_type_2, 0, POP_REJECT, 3, POP_REJECT, -1) > 0) 
                    )
                 )
          )
      )
     and (i_expire_doc is null or 
          (i_expire_doc = 1 and nvl(user_docs.ABOUT_TO_EXPIRE,0) > 0) or
          (i_expire_doc = 2 and nvl(user_docs.EXPIRED,0) > 0))
          
    and (i_business_cases is null or s.business_case_id member of i_business_cases)
  order by case  
             when 1=i_order_by_id then days_left * i_asc_desc_param                          
             when 2=i_order_by_id then call_attemps * i_asc_desc_param                          
            else days_left*1 end) dt 
               
   ) v
   WHERE 
      v.rn > (i_page_number - 1) * i_rows_per_page
      and v.rn <= i_page_number * i_rows_per_page
   ;       
end GET_SCREENS_DATA;

PROCEDURE GET_CALL_CENTER_SCREENS
  (
    O_DATA                        OUT SYS_REFCURSOR
   ,I_FTD_DATE_FROM               IN DATE
   ,I_FTD_DATE_TO                 IN DATE   
   ,I_FE_DATE_FROM                IN DATE default null
   ,I_FE_DATE_TO                  IN DATE default null
   ,I_USER_ID                     IN NUMBER
   ,I_USERN_NAME                  IN VARCHAR2
   ,I_USER_CLASSES                IN NUMBER
   ,I_SKINS                       IN NUMBER_TABLE
   ,I_COUNTRIES                   IN NUMBER_TABLE
   ,I_IS_HAVE_PW                  IN NUMBER
   ,I_DAYS_LEFT                   IN NUMBER_TABLE
   ,I_IS_SPENDED                  IN NUMBER
   ,I_IS_USER_ACTIVE              IN NUMBER
   ,I_IS_COMPLIANCE_APPROVED      IN NUMBER
   ,I_COMPL_DOCUMENTS_UPLOADED    IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_DOCUMENTS_UPLOADED          IN NUMBER
   ,I_FROM_TOTAL_DEPOSIT          IN NUMBER
   ,I_TO_TOTAL_DEPOSIT            IN NUMBER
   ,I_CALL_ATTEMPS                IN NUMBER
   ,I_RATE                        IN NUMBER  
   ,I_ORDER_BY_ID                 IN NUMBER
   ,I_ASC_DESC_PARAM              IN NUMBER
   ,I_PAGE_NUMBER                 IN NUMBER 
   ,I_ROWS_PER_PAGE               IN NUMBER
   ,I_DOC_TYPE_1                  IN NUMBER default -1
   ,I_RESOLUTION_1                IN NUMBER_TABLE default null
   ,I_DOC_TYPE_2                  IN NUMBER default -1
   ,I_RESOLUTION_2                IN NUMBER_TABLE default null
   ,I_EXPIRE_DOC                  IN NUMBER
   ,I_BUSINESS_CASES              IN NUMBER_TABLE DEFAULT NULL
  ) IS

BEGIN 

GET_SCREENS_DATA(
                  o_data
                 ,1
                 ,i_ftd_date_from
                 ,i_ftd_date_to  
                 ,i_fe_date_from
                 ,i_fe_date_to  
                 ,i_user_id
                 ,i_usern_name
                 ,i_user_classes
                 ,i_skins
                 ,i_countries
                 ,i_is_have_pw
                 ,i_days_left
                 ,i_is_spended
                 ,i_is_user_active
                 ,i_is_compliance_approved
                 ,i_documents_uploaded
                 ,i_from_total_deposit
                 ,i_to_total_deposit
                 ,i_rate    
                 ,I_CALL_ATTEMPS     
                 ,i_order_by_id
                 ,i_asc_desc_param
                 ,i_page_number
                 ,i_rows_per_page
                 ,i_doc_type_1
                 ,i_resolution_1
                 ,i_doc_type_2
                 ,i_resolution_2
                 ,i_expire_doc
                 ,i_business_cases);

END GET_CALL_CENTER_SCREENS;

PROCEDURE GET_BACK_OFFICE_SCREENS
  (
    O_DATA                        OUT SYS_REFCURSOR
   ,I_FE_DATE_FROM                IN DATE default null
   ,I_FE_DATE_TO                  IN DATE default null
   ,I_USER_ID                     IN NUMBER
   ,I_USERN_NAME                  IN VARCHAR2
   ,I_USER_CLASSES                IN NUMBER
   ,I_SKINS                       IN NUMBER_TABLE
   ,I_COUNTRIES                   IN NUMBER_TABLE
   ,I_IS_HAVE_PW                  IN NUMBER
   ,I_DAYS_LEFT                   IN NUMBER_TABLE
   ,I_IS_SPENDED                  IN NUMBER
   ,I_IS_USER_ACTIVE              IN NUMBER
   ,I_IS_COMPLIANCE_APPROVED      IN NUMBER
   ,I_COMPL_DOCUMENTS_UPLOADED    IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_FROM_TOTAL_DEPOSIT          IN NUMBER
   ,I_TO_TOTAL_DEPOSIT            IN NUMBER
   ,I_RATE                        IN NUMBER  
   ,I_DOCUMENTS_TYPE              IN NUMBER default null -- REMOVE AFTER DEPLOY
   ,I_ORDER_BY_ID                 IN NUMBER
   ,I_ASC_DESC_PARAM              IN NUMBER
   ,I_PAGE_NUMBER                 IN NUMBER
   ,I_ROWS_PER_PAGE               IN NUMBER
   ,I_DOC_TYPE_1                  IN NUMBER default -1
   ,I_RESOLUTION_1                IN NUMBER_TABLE default null
   ,I_DOC_TYPE_2                  IN NUMBER default -1
   ,I_RESOLUTION_2                IN NUMBER_TABLE default null
   ,I_BUSINESS_CASES              IN NUMBER_TABLE DEFAULT NULL
  ) IS

BEGIN    
GET_SCREENS_DATA(
                  o_data
                 ,2
                 ,null
                 ,null
                 ,i_fe_date_from
                 ,i_fe_date_to
                 ,i_user_id
                 ,i_usern_name
                 ,i_user_classes
                 ,i_skins
                 ,i_countries
                 ,i_is_have_pw
                 ,i_days_left
                 ,i_is_spended
                 ,i_is_user_active
                 ,i_is_compliance_approved
                 ,null
                 ,i_from_total_deposit
                 ,i_to_total_deposit
                 ,i_rate          
                 ,null       
                 ,i_order_by_id
                 ,i_asc_desc_param
                 ,i_page_number
                 ,i_rows_per_page
                 ,i_doc_type_1
                 ,i_resolution_1
                 ,i_doc_type_2
                 ,i_resolution_2
                 ,null
                 ,i_business_cases);

END GET_BACK_OFFICE_SCREENS;

PROCEDURE GET_USER_DATA_BY_ID
   (
     O_DATA                      OUT SYS_REFCURSOR
    ,O_FILE_DATA                 OUT SYS_REFCURSOR
    ,O_USER_FILE_COMMENTS        OUT SYS_REFCURSOR
    ,O_IS_ENABLE_REG_APPROVE     OUT NUMBER
    ,O_IS_ENABLE_REG_APPROVE_MAN OUT NUMBER
    ,I_USER_ID                   IN NUMBER
   ) IS     
  cursor c_get_reg_step is
  select ur.approved_regulation_step
  from users_regulation ur
  where ur.user_id = I_USER_ID;
  
  v_approved_regulation_step number; 
 BEGIN
-- SET USERS
    OPEN O_DATA FOR
      SELECT  u.id user_id      
             ,u.time_created as registration_date
             ,ur.questionnaire_done_date
             ,managers.user_name account_manager
             ,u.balance
             ,u.currency_id
             ,'+'||c.phone_code||'-'||u.mobile_phone mobile_phone
             ,last_logins.last_login_date
             ,nvl(ur.approved_regulation_step,0) as step
             ,nvl(updc.call_attempts,0) call_attemps
             ,updc.last_call_time
             ,updc.last_reached_time
        FROM users u,
             users_regulation ur,
             ( select pu.user_id
                      ,w.user_name 
               from 
                      population_users pu,
                      writers w
                where 
                      pu.curr_assigned_writer_id = w.id
              ) managers,
              ( select user_id 
                       ,max(login_time) last_login_date 
                 from  logins 
                 group by 
                       user_id
               ) last_logins,
               countries c,
               users_pending_docs_call_info updc   
       WHERE u.id = ur.user_id(+)
             and u.country_id = c.id
             and u.id = managers.user_id(+)
             and u.id = last_logins.user_id(+)
             and u.id = updc.user_id(+)
             and u.id = I_USER_ID;
-- SET FILES
    OPEN O_FILE_DATA FOR
          SELECT f.id file_id
                ,ft.name file_type_name
                ,ft.id file_type_id
                ,f.is_approved
                ,f.is_support_rejected 
                ,w_approved.user_name as writer_approved
                ,w_rejected.user_name as writer_rejected
                ,f.exp_date
                ,frr.reason_name
                ,case when f.file_name is not null then f.time_uploaded else null end as time_uploaded
                ,case when f.file_type_id in (23,24) then cc.exp_month||'/'||cc.exp_year 
                  else '' end as cc_exp
                ,cc.cc_number
                ,cct.name cc_type_name    
          FROM 
           files f,
           file_types ft,
           file_reject_reasons frr,
           writers w_approved,
           writers w_rejected,
           credit_cards cc,
           credit_card_types cct,
           users u
           where 
             f.file_type_id = ft.id
             and f.user_id = u.id
             and (ft.file_group_id in (1,2,3) or (u.country_id = 192 and ft.id = 38))
             and f.support_approved_writer_id = w_approved.id(+)
             and f.rejection_writer_id = w_rejected.id(+)
             and f.is_current = 1
             and f.reject_reason_id = frr.id(+)
             and f.cc_id = cc.id(+)
             and cc.type_id = cct.id(+)
             and f.user_id = I_USER_ID
             and f.file_type_id <> 24
          order by f.id;      
        
-- GET comments
	OPEN O_USER_FILE_COMMENTS FOR
    SELECT COMMENTS
      FROM (SELECT COMMENTS
              FROM ISSUE_ACTIONS
             WHERE ISSUE_ID = (SELECT ID
                                 FROM (SELECT ID
                                         FROM ISSUES
                                        WHERE SUBJECT_ID = 56
                                          AND STATUS_ID = 2
                                          AND COMPONENT_ID = 1
                                          AND USER_ID = I_USER_ID
                                        ORDER BY TIME_CREATED DESC
                                      )
                                WHERE ROWNUM = 1
                              )
             ORDER BY ACTION_TIME DESC
          )
    WHERE ROWNUM = 1;

-- SET Approve Regulation BTNs
   IS_CAN_REGULATION_APPROVE( 
                              O_IS_ENABLE_REG_APPROVE
                             ,I_USER_ID
                             ); 
  
-- SET Approve Regulation BTNs Manager
   O_IS_ENABLE_REG_APPROVE_MAN := 0; 
   open c_get_reg_step;
   fetch c_get_reg_step into v_approved_regulation_step;
   close c_get_reg_step;                                    
   if(v_approved_regulation_step > 0 and v_approved_regulation_step < 7) then
     O_IS_ENABLE_REG_APPROVE_MAN := 1;                           
   end if;

END GET_USER_DATA_BY_ID;

  PROCEDURE GET_USER_DATA_BY_ID_TMP
  (
     O_DATA                      OUT SYS_REFCURSOR
    ,O_FILE_DATA                 OUT SYS_REFCURSOR
    ,O_USER_FILE_COMMENTS        OUT SYS_REFCURSOR --,I_BUSINESS_CASES IN NUMBER_TABLE
    ,O_IS_ENABLE_REG_APPROVE     OUT NUMBER
    ,O_IS_ENABLE_REG_APPROVE_MAN OUT NUMBER
    ,I_USER_ID                   IN NUMBER
   ) IS     
  cursor c_get_reg_step is
  select ur.approved_regulation_step
  from users_regulation ur
  where ur.user_id =  I_USER_ID;
  
  v_approved_regulation_step number; 
 BEGIN
-- SET USERS
    OPEN O_DATA FOR
      SELECT  u.id user_id      
             ,u.time_created as registration_date
             ,ur.questionnaire_done_date
             ,managers.user_name account_manager
             ,u.balance
             ,u.currency_id
             ,'+'||c.phone_code||'-'||u.mobile_phone mobile_phone
             ,last_logins.last_login_date
             ,nvl(ur.approved_regulation_step,0) as step
             ,nvl(updc.call_attempts,0) call_attemps
             ,updc.last_call_time
             ,updc.last_reached_time
        FROM users u,
             users_regulation ur,
             ( select pu.user_id
                      ,w.user_name 
               from 
                      population_users pu,
                      writers w
                where 
                      pu.curr_assigned_writer_id = w.id
              ) managers,
              ( select user_id 
                       ,max(login_time) last_login_date 
                 from  logins 
                 group by 
                       user_id
               ) last_logins,
               countries c,
               users_pending_docs_call_info updc                                    
       WHERE u.id = ur.user_id(+)
             and u.country_id = c.id
             and u.id = managers.user_id(+)
             and u.id = last_logins.user_id(+)
             and u.id = updc.user_id(+)
             and u.id = I_USER_ID;
-- SET FILES
    OPEN O_FILE_DATA FOR
          SELECT f.id file_id
                ,ft.name file_type_name
                ,ft.id file_type_id
                ,f.is_approved
                ,f.is_support_rejected 
                ,w_approved.user_name as writer_approved
                ,w_rejected.user_name as writer_rejected
                ,f.exp_date
                ,frr.reason_name
                ,case when f.file_name is not null then f.time_uploaded else null end as time_uploaded
                ,case when f.file_type_id in (23,24) then cc.exp_month||'/'||cc.exp_year 
                  else '' end as cc_exp
          FROM 
           files f,
           file_types ft,
           file_reject_reasons frr,
           writers w_approved,
           writers w_rejected,
           credit_cards cc,
           users u
           where 
             f.file_type_id = ft.id
             and f.user_id = u.id
             and (ft.file_group_id in (1,2,3) or (u.country_id = 192 and ft.id = 38))
             and f.support_approved_writer_id = w_approved.id(+)
             and f.rejection_writer_id = w_rejected.id(+)
             and f.is_current = 1
             and f.reject_reason_id = frr.id(+)
             and f.cc_id = cc.id(+)
             and f.user_id = I_USER_ID
          order by f.id;      
        
-- GET comments
	OPEN O_USER_FILE_COMMENTS FOR
    SELECT COMMENTS
      FROM (SELECT COMMENTS
              FROM ISSUE_ACTIONS
             WHERE ISSUE_ID = (SELECT ID
                                 FROM (SELECT ID
                                         FROM ISSUES
                                        WHERE SUBJECT_ID = 56
                                          AND STATUS_ID = 2
                                          AND COMPONENT_ID = 1
                                          AND USER_ID = I_USER_ID
                                        ORDER BY TIME_CREATED DESC
                                      )
                                WHERE ROWNUM = 1
                              )
             ORDER BY ACTION_TIME DESC
          )
    WHERE ROWNUM = 1;

-- SET Approve Regulation BTNs
   IS_CAN_REGULATION_APPROVE( 
                              O_IS_ENABLE_REG_APPROVE
                             ,I_USER_ID
                             ); 
  
-- SET Approve Regulation BTNs Manager
   O_IS_ENABLE_REG_APPROVE_MAN := 0; 
   open c_get_reg_step;
   fetch c_get_reg_step into v_approved_regulation_step;
   close c_get_reg_step;                                    
   if(v_approved_regulation_step > 0 and v_approved_regulation_step < 7) then
     O_IS_ENABLE_REG_APPROVE_MAN := 1;                           
   end if;

END GET_USER_DATA_BY_ID_TMP;

PROCEDURE GET_USER_REG_FILE_STATE
  (
     O_POI_APPROVED             OUT NUMBER
    ,O_POR_APPROVED             OUT NUMBER
    ,O_APPROVED_REGULATION_STEP OUT NUMBER
    ,O_CNMV_APPROVED            OUT NUMBER
    ,I_USER_ID                  IN NUMBER
  ) IS     

cursor c_get_reg_f_states is 
   select 
         sum(case when ft.file_group_id = 1 and f.is_approved = 1 then 1 else 0 end) as POI_APPROVED
        ,sum(case when ft.file_group_id = 2 and f.is_approved = 1 then 1 else 0 end) as POR_APPROVED
        ,sum(case when ft.id = 38 and f.is_approved = 1 then 1 else 0 end) as CNMV_APPROVED
        ,ur.approved_regulation_step
   from 
        users_regulation ur,
        files f,
        file_types ft
   where ur.user_id = f.user_id
         and f.is_current = 1
         and f.file_type_id = ft.id
         and (ft.file_group_id in (1,2) or ft.id = 38)
         and f.user_id = I_USER_ID
   group by 
         ur.approved_regulation_step;

  BEGIN
       open c_get_reg_f_states;
       fetch c_get_reg_f_states into O_POI_APPROVED, O_POR_APPROVED, O_CNMV_APPROVED, O_APPROVED_REGULATION_STEP;
       close c_get_reg_f_states;                                                                             
  END GET_USER_REG_FILE_STATE;
  
PROCEDURE IS_CAN_REGULATION_APPROVE
  (
   O_IS_CAN_APPROVE  OUT NUMBER
  ,I_USER_ID         IN NUMBER
  ) is
  v_poi_approved number;
  v_por_approved number;
  v_cnmv_approved number;
  v_approved_regulation_step number;
 
  cursor c_get_user_country_id is
    select u.country_id
    from  users u
    where u.id = I_USER_ID;
 
    v_user_country_id number;
  
begin
   GET_USER_REG_FILE_STATE( v_poi_approved 
                           ,v_por_approved
                           ,v_approved_regulation_step
                           ,v_cnmv_approved
                           ,I_USER_ID);  
 open c_get_user_country_id;
 fetch c_get_user_country_id into v_user_country_id; 
 close c_get_user_country_id;                       
  
  O_IS_CAN_APPROVE := 0;                           
  if(v_poi_approved > 0 and v_por_approved > 0 and v_user_country_id != 192) then
     O_IS_CAN_APPROVE := 1;
  end if;
--For customers from country = Spain - regulation approve button will be disabled until all regulation documents(ID/Passport , GBG/UB and CNMV) will be approved  
  if(v_poi_approved > 0 and v_por_approved > 0 and v_cnmv_approved > 0 and v_user_country_id = 192) then
    O_IS_CAN_APPROVE := 1;
  end if;
  
  if v_approved_regulation_step >= 7 then
    O_IS_CAN_APPROVE := 0;
  end if;

end IS_CAN_REGULATION_APPROVE; 

PROCEDURE GET_DOCUMENT_STATUS
  (
   O_DOCUMENT_STATUS  OUT NUMBER
  ,I_USER_ID          IN NUMBER
  ) is
cursor c_doc_state is
--1.Missing
--2.Pending
--3.Approved
---Other N/A
select
   (case 
  --user is REG and have file ONE of POI approved and missing POR
    when (nvl(ur.approved_regulation_step,0) >= 3 
                      and (nvl(user_docs.POI_APPROVE,0)> 0 and  (nvl(user_docs.POR_APPROVE,0) + nvl(user_docs.POR_NO_RESOLUTION,0) = 0)) 
                          and nvl(ur.approved_regulation_step,0) < 7) then 1                                
   --user is REG and have file ONE of POR approved and missing POI
    when (nvl(ur.approved_regulation_step,0) >= 3 
                      and (nvl(user_docs.POR_APPROVE,0)> 0 and  (nvl(user_docs.POI_APPROVE,0) + nvl(user_docs.POI_NO_RESOLUTION,0) = 0)) 
                          and nvl(ur.approved_regulation_step,0) < 7) then 1                                                                                         
   --user is REG and have file POI or POR with NO_RESOLUITON
    when (nvl(ur.approved_regulation_step,0) >= 3 
                      and (nvl(user_docs.POI_NO_RESOLUTION,0) + nvl(user_docs.POR_NO_RESOLUTION,0)> 0) 
                          and nvl(ur.approved_regulation_step,0) < 7) then 2                  
   --user is REG and have resolution for POI and POR and not Final Approve and non Spain
    when (nvl(ur.approved_regulation_step,0) >= 3 
                      and nvl(user_docs.POI_APPROVE,0) > 0 and nvl(user_docs.POR_APPROVE,0)> 0 
                          and nvl(ur.approved_regulation_step,0) < 7 and u.country_id != 192) then 2
  --user is REG from Spain and have resolution for POI and POR and not Final Approve
      when (nvl(ur.approved_regulation_step,0) >= 3 
                        and nvl(user_docs.POI_APPROVE,0) > 0 and nvl(user_docs.POR_APPROVE,0)> 0 and nvl(user_docs.CNMV_APPROVE,0) > 0
                            and nvl(ur.approved_regulation_step,0) < 7 and u.country_id = 192) then 2                            
    --After Approved and have Expire POI or POR docs and we have without resolution
    when ((POI_APPROVE = 0 and POI_NO_RESOLUTION > 0) or (POR_APPROVE = 0 and POI_NO_RESOLUTION > 0) ) and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)  then  2                                                        
    --After Approved and have Expire POI or POR docs
    when (POI_APPROVE = 0 or POR_APPROVE = 0 ) and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)  then  1                              
    --user  have NO resolution for POP
    when (nvl(user_docs.POP_NO_RESOLUTION,0) > 0 and  nvl(ur.approved_regulation_step,7) >= 7) then 2
    --No files and quest. is not done
    when (nvl(user_docs.user_id,0) = 0 and nvl(ur.approved_regulation_step,0) < 3) then  -1
    -- quest. is not done
    when (nvl(ur.approved_regulation_step,99) < 3) then  -1 
    --After Approved and have additonal POI or POR docs. upload
    when (nvl(user_docs.POP_NEED_UPLOAD,0) = 0 and nvl(user_docs.POP_REJECT,0) = 0 and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)) 
      and (nvl(user_docs.POI_NO_RESOLUTION,0) + nvl(user_docs.POR_NO_RESOLUTION,0)> 0)  then  2      
   --After All docs are approved return 3 : 1.When not need upload  or reject POP and if regulated is step 7
    when (nvl(user_docs.POP_NEED_UPLOAD,0) = 0  and nvl(user_docs.POP_REJECT,0) = 0 and (decode(s.is_regulated,1,ur.approved_regulation_step,7) = 7)) then  3
   else 1 end
   ) as user_doc_state
from 
users u
,skins s
,users_regulation ur
,(
            select f.user_id,
                   --POI
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POI_UPLOAD,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 and f.expire_doc = 0 then 1 else 0 end) as POI_NO_RESOLUTION,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POI_REJECT,
                   sum(case when (ft.file_group_id = 1 or (ft.id = 38 and uf.country_id = 192))  and f.file_name is not null and nvl(f.is_approved,0) = 1 and f.expire_doc = 0 then 1 else 0 end) as POI_APPROVE,
                   sum(case when  ft.id = 38 and uf.country_id = 192  and f.file_name is not null and nvl(f.is_approved,0) = 1 then 1 else 0 end) as CNMV_APPROVE,
                   --POR
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POR_UPLOAD,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 and f.expire_doc = 0 then 1 else 0 end) as POR_NO_RESOLUTION,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POR_REJECT,
                   sum(case when ft.file_group_id = 2  and f.file_name is not null and nvl(f.is_approved,0) = 1 and f.expire_doc = 0 then 1 else 0 end) as POR_APPROVE,
                   --POP
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) != 1 then 1 else 0 end) as POP_UPLOAD,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) = 0 and nvl(f.is_approved,0) = 0 then 1 else 0 end) as POP_NO_RESOLUTION,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_support_rejected,0) = 1 then 1 else 0 end) as POP_REJECT,
                   sum(case when ft.file_group_id = 3  and f.file_name is not null and nvl(f.is_approved,0) = 1 then 1 else 0 end) as POP_APPROVE,
                   sum(case when ft.file_group_id = 3  and f.file_name is null then 1 else 0 end) as POP_NEED_UPLOAD
            from
                   file_types ft,
                   users uf, 
                  (select f1.*
                           , case when f1.file_type_id in (1,2,22) 
                                  and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                      and (trunc(f1.exp_date) - trunc(sysdate)) between 1 and 14 then 1
                                  when  f1.file_type_id in (1,2,22) 
                                      and uad.last_deposit_or_invest_date >= add_months(SYSDATE, -6)
                                          and (trunc(f1.exp_date) - trunc(sysdate)) <=0 then 2 
                                    else 0 end as expire_doc   
                  from 
                    files f1,
                    users_active_data uad
                 where 
                    f1.user_id = uad.user_id 
                )f   
            where 
                   (ft.file_group_id in (1,2,3) or ft.id = 38) --38 CNMV File for Spain
                   and ft.id = f.file_type_id
                   and f.user_id = uf.id
                   and f.is_current = 1 
           group by 
                   f.user_id
           ) user_docs
where
u.skin_id = s.id 
and u.id = ur.user_id (+)
and u.id = user_docs.user_id(+)
and u.id  = I_USER_ID;
begin
 open c_doc_state;
 fetch c_doc_state into O_DOCUMENT_STATUS;
 close c_doc_state;

end GET_DOCUMENT_STATUS; 

PROCEDURE LOCK_USER_DOC
  (
   O_LOCKED_WRITER_NAME  OUT VARCHAR2
  ,I_SESSION_ID          IN VARCHAR2
  ,I_WRITER_ID           IN NUMBER
  ,I_USER_ID             IN NUMBER
  ,I_SERVER_ID           IN VARCHAR2
  ,I_SCREEN_ID			     IN NUMBER DEFAULT 2
  ) is
cursor c_get_locked is
 select l.writer_id
       ,w.user_name
	   ,l.screen_id
 from 
       USER_DOC_LOCK l
        ,writers w
 where w.id = l.writer_id       
       and l.user_id = I_USER_ID;

 v_writer_id number:=0;
 v_screen_id number:=0;
 v_writer_name writers.user_name%type;
 
begin
 open c_get_locked;
 fetch c_get_locked into v_writer_id, v_writer_name, v_screen_id;
 close c_get_locked;
 
 if v_writer_id > 0 and v_writer_id != I_WRITER_ID and v_screen_id = i_screen_id then
   O_LOCKED_WRITER_NAME := v_writer_name;
 elsif v_writer_id > 0 and v_writer_id != I_WRITER_ID and v_screen_id != i_screen_id then
       delete user_doc_lock where writer_id = v_writer_id and USER_ID = I_USER_ID;
       insert into user_doc_lock
         (session_id, writer_id, user_id, server_id, screen_id)
       values
       (I_SESSION_ID, I_WRITER_ID, I_USER_ID, I_SERVER_ID, I_SCREEN_ID);
 elsif v_writer_id = 0 then
       UNLOCK_USER_DOC(null, I_WRITER_ID, null, i_screen_id);
       insert into user_doc_lock
         (session_id, writer_id, user_id, server_id, screen_id)
       values
       (I_SESSION_ID, I_WRITER_ID, I_USER_ID, I_SERVER_ID, I_SCREEN_ID);
 end if;

end LOCK_USER_DOC;

PROCEDURE UNLOCK_USER_DOC
  (
   I_SESSION_ID          IN VARCHAR2
  ,I_WRITER_ID           IN NUMBER
  ,I_SERVER_ID           IN VARCHAR2
  ,I_SCREEN_ID           IN NUMBER DEFAULT 2
  ) is
 
BEGIN  
 if I_WRITER_ID is not null and I_SCREEN_ID is not null then
   delete user_doc_lock where writer_id = I_WRITER_ID and SCREEN_ID = I_SCREEN_ID;
 end if;  
 if I_SESSION_ID is not null  then
    delete user_doc_lock  where session_id = I_SESSION_ID;
 end if;
 if I_SERVER_ID is not null  then
    delete user_doc_lock  where server_id = I_SERVER_ID;
 end if;     
end UNLOCK_USER_DOC;

PROCEDURE MIGRATE_CURRNT_STATUS
  (
   i_from_id           IN NUMBER
  ,i_to_id             IN NUMBER
  ) is
cursor c_data is
select * from tmp_is_current tmp
where tmp.state =0
and tmp.id between i_from_id and i_to_id;
begin
  for lp in c_data loop
--lock
      update  tmp_is_current t
      set t.state = 1
      where t.id = lp.id;
     commit;
  pkg_user_documents.SET_IS_CURRENT_DOCS(lp.user_id,lp.file_type_id, lp.cc );
--done
      update  tmp_is_current t
      set t.state = 2
      where t.id = lp.id;
  commit;
  end loop;
end;

PROCEDURE CALC_USERS_PENDING_DOC_CALL
  (
   i_user_id           IN NUMBER
  ) is
cursor c_data is
select 
count(*) as num_calls
,max(ia.action_time)  
,max(case when rs.id = 2 then ia.action_time else null end) as reached
from 
issue_actions ia,
issue_action_types iat,
issue_reached_statuses rs, 
issues i
where 
ia.issue_action_type_id = iat.id
and iat.channel_id = 1
and ia.direction_id = 1
and i.id = ia.issue_id
and iat.reached_status_id = rs.id(+)
and i.subject_id = 56
and i.user_id = i_user_id
group by user_id;

l_call_attempts number;
l_last_call_time date;
l_last_reached_time date;
begin
open c_data;
fetch c_data into l_call_attempts, l_last_call_time, l_last_reached_time; 
close c_data;

delete USERS_PENDING_DOCS_CALL_INFO where user_id = i_user_id;

insert into USERS_PENDING_DOCS_CALL_INFO
   (user_id, call_attempts, last_call_time, last_reached_time)
 values
   (i_user_id, l_call_attempts, l_last_call_time, l_last_reached_time);
 

end;

PROCEDURE GET_USER_POP_DOCUMENTS
  (
    O_DATA    OUT SYS_REFCURSOR
   ,I_USER_ID IN NUMBER
  ) IS     
  BEGIN
  --Get POP File LIST
    OPEN O_DATA FOR
      SELECT  f.file_type_id
             ,f.id
        FROM FILES f,
             FILE_TYPES ft                   
       WHERE f.USER_ID = I_USER_ID    
             and f.file_type_id = ft.id
             and f.is_current = 1
             and ft.file_group_id =3;                                                                            
  END GET_USER_POP_DOCUMENTS;

  PROCEDURE GET_DEFAULT_FILES
  (
      O_DEFAULT_FILES            OUT SYS_REFCURSOR
     ,I_MATCH_FROM               IN DATE
     ,I_MATCH_TO                 IN DATE
     ,I_USER_ID                  IN NUMBER
     ,I_USER_NAME                IN VARCHAR2
     ,I_USER_CLASS               IN NUMBER
     ,I_SKINS                    IN NUMBER_TABLE
     ,I_COUNTRIES                IN NUMBER_TABLE
     ,I_PAGE                     IN NUMBER
     ,I_ROWS_PER_PAGE            IN NUMBER
  ) IS
  BEGIN
  
    OPEN O_DEFAULT_FILES FOR
      SELECT
        *
      FROM
        (SELECT
             v.*
            ,(select max(ff.id) from files ff where ff.user_id = v.userId and ff.time_created = v.lastMatchDate) lastDocumentId
            ,rownum rn
        FROM
              (SELECT
                 f.user_id userId
                ,u.user_name userName
                ,u.first_name firstName
                ,u.last_name lastName
                ,c.country_name country
                ,u.skin_id skinId
                ,count(*) defaultDocumentsCount
                ,max(f.time_created) lastMatchDate
                ,count(*) over() total_count
              FROM
                 users u
                 join files f on (f.user_id = u.id) and (f.file_type_id = 37)
                 join countries c on (u.country_id = c.id)
              WHERE
                    (i_match_from is null or f.time_created >= i_match_from)
                and (i_match_to is null or f.time_created <= i_match_to)
                and (i_user_id is null or u.id = i_user_id)
                and (i_user_name is null or upper(u.user_name) = upper(i_user_name))
                and (i_user_class is null
                     or (i_user_class = 12 and u.class_id in (1, 2))
                     or (i_user_class <> 12 and u.class_id = i_user_class))
                and (i_skins is null or u.skin_id member of i_skins)
                and (i_countries is null or u.country_id member of i_countries)
              GROUP BY
                f.user_id
                ,u.user_name
                ,u.first_name
                ,u.last_name
                ,c.country_name
                ,u.skin_id
              ORDER BY
                lastMatchDate asc) v
        WHERE
              rownum <= i_page * i_rows_per_page
        )
      WHERE
        rn > (i_page - 1) * i_rows_per_page;
        
  END GET_DEFAULT_FILES;
END PKG_USER_DOCUMENTS;
/
