CREATE OR REPLACE PACKAGE COPYOP AS
 
PROCEDURE INIT_TRADING_HOURS(p_hours number);
 
 FUNCTION GET_TRADING_DATE_TIME(p_hours number)
  return date;
  
  PROCEDURE INSERT_HITS(p_user_id number, p_hit number, p_hit_copied number);
  
  PROCEDURE MIGRATE_OFF_USER(p_from number, p_to number);
  
  PROCEDURE MIGRATE_BROKE_USER(p_from number, p_to number);
  
  PROCEDURE MIGRATE_DORMANT_USER(p_from number, p_to number);
  
  FUNCTION CALCULATE_RISK_APPETITE (p_user_id NUMBER)
  return number;
  
  PROCEDURE get_copyop_inv_odds(O_DATA  OUT SYS_REFCURSOR);
end;

/

CREATE OR REPLACE PACKAGE BODY COPYOP
AS

  procedure init_trading_hours(p_hours number) is
  begin
    execute immediate 'truncate table COPYOP.EST_CLOSING';
  
    insert into est_closing
      (opt_date)
      select distinct sys_extract_utc(op.time_est_closing)
      from   opportunities op
      where  sys_extract_utc(op.time_est_closing) between trunc(sysdate - p_hours / 2) and trunc(sysdate + 1)
      and    op.is_settled = 1
      and    op.opportunity_type_id = 1
      and    op.scheduled = 1
      and    op.is_disabled = 0;

    commit;
  end init_trading_hours;

FUNCTION GET_TRADING_DATE_TIME(p_hours number)
  return date is

cursor c_get_op_dates is
select opt_date
from
EST_CLOSING ec
order by 1 desc;

cursor c_get_op_date(v_curr_date date) is
select opt_date
from
EST_CLOSING ec
where ec.opt_date between v_curr_date - 1/24 and v_curr_date
order by 1;


v_search_date date;
v_last_close_date date;
br number := 0;
v_MID varchar2(400) :=null;
v_res date;

  BEGIN
       open c_get_op_dates;
       fetch c_get_op_dates into v_last_close_date;
       close c_get_op_dates;

   for LP in c_get_op_dates loop
         if br = p_hours then
           v_res :=v_search_date;
           GOTO found_date;
         end if;

          --dbms_output.put_line (' SEARCH FOR:'||to_char(v_last_close_date,'dd-mm-yyyy hh24:mi:ss') ||' and '||to_char(v_last_close_date-1/24*1,'dd-mm-yyyy hh24:mi:ss'));
         open c_get_op_date(v_last_close_date);
         fetch c_get_op_date into v_search_date;
         if c_get_op_date%found then
           br := br+1;
--              v_last_close_date := v_last_close_date -1/24;

         end if;
         close c_get_op_date;

          v_last_close_date := v_last_close_date -1/24;
   --                  dbms_output.put_line ('NEXT SEARCH FOR'||to_char(v_last_close_date,'dd-mm-yyyy hh24:mi:ss'));

   end loop;
      if br <> p_hours then
        v_search_date:=null;
        end if;
<<found_date>>
    return v_res;
  END;


PROCEDURE INSERT_HITS(p_user_id number, p_hit number, p_hit_copied number) IS

cursor c_check_rec is
select * from
copyop.copyop_hits ch
where ch.user_id = p_user_id;

v_tmp copyop_hits%rowtype;
v_have_rec boolean := false;

BEGIN
 open c_check_rec;
 fetch c_check_rec into v_tmp;
 if c_check_rec%found then
    v_have_rec := true;
 end if;
 close c_check_rec;

 IF (v_have_rec) THEN
   update copyop_hits
      set
          hit = decode(p_hit,0,v_tmp.hit,p_hit),
          hit_copied = decode(p_hit_copied,0,v_tmp.hit_copied,p_hit_copied ),
          updated_time = sysdate
    where user_id = p_user_id;
 ELSE
      insert into copyop_hits
        (id, user_id, hit, hit_copied, updated_time)
      values
        (seq_copyop_hits.nextval, p_user_id, decode(p_hit,0,null,p_hit), decode(p_hit_copied,0,null,p_hit_copied), sysdate);
 END IF;
END INSERT_HITS;

PROCEDURE MIGRATE_OFF_USER(p_from number, p_to number) is
  
cursor c_off is
select u.id, case when u.time_created <= sysdate - 1/24*48 then 0 else 1 end is_new,
(select count(*) 
                                         from investments i, 
                                              opportunities op 
                                         where 
                                              i.opportunity_id = op.id 
                                          and i.copyop_type_id in (0,2)
                                          and op.scheduled = 1
                                          and i.user_id = u.id
                                          and i.time_created > u.time_created) as count_inv
 from 
  users u
 where id between p_from and p_to;
               

v_reason number;
v_is_frozen number; 


BEGIN
  for lp in c_off loop

  
  if lp.count_inv >= 3 then --3
    if lp.is_new = 0 then 
         v_is_frozen := 0; 
          v_reason := 0;
     else
         v_is_frozen := 1;
          v_reason := 0;
    end if;     
  
  end if; 
  
  if lp.count_inv < 3 then --3
    if lp.is_new = 0 then 
         v_is_frozen := 1; 
         v_reason := 1;
     else
       v_is_frozen := 1;
       v_reason := 0;
    end if;     
 
  end if; 
  
     insert into copyop_frozen
  (id, user_id, is_frozen, reason, investment_id, writer_id, updated_time)
values
  (seq_copyop_frozen.nextval, lp.id, v_is_frozen, v_reason, 0, 0, sysdate);                                   
  commit;
end loop;  
END;

PROCEDURE MIGRATE_BROKE_USER(p_from number, p_to number) is
  

cursor c_broke(v_tr_dates date) is
select * from   (select rownum as num, u.id as user_id 
                      from   
                       users u, 
                       (SELECT  
                           MIN(ilgcmin.value) min_amount , 
                           ilgcmin.CURRENCY_ID 
                           FROM  
                             investment_limits il, 
                             investment_limit_types ilt, 
                             INVESTMENT_LIMIT_GROUP_CURR ilgcmin  
                           WHERE  
                             il.LIMIT_TYPE_ID = ilt.id  
                             AND il.MIN_LIMIT_GROUP_ID = ilgcmin.INVESTMENT_LIMIT_GROUP_ID  
                             AND il.IS_ACTIVE = 1  
                             AND ilt.IS_ACTIVE = 1  
                             AND il.LIMIT_TYPE_ID = 1  
                           GROUP BY   
                             ilgcmin.CURRENCY_ID) min_inv           
               where u.currency_id = min_inv.CURRENCY_ID 
                     and u.balance < min_inv.min_amount 
                     and u.id not in (select user_id from COPYOP_FROZEN fr where fr.is_frozen = 1) 
                     and u.id not in  (select user_id  from  balance_history where time_created >= v_tr_dates )
                     ) where num between p_from and p_to;

 tradingDates date;
               

v_reason number;
v_is_frozen number; 


BEGIN
 INIT_TRADING_HOURS(72); --72h
tradingDates := copyop.GET_TRADING_DATE_TIME(72);
dbms_output.put_line('Get lasss trading dates:'||tradingDates);


dbms_output.put_line( 'RUN c_broke users');
for lp in c_broke(tradingDates) loop
update copyop_frozen
   set 
       is_frozen = 1,
       reason = 2,
       investment_id = 0,
       writer_id = 0,
       updated_time = sysdate
 where user_id = lp.user_id;
commit;
end loop;                             
 
END;


PROCEDURE MIGRATE_DORMANT_USER(p_from number, p_to number) is
  

cursor c_dorm is
select * from   (  SELECT rownum as num, u.id as user_id 
	            			  FROM 
	            			 	 users u ,
                        copyop_frozen fr
	            			  WHERE 
	            			 	 u.id = fr.user_id and fr.is_frozen = 0 
	            			 	 and u.id not in (select distinct user_id from  
															 investments i, 
															 opportunities op 
														 where  op.id = i.opportunity_id 
															 and op.scheduled = 1 
															 and op.opportunity_type_id = 1 
															 and i.copyop_type_id in (0,2) 
															 and i.time_created >= sysdate - 7 )
                     ) where num between p_from and p_to;

 tradingDates date;
               

v_reason number;
v_is_frozen number; 


BEGIN
for lp in c_dorm loop
  
update copyop_frozen
   set 
       is_frozen = 1,
       reason = 3,
       investment_id = 0,
       writer_id = 0,
       updated_time = sysdate
 where user_id = lp.user_id;
commit;
end loop;                           
 
END;

FUNCTION CALCULATE_RISK_APPETITE(p_user_id NUMBER) RETURN number IS
  CURSOR calculate IS
   								SELECT 
                      ROUND(sum(i.amount)/ sum(bh.balance+i.amount), 2)
									FROM 
											 investments i 
											 ,opportunities o 
											 ,users u 
											 ,balance_history bh 
									WHERE 
											 i.user_id = p_user_id 
											AND u.id =			i.user_id 
											AND i.is_canceled	= 0 
											AND (o.scheduled = 1 OR i.is_like_hourly  = 1 )
                      AND o.opportunity_type_id = 1
											AND i.opportunity_id = o.id 
					            AND bh.user_id = i.user_id 
					            AND bh.key_value = i.id 
					            AND bh.command = 10  -- LOG_BALANCE_INSERT_INVESTMENT
					            AND bh.time_created >= u.time_created 
					            AND i.time_created	>= u.time_created 
                      and rownum < 41
									ORDER BY 
											i.ID  ;

  risk_appetite NUMBER := 0;
  BEGIN
       OPEN calculate;
       FETCH calculate INTO risk_appetite;
       CLOSE calculate;
       RETURN risk_appetite;
  END;
  
PROCEDURE GET_COPYOP_INV_ODDS(O_DATA OUT SYS_REFCURSOR) IS
 BEGIN
   OPEN O_DATA FOR
     SELECT A.ODDS_WIN, A.ODDS_LOSE
       FROM COPYOP.CFG_COPIED_INV_ODDS A
      WHERE ROWNUM = 1
      ORDER BY A.ID DESC;
 END GET_COPYOP_INV_ODDS;

end;
/