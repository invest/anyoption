create or replace package pkg_audit is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-06-02 09:39:14
  -- Purpose : audit and check functions

  -- DIFfer functions. Return FALSE if both are equal or both are NULL
  function dif
  (
    i_val1 in number
   ,i_val2 in number
  ) return boolean;

  function dif
  (
    i_val1 in date
   ,i_val2 in date
  ) return boolean;

  function dif
  (
    i_val1 in varchar2
   ,i_val2 in varchar2
  ) return boolean;

end pkg_audit;
/
create or replace package body pkg_audit is

  function dif
  (
    i_val1 in number
   ,i_val2 in number
  ) return boolean is
  begin
    if i_val1 = i_val2
       or (i_val1 is null and i_val2 is null)
    then
      return false;
    else
      return true;
    end if;
  end dif;

  function dif
  (
    i_val1 in date
   ,i_val2 in date
  ) return boolean is
  begin
    if i_val1 = i_val2
       or (i_val1 is null and i_val2 is null)
    then
      return false;
    else
      return true;
    end if;
  end dif;

  function dif
  (
    i_val1 in varchar2
   ,i_val2 in varchar2
  ) return boolean is
  begin
    if i_val1 = i_val2
       or (i_val1 is null and i_val2 is null)
    then
      return false;
    else
      return true;
    end if;
  end dif;

end pkg_audit;
/
