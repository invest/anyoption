create or replace package pkg_maintanance is

  -- Author  : VICTORS
  -- Created : 2016-08-15 08:00:58
  -- Purpose : for maintanance procedures

  procedure maint_stats;

end pkg_maintanance;
/
create or replace package body pkg_maintanance is

  procedure maint_stats is
    l_owner  varchar2(30) := 'ETRADER';
    l_tables string_table := string_table('INVESTMENTS'
                                         ,'TRANSACTIONS'
                                         ,'BALANCE_HISTORY'
                                         ,'GOLDEN_MINUTES_VIEWING'
                                         ,'LOG'
                                         ,'LOGINS'
                                         ,'POPULATION_ENTRIES_HIST'
                                         ,'WRITERS_COMMISSION_INV');
  begin
    --- maintain incremental stats on partitioned tables
    -- executed by dbms_scheduler job
    -- to be executed daily so that execution time stays short
    for i in 1 .. l_tables.count
    loop
      dbms_application_info.set_action(l_tables(i));
      dbms_stats.gather_table_stats(l_owner, l_tables(i), no_invalidate => false, cascade => false);
    end loop;
  end maint_stats;

end pkg_maintanance;
/
