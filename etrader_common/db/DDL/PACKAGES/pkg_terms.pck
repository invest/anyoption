
create or replace package pkg_terms is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-13 10:36:34
  -- Purpose : 

  procedure get_onetouch_invlimit
  (
    o_max_value   out number
   ,i_currency_id in number
   ,i_skin_id     in number
  );

  procedure get_min_first_deposit
  (
    o_min_first_deposit out number
   ,i_currency_id       in number
   ,i_skin_id           in number
  );
  
  PROCEDURE update_file
  (
    i_file_id      IN NUMBER
   ,i_file_title   IN VARCHAR2
   ,i_file_part_id IN NUMBER
   ,i_is_active    IN NUMBER
   ,i_platform_id  IN NUMBER
   ,i_skin_id      IN NUMBER
  );

end pkg_terms;
/
create or replace package body pkg_terms is

  procedure get_onetouch_invlimit
  (
    o_max_value   out number
   ,i_currency_id in number
   ,i_skin_id     in number
  ) is
    l_currency_id number;
  begin
    if i_currency_id = 0
    then
      select default_currency_id into l_currency_id from skins where id = i_skin_id;
    else
      l_currency_id := i_currency_id;
    end if;
  
    select value
    into   o_max_value
    from   (select ilgcmax.value
            from   investment_limits il
            join   investment_limit_types ilt
            on     il.limit_type_id = ilt.id
            join   investment_limit_group_curr ilgcmin
            on     il.min_limit_group_id = ilgcmin.investment_limit_group_id
            join   investment_limit_group_curr ilgcmax
            on     il.max_limit_group_id = ilgcmax.investment_limit_group_id
            where  ilgcmin.currency_id = l_currency_id
            and    ilgcmax.currency_id = l_currency_id
            and    il.is_active = 1
            and    ilt.is_active = 1
            and    il.opportunity_type_id = 2
            and    (il.start_date is null or il.end_date is null or sysdate between il.start_date and il.end_date)
            and    (il.start_time is null or il.end_time is null or
                  trunc(sysdate, 'mi') >= to_date(to_char(sysdate, 'yyyy-mm-dd ') || il.start_time, 'yyyy-mm-dd hh24:mi') or
                  trunc(sysdate, 'mi') < to_date(to_char(sysdate, 'yyyy-mm-dd ') || il.end_time, 'yyyy-mm-dd hh24:mi'))
            order  by ilt.priority)
    where  rownum <= 1;
  end get_onetouch_invlimit;

  procedure get_min_first_deposit
  (
    o_min_first_deposit out number
   ,i_currency_id       in number
   ,i_skin_id           in number
  ) is
  begin
  
    select l.min_first_deposit
    into   o_min_first_deposit
    from   skins s
    join   skin_currencies a
    on     a.skin_id = s.id
    join   limits l
    on     l.id = a.limit_id
    where  s.id = i_skin_id
    and    a.currency_id = case
             when i_currency_id = 0 then
              s.default_currency_id
             else
              i_currency_id
           end;
  
  end get_min_first_deposit;
  
   PROCEDURE update_file
  (
    i_file_id      IN NUMBER
   ,i_file_title   IN VARCHAR2
   ,i_file_part_id IN NUMBER
   ,i_is_active    IN NUMBER
   ,i_platform_id  IN NUMBER
   ,i_skin_id      IN NUMBER
  ) IS
  BEGIN
  
    UPDATE terms_files SET title = i_file_title WHERE id = i_file_id;
  
    UPDATE terms t
    SET t.is_active = i_is_active
    WHERE t.skin_id = i_skin_id
    AND t.part_id = i_file_part_id
    AND t.platform_id = i_platform_id;
  
  END update_file;

end pkg_terms;
/
