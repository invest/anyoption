create or replace package pkg_investments is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-05 10:48:36
  -- Purpose : InvestmentsDAOBase

  procedure get_by_user_id
  (
    o_data       out sys_refcursor
   ,i_user_id    in number
   ,i_period     in number
   ,i_writer_id  in number
   ,i_fromdate   in date
   ,i_todate     in date
   ,i_is_settled in number
   ,i_market_id  in number
   ,i_group_id   in number
   ,i_skin_id    in number
   ,i_pagesize   in number
   ,i_startrow   in number
  );

end pkg_investments;
/
create or replace package body pkg_investments is

  -- Logic is extracted from /anyoption_common_merge/src/com/anyoption/common/daos/InvestmentsDAOBase.java, getByUser() (rev. 40274)
  -- %param o_data 
  -- %param i_user_id            Mandatory!
  -- %param i_period 
  -- %param i_writer_id
  -- %param i_fromdate 
  -- %param i_todate 
  -- %param i_is_settled         Mandatory!
  -- %param i_market_id 
  -- %param i_group_id 
  -- %param i_skin_id 
  -- %param i_pagesize 
  -- %param i_startrow 
  procedure get_by_user_id
  (
    o_data       out sys_refcursor
   ,i_user_id    in number
   ,i_period     in number
   ,i_writer_id  in number
   ,i_fromdate   in date
   ,i_todate     in date
   ,i_is_settled in number
   ,i_market_id  in number
   ,i_group_id   in number
   ,i_skin_id    in number
   ,i_pagesize   in number
   ,i_startrow   in number
  ) is
    l_format   varchar2(50) := 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM';
    l_fromdate date;
    l_todate   date;
    l_sysdate  date := sysdate;
  begin
  
    if i_fromdate is not null
       and i_todate is not null
    then
      l_fromdate := i_fromdate;
      l_todate   := i_todate;
    else
      if i_period is not null
      then
        case
          when i_period = 1 --- one day period
           then
            l_fromdate := l_sysdate - 1;
          when i_period = 2 --- one week period
           then
            l_fromdate := l_sysdate - 7;
          when i_period = 3 --- one month period
           then
            l_fromdate := add_months(l_sysdate, -1);
        end case;
      
        l_todate := l_sysdate;
      end if;
    end if;
  
    if (l_fromdate is null or l_todate is null)
    then
      raise_application_error(-20000
                             ,'Cannot determine time range: i_period:' || i_period || '; i_fromdate:' || to_char(i_fromdate, l_format) ||
                              '; i_todate:' || to_char(i_todate, l_format) || ';');
    end if;
  
    if (i_user_id is null or i_is_settled is null)
    then
      raise_application_error(-20001
                             ,'Mandatory field(s) not specified: i_user_id:' || i_user_id || '; i_is_settled:' || i_is_settled || ';');
    end if;
  
    open o_data for
      select *
      from   (select x.*, rownum rn
              from   (select i.*
                            ,m.display_name market_name
                            ,m.id market_id
                            ,int.display_name type_name
                            ,u.user_name username
                            ,u.first_name firstname
                            ,u.last_name lastname
                            ,to_char(o.time_est_closing, l_format) time_est_closing
                            ,to_char(o.time_last_invest, l_format) time_last_invest
                            ,to_char(o.time_act_closing, l_format) time_act_closing
                            ,to_char(o.time_first_invest, l_format) time_first_invest
                            ,o.closing_level
                            ,o.scheduled scheduled
                            ,u.currency_id
                            ,iref.id rolled_inv_id
                            ,o.current_level event_level
                            ,b.display_name bonus_display_name
                            ,o.opportunity_type_id
                            ,o.is_open_graph
                            ,(select 1
                              from   opportunity_templates ot
                              where  ot.market_id = m.id
                              and    ot.is_active = 1
                              and    ot.scheduled = 1
                              and    rownum <= 1) hashourly
                      from   investments i
                      join   opportunities o
                      on     i.opportunity_id = o.id
                      join   opportunity_types opt
                      on     o.opportunity_type_id = opt.id
                      join   markets m
                      on     o.market_id = m.id
                      join   investment_types int
                      on     i.type_id = int.id
                      join   users u
                      on     u.id = i.user_id
                      
                      left   join bonus_users bu
                      on     bu.id = i.bonus_user_id
                      and    i.bonus_odds_change_type_id > 0
                      left   join bonus b
                      on     bu.bonus_id = b.id
                      left   join investments iref
                      on     iref.reference_investment_id = i.id
                      
                      where  (i.time_created >= l_fromdate and i.time_created < l_todate)
                            
                      and    i.user_id = i_user_id
                      and    i.is_settled = i_is_settled
                            
                      and    ((i_writer_id in (10000, 20000) and opt.product_type_id not in (4, 2, 3)) or
                            (i_writer_id not in (10000, 20000) and opt.product_type_id != 4))
                            
                      and    (i_market_id is null or m.id = i_market_id)
                            
                      and    (i_group_id is null or exists (select 1
                                                            from   skin_market_group_markets smgm
                                                            join   skin_market_groups smg
                                                            on     smgm.skin_market_group_id = smg.id
                                                            where  smgm.market_id = m.id
                                                            and    smg.market_group_id = i_group_id
                                                            and    smg.skin_id = i_skin_id))
                      
                      order  by i.time_created desc) x)
      where  (i_pagesize is null or (rn > i_startrow and rn <= i_startrow + i_pagesize));
  
  end get_by_user_id;

end pkg_investments;
/
