create or replace package pkg_transactions_backend is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-03 12:25:41
  -- Purpose : Procedures used by backend

  -- %param o_transactions
  -- %param i_fromdate               Filter by transactions.time_created or transactions.time_settled, depends on i_datetype
  -- %param i_todate                 Filter by transactions.time_created or transactions.time_settled, depends on i_datetype
  -- %param i_datetype               Filter by time_created(1) or time_settled(2)
  -- %param i_transaction_id
  -- %param i_user_id
  -- %param i_user_classes
  -- %param i_business_cases
  -- %param i_countries
  -- %param i_currencies
  -- %param i_compaigns
  -- %param i_transaction_statuses
  -- %param i_system_writers
  -- %param i_has_balance            Returns only users with balance=0 (i_has_balance=0) or balance>0 (i_has_balance=1)
  -- %param i_clearing_providers
  -- %param i_skins
  -- %param i_page_number            Page number to be returned
  -- %param i_rows_per_page          Number of rows per page
  -- %param i_transaction_types
  -- %param i_gateway
  -- %param i_cc_bin
  -- %param i_cc_last4               TODO; Last 4 digits of the credit card number. Must add new column to credit_cards or decrypt card number in DB.
  -- %param i_epg_id
  -- %param i_trans_class_types      Combining Direct banking transaction types and payment methods ({17,2},{17,3},{17,4})
  -- %param i_receipt_number
  -- %param i_j4postponed
  -- %param i_include_be_writers     Used in combination with i_system_writers for whether to include non-system writers or not (1,0)
  -- %param i_sales_type
  -- %param i_deposit_source
  procedure get_transactions
  (
    o_transactions         out sys_refcursor
   ,i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_user_classes         in number
   ,i_business_cases       in number_table
   ,i_countries            in number_table
   ,i_currencies           in number_table
   ,i_compaigns            in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_has_balance          in number
   ,i_clearing_providers   in number_table
   ,i_skins                in number_table
   ,i_page_number          in number
   ,i_rows_per_page        in number
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_types    in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_include_be_writers   in number
   ,i_reload_transaction   in number default 0
   ,i_sales_type           in number
   ,i_deposit_source       in number
   ,i_amount_from          in number
   ,i_amount_to            in number
   ,i_tran_ids             in number_table DEFAULT NULL
   ,i_was_rerouted         in number DEFAULT NULL
   ,i_is_three_d           in number DEFAULT NULL
   ,i_cc_countries         in number_table DEFAULT NULL
  );

  -- All cursors should return two columns with names ID, VALUE
  -- %param o_departments
  -- %param o_platforms
  -- %param o_skin_business_cases
  -- %param o_user_classes
  -- %param o_countries
  -- %param o_currencies
  -- %param o_transaction_statuses
  -- %param o_transaction_class_types
  -- %param o_clearing_providers
  -- %param o_transaction_types
  -- %param o_transaction_type_class
  -- %param o_skins
  -- %param o_compaigns
  -- %param o_pending_withdraw_types
  procedure load_filters
  (
    o_platforms               out sys_refcursor
   ,o_skin_business_cases     out sys_refcursor
   ,o_user_classes            out sys_refcursor
   ,o_countries               out sys_refcursor
   ,o_transaction_statuses    out sys_refcursor
   ,o_transaction_class_types out sys_refcursor
   ,o_clearing_providers      out sys_refcursor
   ,o_transaction_types       out sys_refcursor
   ,o_transaction_type_class  out sys_refcursor
   ,o_compaigns               out sys_refcursor
   ,o_system_writers          out sys_refcursor
   ,o_pending_withdraw_types  out sys_refcursor
  );

  -- %param i_transaction_id
  -- %param i_writer_id
  procedure cancel_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
   ,i_status_id      in number
  );

  procedure approve_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
  );

  -- %param i_writer_id
  -- %param i_status_id
  -- %param i_time_created
  -- %param i_description
  -- %param i_utc_offset_created
  -- %param i_issue_id
  -- %param i_arn
  -- %param i_time_pos
  -- %param i_time_chb
  -- %param i_time_rep
  -- %param i_time_settled
  -- %param i_state
  -- %param i_transaction_id
  procedure create_chargeback
  (
    i_writer_id          in number
   ,i_status_id          in number
   ,i_time_created       in date
   ,i_description        in varchar2
   ,i_utc_offset_created in varchar2
   ,i_issue_id           in number
   ,i_arn                in varchar2
   ,i_time_pos           in date
   ,i_time_chb           in date
   ,i_time_rep           in date
   ,i_time_settled       in date
   ,i_state              in number
   ,i_transaction_id     in number
  );

  procedure manage_trans_postponed
  (
    i_ppn_id          in number
   ,i_ppn_num_of_days in number
   ,i_ppn_trans_id    in number
   ,i_writer_id       in number
  );

  FUNCTION update_user_balance(
    in_user_id          IN NUMBER,
    in_amount           IN NUMBER,
    in_transaction_type IN NUMBER,
    in_rate             IN NUMBER,
    in_comment          IN VARCHAR2,
    in_utc_offset       IN VARCHAR2,
    in_description      IN VARCHAR2)
  RETURN NUMBER;

  procedure get_rerouting_trans
  (
    o_transactions       out sys_refcursor
   ,i_transaction_id     IN NUMBER
  );

end pkg_transactions_backend;
/
create or replace package body pkg_transactions_backend is

  procedure get_transactions_final
  (
    o_transactions   out sys_refcursor
   ,i_user_classes   in number
   ,i_business_cases in number_table
   ,i_countries      in number_table
   ,i_currencies     in number_table
   ,i_compaigns      in number
   ,i_has_balance    in number
   ,i_skins          in number_table
   ,i_page_number    in number
   ,i_rows_per_page  in number
   ,i_cc_last4       in varchar2
   ,i_sales_type     in number
   ,i_deposit_source in number
   ,i_amount_from    in number
   ,i_amount_to      in number
   ,i_was_rerouted   in number
   ,i_is_three_d     in number
   ,i_cc_countries   in number_table
  ) is
    l_deposit_trns  number_table;
    l_withdraw_trns number_table;
  begin
    select t.id
    bulk   collect
    into   l_deposit_trns
    from   transaction_types t
    join   transaction_class_types c
    on     c.id = t.class_type
    where  c.code in ('C_TYPE_REAL_DEPOSITS', 'C_TYPE_REVERSE_WITHDRAWS', 'C_TYPE_ADMIN_DEPOSITS')
    or     (c.code in ('C_TYPE_FIX_BALANCE') and t.code in ('TRANS_TYPE_FIX_BALANCE_DEPOSIT'))
    or     (c.code in ('C_TYPE_INTERNALS') and t.code in ('TRANS_TYPE_CC_DEPOSIT_REROUTE', 'TRANS_TYPE_MAINTENANCE_FEE_CANCEL'))
    or     (c.code in ('C_TYPE_BONUS_DEPOSITS') and t.code in ('TRANS_TYPE_BONUS_DEPOSIT'));

    select t.id
    bulk   collect
    into   l_withdraw_trns
    from   transaction_types t
    join   transaction_class_types c
    on     c.id = t.class_type
    where  c.code in
           ('C_TYPE_REAL_WITHDRAWALS', 'C_TYPE_ADMIN_WITHDRAWALS', 'C_TYPE_FEES', 'C_TYPE_DEPOSIT_BY_COMPANY', 'C_TYPE_ADMIN_WITHDRAWALS')
    or     (c.code in ('C_TYPE_FIX_BALANCE') and t.code in ('TRANS_TYPE_FIX_BALANCE_WITHDRAW', 'TRANS_TYPE_FIX_NEGATIVE_BALANCE'))
    or     (c.code in ('C_TYPE_INTERNALS') and
          t.code in ('TRANS_TYPE_INTERNAL_CREDIT', 'TRANS_TYPE_SPLIT_CREDIT_AND_CFT', 'TRANS_TYPE_CUP_INTERNAL_CREDIT'))
    or     (c.code in ('C_TYPE_BONUS_WITHRAWALS') and t.code in ('TRANS_TYPE_BONUS_WITHDRAW'));

    open o_transactions for
      select v.total_count
            ,v.sum_deposits
            ,v.sum_withdraws
            ,v.count_deposits
            ,v.count_withdraws
            ,v.transaction_id
            ,v.user_id
            ,v.credit_card_id
            ,v.type_id
            ,v.time_created
            ,v.amount
            ,v.status_id
            ,v.description
            ,v.writer_id
            ,v.ip
            ,v.time_settled
            ,v.comments
            ,v.processed_writer_id
            ,v.cheque_id
            ,v.reference_id
            ,v.wire_id
            ,v.charge_back_id
            ,v.receipt_num
            ,v.auth_number
            ,v.xor_id_authorize
            ,v.xor_id_capture
            ,v.fee_cancel
            ,v.is_accounting_approved
            ,v.utc_offset_settled
            ,v.utc_offset_created
            ,v.rate
            ,v.clearing_provider_id
            ,v.fee_cancel_by_admin
            ,v.credit_amount
            ,v.deposit_reference_id
            ,v.splitted_reference_id
            ,v.deposit_reference_id
            ,v.is_credit_withdrawal
            ,v.payment_type_id
            ,pm.description payment_type_desc
            ,tt.class_type
            ,v.bonus_user_id
            ,v.paypal_email
            ,v.temp_bonus_user_id
            ,v.envoy_account_num
            ,v.internals_amount
            ,v.pixel_run_time
            ,v.moneybookers_email
            ,v.webmoney_purse
            ,v.update_by_admin
            ,v.acquirer_response_id
            ,v.is_rerouted
            ,v.rerouting_transaction_id
            ,v.is_manual_routing
            ,v.selector_id
            ,v.login_id
            ,v.wd_fee_exempt_writer_id
            ,v.rn
            ,w.user_name writer_name
            ,w1.user_name processed_writer_name
            ,v.user_name
            ,v.user_country_id
            ,v.is_credit_withdrawal
            ,cu.country_name
            ,v.cc_number
            ,case
               when v.type_id member of l_deposit_trns then
                1
               when v.type_id member of l_withdraw_trns then
                2
               else
                0
             end deposit_or_withdraw
            ,ct.name cc_name
            ,ct.description cc_type_by_bin
            ,cn.country_name cc_country_name
            ,v.cc_is_allowed
            ,v.cc_utc_offset_created
            ,v.cc_utc_offset_modified
            ,v.cc_exp_year
            ,v.cc_holder_name
            ,v.cc_holder_id_num
            ,v.cc_time_modified
            ,v.cc_time_created
            ,v.cc_exp_month
            ,v.cc_type_id_by_bin
            ,v.cc_is_visible
            ,v.bins_category
            ,v.bins_bank
            ,v.bins_card_payment_type
            ,v.first_name
            ,v.last_name
            ,v.time_house_result
            ,v.time_turnover
            ,v.time_manualy
            ,v.paypal_email
            ,bc.description business_case_desc
            ,v.user_skin_name
            ,v.user_currency_id
            ,v.moneybookers_email
            ,v.acquirer_response_id
            ,po.id ppn_id
            ,po.number_of_days ppn_number_of_days
            ,po.writer_id ppn_writer_id
            ,po.time_updated ppn_time_updated
            ,w.beneficiary_name
            ,w.account_num
            ,w.swift
            ,w.iban
            ,w.bank_name
            ,w.branch
            ,w.branch_name
            ,w.branch_address
            ,qm.text_answer qm_text_answer
            ,qma.answer_name qma_answer_name
            ,cp.name clearing_provider_name
            ,(select count(*)
              from   transactions
              where  type_id = 10
              and    user_id = v.user_id
              and    status_id in (4, 9, 17)
              and    credit_card_id = v.credit_card_id
              and    is_credit_withdrawal = 1
              and    rownum <= 1) have_withdrawals
            ,v.is_3d
      from   (select row_number() over(order by t.time_created desc, t.time_settled desc, t.id desc) rn
                    ,count(*) over() total_count
                    ,nvl(sum(case
                               when t.type_id member of l_deposit_trns then
                                t.amount * t.rate
                             end) over()
                        ,0) sum_deposits
                    ,nvl(sum(case
                               when t.type_id member of l_withdraw_trns then
                                t.amount * t.rate
                             end) over()
                        ,0) sum_withdraws
                    ,count(case
                             when t.type_id member of l_deposit_trns then
                              1
                           end) over() count_deposits
                    ,count(case
                             when t.type_id member of l_withdraw_trns then
                              1
                           end) over() count_withdraws
                    ,u.user_name
                    ,u.country_id user_country_id
                    ,d.cc_number
                    ,d.type_id cc_type_id
                    ,d.country_id cc_country_id
                    ,d.is_allowed cc_is_allowed
                    ,d.utc_offset_created cc_utc_offset_created
                    ,d.utc_offset_modified cc_utc_offset_modified
                    ,d.exp_year cc_exp_year
                    ,d.holder_name cc_holder_name
                    ,d.holder_id_num cc_holder_id_num
                    ,d.time_modified cc_time_modified
                    ,d.time_created cc_time_created
                    ,d.exp_month cc_exp_month
                    ,d.type_id_by_bin cc_type_id_by_bin
                    ,d.is_visible cc_is_visible
                    ,t.*
                    ,t.id transaction_id
                    ,u.first_name
                    ,u.last_name
                    ,u.time_sc_house_result time_house_result
                    ,u.time_sc_turnover time_turnover
                    ,u.time_sc_manualy time_manualy
                    ,s.business_case_id
                    ,s.name user_skin_name
                    ,u.currency_id user_currency_id
                    ,bins.category bins_category
                    ,bins.bank bins_bank
                    ,bins.card_payment_type bins_card_payment_type
              from   gt_transactions t
              join   users u
              on     u.id = t.user_id
              join   skins s
              on     s.id = u.skin_id
              left   join credit_cards d
              on     d.id = t.credit_card_id
              left   join transactions_issues ti
              on     t.id = ti.transaction_id
              left   join bins bins
              on     d.bin = bins.from_bin
              -- Please before change anything below or above, sync with PAVEL TABAKOV!!!
              where  (i_user_classes is null or (i_user_classes = 12 and u.class_id in (1, 2)) or
                     (i_user_classes <> 12 and u.class_id = i_user_classes))

              and    (i_business_cases is null or s.business_case_id member of i_business_cases)

              and    (i_countries is null or u.country_id member of i_countries)

              and    (i_currencies is null or u.currency_id member of i_currencies)

              and    (i_compaigns is null or exists (select 1
                                                     from   marketing_combinations co
                                                     join   marketing_campaigns a
                                                     on     a.id = co.campaign_id
                                                     where  co.id = u.combination_id
                                                     and    a.id = i_compaigns))

              and    (i_skins IS NULL OR u.skin_id member of i_skins)

              and    (i_cc_last4 is null or d.cc_number_last_4_digits = i_cc_last4)

              and    (i_has_balance is null or ((i_has_balance = 1 and u.balance > 0) or (i_has_balance = 0 and u.balance <= 0)))
              and    (i_amount_from is null or t.amount * t.rate >= i_amount_from)
              and    (i_amount_to is null or t.amount * t.rate <= i_amount_to)
              and    (i_sales_type is null or ((i_sales_type = 1 and u.first_deposit_id = t.id) or (i_sales_type = 2 and u.first_deposit_id <> t.id)))
              and    (i_deposit_source is null or ((i_deposit_source = 1 and ti.transaction_id IS NOT NULL) or (i_deposit_source = 2 and ti.transaction_id IS NULL)))
              and    (i_was_rerouted IS NULL OR ((i_was_rerouted = 1 AND t.is_rerouted = 1) OR (i_was_rerouted = 0 AND t.is_rerouted = 0)))
              and    (i_is_three_d IS NULL OR ((i_is_three_d = 1 AND t.is_3d = 1) OR (i_is_three_d = 0 AND t.is_3d = 0)))
              and    (i_cc_countries is null or d.country_id member of i_cc_countries)
              
              ) v
      join   writers w
      on     w.id = v.writer_id
      left   join credit_card_types ct
      on     ct.id = v.cc_type_id
      left   join countries cn
      on     cn.id = v.cc_country_id
      join   countries cu
      on     cu.id = v.user_country_id
      join   skin_business_cases bc
      on     bc.id = v.business_case_id
      left   join transaction_postponed po
      on     po.transaction_id = v.transaction_id
      left   join payment_methods pm
      on     v.payment_type_id = pm.id
      join   transaction_types tt
      on     tt.id = v.type_id
      left join wires w
      on     v.wire_id = w.id
      left join writers w1
      on     w1.id = v.processed_writer_id
      LEFT JOIN qm_user_answers qm
      ON     qm.transaction_id = v.id
      LEFT JOIN qm_answers qma
      ON     qm.answer_id = qma.id
      LEFT JOIN clearing_providers cp
      ON     v.clearing_provider_id = cp.id
      where  v.rn > (i_page_number - 1) * i_rows_per_page
      and    v.rn <= i_page_number * i_rows_per_page
      order  by v.time_created desc, v.time_settled desc, v.transaction_id desc;

  end get_transactions_final;

 procedure get_transactions_by_trnid_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.id = i_transaction_id

      and    (i_user_id is null or t.user_id = i_user_id)

      and    (i_datetype is null or (i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate))

      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_transaction_types is null or t.type_id member of i_transaction_types)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      and    (i_search_mask = 0 or ((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));

  end get_transactions_by_trnid_tmp;
  
 PROCEDURE get_transactions_byid_list_tmp(i_tran_ids IN number_table) IS
 BEGIN
 
   INSERT INTO gt_transactions
     SELECT t.* FROM transactions t WHERE t.id MEMBER OF i_tran_ids;
 
 END get_transactions_byid_list_tmp;

  procedure get_transactions_by_userid_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_user_id              in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.user_id = i_user_id

      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate))

      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_transaction_types is null or t.type_id member of i_transaction_types)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_cc_last4 is null or exists (select 1
                                            from   credit_cards
                                            where  id = t.credit_card_id
                                            and    cc_number_last_4_digits = i_cc_last4))

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      and    (i_search_mask = 0 or ((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));

  end get_transactions_by_userid_tmp;

  procedure get_transactions_by_status_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.status_id in (select /*+ cardinality(a,1) */
                              column_value
                             from   table(i_transaction_statuses) a)

      and    ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate))

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_transaction_types is null or t.type_id member of i_transaction_types)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_cc_last4 is null or exists (select 1
                                            from   credit_cards
                                            where  id = t.credit_card_id
                                            and    cc_number_last_4_digits = i_cc_last4))

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      and    (i_search_mask = 0 or ((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));

  end get_transactions_by_status_tmp;

  procedure get_transactions_by_type_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   transactions t
      where ((i_datetype = 1 and t.time_created >= i_fromdate and t.time_created < i_todate) or
            (i_datetype = 2 and t.time_settled >= i_fromdate and t.time_settled < i_todate))

      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_cc_last4 is null or exists (select 1
                                            from   credit_cards
                                            where  id = t.credit_card_id
                                            and    cc_number_last_4_digits = i_cc_last4))

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      AND    (((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)) or
            t.type_id in (select column_value from table(i_transaction_types) a));




  end get_transactions_by_type_tmp;

  procedure get_transactions_by_timec_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   transactions t
      where  t.time_created >= i_fromdate
      and    t.time_created < i_todate

      and    (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_transaction_types is null or t.type_id member of i_transaction_types)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_cc_last4 is null or exists (select 1
                                            from   credit_cards
                                            where  id = t.credit_card_id
                                            and    cc_number_last_4_digits = i_cc_last4))

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      and    (i_search_mask = 0 or ((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));

  end get_transactions_by_timec_tmp;

  procedure get_transactions_by_times_tmp
  (
    i_fromdate             in date
   ,i_todate               in date
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_clearing_providers   in number_table
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_internal in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_search_mask          in number
  ) is
  begin

    insert into gt_transactions
      select t.*
      from   (select m.*
              from   transactions m
              where  m.time_settled >= i_fromdate
              and    m.time_settled < i_todate
              and    m.time_created >= i_fromdate - 20
              and    m.time_created < i_todate
              and    m.time_settled - m.time_created <= 20
              union all
              select n.*
              from   transactions n
              where  case
                       when n.time_settled - n.time_created > 20 then
                        n.time_settled
                     end >= i_fromdate
              and    case
                      when n.time_settled - n.time_created > 20 then
                       n.time_settled
                    end < i_todate) t

      where  (i_transaction_statuses is null or t.status_id member of i_transaction_statuses)

      and    (i_system_writers is null or t.writer_id member of i_system_writers)

      and    (i_clearing_providers is null or t.clearing_provider_id member of i_clearing_providers)

      and    (i_transaction_types is null or t.type_id member of i_transaction_types)

      and    (i_cc_bin is null or exists (select 1
                                          from   credit_cards
                                          where  id = t.credit_card_id
                                          and    bin = i_cc_bin))

      and    (i_trans_class_internal is null or t.type_id member of i_trans_class_internal)

      and    (i_receipt_number is null or t.receipt_num = i_receipt_number)

      and    (i_epg_id is null or t.xor_id_authorize = i_epg_id)

      and    (i_cc_last4 is null or exists (select 1
                                            from   credit_cards
                                            where  id = t.credit_card_id
                                            and    cc_number_last_4_digits = i_cc_last4))

      and    (i_j4postponed is null or
            (i_j4postponed = 0 and not exists (select 1 from transaction_postponed where transaction_id = t.id)) or
            (i_j4postponed = 1 and exists (select 1 from transaction_postponed where transaction_id = t.id)))

      and    (i_search_mask = 0 or ((bitand(i_search_mask, 1) = 1 and t.type_id = 17 and t.payment_type_id = 2) or
            (bitand(i_search_mask, 2) = 2 and t.type_id = 17 and t.payment_type_id = 3) or
            (bitand(i_search_mask, 4) = 4 and t.type_id = 17 and t.payment_type_id = 4)));

  end get_transactions_by_times_tmp;

  procedure get_transactions
  (
    o_transactions         out sys_refcursor
   ,i_fromdate             in date
   ,i_todate               in date
   ,i_datetype             in number
   ,i_transaction_id       in number
   ,i_user_id              in number
   ,i_user_classes         in number
   ,i_business_cases       in number_table
   ,i_countries            in number_table
   ,i_currencies           in number_table
   ,i_compaigns            in number
   ,i_transaction_statuses in number_table
   ,i_system_writers       in number_table
   ,i_has_balance          in number
   ,i_clearing_providers   in number_table
   ,i_skins                in number_table
   ,i_page_number          in number
   ,i_rows_per_page        in number
   ,i_transaction_types    in number_table
   ,i_gateway              in number
   ,i_cc_bin               in number
   ,i_cc_last4             in varchar2
   ,i_epg_id               in varchar2
   ,i_trans_class_types    in number_table
   ,i_receipt_number       in number
   ,i_j4postponed          in number
   ,i_include_be_writers   in number
   ,i_reload_transaction   in number default 0-- not in use anymore (must be removed after deploy on BGTEST)
   ,i_sales_type           in number
   ,i_deposit_source       in number
   ,i_amount_from          in number
   ,i_amount_to            in number
   ,i_tran_ids             in number_table DEFAULT NULL 
   ,i_was_rerouted         in number DEFAULT NULL
   ,i_is_three_d           in number DEFAULT NULL
   ,i_cc_countries         in number_table DEFAULT NULL
  ) is
    l_noidx_trn_types      number_table := number_table(1, 13, 10, 12, 16, 47, 6, 17, 38, 14, 32, 2, 18, 33); -- if trns.type_id is in this list, do not use index
    l_noidx_trn_statuses   number_table := number_table(2, 3, 6); -- if trns.status_id is in this list, do not use index
    l_tmp_types            number_table;
    l_tmp_statuses         number_table;
    l_trans_class_internal number_table;
    l_transaction_types    number_table;
    l_search_mask          number := 0;
    l_tmp                  number;
    l_tmp_writers          number_table;
  begin
    select count(*) into l_tmp from gt_transactions where rownum <= 1;
    if l_tmp > 0
    then
      raise_application_error(-20010, 'Global temporary table not empty.');
    end if;

    if i_datetype is null
    then
      raise_application_error(-20011, 'Period type is not specified (i_datetype is null)');
    end if;

    if i_trans_class_types is not null
    then
      select id bulk collect into l_trans_class_internal from transaction_types where class_type member of i_trans_class_types;
    end if;

    if i_transaction_types is not null
    then
      --l_transaction_types := number_table();

      -- this is to extract fake negative transaction types. This hack was introduced to be able to filter
      -- types "Direct 24", "Giropay", "Direct EPS"
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=2 PAYMENT_TYPE_DIRECT24 from PAYMENT_METHODS
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=3 PAYMENT_TYPE_GIROPAY from PAYMENT_METHODS
      -- ID=17 DIRECT_BANK_DEPOSIT from TRANSACTION_TYPES + ID=4 PAYMENT_TYPE_EPS from PAYMENT_METHODS
      for i in 1 .. i_transaction_types.count
      loop
        if i_transaction_types(i) > 0
        then
          if l_transaction_types is null
          then
            l_transaction_types := number_table(i_transaction_types(i));
          else
            l_transaction_types.extend;
            l_transaction_types(l_transaction_types.last) := i_transaction_types(i);
          end if;
        else
          case i_transaction_types(i)
            when -172 then
              l_search_mask := l_search_mask + 1;
            when -173 then
              l_search_mask := l_search_mask + 2;
            when -174 then
              l_search_mask := l_search_mask + 4;
          end case;
        end if;
      end loop;
    end if;

    l_tmp_types    := l_transaction_types multiset intersect l_noidx_trn_types;
    l_tmp_statuses := i_transaction_statuses multiset intersect l_noidx_trn_statuses;

    if i_system_writers is not null
       or i_include_be_writers = 1
    then
      if i_include_be_writers = 1
      then
        select id bulk collect into l_tmp_writers from writers where dept_id not in (9);
      else
        l_tmp_writers := number_table();
      end if;

      if i_system_writers is not null
      then
        for i in 1 .. i_system_writers.count
        loop
          l_tmp_writers.extend;
          l_tmp_writers(l_tmp_writers.last) := i_system_writers(i);
        end loop;
      end if;
    end if;
    
    IF i_tran_ids is not null
    then
      -- get transactions list for rerouting
      get_transactions_byid_list_tmp(i_tran_ids);
      
    elsif i_transaction_id is not null
    then
      -- will access transactions table by its PK
      get_transactions_by_trnid_tmp(i_fromdate
                                   ,i_todate
                                   ,i_datetype
                                   ,i_transaction_id
                                   ,i_user_id
                                   ,i_transaction_statuses
                                   ,l_tmp_writers
                                   ,i_clearing_providers
                                   ,l_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_mask);
    elsif i_user_id is not null
    then
      -- will access transactions table by index on user_id
      get_transactions_by_userid_tmp(i_fromdate
                                    ,i_todate
                                    ,i_datetype
                                    ,i_user_id
                                    ,i_transaction_statuses
                                    ,l_tmp_writers
                                    ,i_clearing_providers
                                    ,l_transaction_types
                                    ,i_gateway
                                    ,i_cc_bin
                                    ,i_cc_last4
                                    ,i_epg_id
                                    ,l_trans_class_internal
                                    ,i_receipt_number
                                    ,i_j4postponed
                                    ,l_search_mask);
    elsif l_tmp_statuses is not null
          and l_tmp_statuses.count = 0
    then
      -- will access transactions table by index on status_id
      get_transactions_by_status_tmp(i_fromdate
                                    ,i_todate
                                    ,i_datetype
                                    ,i_transaction_statuses
                                    ,l_tmp_writers
                                    ,i_clearing_providers
                                    ,l_transaction_types
                                    ,i_gateway
                                    ,i_cc_bin
                                    ,i_cc_last4
                                    ,i_epg_id
                                    ,l_trans_class_internal
                                    ,i_receipt_number
                                    ,i_j4postponed
                                    ,l_search_mask);
    elsif l_tmp_types is not null
          and l_tmp_types.count = 0
    then
      -- will access transactions table by index on type_id
      get_transactions_by_type_tmp(i_fromdate
                                  ,i_todate
                                  ,i_datetype
                                  ,i_transaction_statuses
                                  ,l_tmp_writers
                                  ,i_clearing_providers
                                  ,l_transaction_types
                                  ,i_gateway
                                  ,i_cc_bin
                                  ,i_cc_last4
                                  ,i_epg_id
                                  ,l_trans_class_internal
                                  ,i_receipt_number
                                  ,i_j4postponed
                                  ,l_search_mask);
    elsif i_datetype = 1
    then
      -- will access transactions table by partition key on time_created
      get_transactions_by_timec_tmp(i_fromdate
                                   ,i_todate
                                   ,i_transaction_statuses
                                   ,l_tmp_writers
                                   ,i_clearing_providers
                                   ,l_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_mask);
    else
      -- will access transactions table by partition key on time_created + index on time_settled
      get_transactions_by_times_tmp(i_fromdate
                                   ,i_todate
                                   ,i_transaction_statuses
                                   ,l_tmp_writers
                                   ,i_clearing_providers
                                   ,l_transaction_types
                                   ,i_gateway
                                   ,i_cc_bin
                                   ,i_cc_last4
                                   ,i_epg_id
                                   ,l_trans_class_internal
                                   ,i_receipt_number
                                   ,i_j4postponed
                                   ,l_search_mask);
    end if;
    IF i_tran_ids IS NOT NULL
      THEN
        -- return all transactions based on trn rerouting ids
              get_transactions_final(o_transactions, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      ELSE 
       get_transactions_final(o_transactions
                            ,i_user_classes
                            ,i_business_cases
                            ,i_countries
                            ,i_currencies
                            ,i_compaigns
                            ,i_has_balance
                            ,i_skins
                            ,i_page_number
                            ,i_rows_per_page
                            ,i_cc_last4
                            ,i_sales_type
                            ,i_deposit_source
                            ,i_amount_from
                            ,i_amount_to
                            ,i_was_rerouted
                            ,i_is_three_d
                            ,i_cc_countries);
   END IF;

  end get_transactions;

  procedure load_filters
  (
    o_platforms               out sys_refcursor
   ,o_skin_business_cases     out sys_refcursor
   ,o_user_classes            out sys_refcursor
   ,o_countries               out sys_refcursor
   ,o_transaction_statuses    out sys_refcursor
   ,o_transaction_class_types out sys_refcursor
   ,o_clearing_providers      out sys_refcursor
   ,o_transaction_types       out sys_refcursor
   ,o_transaction_type_class  out sys_refcursor
   ,o_compaigns               out sys_refcursor
   ,o_system_writers          out sys_refcursor
   ,o_pending_withdraw_types  out sys_refcursor
  ) is
  begin
    open o_platforms for
      select p.id, p.name value from platforms p order by p.id;

    open o_skin_business_cases for
      select c.id, c.description value from skin_business_cases c order by c.id;

    open o_user_classes for
      select c.id, c.name value from user_classes c order by c.id;

    open o_countries for
      select c.id, c.display_name value from countries c order by c.id;

    open o_transaction_statuses for
      select ts.id, ts.description value from transaction_statuses ts order by ts.id;

    open o_transaction_class_types for
      select tct.id, tct.name value from transaction_class_types tct order by tct.id;

    open o_clearing_providers for
      select p.id, p.name value
      from   clearing_providers p
      where  p.is_active = 1
      and    p.cc_available = 1
      order  by p.name;

    open o_transaction_types for
      select t.id, t.description value
      from   transaction_types t
      -- where  t.id <> 17
      -- and t.is_active = 1 BAC-2817
      -- and t.is_displayed = 1
      order  by t.id;

    open o_transaction_type_class for
      select t.id, t.class_type value from transaction_types t order by t.id;

    open o_compaigns for
      select m.id, m.name value from marketing_campaigns m order by m.id;

    open o_system_writers for
      select w.id, w.user_name value
      from   writers w
      where  w.dept_id = 9
      and    w.user_name not in ('AUTO', 'KEESING')
      order  by w.id;

    open o_pending_withdraw_types for
      select tt.id, tt.description || '.clean' value
      from   transaction_types tt
      where  tt.is_active = 1
      and    tt.is_displayed = 1
      and    tt.class_type = 2
      order  by tt.id;

  end load_filters;

  procedure cancel_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
   ,i_status_id      in number
  ) is
    l_transaction_id     transactions.id%type;
    l_transaction_amount transactions.amount%type;
    l_balance            users.balance%type;
    l_tax_balance        users.tax_balance%type;
    l_user_id            users.id%type;
    l_status_id          transactions.status_id%type;
    l_utc_offset         users.utc_offset%type;
    l_sysdate            date := sysdate;
  begin
    --  will check if the status of the transaction is not final
    --  (2, 3, 5, 6, 8, 12, 13, 14, 15, 16) are final statuses.
    --  We are not supposed to change those once they are set.

    update transactions t
    set    t.status_id           = i_status_id
          ,t.time_settled        = l_sysdate
          ,t.utc_offset_settled =
           (select u.utc_offset from users u where u.id = t.user_id)
          ,t.processed_writer_id = i_writer_id
    where  t.id = i_transaction_id
    and    t.status_id not in (2, 3, 5, 6, 8, 12, 13, 14, 15, 16)
    returning t.amount, t.id, t.user_id into l_transaction_amount, l_transaction_id, l_user_id;

    if sql%found
    then
      update users u
      set    u.balance = u.balance - l_transaction_amount, u.time_modified = l_sysdate, u.utc_offset_modified = u.utc_offset
      where  u.id = l_user_id
      returning u.balance, u.tax_balance, u.utc_offset into l_balance, l_tax_balance, l_utc_offset;

      insert into balance_history
        (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
      values
        (seq_balance_history.nextval
        ,l_sysdate
        ,l_user_id
        ,l_balance
        ,l_tax_balance
        ,i_writer_id
        ,'transactions'
        ,l_transaction_id
        ,2
        ,l_utc_offset);
    else
      -- transaction was not updated
      select t.status_id into l_status_id from transactions t where t.id = i_transaction_id;

      raise_application_error(-20001, 'Transaction status is final(?): ' || l_status_id);
    end if;

  exception
    when no_data_found then
      raise_application_error(-20000, 'No such transaction: ' || i_transaction_id);
  end cancel_deposit;

  -- called to approve wired transactions (?)
  procedure approve_deposit
  (
    i_transaction_id in number
   ,i_writer_id      in number
  ) is
    l_sysdate   date := sysdate;
    l_status_id transactions.status_id%type;
  begin
    update transactions t
    set    t.status_id           = 2
          ,t.time_settled        = l_sysdate
          ,t.utc_offset_settled =
           (select u.utc_offset from users u where u.id = t.user_id)
          ,t.processed_writer_id = i_writer_id
    where  t.id = i_transaction_id
    and    t.status_id not in (2, 3, 5, 6, 8, 12, 13, 14, 15, 16);

    if sql%notfound
    then
      -- transaction was not updated
      select t.status_id into l_status_id from transactions t where t.id = i_transaction_id;

      raise_application_error(-20003, 'Transaction status is final(?): ' || l_status_id);
    end if;

  exception
    when no_data_found then
      raise_application_error(-20002, 'No such transaction: ' || i_transaction_id);
  end approve_deposit;

  procedure create_chargeback
  (
    i_writer_id          in number
   ,i_status_id          in number
   ,i_time_created       in date
   ,i_description        in varchar2
   ,i_utc_offset_created in varchar2
   ,i_issue_id           in number
   ,i_arn                in varchar2
   ,i_time_pos           in date
   ,i_time_chb           in date
   ,i_time_rep           in date
   ,i_time_settled       in date
   ,i_state              in number
   ,i_transaction_id     in number
  ) is
    l_charge_back_id   number;
    l_user_id          number;
    l_sysdate          date := sysdate;
    l_amount_to_return number;
    l_user_balance     number;
    l_user_tax_balance number;
    l_user_utc_offset  users.utc_offset%type;
  begin
    -- in legacy code the 4 date fields were inserted without time part
    insert into charge_backs
      (id
      ,writer_id
      ,status_id
      ,time_created
      ,description
      ,utc_offset_created
      ,issue_id
      ,arn
      ,time_pos
      ,time_chb
      ,time_rep
      ,time_settled
      ,state)
    values
      (seq_charge_backs.nextval
      ,i_writer_id
      ,i_status_id
      ,i_time_created
      ,i_description
      ,i_utc_offset_created
      ,i_issue_id
      ,i_arn
      ,trunc(i_time_pos)
      ,trunc(i_time_chb)
      ,trunc(i_time_rep)
      ,trunc(i_time_settled)
      ,i_state)
    returning id into l_charge_back_id;

    update transactions
    set    charge_back_id = l_charge_back_id
          ,status_id     =
           (select id from transaction_statuses where code = 'TRANS_STATUS_CHARGE_BACK')
    where  id = i_transaction_id
    returning user_id, amount into l_user_id, l_amount_to_return;

    if sql%found
    then
      if i_state = 2
      then
        update users
        set    balance = balance - l_amount_to_return, time_modified = l_sysdate, utc_offset_modified = utc_offset
        where  id = l_user_id
        returning balance, tax_balance, utc_offset into l_user_balance, l_user_tax_balance, l_user_utc_offset;

        insert into balance_history
          (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
        values
          (seq_balance_history.nextval
          ,l_sysdate
          ,l_user_id
          ,l_user_balance
          ,l_user_tax_balance
          ,i_writer_id
          ,'transactions'
          ,i_transaction_id
          ,(select to_number(code)
           from   enumerators
           where  enumerator = 'log_balance'
           and    value = 'log.balance.charge.back')
          ,l_user_utc_offset);
      end if;
    else
      raise_application_error(-20005, 'Transaction does not exists: ' || i_transaction_id);
    end if;
  end create_chargeback;

  procedure manage_trans_postponed
  (
    i_ppn_id          in number
   ,i_ppn_num_of_days in number
   ,i_ppn_trans_id    in number
   ,i_writer_id       in number
  ) is
    l_sysdate date := sysdate;
  begin

    if i_ppn_id is null
    then
      insert into transaction_postponed
        (id, time_created, number_of_days, writer_id, time_updated, transaction_id)
      values
        (seq_transaction_postponed.nextval, l_sysdate, i_ppn_num_of_days, i_writer_id, l_sysdate, i_ppn_trans_id);

    else

      update transaction_postponed
      set    number_of_days = i_ppn_num_of_days, writer_id = i_writer_id, time_updated = l_sysdate
      where  id = i_ppn_id;

      if sql%notfound
      then
        raise_application_error(-20007, 'Record to be updated does not exists: ' || i_ppn_id);
      end if;
    end if;

  end manage_trans_postponed;

  FUNCTION update_user_balance(
      in_user_id          IN NUMBER,
      in_amount           IN NUMBER,
      in_transaction_type IN NUMBER,
      in_rate             IN NUMBER,
      in_comment          IN VARCHAR2,
      in_utc_offset       IN VARCHAR2,
      in_description      IN VARCHAR2)
    RETURN NUMBER
  IS
    l_balance            NUMBER := 0;
    l_return             NUMBER := 0;
    l_amount             NUMBER := in_amount;
    l_transaction_amount NUMBER := 0;
    CURSOR C_BALANCE
    IS
      SELECT balance FROM users WHERE id=in_user_id;
  BEGIN
    OPEN C_BALANCE;
    FETCH C_BALANCE INTO l_balance;
    IF C_BALANCE%NOTFOUND THEN
      CLOSE C_BALANCE;
      RETURN l_return;
    END IF;
    CLOSE C_BALANCE;
    IF l_balance                            +in_amount<0 THEN
      l_amount             :=               -l_balance;
      l_transaction_amount := ABS(in_amount)-l_balance;
      INSERT
      INTO transactions
        (
          ID,
          USER_ID,
          TYPE_ID,
          TIME_CREATED,
          AMOUNT,
          STATUS_ID,
          DESCRIPTION,
          IP,
          TIME_SETTLED,
          COMMENTS,
          UTC_OFFSET_CREATED,
          RATE,
          WRITER_ID,
          PROCESSED_WRITER_ID,
          FEE_CANCEL,
          IS_ACCOUNTING_APPROVED,
          CLEARING_PROVIDER_ID,
          FEE_CANCEL_BY_ADMIN,
          CREDIT_AMOUNT,
          DEPOSIT_REFERENCE_ID,
          SPLITTED_REFERENCE_ID,
          IS_CREDIT_WITHDRAWAL,
          PAYMENT_TYPE_ID,
          UPDATE_BY_ADMIN,
          IS_REROUTED,
          REROUTING_TRANSACTION_ID,
          IS_MANUAL_ROUTING,
          SELECTOR_ID
        )
        VALUES
        (
          SEQ_TRANSACTIONS.nextval,
          in_user_id,
          in_transaction_type,
          sysdate,
          l_transaction_amount,
          2,
          in_description,
          'IP NOT FOUND!',
          sysdate,
          in_comment,
          in_utc_offset,
          in_rate,
          0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        )
      RETURNING id
      INTO l_return;
    END IF;
    UPDATE users
    SET balance           = balance+l_amount,
      time_modified       = sysdate,
      utc_offset_modified = in_utc_offset
    WHERE id              =in_user_id;
    --if we decide to do check before updating
    /*and balance=l_balance;
    if sql%rowcount = 0 then
    return -1;
    end if; */
    RETURN l_return;
  END update_user_balance;

 procedure get_rerouting_trans
  (
    o_transactions       out sys_refcursor
   ,i_transaction_id     IN NUMBER
  ) IS
  REROUTE_FROM_TYPE constant number := 1;
  REROUTE_IDS_TYPE  constant number := 2;
   cursor c_get_transaction is
   select * from
     transactions
   where
      id = i_transaction_id;
   L_TRANSACTIONS TRANSACTIONS%ROWTYPE;
  begin
     open c_get_transaction;
     fetch c_get_transaction into L_TRANSACTIONS;
     close c_get_transaction;

     if (L_TRANSACTIONS.id != L_TRANSACTIONS.REROUTING_TRANSACTION_ID) then
       open o_transactions for
         select REROUTE_FROM_TYPE as rec_type, t.id
         from
          transactions t
          where
            id = L_TRANSACTIONS.REROUTING_TRANSACTION_ID;
     else
       open o_transactions for
         select REROUTE_IDS_TYPE as rec_type, t.id
         from
          transactions t
         where
           t.rerouting_transaction_id = L_TRANSACTIONS.ID
           and t.id !=  L_TRANSACTIONS.ID;
     end if;

  end get_rerouting_trans;

end pkg_transactions_backend;
/
