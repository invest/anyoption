create or replace PACKAGE UTILS
AS
   FUNCTION get_s_date (c_par IN DATE)
      RETURN DATE;

   FUNCTION get_e_date (c_par IN DATE)
      RETURN DATE;

   PROCEDURE remove_dev3_is_risky;

   PROCEDURE withdraw_bonuses;
   
   FUNCTION check_invest (in_type_id IN NUMBER, in_opportunity_id IN NUMBER, in_user_id IN NUMBER, in_time_created IN DATE, in_sec IN NUMBER)
    RETURN VARCHAR2;
   FUNCTION get_nickname
     (p_user_id IN NUMBER) 
	RETURN GET_NICKNAME_TBL_TYPE; 
  
  PROCEDURE SILVERPOP_UPDATES;
  FUNCTION RETURN_INV_FIRST_DATE (p_user_id NUMBER, p_id_id NUMBER) RETURN DATE;
  FUNCTION RETURN_INV_LAST_DATE (p_user_id NUMBER, p_id_id NUMBER) RETURN DATE;
  FUNCTION get_apply_withdrawal_fee (p_user_id IN NUMBER) RETURN NUMBER;
  FUNCTION GET_USER_BONUSES_BE_STATE(p_user_id NUMBER) return NUMBER;
  FUNCTION IS_FIRST_DEPOSIT(P_transaction_id NUMBER, P_user_id NUMBER) RETURN NUMBER;
  FUNCTION GET_USER(P_user_id NUMBER) RETURN USER_OBJ;
  FUNCTION GET_ENUM_VALUE(P_enumerator VARCHAR2, P_code VARCHAR2) RETURN VARCHAR2;
  PROCEDURE INSERT_TERMS(pi_platform_id number, pi_skin_id number, pi_parts varchar2, pi_file_name varchar2);
END;
/

create or replace PACKAGE BODY UTILS
AS
   s_date   DATE;
   e_date   DATE;
   
   FUNCTION check_invest (in_type_id IN NUMBER, in_opportunity_id IN NUMBER, in_user_id IN NUMBER, in_time_created IN DATE, in_sec IN NUMBER)
    RETURN VARCHAR2
   IS
   PRAGMA AUTONOMOUS_TRANSACTION;
   
   cursor c_invest_check is
   select 'x' ch
   from investments
   where type_id=in_type_id
     and opportunity_id=in_opportunity_id
     and user_id=in_user_id 
     and time_created>=in_time_created-in_sec/86400 ;
   
   r_check varchar2(1):='n';
   BEGIN
   
   open c_invest_check;
   fetch c_invest_check into r_check;
   if r_check='x' then
    return 'x';
   end if;
   
   return 'n';
   
   END check_invest;

   FUNCTION get_s_date (c_par IN DATE)
      RETURN DATE
   IS
      s_date   DATE;


      CURSOR p_par
      IS
         SELECT TRUNC (TO_CHAR (c_par, 'HH24') / 6) FROM DUAL;

      v_par    NUMBER;
   BEGIN
      OPEN p_par;

      FETCH p_par INTO v_par;

      CLOSE p_par;

      IF v_par = 0
      THEN
         --exec at sysdate 00:30:00
         s_date := TRUNC (SYSDATE) - 1 + 64800 / 86400;
      ELSIF v_par = 1
      THEN
         --exec at sysdate 06:30:00
         s_date := TRUNC (SYSDATE) + 0 / 86400;
      ELSIF v_par = 2
      THEN
         --exec at sysdate 12:30:00
         s_date := TRUNC (SYSDATE) + 21600 / 86400;
      ELSIF v_par = 3
      THEN
         --exec at sysdate 18:30:00
         s_date := TRUNC (SYSDATE) + 43200 / 86400;
      END IF;

      RETURN (s_date);
   END;

   FUNCTION get_e_date (c_par IN DATE)
      RETURN DATE
   IS
      e_date   DATE;


      CURSOR p_par
      IS
         SELECT TRUNC (TO_CHAR (c_par, 'HH24') / 6) FROM DUAL;

      v_par    NUMBER;
   BEGIN
      OPEN p_par;

      FETCH p_par INTO v_par;

      CLOSE p_par;

      IF v_par = 0
      THEN
         --exec at sysdate 00:30:00
         e_date := TRUNC (SYSDATE) - 1 + 86399 / 86400;
      ELSIF v_par = 1
      THEN
         --exec at sysdate 06:30:00
         e_date := TRUNC (SYSDATE) + 21599 / 86400;
      ELSIF v_par = 2
      THEN
         --exec at sysdate 12:30:00
         e_date := TRUNC (SYSDATE) + 43199 / 86400;
      ELSIF v_par = 3
      THEN
         --exec at sysdate 18:30:00
         e_date := TRUNC (SYSDATE) + 64799 / 86400;
      END IF;

      RETURN (e_date);
   END;

   PROCEDURE remove_dev3_is_risky
   IS
      CURSOR cmain
      IS
         SELECT t.user_id
           FROM TEMPORARY_DEV3_TABLE t;
   BEGIN
      FOR tmp IN cmain
      LOOP
         UPDATE user_market_disable um
            SET um.is_dev3 = 0
          WHERE um.user_id = tmp.user_id;

         UPDATE users u
            SET u.is_risky = 0
          WHERE u.id = tmp.user_id;
      END LOOP;
   END remove_dev3_is_risky;

   PROCEDURE withdraw_bonuses
   IS
      CURSOR cmain
      IS
         SELECT TRUNC (CONVERT_AMOUNT_TO_USD (1, u.currency_id, SYSDATE), 5)
                   rate,
                u.id,
                u.balance,
                tu.amount,
                u.utc_offset
           FROM users u, temp_users tu
          WHERE u.id = tu.user_id;

      CURSOR csec (
         p_user NUMBER)
      IS
         SELECT id
           FROM bonus_users bu
          WHERE     bu.bonus_state_id IN (2, 3)
                AND bu.time_activated < TO_DATE ('01-08-2013', 'dd-mm-yyyy')
                AND bu.user_id = p_user
                AND EXISTS
                       (SELECT 1
                          FROM transactions t
                         WHERE t.time_created >
                                  TO_DATE ('01-07-2013', 'dd-mm-yyyy')
                               AND t.bonus_user_id = bu.id
                               AND t.status_id = 2
                               AND t.type_id = 12);

      trans_id   NUMBER;
      ttime      DATE;
   BEGIN
      FOR tmp IN cmain
      LOOP
         IF tmp.amount > tmp.balance
         THEN
            UPDATE users u
               SET u.balance = 0
             WHERE u.id = tmp.id;

            INSERT INTO TRANSACTIONS (ID,
                                      USER_ID,
                                      TYPE_ID,
                                      TIME_CREATED,
                                      AMOUNT,
                                      STATUS_ID,
                                      DESCRIPTION,
                                      WRITER_ID,
                                      IP,
                                      TIME_SETTLED,
                                      COMMENTS,
                                      PROCESSED_WRITER_ID,
                                      FEE_CANCEL,
                                      IS_ACCOUNTING_APPROVED,
                                      UTC_OFFSET_SETTLED,
                                      UTC_OFFSET_CREATED,
                                      RATE,
                                      CLEARING_PROVIDER_ID,
                                      FEE_CANCEL_BY_ADMIN,
                                      CREDIT_AMOUNT,
                                      DEPOSIT_REFERENCE_ID,
                                      SPLITTED_REFERENCE_ID,
                                      IS_CREDIT_WITHDRAWAL,
                                      PAYMENT_TYPE_ID,
                                      INTERNALS_AMOUNT,
                                      UPDATE_BY_ADMIN,
                                      IS_REROUTED,
                                      REROUTING_TRANSACTION_ID,
                                      IS_MANUAL_ROUTING,
                                      SELECTOR_ID)
                 VALUES (
                           seq_transactions.NEXTVAL,
                           tmp.id,
                           13,
                           SYSDATE,
                           tmp.balance,
                           2,
                           'log.balance.bonus.withdraw',
                           0,
                           'IP NOT FOUND!',
                           SYSDATE,
                           'Bonus funds were withdrawn as wagering wasnt reached in 3 months.',
                           0,
                           0,
                           0,
                           'GMT+02:00',
                           'GMT+02:00',
                           tmp.rate,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           -1)
              RETURNING ID, TIME_CREATED
                   INTO trans_id, ttime;

            INSERT INTO ETRADER.BALANCE_HISTORY (ID,
                                                 USER_ID,
                                                 BALANCE,
                                                 TAX_BALANCE,
                                                 WRITER_ID,
                                                 TIME_CREATED,
                                                 TABLE_NAME,
                                                 KEY_VALUE,
                                                 COMMAND,
                                                 UTC_OFFSET)
                 VALUES (seq_balance_history.NEXTVAL,
                         tmp.id,
                         0,
                         0,
                         0,
                         ttime,
                         'transactions',
                         trans_id,
                         '16',
                         tmp.utc_offset);
         ELSE
            INSERT INTO TRANSACTIONS (ID,
                                      USER_ID,
                                      TYPE_ID,
                                      TIME_CREATED,
                                      AMOUNT,
                                      STATUS_ID,
                                      DESCRIPTION,
                                      WRITER_ID,
                                      IP,
                                      TIME_SETTLED,
                                      COMMENTS,
                                      PROCESSED_WRITER_ID,
                                      FEE_CANCEL,
                                      IS_ACCOUNTING_APPROVED,
                                      UTC_OFFSET_SETTLED,
                                      UTC_OFFSET_CREATED,
                                      RATE,
                                      CLEARING_PROVIDER_ID,
                                      FEE_CANCEL_BY_ADMIN,
                                      CREDIT_AMOUNT,
                                      DEPOSIT_REFERENCE_ID,
                                      SPLITTED_REFERENCE_ID,
                                      IS_CREDIT_WITHDRAWAL,
                                      PAYMENT_TYPE_ID,
                                      INTERNALS_AMOUNT,
                                      UPDATE_BY_ADMIN,
                                      IS_REROUTED,
                                      REROUTING_TRANSACTION_ID,
                                      IS_MANUAL_ROUTING,
                                      SELECTOR_ID)
                 VALUES (
                           seq_transactions.NEXTVAL,
                           tmp.id,
                           13,
                           SYSDATE,
                           tmp.amount,
                           2,
                           'log.balance.bonus.withdraw',
                           0,
                           'IP NOT FOUND!',
                           SYSDATE,
                           'Bonus funds were withdrawn as wagering wasnt reached in 3 months.',
                           0,
                           0,
                           0,
                           'GMT+02:00',
                           'GMT+02:00',
                           tmp.rate,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           0,
                           -1)
              RETURNING ID, TIME_CREATED
                   INTO trans_id, ttime;

            INSERT INTO ETRADER.BALANCE_HISTORY (ID,
                                                 USER_ID,
                                                 BALANCE,
                                                 TAX_BALANCE,
                                                 WRITER_ID,
                                                 TIME_CREATED,
                                                 TABLE_NAME,
                                                 KEY_VALUE,
                                                 COMMAND,
                                                 UTC_OFFSET)
                 VALUES (seq_balance_history.NEXTVAL,
                         tmp.id,
                         tmp.balance - tmp.amount,
                         0,
                         0,
                         ttime,
                         'transactions',
                         trans_id,
                         '16',
                         tmp.utc_offset);

            UPDATE users u
               SET u.balance = u.balance - tmp.amount
             WHERE u.id = tmp.id;
         END IF;

         FOR tmp1 IN csec (tmp.id)
         LOOP
            IF tmp1.id IS NOT NULL
            THEN
               UPDATE bonus_users bu
                  SET bonus_state_id = 4,
                      time_done = SYSDATE,
                      time_updated = SYSDATE
                WHERE tmp1.id = bu.id;
            END IF;
         END LOOP;
      END LOOP;
   END withdraw_bonuses;
   
   FUNCTION get_nickname (p_user_id IN NUMBER)
    return GET_NICKNAME_TBL_TYPE 
    AS
    v_return  GET_NICKNAME_TBL_TYPE;
    
    cursor p_par is
    select GET_NICKNAME_TYPE (A.id,A.nick_name_first,A.nick_name_last)  from
    (select w.id, w.nick_name_first, w.nick_name_last from issues i, issue_actions ia, writers w
    where i.id = ia.issue_id
    and  w.id = ia.writer_id
    and w.nick_name_first is not null
    and i.user_id = p_user_id
    order by ia.id DESC)A where rownum=1;
    
    
    --r1 p_par%rowtype;
    
    begin
      open p_par;
      fetch p_par BULK COLLECT INTO v_return;
      close p_par;
    
    
    RETURN(v_return);
  end get_nickname;
  
  PROCEDURE SILVERPOP_UPDATES IS
v_par NUMBER;
s_date DATE;
e_date DATE;

v_avg_dep NUMBER;
v_count_dep_succ NUMBER;
v_first_succ_time DATE;
v_last_succ_time DATE;

v_count_dep_failed NUMBER;
v_first_dep_failed DATE;

v_avg_inv NUMBER;
v_first_inv_time DATE;
v_last_inv_time DATE;
v_inv_cnt NUMBER;

v_ia_type_id NUMBER;

cursor p_par is
select trunc(to_char(sysdate,'HH24')/6) from dual;

cursor cmain is 
select id from users where time_modified between s_date and e_date /*and is_contact_by_email = 1 and class_id <> 0 and is_active = 1*/;

cursor last_login is 
select id from users where time_last_login between s_date and e_date /*and is_contact_by_email = 1 and class_id <> 0 and is_active = 1*/;

cursor trans ( p_user_id NUMBER) is
select round(nvl(avg(t.amount),0),0)avg_dep,count(*)count_dep_succ,min(t.time_created)first_succ_time,max(t.time_created)last_succ_time from transactions t, transaction_types tt where tt.id = t.type_id and TT.CLASS_TYPE = 1  and p_user_id = t.user_id and t.status_id in (2,7,8);

cursor trans_fail is
select count(*)count_dep_failed,min(t.time_created) first_dep_failed,T.USER_ID from transactions t, transaction_types tt, users u  where t.user_id in  (
select  T.USER_ID from transactions t where time_created between s_date and e_date group by T.USER_ID) and  tt.id = t.type_id and TT.CLASS_TYPE = 1  and  t.status_id = 3 
and t.user_id = u.id /*and u.is_contact_by_email = 1 and u.class_id <> 0 and u.is_active = 1*/
group by T.USER_ID;
/*
cursor inv (p_user_id NUMBER) is
select round(nvl(avg(i.amount),0),0)avg_inv,min(i.time_created)first_inv_time,max(i.time_created)last_inv_time, count(*)inv_cnt from investments i where p_user_id = i.user_id and I.IS_CANCELED = 0;
*/
cursor issue is
select t.user_id,t.ISSUE_ACTION_TYPE_ID from (
select i.user_id,DENSE_RANK() OVER (PARTITION BY i.user_id ORDER BY ia.id DESC) v_rank ,ia.* from issue_actions ia, ISSUE_ACTION_TYPES iat, issues i 
where ia.issue_id = i.id and iat.id = IA.ISSUE_ACTION_TYPE_ID and iat.channel_id IN (1,7) and 
ia.ACTION_TIME between s_date and e_date order by ia.id ) t, users u 
where t.v_rank = 1 and t.user_id = u.id /*and u.is_contact_by_email = 1 and u.class_id <> 0 and u.is_active = 1*/;

begin
  
  open p_par;
  fetch p_par into v_par;
  close p_par;

 if v_par = 0 then
    --exec at sysdate 00:30:00
    s_date := trunc(sysdate) - 1 + 64800 / 86400;
    e_date := trunc(sysdate) - 1 + 86399 / 86400;
    
  elsif v_par = 1 then
    --exec at sysdate 06:30:00
    s_date := trunc(sysdate) + 0 / 86400;
    e_date := trunc(sysdate) + 21599 / 86400;
    
  elsif v_par = 2 then
    --exec at sysdate 12:30:00
    s_date := trunc(sysdate) + 21600 / 86400;
    e_date := trunc(sysdate) + 43199 / 86400;
    
  elsif v_par = 3 then
    --exec at sysdate 18:30:00
    s_date := trunc(sysdate) + 43200 / 86400;
    e_date := trunc(sysdate) + 64799 / 86400;
    
  end if;

for tmp in cmain loop
 open trans (tmp.id);
 fetch trans into v_avg_dep,v_count_dep_succ,v_first_succ_time,v_last_succ_time;
 close trans;
/*
 open inv (tmp.id);
 fetch inv into v_avg_inv,v_first_inv_time,v_last_inv_time,v_inv_cnt;
 close inv; */

 UPDATE silverpop_users su SET 
 -- su.LAST_INV_DATE=v_last_inv_time,
 -- su.AVG_INV_AMOUNT=v_avg_inv,
  su.AVG_DEP_AMOUNT=v_avg_dep,
  su.TRANS_STATUS_SUCCEEDED_COUNT=v_count_dep_succ,
  su.TIME_FIRST_SUCCEEDED_ATTEMPT=v_first_succ_time,
  su.LAST_UPDATE_TIME=SYSDATE,
 -- su.FIRST_INV_TIME=v_first_inv_time,
 -- su.inv_cnt = v_inv_cnt,
  su.last_dep_date = v_last_succ_time
  WHERE tmp.id = su.user_id;
commit; 
 end loop;
 

for tmp1 in last_login loop
 UPDATE silverpop_users su SET 
  su.LAST_UPDATE_TIME=SYSDATE
  WHERE tmp1.id = su.user_id;
 commit;
 end loop;

for tmp2 in issue loop
 UPDATE silverpop_users su SET 
  su.LAST_UPDATE_TIME=SYSDATE,
  su.LAST_ISSUE_ACTION_TYPE = tmp2.ISSUE_ACTION_TYPE_ID
  WHERE tmp2.user_id = su.user_id;
 commit;
 end loop; 

for tmp3 in trans_fail loop
 UPDATE silverpop_users su SET 
  su.LAST_UPDATE_TIME=SYSDATE,
  su.TRANS_STATUS_FAILED_COUNT=tmp3.count_dep_failed,
  su.TIME_FIRST_FAILED_ATTEMPT=tmp3.first_dep_failed
  WHERE tmp3.user_id = su.user_id;
 commit;
 end loop;

END SILVERPOP_UPDATES;

FUNCTION RETURN_INV_FIRST_DATE (p_user_id NUMBER, p_id_id NUMBER) 
RETURN DATE IS
v_first_inv_time DATE;
PRAGMA AUTONOMOUS_TRANSACTION;

CURSOR date_check IS
SELECT min(i.time_created)first_inv_time from investments i where i.user_id = p_user_id and i.IS_CANCELED = 0 and i.id != p_id_id;



BEGIN

open date_check;
fetch date_check into v_first_inv_time;
close date_check;

RETURN v_first_inv_time;
END RETURN_INV_FIRST_DATE;
FUNCTION RETURN_INV_LAST_DATE (p_user_id NUMBER, p_id_id NUMBER) RETURN DATE IS     
v_last_inv_time DATE;
PRAGMA AUTONOMOUS_TRANSACTION;

CURSOR date_check IS
SELECT max(i.time_created)last_inv_time from investments i where i.user_id = p_user_id and i.IS_CANCELED = 0 and i.id != p_id_id;


BEGIN

open date_check;
fetch date_check into v_last_inv_time;
close date_check;

RETURN v_last_inv_time;
END RETURN_INV_LAST_DATE;

FUNCTION get_apply_withdrawal_fee
     (p_user_id IN NUMBER)
RETURN NUMBER is

cursor c_deposit_number is
select count(id), sum(amount*rate) 
from transactions t
where t.type_id in (select id from transaction_types where class_type in (1, 3, 5, 11)) 
  and status_id in (2,7,8)
  and t.user_id = p_user_id;
  
cursor c_investment_number is
select count(id), sum(amount*rate) 
from investments i
where i.user_id = p_user_id;
  
r_deposit_number number :=0;
r_deposit_sum number :=0;
r_investment_number number :=0;
r_investment_sum number :=0;

begin
open c_deposit_number;
fetch c_deposit_number into r_deposit_number, r_deposit_sum;
close c_deposit_number;

if r_deposit_number >= 3 then
  return 0;
end if; 

if r_deposit_number >= 2 and r_deposit_sum >= 150000 then
  return 0;
end if; 

open c_investment_number;
fetch c_investment_number into r_investment_number, r_investment_sum;
close c_investment_number;

if r_investment_number >= 30 then
  return 0;
end if;

if r_investment_sum >= 200000 then
  return 0;
end if;

return 1;

end;

FUNCTION GET_USER_BONUSES_BE_STATE(p_user_id number)
  return number is

cursor c_get_b_state is
select sum(yes_bonus) as yes_b,
       sum(no_bonus) as no_b,
       sum(pending_bonus) as p_b
  from 
  (
    select user_id, 
    case when 
     bb.bonus_state_id in (1,2,3) then 1 else 0 end as yes_bonus,
    case when
      bb.bonus_state_id not in (1,2,3, 10) then 1 else 0 end as no_bonus,
    case when  
      bb.bonus_state_id  in (10) then 1 else 0 end as pending_bonus  
    from
    (select user_id, 
            bonus_state_id
    from bonus_users
    where  user_id = p_user_id
    group by user_id, 
             bonus_state_id
  ) bb
);

v_yes_b number;
v_no_b number;
v_pending_b number;

YES_BONUSES_BE_STATE constant number := 1;
NO_BONUSES_BE_STATE constant number := 2;
PENDING_BONUSES_BE_STATE constant number := 3;

v_be_state number := NO_BONUSES_BE_STATE;

  BEGIN

  open c_get_b_state;
  fetch c_get_b_state into v_yes_b, v_no_b, v_pending_b;
  close c_get_b_state;
  
  if v_yes_b > 0 then
    v_be_state := YES_BONUSES_BE_STATE;
 elsif v_pending_b > 0 then
    v_be_state := PENDING_BONUSES_BE_STATE;
 else 
    v_be_state :=  NO_BONUSES_BE_STATE;
  end if; 
    
    return v_be_state;
  END;
  
  FUNCTION IS_FIRST_DEPOSIT(P_transaction_id NUMBER, P_user_id NUMBER)
    RETURN NUMBER  AS
    -- *** Declaration_section ***
   is_first_deposit NUMBER := 0;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('IS_FIRST_DEPOSIT START');
    select 
      1
  	INTO 
      is_first_deposit
  	from
      dual
  	where
      P_transaction_id = (select 
                                min(t.id) 
                          from 
                                TRANSACTIONS t, 
                                TRANSACTION_TYPES tt 
                          where 
                                t.USER_ID = P_user_id
                                and  tt.ID = t.TYPE_ID  
                                and tt.CLASS_TYPE = 1 
                                and t.STATUS_ID in (2,7,8));
    
    DBMS_OUTPUT.PUT_LINE('IS_FIRST_DEPOSIT END');
    RETURN is_first_deposit;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('IS_FIRST_DEPOSIT END');
      RETURN is_first_deposit;
  END IS_FIRST_DEPOSIT;
  
  FUNCTION GET_USER(P_user_id NUMBER)
    RETURN USER_OBJ  AS
    -- *** Declaration_section ***
   F_user_obj USER_OBJ;
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_USER START');
    select 
      USER_OBJ( u.FIRST_DEPOSIT_ID, 
                u.TIME_CREATED,
                MARKETING_ATTRIBUTION_OBJ(u.id, u.MARKETING_ATTRIBUTION_TYPE_ID, u.COMBINATION_ID, u.DYNAMIC_PARAM, uad.AFF_SUB1, uad.AFF_SUB2, uad.AFF_SUB3, u.AFFILIATE_KEY, u.SPECIAL_CODE, u.AFFILIATE_SYSTEM, u.AFFILIATE_SYSTEM_USER_TIME))
    INTO 
      F_user_obj
    from
      users u,
      USERS_ACTIVE_DATA uad
    where
      u.id = uad.USER_ID
      and u.ID = P_user_id;
      
    DBMS_OUTPUT.PUT_LINE('GET_USER END');
    RETURN F_user_obj;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('GET_USER END');
      RETURN F_user_obj;
  END GET_USER;
  
  FUNCTION GET_ENUM_VALUE(P_enumerator VARCHAR2, P_code VARCHAR2) 
    RETURN VARCHAR2 AS
    -- *** Declaration_section ***
    F_val VARCHAR2(2000) := '0';
  BEGIN
    -- *** Execution_section ***
    DBMS_OUTPUT.PUT_LINE('GET_ENUM_VALUE START');
    SELECT 
      e.value 
    INTO
      F_val
    FROM 
      enumerators e
    WHERE 
      e.enumerator LIKE P_enumerator
      and e.code LIKE P_code;
    DBMS_OUTPUT.PUT_LINE('GET_ENUM_VALUE END');
    RETURN F_val;
    
    EXCEPTION
      WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('GET_ENUM_VALUE END');
      RETURN F_val;
  END GET_ENUM_VALUE;
  
  PROCEDURE INSERT_TERMS(pi_platform_id number, pi_skin_id number, pi_parts varchar2, pi_file_name varchar2) IS


cursor c_get_part is
select p.id from
terms_parts p
where p.parts_name = pi_parts;

cursor c_get_file is
select p.id from
terms_files p
where p.file_name = pi_file_name;

v_parts_id NUMBER:=0;
v_file_id NUMBER:=0;

begin

--Parts
open c_get_part;
fetch c_get_part into v_parts_id;
close c_get_part;

if v_parts_id = 0 then
  v_parts_id := seq_terms_parts.nextval; 
insert into terms_parts
  (id, parts_name, order_num)
values
  (v_parts_id, pi_parts, -1);
end if;

--Files
open c_get_file;
fetch c_get_file into v_file_id;
close c_get_file;

if v_file_id = 0 then
  v_file_id :=  seq_terms_files.nextval;
insert into terms_files
  (id, file_name, title)
values
  (v_file_id, pi_file_name, 'zzzzz');

end if;



if pi_platform_id = 0 then

insert into terms
  (id, platform_id, skin_id, part_id, file_id)
values
  (seq_terms.nextval, 2, pi_skin_id, v_parts_id, v_file_id);

insert into terms
  (id, platform_id, skin_id, part_id, file_id)
values
  (seq_terms.nextval, 3, pi_skin_id, v_parts_id, v_file_id);


else
  insert into terms
  (id, platform_id, skin_id, part_id, file_id)
values
  (seq_terms.nextval, pi_platform_id, pi_skin_id, v_parts_id, v_file_id);
end if;
END INSERT_TERMS;
   
END;
/
