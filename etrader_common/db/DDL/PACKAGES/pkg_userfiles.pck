create or replace package pkg_userfiles is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-25 10:53:24
  -- Purpose :

  -- get user files by id
  -- %param o_files
  -- %param i_file_id
  -- %param i_prevnext  Exact(0), Previous(1) or Next(2)
  procedure get_file
  (
    o_files    out sys_refcursor
   ,i_file_id  in number
   ,i_prevnext in number default 0
  );

  -- to load drop down
  procedure load_file_types(o_file_types out sys_refcursor);

  procedure load_reject_reason_types(o_reasons out sys_refcursor);

  procedure load_reject_reasons(o_reasons out sys_refcursor);

  procedure load_ks_files_for_send
  (
    io_file_id  in out number
   ,o_file_name out varchar2
  );

  procedure create_file
  (
    o_file_id                     out number
   ,i_user_id                     in number
   ,i_file_type_id                in number
   ,i_writer_id                   in number
   ,i_reference                   in varchar2
   ,i_file_name                   in varchar2
   ,i_cc_id                       in number
   ,i_is_approved                 in number
   ,i_file_status_id              in number
   ,i_issue_action_id             in number
   ,i_id_number                   in varchar2
   ,i_first_name                  in varchar2
   ,i_last_name                   in varchar2
   ,i_exp_date                    in date
   ,i_country_id                  in number
   ,i_is_approved_control         in number
   ,i_is_control_rejected         in number
   ,i_is_support_rejected         in number
   ,i_reject_reason_id            in number
   ,i_comments                    in varchar2
   ,i_rejection_writer_id         in number
   ,i_rejection_date              in date
   ,i_uploader_id                 in number
   ,i_support_approved_writer_id  in number
   ,i_time_support_approved       in date
   ,i_control_approved_writer_id  in number
   ,i_time_control_approved       in date
   ,i_control_reject_writer_id    in number
   ,i_time_control_rejected       in date
   ,i_control_rejection_reason_id in number
   ,i_control_rejection_comments  in varchar2
   ,i_time_uploaded               in date
  );

  procedure create_file
  (
    i_user_id         in number
   ,i_file_type_id    in number
   ,i_file_name       in varchar2
   ,i_cc_id           in number
   ,i_file_status_id  in number
   ,i_issue_action_id in number
   ,i_writer_id       in number
  );

    procedure create_file_return_id
  (
    o_file_id         out number
   ,i_user_id         in number
   ,i_file_type_id    in number
   ,i_writer_id       in number 
   ,i_file_name       in varchar2
   ,i_file_status_id  in number
   ,i_uploader_id     in number
   ,i_cc_id                      in number default null
   ,i_is_approved                in number default 0
   ,i_support_approved_writer_id in number default null
   ,i_is_support_rejected        in number default null
   ,i_reject_reason_id           in number default 0
   ,i_rejection_writer_id        in number default null
  );

  procedure create_ks_record
  (
    i_file_id         in number
   ,i_first_name      in varchar2
   ,i_last_name       in varchar2
   ,i_nationality     in varchar2
   ,i_issuing_country in varchar2
   ,i_doc_number      in varchar2
   ,i_expiry_date     in date
   ,i_personal_id     in number
   ,i_date_of_birth   in date
   ,i_gender          in varchar2
   ,i_doc_id          in number
   ,i_check_status    in number
   ,i_reason_nok      in number
   ,i_response        in varchar2
  );

  procedure mark_file_ks_failed(i_file_id in number);

  -- %param o_files
  -- %param i_page_number
  -- %param i_rows_per_page
  -- %param i_uploader          - Multi-select of only "system" writers
  -- %param i_uploader_backend  - Also include "non-system" writers
  -- %param i_business_case
  -- %param i_skins
  -- %param i_countries
  -- %param i_ks_status
  -- %param i_user_id
  -- %param i_user_name         - Use as case insensitive filter pattern for username. i.e. (LIKE %i_user_name%)
  procedure load_ks_files_for_approve
  (
    o_files            out sys_refcursor
   ,i_page_number      in number
   ,i_rows_per_page    in number
   ,i_uploader         in number_table
   ,i_uploader_backend in number
   ,i_business_case    in number
   ,i_skins            in number_table
   ,i_countries        in number_table
   ,i_ks_status        in number
   ,i_user_id          in number
   ,i_user_name        in varchar2
   ,i_user_class_type  in number
  );

  procedure get_file_diffs
  (
    o_diffs   out sys_refcursor
   ,i_file_id in number
  );

  procedure reupload_ks_file(i_file_id in number);

  procedure update_ks_match
  (
    i_ks_response_id     in number
   ,i_flag_first_name    in number
   ,i_flag_last_name     in number
   ,i_flag_country       in number
   ,i_flag_date_of_birth in number
   ,i_flag_gender        in number
   ,i_writer_id          in number
  );

  procedure get_active_ks_response
  (
    o_response out sys_refcursor
   ,i_file_id  in number
  );
  
    procedure is_exist_file_type
  (  
    i_user_id    in number 
   ,i_file_type  in number
   ,i_cc_id      in number
   ,o_is_exist   out number
  );
  
    procedure create_gbg_record
  (
    i_user_id         in number,
    i_country_id      in number      
  );
  
  procedure load_gbg_users_for_send
   (
     o_gbg_users out sys_refcursor
   );
   procedure update_gbg_status_id
     (
       i_gbg_id    in number,
       i_status_id in number
     ); 
   procedure update_gbg_response
     (
       o_resault_by_score out number,
       i_gbg_id           in number,
       i_score            in number,
       i_file_id          in number,
       i_error_code       in varchar2,
       i_error_reason     in varchar2,
       i_request_xml      in clob, 
       i_response_xml     in clob
      ); 
   procedure load_gbg_countries
     (
       o_gbg_countries out sys_refcursor
     );            
     
       procedure update_gbg_file_id
     (
       i_gbg_id    in number,
       i_file_id in number
      );
      
  procedure get_gbg_result
  (
    o_result out sys_refcursor
   ,i_file_id  in number
  );      
  
end pkg_userfiles;
/
create or replace package body pkg_userfiles is

  -- %param o_files
  -- %param i_file_id
  -- %param i_prevnext  Exact(0), Previous(1) or Next(2)
  procedure get_file
  (
    o_files    out sys_refcursor
   ,i_file_id  in number
   ,i_prevnext in number default 0
  ) is
  begin

    open o_files for
      select *
      from   (select f.id
                    ,f.user_id
                    ,f.file_type_id
                    ,f.writer_id
                    ,f.time_created
                    ,f.reference
                    ,f.file_name
                    ,f.utc_offset_created
                    ,f.cc_id
                    ,f.is_approved
                    ,f.file_status_id
                    ,f.time_updated
                    ,f.issue_action_id
                    ,f.id_number
                    ,f.first_name
                    ,f.last_name
                    ,f.exp_date
                    ,f.country_id
                    ,f.is_color
                    ,f.is_approved_control
                    ,f.is_control_rejected
                    ,f.is_support_rejected
                    ,f.reject_reason_id
                    ,f.comments
                    ,f.rejection_writer_id
                    ,f.rejection_date
                    ,f.uploader_id
                    ,f.support_approved_writer_id
                    ,f.time_support_approved
                    ,f.control_approved_writer_id
                    ,f.time_control_approved
                    ,f.control_reject_writer_id
                    ,f.time_control_rejected
                    ,f.control_rejection_reason_id
                    ,f.control_rejection_comments
                    ,f.time_uploaded
                    ,f.ks_status_id
                    ,f.ks_status_date
                    ,t.name
                    ,row_number() over(order by f.id) next_
                    ,row_number() over(order by f.id desc) prev_
                    ,w.user_name writer_name
                    ,u.user_name uploader_name
                    ,f.is_current
              from   files f
              join   file_types t
              on     t.id = f.file_type_id
              join   writers w
              on     w.id = f.writer_id
              left   join writers u
              on     u.id = f.uploader_id
              where  f.user_id = (select user_id from files where id = i_file_id)
              and    ((i_prevnext = 0 and f.id = i_file_id) or (i_prevnext = 1 and f.id < i_file_id) or
                    (i_prevnext = 2 and f.id > i_file_id)))
      where  (i_prevnext = 0 or (i_prevnext = 1 and prev_ = 1) or (i_prevnext = 2 and next_ = 1));

  end get_file;

  procedure load_file_types(o_file_types out sys_refcursor) is
  begin

    open o_file_types for
      select t.id, t.name, t.is_credit_card_file, t.is_keesing_type from file_types t where t.id <> 24 order by t.id;

  end load_file_types;

  procedure load_reject_reason_types(o_reasons out sys_refcursor) is
  begin
    open o_reasons for
      select ftrr.file_type_id, ftrr.reject_reason_id, frr.reason_name
      from   file_type_reject_reasons ftrr
      join   file_reject_reasons frr
      on     frr.id = ftrr.reject_reason_id
      where  frr.enabled = 1
      order  by ftrr.file_type_id, ftrr.reject_reason_id;
  end load_reject_reason_types;

  procedure load_reject_reasons(o_reasons out sys_refcursor) is
  begin
    open o_reasons for
      select frr.id reject_reason_id, frr.reason_name from file_reject_reasons frr where frr.enabled = 1 order by frr.id;
  end load_reject_reasons;

  procedure retry_hanging_ks_files is
    pragma autonomous_transaction;

    l_date    date;
    l_sysdate date := sysdate;
  begin
    l_date := to_date(pkg_gcontext.get_value('ks_last_retry_time'), 'yyyymmddhh24miss');

    if l_date < l_sysdate - 1 / 96
       or l_date is null
    then
      -- ensure that only one process at a time will run the update
      insert into exec_sync (sync_value) values (1);

      -- update all files with status 2 and status_date more than 2 hours ago to 1, so that tha job could retry them
      update files
      set    ks_status_id = 1, ks_status_date = l_sysdate
      where  case
               when ks_status_id <> 3 then
                ks_status_id
             end = 2
      and    ks_status_date < l_sysdate - 1 / 12;

      delete from exec_sync where sync_value = 1;

      -- save the last time the process run, into the global context
      pkg_gcontext.set_value('ks_last_retry_time', to_char(l_sysdate, 'yyyymmddhh24miss'));

      commit;
    end if;
  end retry_hanging_ks_files;

  procedure load_ks_files_for_send
  (
    io_file_id  in out number
   ,o_file_name out varchar2
  ) is
    cursor c_data is
      select /*+ first_rows(1) */
       id
      from   files
      where  case
               when ks_status_id <> 3 then
                ks_status_id
             end = 1
      and    file_name is not null
      order  by time_created, id
      for    update skip locked;

    e_file_locked exception;
    pragma exception_init(e_file_locked, -54);
  begin
    retry_hanging_ks_files;

    if io_file_id is null
    then
      open c_data;
      fetch c_data
        into io_file_id;
      close c_data;
    else
      select id
      into   io_file_id
      from   files
      where  id = io_file_id
      and    ks_status_id = 1
      for    update nowait;
    end if;

    update files set ks_status_id = 2, ks_status_date = sysdate where id = io_file_id returning file_name into o_file_name;

  exception
    when no_data_found then
      raise_application_error(-20002, 'File does not exist or is not in status 1. file_id: ' || io_file_id);
    when e_file_locked then
      raise_application_error(-20003, 'File is locked. file_id: ' || io_file_id);
  end load_ks_files_for_send;

  procedure create_file
  (
    o_file_id                     out number
   ,i_user_id                     in number
   ,i_file_type_id                in number
   ,i_writer_id                   in number
   ,i_reference                   in varchar2
   ,i_file_name                   in varchar2
   ,i_cc_id                       in number
   ,i_is_approved                 in number
   ,i_file_status_id              in number
   ,i_issue_action_id             in number
   ,i_id_number                   in varchar2
   ,i_first_name                  in varchar2
   ,i_last_name                   in varchar2
   ,i_exp_date                    in date
   ,i_country_id                  in number
   ,i_is_approved_control         in number
   ,i_is_control_rejected         in number
   ,i_is_support_rejected         in number
   ,i_reject_reason_id            in number
   ,i_comments                    in varchar2
   ,i_rejection_writer_id         in number
   ,i_rejection_date              in date
   ,i_uploader_id                 in number
   ,i_support_approved_writer_id  in number
   ,i_time_support_approved       in date
   ,i_control_approved_writer_id  in number
   ,i_time_control_approved       in date
   ,i_control_reject_writer_id    in number
   ,i_time_control_rejected       in date
   ,i_control_rejection_reason_id in number
   ,i_control_rejection_comments  in varchar2
   ,i_time_uploaded               in date
  ) is
    l_file            files%rowtype;
    l_sysdate         date := sysdate;
    l_is_keesing_type number;
    l_is_regulated    number;
  begin

    select u.utc_offset
          ,seq_files.nextval
          ,(select is_keesing_type from file_types t where t.id = i_file_type_id)
          ,(select s.is_regulated from skins s where s.id = u.skin_id)
    into   l_file.utc_offset_created, l_file.id, l_is_keesing_type, l_is_regulated
    from   users u
    where  u.id = i_user_id;

    if l_is_keesing_type is null
    then
      raise_application_error(-20000, 'No such file type id: ' || i_file_type_id);
    elsif l_is_keesing_type = 1
    then
      l_file.ks_status_id   := 1;
      l_file.ks_status_date := l_sysdate;
    end if;

    -- logic transfered from trigger before_files
    if l_is_regulated = 1
       and i_is_approved_control = 1
    then
      l_file.file_status_id := 3;
    else
      l_file.file_status_id := i_file_status_id;
    end if;

    l_file.user_id                     := i_user_id;
    l_file.file_type_id                := i_file_type_id;
    l_file.writer_id                   := i_writer_id;
    l_file.time_created                := l_sysdate;
    l_file.reference                   := i_reference;
    l_file.file_name                   := i_file_name;
    l_file.cc_id                       := i_cc_id;
    l_file.is_approved                 := i_is_approved;
    l_file.time_updated                := null;
    l_file.issue_action_id             := i_issue_action_id;
    l_file.id_number                   := i_id_number;
    l_file.first_name                  := i_first_name;
    l_file.last_name                   := i_last_name;
    l_file.exp_date                    := i_exp_date;
    l_file.country_id                  := i_country_id;
    l_file.is_color                    := null;
    l_file.is_approved_control         := i_is_approved_control;
    l_file.is_control_rejected         := i_is_control_rejected;
    l_file.is_support_rejected         := i_is_support_rejected;
    l_file.reject_reason_id            := i_reject_reason_id;
    l_file.comments                    := i_comments;
    l_file.rejection_writer_id         := i_rejection_writer_id;
    l_file.rejection_date              := i_rejection_date;
    l_file.uploader_id                 := i_uploader_id;
    l_file.support_approved_writer_id  := i_support_approved_writer_id;
    l_file.time_support_approved       := i_time_support_approved;
    l_file.control_approved_writer_id  := i_control_approved_writer_id;
    l_file.time_control_approved       := i_time_control_approved;
    l_file.control_reject_writer_id    := i_control_reject_writer_id;
    l_file.time_control_rejected       := i_time_control_rejected;
    l_file.control_rejection_reason_id := i_control_rejection_reason_id;
    l_file.control_rejection_comments  := i_control_rejection_comments;
    l_file.time_uploaded               := i_time_uploaded;

    insert into files values l_file;

    insert into files_history
      (id
      ,file_id
      ,file_type_id
      ,writer_id
      ,reference
      ,file_name
      ,utc_offset_created
      ,cc_id
      ,is_approved
      ,file_status_id
      ,id_number
      ,is_color
      ,is_approved_control
      ,is_control_rejected
      ,is_support_rejected
      ,reject_reason_id
      ,comments
      ,rejection_writer_id
      ,rejection_date
      ,uploader_id
      ,time_updated
      ,issue_action_id
      ,first_name
      ,last_name
      ,exp_date
      ,country_id
      ,support_approved_writer_id
      ,time_support_approved
      ,control_approved_writer_id
      ,time_control_approved
      ,control_reject_writer_id
      ,time_control_rejected
      ,trig_event
      ,control_rejection_reason_id
      ,control_rejection_comments
      ,ks_status_id
      ,ks_status_date
      ,time_uploaded)
    values
      (seq_files_history.nextval
      ,l_file.id
      ,l_file.file_type_id
      ,l_file.writer_id
      ,l_file.reference
      ,l_file.file_name
      ,l_file.utc_offset_created
      ,l_file.cc_id
      ,l_file.is_approved
      ,l_file.file_status_id
      ,l_file.id_number
      ,l_file.is_color
      ,l_file.is_approved_control
      ,l_file.is_control_rejected
      ,l_file.is_support_rejected
      ,l_file.reject_reason_id
      ,l_file.comments
      ,l_file.rejection_writer_id
      ,l_file.rejection_date
      ,l_file.uploader_id
      ,l_file.time_updated
      ,l_file.issue_action_id
      ,l_file.first_name
      ,l_file.last_name
      ,l_file.exp_date
      ,l_file.country_id
      ,l_file.support_approved_writer_id
      ,l_file.time_support_approved
      ,l_file.control_approved_writer_id
      ,l_file.time_control_approved
      ,l_file.control_reject_writer_id
      ,l_file.time_control_rejected
      ,'INSERT'
      ,l_file.control_rejection_reason_id
      ,l_file.control_rejection_comments
      ,l_file.ks_status_id
      ,l_file.ks_status_date
      ,l_file.time_uploaded);

    o_file_id := l_file.id;
  exception
    when no_data_found then
      raise_application_error(-20001, 'No such user: ' || i_user_id);
  end create_file;

  procedure create_file
  (
    i_user_id         in number
   ,i_file_type_id    in number
   ,i_file_name       in varchar2
   ,i_cc_id           in number
   ,i_file_status_id  in number
   ,i_issue_action_id in number
   ,i_writer_id       in number
  ) is
    l_file_id number;
  begin
    create_file(o_file_id                     => l_file_id
               ,i_user_id                     => i_user_id
               ,i_file_type_id                => i_file_type_id
               ,i_writer_id                   => i_writer_id
               ,i_reference                   => i_file_name
               ,i_file_name                   => null
               ,i_cc_id                       => i_cc_id
               ,i_is_approved                 => 0
               ,i_file_status_id              => i_file_status_id
               ,i_issue_action_id             => i_issue_action_id
               ,i_id_number                   => null
               ,i_first_name                  => null
               ,i_last_name                   => null
               ,i_exp_date                    => null
               ,i_country_id                  => null
               ,i_is_approved_control         => null
               ,i_is_control_rejected         => null
               ,i_is_support_rejected         => null
               ,i_reject_reason_id            => 0
               ,i_comments                    => null
               ,i_rejection_writer_id         => null
               ,i_rejection_date              => null
               ,i_uploader_id                 => null
               ,i_support_approved_writer_id  => null
               ,i_time_support_approved       => null
               ,i_control_approved_writer_id  => null
               ,i_time_control_approved       => null
               ,i_control_reject_writer_id    => null
               ,i_time_control_rejected       => null
               ,i_control_rejection_reason_id => null
               ,i_control_rejection_comments  => null
               ,i_time_uploaded               => null);
  end create_file;

   procedure create_file_return_id
  (
    o_file_id                    out number
   ,i_user_id                    in number
   ,i_file_type_id               in number
   ,i_writer_id                  in number 
   ,i_file_name                  in varchar2
   ,i_file_status_id             in number
   ,i_uploader_id                in number
   ,i_cc_id                      in number default null
   ,i_is_approved                in number default 0
   ,i_support_approved_writer_id in number default null
   ,i_is_support_rejected        in number default null
   ,i_reject_reason_id           in number default 0
   ,i_rejection_writer_id        in number default null
  ) is
    l_file_id number;
    v_rejection_date date;
    v_time_support_approved date;
  begin
  if i_reject_reason_id > 0 and i_is_support_rejected = 1 then
    v_rejection_date := sysdate;
  end if; 
  
   if i_is_approved = 1 then
    v_time_support_approved := sysdate;
  end if;   
  
    create_file(o_file_id                     => l_file_id
               ,i_user_id                     => i_user_id
               ,i_file_type_id                => i_file_type_id
               ,i_writer_id                   => i_writer_id
               ,i_reference                   => null
               ,i_file_name                   => i_file_name
               ,i_cc_id                       => i_cc_id
               ,i_is_approved                 => i_is_approved
               ,i_file_status_id              => i_file_status_id
               ,i_issue_action_id             => null
               ,i_id_number                   => null
               ,i_first_name                  => null
               ,i_last_name                   => null
               ,i_exp_date                    => null
               ,i_country_id                  => null
               ,i_is_approved_control         => null
               ,i_is_control_rejected         => null
               ,i_is_support_rejected         => i_is_support_rejected
               ,i_reject_reason_id            => i_reject_reason_id
               ,i_comments                    => null
               ,i_rejection_writer_id         => i_rejection_writer_id
               ,i_rejection_date              => v_rejection_date
               ,i_uploader_id                 => i_uploader_id
               ,i_support_approved_writer_id  => i_support_approved_writer_id
               ,i_time_support_approved       => v_time_support_approved
               ,i_control_approved_writer_id  => null
               ,i_time_control_approved       => null
               ,i_control_reject_writer_id    => null
               ,i_time_control_rejected       => null
               ,i_control_rejection_reason_id => null
               ,i_control_rejection_comments  => null
               ,i_time_uploaded               => sysdate);
               
               o_file_id:= l_file_id; 
  end create_file_return_id;

  -- %param o_files
  -- %param i_page_number
  -- %param i_rows_per_page
  -- %param i_uploader          - Multi-select of only "system" writers
  -- %param i_uploader_backend  - Also include "non-system" writers
  -- %param i_business_case
  -- %param i_skins
  -- %param i_countries
  -- %param i_ks_status
  -- %param i_user_id
  -- %param i_user_name
  procedure load_ks_files_for_approve
  (
    o_files            out sys_refcursor
   ,i_page_number      in number
   ,i_rows_per_page    in number
   ,i_uploader         in number_table
   ,i_uploader_backend in number
   ,i_business_case    in number
   ,i_skins            in number_table
   ,i_countries        in number_table
   ,i_ks_status        in number
   ,i_user_id          in number
   ,i_user_name        in varchar2
   ,i_user_class_type  in number
  ) is
    l_text_filter varchar2(100);
  begin
    l_text_filter := '%' || upper(replace(replace(replace(i_user_name, '%', '\%'), '_', '\_'), '\', '\\')) || '%';

    -- consider removing "count(*) over() total_count" in case of performance problems!
    open o_files for
      select *
      from   (select a.*, rownum rn
              from   (select f.id file_id
                            ,f.user_id
                            ,u.user_name
                            ,u.first_name
                            ,u.last_name
                            ,c.country_name
                            ,s.name skin_name
                            ,s.id skin_id
                            ,f.time_uploaded
                            ,k.check_status
                            ,case
                               when re.approved_regulation_step = 7 then
                                1 -- yes
                               when re.approved_regulation_step <> 7 then
                                2 -- no
                               else
                                0 -- "null"
                             end regulated
                            ,count(*) over() total_count
                            ,f.uploader_id
                      from   files f
                      join   users u
                      on     u.id = f.user_id
                      join   countries c
                      on     c.id = u.country_id
                      join   skins s
                      on     s.id = u.skin_id
                      join   ks_responses k
                      on     k.file_id = f.id
                      left   join users_regulation re
                      on     re.user_id = u.id
                      where  f.ks_status_id = 3
                      and    f.is_approved = 0
                      and    nvl(f.is_support_rejected, 0) = 0
                      and    k.is_active = 1

                      and    ((i_uploader is null and (i_uploader_backend is null or i_uploader_backend = 0)) or
                            (f.uploader_id member of i_uploader) or
                            (i_uploader_backend = 1 and exists (select 1
                                                                  from   writers w
                                                                  where  w.dept_id <> 9
                                                                  and    w.id = f.uploader_id)))
                      and    (i_business_case is null or s.business_case_id = i_business_case)
                      and    (s.id member of i_skins)
                      and    (i_countries is null or u.country_id member of i_countries)
                      and    (i_ks_status is null or k.check_status = i_ks_status)
                      and    (i_user_id is null or u.id = i_user_id)
                      and    (i_user_name is null or u.user_name like l_text_filter)                    
                      and    (i_user_class_type is null or (i_user_class_type = 12 and u.class_id in (1, 2)) or
                                             (i_user_class_type <> 12 and u.class_id = i_user_class_type))
                      order  by f.time_uploaded) a
              where  rownum <= i_page_number * i_rows_per_page)
      where  rn > (i_page_number - 1) * i_rows_per_page;
  end load_ks_files_for_approve;

  procedure create_ks_record
  (
    i_file_id         in number
   ,i_first_name      in varchar2
   ,i_last_name       in varchar2
   ,i_nationality     in varchar2
   ,i_issuing_country in varchar2
   ,i_doc_number      in varchar2
   ,i_expiry_date     in date
   ,i_personal_id     in number
   ,i_date_of_birth   in date
   ,i_gender          in varchar2
   ,i_doc_id          in number
   ,i_check_status    in number
   ,i_reason_nok      in number
   ,i_response        in varchar2
  ) is
    l_rec      ks_responses%rowtype;
    l_def_date date := trunc(sysdate) + 1;
  begin
    select seq_ks_responses.nextval into l_rec.id from dual;

    l_rec.time_created    := sysdate;
    l_rec.is_active       := 1;
    l_rec.file_id         := i_file_id;
    l_rec.first_name      := i_first_name;
    l_rec.last_name       := i_last_name;
    l_rec.nationality     := i_nationality;
    l_rec.issuing_country := i_issuing_country;
    l_rec.doc_number      := i_doc_number;
    l_rec.expiry_date     := i_expiry_date;
    l_rec.personal_id     := i_personal_id;
    l_rec.date_of_birth   := i_date_of_birth;
    l_rec.gender          := i_gender;
    l_rec.doc_id          := i_doc_id;
    l_rec.check_status    := i_check_status;
    l_rec.reason_nok      := i_reason_nok;
    l_rec.response        := i_response;

    insert into ks_responses values l_rec;

    update files f
    set    f.ks_status_id   = 3
          ,f.ks_status_date = sysdate
          ,f.writer_id     =
           (select id from writers where user_name = 'KEESING')
          ,f.exp_date = (case when l_rec.check_status = 0 and l_rec.expiry_date is not null 
                                   then l_rec.expiry_date 
                                       else f.exp_date 
                         end) 
    where  f.id = i_file_id;

    insert into ks_compare_statuses
      (ks_responses_id, flag_first_name, flag_last_name, flag_country, flag_date_of_birth, flag_gender)
      select l_rec.id
            ,case
               when l_rec.first_name is null
                    or upper(u.first_name) != upper(l_rec.first_name) then
                1
               else
                0
             end
            ,case
               when l_rec.last_name is null
                    or upper(u.last_name) != upper(l_rec.last_name) then
                1
               else
                0
             end
            ,case
               when l_rec.nationality is null
                    or c.a3 != l_rec.nationality then
                1
               else
                0
             end
            ,case
               when nvl(trunc(l_rec.date_of_birth), l_def_date) != nvl(trunc(u.time_birth_date), l_def_date) then
                1
               else
                0
             end
            ,case
               when nvl(upper(l_rec.gender), 'x') != nvl(upper(u.gender), 'x') then
                1
               else
                0
             end
      from   files f
      join   users u
      on     u.id = f.user_id
      join   countries c
      on     c.id = u.country_id
      where  f.id = i_file_id;

  end create_ks_record;

  procedure mark_file_ks_failed(i_file_id in number) is
  begin
    update files f set f.ks_status_id = 4 where f.id = i_file_id;
  end mark_file_ks_failed;

  procedure get_file_diffs
  (
    o_diffs   out sys_refcursor
   ,i_file_id in number
  ) is
  begin
    open o_diffs for
      select u.first_name u_first_name
            ,k.first_name k_first_name
            ,ks.flag_first_name
            ,u.last_name u_last_name
            ,k.last_name k_last_name
            ,ks.flag_last_name
            ,c.country_name u_country
            ,ck.country_name k_country
            ,ks.flag_country
            ,trunc(u.time_birth_date) u_date_of_birth
            ,trunc(k.date_of_birth) k_date_of_birth
            ,ks.flag_date_of_birth
            ,u.gender u_gender
            ,k.gender k_gender
            ,ks.flag_gender
            ,k.id ks_response_id
      from   files f
      join   ks_responses k
      on     k.file_id = f.id
      join   ks_compare_statuses ks
      on     ks.ks_responses_id = k.id
      join   users u
      on     u.id = f.user_id
      join   countries c
      on     c.id = u.country_id
      left   join countries ck
      on     ck.a3 = k.nationality
      where  f.id = i_file_id
      and    k.is_active = 1;

  end get_file_diffs;

  -- this should update only a keesing file but
  -- backend calls it not only for keesing files!
  procedure reupload_ks_file(i_file_id in number) is
  begin
    for i in (select f.id
              from   files f
              join   file_types t
              on     t.id = f.file_type_id
              where  f.id = i_file_id
              and    t.is_keesing_type = 1)
    loop
      update ks_responses
      set    is_active = 0
      where  case
               when is_active = 1 then
                file_id
             end = i.id;

      update files f set f.ks_status_id = 1, f.ks_status_date = sysdate where f.id = i.id;
    end loop;
  end reupload_ks_file;

  procedure update_ks_match
  (
    i_ks_response_id     in number
   ,i_flag_first_name    in number
   ,i_flag_last_name     in number
   ,i_flag_country       in number
   ,i_flag_date_of_birth in number
   ,i_flag_gender        in number
   ,i_writer_id          in number
  ) is
    l_first_name             users.first_name%type;
    l_last_name              users.last_name%type;
    l_country_id             number;
    l_date_of_birth          date;
    l_gender                 users.gender%type;
    l_user_id                number;
    
    l_first_name_before      users.first_name%type;
    l_last_name_before       users.last_name%type;
    l_country_id_before      number;
    l_date_of_birth_before   date;
    l_gender_before          users.gender%type;
    l_first_name_after      users.first_name%type;
    l_last_name_after       users.last_name%type;
    l_country_id_after      number;
    l_date_of_birth_after   date;
    l_gender_after          users.gender%type;
  begin
    update ks_compare_statuses s
    set    flag_first_name    = nvl(i_flag_first_name, flag_first_name)
          ,flag_last_name     = nvl(i_flag_last_name, flag_last_name)
          ,flag_country       = nvl(i_flag_country, flag_country)
          ,flag_date_of_birth = nvl(i_flag_date_of_birth, flag_date_of_birth)
          ,flag_gender        = nvl(i_flag_gender, flag_gender)
    where  s.ks_responses_id = i_ks_response_id;

    if sql%notfound
    then
      raise_application_error(-20004, 'Keesing response ID does not exist: ' || i_ks_response_id);
    end if;

    if i_flag_first_name = 2
       or i_flag_last_name = 2
       or i_flag_country = 2
       or i_flag_date_of_birth = 2
       or i_flag_gender = 2
    then
      select r.first_name, r.last_name, c.id, trunc(r.date_of_birth), upper(r.gender), f.user_id
      into   l_first_name, l_last_name, l_country_id, l_date_of_birth, l_gender, l_user_id
      from   ks_responses r
      join   files f
      on     f.id = r.file_id
      join   countries c
      on     c.a3 = r.nationality
      where  r.id = i_ks_response_id;
      
      select u.first_name, u.last_name, u.country_id, trunc(u.time_birth_date), u.gender
      into   l_first_name_before, l_last_name_before, l_country_id_before, l_date_of_birth_before, l_gender_before
      from   users u
      where u.id = l_user_id;

      update users u
      set    u.first_name = case
                              when i_flag_first_name = 2 then
                               l_first_name
                              else
                               u.first_name
                            end
            ,u.last_name = case
                             when i_flag_last_name = 2 then
                              l_last_name
                             else
                              u.last_name
                           end
            ,u.country_id = case
                              when i_flag_country = 2 then
                               l_country_id
                              else
                               u.country_id
                            end
            ,u.time_birth_date = case
                                   when i_flag_date_of_birth = 2 then
                                    l_date_of_birth
                                   else
                                    u.time_birth_date
                                 end
            ,u.gender = case
                          when i_flag_gender = 2 then
                           l_gender
                          else
                           u.gender
                        end
      where  id = l_user_id;

      --After
      select u.first_name, u.last_name, u.country_id, trunc(u.time_birth_date), u.gender
      into   l_first_name_after, l_last_name_after, l_country_id_after, l_date_of_birth_after, l_gender_after
      from   users u
      where u.id = l_user_id;
      
--FN 1
IF nvl(l_first_name_before,'x') != nvl(l_first_name_after,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    l_user_id
   ,i_writer_id
   ,1
   ,l_first_name_before
   ,l_first_name_after);
END IF;
--LN 2
IF nvl(l_last_name_before,'x') != nvl(l_last_name_after,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    l_user_id
   ,i_writer_id
   ,2
   ,l_last_name_before
   ,l_last_name_after);
END IF;
--Country 9
IF nvl(l_country_id_before,0) != nvl(l_country_id_after,0) THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    l_user_id
   ,i_writer_id
   ,9
   ,l_country_id_before
   ,l_country_id_after);
END IF;
--Birthdate 6
IF nvl(l_date_of_birth_before,trunc(sysdate)) != nvl(l_date_of_birth_after,trunc(sysdate)) THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    l_user_id
   ,i_writer_id
   ,6
   ,l_date_of_birth_before
   ,l_date_of_birth_after);
END IF;
--Gender 4
IF nvl(l_gender_before,'x') != nvl(l_gender_after,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    l_user_id
   ,i_writer_id
   ,4
   ,l_gender_before
   ,l_gender_after);
END IF;

    end if;
  end update_ks_match;

  procedure get_active_ks_response
  (
    o_response out sys_refcursor
   ,i_file_id  in number
  ) is
  begin
    open o_response for
      select k.id            ks_response_id
            ,k.time_created
            ,k.is_active
            ,k.first_name
            ,k.last_name
            ,cn.country_name nationality
            ,ci.a3           issuing_country
            ,k.doc_number
            ,k.expiry_date
            ,k.personal_id
            ,k.date_of_birth
            ,k.gender
            ,k.doc_id
            ,k.check_status
            ,k.reason_nok
      from   ks_responses k
      left   join countries cn
      on     cn.a3 = k.nationality
      left   join countries ci
      on     ci.a3 = k.issuing_country
      where  case
               when is_active = 1 then
                file_id
             end = i_file_id;

  end get_active_ks_response;
  
  procedure is_exist_file_type
  (   
    i_user_id    in number
   ,i_file_type  in number
   ,i_cc_id      in number
   ,o_is_exist   out number
  ) is
  
  cursor c_is_exist is
  select 1 
   from files f
   where f.file_type_id = i_file_type
   and f.user_id = i_user_id
   and nvl(f.cc_id,0) = i_cc_id
   and f.is_current = 1;
  
  begin
    o_is_exist:= 0;
    open c_is_exist;
    fetch c_is_exist into o_is_exist;
    close c_is_exist;
 end;
 
  procedure create_gbg_record
  (
    i_user_id         in number,
    i_country_id      in number
  ) is
    l_rec      gbg_responses%rowtype;
    
    cursor c_exist_profile_id is
    select 'x' 
    from gbg_countries g
    where g.country_id = i_country_id
    and not exists (select user_id from gbg_responses where user_id = i_user_id);
    
    v_tmp varchar2(1);     
    
  begin
    open c_exist_profile_id;
    fetch c_exist_profile_id into v_tmp;
    if c_exist_profile_id%found then 
      l_rec.status_id := 0;
      l_rec.time_created := sysdate;
      select SEQ_GBG_RESPONSES.nextval into l_rec.id from dual;
      l_rec.user_id    := i_user_id;    

      insert into gbg_responses values l_rec;    
    end if;
    close c_exist_profile_id;  
  end create_gbg_record;
  
  procedure load_gbg_users_for_send
     (
       o_gbg_users out sys_refcursor
      ) is
    begin
    
    --UPDATE ALL USERS WITH WIRE DEPOSITS(2) to state -1
    update gbg_responses gr
     set gr.status_id = -1
     where gr.status_id = 0
     and gr.user_id in (select t.user_id 
                         from transactions t
                              ,users u
                              ,gbg_responses rr
                         where t.user_id = u.id
                              and t.id = u.first_deposit_id
                              and u.id = rr.user_id
                              and rr.status_id = 0
                              AND t.type_id = 2);
    --UPDATE ALL USERS WITH time_birth_date IS NULL OR u.street IS NULL OR city_name IS NULL to state -2
    update gbg_responses gr
     set gr.status_id = -2
     where gr.status_id = 0
     and gr.user_id in (select rr.user_id 
                         from  
                               users u
                              ,gbg_responses rr
                         where 
                              u.id = rr.user_id
                              and rr.status_id = 0
                              and (u.time_birth_date IS NULL OR u.street IS NULL OR city_name IS NULL)
                             );                              
    
    
      open o_gbg_users for
           select 
               g.id
              ,g.user_id
              ,gc.profile_id
              ,u.first_name
              ,u.last_name
              ,case when u.gender = 'M' then 'Male' 
                        when u.gender = 'F' then 'Female'  else '' end as gender
              ,to_char(u.time_birth_date,'dd')  DOBDay
              ,to_char(u.time_birth_date,'mm')  DOBMonth
              ,to_char(u.time_birth_date,'yyyy')  DOBYear
              ,c.country_name
              ,u.street 
              ,u.street_no
              ,u.city_name
              ,u.zip_code

               from 
                 gbg_responses g,
                 users u,
                 countries c,
                 gbg_countries gc
               where g.status_id = 0
               and u.id = g.user_id
               and u.country_id = c.id
               and c.id = gc.country_id
               order by g.time_created;
   end load_gbg_users_for_send;
   
   procedure update_gbg_status_id
     (
       i_gbg_id    in number,
       i_status_id in number
      ) is
    begin
      update 
        gbg_responses t
      set 
         t.status_id = i_status_id,
         t.time_state_update = sysdate
      where 
         t.id = i_gbg_id;
   end update_gbg_status_id;
   
   procedure update_gbg_response
     (
       o_resault_by_score out number,
       i_gbg_id           in number,
       i_score            in number,
       i_file_id          in number,
       i_error_code       in varchar2,
       i_error_reason     in varchar2,
       i_request_xml      in clob, 
       i_response_xml     in clob
      ) is
    begin
    
    o_resault_by_score :=0;
    
    if(i_score < 0) then
       --PEP Suspend, issue action and reject file
       o_resault_by_score := 1;
    elsif (i_score >= 0 and i_score <= 1110 ) then
       --Reject file Low GBG score
       o_resault_by_score := 2;         
    elsif (i_score >=  1111 and i_score <= 999999 ) then
       -- Approve file
       o_resault_by_score := 3;               
    else 
       o_resault_by_score := 0;
    end if;    
    
      update gbg_responses
         set              
             score = i_score,
             file_id = i_file_id,
             error_code = i_error_code,
             error_reason = i_error_reason,
             status_id = 3,
             request_xml = i_request_xml,
             response_xml = i_response_xml,
             time_state_update = sysdate,
             STATE_AFTER_RESPONSE = o_resault_by_score
       where id = i_gbg_id;                       
   end update_gbg_response;   
   
  procedure load_gbg_countries
     (
       o_gbg_countries out sys_refcursor
     ) is
    begin
      open o_gbg_countries for
        select *
        from gbg_countries;

  end load_gbg_countries;         
  
  procedure update_gbg_file_id
     (
       i_gbg_id    in number,
       i_file_id in number
      ) is
    begin
      update 
        gbg_responses t
      set 
         t.file_id = i_file_id         
      where 
         t.id = i_gbg_id;
   end update_gbg_file_id;
   
  procedure get_gbg_result
  (
    o_result out sys_refcursor
   ,i_file_id  in number
  ) is
  begin
    open o_result for
      select k.id,
             k.request_xml,
             k.response_xml
      
      from   
            gbg_responses k    
      where                  
            file_id = i_file_id
      order by 
            k.id desc;

  end get_gbg_result;
  
end pkg_userfiles;
/
