create or replace package pkg_transactions is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-12 10:07:59
  -- Purpose : /etrader_level_service/src/il/co/etrader/service/transactionsIssues/daos/TransactionsIssuesDAO.java

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  );

  PROCEDURE GET_GM_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                I_USER_ID  IN NUMBER);
                                
  PROCEDURE GET_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                O_IDEAL    OUT NUMBER,
                                I_USER_ID  IN NUMBER);                                
                                
  PROCEDURE GET_TOTAL_DEPOSITS(O_TOTAL_DEPOSITS OUT NUMBER,
                               O_TOTAL_WITHDRAWALS OUT NUMBER,
                               I_GM_TYPE        IN NUMBER,
                               I_USER_ID        IN NUMBER);
                               
  PROCEDURE GET_TRANSACTION_CHANGES(I_TRANSACTION_ID IN NUMBER,
                                    O_TYPES OUT SYS_REFCURSOR,
                                    O_STATUSES OUT SYS_REFCURSOR);
end pkg_transactions;
/
create or replace package body pkg_transactions is

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  ) is
    l_fromdate date;
    l_todate   date;
  begin
    select date_value - i_last_minutes/(24*60), sysdate - i_last_minutes/(24*60)
    into   l_fromdate, l_todate
    from   db_parameters
    where  name = 'last_first_deposit_email_run';
  
    open o_data for
      select u.id
            ,u.user_name
            ,u.skin_id
            ,u.mobile_phone
            ,u.land_line_phone
            ,u.currency_id
            ,t.amount          first_deposit_amount
            ,t.time_created    first_deposit_time_created
            ,c.phone_code      country_phone_code
            ,c.country_name    country_name
      from   transactions t
      join   users u
      on     u.id = t.user_id
      and    u.first_deposit_id = t.id
      join   countries c
      on     u.country_id = c.id
      where  t.time_created between l_fromdate and l_todate
      and    not exists (select 1 from transactions_issues where transaction_id = t.id)
      and    u.class_id <> 0;
  
  end get_last_trnsusers;
  
  PROCEDURE GET_GM_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                I_USER_ID  IN NUMBER) IS
  
  BEGIN
  
    SELECT COUNT(*)
      INTO O_DIRECT24
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 2
       AND T.STATUS_ID = 2; -- only status "succeed"
  
    SELECT COUNT(*)
      INTO O_GIROPAY
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 3
       AND T.STATUS_ID = 2;
  
    SELECT COUNT(*)
      INTO O_EPS
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 4
       AND T.STATUS_ID = 2;
  
  END GET_GM_PERM_DISPLAY;
  
  PROCEDURE GET_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                O_IDEAL    OUT NUMBER,
                                I_USER_ID  IN NUMBER) IS
  
  BEGIN
  
    SELECT COUNT(*)
      INTO O_DIRECT24
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 2
       AND T.STATUS_ID = 2; -- only status "succeed"
  
    SELECT COUNT(*)
      INTO O_GIROPAY
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 3
       AND T.STATUS_ID = 2;
  
    SELECT COUNT(*)
      INTO O_EPS
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 4
       AND T.STATUS_ID = 2;
    
    SELECT COUNT(*)
      INTO O_IDEAL
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 54
       AND T.STATUS_ID = 2;
  
  END GET_PERM_DISPLAY;
 
  PROCEDURE GET_TOTAL_DEPOSITS(O_TOTAL_DEPOSITS OUT NUMBER,
                               O_TOTAL_WITHDRAWALS OUT NUMBER,
                               I_GM_TYPE        IN NUMBER,
                               I_USER_ID        IN NUMBER) IS
 
    L_TRANSACTION_TYPE NUMBER := 17;
  
    DIRECT24_WITHDRAW CONSTANT NUMBER := 59;
    GIROPAY_WITHDRAW  CONSTANT NUMBER := 60;
    EPS_WITHDRAW      CONSTANT NUMBER := 61;
    IDEAL_WITHDRAW    CONSTANT NUMBER := 63;

  
  BEGIN    
     IF I_GM_TYPE = IDEAL_WITHDRAW THEN  
        L_TRANSACTION_TYPE := 54;
     END IF;
     
        SELECT nvl(SUM(T.AMOUNT), 0) AS TOTAL_DEPOSITS
          INTO O_TOTAL_DEPOSITS
          FROM TRANSACTIONS T
         WHERE T.USER_ID = I_USER_ID
           AND T.TYPE_ID = L_TRANSACTION_TYPE  
           AND T.PAYMENT_TYPE_ID = CASE
                 WHEN I_GM_TYPE = 61 THEN
                  4
                 WHEN I_GM_TYPE = 60 THEN
                  3
                 WHEN I_GM_TYPE = 59 THEN
                  2
                 WHEN I_GM_TYPE = 63 THEN
                  0
               END
           AND T.STATUS_ID = 2;
     
        SELECT 
          NVL(
            SUM(
              CASE
                WHEN TR.TYPE_ID IS NOT NULL 
                THEN T.AMOUNT - TR.AMOUNT
                ELSE T.AMOUNT
              END
              ), 
            0) AS TOTAL_WITHDRAWALS
          INTO O_TOTAL_WITHDRAWALS
        FROM 
          TRANSACTIONS T
          LEFT JOIN TRANSACTIONS TR ON (T.ID = TR.REFERENCE_ID AND TR.TYPE_ID = 6)
        WHERE 
          T.TYPE_ID = I_GM_TYPE
          AND T.USER_ID = I_USER_ID
          AND T.STATUS_ID <> 3; 
    
  END GET_TOTAL_DEPOSITS;

  PROCEDURE GET_TRANSACTION_CHANGES(I_TRANSACTION_ID IN NUMBER,
                                    O_TYPES OUT SYS_REFCURSOR,
                                    O_STATUSES OUT SYS_REFCURSOR) IS
  BEGIN
      OPEN O_TYPES FOR
           SELECT TMP.DESCRIPTION, TMP.CLASS_TYPE, TMP.ID FROM (
             SELECT DESCRIPTION, CLASS_TYPE, ID 
                    FROM TRANSACTION_TYPES
                    WHERE ID IN (
                (SELECT TYPE_ID 
                 FROM TRANSACTION_TYPES_CHANGES
                 WHERE TO_TYPE = 1 
                       AND TYPE_ID <> 
                        (SELECT TT.ID
                          FROM TRANSACTIONS T 
                          INNER JOIN TRANSACTION_TYPES_CHANGES TTC 
                              ON T.TYPE_ID = TTC.TYPE_ID
                          JOIN TRANSACTION_TYPES TT
                              ON TTC.TYPE_ID = TT.ID
                          WHERE T.ID = I_TRANSACTION_ID
                              AND TTC.FROM_TYPE = 1   
                          )
                  )
                )
            ) TMP
            JOIN TRANSACTIONS T 
            ON T.ID = I_TRANSACTION_ID
            JOIN TRANSACTION_TYPES TT 
            ON T.TYPE_ID = TT.ID
            WHERE TMP.CLASS_TYPE = 
                  (CASE
                       WHEN TT.CLASS_TYPE = 1 THEN 1 
                       WHEN TT.CLASS_TYPE = 5 THEN 1 
                       WHEN TT.CLASS_TYPE = 2 THEN 2 
                       WHEN TT.CLASS_TYPE = 6 THEN 2 
                  END)
            ;
        
      OPEN O_STATUSES FOR
        SELECT TMP.DESCRIPTION, TMP.ID FROM (
          SELECT TS.DESCRIPTION, TS.ID 
          FROM TRANSACTION_STATUSES TS
          WHERE TS.ID IN (
                      SELECT DISTINCT STATUS_ID 
                      FROM TRANSACTION_STATUSES_CHANGES
                      WHERE TO_TYPE = 1 
                            AND STATUS_ID <> 
                              (SELECT TSC.STATUS_ID STATUS
                               FROM TRANSACTIONS T
                               INNER JOIN TRANSACTION_STATUSES_CHANGES TSC
                               ON TSC.TYPE_ID = T.TYPE_ID
                                  AND TSC.STATUS_ID = T.STATUS_ID
                               JOIN TRANSACTION_TYPES TT
                               ON T.TYPE_ID = TT.ID
                               JOIN TRANSACTION_STATUSES TS
                               ON T.STATUS_ID = TS.ID
                               WHERE T.ID = I_TRANSACTION_ID
                                     AND TSC.FROM_TYPE = 1
                              )
                      ) 
          ) TMP
      JOIN TRANSACTIONS T 
      ON T.ID = I_TRANSACTION_ID 
      JOIN TRANSACTION_TYPES TT
      ON T.TYPE_ID = TT.ID
      WHERE ( (TT.CLASS_TYPE = 1 AND TMP.ID <> 4) 
              OR (TT.CLASS_TYPE = 2 AND TMP.ID <> 11)
            );
  END GET_TRANSACTION_CHANGES;	
  
end pkg_transactions;
/
