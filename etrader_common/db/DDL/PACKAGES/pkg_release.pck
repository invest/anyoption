create or replace package pkg_release is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-04-28 16:16:21
  -- Purpose : helper procedures for release process

  procedure ddl_helper
  (
    i_sql              in varchar2
   ,i_ignore_exception in number default null
   ,i_ddl_lock_timeout in number default null
  );

  procedure update_version(i_new_version in varchar2);
  procedure check_version(i_expected_version in varchar2);
  procedure check_dbuser;

end pkg_release;
/
create or replace package body pkg_release is

  procedure save_log
  (
    i_sql              in varchar2
   ,i_ignore_exception in number
  ) is
    pragma autonomous_transaction;
  begin
    insert into release_execution_log (execute_time, sql_statement, ignore_exception) values (systimestamp, i_sql, i_ignore_exception);
    commit;
  end save_log;

  procedure ddl_helper
  (
    i_sql              in varchar2
   ,i_ignore_exception in number default null
   ,i_ddl_lock_timeout in number default null
  ) is
  begin
    if i_ddl_lock_timeout is not null
    then
      -- DDLs will wait so much seconds for lock before time out
      execute immediate 'alter session set ddl_lock_timeout=' || i_ddl_lock_timeout;
    end if;
  
    if i_sql is not null
    then
      save_log(i_sql, i_ignore_exception);
      execute immediate i_sql;
    end if;
  exception
    when others then
      if sqlcode = i_ignore_exception
      then
        dbms_output.put_line('Ignored exception: ' || i_ignore_exception);
      else
        raise;
      end if;
  end ddl_helper;

  procedure update_version(i_new_version in varchar2) is
  begin
    insert into release_db_version_history
      (curr_version, prev_version, upload_date)
      select i_new_version, value, sysdate from release_db_properties where key = 'db_version';
  
    update release_db_properties set value = i_new_version where key = 'db_version';
  end update_version;

  procedure check_dbuser is
  begin
    if sys_context('USERENV', 'SESSION_USER') != 'ETRADER'
       or sys_context('USERENV', 'CURRENT_SCHEMA') != 'ETRADER'
       or sys_context('USERENV', 'PROXY_USER') is null
    then
      raise_application_error(-20000, 'Upload should be done with ETRADER user');
    end if;
  end check_dbuser;

  procedure check_version(i_expected_version in varchar2) is
    l_current_version varchar2(50);
  begin
    select value into l_current_version from release_db_properties where key = 'db_version';
  
    if l_current_version != i_expected_version
    then
      raise_application_error(-20001, 'Current version is ' || l_current_version || '. Expecting version ' || i_expected_version);
    end if;
  end check_version;
end pkg_release;
/
