create or replace package pkg_accret is

-- Author  : VICTOR.SLAVOV
-- Created : 2016-09-21 15:30:51
-- Purpose : 
  procedure load_filters
  (
    o_user_ranks              out sys_refcursor
   ,o_representatives         out sys_refcursor
   ,o_skins                   out sys_refcursor
  );
  
  procedure get_call_status
  (
    o_call_statuses out sys_refcursor
   ,i_fromdate          in date
   ,i_todate            in date
   ,i_userrank          in number
   ,i_writer_id         in number
   ,i_skin_id           in number
   ,i_page_number       in number
   ,i_rows_per_page     in number
   --,i_retention     in number
  );

end pkg_accret;


create or replace package body pkg_accret is

  procedure load_filters
  (
    o_user_ranks              out sys_refcursor
   ,o_representatives         out sys_refcursor
   ,o_skins                   out sys_refcursor
  ) is
  begin
    open o_user_ranks for
      select u.id id
      , u.rank_name value 
      from users_rank u;

    open o_representatives for
      select 
          w.id id
          ,w.user_name value
      from writers w 
      where 
          w.dept_id not in (9) 
          and w.sales_type in (2,3) 
          and is_active=1 
      order by w.user_name;

    open o_skins for
      select s.id id 
      ,s.name value
      from skins s order by s.id;
  
  end load_filters;

  procedure get_call_status
  (
    o_call_statuses out sys_refcursor
   ,i_fromdate          in date
   ,i_todate            in date
   ,i_userrank          in number
   ,i_writer_id         in number
   ,i_skin_id           in number
   ,i_page_number       in number
   ,i_rows_per_page     in number
   --,i_retention     in number
  ) is
  begin
  
    if i_todate - i_fromdate > 31
    then
      raise_application_error(-20000, 'Period is larger than a month');
    end if;
  
    open o_call_statuses for
     select 
     	writer_id,
		user_name,
		not_reached,
		reached,
		(total_assigned - call_assigned) as not_called ,
		daily_avg,
		rn,
		total_count
      from   (
      select wr.id writer_id
      ,wr.user_name
      ,nvl(sum(case
                 when not_reached is not null
                      and reached is null then
                  1
               end)
          ,0) not_reached
      ,nvl(sum(case
                 when reached is not null then
                  1
               end)
          ,0) reached
      ,nvl(sum(case
                 when 
                    is_curr_assign is not null 
                    and (reached is not null 
                    or not_reached is not null)
                 then
                  1
               end)
          ,0) call_assigned
      ,(SELECT 
          count(pu.id)
        FROM
          POPULATION_USERS pu,
          users u
        where
          pu.CURR_ASSIGNED_WRITER_ID = wr.id
          and pu.USER_ID = u.id
          and (i_userrank is null or u.rank_id = i_userrank)          
          and (i_skin_id is null or u.skin_id = i_skin_id)
      ) total_assigned
      ,trunc((nvl(sum(case
                        when not_reached is not null
                             and reached is null then
                         1
                      end)
                 ,0) + nvl(sum(case
                                  when reached is not null then
                                   1
                                end)
                           ,0)) / (i_todate - i_fromdate)) daily_avg    
               ,row_number() over(order by wr.user_name desc, wr.id desc) rn
               ,count(*) over() total_count
                           
from   writers wr
        left join
		(select a.writer_id
               ,s.user_id
               ,max(case
                      when 
                           at.reached_status_id = 2 then
                       1
                    end) reached
               ,max(case
                      when 
                           at.reached_status_id = 1 then
                       1
                    end) not_reached
			  ,max(case
                      when 
                           pu.USER_ID is not null then
                       1
                    end) is_curr_assign
              from   issues s
              join   issue_actions a
              on     a.issue_id = s.id
              join   users u
              on     u.id = s.user_id
              join   issue_action_types at
              on     at.id = a.issue_action_type_id
              left join POPULATION_USERS pu
              on     pu.USER_ID = u.id and pu.CURR_ASSIGNED_WRITER_ID = a.writer_id
                   where a.action_time >= i_fromdate
              and    a.action_time < i_todate
              and    at.channel_id = 1
              and    at.reached_status_id in (1, 2)
              
              and    (i_userrank is null or u.rank_id = i_userrank)
                    
              and    (i_skin_id is null or u.skin_id = i_skin_id)
              
              group  by a.writer_id, s.user_id
              
              ) iss_info
              on wr.id = iss_info.writer_id
	where  (wr.sales_type in (2, 3)) 
        and    wr.dept_id not in (9)
        and    (i_writer_id is null or wr.id = i_writer_id)
              
      group  by wr.id, wr.user_name
      order by wr.user_name)
  where  rn > (i_page_number - 1) * i_rows_per_page
  and    rn <= i_page_number * i_rows_per_page
  order  by user_name desc, writer_id desc;
      
  end get_call_status;

end pkg_accret;