CREATE OR REPLACE PACKAGE opportunities_manager IS

PROCEDURE CREATE_BITCOIN_OPPORTUNITIES (p_first_inv IN DATE, p_est_closing IN DATE, p_last_inv IN DATE, p_market IN NUMBER DEFAULT 0);

PROCEDURE CREATE_ONETOUCH_OPPORTUNITIES;


--**************CREATE_DAILY_OPPORTUNITIES**************************
--First parameters is days_ahead, second is market id 
PROCEDURE CREATE_DAILY_OPPORTUNITIES (p_days_ahead IN NUMBER, p_market IN NUMBER DEFAULT 0);

--**************PROCEDURE CREATE_WEEK_OPPORTUNITIES*****************
--1.P_flag ? if set to ?1? you can choose custom market to which will be created weekly opportunity
--2.P_days ? number of days ahead: if you set ?0? means current date
--3.P_market ? valid market_ID 
PROCEDURE CREATE_WEEK_OPPORTUNITIES (p_flag IN NUMBER DEFAULT 0, p_days IN NUMBER DEFAULT 8, p_market IN NUMBER DEFAULT NULL);

--**************PROCEDURE CREATE_MONTH_OPPORTUNITIES*****************
--1.P_flag ? if set to ?1? you can choose custom market to which will be created weekly opportunity
--2.P_days ? number of days ahead: if you set ?0? means current date
--3.P_market ? valid market_ID 
PROCEDURE CREATE_MONTH_OPPORTUNITIES (p_flag IN NUMBER DEFAULT 0, p_months IN NUMBER DEFAULT 1, p_market IN NUMBER DEFAULT NULL);

END OPPORTUNITIES_MANAGER;
/
CREATE OR REPLACE PACKAGE BODY opportunities_manager IS


PROCEDURE CREATE_BITCOIN_OPPORTUNITIES (p_first_inv IN DATE, p_est_closing IN DATE, p_last_inv IN DATE, p_market IN NUMBER DEFAULT 0)
IS
   MARKET_BITCOIN_ID   NUMBER := 677;     -- market_id change due to one touch markets separation
   MARKET_USDEUR_ID   NUMBER := 693;      -- market_id change due to one touch markets separation
   MARKET_ALIBABA_ID  NUMBER := 678;      -- market_id change due to one touch markets separation
   MARKET_OIL_ID      NUMBER := 709;      -- market_id change due to one touch markets separation
   MARKET_GOLD_ID      NUMBER := 710;     -- market_id change due to one touch markets separation
   MARKET_DAX_ID      NUMBER := 711;      -- market_id change due to one touch markets separation
   MARKET_SP_ID      NUMBER := 712;     -- market_id change due to one touch markets separation
   
   o_id NUMBER;

   CURSOR cmain
   IS
      SELECT
      A.market_id, 
      current_timestamp a0, 
      to_timestamp_tz(to_char(p_first_inv, 'yyyy-mm-dd') || A.time_first_invest || A.time_zone, 'yyyy-mm-dd hh24:mi TZR') a1, 
      to_timestamp_tz(to_char(p_est_closing, 'yyyy-mm-dd') || A.time_est_closing || A.time_zone, 'yyyy-mm-dd hh24:mi TZR') a2, 
      to_timestamp_tz(to_char(p_last_inv, 'yyyy-mm-dd') || A.time_last_invest || A.time_zone, 'yyyy-mm-dd hh24:mi TZR') a3, 
      1 b1, 0 b2, A.odds_type_id, A.writer_id, A.opportunity_type_id, 0 b3, 
    A.scheduled, A.deduct_win_odds, B.max_exposure, A.UP_DOWN, A.ONE_TOUCH_DECIMAL_POINT, A.max_inv_coeff 
FROM opportunity_templates A LEFT JOIN markets B ON A.market_id = B.id 
     WHERE 
     CASE
                 WHEN p_market != 0 AND A.market_id = p_market THEN 1
                 WHEN p_market = 0 AND A.market_id IN (MARKET_BITCOIN_ID,MARKET_USDEUR_ID, MARKET_OIL_ID, MARKET_GOLD_ID, MARKET_DAX_ID, MARKET_SP_ID) THEN 1
                      ELSE 0
       END=1 AND A.is_active = 1 AND A.OPPORTUNITY_TYPE_ID = 2;
     
BEGIN
savepoint before_insert;
   FOR tmp IN cmain
   LOOP
      INSERT INTO opportunities (id,
                                 market_id,
                                 time_created,
                                 time_first_invest,
                                 time_est_closing,
                                 time_last_invest,
                                 is_published,
                                 is_settled,
                                 odds_type_id,
                                 writer_id,
                                 opportunity_type_id,
                                 is_disabled,
                                 scheduled,
                                 deduct_win_odds,
                                 max_inv_coeff/*,
                                 max_exposure*/)
           VALUES (SEQ_OPPORTUNITIES.NEXTVAL, 
                   tmp.market_id,
                   tmp.a0,
                   tmp.a1,
                   tmp.a2,
                   tmp.a3,
                   tmp.b1,
                   tmp.b2,
                   tmp.odds_type_id,
                   tmp.writer_id,
                   tmp.opportunity_type_id,
                   tmp.b3,
                   tmp.scheduled,
                   tmp.deduct_win_odds,
                   tmp.max_inv_coeff
                   --,tmp.max_exposure
                   )  RETURNING id INTO o_id;
                   
       INSERT INTO opportunity_one_touch (id, opportunity_id, up_down, decimal_point) 
                   VALUES  (SEQ_OPPORTUNITY_ONE_TOUCH.NEXTVAL, o_id, tmp.UP_DOWN, tmp.ONE_TOUCH_DECIMAL_POINT );            
    
        for int_tmp in (select id from skin_groups where id != 0) loop
            INSERT INTO OPPORTUNITY_SKIN_GROUP_MAP
                (ID, OPPORTUNITY_ID, SKIN_GROUP_ID, TIME_CREATED, WRITER_ID, SHIFT_PARAMETER, MAX_EXPOSURE)
            VALUES
                (SEQ_OPPORTUNITY_SKIN_GROUP_MAP.NEXTVAL, o_id, int_tmp.id, SYSDATE, 0, 0, tmp.max_exposure);
        end loop;
        END LOOP;
   
       exception
    when others then
  dbms_output.put_line(sqlerrm);
  raise;
  rollback to before_insert;
  
END CREATE_BITCOIN_OPPORTUNITIES;



PROCEDURE CREATE_ONETOUCH_OPPORTUNITIES
IS
   MARKET_BITCOIN_ID   NUMBER := 677;			-- market_id change due to one touch markets separation
   MARKET_USDEUR_ID   NUMBER := 693;			-- market_id change due to one touch markets separation
   MARKET_ALIBABA_ID  NUMBER := 678;			-- market_id change due to one touch markets separation
   MARKET_OIL_ID      NUMBER := 709;			-- market_id change due to one touch markets separation
   MARKET_GOLD_ID      NUMBER := 710;			-- market_id change due to one touch markets separation
   MARKET_DAX_ID      NUMBER := 711;			-- market_id change due to one touch markets separation
   MARKET_SP_ID      NUMBER := 712;			-- market_id change due to one touch markets separation
   o_id NUMBER;

   CURSOR cmain
   IS
      SELECT A.market_id,
             CURRENT_TIMESTAMP AS a0,
             TO_TIMESTAMP_TZ (
                   TO_CHAR (NEXT_DAY (SYSDATE, 6), 'yyyy-mm-dd')
                || A.time_first_invest
                || A.time_zone,
                'yyyy-mm-dd hh24:mi TZR')
                AS a1,
             TO_TIMESTAMP_TZ (
                   TO_CHAR (NEXT_DAY (SYSDATE, 6) + 7, 'yyyy-mm-dd')
                || A.time_est_closing
                || A.time_zone,
                'yyyy-mm-dd hh24:mi TZR')
                AS a2,
             TO_TIMESTAMP_TZ (
                   TO_CHAR (NEXT_DAY (SYSDATE, 1), 'yyyy-mm-dd')
                || A.time_last_invest
                || A.time_zone,
                'yyyy-mm-dd hh24:mi TZR')
                AS a3,
             1 b1,
             0 b2,
             A.odds_type_id,
             A.writer_id,
             A.opportunity_type_id,
             0 b3,
             A.scheduled,
             A.deduct_win_odds,
             E.VALUE AS max_exposure,
             A.UP_DOWN, 
             A.ONE_TOUCH_DECIMAL_POINT,
             B.EXPOSURE_REACHED_AMOUNT,
             A.max_inv_coeff
        FROM    opportunity_templates A
             LEFT JOIN
                markets B
             ON A.market_id = B.id,
             enumerators E
       WHERE E.code LIKE 'one_touch_max_users'
             AND A.id IN
                    (SELECT id
                       FROM opportunity_templates ot
                      WHERE     ot.is_active = 1
                            AND ot.opportunity_type_id = 2
                            AND ot.scheduled = 5
                            AND ot.market_id NOT IN (MARKET_BITCOIN_ID,MARKET_USDEUR_ID, MARKET_OIL_ID, MARKET_GOLD_ID, MARKET_DAX_ID, MARKET_SP_ID));
BEGIN
savepoint before_insert;
   FOR tmp IN cmain
   LOOP
      INSERT INTO opportunities (id,
                                 market_id,
                                 time_created,
                                 time_first_invest,
                                 time_est_closing,
                                 time_last_invest,
                                 is_published,
                                 is_settled,
                                 odds_type_id,
                                 writer_id,
                                 opportunity_type_id,
                                 is_disabled,
                                 scheduled,
                                 deduct_win_odds,
                                 max_inv_coeff/*,
                                 max_exposure*/)
           VALUES (SEQ_OPPORTUNITIES.NEXTVAL,
                   tmp.market_id,
                   tmp.a0,
                   tmp.a1,
                   tmp.a2,
                   tmp.a3,
                   tmp.b1,
                   tmp.b2,
                   tmp.odds_type_id,
                   tmp.writer_id,
                   tmp.opportunity_type_id,
                   tmp.b3,
                   tmp.scheduled,
                   tmp.deduct_win_odds,
                   tmp.max_inv_coeff
                   --,tmp.max_exposure
                   )  RETURNING id INTO o_id;
                   
  INSERT into opportunity_one_touch
    (id, opportunity_id, up_down, decimal_point, exposure_reached_amount)
  VALUES             
      (SEQ_OPPORTUNITY_ONE_TOUCH.NEXTVAL, o_id, tmp.UP_DOWN, tmp.ONE_TOUCH_DECIMAL_POINT, tmp.EXPOSURE_REACHED_AMOUNT);
      
          for int_tmp in (select id from skin_groups where id != 0) loop
            INSERT INTO OPPORTUNITY_SKIN_GROUP_MAP
                (ID, OPPORTUNITY_ID, SKIN_GROUP_ID, TIME_CREATED, WRITER_ID, SHIFT_PARAMETER, MAX_EXPOSURE)
            VALUES
                (SEQ_OPPORTUNITY_SKIN_GROUP_MAP.NEXTVAL, o_id, int_tmp.id, SYSDATE, 0, 0, tmp.max_exposure);
        end loop;
   END LOOP;
   
       exception
    when others then
  dbms_output.put_line(sqlerrm);
  raise;
  rollback to before_insert;
  
END CREATE_ONETOUCH_OPPORTUNITIES;

PROCEDURE CREATE_DAILY_OPPORTUNITIES_I (p_days_ahead IN NUMBER, p_market IN NUMBER DEFAULT 0) IS

l_id         NUMBER;

cursor cmain(p_days_ahead NUMBER) is
SELECT A.market_id,
                     current_timestamp as a0,
                     to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || A.time_first_invest || A.time_zone, 'yyyy-mm-dd hh24:mi TZR') as a1,
                     to_timestamp_tz(to_char(current_date + p_days_ahead + CASE
                                                                         WHEN A.time_first_invest > A.time_est_closing THEN
                                                                            1
                                                                         ELSE
                                                                            0
                                                                     END,
                                                                     'yyyy-mm-dd') || A.time_est_closing || A.time_zone,
                                                     'yyyy-mm-dd hh24:mi TZR') as a2,
                     to_timestamp_tz(to_char(current_date + p_days_ahead + CASE
                                                                         WHEN A.time_first_invest > A.time_last_invest THEN
                                                                            1
                                                                         ELSE
                                                                            0
                                                                     END,
                                                                     'yyyy-mm-dd') || A.time_last_invest || A.time_zone,
                                                     'yyyy-mm-dd hh24:mi TZR') as a3,
                     0 b0,
                     0 b1,
                     A.odds_type_id,
                     A.writer_id,
                     A.opportunity_type_id,
                     0 b2,
                     A.scheduled,
                     A.deduct_win_odds,
                     CASE
                         WHEN MARKET_GROUP_ID in (4, 5) AND
                                    (to_char(SYS_EXTRACT_UTC(to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || A.time_first_invest || A.time_zone, 'yyyy-mm-dd hh24:mi TZR')),
                                                     'hh24:mi') >= '22:00' OR
                                    to_char(SYS_EXTRACT_UTC(to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || A.time_first_invest || A.time_zone, 'yyyy-mm-dd hh24:mi TZR')),
                                                     'hh24:mi') < '06:00') THEN
                            (SELECT to_number(e.value) FROM enumerators e WHERE e.code LIKE 'NIGHT_MAX_EXPOSURE')
                         ELSE
                            B.max_exposure
                     END as a4,
                     A.is_open_graph,
                     B.quote_params,
                     A.odds_group,
                     B.worst_case_return,
                     A.max_inv_coeff
            FROM opportunity_templates A
            LEFT JOIN markets B
                ON A.market_id = B.id
            LEFT JOIN exchange_holidays C
                ON B.exchange_id = C.exchange_id
             AND trunc(current_date + p_days_ahead) = trunc(holiday)
            LEFT JOIN exchange_holidays eh
                ON B.exchange_id = eh.exchange_id
             AND trunc(current_date + p_days_ahead + 1) = trunc(eh.holiday)
         WHERE (     (to_char(current_date + p_days_ahead, 'D') = 7 AND A.is_full_day = 3) OR
                     ((B.exchange_id = 1 OR B.exchange_id = 3) AND to_char(current_date + p_days_ahead, 'D') = 1 AND C.id IS NULL AND A.is_full_day = 2) OR
                     ((B.exchange_id = 1 OR B.exchange_id = 3)  AND NOT to_char(current_date + p_days_ahead, 'D') = 1 AND (NOT to_char(current_date + p_days_ahead,'D') = 6 OR A.market_id = 638)  AND
                     NOT to_char(current_date + p_days_ahead, 'D') = 7 AND C.id IS NULL AND A.is_full_day = 1) OR
                     ((B.exchange_id = 17 OR B.exchange_id = 18) AND NOT to_char(current_date + p_days_ahead, 'D') = 6 AND NOT to_char(current_date + p_days_ahead, 'D') = 7 AND C.id IS NULL AND
                     A.is_full_day = 1) OR ((B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 18) AND NOT to_char(current_date + p_days_ahead, 'D') = 6 AND
                     NOT to_char(current_date + p_days_ahead, 'D') = 7 AND NOT C.id IS NULL AND C.is_half_day = 1 AND A.is_half_day = 1) OR
                     (A.market_id = 15 AND to_char(current_date + p_days_ahead, 'D') IN (2, 3, 4, 5) AND C.id IS NULL AND A.is_full_day = 1) OR
                     (A.market_id = 15 AND to_char(current_date + p_days_ahead, 'D') IN (2, 3, 4, 5) AND NOT C.id IS NULL AND C.is_half_day = 1 AND A.is_half_day = 1) OR
                     (A.market_id = 15 AND to_char(current_date + p_days_ahead, 'D') = 6 AND C.id IS NULL AND A.is_half_day = 1) OR
                     (A.market_id = 370 AND to_char(current_date + p_days_ahead, 'D') IN (2, 3, 4, 5) AND C.id IS NULL AND A.is_full_day = 1) OR
                     (A.market_id = 370 AND to_char(current_date + p_days_ahead, 'D') IN (2, 3, 4, 5) AND NOT C.id IS NULL AND C.is_half_day = 1 AND A.is_half_day = 1) OR
                     (A.market_id = 370 AND to_char(current_date + p_days_ahead, 'D') = 6 AND C.id IS NULL AND A.is_half_day = 1) OR
                     (NOT (B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 16 OR B.exchange_id = 18) AND NOT A.market_id = 15 AND NOT A.market_id = 370 AND
                     NOT to_char(current_date + p_days_ahead, 'D') = 7 AND NOT to_char(current_date + p_days_ahead, 'D') = 1 AND C.id IS NULL AND A.is_full_day = 1) OR
                     (NOT (B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 16 OR B.exchange_id = 18) AND NOT A.market_id = 15 AND NOT A.market_id = 370 AND
                     NOT to_char(current_date + p_days_ahead, 'D') = 7 AND NOT to_char(current_date + p_days_ahead, 'D') = 1 AND NOT C.id IS NULL AND C.is_half_day = 1 AND
                     A.is_half_day = 1) OR 
                     ((B.exchange_id = 16) AND to_char(current_date + p_days_ahead, 'D') IN (1, 2, 3, 4) AND C.id IS NULL AND A.is_full_day = 1) OR
                     ((B.exchange_id = 16) AND to_char(current_date + p_days_ahead, 'D') IN (1, 2, 3, 4) AND NOT C.id IS NULL AND C.is_half_day = 1 AND A.is_half_day = 1) OR
                     ((A.market_id in (289, 16, 290, 581, 552, 561, 18)) AND (to_char(current_date + p_days_ahead, 'D') = 1) AND A.scheduled = 1 AND
                     to_timestamp_tz(to_char(current_date + p_days_ahead + CASE
                                                                             WHEN A.time_first_invest > A.time_est_closing THEN
                                                                                1
                                                                             ELSE
                                                                                0
                                                                         END,
                                                                         'yyyy-mm-dd') || A.time_first_invest || A.time_zone,
                                                         'yyyy-mm-dd hh24:mi TZR') >= to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || '22:00 Europe/London', 'yyyy-mm-dd hh24:mi TZR')) OR
                     ((A.market_id in (289, 16, 290, 581, 552, 561, 18)) AND (NOT C.id IS NULL AND eh.id IS NULL) AND A.scheduled = 1 AND
                     to_timestamp_tz(to_char(current_date + p_days_ahead + CASE
                                                                             WHEN A.time_first_invest > A.time_est_closing THEN
                                                                                1
                                                                             ELSE
                                                                                0
                                                                         END,
                                                                         'yyyy-mm-dd') || A.time_first_invest || A.time_zone,
                                                         'yyyy-mm-dd hh24:mi TZR') >= to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || '22:00 Europe/London', 'yyyy-mm-dd hh24:mi TZR')) OR
                     (A.market_id in (138, 20, 137) and to_char(current_date + p_days_ahead, 'D') = 1 AND A.time_first_invest >= '20:00' AND A.scheduled = 1))
             AND A.is_active = 1
             AND A.market_id != 436
             AND (A.scheduled = 1 OR
                     (A.scheduled = 2 AND (NOT EXISTS (SELECT 1
                                                                                                FROM opportunity_templates ot
                                                                                             WHERE A.market_id = ot.market_id
                                                                                                 AND ot.is_active = 1
                                                                                                 AND ot.scheduled in (3, 4)) OR
                        (NOT trunc(current_date + p_days_ahead) = Week_last_working_day(B.exchange_id, floor((to_char(current_date, 'D') + p_days_ahead) / 7)) AND
                        NOT trunc(current_date + p_days_ahead) = Month_last_working_day(B.exchange_id, 0)))))
             AND NOT (A.scheduled = 1 AND to_char(current_date + p_days_ahead, 'D') = 6 AND
                        to_timestamp_tz(to_char(current_date + p_days_ahead + CASE
                                                                                    WHEN A.time_first_invest > A.time_est_closing THEN
                                                                                     1
                                                                                    ELSE
                                                                                     0
                                                                                END,
                                                                                'yyyy-mm-dd') || A.time_est_closing || A.time_zone,
                                                                'yyyy-mm-dd hh24:mi TZR') > to_timestamp_tz(to_char(current_date + p_days_ahead, 'yyyy-mm-dd') || '20:30 GMT', 'yyyy-mm-dd hh24:mi TZR'))
AND    CASE
                 WHEN p_market != 0 AND A.market_id = p_market THEN 1
                 WHEN p_market = 0 AND A.market_id = A.market_id THEN 1
                      ELSE 0
            END=1;

BEGIN
    for tmp in cmain(p_days_ahead) loop
        INSERT INTO OPPORTUNITIES --TEST_OPP
            (id,
             market_id,
             time_created,
             time_first_invest,
             time_est_closing,
             time_last_invest,
             is_published,
             is_settled,
             odds_type_id,
             writer_id,
             opportunity_type_id,
             is_disabled,
             scheduled,
             deduct_win_odds,
             --max_exposure,
             is_open_graph,
             quote_params,
             odds_group,
             max_inv_coeff)
        VALUES
            (SEQ_OPPORTUNITIES.NEXTVAL,
             tmp.market_id,
             tmp.a0,
             tmp.a1,
             tmp.a2,
             tmp.a3,
             tmp.b0,
             tmp.b1,
             tmp.odds_type_id,
             tmp.writer_id,
             tmp.opportunity_type_id,
             tmp.b2,
             tmp.scheduled,
             tmp.deduct_win_odds,
            -- tmp.a4,
             tmp.is_open_graph,
             tmp.quote_params,
             get_with_default_value(tmp.odds_group),
             tmp.max_inv_coeff)
        RETURNING id INTO l_id;
    
        for int_tmp in (select id from skin_groups where id != 0) loop
            INSERT INTO OPPORTUNITY_SKIN_GROUP_MAP
                (ID, OPPORTUNITY_ID, SKIN_GROUP_ID, TIME_CREATED, WRITER_ID, SHIFT_PARAMETER, MAX_EXPOSURE, WORST_CASE_RETURN)
            VALUES
                (SEQ_OPPORTUNITY_SKIN_GROUP_MAP.NEXTVAL, l_id, int_tmp.id, SYSDATE, 0, 0, tmp.a4, tmp.worst_case_return);
        end loop;
    
    end loop;
END CREATE_DAILY_OPPORTUNITIES_I;

  PROCEDURE create_daily_opportunities
  (
    p_days_ahead IN NUMBER,
    p_market     IN NUMBER DEFAULT 0
  ) IS  
    e_invalid_date EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_invalid_date, -1878);
  BEGIN
    FOR i IN (SELECT id FROM markets WHERE (p_market = 0 OR id = p_market)) LOOP
      BEGIN
        create_daily_opportunities_i(p_days_ahead, i.id);
      EXCEPTION
        WHEN e_invalid_date THEN
          -- this exception is thrown when (opportunity_templates.time_* || opportunity_templates.time_zone) combination
          --   is not valid due to DST for some time zones
          dbms_output.put_line('Creation of opportunities for market ID ' || i.id || ' failed');
      END;
    END LOOP;
  END create_daily_opportunities;  

PROCEDURE CREATE_WEEK_OPPORTUNITIES (p_flag IN NUMBER DEFAULT 0, p_days IN NUMBER DEFAULT 8, p_market IN NUMBER DEFAULT NULL) IS

    crrOppTmpl opportunity_templates%ROWTYPE;
    crrMarket markets%ROWTYPE;
    crrDayOfWeek INTEGER;
    numOfHolidays INTEGER;
    TYPE holidayCursorType IS REF CURSOR RETURN exchange_holidays%ROWTYPE;
    holiday_ct holidayCursorType;
    holiday exchange_holidays%ROWTYPE;
    halfDayCloseTime opportunity_templates.time_est_closing%TYPE;
    halfDayLastInvest opportunity_templates.time_last_invest%TYPE;
    toInsTimeFirstInvest TIMESTAMP WITH TIME ZONE;
    toInsTimeEstClosing TIMESTAMP WITH TIME ZONE;
    toInsTimeLastInvest TIMESTAMP WITH TIME ZONE;
    wkSun DATE;
    wkMon DATE;
    wkTue DATE;
    wkWed DATE;
    wkThu DATE;
    wkFri DATE;
    wkSat DATE;
    isHoliday INTEGER;
    toIns opportunities%ROWTYPE;
    l_add_days_to_lastinv number;
    l_add_days_to_estclose number;
BEGIN
savepoint before_insert;

    crrDayOfWeek := to_char(current_date, 'D');
    wkSun := trunc(current_date + p_days - crrDayOfWeek);
    wkMon := wkSun + 1;
    wkTue := wkMon + 1;
    wkWed := wkTue + 1;
    wkThu := wkWed + 1;
    wkFri := wkThu + 1;
    wkSat := wkFri + 1;
    FOR item IN (
        SELECT
            A.market_id,
            A.time_first_invest,
            A.time_est_closing,
            A.time_last_invest,
            A.time_zone,
            A.odds_type_id,
            A.writer_id,
            A.opportunity_type_id,
            A.scheduled,
            A.deduct_win_odds,
      A.is_open_graph,
      A.ODDS_GROUP,
            B.exchange_id,
            B.max_exposure,
            A.max_inv_coeff,
            B.Type_Id,
            B.Worst_Case_Return
        FROM
            opportunity_templates A,
            markets B
        WHERE
            A.market_id = B.id AND
            A.is_active = 1 AND
            A.scheduled = 3 AND
            CASE
                 WHEN p_flag = 1 AND A.market_id = p_market THEN 1
                 WHEN p_flag = 0 AND A.market_id = A.market_id THEN 1
                      ELSE 0
            END=1
        )
    loop
    
        l_add_days_to_lastinv := CASE
                           WHEN item.time_first_invest >= item.time_last_invest THEN
                              1
                           ELSE
                              0
                         end;

        l_add_days_to_estclose := CASE
                           WHEN item.time_first_invest >= item.time_est_closing THEN
                              1
                           ELSE
                              0
                         end;
    
        IF ((item.exchange_id = 1) OR  (item.exchange_id = 17) or (item.exchange_id = 18)) THEN
            SELECT
                COUNT(id) INTO numOfHolidays
            FROM
                exchange_holidays
            WHERE
                exchange_id = item.exchange_id AND
                trunc(holiday) >= wkSun AND
                trunc(holiday) <= wkThu AND
                is_half_day = 0;
            IF numOfHolidays < 5 THEN
                FOR i IN 0..4
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkSun + i; -- AND
--                        is_half_day = 0;
                    FETCH holiday_ct INTO holiday;
                    IF holiday_ct%NOTFOUND THEN
                        toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        CLOSE holiday_ct;
                        EXIT;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
                
                FOR i IN REVERSE 0..4
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkSun + i;
                                        FETCH holiday_ct INTO holiday;
                    IF holiday_ct%NOTFOUND THEN
                        IF (item.exchange_id = 1) AND i = 0 THEN
                            SELECT
                                time_est_closing,
                                time_last_invest
                            INTO
                                halfDayCloseTime,
                                halfDayLastInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = item.market_id AND
                                scheduled = 2 AND
                                is_full_day = 2 AND
                                is_active = 1;
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        ELSE
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i + l_add_days_to_estclose, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i + l_add_days_to_lastinv, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            SELECT
                                time_est_closing,
                                time_last_invest
                            INTO
                                halfDayCloseTime,
                                halfDayLastInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = item.market_id AND
                                scheduled = 2 AND
                                is_half_day = 1 AND
                                is_active = 1;
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
            ELSE
                GOTO end_loop;
            END IF;
        END IF;
        
        IF NOT ((item.exchange_id = 1) OR (item.exchange_id = 17) OR (item.exchange_id = 16) OR (item.exchange_id = 18)) THEN
            SELECT
                COUNT(id) INTO numOfHolidays
            FROM
                exchange_holidays
            WHERE
                exchange_id = item.exchange_id AND
                trunc(holiday) >= wkMon AND
                trunc(holiday) <= wkFri AND
                is_half_day = 0;
            IF numOfHolidays < 5 THEN
                FOR i IN 0..4
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkMon + i; -- AND
--                        is_half_day = 0;
                    FETCH holiday_ct INTO holiday;
                    if holiday_ct%NOTFOUND THEN -- if not a holiday
                                        -- if the jakarta week doesn't open on friday it should open by the wekly template close time
                                        -- if its friday open it by daily half day template
                                                IF item.market_id = 370 AND i = 4 THEN
                            -- if the jakarta week doesn't open on friday it should open by the wekly template close time
                                                        -- if its friday open it by daily half day template
                            SELECT
                                to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
                            INTO
                                toInsTimeFirstInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = 370 AND
                                scheduled = 2 AND
                                is_half_day = 1;
                        ELSE
                                                        toInsTimeFirstInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                                                END IF;
                        CLOSE holiday_ct;
                        EXIT;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            toInsTimeFirstInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
                FOR i IN REVERSE 0..4
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkMon + i;
                    FETCH holiday_ct INTO holiday;
                    IF holiday_ct%NOTFOUND THEN -- if not a holiday
                        IF item.market_id = 15 AND i != 4 THEN
                            -- if the ILS week doesn't close on friday it should close by the day template close time
                            -- the week template is done for friday when it close at 13:00
                            SELECT
                                to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_est_closing || time_zone, 'yyyy-mm-dd hh24:mi TZR'),
                                to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
                            INTO
                                toInsTimeEstClosing,
                                toInsTimeLastInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = 15 AND
                                scheduled = 2 AND
                                is_half_day = 0;
                        ELSE
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkMon + i + l_add_days_to_estclose, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkMon + i + l_add_days_to_lastinv, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        END IF;
                        CLOSE holiday_ct;
                        EXIT;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            SELECT
                                time_est_closing,
                                time_last_invest
                            INTO
                                halfDayCloseTime,
                                halfDayLastInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = item.market_id AND
                                scheduled = 2 AND
                                is_half_day = 1 AND
                                is_active = 1;
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
            ELSE
                GOTO end_loop;
            END IF;
        END IF;

        IF (item.exchange_id = 16) THEN
            SELECT
                COUNT(id) INTO numOfHolidays
            FROM
                exchange_holidays
            WHERE
                exchange_id = item.exchange_id AND
                trunc(holiday) >= wkSun AND
                trunc(holiday) <= wkWed AND
                is_half_day = 0;
            IF numOfHolidays < 4 THEN
                FOR i IN 0..3
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkSun + i; -- AND
--                        is_half_day = 0;
                    FETCH holiday_ct INTO holiday;
                    if holiday_ct%NOTFOUND THEN -- if not a holiday
                        toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        CLOSE holiday_ct;
                        EXIT;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
                FOR i IN REVERSE 0..3
                LOOP
                    OPEN holiday_ct FOR
                    SELECT
                        *
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = wkSun + i;
                    FETCH holiday_ct INTO holiday;
                    IF holiday_ct%NOTFOUND THEN -- if not a holiday
                        toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i + l_add_days_to_estclose, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i + l_add_days_to_lastinv, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        CLOSE holiday_ct;
                        EXIT;
                    ELSE
                        IF holiday.is_half_day = 1 THEN
                            SELECT
                                time_est_closing,
                                time_last_invest
                            INTO
                                halfDayCloseTime,
                                halfDayLastInvest
                            FROM
                                opportunity_templates
                            WHERE
                                market_id = item.market_id AND
                                scheduled = 2 AND
                                is_half_day = 1 AND
                                is_active = 1;
                            toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                            CLOSE holiday_ct;
                            EXIT;
                        END IF;
                    END IF;
                    CLOSE holiday_ct;
                END LOOP;
            ELSE
                GOTO end_loop;
            END IF;
        END IF;

        -- check that the last working day of this week is not the same as the last working day of this month
        -- if yes don't create this opp
        IF trunc(toInsTimeEstClosing) = Month_last_working_day(item.exchange_id, 0) THEN
            GOTO end_loop;
        END IF;

        SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM dual;
        toIns.market_id := item.market_id;
        toIns.time_created := current_timestamp;
        toIns.time_first_invest := toInsTimeFirstInvest;
        toIns.time_est_closing := toInsTimeEstClosing;
        toIns.time_last_invest := toInsTimeLastInvest;
        toIns.is_published := 0;
        toIns.is_settled := 0;
        toIns.odds_type_id := item.odds_type_id;
        toIns.writer_id := item.writer_id;
        toIns.opportunity_type_id := item.opportunity_type_id;
        toIns.is_disabled := 0;
        toIns.is_disabled_service := 0;
        toIns.is_disabled_trader := 0;
        toIns.scheduled := item.scheduled;
        toIns.deduct_win_odds := item.deduct_win_odds;
                toIns.is_open_graph := item.is_open_graph;
      --  toIns.max_exposure := item.max_exposure;
        toIns.Odds_Group := get_with_default_value(item.odds_group);
        toIns.max_inv_coeff := item.max_inv_coeff;
        toIns.is_suspended := 0;
        INSERT INTO opportunities VALUES toIns;
        
        for int_tmp in (select id from skin_groups where id != 0) loop
            INSERT INTO OPPORTUNITY_SKIN_GROUP_MAP
                (ID, OPPORTUNITY_ID, SKIN_GROUP_ID, TIME_CREATED, WRITER_ID, SHIFT_PARAMETER, MAX_EXPOSURE, WORST_CASE_RETURN)
            VALUES
                (SEQ_OPPORTUNITY_SKIN_GROUP_MAP.NEXTVAL, toIns.id, int_tmp.id, SYSDATE, 0, 0, item.max_exposure, case when item.type_id=5 then item.Worst_Case_Return end);
        end loop;
--        DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
        <<end_loop>>
        NULL;
    END LOOP;
        exception
    when others then
  dbms_output.put_line(sqlerrm);
  raise;
    rollback to before_insert;
END CREATE_WEEK_OPPORTUNITIES;


PROCEDURE CREATE_MONTH_OPPORTUNITIES (p_flag IN NUMBER DEFAULT 0, p_months IN NUMBER DEFAULT 1, p_market IN NUMBER DEFAULT NULL) IS
    monthFirstDay DATE;
    monthLastDay DATE;
    toInsTimeFirstInvest TIMESTAMP WITH TIME ZONE;
    toInsTimeEstClosing TIMESTAMP WITH TIME ZONE;
    toInsTimeLastInvest TIMESTAMP WITH TIME ZONE;
    halfDayHoliday exchange_holidays.is_half_day%TYPE;
    halfDayCloseTime opportunity_templates.time_est_closing%TYPE;
    halfDayLastInvest opportunity_templates.time_last_invest%TYPE;
    toIns opportunities%ROWTYPE;
BEGIN
savepoint before_insert;
    monthFirstDay := to_date(to_char(add_months(current_date, p_months), 'yyyy-mm') || '-01', 'yyyy-mm-dd');
    monthLastDay := last_day(monthFirstDay);

    FOR item IN (
        SELECT
            A.market_id,
            A.time_first_invest,
            A.time_est_closing,
            A.time_last_invest,
            A.time_zone,
            A.odds_type_id,
            A.writer_id,
            A.opportunity_type_id,
            A.scheduled,
            A.deduct_win_odds,
      A.is_open_graph,
      A.ODDS_GROUP,
            B.exchange_id,
            B.max_exposure,
            A.max_inv_coeff
        FROM
            opportunity_templates A,
            markets B
        WHERE
            A.market_id = B.id AND
            A.is_active = 1 AND
            A.scheduled = 4 AND
            CASE
                 WHEN p_flag = 1 AND A.market_id = p_market THEN 1
                 WHEN p_flag = 0 AND A.market_id = A.market_id THEN 1
                      ELSE 0
            END=1
        )
    LOOP
        -- we asume that there is at least 1 working day in the month

        -- find the first working day of the month and set the time first invest to be on it
        FOR i IN 0..30
        LOOP
            IF ((item.exchange_id = 1 OR  item.exchange_id = 17 OR  item.exchange_id = 18) AND NOT to_char(monthFirstDay + i, 'D') = 6 AND NOT to_char(monthFirstDay + i, 'D') = 7) OR
                (NOT (item.exchange_id = 1 OR  item.exchange_id = 17 OR item.exchange_id = 16 OR item.exchange_id = 18) AND NOT to_char(monthFirstDay + i, 'D') = 7 AND NOT to_char(monthFirstDay + i, 'D') = 1) OR
                ((item.exchange_id = 16) AND NOT to_char(monthFirstDay + i, 'D') = 5 AND NOT to_char(monthFirstDay + i, 'D') = 6 AND NOT to_char(monthFirstDay + i, 'D') = 7) THEN
                BEGIN
                    SELECT
                        is_half_day INTO halfDayHoliday
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = monthFirstDay + i;
                    EXCEPTION WHEN NO_DATA_FOUND THEN
                        halfDayHoliday := NULL;
                END;

                IF halfDayHoliday IS NULL OR halfDayHoliday = 1 THEN
                    -- working day... at least half
                                          IF item.market_id = 370 AND to_char(monthFirstDay + i, 'D') = 6 THEN
                                                -- if the jakarta monthly doesn't open on friday it should open by the monthly template close time
                                                -- if its friday open it by daily half day template
                                                SELECT
                                                        to_timestamp_tz(to_char(monthFirstDay + i, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
                                                INTO
                                                        toInsTimeFirstInvest
                                                FROM
                                                        opportunity_templates
                                                WHERE
                                                        market_id = 370 AND
                                                        scheduled = 2 AND
                                                        is_half_day = 1;
                                        ELSE
                                                toInsTimeFirstInvest := to_timestamp_tz(to_char(monthFirstDay + i, 'yyyy-mm-dd') || ' ' || item.time_first_invest || ' ' || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                                        END IF;
                    EXIT;
                END IF;
            END IF;
        END LOOP;

        -- find the last working day of the month and set the time last invest and time est closing
        FOR i IN 0..30
        LOOP
            IF (item.exchange_id = 1 OR  item.exchange_id = 17 OR  item.exchange_id = 18) AND NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7 THEN
                BEGIN
                    SELECT
                        is_half_day INTO halfDayHoliday
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = monthLastDay - i;
                    EXCEPTION WHEN NO_DATA_FOUND THEN
                        halfDayHoliday := NULL;
                END;

                IF halfDayHoliday IS NULL THEN
                    -- full working day
                    toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                    toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                    EXIT;
                ELSE
                    IF halfDayHoliday = 1 THEN
                        -- working day... at least half
                        SELECT
                            time_est_closing,
                            time_last_invest
                        INTO
                            halfDayCloseTime,
                            halfDayLastInvest
                        FROM
                            opportunity_templates
                        WHERE
                            market_id = item.market_id AND
                            scheduled = 2 AND
                            is_half_day = 1 AND
                            is_active = 1;
                        toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        EXIT;
                    END IF;
                END IF;
            END IF;

            IF NOT (item.exchange_id = 1 OR  item.exchange_id = 17 OR item.exchange_id = 16 OR  item.exchange_id = 18) AND NOT to_char(monthLastDay - i, 'D') = 7 AND NOT to_char(monthLastDay - i, 'D') = 1 THEN
                BEGIN
                    SELECT
                        is_half_day INTO halfDayHoliday
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = monthLastDay - i;
                    EXCEPTION WHEN NO_DATA_FOUND THEN
                        halfDayHoliday := NULL;
                END;

                IF halfDayHoliday IS NULL THEN
                    -- full working day
                    IF item.market_id = 15 AND to_char(monthLastDay - i, 'D') = 6 THEN
                        -- ILS has different working time on friday so if the last working day is friday take its special closing time
                        SELECT
                            to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR'),
                            to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR')
                        INTO
                            toInsTimeEstClosing,
                            toInsTimeLastInvest
                        FROM
                            opportunity_templates
                        WHERE
                            market_id = 15 AND
                            scheduled = 2 AND
                            is_full_day = 0 AND
                            is_half_day = 1 AND
                            is_active = 1;
                    ELSE
                        toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                    END IF;
                    EXIT;
                ELSE
                    IF halfDayHoliday = 1 THEN
                        -- working day... at least half
                        SELECT
                            time_est_closing,
                            time_last_invest
                        INTO
                            halfDayCloseTime,
                            halfDayLastInvest
                        FROM
                            opportunity_templates
                        WHERE
                            market_id = item.market_id AND
                            scheduled = 2 AND
                            is_half_day = 1 AND
                            is_active = 1;
                        toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        EXIT;
                    END IF;
                END IF;
            END IF;

            IF (item.exchange_id = 16) AND NOT to_char(monthLastDay - i, 'D') = 5 AND NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7 THEN
                BEGIN
                    SELECT
                        is_half_day INTO halfDayHoliday
                    FROM
                        exchange_holidays
                    WHERE
                        exchange_id = item.exchange_id AND
                        trunc(holiday) = monthLastDay - i;
                    EXCEPTION WHEN NO_DATA_FOUND THEN
                        halfDayHoliday := NULL;
                END;

                IF halfDayHoliday IS NULL THEN
                    -- full working day
                        toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                    EXIT;
                ELSE
                    IF halfDayHoliday = 1 THEN
                        -- working day... at least half
                        SELECT
                            time_est_closing,
                            time_last_invest
                        INTO
                            halfDayCloseTime,
                            halfDayLastInvest
                        FROM
                            opportunity_templates
                        WHERE
                            market_id = item.market_id AND
                            scheduled = 2 AND
                            is_half_day = 1 AND
                            is_active = 1;
                        toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                        EXIT;
                    END IF;
                END IF;
            END IF;
        END LOOP;

        SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM dual;
        toIns.market_id := item.market_id;
        toIns.time_created := current_timestamp;
        toIns.time_first_invest := toInsTimeFirstInvest;
        toIns.time_est_closing := toInsTimeEstClosing;
        toIns.time_last_invest := toInsTimeLastInvest;
        toIns.is_published := 0;
        toIns.is_settled := 0;
        toIns.odds_type_id := item.odds_type_id;
        toIns.writer_id := item.writer_id;
        toIns.opportunity_type_id := item.opportunity_type_id;
        toIns.is_disabled := 0;
        toIns.is_disabled_service := 0;
        toIns.is_disabled_trader := 0;
        toIns.scheduled := item.scheduled;
        toIns.deduct_win_odds := item.deduct_win_odds;
    toIns.is_open_graph := item.is_open_graph;
    --    toIns.max_exposure := item.max_exposure;
    toIns.Odds_Group := get_with_default_value(item.odds_group);
        toIns.max_inv_coeff := item.max_inv_coeff;
        toIns.is_suspended := 0;
        INSERT INTO opportunities VALUES toIns;
        for int_tmp in (select id from skin_groups where id != 0) loop
            INSERT INTO OPPORTUNITY_SKIN_GROUP_MAP
                (ID, OPPORTUNITY_ID, SKIN_GROUP_ID, TIME_CREATED, WRITER_ID, SHIFT_PARAMETER, MAX_EXPOSURE)
            VALUES
                (SEQ_OPPORTUNITY_SKIN_GROUP_MAP.NEXTVAL, toIns.id, int_tmp.id, SYSDATE, 0, 0, item.max_exposure);
        end loop;
--        DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
    END LOOP;
        exception
    when others then
  dbms_output.put_line(sqlerrm);
  raise;
  rollback to before_insert;
END CREATE_MONTH_OPPORTUNITIES;
END OPPORTUNITIES_MANAGER;
/
