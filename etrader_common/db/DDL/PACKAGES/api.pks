-------- Create package for API --------
create or replace PACKAGE "API" is

	FUNCTION GET_MARKETING_USERS(P_date varchar2, P_affiliate_key NUMBER)
	RETURN SYS_REFCURSOR;
  
  FUNCTION GET_MARKETING_FTDS(P_date varchar2, P_affiliate_key NUMBER)
	RETURN SYS_REFCURSOR;

End API;

-------- Create package BODY for API --------
create or replace PACKAGE BODY "API" is
	FUNCTION GET_MARKETING_USERS(P_date varchar2, P_affiliate_key NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** FUNCTION'S PARAMETERS ***
    F_cursor SYS_REFCURSOR;
    F_user_class_test NUMBER := 0;
    F_affiliate_system_netrefer NUMBER := 1;
  BEGIN
    -- *** DBMS_OUTPUT ***
    DBMS_OUTPUT.PUT_LINE('Function API.GET_MARKETING_USERS started.');
    OPEN F_cursor FOR
    SELECT 
      DISTINCT (u.DYNAMIC_PARAM) as dp,
      count(u.id) OVER (PARTITION BY u.AFFILIATE_KEY) as num_users
    FROM 
      users u
    WHERE 
      u.affiliate_key = P_affiliate_key
      and u.CLASS_ID <> F_user_class_test
      and u.AFFILIATE_SYSTEM = F_affiliate_system_netrefer 
      and u.AFFILIATE_SYSTEM_USER_TIME is not null
      and TRUNC(u.TIME_CREATED) = TO_DATE(P_date, 'YYYY-MM-DD');
    
    DBMS_OUTPUT.PUT_LINE('Function API.GET_MARKETING_USERS end.');
    RETURN F_cursor;
  END GET_MARKETING_USERS;
  
  FUNCTION GET_MARKETING_FTDS(P_date varchar2, P_affiliate_key NUMBER)
    RETURN SYS_REFCURSOR
  AS
    -- *** FUNCTION'S PARAMETERS ***
    F_cursor SYS_REFCURSOR;
    F_user_class_test NUMBER := 0;
    F_affiliate_system_netrefer NUMBER := 1;
    F_trans_class_deposit NUMBER := 1;
    F_trans_status_succeed NUMBER := 2;
    F_trans_status_pending NUMBER := 7;
    F_trans_status_canceled_et NUMBER := 8;
  BEGIN
    -- *** DBMS_OUTPUT ***
    DBMS_OUTPUT.PUT_LINE('Function API.GET_MARKETING_FTDS started.');
    OPEN F_cursor FOR
    SELECT
      count(users_ftd.ftd_id) as num_ftds
    FROM
      (SELECT 
        u.id as user_id,
        min(t.id) as ftd_id
      FROM 
        users u,
        transactions t, 
        transaction_types tt
      WHERE 
        u.id = t.user_id
        and t.type_id = tt.id 
        and u.affiliate_key = P_affiliate_key
        and u.CLASS_ID <> F_user_class_test
        and u.AFFILIATE_SYSTEM = F_affiliate_system_netrefer 
        and u.AFFILIATE_SYSTEM_USER_TIME is not null
        and tt.class_type = F_trans_class_deposit 
        and t.status_id in (F_trans_status_succeed, F_trans_status_pending, F_trans_status_canceled_et)
        and TRUNC(t.TIME_SETTLED) = TO_DATE(P_date, 'YYYY-MM-DD')
      GROUP BY
        u.id) users_ftd;
    
    DBMS_OUTPUT.PUT_LINE('Function API.GET_MARKETING_FTDS end.');
    RETURN F_cursor;
  END GET_MARKETING_FTDS;
END API;