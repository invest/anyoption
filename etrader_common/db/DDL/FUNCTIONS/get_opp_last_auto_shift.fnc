CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:51 (QP5 v5.149.1003.31008) */
FUNCTION "GET_OPP_LAST_AUTO_SHIFT" (p_opp_id IN NUMBER)
   RETURN NUMBER
AS
   last_shift   NUMBER;
   is_up        NUMBER;
BEGIN
   last_shift := 0;

   SELECT shifting, is_up
     INTO last_shift, is_up
     FROM (  SELECT shifting, is_up, SHIFTING_TIME
               FROM opportunity_shiftings
              WHERE     opportunity_id = p_opp_id
                    AND writer_id = 0
                    AND TRUNC (shifting_time) = TRUNC (CURRENT_DATE)
           ORDER BY SHIFTING_TIME DESC)
    WHERE ROWNUM <= 1;

   IF is_up = 0
   THEN
      RETURN (-last_shift);
   END IF;

   RETURN (last_shift);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END;
/
