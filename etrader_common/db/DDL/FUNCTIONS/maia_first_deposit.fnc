CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:58 (QP5 v5.149.1003.31008) */
FUNCTION "MAIA_FIRST_DEPOSIT" (m_user_id IN NUMBER, m_order IN NUMBER)
   RETURN NUMBER
AS
   trans_id   NUMBER;
BEGIN
   SELECT MIN (t.id)
     INTO trans_id
     FROM transactions t, transaction_types tt
    WHERE     t.type_id = tt.id
          AND t.user_id = m_user_id
          AND tt.class_type = 1
          AND t.status_id IN (2, 7, 8);

   --select id into trans_id
   --  from (
   --  select
   --  a.*, ROWNUM rnum
   --      from (select t.id
   --    from users u , transactions t , transaction_types tt
   --    where t.user_id = u.id
   --    and t.type_id = tt.id
   --    and tt.class_type = 1
   --    and t.status_id IN (2,7, 8)
   --    and u.CLASS_ID <> 0
   --    and t.TYPE_ID NOT IN (4, 6, 12)
   --  and u.id =  m_user_id
   --    order by t.TIME_CREATED) a
   --    where ROWNUM <= m_order)
   --where rnum  >= m_order;


   RETURN (trans_id);
END MAIA_FIRST_DEPOSIT;
/
