CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:47 (QP5 v5.149.1003.31008) */
FUNCTION "GET_CLEARING_ROUTE" (p_skin_id       IN NUMBER,
                               p_bin           IN VARCHAR2,
                               p_currency_id   IN NUMBER,
                               p_is_deposit    IN NUMBER,
                               p_country_id    IN NUMBER)
   RETURN NUMBER
AS
   res   NUMBER;
BEGIN
   BEGIN
      SELECT clearing_provider_id
        INTO res
        FROM clearing_bin_routes
       WHERE     skin_id = p_skin_id
             AND start_bin <= p_bin
             AND end_bin >= p_bin
             AND currency_id = p_currency_id
             AND is_deposit = p_is_deposit
             AND ROWNUM <= 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         res := NULL;
   END;

   IF res IS NULL
   THEN
      BEGIN
         SELECT clearing_provider_id
           INTO res
           FROM clearing_country_routes
          WHERE     skin_id = p_skin_id
                AND country_id = p_country_id
                AND currency_id = p_currency_id
                AND is_deposit = p_is_deposit
                AND ROWNUM <= 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            res := NULL;
      END;
   END IF;

   IF res IS NULL
   THEN
      BEGIN
         SELECT clearing_provider_id
           INTO res
           FROM clearing_currency_routes
          WHERE     skin_id = p_skin_id
                AND currency_id = p_currency_id
                AND is_deposit = p_is_deposit
                AND ROWNUM <= 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            res := NULL;
      END;
   END IF;

   RETURN (res);
END;
/
