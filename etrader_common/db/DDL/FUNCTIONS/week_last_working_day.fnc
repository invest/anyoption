CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:31:00 (QP5 v5.149.1003.31008) */
FUNCTION "WEEK_LAST_WORKING_DAY" (p_exchange_id   IN NUMBER,
                                  p_add_weeks     IN NUMBER)
   RETURN DATE
AS
   numOfHolidays      INTEGER;

   TYPE holidayCursorType IS REF CURSOR
      RETURN exchange_holidays%ROWTYPE;

   holiday_ct         holidayCursorType;
   holiday            exchange_holidays%ROWTYPE;
   wkSun              DATE;
   wkMon              DATE;
   last_working_day   DATE;
BEGIN
   wkSun :=
      TRUNC (
         CURRENT_DATE + (p_add_weeks * 7) + 1 - TO_CHAR (CURRENT_DATE, 'D'));
   wkMon := wkSun + 1;
   last_working_day := NULL;

   IF ( (p_exchange_id = 1) OR (p_exchange_id = 17) OR (p_exchange_id = 18))
   THEN
      SELECT COUNT (id)
        INTO numOfHolidays
        FROM exchange_holidays
       WHERE     exchange_id = p_exchange_id
             AND TRUNC (holiday) >= wkSun
             AND TRUNC (holiday) <= wkSun + 4
             AND is_half_day = 0;

      IF numOfHolidays < 5
      THEN
         FOR i IN REVERSE 0 .. 4
         LOOP
            OPEN holiday_ct FOR
               SELECT *
                 FROM exchange_holidays
                WHERE exchange_id = p_exchange_id
                      AND TRUNC (holiday) = wkSun + i;                  -- AND

            --     is_half_day = 0;
            FETCH holiday_ct INTO holiday;

            IF holiday_ct%NOTFOUND
            THEN
               last_working_day := wkSun + i;

               CLOSE holiday_ct;

               EXIT;
            ELSE
               IF holiday.is_half_day = 1
               THEN                             -- working day (at least half)
                  last_working_day := wkSun + i;

                  CLOSE holiday_ct;

                  EXIT;
               END IF;
            END IF;

            CLOSE holiday_ct;
         END LOOP;
      END IF;
   END IF;

   IF NOT (   (p_exchange_id = 1)
           OR (p_exchange_id = 17)
           OR (p_exchange_id = 16)
           OR (p_exchange_id = 18))
   THEN
      SELECT COUNT (id)
        INTO numOfHolidays
        FROM exchange_holidays
       WHERE     exchange_id = p_exchange_id
             AND TRUNC (holiday) >= wkMon
             AND TRUNC (holiday) <= wkMon + 4
             AND is_half_day = 0;

      IF numOfHolidays < 5
      THEN
         FOR i IN REVERSE 0 .. 4
         LOOP
            OPEN holiday_ct FOR
               SELECT *
                 FROM exchange_holidays
                WHERE exchange_id = p_exchange_id
                      AND TRUNC (holiday) = wkMon + i;                  -- AND

            --     is_half_day = 0;
            FETCH holiday_ct INTO holiday;

            IF holiday_ct%NOTFOUND
            THEN                                           -- if not a holiday
               last_working_day := wkMon + i;

               CLOSE holiday_ct;

               EXIT;
            ELSE
               IF holiday.is_half_day = 1
               THEN                             -- working day (at least half)
                  last_working_day := wkMon + i;

                  CLOSE holiday_ct;

                  EXIT;
               END IF;
            END IF;

            CLOSE holiday_ct;
         END LOOP;
      END IF;
   END IF;

   IF (p_exchange_id = 16)
   THEN
      SELECT COUNT (id)
        INTO numOfHolidays
        FROM exchange_holidays
       WHERE     exchange_id = p_exchange_id
             AND TRUNC (holiday) >= wkSun
             AND TRUNC (holiday) <= wkSun + 3
             AND is_half_day = 0;

      IF numOfHolidays < 4
      THEN
         FOR i IN REVERSE 0 .. 3
         LOOP
            OPEN holiday_ct FOR
               SELECT *
                 FROM exchange_holidays
                WHERE exchange_id = p_exchange_id
                      AND TRUNC (holiday) = wkSun + i;                  -- AND

            --     is_half_day = 0;
            FETCH holiday_ct INTO holiday;

            IF holiday_ct%NOTFOUND
            THEN
               last_working_day := wkSun + i;

               CLOSE holiday_ct;

               EXIT;
            ELSE
               IF holiday.is_half_day = 1
               THEN                             -- working day (at least half)
                  last_working_day := wkSun + i;

                  CLOSE holiday_ct;

                  EXIT;
               END IF;
            END IF;

            CLOSE holiday_ct;
         END LOOP;
      END IF;
   END IF;

   RETURN (last_working_day);
END;
/
