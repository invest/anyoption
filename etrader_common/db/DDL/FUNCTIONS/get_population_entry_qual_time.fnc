CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:53 (QP5 v5.149.1003.31008) */
FUNCTION "GET_POPULATION_ENTRY_QUAL_TIME" (p_population_entry_id IN NUMBER)
   RETURN DATE
AS
   v_qualification_time   DATE;
BEGIN
   SELECT MAX (time_created)
     INTO v_qualification_time
     FROM population_entries_hist
    WHERE population_entry_id = p_population_entry_id AND status_id IN (1, 5);

   RETURN (v_qualification_time);
END;
/
