CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:48 (QP5 v5.149.1003.31008) */
FUNCTION "GET_IS_DORMANT_ABOVE_BALANCE" (p_user_id              IN NUMBER,
                                         p_time_threshold          NUMBER,
                                         p_balance_threshold       NUMBER,
                                         p_users_curr_balance      NUMBER)
   RETURN NUMBER
AS
   /* ------------------------------------------------------------------------------
       This function checks if the user was above min balance
       during all thershold time and didn't have any balance increasment
       -------------------------------------------------------------------------------*/

   last_balance   NUMBER := p_users_curr_balance;
BEGIN
   -- Go through all users balance history records from latest to oldest
   FOR v_balance_his IN (  SELECT *
                             FROM balance_history bh
                            WHERE bh.user_id = p_user_id
                         ORDER BY bh.time_created DESC)
   LOOP
      -- If  balance_history's balance is under min balance return false
      IF (v_balance_his.balance < p_balance_threshold)
      THEN
         RETURN 0;
      ELSE
         -- If curr balance is outside of threshold time return true;
         IF (v_balance_his.time_created < SYSDATE - p_time_threshold)
         THEN
            RETURN 1;
         ELSE
            -- If curr balance_history's balance is under last balance_history's balance return false;
            IF (v_balance_his.balance < last_balance)
            THEN
               RETURN 0;
            END IF;          -- IF (v_balance_his.balance < last_balance) THEN
         END IF;   -- IF (last_balance_date < sysdate - p_time_threshold) THEN
      END IF;         -- IF (v_balance_his.balance < p_balance_threshold) THEN

      -- Keep last balance history data;
      last_balance := v_balance_his.balance;
   END LOOP;

   RETURN 0;
END GET_IS_DORMANT_ABOVE_BALANCE;
/
