CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:59 (QP5 v5.149.1003.31008) */
FUNCTION "TRANSACTION_BY_ORDER" (m_user_id   IN NUMBER,
                                 m_order     IN NUMBER,
                                 m_status    IN NUMBER,
                                 m_t_class   IN NUMBER)
   RETURN NUMBER
AS
   trans_id   NUMBER;
BEGIN
   SELECT id
     INTO trans_id
     FROM (SELECT a.*, ROWNUM rnum
             FROM (  SELECT t.id
                       FROM users u, transactions t, transaction_types tt
                      WHERE     t.user_id = u.id
                            AND t.type_id = tt.id
                            AND tt.class_type = m_t_class
                            AND t.status_id = m_status
                            AND u.CLASS_ID <> 0
                            AND t.TYPE_ID <> 6
                            AND u.id = m_user_id
                   ORDER BY t.TIME_CREATED) a
            WHERE ROWNUM <= m_order)
    WHERE rnum >= m_order;


   RETURN (trans_id);
END;
/
