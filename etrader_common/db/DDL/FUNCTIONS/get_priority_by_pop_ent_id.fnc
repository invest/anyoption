CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:53 (QP5 v5.149.1003.31008) */
FUNCTION "GET_PRIORITY_BY_POP_ENT_ID" (popEntId IN NUMBER)
   RETURN NUMBER
AS
   priority   NUMBER;
BEGIN
   SELECT pt.priority
     INTO priority
     FROM POPULATIONS p, POPULATION_ENTRIES pe, population_types pt
    WHERE     pe.id = popEntId
          AND p.id = pe.POPULATION_ID
          AND pt.id = p.population_type_id;

   RETURN priority;
END GET_PRIORITY_BY_POP_ENT_ID;
/
