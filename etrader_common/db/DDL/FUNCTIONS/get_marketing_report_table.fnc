CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:50 (QP5 v5.149.1003.31008) */
FUNCTION "GET_MARKETING_REPORT_TABLE" (p_start_date   IN DATE,
                                       p_end_date     IN DATE)
   RETURN MARKETING_REPORT_DATA_LIST
   PIPELINED
AS
   v_data                    MARKETING_REPORT_DATA;

   frst_dep_amount_avg       NUMBER;
   reg_frst_dep_amount_avg   NUMBER;
   registered_users_other    NUMBER;
   frst_dep_users_other      NUMBER;
   frst_dep_amount_other     NUMBER;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('GET_MARKETING_REPORT_TABLE started at ' || SYSDATE);

   FOR v_record
      IN (  SELECT DISTINCT
                   mcom.id AS comb_id,
                   mca.name campaign_name,
                   w.user_name writer,
                   mmed.name medium,
                   mcon.name content,
                   ms.size_horizontal,
                   ms.size_vertical,
                   mt.name market_type,
                   mloc.location,
                   mcom.skin_id,
                   s.name skin_name,
                   mlp.name landing_page_name,
                   msource.name AS source_name,
                   clk.clicks_num,
                   contacts_no_users.short_reg_contacts_only,            /*7*/
                   reg_users_after.short_reg_users,                      /*8*/
                   frst_dep_after.frst_dep_users_after_short,            /*9*/
                   frst_dep_after.frst_dep_amount_after_short / 100
                      frst_dep_amount_after_short,                      /*10*/
                   contacts_no_users.call_me_contacts_only,             /*11*/
                   reg_users_after.call_me_users,                       /*12*/
                   frst_dep_after.frst_dep_users_after_call_me,         /*13*/
                   frst_dep_after.frst_dep_amount_after_call_me / 100
                      frst_dep_amount_after_call_me,                    /*14*/
                   reg_users.registered_users,
                   frst_dep.frst_dep_users,
                   frst_dep.frst_dep_sales_users,
                   frst_dep.frst_dep_ind_users,
                   frst_dep.reg_frst_dep_users,
                   frst_dep.reg_frst_dep_sales_users,
                   frst_dep.reg_frst_dep_ind_users,
                   frst_dep.frst_dep_amount / 100 frst_dep_amount,
                   frst_dep.frst_dep_sales_amount / 100 frst_dep_sales_amount,
                   frst_dep.frst_dep_ind_amount / 100 frst_dep_ind_amount,
                   frst_dep.reg_frst_dep_amount / 100 reg_frst_dep_amount,
                   frst_dep.reg_frst_dep_sales_amount / 100
                      reg_frst_dep_sales_amount,
                   frst_dep.reg_frst_dep_ind_amount / 100
                      reg_frst_dep_ind_amount,
                   sums_transactions.dep_count,
                   sums_transactions.dep_sum / 100 dep_sum
              --                          sums_old_transactions.dep_old_sum/100 dep_sum_out_of_range,
              --                          sums_investments.house_win/100 house_win,
              --                          sums_investments.house_win_reg/100 house_win_reg,
              --                          fail_min.fail_min_users,
              --                          fail.fail_users


              FROM skins s,
                   writers w,
                   marketing_campaigns mca,
                   marketing_mediums mmed,
                   marketing_contents mcon,
                   marketing_sources msource,
                                                    marketing_combinations mcom
                                                 LEFT JOIN
                                                    marketing_landing_pages mlp
                                                 ON mcom.landing_page_id =
                                                       mlp.id
                                              LEFT JOIN
                                                 marketing_sizes ms
                                              ON mcom.size_id = ms.id
                                           LEFT JOIN
                                              marketing_locations mloc
                                           ON mcom.location_id = mloc.id
                                        LEFT JOIN
                                           marketing_types mt
                                        ON mcom.type_id = mt.id
                                     LEFT JOIN
                                        (  SELECT cl.marketing_combination_id,
                                                  SUM (cl.clicks_num)
                                                     AS clicks_num
                                             FROM clicks_comb_day cl
                                            WHERE cl.day BETWEEN p_start_date
                                                             AND p_end_date
                                         GROUP BY cl.marketing_combination_id) clk
                                     ON mcom.id = clk.marketing_combination_id
                                  -------------------------------------------------------------------------
                                  LEFT JOIN /*************************  7 + 11 in doc ***************************/
                                     (  SELECT c.combination_id,
                                               -- count(*) as short_reg_contacts_only,
                                               SUM (
                                                  CASE
                                                     WHEN c.TYPE = 5 THEN 1
                                                     ELSE 0
                                                  END)          /* 7 in doc */
                                                  AS short_reg_contacts_only,
                                               SUM (
                                                  CASE
                                                     WHEN c.TYPE = 1 THEN 1
                                                     ELSE 0
                                                  END)         /* 11 in doc */
                                                  AS call_me_contacts_only
                                          FROM contacts c
                                         WHERE c.user_id = 0
                                               AND c.time_created BETWEEN p_start_date
                                                                      AND p_end_date
                                      GROUP BY c.combination_id) contacts_no_users
                                  ON mcom.id = contacts_no_users.combination_id
                               -------------------------------------------------------------------------
                               LEFT JOIN /***********************   9  + 10  + 13 + 14 ***********************/
                                  (  SELECT u.combination_id,
                                            SUM (
                                               CASE
                                                  WHEN c.TYPE = 5 THEN 1
                                                  ELSE 0
                                               END)
                                               AS frst_dep_users_after_short, /******* 9 *******/
                                            SUM (
                                               CASE
                                                  WHEN c.TYPE = 1 THEN 1
                                                  ELSE 0
                                               END)
                                               AS frst_dep_users_after_call_me, /******* 13 *******/
                                            SUM (
                                               CASE
                                                  WHEN c.TYPE = 5
                                                  THEN
                                                     CASE
                                                        WHEN u.skin_id = 1
                                                        THEN
                                                           t.amount
                                                        ELSE
                                                           t.amount * t.rate
                                                     END
                                                  ELSE
                                                     0
                                               END)
                                               AS frst_dep_amount_after_short, /******* 10 *******/
                                            SUM (
                                               CASE
                                                  WHEN c.TYPE = 1
                                                  THEN
                                                     CASE
                                                        WHEN u.skin_id = 1
                                                        THEN
                                                           t.amount
                                                        ELSE
                                                           t.amount * t.rate
                                                     END
                                                  ELSE
                                                     0
                                               END)
                                               AS frst_dep_amount_after_call_me /******* 14 *******/
                                       FROM    users u
                                            LEFT JOIN
                                               transactions first_dep
                                            ON u.first_deposit_id = first_dep.id,
                                            transactions t,
                                            transaction_types tt,
                                            contacts c
                                      WHERE     c.user_id = u.id
                                            AND c.id = (SELECT MIN (c.id)
                                                          FROM contacts c
                                                         WHERE c.user_id = u.id)
                                            AND c.time_created < u.time_created
                                            AND u.class_id <> 0
                                            AND t.id = u.first_deposit_id
                                            AND t.time_created BETWEEN p_start_date
                                                                   AND p_end_date
                                            AND u.id = t.user_id
                                            AND t.type_id = tt.id
                                            AND tt.class_type = 1
                                            AND t.status_id IN (2, 7, 8)
                                   GROUP BY u.combination_id) frst_dep_after
                               ON mcom.id = frst_dep_after.combination_id
                            -------------------------------------------------------------------------
                            LEFT JOIN /********************** 8 + 12 in doc ************************** */
                               (  SELECT u.combination_id,
                                         --count(*) as registered_users ,
                                         SUM (
                                            CASE
                                               WHEN c.TYPE = 5 THEN 1
                                               ELSE 0
                                            END)
                                            AS short_reg_users,   /*8 in doc*/
                                         SUM (
                                            CASE
                                               WHEN c.TYPE = 1 THEN 1
                                               ELSE 0
                                            END)
                                            AS call_me_users     /*12 in doc*/
                                    FROM users u, contacts c
                                   WHERE     c.time_created < u.time_created
                                         AND c.id = (SELECT MIN (c.id)
                                                       FROM contacts c
                                                      WHERE c.user_id = u.id)
                                         AND u.class_id <> 0
                                         AND u.id = c.user_id
                                         AND u.time_created BETWEEN p_start_date
                                                                AND p_end_date
                                GROUP BY u.combination_id) reg_users_after
                            ON mcom.id = reg_users_after.combination_id
                         -------------------------------------------------------------------------
                         LEFT JOIN
                            (  SELECT u.combination_id,
                                      COUNT (*) AS registered_users
                                 FROM users u
                                WHERE u.class_id <> 0
                                      AND u.time_created BETWEEN p_start_date
                                                             AND p_end_date
                             -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
                             -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                             GROUP BY u.combination_id) reg_users
                         ON mcom.id = reg_users.combination_id
                      -------------------------------------------------------------------------
                      LEFT JOIN
                         (  SELECT u.combination_id,
                                   COUNT (*) AS frst_dep_users,
                                   SUM (
                                      CASE
                                         WHEN ti.transaction_id IS NOT NULL
                                         THEN
                                            1
                                         ELSE
                                            0
                                      END)
                                      AS frst_dep_sales_users,
                                   SUM (
                                      CASE
                                         WHEN ti.transaction_id IS NULL THEN 1
                                         ELSE 0
                                      END)
                                      AS frst_dep_ind_users,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                         THEN
                                            1
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_users,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                              AND ti.transaction_id IS NOT NULL
                                         THEN
                                            1
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_sales_users,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                              AND ti.transaction_id IS NULL
                                         THEN
                                            1
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_ind_users,
                                   SUM (
                                      CASE
                                         WHEN u.skin_id = 1 THEN t.amount
                                         ELSE t.amount * t.rate
                                      END)
                                      AS frst_dep_amount,
                                   SUM (
                                      CASE
                                         WHEN ti.transaction_id IS NOT NULL
                                         THEN
                                            CASE
                                               WHEN u.skin_id = 1 THEN t.amount
                                               ELSE t.amount * t.rate
                                            END
                                         ELSE
                                            0
                                      END)
                                      AS frst_dep_sales_amount,
                                   SUM (
                                      CASE
                                         WHEN ti.transaction_id IS NULL
                                         THEN
                                            CASE
                                               WHEN u.skin_id = 1 THEN t.amount
                                               ELSE t.amount * t.rate
                                            END
                                         ELSE
                                            0
                                      END)
                                      AS frst_dep_ind_amount,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                         THEN
                                            CASE
                                               WHEN u.skin_id = 1 THEN t.amount
                                               ELSE t.amount * t.rate
                                            END
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_amount,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                              AND ti.transaction_id IS NOT NULL
                                         THEN
                                            CASE
                                               WHEN u.skin_id = 1 THEN t.amount
                                               ELSE t.amount * t.rate
                                            END
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_sales_amount,
                                   SUM (
                                      CASE
                                         WHEN u.time_created BETWEEN p_start_date
                                                                 AND p_end_date
                                              AND ti.transaction_id IS NULL
                                         THEN
                                            CASE
                                               WHEN u.skin_id = 1 THEN t.amount
                                               ELSE t.amount * t.rate
                                            END
                                         ELSE
                                            0
                                      END)
                                      AS reg_frst_dep_ind_amount
                              FROM users u,
                                      transactions t
                                   LEFT JOIN
                                      transactions_issues ti
                                   ON t.id = ti.transaction_id
                             WHERE u.class_id <> 0 AND t.id = u.first_deposit_id
                                   AND t.time_created BETWEEN p_start_date
                                                          AND p_end_date
                          -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
                          -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                          GROUP BY u.combination_id) frst_dep
                      ON mcom.id = frst_dep.combination_id
                   -------------------------------------------------------------------------
                   LEFT JOIN
                      (  SELECT u.combination_id,
                                COUNT (*) dep_count,
                                SUM (
                                   CASE
                                      WHEN u.skin_id = 1 THEN t.amount
                                      ELSE t.amount * t.rate
                                   END)
                                   AS dep_sum
                           FROM users u, transactions t, transaction_types tt
                          WHERE     u.id = t.user_id
                                AND t.type_id = tt.id
                                AND u.class_id <> 0
                                -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
                                -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                AND tt.class_type = 1
                                AND t.status_id IN (2, 7, 8)
                                AND t.time_created BETWEEN p_start_date
                                                       AND p_end_date
                       GROUP BY u.combination_id) sums_transactions
                   ON mcom.id = sums_transactions.combination_id
             -------------------------------------------------------------------------
             --                        LEFT JOIN
             --                        (SELECT
             --                             u.combination_id,
             --                             SUM(CASE WHEN u.skin_id = 1 THEN t.amount ELSE t.amount*t.rate END) as dep_old_sum
             --                         FROM
             --                             users u
             --                                left join transactions first_dep on u.first_deposit_id = first_dep.id,
             --                             transactions t,
             --                             transaction_types tt
             --                         WHERE
             --                             u.id = t.user_id
             --                             AND t.type_id = tt.id
             --                             AND u.class_id <> 0
             --                             -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
             --                             -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
             --                             AND tt.class_type = 1
             --                             AND t.status_id IN (2, 7, 8)
             --                             AND t.time_created between p_start_date and p_end_date
             --                             AND not first_dep.time_created between p_start_date and p_end_date
             --                          GROUP BY
             --                             u.combination_id) sums_old_transactions on mcom.id = sums_old_transactions.combination_id
             -------------------------------------------------------------------------
             --                        LEFT JOIN
             --                        (SELECT
             --                             u.combination_id,
             --                             SUM(CASE WHEN u.skin_id = 1 THEN i.amount ELSE i.amount*i.rate END)
             --                                 as house_win,
             --                             SUM(CASE WHEN u.time_created between p_start_date and p_end_date THEN
             --                                  CASE WHEN u.skin_id = 1 THEN i.amount ELSE i.amount*i.rate END
             --                                 ELSE 0 END) as house_win_reg
             --                         FROM
             --                             users u,
             --                             investments i,
             --                             transactions t
             --                         WHERE
             --                             u.id = i.user_id
             --                             AND u.class_id <> 0
             --                             -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
             --                             -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
             --                             AND i.is_settled = 1
             --                             AND i.is_canceled = 0
             --                             AND u.first_deposit_id = t.id
             --
             --                             AND t.time_created between p_start_date and p_end_date
             --                             AND i.time_settled between p_start_date and p_end_date
             --                         GROUP BY
             --                             u.combination_id) sums_investments on mcom.id = sums_investments.combination_id
             -------------------------------------------------------------------------                                                  GROUP BY u.combination_id) sums_investments on mcom.id = sums_investments.combination_id
             --                        LEFT JOIN
             --                        (SELECT
             --                             u.combination_id,
             --                             count(DISTINCT(u.id))  as fail_min_users
             --                         FROM
             --                             users u,
             --                             transactions t,
             --                             transaction_types tt
             --                         WHERE
             --                             u.class_id <> 0
             --                             AND u.id = t.user_id
             --                             AND t.type_id = tt.id
             --                             AND tt.class_type = 1
             --                             -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
             --                             -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
             --                             AND u.time_created between p_start_date and p_end_date
             --                             AND not exists (SELECT t2.id
             --                                             FROM transactions t2, transaction_types tt2
             --                                             WHERE u.id = t2.user_id
             --                                               AND t2.time_created > p_start_date
             --                                               AND t2.type_id = tt2.id
             --                                               AND tt2.class_type = 1
             --                                               AND (t2.status_id IN (2, 7, 8)
             --                                                    OR
             --                                                   ((t2.status_id = 3
             --                                                    AND  t2.description NOT LIKE '%min%')
             --                                                         OR
             --                                                         t2.description is NULL)))
             --                         GROUP BY u.combination_id) fail_min on mcom.id = fail_min.combination_id
             -------------------------------------------------------------------------
             --                        LEFT JOIN
             --                        (SELECT
             --                             u.combination_id,
             --                             count(DISTINCT(t.user_id)) as fail_users
             --                         FROM
             --                             users u,
             --                             transactions t,
             --                             transaction_types tt
             --                         WHERE
             --                             u.id = t.user_id
             --                             AND t.type_id = tt.id
             --                             AND tt.class_type = 1
             --                             AND t.status_id = 3
             --                             AND (t.description NOT LIKE '%min%' OR t.description is NULL)
             --                             AND u.class_id <> 0
             --                             -- AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
             --                             -- AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
             --                             AND u.time_created between p_start_date and p_end_date
             --                             AND not exists (SELECT t.id
             --                                             FROM transactions t2, transaction_types tt2
             --                                             WHERE u.id = t2.user_id
             --                                               AND t2.time_created > p_start_date
             --                                               AND t2.type_id = tt2.id
             --                                               AND tt2.class_type = 1
             --                                               AND t2.status_id IN (2, 7, 8))
             --                         GROUP BY u.combination_id) fail on mcom.id = fail.combination_id
             WHERE     mcom.campaign_id = mca.id
                   AND mcom.medium_id = mmed.id
                   AND mcom.content_id = mcon.id
                   -- AND (p_skin_id  = 0 or mcom.skin_id = p_skin_id)
                   -- AND (p_business_skin_id = 0 or mcom.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                   AND msource.id = mca.source_id
                   AND mcom.skin_id = s.id
                   AND mca.campaign_manager = w.id
          ORDER BY mcom.skin_id, mcom.id)
   LOOP
      IF (v_record.frst_dep_users != 0)
      THEN
         frst_dep_amount_avg :=
            v_record.frst_dep_amount / v_record.frst_dep_users;
      ELSE
         frst_dep_amount_avg := 0;
      END IF;

      IF (v_record.frst_dep_users != 0)
      THEN
         frst_dep_amount_avg :=
            v_record.frst_dep_amount / v_record.frst_dep_users;
      ELSE
         frst_dep_amount_avg := 0;
      END IF;

      IF (v_record.reg_frst_dep_users != 0)
      THEN
         reg_frst_dep_amount_avg :=
            v_record.reg_frst_dep_amount / v_record.reg_frst_dep_users;
      ELSE
         reg_frst_dep_amount_avg := 0;
      END IF;

      IF (v_record.registered_users != 0)
      THEN
         registered_users_other :=
            v_record.registered_users
            - (v_record.short_reg_users + v_record.call_me_users);
      ELSE
         registered_users_other := 0;
      END IF;

      IF (v_record.frst_dep_users != 0)
      THEN
         frst_dep_users_other :=
            v_record.frst_dep_users
            - (v_record.frst_dep_users_after_short
               + v_record.frst_dep_users_after_call_me);
      ELSE
         frst_dep_users_other := 0;
      END IF;

      IF (v_record.frst_dep_amount != 0)
      THEN
         frst_dep_amount_other :=
            v_record.frst_dep_amount
            - (v_record.frst_dep_amount_after_short
               + v_record.frst_dep_amount_after_call_me);
      ELSE
         frst_dep_amount_other := 0;
      END IF;

      v_data :=
         MARKETING_REPORT_DATA (v_record.comb_id,
                                v_record.campaign_name,
                                v_record.writer,
                                v_record.medium,
                                v_record.content,
                                v_record.size_horizontal,
                                v_record.size_vertical,
                                v_record.market_type,
                                v_record.location,
                                v_record.skin_id,
                                v_record.skin_name,
                                v_record.landing_page_name,
                                v_record.source_name,
                                v_record.clicks_num,
                                v_record.short_reg_contacts_only,        /*7*/
                                v_record.short_reg_users,                /*8*/
                                v_record.frst_dep_users_after_short,     /*9*/
                                v_record.frst_dep_amount_after_short,   /*10*/
                                v_record.call_me_contacts_only,         /*11*/
                                v_record.call_me_users,                 /*12*/
                                v_record.frst_dep_users_after_call_me,  /*13*/
                                v_record.frst_dep_amount_after_call_me, /*14*/
                                --                                    v_record.registered_users_other,  /*15*/
                                --                                   v_record.frst_dep_users_other,  /*16*/
                                --                                    v_record.frst_dep_amount_other, /*17*/

                                v_record.registered_users,
                                v_record.frst_dep_users,
                                v_record.frst_dep_sales_users,
                                v_record.frst_dep_ind_users,
                                v_record.reg_frst_dep_users,
                                v_record.reg_frst_dep_sales_users,
                                v_record.reg_frst_dep_ind_users,
                                v_record.frst_dep_amount,
                                v_record.frst_dep_sales_amount,
                                v_record.frst_dep_ind_amount,
                                v_record.reg_frst_dep_amount,
                                v_record.reg_frst_dep_sales_amount,
                                v_record.reg_frst_dep_ind_amount,
                                v_record.dep_count,
                                v_record.dep_sum--                                   v_record.dep_sum_out_of_range,
                                                --                                   v_record.house_win,
                                                --                                   v_record.house_win_reg,
                                                --                                   v_record.fail_min_users,
                                                --                                   v_record.fail_users
                                );
      PIPE ROW (v_data);
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('GET_MARKETING_REPORT_TABLE ended at ' || SYSDATE);

   RETURN;
END GET_MARKETING_REPORT_TABLE;
/
