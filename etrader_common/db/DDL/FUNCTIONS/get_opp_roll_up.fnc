CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:52 (QP5 v5.149.1003.31008) */
FUNCTION "GET_OPP_ROLL_UP" (OPP_ID IN NUMBER)
   RETURN NUMBER
AS
   next_opp_id   NUMBER;
BEGIN
   SELECT *
     INTO next_opp_id
     FROM (  SELECT op1.id
               FROM opportunities op1,
                    (SELECT op.*
                       FROM opportunities op
                      WHERE op.id = OPP_ID AND op.is_settled = 0) A
              WHERE op1.market_id = A.market_id AND op1.is_settled = 0
                    AND SYS_EXTRACT_UTC (op1.time_est_closing) - 2 / 24 <
                           SYS_EXTRACT_UTC (A.time_est_closing)
                    AND SYS_EXTRACT_UTC (op1.time_est_closing) >
                           SYS_EXTRACT_UTC (A.time_est_closing)
                    AND op1.scheduled IN (1, 2)
           ORDER BY op1.time_est_closing)
    WHERE ROWNUM = 1;

   RETURN (next_opp_id);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END GET_OPP_ROLL_UP;
/
