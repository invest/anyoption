CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:58 (QP5 v5.149.1003.31008) */
FUNCTION "MAIA_SUCCESS_DEPOSIT_BY_ORDER" (m_user_id   IN NUMBER,
                                          m_order     IN NUMBER)
   RETURN NUMBER
AS
   trans_id   NUMBER;
BEGIN
   SELECT id
     INTO trans_id
     FROM (SELECT a.*, ROWNUM rnum
             FROM (  SELECT t.id
                       FROM users u, transactions t, transaction_types tt
                      WHERE     t.user_id = u.id
                            AND t.type_id = tt.id
                            AND tt.class_type = 1
                            AND t.status_id = 2
                            AND u.CLASS_ID <> 0
                            AND u.id = m_user_id
                   ORDER BY t.TIME_CREATED) a
            WHERE ROWNUM <= m_order)
    WHERE rnum >= m_order;


   RETURN (trans_id);
END MAIA_SUCCESS_DEPOSIT_BY_ORDER;
/
