CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:46 (QP5 v5.149.1003.31008) */
FUNCTION "GET_ACTION_ENTRY_TYPE" (p_action_id     IN NUMBER,
                                  p_action_time   IN DATE)
   RETURN NUMBER
AS
   entryTypeId    NUMBER := 0;
   popUserId      NUMBER;
   maxHistoryId   NUMBER;
BEGIN
   -- Find action pop user id
   BEGIN
      SELECT pe.population_users_id
        INTO popUserId
        FROM issue_actions ia, issues i, population_entries pe
       WHERE     ia.issue_id = i.id
             AND i.population_entry_id = pe.id
             AND ia.id = p_action_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         popUserId := 0;
   END;

   IF (popUserId > 0)
   THEN
      BEGIN
         -- Find last history record that changed entry type
         SELECT pre_his.id
           INTO maxHistoryId
           FROM (  SELECT peh.*
                     FROM population_entries_hist peh,
                          population_entries pe,
                          population_entries_hist_status pehs
                    WHERE     peh.population_entry_id = pe.id
                          AND pe.population_users_id = popUserId
                          AND peh.status_id = pehs.id
                          AND peh.time_created < p_action_time
                          AND pehs.entry_type_change IS NOT NULL
                 ORDER BY peh.time_created DESC, peh.id DESC) pre_his
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            maxHistoryId := NULL;
      END;

      -- If found history record that changed entry type return that entry type
      IF (maxHistoryId IS NOT NULL)
      THEN
         SELECT pehs.entry_type_change
           INTO entryTypeId
           FROM population_entries_hist_status pehs,
                population_entries_hist peh
          WHERE peh.id = maxHistoryId AND pehs.id = peh.status_id;
      ELSE
         -- If no record that changed entry type was found return GENERAL
         entryTypeId := 1;
      END IF;                           --  IF (maxHistoryId is not null) THEN
   END IF;                                          -- IF (popUserId > 0) THEN

   RETURN entryTypeId;
END GET_ACTION_ENTRY_TYPE;
/
