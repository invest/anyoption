CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:49 (QP5 v5.149.1003.31008) */
FUNCTION "GET_LAST_INVEST_TIME" (p_userId IN NUMBER)
   RETURN DATE
AS
   last_invest_time   DATE := NULL;
BEGIN
   SELECT MAX (TIME_CREATED)
     INTO last_invest_time
     FROM investments
    WHERE user_id = p_userId;

   RETURN last_invest_time;
END;
/
