CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:46 (QP5 v5.149.1003.31008) */
FUNCTION "GET_BALANCE_UNDER_INV_LIMIT" (par_userId       IN NUMBER,
                                        par_currencyId   IN NUMBER)
   RETURN DATE
AS
   par_firstTime    DATE;
   temp_min_limit   NUMBER;
BEGIN
   SELECT ilgc.VALUE
     INTO temp_min_limit
     FROM INVESTMENT_LIMITS il, investment_limit_group_curr ilgc
    WHERE     il.id = 1                            -- Default binary limits id
          AND il.min_limit_group_id = ilgc.investment_limit_group_id
          AND ilgc.CURRENCY_ID = par_currencyId;


   SELECT *
     INTO par_firstTime
     FROM (  SELECT bh.TIME_CREATED
               FROM BALANCE_HISTORY bh
              WHERE bh.TIME_CREATED > (SELECT *
                                         FROM (  SELECT bh1.TIME_CREATED
                                                   FROM BALANCE_HISTORY bh1
                                                  WHERE bh1.BALANCE >
                                                           temp_min_limit
                                                        AND bh1.USER_ID =
                                                               par_userId
                                               ORDER BY bh1.time_created DESC)
                                        WHERE ROWNUM = 1)
                    AND bh.USER_ID = par_userId
           ORDER BY bh.TIME_CREATED)
    WHERE ROWNUM = 1;

   RETURN par_firstTime;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      SELECT TIME_CREATED
        INTO par_firstTime
        FROM (  SELECT bh.TIME_CREATED
                  FROM BALANCE_HISTORY bh
                 WHERE bh.user_id = par_userid
              ORDER BY time_created ASC)
       WHERE ROWNUM = 1;


      RETURN par_firstTime;
END GET_BALANCE_UNDER_INV_LIMIT;
/
