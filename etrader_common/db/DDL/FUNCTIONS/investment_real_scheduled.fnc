CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:57 (QP5 v5.149.1003.31008) */
FUNCTION "INVESTMENT_REAL_SCHEDULED" (p_investment_id IN NUMBER)
   RETURN NUMBER
AS
   invOppTimeEstClosing   opportunities.time_est_closing%TYPE;
   invOppScheduled        opportunities.scheduled%TYPE;
   invOppMarket           opportunities.market_id%TYPE;
   realScheduled          NUMBER;
   openedHourOpps         NUMBER;
   activeHourTemplates    NUMBER;
BEGIN
   SELECT B.time_est_closing, B.scheduled, B.market_id
     INTO invOppTimeEstClosing, invOppScheduled, invOppMarket
     FROM investments A, opportunities B
    WHERE A.opportunity_id = B.id AND A.id = p_investment_id;

   realScheduled := invOppScheduled;

   -- Last week of month opp is considered as week
   -- If we have a week that has its last 3 (or more) 'working days' as holidays don't take over last week last days
   IF invOppScheduled = 4
      AND invOppTimeEstClosing - CURRENT_TIMESTAMP <=
             TO_DSINTERVAL (
                TO_CHAR (invOppTimeEstClosing, 'D') || ' 00:00:00')
   THEN
      realScheduled := 3;
   END IF;

   -- Last day of week/month opp is considered as day
   IF invOppScheduled >= 3
      AND TRUNC (invOppTimeEstClosing) = TRUNC (CURRENT_TIMESTAMP)
   THEN
      realScheduled := 2;
   END IF;

   -- Last hour of day/week/month opp is considered hour
   IF invOppScheduled >= 2
   THEN -- AND invOppTimeEstClosing - current_timestamp <= to_dsinterval('0 01:00:00') THEN
      SELECT COUNT (id)
        INTO openedHourOpps
        FROM opportunities
       WHERE market_id = invOppMarket AND is_published = 1 AND scheduled = 1;

      SELECT COUNT (id)
        INTO activeHourTemplates
        FROM opportunity_templates
       WHERE market_id = invOppMarket AND is_active = 1 AND scheduled = 1;

      IF (activeHourTemplates > 0 AND openedHourOpps = 0)
         OR (activeHourTemplates = 0
             AND invOppTimeEstClosing - CURRENT_TIMESTAMP <=
                    TO_DSINTERVAL ('0 01:00:00'))
      THEN
         realScheduled := 1;
      END IF;
   END IF;

   RETURN (realScheduled);
END;
/
