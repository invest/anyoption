CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:58 (QP5 v5.149.1003.31008) */
FUNCTION "IS_POPULATION_ENTRY_LOCKED" (p_population_entry_id   IN NUMBER,
                                       p_writer_id             IN NUMBER)
   RETURN NUMBER
AS
BEGIN
   FOR v_hist_entry IN (  SELECT status_id, writer_id
                            FROM population_entries_hist
                           WHERE population_entry_id = p_population_entry_id
                        ORDER BY time_created DESC)
   LOOP
      IF v_hist_entry.status_id = 3
         AND (p_writer_id = 0 OR v_hist_entry.writer_id = p_writer_id)
      THEN
         RETURN (v_hist_entry.writer_id);
      END IF;

      IF v_hist_entry.status_id = 4
         AND (p_writer_id = 0 OR v_hist_entry.writer_id = p_writer_id)
      THEN
         RETURN (0);
      END IF;
   END LOOP;

   RETURN (0);
END;
/
