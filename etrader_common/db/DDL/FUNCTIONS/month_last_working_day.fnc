CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:58 (QP5 v5.149.1003.31008) */
FUNCTION "MONTH_LAST_WORKING_DAY" (p_exchange_id   IN NUMBER,
                                   p_add_months    IN NUMBER)
   RETURN DATE
AS
   monthFirstDay    DATE;
   monthLastDay     DATE;
   halfDayHoliday   exchange_holidays.is_half_day%TYPE;
   lastWorkingDay   DATE;
BEGIN
   monthFirstDay :=
      TO_DATE (
         TO_CHAR (ADD_MONTHS (CURRENT_DATE, p_add_months), 'yyyy-mm')
         || '-01',
         'yyyy-mm-dd');
   monthLastDay := LAST_DAY (monthFirstDay);
   lastWorkingDay := NULL;

   FOR i IN 0 .. 30
   LOOP
      IF ( ( (   p_exchange_id = 1
              OR (p_exchange_id = 17)
              OR (p_exchange_id = 18))
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 6
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 7)
          OR (NOT (   (p_exchange_id = 1)
                   OR (p_exchange_id = 17)
                   OR (p_exchange_id = 16)
                   OR (p_exchange_id = 18))
              AND NOT TO_CHAR (monthLastDay - i, 'D') = 7
              AND NOT TO_CHAR (monthLastDay - i, 'D') = 1)
          OR (    (p_exchange_id = 16)
              AND NOT TO_CHAR (monthLastDay - i, 'D') = 5
              AND NOT TO_CHAR (monthLastDay - i, 'D') = 6
              AND NOT TO_CHAR (monthLastDay - i, 'D') = 7))
      THEN
         BEGIN
            SELECT is_half_day
              INTO halfDayHoliday
              FROM exchange_holidays
             WHERE exchange_id = p_exchange_id
                   AND TRUNC (holiday) = monthLastDay - i;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               halfDayHoliday := NULL;
         END;

         IF halfDayHoliday IS NULL
         THEN
            -- full working day
            lastWorkingDay := monthLastDay - i;
            EXIT;
         ELSE
            IF halfDayHoliday = 1
            THEN
               -- working day... at least half
               lastWorkingDay := monthLastDay - i;
               EXIT;
            END IF;
         END IF;
      END IF;
   END LOOP;

   RETURN (lastWorkingDay);
END;
/
