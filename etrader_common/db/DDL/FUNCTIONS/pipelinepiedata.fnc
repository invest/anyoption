CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:59 (QP5 v5.149.1003.31008) */
FUNCTION "PIPELINEPIEDATA" (FROM_DATE IN DATE, TILL_DATE IN DATE)
   RETURN PieDataList
   PIPELINED
AS
   v_pd   PieData;
BEGIN
   FOR v_usr IN (SELECT id, SuccessCount, SuccessMarketsCount
                   FROM (SELECT A.id,
                                (SELECT COUNT (id)
                                   FROM investments invs
                                  WHERE     user_id = A.id
                                        AND win > 0
                                        AND is_canceled = 0
                                        AND invs.time_created >= FROM_DATE
                                        AND invs.time_created <= TILL_DATE)
                                   AS SuccessCount,
                                (SELECT COUNT (DISTINCT Y.market_id)
                                   FROM investments X, opportunities Y
                                  WHERE     X.opportunity_id = Y.id
                                        AND X.user_id = A.id
                                        AND X.win > 0
                                        AND X.is_canceled = 0
                                        AND X.time_created >= FROM_DATE
                                        AND X.time_created <= TILL_DATE)
                                   AS SuccessMarketsCount
                           FROM users A
                          WHERE a.class_id <> 0)
                  WHERE successcount >= 20 AND successmarketscount >= 3)
   LOOP
      FOR v_m
         IN (  SELECT B.market_id, COUNT (A.id) AS cnt
                 FROM investments A, opportunities B, users U
                WHERE     A.opportunity_id = B.id
                      AND A.user_id = v_usr.id
                      AND A.win > 0
                      AND A.is_canceled = 0
                      AND A.time_created >= FROM_DATE
                      AND A.time_created <= TILL_DATE
                      AND U.id = A.user_id
                      AND U.is_active = 1
                      AND A.type_id <> 3
             GROUP BY B.market_id
             ORDER BY COUNT (A.id) DESC)
      LOOP
         v_pd := PieData (v_usr.id, v_m.market_id, v_m.cnt);
         PIPE ROW (v_pd);
      END LOOP;
   END LOOP;

   RETURN;
END PipelinePieData;
/
