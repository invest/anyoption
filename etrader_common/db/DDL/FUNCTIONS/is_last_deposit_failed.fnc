CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:57 (QP5 v5.149.1003.31008) */
FUNCTION "IS_LAST_DEPOSIT_FAILED" (userId IN NUMBER)
   RETURN VARCHAR2
AS
   tran           transactions%ROWTYPE;
   v_class_type   NUMBER;

   CURSOR tranCur (UID NUMBER)
   IS
        SELECT *
          FROM transactions
         WHERE user_id = UID
      ORDER BY time_created DESC;
BEGIN
   OPEN tranCur (userId);

   FETCH tranCur INTO tran;

   IF tranCur%NOTFOUND
   THEN
      RETURN NULL;
   END IF;

   CLOSE tranCur;

   SELECT tt.class_type
     INTO v_class_type
     FROM transaction_types tt
    WHERE tt.id = tran.type_id;

   IF (v_class_type = 1 AND tran.status_id = 3)
   THEN
      RETURN 1;
   END IF;

   RETURN NULL;
END;
/
