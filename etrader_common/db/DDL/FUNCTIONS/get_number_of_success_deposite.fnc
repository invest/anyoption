CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:51 (QP5 v5.149.1003.31008) */
FUNCTION "GET_NUMBER_OF_SUCCESS_DEPOSITE" (userId IN NUMBER)
   RETURN NUMBER
AS
   dep_number   NUMBER;
BEGIN
   dep_number := 0;

   SELECT COUNT (*)
     INTO dep_number
     FROM TRANSACTIONS t, TRANSACTION_TYPES tt
    WHERE     tt.CLASS_TYPE = 1
          AND t.TYPE_ID = tt.id
          AND t.STATUS_ID IN (2, 7)
          AND t.USER_ID = userId;

   RETURN dep_number;
END;
/
