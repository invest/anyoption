CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:54 (QP5 v5.149.1003.31008) */
FUNCTION "GET_TIME_BY_PERIOD" (p_date IN DATE)
   RETURN DATE
AS
   resDate     DATE := p_date;
   dateMonth   NUMBER := NULL;
BEGIN
   IF (p_date IS NOT NULL)
   THEN
      dateMonth := EXTRACT (MONTH FROM p_date);

      IF (dateMonth >= 3 AND dateMonth <= 9)
      THEN
         resDate := resDate + 3 / 24 - 2 / 1440;
      ELSE
         resDate := resDate + 2 / 24 - 2 / 1440;
      END IF;
   END IF;

   RETURN resDate;
END GET_TIME_BY_PERIOD;
/
