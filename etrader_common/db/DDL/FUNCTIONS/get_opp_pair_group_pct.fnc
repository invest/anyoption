CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:52 (QP5 v5.149.1003.31008) */
FUNCTION get_opp_pair_group_pct (in_group IN VARCHAR2)
   RETURN VARCHAR2
IS
   v_groups       VARCHAR2 (500);
   v_out          VARCHAR2 (500);

   CURSOR c_odds_pairs
   IS
        SELECT *
          FROM Opportunity_Odds_pair oop
         WHERE oop.selector_id IN (    SELECT REGEXP_SUBSTR (v_groups,
                                                             '[^,]+',
                                                             1,
                                                             LEVEL)
                                         FROM DUAL
                                   CONNECT BY REGEXP_SUBSTR (v_groups,
                                                             '[^,]+',
                                                             1,
                                                             LEVEL)
                                                 IS NOT NULL)
      ORDER BY return DESC;

   v_odds_paris   Opportunity_Odds_pair%ROWTYPE;
BEGIN
   v_groups := REPLACE (in_group, ' ', ',');

   FOR lp_pair IN c_odds_pairs
   LOOP
      v_out :=
         lp_pair.return || '%vs' || lp_pair.refund || '%;' || ' ' || v_out;
   END LOOP;

   RETURN (v_out);
END get_opp_pair_group_pct;
/
