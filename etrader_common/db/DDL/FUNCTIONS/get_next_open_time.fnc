CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:51 (QP5 v5.149.1003.31008) */
FUNCTION "GET_NEXT_OPEN_TIME" (par_marketId IN NUMBER)
   RETURN VARCHAR2
AS
   par_time_first_invest   VARCHAR2 (5 CHAR);
   par_exchange_id         NUMBER;
   firstDay                DATE;
   par_time_zone           VARCHAR2 (30 CHAR);
   halfDayHoliday          exchange_holidays.is_half_day%TYPE;
   toInsTimeFirstInvest    VARCHAR2 (200 CHAR);
   turkishEndSistaTime     VARCHAR2 (30 CHAR);
BEGIN
   firstDay := CURRENT_DATE + 1;
   turkishEndSistaTime := '14:30';

   SELECT A.time_first_invest, B.exchange_id, A.time_zone
     INTO par_time_first_invest, par_exchange_id, par_time_zone
     FROM opportunity_templates A, markets B
    WHERE     B.id = par_marketId
          AND A.market_id = B.id
          AND A.is_active = 1
          AND A.is_full_day = 1
          AND A.scheduled = 2;

   -- if its turkish market and its not holiday day
   IF (    par_exchange_id = 24
       AND                                                      -- its turkish
          NOT TO_CHAR (SYSDATE, 'D') = 7
       AND                                                     -- not saturday
          NOT TO_CHAR (SYSDATE, 'D') = 1
       AND                                                       -- not sunday
          TO_TIMESTAMP_TZ (
                 TO_CHAR (SYSDATE, 'yyyy-mm-dd')
              || turkishEndSistaTime
              || par_time_zone,
              'yyyy-mm-dd hh24:mi TZR') > SYSDATE) -- current time is b4 end of sista
   THEN
      BEGIN
         SELECT is_half_day
           INTO halfDayHoliday
           FROM exchange_holidays
          WHERE exchange_id = par_exchange_id AND TRUNC (holiday) = SYSDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            halfDayHoliday := NULL;
      END;

      IF halfDayHoliday IS NULL
      THEN                                                  -- if its full day
         toInsTimeFirstInvest :=
            TO_CHAR (
               TO_TIMESTAMP_TZ (
                     TO_CHAR (SYSDATE, 'yyyy-mm-dd')
                  || turkishEndSistaTime
                  || par_time_zone,
                  'yyyy-mm-dd hh24:mi TZR'),
               'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM');
         RETURN toInsTimeFirstInvest;
      END IF;
   END IF;

   -- we asume that there is at least 1 working day in the month

   -- find the first working day of the month and set the time first invest to be on it
   FOR i IN 0 .. 30
   LOOP
      IF ( (   par_exchange_id = 1
            OR par_exchange_id = 17
            OR par_exchange_id = 18)
          AND NOT TO_CHAR (firstDay + i, 'D') = 6
          AND NOT TO_CHAR (firstDay + i, 'D') = 7)
         OR (NOT (   par_exchange_id = 1
                  OR par_exchange_id = 17
                  OR par_exchange_id = 16
                  OR par_exchange_id = 18)
             AND NOT TO_CHAR (firstDay + i, 'D') = 7
             AND NOT TO_CHAR (firstDay + i, 'D') = 1)
         OR (    (par_exchange_id = 16)
             AND NOT TO_CHAR (firstDay + i, 'D') = 5
             AND NOT TO_CHAR (firstDay + i, 'D') = 6
             AND NOT TO_CHAR (firstDay + i, 'D') = 7)
      THEN
         BEGIN
            SELECT is_half_day
              INTO halfDayHoliday
              FROM exchange_holidays
             WHERE exchange_id = par_exchange_id
                   AND TRUNC (holiday) = firstDay + i;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               halfDayHoliday := NULL;
         END;

         IF halfDayHoliday IS NULL OR halfDayHoliday = 1
         THEN
            -- working day... at least half
            toInsTimeFirstInvest :=
               TO_CHAR (
                  TO_TIMESTAMP_TZ (
                        TO_CHAR (firstDay + i, 'yyyy-mm-dd')
                     || par_time_first_invest
                     || par_time_zone,
                     'yyyy-mm-dd hh24:mi TZR'),
                  'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM');
            EXIT;
         END IF;
      END IF;
   END LOOP;

   RETURN toInsTimeFirstInvest;
END GET_NEXT_OPEN_TIME;
/
