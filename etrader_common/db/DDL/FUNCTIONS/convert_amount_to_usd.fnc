CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:46 (QP5 v5.149.1003.31008) */
FUNCTION "CONVERT_AMOUNT_TO_USD" (p_amount        IN NUMBER,
                                  p_currency_id   IN NUMBER,
                                  p_time          IN DATE)
   RETURN NUMBER
AS
   amount      NUMBER;
   market_id   NUMBER;
   LEVEL       NUMBER;
BEGIN
   amount := p_amount;

   IF p_currency_id != 2
   THEN
      market_id := get_currency_market (p_currency_id);
      LEVEL := get_last_closing_level (market_id, p_time);

      IF (is_multiply_to_convert (market_id) = 1)
      THEN
         amount := p_amount * LEVEL;
      ELSE
         amount := p_amount / LEVEL;
      END IF;
   END IF;

   RETURN (amount);
END;
/
