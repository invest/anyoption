CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:53 (QP5 v5.149.1003.31008) */
FUNCTION "GET_RU_TOTAL_PREMIA_AMOUNT" (par_invId IN NUMBER)
   RETURN NUMBER
AS
   invRow      INVESTMENTS%ROWTYPE;
   premiaSum   NUMBER := 0;
   nextInvId   NUMBER;
BEGIN
   SELECT *
     INTO invRow
     FROM INVESTMENTS
    WHERE id = par_invId;

   IF (invRow.INSURANCE_AMOUNT_RU IS NOT NULL)
   THEN
      premiaSum := invRow.INSURANCE_AMOUNT_RU;
   END IF;

   WHILE (invRow.REFERENCE_INVESTMENT_ID IS NOT NULL)
   LOOP
      nextInvId := invRow.REFERENCE_INVESTMENT_ID;

      SELECT *
        INTO invRow
        FROM INVESTMENTS
       WHERE id = nextInvId;

      IF (invRow.INSURANCE_AMOUNT_RU IS NOT NULL)
      THEN
         premiaSum := premiaSum + invRow.INSURANCE_AMOUNT_RU;
      END IF;
   END LOOP;


   RETURN premiaSum;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      -- do nothing.
      RETURN premiaSum;
END GET_RU_TOTAL_PREMIA_AMOUNT;
/
