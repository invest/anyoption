CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:45 (QP5 v5.149.1003.31008) */
FUNCTION CONVERT_AMOUNT_TO_EUR (p_amount        IN NUMBER,
                                p_currency_id   IN NUMBER,
                                p_time          IN DATE)
   RETURN NUMBER
AS
   amount      NUMBER;
   LEVEL       NUMBER;
   market_id   NUMBER := 16;
BEGIN
   amount := p_amount;

   IF P_CURRENCY_ID != 3
   THEN
      amount := convert_amount_to_usd (p_amount, p_currency_id, p_time);
      LEVEL := get_last_closing_level (market_id, p_time);
      amount := amount / LEVEL;
   END IF;

   RETURN (amount);
END;
/
