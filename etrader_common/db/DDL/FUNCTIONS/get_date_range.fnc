CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:48 (QP5 v5.149.1003.31008) */
FUNCTION "GET_DATE_RANGE" (from_dt IN DATE, to_dt IN DATE)
   RETURN date_table
AS
   a_date_table   date_table := date_table ();
   cur_dt         DATE := from_dt;
BEGIN
   WHILE cur_dt <= to_dt
   LOOP
      a_date_table.EXTEND;
      a_date_table (a_date_table.COUNT) := cur_dt;
      cur_dt := cur_dt + 1;
   END LOOP;

   RETURN a_date_table;
END get_date_range;
/
