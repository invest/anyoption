CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:46 (QP5 v5.149.1003.31008) */
FUNCTION "ELIRAN_CHECK_NO_ANSWER" (p_curr_pop_entry_id IN NUMBER)
   RETURN VARCHAR2
AS
   last_call_date   DATE := SYSDATE + 1;
   count_actions    NUMBER := 0;
BEGIN
   FOR action
      IN (  SELECT ia.*
              FROM issue_actions ia, issues i
             WHERE     i.id = ia.issue_id
                   AND i.population_entry_id = p_curr_pop_entry_id
                   AND ia.channel_id = 1
          ORDER BY ia.action_time DESC)
   LOOP
      IF (action.reached_status_id = 1 AND action.reaction_id IN (1, 7))
      THEN
         IF (TO_CHAR (last_call_date, 'dd/mm/yyyy') !=
                TO_CHAR (action.action_time, 'dd/mm/yyyy'))
         THEN
            last_call_date := action.action_time;
            count_actions := count_actions + 1;
         END IF;
      ELSE
         EXIT;
      END IF;
   END LOOP;

   RETURN count_actions;
END ELIRAN_CHECK_NO_ANSWER;
/
