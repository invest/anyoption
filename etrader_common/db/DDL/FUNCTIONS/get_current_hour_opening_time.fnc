CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:47 (QP5 v5.149.1003.31008) */
FUNCTION "GET_CURRENT_HOUR_OPENING_TIME" (p_market_id IN NUMBER)
   RETURN VARCHAR2
AS
BEGIN
   -- first check if we have opened hour opp
   FOR v_entry
      IN (  SELECT TO_CHAR (time_first_invest,
                            'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM')
                      AS time_first_invest
              FROM opportunities
             WHERE     market_id = p_market_id
                   AND scheduled = 1
                   AND is_published = 1
          ORDER BY time_est_closing)
   LOOP
      RETURN (v_entry.time_first_invest);
   END LOOP;

   -- if not get the the time last opp on this market closed
   FOR v_entry
      IN (  SELECT TO_CHAR (time_est_closing,
                            'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM')
                      AS time_est_closing
              FROM opportunities
             WHERE market_id = p_market_id AND NOT time_act_closing IS NULL
          ORDER BY time_est_closing DESC)
   LOOP
      RETURN (v_entry.time_est_closing);
   END LOOP;

   RETURN (NULL);
END;
/
