CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:56 (QP5 v5.149.1003.31008) */
FUNCTION "GET_USER_X_DEPOSIT" (m_user_id IN NUMBER, m_order IN NUMBER)
   RETURN NUMBER
AS
   trans_id   NUMBER;
BEGIN
   SELECT id
     INTO trans_id
     FROM (SELECT a.*, ROWNUM rnum
             FROM (  SELECT t.id
                       FROM users u, transactions t, transaction_types tt
                      WHERE     t.user_id = u.id
                            AND t.type_id = tt.id
                            AND tt.class_type = 1
                            AND t.status_id IN (2, 7, 8)
                            AND u.CLASS_ID <> 0
                            AND t.TYPE_ID NOT IN (4, 6, 12)
                            AND u.id = m_user_id
                   ORDER BY t.TIME_CREATED) a
            WHERE ROWNUM <= m_order)
    WHERE rnum >= m_order;


   RETURN (trans_id);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END GET_USER_X_DEPOSIT;
/
