CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:53 (QP5 v5.149.1003.31008) */
FUNCTION "GET_POPULATION_ENTRY_STATUS" (p_population_entry_id IN NUMBER)
   RETURN NUMBER
AS
BEGIN
   FOR v_hist_entry
      IN (  SELECT peh.*
              FROM population_entries_hist peh,
                   population_entries_hist_status pehs
             WHERE     peh.population_entry_id = p_population_entry_id
                   AND peh.status_id = pehs.id
                   AND (peh.status_id IN (1, 5, 7, 8) OR pehs.is_remove = 1)
          ORDER BY peh.time_created DESC)
   LOOP
      RETURN (v_hist_entry.status_id);
   END LOOP;

   -- this should not be reached
   RETURN (0);
END;
/
