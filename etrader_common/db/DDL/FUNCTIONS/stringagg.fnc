create or replace function stringagg(input varchar2) return varchar2
  parallel_enable
  aggregate using stringagg_t;
/
