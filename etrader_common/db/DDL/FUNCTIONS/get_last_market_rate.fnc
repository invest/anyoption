CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:49 (QP5 v5.149.1003.31008) */
FUNCTION "GET_LAST_MARKET_RATE" (p_market_id IN NUMBER)
   RETURN NUMBER
AS
   lastRate   NUMBER;
BEGIN
   FOR item IN (  SELECT rate
                    FROM market_rates
                   WHERE market_id = p_market_id
                ORDER BY id DESC)
   LOOP
      lastRate := item.rate;
      EXIT;
   END LOOP;

   RETURN (lastRate);
END;
/
