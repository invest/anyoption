CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:57 (QP5 v5.149.1003.31008) */
FUNCTION "INVESTMENT_BY_ORDER" (m_user_id IN NUMBER, m_order IN NUMBER)
   RETURN NUMBER
AS
   invest_id   NUMBER;
BEGIN
   SELECT id
     INTO invest_id
     FROM (SELECT a.*, ROWNUM rnum
             FROM (  SELECT i.id
                       FROM investments i
                      WHERE     i.IS_SETTLED = 1
                            AND i.IS_CANCELED = 0
                            AND i.user_id = m_user_id
                   ORDER BY i.TIME_CREATED) a
            WHERE ROWNUM <= m_order)
    WHERE rnum >= m_order;

   RETURN (invest_id);
END;
/
