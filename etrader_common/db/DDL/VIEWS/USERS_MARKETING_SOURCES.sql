create or replace view users_marketing_sources as
select u.id as user_id,

       case
         when (
                (u.affiliate_system_user_time is not null or mc.id = 365)
                and u.AFFILIATE_KEY not in (661991,661142,660986 ,661947)
                and ba.affiliate_key is null
              ) then
          'affiliete'
         when (
                (u.affiliate_system_user_time is not null or mc.id = 365)
                and u.AFFILIATE_KEY  in (661991,661142,660986 ,661947)
              ) then
          'Jason and Tony'
         when (
                u.affiliate_system_user_time is null and mc.campaign_manager in (1264)
              ) then
          'Media Tal V'
         when (
                ((u.affiliate_system_user_time is not null or mc.id = 365) and ba.affiliate_key is not null)
                or
                (mc.campaign_manager in (1272) and u.affiliate_system_user_time is null)
              ) then
          'Beni'
        when (
            u.affiliate_system_user_time is null
            and mc.campaign_manager not in (839,546,993)
            and ms.id in (105,25,320,333,316,534,569,822,808,341,591)
            and mc.payment_recipient_id != 700
            ) then
        'ppc'
        when (
            u.affiliate_system_user_time is null
            and mc.payment_recipient_id = 700
            ) then
        'google display'
        when (
              u.affiliate_system_user_time is null
              and (mc.id in (112, 68, 636) or mc.campaign_manager in (839, 546, 993))
             ) then
          'free'
         else
          'Media Tal T'
       end as channel
  from users u
 inner join MARKETING_COMBINATIONS mco
    on u.COMBINATION_ID = mco.id
 inner join MARKETING_CAMPAIGNS mc
    on mco.CAMPAIGN_ID = mc.id
 inner join MARKETING_SOURCES ms
    on mc.SOURCE_ID = ms.id
  left join BENI_affiliates ba
    on u.AFFILIATE_KEY = ba.AFFILIATE_KEY;
