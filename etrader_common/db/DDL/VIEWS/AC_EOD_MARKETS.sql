----------
-- AnyCapital.
--
CREATE OR REPLACE VIEW ETRADER.AC_EOD_MARKETS AS
SELECT M.ID
      ,M.NAME
      ,CLOSING_LEVEL
      ,TIME_EST_CLOSING
  FROM OPPORTUNITIES O
  JOIN MARKETS M ON (M.ID = O.MARKET_ID)
 WHERE O.SCHEDULED = 2
   AND O.IS_SETTLED = 1
   AND O.CLOSING_LEVEL IS NOT NULL
   AND O.TIME_EST_CLOSING > (SYSDATE - 365)
 ORDER BY M.ID ASC
         ,O.ID DESC;

----------
-- Permissions & accessability.
--
CREATE OR REPLACE SYNONYM ANYCAPITAL.AC_EOD_MARKETS FOR ETRADER.AC_EOD_MARKETS;

GRANT SELECT ON ETRADER.MARKETS        TO ANYCAPITAL;
GRANT SELECT ON ETRADER.OPPORTUNITIES  TO ANYCAPITAL;
GRANT SELECT ON ETRADER.AC_EOD_MARKETS TO ANYCAPITAL;
