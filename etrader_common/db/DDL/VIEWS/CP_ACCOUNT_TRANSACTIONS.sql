CREATE OR REPLACE FORCE VIEW "ETRADER"."CP_ACCOUNT_TRANSACTIONS" ("ACCOUNTID", "ISDEMO", "TRANSACTIONID", "ISWITHDRAWAL", "ISWDAPPROVED", "BONUSID", "CURRENCY", "AMOUNT", "DATE", "BALANCE", "AGENTID", "AGENTNAME", "IS_FAILED_DEPOSIT") AS 
  select u.id accountid
      ,case
         when u.class_id = 0 then
          1
         else
          0
       end isdemo
      ,t.id transactionid
      ,case
         when tt.class_type = 2
              and t.status_id in (2, 4, 9, 17) then
          1
         else
          0
       end iswithdrawal
      ,case
         when tt.class_type = 2
              and t.status_id in (2, 4, 9, 17) then
          case
            when t.status_id = 4 then
             1
            else
             0
          end
         else
          null
       end iswdapproved
      ,bo.bonus_id bonusid
      ,c.code currency
      ,t.amount / 100 amount
      ,t.time_created "DATE"
       ,u.balance / 100 balance
       ,w.id agentid
       ,w.user_name agentname
       ,case
         when tt.class_type = 1
              and t.status_id = 3 then
          1
         else
          0
       end is_failed_deposit
from   transactions t
join   transaction_types tt
on     tt.id = t.type_id
join   users u
on     u.id = t.user_id
join   currencies c
on     c.id = u.currency_id
left   join population_users pu
on     pu.user_id = u.id
left   join writers w
on     w.id = pu.curr_assigned_writer_id
left   join bonus_users bo
on     bo.id = t.bonus_user_id
and    t.type_id in (12, 13)
where  ((tt.class_type = 1 and t.status_id in (2, 3,7, 8)) -- deposit
       or (tt.class_type = 2 and t.status_id in (2, 4, 9, 17)) -- withdrawal
       or (t.type_id in (12, 13) and t.status_id = 2) --bonus
       )
and    t.time_created between sysdate-31 and sysdate;
