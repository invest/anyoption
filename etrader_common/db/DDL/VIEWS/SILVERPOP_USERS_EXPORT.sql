CREATE OR REPLACE VIEW ETRADER.SILVERPOP_USERS_EXPORT
AS
SELECT u.user_name
      ,(u.balance / 100) AS balance
      ,nvl(bu.bonus_amount - bu.bonus_winnings, 0) / 100 AS bonus_balance
      ,CASE
         WHEN (u.balance - nvl(bu.bonus_amount, 0)) / 100 < 0 THEN
          0
         ELSE
          (u.balance - nvl(bu.bonus_amount, 0)) / 100
       END AS cash_balance
      ,u.first_deposit_id
      ,u.time_created
      ,u.first_name
      ,u.last_name
      ,u.email
      ,u.country_id
      ,u.currency_id
      ,u.skin_id
      ,u.time_last_login
      ,u.combination_id
      ,u.affiliate_key
      ,u.affiliate_system_user_time
      ,u.bonus_abuser
      ,u.dynamic_param
      ,u.writer_id
      ,u.status_id
      ,u.rank_id
      ,u.class_id
      ,u.is_active
      ,u.is_contact_by_email
      ,u.gender
      ,u.time_birth_date
      ,mc.campaign_id
      ,c.symbol
      ,co.country_name
      ,su.ID
      ,su.USER_ID
      ,su.LAST_INV_DATE
      ,(su.AVG_INV_AMOUNT / 100) AS AVG_INV_AMOUNT
      ,(su.AVG_DEP_AMOUNT / 100) AS AVG_DEP_AMOUNT
      ,su.TRANS_STATUS_FAILED_COUNT
      ,su.TRANS_STATUS_SUCCEEDED_COUNT
      ,su.TIME_FIRST_FAILED_ATTEMPT
      ,su.TIME_FIRST_SUCCEEDED_ATTEMPT
      ,su.LAST_ISSUE_ACTION_TYPE
      ,su.LAST_UPDATE_TIME
      ,su.FIRST_INV_TIME
      ,su.LAST_DEP_DATE
      ,rawtohex(u.email) email_encrypt
FROM users u
    ,silverpop_users su
    ,currencies c
    ,countries co
    ,marketing_combinations mc
    ,(SELECT user_id
            ,SUM(bu.adjusted_amount) AS bonus_amount
            ,CASE
               WHEN SUM(nvl(bu.adjusted_amount, 0) - nvl(bu.bonus_amount, 0)) > 0 THEN
                SUM(nvl(bu.adjusted_amount, 0) - nvl(bu.bonus_amount, 0))
               ELSE
                0
             END AS bonus_winnings
      FROM bonus_users bu
      WHERE bu.bonus_state_id IN (2, 3)
      GROUP BY user_id) bu
WHERE u.id = su.user_id
AND u.currency_id = c.id
AND u.country_id = co.id
AND mc.id = u.combination_id
AND u.id = bu.user_id(+)
AND su.last_update_time > (SELECT last_run_time FROM jobs WHERE id = 2);
