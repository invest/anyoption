
  CREATE OR REPLACE FORCE VIEW "ETRADER"."VS_ACCOUNTS" ("USER_ID", "CONTACT_ID", "FTD_DATE", "RANK_NAME", "STATUS_NAME", "CURRENT_BALANCE", "CURRENCY", "NUM_OF_DEP", "LAST_REACHED_CALL", "SKIN", "COUNTRY", "COUNTRY_CODE", "COUNTRY_TIER", "TIME_ZONE", "CAMPAIGN_ID", "PRIORITY", "AFFILIATE_KEY", "MOBILE_PHONE", "LAND_LINE_PHONE", "LOGIN_IN24_H", "BRAND", "PLATFORM", "LANGUAGE", "REG_DATE", "CURRENT_ASSIGNED_WRITER", "IS_ACTIVE", "CLOSURE_REASON", "POPULATION", "POPULATION_TIME", "CURRENT_POPULATION_ENTRY_ID", "CALLS", "REACHED", "REACHED_CALLS_ON_POPULATION", "LAST_REACHED_ON_CURR_POP") AS 
  WITH
  /* Sub-view for the Calls. */
  v_calls AS
  ( select /*+ MATERIALIZE */
           ia.id  as issue_action_id
          ,iat.id as issue_action_type_id
          ,i.user_id
          ,i.contact_id
          ,ia.action_time
          ,iat.reached_status_id
      from      etrader.issues             i
     inner join etrader.issue_actions      ia  on i.id = ia.issue_id
     inner join etrader.issue_action_types iat on ia.issue_action_type_id = iat.id
     where i.time_created > (sysdate - 30)
       and iat.channel_id = 1
  )
/* The Main SELECT. */
select usr.id           as user_id
      ,c.id             as contact_id
      ,ftd.time_created as ftd_date
      ,urr.rank_name
      ,us.status_name
      ,usr.balance/100  as current_balance
      ,curr.code        as currency
      --
      ,num_of_dep.num_of_dep
      --
      ,case when usr.id is not null then last_reached_users.last_reached_users else last_reached_contacts.last_reached_contacts end as last_reached_call
      --
      ,case when usr.id is not null then sk.name                    else s.name                      end as skin
      ,case when usr.id is not null then cn.country_name            else co.country_name             end as country
      ,case when usr.id is not null then cn.phone_code              else co.phone_code               end as country_code
      ,case when usr.id is not null then cg.COUNTRIES_GROUP_TIER_ID else cgc.COUNTRIES_GROUP_TIER_ID end as country_tier
      ,case when usr.id is not null then cn.gmt_offset              else co.gmt_offset               end as time_zone
      ,case when usr.id is not null then mc.campaign_id             else mco.campaign_id             end as campaign_id
      ,case when usr.id is not null then mcu.priority               else mcc.priority                end as priority
      ,case when usr.id is not null then usr.affiliate_key          else c.affiliate_key             end as affiliate_key
      ,case when usr.id is not null then usr.mobile_phone           else c.mobile_phone              end as mobile_phone
      ,case when usr.id is not null then usr.land_line_phone        else c.land_line_phone           end as land_line_phone
      ,case when (sysdate - usr.TIME_LAST_LOGIN) < 24 then 'Yes'    else 'No'                        end as login_in24_h
      ,case when usr.id is not null and usr.writer_id     in (10000,20000      ) then 'CO'
            when usr.id is not null and usr.writer_id     in (            26000) then 'Bubbles DAP'
            when usr.id is not null and usr.writer_id not in (10000,20000,26000) then 'AO'
            when usr.id is     null and c.writer_id       in (10000,20000      ) then 'CO'
            when usr.id is     null and c.writer_id       in (            26000) then 'Bubbles DAP'
            else 'AO'
       end as Brand
      ,case when usr.id is not null and usr.writer_id     in (  200,20000,26000) then 'Mobile'
            when usr.id is not null and usr.writer_id not in (  200,20000,26000) then 'Web'
            when usr.id is     null and c.writer_id       in (  200,20000,26000) then 'Mobile'
            else 'Web'
       end as Platform
      ,case when usr.id is not null then l.display_name  else '' end as LANGUAGE
      ,usr.time_created as reg_date
      ,w.user_name      as current_assigned_writer
      ,usr.is_active
      --
      ,case when usr.is_active = 0 and closing_reason.closing_reason <> 39 then 'Fraud'
            when usr.is_active = 0 and closing_reason.closing_reason =  39 then 'Multiple Login'
            else null
       end as Closure_Reason
      ,p.name                      as population
      ,pe.qualification_time       as population_time
      ,pu.curr_population_entry_id as Current_population_entry_id
      --
      -- Calls
      ,count(distinct case when usr.id is not null then calls_users.issue_action_type_id
                           when usr.id is     null then calls_contacts.issue_action_type_id
                           else null
                      end
            ) as calls
      --
      -- Reached
      ,count(distinct case when usr.id is not null and calls_users.reached_status_id    = 2 then calls_users.issue_action_type_id
                           when usr.id is     null and calls_contacts.reached_status_id = 2 then calls_contacts.issue_action_type_id
                           else null
                      end
            ) as reached
      --
      -- Reached calls on population
      ,count(distinct case when usr.id is not null and calls_users.reached_status_id    = 2 and calls_users.action_time    >= pe.qualification_time then calls_users.issue_action_type_id
                           when usr.id is     null and calls_contacts.reached_status_id = 2 and calls_contacts.action_time >= pe.qualification_time then calls_contacts.issue_action_type_id
                           else null
                      end
            ) as reached_calls_on_population
      ,max(case when usr.id is not null and calls_users.reached_status_id=2 and calls_users.action_time>=pe.qualification_time then calls_users.action_time
                when usr.id is null and calls_contacts.reached_status_id=2   and calls_contacts.action_time>=pe.qualification_time then calls_contacts.action_time
                else null 
           end ) as last_reached_on_curr_pop
  --
  from      etrader.population_users       pu
  left join etrader.writers                w    on pu.curr_assigned_writer_id = w.id
  --
  left join etrader.population_entries     pe   on pu.curr_population_entry_id = pe.id
  left join etrader.populations            p    on pe.population_id   = p.id
  --
  left join etrader.users                  usr  on pu.user_id         = usr.id
  left join etrader.users_rank             urr  on usr.rank_id        = urr.id
  left join etrader.users_status           us   on usr.status_id      = us.id
  left join etrader.currencies             curr on usr.currency_id    = curr.id
  left join etrader_anal.users_ftds_bi     ftd  on usr.id             = ftd.user_id    /* view */
  left join etrader.contacts               c    on pu.contact_id      = c.id
  left join etrader.skins                  sk   on usr.skin_id        = sk.id
  left join etrader.skins                  s    on c.skin_id          = s.id
  left join etrader.languages              l    on usr.language_id    = l.id
  --
  left join etrader.countries              cn   on usr.country_id     = cn.id
  left join etrader.countries_group        cg   on cn.id              = cg.country_id
  left join etrader.countries              co   on c.country_id       = co.id
  left join etrader.countries_group        cgc  on cn.id              = cgc.country_id
  --
  left join etrader.marketing_combinations mc   on usr.combination_id = mc.id
  left join etrader.marketing_campaigns    mcu  on mc.campaign_id     = mcu.id
  left join etrader.marketing_combinations mco  on c.combination_id   = mco.id
  left join etrader.marketing_campaigns    mcc  on mco.campaign_id    = mcc.id
  --
  left join v_calls              calls_users    on usr.id = calls_users.user_id
  left join v_calls              calls_contacts on c.id = calls_contacts.contact_id
  --
  left join (select user_id
                   ,max(action_time) last_reached_users
               from v_calls
              where reached_status_id = 2
              group by user_id
            ) last_reached_users on usr.id = last_reached_users.user_id
  --
  left join (select contact_id
                   ,max(action_time) last_reached_contacts
               from v_calls
              where contact_id is not null
                and reached_status_id = 2
              group by contact_id
            ) last_reached_contacts on c.id = last_reached_contacts.contact_id
  --
  left join (select user_id
                   ,max(issue_action_type_id) KEEP (DENSE_RANK LAST ORDER BY action_time) closing_reason
               from v_calls
              group by user_id
            ) closing_reason on usr.id = closing_reason.user_id
  --
  left join (select t.user_id
                   ,count(t.id) num_of_dep
               from etrader.transactions t
              inner join etrader.transaction_types tt on t.type_id = tt.id
              where t.status_id in (2,7,8)
                and tt.class_type = 1
              group by t.user_id
            ) num_of_dep on usr.id = num_of_dep.user_id
  --
 where pe.qualification_time > (sysdate - 30)
   and (   (    usr.id is not null
            and usr.class_id <> 0
           )
        or c.id is not null
       )
 group by usr.id
         ,c.id
         ,ftd.time_created
         ,urr.rank_name
         ,us.status_name
         ,usr.balance/100
         ,curr.code
         ,num_of_dep
         ,case when usr.id is not null then last_reached_users.last_reached_users else last_reached_contacts.last_reached_contacts end
         ,case when usr.id is not null then sk.name                    else s.name            end
         ,case when usr.id is not null then cn.country_name            else co.country_name   end
         ,case when usr.id is not null then cn.phone_code              else co.phone_code     end
         ,case when usr.id is not null then cg.COUNTRIES_GROUP_TIER_ID else cgc.COUNTRIES_GROUP_TIER_ID  end
         ,case when usr.id is not null then cn.gmt_offset              else co.gmt_offset     end
         ,case when usr.id is not null then mc.campaign_id             else mco.campaign_id   end
         ,case when usr.id is not null then mcu.priority               else mcc.priority      end
         ,case when usr.id is not null then usr.affiliate_key          else c.affiliate_key   end
         ,case when usr.id is not null then usr.mobile_phone           else c.mobile_phone    end
         ,case when usr.id is not null then usr.land_line_phone        else c.land_line_phone end
         ,case when (sysdate - usr.TIME_LAST_LOGIN) < 24 then 'Yes'    else 'No'              end
         ,case when usr.id is not null  and usr.writer_id     in (10000,20000      ) then 'CO'
               when usr.id is not null  and usr.writer_id     in (            26000) then 'Bubbles DAP'
               when usr.id is not null  and usr.writer_id not in (10000,20000,26000) then 'AO'
               when usr.id is     null  and c.writer_id       in (10000,20000      ) then 'CO'
               when usr.id is     null  and c.writer_id       in (            26000) then 'Bubbles DAP'
               else 'AO'
          end
         ,case when usr.id is not null  and usr.writer_id     in (  200,20000,26000) then 'Mobile'
               when usr.id is not null  and usr.writer_id not in (  200,20000,26000) then 'Web'
               when usr.id is     null  and c.writer_id       in (  200,20000,26000) then 'Mobile'
               else 'Web'
          end
         ,case when usr.id is not null then l.display_name  else ''  end
         ,usr.time_created
         ,w.USER_NAME
         ,usr.is_active
         ,case when usr.is_active = 0 and  closing_reason.closing_reason <> 39 then 'Fraud'
               when usr.is_active = 0 and  closing_reason.closing_reason =  39 then 'Multiple Login'
               else null
          end
         ,p.name
         ,pe.qualification_time
         ,pu.CURR_POPULATION_ENTRY_ID;
