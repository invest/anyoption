﻿create or replace view last_levels 
as
select closing_level, market_id, time_est_closing
from   opportunities
where  closing_level <> 0
and    scheduled in (2, 3, 4) -- 2 - day, 3 - week, 4 - month
and    opportunity_type_id = 1
and    market_id in (14, 15, 16, 17, 26, 289, 604, 619, 637, 651, 681, 682, 692, 787)
order  by time_est_closing desc;
