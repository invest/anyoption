CREATE OR REPLACE FORCE VIEW "ETRADER"."CP_ACCOUNT_CLOSED_POSITIONS" ("ACCOUNTID", "ISDEMO", "MARKETID", "TRADE_ID", "ENTRYTIME", "ENTRYPRICE", "EXITTIME", "EXITPRICE", "AMOUNT", "CALL", "PAYOUT", "PRODUCT", "BALANCE", "AGENTID", "AGENTNAME", "IP") AS 
  select u.id accountid
      ,case
         when u.class_id = 0 then
          1
         else
          0
       end isdemo
      ,mns.short_name marketid
      ,i.id trade_id
      ,i.time_created entrytime
      ,i.current_level entryprice
      ,i.time_settled exittime
      ,i.settled_level exitprice
      ,i.amount / 100 amount
      ,it.name call
      ,(i.amount + i.win - i.lose) / 100 payout
      ,ot.description product
      ,u.balance / 100 balance
      ,w.id agentid
      ,w.user_name agentname,
      i.ip
from   investments i
join   users u
on     u.id = i.user_id
join   opportunities o
on     o.id = i.opportunity_id
join   opportunity_types ot
on     ot.id = o.opportunity_type_id
join   investment_types it
on     it.id = i.type_id
left   join population_users pu
on     pu.user_id = u.id
left   join writers w
on     w.id = pu.curr_assigned_writer_id
left   join market_name_skin mns
on     o.market_id = mns.market_id
where  i.is_settled = 1
and    i.time_created between sysdate - 31 and sysdate
and    mns.skin_id=16;