CREATE OR REPLACE TRIGGER trg_before_opp_odds_group
  AFTER DELETE OR INSERT OR UPDATE ON opportunity_odds_group
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  TRIG_EVENT VARCHAR2(10);
BEGIN

  IF INSERTING THEN
    TRIG_EVENT := 'INSERT';
    INSERT INTO opportunity_odds_group_history
      (id,
       odds_group_id,
       odds_group,
       base,
       odds_group_default,
       writer_id,
       new_pair,
       date_changed,
       trig_event)
    VALUES
      (opp_odds_group_history_seq.nextval,
       :new.odds_group_id,
       :new.odds_group,
       :new.base,
       :new.odds_group_default,
       :new.writer_id,
       :new.odds_group_default,
       sysdate,
       TRIG_EVENT);
  END IF;

  IF UPDATING THEN
    TRIG_EVENT := 'UPDATE';
  
    IF :new.odds_group_id != :old.odds_group_id OR
       :new.odds_group != :old.odds_group OR :new.base != :old.base OR
       :new.odds_group_default != :old.odds_group_default OR
       :new.writer_id != :old.writer_id THEN
    
      INSERT INTO opportunity_odds_group_history
        (id,
         odds_group_id,
         odds_group,
         base,
         odds_group_default,
         writer_id,
         new_pair,
         date_changed,
         trig_event)
      VALUES
        (opp_odds_group_history_seq.nextval,
         :new.odds_group_id,
         :new.odds_group,
         :new.base,
         :old.odds_group_default,
         :new.writer_id,
         :new.odds_group_default,
         sysdate,
         TRIG_EVENT);
    
    END IF;
  END IF;

  IF DELETING THEN
    TRIG_EVENT := 'DELETE';
    INSERT INTO opportunity_odds_group_history
      (id,
       odds_group_id,
       odds_group,
       base,
       odds_group_default,
       writer_id,
       new_pair,
       date_changed,
       trig_event)
    VALUES
      (opp_odds_group_history_seq.nextval,
       :old.odds_group_id,
       :old.odds_group,
       :old.base,
       :old.odds_group_default,
       :old.writer_id,
       :old.odds_group_default,
       sysdate,
       TRIG_EVENT);
  END IF;
END before_opportunity_odds_group;