create or replace TRIGGER ETRADER.TRG_AFT_INS_ISSUE_ACTION
AFTER INSERT ON ETRADER.ISSUE_ACTIONS FOR EACH ROW
DECLARE
        issue_action_type   ISSUE_ACTION_TYPES%ROWTYPE;

        cursor c_get_customerId is
        select i.user_id, i.contact_id
        from issues i
        where i.id = :new.issue_id;

        cursor c_get_userId is
        select uai.user_id
        from users_additional_info uai, issues i
        where i.id =  :new.issue_id and i.user_id = uai.user_id ;

        cursor c_get_contactId is
        select cai.contact_id
        from contacts_additional_info cai, issues i
        where i.id =  :new.issue_id and i.contact_id = cai.contact_id;

        v_user_id number:=0;
        v_contact_id number:=0;
        vu_user_id number:=0;
        vu_contact_id number:=0;
        v_is_active number :=-1;

        v_is_suspended number:=0;
        v_suspended_reason VARCHAR2(200 CHAR);
        v_time_suspended_regulation date;
        
        cursor c_calc_pending_doc_call is
        select 
          I.USER_ID, inf.call_attempts, rs.id
        from 
          (select :new.issue_action_type_id issue_action_type_id, :new.direction_id direction_id from dual) ia,
          issue_action_types iat,
          issue_reached_statuses rs,
          issues i, 
          USERS_PENDING_DOCS_CALL_INFO inf
        where 
          i.subject_id = 56
          and i.user_id = inf.user_id(+)
          and i.Id = :new.issue_id
          and ia.issue_action_type_id = iat.id
          and iat.channel_id = 1
          and ia.direction_id = 1
          and iat.reached_status_id = rs.id(+);                    

       v_calc_pd_user_id number;  
       v_call_attempts number; 
       v_reached_statuses number:=-1;
BEGIN

        Select *
        into      issue_action_type
        from     issue_action_types iat
        where   iat.id = :new.issue_action_type_id;

        -- If call update last_call_action_id and Check if reached
        IF (issue_action_type.channel_id = 1) THEN
              -- IF reached call update calls_num and reached_calls_num
              IF (issue_action_type.reached_status_id = 2) THEN
                      update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  last_connection_action_id = :new.id,
                                  calls_num = calls_num + 1,
                                  reached_calls_num = reached_calls_num + 1,
                                  last_reached_call_action_id = :new.id
                      where  id = :new.issue_id;
              ELSE
                     update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  last_connection_action_id = :new.id,
                                  calls_num = calls_num + 1
                      where  id = :new.issue_id;
              END IF; -- IF (issue_action_type.reached_status_id = 2) THEN
        ELSE
              IF (issue_action_type.channel_id in (3,8)) THEN
                      update issues
                      set       last_action_id = :new.id,
                                      last_connection_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant
                      where  id = :new.issue_id;
              ELSE
                      update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant
                      where  id = :new.issue_id;
              END IF;
        END IF; --  IF (issue_action_type.channel_id = 1) THEN

        --Inser into Velocity Check Table
        IF :new.issue_action_type_id = 45 or :new.issue_action_type_id = 46 THEN

            open c_get_customerId;
            fetch c_get_customerId into v_user_id, v_contact_id;
            close c_get_customerId;

            if :new.issue_action_type_id = 45 then
              v_is_active := 0;
            else v_is_active := 1;
            end if;

            insert into velocity_check
              (id, user_id, issue_actions_id, is_active)
            values
              (seq_velocity_check.nextval, v_user_id, :new.id, v_is_active);

        END IF;

     --Update Suspend/Unsuspend for User Regulation
       IF :new.issue_action_type_id = 47 or :new.issue_action_type_id = 48 THEN

            open c_get_customerId;
            fetch c_get_customerId into v_user_id, v_contact_id;
            close c_get_customerId;

            if :new.issue_action_type_id = 47 then
              v_is_suspended := 4;
              v_suspended_reason:= 'Suspended with IssueId:'||:new.issue_id||' and ISSUE_ACTIONS_ID:'||:new.ID;
              v_time_suspended_regulation := :new.action_time;
            else
              v_is_suspended := 0;
              v_suspended_reason := 'Unsuspended with IssueId:'||:new.issue_id||' and ISSUE_ACTIONS_ID:'||:new.ID;
              v_time_suspended_regulation := null;
            end if;

           update users_regulation a
              set
                  COMMENTS = v_suspended_reason,
                  time_suspended_regulation = v_time_suspended_regulation,
                  SUSPENDED_REASON_ID = v_is_suspended
            where user_id = v_user_id;

        END IF;

        --last rep chat
        IF :new.issue_action_type_id = 38 THEN
                open c_get_customerId;
                fetch c_get_customerId into v_user_id, v_contact_id;
                close c_get_customerId;

                open c_get_userId;
                fetch c_get_userId into vu_user_id;
                close c_get_userId;

                open c_get_contactId;
                fetch c_get_contactId into vu_contact_id;
                close c_get_contactId;

                IF v_user_id != 0 THEN
                      IF vu_user_id != 0 THEN
                              update
                                          users_additional_info
                              set
                                          last_chat_writer_id =:new.writer_id,
                                          time_updated = sysdate
                              where
                                          user_id = vu_user_id;
                       ELSE
                                insert into users_additional_info
                                  (id, user_id, last_chat_writer_id, time_created,time_updated)
                              values
                                  (seq_users_additional_info.nextval, v_user_id, :new.writer_id, sysdate,sysdate);
                         END IF;
                 END IF;

                 IF v_contact_id != 0 THEN
                         IF vu_contact_id != 0 THEN
                                 update
                                                contacts_additional_info
                                  set
                                                last_chat_writer_id =:new.writer_id,
                                                time_updated = sysdate
                                  where
                                                contact_id = vu_contact_id;
                          ELSE
                                insert into contacts_additional_info
                                    (id, contact_id, last_chat_writer_id, time_created,time_updated)
                                values
                                    (seq_contacts_additional_info.nextval, v_contact_id, :new.writer_id, sysdate,sysdate);
                            END IF;
                 END IF;
        END IF;
IF :new.issue_action_type_id = 23 or :new.issue_action_type_id = 16 or :new.issue_action_type_id = 17 or :new.issue_action_type_id = 18 or :new.issue_action_type_id = 32 or :new.issue_action_type_id = 34 THEN
  open c_get_customerId;
  fetch c_get_customerId into v_user_id, v_contact_id;
  close c_get_customerId;

  if :new.issue_action_type_id = 23 then
    update users_regulation set not_interested = 1 where user_id = v_user_id;
  else
    update users_regulation set not_interested = 2 where user_id = v_user_id;
  end if;
end if;

IF :new.direction_id = 1 THEN
  open c_calc_pending_doc_call;
  fetch c_calc_pending_doc_call into v_calc_pd_user_id, v_call_attempts, v_reached_statuses ;
  if c_calc_pending_doc_call%found then
     if nvl(v_call_attempts,-1) > 0  then
        update users_pending_docs_call_info
           set 
               call_attempts = v_call_attempts + 1,
               last_call_time = :new.action_time,
               last_reached_time = decode(v_reached_statuses, 2, :new.action_time, last_reached_time)
         where user_id = v_calc_pd_user_id;         
       else
         insert into USERS_PENDING_DOCS_CALL_INFO
             (user_id, call_attempts, last_call_time, last_reached_time)
         values
             (v_calc_pd_user_id, 1, :new.action_time, decode(v_reached_statuses, 2, :new.action_time, null));
     end if;    
  end if;  
  close c_calc_pending_doc_call;
END IF;

END;