CREATE OR REPLACE TRIGGER "TRG_AFT_UPD_ISSUE_ACTION" 
AFTER UPDATE ON issue_actions
FOR EACH ROW
BEGIN

        IF (:new.is_significant = 1 AND :old.is_significant = 0) THEN
        
              update issues
              set  is_significant = is_significant + 1
              where  id = :new.issue_id;       
              
        ELSIF (:new.is_significant = 0 AND :old.is_significant = 1) THEN
        
              update issues
              set  is_significant = is_significant - 1
              where  id = :new.issue_id;  
              
        END IF;
        
END;
/
