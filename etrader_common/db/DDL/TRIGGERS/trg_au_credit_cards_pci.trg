create or replace  TRIGGER etrader.trg_au_credit_cards_pci
  AFTER UPDATE OF CC_NUMBER ON etrader.CREDIT_CARDS FOR EACH ROW
BEGIN
  IF pkg_audit.dif(:OLD.CC_NUMBER, :NEW.CC_NUMBER)
  THEN
    delete ENCRYPTION_MIGRATION_C_CARDS where cc_id=:NEW.ID;
  END if;
END;
/