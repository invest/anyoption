create or replace TRIGGER AFTER_BONUS_USERS
AFTER INSERT OR UPDATE ON BONUS_USERS 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW 

declare

cursor email_alert_c is
select ua.user_id, ua.alert_email_subject, ua.alert_email_recepients, u.user_name
from users_alerts ua, users u 
where ua.user_id = :new.user_id
  and :old.time_done is null
  and :new.time_done is not null
  and ua.user_id=u.id  
  and ua.alert_type=1;
  
user_id  number;
alert_email_subject varchar2(100);
alert_email_recepients varchar2(4000);
user_name varchar2(100);

TRIG_EVENT VARCHAR2(10);

BEGIN

IF INSERTING THEN
 TRIG_EVENT := 'INSERT';
 
 INSERT INTO BONUS_USERS_HISTORY (ID, BONUS_USERS_ID, USER_ID, BONUS_STATE_ID, TRIG_EVENT) 
 values (SEQ_BONUS_USERS_HISTORY.NEXTVAL, :NEW.ID, :NEW.USER_ID, :NEW.BONUS_STATE_ID, TRIG_EVENT);

END IF;

IF UPDATING THEN
 TRIG_EVENT := 'UPDATE';
 
 IF (:NEW.BONUS_STATE_ID != :OLD.BONUS_STATE_ID) THEN
   INSERT INTO BONUS_USERS_HISTORY (ID, BONUS_USERS_ID, USER_ID, BONUS_STATE_ID, TRIG_EVENT) 
   values (SEQ_BONUS_USERS_HISTORY.NEXTVAL, :NEW.ID, :NEW.USER_ID, :NEW.BONUS_STATE_ID, TRIG_EVENT);
 END IF;  
 
    open email_alert_c;
    fetch email_alert_c into user_id, alert_email_subject, alert_email_recepients, user_name;
    if email_alert_c%FOUND then
      insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '1', user_name||': '||alert_email_subject, user_name||': '||alert_email_subject||' bonus is done ', alert_email_recepients, 0, null, systimestamp);
    end if;
    close email_alert_c;
END IF;

END;