create or replace TRIGGER BEFORE_BONUS_USERS 
BEFORE INSERT ON BONUS_USERS 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE

--bonus restrict
user_id  number;
user_skin_id number;
user_registration_date DATE;
etrader_bonus_restrict_date varchar2(10) := '';
etrader_bonus_restrict varchar2(5) := '';


cursor user_bonus_restrict is
select 
            u.time_created
            ,u.skin_id
from 
            users u 
where 
            u.id = :new.user_id;

cursor enum_etrader_bonus_date is
select
            e.value 
from 
            enumerators e
where
          e.enumerator = 'etrader_bonus'
          and e.code = 'etrader_bonus_restrict_date' ;
          
          
cursor enum_etrader_bonus_restrict is
select
            e.value 
from 
            enumerators e
where
          e.enumerator = 'etrader_bonus'
          and e.code = 'etrader_bonus_restrict' ;

BEGIN

    --get restrict date 'constant'.
   open enum_etrader_bonus_date;
          fetch enum_etrader_bonus_date into etrader_bonus_restrict_date;
    close enum_etrader_bonus_date;
      
    --get restrict condition 'constant'.
    open enum_etrader_bonus_restrict;
           fetch enum_etrader_bonus_restrict into etrader_bonus_restrict;
    close enum_etrader_bonus_restrict;
    
    --get user details.
     open user_bonus_restrict;
            fetch user_bonus_restrict into user_registration_date, user_skin_id;
      close user_bonus_restrict;  
            
      --restrict condition.
      if ( user_skin_id = 1 
        --        and to_date(etrader_bonus_restrict_date, 'DD/MM/YYYY') <= user_registration_date
                and etrader_bonus_restrict = 'true'
            ) then  
                    raise_application_error(-20100, 'bonus restriction: Terminating Bonuses for etrader. UserId: '||:new.user_id||'; Id: '||:new.id||' ');     
      end if; 

END;
