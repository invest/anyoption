CREATE OR REPLACE TRIGGER AFTER_CLEARING_PROVIDERS
AFTER UPDATE
ON CLEARING_PROVIDERS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
begin
 if :new.is_active = 0 and :new.is_active != :old.is_active then
    DELETE FROM user_to_provider WHERE provider_id = :new.id and distribution_type = :new.distribution_type;
 end if;
end;
/
