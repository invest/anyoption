create or replace TRIGGER trg_biu_users
  BEFORE INSERT OR UPDATE OF MOBILE_PHONE ON USERS FOR EACH ROW
BEGIN
  IF pkg_audit.dif(:OLD.mobile_phone, :NEW.mobile_phone) or pkg_audit.dif(:OLD.country_id, :NEW.country_id)
  THEN
    :NEW.MOBILE_PHONE_VALIDATED := 0 ;
    :NEW.MOBILE_PHONE_VERIFIED := 0 ;
  END if;
END;
/
