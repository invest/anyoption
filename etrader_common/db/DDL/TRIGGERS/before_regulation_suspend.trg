CREATE OR REPLACE TRIGGER BEFORE_REGULATION_SUSPEND
BEFORE INSERT OR UPDATE
ON USERS_REGULATION
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
  
DECLARE
  C_SUSPEND_TRESHOLD_X_LOW CONSTANT numeric := 500000;
  C_SUSPEND_TRESHOLD_Y_HIGH CONSTANT numeric := 2000000;

  v_issue_id number;
  v_comment varchar2(4000);
  v_SUSPEND_TRESHOLD  number :=0;
BEGIN
  IF nvl(:OLD.APPROVED_REGULATION_STEP,-1) != nvl(:NEW.APPROVED_REGULATION_STEP,-1) 
     AND :NEW.APPROVED_REGULATION_STEP = 104 AND :NEW.SCORE_GROUP = 3  THEN
     
         insert into issues (id, user_id, subject_id, priority_id, status_id, is_significant, time_created)
         values (seq_issues.nextval, :NEW.USER_ID, 70, 3, 3, 1, sysdate) returning id into v_issue_id;
        
         v_comment :='User is suspended due to low score:'||:NEW.SCORE;
         insert into issue_actions (id, issue_id, writer_id, action_time, action_time_offset, comments, is_significant, issue_action_type_id, CHANNEL_ID)
         values (seq_issue_actions.nextval, v_issue_id, 0, sysdate, 'GMT+00:00', v_comment, 1, 67, 7);
         
         :NEW.TIME_SUSPENDED_REGULATION := sysdate;
         :NEW.COMMENTS := v_comment;
         :NEW.SUSPENDED_REASON_ID := 10;
     
  END IF;
  
  IF :NEW.APPROVED_REGULATION_STEP = 104 AND nvl(:OLD.THRESHOLD,-1) != nvl(:NEW.THRESHOLD,-1) AND :NEW.SUSPENDED_REASON_ID = 0
    AND ((:NEW.THRESHOLD >= C_SUSPEND_TRESHOLD_X_LOW AND :NEW.SCORE_GROUP = 3) OR (:NEW.THRESHOLD >= C_SUSPEND_TRESHOLD_Y_HIGH AND :NEW.SCORE_GROUP = 2))  THEN
    
         insert into issues (id, user_id, subject_id, priority_id, status_id, is_significant, time_created)
         values (seq_issues.nextval, :NEW.USER_ID, 70, 3, 3, 1, sysdate) returning id into v_issue_id;
         
         if :NEW.SCORE_GROUP = 3 then 
            :NEW.SUSPENDED_REASON_ID :=11;
            v_SUSPEND_TRESHOLD:= C_SUSPEND_TRESHOLD_X_LOW; 
         else 
           :NEW.SUSPENDED_REASON_ID :=12;
           v_SUSPEND_TRESHOLD:= C_SUSPEND_TRESHOLD_Y_HIGH;
         end if; 
         v_comment :=v_SUSPEND_TRESHOLD;
         
         insert into issue_actions (id, issue_id, writer_id, action_time, action_time_offset, comments, is_significant, issue_action_type_id, CHANNEL_ID)
         values (seq_issue_actions.nextval, v_issue_id, 0, sysdate, 'GMT+00:00', ' User is suspended due to '|| v_SUSPEND_TRESHOLD/100 || ' total investments loss', 1, 68, 7);
         
         :NEW.TIME_SUSPENDED_REGULATION := sysdate;
         :NEW.COMMENTS := v_comment;
         
          insert into users_reg_activation_mail
            (id, user_id, suspended_reason_id,  is_send)
          values
            (seq_users_reg_activation_mail.nextval, :new.user_id, :NEW.SUSPENDED_REASON_ID, 0);                   
    
 END IF;   
     
END;
