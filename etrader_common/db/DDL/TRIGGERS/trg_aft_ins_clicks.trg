CREATE OR REPLACE TRIGGER "TRG_AFT_INS_CLICKS" 
AFTER INSERT ON CLICKS 
FOR EACH ROW
DECLARE
        clicks_comb_day_id NUMBER;
        old_clicks_num NUMBER;
BEGIN

        IF (:new.combination_id is not null) THEN

              BEGIN
              
                    select ccd.id, ccd.clicks_num
                    into clicks_comb_day_id, old_clicks_num
                    from clicks_comb_day ccd
                    where ccd.day = trunc(:new.time_created) 
                        and ccd.marketing_combination_id = :new.combination_id
                        and rownum = 1;
            
                EXCEPTION                  
                      WHEN no_data_found THEN
                            clicks_comb_day_id := 0;
                            old_clicks_num := 0;
                END;
                
                IF (clicks_comb_day_id = 0) THEN
                        insert into clicks_comb_day
                        values (SEQ_CLICKS_COMB_DAY.nextval ,trunc(:new.time_created), :new.combination_id, 1);
                ELSE
                        update clicks_comb_day 
                        set clicks_num = clicks_num +1
                        where id = clicks_comb_day_id;
                END IF;
                
         END IF;

END;
/
