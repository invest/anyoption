CREATE OR REPLACE TRIGGER TRG_BEFORE_INS_POP_ENTRIES
  before insert on population_entries
  for each row
declare
  is_cancel_entry_display number;
begin

  -- Find user/contact country id
  begin
      select 1
      into is_cancel_entry_display
      from
           populations p,
           population_users pu
              LEFT JOIN users u on u.id = pu.user_id
              LEFT JOIN contacts c on c.id = pu.contact_id
                   LEFT JOIN countries cn on (u.country_id = cn.id OR (u.country_id is null AND c.country_id = cn.id))
      where pu.id= :new.population_users_id
        and p.id = :new.population_id
        and ((cn.is_display_pop_call_me = 0 and p.population_type_id = 6)
             or
             (cn.is_display_pop_rnd = 0 and p.population_type_id = 5)
             or
             (cn.is_display_pop_short_reg = 0 and p.population_type_id = 20)
             or
             (cn.is_display_pop_reg_attempt = 0 and p.population_type_id = 21));
  exception
    when no_data_found then
      is_cancel_entry_display := 0;
  end;

  -- Enter users from India to group 0
  if is_cancel_entry_display = 1 then
      :new.is_displayed := 0;
  end if;

end TRG_BEFORE_INS_POP_ENTRIES;
/
