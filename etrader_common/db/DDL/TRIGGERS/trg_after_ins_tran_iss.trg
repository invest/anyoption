CREATE OR REPLACE TRIGGER "TRG_AFTER_INS_TRAN_ISS" 
  after insert on transactions_issues  
  for each row
begin

  update population_users pu
  set pu.last_sales_dep_rep_id =  (select ia.writer_id
                                   from issue_actions ia
                                   where ia.id = :new.issue_action_id)
  where pu.user_id = (select t.user_id
                      from transactions t
                      where t.id = :new.transaction_id);

end TRG_AFTER_INS_TRAN_ISS;
/
