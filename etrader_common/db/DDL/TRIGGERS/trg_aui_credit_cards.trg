create or replace trigger trg_aui_credit_cards
  after insert or update on credit_cards
  for each row
begin
  if updating
     and pkg_audit.dif(:old.holder_name, :new.holder_name)     
  then
  
    insert into card_holder_history
      (credit_card_id, cc_number, user_id, holder_name, time_created, operation_id)
    values
      (:new.id, :new.cc_number, :new.user_id, :old.holder_name, sysdate, 1);
      
  elsif inserting  
  then
  
    insert into card_holder_history
      (credit_card_id, cc_number, user_id, holder_name, time_created, operation_id)
    values
      (:new.id, :new.cc_number, :new.user_id, :new.holder_name, sysdate, 2);
      
  end if;
end trg_aui_credit_cards;
/
