CREATE OR REPLACE TRIGGER TRG_AFS_INS_POP_ENTRIES_HIS
  before insert on population_entries_hist  
  for each row
begin
  if (:new.status_id = 1) then
    
    update population_users pu
    set pu.first_his_id = :new.id
    where pu.id = (select pe.population_users_id 
                   from population_entries pe 
                   where pe.id = :new.population_entry_id)
          and pu.first_his_id is null;
           
  end if;
end TRG_AFS_INS_POP_ENTRIES_HIS;
/
