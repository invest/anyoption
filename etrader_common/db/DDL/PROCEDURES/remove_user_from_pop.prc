CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:58 (QP5 v5.149.1003.31008) */
PROCEDURE "REMOVE_USER_FROM_POP" (populationEntryId IN NUMBER)
AS
   v_peh   population_entries_hist%ROWTYPE;
BEGIN
   SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

   v_peh.population_entry_id := populationEntryId;
   v_peh.status_id := 105;
   v_peh.comments := 'Remove to other qualification';
   v_peh.writer_id := 0;
   v_peh.time_created := CURRENT_DATE;

   INSERT INTO population_entries_hist
        VALUES v_peh;
END REMOVE_USER_FROM_POP;
/
