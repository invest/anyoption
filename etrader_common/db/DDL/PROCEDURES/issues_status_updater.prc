CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:55 (QP5 v5.149.1003.31008) */
PROCEDURE "ISSUES_STATUS_UPDATER"
AS
   CURSOR c_to_low
   IS
      SELECT i.*
        FROM ISSUES i, users u
       WHERE     i.user_id = u.id
             AND u.time_last_login + 60 <= SYSDATE
             AND i.TYPE = 2
             AND NOT EXISTS
                    (SELECT issue_id
                       FROM issues_state_change st
                      WHERE st.issue_id = i.id AND st.user_id = u.id);

   CURSOR c_to_normal
   IS
      SELECT st.*
        FROM ISSUES_STATE_CHANGE st, logins l
       WHERE     st.user_id = l.user_id
             AND l.login_time BETWEEN SYSDATE - 30 AND SYSDATE + 30
             AND l.is_success = 1;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('RISK MANAGEMENT BATCH started at: ' || SYSDATE);

   FOR tmp IN c_to_low
   LOOP
      INSERT INTO ISSUES_STATE_CHANGE (issue_id,
                                       priority_id,
                                       status_id,
                                       user_id)
           VALUES (tmp.id,
                   tmp.priority_id,
                   tmp.status_id,
                   tmp.user_id);

      UPDATE issues
         SET priority_id = 1, status_id = 12
       WHERE user_id = tmp.user_id AND id = tmp.id;
   END LOOP;

   FOR tmp IN c_to_normal
   LOOP
      UPDATE issues
         SET priority_id = tmp.priority_id, status_id = tmp.status_id
       WHERE user_id = tmp.user_id AND id = tmp.issue_id;

      DELETE ISSUES_STATE_CHANGE
       WHERE user_id = tmp.user_id AND issue_id = tmp.issue_id;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('RISK MANAGEMENT BATCH finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');

   NULL;
END;
/
