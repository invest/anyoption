create or replace PROCEDURE "TRANSACTIONS_ISSUES_FILL" (p_exec_type IN NUMBER)AS 
  -- p_exec_type Values
  BOTH_TABLES_RUN number := 0;
  -- These are also these parameters ids on DB_PARAMETERS:
  TRANSACTION_ISSUES_RUN number := 1;
  TRANSACTION_ISSUES_S_RUN number := 2;
  start_run_time date;
  last_trans_issues_run_time date := NULL;
  last_trans_issues_s_run_time date := NULL;
  max_deposit_during_call_days number;
  deposit_during_call_days number;
  other_reactions_days number;
  action_id number;
  check_transaction_1 number;
  check_transaction_2 number;
  count_1 number := 0;
  count_2 number := 0;
  last_users_transaction_time date;
  start_check_time date;
  callers_count number;
  is_first_deposit number;
BEGIN 
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------------------------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_FILL started at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
  -- Get the max deposit during call days in order not to miss last deposit during call transactions
  SELECT 
    max(deposit_during_call_days) 
  INTO 
    max_deposit_during_call_days
  FROM 
    skins;

  -- If need to run transaction issues
  IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN 
  -- Get last transaction time from transactions_issues table so that we could go check only transations that we didn't check yet.
    SELECT 
      date_value 
    INTO 
      last_trans_issues_run_time
    FROM 
      db_parameters
    WHERE 
      id = TRANSACTION_ISSUES_RUN; -- last_transactions_issues_run

    DBMS_OUTPUT.PUT_LINE(' transactions_issues last run on: ' || to_char(last_trans_issues_run_time, 'dd-mm-yyyy hh24:mi:ss'));
  ELSE 
    last_trans_issues_run_time := sysdate;
  END IF;

  -- If need to run transaction issues_s
  IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN 
    -- Get last transaction time from transactions_issues_s table so that we could go check only transations that we didn't check yet.
    SELECT 
      date_value 
    INTO 
      last_trans_issues_s_run_time
    FROM 
      db_parameters
    WHERE 
      id = TRANSACTION_ISSUES_S_RUN; -- last_transactions_issues_s_run

    DBMS_OUTPUT.PUT_LINE(' transactions_issues_s last run on: ' || to_char(last_trans_issues_s_run_time, 'dd-mm-yyyy hh24:mi:ss'));
  ELSE 
    last_trans_issues_s_run_time := sysdate;
  END IF;

  IF (last_trans_issues_run_time IS NULL OR last_trans_issues_s_run_time IS NULL) THEN 
    start_run_time := to_date('01/01/1970-00:00','DD/MM/YYYY-HH24:MI');
  ELSIF (last_trans_issues_run_time < last_trans_issues_s_run_time) THEN 
    start_run_time := last_trans_issues_run_time;
  ELSE 
    start_run_time := last_trans_issues_s_run_time;
  END IF;

  DBMS_OUTPUT.PUT_LINE(' start_transaction_time from : ' || to_char(start_run_time, 'dd-mm-yyyy hh24:mi:ss'));
  -- Find all transations since last run or relevant table insert until now minus the deposit_during_call_time check by given skin id
  -- and that are'nt in transactions_issues table
  FOR transaction1 IN
    ( SELECT 
        t.*
      FROM 
        transactions t,
        transaction_types tt
      WHERE 
        -- need to check transactions in the deposit_during_call_days time before start_run_time
        -- and until deposit_during_call_days time before now.
        (t.time_created + max_deposit_during_call_days) BETWEEN start_run_time AND sysdate
        AND t.type_id = tt.id
        AND tt.class_type = 1 -- All deposite
        AND t.status_id IN (2,7,8) -- Succeed Deposits\ Pending and canceled
        -- and not exists (select *
        --                 from transactions_issues
        --                 where transaction_id = t.id)
      ORDER BY 
        time_created) 
  LOOP 
    -- Initialize flags
    check_transaction_1 := 0;
    check_transaction_2 := 0;
    -- Get days range fields by transactions' user skin id
    SELECT 
      s.deposit_during_call_days,
      s.other_reactions_days 
    INTO 
      deposit_during_call_days,
      other_reactions_days
    FROM 
      skins s,
      users u
    WHERE 
      s.id = u.skin_id
      AND transaction1.user_id = u.id;
  
    DBMS_OUTPUT.PUT_LINE('-----');
    DBMS_OUTPUT.PUT_LINE('CHECK, transatction id: ' ||transaction1.id|| ', user _id: ' ||transaction1.user_id|| ', deposit_during_call_days: ' || deposit_during_call_days|| ', other_reactions_days: ' || other_reactions_days);
    -- If need to run transaction issues
    IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN -- Check whether this transaction exists in the TRANSACTION_ISSUES table
      SELECT 
        count(*) 
      INTO 
        check_transaction_1
      FROM 
        transactions_issues ti
      WHERE 
        ti.transaction_id = transaction1.id;
      -- Handle Transaction_Issues Table. Only for retention!
      IF (check_transaction_1 = 0) THEN 
        action_id := 0;
        BEGIN -- Find closest Deposit During Call to the curennt transaction.
          SELECT 
            min(ia.id) 
          INTO 
            action_id
          FROM 
            issues i,
            issue_actions ia,
            writers w,
            issue_action_types iat
          WHERE 
            i.id = ia.issue_id
            AND i.user_id = transaction1.user_id
            AND i.population_entry_id IS NOT NULL -- Only for retention issues
            AND iat.id = ia.issue_action_type_id
            AND iat.deposit_search_type = 1 -- Deposit during call
            AND ia.action_time BETWEEN (transaction1.time_created) AND (transaction1.time_created + deposit_during_call_days)
            AND ia.writer_id = w.id
            AND w.dept_id = 3 -- RETENTION
            AND NOT EXISTS
              ( SELECT 
                  * -- Check that this issue doesn't exist it transactions_issues table
                FROM 
                  transactions_issues ti,
                  issue_actions ia2
                WHERE 
                  ti.issue_action_id = ia2.id
                  AND ia2.issue_id = i.id) ;
          IF (action_id IS NULL) THEN 
            action_id := 0;
          END IF;
        END;

        IF (action_id = 0) THEN 
          -- All other reactions (besides deposit during call) should be checked from the latest date between:
          -- 1. the curren transaction creation time - other_reactions_days
          -- 2. the last transaction of the current user in TRANSACTION_ISSUES table.
          start_check_time := transaction1.time_created - other_reactions_days; -- Get the time of the last transaction in TRANSACTION_ISSUES table of the current user.
          SELECT 
            max(t.time_created) 
          INTO 
            last_users_transaction_time
          FROM transactions_issues ti,
            transactions t
          WHERE 
            ti.transaction_id = t.id
            AND t.user_id = transaction1.user_id
            AND t.time_created < transaction1.time_created;
          
          IF (last_users_transaction_time IS NOT NULL AND last_users_transaction_time > start_check_time) THEN 
            start_check_time := last_users_transaction_time;
          END IF;
          -- Find closest Cooperating  to the curennt transaction.
          SELECT 
            max(ia.id) 
          INTO 
            action_id
          FROM 
            issues i,
            issue_actions ia,
            writers w,
            issue_action_types iat
          WHERE 
            i.id = ia.issue_id
            AND i.user_id = transaction1.user_id
            AND i.population_entry_id IS NOT NULL -- Only for retention issues
            AND iat.id = ia.issue_action_type_id
            AND iat.deposit_search_type = 2 -- Will deposit later, Not interested now and Not Sure
            AND ia.action_time BETWEEN (start_check_time) AND (transaction1.time_created)
            AND ia.writer_id = w.id
            AND w.dept_id = 3 -- RETENTION
            AND NOT EXISTS
              ( SELECT 
                  * -- check that this issue doesn't exist it transactions_issues table
                FROM 
                  transactions_issues ti,
                  issue_actions ia2
                WHERE 
                  ti.issue_action_id = ia2.id
                  AND ia2.issue_id = i.id) ;
          
          IF (action_id IS NULL) THEN 
            action_id := 0;
          END IF;
        END IF; --IF (action_id = 0) THEN
        IF (action_id != 0) THEN 
          SELECT 
            count(distinct(ia.writer_id)) 
          INTO 
            callers_count
          FROM 
            issue_actions ia
          WHERE 
            ia.issue_id = ( SELECT 
                              ia2.issue_id
                            FROM 
                              issue_actions ia2
                            WHERE 
                              ia2.id = action_id);
          is_first_deposit := UTILS.IS_FIRST_DEPOSIT(transaction1.id, transaction1.user_id);
          INSERT INTO transactions_issues(transaction_id, issue_action_id,callers_num, time_inserted, insert_type, transactions_refference_id, is_first_deposit)
          VALUES(transaction1.id, action_id, callers_count, sysdate, 0, transaction1.id, is_first_deposit);
          count_1 := count_1 + 1;
          DBMS_OUTPUT.PUT_LINE('INSERT, transatction id: ' || transaction1.id|| ', action_id: ' || action_id);
        ELSE
          DBMS_OUTPUT.PUT_LINE('Transatction id: '||transaction1.id||' will not insert to TRANSACTIONS_ISSUES table, No found relevant ISSUE to this transaction.');
        END IF; --IF (action_id != 0) THEN
      ELSE
        DBMS_OUTPUT.PUT_LINE('Transatction id: '||transaction1.id||' will not insert to TRANSACTIONS_ISSUES table, Transaction exist on TRANSACTIONS_ISSUES.');
      END IF; -- IF  (check_transaction_1 = 0) THEN
    END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN
    -- If need to run transaction issues_s
    IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN -- Check whether this transaction exists in the TRANSACTION_ISSUES_S table
      SELECT 
        count(*) 
      INTO 
        check_transaction_2
      FROM 
        transactions_issues_s ti
      WHERE 
        ti.transaction_id = transaction1.id;
      
      -- Handle Transaction_Issues_S  Table. Only for Decline after approve population.
      IF (check_transaction_2 = 0) THEN 
        action_id := 0;
        BEGIN 
          -- Find closest Deposit During Call to the curennt transaction.
          SELECT 
            min(ia.id) 
          INTO 
            action_id
          FROM 
            issues i,
            issue_actions ia,
            population_entries pe,
            populations p,
            issue_action_types iat
          WHERE 
            i.id = ia.issue_id
            AND i.user_id = transaction1.user_id
            AND i.population_entry_id = pe.id
            AND pe.population_id = p.id
            AND p.population_type_id = 2 -- Decline after approve          
            AND iat.id = ia.issue_action_type_id
            AND iat.deposit_search_type = 1 -- Deposit during call
            AND ia.action_time BETWEEN (transaction1.time_created) AND (transaction1.time_created + deposit_during_call_days)
            AND NOT EXISTS
              ( SELECT 
                  * -- Check that this issue doesn't exist it transactions_issues table
                FROM 
                  transactions_issues_s ti,
                  issue_actions ia2
                WHERE 
                  ti.issue_action_id = ia2.id
                  AND ia2.issue_id = i.id) ;
          
          IF (action_id IS NULL) THEN 
            action_id := 0;
          END IF;
        END;
        IF (action_id = 0) THEN 
          -- All other reactions (besides deposit during call) should be checked from the latest date between:
          -- 1. the curren transaction creation time - other_reactions_days
          -- 2. the last transaction of the current user in TRANSACTION_ISSUES_S table.
          start_check_time := transaction1.time_created - other_reactions_days; 
          -- Get the time of the last transaction in TRANSACTION_ISSUES_S table of the current user.
          SELECT 
            max(t.time_created) 
          INTO 
            last_users_transaction_time
          FROM 
            transactions_issues_s ti,
            transactions t
          WHERE 
            ti.transaction_id = t.id
            AND t.user_id = transaction1.user_id
            AND t.time_created < transaction1.time_created;
 
          IF (last_users_transaction_time IS NOT NULL AND last_users_transaction_time > start_check_time) THEN 
            start_check_time := last_users_transaction_time;
          END IF;
          -- Find closest Cooperating  to the curennt transaction.
          SELECT 
            max(ia.id) 
          INTO 
            action_id
          FROM 
            issues i,
            issue_actions ia,
            population_entries pe,
            populations p,
            issue_action_types iat
          WHERE 
            i.id = ia.issue_id
            AND i.user_id = transaction1.user_id
            AND i.population_entry_id = pe.id
            AND pe.population_id = p.id
            AND p.population_type_id = 2 -- Decline after approve
            AND iat.id = ia.issue_action_type_id
            AND iat.is_transaction_issues_s_action = 1 -- Cooperating, Will deposit later, Not interested now and Not Sure
            AND ia.action_time BETWEEN start_check_time AND (transaction1.time_created)
            AND NOT EXISTS
              ( SELECT 
                  * -- check that this issue doesn't exist it transactions_issues table
                FROM 
                  transactions_issues_s ti,
                  issue_actions ia2
                WHERE 
                  ti.issue_action_id = ia2.id
                  AND ia2.issue_id = i.id) ;
          
          IF (action_id IS NULL) THEN 
            action_id := 0;
          END IF;
        END IF; --IF (action_id = 0) THEN
        IF (action_id != 0) THEN 
          -- DBMS_OUTPUT.PUT_LINE('insert.... trans_id: ' || transaction1.id);
          -- DBMS_OUTPUT.PUT_LINE('insert.... action_id: ' || action_id);
          SELECT 
            count(distinct(ia.writer_id)) 
          INTO 
            callers_count
          FROM 
            issue_actions ia
          WHERE 
            ia.issue_id = ( SELECT 
                              ia2.issue_id
                            FROM 
                              issue_actions ia2
                            WHERE 
                              ia2.id = action_id);
          INSERT INTO transactions_issues_s(transaction_id, issue_action_id,callers_num)
          VALUES(transaction1.id, action_id, callers_count);
          count_2 := count_2 + 1;
        END IF; --IF (action_id != 0) THEN
      END IF; -- IF  (check_transaction_2 = 0) THEN
    END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
  -- Update last_transactions_issues_run
  IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN
    UPDATE 
      db_parameters
    SET 
      date_value = sysdate
    WHERE 
      id = TRANSACTION_ISSUES_RUN; -- last_transactions_issues_run
    
    DBMS_OUTPUT.PUT_LINE(count_1 || ' New Records in transactions_issues');
  END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN
  -- Update last_transactions_issues_s_run
  IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN
    UPDATE 
      db_parameters
    SET 
      date_value = sysdate
    WHERE 
      id = TRANSACTION_ISSUES_S_RUN; -- last_transactions_issues_s_run
      
    DBMS_OUTPUT.PUT_LINE(count_2 || ' New Records in transactions_issues_s');
  END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN
  DBMS_OUTPUT.PUT_LINE('-----');
  DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_FILL ended at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
  NULL;
END TRANSACTIONS_ISSUES_FILL;