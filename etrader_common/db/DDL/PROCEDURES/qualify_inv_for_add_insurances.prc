create or replace procedure qualify_inv_for_add_insurances
(
  i_start_minutes in varchar2
 ,i_end_minutes   in varchar2
) is
  l_interval_start interval day to second := numtodsinterval(to_number(i_start_minutes), 'MINUTE');
  l_interval_end   interval day to second := numtodsinterval(to_number(i_end_minutes), 'MINUTE');
  l_sysdate        date := sysdate;
begin
  -- process Golden Minutes investments
  -- investment has to have reference_inv_id which has to have reference_inv_id which has to not have reference_inv_id
  update investments
  set    insurance_flag_add = 2
  where  id in (select a.id
                from   investments a, opportunities b, markets c
                where  a.copyop_type_id <> 1 -- exclude copied investments
                and    get_opp_roll_up(b.id) != 0
                and    a.opportunity_id = b.id
                and    b.is_disabled = 0
                and    b.market_id = c.id
                and    c.is_roll_up = 1
                and    b.scheduled = 1
                and    a.is_settled = 0
                and    a.bonus_odds_change_type_id = 0
                and    b.opportunity_type_id = 1
                and    a.insurance_amount_ru + ((a.amount - a.insurance_amount_ru) * c.roll_up_premia_percent) <
                       (a.amount - a.insurance_amount_ru) * a.odds_win
                and    a.time_created <= b.time_est_closing - to_dsinterval('0 00:' || i_start_minutes || ':00')
                and    systimestamp >= b.time_est_closing - to_dsinterval('0 00:' || i_start_minutes || ':00')
                and    systimestamp <= b.time_est_closing - to_dsinterval('0 00:' || i_end_minutes || ':00')
                and    not exists (select 1 from golden_minutes_viewing v where v.investment_id = a.id)
                and    not exists (select 1
                        from   investments aa
                        where  aa.id = a.reference_investment_id
                        and    exists (select 1 from investments ab where ab.id = aa.reference_investment_id))
                and    extract(minute from(b.time_est_closing - b.time_last_invest)) = 5
                      
                and    case
                        when a.type_id = 1 then
                         b.gm_level - a.current_level
                        else
                         a.current_level - b.gm_level
                      end / a.current_level < -c.roll_up_qualify_percent);
  update investments
  set    insurance_flag_add = 0
  where  id in (select a.id
                from   investments a, opportunities b, markets c
                where  a.copyop_type_id <> 1 -- exclude copied investments
                      --                           AND GET_OPP_ROLL_UP (B.id) != 0
                and    a.opportunity_id = b.id
                      --                           AND B.is_disabled = 0
                and    b.market_id = c.id
                      --                           AND C.is_roll_up = 1
                and    b.scheduled = 1
                and    a.is_settled = 0
                and    a.bonus_odds_change_type_id = 0
                and    b.opportunity_type_id = 1
                and    a.time_created <= b.time_est_closing - to_dsinterval('0 00:' || i_start_minutes || ':00')
                and    systimestamp >= b.time_est_closing - to_dsinterval('0 00:' || i_start_minutes || ':00')
                and    systimestamp <= b.time_est_closing - to_dsinterval('0 00:' || i_end_minutes || ':00')
                and    a.insurance_flag_add is null);
end qualify_inv_for_add_insurances;
/
