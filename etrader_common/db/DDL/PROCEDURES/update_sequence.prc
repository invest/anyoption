-- create PROCEDURE UPDATE_SEQUENCE
create or replace PROCEDURE UPDATE_SEQUENCE (tableName IN VARCHAR2, sequenceName IN VARCHAR2) AS
  incrementSeq NUMBER := 0;
  temp_seq NUMBER;
  queryStr VARCHAR2(4000);
BEGIN
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('Update ' || sequenceName || ' at: ' || sysdate);
  
  -- Get number to increment SEQUENCE
  queryStr := 'SELECT 
                (maxId - ' || sequenceName || '.NEXTVAL)
              FROM 
                (SELECT 
                  MAX(tab.id) as maxId 
                FROM '
                  || tableName || ' tab)';
  EXECUTE IMMEDIATE queryStr INTO incrementSeq;
       
  -- update SEQUENCE
  DBMS_OUTPUT.PUT_LINE('about to increment ' || sequenceName || ', incrementSeq:' || incrementSeq); 
  EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || sequenceName || ' INCREMENT BY ' || incrementSeq;
  EXECUTE IMMEDIATE 'SELECT ' || sequenceName || '.NEXTVAL FROM dual' INTO temp_seq;
  EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || sequenceName || ' INCREMENT BY 1';
  
  DBMS_OUTPUT.PUT_LINE('End to update ' || sequenceName || ' at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
END;