CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:59 (QP5 v5.149.1003.31008) */
PROCEDURE "SPECIAL_HANDLER"
AS
   v_peh              population_entries_hist%ROWTYPE;
   assignedWriterId   NUMBER;
BEGIN
   FOR v_pop_usr
      IN (SELECT pu.id, pu.curr_population_entry_id
            FROM population_users pu, population_entries pe, populations p
           WHERE     pu.entry_type_id = 1 -- not users that was removed from sales
                 AND pu.curr_population_entry_id = pe.id
                 AND pe.population_id = p.id
                 AND p.population_type_id IN (11, 28)    -- Special popualtion
                 AND pu.curr_assigned_writer_id IS NULL)
   LOOP
      -- Insert a remove record to  population_entry_history
      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

      v_peh.population_entry_id := v_pop_usr.curr_population_entry_id;
      v_peh.status_id := 114;                           -- remove from special
      v_peh.writer_id := 0;
      v_peh.assigned_writer_id := NULL;
      v_peh.time_created := SYSDATE;
      v_peh.issue_action_id := NULL;

      INSERT INTO population_entries_hist
           VALUES v_peh;

      -- Update user entry in population_users table
      UPDATE population_users pu
         SET pu.curr_population_entry_id = NULL
       WHERE pu.id = v_pop_usr.id;

      COMMIT;
   END LOOP;

   NULL;
END SPECIAL_HANDLER;
/
