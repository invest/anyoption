CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:54 (QP5 v5.149.1003.31008) */
PROCEDURE DORMENT_CONVERSION_HANDLER
AS
   POP_DORMENT_CONVERSION_ID   NUMBER := 24;
   POP_RETENTION_ID            NUMBER := 23;
   populationEntryId           NUMBER := 0;
   populationId                NUMBER := 0;
   populationUserId            NUMBER := 0;
   populationEntryHistId       NUMBER := 0;

   users_out_of_dorment        NUMBER := 0;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.
    PUT_LINE ('DORMENT_CONVERSION_HANDLER started at: ' || SYSDATE);

   FOR v_user
      IN (SELECT u.id AS user_id,
                 u.skin_id AS user_skin_id,
                 u.balance AS user_balance,
                 ilgc.VALUE AS limit_value,
                 pu.id AS population_user_id,
                 pe.qualification_time AS entry_qualification_time
            FROM population_users pu,
                 population_entries pe,
                 populations p,
                 users u,
                 investment_limit_group_curr ilgc
           WHERE     pu.user_id = u.id
                 AND pu.curr_population_entry_id = pe.id
                 AND pe.population_id = p.id
                 AND p.population_type_id = POP_DORMENT_CONVERSION_ID
                 AND ilgc.currency_id = u.currency_id
                 AND pu.entry_type_id <> 5
                 AND ilgc.investment_limit_group_id = 1)
   LOOP
      IF (v_user.user_balance < v_user.limit_value
          OR v_user.entry_qualification_time < SYSDATE - 7)
      THEN
         SELECT p.id
           INTO populationId
           FROM populations p, population_types pt
          WHERE     pt.id = p.population_type_id
                AND p.skin_id = v_user.user_skin_id
                AND p.population_type_id = POP_RETENTION_ID;

         SELECT pu.id
           INTO populationUserId
           FROM population_users pu
          WHERE pu.user_id = v_user.user_id;

         IF (populationUserId > 0 AND populationId > 0)
         THEN
            SELECT SEQ_POPULATION_ENTRIES.NEXTVAL
              INTO populationEntryId
              FROM DUAL;

            INSERT INTO population_entries (id,
                                            population_id,
                                            GROUP_ID,
                                            qualification_time,
                                            population_users_id,
                                            is_displayed,
                                            base_qualification_time)
                 VALUES (populationEntryId,
                         populationId,
                         1,
                         SYSDATE,
                         populationUserId,
                         1,
                         SYSDATE);

            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO populationEntryHistId
              FROM DUAL;

            INSERT INTO population_entries_hist (id,
                                                 population_entry_id,
                                                 status_id,
                                                 issue_action_id,
                                                 writer_id,
                                                 time_created,
                                                 assigned_writer_id)
                 VALUES (populationEntryHistId,
                         populationEntryId,
                         1,
                         NULL,
                         0,
                         SYSDATE,
                         0);

            UPDATE population_users pu
               SET entry_type_id = 1,
                   curr_population_entry_id = populationEntryId,
                   curr_assigned_writer_id = NULL
             WHERE pu.id = populationUserId;

            users_out_of_dorment := users_out_of_dorment + 1;
         END IF;
      END IF;
   END LOOP;

   DBMS_OUTPUT.
    PUT_LINE ('DORMENT_CONVERSION_HANDLER finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('users_out_of_dorment: ' || users_out_of_dorment);
   NULL;
END DORMENT_CONVERSION_HANDLER;
/
