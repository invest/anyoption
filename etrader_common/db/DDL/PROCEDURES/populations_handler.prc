create or replace PROCEDURE         "POPULATIONS_HANDLER" (runType NUMBER) AS

  -- Run Types
  RUN_TYPE_ALL_USERS NUMBER := 1;
  RUN_TYPE_NEW_USERS NUMBER := 2;
  RUN_TYPE_ONLY_CONTACTS NUMBER := 3;

	v_pe population_entries%ROWTYPE;
  v_pu population_users%ROWTYPE;
	v_peh population_entries_hist%ROWTYPE;
  populationEntHist population_entries_hist%ROWTYPE;

  populationEntryId NUMBER;
  qualifiedPopualtionId NUMBER;
  currentPopulationId NUMBER;
  currentPopulationPriority NUMBER;
  is_exist_pop NUMBER;
  isAssignedInThePastXDays NUMBER;
  isExistDormentConvertionPop NUMBER;

  popQualificationTime DATE;
  popBaseQualificationTime DATE;
  popBaseQualificationTimeOld DATE;
  tranTime DATE;

  newPopualtionThreshold NUMBER;
  qualificationStatus NUMBER;
  assignedWriterId NUMBER;

  users_popualtion_users_id NUMBER;
  lastSalesDepositTime DATE;

  v_counter NUMBER;
  isExist NUMBER;
  isExistSuccessfullDeposit NUMBER;
  isExistTranIssue NUMBER;
  isExistOpenWithdraw NUMBER;
  isFirstMinimumBalanceCheck NUMBER;
  userNumOfSuccesDeposits NUMBER;
  userAmountOfSuccesDeposits NUMBER;
  isCheckQualificationTime NUMBER;

  count_new_qualified_users number := 0;
  count_new_qualified_contacts number := 0;
  runTypeName varchar2(30);
  SEQ_ID NUMBER;
  W_SEQ_ID NUMBER;
BEGIN
  
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER started at: ' || sysdate);

  CASE
    WHEN runType = RUN_TYPE_ALL_USERS THEN
      runTypeName := 'Run Type All Users';
    WHEN runType = RUN_TYPE_NEW_USERS THEN
      runTypeName := 'Run Type New Users';
    WHEN runType = RUN_TYPE_ONLY_CONTACTS THEN
      runTypeName := 'Run Type Only Contacts';
    ELSE
      DBMS_OUTPUT.PUT_LINE('run type: ' || runType || ' doesnt exist');
  END CASE;
  
  W_SEQ_ID := null;
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO W_SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (W_SEQ_ID,1,runTypeName,SYSDATE,null,'POPULATIONS_HANDLER'); 
  
  DBMS_OUTPUT.PUT_LINE('run type: ' || runTypeName);

  IF (runType != RUN_TYPE_ONLY_CONTACTS) THEN
    -- RUN MARKETING_HANDLER
    marketing_handler();

    FOR v_usr IN (
      SELECT
        u.id user_id,
        u.skin_id,
        u.balance,
        u.time_created,
        u.currency_id,
        u.is_declined,
        u.is_contact_for_daa,
        u.last_decline_date,
        u.time_last_login,
        u.contact_id,
        u.time_sc_house_result ,
        u.first_deposit_id ,
		u.affiliate_key,
        u.writer_id,
        p.id population_id,
        p.population_type_id curr_pop_type_id,
        pu.id population_user_id,
        pu.curr_population_entry_id,
        pu.curr_assigned_writer_id,
        pu.entry_type_id,
        pu.entry_type_action_id,
        pt.priority,
        ilgc.value min_investment_limit,
        s.other_reactions_days
      FROM
        investment_limits il,
        investment_limit_group_curr ilgc,
        skins s,
        users u LEFT JOIN population_users pu on u.id = pu.user_id
                        LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                            LEFT JOIN populations p on pe.population_id = p.id
                                  LEFT JOIN population_types pt on p.population_type_id = pt.id
      WHERE
        u.currency_id = ilgc.currency_id
        AND il.min_limit_group_id = ilgc.investment_limit_group_id
        AND il.id = 1 -- Default binary limits id
        AND u.skin_id = s.id
        AND u.class_id !=0
        AND u.is_active = 1
        AND u.is_false_account = 0
        AND u.is_contact_by_phone = 1
        AND u.id NOT in (select USER_ID from frauds)
        AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
        AND pu.lock_history_id is null
        AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
        AND u.is_vip = 0
        --AND u.time_sc_house_result is null
        AND (runType = RUN_TYPE_ALL_USERS or u.time_created > sysdate - 1)
        --and u.id =134541

    ) LOOP

        -- DBMS_OUTPUT.PUT_LINE('user12 : ' || v_usr.);
          v_peh := null;
          populationEntryId := 0;
          qualifiedPopualtionId := 0;
          newPopualtionThreshold := 0;
          isFirstMinimumBalanceCheck := 1;
          assignedWriterId := v_usr.curr_assigned_writer_id;
          users_popualtion_users_id := v_usr.population_user_id ;
          lastSalesDepositTime := null;

          IF (v_usr.population_id is null) THEN
                currentPopulationId := 0;
                currentPopulationPriority := 999999; -- Lowest proirity
          ELSE
                currentPopulationId := v_usr.population_id;
                currentPopulationPriority := v_usr.priority;
          END IF;

          FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable,
                                    pt.priority
                              FROM
                                    populations p,

                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND  (pt.priority < currentPopulationPriority OR currentPopulationPriority =10) -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_usr.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = runType
                              ORDER BY pt.priority -- Highest priority is 1
          ) LOOP

              populationEntHist := null;
              popQualificationTime := null;
              popBaseQualificationTime := null;
              isCheckQualificationTime := 0;

              CASE
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 1 THEN --POPULATION_FIRST_DEPO_FAILED

                         IF (v_usr.is_declined = 1) THEN

                              -- Check whether this user had some successed deposits in the past
                              select
                                 max(t.id)
                              into
                                 isExistSuccessfullDeposit
                              from
                                 transactions t,
                                 transaction_types tt
                              where
                                 tt.class_type = 1 AND
                                 user_id = v_usr.user_id AND
                                 type_id = tt.id AND
                                 status_id in (2,7,8);

                              IF (isExistSuccessfullDeposit is null) THEN

                                    popBaseQualificationTime  := v_usr.last_decline_date;
                                    popQualificationTime := popBaseQualificationTime;

                                     isCheckQualificationTime := 1;

                              END IF;
                           END IF; -- IF (v_usr.is_declined = 1) THEN

                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 2 THEN -- POPULATION_LAST_DEPO_FAILED

                        -- If users doesn't want to ne called for decline after approve we don't insert him into DDA population.
                        IF (v_usr.is_contact_for_daa != 0 AND v_usr.is_declined = 1) THEN

                              popBaseQualificationTime  := v_usr.last_decline_date;
                              popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                              IF (sysdate  >= popQualificationTime) THEN

                                        -- Check whether this user had some successed deposits in the past
                                        IF (isExistSuccessfullDeposit is not null) THEN

                                               isCheckQualificationTime := 1;

                                        END IF; -- IF (isExistSuccessfullDeposit is not null) THEN
                              END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 5 THEN -- POPULATION_NO_DEPOSIT_FILL

                        popBaseQualificationTime := v_usr.time_created;
                        popQualificationTime := popBaseQualificationTime + POPULATION.GET_NUM_MINUTES(v_population.population_type_id, v_usr.affiliate_key, v_usr.writer_id, v_population.number_of_minutes)/1440 ;

                        IF (sysdate  >= popQualificationTime) THEN

                              select
                                max(t.id)
                              into
                                isExist
                              from
                                transactions t,
                                transaction_types tt
                              where
                                tt.class_type = 1 AND
                                user_id = v_usr.user_id AND
                                type_id = tt.id;

                                IF (isExist is null) THEN
                                       isCheckQualificationTime := 1;

                                END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 9 THEN -- POPULATION_DORMANT_ACCOUNT

                        IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                                -- IF we didn't find a sales deposit for that user before, search it.
                                IF (lastSalesDepositTime is null) THEN

                                      select
                                            max(t.time_created)
                                       into
                                            lastSalesDepositTime
                                       from
                                            transactions t,
                                            transactions_issues ti
                                      where
                                            t.id = ti.transaction_id
                                            and t.user_id = v_usr.user_id;
                                END IF; --  IF (lastSalesDepositTime is null) THEN

                                -- If user had no sales deposit in the past or v_population.LAST_SALES_DEPOSIT_DAYS has passed since last sales deposit time... go on...
                                IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN

                                      popBaseQualificationTime := GET_LAST_INVEST_TIME(v_usr.user_id);

                                      IF (popBaseQualificationTime is not null) THEN

                                           -- If v_population.number_of_days has passed since last user login time
                                           -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                                           IF (v_usr.time_last_login < sysdate - v_population.last_invest_day
                                                      OR
                                                 popBaseQualificationTime < sysdate - v_population.max_last_login_time_days) THEN

                                                    popQualificationTime := popBaseQualificationTime + v_population.last_invest_day;

                                                    IF (sysdate  >= popQualificationTime) THEN

                                                           -- Check if user was above min balance during the all threshold time and didn't had a balance increase
                                                           IF (get_is_dormant_above_balance(v_usr.user_id,
                                                                                                                  v_population.last_invest_day,
                                                                                                                  v_usr.min_investment_limit,
                                                                                                                  v_usr.balance) = 1) THEN

                                                                 isCheckQualificationTime := 1;
                                                            END IF;

                                                    END IF; -- IF (sysdate  >= popQualificationTime) THEN
                                              END IF; -- IF (v_usr.time_last_login < sysdate - v_population.number_of_days...
                                      END IF; -- IF (popBaseQualificationTime is not null) THEN
                                END IF; -- IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN
                        END IF; -- IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 10 THEN -- POPULATION_NO_INVESTMENTS

                          popBaseQualificationTime := GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.user_id);

                          IF (popBaseQualificationTime is not null) THEN

                                popQualificationTime := popBaseQualificationTime + v_population.number_of_days;

                                IF(sysdate  >  popQualificationTime) THEN

                                      select   max(i.id)
                                      into      isExist
                                      from     investments i
                                      where   v_usr.user_id = i.user_id;

                                      IF (isExist is null) THEN

                                            isCheckQualificationTime := 1;

                                      END IF;
                                END IF;
                          END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 22 THEN -- POPULATION OPEN WITHDRAW
                              SELECT count(t.id) into isExistOpenWithdraw
                               FROM transactions t, transaction_types tt
                               WHERE t.user_id = v_usr.user_id
                              AND tt.id = t.type_id
                              AND tt.class_type = 2
                              AND t.status_id = 4;

                              IF (isExistOpenWithdraw > 0) THEN
                                      isCheckQualificationTime := 1;
                                      popQualificationTime := sysdate;
                               END IF;
                     --------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 24 THEN -- POPULATION_DORMENT_CONVERSION

                              IF (v_usr.first_deposit_id is not null AND v_usr.first_deposit_id > 0) THEN

                                      select  count(ti.transaction_id) into isExistTranIssue
                                      from transactions_issues ti
                                      where ti.transaction_id = v_usr.first_deposit_id
                                      and ti.time_settled is null;

                                      if (isExistTranIssue > 0) THEN
                                      
                                              if (v_usr.curr_assigned_writer_id is null) THEN
                                                      select  t.time_created into tranTime
                                                      from transactions t
                                                      where t.id = v_usr.first_deposit_id;

                                                      if (tranTime + 7 < sysdate) THEN
                                                      
                                                              select count(peh.ID) into isAssignedInThePastXDays
                                                              from population_users pu, population_entries pe, population_entries_hist peh, populations p, population_entries_hist_status pehs
                                                              where pu.id = pe.population_users_id
                                                              and pe.id = peh.population_entry_id
                                                              and pu.user_id = v_usr.user_id
                                                              and p.id = pe.population_id
                                                              and pehs.id= peh.status_id
                                                              and peh.status_id = 2
                                                              and peh.time_created > tranTime; -- not assigned in the past X days, X = 7

                                                              if (isAssignedInThePastXDays  = 0 ) THEN
                                                              
                                                                      select count(peh.ID) into isExistDormentConvertionPop
                                                                      from population_users pu, population_entries pe, population_entries_hist peh, populations p, population_entries_hist_status pehs
                                                                      where pu.id = pe.population_users_id
                                                                      and pe.id = peh.population_entry_id
                                                                      and pu.user_id = v_usr.user_id
                                                                      and p.id = pe.population_id
                                                                      and pehs.id= peh.status_id
                                                                      and p.population_type_id = 24;

                                                                      if (isExistDormentConvertionPop = 0) THEN
                                                                      
                                                                              isCheckQualificationTime := 1;
                                                                              popQualificationTime := sysdate;
                                                                      END IF;
                                                              END IF;
                                                     END IF;
                                              END IF;
                                      END IF;
                              END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    ELSE  --IF  v_population.population_type_id wasn't found
                        -- No handler for this population type
                      DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
                END CASE;

                -- IF isCheckQualificationTime = 1, User is qualified to that populaiton, now check his qualification time
                IF (isCheckQualificationTime = 1) THEN

                      populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                      IF (populationEntHist.status_id is null
                                 OR
                            popBaseQualificationTime >= populationEntHist.TIME_CREATED
                                 OR
                           (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                                OR
                           (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                          -- Find qualification time of this population last entry
                          BEGIN
                              select base_qualification_time
                              into popBaseQualificationTimeOld
                              from population_entries pe
                              where pe.id = populationEntHist.population_entry_id
                                  and qualification_time is not null ;
                        EXCEPTION
                              WHEN no_data_found THEN
                                      popBaseQualificationTimeOld := null;
                           END;

                          -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                          IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                                 populationEntryId :=  populationEntHist.population_entry_id;
                          END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                          -- Update user entry with this population id.
                          qualifiedPopualtionId := v_population.id;

                           -- Keep current popualtion threshold
                          newPopualtionThreshold := v_population.threshold;

                           -- If user was finally found qualified for current popualtion, break the loop
                          GOTO end_loop;

                      END IF; -- IF (populationEntHist.status_id is null ....
                END IF; --   IF (isCheckQualificationTime = 1) THEN

          END LOOP;
           <<end_loop>>

          -- If got a newPopualtionId, insert user to new population.
          IF (qualifiedPopualtionId != 0) THEN
                count_new_qualified_users := count_new_qualified_users +1;

                -- If this  is a contact.
                IF (v_usr.user_id = 0) THEN
                   assignedWriterId := null;
                END IF;

                -- If user is already in a population, remove it first.
                IF (v_usr.curr_population_entry_id is not null) THEN
                      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                      v_peh.population_entry_id := v_usr.curr_population_entry_id;
                      v_peh.status_id := 105;
                      v_peh.writer_id := 0;
                      v_peh.assigned_writer_id := assignedWriterId;
                      v_peh.time_created := sysdate;
                      v_peh.issue_action_id := null;
                      INSERT INTO population_entries_hist VALUES v_peh;
                END IF; -- IF (v_usr.curr_population_entry_id is not null) THEN

                 -- If user doesn't have an entry in population_users, create one.
                IF (users_popualtion_users_id is null) THEN
                      SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                      v_pu.id := users_popualtion_users_id;
                      v_pu.user_id := v_usr.user_id;
                      if (v_usr.contact_id != 0) then
                        v_pu.contact_id := v_usr.contact_id;
                      else
                        v_pu.contact_id := null;
                      end if;
                      v_pu.curr_assigned_writer_id := null;
                      v_pu.curr_population_entry_id := null;
                      v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                      v_pu.entry_type_action_id := null;
                      v_pu.lock_history_id := null;
                      v_pu.time_created := sysdate;

                      INSERT INTO population_users VALUES v_pu;
                END IF; --  IF (users_popualtion_users_id is null) THEN

                -- If population entry id hasn't been changed create a new one.
                IF (populationEntryId = 0) THEN
                      -- Count the number of users in the new population in order to determine user's entry group.
                      SELECT
                              COUNT(pe.id) INTO v_counter
                      FROM
                              population_entries pe,
                              population_users pu
                      WHERE
                              pe.id = pu.curr_population_entry_id AND
                              pe.population_id = qualifiedPopualtionId;

                      -- Insert a new population_entry
                      SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                      v_pe.id := populationEntryId;
                      v_pe.population_id := qualifiedPopualtionId;
                      v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                      v_pe.population_users_id := users_popualtion_users_id;
                      v_pe.qualification_time := popQualificationTime ;
                      v_pe.base_qualification_time := popBaseQualificationTime ;
                      v_pe.is_displayed := 1;
                      INSERT INTO population_entries VALUES v_pe;

                      qualificationStatus := 1; -- Qualification
                ELSE
                      qualificationStatus := 7; -- reQualification
                END IF; --  IF (populationEntryId = 0) THEN


                 -- Insert a new population_entry_history
                SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                v_peh.population_entry_id :=populationEntryId;
                v_peh.status_id := qualificationStatus;
                v_peh.writer_id := 0;
                v_peh.assigned_writer_id := assignedWriterId;
                v_peh.time_created := sysdate;
                v_peh.issue_action_id := null;
                INSERT INTO population_entries_hist VALUES v_peh;

                -- Update user entry in population_users table
                UPDATE
                      population_users pu
                SET
                      pu.curr_population_entry_id = populationEntryId,
                      pu.curr_assigned_writer_id = assignedWriterId
                WHERE
                     pu.user_id = v_usr.user_id;

          END IF; -- IF (qualifiedPopualtionId != 0) THEN

         COMMIT;

    END LOOP;

    if (runType = RUN_TYPE_ALL_USERS) then
          update db_parameters dp
          set date_value = sysdate
          where dp.id = 9;
    end if;

    -- RUN DELAY HANDLER
    delay_handler();

    -- RUN TRACKING HANDLER
    tracking_handler();

      -- RUN SPECIAL HANDLER
    special_handler();

    -- RUN CALLBACK HANDLER
    callback_handler();

 IF (runType = RUN_TYPE_ALL_USERS) THEN
     -- RUN DORMENT CONVERSION HANDLER
 
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (SEQ_ID,1,'ALL USERS: DORMENT CONVERSION HANDLER STAGE.',SYSDATE,null,'POPULATIONS_HANDLER');

    dorment_conversion_handler();
    
UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = SEQ_ID;
    -- RUN USERS STATUS HANDLER
    
         SEQ_ID := null;
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (SEQ_ID,2,'ALL USERS: USERS STATUS HANDLER STAGE.',SYSDATE,null,'POPULATIONS_HANDLER');
  
    users_status_handler();
    
UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = SEQ_ID;

    -- RUN USERS RANK HANDLER
    
     SEQ_ID := null;
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (SEQ_ID,3,'ALL USERS: USERS RANK HANDLER STAGE.',SYSDATE,null,'POPULATIONS_HANDLER');
  
    users_rank_handler();
    
UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = SEQ_ID;
    --RUN RETURN FROM TRAKING HANDLER
    
        SEQ_ID := null;
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (SEQ_ID,4,'ALL USERS: RETURN FROM TRAKING HANDLER STAGE.',SYSDATE,null,'POPULATIONS_HANDLER');
   
    return_from_tracking();
    
UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = SEQ_ID;

    --RUN MANUALLY IMPORTED CON HANDLER
        SEQ_ID := null;
 select  SEQ_CONTROL_LOGS.NEXTVAL INTO SEQ_ID from dual;

  INSERT INTO CONTROL_LOGS (
  ID,
  STEP_ID,
  STEP_NAME,
  BEGIN_TIME,
  END_TIME,
  UNIT_NAME) VALUES (SEQ_ID,5,'ALL USERS: MANUALLY IMPORTED CON HANDLER STAGE.',SYSDATE,null,'POPULATIONS_HANDLER'); 
     
    manually_imported_con_handler();
    
   UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = SEQ_ID; 
END IF;
    DBMS_OUTPUT.PUT_LINE('count_new_qualified_users  = ' || count_new_qualified_users);

  END IF;

  FOR v_con IN (
    SELECT
      c.id contact_id,
      c.skin_id,
      c.time_created,
      c.type,
	  c.affiliate_key,
      c.writer_id,
      p.id population_id,
      p.population_type_id curr_pop_type_id,
      pu.id population_user_id,
      pu.curr_population_entry_id,
      pu.curr_assigned_writer_id,
      pu.entry_type_id,
      pu.entry_type_action_id,
      pt.priority,
      s.other_reactions_days
    FROM
      skins s,
      contacts c
          LEFT JOIN population_users pu on c.id = pu.contact_id
              LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                  LEFT JOIN populations p on pe.population_id = p.id
                        LEFT JOIN population_types pt on p.population_type_id = pt.id
    WHERE
      c.skin_id = s.id
      AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
      AND pu.lock_history_id is null
      AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
      AND c.type in (5, 6, 7) -- Short Reg Form contacts and register attempt
      AND (c.land_line_phone is not null OR c.mobile_phone is not null)
      AND c.user_id = 0

  ) LOOP

        v_peh := null;
        populationEntryId := 0;
        qualifiedPopualtionId := 0;
        newPopualtionThreshold := 0;
        assignedWriterId := v_con.curr_assigned_writer_id;
        users_popualtion_users_id := v_con.population_user_id ;

        IF (v_con.population_id is null) THEN
              currentPopulationId := 0;
              currentPopulationPriority := 999999; -- Lowest proirity
        ELSE
              currentPopulationId := v_con.population_id;
              currentPopulationPriority := v_con.priority;
        END IF;

        FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable
                              FROM
                                    populations p,
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND pt.priority < currentPopulationPriority -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_con.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = RUN_TYPE_ONLY_CONTACTS
                              ORDER BY pt.priority -- Highest priority is 1
        ) LOOP

            populationEntHist := null;
            popQualificationTime := null;
            popBaseQualificationTime := null;
            isCheckQualificationTime := 0;

            CASE
                ------------------------------------------------------------------------------------------------------------------------------------------------
                WHEN v_population.population_type_id in (20, 21, 27) THEN --POPULATION_SHORT_REG_FORM AND POPLULATION_REGISTER_ATTEMPT

                    popBaseQualificationTime := v_con.time_created;

                    popQualificationTime := popBaseQualificationTime + POPULATION.GET_NUM_MINUTES(v_population.population_type_id, v_con.affiliate_key, v_con.writer_id, v_population.number_of_minutes)/1440 ;

                    if (popQualificationTime < sysdate) then

                       isCheckQualificationTime := 1;
                    end if;
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  ELSE  --IF  v_population.population_type_id wasn't found
                      -- No handler for this population type
                    DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
             END CASE;

              -- IF isCheckQualificationTime = 1, contact is qualified to that populaiton, now check his qualification time
              IF (isCheckQualificationTime = 1) THEN

                    populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                    IF (populationEntHist.status_id is null
                               OR
                          popBaseQualificationTime >= populationEntHist.TIME_CREATED
                               OR
                         (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                               OR
                         (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                        -- Find qualification time of this population last entry
                        BEGIN
                            select base_qualification_time
                            into popBaseQualificationTimeOld
                            from population_entries pe
                            where pe.id = populationEntHist.population_entry_id
                                and qualification_time is not null ;
                        EXCEPTION
                            WHEN no_data_found THEN
                                    popBaseQualificationTimeOld := null;
                        END;

                        -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                        IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                               populationEntryId :=  populationEntHist.population_entry_id;
                        END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                        if( ( v_con.type = 5 AND v_population.population_type_id = 20 ) OR   ( v_con.type = 6  AND v_population.population_type_id = 21 ) OR (v_con.type = 7  AND v_population.population_type_id = 27)) then
                        -- Update contact entry with this population id.
                              qualifiedPopualtionId := v_population.id;
                              -- Keep current popualtion threshold
                              newPopualtionThreshold := v_population.threshold;
                              -- If contact was finally found qualified for current popualtion, break the loop
                              GOTO end_loop;
                        end if;
                    END IF; -- IF (populationEntHist.status_id is null ....
              END IF; --   IF (isCheckQualificationTime = 1) THEN

        END LOOP;
         <<end_loop>>

        -- If got a newPopualtionId, insert contact to new population.
        IF (qualifiedPopualtionId != 0) THEN

              count_new_qualified_contacts := count_new_qualified_contacts +1;

              -- If this contact is in general entry_type preform auto cancel assign.
              IF (v_con.entry_type_id = 1) THEN
                 assignedWriterId := null;
              END IF;

              -- If contact is already in a population, remove it first.
              IF (v_con.curr_population_entry_id is not null) THEN
                    SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                    v_peh.population_entry_id := v_con.curr_population_entry_id;
                    v_peh.status_id := 105;
                    v_peh.writer_id := 0;
                    v_peh.assigned_writer_id := assignedWriterId;
                    v_peh.time_created := sysdate;
                    v_peh.issue_action_id := null;
                    INSERT INTO population_entries_hist VALUES v_peh;
              END IF; -- IF (v_con.curr_population_entry_id is not null) THEN

               -- If contact doesn't have an entry in population_users, create one.
              IF (users_popualtion_users_id is null) THEN
                    SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                    v_pu.id := users_popualtion_users_id;
                    v_pu.user_id := null;
                    v_pu.contact_id := v_con.contact_id;
                    v_pu.curr_assigned_writer_id := null;
                    v_pu.curr_population_entry_id := null;
                    v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                    v_pu.entry_type_action_id := null;
                    v_pu.lock_history_id := null;
                    v_pu.time_created := sysdate;
--                    DBMS_OUTPUT.PUT_LINE('contact_id: ' || v_con.contact_id);
                    INSERT INTO population_users VALUES v_pu;
              END IF; --  IF (users_popualtion_users_id is null) THEN

              -- If population entry id hasn't been changed create a new one.
              IF (populationEntryId = 0) THEN
                    -- Count the number of users in the new population in order to determine user's entry group.
                    SELECT
                            COUNT(pe.id) INTO v_counter
                    FROM
                            population_entries pe,
                            population_users pu
                    WHERE
                            pe.id = pu.curr_population_entry_id AND
                            pe.population_id = qualifiedPopualtionId;

                    -- Insert a new population_entry
                    SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                    v_pe.id := populationEntryId;
                    v_pe.population_id := qualifiedPopualtionId;
                    v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                    v_pe.population_users_id := users_popualtion_users_id;
                    v_pe.qualification_time := popQualificationTime ;
                    v_pe.base_qualification_time := popBaseQualificationTime ;
                    v_pe.is_displayed := 1;
                    INSERT INTO population_entries VALUES v_pe;

                    qualificationStatus := 1; -- Qualification
              ELSE
                    qualificationStatus := 7; -- reQualification
              END IF; --  IF (populationEntryId = 0) THEN


               -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              v_peh.population_entry_id :=populationEntryId;
              v_peh.status_id := qualificationStatus;
              v_peh.writer_id := 0;
              v_peh.assigned_writer_id := assignedWriterId;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := null;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update contact entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.curr_population_entry_id = populationEntryId,
                    pu.curr_assigned_writer_id = assignedWriterId
              WHERE
                   pu.contact_id = v_con.contact_id;

        END IF; -- IF (qualifiedPopualtionId != 0) THEN

       COMMIT;

  END LOOP;
UPDATE CONTROL_LOGS CL SET CL.END_TIME = SYSDATE WHERE CL.ID = W_SEQ_ID;
  DBMS_OUTPUT.PUT_LINE('count_new_qualified_conacts  = ' || count_new_qualified_contacts);

  DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');

  NULL;
END;