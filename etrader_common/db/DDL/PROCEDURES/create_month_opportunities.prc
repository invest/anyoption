CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:53 (QP5 v5.149.1003.31008) */
PROCEDURE "CREATE_MONTH_OPPORTUNITIES"
AS
   monthFirstDay          DATE;
   monthLastDay           DATE;
   toInsTimeFirstInvest   TIMESTAMP WITH TIME ZONE;
   toInsTimeEstClosing    TIMESTAMP WITH TIME ZONE;
   toInsTimeLastInvest    TIMESTAMP WITH TIME ZONE;
   halfDayHoliday         exchange_holidays.is_half_day%TYPE;
   halfDayCloseTime       opportunity_templates.time_est_closing%TYPE;
   halfDayLastInvest      opportunity_templates.time_last_invest%TYPE;
   toIns                  opportunities%ROWTYPE;
BEGIN
   monthFirstDay :=
      TO_DATE (TO_CHAR (ADD_MONTHS (CURRENT_DATE, 1), 'yyyy-mm') || '-01',
               'yyyy-mm-dd');
   monthLastDay := LAST_DAY (monthFirstDay);

   FOR item
      IN (SELECT A.market_id,
                 A.time_first_invest,
                 A.time_est_closing,
                 A.time_last_invest,
                 A.time_zone,
                 A.odds_type_id,
                 A.writer_id,
                 A.opportunity_type_id,
                 A.scheduled,
                 A.deduct_win_odds,
                 A.is_open_graph,
                 A.ODDS_GROUP,
                 B.exchange_id,
                 B.max_exposure
            FROM opportunity_templates A, markets B
           WHERE A.market_id = B.id AND A.is_active = 1 AND A.scheduled = 4)
   LOOP
      -- we asume that there is at least 1 working day in the month

      -- find the first working day of the month and set the time first invest to be on it
      FOR i IN 0 .. 30
      LOOP
         IF ( (   item.exchange_id = 1
               OR item.exchange_id = 17
               OR item.exchange_id = 18)
             AND NOT TO_CHAR (monthFirstDay + i, 'D') = 6
             AND NOT TO_CHAR (monthFirstDay + i, 'D') = 7)
            OR (NOT (   item.exchange_id = 1
                     OR item.exchange_id = 17
                     OR item.exchange_id = 16
                     OR item.exchange_id = 18)
                AND NOT TO_CHAR (monthFirstDay + i, 'D') = 7
                AND NOT TO_CHAR (monthFirstDay + i, 'D') = 1)
            OR (    (item.exchange_id = 16)
                AND NOT TO_CHAR (monthFirstDay + i, 'D') = 5
                AND NOT TO_CHAR (monthFirstDay + i, 'D') = 6
                AND NOT TO_CHAR (monthFirstDay + i, 'D') = 7)
         THEN
            BEGIN
               SELECT is_half_day
                 INTO halfDayHoliday
                 FROM exchange_holidays
                WHERE exchange_id = item.exchange_id
                      AND TRUNC (holiday) = monthFirstDay + i;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  halfDayHoliday := NULL;
            END;

            IF halfDayHoliday IS NULL OR halfDayHoliday = 1
            THEN
               -- working day... at least half
               IF item.market_id = 370
                  AND TO_CHAR (monthFirstDay + i, 'D') = 6
               THEN
                  -- if the jakarta monthly doesn't open on friday it should open by the monthly template close time
                  -- if its friday open it by daily half day template
                  SELECT TO_TIMESTAMP_TZ (
                               TO_CHAR (monthFirstDay + i, 'yyyy-mm-dd')
                            || time_first_invest
                            || time_zone,
                            'yyyy-mm-dd hh24:mi TZR')
                    INTO toInsTimeFirstInvest
                    FROM opportunity_templates
                   WHERE     market_id = 370
                         AND scheduled = 2
                         AND is_half_day = 1;
               ELSE
                  toInsTimeFirstInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthFirstDay + i, 'yyyy-mm-dd')
                        || ' '
                        || item.time_first_invest
                        || ' '
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
               END IF;

               EXIT;
            END IF;
         END IF;
      END LOOP;

      -- find the last working day of the month and set the time last invest and time est closing
      FOR i IN 0 .. 30
      LOOP
         IF (   item.exchange_id = 1
             OR item.exchange_id = 17
             OR item.exchange_id = 18)
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 6
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 7
         THEN
            BEGIN
               SELECT is_half_day
                 INTO halfDayHoliday
                 FROM exchange_holidays
                WHERE exchange_id = item.exchange_id
                      AND TRUNC (holiday) = monthLastDay - i;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  halfDayHoliday := NULL;
            END;

            IF halfDayHoliday IS NULL
            THEN
               -- full working day
               toInsTimeEstClosing :=
                  TO_TIMESTAMP_TZ (
                        TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                     || item.time_est_closing
                     || item.time_zone,
                     'yyyy-mm-dd hh24:mi TZR');
               toInsTimeLastInvest :=
                  TO_TIMESTAMP_TZ (
                        TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                     || item.time_last_invest
                     || item.time_zone,
                     'yyyy-mm-dd hh24:mi TZR');
               EXIT;
            ELSE
               IF halfDayHoliday = 1
               THEN
                  -- working day... at least half
                  SELECT time_est_closing, time_last_invest
                    INTO halfDayCloseTime, halfDayLastInvest
                    FROM opportunity_templates
                   WHERE     market_id = item.market_id
                         AND scheduled = 2
                         AND is_half_day = 1
                         AND is_active = 1;

                  toInsTimeEstClosing :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayCloseTime
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  toInsTimeLastInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayLastInvest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  EXIT;
               END IF;
            END IF;
         END IF;

         IF NOT (   item.exchange_id = 1
                 OR item.exchange_id = 17
                 OR item.exchange_id = 16
                 OR item.exchange_id = 18)
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 7
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 1
         THEN
            BEGIN
               SELECT is_half_day
                 INTO halfDayHoliday
                 FROM exchange_holidays
                WHERE exchange_id = item.exchange_id
                      AND TRUNC (holiday) = monthLastDay - i;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  halfDayHoliday := NULL;
            END;

            IF halfDayHoliday IS NULL
            THEN
               -- full working day
               IF item.market_id = 15 AND TO_CHAR (monthLastDay - i, 'D') = 6
               THEN
                  -- ILS has different working time on friday so if the last working day is friday take its special closing time
                  SELECT TO_TIMESTAMP_TZ (
                               TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                            || time_est_closing
                            || item.time_zone,
                            'yyyy-mm-dd hh24:mi TZR'),
                         TO_TIMESTAMP_TZ (
                               TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                            || time_last_invest
                            || item.time_zone,
                            'yyyy-mm-dd hh24:mi TZR')
                    INTO toInsTimeEstClosing, toInsTimeLastInvest
                    FROM opportunity_templates
                   WHERE     market_id = 15
                         AND scheduled = 2
                         AND is_full_day = 0
                         AND is_half_day = 1
                         AND is_active = 1;
               ELSE
                  toInsTimeEstClosing :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || item.time_est_closing
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  toInsTimeLastInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || item.time_last_invest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
               END IF;

               EXIT;
            ELSE
               IF halfDayHoliday = 1
               THEN
                  -- working day... at least half
                  SELECT time_est_closing, time_last_invest
                    INTO halfDayCloseTime, halfDayLastInvest
                    FROM opportunity_templates
                   WHERE     market_id = item.market_id
                         AND scheduled = 2
                         AND is_half_day = 1
                         AND is_active = 1;

                  toInsTimeEstClosing :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayCloseTime
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  toInsTimeLastInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayLastInvest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  EXIT;
               END IF;
            END IF;
         END IF;

         IF     (item.exchange_id = 16)
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 5
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 6
            AND NOT TO_CHAR (monthLastDay - i, 'D') = 7
         THEN
            BEGIN
               SELECT is_half_day
                 INTO halfDayHoliday
                 FROM exchange_holidays
                WHERE exchange_id = item.exchange_id
                      AND TRUNC (holiday) = monthLastDay - i;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  halfDayHoliday := NULL;
            END;

            IF halfDayHoliday IS NULL
            THEN
               -- full working day
               toInsTimeEstClosing :=
                  TO_TIMESTAMP_TZ (
                        TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                     || item.time_est_closing
                     || item.time_zone,
                     'yyyy-mm-dd hh24:mi TZR');
               toInsTimeLastInvest :=
                  TO_TIMESTAMP_TZ (
                        TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                     || item.time_last_invest
                     || item.time_zone,
                     'yyyy-mm-dd hh24:mi TZR');
               EXIT;
            ELSE
               IF halfDayHoliday = 1
               THEN
                  -- working day... at least half
                  SELECT time_est_closing, time_last_invest
                    INTO halfDayCloseTime, halfDayLastInvest
                    FROM opportunity_templates
                   WHERE     market_id = item.market_id
                         AND scheduled = 2
                         AND is_half_day = 1
                         AND is_active = 1;

                  toInsTimeEstClosing :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayCloseTime
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  toInsTimeLastInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (monthLastDay - i, 'yyyy-mm-dd')
                        || halfDayLastInvest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  EXIT;
               END IF;
            END IF;
         END IF;
      END LOOP;

      SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM DUAL;

      toIns.market_id := item.market_id;
      toIns.time_created := CURRENT_TIMESTAMP;
      toIns.time_first_invest := toInsTimeFirstInvest;
      toIns.time_est_closing := toInsTimeEstClosing;
      toIns.time_last_invest := toInsTimeLastInvest;
      toIns.is_published := 0;
      toIns.is_settled := 0;
      toIns.odds_type_id := item.odds_type_id;
      toIns.writer_id := item.writer_id;
      toIns.opportunity_type_id := item.opportunity_type_id;
      toIns.is_disabled := 0;
      toIns.is_disabled_service := 0;
      toIns.is_disabled_trader := 0;
      toIns.scheduled := item.scheduled;
      toIns.deduct_win_odds := item.deduct_win_odds;
      toIns.is_open_graph := item.is_open_graph;
      toIns.max_exposure := item.max_exposure;
      toIns.Odds_Group := item.odds_group;

      INSERT INTO opportunities
           VALUES toIns;
   --  DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
   END LOOP;
END;
/
