----------------------
-- Deprecated !!!
----------------------

create or replace PROCEDURE WRITERS_COMMISSION_INV_CHECK AS 
  last_run DATE;
  time_canceled NUMBER :=0;
  max_deposit_during_call_days number;
  bonus_time_done DATE;
  bonus_user_id NUMBER;
  max_date DATE;
  min_date DATE;
  is_qualified NUMBER := 0;
  counter NUMBER :=0;
  not_good NUMBER := 0;
  r_check_writers_commission_dep NUMBER :=0;

  CURSOR c_check_writers_commission_dep(rep NUMBER, p_user_id NUMBER, assign_time DATE, c_inv_time DATE) IS
    SELECT 
      count(wcd.ID) count_writers_commission_dep
    FROM 
      WRITERS_COMMISSION_DEP wcd,
      TRANSACTIONS t
    WHERE 
      wcd.TRANSACTION_ID = t.ID
      AND t.USER_ID = p_user_id
      AND wcd.WRITER_ID = rep
      AND wcd.TIME_CREATED BETWEEN assign_time AND c_inv_time;
BEGIN 
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_INV_CHECK started at: ' || sysdate);
  -- Get the max deposit during call days in order not to miss last deposit during call transactions
  SELECT 
    max(deposit_during_call_days) 
  INTO 
    max_deposit_during_call_days
  FROM 
    skins;

  FOR v_inv IN
    (SELECT 
      i.id AS inv_id,
      i.user_id AS user_id,
      i.time_created AS inv_time,
      rtf.factor AS factor
    FROM 
      investments i,
      retention_turnover_factor rtf,
      opportunities op
    WHERE 
      i.is_canceled = 0
      AND i.opportunity_id = op.id
      AND op.opportunity_type_id = rtf.opportunity_type_id 
      --and i.time_created >= date'2013-02-02'
      --and i.time_created < date'2013-02-03'
      --AND i.time_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400
      --and i.id not in (select wci.investments_id from writers_commission_inv wci)
      AND i.id = 177043176 ) 
  LOOP
    FOR 
      v_data 
    IN
      ( SELECT 
          *
        FROM
          ( SELECT 
              aa.assigned_writer_id AS sales_rep,
              aa.user_id AS user_id,
              aa.assigned_time AS assignment_time,
              aa.call AS first_call,
              ll.time_settled AS time_qualified,
              ll.transaction_issue_id AS transaction_issue_id
            FROM
              ( SELECT 
                  a.assigned_writer_id,
                  a.user_id,
                  a.assigned_time,
                  min(a.call) AS CALL
                FROM
                  ( SELECT 
                      z.assigned_writer_id,
                      f.id AS user_id,
                      z.pehtime AS assigned_time,
                      f.CALL,
                      CASE WHEN f.CALL > z.pehtime THEN 1 ELSE 0 END AS call_after_assign
                    FROM
                      ( SELECT 
                          pu.user_id,
                          peh.time_created AS pehtime,
                          peh.assigned_writer_id,
                          peh.id AS pehid
                        FROM 
                          population_users pu,
                          population_entries pe,
                          population_entries_hist peh,
                          users u
                        WHERE 
                          pu.id=pe.population_users_id
                          AND peh.population_entry_id=pe.id
                          AND pu.user_id=u.id
                          AND peh.status_id=2
                          AND u.first_deposit_id<>0
                          AND u.class_id<>0
                          AND peh.assigned_writer_id IS NOT NULL
                          AND pu.user_id IS NOT NULL
                          AND peh.time_created>date '2012-09-01'
                          AND pu.user_id = v_inv.user_id) z,
                      ( SELECT 
                          ia.writer_id,
                          u.id,
                          ia.action_time AS CALL
                        FROM 
                          users u,
                          issues i,
                          issue_actions ia,
                          issue_action_types iat
                        WHERE 
                          i.user_id=u.id
                          AND ia.issue_id=i.id
                          AND ia.issue_action_type_id=iat.id
                          AND iat.channel_id = 1
                          AND iat.reached_status_id=2
                          AND ia.action_time>date '2012-09-01'
                          AND i.user_id = v_inv.user_id) f
                    WHERE 
                      z.user_id=f.id
                      AND z.assigned_writer_id=f.writer_id) a
                WHERE 
                  a.call_after_assign = 1
                GROUP BY 
                  a.assigned_writer_id,
                  a.assigned_time,
                  a.user_id)aa
              LEFT JOIN
                ( SELECT 
                    u.id AS transaction_issue_id,
                    ti.time_settled
                  FROM 
                    users u,
                    transactions t,
                    transactions_issues ti
                  WHERE 
                    u.first_deposit_id=t.id
                    AND t.id=ti.transaction_id
                    AND ti.time_settled IS NOT NULL)ll ON aa.user_id = ll.transaction_issue_id
            WHERE 
              ((ll.transaction_issue_id IS NOT NULL
                AND ll.time_settled IS NOT NULL)
              OR (ll.transaction_issue_id IS NULL
                 AND ll.time_settled IS NULL))
            ORDER BY 
              aa.assigned_time DESC)
        WHERE 
          rownum=1 ) 
    LOOP
      SELECT 
        max(bu.id) 
      INTO 
        bonus_user_id
      FROM 
        bonus_users bu
      WHERE 
        bu.user_id = v_inv.user_id
        AND bu.bonus_state_id IN (2,3) ;
      
      DBMS_OUTPUT.PUT_LINE('bonus_user_id: ' || bonus_user_id);
      IF (bonus_user_id IS NULL) THEN 
        DBMS_OUTPUT.PUT_LINE('v_data.assignment_time: ' || v_data.assignment_time);
        DBMS_OUTPUT.PUT_LINE('v_data.first_call: ' || v_data.first_call);
        DBMS_OUTPUT.PUT_LINE('v_data.time_qualified: ' || v_data.time_qualified);
        IF (v_data.assignment_time > v_data.first_call AND v_data.assignment_time >v_data.time_qualified) THEN 
          min_date := v_data.assignment_time;
        END IF;
        IF (v_data.first_call > v_data.assignment_time AND v_data.first_call > v_data.time_qualified) THEN 
          min_date :=v_data.first_call;
        END IF;
        IF (v_data.time_qualified > v_data.assignment_time AND v_data.time_qualified > v_data.first_call) THEN 
          min_date :=v_data.time_qualified;
        END IF;
        DBMS_OUTPUT.PUT_LINE('v_inv.inv_time: ' || v_inv.inv_time);
        DBMS_OUTPUT.PUT_LINE('min_date: ' || min_date);
        SELECT 
          count(time_created) 
        INTO 
          time_canceled
        FROM
          ( SELECT 
              peh.time_created
            FROM 
              population_users pu,
              population_entries pe,
              population_entries_hist peh,
              populations p,
              population_entries_hist_status pehs
            WHERE 
              pu.id = pe.population_users_id
              AND pe.id = peh.population_entry_id
              AND pu.user_id = v_inv.user_id
              AND p.id = pe.population_id
              AND pehs.id= peh.status_id
              AND peh.time_created <= v_inv.inv_time
              AND peh.time_created >= min_date
              AND (pehs.id = 6 OR pehs.is_cancel_assign = 1)
            ORDER BY 
              peh.time_created ASC)
        WHERE 
          rownum = 1;

        -- An investment enters this table under representative X only if it took place AFTER a Sales deposit credited to representative X
        OPEN c_check_writers_commission_dep(v_data.sales_rep, v_inv.user_id, v_data.assignment_time, v_inv.inv_time);
        FETCH c_check_writers_commission_dep 
        INTO r_check_writers_commission_dep;
        CLOSE c_check_writers_commission_dep;

        IF r_check_writers_commission_dep = 0 THEN 
          GOTO END_LOOP;
        END IF;
        IF(time_canceled = 0) THEN 
          IF(v_inv.inv_time >= v_data.first_call) THEN --Write what ever you want here
            DBMS_OUTPUT.PUT_LINE('Simulate insert');
            counter := counter +1;
          END IF;
        END IF;
      END IF;
      <<END_LOOP>> NULL;
    END LOOP;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('counter: ' || counter);
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_INV_CHECK finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_INV_CHECK;