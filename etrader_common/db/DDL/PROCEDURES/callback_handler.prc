CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:52 (QP5 v5.149.1003.31008) */
PROCEDURE "CALLBACK_HANDLER"
AS
   v_peh   population_entries_hist%ROWTYPE;
BEGIN
   -- Find all users that needs to be removed from tracking by time or by decline
   FOR v_pop_user
      IN (  SELECT pu.id,
                   i.population_entry_id,
                   ia.id entry_type_action_id,
                   pu.entry_type_id,
                   ia.action_time,
                   p.population_type_id,
                   pe.qualification_time,
                   iat.is_tracking_action,
                   pu.curr_assigned_writer_id,
                   MAX (
                      CASE
                         WHEN tt.class_type = 1 THEN t.time_created
                         ELSE NULL
                      END)
                      last_dep_time,
                   s.other_reactions_days tracking_days
              FROM                   population_users pu
                                  LEFT JOIN
                                     users u
                                  ON pu.user_id = u.id
                               LEFT JOIN
                                  transactions t
                               ON t.user_id = u.id AND t.status_id IN (2, 7, 8)
                            LEFT JOIN
                               transaction_types tt
                            ON t.type_id = tt.id AND tt.class_type = 1
                         LEFT JOIN
                            contacts c
                         ON pu.contact_id = c.id
                      LEFT JOIN
                         population_entries pe
                      ON pu.curr_population_entry_id = pe.id
                   LEFT JOIN
                      populations p
                   ON pe.population_id = p.id,
                   issue_actions ia,
                   issue_action_types iat,
                   issues i,
                   skins s
             WHERE     pu.entry_type_id = 3                       -- Call Back
                   AND pu.lock_history_id IS NULL
                   AND pu.entry_type_action_id = ia.id
                   AND ia.issue_id = i.id
                   AND ia.issue_action_type_id = iat.id
                   AND ( (u.id IS NOT NULL AND s.id = u.skin_id)
                        OR (u.id IS NULL AND s.id = c.skin_id))
                   AND INSTR (s.working_days, TO_CHAR (SYSDATE - 1, 'D')) > 0
                   AND TRUNC (SYSDATE) > TRUNC (ia.callback_time) + 1
          GROUP BY pu.id,
                   i.population_entry_id,
                   ia.id,
                   pu.entry_type_id,
                   ia.action_time,
                   p.population_type_id,
                   pe.qualification_time,
                   iat.is_tracking_action,
                   pu.curr_assigned_writer_id,
                   s.other_reactions_days)
   LOOP
      -- Init params
      v_peh := NULL;

      -- Insert a new population_entry_history
      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

      -- If users cb action was supposed to lead to tracking and user don't have a success deposit since, move to tracking
      -- Otherwise Rterun to general.
      IF (v_pop_user.is_tracking_action = 1
          AND SYSDATE < v_pop_user.action_time + v_pop_user.tracking_days
          AND (v_pop_user.last_dep_time IS NULL
               OR v_pop_user.action_time > v_pop_user.last_dep_time))
      THEN
         v_peh.status_id := 215;                           -- move to tracking
         v_pop_user.entry_type_id := 2;                           -- tracking;
      ELSE
         v_peh.status_id := 216;                            -- move to general
         v_pop_user.entry_type_id := 1;                            -- general;
         v_pop_user.curr_assigned_writer_id :=
            v_pop_user.curr_assigned_writer_id;
         v_pop_user.entry_type_action_id := NULL;
      END IF;

      v_peh.population_entry_id := v_pop_user.population_entry_id;
      v_peh.writer_id := 0;
      v_peh.assigned_writer_id := v_pop_user.curr_assigned_writer_id;
      v_peh.time_created := SYSDATE;
      v_peh.issue_action_id := v_pop_user.entry_type_action_id;

      INSERT INTO population_entries_hist
           VALUES v_peh;

      -- Update user entry in population_users table
      UPDATE population_users pu
         SET pu.entry_type_id = v_pop_user.entry_type_id, -- population.enrty.type.general
             pu.entry_type_action_id = v_pop_user.entry_type_action_id,
             pu.curr_assigned_writer_id = v_pop_user.curr_assigned_writer_id
       WHERE pu.id = v_pop_user.id;

      COMMIT;
   END LOOP;

   NULL;
END CALLBACK_HANDLER;
/
