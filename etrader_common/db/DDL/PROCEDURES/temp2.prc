CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:00 (QP5 v5.149.1003.31008) */
PROCEDURE TEMP2
AS
   v_user_id      NUMBER;
   v_START_TIME   DATE;
   v_CHAT_ID      NUMBER;
BEGIN
   FOR v_inv IN (  SELECT user_id, START_TIME
                     FROM LIVEPERSON_CHAT_SEPTEMBER1 lco
                 ORDER BY START_TIME)
   LOOP
      v_user_id := v_inv.user_id;
      v_START_TIME := v_inv.START_TIME;

      FOR v_inv2
         IN (SELECT ia.CHAT_ID
               FROM ISSUE_ACTIONS ia, issues i
              WHERE     ia.issue_id = i.id
                    AND ia.ISSUE_ACTION_TYPE_ID = 38
                    AND i.user_id = v_user_id
                    AND ia.action_time = v_START_TIME
                    AND ia.CHAT_ID IS NOT NULL)
      LOOP
         v_CHAT_ID := v_inv2.CHAT_ID;

         --DBMS_OUTPUT.PUT_LINE(v_user_id || ' ' || v_START_TIME || ' ' || v_CHAT_ID);
         UPDATE LIVEPERSON_CHAT_SEPTEMBER1 lco2
            SET lco2.chat_id = v_CHAT_ID
          WHERE lco2.user_id = v_user_id AND lco2.START_TIME = v_START_TIME;
      END LOOP;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('EEEEEEEEEEEEEEEND');
END TEMP2;
/
