CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:58 (QP5 v5.149.1003.31008) */
PROCEDURE "PROCESS_OLD_HTTP_REFERRER"
IS
   -- global variables
   start_index     NUMBER;
   end_index       NUMBER;
   decoded_query   VARCHAR2 (5000);
   temp            VARCHAR2 (5000);
   current_index   NUMBER;

   CURSOR c_main
   IS
      SELECT 'google' p1, 'q' p2 FROM DUAL
      UNION ALL
      SELECT 'yahoo' p1, 'p' p2 FROM DUAL
      UNION ALL
      SELECT 'bing' page, 'q' par FROM DUAL
      UNION ALL
      SELECT 'ask' page, 'q' par FROM DUAL
      UNION ALL
      SELECT 'baidu' page, 'wd' par FROM DUAL
      UNION ALL
      SELECT 'navar' page, 'query' par FROM DUAL
      UNION ALL
      SELECT 'daum' page, 'q' par FROM DUAL
      UNION ALL
      SELECT 'http://yandex' p1, 'text' p2 FROM DUAL
      UNION ALL
      SELECT 'http://search' page, 'q' par FROM DUAL;

   CURSOR c1 (
      p1    VARCHAR2,
      p2    VARCHAR2)
   IS
      SELECT id, http_referer
        FROM users
       WHERE     http_referer LIKE '%' || p1 || '%'
             AND http_referer LIKE '%' || p2 || '=%'
             AND http_referer NOT LIKE '%' || p2 || '=cache%';
BEGIN
   FOR tmp IN c_main
   LOOP
      current_index := 0;

      FOR item IN c1 (tmp.p1, tmp.p2)
      LOOP
         BEGIN
            temp := UTL_URL.unescape (item.http_referer, 'UTF-8');
         EXCEPTION
            WHEN UTL_URL.bad_url
            THEN
               DBMS_OUTPUT.put_line ('bad_url');
            WHEN UTL_URL.bad_fixed_width_charset
            THEN
               DBMS_OUTPUT.put_line ('BAD_FIXED_WIDTH_CHARSET');
         END;

         start_index := INSTR (temp, CHR (38) || tmp.p2 || '='); -- find the google search query starting with &q=

         IF start_index = 0
         THEN
            start_index := INSTR (temp, '?' || tmp.p2 || '='); -- the search query is the first parameter after ?
         END IF;

         decoded_query := SUBSTR (temp, start_index + 3); -- extract from start till the end of string
         END_INDEX := INSTR (DECODED_QUERY, CHR (38));

         IF end_index = 0 OR end_index IS NULL
         THEN
            END_INDEX := LENGTH (decoded_query); -- the search query is the last parameter in the http_referer
         END IF;

         decoded_query := SUBSTR (decoded_query, 1, end_index - 1); -- extract from beginning till the first ampersand
         decoded_query := REPLACE (decoded_query, '+', ' '); -- replace plus sign

         IF LENGTH (decoded_query) < 200
         THEN
            UPDATE users
               SET decoded_source_query = decoded_query
             WHERE id = item.id;                           -- update the table
         END IF;

         current_index := current_index + 1;
      END LOOP;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN                                                  -- handles all errors
      ROLLBACK;
      DBMS_OUTPUT.put_line ('---------');
      DBMS_OUTPUT.put_line (current_index);
      DBMS_OUTPUT.put_line (temp);
      DBMS_OUTPUT.put_line (DBMS_UTILITY.format_error_backtrace);
END;
/
