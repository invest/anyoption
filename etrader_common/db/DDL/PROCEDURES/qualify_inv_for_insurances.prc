CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:58 (QP5 v5.149.1003.31008) */
PROCEDURE "QUALIFY_INV_FOR_INSURANCES" (p_gm_start   IN VARCHAR,
                                        p_gm_end     IN VARCHAR)
AS
   v_cg   NUMBER;
BEGIN
   FOR v_user
      IN (SELECT O.id, O.user_name
            FROM users O
           WHERE EXISTS
                    (SELECT A.id
                       FROM investments A, opportunities B
                      WHERE     A.opportunity_id = B.id
                            AND A.user_id = O.id
                            AND B.scheduled = 1
                            AND A.is_settled = 0
                            AND A.bonus_odds_change_type_id = 0
                            AND B.opportunity_type_id = 1
                            AND A.time_created <=
                                   B.time_est_closing
                                   - TO_DSINTERVAL (
                                        '0 00:' || p_gm_start || ':00')
                            AND SYSTIMESTAMP >=
                                   B.time_est_closing
                                   - TO_DSINTERVAL (
                                        '0 00:' || p_gm_start || ':00')
                            AND SYSTIMESTAMP <=
                                   B.time_est_closing
                                   - TO_DSINTERVAL (
                                        '0 00:' || p_gm_end || ':00')))
   LOOP
      v_cg := 0;

      IF LENGTH (v_user.user_name) > 3
         AND SUBSTR (v_user.user_name, LENGTH (v_user.user_name) - 2, 3) =
                '_CG'
      THEN
         v_cg := 1;
      END IF;

      -- Insurances only for not calcalist game users
      IF v_cg = 0
      THEN
         -- process Golden Minutes investments
         UPDATE investments
            SET insurance_flag = 1
          WHERE id IN
                   (SELECT A.id
                      FROM investments A, opportunities B, markets C
                     WHERE     A.COPYOP_TYPE_ID <> 1 -- exclude copied investments 
                           AND A.opportunity_id = B.id
                           AND B.is_disabled = 0
                           AND B.market_id = C.id
                           AND C.is_golden_minutes = 1
                           AND B.scheduled = 1
                           AND A.is_settled = 0
                           AND A.bonus_odds_change_type_id = 0
                           AND B.opportunity_type_id = 1
                           AND A.user_id = v_user.id
                           AND (A.insurance_amount_ru = 0
                                OR A.insurance_amount_ru IS NULL)
                           AND A.time_created <=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_start || ':00')
                           AND SYSTIMESTAMP >=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_start || ':00')
                           AND SYSTIMESTAMP <=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_end || ':00')
                           AND CASE
                                  WHEN A.type_id = 1
                                  THEN
                                     B.gm_level - A.current_level
                                  ELSE
                                     A.current_level - B.gm_level
                               END
                               / A.current_level >
                                  C.insurance_qualify_percent);
      END IF;

      -- if no Golden Minutes investments then process Roll Up
      IF v_cg = 0 AND SQL%ROWCOUNT = 0
      THEN
         UPDATE investments
            SET insurance_flag = 2
          WHERE id IN
                   (SELECT A.id
                      FROM investments A, opportunities B, markets C
                     WHERE     A.COPYOP_TYPE_ID <> 1 -- exclude copied investments
                           AND GET_OPP_ROLL_UP (B.id) != 0
                           AND A.opportunity_id = B.id
                           AND B.is_disabled = 0
                           AND B.market_id = C.id
                           AND C.is_roll_up = 1
                           AND B.scheduled = 1
                           AND A.is_settled = 0
                           AND A.bonus_odds_change_type_id = 0
                           AND B.opportunity_type_id = 1
                           AND A.user_id = v_user.id
                           AND A.insurance_amount_ru
                               + ( (A.amount - A.insurance_amount_ru)
                                  * C.roll_up_premia_percent) <
                                  (A.amount - A.insurance_amount_ru)
                                  * A.odds_win
                           AND A.time_created <=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_start || ':00')
                           AND SYSTIMESTAMP >=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_start || ':00')
                           AND SYSTIMESTAMP <=
                                  B.time_est_closing
                                  - TO_DSINTERVAL (
                                       '0 00:' || p_gm_end || ':00')
                           AND CASE
                                  WHEN A.type_id = 1
                                  THEN
                                     B.gm_level - A.current_level
                                  ELSE
                                     A.current_level - B.gm_level
                               END
                               / A.current_level < -C.roll_up_qualify_percent);
      END IF;

      UPDATE investments
         SET insurance_flag = 0
       WHERE id IN
                (SELECT A.id
                   FROM investments A, opportunities B
                  WHERE     A.opportunity_id = B.id
                        AND B.scheduled = 1
                        AND A.is_settled = 0
                        AND A.bonus_odds_change_type_id = 0
                        AND B.opportunity_type_id = 1
                        AND A.user_id = v_user.id
                        AND A.time_created <=
                               B.time_est_closing
                               - TO_DSINTERVAL (
                                    '0 00:' || p_gm_start || ':00')
                        AND SYSTIMESTAMP >=
                               B.time_est_closing
                               - TO_DSINTERVAL (
                                    '0 00:' || p_gm_start || ':00')
                        AND SYSTIMESTAMP <=
                               B.time_est_closing
                               - TO_DSINTERVAL ('0 00:' || p_gm_end || ':00')
                        AND A.insurance_flag IS NULL);
   END LOOP;
END;
