CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:56 (QP5 v5.149.1003.31008) */
PROCEDURE "MARKETING_HANDLER"
AS
   v_peh              population_entries_hist%ROWTYPE;
   assignedWriterId   NUMBER;
BEGIN
   FOR v_pop_usr
      IN (SELECT pu.id,
                 pu.curr_population_entry_id,
                 pu.entry_type_id,
                 pu.curr_assigned_writer_id
            FROM population_users pu,
                 population_entries pe,
                 populations p,
                 population_entries_hist peh,
                 issue_actions ia,
                 marketing_operations mo
           WHERE     pu.entry_type_id != 5 -- not users that was removed from sales
                 AND pu.curr_population_entry_id = pe.id
                 AND pe.population_id = p.id
                 AND p.population_type_id = 13         -- Marketing popualtion
                 AND peh.population_entry_id = pe.id
                 AND peh.status_id = 1                        -- qualification
                 AND peh.issue_action_id = ia.id
                 AND ia.marketing_operation_id = mo.id
                 AND pe.qualification_time + mo.duration_days < SYSDATE)
   LOOP
      -- Init params
      v_peh := NULL;
      assignedWriterId := NULL;

      -- If user is in general entry type id cancel assign
      IF (v_pop_usr.entry_type_id != 1)
      THEN
         assignedWriterId := v_pop_usr.curr_assigned_writer_id;
      END IF;

      -- Insert a remove record to  population_entry_history
      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

      v_peh.population_entry_id := v_pop_usr.curr_population_entry_id;
      v_peh.status_id := 113;                       -- remove from pop by time
      v_peh.writer_id := 0;
      v_peh.assigned_writer_id := assignedWriterId;
      v_peh.time_created := SYSDATE;
      v_peh.issue_action_id := NULL;

      INSERT INTO population_entries_hist
           VALUES v_peh;

      -- Update user entry in population_users table
      UPDATE population_users pu
         SET pu.curr_population_entry_id = NULL,
             pu.curr_assigned_writer_id = assignedWriterId
       WHERE pu.id = v_pop_usr.id;


      COMMIT;
   END LOOP;

   NULL;
END MARKETING_HANDLER;
/
