CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:55 (QP5 v5.149.1003.31008) */
PROCEDURE DUPLICATED_CONTACTS_HANDLER
AS
   count_dup_contacts_insert       NUMBER := 0;
   count_dup_contacts_update       NUMBER := 0;
   last_dup_contacts_handler_run   DATE := NULL;
   dup_id                          NUMBER := NULL;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.
    PUT_LINE ('DUPLICATED_CONTACTS_HANDLER started at: ' || SYSDATE);

   FOR v_dup
      IN (  SELECT a.id AS contact_id,
                   a.email,
                   listagg (b.id, ',') WITHIN GROUP (ORDER BY b.ID)
                      AS list_of_ids
              FROM contacts a, contacts b
             WHERE     a.email = b.email
                   AND a.id != b.id
                   AND a.class_id <> 0
                   AND a.user_id = 0
                   AND b.user_id = 0
                   AND b.class_id <> 0
                   AND a.email NOT LIKE '%asaf%@etrader.co.il'
          GROUP BY a.id, a.email)
   LOOP
      SELECT MAX (dc.contact_id)
        INTO dup_id
        FROM duplicates_contacts dc
       WHERE dc.contact_id = v_dup.contact_id;

      IF (dup_id IS NOT NULL)
      THEN
         UPDATE duplicates_contacts
            SET dup_contact_id = v_dup.list_of_ids, time_updated = SYSDATE
          WHERE contact_id = dup_id;

         count_dup_contacts_update := count_dup_contacts_update + 1;
      ELSE
         INSERT INTO DUPLICATES_CONTACTS (ID,
                                          CONTACT_ID,
                                          DUP_CONTACT_ID,
                                          TIME_CREATED,
                                          TIME_UPDATED)
              VALUES (SEQ_DUPLICATED_CONTACTS.NEXTVAL,
                      v_dup.contact_id,
                      v_dup.list_of_ids,
                      SYSDATE,
                      SYSDATE);

         count_dup_contacts_insert := count_dup_contacts_insert + 1;
      END IF;
   END LOOP;



   DBMS_OUTPUT.PUT_LINE ('DB_PARAMETERS updated with: ' || SYSDATE);
   DBMS_OUTPUT.
    PUT_LINE ('count_dup_contacts inserted= ' || count_dup_contacts_insert);
   DBMS_OUTPUT.
    PUT_LINE ('count_dup_contacts updated= ' || count_dup_contacts_update);
   DBMS_OUTPUT.
    PUT_LINE ('DUPLICATED_CONTACTS_HANDLER finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
END DUPLICATED_CONTACTS_HANDLER;
/
