create or replace PROCEDURE WRITERS_COMMISSION_EXAMINATION(PRC_last_run DATE, PRC_start_time DATE, PRC_dep_during_call_days NUMBER, PRC_transaction_id NUMBER) AS 
  PRC_db_parameter_id NUMBER := 14;
  PRC_count_deposits NUMBER;
  PRC_count_withdrawals NUMBER;
BEGIN
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_EXAMINATION started at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  
  -- Deposit_handler
  PRC_count_deposits := WRITERS_COMMISSION.DEPOSIT_HANDLER_CHECK(PRC_last_run, PRC_start_time, PRC_dep_during_call_days, PRC_transaction_id);
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- WITHDRAW_HANDLER_CHECK
  PRC_count_withdrawals := WRITERS_COMMISSION.WITHDRAW_HANDLER_CHECK(PRC_last_run, PRC_start_time, PRC_dep_during_call_days, PRC_transaction_id);
  DBMS_OUTPUT.PUT_LINE(' ');

  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_EXAMINATION finished at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_EXAMINATION;