----------------------
-- Deprecated !!!
----------------------

create or replace PROCEDURE WRITERS_COMMISSION_DEP_MANUAL AS 
  last_run DATE;
  assign_writer_id NUMBER := 0;
  assign_date DATE ;
  first_call_time DATE ;
  time_canceled NUMBER := 0;
  max_deposit_during_call_days number;
  dep_during_call NUMBER :=0;
  delta number :=0;
  counter NUMBER :=0;
BEGIN
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_MANUAL started at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  -- Get the max deposit during call days in order not to miss last deposit during call transactions
  select 
    max(deposit_during_call_days)
  into 
    max_deposit_during_call_days
  from 
    skins;
    
  FOR 
    v_tran 
  IN 
    ( SELECT 
        t.id as tran_id,
        t.user_id as user_id,
        t.time_created as tran_time
      from 
        transactions t, 
        transaction_types tt
      where 
        t.type_id = tt.id
        and tt.class_type = 1
        and t.status_id in (2,7,8)
        AND t.time_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400
        --and t.time_created >= date'2013-01-27'
        --and t.time_created < date'2013-01-28'
        and t.id not in ( select 
                            wcd.transaction_id 
                          from 
                            writers_commission_dep wcd)) 
  LOOP
    DBMS_OUTPUT.PUT_LINE('-----');
    DBMS_OUTPUT.PUT_LINE('CHECK, User id: '||v_tran.user_id||', Transaction id: '||v_tran.tran_id||', Transaction time created: '||v_tran.tran_time);
    FOR 
      v_data 
    IN 
      ( select 
          x.sales_rep as sales_rep, 
          x.assignment_time as assignment_time, 
          x.first_call as first_call 
        from
          ( select 
              aa.assigned_writer_id as sales_rep, 
              aa.assigned_time as assignment_time, 
              aa.call as first_call
            from
              ( select 
                  a.assigned_writer_id,
                  a.user_id, 
                  a.assigned_time , 
                  min(a.call) as call
                from
                  ( select 
                      z.assigned_writer_id, 
                      f.id as user_id, 
                      z.pehtime as assigned_time, 
                      f.call , 
                      case when f.call > z.pehtime THEN 1  else  0 end as call_after_assign  
                    from    
                      ( select 
                          pu.user_id , 
                          peh.time_created as pehtime, 
                          peh.assigned_writer_id , 
                          peh.id as pehid     
                        from 
                          population_users pu, 
                          population_entries pe, 
                          population_entries_hist peh, 
                          users u     
                        where 
                          pu.id=pe.population_users_id     
                          and peh.population_entry_id=pe.id     
                          and pu.user_id=u.id    
                          and peh.status_id=2     
                          and u.first_deposit_id<>0
                          and u.class_id<>0    
                          and  peh.assigned_writer_id is not null     
                          and pu.user_id is not null     
                          and peh.time_created>date '2012-09-01'
                          and pu.user_id = v_tran.user_id) z,                       
                      ( select 
                          ia.writer_id, 
                          u.id, 
                          ia.action_time as call    
                        from 
                          users u, 
                          issues i, 
                          issue_actions ia, 
                          issue_action_types iat    
                        where 
                          i.user_id=u.id    
                          and ia.issue_id=i.id    
                          and ia.issue_action_type_id=iat.id    
                          and iat.channel_id = 1    
                          and iat.reached_status_id=2    
                          and ia.action_time>date '2012-09-01'
                          and i.user_id = v_tran.user_id) f    
                        where 
                          z.user_id=f.id    
                          and z.assigned_writer_id=f.writer_id) a
                    where 
                      a.call_after_assign = 1
                    group by 
                      a.assigned_writer_id, 
                      a.assigned_time, 
                      a.user_id)aa
              order by 
                aa.assigned_time desc) x
          where 
            rownum=1)
      LOOP
        select 
          count(time_created) 
        into 
          time_canceled     
        from 
          ( select 
              peh.time_created 
            from 
              population_users pu, 
              population_entries pe, 
              population_entries_hist peh, 
              populations p, 
              population_entries_hist_status pehs
            where 
              pu.id = pe.population_users_id
              and pe.id = peh.population_entry_id
              and pu.user_id = v_tran.user_id  
              and p.id = pe.population_id
              and pehs.id= peh.status_id
              and peh.time_created <= v_tran.tran_time
              and peh.time_created >= v_data.first_call
              and (pehs.id = 6 OR pehs.is_cancel_assign = 1)
            order by 
              peh.time_created asc)
        where 
          rownum = 1;
                  
        DBMS_OUTPUT.PUT_LINE('About to check, assigned to writer id: '||v_data.sales_rep||', assignment time: '||v_data.assignment_time||', first call: '||v_data.first_call||', time canceled assignment: '||time_canceled);                  
        IF(time_canceled = 0) THEN
          SELECT 
            COUNT(ti.transaction_id) 
          into 
            dep_during_call
          FROM 
            transactions_issues ti, 
            issue_actions ia
          WHERE 
            ti.transaction_id = v_tran.tran_id
            AND ti.issue_action_id = ia.id
            AND ia.issue_action_type_id = 31;
                                                    
          IF(dep_during_call > 0) THEN
            delta := max_deposit_during_call_days;
          ELSE 
            delta := 0;
          END IF;
          DBMS_OUTPUT.PUT_LINE('delta: '||delta);
          IF(v_tran.tran_time >= v_data.first_call - delta) THEN
            DBMS_OUTPUT.PUT_LINE('Going to insert transaction: '||v_tran.tran_id||' to WRITERS_COMMISSION_DEP table.');
            INSERT INTO writers_commission_dep(id, transaction_id, writer_id ,time_created)
            VALUES (SEQ_WRITERS_COMMISSION_DEP.nextval, v_tran.tran_id ,v_data.sales_rep, sysdate); 
            counter := counter +1;
          ELSE
            DBMS_OUTPUT.PUT_LINE('No insert, tran time < first_call - delta');
          END IF;
        ELSE
          DBMS_OUTPUT.PUT_LINE('No insert, Found cancel investment between first call to transaction time.');
        END IF;
      END LOOP;                                  
  END LOOP;                                                           
  DBMS_OUTPUT.PUT_LINE('-----');
  DBMS_OUTPUT.PUT_LINE('Summary: Insert ' || counter || ' transactions to WRITERS_COMMISSION_DEP table');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_MANUAL finished at: ' || to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_DEP_MANUAL;