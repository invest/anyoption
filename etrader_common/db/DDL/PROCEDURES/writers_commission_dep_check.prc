----------------------
-- Deprecated !!!
----------------------

CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:04 (QP5 v5.149.1003.31008) */
PROCEDURE WRITERS_COMMISSION_DEP_CHECK
AS
   last_run                       DATE;
   assign_writer_id               NUMBER := 0;
   assign_date                    DATE;
   first_call_time                DATE;
   time_canceled                  NUMBER := 0;
   max_deposit_during_call_days   NUMBER;
   dep_during_call                NUMBER := 0;
   delta                          NUMBER := 0;
   counter                        NUMBER := 0;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.
    PUT_LINE ('WRITERS_COMMISSION_DEP_CHECK started at: ' || SYSDATE);


   -- Get the max deposit during call days in order not to miss last deposit during call transactions
   SELECT MAX (deposit_during_call_days)
     INTO max_deposit_during_call_days
     FROM skins;

   FOR v_tran
      IN (SELECT t.id AS tran_id,
                 t.user_id AS user_id,
                 t.time_created AS tran_time
            FROM transactions t, transaction_types tt
           WHERE     t.type_id = tt.id
                 AND tt.class_type = 1
                 AND t.status_id IN (2, 7, 8)
                 --AND t.time_created BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400
                 --and t.time_created >= date'2013-01-27'
                 --and t.time_created < date'2013-01-28'
                 AND t.id IN (8842630))
   LOOP
      FOR v_data
         IN (SELECT x.sales_rep AS sales_rep,
                    x.assignment_time AS assignment_time,
                    x.first_call AS first_call
               FROM (  SELECT aa.assigned_writer_id AS sales_rep,
                              aa.assigned_time AS assignment_time,
                              aa.call AS first_call
                         FROM (  SELECT a.assigned_writer_id,
                                        a.user_id,
                                        a.assigned_time,
                                        MIN (a.call) AS call
                                   FROM (SELECT z.assigned_writer_id,
                                                f.id AS user_id,
                                                z.pehtime AS assigned_time,
                                                f.call,
                                                CASE
                                                   WHEN f.call > z.pehtime THEN 1
                                                   ELSE 0
                                                END
                                                   AS call_after_assign
                                           FROM (SELECT pu.user_id,
                                                        peh.time_created
                                                           AS pehtime,
                                                        peh.assigned_writer_id,
                                                        peh.id AS pehid
                                                   FROM population_users pu,
                                                        population_entries pe,
                                                        population_entries_hist peh,
                                                        users u
                                                  WHERE pu.id =
                                                           pe.population_users_id
                                                        AND peh.
                                                             population_entry_id =
                                                               pe.id
                                                        AND pu.user_id = u.id
                                                        AND peh.status_id = 2
                                                        AND u.first_deposit_id <>
                                                               0
                                                        AND u.class_id <> 0
                                                        AND peh.
                                                             assigned_writer_id
                                                               IS NOT NULL
                                                        AND pu.user_id
                                                               IS NOT NULL
                                                        AND peh.time_created >
                                                               DATE '2012-09-01'
                                                        AND pu.user_id =
                                                               v_tran.user_id) z,
                                                (SELECT ia.writer_id,
                                                        u.id,
                                                        ia.action_time AS call
                                                   FROM users u,
                                                        issues i,
                                                        issue_actions ia,
                                                        issue_action_types iat
                                                  WHERE i.user_id = u.id
                                                        AND ia.issue_id = i.id
                                                        AND ia.
                                                             issue_action_type_id =
                                                               iat.id
                                                        AND iat.channel_id = 1
                                                        AND iat.reached_status_id =
                                                               2
                                                        AND ia.action_time >
                                                               DATE '2012-09-01'
                                                        AND i.user_id =
                                                               v_tran.user_id) f
                                          WHERE z.user_id = f.id
                                                AND z.assigned_writer_id =
                                                       f.writer_id) a
                                  WHERE a.call_after_assign = 1
                               GROUP BY a.assigned_writer_id,
                                        a.assigned_time,
                                        a.user_id) aa
                     ORDER BY aa.assigned_time DESC) x
              WHERE ROWNUM = 1)
      LOOP
         DBMS_OUTPUT.PUT_LINE ('v_tran.tran_time: ' || v_tran.tran_time);
         DBMS_OUTPUT.PUT_LINE ('v_data.first_call: ' || v_data.first_call);

         SELECT COUNT (time_created)
           INTO time_canceled
           FROM (  SELECT peh.time_created
                     FROM population_users pu,
                          population_entries pe,
                          population_entries_hist peh,
                          populations p,
                          population_entries_hist_status pehs
                    WHERE     pu.id = pe.population_users_id
                          AND pe.id = peh.population_entry_id
                          AND pu.user_id = v_tran.user_id
                          AND p.id = pe.population_id
                          AND pehs.id = peh.status_id
                          AND peh.time_created <= v_tran.tran_time
                          AND peh.time_created >= v_data.first_call
                          AND (pehs.id = 6 OR pehs.is_cancel_assign = 1)
                 ORDER BY peh.time_created ASC)
          WHERE ROWNUM = 1;

         DBMS_OUTPUT.PUT_LINE ('time_canceled: ' || time_canceled);

         IF (time_canceled = 0)
         THEN
            SELECT COUNT (ti.transaction_id)
              INTO dep_during_call
              FROM transactions_issues ti, issue_actions ia
             WHERE     ti.transaction_id = v_tran.tran_id
                   AND ti.issue_action_id = ia.id
                   AND ia.issue_action_type_id = 31;

            IF (dep_during_call > 0)
            THEN
               delta := max_deposit_during_call_days;
            ELSE
               delta := 0;
            END IF;

            IF (v_tran.tran_time >= v_data.first_call - delta)
            THEN
               DBMS_OUTPUT.PUT_LINE ('insert');
               --INSERT INTO writers_commission_dep(id, transaction_id, writer_id ,time_created)
               --VALUES (SEQ_WRITERS_COMMISSION_DEP.nextval, v_tran.tran_id ,v_data.sales_rep, sysdate);
               counter := counter + 1;
            END IF;
         END IF;
      END LOOP;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('counter: ' || counter);
   DBMS_OUTPUT.
    PUT_LINE ('WRITERS_COMMISSION_DEP_CHECK finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
END WRITERS_COMMISSION_DEP_CHECK;
/
