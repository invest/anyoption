CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:57 (QP5 v5.149.1003.31008) */
PROCEDURE "POP_ENTRIES_HANDLING_FILL" (data_range_days IN NUMBER)
IS
   time_assigned                  DATE;
   first_call_action_id           NUMBER;
   first_reached_call_action_id   NUMBER;
   remove_pop_his_id              NUMBER;
   entries_count                  NUMBER := 0;
BEGIN
   DBMS_OUTPUT.
    put_line (
      'procedure start: ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy - hh24:mi'));
   DBMS_OUTPUT.
    put_line (
      'data from: '
      || TO_CHAR (SYSDATE - data_range_days, 'dd/mm/yyyy - hh24:mi'));

   DELETE FROM population_entries_handling;

   FOR v_data
      IN (SELECT pe.id pe_id,
                 ph.time_created pe_time_created,
                 ph.assigned_writer_id,
                 w.user_name assigned_writer_name,
                 p.skin_id
            FROM population_entries pe,
                 populations p,
                    population_entries_hist ph
                 LEFT JOIN
                    writers w
                 ON w.id = ph.assigned_writer_id
           WHERE     pe.population_id = p.id
                 AND p.population_type_id = 1
                 AND ph.population_entry_id = pe.id
                 AND ph.status_id = 1
                 AND ph.time_created > SYSDATE - data_range_days)
   LOOP
      entries_count := entries_count + 1;

      -- Fill assigned writer name
      IF (v_data.assigned_writer_id IS NULL)
      THEN
         BEGIN
            SELECT w.user_name, ph.time_created
              INTO v_data.assigned_writer_name, time_assigned
              FROM population_entries_hist ph, writers w
             WHERE ph.id =
                      (SELECT MIN (ph2.id)
                         FROM population_entries_hist ph2
                        WHERE     ph2.population_entry_id = v_data.pe_id
                              AND ph2.status_id = 2
                              AND ph2.time_created > v_data.pe_time_created)
                   AND w.id = ph.assigned_writer_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               time_assigned := NULL;
               v_data.assigned_writer_name := NULL;
         --dbms_output.put_line('unassigned lead, pe id:' || v_data.pe_id);
         END;
      ELSE
         time_assigned := NULL;
      END IF;

      BEGIN
         -- Fill first call action id
         SELECT MIN (ia.id),
                MIN (
                   CASE
                      WHEN it.reached_status_id = 2 THEN ia.id
                      ELSE NULL
                   END)
           INTO first_call_action_id, first_reached_call_action_id
           FROM issues i,
                issue_actions ia,
                issue_action_types it,
                writers w
          WHERE     i.population_entry_id = v_data.pe_id
                AND ia.issue_id = i.id
                AND ia.issue_action_type_id = it.id
                AND it.channel_id = 1
                AND ia.writer_id = w.id
                AND w.dept_id = 3
                AND ia.action_time > v_data.pe_time_created;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            first_call_action_id := NULL;
      --dbms_output.put_line('uncalled lead, pe id:' || v_data.pe_id);
      END;

      BEGIN
         -- Fill pop remove history id
         SELECT MIN (ph.id)
           INTO remove_pop_his_id
           FROM population_entries_hist ph, population_entries_hist_status ps
          WHERE     ph.population_entry_id = v_data.pe_id
                AND ph.status_id = ps.id
                AND ps.is_remove = 1
                AND ph.time_created > v_data.pe_time_created;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            remove_pop_his_id := NULL;
      --dbms_output.put_line('unremoved lead, pe id:' || v_data.pe_id);
      END;

      -- insert into population_entries_handling
      INSERT INTO population_entries_handling
           VALUES (v_data.pe_id,
                   v_data.pe_time_created,
                   time_assigned,
                   v_data.assigned_writer_name,
                   v_data.skin_id,
                   first_call_action_id,
                   first_reached_call_action_id,
                   remove_pop_his_id);
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line ('entries number: ' || entries_count);
   DBMS_OUTPUT.
    put_line ('procedure end: ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy - hh24:mi'));
END pop_entries_handling_fill;
/
