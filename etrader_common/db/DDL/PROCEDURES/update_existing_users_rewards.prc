create or replace
PROCEDURE UPDATE_EXISTING_USERS_REWARDS AS 
  counter NUMBER := 0;
  counter_tier_0 NUMBER := 0;
  counter_tier_1 NUMBER := 0;
  counter_tier_2 NUMBER := 0;
  counter_tier_3 NUMBER := 0;
  counter_tier_4 NUMBER := 0;
  
  exp_points NUMBER := 0;
  current_tier NUMBER := 0;
  is_need_grant_bonus NUMBER := 1;
BEGIN
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------');
  DBMS_OUTPUT.PUT_LINE('UPDATE EXISTING USERS REWARDS started at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------');
  
    FOR v_data IN (
                  select
                    u.id as user_id,
                    usr_inv.turnover,
                    usr_trn.min_trn,
                    usr_issue.issue_action_type_id,
                    u.is_active,
                    u.bonus_abuser
                  from
                    users u
                    LEFT JOIN (select
                                inv.user_id,
                                sum((inv.amount / 100) * inv.rate) as turnover
                              from
                                investments inv
                              where
                                inv.is_canceled = 0
                                and inv.is_settled = 1
                              group by 
                                inv.user_id) usr_inv on usr_inv.user_id = u.id
                    LEFT JOIN (select
                                t.user_id,
                                min(t.id) as min_trn
                              from
                                transactions t,
                                transaction_types tt
                              where
                                t.type_id = tt.id
                                and tt.CLASS_TYPE = 1 -- real deposit
                                and t.STATUS_ID in (2,7,8) -- only success deposits
                              group by
                                t.user_id) usr_trn on usr_trn.user_id = u.id
                    LEFT JOIN (select
                                i1.user_id,
                                ia1.issue_action_type_id
                              from
                                issue_actions ia1,
                                issues i1
                              where
                                ia1.issue_id = i1.id
                                and ia1.id in
                                (select
                                  max(ia.id) as max_iss_act_close
                                from
                                  issues i,
                                  issue_actions ia,
                                  issue_action_types iat
                                where
                                  i.id = ia.issue_id
                                  and ia.issue_action_type_id = iat.id
                                  and iat.is_close_account = 1
                                group by 
                                  i.user_id)) usr_issue on usr_issue.user_id = u.id
                )
                LOOP
                  IF (v_data.min_trn is null) THEN
                    -- Tier 0 --
                      exp_points := 10;
                      current_tier := 0;
                      counter_tier_0 := counter_tier_0 + 1;
                  ELSE IF (v_data.turnover >= 7000) THEN
                      -- Tier 3 --
                      exp_points := 1200;
                      current_tier := 3;
                      counter_tier_3 := counter_tier_3 + 1;
                  ELSE IF (v_data.turnover >= 1000 and v_data.turnover < 7000) THEN 
                      -- Tier 2 --
                      exp_points := 300;
                      current_tier := 2;
                      counter_tier_2 := counter_tier_2 + 1;
                  ELSE 
                    -- Tier 1 --
                    exp_points := 11;
                    current_tier := 1;
                    counter_tier_1 := counter_tier_1 + 1;
                  END IF;
                  END IF;
                  END IF;
                  
                  -- check if need to grant bonus to user
                  is_need_grant_bonus := 1;
                  IF (v_data.bonus_abuser = 1 OR (v_data.is_active = 0 and v_data.issue_action_type_id is not null and v_data.issue_action_type_id != 39)) THEN
                    is_need_grant_bonus := 0;
                  END IF;
                  
                  -- insert into DB:
                  -- Fix experience points (RWD_USERS & RWD_USERS_HISTORY)
                  INSERT INTO RWD_USERS (ID, USER_ID, RWD_TIER_ID, EXP_POINTS, TIME_LAST_ACTIVITY, TIME_LAST_REDUCED_POINTS)
                    VALUES (SEQ_RWD_USERS.nextval, v_data.user_id, current_tier, exp_points, sysdate, null);
                  INSERT INTO RWD_USERS_HISTORY (ID, WRITER_ID, USER_ID, RWD_TIER_ID, RWD_ACTION_TYPE_ID, RWD_ACTION_ID, REFERENCE_ID, RWD_HIS_TYPE_ID, BALANCE_EXP_POINTS, COMMENTS, TIME_CREATED, AMOUNT_EXP_POINTS)
                      VALUES (SEQ_RWD_USERS_HISTORY.nextval, 0, v_data.user_id, 0, '', '', '', 7, exp_points, 'Kickoff, Run by procedure for all existing users', sysdate, exp_points);
                  
                  -- insert task: 'open account' - RWD_USER_TASK
                  INSERT INTO RWD_USER_TASKS (ID, USER_ID, RWD_TASK_ID, NUM_ACTIONS, NUM_RECURRING ,IS_DONE)
                    VALUES (SEQ_RWD_USER_TASKS.nextval, v_data.user_id, 1, 1, 1, 1);
                  
                  IF (current_tier != 0) THEN
                  
                    -- insert task: 'FTD' - RWD_USER_TASK
                    INSERT INTO RWD_USER_TASKS (ID, USER_ID, RWD_TASK_ID, NUM_ACTIONS, NUM_RECURRING ,IS_DONE)
                      VALUES (SEQ_RWD_USER_TASKS.nextval, v_data.user_id, 2, 1, 1, 1);  
                  
                    -- Promote Tier - RWD_USERS_HISTORY
                    INSERT INTO RWD_USERS_HISTORY (ID, WRITER_ID, USER_ID, RWD_TIER_ID, RWD_ACTION_TYPE_ID, RWD_ACTION_ID, REFERENCE_ID, RWD_HIS_TYPE_ID, BALANCE_EXP_POINTS, COMMENTS, TIME_CREATED, AMOUNT_EXP_POINTS)
                        VALUES (SEQ_RWD_USERS_HISTORY.nextval, 0, v_data.user_id, current_tier, '', '', '', 3, exp_points, 'Kickoff, Run by procedure for all existing users', sysdate, 0);
                        
                    -- Rewards Grant - RWD_USER_BENEFITS
                    INSERT INTO RWD_USER_BENEFITS (ID, USER_ID, RWD_BENEFIT_ID, IS_ACTIVE, TIME_CREATED)
                      SELECT
                        SEQ_RWD_USER_BENEFITS.nextval,
                        v_data.user_id,
                        rb.id,
                        1,
                        sysdate
                      FROM
                        RWD_BENEFITS rb
                      where
                        (rb.rwd_tier_id <= current_tier and rb.bonus_id is null) 
                        OR 
                        (rb.rwd_tier_id = current_tier and rb.bonus_id is not null and rb.is_grant_when_skip = 1 and is_need_grant_bonus = 1);
                        
                    -- Rewards Grant - RWD_USERS_HISTORY
                    INSERT INTO RWD_USERS_HISTORY (ID, WRITER_ID, USER_ID, RWD_TIER_ID, RWD_ACTION_TYPE_ID, RWD_ACTION_ID, REFERENCE_ID, RWD_HIS_TYPE_ID, BALANCE_EXP_POINTS, COMMENTS, TIME_CREATED, AMOUNT_EXP_POINTS)
                      SELECT
                        seq_rwd_users_history.nextval,
                        0,
                        v_data.user_id,
                        current_tier,
                        2,
                        rub.id,
                        null,
                        1,
                        exp_points,
                        'Kickoff, Run by procedure for all existing users',
                        sysdate,
                        0
                      FROM
                        RWD_USER_BENEFITS rub,
                        RWD_BENEFITS rb
                      where
                        rb.id = rub.rwd_benefit_id
                        and rub.user_id = v_data.user_id;
                  END IF;  
                  
                  counter := counter + 1;
                END LOOP;
    
    DBMS_OUTPUT.PUT_LINE('counter_tier_0  = ' || counter_tier_0);
    DBMS_OUTPUT.PUT_LINE('counter_tier_1  = ' || counter_tier_1);
    DBMS_OUTPUT.PUT_LINE('counter_tier_2  = ' || counter_tier_2);
    DBMS_OUTPUT.PUT_LINE('counter_tier_3  = ' || counter_tier_3);
    DBMS_OUTPUT.PUT_LINE('counter_tier_4  = ' || counter_tier_4);
    DBMS_OUTPUT.PUT_LINE('counter  = ' || counter);
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------');
    DBMS_OUTPUT.PUT_LINE('UPDATE EXISTING USERS REWARDS finished at: ' || sysdate);
    DBMS_OUTPUT.PUT_LINE('------------------------------------------------');
  NULL;
END UPDATE_EXISTING_USERS_REWARDS;