create or replace TYPE         "GET_NICKNAME_TYPE"                                         IS OBJECT
(
id INTEGER,
nick_name_first VARCHAR2(30 byte),
nick_name_last VARCHAR2(30 byte)
);