create or replace TYPE "ANALYTICS_DEPOSITS_OBJ" AS OBJECT (
    platform_id NUMBER,
    platform_name VARCHAR(30 CHAR),
    count_ftds NUMBER,
    deposits_amount NUMBER,
    time_data_fetched DATE
    );