CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:33:58 (QP5 v5.149.1003.31008) */
TYPE "MARKETING_BASE_REPORT_OBJ_TYPE" IS OBJECT
(campaign_manager VARCHAR2 (30),
 combination_id NUMBER,
 campaign_name VARCHAR2 (200),
 source_name VARCHAR2 (50),
 medium_name VARCHAR2 (50),
 content_name VARCHAR2 (50),
 marketing_size_horizontal NUMBER,
 marketing_size_vertical NUMBER,
 m_location VARCHAR2 (4000),
 landing_page_name VARCHAR2 (50),
 SKIN_ID NUMBER,
 short_reg NUMBER,
 registered_users_num NUMBER,
 FTD NUMBER,
 RFD NUMBER,
 first_remarketing_dep_num NUMBER,
 house_win NUMBER,
 failed_users NUMBER,
 fi_source_id NUMBER,
 fi_campaign_manager_id NUMBER);
/
