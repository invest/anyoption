create or replace type stringagg_t as object
(
  total varchar2(4000),

  static function odciaggregateinitialize(sctx in out stringagg_t) return number,

  member function odciaggregateiterate
  (
    self  in out stringagg_t
   ,value in varchar2
  ) return number,

  member function odciaggregateterminate
  (
    self        in stringagg_t
   ,returnvalue out varchar2
   ,flags       in number
  ) return number,

  member function odciaggregatemerge
  (
    self in out stringagg_t
   ,ctx2 in stringagg_t
  ) return number
)
/
create or replace type body stringagg_t is
  static function odciaggregateinitialize(sctx in out stringagg_t) return number is
  begin
    sctx := stringagg_t(null);
    return odciconst.success;
  end;

  member function odciaggregateiterate
  (
    self  in out stringagg_t
   ,value in varchar2
  ) return number is
  begin
    self.total := self.total || ',' || value;
    return odciconst.success;
  end;

  member function odciaggregateterminate
  (
    self        in stringagg_t
   ,returnvalue out varchar2
   ,flags       in number
  ) return number is
  begin
    returnvalue := ltrim(self.total, ',');
    return odciconst.success;
  end;

  member function odciaggregatemerge
  (
    self in out stringagg_t
   ,ctx2 in stringagg_t
  ) return number is
  begin
    self.total := self.total || ctx2.total;
    return odciconst.success;
  end;
end;
/
