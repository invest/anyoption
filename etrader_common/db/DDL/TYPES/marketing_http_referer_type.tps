CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:33:59 (QP5 v5.149.1003.31008) */
TYPE MARKETING_HTTP_REFERER_TYPE IS OBJECT
(HTTP_REFERER NVARCHAR2 (4000),
 DSQ NVARCHAR2 (4000),
 DP NVARCHAR2 (4000),
 campaign_manager VARCHAR2 (300),
 combination_id NUMBER,
 campaign_name VARCHAR2 (2000),
 source_name VARCHAR2 (500),
 medium_name VARCHAR2 (500),
 SKIN_ID NUMBER,
 short_reg NUMBER,
 registered_users_num NUMBER,
 FTD NUMBER,
 first_remarketing_dep_num NUMBER);
/
