﻿  
begin
  dbms_scheduler.create_job(job_name        => 'etrader.maintanance_job'
                           ,job_type        => 'STORED_PROCEDURE'
                           ,job_action      => 'pkg_maintanance.maint_stats'
                           ,start_date      => from_tz(cast(trunc(sysdate) + 1 + 3 / 24 as timestamp), 'UTC')
                           ,repeat_interval => 'FREQ=DAILY; INTERVAL=1'
                           ,enabled         => true
                           ,auto_drop       => false
                           ,comments        => 'will analyze partitionned tables daily');
end;
/

