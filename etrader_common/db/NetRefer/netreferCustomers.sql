SELECT 
    u.id CustomerID,
    c.a2 Country_ID,
    nvl(replace((CASE WHEN u.special_code is not null THEN u.special_code ELSE u.dynamic_param END), ' ', ''),' ') as  Tag,
    u.ip Registration_ip,
    nvl(to_char(u.time_created,'yyyy-mm-dd'), ' ') Registration_Date,
    c.COUNTRIES_GROUP_NETREFER_ID CustomerTypeID,
    decode(u.skin_id, 22, 2, 9, 3, 20, 3, 1) BrandID,
    CASE u.writer_id 
    WHEN 200 then 'Mobile'
    WHEN 20000 then 'Mobile'
    WHEN 1 then 'Web'
    WHEN 10000 then 'Web'
    ELSE 'Other' END "Origin"
FROM
    users u,
    countries c
WHERE
    u.country_id = c.id
    and trunc(u.affiliate_system_user_time) = trunc(sysdate-1/24)
    and u.class_id <> 0
    and u.affiliate_system = 1;