REM INSERTING into LANGUAGES
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (1,'iw','languages.heb');
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (2,'en','languages.eng');
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (3,'tr','languages.tur');
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (4,'ru','languages.rus');
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (5,'es','languages.spa');
Insert into LANGUAGES (ID,CODE,DISPLAY_NAME) values (6,'it','languages.ita');
