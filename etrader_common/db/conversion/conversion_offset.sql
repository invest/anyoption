-- Dates conversion queries

UPDATE
	BALANCE_HISTORY
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM');

UPDATE
	BIN_BLACK_LIST
SET
	TIME_CREATED = TIME_CREATED - to_char(to_timestamp_tz(to_char(TIME_CREATED, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	CC_BLACK_LIST
SET
	TIME_CREATED = TIME_CREATED - to_char(to_timestamp_tz(to_char(TIME_CREATED, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	CHARGE_BACKS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM');

UPDATE
	CLICKS
SET
	TIME_CREATED = TIME_CREATED - to_char(to_timestamp_tz(to_char(TIME_CREATED, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	CREDIT_CARDS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM'),
	time_modified = CASE WHEN NOT time_modified IS NULL THEN time_modified - to_char(to_timestamp_tz(to_char(time_modified, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_modified = CASE WHEN NOT time_modified IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_modified, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END;

UPDATE
	FILES
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM');

UPDATE
	FRAUDS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM');

UPDATE
	INVESTMENTS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM'),
	time_settled = CASE WHEN NOT time_settled IS NULL THEN time_settled - to_char(to_timestamp_tz(to_char(time_settled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_settled = CASE WHEN NOT time_settled IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_settled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END,
	time_canceled = CASE WHEN NOT time_canceled IS NULL THEN time_canceled - to_char(to_timestamp_tz(to_char(time_canceled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_cancelled = CASE WHEN NOT time_canceled IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_canceled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END;

UPDATE
	ISSUES
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM'),
	time_progress = CASE WHEN NOT time_progress IS NULL THEN time_progress - to_char(to_timestamp_tz(to_char(time_progress, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_progress = CASE WHEN NOT time_progress IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_progress, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END,
	time_finish = CASE WHEN NOT time_finish IS NULL THEN time_finish - to_char(to_timestamp_tz(to_char(time_finish, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_finish = CASE WHEN NOT time_finish IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_finish, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END;

UPDATE
	LOG
SET
	TIME_CREATED = TIME_CREATED - to_char(to_timestamp_tz(to_char(TIME_CREATED, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	MESSAGES
SET
	START_EFF_DATE = START_EFF_DATE - to_char(to_timestamp_tz(to_char(START_EFF_DATE, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	END_EFF_DATE = END_EFF_DATE - to_char(to_timestamp_tz(to_char(END_EFF_DATE, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	OPPORTUNITY_SHIFTINGS
SET
	SHIFTING_TIME = SHIFTING_TIME - to_char(to_timestamp_tz(to_char(SHIFTING_TIME, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24;

UPDATE
	TAX_HISTORY
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM');

UPDATE
	TRANSACTIONS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM'),
	time_settled = CASE WHEN NOT time_settled IS NULL THEN time_settled - to_char(to_timestamp_tz(to_char(time_settled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_settled = CASE WHEN NOT time_settled IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_settled, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END;

UPDATE
	USERS
SET
	time_created = time_created - to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24,
	utc_offset_created = 'GMT' || to_char(to_timestamp_tz(to_char(time_created, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM'),
	time_last_login = CASE WHEN NOT time_last_login IS NULL THEN time_last_login - to_char(to_timestamp_tz(to_char(time_last_login, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	time_modified = CASE WHEN NOT time_modified IS NULL THEN time_modified - to_char(to_timestamp_tz(to_char(time_modified, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset_modified = CASE WHEN NOT time_modified IS NULL THEN 'GMT' || to_char(to_timestamp_tz(to_char(time_modified, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH:TZM') ELSE NULL END,
	last_failed_time = CASE WHEN NOT last_failed_time IS NULL THEN last_failed_time - to_char(to_timestamp_tz(to_char(last_failed_time, 'yyyy-mm-dd hh24:mi') || ' Israel', 'yyyy-mm-dd hh24:mi TZR'), 'TZH')/24 ELSE NULL END,
	utc_offset = 'GMT+02:00';
