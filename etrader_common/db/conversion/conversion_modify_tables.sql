alter table BALANCE_HISTORY add ( "UTC_OFFSET"  VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );


alter table CHARGE_BACKS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

alter table CHEQUES add ( CITY_NAME VARCHAR2(120) );
alter table CREDIT_CARDS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );
alter table CREDIT_CARDS add ( UTC_OFFSET_MODIFIED VARCHAR2(9));

alter table CREDIT_CARDS modify ( CC_PASS NUMBER (4,0) );
alter table CURRENCIES add ( DISPLAY_NAME VARCHAR2(20)  DEFAULT 'currencies.ils' NOT NULL  );
alter table CURRENCIES add ( DEPOSIT_SUM_ALLOW NUMBER DEFAULT 0 );
alter table CURRENCIES add ( DEPOSIT_AMOUNT_FOR_LOCK NUMBER DEFAULT 0 );
alter table CURRENCIES add ( DEPOSIT_AMOUNT_FOR_EMAIL NUMBER DEFAULT 0);
alter table FILES add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );
alter table FRAUDS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

alter table INVESTMENTS add ( UTC_OFFSET_CANCELLED VARCHAR2(9));
alter table INVESTMENTS add ( UTC_OFFSET_SETTLED VARCHAR2(9));
alter table INVESTMENTS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00' NOT NULL);

alter table ISSUES add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );
alter table ISSUES add ( UTC_OFFSET_PROGRESS VARCHAR2(9) );

alter table ISSUES add ( UTC_OFFSET_FINISH VARCHAR2(9));


alter table LIMITS add ( MIN_BANK_WIRE NUMBER  DEFAULT 0 );
alter table LIMITS add ( BANK_WIRE_MAX_AMOUNT_FOR_FEE NUMBER DEFAULT 0);
alter table LIMITS add ( FREE_INVEST_MIN NUMBER DEFAULT 50000 );
alter table LIMITS add ( MAX_BANK_WIRE NUMBER DEFAULT 0);
alter table LIMITS add ( FREE_INVEST_MAX NUMBER DEFAULT 300000);
alter table LIMITS add ( BANK_WIRE_FEE NUMBER DEFAULT 0);
alter table LIMITS add ( FEE_CC NUMBER DEFAULT 1000);
alter table LIMITS add ( MAX_AMOUNT_FOR_FEE NUMBER  DEFAULT 50000);
alter table LIMITS add ( FEE NUMBER DEFAULT 1000);

alter table  MARKETS  drop column HOME_PAGE_PRIORITY ;
alter table MARKETS drop column HOME_PAGE_HOUR_FIRST;
alter table MARKETS drop column GROUP_PRIORITY;

ALTER TABLE TRANSACTIONS  RENAME COLUMN CC_FEE_CANCEL TO  FEE_CANCEL;


alter table MESSAGES add ( LANGUAGE_ID NUMBER  DEFAULT 1 NOT NULL);
alter table MESSAGES add ( SKIN_ID NUMBER  DEFAULT 1 NOT NULL );
alter table OPPORTUNITY_ONE_TOUCH add ( SKIN_ID NUMBER  DEFAULT 1 NOT NULL);
alter table TAX_HISTORY add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

alter table TRANSACTIONS add ( UTC_OFFSET_SETTLED VARCHAR2(9) );
alter table TRANSACTIONS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

alter table USERS add ( ID_DOC_VERIFY NUMBER (1) DEFAULT 0);
alter table USERS add ( UTC_OFFSET_MODIFIED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

alter table USERS add ( LANGUAGE_ID NUMBER  DEFAULT 1  NOT NULL) ;
--ALTER TABLE USERS add CONSTRAINT FK_users_LANGUAGE_ID FOREIGN KEY (LANGUAGE_ID) REFERENCES LANGUAGES(ID);
alter table USERS add ( DOCS_REQUIRED NUMBER (1) DEFAULT 0);
alter table USERS add ( UTC_OFFSET VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );
alter table USERS add ( STATE_CODE NUMBER );
alter table USERS add ( SKIN_ID NUMBER  DEFAULT 1 NOT NULL );
--ALTER TABLE USERS add CONSTRAINT FK_users_SKIN_ID FOREIGN KEY (SKIN_ID) REFERENCES SKINS(ID);
alter table USERS add ( COUNTRY_ID NUMBER  DEFAULT 1 NOT NULL );
--ALTER TABLE USERS add CONSTRAINT FK_users_COUNTRY_ID FOREIGN KEY (COUNTRY_ID) REFERENCES COUNTRIES(ID);
alter table USERS add ( CITY_NAME VARCHAR2(120) );
alter table USERS add ( UTC_OFFSET_CREATED VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );
alter table USERS modify (MOBILE_PHONE  VARCHAR2(16) );
alter table USERS modify (LAND_LINE_PHONE VARCHAR2(16));

alter table WIRES add ( SWIFT VARCHAR2(160) );
alter table WIRES add ( BENEFICIARY_NAME VARCHAR2(160) );
alter table WIRES add ( BANK_NAME VARCHAR2(160) );
alter table WIRES add ( IBAN VARCHAR2(160) );
alter table WRITERS add ( UTC_OFFSET VARCHAR2(9) DEFAULT 'GMT+02:00'  NOT NULL );

-- add TRANSACTIONS.RATE

ALTER TABLE TRANSACTIONS
ADD ("RATE" NUMBER(10, 5) DEFAULT 0 NOT NULL)
;

COMMENT ON COLUMN TRANSACTIONS."RATE" IS 'rate between transaction currency and $'
;

-- add INVESTMENTS.RATE

ALTER TABLE INVESTMENTS
ADD ("RATE" NUMBER(10, 5) DEFAULT 0 NOT NULL)
;

COMMENT ON COLUMN TRANSACTIONS."RATE" IS 'rate between investments currency and $'
;

update CURRENCIES set IS_LEFT_SYMBOL=1 where id = 1;