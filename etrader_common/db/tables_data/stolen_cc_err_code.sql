insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '41', 'Lost Card Pickup');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '42', 'Special Pickup');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '43', 'Hot Card Pickup (if possible)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '44', 'Pickup Card');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '59', 'Suspected Fraud');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (1, '67', 'Retain Card; no rason specified');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '4', 'Retain Card.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '34', 'Suspicion of Manipulation.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '43', 'Stolen Card - pick up.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '59', 'Suspected fraud.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '351', 'Pick up card.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '352', 'Pick up card - special condition');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '353', 'Card lost - pick up.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '354', 'Stolen Card - pick up.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (2, '507', 'General risk management rejection.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '1', 'The card is blocked, confiscate it.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2', 'The card is stolen, confiscate it.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '5', 'The card is forged, confiscate it.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '423', 'The card failed the fraud detection test.');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2124', 'Lost Card, Pickup');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2125', 'Special Pickup');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2126', 'Hot Card, Pickup (if possible)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2127', 'Pickup Card');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '2134', 'Suspected Fraud');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, ' 2140', ' Retain Card; no reason specified');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8001', 'Fraud profile detected violation of blocked cards');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8002', 'Fraud profile detected violation of blocked card brands');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8003', 'Fraud profile detected violation of blocked email addresses');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8004', 'Fraud profile detected violation of blocked card bins');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8005', 'Fraud profile detected violation of blocked countries');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8006', 'Fraud profile detected violation of blocked card holder names');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8007', 'Fraud profile detected violation of blocked IP addresses');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8008', 'Fraud profile detected violation of blocked card holder IP countries');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8011', 'Fraud profile detected violation of minimum transaction amount');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8012', 'Fraud profile detected violation of maximum transaction amount');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8014', 'Fraud profile detected violation of BIN country IP country mismatch');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8015', 'Fraud profile detected violation of limit number of approved attempts using same email in a period of time (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8016', 'Fraud profile detected violation of limit number of approved attempts using same card in a period of time (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8017', 'Fraud profile detected violation of limit number of rejected attempts using same IP in a period of time (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8018', 'Fraud profile detected violation of limit interval between attempts using same card number (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8019', 'Fraud profile detected violation of limit interval between attempts using same IP (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8020', 'Fraud profile detected violation of limit number of different cards using same email in a period of time (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8021', 'Fraud profile detected violation of limit same credit card to amount in a period of time (per terminal)');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8022', 'Fraud profile detected violation of limit terminal overall monthly transactions volume in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8023', 'Fraud profile detected violation of limit terminal overall monthly transactions credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8024', 'Fraud profile detected violation of limit terminal overall monthly transactions MC volume in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8025', 'Fraud profile detected violation of limit terminal overall monthly transactions MC credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8026', 'Fraud profile detected violation of limit terminal overall monthly transactions VISA volume in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8027', 'Fraud profile detected violation of limit terminal overall monthly transactions VISA credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8028', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions volume in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8029', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8030', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions MC volume in base ');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8031', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions MC credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8032', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions Visa volume in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8033', 'Fraud profile detected violation of limit terminal overall monthly unique credit card transactions Visa credit amount in base currency');

insert into STOLEN_CC_ERR_CODE (PROVIDER_TYPE, CODE, DESCRIPTION)
values (3, '8034', 'Fraud profile detected violation of external data blocked card numbers (TC40,T849,SF)');
