REM INSERTING into MARKET_DISPLAY_GROUPS
Insert into MARKET_DISPLAY_GROUPS (ID,NAME,DISPLAY_NAME,TIME_CREATED,WRITER_ID) values (1,'US ','markets.displaygroups.us',to_timestamp('02-11-2008','DD-MM-RRRR HH24:MI:SSXFF'),1);
Insert into MARKET_DISPLAY_GROUPS (ID,NAME,DISPLAY_NAME,TIME_CREATED,WRITER_ID) values (2,'Europe','markets.displaygroups.europe',to_timestamp('02-11-2008','DD-MM-RRRR HH24:MI:SSXFF'),1);
Insert into MARKET_DISPLAY_GROUPS (ID,NAME,DISPLAY_NAME,TIME_CREATED,WRITER_ID) values (3,'Asia  Oceania','markets.groups.asiaoceania',to_timestamp('02-11-2008','DD-MM-RRRR HH24:MI:SSXFF'),1);
Insert into MARKET_DISPLAY_GROUPS (ID,NAME,DISPLAY_NAME,TIME_CREATED,WRITER_ID) values (4,'South America','markets.groups.southamerica',to_timestamp('02-11-2008','DD-MM-RRRR HH24:MI:SSXFF'),1);
Insert into MARKET_DISPLAY_GROUPS (ID,NAME,DISPLAY_NAME,TIME_CREATED,WRITER_ID) values (5,'Middle East','markets.groups.middleeast',to_timestamp('02-11-2008','DD-MM-RRRR HH24:MI:SSXFF'),1);