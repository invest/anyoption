
insert into terms_files (ID, FILE_NAME, TITLE)
values (252, 'agreement_AO_nonreg_en.html', 'AGREEMENT');

insert into terms_files (ID, FILE_NAME, TITLE)
values (253, 'annex_A_AO_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (254, 'annex_B_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (255, 'annex_C_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (256, 'asset_info_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (257, 'bonus_terms_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (258, 'agreement_AO_nonreg_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (259, 'annex_A_AO_nonreg_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (260, 'annex_B_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (261, 'annex_C_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (262, 'asset_info_nonreg_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (263, 'bonus_terms_nonreg_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (264, 'agreement_AO_nonreg_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (265, 'annex_A_AO_nonreg_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (266, 'annex_B_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (267, 'annex_C_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (268, 'asset_info_nonreg_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (269, 'bonus_terms_nonreg_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (270, 'agreement_AO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (271, 'annex_A_AO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (272, 'annex_B_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (273, 'annex_C_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (274, 'asset_info_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (275, 'bonus_terms_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (276, 'agreement_AO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (277, 'annex_A_AO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (278, 'annex_B_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (279, 'annex_C_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (280, 'asset_info_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (281, 'bonus_terms_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (282, 'agreement_AO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (283, 'annex_A_AO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (284, 'annex_B_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (285, 'annex_C_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (286, 'asset_info_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (287, 'bonus_terms_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (288, 'agreement_AO_nonreg_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (289, 'annex_A_AO_nonreg_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (290, 'annex_B_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (291, 'annex_C_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (292, 'asset_info_nonreg_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (293, 'bonus_terms_nonreg_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (294, 'agreement_AO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (295, 'annex_A_AO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (296, 'annex_B_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (297, 'annex_C_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (298, 'asset_info_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (299, 'bonus_terms_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (300, 'agreement_AO_nonreg_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (301, 'annex_A_AO_nonreg_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (302, 'annex_B_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (303, 'annex_C_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (304, 'asset_info_nonreg_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (305, 'bonus_terms_nonreg_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (306, 'agreement_AO_nonreg_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (307, 'annex_A_AO_nonreg_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (308, 'annex_B_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (309, 'annex_C_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (310, 'asset_info_nonreg_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (311, 'bonus_terms_nonreg_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (312, 'agreement_AO_nonreg_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (313, 'annex_A_AO_nonreg_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (314, 'annex_B_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (315, 'annex_C_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (316, 'asset_info_nonreg_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (317, 'bonus_terms_nonreg_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (318, 'agreement_AO_reg_en.html', 'AGREEMENT');

insert into terms_files (ID, FILE_NAME, TITLE)
values (319, 'annex_A_AO_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (320, 'annex_D_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (321, 'annex_E_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (322, 'annex_F_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (323, 'annex_G_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (324, 'annex_H_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (325, 'asset_info_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (326, 'bonus_terms_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (327, 'agreement_AO_nonreg_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (328, 'annex_A_AO_nonreg_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (329, 'annex_B_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (330, 'annex_C_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (331, 'asset_info_nonreg_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (332, 'bonus_terms_nonreg_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (333, 'agreement_AO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (334, 'annex_A_AO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (335, 'annex_D_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (336, 'annex_E_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (337, 'annex_F_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (338, 'annex_G_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (339, 'annex_H_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (340, 'asset_info_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (341, 'bonus_terms_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (342, 'agreement_AO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (343, 'annex_A_AO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (344, 'annex_D_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (345, 'annex_E_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (346, 'annex_F_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (347, 'annex_G_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (348, 'annex_H_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (349, 'asset_info_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (350, 'bonus_terms_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (351, 'agreement_AO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (352, 'annex_A_AO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (353, 'annex_D_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (354, 'annex_E_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (355, 'annex_F_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (356, 'annex_G_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (357, 'annex_H_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (358, 'asset_info_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (359, 'bonus_terms_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (360, 'agreement_AO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (361, 'annex_A_AO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (362, 'annex_D_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (363, 'annex_E_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (364, 'annex_F_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (365, 'annex_G_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (366, 'annex_H_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (367, 'asset_info_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (368, 'bonus_terms_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (369, 'agreement_AO_nonreg_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (370, 'annex_A_AO_nonreg_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (371, 'annex_B_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (372, 'annex_C_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (373, 'asset_info_nonreg_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (374, 'bonus_terms_nonreg_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (375, 'agreement_AO_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (376, 'annex_A_AO_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (377, 'annex_B_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (378, 'annex_C_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (379, 'annex_D_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (380, 'annex_E_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (381, 'annex_F_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (382, 'annex_G_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (383, 'annex_H_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (384, 'asset_info_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (385, 'bonus_terms_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (386, 'agreement_AO_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (387, 'annex_A_AO_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (388, 'annex_B_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (389, 'annex_C_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (390, 'annex_D_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (391, 'annex_E_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (392, 'annex_F_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (393, 'annex_G_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (394, 'annex_H_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (395, 'asset_info_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (396, 'bonus_terms_reg_sv.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (397, 'agreement_CO_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (398, 'annex_A_CO_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (399, 'agreement_CO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (400, 'annex_A_CO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (401, 'agreement_CO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (402, 'annex_A_CO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (403, 'agreement_CO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (404, 'annex_A_CO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (405, 'agreement_CO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (406, 'annex_A_CO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (407, 'agreement_CO_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (408, 'annex_A_CO_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (409, 'agreement_CO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (410, 'annex_A_CO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (411, 'agreement_CO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (412, 'annex_A_CO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (413, 'agreement_CO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (414, 'annex_A_CO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (415, 'agreement_CO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (416, 'annex_A_CO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (417, 'risk_disclosure_AO_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (418, 'risk_disclosure_CO_nonreg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (419, 'risk_disclosure_AO_nonreg_tr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (420, 'risk_disclosure_AO_nonreg_ar.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (421, 'risk_disclosure_AO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (422, 'risk_disclosure_CO_nonreg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (423, 'risk_disclosure_AO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (424, 'risk_disclosure_CO_nonreg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (425, 'risk_disclosure_AO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (426, 'risk_disclosure_CO_nonreg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (427, 'risk_disclosure_AO_nonreg_ru.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (428, 'risk_disclosure_AO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (429, 'risk_disclosure_CO_nonreg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (430, 'risk_disclosure_AO_nonreg_en_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (431, 'risk_disclosure_AO_nonreg_es_us.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (432, 'risk_disclosure_AO_nonreg_zh.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (433, 'risk_disclosure_AO_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (434, 'risk_disclosure_CO_reg_en.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (435, 'risk_disclosure_AO_nonreg_kr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (436, 'risk_disclosure_AO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (437, 'risk_disclosure_CO_reg_es.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (438, 'risk_disclosure_AO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (439, 'risk_disclosure_CO_reg_de.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (440, 'risk_disclosure_AO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (441, 'risk_disclosure_CO_reg_it.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (442, 'risk_disclosure_AO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (443, 'risk_disclosure_CO_reg_fr.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (444, 'risk_disclosure_AO_nonreg_168QIQUAN.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (445, 'risk_disclosure_AO_reg_nl.html', null);

insert into terms_files (ID, FILE_NAME, TITLE)
values (446, 'risk_disclosure_AO_reg_sv.html', null);


