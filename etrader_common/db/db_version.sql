-****************
--BEGIN 17.6.1
--****************
-------------------------------------------------------------------------------------------------------------------------------------------
-- START Jamal
-- BAC-3647 BE -> Transactions -> add option for "CC Country" filter
-------------------------------------------------------------------------------------------------------------------------------------------
--run pkg
@packages/pkg_transactions_backend;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END
-- BAC-3647 BE -> Transactions -> add option for "CC Country" filter
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Youriy
-- BAC-4165 - [SQL] Replace timezone with countries current time in the country column in CallCenter&BackOffice screens
-------------------------------------------------------------------------------------------------------------------------------------------
-- Creating a temporary table.
CREATE TABLE ETRADER.TIME_ZONES_BAC_4165
(COUNTRY_CODE      VARCHAR2(2 CHAR) NOT NULL
,MAIN_TIME_ZONE    NUMBER(1)
,COORDINATES       VARCHAR2(20 CHAR)
,TIME_ZONE         VARCHAR2(50 CHAR) NOT NULL
,COMMENTS          VARCHAR2(100 CHAR)
);

-- Importing the input file with the time zones.
@table_data/BAC-4165_SQL_Input_Timezones.sql

ALTER TABLE ETRADER.COUNTRIES ADD (TIME_ZONE VARCHAR2(50 CHAR));

----------
-- Setting the value of the new column TIME_ZONE in the table COUNTRIES.
--
set lines 220 pages 50000 time on timing on verify off
set serveroutput on size 1000000

DECLARE

  ln_processed NUMBER := 0;
  ln_updated   NUMBER := 0;

BEGIN

  FOR c_update_time_zone IN
    (SELECT COUNTRY_CODE
           ,TIME_ZONE
       FROM ETRADER.TIME_ZONES_BAC_4165
      WHERE MAIN_TIME_ZONE = 1
    )
  LOOP
    UPDATE ETRADER.COUNTRIES C
       SET C.TIME_ZONE = c_update_time_zone.TIME_ZONE
     WHERE C.A2 = c_update_time_zone.COUNTRY_CODE;

    ln_updated   := ln_updated + SQL%ROWCOUNT;
    ln_processed := ln_processed + 1;

  END LOOP;

  COMMIT;

  DBMS_OUTPUT.PUT_LINE('Processed: ' || TO_CHAR(ln_processed, '999') || ' time zones.');
  DBMS_OUTPUT.PUT_LINE('Updated..: ' || TO_CHAR(ln_updated  , '999') || ' countries.');

END;
/
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Youriy
-- BAC-4165 - [SQL] Replace timezone with countries current time in the country column in CallCenter&BackOffice screens


-------------------------------------------------------------------------------------------------------------------------------------------
-- START Jamal
-- BAC-3275 [server] Add CC Name column to customer's file table 
-------------------------------------------------------------------------------------------------------------------------------------------
--run pkg
@packages/PKG_USER_DOCUMENTS;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END
-- BAC-3275 [server] Add CC Name column to customer's file table 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------
---Pavel Tabakov
-- Arabic skin DB configuration
-------------------------------

DECLARE
  l_new_skin           skins%ROWTYPE;
  l_src_config_skin_id NUMBER := 25;
  l_tmp_id             NUMBER;
BEGIN
  SELECT * INTO l_new_skin FROM skins WHERE id = l_src_config_skin_id;

  --- update fields for the new skin
  l_new_skin.id                  := 30;
  l_new_skin.visible_type_id     := 2;
  l_new_skin.default_currency_id := 3;
  l_new_skin.default_country_id  := 180; -- Saudi Arabia currently blocked!!!
  l_new_skin.default_language_id := 7;
  l_new_skin.type_id             := 4;

  INSERT INTO skins VALUES l_new_skin;

  UPDATE skins
  SET display_name     = 'skins.ar'
     ,locale_subdomain = 'ar'
     ,subdomain        = 'ar'
     ,NAME             = 'Arabic'
  WHERE id = 30;

  INSERT INTO asset_indexs_info
    (id
    ,market_id
    ,skin_id
    ,market_description
    ,additional_text
    ,page_title
    ,page_keywords
    ,page_description
    ,market_description_page)
    (SELECT seq_asset_indexs_info.nextval
           ,market_id
           ,l_new_skin.id
           ,market_description
           ,additional_text
           ,page_title
           ,page_keywords
           ,page_description
           ,market_description_page
     FROM asset_indexs_info
     WHERE skin_id = l_src_config_skin_id);

  INSERT INTO bonus_skins
    (id
    ,bonus_id
    ,skin_id)
    (SELECT seq_bonus_skins.nextval
           ,bonus_id
           ,l_new_skin.id
     FROM bonus_skins
     WHERE skin_id = l_src_config_skin_id);

  INSERT INTO mailbox_templates
    (id
    ,NAME
    ,subject
    ,type_id
    ,time_created
    ,writer_id
    ,sender_id
    ,template
    ,is_high_priority
    ,skin_id
    ,language_id
    ,popup_type_id)
    SELECT seq_mailbox_templates.nextval
          ,NAME
          ,subject
          ,type_id
          ,SYSDATE
          ,writer_id
          ,sender_id
          ,template
          ,is_high_priority
          ,l_new_skin.id
          ,l_new_skin.default_language_id
          ,popup_type_id
    FROM mailbox_templates
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO market_name_skin
    (id
    ,market_id
    ,skin_id
    ,NAME
    ,short_name)
    SELECT seq_market_name_skin.nextval
          ,market_id
          ,l_new_skin.id
          ,NAME
          ,short_name
    FROM market_name_skin
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO messages
    (id
    ,text
    ,web_screen
    ,start_eff_date
    ,end_eff_date
    ,language_id
    ,skin_id)
    SELECT seq_messages.nextval
          ,text
          ,web_screen
          ,start_eff_date
          ,end_eff_date
          ,l_new_skin.default_language_id
          ,l_new_skin.id
    FROM messages
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO one_touch_markets_skins
    (id
    ,market_id
    ,skin_id
    ,priority)
    SELECT seq_one_touch_markets_skins.nextval
          ,market_id
          ,l_new_skin.id
          ,priority
    FROM one_touch_markets_skins
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO populations
    (id
    ,NAME
    ,skin_id
    ,language_id
    ,start_date
    ,end_date
    ,threshold
    ,num_issues
    ,is_active
    ,refresh_time_period
    ,number_of_minutes
    ,number_of_days
    ,writer_id
    ,time_created
    ,balance_amount
    ,last_invest_day
    ,population_type_id
    ,number_of_deposits
    ,not_interested_delay
    ,dept_id
    ,max_last_login_time_days
    ,last_sales_deposit_days)
    SELECT seq_populations.nextval
          ,NAME
          ,l_new_skin.id
          ,l_new_skin.default_language_id
          ,start_date
          ,end_date
          ,threshold
          ,num_issues
          ,is_active
          ,refresh_time_period
          ,number_of_minutes
          ,number_of_days
          ,writer_id
          ,time_created
          ,balance_amount
          ,last_invest_day
          ,population_type_id
          ,number_of_deposits
          ,not_interested_delay
          ,dept_id
          ,max_last_login_time_days
          ,last_sales_deposit_days
    FROM populations
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO skin_currencies
    (id
    ,skin_id
    ,currency_id
    ,limit_id
    ,is_default
    ,cash_to_points_rate)
    SELECT seq_skin_currencies.nextval
          ,l_new_skin.id
          ,currency_id
          ,limit_id
          ,is_default
          ,cash_to_points_rate
    FROM skin_currencies
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO limitation_deposits
    (id
    ,is_remarketing
    ,affiliate_id
    ,campaign_id
    ,payment_recipient_id
    ,country_id
    ,currency_id
    ,skin_id
    ,is_active
    ,group_id
    ,minimum_first_deposit
    ,minimum_deposit
    ,comments
    ,writer_id
    ,time_updated)
    (SELECT seq_limitation_deposits.nextval
           ,is_remarketing
           ,affiliate_id
           ,campaign_id
           ,payment_recipient_id
           ,country_id
           ,currency_id
           ,l_new_skin.id
           ,is_active
           ,group_id
           ,minimum_first_deposit
           ,minimum_deposit
           ,comments
           ,writer_id
           ,time_updated
     FROM limitation_deposits
     WHERE skin_id = l_src_config_skin_id
     AND currency_id IN (SELECT currency_id
                        FROM skin_currencies
                        WHERE skin_id = l_new_skin.id));

  INSERT INTO skin_languages
    (id
    ,skin_id
    ,language_id)
    SELECT seq_skin_languages.nextval
          ,l_new_skin.id
          ,l_new_skin.default_language_id
    FROM skin_languages
    WHERE skin_id = l_src_config_skin_id;

  FOR i IN (SELECT *
            FROM skin_market_groups
            WHERE skin_id = l_src_config_skin_id)
  LOOP
    INSERT INTO skin_market_groups
      (id
      ,skin_id
      ,market_group_id
      ,priority)
    VALUES
      (seq_skin_market_groups.nextval
      ,l_new_skin.id
      ,i.market_group_id
      ,i.priority)
    RETURNING id INTO l_tmp_id;
  
    INSERT INTO skin_market_group_markets
      (id
      ,skin_market_group_id
      ,market_id
      ,group_priority
      ,home_page_priority
      ,skin_display_group_id
      ,ticker_priority
      ,banners_priority)
      SELECT seq_skin_market_group_markets.nextval
            ,l_tmp_id
            ,market_id
            ,group_priority
            ,home_page_priority
            ,skin_display_group_id
            ,ticker_priority
            ,banners_priority
      FROM skin_market_group_markets
      WHERE skin_market_group_id = i.id;
  END LOOP;

  INSERT INTO terms
    (id
    ,platform_id
    ,skin_id
    ,part_id
    ,file_id)
    SELECT seq_terms.nextval
          ,platform_id
          ,l_new_skin.id
          ,part_id
          ,file_id
    FROM terms
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO skin_payment_methods
    (id
    ,skin_id
    ,transaction_type_id)
    SELECT seq_skin_payment_methods.nextval
          ,l_new_skin.id
          ,transaction_type_id
    FROM skin_payment_methods
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO skin_templates
    (id
    ,template_id
    ,skin_id)
    SELECT seq_skin_templates.nextval
          ,template_id
          ,l_new_skin.id
    FROM skin_templates
    WHERE skin_id = l_src_config_skin_id;

  INSERT INTO skin_url_country_map
    (id
    ,skin_id
    ,url_id
    ,country_id)
    SELECT seq_skin_url_country_map.nextval
          ,l_new_skin.id
          ,url_id
          ,country_id
    FROM skin_url_country_map sucm
    WHERE sucm.id = l_src_config_skin_id;

  ---insert into currencies_rules ----???????

  INSERT INTO writers_skin
    (id
    ,skin_id
    ,writer_id
    ,assign_limit)
    SELECT seq_writers_skin.nextval
          ,l_new_skin.id
          ,writer_id
          ,assign_limit
    FROM writers_skin
    WHERE skin_id = l_src_config_skin_id;

END;
/
-------------------------------
---Pavel Tabakov
-- Arabic skin DB configuration
-------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Jamal
-- BAC-4013 Send email notification if customer reached 2k+ total deposit
-------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE users_auto_mail
  ADD comments varchar2(500);
  
comment on column users_auto_mail.reason_id is '1 - After FTD Email for Docs; 2 - Notife managers by email when user reached total su, deposits';

update jobs j
set j.config = 'email_to=anna.mavridou@ouroboros.com.cy;gadiel.haber@ouroboros.com.cy'
where j.job_class = 'il.co.etrader.jobs.UsersAutoMailJob';

--run pkg
@packages/PKG_USERS_AUTO_MAIL;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END
-- BAC-4013 Send email notification if customer reached 2k+ total deposit
-------------------------------------------------------------------------------------------------------------------------------------------



--****************
--END 17.6.1
--****************
-- DDL generated by DBeaver
-- WARNING: It may differ from actual native database DDL
CREATE TABLE ETRADER.INVEST_MIGRATION (
	ID NUMBER(22,0) DEFAULT "ETRADER"."SEQ_INVEST_MIGRATION"."NEXTVAL",
	USER_ID NUMBER(22,0),
	DATE_INSERTED DATE DEFAULT sysdate ,
	DATE_PROCESSED DATE,
	TARGET_PLATFORM NUMBER(2,0),
	CONSTRAINT INVEST_MIGRATION_PK PRIMARY KEY (ID),
	CONSTRAINT INVEST_MIGRATION_UN UNIQUE (USER_ID),
	CONSTRAINT SYS_C00319994 CHECK (ID),
	CONSTRAINT SYS_C00319995 CHECK (USER_ID),
	CONSTRAINT SYS_C00319996 CHECK (DATE_INSERTED),
	CONSTRAINT SYS_C00320001 CHECK (TARGET_PLATFORM),
	CONSTRAINT INVEST_MIGRATION_USERS_FK FOREIGN KEY (USER_ID) REFERENCES ETRADER.USERS(ID)
) ;
CREATE UNIQUE INDEX INVEST_MIGRATION_PK ON ETRADER.INVEST_MIGRATION (ID) ;
CREATE UNIQUE INDEX INVEST_MIGRATION_UN ON ETRADER.INVEST_MIGRATION (USER_ID) ;

-- DDL generated by DBeaver
-- WARNING: It may differ from actual native database DDL
CREATE TABLE ETRADER.INVEST_MIGRATION_USERS (
	ID NUMBER(22,0),
	USER_ID NUMBER(22,0),
	FIRST_NAME VARCHAR2(20),
	LAST_NAME VARCHAR2(20),
	EMAIL VARCHAR2(50),
	PHONE VARCHAR2(25),
	COUNTRY VARCHAR2(40),
	CURRENCY VARCHAR2(20),
	BALANCE NUMBER(22,0),
	ACCEPTED_MAILING NUMBER(1,0) DEFAULT 0  ,
	ACCEPTED_TRANSFER NUMBER(1,0) DEFAULT 0  ,
	TIME_PROCESSED DATE,
	LANGUAGE VARCHAR2(40),
	CONSTRAINT INVEST_MIGRATION_USERS_UN UNIQUE (USER_ID),
	CONSTRAINT SYS_C00320132 CHECK (ID),
	CONSTRAINT SYS_C00320133 CHECK (USER_ID),
	CONSTRAINT SYS_C00320138 CHECK (COUNTRY),
	CONSTRAINT SYS_C00320139 CHECK (CURRENCY)
) ;
CREATE UNIQUE INDEX INVEST_MIGRATION_USERS_UN ON ETRADER.INVEST_MIGRATION_USERS (USER_ID) ;



create public synonym INVEST_MIGRATION_USERS for ETRADER.INVEST_MIGRATION_USERS;
GRANT ALL ON ETRADER.INVEST_MIGRATION_USERS to ETRADER_WEB_CONNECT;
create public synonym INVEST_MIGRATION FOR etrader.INVEST_MIGRATION;
GRANT ALL ON ETRADER.INVEST_MIGRATION to ETRADER_WEB_CONNECT;

INSERT INTO ETRADER.ISSUE_SUBJECTS
(ID, NAME, IS_DISPLAYED, REAL_NAME)
VALUES(82, 'migration.suspend', 1, 'Migration Suspend');

INSERT INTO ETRADER.ISSUE_ACTION_TYPES
(ID, NAME, CHANNEL_ID, REACHED_STATUS_ID, IS_ACTIVE, DEPOSIT_SEARCH_TYPE, IS_TRANSACTION_ISSUES_S_ACTION, IS_TRACKING_ACTION, IS_CLOSE_ACCOUNT, TMP_REACTION_ACTIVITY_ID)
VALUES(94, 'issue.action.migration.suspended', 7, NULL, 1, 0, 0, 0, 0, NULL);

INSERT INTO ETRADER.WRITERS
(ID, USER_NAME, PASSWORD, FIRST_NAME, LAST_NAME, STREET, CITY_ID, ZIP_CODE, EMAIL, COMMENTS, TIME_BIRTH_DATE, MOBILE_PHONE, LAND_LINE_PHONE, IS_ACTIVE, STREET_NO, UTC_OFFSET, GROUP_ID, IS_SUPPORT_ENABLE, DEPT_ID, SALES_TYPE, NICK_NAME_FIRST, NICK_NAME_LAST, AUTO_ASSIGN, CMS_LOGIN, LAST_FAILED_TIME, FAILED_COUNT, SALES_TYPE_DEPT_ID, EMP_ID, ROLE_ID, TIME_CREATED, IS_PASSWORD_RESET, WRITER_CP_GROUP_ID, SITE)
VALUES(40000, 'InvMigration', 'InvMigration', 'InvMigration', 'InvMigration', 'na', 1, NULL, 'na@mail.bg', NULL, TIMESTAMP '2017-04-06 00:00:00.000000', NULL, NULL, 1, '0', 'GMT+00:00', 0, 1, 9, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

INSERT INTO ETRADER.USERS_SUSPENDED_REASONS
(ID, REASON_NAME, COMMENTS)
VALUES(7, 'Invest migration', 'User choose to migrate to IDC');

CREATE SEQUENCE ETRADER.SEQ_INVEST_MIGRATION_USERS INCREMENT BY 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER  ;
create public synonym SEQ_INVEST_MIGRATION_USERS for ETRADER.SEQ_INVEST_MIGRATION_USERS;

ALTER TABLE ETRADER.INVEST_MIGRATION_USERS ADD CONSTRAINT INVEST_MIGRATION_USERS_UN UNIQUE (USER_ID) ;

INSERT INTO ETRADER.JOBS
(ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
VALUES(61, 0, TIMESTAMP '2017-10-13 11:22:00.000000', 'Local-SERVER', 1, 'Transfer users', 'com.anyoption.backend.jobs.InvestMigrationJob', 'emails=pavlin.m@invest.com,fileName=ao2idc', 'Local-SERVER', 'backend');
