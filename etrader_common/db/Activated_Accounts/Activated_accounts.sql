select  to_char('USER ID'), to_char('USER NAME'),to_char('RESON'),to_char('DATE CLOSE'),to_char('WRITER CLOSE'),to_char('WRITER OPEN'), to_char('OPEN COMMENT')
from dual
union all
SELECT DISTINCT to_char(usr.id),
        to_char(usr.user_name),
        to_char(iatype.name),
        to_char(iaclose.action_time, 'dd-mm-yyyy'),
        to_char(wclose.first_name||' '||wclose.last_name),
        to_char(w.first_name||' '||w.last_name),
        to_char(iactions.comments)        
FROM 
        users usr,       
        issues issue,
        issue_actions iactions,
        writers w,
        issues iclose,
        issue_actions iaclose,
        writers wclose,
        issue_action_types iatype
WHERE
       issue.user_id = usr.id and
       iactions.issue_id = issue.id and
       iactions.writer_id = w.id and
        iactions.issue_action_type_id = 12 and
        iactions.action_time between ADD_MONTHS(SYSDATE, -1) and SYSDATE and
        iaclose.id in (select ia1.issue_id from issue_actions ia1 where id in
          (select max(ia2.id) from issue_actions ia2, issues i2 where ia2.issue_id = i2.id and i2.user_id = usr.id and ia2.issue_action_type_id in (1,9,10,11,13,39)) and
          ia1.action_time< iactions.action_time and ia1.issue_action_type_id in (1,9,10,11,13))
        and iaclose.issue_id=iclose.id 
        and iaclose.writer_id=wclose.id
        and iaclose.issue_action_type_id=iatype.id ;