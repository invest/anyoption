CREATE OR REPLACE FUNCTION Week_first_working_day (p_exchange_id IN NUMBER, p_add_weeks IN NUMBER) RETURN DATE AS
	numOfHolidays INTEGER;
	TYPE holidayCursorType IS REF CURSOR RETURN exchange_holidays%ROWTYPE;
	holiday_ct holidayCursorType;
	holiday exchange_holidays%ROWTYPE;
	wkSun DATE;
	wkMon DATE;
	first_working_day DATE;
BEGIN
	wkSun := trunc(current_date + (p_add_weeks * 7) + 1 - to_char(current_date, 'D'));
	wkMon := wkSun + 1;
	first_working_day := NULL;
	IF ((p_exchange_id = 1) OR  (p_exchange_id = 17) or (p_exchange_id = 18))  THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkSun AND
			trunc(holiday) <= wkSun + 4 AND
			is_half_day = 0;
		IF numOfHolidays < 5 THEN
			FOR i IN 0..4
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkSun + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				IF holiday_ct%NOTFOUND THEN
					first_working_day := wkSun + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						first_working_day := wkSun + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
	IF NOT ((p_exchange_id = 1) OR (p_exchange_id = 17) OR (p_exchange_id = 16) OR (p_exchange_id = 18)) THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkMon AND
			trunc(holiday) <= wkMon + 4 AND
			is_half_day = 0;
		IF numOfHolidays < 5 THEN
			FOR i IN 0..4
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkMon + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				if holiday_ct%NOTFOUND THEN -- if not a holiday
					first_working_day := wkMon + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						first_working_day := wkMon + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
	IF (p_exchange_id = 16) THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkSun AND
			trunc(holiday) <= wkSun + 3 AND
			is_half_day = 0;
		IF numOfHolidays < 4 THEN
			FOR i IN 0..3
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkSun + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				IF holiday_ct%NOTFOUND THEN
					first_working_day := wkSun + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						first_working_day := wkSun + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
	RETURN(first_working_day);
END;

create or replace FUNCTION Week_last_working_day (p_exchange_id IN NUMBER, p_add_weeks IN NUMBER) RETURN DATE AS
	numOfHolidays INTEGER;
	TYPE holidayCursorType IS REF CURSOR RETURN exchange_holidays%ROWTYPE;
	holiday_ct holidayCursorType;
	holiday exchange_holidays%ROWTYPE;
	wkSun DATE;
	wkMon DATE;
	last_working_day DATE;
BEGIN
	wkSun := trunc(current_date + (p_add_weeks * 7) + 1 - to_char(current_date, 'D'));
	wkMon := wkSun + 1;
	last_working_day := NULL;
	IF ((p_exchange_id = 1) OR  (p_exchange_id = 17) or (p_exchange_id = 18))  THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkSun AND
			trunc(holiday) <= wkSun + 4 AND
			is_half_day = 0;
		IF numOfHolidays < 5 THEN
			FOR i IN REVERSE 0..4
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkSun + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				IF holiday_ct%NOTFOUND THEN
					last_working_day := wkSun + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						last_working_day := wkSun + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
	IF NOT ((p_exchange_id = 1) OR (p_exchange_id = 17) OR (p_exchange_id = 16) OR (p_exchange_id = 18)) THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkMon AND
			trunc(holiday) <= wkMon + 4 AND
			is_half_day = 0;
		IF numOfHolidays < 5 THEN
			FOR i IN REVERSE 0..4
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkMon + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				if holiday_ct%NOTFOUND THEN -- if not a holiday
					last_working_day := wkMon + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						last_working_day := wkMon + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
        IF (p_exchange_id = 16)  THEN
		SELECT
			COUNT(id) INTO numOfHolidays
		FROM
			exchange_holidays
		WHERE
			exchange_id = p_exchange_id AND
			trunc(holiday) >= wkSun AND
			trunc(holiday) <= wkSun + 3 AND
			is_half_day = 0;
		IF numOfHolidays < 4 THEN
			FOR i IN REVERSE 0..3
			LOOP
				OPEN holiday_ct FOR
				SELECT
					*
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = wkSun + i; -- AND
--					is_half_day = 0;
				FETCH holiday_ct INTO holiday;
				IF holiday_ct%NOTFOUND THEN
					last_working_day := wkSun + i;
					CLOSE holiday_ct;
					EXIT;
				ELSE
					IF holiday.is_half_day = 1 THEN -- working day (at least half)
						last_working_day := wkSun + i;
						CLOSE holiday_ct;
						EXIT;
					END IF;
				END IF;
				CLOSE holiday_ct;
			END LOOP;
		END IF;
	END IF;
	RETURN(last_working_day);
END;

create or replace FUNCTION Month_last_working_day (p_exchange_id IN NUMBER, p_add_months IN NUMBER) RETURN DATE AS
	monthFirstDay DATE;
	monthLastDay DATE;
	halfDayHoliday exchange_holidays.is_half_day%TYPE;
	lastWorkingDay DATE;
BEGIN
	monthFirstDay := to_date(to_char(add_months(current_date, p_add_months), 'yyyy-mm') || '-01', 'yyyy-mm-dd');
	monthLastDay := last_day(monthFirstDay);
	lastWorkingDay := NULL;

	FOR i IN 0..30
	LOOP
		IF (((p_exchange_id = 1 OR  (p_exchange_id = 17) or (p_exchange_id = 18)) AND NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7) OR
			(NOT ((p_exchange_id = 1) OR (p_exchange_id = 17) OR (p_exchange_id = 16) OR (p_exchange_id = 18)) AND NOT to_char(monthLastDay - i, 'D') = 7 AND NOT to_char(monthLastDay - i, 'D') = 1) OR
                        ((p_exchange_id = 16) AND NOT to_char(monthLastDay - i, 'D') = 5 AND  NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7)) THEN
			BEGIN
				SELECT
					is_half_day INTO halfDayHoliday
				FROM
					exchange_holidays
				WHERE
					exchange_id = p_exchange_id AND
					trunc(holiday) = monthLastDay - i;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					halfDayHoliday := NULL;
			END;

			IF halfDayHoliday IS NULL THEN
				-- full working day
				lastWorkingDay := monthLastDay - i;
				EXIT;
			ELSE
				IF halfDayHoliday = 1 THEN
					-- working day... at least half
					lastWorkingDay := monthLastDay - i;
					EXIT;
				END IF;
			END IF;
		END IF;
	END LOOP;
	RETURN(lastWorkingDay);
END;


create or replace PROCEDURE Create_week_opportunities AS
	crrOppTmpl opportunity_templates%ROWTYPE;
	crrMarket markets%ROWTYPE;
	crrDayOfWeek INTEGER;
	numOfHolidays INTEGER;
	TYPE holidayCursorType IS REF CURSOR RETURN exchange_holidays%ROWTYPE;
	holiday_ct holidayCursorType;
	holiday exchange_holidays%ROWTYPE;
	halfDayCloseTime opportunity_templates.time_est_closing%TYPE;
	halfDayLastInvest opportunity_templates.time_last_invest%TYPE;
	toInsTimeFirstInvest TIMESTAMP WITH TIME ZONE;
	toInsTimeEstClosing TIMESTAMP WITH TIME ZONE;
	toInsTimeLastInvest TIMESTAMP WITH TIME ZONE;
	wkSun DATE;
	wkMon DATE;
	wkTue DATE;
	wkWed DATE;
	wkThu DATE;
	wkFri DATE;
	wkSat DATE;
	isHoliday INTEGER;
	toIns opportunities%ROWTYPE;
BEGIN
	crrDayOfWeek := to_char(current_date, 'D');
	wkSun := trunc(current_date + 8 - crrDayOfWeek);
	wkMon := wkSun + 1;
	wkTue := wkMon + 1;
	wkWed := wkTue + 1;
	wkThu := wkWed + 1;
	wkFri := wkThu + 1;
	wkSat := wkFri + 1;
	FOR item IN (
		SELECT
			A.market_id,
			A.time_first_invest,
			A.time_est_closing,
			A.time_last_invest,
			A.time_zone,
			A.odds_type_id,
			A.writer_id,
			A.opportunity_type_id,
			A.scheduled,
			A.deduct_win_odds,
                        A.is_open_graph,
			B.exchange_id,
			B.max_exposure
		FROM
			opportunity_templates A,
			markets B
		WHERE
			A.market_id = B.id AND
			A.is_active = 1 AND
			A.scheduled = 3
        )
	LOOP
		IF ((item.exchange_id = 1) OR  (item.exchange_id = 17) or (item.exchange_id = 18)) THEN
			SELECT
				COUNT(id) INTO numOfHolidays
			FROM
				exchange_holidays
			WHERE
				exchange_id = item.exchange_id AND
				trunc(holiday) >= wkSun AND
				trunc(holiday) <= wkThu AND
				is_half_day = 0;
			IF numOfHolidays < 5 THEN
				FOR i IN 0..4
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkSun + i; -- AND
--						is_half_day = 0;
					FETCH holiday_ct INTO holiday;
					IF holiday_ct%NOTFOUND THEN
						toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
				FOR i IN REVERSE 0..4
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkSun + i;
                                        FETCH holiday_ct INTO holiday;
					IF holiday_ct%NOTFOUND THEN
						toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							SELECT
								time_est_closing,
								time_last_invest
							INTO
								halfDayCloseTime,
								halfDayLastInvest
							FROM
								opportunity_templates
							WHERE
								market_id = item.market_id AND
								scheduled = 2 AND
								is_half_day = 1 AND
								is_active = 1;
							toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
			ELSE
				GOTO end_loop;
			END IF;
		END IF;
		IF NOT ((item.exchange_id = 1) OR (item.exchange_id = 17) OR (item.exchange_id = 16) OR (item.exchange_id = 18)) THEN
			SELECT
				COUNT(id) INTO numOfHolidays
			FROM
				exchange_holidays
			WHERE
				exchange_id = item.exchange_id AND
				trunc(holiday) >= wkMon AND
				trunc(holiday) <= wkFri AND
				is_half_day = 0;
			IF numOfHolidays < 5 THEN
				FOR i IN 0..4
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkMon + i; -- AND
--						is_half_day = 0;
					FETCH holiday_ct INTO holiday;
					if holiday_ct%NOTFOUND THEN -- if not a holiday
                                        -- if the jakarta week doesn't open on friday it should open by the wekly template close time
                                        -- if its friday open it by daily half day template
                                                IF item.market_id = 370 AND i = 4 THEN
							-- if the jakarta week doesn't open on friday it should open by the wekly template close time
                                                        -- if its friday open it by daily half day template
							SELECT
								to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
							INTO
								toInsTimeFirstInvest
							FROM
								opportunity_templates
							WHERE
								market_id = 370 AND
								scheduled = 2 AND
								is_half_day = 1;
						ELSE
                                                        toInsTimeFirstInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                                                END IF;
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							toInsTimeFirstInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
				FOR i IN REVERSE 0..4
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkMon + i;
					FETCH holiday_ct INTO holiday;
					IF holiday_ct%NOTFOUND THEN -- if not a holiday
						IF item.market_id = 15 AND i != 4 THEN
							-- if the ILS week doesn't close on friday it should close by the day template close time
							-- the week template is done for friday when it close at 13:00
							SELECT
								to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_est_closing || time_zone, 'yyyy-mm-dd hh24:mi TZR'),
								to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
							INTO
								toInsTimeEstClosing,
								toInsTimeLastInvest
							FROM
								opportunity_templates
							WHERE
								market_id = 15 AND
								scheduled = 2 AND
								is_half_day = 0;
						ELSE
							toInsTimeEstClosing := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							toInsTimeLastInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						END IF;
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							SELECT
								time_est_closing,
								time_last_invest
							INTO
								halfDayCloseTime,
								halfDayLastInvest
							FROM
								opportunity_templates
							WHERE
								market_id = item.market_id AND
								scheduled = 2 AND
								is_half_day = 1 AND
								is_active = 1;
							toInsTimeEstClosing := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							toInsTimeLastInvest := to_timestamp_tz(to_char(wkMon + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
			ELSE
				GOTO end_loop;
			END IF;
		END IF;

		IF (item.exchange_id = 16) THEN
			SELECT
				COUNT(id) INTO numOfHolidays
			FROM
				exchange_holidays
			WHERE
				exchange_id = item.exchange_id AND
				trunc(holiday) >= wkSun AND
				trunc(holiday) <= wkWed AND
				is_half_day = 0;
			IF numOfHolidays < 4 THEN
				FOR i IN 0..3
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkSun + i; -- AND
--						is_half_day = 0;
					FETCH holiday_ct INTO holiday;
					if holiday_ct%NOTFOUND THEN -- if not a holiday
						toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							toInsTimeFirstInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_first_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
				FOR i IN REVERSE 0..3
				LOOP
					OPEN holiday_ct FOR
					SELECT
						*
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = wkSun + i;
					FETCH holiday_ct INTO holiday;
					IF holiday_ct%NOTFOUND THEN -- if not a holiday
						toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						CLOSE holiday_ct;
						EXIT;
					ELSE
						IF holiday.is_half_day = 1 THEN
							SELECT
								time_est_closing,
								time_last_invest
							INTO
								halfDayCloseTime,
								halfDayLastInvest
							FROM
								opportunity_templates
							WHERE
								market_id = item.market_id AND
								scheduled = 2 AND
								is_half_day = 1 AND
								is_active = 1;
							toInsTimeEstClosing := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							toInsTimeLastInvest := to_timestamp_tz(to_char(wkSun + i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
							CLOSE holiday_ct;
							EXIT;
						END IF;
					END IF;
					CLOSE holiday_ct;
				END LOOP;
			ELSE
				GOTO end_loop;
			END IF;
		END IF;

		-- check that the last working day of this week is not the same as the last working day of this month
		-- if yes don't create this opp
		IF trunc(toInsTimeEstClosing) = Month_last_working_day(item.exchange_id, 0) THEN
			GOTO end_loop;
		END IF;

		SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM dual;
		toIns.market_id := item.market_id;
		toIns.time_created := current_timestamp;
		toIns.time_first_invest := toInsTimeFirstInvest;
		toIns.time_est_closing := toInsTimeEstClosing;
		toIns.time_last_invest := toInsTimeLastInvest;
		toIns.is_published := 0;
		toIns.is_settled := 0;
		toIns.odds_type_id := item.odds_type_id;
		toIns.writer_id := item.writer_id;
		toIns.opportunity_type_id := item.opportunity_type_id;
		toIns.is_disabled := 0;
		toIns.is_disabled_service := 0;
		toIns.is_disabled_trader := 0;
		toIns.scheduled := item.scheduled;
		toIns.deduct_win_odds := item.deduct_win_odds;
                toIns.is_open_graph := item.is_open_graph;
		toIns.max_exposure := item.max_exposure;
		INSERT INTO opportunities VALUES toIns;

--		DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
		<<end_loop>>
		NULL;
	END LOOP;
END;



create or replace PROCEDURE Create_month_opportunities AS
	monthFirstDay DATE;
	monthLastDay DATE;
	toInsTimeFirstInvest TIMESTAMP WITH TIME ZONE;
	toInsTimeEstClosing TIMESTAMP WITH TIME ZONE;
	toInsTimeLastInvest TIMESTAMP WITH TIME ZONE;
	halfDayHoliday exchange_holidays.is_half_day%TYPE;
	halfDayCloseTime opportunity_templates.time_est_closing%TYPE;
	halfDayLastInvest opportunity_templates.time_last_invest%TYPE;
	toIns opportunities%ROWTYPE;
BEGIN
	monthFirstDay := to_date(to_char(add_months(current_date, 1), 'yyyy-mm') || '-01', 'yyyy-mm-dd');
	monthLastDay := last_day(monthFirstDay);

	FOR item IN (
		SELECT
			A.market_id,
			A.time_first_invest,
			A.time_est_closing,
			A.time_last_invest,
			A.time_zone,
			A.odds_type_id,
			A.writer_id,
			A.opportunity_type_id,
			A.scheduled,
			A.deduct_win_odds,
                        A.is_open_graph,
			B.exchange_id,
			B.max_exposure
		FROM
			opportunity_templates A,
			markets B
		WHERE
			A.market_id = B.id AND
			A.is_active = 1 AND
			A.scheduled = 4
        )
	LOOP
		-- we asume that there is at least 1 working day in the month

		-- find the first working day of the month and set the time first invest to be on it
		FOR i IN 0..30
		LOOP
			IF ((item.exchange_id = 1 OR  item.exchange_id = 17 OR  item.exchange_id = 18) AND NOT to_char(monthFirstDay + i, 'D') = 6 AND NOT to_char(monthFirstDay + i, 'D') = 7) OR
				(NOT (item.exchange_id = 1 OR  item.exchange_id = 17 OR item.exchange_id = 16 OR item.exchange_id = 18) AND NOT to_char(monthFirstDay + i, 'D') = 7 AND NOT to_char(monthFirstDay + i, 'D') = 1) OR
				((item.exchange_id = 16) AND NOT to_char(monthFirstDay + i, 'D') = 5 AND NOT to_char(monthFirstDay + i, 'D') = 6 AND NOT to_char(monthFirstDay + i, 'D') = 7) THEN
				BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = monthFirstDay + i;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;

				IF halfDayHoliday IS NULL OR halfDayHoliday = 1 THEN
					-- working day... at least half
                                          IF item.market_id = 370 AND to_char(monthFirstDay + i, 'D') = 6 THEN
                                                -- if the jakarta monthly doesn't open on friday it should open by the monthly template close time
                                                -- if its friday open it by daily half day template
                                                SELECT
                                                        to_timestamp_tz(to_char(monthFirstDay + i, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR')
                                                INTO
                                                        toInsTimeFirstInvest
                                                FROM
                                                        opportunity_templates
                                                WHERE
                                                        market_id = 370 AND
                                                        scheduled = 2 AND
                                                        is_half_day = 1;
                                        ELSE
                                                toInsTimeFirstInvest := to_timestamp_tz(to_char(monthFirstDay + i, 'yyyy-mm-dd') || ' ' || item.time_first_invest || ' ' || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
                                        END IF;
					EXIT;
				END IF;
			END IF;
		END LOOP;

		-- find the last working day of the month and set the time last invest and time est closing
		FOR i IN 0..30
		LOOP
			IF (item.exchange_id = 1 OR  item.exchange_id = 17 OR  item.exchange_id = 18) AND NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7 THEN
				BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = monthLastDay - i;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;

				IF halfDayHoliday IS NULL THEN
					-- full working day
					toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
					toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
					EXIT;
				ELSE
					IF halfDayHoliday = 1 THEN
						-- working day... at least half
						SELECT
							time_est_closing,
							time_last_invest
						INTO
							halfDayCloseTime,
							halfDayLastInvest
						FROM
							opportunity_templates
						WHERE
							market_id = item.market_id AND
							scheduled = 2 AND
							is_half_day = 1 AND
							is_active = 1;
						toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						EXIT;
					END IF;
				END IF;
			END IF;

			IF NOT (item.exchange_id = 1 OR  item.exchange_id = 17 OR item.exchange_id = 16 OR  item.exchange_id = 18) AND NOT to_char(monthLastDay - i, 'D') = 7 AND NOT to_char(monthLastDay - i, 'D') = 1 THEN
				BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = monthLastDay - i;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;

				IF halfDayHoliday IS NULL THEN
					-- full working day
					IF item.market_id = 15 AND to_char(monthLastDay - i, 'D') = 6 THEN
						-- ILS has different working time on friday so if the last working day is friday take its special closing time
						SELECT
							to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR'),
							to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR')
						INTO
							toInsTimeEstClosing,
							toInsTimeLastInvest
						FROM
							opportunity_templates
						WHERE
							market_id = 15 AND
							scheduled = 2 AND
							is_full_day = 0 AND
							is_half_day = 1 AND
							is_active = 1;
					ELSE
						toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
					END IF;
					EXIT;
				ELSE
					IF halfDayHoliday = 1 THEN
						-- working day... at least half
						SELECT
							time_est_closing,
							time_last_invest
						INTO
							halfDayCloseTime,
							halfDayLastInvest
						FROM
							opportunity_templates
						WHERE
							market_id = item.market_id AND
							scheduled = 2 AND
							is_half_day = 1 AND
							is_active = 1;
						toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						EXIT;
					END IF;
				END IF;
			END IF;

			IF (item.exchange_id = 16) AND NOT to_char(monthLastDay - i, 'D') = 5 AND NOT to_char(monthLastDay - i, 'D') = 6 AND NOT to_char(monthLastDay - i, 'D') = 7 THEN
				BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = item.exchange_id AND
						trunc(holiday) = monthLastDay - i;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;

				IF halfDayHoliday IS NULL THEN
					-- full working day
						toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_est_closing || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || item.time_last_invest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
					EXIT;
				ELSE
					IF halfDayHoliday = 1 THEN
						-- working day... at least half
						SELECT
							time_est_closing,
							time_last_invest
						INTO
							halfDayCloseTime,
							halfDayLastInvest
						FROM
							opportunity_templates
						WHERE
							market_id = item.market_id AND
							scheduled = 2 AND
							is_half_day = 1 AND
							is_active = 1;
						toInsTimeEstClosing := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayCloseTime || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						toInsTimeLastInvest := to_timestamp_tz(to_char(monthLastDay - i, 'yyyy-mm-dd') || halfDayLastInvest || item.time_zone, 'yyyy-mm-dd hh24:mi TZR');
						EXIT;
					END IF;
				END IF;
			END IF;
		END LOOP;

		SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM dual;
		toIns.market_id := item.market_id;
		toIns.time_created := current_timestamp;
		toIns.time_first_invest := toInsTimeFirstInvest;
		toIns.time_est_closing := toInsTimeEstClosing;
		toIns.time_last_invest := toInsTimeLastInvest;
		toIns.is_published := 0;
		toIns.is_settled := 0;
		toIns.odds_type_id := item.odds_type_id;
		toIns.writer_id := item.writer_id;
		toIns.opportunity_type_id := item.opportunity_type_id;
		toIns.is_disabled := 0;
		toIns.is_disabled_service := 0;
		toIns.is_disabled_trader := 0;
		toIns.scheduled := item.scheduled;
		toIns.deduct_win_odds := item.deduct_win_odds;
                toIns.in_open_graph := item.is_open_graph;
		toIns.max_exposure := item.max_exposure;
		INSERT INTO opportunities VALUES toIns;

--		DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
	END LOOP;
END;



CREATE OR REPLACE FUNCTION investment_real_scheduled (p_investment_id IN NUMBER) RETURN NUMBER AS
	invOppTimeEstClosing opportunities.time_est_closing%TYPE;
	invOppScheduled opportunities.scheduled%TYPE;
	invOppMarket opportunities.market_id%TYPE;
	realScheduled NUMBER;
	openedHourOpps NUMBER;
	activeHourTemplates NUMBER;
BEGIN
	SELECT
		B.time_est_closing,
		B.scheduled,
		B.market_id
	INTO
		invOppTimeEstClosing,
		invOppScheduled,
		invOppMarket
	FROM
		investments A,
		opportunities B
	WHERE
		A.opportunity_id = B.id AND
		A.id = p_investment_id;

	realScheduled := invOppScheduled;

	-- Last week of month opp is considered as week
	-- If we have a week that has its last 3 (or more) 'working days' as holidays don't take over last week last days
	IF invOppScheduled = 4 AND invOppTimeEstClosing - current_timestamp <= to_dsinterval(to_char(invOppTimeEstClosing, 'D') || ' 00:00:00') THEN
		realScheduled := 3;
	END IF;

	-- Last day of week/month opp is considered as day
	IF invOppScheduled >= 3 AND trunc(invOppTimeEstClosing) = trunc(current_timestamp) THEN
		realScheduled := 2;
	END IF;

	-- Last hour of day/week/month opp is considered hour
	IF invOppScheduled >= 2 THEN -- AND invOppTimeEstClosing - current_timestamp <= to_dsinterval('0 01:00:00') THEN
		SELECT
			COUNT(id)
		INTO
			openedHourOpps
		FROM
			opportunities
		WHERE
			market_id = invOppMarket AND
			is_published = 1 AND
			scheduled = 1;

		SELECT
			COUNT(id)
		INTO
			activeHourTemplates
		FROM
			opportunity_templates
		WHERE
			market_id = invOppMarket AND
			is_active = 1 AND
			scheduled = 1;

		IF (activeHourTemplates > 0 AND openedHourOpps = 0) OR
			(activeHourTemplates = 0 AND invOppTimeEstClosing - current_timestamp <= to_dsinterval('0 01:00:00')) THEN
			realScheduled := 1;
		END IF;
	END IF;

	RETURN(realScheduled);
END;

CREATE OR REPLACE FUNCTION get_currency_market (p_currency_id IN NUMBER) RETURN NUMBER AS
	market_id NUMBER;
BEGIN
	SELECT
		A.id
	INTO
		market_id
	FROM
		markets A,
		currencies B
	WHERE
		A.feed_name = B.code || '=' AND
		B.id = p_currency_id;

	RETURN(market_id);
END;

CREATE OR REPLACE FUNCTION get_last_closing_level (p_market_id IN NUMBER, p_time IN DATE) RETURN NUMBER AS
	level NUMBER;
BEGIN
	SELECT
		closing_level
	INTO
		level
	FROM
		(SELECT
			closing_level
		FROM
			LAST_LEVELS
		WHERE
			market_id = p_market_id AND
            trunc(time_est_closing) <= p_time - 1
            order by time_est_closing desc
		)
	WHERE
		rownum <= 1;

	RETURN(level);
END;

CREATE VIEW LAST_LEVELS AS SELECT
			closing_level,market_id,time_est_closing
		FROM
			opportunities , markets
		WHERE
			opportunities.market_id=markets.id AND
			not closing_level is null	AND
		    scheduled =  2 AND
			opportunity_type_id = 1
		and market_id In(14,15,16,17)
		ORDER BY
			time_est_closing DESC;

CREATE OR REPLACE FUNCTION is_multiply_to_convert (p_market_id IN NUMBER) RETURN NUMBER AS
	multiply NUMBER;
BEGIN
	IF p_market_id = 14 OR p_market_id = 16 OR p_market_id = 26 THEN
		multiply := 1;
	ELSE
		multiply := 0;
	END IF;

	RETURN(multiply);
END;

CREATE OR REPLACE FUNCTION convert_amount_to_usd (p_amount IN NUMBER, p_currency_id IN NUMBER, p_time IN DATE) RETURN NUMBER AS
	amount NUMBER;
	market_id NUMBER;
	level NUMBER;
BEGIN
	amount := p_amount;
	IF p_currency_id != 2 THEN
		market_id := get_currency_market(p_currency_id);
		level := get_last_closing_level(market_id, p_time);
		IF (is_multiply_to_convert(market_id) = 1) THEN
			amount := p_amount * level;
		ELSE
			amount := p_amount / level;
		END IF;
	END IF;

	RETURN(amount);
END;

CREATE OR REPLACE FUNCTION get_last_market_rate (p_market_id IN NUMBER) RETURN NUMBER AS
	lastRate NUMBER;
BEGIN
	SELECT
		rate
	INTO
		lastRate
	FROM
		(SELECT
			rate
		FROM
			market_rates
		WHERE
			market_id = p_market_id
		ORDER BY
			id DESC)
	WHERE
		rownum <= 1;

	RETURN(lastRate);
END;

CREATE OR REPLACE FUNCTION get_last_market_rate (p_market_id IN NUMBER) RETURN NUMBER AS
	lastRate NUMBER;
BEGIN
	FOR item IN (
		SELECT
			rate
		FROM
			market_rates
		WHERE
			market_id = p_market_id
		ORDER BY
			id DESC
		)
	LOOP
		lastRate := item.rate;
		EXIT;
	END LOOP;

	RETURN(lastRate);
END;

CREATE OR REPLACE FUNCTION get_tkr_last_day_closing_level (p_market_id IN NUMBER) RETURN NUMBER AS
BEGIN
	FOR opp IN (
		SELECT
			*
		FROM
			opportunities
		WHERE
			NOT closing_level IS NULL AND
			opportunity_type_id = 1 AND
			market_id = p_market_id AND
			scheduled > 1
		ORDER BY
			time_est_closing DESC
	) LOOP
		RETURN opp.closing_level;
	END LOOP;
	RETURN NULL;
END;

CREATE OR REPLACE FUNCTION get_tkr_last_day_closing_time (p_market_id IN NUMBER) RETURN TIMESTAMP WITH TIME ZONE AS
BEGIN
	FOR opp IN (
		SELECT
			*
		FROM
			opportunities
		WHERE
			NOT closing_level IS NULL AND
			opportunity_type_id = 1 AND
			market_id = p_market_id AND
			scheduled > 1
		ORDER BY
			time_est_closing DESC
	) LOOP
		RETURN opp.time_est_closing;
	END LOOP;
	RETURN NULL;
END;

CREATE OR REPLACE FUNCTION get_tkr_last_closing_level (p_market_id IN NUMBER) RETURN NUMBER AS
BEGIN
	FOR opp IN (
		SELECT
			*
		FROM
			opportunities
		WHERE
			NOT closing_level IS NULL AND
			opportunity_type_id = 1 AND
			market_id = p_market_id
		ORDER BY
			time_est_closing DESC
	) LOOP
		IF opp.scheduled = 1 THEN
			RETURN opp.closing_level;
		ELSE
			RETURN NULL;
		END IF;
	END LOOP;
	RETURN NULL;
END;

CREATE OR REPLACE FUNCTION get_tkr_last_closing_time (p_market_id IN NUMBER) RETURN TIMESTAMP WITH TIME ZONE AS
BEGIN
	FOR opp IN (
		SELECT
			*
		FROM
			opportunities
		WHERE
			NOT closing_level IS NULL AND
			opportunity_type_id = 1 AND
			market_id = p_market_id
		ORDER BY
			time_est_closing DESC
	) LOOP
		IF opp.scheduled = 1 THEN
			RETURN opp.time_est_closing;
		ELSE
			RETURN NULL;
		END IF;
	END LOOP;
	RETURN NULL;
END;

-- copied from the db
create or replace
FUNCTION get_currency_by_user (p_user_id IN NUMBER) RETURN NUMBER AS
	curr_id NUMBER;
BEGIN
	SELECT
		currency_id
	INTO
		curr_id
	FROM
		(select currency_id from users where
			id = p_user_id	)
	WHERE
		rownum <= 1;
	RETURN(curr_id);
END;

create or replace
FUNCTION "INVESTMENT_BY_ORDER"
( m_user_id IN NUMBER
, m_order IN NUMBER
) RETURN NUMBER AS
invest_id NUMBER;
BEGIN

select id into invest_id
  from (
  select
  a.*, ROWNUM rnum
      from (select i.id
    from investments i
    where i.IS_SETTLED = 1
    and i.IS_CANCELED = 0
    and i.user_id =  m_user_id
    order by i.TIME_CREATED) a
    where ROWNUM <= m_order)
where rnum  >= m_order;

RETURN (invest_id);

END;

create or replace
FUNCTION "TRANSACTION_BY_ORDER"
( m_user_id IN NUMBER
, m_order IN NUMBER
, m_status IN NUMBER
, m_t_class IN NUMBER
) RETURN NUMBER AS
trans_id NUMBER;
BEGIN

select id into trans_id
  from (
  select
  a.*, ROWNUM rnum
      from (select t.id
    from users u , transactions t , transaction_types tt
    where t.user_id = u.id
    and t.type_id = tt.id
    and tt.class_type = m_t_class
    and t.status_id = m_status
    and u.CLASS_ID <> 0
    and t.TYPE_ID <> 6
    and u.id =  m_user_id
    order by t.TIME_CREATED) a
    where ROWNUM <= m_order)
where rnum  >= m_order;


RETURN (trans_id);

END;

CREATE OR REPLACE FUNCTION get_opp_last_auto_shift (p_opp_id IN NUMBER) RETURN NUMBER AS
	last_shift NUMBER;
	is_up NUMBER;
BEGIN
	last_shift := 0;
	select
            shifting,
            is_up
        INTO
            last_shift,
            is_up
        from (
              SELECT
                    shifting,
                    is_up,
                    SHIFTING_TIME
              FROM
                    opportunity_shiftings
              WHERE
                    opportunity_id = p_opp_id AND
                    writer_id = 0 AND
                    trunc(shifting_time) = trunc(current_date)
              order by SHIFTING_TIME  desc
        )
        where rownum <= 1;

	IF is_up = 0 THEN
		RETURN(-last_shift);
	END IF;

	RETURN(last_shift);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
       RETURN 0;
END;

CREATE OR REPLACE FUNCTION get_clearing_route (p_skin_id IN NUMBER, p_bin IN VARCHAR2, p_currency_id IN NUMBER, p_is_deposit IN NUMBER, p_country_id IN NUMBER) RETURN NUMBER AS
	res NUMBER;
BEGIN
	BEGIN
		SELECT
			clearing_provider_id INTO res
		FROM
			clearing_bin_routes
		WHERE
			skin_id = p_skin_id AND
			start_bin <= p_bin AND
			end_bin >= p_bin AND
			currency_id = p_currency_id AND
			is_deposit = p_is_deposit AND
			rownum <= 1;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			res := NULL;
	END;

	IF res IS NULL THEN
		BEGIN
			SELECT
				clearing_provider_id INTO res
			FROM
				clearing_country_routes
			WHERE
				skin_id = p_skin_id AND
				country_id = p_country_id AND
				currency_id = p_currency_id AND
				is_deposit = p_is_deposit AND
				rownum <= 1;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				res := NULL;
		END;
	END IF;

	IF res IS NULL THEN
		BEGIN
			SELECT
				clearing_provider_id INTO res
			FROM
				clearing_currency_routes
			WHERE
				skin_id = p_skin_id AND
				currency_id = p_currency_id AND
				is_deposit = p_is_deposit AND
				rownum <= 1;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				res := NULL;
		END;
	END IF;

	return(res);
END;

CREATE TYPE PieData AS OBJECT (
	userId NUMBER,
	marketId NUMBER,
	investmentsCount NUMBER
);

CREATE TYPE PieDataList AS TABLE OF PieData;

create or replace
FUNCTION  "PIPELINEPIEDATA" (FROM_DATE IN DATE, TILL_DATE IN DATE) RETURN PieDataList PIPELINED AS
	v_pd PieData;
BEGIN
	FOR v_usr IN (
		SELECT
			id,
			SuccessCount,
			SuccessMarketsCount
		FROM
			(SELECT
				A.id,
				(SELECT count(id) FROM investments invs WHERE user_id = A.id AND win > 0 AND is_canceled = 0 and invs.time_created>=FROM_DATE and invs.time_created<= TILL_DATE) AS SuccessCount,
				(SELECT count(distinct Y.market_id) FROM investments X, opportunities Y WHERE X.opportunity_id = Y.id AND X.user_id = A.id AND X.win > 0 AND X.is_canceled = 0 and X.time_created >=FROM_DATE and X.time_created <= TILL_DATE) AS SuccessMarketsCount
			FROM
				users A where a.class_id <> 0)
		WHERE
			successcount >= 20 and
			successmarketscount >= 3
      
	) LOOP
		FOR v_m IN (
			SELECT
				B.market_id,
				COUNT(A.id) AS cnt
			FROM
				investments A,
				opportunities B,
        		users U
			WHERE
				A.opportunity_id = B.id AND
				A.user_id = v_usr.id AND
       			A.win > 0 AND A.is_canceled = 0 AND
        		A.time_created>=FROM_DATE and A.time_created<=TILL_DATE AND
        		U.id = A.user_id AND
        		U.is_active = 1 AND
        		A.type_id <> 3 
			GROUP BY
				B.market_id
			ORDER BY
				COUNT(A.id) DESC
		) LOOP
			v_pd := PieData(v_usr.id, v_m.market_id, v_m.cnt);
			PIPE ROW(v_pd);
		END LOOP;
	END LOOP; 
	RETURN;
END PipelinePieData;

SELECT * FROM TABLE(PipelinePieData);

create or replace PROCEDURE POPULATION_DORMANT_ACCOUNT AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                        A.id
                                                                 from (
                                                                                  select
                                                                                          Y.*
                                                                                  from
                                                                                          population_entries Y, populations P
                                                                                  where
                                                                                           Y.user_id = p_user_id  AND
                                                                                           Y.population_id = P.id AND
                                                                                           P.population_type_id = pop_type

                                                                                  order by
                                                                                          Y.TIME_CREATED desc
                                                                                ) A
                                                                 WHERE
                                                                      rownum = 1)

                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
            B.LAST_INVEST_DAY
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 4 AND
			current_date - GET_LAST_INVEST_TIME(A.id) >= B.LAST_INVEST_DAY  AND

			IS_USER_IN_POP_TYPE(A.id, 4 )= 0 AND

           EXISTS(
				SELECT
					t.id
				FROM
					transactions t,
                	TRANSACTION_TYPES tt
				WHERE
                	tt.CLASS_TYPE = 1 AND
					user_id = A.id AND
					type_id = tt.id AND
                    t.status_id in (2,7)) AND

          EXISTS(
				SELECT
					1
				FROM
					INVESTMENTS i
				WHERE
					i.user_id = A.id ) AND


          NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
                    (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.id IN (SELECT USER_ID FROM frauds))
                ) AND

          NOT EXISTS(
                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
           )


	) LOOP

			open get_user_last_population_type(v_usr.id, 4);
            fetch get_user_last_population_type into populationEntHist;

             IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107))  THEN

                 IF (GET_LAST_invest_time(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                        close get_user_last_population_type;
                        GOTO end_loop;
                 END IF;
            END IF;

            close get_user_last_population_type;

            open get_user_last_population(v_usr.id);
            fetch get_user_last_population into populationEntHist;

            IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                    SELECT priority into populationPriority FROM population_types  WHERE id = 4;
                    populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                    IF (populationPriority < populationEntHistPriority) THEN
                            REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                    END IF;
                    IF (populationPriority > populationEntHistPriority) THEN
                            close get_user_last_population;
                            GOTO end_loop;
                    END IF;
            END IF;

            close get_user_last_population;

            SELECT
                    COUNT(id) INTO v_counter
            FROM
                    population_entries
            WHERE
                    population_id = v_usr.populationId;

            SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
            v_pe.population_id := v_usr.populationId;
            v_pe.user_id := v_usr.id;
            v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
            v_pe.writer_id := 0;
            v_pe.time_created := sysdate;
            v_pe.ASSIGN_WRITER_ID := NULL;
            v_pe.is_locked := 0;
            INSERT INTO population_entries VALUES v_pe;

--		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

--                        SELECT
--                                id INTO v_populationEntryId
--                        FROM
 --                               population_entries
 --                       WHERE
--                                population_id = v_usr.populationId AND
 --                               user_id = v_usr.id;

            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
            v_peh.population_entry_id := v_pe.id;
            v_peh.status_id := 1;
            v_peh.comments := 'Dormant Accounts';
            v_peh.writer_id := 0;
            v_peh.time_created := GET_LAST_INVEST_TIME(v_usr.id) +v_usr.LAST_INVEST_DAY;
            INSERT INTO population_entries_hist VALUES v_peh;

			<<end_loop>>
            NULL;
	END LOOP;
END;

create or replace PROCEDURE POPULATION_FIRST_DEPO_FAILED AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                                        A.id
                                                                                 from (
                                                                                                  select
                                                                                                          Y.*
                                                                                                  from
                                                                                                          population_entries Y, populations P
                                                                                                  where
                                                                                                           Y.user_id = p_user_id  AND
                                                                                                           Y.population_id = P.id AND
                                                                                                           P.population_type_id = pop_type

                                                                                                  order by
                                                                                                          Y.TIME_CREATED desc
                                                                                                ) A
                                                                                 WHERE
                                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 1 AND
                        B.is_active = 1 AND
			NOT EXISTS(
				SELECT
					t.id
				FROM
					transactions t,
                                        TRANSACTION_TYPES tt
				WHERE
                                        tt.CLASS_TYPE = 1 AND
					user_id = A.id AND
					type_id = tt.id AND
                                        status_id <> 3) AND

                         EXISTS(
				SELECT
					t.id
				FROM
					transactions t,
                                        TRANSACTION_TYPES tt
				WHERE
                                        tt.CLASS_TYPE = 1 AND
					user_id = A.id AND
					type_id = tt.id AND
                                        status_id = 3) AND

                      IS_USER_IN_POP_TYPE(A.id, 1 )= 0 AND

                       NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
                                        (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0 OR  u.id in (select USER_ID from frauds))
                                        ) AND

                      NOT EXISTS(
                                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       )
	) LOOP
                        open get_user_last_population_type(v_usr.id, 1);
                        fetch get_user_last_population_type into populationEntHist;

                        IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107 ))  THEN

                             IF (GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                                    close get_user_last_population_type;
                                    GOTO end_loop;
                             END IF;
                        END IF;

                        close get_user_last_population_type;

                         open get_user_last_population(v_usr.id);
                        fetch get_user_last_population into populationEntHist;

                        IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = 1;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        close get_user_last_population;
                                        GOTO end_loop;
                                END IF;
                        END IF;

                        close get_user_last_population;

                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

 --                       SELECT
 --                               id INTO v_populationEntryId
 --                       FROM
--                                population_entries
 --                       WHERE
--                                population_id = v_usr.populationId AND
--                                user_id = v_usr.id;

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'First deposite failed';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) ;
                        INSERT INTO population_entries_hist VALUES v_peh;

                        <<end_loop>>
                        NULL;

	END LOOP;
END;

create or replace PROCEDURE POPULATION_LAST_DEPO_FAILED AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                                        A.id
                                                                                 from (
                                                                                                  select
                                                                                                          Y.*
                                                                                                  from
                                                                                                          population_entries Y, populations P
                                                                                                  where
                                                                                                           Y.user_id = p_user_id  AND
                                                                                                           Y.population_id = P.id AND
                                                                                                           P.population_type_id = pop_type
                                                                                                  order by
                                                                                                          Y.TIME_CREATED desc
                                                                                                ) A
                                                                                 WHERE
                                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
                        B.number_of_minutes
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 2 AND
			current_date - GET_LAST_DEPOSIT_BY_CC_TIME(A.id) >= B.number_of_minutes / 1440 AND
                        B.is_active = 1 AND
                        IS_LAST_DEPOSIT_FAILED(A.ID) = 1  AND

			 EXISTS(
				SELECT
					t.id
				FROM
					transactions t,
                                        TRANSACTION_TYPES tt
				WHERE
                                        tt.CLASS_TYPE = 1 AND
					user_id = A.id AND
					type_id = tt.id AND
                                        status_id IN (2, 7)) AND

			IS_USER_IN_POP_TYPE(A.id, 2 )= 0 AND

                       NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
                                        (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0 OR u.id IN (SELECT USER_ID FROM frauds))
                                        ) AND

                      NOT EXISTS(
                                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       )
	) LOOP

                        open get_user_last_population_type(v_usr.id, 2);
                        fetch get_user_last_population_type into populationEntHist;

                        IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107 ))  THEN

                             IF (GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                                    close get_user_last_population_type;
                                    GOTO end_loop;
                             END IF;
                        END IF;

                        close get_user_last_population_type;

                        open get_user_last_population(v_usr.id);
                        fetch get_user_last_population into populationEntHist;

                        IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = 2;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        close get_user_last_population;
                                        GOTO end_loop;
                                END IF;
                        END IF;

                        close get_user_last_population;

                        SELECT
                                COUNT(id) INTO v_counter
                        FROM
                                population_entries
                        WHERE
                                population_id = v_usr.populationId;

                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);
                        /*
                        SELECT
                                id INTO v_populationEntryId
                        FROM
                                population_entries
                        WHERE
                                population_id = v_usr.populationId AND
                                user_id = v_usr.id;
                        */

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        --v_peh.population_entry_id := v_populationEntryId;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'Decline After Approved';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) + v_usr.number_of_minutes/1440;
                        INSERT INTO population_entries_hist VALUES v_peh;

                        <<end_loop>>
                        NULL;
	END LOOP;
END;


create or replace PROCEDURE population_no_deposit_fill AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
	v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                                        A.id
                                                                                 from (
                                                                                                  select
                                                                                                          Y.*
                                                                                                  from
                                                                                                          population_entries Y, populations P
                                                                                                  where
                                                                                                           Y.user_id = p_user_id  AND
                                                                                                           Y.population_id = P.id AND
                                                                                                           P.population_type_id = pop_type

                                                                                                  order by
                                                                                                          Y.TIME_CREATED desc
                                                                                                ) A
                                                                                 WHERE
                                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
                        A.TIME_CREATED,
                        B.number_of_minutes
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 5 AND
			current_date - A.time_created >= B.number_of_minutes / 1440 AND
                        B.is_active = 1 AND
			NOT EXISTS(
				SELECT
					t.id
				FROM
					transactions t,
                                        TRANSACTION_TYPES tt
				WHERE
                                        tt.CLASS_TYPE = 1 AND
					user_id = A.id AND
					type_id = tt.id) AND

                        IS_USER_IN_POP_TYPE(A.id, 5 )= 0 AND

			NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
					(u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0 OR u.id IN (SELECT USER_ID FROM frauds))
                                        ) AND

                      NOT EXISTS(
                                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       )
	) LOOP

                open get_user_last_population_type(v_usr.id, 5);
                fetch get_user_last_population_type into populationEntHist;

                IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107 ))  THEN

                      close get_user_last_population_type;
                      GOTO end_loop;
                END IF;

                close get_user_last_population_type;

                open get_user_last_population(v_usr.id);
                fetch get_user_last_population into populationEntHist;

                IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                        SELECT priority into populationPriority FROM population_types  WHERE id = 5;
                        populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                        IF (populationPriority < populationEntHistPriority) THEN
                                REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                        END IF;
                        IF (populationPriority > populationEntHistPriority) THEN
                                close get_user_last_population;
                                GOTO end_loop;
                        END IF;
                END IF;

               close get_user_last_population;

                SELECT
                        COUNT(id) INTO v_counter
                FROM
                        population_entries
                WHERE
                        population_id = v_usr.populationId;

                SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                v_pe.population_id := v_usr.populationId;
                v_pe.user_id := v_usr.id;
                v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                v_pe.writer_id := 0;
                v_pe.time_created := sysdate;
                v_pe.ASSIGN_WRITER_ID := NULL;
                v_pe.is_locked := 0;
                INSERT INTO population_entries VALUES v_pe;

--			DBMS_OUTPUT.PUT_LINE('UP - 1, ' || v_up.population_id || ', ' || v_up.user_id || ', ' || v_up.is_active || ', ' || v_up.group_id || ', ' || v_up.writer_id || ', ' || v_up.time_created);

      --          SELECT
  --                      id INTO v_populationEntryId
    --            FROM
    --                    population_entries
 --               WHERE
 --                       population_id = v_usr.populationId AND
  --                      user_id = v_usr.id;

                SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                v_peh.population_entry_id := v_pe.id;
                v_peh.status_id := 1;
                v_peh.comments := 'No deposit since registration';
                v_peh.writer_id := 0;
                v_peh.time_created := v_usr.TIME_CREATED + v_usr.number_of_minutes/1440;
                INSERT INTO population_entries_hist VALUES v_peh;

                <<end_loop>>
                 NULL;
	END LOOP;
END;

create or replace PROCEDURE POPULATION_ONE_TIME_DEPOSITE AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                                        A.id
                                                                                 from (
                                                                                                  select
                                                                                                          Y.*
                                                                                                  from
                                                                                                          population_entries Y, populations P
                                                                                                  where
                                                                                                           Y.user_id = p_user_id  AND
                                                                                                           Y.population_id = P.id AND
                                                                                                           P.population_type_id = pop_type

                                                                                                  order by
                                                                                                          Y.TIME_CREATED desc
                                                                                                ) A
                                                                                 WHERE
                                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
                        B.NUMBER_OF_DAYS
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 3 AND
                        B.is_active = 1 AND
			sysdate - GET_LAST_INVEST_TIME(A.id) >= B.LAST_INVEST_DAY AND

                        sysdate - GET_LAST_DEPOSIT_BY_CC_TIME(A.id) >= B.NUMBER_OF_DAYS AND

                        A.balance <= B.BALANCE_AMOUNT AND

                       GET_NUMBER_OF_SUCCESS_DEPOSITE(A.id) = 1 AND

                      IS_USER_IN_POP_TYPE(A.id, 3 )= 0 AND

                       NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
                                        (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0  OR u.id in (select USER_ID from frauds))
                                        ) AND

                      NOT EXISTS(
                                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       )
	) LOOP
                        open get_user_last_population_type(v_usr.id, 2);
                        fetch get_user_last_population_type into populationEntHist;

                        IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107 ))  THEN

                             IF (GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) <= populationEntHist.TIME_CREATED and GET_LAST_INVEST_TIME(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                                    close get_user_last_population_type;
                                    GOTO end_loop;
                             END IF;
                        END IF;

                        close get_user_last_population_type;

                        open get_user_last_population(v_usr.id);
                        fetch get_user_last_population into populationEntHist;

                        IF (get_user_last_population%FOUND and populationEntHist.STATUS_ID<=100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = 3;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        close get_user_last_population;
                                        GOTO end_loop;
                                END IF;
                        END IF;

                        close get_user_last_population;

                        SELECT
                                COUNT(id) INTO v_counter
                        FROM
                                population_entries
                        WHERE
                                population_id = v_usr.populationId;

                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

 --                      SELECT
   --                             id INTO v_populationEntryId
   --                     FROM
  --                              population_entries
   --                     WHERE
     --                           population_id = v_usr.populationId AND
    --                            user_id = v_usr.id;

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'One Time Depositors';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_LAST_DEPOSIT_BY_CC_TIME(v_usr.id) + v_usr.NUMBER_OF_DAYS;
                        INSERT INTO population_entries_hist VALUES v_peh;

                        <<end_loop>>
                        NULL;
	END LOOP;
END;


create or replace FUNCTION GET_LAST_DEPOSIT_BY_CC_TIME ( userID IN NUMBER ) RETURN DATE AS
tran  transactions%ROWTYPE;

cursor tranCur(Uid number) is
    select t.*
    from transactions t,
  		 TRANSACTION_TYPES tt
    where
	      tt.CLASS_TYPE = 1 AND
	      user_id = Uid and
	      type_id = tt.id
    order by
		  time_created  DESC;

BEGIN
      open tranCur(userID);
      fetch tranCur into tran;

      if tranCur%NOTFOUND then
          return current_date;
      end if;

      close tranCur;


      return tran.TIME_CREATED ;
End;

create or replace FUNCTION GET_LAST_INVEST_TIME ( userId IN NUMBER) RETURN DATE AS
inv  investments%ROWTYPE;
cursor investCur(Uid number) is
    select *
    from investments
    where user_id = Uid
    order by time_created  DESC;

BEGIN
      open investCur(userId);
      fetch investCur into inv;

      if investCur%NOTFOUND then
          return current_date;
      end if;

      close investCur;


      return inv.TIME_CREATED ;
END;

create or replace FUNCTION GET_NUMBER_OF_SUCCESS_DEPOSITE ( userId IN NUMBER) RETURN NUMBER AS
dep_number NUMBER;
BEGIN
  dep_number := 0 ;
  SELECT
          count(*)
  INTO
          dep_number
  FROM
          TRANSACTIONS t,
          TRANSACTION_TYPES tt
  WHERE
          tt.CLASS_TYPE = 1 AND
          t.TYPE_ID = tt.id AND
          t.STATUS_ID in (2,7) AND
          t.USER_ID = userId;

  RETURN dep_number;
END ;

create or replace FUNCTION GET_PRIORITY_BY_POP_ENT_ID( popEntId IN NUMBER) RETURN NUMBER AS
priority number;
BEGIN
  select
        pt.priority
  into
        priority
  from
        POPULATIONS p,
        POPULATION_ENTRIES pe,
        population_types pt
  where
        pe.id = popEntId and
        p.id = pe.POPULATION_ID and
        pt.id = p.population_type_id;

RETURN priority;
END GET_PRIORITY_BY_POP_ENT_ID;

create or replace FUNCTION IS_LAST_DEPOSIT_FAILED ( userId IN NUMBER) RETURN VARCHAR2 AS
tran  transactions%ROWTYPE;
v_class_type NUMBER;

cursor tranCur(Uid number) is
    select *
    from transactions
    where user_id = Uid
    order by time_created  DESC;

BEGIN
      open tranCur(userId);
      fetch tranCur into tran;

      if tranCur%NOTFOUND then
          return null;
      end if;

      close tranCur;

      select  tt.class_type
      into v_class_type
      from transaction_types tt
      where tt.id = tran.type_id;

      if  (v_class_type = 1 and tran.status_id = 3) then
          return 1;
      end if;

      return null;
END;

create or replace FUNCTION IS_USER_IN_POP_TYPE( U_ID IN NUMBER, POP_TYPE IN NUMBER) RETURN NUMBER AS

existFlag number;

BEGIN

existFlag := 0;

      BEGIN

            SELECT
                   1
            into
                   existFlag
            FROM
                  ( SELECT
                            PEHO.*
                    FROM
                             (SELECT
                                Z.*
                              FROM
                                  population_entries_hist Z
                               WHERE
                               	  Z.status_id not in (3, 4) AND
                                  Z.population_entry_id =(select
                                                                                              D.id
                                                                                    From(
                                                                                              select
                                                                                                      *
                                                                                              from (
                                                                                                          select
                                                                                                                  Y.*
                                                                                                          from
                                                                                                                  population_entries Y
                                                                                                          where
                                                                                                                   Y.user_id = U_ID
                                                                                                          order by
                                                                                                                  Y.TIME_CREATED desc
                                                                                                        ) A
                                                                                                WHERE
                                                                                                        rownum = 1) D,
                                                                                               POPULATIONS P
                                                                                        WHERE
                                                                                              D.population_id = P.id AND
                                                                                              P.population_type_id = POP_TYPE )

                                ORDER BY
                                  Z.TIME_CREATED desc) PEHO
                    WHERE
                            rownum = 1)
            WHERE
                status_id < 100;
             EXCEPTION WHEN NO_DATA_FOUND THEN
             return existFlag;
      END;
return existFlag;
END;

create or replace PROCEDURE REMOVE_USER_FROM_POP( populationEntryId IN NUMBER) AS

v_peh population_entries_hist%ROWTYPE;
BEGIN

SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
v_peh.population_entry_id := populationEntryId;
v_peh.status_id := 105;
v_peh.comments := 'Remove to other qualification';
v_peh.writer_id := 0;
v_peh.time_created := current_date;
INSERT INTO population_entries_hist VALUES v_peh;
END REMOVE_USER_FROM_POP;

create or replace TYPE population_table_data AS OBJECT (
	id NUMBER,
	time_created DATE,
	user_id NUMBER,
  	contact_id NUMBER,
	name NVARCHAR2(41 CHAR),
	population VARCHAR(30 CHAR),
	language VARCHAR(20 CHAR),
	country VARCHAR(40 CHAR),
	last_call_writer NVARCHAR2(41 CHAR),
	last_call_time DATE,
	last_call_time_offset VARCHAR(9 CHAR),
	last_reached_status_id NUMBER,
	last_reaction_id NUMBER,
	calls_count NUMBER,
	email_time DATE,
	user_name VARCHAR2(30 CHAR),
	account_status NUMBER,
	reg_date DATE,
	reg_date_offset VARCHAR(9 CHAR),
	birth_date DATE,
	mobile VARCHAR2(25 CHAR),
	land_line VARCHAR(25 CHAR),
	last_deposit DATE,
	last_deposit_offset VARCHAR(9 CHAR),
	last_decline DATE,
	last_decline_offset VARCHAR(9 CHAR),
	total_decline NUMBER,
	decline_count NUMBER,
	balance NUMBER,
	assigned_writer NVARCHAR2(41 CHAR),
	skin_id NUMBER,
	callback_time DATE,
	callback_time_offset VARCHAR(9 CHAR),
	population_type_id NUMBER,
  	email NVARCHAR2(50 CHAR),
  	currency_id NUMBER,
 	is_locked NUMBER,
   	id_num VARCHAR2(10 CHAR),
    is_contact_by_phone NUMBER,
    combId NUMBER
);



create or replace TYPE population_table_data_list AS TABLE OF population_table_data

create or replace FUNCTION get_population_table_data (
		p_population_ids IN VARCHAR2,
		p_skin_id IN NUMBER,
		p_language IN NUMBER,
		p_writer_id_for_skin IN NUMBER,
		p_writer_id IN NUMBER,
		p_writer_filter IN NUMBER,
		p_has_user IN NUMBER,
		p_user_id IN NUMBER,
		p_user_id_num IN VARCHAR2,
		p_user_name IN VARCHAR2,
    	p_user_phone IN VARCHAR2,
		p_start_row IN NUMBER,
		p_page_size IN NUMBER) RETURN population_table_data_list PIPELINED AS
	v_d population_table_data;

	v_name NVARCHAR2(41 CHAR);
	v_language VARCHAR2(20 CHAR);
	v_country VARCHAR2(40 CHAR);
	v_last_call_writer NVARCHAR2(41 CHAR);
	v_last_call_time DATE;
	v_last_call_time_offset VARCHAR2(9 CHAR);
	v_last_reached_status_id NUMBER;
	v_last_reaction_id NUMBER;
	v_calls_count NUMBER;
	v_user_name VARCHAR2(30 CHAR);
	v_account_status NUMBER;
	v_reg_date DATE;
	v_reg_date_offset VARCHAR(9 CHAR);
	v_birth_date DATE;
	v_mobile VARCHAR2(25CHAR);
	v_land_line VARCHAR(25 CHAR);
	v_last_deposit DATE;
	v_last_deposit_offset VARCHAR(9 CHAR);
	v_last_decline DATE;
	v_last_decline_offset VARCHAR(9 CHAR);
	v_total_decline NUMBER;
	v_decline_count NUMBER;
	v_balance NUMBER;
	v_skin_id NUMBER;
	v_callback_time DATE;
	v_callback_time_offset VARCHAR(9 CHAR);
    v_email NVARCHAR2(50 CHAR);
    v_currency_id NUMBER;
    v_id_num VARCHAR2(10 CHAR);
    v_is_contact_by_phone NUMBER;
    v_combination_id NUMBER;
BEGIN
	FOR v_entry IN (
			SELECT
				*
			FROM
				(SELECT
					A.*,
					rownum AS rownumIn
				FROM
					(SELECT
						A.id,
						A.qualification_time AS time_created,
						A.user_id,
						A.contact_id,
						B.name AS population,
						CASE WHEN D.first_name IS NULL AND D.last_name IS NULL THEN NULL ELSE D.first_name || ' ' || D.last_name END AS assigned_writer,
						A.callback_time,
						A.callback_time_offset,
						B.population_type_id,
            			A.is_locked
					FROM
						populations B,
						population_entries A LEFT JOIN
						users C ON A.user_id = C.id LEFT JOIN
						writers D ON A.assign_writer_id = D.id LEFT JOIN
          				contacts E ON A.contact_id = E.id
					WHERE
			A.population_id = B.id AND
     		A.group_id = 1 AND
			(C.class_id IS NULL OR NOT C.class_id = 0) AND
			(p_user_id = 0 OR A.user_id = p_user_id) AND
			(p_user_id_num IS NULL OR C.id_num = p_user_id_num) AND
			(p_user_name IS NULL OR C.user_name = UPPER(p_user_name)) AND
      		(p_user_phone IS NULL OR C.land_line_phone = p_user_phone OR C.mobile_phone = p_user_phone OR E.phone = p_user_phone) AND
      		(B.skin_id IN (SELECT skin_id  FROM 	writers_skin 	WHERE 	writer_id = p_writer_id_for_skin)) AND

     		-- Any other way record should match to the entity filter.
     		((p_has_user = 0 AND A.user_id IS NULL) OR (p_has_user = 1 AND NOT A.user_id IS NULL)) AND

           -- AND -> Either the record is locked or has callback
           (((( A.is_locked > 0) AND (p_writer_id = 0 	OR 	(p_writer_id > 0 AND A.is_locked = p_writer_id)))
           OR (p_writer_filter != 1 AND
                   (((p_writer_id = 0 AND A.callback_time IS NOT NULL)
                   OR
                   (p_writer_id > 0 AND A.callback_time - current_date <= 1/144 AND	(A.assign_writer_id = p_writer_id OR A.assign_writer_id IS NULL AND A.is_locked = 0))))))


          OR

           -- Or it's corresponding to the requested filter (except for entity filter and userId filter).
           ((p_population_ids IS NULL OR A.population_id IN (SELECT p.id FROM populations p WHERE p_population_ids LIKE '%,' || p.id || ',%')) AND
						(p_skin_id = 0 OR B.skin_id = p_skin_id) AND
						(p_language = 0 OR C.language_id IS NULL OR C.language_id = p_language) AND
            A.callback_time IS NULL AND
						A.status_id < 100 AND
						 (
							(p_writer_filter = 0 AND
								(
									(p_writer_id = 0)
									OR
									(p_writer_id > 0 AND
										(
											A.assign_writer_id = p_writer_id
											OR
											A.is_locked = p_writer_id
											OR
											(A.assign_writer_id IS NULL AND A.is_locked = 0)
										)
									)
								)
							)
--             Don't need this part, because we always show locked records.
--							OR
--							(p_writer_filter = 1 AND
--								(
--									(p_writer_id = 0 AND A.is_locked > 0)
--									OR
--									(p_writer_id > 0 AND A.is_locked = p_writer_id)
--								)
--							)
							OR
							(p_writer_filter = 2 AND
								(
									(p_writer_id = 0 AND NOT A.assign_writer_id IS NULL)
									OR
									(p_writer_id > 0 AND A.assign_writer_id = p_writer_id)
								)
							)
						)))
					ORDER BY
						A.callback_time,
						A.qualification_time DESC) A)
			WHERE
				rownumIn > p_start_row AND
				rownumIn <= p_start_row + p_page_size
	) LOOP
		IF v_entry.contact_id IS NULL THEN
			SELECT
				A.first_name || ' ' || A.last_name,
				B.country_name,
				C.display_name,
				A.user_name,
				A.is_active,
				A.time_created,
				A.utc_offset_created,
				A.time_birth_date,
				A.skin_id,
				B.phone_code || ' ' || A.mobile_phone,
				B.phone_code || ' ' || A.land_line_phone,
				A.balance,
        		A.email,
        		A.currency_id,
        		A.id_num,
        		A.is_contact_by_phone,
        		A.combination_id
			INTO
				v_name,
				v_country,
				v_language,
				v_user_name,
				v_account_status,
				v_reg_date,
				v_reg_date_offset,
				v_birth_date,
				v_skin_id,
				v_mobile,
				v_land_line,
				v_balance,
       		 	v_email,
        		v_currency_id,
        		v_id_num,
        		v_is_contact_by_phone,
        		v_combination_id
			FROM
				users A,
				countries B,
				languages C
			WHERE
				A.id = v_entry.user_id AND
				A.country_id = B.id AND
				A.language_id = C.id;

			BEGIN
				SELECT
					time_created,
					utc_offset_created
				INTO
					v_last_deposit,
					v_last_deposit_offset
				FROM
					(SELECT
						A.*,
						rownum AS rownumIn
					FROM
						(SELECT
							time_created,
							utc_offset_created
						FROM
							transactions
						WHERE
							user_id = v_entry.user_id AND
							type_id IN (1, 2, 9) AND
							status_id IN (2, 7, 9)
						ORDER BY
							time_created DESC) A)
				WHERE
					rownumIn = 1;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					v_last_deposit := NULL;
					v_last_deposit_offset := NULL;
			END;

			BEGIN
				SELECT
					time_created,
					utc_offset_created
				INTO
					v_last_decline,
					v_last_decline_offset
				FROM
					(SELECT
						A.*,
						rownum AS rownumIn
					FROM
						(SELECT
							time_created,
							utc_offset_created
						FROM
							transactions
						WHERE
							user_id = v_entry.user_id AND
							type_id IN (1, 2, 9) AND
							status_id = 3
						ORDER BY
							time_created DESC) A)
				WHERE
					rownumIn = 1;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					v_last_decline := NULL;
					v_last_decline_offset := NULL;
			END;

			SELECT
				COUNT(A.id),
				SUM(A.amount)
			INTO
				v_decline_count,
				v_total_decline
			FROM
				transactions A,
				transaction_types B
			WHERE
				A.user_id = v_entry.user_id AND
				A.type_id = B.id AND
				B.class_type = 1 AND
				A.status_id = 3 AND
				(v_last_deposit IS NULL OR A.time_created > v_last_deposit);
		END IF;

		IF v_entry.user_id IS NULL THEN
			SELECT
				A.name,
				A.user_id,
     		    A.skin_id,
				B.country_name,
				NULL,
        		B.phone_code || ' ' || A.phone,
        		A.email,
        		A.combination_id
			INTO
				v_name,
				v_entry.user_id,
        		v_skin_id,
				v_country,
				v_language,
        		v_mobile,
        		v_email,
        		v_combination_id
			FROM
				contacts A LEFT JOIN	countries B on 	A.country_id = B.id
			WHERE
				A.id = v_entry.contact_id;
		END IF;

		SELECT
			COUNT(B.id)
		INTO
			v_calls_count
		FROM
			issues A,
			issue_actions B
		WHERE
			A.population_entry_id = v_entry.id AND
			A.id = B.issue_id AND
			B.direction_id IS NOT NULL;

		IF v_calls_count > 0 THEN
			SELECT
				B.first_name || ' ' || B.last_name,
				A.action_time,
				A.action_time_offset,
				A.reached_status_id,
				A.reaction_id
			INTO
				v_last_call_writer,
				v_last_call_time,
				v_last_call_time_offset,
				v_last_reached_status_id,
				v_last_reaction_id
			FROM
				issue_actions A,
				writers B
			WHERE
				A.writer_id = B.id AND
				A.id = (
					SELECT
						MAX(id)
					FROM
						issue_actions
					WHERE
						direction_id IS NOT NULL AND
						issue_id = (
							SELECT
								id
							FROM
								issues
							WHERE
								population_entry_id = v_entry.id
						)
				);
		ELSE
			v_last_call_writer := NULL;
			v_last_call_time := NULL;
			v_last_call_time_offset := NULL;
			v_last_reached_status_id := NULL;
			v_last_reaction_id := NULL;
		END IF;

		v_d := population_table_data(v_entry.id, v_entry.time_created, v_entry.user_id, v_entry.contact_id,v_name, v_entry.population, v_language, v_country, v_last_call_writer, v_last_call_time, v_last_call_time_offset, v_last_reached_status_id, v_last_reaction_id, v_calls_count, NULL, v_user_name, v_account_status, v_reg_date, v_reg_date_offset, v_birth_date, v_mobile, v_land_line, v_last_deposit, v_last_deposit_offset, v_last_decline, v_last_decline_offset, v_total_decline, v_decline_count, v_balance, v_entry.assigned_writer, v_skin_id, v_entry.callback_time, v_entry.callback_time_offset, v_entry.population_type_id,v_email,v_currency_id,v_entry.is_locked,v_id_num,v_is_contact_by_phone);
		PIPE ROW(v_d);
	END LOOP;

	RETURN;
END;

create or replace FUNCTION get_population_table_count(
		p_population_ids IN VARCHAR2,
		p_skin_id IN NUMBER,
		p_language IN NUMBER,
		p_writer_id_for_skin IN NUMBER,
		p_writer_id IN NUMBER,
		p_writer_filter IN NUMBER,
		p_has_user IN NUMBER,
		p_user_id IN NUMBER,
		p_user_id_num IN VARCHAR2,
		p_user_name IN VARCHAR2,
   		p_user_phone IN VARCHAR2) RETURN NUMBER AS
	v_count NUMBER;
BEGIN
	SELECT
		COUNT(A.id)
	INTO
		v_count
	FROM
		populations B,
		population_entries A LEFT JOIN
		users C ON A.user_id = C.id LEFT JOIN
   		contacts E ON A.contact_id = E.id
	WHERE
    A.population_id = B.id AND
    A.group_id = 1 AND
    (C.class_id IS NULL OR NOT C.class_id = 0) AND
    (p_user_id = 0 OR A.user_id = p_user_id) AND
    (p_user_id_num IS NULL OR C.id_num = p_user_id_num) AND
    (p_user_name IS NULL OR C.user_name = UPPER(p_user_name)) AND
    (p_user_phone IS NULL OR C.land_line_phone = p_user_phone OR C.mobile_phone = p_user_phone OR E.phone = p_user_phone) AND
    (B.skin_id IN (SELECT skin_id  FROM 	writers_skin 	WHERE 	writer_id = p_writer_id_for_skin)) AND

    -- Any other way record should match to the entity filter.
    ((p_has_user = 0 AND A.user_id IS NULL) OR (p_has_user = 1 AND NOT A.user_id IS NULL)) AND

    -- AND -> Either the record is locked or has callback
           (((( A.is_locked > 0) AND (p_writer_id = 0 	OR 	(p_writer_id > 0 AND A.is_locked = p_writer_id)))
           OR (p_writer_filter != 1 AND
                   (((p_writer_id = 0 AND A.callback_time IS NOT NULL)
                   OR
                   (p_writer_id > 0 AND A.callback_time - current_date <= 1/144 AND	(A.assign_writer_id = p_writer_id OR A.assign_writer_id IS NULL AND A.is_locked = 0))))))

    OR

    -- Or it's corresponding to the requested filter (except for entity filter and userId filter).
   		((p_population_ids IS NULL OR A.population_id IN (SELECT p.id FROM populations p WHERE p_population_ids LIKE '%,' || p.id || ',%')) AND
		(p_skin_id = 0 OR B.skin_id = p_skin_id) AND
		(p_language = 0 OR C.language_id IS NULL OR C.language_id = p_language) AND
    	A.callback_time IS NULL AND
		A.status_id < 100 AND
		(
			(p_writer_filter = 0 AND
				(
					(p_writer_id = 0)
					OR
					(p_writer_id > 0 AND
						(
							A.assign_writer_id = p_writer_id
							OR
							A.is_locked = p_writer_id
							OR
							(A.assign_writer_id IS NULL AND A.is_locked = 0)
						)
					)
				)
			)
--      Don't need this part, because we always show locked records.
--			OR
--			(p_writer_filter = 1 AND
--				(
--					(p_writer_id = 0 AND A.is_locked > 0)
--					OR
--					(p_writer_id > 0 AND A.is_locked = p_writer_id)
--				)
--			)
			OR
			(p_writer_filter = 2 AND
				(
					(p_writer_id = 0 AND NOT A.assign_writer_id IS NULL)
					OR
					(p_writer_id > 0 AND A.assign_writer_id = p_writer_id)
				)
			)
		)));
	RETURN(v_count);
END;


create or replace FUNCTION is_population_entry_locked(p_population_entry_id IN NUMBER, p_writer_id IN NUMBER) RETURN NUMBER AS
BEGIN
	FOR v_hist_entry IN (
		SELECT
			status_id,
			writer_id
		FROM
			population_entries_hist
		WHERE
			population_entry_id = p_population_entry_id
		ORDER BY
			time_created DESC
	) LOOP
		IF v_hist_entry.status_id = 3 AND (p_writer_id = 0 OR v_hist_entry.writer_id = p_writer_id) THEN
			RETURN(v_hist_entry.writer_id);
		END IF;
		IF v_hist_entry.status_id = 4 AND (p_writer_id = 0 OR v_hist_entry.writer_id = p_writer_id) THEN
			RETURN(0);
		END IF;
	END LOOP;
	RETURN(0);
END;

create or replace FUNCTION get_population_entry_status(p_population_entry_id IN NUMBER) RETURN NUMBER AS
BEGIN
	FOR v_hist_entry IN (
		SELECT
			*
		FROM
			population_entries_hist
		WHERE
			population_entry_id = p_population_entry_id
		ORDER BY
			time_created DESC
	) LOOP
		IF NOT v_hist_entry.status_id IN (3, 4) THEN
			RETURN(v_hist_entry.status_id);
		END IF;
	END LOOP;
	-- this should not be reached
	RETURN(0);
END;

create or replace FUNCTION get_population_entry_qual_time(p_population_entry_id IN NUMBER) RETURN DATE AS
	v_qualification_time DATE;
BEGIN
	SELECT
		MAX(time_created)
	INTO
		v_qualification_time
	FROM
		population_entries_hist
	WHERE
		population_entry_id = p_population_entry_id AND
		status_id IN (1, 5);
	RETURN(v_qualification_time);
END;

create or replace FUNCTION get_population_entry_cb_time(p_population_entry_id IN NUMBER) RETURN DATE AS
BEGIN
	FOR v_action IN (
		SELECT
			reaction_id,
			callback_time
		FROM
			issue_actions
		WHERE
			issue_id = (
				SELECT
					id
				FROM
					issues
				WHERE
					population_entry_id = p_population_entry_id
			)
		ORDER BY
			action_time DESC
	) LOOP
		IF v_action.reaction_id = 4 THEN
			RETURN(v_action.callback_time);
		ELSE
			-- not a call back
			RETURN(NULL);
		END IF;
	END LOOP;
	-- if no issue
	RETURN(NULL);
END;

create or replace FUNCTION get_population_entry_cb_id(p_population_entry_id IN NUMBER) RETURN NUMBER AS
BEGIN
	FOR v_action IN (
		SELECT
			id,
			reaction_id
		FROM
			issue_actions
		WHERE
			issue_id = (
				SELECT
					id
				FROM
					issues
				WHERE
					population_entry_id = p_population_entry_id
			)
		ORDER BY
			action_time DESC
	) LOOP
		IF v_action.reaction_id = 4 THEN
			RETURN(v_action.id);
		ELSE
			-- not a call back
			RETURN(NULL);
		END IF;
	END LOOP;
        -- if no issue
        RETURN(NULL);
END;

create or replace TRIGGER trg_aft_ins_investment
AFTER INSERT
	ON investments
	FOR EACH ROW
DECLARE
	v_amount_left NUMBER;
	v_balance NUMBER;
	v_tax_balance NUMBER;
	v_total_bonus NUMBER;
BEGIN
	-- write the balance_history record for the investment
	SELECT
		balance,
		tax_balance
	INTO
		v_balance,
		v_tax_balance
	FROM
		users
	WHERE
		id = :new.user_id;

	INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
	VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, :new.user_id, v_balance, v_tax_balance, 0, current_date, 'investments', :new.id, 10, 'GMT+00:00');

	-- check if any bonus amount used in this investment
--	SELECT
--		balance
--	INTO
--		v_balance
--	FROM
--		users
--	WHERE
--		id = :new.user_id;

	SELECT
		SUM(bonus_amount)
	INTO
		v_total_bonus
	FROM
		bonus_users
	WHERE
		user_id = :new.user_id AND
		bonus_state_id IN (2, 3);

	IF v_balance < v_total_bonus THEN
		-- bonus money were used in this investment
		v_amount_left := v_total_bonus - v_balance;

		FOR v_bonus IN (
			SELECT
				*
			FROM
				bonus_users
			WHERE
				user_id = :new.user_id AND
				bonus_state_id IN (2, 3) AND
				type_id IN (1, 3, 4, 5)
			ORDER BY
				time_created
		) LOOP
			UPDATE
				bonus_users
			SET
				bonus_state_id = 3,
				time_used = current_date
			WHERE
				id = v_bonus.id;

			v_amount_left := v_amount_left - v_bonus.bonus_amount;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END LOOP;
	END IF;

	-- now process active and used bonuses that have wagering condition
	v_amount_left := :new.amount;
	FOR v_bonus IN (
		SELECT
			*
		FROM
			bonus_users
		WHERE
			user_id = :new.user_id AND
			bonus_state_id IN (2, 3) AND
			NOT sum_invest_withdraw IS NULL AND
			sum_invest_withdraw > 0
		ORDER BY
			time_created
	) LOOP
		IF v_bonus.sum_invest_withdraw - v_bonus.sum_invest_withdraw_reached > v_amount_left THEN
			UPDATE
				bonus_users
			SET
				sum_invest_withdraw_reached = sum_invest_withdraw_reached + v_amount_left
			WHERE
				id = v_bonus.id;
			EXIT;
		ELSE
			v_amount_left := v_amount_left - (v_bonus.sum_invest_withdraw - v_bonus.sum_invest_withdraw_reached);

			-- bonus done
			UPDATE
				bonus_users
			SET
				sum_invest_withdraw_reached = sum_invest_withdraw,
				bonus_state_id = 4,
				time_done = current_date
			WHERE
				id = v_bonus.id;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;

	-- process granted bonuses that have wagering condition (types 5 and 6)
	v_amount_left := :new.amount;
	FOR v_bonus IN (
		SELECT
			*
		FROM
			bonus_users
		WHERE
			user_id = :new.user_id AND
			bonus_state_id = 1 AND
			(start_date IS NULL OR start_date <= current_date) AND
			(end_date IS NULL OR end_date >= current_date) AND
			NOT sum_invest_qualify IS NULL AND
			sum_invest_qualify > 0 AND
			type_id IN (5, 6)
		ORDER BY
			time_created
	) LOOP
		IF v_bonus.sum_invest_qualify - v_bonus.sum_invest_qualify_reached > v_amount_left THEN
			UPDATE
				bonus_users
			SET
				sum_invest_qualify_reached = sum_invest_qualify_reached + v_amount_left
			WHERE
				id = v_bonus.id;
			EXIT;
		ELSE
			v_amount_left := v_amount_left - (v_bonus.sum_invest_qualify - v_bonus.sum_invest_qualify_reached);

			-- qualified for bonus
			IF v_bonus.type_id = 5 THEN
				UPDATE
					bonus_users
				SET
					sum_invest_qualify_reached = sum_invest_qualify,
					bonus_state_id = 2,
					time_activated = current_date
				WHERE
					id = v_bonus.id;

				UPDATE
					users
				SET
					balance = balance + v_bonus.bonus_amount
				WHERE
					id = v_bonus.user_id;

				SELECT
					balance,
					tax_balance
				INTO
					v_balance,
					v_tax_balance
				FROM
					users
				WHERE
					id = v_bonus.user_id;

				INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
				VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, v_bonus.user_id, v_balance, v_tax_balance, 0, current_date, 'bonus_users', v_bonus.id, 15, 'GMT+00:00');
			END IF;

			IF v_bonus.type_id = 6 THEN
				UPDATE
					bonus_users
				SET
					sum_invest_qualify_reached = sum_invest_qualify,
					bonus_state_id = 2,
					time_activated = current_date
				WHERE
					id = v_bonus.id;

--				UPDATE
--					users
--				SET
--					is_next_invest_on_us = 1
--				WHERE
--					id = :new.user_id;
			END IF;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;
END;

CREATE OR REPLACE TRIGGER trg_aft_upd_transaction
AFTER UPDATE
	ON transactions
	FOR EACH ROW
DECLARE
	v_transaction_class NUMBER;
	v_sum_invest_withdraw NUMBER;
	v_amount NUMBER;
	v_balance NUMBER;
	v_tax_balance NUMBER;
	v_bonus_tr_id NUMBER;
BEGIN
	SELECT
		class_type
	INTO
		v_transaction_class
	FROM
		transaction_types
	WHERE
		id = :new.type_id;

	IF v_transaction_class = 1 AND :new.status_id IN (2, 7, 9) AND NOT :old.status_id IN (2, 7, 9) THEN
		-- successful deposit
		FOR v_bonus IN (
			SELECT
				*
			FROM
				bonus_users
			WHERE
				user_id = :new.user_id AND
				bonus_state_id = 1 AND
				(start_date IS NULL OR start_date <= current_date) AND
				(end_date IS NULL OR end_date >= current_date) AND
				type_id IN (3, 4)
			ORDER BY
				time_created
		) LOOP
			IF v_bonus.type_id = 3 OR (v_bonus.type_id = 4 AND :new.amount >= v_bonus.min_deposit_amount AND :new.amount <= v_bonus.max_deposit_amount) THEN
				IF v_bonus.number_of_actions_reached + 1 = v_bonus.number_of_actions THEN
					IF v_bonus.type_id = 3 THEN
						v_amount := v_bonus.bonus_amount;
						v_sum_invest_withdraw := v_bonus.sum_invest_withdraw;
					ELSE
						v_amount := :new.amount * v_bonus.bonus_percent;
						v_sum_invest_withdraw := v_amount * v_bonus.wagering_parameter;
					END IF;

					UPDATE
						bonus_users
					SET
						bonus_state_id = 2,
						time_activated = current_date,
						number_of_actions_reached = number_of_actions_reached + 1,
						bonus_amount = v_amount,
						sum_invest_withdraw = v_sum_invest_withdraw
					WHERE
						id = v_bonus.id;

					UPDATE
						users
					SET
						balance = balance + v_amount
					WHERE
						id = v_bonus.user_id;

					SELECT
						balance,
						tax_balance
					INTO
						v_balance,
						v_tax_balance
					FROM
						users
					WHERE
						id = v_bonus.user_id;

					SELECT
						SEQ_TRANSACTIONS.NEXTVAL
					INTO
						v_bonus_tr_id
					FROM
						dual;

					INSERT INTO transactions (id, user_id, type_id, time_created, amount, status_id, writer_id, ip, time_settled, comments, utc_offset_created, rate, bonus_user_id)
					VALUES (v_bonus_tr_id, v_bonus.user_id, 12, current_date, v_amount, 2, :new.writer_id, :new.ip, current_date, 'bonus ' || v_bonus.id, :new.utc_offset_created, :new.rate, v_bonus.id);

					INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
					VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, v_bonus.user_id, v_balance, v_tax_balance, 0, current_date, 'transactions', v_bonus_tr_id, 15, 'GMT+00:00');

--					INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
--					VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, v_bonus.user_id, v_balance, v_tax_balance, 0, current_date, 'bonus_users', v_bonus.id, 15, 'GMT+00:00');
				ELSE
					UPDATE
						bonus_users
					SET
						number_of_actions_reached = number_of_actions_reached + 1
					WHERE
						id = v_bonus.id;
				END IF;
			END IF;
		END LOOP;
	END IF;
END;

insert into bonus_users (id, user_id, user_action_id, start_date, end_date, bonus_state_id, bonus_amount, bonus_percent, min_deposit_amount, max_deposit_amount, sum_invest_qualify, writer_id, time_created, sum_invest_qualify_reached, sum_invest_withdraw, sum_invest_withdraw_reached, type_id)
values (seq_bonus_users.nextval, 9927, 1, null, null, 1, 10000, null, null, null, 100000, 0, current_date, 0, 100000, 0, 5);

insert into bonus_users (id, user_id, user_action_id, start_date, end_date, bonus_state_id, bonus_amount, bonus_percent, min_deposit_amount, max_deposit_amount, sum_invest_qualify, writer_id, time_created, sum_invest_qualify_reached, sum_invest_withdraw, sum_invest_withdraw_reached, type_id)
values (seq_bonus_users.nextval, 9927, 1, null, null, 1, 0, null, null, null, 100000, 0, current_date, 0, 100000, 0, 6);

insert into bonus_users (id, user_id, user_action_id, start_date, end_date, bonus_state_id, bonus_amount, bonus_percent, min_deposit_amount, max_deposit_amount, sum_invest_qualify, writer_id, time_created, sum_invest_qualify_reached, sum_invest_withdraw, sum_invest_withdraw_reached, type_id)
values (seq_bonus_users.nextval, 9927, 1, null, null, 1, 10000, null, 10000, 100000, null, 0, current_date, 0, 100000, 0, 3);

insert into bonus_users (id, user_id, user_action_id, start_date, end_date, bonus_state_id, bonus_amount, bonus_percent, min_deposit_amount, max_deposit_amount, sum_invest_qualify, writer_id, time_created, sum_invest_qualify_reached, sum_invest_withdraw, sum_invest_withdraw_reached, type_id)
values (seq_bonus_users.nextval, 9927, 1, null, null, 1, null, 0.1, 10000, 100000, null, 0, current_date, 0, 100000, 0, 4);

create or replace TRIGGER trg_aft_ins_pop_entries_hist
AFTER INSERT
	ON population_entries_hist
	FOR EACH ROW
DECLARE
v_status_id number;
v_is_locked number;
v_qualification_time date;

BEGIN
      SELECT qualification_time, is_locked, status_id
      INTO v_qualification_time, v_is_locked, v_status_id
      FROM population_entries
      WHERE 	id = :new.population_entry_id;

       if (:new.status_id = 3) then
            -- if status id = lock, update the locking writer
            v_is_locked := :new.writer_id;
        elsif (:new.status_id = 4) then
            --  if status id = lock, update the entry to bu unlocked
            v_is_locked := 0;
        else
            -- if status id is not lock or unlock update status_id
            v_status_id := :new.status_id;
            if (:new.status_id in (1,5)) then
                  v_qualification_time := :new.time_created;
            end if;
        end if;

	UPDATE
		population_entries
	SET
       status_id = v_status_id ,
       is_locked = v_is_locked,
       qualification_time = v_qualification_time

	WHERE
		id = :new.population_entry_id;
END;




CREATE OR REPLACE TRIGGER TRG_AFT_INS_ISSUE_ACTION
AFTER INSERT ON ISSUE_ACTIONS
FOR EACH ROW

DECLARE
        issue_action_type   ISSUE_ACTION_TYPES%ROWTYPE;
BEGIN

        Select *
        into      issue_action_type
        from     issue_action_types iat
        where   iat.id = :new.issue_action_type_id;

        -- If call update last_call_action_id and Check if reached
        IF (issue_action_type.channel_id = 1) THEN
              -- IF reached call update calls_num and reached_calls_num
              IF (issue_action_type.reached_status_id = 2) THEN
                      update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  calls_num = calls_num + 1,
                                  reached_calls_num = reached_calls_num + 1,
                                  last_reached_call_action_id = :new.id
                      where  id = :new.issue_id;
              ELSE
                     update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  calls_num = calls_num + 1
                      where  id = :new.issue_id;
              END IF; -- IF (issue_action_type.reached_status_id = 2) THEN
        ELSE
              update issues
              set       last_action_id = :new.id,
                          is_significant = is_significant + :new.is_significant
              where  id = :new.issue_id;
        END IF; --  IF (issue_action_type.channel_id = 1) THEN

END;


CREATE OR REPLACE FUNCTION get_today_last_hclosing_time(p_market_id IN NUMBER) RETURN VARCHAR2 AS
BEGIN
	FOR v_entry IN (
		SELECT
			to_char(time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing
		FROM
			opportunities
		WHERE
			market_id = p_market_id AND
			NOT time_act_closing IS NULL
--			scheduled = 1 AND
--			time_act_closing > current_timestamp - to_dsinterval('0 02:00:00')
		ORDER BY
			time_est_closing DESC
	) LOOP
		RETURN(v_entry.time_est_closing);
	END LOOP;
	RETURN(NULL);
END;

CREATE OR REPLACE FUNCTION get_current_hour_opening_time(p_market_id IN NUMBER) RETURN VARCHAR2 AS
BEGIN
	-- first check if we have opened hour opp
	FOR v_entry IN (
		SELECT
			to_char(time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest
		FROM
			opportunities
		WHERE
			market_id = p_market_id AND
			scheduled = 1 AND
			is_published = 1
		ORDER BY
			time_est_closing
	) LOOP
		RETURN(v_entry.time_first_invest);
	END LOOP;

	-- if not get the the time last opp on this market closed
	FOR v_entry IN (
		SELECT
			to_char(time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing
		FROM
			opportunities
		WHERE
			market_id = p_market_id AND
			NOT time_act_closing IS NULL
		ORDER BY
			time_est_closing DESC
	) LOOP
		RETURN(v_entry.time_est_closing);
	END LOOP;

	RETURN(NULL);
END;

create or replace TRIGGER trg_aft_ins_investment
AFTER INSERT
	ON investments
	FOR EACH ROW
DECLARE
	v_amount_left NUMBER;
	v_balance NUMBER;
	v_tax_balance NUMBER;
	v_total_bonus NUMBER;
BEGIN
	-- write the balance_history record for the investment
	SELECT
		balance,
		tax_balance
	INTO
		v_balance,
		v_tax_balance
	FROM
		users
	WHERE
		id = :new.user_id;

	INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
	VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, :new.user_id, v_balance, v_tax_balance, 0, current_date, 'investments', :new.id, 10, 'GMT+00:00');

	-- check if any bonus amount used in this investment
--	SELECT
--		balance
--	INTO
--		v_balance
--	FROM
--		users
--	WHERE
--		id = :new.user_id;

	SELECT
		SUM(bonus_amount)
	INTO
		v_total_bonus
	FROM
		bonus_users
	WHERE
		user_id = :new.user_id AND
		bonus_state_id IN (2, 3);

	IF v_balance < v_total_bonus THEN
		-- bonus money were used in this investment
		v_amount_left := v_total_bonus - v_balance;

		FOR v_bonus IN (
			SELECT
				*
			FROM
				bonus_users
			WHERE
				user_id = :new.user_id AND
				bonus_state_id IN (2, 3) AND
				type_id IN (1, 3, 4, 5)
			ORDER BY
				time_created
		) LOOP
			UPDATE
				bonus_users
			SET
				bonus_state_id = 3,
				time_used = current_date
			WHERE
				id = v_bonus.id;

			v_amount_left := v_amount_left - v_bonus.bonus_amount;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END LOOP;
	END IF;

	-- now process active and used bonuses that have wagering condition
	v_amount_left := :new.amount;
	FOR v_bonus IN (
		SELECT
			*
		FROM
			bonus_users
		WHERE
			user_id = :new.user_id AND
			bonus_state_id IN (2, 3) AND
			NOT sum_invest_withdraw IS NULL AND
			sum_invest_withdraw > 0
		ORDER BY
			time_created
	) LOOP
		IF v_bonus.sum_invest_withdraw - v_bonus.sum_invest_withdraw_reached > v_amount_left THEN
			UPDATE
				bonus_users
			SET
				sum_invest_withdraw_reached = sum_invest_withdraw_reached + v_amount_left
			WHERE
				id = v_bonus.id;
			EXIT;
		ELSE
			v_amount_left := v_amount_left - (v_bonus.sum_invest_withdraw - v_bonus.sum_invest_withdraw_reached);

			-- bonus done
			UPDATE
				bonus_users
			SET
				sum_invest_withdraw_reached = sum_invest_withdraw,
				bonus_state_id = 4,
				time_done = current_date
			WHERE
				id = v_bonus.id;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;

	-- process granted bonuses that have wagering condition (types 5 and 6)
	v_amount_left := :new.amount;
	FOR v_bonus IN (
		SELECT
			*
		FROM
			bonus_users
		WHERE
			user_id = :new.user_id AND
			bonus_state_id = 1 AND
			(start_date IS NULL OR start_date <= current_date) AND
			(end_date IS NULL OR end_date >= current_date) AND
			NOT sum_invest_qualify IS NULL AND
			sum_invest_qualify > 0 AND
			type_id IN (5, 6)
		ORDER BY
			time_created
	) LOOP
		IF v_bonus.sum_invest_qualify - v_bonus.sum_invest_qualify_reached > v_amount_left THEN
			UPDATE
				bonus_users
			SET
				sum_invest_qualify_reached = sum_invest_qualify_reached + v_amount_left
			WHERE
				id = v_bonus.id;
			EXIT;
		ELSE
			v_amount_left := v_amount_left - (v_bonus.sum_invest_qualify - v_bonus.sum_invest_qualify_reached);

			-- qualified for bonus
			IF v_bonus.type_id = 5 THEN
				UPDATE
					bonus_users
				SET
					sum_invest_qualify_reached = sum_invest_qualify,
					bonus_state_id = 2,
					time_activated = current_date
				WHERE
					id = v_bonus.id;

				UPDATE
					users
				SET
					balance = balance + v_bonus.bonus_amount
				WHERE
					id = v_bonus.user_id;

				SELECT
					balance,
					tax_balance
				INTO
					v_balance,
					v_tax_balance
				FROM
					users
				WHERE
					id = v_bonus.user_id;

				INSERT INTO balance_history (id, user_id, balance, tax_balance, writer_id, time_created, table_name, key_value, command, utc_offset)
				VALUES (SEQ_BALANCE_HISTORY.NEXTVAL, v_bonus.user_id, v_balance, v_tax_balance, 0, current_date, 'bonus_users', v_bonus.id, 15, 'GMT+00:00');
			END IF;

			IF v_bonus.type_id = 6 THEN
				UPDATE
					bonus_users
				SET
					sum_invest_qualify_reached = sum_invest_qualify,
					bonus_state_id = 2,
					time_activated = current_date
				WHERE
					id = v_bonus.id;

--				UPDATE
--					users
--				SET
--					is_next_invest_on_us = 1
--				WHERE
--					id = :new.user_id;
			END IF;

			IF v_amount_left <= 0 THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;

	FOR v_bonus IN (
		SELECT
			*
		FROM
			bonus_users
		WHERE
			user_id = :new.user_id AND
			bonus_state_id = 1 AND
			(start_date IS NULL OR start_date <= current_date) AND
			(end_date IS NULL OR end_date >= current_date) AND
			type_id= 7
   		ORDER BY
			time_created
	) LOOP
		IF v_bonus.number_of_actions_reached +1 = v_bonus.number_of_actions THEN
		    UPDATE
                bonus_users
            SET
                bonus_state_id=2,
                number_of_actions_reached = number_of_actions_reached +1,
                time_activated=current_date
            WHERE
                user_id=v_bonus.user_id;

            EXIT;
		ELSE

			IF v_bonus.number_of_actions_reached +1 < v_bonus.number_of_actions THEN
				UPDATE
					bonus_users
				SET
					number_of_actions_reached = number_of_actions_reached+1
    			WHERE
					user_id=v_bonus.user_id;
                EXIT;
			END IF;
		END IF;
	END LOOP;
END;

create create or replace PROCEDURE POPULATION_MINIMUM_BALANCE AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                        A.id
                                                                 from (
                                                                                  select
                                                                                          Y.*
                                                                                  from
                                                                                          population_entries Y, populations P
                                                                                  where
                                                                                           Y.user_id = p_user_id  AND
                                                                                           Y.population_id = P.id AND
                                                                                           P.population_type_id = pop_type

                                                                                  order by
                                                                                          Y.TIME_CREATED desc
                                                                                ) A
                                                                 WHERE
                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
      B.number_of_days,
      A.currency_id
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 8 AND
      B.is_active = 1 AND
			IS_USER_IN_POP_TYPE(A.id, 8 )= 0 AND
      B.number_of_deposits = CASE WHEN GET_NUMBER_OF_SUCCESS_DEPOSITE(A.id) >= 6 THEN 6 ELSE GET_NUMBER_OF_SUCCESS_DEPOSITE(A.id)  END AND
      A.balance < (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) AND
      NOT EXISTS (
                              select
                                1
                              from
                                transactions t
                              where
                                t.type_id in (select id from transaction_types where class_type = 2) and
                                t.status_id in (4,9) and
--                                t.time_created > sysdate - (B.NUMBER_OF_DAYS) and
                                t.user_id = A.id
                              ) AND
--      A.id in (
--                      select
--                        distinct bh.user_id
--                      from
--                        balance_history bh
----                      where get_is_user_under_balance(bh.user_id, (B.NUMBER_OF_DAYS) , (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) ) = 1
----                      where bh.user_id not in (
----                                                                      select bh.user_id
----                                                                      from balance_history bh
----                                                                      where bh.balance > (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) and
----                                                                      bh.time_created > sysdate - (B.NUMBER_OF_DAYS)
----                                                                    )
--                      where  bh.user_id in (
--                                                                  select
--                                                                    distinct u.id
--                                                                  from
--                                                                    users u
--                                                                  where
--                                                                    u.balance < (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID)
--                                                                ) and
--                      bh.user_id not in (
--                                                            select
--                                                              distinct t.user_id
--                                                            from
--                                                              transactions t
--                                                            where
--                                                              t.type_id in (select id from transaction_types where class_type = 2) and
--                                                              t.status_id in (4,9) and
--                                                              t.time_created > sysdate - (B.NUMBER_OF_DAYS)
--                                                            )
--              ) AND
NOT EXISTS (
                          select
                            1
                          from
                            INVESTMENTS i
                           where
                             i.IS_SETTLED = 0 and
                             i.USER_ID = A.id
                      ) AND
NOT EXISTS(
                        SELECT
                          1
                        FROM
                          users u
                        WHERE
                          u.id = A.id AND
                          (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0 OR u.id IN (SELECT USER_ID FROM frauds))
                        ) AND
NOT EXISTS(
                        SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       ) AND
get_is_user_under_balance(A.id, (B.NUMBER_OF_DAYS) , (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) ) = 1
	) LOOP

                        open get_user_last_population_type(v_usr.id, 8);
                        fetch get_user_last_population_type into populationEntHist;

                        IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107))  THEN

                             IF (GET_BALANCE_UNDER_INV_LIMIT(v_usr.id,v_usr.currency_id) <= populationEntHist.TIME_CREATED) THEN
                                    close get_user_last_population_type;
                                    GOTO end_loop;
                             END IF;
                        END IF;

                        close get_user_last_population_type;

                        open get_user_last_population(v_usr.id);
                        fetch get_user_last_population into populationEntHist;

                        IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = 8;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        close get_user_last_population;
                                        GOTO end_loop;
                                END IF;
                        END IF;

                        close get_user_last_population;

                        SELECT
                                COUNT(id) INTO v_counter
                        FROM
                                population_entries
                        WHERE
                                population_id = v_usr.populationId;

                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

--                        SELECT
--                                id INTO v_populationEntryId
--                        FROM
 --                               population_entries
 --                       WHERE
--                                population_id = v_usr.populationId AND
 --                               user_id = v_usr.id;

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'Minimum balance no deposit';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_BALANCE_UNDER_INV_LIMIT(v_usr.id,v_usr.currency_id) + v_usr.number_of_days;
                        INSERT INTO population_entries_hist VALUES v_peh;
                        <<end_loop>>
                        NULL;
	END LOOP;
END;

====================================================================================

create or replace
FUNCTION           "GET_BALANCE_UNDER_INV_LIMIT"
( par_userId IN NUMBER
, par_currencyId IN NUMBER
) RETURN DATE AS
par_firstTime DATE;
temp_min_limit NUMBER;

BEGIN
      select
                ilgc.value
                into temp_min_limit
          from
                INVESTMENT_LIMITS il,
                investment_limit_group_curr ilgc
          where
                 il.id = 1 -- Default binary limits id
                AND il.min_limit_group_id = ilgc.investment_limit_group_id
                AND ilgc.CURRENCY_ID = par_currencyId;


      select
        *
      INTO par_firstTime
      from
        (select
              bh.TIME_CREATED
        from
              BALANCE_HISTORY bh
        where
              bh.TIME_CREATED > (select
                                                          *
                                                    from
                                                          (select
                                                                bh1.TIME_CREATED
                                                          from
                                                                BALANCE_HISTORY bh1
                                                          where
                                                                 bh1.BALANCE > temp_min_limit AND
                                                                 bh1.USER_ID = par_userId
                                                          order by
                                                                 bh1.time_created desc)
                                                      where
                                                          rownum = 1) AND
              bh.USER_ID = par_userId
        ORDER BY
              bh.TIME_CREATED)
      WHERE
        rownum = 1;

RETURN par_firstTime;

EXCEPTION
  when no_data_found then
  select TIME_CREATED
  into par_firstTime
  from(
        select
              bh.TIME_CREATED
        from
              BALANCE_HISTORY bh
         where
              bh.user_id = par_userid
         order by time_created asc)
where rownum =1;


         RETURN par_firstTime;

END GET_BALANCE_UNDER_INV_LIMIT;

====================================================================================

create or replace
PROCEDURE POPULATION_DORMANT_ACCOUNT_NEW AS
	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
        v_peh_old population_entries_hist%ROWTYPE;
        populationEntHist population_entries_hist%ROWTYPE;
        populationEntHistPriority NUMBER;
        populationPriority NUMBER;
        populationEntHistStatus NUMBER;

        cursor get_user_last_population( p_user_id NUMBER) is
        SELECT
            PEHO.*
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                                      A.id
                                                                                            from (
                                                                                                      select
                                                                                                              Y.*
                                                                                                      from
                                                                                                              population_entries Y
                                                                                                      where
                                                                                                               Y.user_id = p_user_id
                                                                                                      order by
                                                                                                              Y.TIME_CREATED desc
                                                                                                    ) A
                                                                                          WHERE
                                                                                                  rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


        cursor get_user_last_population_type( p_user_id NUMBER, pop_type NUMBER) is
        SELECT
                      PEHO.*
              FROM
                       (SELECT
                          Z.*
                        FROM
                            population_entries_hist Z
                         WHERE
                            Z.population_entry_id = (select
                                                                        A.id
                                                                 from (
                                                                                  select
                                                                                          Y.*
                                                                                  from
                                                                                          population_entries Y, populations P
                                                                                  where
                                                                                           Y.user_id = p_user_id  AND
                                                                                           Y.population_id = P.id AND
                                                                                           P.population_type_id = pop_type

                                                                                  order by
                                                                                          Y.TIME_CREATED desc
                                                                                ) A
                                                                 WHERE
                                                                      rownum = 1)
                          ORDER BY
                            Z.TIME_CREATED desc) PEHO
              WHERE
                      rownum = 1 AND
                      PEHO.STATUS_ID not in(4,3);

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
                        B.LAST_INVEST_DAY
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
			B.population_type_id = 9 AND
			current_date - GET_LAST_INVEST_TIME(A.id) >= B.LAST_INVEST_DAY  AND
      A.balance > (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) and
                          NOT EXISTS(
                                select
                                      1
                                from
                                      balance_history bh
                                where
                                      bh.user_id = A.id AND
                                      bh.balance < (select min from INVESTMENT_LIMITS il where A.CURRENCY_ID = il.CURRENCY_ID) and
                                      bh.time_created >   current_date - B.LAST_INVEST_DAY
                         ) AND

			IS_USER_IN_POP_TYPE(A.id, 9 )= 0 AND


                      NOT EXISTS(
				SELECT
					1
				FROM
					users u
				WHERE
					u.id = A.id AND
                                        (u.class_id = 0 OR u.is_active = 0 OR u.is_false_account = 1 OR u.is_contact_by_phone = 0 OR u.id IN (SELECT USER_ID FROM frauds))
                                        ) AND



                      NOT EXISTS(
                                SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
                       )


	) LOOP

                        open get_user_last_population_type(v_usr.id, 9);
                        fetch get_user_last_population_type into populationEntHist;

                        IF ((get_user_last_population_type%FOUND) AND (populationEntHist.status_id = 100 OR populationEntHist.status_id = 101
                                  OR populationEntHist.status_id = 107))  THEN

                             IF (GET_LAST_invest_time(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                                    close get_user_last_population_type;
                                    GOTO end_loop;
                             END IF;
                        END IF;

                        close get_user_last_population_type;

                        open get_user_last_population(v_usr.id);
                        fetch get_user_last_population into populationEntHist;

                        IF (get_user_last_population%FOUND and populationEntHist.status_id < 100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = 9;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        close get_user_last_population;
                                        GOTO end_loop;
                                END IF;
                        END IF;

                        close get_user_last_population;

                        SELECT
                                COUNT(id) INTO v_counter
                        FROM
                                population_entries
                        WHERE
                                population_id = v_usr.populationId;

                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

--                        SELECT
--                                id INTO v_populationEntryId
--                        FROM
 --                               population_entries
 --                       WHERE
--                                population_id = v_usr.populationId AND
 --                               user_id = v_usr.id;

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'New Dormant Accounts';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_LAST_INVEST_TIME(v_usr.id) +v_usr.LAST_INVEST_DAY;
                        INSERT INTO population_entries_hist VALUES v_peh;
                        <<end_loop>>
                        NULL;
	END LOOP;
END;

create or replace PROCEDURE "QUALIFY_INV_FOR_INSURANCES" (p_gm_start IN VARCHAR, p_gm_end IN VARCHAR) AS
	v_cg NUMBER;
BEGIN
	FOR v_user IN (
		SELECT
			O.id,
			O.user_name
		FROM
			users O
		WHERE
			EXISTS(
				SELECT
					A.id
				FROM
					investments A,
					opportunities B
				WHERE
					A.opportunity_id = B.id AND
					A.user_id = O.id AND
					B.scheduled = 1 AND
					A.is_settled = 0 AND
					A.bonus_odds_change_type_id = 0 AND
					B.opportunity_type_id = 1 AND
					A.time_created <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
					current_timestamp >= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
					current_timestamp <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_end || ':00'))
	) LOOP
		v_cg := 0;
		IF LENGTH(v_user.user_name) > 3 AND SUBSTR(v_user.user_name, LENGTH(v_user.user_name) - 2, 3) = '_CG' THEN
			v_cg := 1;
		END IF;

		-- Insurances only for not calcalist game users
		IF v_cg = 0 THEN
			-- process Golden Minutes investments
			UPDATE
				investments
			SET
				insurance_flag = 1
			WHERE
				id IN (
					SELECT
						A.id
					FROM
						investments A,
						opportunities B,
						markets C
					WHERE
						A.opportunity_id = B.id AND
						B.market_id = C.id AND
						C.is_golden_minutes = 1 AND
						B.scheduled = 1 AND
						A.is_settled = 0 AND
						A.bonus_odds_change_type_id = 0 AND
						B.opportunity_type_id = 1 AND
						A.user_id = v_user.id AND
						(A.insurance_amount_ru = 0 OR A.insurance_amount_ru is null) AND
						A.time_created <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
						current_timestamp >= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
						current_timestamp <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_end || ':00') AND
						CASE WHEN A.type_id = 1 THEN B.gm_level - A.current_level ELSE A.current_level - B.gm_level END / A.current_level > C.insurance_qualify_percent
						exists ( SELECT
                                  1
                            FROM
                                  opportunities o, opportunities o2
                            WHERE
                                  o2.id =  A.opportunity_id
                                  AND  o.market_id =  o2.market_id
                                  AND o.TIME_EST_CLOSING >  o2.TIME_EST_CLOSING
                                  AND ((o.market_id  = 3  AND o.scheduled = 1) OR ( o.market_id != 3  AND o.scheduled in (1,2)))
                                  AND to_char(o.time_est_closing,'DDMMYYYY') =   to_char(o2.time_est_closing,'DDMMYYYY')
                        )
				);
		END IF;

		-- if no Golden Minutes investments then process Roll Up
		IF v_cg = 0 AND SQL%ROWCOUNT = 0 THEN
			UPDATE
				investments
			SET
				insurance_flag = 2
			WHERE
				id IN (
					SELECT
						A.id
					FROM
						investments A,
						opportunities B,
						markets C
					WHERE
						A.opportunity_id = B.id AND
						B.market_id = C.id AND
						C.is_roll_up = 1 AND
						B.scheduled = 1 AND
						A.is_settled = 0 AND
						A.bonus_odds_change_type_id = 0 AND
						B.opportunity_type_id = 1 AND
						A.user_id = v_user.id AND
						A.insurance_amount_ru + ((A.amount - A.insurance_amount_ru) * C.roll_up_premia_percent) < (A.amount - A.insurance_amount_ru) * A.odds_win AND
						A.time_created <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
						current_timestamp >= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
						current_timestamp <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_end || ':00') AND
						CASE WHEN A.type_id = 1 THEN B.gm_level - A.current_level ELSE A.current_level - B.gm_level END / A.current_level < -C.roll_up_qualify_percent
				);
		END IF;

		UPDATE
			investments
		SET
			insurance_flag = 0
		WHERE
			id IN (
				SELECT
					A.id
				FROM
					investments A,
					opportunities B
				WHERE
					A.opportunity_id = B.id AND
					B.scheduled = 1 AND
					A.is_settled = 0 AND
					A.bonus_odds_change_type_id = 0 AND
					B.opportunity_type_id = 1 AND
					A.user_id = v_user.id AND
					A.time_created <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
					current_timestamp >= B.time_est_closing - to_dsinterval('0 00:' || p_gm_start || ':00') AND
					current_timestamp <= B.time_est_closing - to_dsinterval('0 00:' || p_gm_end || ':00') AND
					A.insurance_flag IS NULL
		 	);
	END LOOP;
END;

create or replace FUNCTION GET_LOYALTY_USERS
RETURN LOYALTY_TABLE_DATA_LIST PIPELINED AS

v_data  LOYALTY_TABLE_DATA;

TIER_ACTION_POINTS_EXPIRED NUMBER := 5;
TIER_ACTION_EXPIRATION_WARNING NUMBER := 8;

-- Statuses from Constant base.
JOB_ACTION_UPGRADE NUMBER := 1;
JOB_ACTION_DOWNGRADE NUMBER := 2;
JOB_ACTION_EXPIRED NUMBER := 3;
JOB_ACTION_WARNING NUMBER :=4;
JOB_ACTION_REQUALIFICATION NUMBER :=5;

WARNING_PERIOD NUMBER  :=30;

v_tier_user              tier_users%ROWTYPE;
v_tier                      tiers %ROWTYPE;
v_next_tier              tiers %ROWTYPE;
v_user_history         tier_users_history %ROWTYPE;

next_tier_level NUMBER;
last_tier_history_action NUMBER;
previous_tier_id NUMBER;
new_tier_id NUMBER;
user_current_tier_level NUMBER;
user_tier_level_before NUMBER;
user_tier_level_after NUMBER;
accumulated_points  NUMBER;
points_for_job  NUMBER;
accumulation_period NUMBER;
exp_accumulated_points NUMBER;
is_expiration_warning NUMBER;
is_expired NUMBER;
loyalty_action NUMBER;
exp_warning_date DATE;
exp_warning_check_date DATE;
is_downgrade NUMBER;
is_upgrade NUMBER;
is_requalified NUMBER;

BEGIN
    -- Check skins which have loyalty tiers??
DBMS_OUTPUT.PUT_LINE('GET_LOYALTY_UPDATED_USERS started at ' || sysdate);

    FOR v_user IN (SELECT	*
                              FROM    users u
--                              WHERE id = 11327
                              WHERE u.class_id != 0
                                    AND u.TIER_USER_ID is not null
                                    ) LOOP

           SELECT      temp.tier_his_action
           INTO           last_tier_history_action
           FROM         (SELECT      tuh.tier_his_action
                                 FROM         tier_users_history tuh
                                 WHERE       tuh.user_id = v_user.id
                                 ORDER BY tuh.time_created desc) temp
           WHERE       rownum = 1;

          -- If  users last tier history action is expired, do nothing, oterwise check loyalty.
          if (last_tier_history_action != TIER_ACTION_POINTS_EXPIRED) then
                    -- Action flags
                     is_expired := 0;
                     is_expiration_warning := 0;
                     is_downgrade := 0;
                     is_upgrade := 0;
                     is_requalified := 0;

                     loyalty_action :=0;
                     new_tier_id := 0;
                     points_for_job := 0;

--                    DBMS_OUTPUT.PUT_LINE('v_user  ' || v_user.id);

                     -- Get current tier details for the user.
                    SELECT *
                    INTO      v_tier_user
                    FROM    tier_users tu
                    WHERE tu.user_id = v_user.id
                          AND rownum = 1;

                    previous_tier_id :=  v_tier_user.tier_id;

                    if (previous_tier_id is null) then
                        previous_tier_id := 0;
                   end if;

--                  DBMS_OUTPUT.PUT_LINE('previous_tier_id  ' || previous_tier_id);

                   --  if the user is in one of the tiers
                     if (previous_tier_id > 0) then

                          -- Get user tier details.
                          SELECT *
                          INTO      v_tier
                          FROM    tiers t
                          WHERE t.id= previous_tier_id;

                          -- if the user has passed the registration limitation time
                          if ((v_user.time_created + v_tier.MIN_QUALIFICATION_PERIOD) < sysdate) then

                                   -- Get user current tier level.
                                   SELECT ts.tier_level
                                   INTO      user_current_tier_level
                                   FROM    tier_skins ts
                                   WHERE  ts.tier_id = previous_tier_id
                                         AND  ts.skin_id = v_user.skin_id;

                                   -- Get points accumulation period in days for user's skin.
                                  SELECT s.tier_qualification_period
                                   INTO      accumulation_period
                                   FROM    skins s
                                   WHERE  s.id = v_user.skin_id;


                                  -- Save the current user tier level (in case there will be a down grade)
                                  user_tier_level_before :=  user_current_tier_level;
                                  user_tier_level_after := user_current_tier_level;

--                                   DBMS_OUTPUT.PUT_LINE('user_current_tier_level  ' || user_current_tier_level);

                                  -- Get user points for the relevant accumulation period.
                                   accumulated_points := get_loyalty_accumulated_points(v_user.id,v_tier_user.qualification_time);

                                   -- If user is in a tier level higer then the lowest one, check downgrade.
                                   if  (user_current_tier_level > 0) then

--                                            DBMS_OUTPUT.PUT_LINE('v_tier_user.qualification_time  ' || v_tier_user.qualification_time);
                                          -- Check whether downgrade is needed for that user.
                                          if  ((v_tier_user.qualification_time + accumulation_period) <=  sysdate) then

--                                                  DBMS_OUTPUT.PUT_LINE('downgrade or requalification');
                                                  user_current_tier_level := 0;

                                            end if; --  if  ((v_tier_user.qualification_time + accumulation_period) <=  sysdate) then

                                   end if; -- if  (user_current_tier_level > 0) then

--                                  DBMS_OUTPUT.PUT_LINE('accumulated_points  ' || accumulated_points);
--
                                   -- Get the new tier level where the user should be at.
                                   SELECT  MAX(ts2.tier_level)
                                   INTO       next_tier_level
                                   FROM     tier_skins ts2,tiers t
                                   WHERE   ts2.tier_id = t.id
                                          AND  ts2.skin_id = v_user.skin_id
                                          AND  accumulated_points >= t.qualification_points;

                                    -- If user has fewer point then the lowest level tier qualification points, set his tier to the lowest.
                                    if (next_tier_level is null) then

                                            SELECT  MIN(ts2.tier_level)
                                            INTO       next_tier_level
                                            FROM     tier_skins ts2,tiers t
                                            WHERE   ts2.tier_id = t.id
                                                  AND  ts2.skin_id = v_user.skin_id;

                                    end if; -- if (next_tier_level is null)

                                     -- Get the tier where the user should be at.
                                     SELECT t.*
                                     INTO     v_next_tier
                                     FROM   tier_skins ts, tiers t
                                     WHERE t.id = ts.tier_id
                                          AND ts.skin_id = v_user.skin_id
                                           AND ts.tier_level  = next_tier_level;

--                                    DBMS_OUTPUT.PUT_LINE('next_tier_level  ' || next_tier_level);
--                                    DBMS_OUTPUT.PUT_LINE('user_current_tier_level  ' || user_current_tier_level);

                                    -- Check whether the user should be in a higher level
                                    if (user_current_tier_level < next_tier_level) then
--                                          DBMS_OUTPUT.PUT_LINE('upgrade tier (or downgrade)');
                                          user_tier_level_after :=  next_tier_level;
                                          new_tier_id :=  v_next_tier.id;

--                                          DBMS_OUTPUT.PUT_LINE('user_tier_level_before  ' || user_tier_level_before);
--                                          DBMS_OUTPUT.PUT_LINE('user_tier_level_after  ' || user_tier_level_after);
                                          if (user_tier_level_before > user_tier_level_after) then
--                                                DBMS_OUTPUT.PUT_LINE('downgrade');
                                                 is_downgrade := 1;
                                          elsif (user_tier_level_before < user_tier_level_after) then
--                                               DBMS_OUTPUT.PUT_LINE('upgrade');
                                                 is_upgrade := 1;
                                                 points_for_job := accumulated_points - v_next_tier.qualification_points;
                                          elsif (user_tier_level_before = user_tier_level_after) then
--                                                 DBMS_OUTPUT.PUT_LINE('reQualified');
                                                 is_requalified := 1;
                                                 points_for_job := accumulated_points - v_next_tier.qualification_points;
                                          end if;

--                                          loyalty_update_users_tier(v_user.id, new_tier_id ,0,action_points);

                                    end if; --  if (user_current_tier_level < v_next_tier_level.tier_level) then

                                    -- Check expiration.
--                                    if (accumulated_points <= 0 and v_tier_user.points > 0) then
                                      if (accumulated_points <= 0) then
--                                          DBMS_OUTPUT.PUT_LINE('Check expiration.');
                                           -- Check whether the user was warned in the last warning period.
                                           BEGIN

                                                     SELECT   *
                                                     INTO       v_user_history
                                                     FROM     tier_users_history tuh
                                                     WHERE   tuh.user_id = v_user.id
                                                           AND   tuh.tier_his_action = TIER_ACTION_EXPIRATION_WARNING
                                                           AND   tuh.time_created > sysdate - v_tier.expiration_days + WARNING_PERIOD
                                                           AND   rownum = 1;

                                            EXCEPTION
                                                    WHEN NO_DATA_FOUND THEN
                                                            v_user_history.id := null;
                                            END;

--                                            DBMS_OUTPUT.PUT_LINE('v_user_history.id: ' || v_user_history.id);
                                            if (v_user_history.id is null) then

                                                   -- Count accumulated points in the expiration period - warining period to check whether user should be warned.
                                                   exp_warning_check_date  :=  sysdate - v_tier.expiration_days + WARNING_PERIOD;
                                                   exp_accumulated_points := get_loyalty_accumulated_points(v_user.id,exp_warning_check_date);

                                                    -- If user got no point send warning.
                                                    if (exp_accumulated_points <= 0) then
                                                         -- Mark as needs to be warned
--                                                        DBMS_OUTPUT.PUT_LINE('warning');
                                                        is_expiration_warning := 1;
                                                        points_for_job := v_tier_user.points;
                                                    end if;

                                           else -- v_user_history.id is NOT null

                                                    -- Check if user passed his warning period.
                                                    exp_warning_date := v_user_history.time_created;

--                                                    DBMS_OUTPUT.PUT_LINE('exp_warning_date: ' || exp_warning_date);

                                                    if (sysdate > exp_warning_date  + WARNING_PERIOD) THEN

                                                          -- Count points since warning
                                                          exp_accumulated_points := get_loyalty_accumulated_points(v_user.id,exp_warning_date);

--                                                          DBMS_OUTPUT.PUT_LINE('exp_accumulated_points: ' || exp_accumulated_points);

                                                          if (exp_accumulated_points <= 0) then

                                                                    -- If user dont have any points since warning, mark as expired.
--                                                                    DBMS_OUTPUT.PUT_LINE('expired');
                                                                    is_expired := 1;
                                                                    points_for_job := 0-v_tier_user.points;
                                                            end if;
                                                    end if; -- if (exp_accumulated_points = 0 and v_tier_user.points > 0) then

                                           end if; -- if (v_user_history.id is null) then

                                    end if; --  if (accumulated_points <= 0 and v_tier_user.points > 0) then


                                     if (is_expired = 1) then
                                          loyalty_action := JOB_ACTION_EXPIRED;
                                    elsif (is_expiration_warning = 1) then
                                          loyalty_action := JOB_ACTION_WARNING;
                                    elsif (is_upgrade = 1) then
                                          loyalty_action := JOB_ACTION_UPGRADE;
                                    elsif (is_downgrade = 1) then
                                           loyalty_action := JOB_ACTION_DOWNGRADE;
                                    elsif (is_requalified = 1) then
                                          loyalty_action := JOB_ACTION_REQUALIFICATION;
                                    end if; --  if (is_expired = 1) then

                                    if (loyalty_action > 0) then
                                          v_data := LOYALTY_TABLE_DATA(v_user.id, v_user.user_name, previous_tier_id, new_tier_id,points_for_job,v_user.utc_offset, loyalty_action);
                                          PIPE ROW(v_data);
                                    end if;


                            end if; -- if  v_user.time_created < sysdate - v_tier.MIN_QUALIFICATION_PERIOD) then
                  end if; -- if (previous_tier_id > 0) then
           end if; -- if (last_tier_history_action != TIER_ACTION_POINTS_EXPIRED) then

    END LOOP;


DBMS_OUTPUT.PUT_LINE('GET_LOYALTY_UPDATED_USERS ended at ' || sysdate);

--EXCEPTION
--      WHEN OTHERS THEN
--            DBMS_OUTPUT.PUT_LINE('SQLCODE: ' || SQLCODE);
--            DBMS_OUTPUT.PUT_LINE('SQLERRM: ' || SQLERRM);
  RETURN;
END GET_LOYALTY_USERS;

----------------------------------------------------------------------------------------------

create or replace
PROCEDURE           "TRANSACTIONS_ISSUES_FILL" (p_exec_type in NUMBER)AS
-- p_exec_type Values
BOTH_TABLES_RUN number := 0;
-- These are also these parameters ids on DB_PARAMETERS:
TRANSACTION_ISSUES_RUN number := 1;
TRANSACTION_ISSUES_S_RUN number := 2;

start_run_time date;
last_trans_issues_run_time date := null;
last_trans_issues_s_run_time date := null;
max_deposit_during_call_days number;
deposit_during_call_days number;
other_reactions_days number;
action_id number;
check_transaction_1 number;
check_transaction_2 number;
count_1 number := 0;
count_2 number := 0;
last_users_transaction_time date;
start_check_time date;
callers_count number;

BEGIN

DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_FILL started at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');

-- Get the max deposit during call days in order not to miss last deposit during call transactions
select max(deposit_during_call_days)
into max_deposit_during_call_days
from skins;

-- If need to run transaction issues
IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN

        -- Get last transaction time from transactions_issues table so that we could go check only transations that we didn't check yet.
        select date_value
        into last_trans_issues_run_time
        from db_parameters
        where id = TRANSACTION_ISSUES_RUN; -- last_transactions_issues_run

        DBMS_OUTPUT.PUT_LINE(' transactions_issues last run on: ' || last_trans_issues_run_time);
ELSE
      last_trans_issues_run_time := sysdate;
END IF;

-- If need to run transaction issues_s
IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN

        -- Get last transaction time from transactions_issues_s table so that we could go check only transations that we didn't check yet.
        select date_value
        into last_trans_issues_s_run_time
        from db_parameters
        where id = TRANSACTION_ISSUES_S_RUN; -- last_transactions_issues_s_run

        DBMS_OUTPUT.PUT_LINE(' transactions_issues_s last run on: ' || last_trans_issues_s_run_time);
ELSE
      last_trans_issues_s_run_time := sysdate;
END IF;


IF (last_trans_issues_run_time is null or last_trans_issues_s_run_time is null) then
    start_run_time := to_date('01/01/1970-00:00','DD/MM/YYYY-HH24:MI');
ELSIF (last_trans_issues_run_time < last_trans_issues_s_run_time) then
    start_run_time := last_trans_issues_run_time;
ELSE
    start_run_time := last_trans_issues_s_run_time;
END IF;

DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('start_transaction_time from : ' || start_run_time);

-- Find all transations since last run or relevant table insert until now minus the deposit_during_call_time check by given skin id
-- and that are'nt in transactions_issues table
FOR transaction1 IN (select
                                              t.*
                                  from
                                              transactions t ,
                                              transaction_types tt
                                  where
                                              -- need to check transactions in the deposit_during_call_days time before start_run_time
                                              -- and until deposit_during_call_days time before now.
                                              (t.time_created + max_deposit_during_call_days) between  start_run_time  and sysdate
                                              and t.type_id = tt.id
                                              and tt.class_type = 1 -- All deposite
                                              and t.status_id in (2,7,8) -- Succeed Deposits\ Pending and canceled
--                                              and not exists (select *
--                                                                       from transactions_issues
--                                                                       where transaction_id = t.id)
                                 order by
                                              time_created) LOOP

    -- Initialize flags
    check_transaction_1 := 0;
    check_transaction_2 := 0;

    -- Get days range fields by transactions' user skin id
    select s.deposit_during_call_days, s.other_reactions_days
    into deposit_during_call_days, other_reactions_days
    from skins s, users u
    where s.id = u.skin_id
        and transaction1.user_id = u.id;


--      DBMS_OUTPUT.PUT_LINE('%%%%%%%%%%: ');
--      DBMS_OUTPUT.PUT_LINE('transatction _id: ' || transaction1.id);
--      DBMS_OUTPUT.PUT_LINE('user _id: ' || transaction1.user_id);
--      DBMS_OUTPUT.PUT_LINE('deposit_during_call_days: ' || deposit_during_call_days);
--      DBMS_OUTPUT.PUT_LINE('other_reactions_days: ' || other_reactions_days);

    -- If need to run transaction issues
    IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN

        -- Check whether this transaction exists in the TRANSACTION_ISSUES table
          select count(*)
          into check_transaction_1
          from transactions_issues ti
          where ti.transaction_id = transaction1.id;

        -- Handle Transaction_Issues Table. Only for retention!
         IF  (check_transaction_1 = 0) THEN
                  action_id := 0;

                   BEGIN
                       -- Find closest Deposit During Call to the curennt transaction.
                      select min(ia.id)
                      into action_id
                      from issues i, issue_actions ia, writers w, issue_action_types iat
                      where  i.id = ia.issue_id
                        and  i.user_id = transaction1.user_id
                        and i.population_entry_id is not null -- Only for retention issues
                        and iat.id = ia.issue_action_type_id
                        and iat.deposit_search_type = 1 -- Deposit during call
                        and ia.action_time between (transaction1.time_created) and (transaction1.time_created + deposit_during_call_days)
                        and ia.writer_id = w.id
                        and w.dept_id = 3 -- RETENTION
                        and not exists (select *  -- Check that this issue doesn't exist it transactions_issues table
                                                  from transactions_issues ti, issue_actions ia2
                                                  where ti.issue_action_id = ia2.id
                                                      and ia2.issue_id = i.id) ;

                       IF (action_id is NULL) THEN
                          action_id := 0;
                      END IF;

                  END;

                  IF (action_id = 0) THEN

                          -- All other reactions (besides deposit during call) should be checked from the latest date between:
                          -- 1. the curren transaction creation time - other_reactions_days
                          -- 2. the last transaction of the current user in TRANSACTION_ISSUES table.
                          start_check_time := transaction1.time_created - other_reactions_days;

                          -- Get the time of the last transaction in TRANSACTION_ISSUES table of the current user.
                          select max(t.time_created)
                          into     last_users_transaction_time
                          from    transactions_issues ti, transactions t
                          where ti.transaction_id = t.id
                              and t.user_id = transaction1.user_id
                              and t.time_created < transaction1.time_created;

                           if (last_users_transaction_time is not null  and  last_users_transaction_time > start_check_time) then
                                start_check_time := last_users_transaction_time;
                           end if;

                          -- Find closest Cooperating  to the curennt transaction.
                          select max(ia.id)
                          into action_id
                          from issues i, issue_actions ia, writers w, issue_action_types iat
                          where  i.id = ia.issue_id
                            and  i.user_id = transaction1.user_id
                            and i.population_entry_id is not null -- Only for retention issues
                            and iat.id = ia.issue_action_type_id
                            and iat.deposit_search_type = 2 -- Will deposit later, Not interested now and Not Sure
                            and ia.action_time between (start_check_time) and (transaction1.time_created)
                            and ia.writer_id = w.id
                            and w.dept_id = 3 -- RETENTION
                            and not exists (select *  -- check that this issue doesn't exist it transactions_issues table
                                                      from transactions_issues ti, issue_actions ia2
                                                      where ti.issue_action_id = ia2.id
                                                          and ia2.issue_id = i.id) ;


                          IF (action_id is NULL) THEN
                              action_id := 0;
                          END IF;

                  END IF;  --IF (action_id = 0) THEN

                  IF (action_id != 0) THEN

      --                  DBMS_OUTPUT.PUT_LINE('insert.... trans_id: ' || transaction1.id);
      --                  DBMS_OUTPUT.PUT_LINE('insert.... action_id: ' || action_id);

                        select count(distinct(ia.writer_id))
                        into    callers_count
                        from   issue_actions ia
                        where ia.issue_id = (select ia2.issue_id from issue_actions ia2 where ia2.id = action_id);

                        insert into transactions_issues(transaction_id, issue_action_id,callers_num, time_inserted, insert_type)
                        values(transaction1.id, action_id,callers_count, sysdate,0);
                        count_1 := count_1 + 1;

                  END IF; --IF (action_id != 0) THEN
           END IF; -- IF  (check_transaction_1 = 0) THEN
     END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN

      -- If need to run transaction issues_s
     IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN

           -- Check whether this transaction exists in the TRANSACTION_ISSUES_S table
          select count(*)
          into check_transaction_2
          from transactions_issues_s ti
          where ti.transaction_id = transaction1.id;


            -- Handle Transaction_Issues_S  Table. Only for Decline after approve population.
           IF  (check_transaction_2 = 0) THEN
                   action_id := 0;

                   BEGIN
                       -- Find closest Deposit During Call to the curennt transaction.
                      select min(ia.id)
                      into action_id
                      from issues i, issue_actions ia, population_entries pe, populations p, issue_action_types iat
                      where  i.id = ia.issue_id
                        and  i.user_id = transaction1.user_id
                        and i.population_entry_id = pe.id
                        and pe.population_id = p.id
                        and p.population_type_id = 2 -- Decline after approve
                        and iat.id = ia.issue_action_type_id
                        and iat.deposit_search_type = 1 -- Deposit during call
                        and ia.action_time between (transaction1.time_created) and (transaction1.time_created + deposit_during_call_days)
                        and not exists (select *  -- Check that this issue doesn't exist it transactions_issues table
                                                  from transactions_issues_s ti, issue_actions ia2
                                                  where ti.issue_action_id = ia2.id
                                                      and ia2.issue_id = i.id) ;

                       IF (action_id is NULL) THEN
                          action_id := 0;
                      END IF;

                  END;

                  IF (action_id = 0) THEN

                          -- All other reactions (besides deposit during call) should be checked from the latest date between:
                          -- 1. the curren transaction creation time - other_reactions_days
                          -- 2. the last transaction of the current user in TRANSACTION_ISSUES_S table.
                          start_check_time := transaction1.time_created - other_reactions_days;

                          -- Get the time of the last transaction in TRANSACTION_ISSUES_S table of the current user.
                          select max(t.time_created)
                          into     last_users_transaction_time
                          from    transactions_issues_s ti, transactions t
                          where ti.transaction_id = t.id
                              and t.user_id = transaction1.user_id
                               and t.time_created < transaction1.time_created;

                           if (last_users_transaction_time is not null  and  last_users_transaction_time > start_check_time) then
                                start_check_time := last_users_transaction_time;
                           end if;

                          -- Find closest Cooperating  to the curennt transaction.
                          select max(ia.id)
                          into action_id
                          from issues i, issue_actions ia, population_entries pe, populations p, issue_action_types iat
                          where  i.id = ia.issue_id
                            and  i.user_id = transaction1.user_id
                            and i.population_entry_id = pe.id
                            and pe.population_id = p.id
                            and p.population_type_id = 2 -- Decline after approve
                            and iat.id = ia.issue_action_type_id
                            and iat.is_transaction_issues_s_action = 1 -- Cooperating, Will deposit later, Not interested now and Not Sure
                            and ia.action_time between start_check_time and (transaction1.time_created)
                            and not exists (select *  -- check that this issue doesn't exist it transactions_issues table
                                                      from transactions_issues_s ti, issue_actions ia2
                                                      where ti.issue_action_id = ia2.id
                                                          and ia2.issue_id = i.id) ;


                          IF (action_id is NULL) THEN
                              action_id := 0;
                          END IF;

                  END IF;  --IF (action_id = 0) THEN

                  IF (action_id != 0) THEN

      --                  DBMS_OUTPUT.PUT_LINE('insert.... trans_id: ' || transaction1.id);
      --                  DBMS_OUTPUT.PUT_LINE('insert.... action_id: ' || action_id);

                        select count(distinct(ia.writer_id))
                        into    callers_count
                        from   issue_actions ia
                        where ia.issue_id = (select ia2.issue_id from issue_actions ia2 where ia2.id = action_id);

                        insert into transactions_issues_s(transaction_id, issue_action_id,callers_num)
                        values(transaction1.id, action_id,callers_count);
                        count_2 := count_2 + 1;

                  END IF; --IF (action_id != 0) THEN
           END IF; -- IF  (check_transaction_2 = 0) THEN
      END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN

END LOOP;

DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_FILL ended at: ' || sysdate);

-- Update last_transactions_issues_run
IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN

      update db_parameters
      set date_value = sysdate
      where id = TRANSACTION_ISSUES_RUN; -- last_transactions_issues_run

      DBMS_OUTPUT.PUT_LINE(count_1 || ' New Records in transactions_issues');
END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_RUN) THEN

-- Update last_transactions_issues_s_run
IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN

      update db_parameters
      set date_value = sysdate
      where id = TRANSACTION_ISSUES_S_RUN; -- last_transactions_issues_s_run

      DBMS_OUTPUT.PUT_LINE(count_2 || ' New Records in transactions_issues_s');
END IF; -- IF (p_exec_type = BOTH_TABLES_RUN OR p_exec_type = TRANSACTION_ISSUES_S_RUN) THEN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');

  NULL;
END TRANSACTIONS_ISSUES_FILL;

---------------------------------------------------------------------------------------------------

create or replace
FUNCTION GET_MARKETING_REPORT_TABLE( p_skin_id IN NUMBER
                                                                                    , p_business_skin_id IN NUMBER
                                                                                    , p_start_date IN DATE
                                                                                    , p_end_date IN DATE
                                                                                     ,p_source IN NUMBER
                                                                                     ,p_landing_page_id IN NUMBER)

RETURN  MARKETING_REPORT_DATA_LIST PIPELINED AS
v_data  MARKETING_REPORT_DATA;

sum_deposits NUMBER;
turn_over NUMBER;
house_win NUMBER;

BEGIN

-- all changes are made for getting AO data when p_business_skin_id != 0

  DBMS_OUTPUT.PUT_LINE('GET_MARKETING_REPORT_TABLE started at ' || sysdate);

  FOR v_record IN (	SELECT distinct mcom.id  as combId,
                                                mca.name  campaignName,
                                                mmed.name medium ,
                                                mcon.name content,
                                                ms.size_horizontal marketSizeHorizontal,
                                                ms.size_vertical marketSizeVertical,
                                                mt.name marketType,
                                                mloc.location location,
                                                mcom.skin_id skinId,
                                                comb_users.registered_users registeredUsersNum,
                                                clk.clicks_num clicksNum,
                                                comb_users.reg_frst_dep_users firstRegDepositersNum,
                                                comb_users.frst_dep_users firstDepositersNum,
                                                sums_transactions.sum_deposits_et sumDepositsEt,
                                                sums_transactions.sum_deposits_ao sumDepositsAo,
                                                sums_investments.turnover_et  turnOverEt,
                                                sums_investments.turnover_ao  turnOverAo,
                                                sums_investments.house_win_et  houseWinEt,
                                                sums_investments.house_win_ao  houseWinAo,
                                                fail_min.fail_min_users failMinUsers,
                                                fail.fail_users  failUsers,
                                                mlp.name landingPageName,
                                                msource.name as source_name

                                FROM  marketing_campaigns mca,
                                              marketing_mediums mmed,
                                              marketing_contents mcon,
                                              marketing_sources msource,
                                              marketing_combinations mcom

                                              LEFT JOIN marketing_landing_pages mlp on mcom.landing_page_id = mlp.id
                                              LEFT JOIN marketing_sizes ms on mcom.size_id = ms.id
                                              LEFT JOIN marketing_locations mloc on mcom.location_id = mloc.id
                                              LEFT JOIN marketing_types mt on mcom.type_id = mt.id
                                              LEFT JOIN
                                              (SELECT
                                                        u.combination_id,
                                                        SUM(CASE WHEN u.time_created between p_start_date and p_end_date THEN 1 ELSE 0 END) as registered_users,
                                                        SUM(CASE WHEN t.time_created between p_start_date and p_end_date THEN 1 ELSE 0 END) as frst_dep_users,
                                                        SUM(CASE WHEN u.time_created between p_start_date and p_end_date
                                                                                         AND
                                                                                          t.time_created between p_start_date and p_end_date THEN 1 ELSE 0 END) as reg_frst_dep_users
                                                FROM
                                                      users u
                                                            LEFT JOIN  users_first_deposit ufd on u.id = ufd.user_id
                                                                  LEFT JOIN transactions t on  t.id = ufd.first_deposit_id
                                                WHERE
                                                      u.class_id <> 0
                                                      AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
--                                                      AND (p_business_skin_id = 0 or mcom.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                                      AND (p_business_skin_id = 0 or u.skin_id not in (1))
                                                GROUP  BY
                                                      u.combination_id) comb_users on mcom.id = comb_users.combination_id

                                               LEFT JOIN
                                              (SELECT cl.marketing_combination_id, SUM(cl.clicks_num) as clicks_num
                                                FROM clicks_comb_day cl
                                                WHERE cl.day between p_start_date and p_end_date - 1
                                                GROUP  BY cl.marketing_combination_id) clk on mcom.id = clk.marketing_combination_id

                                                 LEFT JOIN
                                                (SELECT u.combination_id, SUM(t.amount/100) as sum_deposits_et, SUM(t.amount*t.rate/100) as sum_deposits_ao
                                                  FROM users u, transactions t, transaction_types tt
                                                  WHERE u.id = t.user_id
                                                  AND t.type_id = tt.id
                                                  AND u.class_id <> 0
                                                  AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
--                                                  AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                                  AND (p_business_skin_id = 0 or u.skin_id not in (1))
                                                  AND tt.class_type = 1
                                                  AND t.status_id IN (2, 7, 8)
                                                  AND u.time_created between p_start_date and p_end_date
                                                  -------------------------------------------------------------------------
                                                  AND t.time_created >= p_start_date
                                                  GROUP BY u.combination_id) sums_transactions on mcom.id = sums_transactions.combination_id

                                                  LEFT JOIN
                                                 (SELECT u.combination_id, SUM(i.amount/100) as turnover_et, SUM(i.house_result/100) as house_win_et,
                                                                                            SUM(i.amount*i.rate/100) as turnover_ao, SUM(i.house_result*i.rate/100) as house_win_ao
                                                  FROM users u,  investments i
                                                  WHERE  u.id = i.user_id
                                                  AND u.class_id <> 0
                                                  AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
--                                                  AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                                  AND (p_business_skin_id = 0 or u.skin_id not in (1))
                                                  AND i.is_settled = 1
                                                  AND i.is_canceled = 0
                                                  AND u.time_created  between p_start_date and p_end_date
                                                  -------------------------------------------------------------------------
                                                  AND i.time_created >= p_start_date
                                                  GROUP BY u.combination_id) sums_investments on mcom.id = sums_investments.combination_id

                                                  LEFT JOIN
                                                 (SELECT  u.combination_id, count(DISTINCT(u.id))  as fail_min_users
                                                  FROM users u, transactions t, transaction_types tt
                                                  WHERE  u.class_id <> 0
                                                  AND u.id = t.user_id
                                                  AND t.type_id = tt.id
                                                  AND tt.class_type = 1
                                                  AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
--                                                  AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                                  AND (p_business_skin_id = 0 or u.skin_id not in (1))
                                                  AND u.time_created between p_start_date and p_end_date
                                                  AND not exists (SELECT t2.id
                                                                              FROM transactions t2, transaction_types tt2
                                                                              WHERE u.id = t2.user_id
                                                                              AND t2.type_id = tt2.id
                                                                              AND tt2.class_type = 1
                                                                              AND (t2.status_id IN (2, 7, 8)
                                                                                        OR (t2.status_id = 3  AND  t2.description NOT LIKE '%min%'
                                                                                                                                   OR
                                                                                                                                   t2.description is NULL)))
                                                  group by u.combination_id) fail_min on mcom.id = fail_min.combination_id

                                                  LEFT JOIN
                                                  (SELECT  u.combination_id, count(DISTINCT(t.user_id)) as fail_users
                                                    FROM users u, transactions t, transaction_types tt
                                                    WHERE u.id = t.user_id
                                                    AND t.type_id = tt.id
                                                    AND tt.class_type = 1
                                                    AND t.status_id = 3
                                                    AND (t.description NOT LIKE '%min%' OR t.description is NULL)
                                                    AND u.class_id <> 0
                                                    AND (p_skin_id  = 0 or u.skin_id = p_skin_id)
--                                                    AND (p_business_skin_id = 0 or u.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                                    AND (p_business_skin_id = 0 or u.skin_id not in (1))
                                                    AND u.time_created between p_start_date and p_end_date
                                                    AND not exists (SELECT t.id
                                                                                FROM transactions t2, transaction_types tt2
                                                                                WHERE u.id = t2.user_id
                                                                                AND t2.type_id = tt2.id
                                                                                AND tt2.class_type = 1
                                                                                AND t2.status_id IN (2, 7, 8))
                                                    group by u.combination_id) fail on mcom.id = fail.combination_id
                                WHERE mcom.campaign_id = mca.id

                                      AND mcom.medium_id = mmed.id
                                      AND mcom.content_id = mcon.id
                                      AND (p_skin_id  = 0 or mcom.skin_id = p_skin_id)
--                                      AND (p_business_skin_id = 0 or mcom.skin_id in (select s.id from skins s where p_business_skin_id = s.business_case_id))
                                      AND (p_business_skin_id = 0 or mcom.skin_id not in (1))
                                      AND (p_source=0 or mca.source_id = p_source)
                                      AND (p_landing_page_id = 0 or mcom.landing_page_id = p_landing_page_id)
                                      AND msource.id = mca.source_id
                                ORDER BY mcom.id) LOOP

  -- If etrader get unrated sums
--  if (p_skin_id = 1 or p_business_skin_id = 1) then
if (p_skin_id = 1) then
      sum_deposits := v_record.sumDepositsEt;
      turn_over := v_record.turnOverEt;
      house_win :=  v_record.houseWinEt;
  else
      sum_deposits := v_record.sumDepositsAo;
      turn_over := v_record.turnOverAo;
      house_win :=  v_record.houseWinAo;
  end if;


   v_data := MARKETING_REPORT_DATA(v_record.combId,
                                                                      v_record.campaignName,
                                                                      v_record.medium,
                                                                      v_record.content,
                                                                      v_record.marketSizeHorizontal,
                                                                      v_record.marketSizeVertical,
                                                                      v_record.marketType,
                                                                      v_record.location,
                                                                      v_record.skinId,
                                                                      v_record.registeredUsersNum,
                                                                      v_record.clicksNum,
                                                                      v_record.firstRegDepositersNum,
                                                                      v_record.firstDepositersNum,
                                                                      sum_deposits,
                                                                      turn_over,
                                                                      house_win,
                                                                      v_record.failMinUsers,
                                                                      v_record.failUsers,
                                                                      v_record.landingPageName,
                                                                      v_record.source_name
                                                                      );
   PIPE ROW(v_data);

  END LOOP;

  DBMS_OUTPUT.PUT_LINE('GET_MARKETING_REPORT_TABLE ended at ' || sysdate);

  RETURN;
END GET_MARKETING_REPORT_TABLE;

-------------------------------------------------------------------------------------------------

create or replace PROCEDURE POPULATION_NO_INVESTMENTS AS

  THIS_POPULATION_TYPE NUMBER := 10;

	v_counter NUMBER;
	v_pe population_entries%ROWTYPE;
	v_populationEntryId NUMBER;
	v_peh population_entries_hist%ROWTYPE;
  v_peh_old population_entries_hist%ROWTYPE;
  populationEntHist population_entries_hist%ROWTYPE;
  populationEntHistPriority NUMBER;
  populationPriority NUMBER;
  populationEntHistStatus NUMBER;

BEGIN
	FOR v_usr IN (
		SELECT
			A.id,
			A.skin_id,
			B.id AS populationId,
			B.threshold,
      B.number_of_days
		FROM
			users A,
			populations B
		WHERE
			A.skin_id = B.skin_id AND
		  B.population_type_id = THIS_POPULATION_TYPE AND
      B.is_active = 1 AND
      A.class_id !=0 AND
      A.is_active = 1 AND
      A.is_false_account = 0 AND
      A.is_contact_by_phone = 1 AND
      A.id NOT in (select USER_ID from frauds) AND
      NOT EXISTS(
            SELECT 1 FROM POPULATION_ENTRIES  WHERE is_population_entry_locked(id, 0) > 0 and A.id = user_id
      ) AND
      IS_USER_IN_POP_TYPE(A.id, THIS_POPULATION_TYPE )= 0 AND

      -- Population Conditions:
      -----------------------------

      -- User has no investments.
      NOT EXISTS(
                              SELECT
                                1
                              FROM
                                investments i
                              WHERE
                               A.id = i.user_id) AND

       -- X days passed from last approved deposit
         sysdate - B.number_of_days >  GET_LAST_SUCCESS_DEPOSIT_TIME(A.id)

	) LOOP


                         populationEntHist := get_user_last_population_type1(v_usr.id, THIS_POPULATION_TYPE);

                         IF (populationEntHist.status_id IN (100,101,107))  THEN
                              IF (GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.id) <= populationEntHist.TIME_CREATED) THEN
                                     GOTO end_loop;
                             END IF;
                        END IF;

                        populationEntHist := get_user_last_population1(v_usr.id);

                        IF (populationEntHist.status_id < 100) THEN
                                SELECT priority into populationPriority FROM population_types  WHERE id = THIS_POPULATION_TYPE;
                                populationEntHistPriority := GET_PRIORITY_BY_POP_ENT_ID(populationEntHist.POPULATION_ENTRY_ID);
                                IF (populationPriority < populationEntHistPriority) THEN
                                        REMOVE_USER_FROM_POP(populationEntHist.POPULATION_ENTRY_ID);
                                END IF;
                                IF (populationPriority > populationEntHistPriority) THEN
                                        GOTO end_loop;
                                END IF;
                        END IF;


                        SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO v_pe.id FROM dual;
                        v_pe.population_id := v_usr.populationId;
                        v_pe.user_id := v_usr.id;
                        v_pe.group_id := CASE WHEN v_usr.threshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, v_usr.threshold) = 0 THEN 0 ELSE 1 END END;
                        v_pe.writer_id := 0;
                        v_pe.time_created := sysdate;
                        v_pe.ASSIGN_WRITER_ID := NULL;
                        v_pe.is_locked := 0;
                        INSERT INTO population_entries VALUES v_pe;

        --		DBMS_OUTPUT.PUT_LINE('PE - 1, ' || v_pe.population_id || ', ' || v_pe.user_id || ', ' || v_pe.is_active || ', ' || v_pe.group_id || ', ' || v_pe.writer_id || ', ' || v_pe.time_created);

 --                       SELECT
 --                               id INTO v_populationEntryId
 --                       FROM
--                                population_entries
 --                       WHERE
--                                population_id = v_usr.populationId AND
--                                user_id = v_usr.id;

                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;
                        v_peh.population_entry_id := v_pe.id;
                        v_peh.status_id := 1;
                        v_peh.comments := 'No investments';
                        v_peh.writer_id := 0;
                        v_peh.time_created := GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.id) + v_usr.number_of_days ;
                        INSERT INTO population_entries_hist VALUES v_peh;

                        <<end_loop>>
                        NULL;
	END LOOP;
END;


create or replace FUNCTION GET_LAST_SUCCESS_DEPOSIT_TIME ( p_user_id IN NUMBER ) RETURN DATE AS

last_success_deposit DATE;

BEGIN

    select  max(t.time_created)
    into last_success_deposit
    from transactions t,
              TRANSACTION_TYPES tt
    where
              tt.CLASS_TYPE = 1 and -- deposit
              user_id = p_user_id and
              type_id = tt.id and
              t.status_id in (2,7);    -- success

     return  last_success_deposit;

EXCEPTION
      when no_data_found then
               return current_date;
End;

create or replace FUNCTION GET_USER_LAST_POPULATION1
( p_user_id IN NUMBER
) RETURN population_entries_hist%ROWTYPE IS

populationEntHist  population_entries_hist%ROWTYPE;

BEGIN

          SELECT
                   PEHO.*
          INTO
                   populationEntHist
          FROM
                   (SELECT
                      Z.*
                    FROM
                        population_entries_hist Z
                     WHERE
                        Z.population_entry_id = (select
                                                                                    A.id
                                                                            from (
                                                                                        select
                                                                                                Y.*
                                                                                        from
                                                                                                population_entries Y
                                                                                        where
                                                                                                 Y.user_id = p_user_id
                                                                                        order by
                                                                                                Y.TIME_CREATED desc
                                                                                      ) A
                                                                            WHERE
                                                                                    rownum = 1)

                      ORDER BY
                        Z.TIME_CREATED desc) PEHO
          WHERE
                  rownum = 1 AND
                  PEHO.STATUS_ID not in(4,3);


  RETURN populationEntHist;

EXCEPTION
   WHEN no_data_found THEN
        RETURN NULL;

END GET_USER_LAST_POPULATION1;

create or replace FUNCTION           "GET_NEXT_OPEN_TIME" ( par_marketId IN NUMBER) RETURN VARCHAR2 AS
par_time_first_invest VARCHAR2(5 char);
par_exchange_id NUMBER;
firstDay DATE;
par_time_zone VARCHAR2(30 char);
halfDayHoliday exchange_holidays.is_half_day%TYPE;
toInsTimeFirstInvest VARCHAR2(200 char);
turkishEndSistaTime VARCHAR2(30 char);
BEGIN
        firstDay :=  current_date + 1;
        turkishEndSistaTime := '14:30';

        SELECT
                A.time_first_invest,
                B.exchange_id,
                A.time_zone
        INTO
                par_time_first_invest,
                par_exchange_id,
                par_time_zone
        FROM
                opportunity_templates A,
                markets B
        WHERE
                B.id = par_marketId AND
                A.market_id = B.id AND
                A.is_active = 1 AND
                A.is_full_day = 1 AND
                A.scheduled = 2;

                -- if its turkish market and its not holiday day
                IF (par_exchange_id = 24 AND -- its turkish
                      NOT to_char(sysdate, 'D') = 7 AND -- not saturday
                      NOT to_char(sysdate, 'D') = 1 AND  -- not sunday
                      to_timestamp_tz(to_char(sysdate, 'yyyy-mm-dd') || turkishEndSistaTime || par_time_zone, 'yyyy-mm-dd hh24:mi TZR') > sysdate)  -- current time is b4 end of sista
                      THEN

                                BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = par_exchange_id AND
						trunc(holiday) = sysdate;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;
                                IF halfDayHoliday IS NULL THEN -- if its full day
                                        toInsTimeFirstInvest := to_char(to_timestamp_tz(to_char(sysdate, 'yyyy-mm-dd') || turkishEndSistaTime || par_time_zone, 'yyyy-mm-dd hh24:mi TZR'), 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM');
                                        return toInsTimeFirstInvest;
                                END IF;
                END IF;

                -- we asume that there is at least 1 working day in the month

		-- find the first working day of the month and set the time first invest to be on it
		FOR i IN 0..30
		LOOP
			IF ((par_exchange_id = 1 OR  par_exchange_id = 17 OR  par_exchange_id = 18) AND NOT to_char(firstDay + i, 'D') = 6 AND NOT to_char(firstDay + i, 'D') = 7) OR
				(NOT (par_exchange_id = 1 OR  par_exchange_id = 17 OR par_exchange_id = 16 OR par_exchange_id = 18) AND NOT to_char(firstDay + i, 'D') = 7 AND NOT to_char(firstDay + i, 'D') = 1) OR
				((par_exchange_id = 16) AND NOT to_char(firstDay + i, 'D') = 5 AND NOT to_char(firstDay + i, 'D') = 6 AND NOT to_char(firstDay + i, 'D') = 7) THEN
				BEGIN
					SELECT
						is_half_day INTO halfDayHoliday
					FROM
						exchange_holidays
					WHERE
						exchange_id = par_exchange_id AND
						trunc(holiday) = firstDay + i;
					EXCEPTION WHEN NO_DATA_FOUND THEN
						halfDayHoliday := NULL;
				END;

				IF halfDayHoliday IS NULL OR halfDayHoliday = 1 THEN
					-- working day... at least half
                                        toInsTimeFirstInvest := to_char(to_timestamp_tz(to_char(firstDay + i, 'yyyy-mm-dd') || par_time_first_invest || par_time_zone, 'yyyy-mm-dd hh24:mi TZR'), 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM');
					EXIT;
				END IF;
			END IF;
		END LOOP;

  RETURN toInsTimeFirstInvest;
END GET_NEXT_OPEN_TIME;

----------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE "POPULATIONS_HANDLER" (runType NUMBER) AS

  -- Run Types
  RUN_TYPE_ALL_USERS NUMBER := 1;
  RUN_TYPE_NEW_USERS NUMBER := 2;
  RUN_TYPE_ONLY_CONTACTS NUMBER := 3;

	v_pe population_entries%ROWTYPE;
  v_pu population_users%ROWTYPE;
	v_peh population_entries_hist%ROWTYPE;
  populationEntHist population_entries_hist%ROWTYPE;

  populationEntryId NUMBER;
  qualifiedPopualtionId NUMBER;
  currentPopulationId NUMBER;
  currentPopulationPriority NUMBER;

  popQualificationTime DATE;
  popBaseQualificationTime DATE;
  popBaseQualificationTimeOld DATE;

  newPopualtionThreshold NUMBER;
  qualificationStatus NUMBER;
  assignedWriterId NUMBER;

  users_popualtion_users_id NUMBER;
  lastSalesDepositTime DATE;

  v_counter NUMBER;
  isExist NUMBER;
  isExistSuccessfullDeposit NUMBER;
  isFirstMinimumBalanceCheck NUMBER;
  userNumOfSuccesDeposits NUMBER;
  isCheckQualificationTime NUMBER;

  count_new_qualified_users number := 0;
  count_new_qualified_contacts number := 0;
  runTypeName varchar2(30);
BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER started at: ' || sysdate);

  CASE
    WHEN runType = RUN_TYPE_ALL_USERS THEN
      runTypeName := 'Run Type All Users';
    WHEN runType = RUN_TYPE_NEW_USERS THEN
      runTypeName := 'Run Type New Users';
    WHEN runType = RUN_TYPE_ONLY_CONTACTS THEN
      runTypeName := 'Run Type Only Contacts';
    ELSE
      DBMS_OUTPUT.PUT_LINE('run type: ' || runType || ' doesnt exist');
  END CASE;

  DBMS_OUTPUT.PUT_LINE('run type: ' || runTypeName);

  IF (runType != RUN_TYPE_ONLY_CONTACTS) THEN
    -- RUN MARKETING_HANDLER
    marketing_handler();

    FOR v_usr IN (
      SELECT
        u.id user_id,
        u.skin_id,
        u.balance,
        u.time_created,
        u.currency_id,
        u.is_declined,
        u.is_contact_for_daa,
        u.last_decline_date,
        u.time_last_login,
        u.contact_id,
        u.time_sc_house_result ,
        p.id population_id,
        p.population_type_id curr_pop_type_id,
        pu.id population_user_id,
        pu.curr_population_entry_id,
        pu.curr_assigned_writer_id,
        pu.entry_type_id,
        pu.entry_type_action_id,
        pt.priority,
        ilgc.value min_investment_limit,
        s.other_reactions_days
      FROM
        investment_limits il,
        investment_limit_group_curr ilgc,
        skins s,
        users u LEFT JOIN population_users pu on u.id = pu.user_id
                        LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                            LEFT JOIN populations p on pe.population_id = p.id
                                  LEFT JOIN population_types pt on p.population_type_id = pt.id
      WHERE
        u.currency_id = ilgc.currency_id
        AND il.min_limit_group_id = ilgc.investment_limit_group_id
        AND il.id = 1 -- Default binary limits id
        AND u.skin_id = s.id
        AND u.class_id !=0
        AND u.is_active = 1
        AND u.is_false_account = 0
        AND u.is_contact_by_phone = 1
        AND u.id NOT in (select USER_ID from frauds)
        AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
        AND pu.lock_history_id is null
        AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
        AND u.is_vip = 0
        AND u.time_sc_house_result is null
        AND (runType = RUN_TYPE_ALL_USERS or u.time_created > sysdate - 7)
   --  and u.id = 52197

    ) LOOP

        -- DBMS_OUTPUT.PUT_LINE('user12 : ' || v_usr.user_id);
          v_peh := null;
          populationEntryId := 0;
          qualifiedPopualtionId := 0;
          newPopualtionThreshold := 0;
          isFirstMinimumBalanceCheck := 1;
          assignedWriterId := v_usr.curr_assigned_writer_id;
          users_popualtion_users_id := v_usr.population_user_id ;
          lastSalesDepositTime := null;

          IF (v_usr.population_id is null) THEN
                currentPopulationId := 0;
                currentPopulationPriority := 999999; -- Lowest proirity
          ELSE
                currentPopulationId := v_usr.population_id;
                currentPopulationPriority := v_usr.priority;
          END IF;

          FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable
                              FROM
                                    populations p,
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND pt.priority < currentPopulationPriority -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_usr.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = runType
                              ORDER BY pt.priority -- Highest priority is 1
          ) LOOP

              populationEntHist := null;
              popQualificationTime := null;
              popBaseQualificationTime := null;
              isCheckQualificationTime := 0;

              CASE
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 1 THEN --POPULATION_FIRST_DEPO_FAILED
              --    DBMS_OUTPUT.PUT_LINE('user : ' || v_usr.user_id);
                         IF (v_usr.is_declined = 1) THEN

                              -- Check whether this user had some successed deposits in the past
                              select
                                 max(t.id)
                              into
                                 isExistSuccessfullDeposit
                              from
                                 transactions t,
                                 transaction_types tt
                              where
                                 tt.class_type = 1 AND
                                 user_id = v_usr.user_id AND
                                 type_id = tt.id AND
                                 status_id in (2,7,8);

                              IF (isExistSuccessfullDeposit is null) THEN

                                    popBaseQualificationTime  := v_usr.last_decline_date;
                                    popQualificationTime := popBaseQualificationTime;

                                     isCheckQualificationTime := 1;

                              END IF;
                           END IF; -- IF (v_usr.is_declined = 1) THEN

                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 2 THEN -- POPULATION_LAST_DEPO_FAILED

                        -- If users doesn't want to ne called for decline after approve we don't insert him into DDA population.
                        IF (v_usr.is_contact_for_daa != 0 AND v_usr.is_declined = 1) THEN

                              popBaseQualificationTime  := v_usr.last_decline_date;
                              popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                              IF (sysdate  >= popQualificationTime) THEN

                                        -- Check whether this user had some successed deposits in the past
                                        IF (isExistSuccessfullDeposit is not null) THEN

                                               isCheckQualificationTime := 1;

                                        END IF; -- IF (isExistSuccessfullDeposit is not null) THEN
                              END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 5 THEN -- POPULATION_NO_DEPOSIT_FILL

                        popBaseQualificationTime := v_usr.time_created;
                        popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                        IF (sysdate  >= popQualificationTime) THEN

                              select
                                max(t.id)
                              into
                                isExist
                              from
                                transactions t,
                                transaction_types tt
                              where
                                tt.class_type = 1 AND
                                user_id = v_usr.user_id AND
                                type_id = tt.id;

                                IF (isExist is null) THEN

                                       isCheckQualificationTime := 1;

                                END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 9 THEN -- POPULATION_DORMANT_ACCOUNT

                        IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                                -- IF we didn't find a sales deposit for that user before, search it.
                                IF (lastSalesDepositTime is null) THEN

                                      select
                                            max(t.time_created)
                                       into
                                            lastSalesDepositTime
                                       from
                                            transactions t,
                                            transactions_issues ti
                                      where
                                            t.id = ti.transaction_id
                                            and t.user_id = v_usr.user_id;
                                END IF; --  IF (lastSalesDepositTime is null) THEN

                                -- If user had no sales deposit in the past or v_population.LAST_SALES_DEPOSIT_DAYS has passed since last sales deposit time... go on...
                                IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN

                                      popBaseQualificationTime := GET_LAST_INVEST_TIME(v_usr.user_id);

                                      IF (popBaseQualificationTime is not null) THEN

                                           -- If v_population.number_of_days has passed since last user login time
                                           -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                                           IF (v_usr.time_last_login < sysdate - v_population.last_invest_day
                                                      OR
                                                 popBaseQualificationTime < sysdate - v_population.max_last_login_time_days) THEN

                                                    popQualificationTime := popBaseQualificationTime + v_population.last_invest_day;

                                                    IF (sysdate  >= popQualificationTime) THEN

                                                           -- Check if user was above min balance during the all threshold time and didn't had a balance increase
                                                           IF (get_is_dormant_above_balance(v_usr.user_id,
                                                                                                                  v_population.last_invest_day,
                                                                                                                  v_usr.min_investment_limit,
                                                                                                                  v_usr.balance) = 1) THEN

                                                                 isCheckQualificationTime := 1;
                                                            END IF;

                                                    END IF; -- IF (sysdate  >= popQualificationTime) THEN
                                              END IF; -- IF (v_usr.time_last_login < sysdate - v_population.number_of_days...
                                      END IF; -- IF (popBaseQualificationTime is not null) THEN
                                END IF; -- IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN
                        END IF; -- IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 10 THEN -- POPULATION_NO_INVESTMENTS

                          popBaseQualificationTime := GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.user_id);

                          IF (popBaseQualificationTime is not null) THEN

                                popQualificationTime := popBaseQualificationTime + v_population.number_of_days;

                                IF(sysdate  >  popQualificationTime) THEN

                                      select   max(i.id)
                                      into      isExist
                                      from     investments i
                                      where   v_usr.user_id = i.user_id;

                                      IF (isExist is null) THEN

                                            isCheckQualificationTime := 1;

                                      END IF;
                                END IF;
                          END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id in (14,15,16,17,18,19) THEN -- POPULATION_MINIMUM_BALANCE
                                 userNumOfSuccesDeposits := GET_NUMBER_OF_SUCCESS_DEPOSITE(v_usr.user_id);

                         DBMS_OUTPUT.PUT_LINE(' userNumOfSuccesDeposits ' ||  userNumOfSuccesDeposits);
                         IF( v_usr.time_sc_house_result is  null )  THEN

                                 IF (userNumOfSuccesDeposits = v_population.number_of_deposits OR
                                        (userNumOfSuccesDeposits >= 6 AND v_population.number_of_deposits = 6)) THEN

                                        popBaseQualificationTime := v_usr.time_last_login;
                                        -- If v_population.number_of_days has passed since last user login time
                                         -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                                         IF (v_usr.time_last_login < sysdate - v_population.number_of_days) THEN

                                                popQualificationTime := popBaseQualificationTime + v_population.number_of_days;

                                                 isCheckQualificationTime := 1;
                                          END IF;
                                 END IF;
                         END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    ELSE  --IF  v_population.population_type_id wasn't found
                        -- No handler for this population type
                      DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
                END CASE;

                -- IF isCheckQualificationTime = 1, User is qualified to that populaiton, now check his qualification time
                IF (isCheckQualificationTime = 1) THEN

                      populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                      IF (populationEntHist.status_id is null
                                 OR
                            popBaseQualificationTime >= populationEntHist.TIME_CREATED
                                 OR
                           (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                                OR
                           (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                          -- Find qualification time of this population last entry
                          BEGIN
                              select base_qualification_time
                              into popBaseQualificationTimeOld
                              from population_entries pe
                              where pe.id = populationEntHist.population_entry_id
                                  and qualification_time is not null ;
                        EXCEPTION
                              WHEN no_data_found THEN
                                      popBaseQualificationTimeOld := null;
                           END;

                          -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                          IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                                 populationEntryId :=  populationEntHist.population_entry_id;
                          END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                          -- Update user entry with this population id.
                          qualifiedPopualtionId := v_population.id;

                           -- Keep current popualtion threshold
                          newPopualtionThreshold := v_population.threshold;

                           -- If user was finally found qualified for current popualtion, break the loop
                          GOTO end_loop;

                      END IF; -- IF (populationEntHist.status_id is null ....
                END IF; --   IF (isCheckQualificationTime = 1) THEN

          END LOOP;
           <<end_loop>>

          -- If got a newPopualtionId, insert user to new population.
          IF (qualifiedPopualtionId != 0) THEN

                count_new_qualified_users := count_new_qualified_users +1;
  --              DBMS_OUTPUT.PUT_LINE('user_id: ' || v_usr.user_id);

                -- If this user is in general entry_type preform auto cancel assign.
                IF (v_usr.entry_type_id = 1) THEN
                   assignedWriterId := null;
                END IF;

                -- If user is already in a population, remove it first.
                IF (v_usr.curr_population_entry_id is not null) THEN
                      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                      v_peh.population_entry_id := v_usr.curr_population_entry_id;
                      v_peh.status_id := 105;
                      v_peh.writer_id := 0;
                      v_peh.assigned_writer_id := assignedWriterId;
                      v_peh.time_created := sysdate;
                      v_peh.issue_action_id := null;
                      INSERT INTO population_entries_hist VALUES v_peh;
                END IF; -- IF (v_usr.curr_population_entry_id is not null) THEN

                 -- If user doesn't have an entry in population_users, create one.
                IF (users_popualtion_users_id is null) THEN
                      SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                      v_pu.id := users_popualtion_users_id;
                      v_pu.user_id := v_usr.user_id;
                      if (v_usr.contact_id != 0) then
                        v_pu.contact_id := v_usr.contact_id;
                      else
                        v_usr.contact_id := null;
                      end if;
                      v_pu.curr_assigned_writer_id := null;
                      v_pu.curr_population_entry_id := null;
                      v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                      v_pu.entry_type_action_id := null;
                      v_pu.lock_history_id := null;
  --                    DBMS_OUTPUT.PUT_LINE('user_id: ' || v_usr.user_id);
                      INSERT INTO population_users VALUES v_pu;
                END IF; --  IF (users_popualtion_users_id is null) THEN

                -- If population entry id hasn't been changed create a new one.
                IF (populationEntryId = 0) THEN
                      -- Count the number of users in the new population in order to determine user's entry group.
                      SELECT
                              COUNT(pe.id) INTO v_counter
                      FROM
                              population_entries pe,
                              population_users pu
                      WHERE
                              pe.id = pu.curr_population_entry_id AND
                              pe.population_id = qualifiedPopualtionId;

                      -- Insert a new population_entry
                      SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                      v_pe.id := populationEntryId;
                      v_pe.population_id := qualifiedPopualtionId;
                      v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                      v_pe.population_users_id := users_popualtion_users_id;
                      v_pe.qualification_time := popQualificationTime ;
                      v_pe.base_qualification_time := popBaseQualificationTime ;
                      v_pe.is_displayed := 1;
                      INSERT INTO population_entries VALUES v_pe;

                      qualificationStatus := 1; -- Qualification
                ELSE
                      qualificationStatus := 7; -- reQualification
                END IF; --  IF (populationEntryId = 0) THEN


                 -- Insert a new population_entry_history
                SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                v_peh.population_entry_id :=populationEntryId;
                v_peh.status_id := qualificationStatus;
                v_peh.writer_id := 0;
                v_peh.assigned_writer_id := assignedWriterId;
                v_peh.time_created := sysdate;
                v_peh.issue_action_id := null;
                INSERT INTO population_entries_hist VALUES v_peh;

                -- Update user entry in population_users table
                UPDATE
                      population_users pu
                SET
                      pu.curr_population_entry_id = populationEntryId,
                      pu.curr_assigned_writer_id = assignedWriterId
                WHERE
                     pu.user_id = v_usr.user_id;

          END IF; -- IF (qualifiedPopualtionId != 0) THEN

         COMMIT;

    END LOOP;

    if (runType = RUN_TYPE_ALL_USERS) then
          update db_parameters dp
          set date_value = sysdate
          where dp.id = 9;
    end if;

    -- RUN DELAY HANDLER
    delay_handler();

    -- RUN TRACKING HANDLER
    tracking_handler();

      -- RUN SPECIAL HANDLER
    special_handler();

    -- RUN CALLBACK HANDLER
    callback_handler();

    DBMS_OUTPUT.PUT_LINE('count_new_qualified_users  = ' || count_new_qualified_users);

  END IF;

  FOR v_con IN (
    SELECT
      c.id contact_id,
      c.skin_id,
      c.time_created,
      c.type,
      p.id population_id,
      p.population_type_id curr_pop_type_id,
      pu.id population_user_id,
      pu.curr_population_entry_id,
      pu.curr_assigned_writer_id,
      pu.entry_type_id,
      pu.entry_type_action_id,
      pt.priority,
      s.other_reactions_days
    FROM
      skins s,
      contacts c
          LEFT JOIN population_users pu on c.id = pu.contact_id
              LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                  LEFT JOIN populations p on pe.population_id = p.id
                        LEFT JOIN population_types pt on p.population_type_id = pt.id
    WHERE
      c.skin_id = s.id
      AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
      AND pu.lock_history_id is null
      AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
      AND c.type in (5,6) -- Short Reg Form contacts and register attempt
      AND (c.land_line_phone is not null OR c.mobile_phone is not null)
      AND c.user_id = 0
  ) LOOP

        v_peh := null;
        populationEntryId := 0;
        qualifiedPopualtionId := 0;
        newPopualtionThreshold := 0;
        assignedWriterId := v_con.curr_assigned_writer_id;
        users_popualtion_users_id := v_con.population_user_id ;

        IF (v_con.population_id is null) THEN
              currentPopulationId := 0;
              currentPopulationPriority := 999999; -- Lowest proirity
        ELSE
              currentPopulationId := v_con.population_id;
              currentPopulationPriority := v_con.priority;
        END IF;

        FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable
                              FROM
                                    populations p,
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND pt.priority < currentPopulationPriority -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_con.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = RUN_TYPE_ONLY_CONTACTS
                              ORDER BY pt.priority -- Highest priority is 1
        ) LOOP

            populationEntHist := null;
            popQualificationTime := null;
            popBaseQualificationTime := null;
            isCheckQualificationTime := 0;

            CASE
                ------------------------------------------------------------------------------------------------------------------------------------------------
                 WHEN v_population.population_type_id in (20, 21) THEN --POPULATION_SHORT_REG_FORM AND POPLULATION_REGISTER_ATTEMPT

                    popBaseQualificationTime := v_con.time_created;
                    popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                    if (popQualificationTime < sysdate) then
                       isCheckQualificationTime := 1;
                    end if;
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  ELSE  --IF  v_population.population_type_id wasn't found
                      -- No handler for this population type
                    DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
             END CASE;

              -- IF isCheckQualificationTime = 1, contact is qualified to that populaiton, now check his qualification time
              IF (isCheckQualificationTime = 1) THEN

                    populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                    IF (populationEntHist.status_id is null
                               OR
                          popBaseQualificationTime >= populationEntHist.TIME_CREATED
                               OR
                         (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                               OR
                         (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                        -- Find qualification time of this population last entry
                        BEGIN
                            select base_qualification_time
                            into popBaseQualificationTimeOld
                            from population_entries pe
                            where pe.id = populationEntHist.population_entry_id
                                and qualification_time is not null ;
                        EXCEPTION
                            WHEN no_data_found THEN
                                    popBaseQualificationTimeOld := null;
                        END;

                        -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                        IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                               populationEntryId :=  populationEntHist.population_entry_id;
                        END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                        if( ( v_con.type = 5 AND v_population.population_type_id = 20 ) OR   ( v_con.type = 6  AND v_population.population_type_id = 21 )) then
                        -- Update contact entry with this population id.
                              qualifiedPopualtionId := v_population.id;
                              -- Keep current popualtion threshold
                              newPopualtionThreshold := v_population.threshold;
                              -- If contact was finally found qualified for current popualtion, break the loop
                              GOTO end_loop;

                    END IF; -- IF (populationEntHist.status_id is null ....
              END IF; --   IF (isCheckQualificationTime = 1) THEN

        END LOOP;
         <<end_loop>>

        -- If got a newPopualtionId, insert contact to new population.
        IF (qualifiedPopualtionId != 0) THEN

              count_new_qualified_contacts := count_new_qualified_contacts +1;

              -- If this contact is in general entry_type preform auto cancel assign.
              IF (v_con.entry_type_id = 1) THEN
                 assignedWriterId := null;
              END IF;

              -- If contact is already in a population, remove it first.
              IF (v_con.curr_population_entry_id is not null) THEN
                    SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                    v_peh.population_entry_id := v_con.curr_population_entry_id;
                    v_peh.status_id := 105;
                    v_peh.writer_id := 0;
                    v_peh.assigned_writer_id := assignedWriterId;
                    v_peh.time_created := sysdate;
                    v_peh.issue_action_id := null;
                    INSERT INTO population_entries_hist VALUES v_peh;
              END IF; -- IF (v_con.curr_population_entry_id is not null) THEN

               -- If contact doesn't have an entry in population_users, create one.
              IF (users_popualtion_users_id is null) THEN
                    SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                    v_pu.id := users_popualtion_users_id;
                    v_pu.user_id := null;
                    v_pu.contact_id := v_con.contact_id;
                    v_pu.curr_assigned_writer_id := null;
                    v_pu.curr_population_entry_id := null;
                    v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                    v_pu.entry_type_action_id := null;
                    v_pu.lock_history_id := null;
--                    DBMS_OUTPUT.PUT_LINE('contact_id: ' || v_con.contact_id);
                    INSERT INTO population_users VALUES v_pu;
              END IF; --  IF (users_popualtion_users_id is null) THEN

              -- If population entry id hasn't been changed create a new one.
              IF (populationEntryId = 0) THEN
                    -- Count the number of users in the new population in order to determine user's entry group.
                    SELECT
                            COUNT(pe.id) INTO v_counter
                    FROM
                            population_entries pe,
                            population_users pu
                    WHERE
                            pe.id = pu.curr_population_entry_id AND
                            pe.population_id = qualifiedPopualtionId;

                    -- Insert a new population_entry
                    SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                    v_pe.id := populationEntryId;
                    v_pe.population_id := qualifiedPopualtionId;
                    v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                    v_pe.population_users_id := users_popualtion_users_id;
                    v_pe.qualification_time := popQualificationTime ;
                    v_pe.base_qualification_time := popBaseQualificationTime ;
                    v_pe.is_displayed := 1;
                    INSERT INTO population_entries VALUES v_pe;

                    qualificationStatus := 1; -- Qualification
              ELSE
                    qualificationStatus := 7; -- reQualification
              END IF; --  IF (populationEntryId = 0) THEN


               -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              v_peh.population_entry_id :=populationEntryId;
              v_peh.status_id := qualificationStatus;
              v_peh.writer_id := 0;
              v_peh.assigned_writer_id := assignedWriterId;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := null;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update contact entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.curr_population_entry_id = populationEntryId,
                    pu.curr_assigned_writer_id = assignedWriterId
              WHERE
                   pu.contact_id = v_con.contact_id;

        END IF; -- IF (qualifiedPopualtionId != 0) THEN

       COMMIT;

  END LOOP;

  DBMS_OUTPUT.PUT_LINE('count_new_qualified_conacts  = ' || count_new_qualified_contacts);

  DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');

  NULL;
END;

----------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION GET_USER_LAST_POP_BY_TYPE
( pop_users_id IN NUMBER
, p_pop_type IN NUMBER
) RETURN  population_entries_hist%ROWTYPE IS

populationEntHist  population_entries_hist%ROWTYPE;

BEGIN

       SELECT
                PEHO.*
        INTO
                populationEntHist
        FROM
                 (SELECT
                      peh.*
                  FROM
                      population_entries_hist peh,
                      population_entries_hist_status pehs
                   WHERE
                      peh.status_id = pehs.id AND
                      pehs.is_remove = 1 AND
                      peh.population_entry_id =(select
                                                    max(pe.id)
                                                from
                                                        population_entries pe,
                                                        population_users pu,
                                                        populations p
                                                where
                                                         pe.population_users_id = pu.id AND
                                                         pu.id = pop_users_id  AND
                                                         pe.population_id = p.id AND
                                                         p.population_type_id = p_pop_type)
                    ORDER BY
                      peh.TIME_CREATED desc) PEHO
        WHERE
                rownum = 1;

  RETURN populationEntHist;

EXCEPTION
   WHEN no_data_found THEN
        RETURN NULL;

END GET_USER_LAST_POP_BY_TYPE;

 ------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE "TRANSACTIONS_ISSUES_CYCLE_FILL" AS

last_transaction_issues transactions_issues%ROWTYPE;
cycle_start_time date;
cycle_last_investment_id number;
investment_time_created date;

p_cycle_reached number;
p_last_investment_remainder number;
p_last_investment_id number;
p_time_settled date;

count_updated_transactions number := 0;
count_settled_transactions number := 0;
is_update_transaction number;
is_handle_transaction number;

inv_delay_hours number;

BEGIN

DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_CYCLE_FILL started at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');


-- Get the hours delay from db - delay that suppued to take only investments that happned before it in order to avoid getting RF investments
-- and to get real amount of Take Profit investments
select num_value
into inv_delay_hours
from db_parameters
where id = 3;

-- Find all transactions issues records which hasn't been settled.
FOR transaction1 IN (select
                                              t.id,
                                              t.time_created,
                                              t.amount,
                                              t.user_id,
                                              ti.cycle_reached,
                                              ti.time_settled,
                                              ti.last_investment_id,
                                              ti.last_investment_remainder
                                  from
                                              transactions_issues ti,
                                              transactions t
                                  where
                                              t.id = ti.transaction_id
                                              and ti.time_settled is null
                                              and t.time_created <= sysdate - inv_delay_hours / 24
                                              and exists (select i.id
                                                                 from investments i
                                                                 where i.user_id = t.user_id
                                                                      and i.time_created >= t.time_created
                                                                      and i.time_created < sysdate - inv_delay_hours / 24
                                                                      and i.is_canceled = 0)
--                                            and t.user_id  =12479
                                 order by
                                              -- need to order by user and then by investment id, to take last transaction that started to count investments
                                              t.user_id, ti.last_investment_id desc nulls last,  t.time_created) LOOP


--      DBMS_OUTPUT.PUT_LINE('%%%%%%%%%%: ');
--      DBMS_OUTPUT.PUT_LINE('transatction _id: ' || transaction1.id);
--      DBMS_OUTPUT.PUT_LINE('user _id: ' || transaction1.user_id);

   -- Initialize
   cycle_start_time := null;
   cycle_last_investment_id := 0;
   investment_time_created := null;

   p_cycle_reached := 0;
   p_last_investment_remainder := 0;
   p_time_settled := null;
   p_last_investment_id := 0;

   is_handle_transaction := 0;
   is_update_transaction := 0;

  BEGIN
          -- If last_investment_id of current transaction_issues is greater than 0 it means we'va already started handeling this transaction
          -- Else we'll look for the last transaction in this table for the current user.
          IF (transaction1.last_investment_id > 0) THEN

                -- Set parameters from current transaction.
                cycle_last_investment_id := transaction1.last_investment_id;
                p_cycle_reached := transaction1.cycle_reached;
                is_handle_transaction := 1;

          ELSE

                BEGIN
                    -- Find the last user's transaction_issues entry that was qualified for that user.
                    select *
                    into last_transaction_issues
                    from (select ti.*
                              from transactions_issues ti, transactions t
                              where ti.transaction_id = t.id
                                  and t.user_id = transaction1.user_id
                                  and ti.last_investment_id > 0
--                                  and t.time_created <= transaction1.time_created
                                  and t.id != transaction1.id
                               -- order by desc investment id, so we'll take the last investment that was we used for qualification
                              order by ti.last_investment_id desc)
                    where rownum = 1;

                    -- If prior transaction was settled, get parameters from it.
                    IF (last_transaction_issues.time_settled is not null) THEN
                          cycle_last_investment_id := last_transaction_issues.last_investment_id;
                          is_handle_transaction := 1;

                          -- If the investment that settled the priot transaction was created before the transaction currently being checked
                          -- And there's a remainder... get the remainder.
                          IF (last_transaction_issues.time_settled > transaction1.time_created AND
                              last_transaction_issues.last_investment_remainder > 0) THEN
                                p_cycle_reached := last_transaction_issues.last_investment_remainder;
                                p_last_investment_id := last_transaction_issues.last_investment_id;
                                investment_time_created := last_transaction_issues.time_settled;
                                is_update_transaction := 1;
                          END IF;
                    ELSE
                       -- If prior transaction wasn't settled, stop handling this one.
                       is_handle_transaction := 0;
                    END IF;

                EXCEPTION
                     -- If we couldn't find prior transaction set parameters to default.
                      WHEN no_data_found THEN
                            p_cycle_reached := 0;
                            cycle_last_investment_id  := 0;
                            is_handle_transaction := 1;
                END;

          END IF; -- IF (transaction1.last_investment_id > 0) THEN

          -- If last_investment_id greater than 0, set cycle_start_time as investment time,
          -- Else set cycle_start_time as transaction time.
          IF (cycle_last_investment_id > 0) THEN

                  select i.time_created
                  into cycle_start_time
                  from investments i
                  where i.id = cycle_last_investment_id;

                   -- If the last investment was created before transaction set cycle_start_time as transaction time.
                  IF (cycle_start_time < transaction1.time_created) THEN
                        cycle_start_time := transaction1.time_created;
                  END IF;
          ELSE
                  cycle_start_time := transaction1.time_created;
                  cycle_last_investment_id := 0;
          END IF; -- IF (cycle_last_investment_id > 0) THEN

    EXCEPTION
          WHEN no_data_found THEN
                cycle_start_time := null;
                is_handle_transaction := 0;
                DBMS_OUTPUT.PUT_LINE('ERROR - problem with transaction: ' || last_transaction_issues.transaction_id ||
                                                                ' on processing transaction: ' || transaction1.id);
    END;


   IF  (is_handle_transaction = 1) THEN

            -- In case that transcation was settled by remainder no need to check other invetments
            IF (p_cycle_reached < transaction1.amount) THEN
                    -- Find all investments after the cycle_start_time.
                    FOR investment IN (select
                                                                i.*
                                                    from
                                                                investments i
                                                    where
                                                                i.time_created >= cycle_start_time
                                                                and i.time_created < sysdate - inv_delay_hours / 24
                                                                and i.is_canceled = 0
                                                                and i.user_id = transaction1.user_id
                                                                and i.id != cycle_last_investment_id
                                                   order by
                                                                time_created) LOOP

                          IF (is_update_transaction = 0) THEN
                               is_update_transaction := 1;
                          END IF;

                          p_last_investment_id := investment.id;
                          p_cycle_reached := p_cycle_reached + investment.amount;
                          investment_time_created := investment.time_created;

                          EXIT WHEN (p_cycle_reached >= transaction1.amount) ;

                  END LOOP;
            END IF; -- IF (p_cycle_reached < transaction1.amount) THEN

             -- If we had any relevant investments, update transaction_issues table.
            IF (is_update_transaction = 1) THEN

                   -- If transaction was settles set the relevant parameters.
                  IF (p_cycle_reached >= transaction1.amount) THEN
                        p_last_investment_remainder := p_cycle_reached - transaction1.amount;
                        p_cycle_reached := transaction1.amount;
                        p_time_settled := investment_time_created;

                        count_settled_transactions := count_settled_transactions + 1;
                  END IF; -- IF (p_cycle_reached >= transaction1.amount) THEN

--                            DBMS_OUTPUT.PUT_LINE('UPDATE: ');
--                            DBMS_OUTPUT.PUT_LINE('cycle_reached: ' || p_cycle_reached);
--                            DBMS_OUTPUT.PUT_LINE('time_settled: ' || p_time_settled);
--                            DBMS_OUTPUT.PUT_LINE('last_investment_id: ' || p_last_investment_id);
                            DBMS_OUTPUT.PUT_LINE(' tran id: ' ||  transaction1.id);

                    UPDATE
                            transactions_issues ti
                    SET
                            ti.cycle_reached = p_cycle_reached,
                            ti.time_settled = p_time_settled,
                            ti.last_investment_id = p_last_investment_id,
                            ti.last_investment_remainder = p_last_investment_remainder
                    WHERE
                            ti.transaction_id = transaction1.id;

                    count_updated_transactions := count_updated_transactions + 1;

                    COMMIT;
            END IF; -- IF (is_update_transaction = 1) THEN

     END IF; -- IF  (is_handle_transaction = 1) THEN

END LOOP;

      update db_parameters
      set date_value = sysdate
      where id = 4; -- last cycle fill run

DBMS_OUTPUT.PUT_LINE('Number of updated transactions : ' || count_updated_transactions);
DBMS_OUTPUT.PUT_LINE('Number of settled transactions : ' || count_settled_transactions);

DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('TRANSACTIONS_ISSUES_CYCLE_FILL ended at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------------------------:');

  NULL;
END TRANSACTIONS_ISSUES_CYCLE_FILL;

 ----------------------------------------------------------------------------------

create or replace
FUNCTION           "GET_LAST_INVEST_TIME" ( p_userId IN NUMBER) RETURN DATE AS

last_invest_time DATE := null;

BEGIN

    select max(TIME_CREATED)
    into last_invest_time
    from investments
    where user_id = p_userId;

     return last_invest_time ;
END;

-------------------------------------------------------------------------------------
create or replace PROCEDURE TRACKING_HANDLER AS

v_peh population_entries_hist%ROWTYPE;

BEGIN

  -- Find all users that needs to be removed from tracking by time or by decline
  FOR v_pop_user IN (SELECT
                                            pu.id,
                                            i.population_entry_id,
                                            ia.action_time,
                                            pu.curr_population_entry_id,
                                            p.population_type_id,
                                            pe.qualification_time,
                                            (CASE WHEN (sysdate > ia.action_time + s.other_reactions_days) THEN 1 ELSE 0 END) as is_time_end
                                    FROM
                                            population_users pu
                                                  left join users u on pu.user_id = u.id
                                                  left join contacts c on pu.contact_id = c.id
                                                  left join population_entries pe on pu.curr_population_entry_id = pe.id
                                                        left join populations p on pe.population_id = p.id,
                                            issue_actions ia,
                                            issues i,
                                            skins s
                                    WHERE
                                            pu.entry_type_id = 2 -- Tracking
                                            and pu.lock_history_id is null
                                            and pu.entry_type_action_id = ia.id
                                            and ia.issue_id = i.id
                                            and ((u.id is not null and s.id = u.skin_id) or (u.id is null and s.id = c.skin_id))
                                            and (sysdate > ia.action_time + s.other_reactions_days
                                                              or -- users re/entered to decline pop
                                                    (p.population_type_id = 1 and (pe.id !=  i.population_entry_id or pe.qualification_time > ia.action_time)))
                                    ) LOOP
            -- Init params
            v_peh := null;

             -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              -- If users' tracking time ended remove it
              -- Else user entered to first decline after it got into TRACKING remove it by decline
              IF (v_pop_user.is_time_end = 1) THEN
                    v_peh.population_entry_id := v_pop_user.population_entry_id;
                    v_peh.status_id := 202;  -- remove from tracking by time
              ELSE
                    v_peh.population_entry_id := v_pop_user.curr_population_entry_id;
                    v_peh.status_id := 206;  -- remove from tracking by decline
              END IF;

              v_peh.writer_id := 0;
              v_peh.assigned_writer_id := null;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := null;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update user entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.entry_type_id = 1, -- population.enrty.type.general
                    pu.entry_type_action_id = null,
                    pu.curr_assigned_writer_id = null
              WHERE
                   pu.id = v_pop_user.id;

  END LOOP;

  NULL;
END TRACKING_HANDLER;
-------------------------------------------------------------------------------------


create or replace PROCEDURE MARKETING_HANDLER AS

	v_peh population_entries_hist%ROWTYPE;
  assignedWriterId NUMBER;

BEGIN

FOR v_pop_usr IN (SELECT
                                        pu.id,
                                        pu.curr_population_entry_id,
                                        pu.entry_type_id,
                                        pu.curr_assigned_writer_id
                                 FROM
                                        population_users pu,
                                        population_entries pe,
                                        populations p,
                                        population_entries_hist peh,
                                        issue_actions ia,
                                        marketing_operations mo
                                 WHERE
                                        pu.entry_type_id != 5 -- not users that was removed from sales
                                        and pu.curr_population_entry_id = pe.id
                                        and pe.population_id = p.id
                                        and p.population_type_id = 13 -- Marketing popualtion
                                        and peh.population_entry_id = pe.id
                                        and peh.status_id = 1 -- qualification
                                        and peh.issue_action_id = ia.id
                                        and ia.marketing_operation_id = mo.id
                                        and pe.qualification_time + mo.duration_days < sysdate
                                        ) LOOP
        -- Init params
        v_peh := null;
        assignedWriterId := null;

        -- If user is in general entry type id cancel assign
        IF (v_pop_usr.entry_type_id != 1) THEN
               assignedWriterId := v_pop_usr.curr_assigned_writer_id;
        END IF;

        -- Insert a remove record to  population_entry_history
        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

        v_peh.population_entry_id := v_pop_usr.curr_population_entry_id;
        v_peh.status_id := 113; -- remove from pop by time
        v_peh.writer_id := 0;
        v_peh.assigned_writer_id := assignedWriterId;
        v_peh.time_created := sysdate;
        v_peh.issue_action_id := null;
        INSERT INTO population_entries_hist VALUES v_peh;

        -- Update user entry in population_users table
        UPDATE
              population_users pu
        SET
              pu.curr_population_entry_id = null,
              pu.curr_assigned_writer_id = assignedWriterId
        WHERE
             pu.id = v_pop_usr.id;

END LOOP;

  NULL;
END MARKETING_HANDLER;

create or replace FUNCTION  "GET_IS_DORMANT_ABOVE_BALANCE" ( p_user_id IN NUMBER,
                                                                                                p_time_threshold NUMBER,
                                                                                                p_balance_threshold NUMBER,
                                                                                                p_users_curr_balance NUMBER ) RETURN NUMBER AS

      /* ------------------------------------------------------------------------------
          This function checks if the user was above min balance
          during all thershold time and didn't have any balance increasment
          -------------------------------------------------------------------------------*/

      last_balance  NUMBER := p_users_curr_balance;

BEGIN

  -- Go through all users balance history records from latest to oldest
  FOR v_balance_his IN (select     *
                                        from       balance_history bh
                                        where     bh.user_id  = p_user_id
                                        order by bh.time_created desc) LOOP

      -- If  balance_history's balance is under min balance return false
      IF (v_balance_his.balance < p_balance_threshold) THEN
            RETURN 0;
      ELSE

             -- If curr balance is outside of threshold time return true;
             IF (v_balance_his.time_created < sysdate - p_time_threshold) THEN
                  RETURN 1;

              ELSE
                  -- If curr balance_history's balance is under last balance_history's balance return false;
                  IF (v_balance_his.balance < last_balance) THEN

                          RETURN 0;

                  END IF; -- IF (v_balance_his.balance < last_balance) THEN

             END IF; -- IF (last_balance_date < sysdate - p_time_threshold) THEN

        END IF; -- IF (v_balance_his.balance < p_balance_threshold) THEN

        -- Keep last balance history data;
        last_balance := v_balance_his.balance;
  END LOOP;

  RETURN 0;

END GET_IS_DORMANT_ABOVE_BALANCE;


/*******************************************************************************************
       This function returns the entry type id which the user was in when action was inserted
********************************************************************************************/
create or replace
FUNCTION GET_ACTION_ENTRY_TYPE ( p_action_id IN NUMBER, p_action_time IN DATE)
RETURN NUMBER AS

  entryTypeId NUMBER := 0;
  popUserId NUMBER;
  maxHistoryId NUMBER;

BEGIN

    -- Find action pop user id
    BEGIN
            SELECT
                  pe.population_users_id
            INTO
                  popUserId
            FROM
                  issue_actions ia,
                  issues i,
                  population_entries pe
            WHERE
                  ia.issue_id = i.id
                  and i.population_entry_id = pe.id
                  and ia.id = p_action_id;

      EXCEPTION
              WHEN no_data_found THEN
                      popUserId := 0;
      END;

      IF (popUserId > 0) THEN

              -- Find last history record that changed entry type
              SELECT
                     pre_his.id
              INTO
                      maxHistoryId
              FROM
                      (SELECT
                              peh.*
                      FROM
                              population_entries_hist peh,
                              population_entries pe,
                              population_entries_hist_status pehs
                      WHERE
                            peh.population_entry_id = pe.id
                            and pe.population_users_id = popUserId
                            and peh.status_id = pehs.id
                            and peh.time_created < p_action_time
                            and pehs.entry_type_change is not null
                      ORDER BY
                            peh.time_created desc) pre_his
                WHERE
                      rownum = 1;

              -- If found history record that changed entry type return that entry type
              IF (maxHistoryId is not null) THEN

                        SELECT
                               pehs.entry_type_change
                        INTO
                              entryTypeId
                        FROM
                                population_entries_hist_status  pehs,
                                population_entries_hist peh
                        WHERE
                                peh.id = maxHistoryId
                                and pehs.id = peh.status_id;

              ELSE
                      -- If no record that changed entry type was found return GENERAL
                      entryTypeId := 1;
              END IF; --  IF (maxHistoryId is not null) THEN

      END IF; -- IF (popUserId > 0) THEN

  RETURN entryTypeId;
END GET_ACTION_ENTRY_TYPE;



/***************************************************************************************
This procedure takes all the unassigned users that are in special pop and are in entry type 1 (GENERAL)
 and removes them from special population
****************************************************************************************/
create or replace
PROCEDURE SPECIAL_HANDLER AS

	v_peh population_entries_hist%ROWTYPE;
  assignedWriterId NUMBER;

BEGIN

FOR v_pop_usr IN (SELECT
                                        pu.id,
                                        pu.curr_population_entry_id
                                 FROM
                                        population_users pu,
                                        population_entries pe,
                                        populations p
                                 WHERE
                                        pu.entry_type_id = 1 -- not users that was removed from sales
                                        and pu.curr_population_entry_id = pe.id
                                        and pe.population_id = p.id
                                        and p.population_type_id = 11 -- Special popualtion
                                        and pu.curr_assigned_writer_id is null
                                        ) LOOP

        -- Insert a remove record to  population_entry_history
        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

        v_peh.population_entry_id := v_pop_usr.curr_population_entry_id;
        v_peh.status_id := 114; -- remove from special
        v_peh.writer_id := 0;
        v_peh.assigned_writer_id := null;
        v_peh.time_created := sysdate;
        v_peh.issue_action_id := null;
        INSERT INTO population_entries_hist VALUES v_peh;

        -- Update user entry in population_users table
        UPDATE
              population_users pu
        SET
              pu.curr_population_entry_id = null
        WHERE
             pu.id = v_pop_usr.id;

END LOOP;

  NULL;
END SPECIAL_HANDLER;

-----------------------------------------------------------------------------------
create or replace
PROCEDURE DELAY_HANDLER AS

v_peh population_entries_hist%ROWTYPE;
delayTime NUMBER;
isCancelDelayBySwitchPop NUMBER;

BEGIN

  -- Find all users that are in delay entry_type_id (6)
  FOR v_pop_user IN (SELECT
                                            pu.id,
                                            pu.delay_id,
                                            pu.curr_population_entry_id,
                                            pt.is_delay_enable,
                                            pt.id curr_pop_type_id,
                                            pe.qualification_time curr_qualification_time,
                                            p.dept_id curr_pop_dept,
                                            d_ia.action_time delay_start_time,
                                            d_pe.id delay_pop_entry_id,
                                            d_pdt.delay_days_same_pop,
                                            d_pdt.delay_days_other_pop,
                                            d_p.population_type_id delay_pop_type_id,
                                            d_pd.delay_type_id,
                                            d_pd.delay_count,
                                            (CASE WHEN pe.id is not null AND pe.qualification_time > d_pe.qualification_time THEN
                                                              1
                                                          ELSE
                                                              0 END) is_new_pop_entry,
                                            (CASE WHEN pe.id is not null AND pe.qualification_time > d_ia.action_time THEN
                                                              1
                                                          ELSE
                                                              0 END) is_qualified_after_delay
                                    FROM
                                            population_users pu
                                                  left join users u on pu.user_id = u.id
                                                  left join contacts c on pu.contact_id = c.id
                                                  left join population_entries pe on pu.curr_population_entry_id = pe.id
                                                        left join populations p on pe.population_id = p.id
                                                            left join population_types pt on p.population_type_id = pt.id
                                                  left join population_delays d_pd on pu.delay_id = d_pd.id
                                                        left join population_delay_types d_pdt on d_pd.delay_type_id = d_pdt.id,
                                            issue_actions d_ia,
                                            issues d_i,
                                            population_entries d_pe,
                                            populations d_p

                                    WHERE
                                            pu.entry_type_id = 6 -- Delay
                                            and pu.lock_history_id is null
                                            and pu.entry_type_action_id = d_ia.id
                                            and d_ia.issue_id = d_i.id
                                            and d_i.population_entry_id = d_pe.id
                                            and d_pe.population_id = d_p.id

                                    ) LOOP
              -- Init params
              v_peh := null;
              delayTime := 0;
              isCancelDelayBySwitchPop := 0;

              -- First, If the delay was activated from DAA - remove that user from it's population
              IF (v_pop_user.curr_pop_dept = 2 AND v_pop_user.curr_population_entry_id = v_pop_user.delay_pop_entry_id AND v_pop_user.is_qualified_after_delay =0) THEN

                   -- Insert a new population_entry_history
                   SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                    v_peh.status_id := 602; -- remove from population in delay type
                    v_peh.population_entry_id := v_pop_user.curr_population_entry_id;
                    v_peh.writer_id := 0;
                    v_peh.assigned_writer_id := null;
                    v_peh.time_created := sysdate;
                    v_peh.issue_action_id := null;
                    INSERT INTO population_entries_hist VALUES v_peh;

                    -- Update user entry in population_users table
                    UPDATE
                          population_users pu
                    SET
                          pu.curr_population_entry_id = null,
                          pu.curr_assigned_writer_id = null
                    WHERE
                         pu.id = v_pop_user.id;

                      -- Init v_peh
                     v_peh := null;

              END IF;

              -- Check Delay
              IF (v_pop_user.delay_id is null) THEN

                    v_peh.status_id := 653;  -- remove from delay by call

              ELSE

                    -- Check If population has changed since the delay.
                    IF (v_pop_user.delay_pop_type_id != v_pop_user.curr_pop_type_id AND v_pop_user.is_new_pop_entry = 1) THEN

                           IF (v_pop_user.is_delay_enable = 0) THEN
                                  isCancelDelayBySwitchPop := 1;
                                  v_peh.status_id := 650;  -- remove from delay by  pop
                            ELSE
                                    -- If user'pop changed to a different one with a qualification time after delay, check delay with delay_days_other_pop param
                                    IF (v_pop_user.delay_type_id = 1) THEN
                                          delayTime := v_pop_user.delay_days_other_pop;
                                    ELSIF (v_pop_user.delay_type_id = 2) THEN
                                          delayTime := v_pop_user.delay_days_other_pop;
                                    END IF;

                                    v_peh.status_id := 651;  -- remove from delay by time switch pop
                            END IF;

                    ELSE
                            -- If it's the same population type, but a new population entry and it's an delay disable type, cancel delay
                            IF (v_pop_user.is_qualified_after_delay = 1 AND v_pop_user.is_delay_enable = 0) THEN
                                  isCancelDelayBySwitchPop := 1;
                                  v_peh.status_id := 650;  -- remove from delay by pop
                            ELSE
                                  -- If user is in the same pop, or in no pop or in a different one with a qualification time before delay (old one),
                                  -- check delay with delay_days_same_pop param
                                  IF (v_pop_user.delay_type_id = 1) THEN
                                        delayTime := v_pop_user.delay_days_same_pop * v_pop_user.delay_count;
                                  ELSIF (v_pop_user.delay_type_id = 2) THEN
                                        delayTime := v_pop_user.delay_days_same_pop;
                                  END IF;

                                  v_peh.status_id := 652;  -- remove from delay by time same pop
                            END IF;
                    END IF;
               END IF;

              IF (v_pop_user.delay_id is null OR sysdate > v_pop_user.delay_start_time + delayTime OR isCancelDelayBySwitchPop = 1) THEN

                       -- Insert a new population_entry_history
                        SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                        v_peh.population_entry_id := v_pop_user.delay_pop_entry_id;
                        v_peh.writer_id := 0;
                        v_peh.assigned_writer_id := null;
                        v_peh.time_created := sysdate;
                        v_peh.issue_action_id := null;
                        INSERT INTO population_entries_hist VALUES v_peh;

                        -- Update user entry in population_users table
                        UPDATE
                              population_users pu
                        SET
                              pu.entry_type_id = 1, -- population.enrty.type.general
                              pu.entry_type_action_id = null,
                              pu.curr_assigned_writer_id = null
                        WHERE
                             pu.id = v_pop_user.id;

              END IF;


  END LOOP;

  NULL;
END DELAY_HANDLER;

--------------------------------------------------------------------------------------------------

create or replace
TRIGGER TRG_AFT_INS_ISSUE_ACTION
AFTER INSERT ON ISSUE_ACTIONS
FOR EACH ROW

DECLARE
        issue_action_type   ISSUE_ACTION_TYPES%ROWTYPE;
BEGIN

        Select *
        into      issue_action_type
        from     issue_action_types iat
        where   iat.id = :new.issue_action_type_id;

        -- If call update last_call_action_id and Check if reached
        IF (issue_action_type.channel_id = 1) THEN
              -- IF reached call update calls_num and reached_calls_num
              IF (issue_action_type.reached_status_id = 2) THEN
                      update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  calls_num = calls_num + 1,
                                  reached_calls_num = reached_calls_num + 1
                      where  id = :new.issue_id;
              ELSE
                     update issues
                      set       last_action_id = :new.id,
                                  is_significant = is_significant + :new.is_significant,
                                  last_call_action_id = :new.id,
                                  calls_num = calls_num + 1
                      where  id = :new.issue_id;
              END IF; -- IF (issue_action_type.reached_status_id = 2) THEN
        ELSE
              update issues
              set       last_action_id = :new.id,
                          is_significant = is_significant + :new.is_significant
              where  id = :new.issue_id;
        END IF; --  IF (issue_action_type.channel_id = 1) THEN

END;

----------------------------------------------------------------------

create or replace
TRIGGER trg_aft_upd_issue_action
AFTER UPDATE ON issue_actions
FOR EACH ROW

BEGIN

        IF (:new.is_significant = 1 AND :old.is_significant = 0) THEN

              update issues
              set  is_significant = is_significant + 1
              where  id = :new.issue_id;

        ELSIF (:new.is_significant = 0 AND :old.is_significant = 1) THEN

              update issues
              set  is_significant = is_significant - 1
              where  id = :new.issue_id;

        END IF;

END;

--------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE CALLBACK_HANDLER AS

v_peh population_entries_hist%ROWTYPE;

BEGIN

  -- Find all users that needs to be removed from tracking by time or by decline
  FOR v_pop_user IN (SELECT
                                            pu.id,
                                            i.population_entry_id,
                                            ia.id entry_type_action_id,
                                            pu.entry_type_id,
                                            ia.action_time,
                                            p.population_type_id,
                                            pe.qualification_time,
                                            iat.is_tracking_action,
                                            pu.curr_assigned_writer_id,
                                            max (CASE WHEN tt.class_type = 1
                                                                THEN t.time_created
                                                                ELSE null END) last_dep_time,
                                          s.other_reactions_days tracking_days
                                    FROM
                                            population_users pu
                                                  left join users u on pu.user_id = u.id
                                                        left join transactions t on t.user_id = u.id and t.status_id in (2,7,8)
                                                           left join transaction_types tt  on t.type_id = tt.id and tt.class_type = 1
                                                  left join contacts c on pu.contact_id = c.id
                                                  left join population_entries pe on pu.curr_population_entry_id = pe.id
                                                        left join populations p on pe.population_id = p.id,
                                            issue_actions ia,
                                            issue_action_types iat,
                                            issues i,
                                            skins s
                                    WHERE
                                            pu.entry_type_id = 3 -- Call Back
                                            and pu.lock_history_id is null
                                            and pu.entry_type_action_id = ia.id
                                            and ia.issue_id = i.id
                                            and ia.issue_action_type_id = iat.id
                                            and ((u.id is not null and s.id = u.skin_id) or (u.id is null and s.id = c.skin_id))
                                            and instr(s.working_days ,to_char(sysdate - 1, 'D')) > 0
                                            and trunc(sysdate) > trunc(ia.callback_time) + 1
                                    group by pu.id, i.population_entry_id, ia.id, pu.entry_type_id, ia.action_time, p.population_type_id, pe.qualification_time, iat.is_tracking_action, pu.curr_assigned_writer_id, s.other_reactions_days
                                    ) LOOP
            -- Init params
            v_peh := null;

             -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              -- If users cb action was supposed to lead to tracking and user don't have a success deposit since, move to tracking
              -- Otherwise Rterun to general.
              IF (v_pop_user.is_tracking_action = 1 AND
                    sysdate < v_pop_user.action_time + v_pop_user.tracking_days AND
                            (v_pop_user.last_dep_time is null
                                  OR
                             v_pop_user.action_time > v_pop_user.last_dep_time)) THEN
                    v_peh.status_id := 215;  -- move to tracking
                    v_pop_user.entry_type_id := 2; -- tracking;
              ELSE
                    v_peh.status_id := 216;  -- move to general
                    v_pop_user.entry_type_id := 1; -- general;
                    v_pop_user.curr_assigned_writer_id := null;
                    v_pop_user.entry_type_action_id := null;
              END IF;

              v_peh.population_entry_id := v_pop_user.population_entry_id;
              v_peh.writer_id := 0;
              v_peh.assigned_writer_id :=  v_pop_user.curr_assigned_writer_id;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := v_pop_user.entry_type_action_id;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update user entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.entry_type_id = v_pop_user.entry_type_id, -- population.enrty.type.general
                    pu.entry_type_action_id = v_pop_user.entry_type_action_id,
                    pu.curr_assigned_writer_id = v_pop_user.curr_assigned_writer_id
              WHERE
                   pu.id = v_pop_user.id;

              COMMIT;
  END LOOP;

  NULL;
END CALLBACK_HANDLER;

------------------------------------------------------------------------------------------------------------------
create or replace procedure UPDATE_MARKETING_GOOGLE AS

BEGIN
   update_marketing_google_day(sysdate - 1);
END UPDATE_MARKETING_GOOGLE;
------------------------------------------------------------------------------------------------
create or replace procedure UPDATE_MARKETING_GOOGLE_DAY (date_of_data date) AS

  is_check_existing_data number := 1;
  is_existing_data number := 0;
  rows_count numeric := 0;

BEGIN

  IF (is_check_existing_data = 1) THEN
      select count(*)
      into is_existing_data
      from Marketing_Google_Report m
      where date_of_data = m.day;
  END IF;

  IF (is_existing_data = 0) THEN
      For v_data in (
              SELECT
                   distinct
                   u.dynamic_param,
                   u.combination_id,
                   clk.clicks_num ,
                   reg_users.registered_users,
                   dep_users.reg_frst_dep_users,
                   dep_users.frst_dep_users,
                   sums_transactions.sum_deposits,
                   sums_investments.turnover,
                   sums_investments.house_win
               FROM
                   users u

                     LEFT JOIN
                     (SELECT
                             u.dynamic_param,
                             u.combination_id,
                             count(*) as registered_users
                       FROM
                             users u
                       WHERE
                             u.class_id <> 0
                             AND u.time_created >= trunc(date_of_data ) and u.time_created < trunc(date_of_data + 1)
                             AND u.combination_id in (select mc.id
                                                      from   marketing_combinations mc ,
                                                             marketing_campaigns mca,
                                                             marketing_payment_recipients mp,
                                                             marketing_sources ms
                                                      where  mc.campaign_id = mca.id
                                                        and  mca.payment_recipient_id = mp.id
                                                        and  ms.id = mca.source_id
                                                        and  upper(mp.agent_name) = upper(ms.name)
                                                        and  ms.is_keep_daily_data = 1)
                       GROUP  BY
                             u.dynamic_param, u.combination_id) reg_users
                                              on NVL(u.dynamic_param,0) = NVL(reg_users.dynamic_param,0)
                                              and u.combination_id = reg_users.combination_id

                     LEFT JOIN
                     (SELECT
                             u.dynamic_param,
                             u.combination_id,
                             count(*) as frst_dep_users,
                             SUM(CASE WHEN trunc(u.time_created) = trunc(date_of_data )
                                      THEN 1 ELSE 0 END) as reg_frst_dep_users
                       FROM
                             users u,
                             users_first_deposit ufd,
                             transactions t
                       WHERE
                             u.id = ufd.user_id
                             AND t.id = ufd.first_deposit_id
                             AND u.id = t.user_id
                             AND t.time_created >= trunc(date_of_data ) and t.time_created < trunc(date_of_data + 1)
                             AND u.class_id <> 0
                             AND u.combination_id in (select mc.id
                                                      from   marketing_combinations mc ,
                                                             marketing_campaigns mca,
                                                             marketing_payment_recipients mp,
                                                             marketing_sources ms
                                                      where  mc.campaign_id = mca.id
                                                        and  mca.payment_recipient_id = mp.id
                                                        and  ms.id = mca.source_id
                                                        and  upper(mp.agent_name) = upper(ms.name)
                                                        and  ms.is_keep_daily_data = 1)
                       GROUP  BY
                             u.dynamic_param, u.combination_id) dep_users
                                              on NVL(u.dynamic_param,0) = NVL(dep_users.dynamic_param,0)
                                              and u.combination_id = dep_users.combination_id

                     LEFT JOIN
                     (SELECT
                          cl.dynamic_param,
                          cl.combination_id,
                          count(*) as clicks_num
                       FROM clicks cl
                       WHERE cl.time_created >= trunc(date_of_data ) and cl.time_created < trunc(date_of_data + 1)
                             AND cl.combination_id in (select mc.id
                                                      from   marketing_combinations mc ,
                                                             marketing_campaigns mca,
                                                             marketing_payment_recipients mp,
                                                             marketing_sources ms
                                                      where  mc.campaign_id = mca.id
                                                        and  mca.payment_recipient_id = mp.id
                                                        and  ms.id = mca.source_id
                                                        and  upper(mp.agent_name) = upper(ms.name)
                                                        and  ms.is_keep_daily_data = 1)
                             AND exists (select *
                                         from users u
                                         where u.combination_id = cl.combination_id
                                           and u.dynamic_param = cl.dynamic_param
                                           and u.class_id <> 0)
                       GROUP BY cl.dynamic_param, cl.combination_id) clk
                                                  on NVL(u.dynamic_param,0) = NVL(clk.dynamic_param ,0)
                                                  and u.combination_id = clk.combination_id

                      LEFT JOIN
                     (SELECT
                      /*+ index(t TRANSACTIONS_IDX_TIME_C) */
                      u.dynamic_param,
                      u.combination_id,
                      SUM(t.amount*t.rate/100) as sum_deposits
                       FROM users u, transactions t, transaction_types tt
                       WHERE u.id = t.user_id
                       AND t.type_id = tt.id
                       AND tt.class_type =  1
                       AND t.status_id IN ( 2,7,8 )
                       AND u.class_id <> 0
                       AND u.combination_id in (select mc.id
                                                from   marketing_combinations mc ,
                                                       marketing_campaigns mca,
                                                       marketing_payment_recipients mp,
                                                       marketing_sources ms
                                                where  mc.campaign_id = mca.id
                                                  and  mca.payment_recipient_id = mp.id
                                                  and  ms.id = mca.source_id
                                                  and  upper(mp.agent_name) = upper(ms.name)
                                                  and  ms.is_keep_daily_data = 1
                                                 )
                       AND t.time_created >= trunc(date_of_data ) and t.time_created < trunc(date_of_data + 1)
                       GROUP BY u.dynamic_param, u.combination_id) sums_transactions
                                                 on NVL(u.dynamic_param,0) = NVL(sums_transactions.dynamic_param ,0)
                                                 and u.combination_id = sums_transactions.combination_id

                       LEFT JOIN
                      (SELECT
                          /*+ index(i INVESTMENTS_TIME_CREATED) */
                          u.dynamic_param,
                          u.combination_id,
                          SUM(i.amount*i.rate/100) as turnover,
                          SUM(i.house_result*i.rate/100) as house_win
                       FROM users u,  investments i
                       WHERE u.id = i.user_id
                       AND i.is_settled = 1
                       AND i.is_canceled = 0
                       AND u.class_id <> 0
                       AND u.combination_id in (select mc.id
                                                from   marketing_combinations mc ,
                                                       marketing_campaigns mca,
                                                       marketing_payment_recipients mp,
                                                       marketing_sources ms
                                                where  mc.campaign_id = mca.id
                                                  and  mca.payment_recipient_id = mp.id
                                                  and  ms.id = mca.source_id
                                                  and  upper(mp.agent_name) = upper(ms.name)
                                                  and  ms.is_keep_daily_data = 1
                                                )
                       AND i.time_created >= trunc(date_of_data ) and i.time_created < trunc(date_of_data + 1)
                       GROUP BY u.dynamic_param, u.combination_id) sums_investments
                                                 on NVL(u.dynamic_param,0) = NVL(sums_investments.dynamic_param,0)
                                                 and u.combination_id = sums_investments.combination_id

               WHERE
                 u.class_id <> 0
                 AND u.combination_id in (select mc.id
                                          from   marketing_combinations mc ,
                                                 marketing_campaigns mca,
                                                 marketing_payment_recipients mp,
                                                 marketing_sources ms
                                          where  mc.campaign_id = mca.id
                                            and  mca.payment_recipient_id = mp.id
                                            and  ms.id = mca.source_id
                                            and  upper(mp.agent_name) = upper(ms.name)
                                            and  ms.is_keep_daily_data = 1)
                 AND (clk.clicks_num is not null
                      OR reg_users.registered_users is not null
                      OR dep_users.reg_frst_dep_users is not null
                      OR dep_users.frst_dep_users is not null
                      OR sums_transactions.sum_deposits is not null
                      OR sums_investments.turnover is not null
                      OR sums_investments.house_win is not null)
               ORDER BY u.dynamic_param
               ) loop

           insert into Marketing_Google_Report
           values (trunc(date_of_data ),
                   v_data.dynamic_param,
                   v_data.combination_id,
                   v_data.clicks_num,
                   v_data.registered_users,
                   v_data.reg_frst_dep_users,
                   v_data.frst_dep_users,
                   v_data.sum_deposits,
                   v_data.turnover,
                   v_data.house_win);

            rows_count := rows_count + 1;
       end loop;

       commit;
       Dbms_Output.put_line('There were ' || rows_count || ' rows added for ' || date_of_data);
  ELSE
       Dbms_Output.put_line('ERROR! There is already an existing data for ' || date_of_data);
  END IF;
END UPDATE_MARKETING_GOOGLE_DAY;

------------------------------------------------------------------------------------------------
create or replace procedure UPDATE_OPP_PLUS_INV is
begin
  insert into options_plus_investments
    select
        inv.id investment_id,
        opr.id promile_range_id,
        opm.id minutes_range_id,
        opp.price

    from
        (SELECT
            i.id ,
            o.market_id,
             CASE WHEN i.type_id = 2
               THEN (i.current_level - i.settled_level)/i.current_level
                 ELSE (i.settled_level - i.current_level)/i.current_level  END * 1000 promile,
            extract (MINUTE FROM (i.time_settled - sys_extract_utc(o.time_first_invest))) +
            extract (HOUR FROM (i.time_settled - sys_extract_utc(o.time_first_invest)))*60 min_diff
        FROM investments i , users u , opportunities o
        WHERE i.user_id = u.id
        AND i.opportunity_id = o.id
        AND o.opportunity_type_id = 3
        AND i.is_canceled = 0
        AND i.is_settled = 1
        AND u.class_id <> 0
        AND i.time_settled > (select max(i2.time_settled)
                              from investments i2, options_plus_investments op
                              where i2.id = op.investment_id)
        ) INV

            left join options_plus_minutes_range opm on opm.market_id = inv.market_id
                                                        AND inv.min_diff >= opm.min_minutes
                                                        AND inv.min_diff < opm.max_minutes
            left join options_plus_promile_range opr on opr.market_id = inv.market_id
                                                        AND inv.promile >= opr.min_promile
                                                        AND inv.promile < opr.max_promile
                left join  options_plus_prices opp on opp.minutes_range_id = opm.id
                                                      AND opp.promile_range_id = opr.id;

end UPDATE_OPP_PLUS_INV;


----------------------------------------------------------------------------------------------
first deposit triggers
----------------------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRG_AFTER_INS_TRANSACTION
  after insert on transactions
  for each row
declare
begin

  -- Check transaction status
  if (:new.status_id in (2,7)) then

     -- update user's first deposit id id necessary
     update users u
     set u.first_deposit_id = :new.id
     where u.id = :new.user_id
       and u.first_deposit_id is null
       and exists (select *
                   from transaction_types tt
                   where tt.class_type = 1
                     and tt.id = :new.type_id);

  end if;

end TRG_AFTER_INS_TRANSACTION;


---------------------------------------

CREATE OR REPLACE TRIGGER TRG_AFTER_UPT_TRANSACTION2
  AFTER UPDATE ON Transactions
FOR EACH ROW
declare
   PRAGMA AUTONOMOUS_TRANSACTION;
begin

  -- check if status has change from unsuccessful to successful
  if (:old.status_id not in (2,7,8) and :new.status_id in (2,7,8)) then

       -- update user's first deposit id id necesary
       update users u
       set u.first_deposit_id = :new.id
       where u.id = :new.user_id
         and u.first_deposit_id is null
         and exists (select *
                     from transaction_types tt
                     where tt.class_type = 1
                       and tt.id = :new.type_id);

   -- else check if status has change from successful to unsuccessful
   elsif (:old.status_id in (2,7,8) and :new.status_id not in (2,7,8)) then

      -- update user's first deposit id
      update users u
      set u.first_deposit_id = (select min(t.id)
                                from transactions t
                                where t.user_id = u.id
                                  and t.id != :new.id
                                  and t.type_id in (select tt.id
                                                    from transaction_types tt
                                                    where tt.class_type = 1)
                                  and t.status_id in (2,7,8))
      where u.id = :new.user_id
        and u.first_deposit_id = :new.id
        ;

  end if;

  commit;
  exception
     when OTHERS THEN
         dbms_output.put_line('no data found ');
end TRG_AFTER_UPT_TRANSACTION;

---------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER TRG_BEFORE_INS_POP_ENTRIES
  before insert on population_entries
  for each row
declare
  is_cancel_entry_display number;
begin

  -- Find user/contact country id
  begin
      select 1
      into is_cancel_entry_display
      from
           populations p,
           population_users pu
              LEFT JOIN users u on u.id = pu.user_id
              LEFT JOIN contacts c on c.id = pu.contact_id
                   LEFT JOIN countries cn on (u.country_id = cn.id OR (u.country_id is null AND c.country_id = cn.id))
      where pu.id= :new.population_users_id
        and p.id = :new.population_id
        and ((cn.is_display_pop_call_me = 0 and p.population_type_id = 6)
             or
             (cn.is_display_pop_rnd = 0 and p.population_type_id = 5)
             or
             (cn.is_display_pop_short_reg = 0 and p.population_type_id = 20)
             or
             (cn.is_display_pop_reg_attempt = 0 and p.population_type_id = 21));
  exception
    when no_data_found then
      is_cancel_entry_display := 0;
  end;

  -- Enter users from India to group 0
  if is_cancel_entry_display = 1 then
      :new.is_displayed := 0;
  end if;

end TRG_BEFORE_INS_POP_ENTRIES;


-----------------------------------------------------------------------------------
-- idan 28.7.2011 -  Add function - updated on 03.08.11

create or replace
FUNCTION "GET_OPP_ROLL_UP"
(
  OPP_ID IN NUMBER
) RETURN NUMBER AS next_opp_id NUMBER;
BEGIN
    SELECT
        *
    INTO
        next_opp_id
    FROM
 (
            SELECT
              op1.id
            FROM
              opportunities op1,
              opportunities op2
            WHERE
              op2.id=OPP_ID AND
              op2.is_settled = 0  AND
              op1.market_id =op2.market_id AND
              op1.is_settled = 0 AND
              SYS_EXTRACT_UTC( op1.time_est_closing) - 2/24 <  SYS_EXTRACT_UTC(op2 .time_est_closing)  AND
              SYS_EXTRACT_UTC( op1.time_est_closing)  > SYS_EXTRACT_UTC(op2 .time_est_closing) AND
              op1.scheduled IN(1,2)
            ORDER BY
              op1.time_est_closing
              )
         WHERE rownum = 1;

RETURN (next_opp_id);

        exception
              when  NO_DATA_FOUND then
                    return 0;

END GET_OPP_ROLL_UP;

-- Add line at stored procedure QUALIFY_INV_FOR_INSURANCES. location: after
					insurance_flag = 2
			WHERE
				id IN (
					SELECT
						A.id
					FROM
						investments A,
						opportunities B,
						markets C
					WHERE
GET_OPP_ROLL_UP(A.id)  != 0 AND

-----------------------------------------------------------------------------------
-- idan 19.1.2012 -  Add another condition to stored procedure QUALIFY_INV_FOR_INSURANCES in 2 places:
-- 						lines 55 + 87 (Inside the where of 2 if's)
B.is_disabled = 0 AND

-------------------------------------------------------------------------------------

------------ oshik 26.12.2012 add function to convert amount to euro in trnasaction

create or replace
FUNCTION CONVERT_AMOUNT_TO_EUR
(  p_amount IN NUMBER  , p_currency_id IN NUMBER  , p_time IN DATE  ) RETURN NUMBER AS
	amount NUMBER;
	level NUMBER;
  market_id number := 16;
  BEGIN
  amount := p_amount;
  IF P_CURRENCY_ID != 3 THEN
    amount := convert_amount_to_usd( p_amount , p_currency_id , p_time );
    level := get_last_closing_level(market_id, p_time);
    amount := amount/level;
  END IF;
RETURN (amount) ;
END;

---------------------------------------------------------------------------------------

--Ofer 26.02.2012

create or replace
PROCEDURE DUPLICATED_CONTACTS_HANDLER AS

count_dup_contacts_insert number := 0;
count_dup_contacts_update number := 0;
last_dup_contacts_handler_run date := null;
dup_id number := null;

BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('DUPLICATED_CONTACTS_HANDLER started at: ' || sysdate);

FOR  v_dup IN (
                             SELECT  a.id AS contact_id, a.email, listagg(b.id,',') WITHIN GROUP (ORDER BY b.ID) AS list_of_ids
                             FROM    contacts a, contacts b
                             WHERE   a.email = b.email
                             AND a.id != b.id
                             AND a.class_id <> 0
                             AND a.user_id = 0
                             AND b.user_id = 0
                             AND b.class_id <> 0
                             AND a.email NOT LIKE '%asaf%@etrader.co.il'
                             GROUP BY a.id, a.email
                              )
                              LOOP

                                      SELECT max(dc.contact_id) into dup_id
                                      FROM duplicates_contacts dc
                                      WHERE dc.contact_id = v_dup.contact_id;

                                      IF(dup_id is not null) THEN
                                              UPDATE duplicates_contacts
                                              SET dup_contact_id = v_dup.list_of_ids,
                                                      time_updated = sysdate
                                              WHERE contact_id = dup_id;
                                              count_dup_contacts_update := count_dup_contacts_update +1;
                                       ELSE
                                               INSERT INTO DUPLICATES_CONTACTS(ID,CONTACT_ID,DUP_CONTACT_ID,TIME_CREATED,TIME_UPDATED)
                                               VALUES (SEQ_DUPLICATED_CONTACTS.nextval,v_dup.contact_id, v_dup.list_of_ids,sysdate, sysdate);
                                               count_dup_contacts_insert := count_dup_contacts_insert +1;
                                       END IF;
                              END LOOP;



DBMS_OUTPUT.PUT_LINE('DB_PARAMETERS updated with: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('count_dup_contacts inserted= ' || count_dup_contacts_insert);
DBMS_OUTPUT.PUT_LINE('count_dup_contacts updated= ' || count_dup_contacts_update);
  DBMS_OUTPUT.PUT_LINE('DUPLICATED_CONTACTS_HANDLER finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END DUPLICATED_CONTACTS_HANDLER;

----------------------------------------------------------------------------------

--OFER 02/09/2012 new sales 
-- population handler


create or replace
PROCEDURE "POPULATIONS_HANDLER" (runType NUMBER) AS

  -- Run Types
  RUN_TYPE_ALL_USERS NUMBER := 1;
  RUN_TYPE_NEW_USERS NUMBER := 2;
  RUN_TYPE_ONLY_CONTACTS NUMBER := 3;

	v_pe population_entries%ROWTYPE;
  v_pu population_users%ROWTYPE;
	v_peh population_entries_hist%ROWTYPE;
  populationEntHist population_entries_hist%ROWTYPE;

  populationEntryId NUMBER;
  qualifiedPopualtionId NUMBER;
  currentPopulationId NUMBER;
  currentPopulationPriority NUMBER;

  popQualificationTime DATE;
  popBaseQualificationTime DATE;
  popBaseQualificationTimeOld DATE;
   isExistDormentConvertionPop DATE;

  newPopualtionThreshold NUMBER;
  qualificationStatus NUMBER;
  assignedWriterId NUMBER;

  users_popualtion_users_id NUMBER;
  lastSalesDepositTime DATE;

  v_counter NUMBER;
  isExist NUMBER;
  isExistSuccessfullDeposit NUMBER;
  isExistTranIssue NUMBER;
  isExistOpenWithdraw NUMBER;
  isFirstMinimumBalanceCheck NUMBER;
  userNumOfSuccesDeposits NUMBER;
  userAmountOfSuccesDeposits NUMBER;
  isCheckQualificationTime NUMBER;

  count_new_qualified_users number := 0;
  count_new_qualified_contacts number := 0;
  runTypeName varchar2(30);
BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER started at: ' || sysdate);

  CASE
    WHEN runType = RUN_TYPE_ALL_USERS THEN
      runTypeName := 'Run Type All Users';
    WHEN runType = RUN_TYPE_NEW_USERS THEN
      runTypeName := 'Run Type New Users';
    WHEN runType = RUN_TYPE_ONLY_CONTACTS THEN
      runTypeName := 'Run Type Only Contacts';
    ELSE
      DBMS_OUTPUT.PUT_LINE('run type: ' || runType || ' doesnt exist');
  END CASE;

  DBMS_OUTPUT.PUT_LINE('run type: ' || runTypeName);

  IF (runType != RUN_TYPE_ONLY_CONTACTS) THEN
    -- RUN MARKETING_HANDLER
    marketing_handler();

    FOR v_usr IN (
      SELECT
        u.id user_id,
        u.skin_id,
        u.balance,
        u.time_created,
        u.currency_id,
        u.is_declined,
        u.is_contact_for_daa,
        u.last_decline_date,
        u.time_last_login,
        u.contact_id,
        u.time_sc_house_result , 
        u.first_deposit_id ,
        p.id population_id,
        p.population_type_id curr_pop_type_id,
        pu.id population_user_id,
        pu.curr_population_entry_id,
        pu.curr_assigned_writer_id,
        pu.entry_type_id,
        pu.entry_type_action_id,
        pt.priority,
        ilgc.value min_investment_limit,
        s.other_reactions_days
      FROM
        investment_limits il,
        investment_limit_group_curr ilgc,
        skins s,
        users u LEFT JOIN population_users pu on u.id = pu.user_id
                        LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                            LEFT JOIN populations p on pe.population_id = p.id
                                  LEFT JOIN population_types pt on p.population_type_id = pt.id
      WHERE
        u.currency_id = ilgc.currency_id
        AND il.min_limit_group_id = ilgc.investment_limit_group_id
        AND il.id = 1 -- Default binary limits id
        AND u.skin_id = s.id
        AND u.class_id !=0
        AND u.is_active = 1
        AND u.is_false_account = 0
        AND u.is_contact_by_phone = 1
        AND u.id NOT in (select USER_ID from frauds)
        AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
        AND pu.lock_history_id is null
        AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
        AND u.is_vip = 0
        --AND u.time_sc_house_result is null
        AND (runType = RUN_TYPE_ALL_USERS or u.time_created > sysdate - 1)
        --and u.id = 134201
   
    ) LOOP

        -- DBMS_OUTPUT.PUT_LINE('user12 : ' || v_usr.);
          v_peh := null;
          populationEntryId := 0;
          qualifiedPopualtionId := 0;
          newPopualtionThreshold := 0;
          isFirstMinimumBalanceCheck := 1;
          assignedWriterId := v_usr.curr_assigned_writer_id;
          users_popualtion_users_id := v_usr.population_user_id ;
          lastSalesDepositTime := null;

          IF (v_usr.population_id is null) THEN
                currentPopulationId := 0;
                currentPopulationPriority := 999999; -- Lowest proirity
          ELSE
                currentPopulationId := v_usr.population_id;
                currentPopulationPriority := v_usr.priority;
          END IF;

          FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable,
                                    pt.priority
                              FROM
                                    populations p,
                                    
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND  (pt.priority < currentPopulationPriority OR currentPopulationPriority =10) -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_usr.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = runType
                              ORDER BY pt.priority -- Highest priority is 1
          ) LOOP

              populationEntHist := null;
              popQualificationTime := null;
              popBaseQualificationTime := null;
              isCheckQualificationTime := 0;

              CASE
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 1 THEN --POPULATION_FIRST_DEPO_FAILED
              --    DBMS_OUTPUT.PUT_LINE('user : ' || v_usr.user_id);
                         IF (v_usr.is_declined = 1) THEN

                              -- Check whether this user had some successed deposits in the past
                              select
                                 max(t.id)
                              into
                                 isExistSuccessfullDeposit
                              from
                                 transactions t,
                                 transaction_types tt
                              where
                                 tt.class_type = 1 AND
                                 user_id = v_usr.user_id AND
                                 type_id = tt.id AND
                                 status_id in (2,7,8);

                              IF (isExistSuccessfullDeposit is null) THEN

                                    popBaseQualificationTime  := v_usr.last_decline_date;
                                    popQualificationTime := popBaseQualificationTime;

                                     isCheckQualificationTime := 1;

                              END IF;
                           END IF; -- IF (v_usr.is_declined = 1) THEN

                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 2 THEN -- POPULATION_LAST_DEPO_FAILED

                        -- If users doesn't want to ne called for decline after approve we don't insert him into DDA population.
                        IF (v_usr.is_contact_for_daa != 0 AND v_usr.is_declined = 1) THEN

                              popBaseQualificationTime  := v_usr.last_decline_date;
                              popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                              IF (sysdate  >= popQualificationTime) THEN

                                        -- Check whether this user had some successed deposits in the past
                                        IF (isExistSuccessfullDeposit is not null) THEN

                                               isCheckQualificationTime := 1;

                                        END IF; -- IF (isExistSuccessfullDeposit is not null) THEN
                              END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 5 THEN -- POPULATION_NO_DEPOSIT_FILL

                        popBaseQualificationTime := v_usr.time_created;
                        popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;
                                 
                        IF (sysdate  >= popQualificationTime) THEN
                        
                              select
                                max(t.id)
                              into
                                isExist
                              from
                                transactions t,
                                transaction_types tt
                              where
                                tt.class_type = 1 AND
                                user_id = v_usr.user_id AND
                                type_id = tt.id;
                              
                                IF (isExist is null) THEN
                                       isCheckQualificationTime := 1;

                                END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 9 THEN -- POPULATION_DORMANT_ACCOUNT

                        IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                                -- IF we didn't find a sales deposit for that user before, search it.
                                IF (lastSalesDepositTime is null) THEN

                                      select
                                            max(t.time_created)
                                       into
                                            lastSalesDepositTime
                                       from
                                            transactions t,
                                            transactions_issues ti
                                      where
                                            t.id = ti.transaction_id
                                            and t.user_id = v_usr.user_id;
                                END IF; --  IF (lastSalesDepositTime is null) THEN

                                -- If user had no sales deposit in the past or v_population.LAST_SALES_DEPOSIT_DAYS has passed since last sales deposit time... go on...
                                IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN

                                      popBaseQualificationTime := GET_LAST_INVEST_TIME(v_usr.user_id);

                                      IF (popBaseQualificationTime is not null) THEN

                                           -- If v_population.number_of_days has passed since last user login time
                                           -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                                           IF (v_usr.time_last_login < sysdate - v_population.last_invest_day
                                                      OR
                                                 popBaseQualificationTime < sysdate - v_population.max_last_login_time_days) THEN

                                                    popQualificationTime := popBaseQualificationTime + v_population.last_invest_day;

                                                    IF (sysdate  >= popQualificationTime) THEN

                                                           -- Check if user was above min balance during the all threshold time and didn't had a balance increase
                                                           IF (get_is_dormant_above_balance(v_usr.user_id,
                                                                                                                  v_population.last_invest_day,
                                                                                                                  v_usr.min_investment_limit,
                                                                                                                  v_usr.balance) = 1) THEN

                                                                 isCheckQualificationTime := 1;
                                                            END IF;

                                                    END IF; -- IF (sysdate  >= popQualificationTime) THEN
                                              END IF; -- IF (v_usr.time_last_login < sysdate - v_population.number_of_days...
                                      END IF; -- IF (popBaseQualificationTime is not null) THEN
                                END IF; -- IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN
                        END IF; -- IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 10 THEN -- POPULATION_NO_INVESTMENTS

                          popBaseQualificationTime := GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.user_id);

                          IF (popBaseQualificationTime is not null) THEN

                                popQualificationTime := popBaseQualificationTime + v_population.number_of_days;

                                IF(sysdate  >  popQualificationTime) THEN

                                      select   max(i.id)
                                      into      isExist
                                      from     investments i
                                      where   v_usr.user_id = i.user_id;

                                      IF (isExist is null) THEN

                                            isCheckQualificationTime := 1;

                                      END IF;
                                END IF;
                          END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------    
                    WHEN v_population.population_type_id = 22 THEN -- POPULATION OPEN WITHDRAW
                              SELECT count(t.id) into isExistOpenWithdraw
                               FROM transactions t, transaction_types tt 
                               WHERE t.user_id = v_usr.user_id
                              AND tt.id = t.type_id 
                              AND tt.class_type = 2
                              AND t.status_id = 4;
                              
                              IF (isExistOpenWithdraw > 0) THEN 
                                      isCheckQualificationTime := 1;
                                      popQualificationTime := sysdate;
                               END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 24 THEN -- POPULATION_DORMENT_CONVERSION

                              IF (v_usr.curr_pop_type_id = 23) THEN
                              
                                      SELECT tab.qualification_time into isExistDormentConvertionPop
                                      FROM (
                                      SELECT pe.*
                                      FROM population_users pu, population_entries pe, 
                                      population_entries_hist peh, 
                                      populations p, 
                                      population_entries_hist_status pehs
                                      WHERE pu.id = pe.population_users_id
                                      AND pe.id = peh.population_entry_id
                                      AND pu.user_id = v_usr.user_id
                                      AND p.id = pe.population_id
                                      AND p.population_type_id = 23
                                      ORDER BY pe.qualification_time) tab
                                      WHERE rownum = 1;

                                      IF(isExistDormentConvertionPop is not null AND isExistDormentConvertionPop < sysdate - v_population.NUMBER_OF_DAYS) THEN               

                                            IF (v_usr.curr_assigned_writer_id is null) THEN

                                                    SELECT count(ti.TRANSACTION_ID) into isExistTranIssue
                                                    FROM users u, transactions t, transactions_issues ti
                                                    where u.id = v_usr.user_id
                                                    and u.id = t.user_id
                                                    and t.id = ti.transaction_id
                                                    and ti.time_settled is null;
                                                    
                                                    IF(isExistTranIssue > 0) THEN
                                                            isCheckQualificationTime := 1;
                                                            popQualificationTime := sysdate;
                                                    END IF;
                                            END IF;
                                      END IF;                                    
                              END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    ELSE  --IF  v_population.population_type_id wasn't found
                        -- No handler for this population type
                      DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
                END CASE;
                
                -- IF isCheckQualificationTime = 1, User is qualified to that populaiton, now check his qualification time
                IF (isCheckQualificationTime = 1) THEN

                      populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                      IF (populationEntHist.status_id is null
                                 OR
                            popBaseQualificationTime >= populationEntHist.TIME_CREATED
                                 OR
                           (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                                OR
                           (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                          -- Find qualification time of this population last entry
                          BEGIN
                              select base_qualification_time
                              into popBaseQualificationTimeOld
                              from population_entries pe
                              where pe.id = populationEntHist.population_entry_id
                                  and qualification_time is not null ;
                        EXCEPTION
                              WHEN no_data_found THEN
                                      popBaseQualificationTimeOld := null;
                           END;

                          -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                          IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                                 populationEntryId :=  populationEntHist.population_entry_id;
                          END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                          -- Update user entry with this population id.
                          qualifiedPopualtionId := v_population.id;

                           -- Keep current popualtion threshold
                          newPopualtionThreshold := v_population.threshold;

                           -- If user was finally found qualified for current popualtion, break the loop
                          GOTO end_loop;

                      END IF; -- IF (populationEntHist.status_id is null ....
                END IF; --   IF (isCheckQualificationTime = 1) THEN

          END LOOP;
           <<end_loop>>

          -- If got a newPopualtionId, insert user to new population.
          IF (qualifiedPopualtionId != 0) THEN
                count_new_qualified_users := count_new_qualified_users +1;

                -- If this  is a contact.
                IF (v_usr.user_id = 0) THEN
                   assignedWriterId := null;
                END IF;

                -- If user is already in a population, remove it first.
                IF (v_usr.curr_population_entry_id is not null) THEN
                      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                      v_peh.population_entry_id := v_usr.curr_population_entry_id;
                      v_peh.status_id := 105;
                      v_peh.writer_id := 0;
                      v_peh.assigned_writer_id := assignedWriterId;
                      v_peh.time_created := sysdate;
                      v_peh.issue_action_id := null;
                      INSERT INTO population_entries_hist VALUES v_peh;
                END IF; -- IF (v_usr.curr_population_entry_id is not null) THEN

                 -- If user doesn't have an entry in population_users, create one.
                IF (users_popualtion_users_id is null) THEN
                      SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                      v_pu.id := users_popualtion_users_id;
                      v_pu.user_id := v_usr.user_id;
                      if (v_usr.contact_id != 0) then
                        v_pu.contact_id := v_usr.contact_id;
                      else
                        v_pu.contact_id := null;
                      end if;
                      v_pu.curr_assigned_writer_id := null;
                      v_pu.curr_population_entry_id := null;
                      v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                      v_pu.entry_type_action_id := null;
                      v_pu.lock_history_id := null;
  --                    DBMS_OUTPUT.PUT_LINE('user_id: ' || v_usr.user_id);
                      INSERT INTO population_users VALUES v_pu;
                END IF; --  IF (users_popualtion_users_id is null) THEN

                -- If population entry id hasn't been changed create a new one.
                IF (populationEntryId = 0) THEN
                      -- Count the number of users in the new population in order to determine user's entry group.
                      SELECT
                              COUNT(pe.id) INTO v_counter
                      FROM
                              population_entries pe,
                              population_users pu 
                      WHERE
                              pe.id = pu.curr_population_entry_id AND
                              pe.population_id = qualifiedPopualtionId;

                      -- Insert a new population_entry
                      SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                      v_pe.id := populationEntryId;
                      v_pe.population_id := qualifiedPopualtionId;
                      v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                      v_pe.population_users_id := users_popualtion_users_id;
                      v_pe.qualification_time := popQualificationTime ;
                      v_pe.base_qualification_time := popBaseQualificationTime ;
                      v_pe.is_displayed := 1;
                      INSERT INTO population_entries VALUES v_pe;

                      qualificationStatus := 1; -- Qualification
                ELSE
                      qualificationStatus := 7; -- reQualification
                END IF; --  IF (populationEntryId = 0) THEN


                 -- Insert a new population_entry_history
                SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                v_peh.population_entry_id :=populationEntryId;
                v_peh.status_id := qualificationStatus;
                v_peh.writer_id := 0;
                v_peh.assigned_writer_id := assignedWriterId;
                v_peh.time_created := sysdate;
                v_peh.issue_action_id := null;
                INSERT INTO population_entries_hist VALUES v_peh;

                -- Update user entry in population_users table
                UPDATE
                      population_users pu
                SET
                      pu.curr_population_entry_id = populationEntryId,
                      pu.curr_assigned_writer_id = assignedWriterId
                WHERE
                     pu.user_id = v_usr.user_id;

          END IF; -- IF (qualifiedPopualtionId != 0) THEN

         COMMIT;

    END LOOP;

    if (runType = RUN_TYPE_ALL_USERS) then
          update db_parameters dp
          set date_value = sysdate
          where dp.id = 9;
    end if;

    -- RUN DELAY HANDLER
    delay_handler();

    -- RUN TRACKING HANDLER
    tracking_handler();

      -- RUN SPECIAL HANDLER
    special_handler();

    -- RUN CALLBACK HANDLER
    callback_handler();
    
 IF (runType = RUN_TYPE_ALL_USERS) THEN   
    -- RUN DORMENT CONVERSION HANDLER
    dorment_conversion_handler();

    -- RUN USERS STATUS HANDLER
    users_status_handler();

    -- RUN USERS RANK HANDLER    
    users_rank_handler();
    
    --RUN RETURN FROM TRAKING HANDLER
    return_from_tracking();
END IF;
    DBMS_OUTPUT.PUT_LINE('count_new_qualified_users  = ' || count_new_qualified_users);

  END IF;
       
  FOR v_con IN (
    SELECT
      c.id contact_id,
      c.skin_id,
      c.time_created,
      c.type,
      p.id population_id,
      p.population_type_id curr_pop_type_id,
      pu.id population_user_id,
      pu.curr_population_entry_id,
      pu.curr_assigned_writer_id,
      pu.entry_type_id,
      pu.entry_type_action_id,
      pt.priority,
      s.other_reactions_days
    FROM
      skins s,
      contacts c
          LEFT JOIN population_users pu on c.id = pu.contact_id
              LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                  LEFT JOIN populations p on pe.population_id = p.id
                        LEFT JOIN population_types pt on p.population_type_id = pt.id
    WHERE
      c.skin_id = s.id
      AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
      AND pu.lock_history_id is null
      AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
      AND c.type in (5, 6) -- Short Reg Form contacts and register attempt
      AND (c.land_line_phone is not null OR c.mobile_phone is not null)    
      AND c.user_id = 0
  ) LOOP

        v_peh := null;
        populationEntryId := 0;
        qualifiedPopualtionId := 0;
        newPopualtionThreshold := 0;
        assignedWriterId := v_con.curr_assigned_writer_id;
        users_popualtion_users_id := v_con.population_user_id ;

        IF (v_con.population_id is null) THEN
              currentPopulationId := 0;
              currentPopulationPriority := 999999; -- Lowest proirity
        ELSE
              currentPopulationId := v_con.population_id;
              currentPopulationPriority := v_con.priority;
        END IF;

        FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable
                              FROM
                                    populations p,
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND pt.priority < currentPopulationPriority -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_con.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = RUN_TYPE_ONLY_CONTACTS
                              ORDER BY pt.priority -- Highest priority is 1
        ) LOOP

            populationEntHist := null;
            popQualificationTime := null;
            popBaseQualificationTime := null;
            isCheckQualificationTime := 0;

            CASE
                ------------------------------------------------------------------------------------------------------------------------------------------------
                WHEN v_population.population_type_id in (20, 21) THEN --POPULATION_SHORT_REG_FORM AND POPLULATION_REGISTER_ATTEMPT
                   
                    popBaseQualificationTime := v_con.time_created;
                      
                    popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;
                    
                    if (popQualificationTime < sysdate) then
                   
                       isCheckQualificationTime := 1;
                    end if;
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  ELSE  --IF  v_population.population_type_id wasn't found
                      -- No handler for this population type
                    DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
             END CASE;

              -- IF isCheckQualificationTime = 1, contact is qualified to that populaiton, now check his qualification time
              IF (isCheckQualificationTime = 1) THEN

                    populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                    IF (populationEntHist.status_id is null
                               OR
                          popBaseQualificationTime >= populationEntHist.TIME_CREATED
                               OR
                         (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                               OR
                         (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                        -- Find qualification time of this population last entry
                        BEGIN
                            select base_qualification_time
                            into popBaseQualificationTimeOld
                            from population_entries pe
                            where pe.id = populationEntHist.population_entry_id
                                and qualification_time is not null ;
                        EXCEPTION
                            WHEN no_data_found THEN
                                    popBaseQualificationTimeOld := null;
                        END;

                        -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                        IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                               populationEntryId :=  populationEntHist.population_entry_id;
                        END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
              
                        if( ( v_con.type = 5 AND v_population.population_type_id = 20 ) OR   ( v_con.type = 6  AND v_population.population_type_id = 21 )) then  
                        -- Update contact entry with this population id.
                              qualifiedPopualtionId := v_population.id;
                              -- Keep current popualtion threshold
                              newPopualtionThreshold := v_population.threshold;
                              -- If contact was finally found qualified for current popualtion, break the loop
                              GOTO end_loop;
                        end if;
                    END IF; -- IF (populationEntHist.status_id is null ....
              END IF; --   IF (isCheckQualificationTime = 1) THEN

        END LOOP;
         <<end_loop>>

        -- If got a newPopualtionId, insert contact to new population.
        IF (qualifiedPopualtionId != 0) THEN

              count_new_qualified_contacts := count_new_qualified_contacts +1;

              -- If this contact is in general entry_type preform auto cancel assign.
              IF (v_con.entry_type_id = 1) THEN
                 assignedWriterId := null;
              END IF;

              -- If contact is already in a population, remove it first.
              IF (v_con.curr_population_entry_id is not null) THEN
                    SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                    v_peh.population_entry_id := v_con.curr_population_entry_id;
                    v_peh.status_id := 105;
                    v_peh.writer_id := 0;
                    v_peh.assigned_writer_id := assignedWriterId;
                    v_peh.time_created := sysdate;
                    v_peh.issue_action_id := null;
                    INSERT INTO population_entries_hist VALUES v_peh;
              END IF; -- IF (v_con.curr_population_entry_id is not null) THEN

               -- If contact doesn't have an entry in population_users, create one.
              IF (users_popualtion_users_id is null) THEN
                    SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                    v_pu.id := users_popualtion_users_id;
                    v_pu.user_id := null;
                    v_pu.contact_id := v_con.contact_id;
                    v_pu.curr_assigned_writer_id := null;
                    v_pu.curr_population_entry_id := null;
                    v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                    v_pu.entry_type_action_id := null;
                    v_pu.lock_history_id := null;
                    v_pu.time_created := sysdate;
--                    DBMS_OUTPUT.PUT_LINE('contact_id: ' || v_con.contact_id);
                    INSERT INTO population_users VALUES v_pu;
              END IF; --  IF (users_popualtion_users_id is null) THEN

              -- If population entry id hasn't been changed create a new one.
              IF (populationEntryId = 0) THEN
                    -- Count the number of users in the new population in order to determine user's entry group.
                    SELECT
                            COUNT(pe.id) INTO v_counter
                    FROM
                            population_entries pe,
                            population_users pu
                    WHERE
                            pe.id = pu.curr_population_entry_id AND
                            pe.population_id = qualifiedPopualtionId;

                    -- Insert a new population_entry
                    SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                    v_pe.id := populationEntryId;
                    v_pe.population_id := qualifiedPopualtionId;
                    v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                    v_pe.population_users_id := users_popualtion_users_id;
                    v_pe.qualification_time := popQualificationTime ;
                    v_pe.base_qualification_time := popBaseQualificationTime ;
                    v_pe.is_displayed := 1;
                    INSERT INTO population_entries VALUES v_pe;

                    qualificationStatus := 1; -- Qualification
              ELSE
                    qualificationStatus := 7; -- reQualification
              END IF; --  IF (populationEntryId = 0) THEN


               -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              v_peh.population_entry_id :=populationEntryId;
              v_peh.status_id := qualificationStatus;
              v_peh.writer_id := 0;
              v_peh.assigned_writer_id := assignedWriterId;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := null;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update contact entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.curr_population_entry_id = populationEntryId,
                    pu.curr_assigned_writer_id = assignedWriterId
              WHERE
                   pu.contact_id = v_con.contact_id;

        END IF; -- IF (qualifiedPopualtionId != 0) THEN

       COMMIT;

  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('count_new_qualified_conacts  = ' || count_new_qualified_contacts);

  DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');

  NULL;
END;

----------------------------------------------------------------------------------

--OFER 02/09/2012 new sales 
--dorment conversion handler

create or replace
PROCEDURE DORMENT_CONVERSION_HANDLER AS 

POP_DORMENT_CONVERSION_ID NUMBER := 24;
POP_RETENTION_ID NUMBER := 23;
populationEntryId  NUMBER :=0;
populationId NUMBER :=0;
populationUserId NUMBER :=0;
populationEntryHistId NUMBER :=0;

users_out_of_dorment NUMBER :=0;

  
  
BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('DORMENT_CONVERSION_HANDLER started at: ' || sysdate);

FOR  v_user IN (
                                  SELECT 
                                                    u.id as user_id,
                                                    u.skin_id as user_skin_id,
                                                    u.balance as user_balance,
                                                    ilgc.value as limit_value,
                                                    pu.id as population_user_id,
                                                    pe.qualification_time as entry_qualification_time
                                  FROM 
                                                    population_users pu, 
                                                    population_entries pe, 
                                                    populations p, 
                                                    users u,
                                                    investment_limit_group_curr ilgc
                                   WHERE 
                                                    pu.user_id = u.id and
                                                    pu.curr_population_entry_id = pe.id and
                                                    pe.population_id = p.id and
                                                    p.population_type_id = POP_DORMENT_CONVERSION_ID and
                                                    ilgc.currency_id = u.currency_id and
                                                    ilgc.investment_limit_group_id  = 1      
                                  )
                                  LOOP
                                          IF (v_user.user_balance < v_user.limit_value OR v_user.entry_qualification_time < sysdate - 7) THEN

                                                              SELECT 
                                                               p.id INTO populationId
                                                              FROM  
                                                               populations p,  
                                                               population_types pt  
                                                              WHERE  
                                                               pt.id = p.population_type_id 
                                                               AND p.skin_id = v_user.user_skin_id
                                                               AND p.population_type_id = POP_RETENTION_ID;           
                                                               
                                                               SELECT pu.id INTO populationUserId
                                                               FROM population_users pu
                                                               where pu.user_id = v_user.user_id;
                                                               
                                                               IF (populationUserId > 0 AND populationId > 0) THEN 
                                                               
                                                                       SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                                                                      
                                                                       INSERT INTO population_entries(id, population_id, group_id, qualification_time, population_users_id, is_displayed, base_qualification_time) 
                                                                       VALUES (populationEntryId, populationId, 1, sysdate, populationUserId,1,sysdate);                                                                                                                                         
                                                                       
                                                                       SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO populationEntryHistId
                                                                       FROM dual;

                                                                       INSERT INTO population_entries_hist(id, population_entry_id, status_id, issue_action_id,  writer_id, time_created, assigned_writer_id) 
                                                                       VALUES (populationEntryHistId, populationEntryId, 1, null , 0 ,sysdate, 0);
                                                                       
                                                                      UPDATE 
                                                                                        population_users pu 
                                                                       SET 
                                                                                       entry_type_id = 1, 
                                                                                       curr_population_entry_id = populationEntryId, 
                                                                                       curr_assigned_writer_id = 0
                                                                       WHERE 
                                                                                      pu.id = populationUserId;
                                                                       
                                                                       users_out_of_dorment := users_out_of_dorment +1;

                                                                END IF;
                                          END IF;
                                  END LOOP;
                                  
DBMS_OUTPUT.PUT_LINE('DORMENT_CONVERSION_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');  
DBMS_OUTPUT.PUT_LINE('users_out_of_dorment: ' || users_out_of_dorment);
  NULL;
END DORMENT_CONVERSION_HANDLER;

----------------------------------------------------------------------------------

--OFER 02/09/2012 new sales 
--User rank handler

create or replace
PROCEDURE USERS_RANK_HANDLER AS 

RANK_NEWBIES NUMBER := 1;
transation_id number:=0;
transaction_time DATE := null;
user_rank_id number :=1;
num_of_days number := 0;
users_updated number := 0;
--This handler takes all the newbies and check if 7 days has passed since first success deposit insert to history.
BEGIN

select ur.min_num_of_days_in_rank into num_of_days
from users_rank ur
where ur.id = RANK_NEWBIES;

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('USERS_RANK_HANDLER started at: ' || sysdate);
        FOR v_usr IN (
                                        select u.id as user_id, u.first_deposit_id as fdi
                                        from users u
                                        where u.rank_id = RANK_NEWBIES
                                        )
                                        LOOP

                                                select t.time_created into transaction_time
                                                from transactions t
                                                where t.id = v_usr.fdi;
                                                
                                                if (transaction_time < sysdate - num_of_days) THEN      

                                                       select tran_id  into transation_id
                                                       from (
                                                      select t. id as tran_id
                                                      from transactions t , transaction_types tt
                                                      where t.type_id = tt.id
                                                      and tt.class_type = 1
                                                      and t.status_id in (2,7,8)
                                                      and t.user_id = v_usr.user_id
                                                      order by t.time_created desc)
                                                      where rownum = 1;
                                                      
                                                      SELECT  
                                                                   ur.id into user_rank_id  
                                                       FROM (  
                                                             SELECT   
                                                                   sum((t.amount/100) * t.rate) as userAmount   
                                                             FROM  
                                                                   transactions t, transaction_types tt  
                                                             WHERE   
                                                                   t.user_id = v_usr.user_id 
                                                                   AND t.type_id = tt.id   
                                                                   AND tt.class_type = 1  
                                                                   AND t.status_id in ( 2,8,7 )  
                                                             ) tab, users_rank ur  
                                                       WHERE  
                                                          (tab.userAmount > ur.min_sum_of_deposits AND tab.userAmount <= ur.max_sum_of_deposits)  
                                                             OR (tab.userAmount > ur.min_sum_of_deposits AND ur.max_sum_of_deposits is null)  ;
                                                      
                                                      UPDATE users
                                                      SET rank_id = user_rank_id
                                                      WHERE id = v_usr.user_id;
                                                      
                                                      INSERT INTO users_rank_hist (ID, USER_ID, USER_RANK_ID, QUALIFICATION_DATE, transaction_id)
                                                      VALUES (SEQ_USERS_RANK_HIST.nextval, v_usr.user_id, user_rank_id, sysdate, transation_id);
                                                      
                                                      users_updated := users_updated + 1;
                                                END IF;
                                        END LOOP;
  DBMS_OUTPUT.PUT_LINE('users updated : ' || users_updated);                                      
DBMS_OUTPUT.PUT_LINE('USERS_RANK_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END USERS_RANK_HANDLER;


----------------------------------------------------------------------------------

--OFER 02/09/2012 new sales 
--user status handler
create or replace
PROCEDURE USERS_STATUS_HANDLER AS 

  -- Status
  ACTIVE NUMBER := 1;
  SLEEP NUMBER := 2;
  COMMA NUMBER := 3;

count_active_users_update number := 0;
count_sleep_users_update number := 0;
count_comma_users_update number := 0;
count_dep_no_inv_24h_update number := 0;

status_active_min_num_of_days number := 0;
status_active_max_num_of_days number := 0;
status_sleep_min_num_of_days number := 0;
status_sleep_max_num_of_days number := 0;
status_comma_min_num_of_days number := 0;
status_comma_max_num_of_days number := 0;

v_us_active users_status%ROWTYPE;
v_us_sleep users_status%ROWTYPE;
v_us_comma users_status%ROWTYPE;
last_action DATE :=null;
last_dep DATE :=null;
last_inv DATE :=null;
firstDepositTime DATE :=null;

BEGIN

SELECT us.* INTO v_us_active
FROM users_status us
WHERE us.id =ACTIVE ;

SELECT us.* INTO v_us_sleep
FROM users_status us
WHERE us.id =SLEEP ;

SELECT us.* INTO v_us_comma
FROM users_status us
WHERE us.id =COMMA ;

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('USERS_STATUS_HANDLER started at: ' || sysdate);

FOR v_user IN (
                                    SELECT u.id as user_id,
                                                      u.status_id as user_status_id,
                                                      u.first_deposit_id as first_deposit_id,
                                                      u.rank_id as user_rank_id
                                    FROM users u
                                    WHERE u.class_id <> 0 AND
                                                    u.is_active = 1  
                                    )
                                    LOOP
                                    
                                            SELECT max(t.time_created) INTO last_dep
                                            FROM transactions t, transaction_types tt
                                            WHERE t.user_id =v_user.user_id  AND
                                                            t.type_id = tt.id AND
                                                            tt.class_type = 1 ;
        
                                            SELECT max(i.time_created) INTO last_inv
                                            FROM investments i
                                            WHERE i.user_id = v_user.user_id AND
                                                            i.is_canceled = 0;
                                            
                                            IF (last_dep is not null AND last_inv is not null) THEN        
                                                    IF (last_dep > last_inv) THEN 
                                                            last_action := last_dep;
                                                    ELSE 
                                                            last_action := last_inv;
                                                    END IF;
                                            ELSE 
                                                    IF (last_dep is null AND last_inv is not null) THEN
                                                            last_action := last_inv;
                                                    ELSE 
                                                            last_action := last_dep;
                                                    END IF;
                                            END IF;
                                            
                                            IF (last_action is null OR last_action < sysdate - v_us_comma.min_num_of_days) THEN
                                                    IF (v_user.user_status_id <> COMMA) THEN                                                            
                                                            count_comma_users_update := count_comma_users_update + 1;
                                                            
                                                            UPDATE users
                                                            SET status_id = COMMA
                                                            WHERE id = v_user.user_id;
                                                            
                                                            INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
                                                            VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, COMMA, sysdate);                                                       
                                                    END IF;
                                            ELSE
                                                    IF (last_action <= sysdate - v_us_sleep.min_num_of_days and last_action > sysdate - v_us_sleep.max_num_of_days) THEN
                                                            IF (v_user.user_status_id <> SLEEP) THEN
                                                                    count_sleep_users_update := count_sleep_users_update + 1;
                                                                    
                                                                    UPDATE users
                                                                    SET status_id = SLEEP
                                                                     WHERE id = v_user.user_id;
                                                                     
                                                                     INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
                                                                      VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, SLEEP, sysdate);
                                                            END IF;
                                                    ELSE 
                                                            IF (last_action <= sysdate -  v_us_active.min_num_of_days)  THEN
                                                                    IF (v_user.user_status_id <> ACTIVE) THEN
                                                                          count_active_users_update := count_active_users_update + 1;
                                                                          
                                                                          UPDATE users
                                                                          SET status_id = ACTIVE
                                                                          WHERE id = v_user.user_id;
                                                                          
                                                                          INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
                                                                          VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, ACTIVE, sysdate);
                                                                   END IF;
                                                            END IF;
                                                    END IF;
                                            END IF;
                                            last_action  := null;
                                            IF (last_action is null) THEN
                                                    IF (last_dep < last_inv - 1) THEN 
                                                            UPDATE users
                                                            SET DEP_NO_INV_24H = 1
                                                            WHERE id = v_user.user_id;
                                                            
                                                            count_dep_no_inv_24h_update := count_dep_no_inv_24h_update + 1;
                                                    END IF;
                                            END IF;                                   
                              END LOOP;                                      
DBMS_OUTPUT.PUT_LINE('update users to active= ' || count_active_users_update);                                    
DBMS_OUTPUT.PUT_LINE('update users to sleep= ' || count_sleep_users_update);                                            
DBMS_OUTPUT.PUT_LINE('update users to comma= ' || count_comma_users_update);
DBMS_OUTPUT.PUT_LINE('update users dep_no_inv_24h= ' || count_dep_no_inv_24h_update);
DBMS_OUTPUT.PUT_LINE('USERS_STATUS_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END USERS_STATUS_HANDLER;
-----------------------------------------------------------------------------------------------
create or replace
PROCEDURE WRITERS_COMMISSION_INV_HANDLER AS 
last_run DATE;
time_canceled NUMBER :=0;
max_deposit_during_call_days number;
bonus_time_done DATE;
bonus_user_id NUMBER;
max_date DATE;
min_date DATE;
is_qualified NUMBER := 0;

not_good NUMBER := 0;
BEGIN
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_INV_HANDLER started at: ' || sysdate);

SELECT date_value into last_run
from db_parameters 
where id = 13;

-- Get the max deposit during call days in order not to miss last deposit during call transactions
select max(deposit_during_call_days)
into max_deposit_during_call_days
from skins;

FOR v_inv IN ( 
                                   select i.id as inv_id,
                                   i.user_id as user_id,
                                   i.time_created as inv_time
                                  from investments i
                                   where (i.time_created) between  last_run  and sysdate        
                                  ) LOOP     
                                  FOR v_data IN (
                                  
                                  select * 
                                  from (
                                     select aa.assigned_writer_id as sales_rep, 
                                   aa.user_id as user_id, 
                                   aa.assigned_time as assignment_time, 
                                   aa.call as first_call, 
                                   ll.time_settled as time_qualified,
                                   ll.transaction_issue_id as transaction_issue_id
                                    from
                                            (select a.assigned_writer_id,a.user_id, a.assigned_time , min(a.call) as call
                                            from
                                                    (select z.assigned_writer_id, f.id as user_id, z.pehtime as assigned_time, f.call , case when f.call > z.pehtime THEN 1  else  0 end as call_after_assign  
                                                    from    
                                                            (select pu.user_id , peh.time_created as pehtime, peh.assigned_writer_id , peh.id as pehid     
                                                            from population_users pu, population_entries pe, population_entries_hist peh, users u     
                                                            where pu.id=pe.population_users_id     
                                                            and peh.population_entry_id=pe.id     
                                                            and pu.user_id=u.id    
                                                            and peh.status_id=2     
                                                            and u.first_deposit_id<>0
                                                            and u.class_id<>0    
                                                            and  peh.assigned_writer_id is not null     
                                                            and pu.user_id is not null     
                                                            and peh.time_created>date '2012-09-01'
                                                            and pu.user_id = v_inv.user_id) z,                       
                                                            (select ia.writer_id, u.id, ia.action_time as call    
                                                            from users u, issues i, issue_actions ia, issue_action_types iat    
                                                            where i.user_id=u.id    
                                                            and ia.issue_id=i.id    
                                                            and ia.issue_action_type_id=iat.id    
                                                            and iat.channel_id = 1    
                                                            and iat.reached_status_id=2    
                                                            and ia.action_time>date '2012-09-01'
                                                            and i.user_id = v_inv.user_id) f    
                                                    where z.user_id=f.id    
                                                    and z.assigned_writer_id=f.writer_id) a
                                            where a.call_after_assign = 1
                                            group by a.assigned_writer_id, a.assigned_time, a.user_id)aa
                                    LEFT JOIN (
                                    select u.id as transaction_issue_id, ti.time_settled    
                                    from users u, transactions t, transactions_issues ti    
                                    where u.first_deposit_id=t.id    
                                    and t.id=ti.transaction_id    
                                    and ti.time_settled is not null)ll   on  aa.user_id = ll.transaction_issue_id
                                    WHERE ((ll.transaction_issue_id is not null and ll.time_settled is not null) OR (ll.transaction_issue_id is  null and ll.time_settled is  null))
                                    order by aa.assigned_time desc)
                                    where rownum=1
                                      ) LOOP
                                      select max(bu.id) into bonus_user_id 
                                      from bonus_users bu    
                                      where bu.user_id = v_inv.user_id
                                      and bu.bonus_state_id not in (7,6,5) ;
                                      
                                      IF (bonus_user_id is  null) THEN
                                                            IF (v_data.assignment_time > v_data.first_call AND v_data.assignment_time >v_data.time_qualified) THEN
                                                                    min_date := v_data.assignment_time;
                                                            END IF;
                                                            IF (v_data.first_call > v_data.assignment_time AND v_data.first_call > v_data.time_qualified) THEN 
                                                                    min_date :=v_data.first_call;
                                                            END IF;
                                                            IF (v_data.time_qualified > v_data.assignment_time AND v_data.time_qualified > v_data.first_call) THEN 
                                                                    min_date :=v_data.time_qualified;
                                                            END IF;       
                                         ELSE     
                                                                        select bu.time_done into bonus_time_done 
                                                                        from bonus_users bu    
                                                                        where bu.id = bonus_user_id;
                                                                         
                                                                    IF (bonus_time_done is  not null) THEN
        
                                                                            IF (v_data.time_qualified is not null) THEN
                                                                                    IF (v_data.assignment_time > v_data.first_call AND v_data.assignment_time >v_data.time_qualified AND v_data.assignment_time > bonus_time_done) THEN
                                                                                            min_date := v_data.assignment_time;
                                                                                    END IF;
                                                                                    IF (v_data.first_call > v_data.assignment_time AND v_data.first_call > v_data.time_qualified AND v_data.first_call > bonus_time_done) THEN 
                                                                                            min_date :=v_data.first_call;
                                                                                    END IF;
                                                                                    IF (v_data.time_qualified > v_data.assignment_time AND v_data.time_qualified > v_data.first_call AND v_data.time_qualified > bonus_time_done) THEN                                                                         
                                                                                            min_date :=v_data.time_qualified;
                                                                                    END IF;
                                                                                    IF (bonus_time_done > v_data.assignment_time AND bonus_time_done < v_data.first_call AND bonus_time_done < v_data.time_qualified) THEN 
                                                                                            min_date :=bonus_time_done;
                                                                                    END IF;
                                                                            ELSE
                                                                                    IF (v_data.assignment_time > v_data.first_call  AND v_data.assignment_time > bonus_time_done) THEN
                                                                                            min_date := v_data.assignment_time;
                                                                                    END IF;
                                                                                    IF (v_data.first_call > v_data.assignment_time  AND v_data.first_call > bonus_time_done) THEN 
                                                                                            min_date :=v_data.first_call;
                                                                                    END IF;
                                                                                    IF (bonus_time_done > v_data.assignment_time AND bonus_time_done < v_data.first_call) THEN 
                                                                                            min_date :=bonus_time_done;
                                                                                    END IF;
                                                                            END IF;
                                                                    END IF;                                                                 
                                                    END IF;

                                                     select count(time_created) into time_canceled     
                                                    from (
                                                    select peh.time_created 
                                                    from population_users pu, population_entries pe, population_entries_hist peh, populations p, population_entries_hist_status pehs
                                                    where pu.id = pe.population_users_id
                                                    and pe.id = peh.population_entry_id
                                                    and pu.user_id = v_inv.user_id  
                                                    and p.id = pe.population_id
                                                    and pehs.id= peh.status_id
                                                    and peh.time_created <= v_inv.inv_time
                                                    and peh.time_created >= min_date
                                                    and (pehs.id = 6 OR pehs.is_cancel_assign = 1)
                                                    order by peh.time_created asc)
                                                    where rownum = 1;          
                                                    
                                                    IF(time_canceled = 0) THEN
                                                            IF(v_inv.inv_time >= v_data.first_call) THEN
                                                            --DBMS_OUTPUT.PUT_LINE('ofer');
                                                                     INSERT INTO writers_commission_inv(id,investments_id, writer_id ,time_created)
                                                                    VALUES (SEQ_WRITERS_COMMISSION_INV.nextval, v_inv.inv_id ,v_data.sales_rep, sysdate); 
                                                            END IF;
                                                    END IF;
                                      END LOOP;                                                                                                                                                                                                                                                   
                                    END LOOP;
                                    
      update db_parameters
      set date_value = sysdate
      where id =13 ;                                           
                                    

DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_INV_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_INV_HANDLER;

---------------------------------------------------------------------------------------
create or replace
PROCEDURE WRITERS_COMMISSION_DEP_HANDLER AS 
last_run DATE;
assign_writer_id NUMBER := 0;
assign_date DATE ;
first_call_time DATE ;
time_canceled NUMBER := 0;
max_deposit_during_call_days number;
dep_during_call NUMBER :=0;
delta number :=0;
BEGIN
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_HANDLER started at: ' || sysdate);

SELECT date_value into last_run
from db_parameters 
where id = 14;

-- Get the max deposit during call days in order not to miss last deposit during call transactions
select max(deposit_during_call_days)
into max_deposit_during_call_days
from skins;

FOR v_tran IN ( 
                                   SELECT 
                                                  t.id as tran_id,
                                                  t.user_id as user_id,
                                                  t.time_created as tran_time
                                  FROM 
                                                  transactions t, users u
                                  WHERE 
                                                  u.id = t.user_id AND
                                                  u.first_deposit_id <> t.id AND
                                                  (t.time_created + max_deposit_during_call_days) between  last_run  and sysdate       
                            ) LOOP
                                      FOR v_data IN (
                                      select x.sales_rep as sales_rep, 
                                                       x.assignment_time as assignment_time, 
                                                       x.first_call as first_call from
                                      (select aa.assigned_writer_id as sales_rep, 
                                                       aa.assigned_time as assignment_time, 
                                                       aa.call as first_call
                                    from
                                            (select a.assigned_writer_id,a.user_id, a.assigned_time , min(a.call) as call
                                            from
                                                    (select z.assigned_writer_id, f.id as user_id, z.pehtime as assigned_time, f.call , case when f.call > z.pehtime THEN 1  else  0 end as call_after_assign  
                                                    from    
                                                            (select pu.user_id , peh.time_created as pehtime, peh.assigned_writer_id , peh.id as pehid     
                                                            from population_users pu, population_entries pe, population_entries_hist peh, users u     
                                                            where pu.id=pe.population_users_id     
                                                            and peh.population_entry_id=pe.id     
                                                            and pu.user_id=u.id    
                                                            and peh.status_id=2     
                                                            and u.first_deposit_id<>0
                                                            and u.class_id<>0    
                                                            and  peh.assigned_writer_id is not null     
                                                            and pu.user_id is not null     
                                                            and peh.time_created>date '2012-09-01'
                                                            and pu.user_id = v_tran.user_id) z,                       
                                                            (select ia.writer_id, u.id, ia.action_time as call    
                                                            from users u, issues i, issue_actions ia, issue_action_types iat    
                                                            where i.user_id=u.id    
                                                            and ia.issue_id=i.id    
                                                            and ia.issue_action_type_id=iat.id    
                                                            and iat.channel_id = 1    
                                                            and iat.reached_status_id=2    
                                                            and ia.action_time>date '2012-09-01'
                                                            and i.user_id = v_tran.user_id) f    
                                                    where z.user_id=f.id    
                                                    and z.assigned_writer_id=f.writer_id) a
                                            where a.call_after_assign = 1
                                            group by a.assigned_writer_id, a.assigned_time, a.user_id)aa
                                            order by aa.assigned_time desc
                                            ) x
                                            where rownum=1)
                                            LOOP
                                                     select count(time_created) into time_canceled     
                                                    from (
                                                    select peh.time_created 
                                                    from population_users pu, population_entries pe, population_entries_hist peh, populations p, population_entries_hist_status pehs
                                                    where pu.id = pe.population_users_id
                                                    and pe.id = peh.population_entry_id
                                                    and pu.user_id = v_tran.user_id  
                                                    and p.id = pe.population_id
                                                    and pehs.id= peh.status_id
                                                    and peh.time_created <= v_tran.tran_time
                                                    and peh.time_created >= v_data.first_call
                                                    and (pehs.id = 6 OR pehs.is_cancel_assign = 1)
                                                    order by peh.time_created asc)
                                                    where rownum = 1;
                  
                                            IF(time_canceled = 0) THEN

                                                    SELECT COUNT(ti.transaction_id) into dep_during_call
                                                    FROM transactions_issues ti, issue_actions ia
                                                    WHERE ti.transaction_id = v_tran.tran_id
                                                    AND ti.issue_action_id = ia.id
                                                    AND ia.issue_action_type_id = 31;
                                                    
                                                    IF(dep_during_call > 0) THEN
                                                           delta := max_deposit_during_call_days;
                                                    ELSE 
                                                          delta := 0;
                                                    END IF;

                                                    IF(v_tran.tran_time >= v_data.first_call - delta) THEN
                                                            INSERT INTO writers_commission_dep(id, transaction_id, writer_id ,time_created)
                                                            VALUES (SEQ_WRITERS_COMMISSION_DEP.nextval, v_tran.tran_id ,v_data.sales_rep, sysdate); 
                                                    END IF;
                                            END IF;
                                            END LOOP;                                  
                              END LOOP;
                                    
      update db_parameters
      set date_value = sysdate
      where id =14 ;                                

DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_DEP_HANDLER;

-----------------------------------------------------------------------------------------------
--16/12/2012
--Ofer
--Run db_version

create or replace
PROCEDURE "POPULATIONS_HANDLER" (runType NUMBER) AS

  -- Run Types
  RUN_TYPE_ALL_USERS NUMBER := 1;
  RUN_TYPE_NEW_USERS NUMBER := 2;
  RUN_TYPE_ONLY_CONTACTS NUMBER := 3;

	v_pe population_entries%ROWTYPE;
  v_pu population_users%ROWTYPE;
	v_peh population_entries_hist%ROWTYPE;
  populationEntHist population_entries_hist%ROWTYPE;

  populationEntryId NUMBER;
  qualifiedPopualtionId NUMBER;
  currentPopulationId NUMBER;
  currentPopulationPriority NUMBER;
  is_exist_pop NUMBER;

  popQualificationTime DATE;
  popBaseQualificationTime DATE;
  popBaseQualificationTimeOld DATE;
   isExistDormentConvertionPop DATE;

  newPopualtionThreshold NUMBER;
  qualificationStatus NUMBER;
  assignedWriterId NUMBER;

  users_popualtion_users_id NUMBER;
  lastSalesDepositTime DATE;

  v_counter NUMBER;
  isExist NUMBER;
  isExistSuccessfullDeposit NUMBER;
  isExistTranIssue NUMBER;
  isExistOpenWithdraw NUMBER;
  isFirstMinimumBalanceCheck NUMBER;
  userNumOfSuccesDeposits NUMBER;
  userAmountOfSuccesDeposits NUMBER;
  isCheckQualificationTime NUMBER;

  count_new_qualified_users number := 0;
  count_new_qualified_contacts number := 0;
  runTypeName varchar2(30);
BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER started at: ' || sysdate);

  CASE
    WHEN runType = RUN_TYPE_ALL_USERS THEN
      runTypeName := 'Run Type All Users';
    WHEN runType = RUN_TYPE_NEW_USERS THEN
      runTypeName := 'Run Type New Users';
    WHEN runType = RUN_TYPE_ONLY_CONTACTS THEN
      runTypeName := 'Run Type Only Contacts';
    ELSE
      DBMS_OUTPUT.PUT_LINE('run type: ' || runType || ' doesnt exist');
  END CASE;

  DBMS_OUTPUT.PUT_LINE('run type: ' || runTypeName);

  IF (runType != RUN_TYPE_ONLY_CONTACTS) THEN
    -- RUN MARKETING_HANDLER
    marketing_handler();

    FOR v_usr IN (
      SELECT
        u.id user_id,
        u.skin_id,
        u.balance,
        u.time_created,
        u.currency_id,
        u.is_declined,
        u.is_contact_for_daa,
        u.last_decline_date,
        u.time_last_login,
        u.contact_id,
        u.time_sc_house_result , 
        u.first_deposit_id ,
        p.id population_id,
        p.population_type_id curr_pop_type_id,
        pu.id population_user_id,
        pu.curr_population_entry_id,
        pu.curr_assigned_writer_id,
        pu.entry_type_id,
        pu.entry_type_action_id,
        pt.priority,
        ilgc.value min_investment_limit,
        s.other_reactions_days
      FROM
        investment_limits il,
        investment_limit_group_curr ilgc,
        skins s,
        users u LEFT JOIN population_users pu on u.id = pu.user_id
                        LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                            LEFT JOIN populations p on pe.population_id = p.id
                                  LEFT JOIN population_types pt on p.population_type_id = pt.id
      WHERE
        u.currency_id = ilgc.currency_id
        AND il.min_limit_group_id = ilgc.investment_limit_group_id
        AND il.id = 1 -- Default binary limits id
        AND u.skin_id = s.id
        AND u.class_id !=0
        AND u.is_active = 1
        AND u.is_false_account = 0
        AND u.is_contact_by_phone = 1
        AND u.id NOT in (select USER_ID from frauds)
        AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
        AND pu.lock_history_id is null
        AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
        AND u.is_vip = 0
        --AND u.time_sc_house_result is null
        AND (runType = RUN_TYPE_ALL_USERS or u.time_created > sysdate - 1)
        --and u.id = 8776
   
    ) LOOP

        -- DBMS_OUTPUT.PUT_LINE('user12 : ' || v_usr.);
          v_peh := null;
          populationEntryId := 0;
          qualifiedPopualtionId := 0;
          newPopualtionThreshold := 0;
          isFirstMinimumBalanceCheck := 1;
          assignedWriterId := v_usr.curr_assigned_writer_id;
          users_popualtion_users_id := v_usr.population_user_id ;
          lastSalesDepositTime := null;

          IF (v_usr.population_id is null) THEN
                currentPopulationId := 0;
                currentPopulationPriority := 999999; -- Lowest proirity
          ELSE
                currentPopulationId := v_usr.population_id;
                currentPopulationPriority := v_usr.priority;
          END IF;

          FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable,
                                    pt.priority
                              FROM
                                    populations p,
                                    
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND  (pt.priority < currentPopulationPriority OR currentPopulationPriority =10) -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_usr.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = runType
                              ORDER BY pt.priority -- Highest priority is 1
          ) LOOP

              populationEntHist := null;
              popQualificationTime := null;
              popBaseQualificationTime := null;
              isCheckQualificationTime := 0;

              CASE
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 1 THEN --POPULATION_FIRST_DEPO_FAILED

                         IF (v_usr.is_declined = 1) THEN

                              -- Check whether this user had some successed deposits in the past
                              select
                                 max(t.id)
                              into
                                 isExistSuccessfullDeposit
                              from
                                 transactions t,
                                 transaction_types tt
                              where
                                 tt.class_type = 1 AND
                                 user_id = v_usr.user_id AND
                                 type_id = tt.id AND
                                 status_id in (2,7,8);

                              IF (isExistSuccessfullDeposit is null) THEN

                                    popBaseQualificationTime  := v_usr.last_decline_date;
                                    popQualificationTime := popBaseQualificationTime;

                                     isCheckQualificationTime := 1;

                              END IF;
                           END IF; -- IF (v_usr.is_declined = 1) THEN

                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  WHEN v_population.population_type_id = 2 THEN -- POPULATION_LAST_DEPO_FAILED

                        -- If users doesn't want to ne called for decline after approve we don't insert him into DDA population.
                        IF (v_usr.is_contact_for_daa != 0 AND v_usr.is_declined = 1) THEN

                              popBaseQualificationTime  := v_usr.last_decline_date;
                              popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;

                              IF (sysdate  >= popQualificationTime) THEN

                                        -- Check whether this user had some successed deposits in the past
                                        IF (isExistSuccessfullDeposit is not null) THEN

                                               isCheckQualificationTime := 1;

                                        END IF; -- IF (isExistSuccessfullDeposit is not null) THEN
                              END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 5 THEN -- POPULATION_NO_DEPOSIT_FILL

                        popBaseQualificationTime := v_usr.time_created;
                        popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;
                                 
                        IF (sysdate  >= popQualificationTime) THEN
                        
                              select
                                max(t.id)
                              into
                                isExist
                              from
                                transactions t,
                                transaction_types tt
                              where
                                tt.class_type = 1 AND
                                user_id = v_usr.user_id AND
                                type_id = tt.id;
                              
                                IF (isExist is null) THEN
                                       isCheckQualificationTime := 1;

                                END IF;
                        END IF;

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 9 THEN -- POPULATION_DORMANT_ACCOUNT

                        IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                                -- IF we didn't find a sales deposit for that user before, search it.
                                IF (lastSalesDepositTime is null) THEN

                                      select
                                            max(t.time_created)
                                       into
                                            lastSalesDepositTime
                                       from
                                            transactions t,
                                            transactions_issues ti
                                      where
                                            t.id = ti.transaction_id
                                            and t.user_id = v_usr.user_id;
                                END IF; --  IF (lastSalesDepositTime is null) THEN

                                -- If user had no sales deposit in the past or v_population.LAST_SALES_DEPOSIT_DAYS has passed since last sales deposit time... go on...
                                IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN

                                      popBaseQualificationTime := GET_LAST_INVEST_TIME(v_usr.user_id);

                                      IF (popBaseQualificationTime is not null) THEN

                                           -- If v_population.number_of_days has passed since last user login time
                                           -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                                           IF (v_usr.time_last_login < sysdate - v_population.last_invest_day
                                                      OR
                                                 popBaseQualificationTime < sysdate - v_population.max_last_login_time_days) THEN

                                                    popQualificationTime := popBaseQualificationTime + v_population.last_invest_day;

                                                    IF (sysdate  >= popQualificationTime) THEN

                                                           -- Check if user was above min balance during the all threshold time and didn't had a balance increase
                                                           IF (get_is_dormant_above_balance(v_usr.user_id,
                                                                                                                  v_population.last_invest_day,
                                                                                                                  v_usr.min_investment_limit,
                                                                                                                  v_usr.balance) = 1) THEN

                                                                 isCheckQualificationTime := 1;
                                                            END IF;

                                                    END IF; -- IF (sysdate  >= popQualificationTime) THEN
                                              END IF; -- IF (v_usr.time_last_login < sysdate - v_population.number_of_days...
                                      END IF; -- IF (popBaseQualificationTime is not null) THEN
                                END IF; -- IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN
                        END IF; -- IF  (v_usr.balance > v_usr.min_investment_limit) THEN

                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 10 THEN -- POPULATION_NO_INVESTMENTS

                          popBaseQualificationTime := GET_LAST_SUCCESS_DEPOSIT_TIME(v_usr.user_id);

                          IF (popBaseQualificationTime is not null) THEN

                                popQualificationTime := popBaseQualificationTime + v_population.number_of_days;

                                IF(sysdate  >  popQualificationTime) THEN

                                      select   max(i.id)
                                      into      isExist
                                      from     investments i
                                      where   v_usr.user_id = i.user_id;

                                      IF (isExist is null) THEN

                                            isCheckQualificationTime := 1;

                                      END IF;
                                END IF;
                          END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------    
                    WHEN v_population.population_type_id = 22 THEN -- POPULATION OPEN WITHDRAW
                              SELECT count(t.id) into isExistOpenWithdraw
                               FROM transactions t, transaction_types tt 
                               WHERE t.user_id = v_usr.user_id
                              AND tt.id = t.type_id 
                              AND tt.class_type = 2
                              AND t.status_id = 4;
                              
                              IF (isExistOpenWithdraw > 0) THEN 
                                      isCheckQualificationTime := 1;
                                      popQualificationTime := sysdate;
                               END IF;
                     --------------------------------------------------------------------------------------------------------------------------------------------------
                    WHEN v_population.population_type_id = 24 THEN -- POPULATION_DORMENT_CONVERSION

                              IF (v_usr.curr_pop_type_id = 23) THEN
                              
                                      SELECT tab.qualification_time into isExistDormentConvertionPop
                                      FROM (
                                      SELECT pe.*
                                      FROM population_users pu, population_entries pe, 
                                      population_entries_hist peh, 
                                      populations p, 
                                      population_entries_hist_status pehs
                                      WHERE pu.id = pe.population_users_id
                                      AND pe.id = peh.population_entry_id
                                      AND pu.user_id = v_usr.user_id
                                      AND p.id = pe.population_id
                                      AND p.population_type_id = 23
                                      ORDER BY pe.qualification_time) tab
                                      WHERE rownum = 1;

                                      IF(isExistDormentConvertionPop is not null AND isExistDormentConvertionPop < sysdate - v_population.NUMBER_OF_DAYS) THEN               

                                            IF (v_usr.curr_assigned_writer_id is null) THEN

                                                    SELECT count(ti.TRANSACTION_ID) into isExistTranIssue
                                                    FROM users u, transactions t, transactions_issues ti
                                                    where u.id = v_usr.user_id
                                                    and u.id = t.user_id
                                                    and t.id = ti.transaction_id
                                                    and ti.time_settled is null;
                                                    
                                                    IF(isExistTranIssue > 0) THEN
                                                            isCheckQualificationTime := 1;
                                                            popQualificationTime := sysdate;
                                                    END IF;
                                            END IF;
                                      END IF;                                    
                              END IF;
                    ------------------------------------------------------------------------------------------------------------------------------------------------
                    ELSE  --IF  v_population.population_type_id wasn't found
                        -- No handler for this population type
                      DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
                END CASE;
                
                -- IF isCheckQualificationTime = 1, User is qualified to that populaiton, now check his qualification time
                IF (isCheckQualificationTime = 1) THEN

                      populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                      IF (populationEntHist.status_id is null
                                 OR
                            popBaseQualificationTime >= populationEntHist.TIME_CREATED
                                 OR
                           (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                                OR
                           (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                          -- Find qualification time of this population last entry
                          BEGIN
                              select base_qualification_time
                              into popBaseQualificationTimeOld
                              from population_entries pe
                              where pe.id = populationEntHist.population_entry_id
                                  and qualification_time is not null ;
                        EXCEPTION
                              WHEN no_data_found THEN
                                      popBaseQualificationTimeOld := null;
                           END;

                          -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                          IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                                 populationEntryId :=  populationEntHist.population_entry_id;
                          END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                          -- Update user entry with this population id.
                          qualifiedPopualtionId := v_population.id;

                           -- Keep current popualtion threshold
                          newPopualtionThreshold := v_population.threshold;

                           -- If user was finally found qualified for current popualtion, break the loop
                          GOTO end_loop;

                      END IF; -- IF (populationEntHist.status_id is null ....
                END IF; --   IF (isCheckQualificationTime = 1) THEN

          END LOOP;
           <<end_loop>>

          -- If got a newPopualtionId, insert user to new population.
          IF (qualifiedPopualtionId != 0) THEN
                count_new_qualified_users := count_new_qualified_users +1;

                -- If this  is a contact.
                IF (v_usr.user_id = 0) THEN
                   assignedWriterId := null;
                END IF;

                -- If user is already in a population, remove it first.
                IF (v_usr.curr_population_entry_id is not null) THEN
                      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                      v_peh.population_entry_id := v_usr.curr_population_entry_id;
                      v_peh.status_id := 105;
                      v_peh.writer_id := 0;
                      v_peh.assigned_writer_id := assignedWriterId;
                      v_peh.time_created := sysdate;
                      v_peh.issue_action_id := null;
                      INSERT INTO population_entries_hist VALUES v_peh;
                END IF; -- IF (v_usr.curr_population_entry_id is not null) THEN

                 -- If user doesn't have an entry in population_users, create one.
                IF (users_popualtion_users_id is null) THEN
                      SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                      v_pu.id := users_popualtion_users_id;
                      v_pu.user_id := v_usr.user_id;
                      if (v_usr.contact_id != 0) then
                        v_pu.contact_id := v_usr.contact_id;
                      else
                        v_pu.contact_id := null;
                      end if;
                      v_pu.curr_assigned_writer_id := null;
                      v_pu.curr_population_entry_id := null;
                      v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                      v_pu.entry_type_action_id := null;
                      v_pu.lock_history_id := null;

                      INSERT INTO population_users VALUES v_pu;
                END IF; --  IF (users_popualtion_users_id is null) THEN

                -- If population entry id hasn't been changed create a new one.
                IF (populationEntryId = 0) THEN
                      -- Count the number of users in the new population in order to determine user's entry group.
                      SELECT
                              COUNT(pe.id) INTO v_counter
                      FROM
                              population_entries pe,
                              population_users pu 
                      WHERE
                              pe.id = pu.curr_population_entry_id AND
                              pe.population_id = qualifiedPopualtionId;

                      -- Insert a new population_entry
                      SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                      v_pe.id := populationEntryId;
                      v_pe.population_id := qualifiedPopualtionId;
                      v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                      v_pe.population_users_id := users_popualtion_users_id;
                      v_pe.qualification_time := popQualificationTime ;
                      v_pe.base_qualification_time := popBaseQualificationTime ;
                      v_pe.is_displayed := 1;
                      INSERT INTO population_entries VALUES v_pe;

                      qualificationStatus := 1; -- Qualification
                ELSE
                      qualificationStatus := 7; -- reQualification
                END IF; --  IF (populationEntryId = 0) THEN


                 -- Insert a new population_entry_history
                SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                v_peh.population_entry_id :=populationEntryId;
                v_peh.status_id := qualificationStatus;
                v_peh.writer_id := 0;
                v_peh.assigned_writer_id := assignedWriterId;
                v_peh.time_created := sysdate;
                v_peh.issue_action_id := null;
                INSERT INTO population_entries_hist VALUES v_peh;

                -- Update user entry in population_users table
                UPDATE
                      population_users pu
                SET
                      pu.curr_population_entry_id = populationEntryId,
                      pu.curr_assigned_writer_id = assignedWriterId
                WHERE
                     pu.user_id = v_usr.user_id;

          END IF; -- IF (qualifiedPopualtionId != 0) THEN

         COMMIT;

    END LOOP;

    if (runType = RUN_TYPE_ALL_USERS) then
          update db_parameters dp
          set date_value = sysdate
          where dp.id = 9;
    end if;

    -- RUN DELAY HANDLER
    delay_handler();

    -- RUN TRACKING HANDLER
    tracking_handler();

      -- RUN SPECIAL HANDLER
    special_handler();

    -- RUN CALLBACK HANDLER
    callback_handler();
    
    -- RUN ISSUES_STATUS_UPDATER
    issues_status_updater();
    
 IF (runType = RUN_TYPE_ALL_USERS) THEN   
    -- RUN DORMENT CONVERSION HANDLER
    dorment_conversion_handler();

    -- RUN USERS STATUS HANDLER
    users_status_handler();

    -- RUN USERS RANK HANDLER    
    users_rank_handler();
    
    --RUN RETURN FROM TRAKING HANDLER
    return_from_tracking();
END IF;
    DBMS_OUTPUT.PUT_LINE('count_new_qualified_users  = ' || count_new_qualified_users);

  END IF;
       
  FOR v_con IN (
    SELECT
      c.id contact_id,
      c.skin_id,
      c.time_created,
      c.type,
      p.id population_id,
      p.population_type_id curr_pop_type_id,
      pu.id population_user_id,
      pu.curr_population_entry_id,
      pu.curr_assigned_writer_id,
      pu.entry_type_id,
      pu.entry_type_action_id,
      pt.priority,
      s.other_reactions_days
    FROM
      skins s,
      contacts c
          LEFT JOIN population_users pu on c.id = pu.contact_id
              LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id
                  LEFT JOIN populations p on pe.population_id = p.id
                        LEFT JOIN population_types pt on p.population_type_id = pt.id
    WHERE
      c.skin_id = s.id
      AND (pu.entry_type_id is null OR pu.entry_type_id != 5)  -- population.enrty.type.remove.permanently
      AND pu.lock_history_id is null
      AND (pt.is_replaceable_by_handler = 1  OR pt.is_replaceable_by_handler is null)
      AND c.type in (5, 6) -- Short Reg Form contacts and register attempt
      AND (c.land_line_phone is not null OR c.mobile_phone is not null)    
      AND c.user_id = 0
  ) LOOP

        v_peh := null;
        populationEntryId := 0;
        qualifiedPopualtionId := 0;
        newPopualtionThreshold := 0;
        assignedWriterId := v_con.curr_assigned_writer_id;
        users_popualtion_users_id := v_con.population_user_id ;

        IF (v_con.population_id is null) THEN
              currentPopulationId := 0;
              currentPopulationPriority := 999999; -- Lowest proirity
        ELSE
              currentPopulationId := v_con.population_id;
              currentPopulationPriority := v_con.priority;
        END IF;

        FOR v_population IN (SELECT
                                    p.*,
                                    pt.is_delay_enable
                              FROM
                                    populations p,
                                    population_types pt
                              WHERE
                                    p.population_type_id = pt.id
                                    AND pt.priority < currentPopulationPriority -- Highest priority is 1
                                    AND p.id != currentPopulationId
                                    AND p.is_active = 1
                                    AND p.skin_id = v_con.skin_id
                                     -- This pop type is handled by this procedure
                                    AND pt.is_population_handler = 1
                                    AND pt.handler_run_type = RUN_TYPE_ONLY_CONTACTS
                              ORDER BY pt.priority -- Highest priority is 1
        ) LOOP

            populationEntHist := null;
            popQualificationTime := null;
            popBaseQualificationTime := null;
            isCheckQualificationTime := 0;

            CASE
                ------------------------------------------------------------------------------------------------------------------------------------------------
                WHEN v_population.population_type_id in (20, 21) THEN --POPULATION_SHORT_REG_FORM AND POPLULATION_REGISTER_ATTEMPT
                   
                    popBaseQualificationTime := v_con.time_created;
                      
                    popQualificationTime := popBaseQualificationTime + v_population.number_of_minutes/1440 ;
                    
                    if (popQualificationTime < sysdate) then
                   
                       isCheckQualificationTime := 1;
                    end if;
                  ------------------------------------------------------------------------------------------------------------------------------------------------
                  ELSE  --IF  v_population.population_type_id wasn't found
                      -- No handler for this population type
                    DBMS_OUTPUT.PUT_LINE('population_type_id: ' || v_population.population_type_id || ' wasnt found');
             END CASE;

              -- IF isCheckQualificationTime = 1, contact is qualified to that populaiton, now check his qualification time
              IF (isCheckQualificationTime = 1) THEN

                    populationEntHist := GET_USER_LAST_POP_BY_TYPE(users_popualtion_users_id,v_population.population_type_id);

                    IF (populationEntHist.status_id is null
                               OR
                          popBaseQualificationTime >= populationEntHist.TIME_CREATED
                               OR
                         (v_population.is_delay_enable = 0 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))
                               OR
                         (v_population.is_delay_enable = 1 AND NOT (populationEntHist.status_id IN (100,101,107,222,112,602,115)))) THEN

                        -- Find qualification time of this population last entry
                        BEGIN
                            select base_qualification_time
                            into popBaseQualificationTimeOld
                            from population_entries pe
                            where pe.id = populationEntHist.population_entry_id
                                and qualification_time is not null ;
                        EXCEPTION
                            WHEN no_data_found THEN
                                    popBaseQualificationTimeOld := null;
                        END;

                        -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                        IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
                               populationEntryId :=  populationEntHist.population_entry_id;
                        END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN
              
                        if( ( v_con.type = 5 AND v_population.population_type_id = 20 ) OR   ( v_con.type = 6  AND v_population.population_type_id = 21 )) then  
                        -- Update contact entry with this population id.
                              qualifiedPopualtionId := v_population.id;
                              -- Keep current popualtion threshold
                              newPopualtionThreshold := v_population.threshold;
                              -- If contact was finally found qualified for current popualtion, break the loop
                              GOTO end_loop;
                        end if;
                    END IF; -- IF (populationEntHist.status_id is null ....
              END IF; --   IF (isCheckQualificationTime = 1) THEN

        END LOOP;
         <<end_loop>>

        -- If got a newPopualtionId, insert contact to new population.
        IF (qualifiedPopualtionId != 0) THEN

              count_new_qualified_contacts := count_new_qualified_contacts +1;

              -- If this contact is in general entry_type preform auto cancel assign.
              IF (v_con.entry_type_id = 1) THEN
                 assignedWriterId := null;
              END IF;

              -- If contact is already in a population, remove it first.
              IF (v_con.curr_population_entry_id is not null) THEN
                    SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

                    v_peh.population_entry_id := v_con.curr_population_entry_id;
                    v_peh.status_id := 105;
                    v_peh.writer_id := 0;
                    v_peh.assigned_writer_id := assignedWriterId;
                    v_peh.time_created := sysdate;
                    v_peh.issue_action_id := null;
                    INSERT INTO population_entries_hist VALUES v_peh;
              END IF; -- IF (v_con.curr_population_entry_id is not null) THEN

               -- If contact doesn't have an entry in population_users, create one.
              IF (users_popualtion_users_id is null) THEN
                    SELECT SEQ_POPULATION_USERS.NEXTVAL INTO users_popualtion_users_id FROM dual;

                    v_pu.id := users_popualtion_users_id;
                    v_pu.user_id := null;
                    v_pu.contact_id := v_con.contact_id;
                    v_pu.curr_assigned_writer_id := null;
                    v_pu.curr_population_entry_id := null;
                    v_pu.entry_type_id :=   1;  -- population.enrty.type.general
                    v_pu.entry_type_action_id := null;
                    v_pu.lock_history_id := null;
                    v_pu.time_created := sysdate;
--                    DBMS_OUTPUT.PUT_LINE('contact_id: ' || v_con.contact_id);
                    INSERT INTO population_users VALUES v_pu;
              END IF; --  IF (users_popualtion_users_id is null) THEN

              -- If population entry id hasn't been changed create a new one.
              IF (populationEntryId = 0) THEN
                    -- Count the number of users in the new population in order to determine user's entry group.
                    SELECT
                            COUNT(pe.id) INTO v_counter
                    FROM
                            population_entries pe,
                            population_users pu
                    WHERE
                            pe.id = pu.curr_population_entry_id AND
                            pe.population_id = qualifiedPopualtionId;

                    -- Insert a new population_entry
                    SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                    v_pe.id := populationEntryId;
                    v_pe.population_id := qualifiedPopualtionId;
                    v_pe.group_id := CASE WHEN newPopualtionThreshold = 1 THEN 1 ELSE CASE WHEN MOD(v_counter, newPopualtionThreshold) = 0 THEN 0 ELSE 1 END END;
                    v_pe.population_users_id := users_popualtion_users_id;
                    v_pe.qualification_time := popQualificationTime ;
                    v_pe.base_qualification_time := popBaseQualificationTime ;
                    v_pe.is_displayed := 1;
                    INSERT INTO population_entries VALUES v_pe;

                    qualificationStatus := 1; -- Qualification
              ELSE
                    qualificationStatus := 7; -- reQualification
              END IF; --  IF (populationEntryId = 0) THEN


               -- Insert a new population_entry_history
              SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM dual;

              v_peh.population_entry_id :=populationEntryId;
              v_peh.status_id := qualificationStatus;
              v_peh.writer_id := 0;
              v_peh.assigned_writer_id := assignedWriterId;
              v_peh.time_created := sysdate;
              v_peh.issue_action_id := null;
              INSERT INTO population_entries_hist VALUES v_peh;

              -- Update contact entry in population_users table
              UPDATE
                    population_users pu
              SET
                    pu.curr_population_entry_id = populationEntryId,
                    pu.curr_assigned_writer_id = assignedWriterId
              WHERE
                   pu.contact_id = v_con.contact_id;

        END IF; -- IF (qualifiedPopualtionId != 0) THEN

       COMMIT;

  END LOOP;
  
  DBMS_OUTPUT.PUT_LINE('count_new_qualified_conacts  = ' || count_new_qualified_contacts);

  DBMS_OUTPUT.PUT_LINE('POPULATIONS_HANDLER finished at: ' || sysdate);
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');

  NULL;
END;
-------------------------------------------------------------------------------------

--Ofer 02/12/12
--Dev 299

create or replace
PROCEDURE MANUALLY_IMPORTED_CON_HANDLER AS 

MANUALLY_IMPORTED_CON_ID NUMBER := 25;
POP_RETENTION_ID NUMBER := 23;
populationEntryId  NUMBER :=0;
populationId NUMBER :=0;
populationUserId NUMBER :=0;
populationEntryHistId NUMBER :=0;

users_out_of_mic NUMBER :=0;

  
  
BEGIN

DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('MANUALLY_IMPORTED_CONVERSION started at: ' || sysdate);

FOR  v_user IN (
                                  SELECT 
                                                    u.id as user_id,
                                                    u.skin_id as user_skin_id,
                                                    pu.id as population_user_id,
                                                    pe.qualification_time as entry_qualification_time
                                  FROM 
                                                    population_users pu, 
                                                    population_entries pe, 
                                                    populations p, 
                                                    users u
                                   WHERE 
                                                    pu.user_id = u.id and
                                                    pu.curr_population_entry_id = pe.id and
                                                    pe.population_id = p.id and
                                                    p.population_type_id = 25 and
                                                    u.first_deposit_id is not null and
                                                    pe.qualification_time <= sysdate - p.number_of_days
                                  )
                                  LOOP
                                                    SELECT 
                                                     p.id INTO populationId
                                                    FROM  
                                                     populations p,  
                                                     population_types pt  
                                                    WHERE  
                                                     pt.id = p.population_type_id 
                                                     AND p.skin_id = v_user.user_skin_id
                                                     AND p.population_type_id = POP_RETENTION_ID;           
                                                     
                                                     SELECT pu.id INTO populationUserId
                                                     FROM population_users pu
                                                     where pu.user_id = v_user.user_id;
                                                     
                                                     IF (populationUserId > 0 AND populationId > 0) THEN 
                                                     
                                                             SELECT SEQ_POPULATION_ENTRIES.NEXTVAL INTO populationEntryId FROM dual;
                                                            
                                                             INSERT INTO population_entries(id, population_id, group_id, qualification_time, population_users_id, is_displayed, base_qualification_time) 
                                                             VALUES (populationEntryId, populationId, 1, sysdate, populationUserId,1,sysdate);                                                                                                                                         
                                                             
                                                             SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO populationEntryHistId
                                                             FROM dual;

                                                             INSERT INTO population_entries_hist(id, population_entry_id, status_id, issue_action_id,  writer_id, time_created, assigned_writer_id) 
                                                             VALUES (populationEntryHistId, populationEntryId, 1, null , 0 ,sysdate, 0);
                                                             
                                                            UPDATE 
                                                                              population_users pu 
                                                             SET 
                                                                             entry_type_id = 1, 
                                                                             curr_population_entry_id = populationEntryId, 
                                                                             curr_assigned_writer_id = null
                                                             WHERE 
                                                                            pu.id = populationUserId;
                                                             
                                                             users_out_of_mic := users_out_of_mic +1;

                                                      END IF;
                                  END LOOP;
                                  
DBMS_OUTPUT.PUT_LINE('MANUALLY_IMPORTED_CONVERSION_HANDLER finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');  
DBMS_OUTPUT.PUT_LINE('users_out_of_manually_imported_conversion: ' || users_out_of_mic);
  NULL;
END MANUALLY_IMPORTED_CON_HANDLER;
------------------------------------------------------------------------------------------------------------------------------------------------
-- Begin Jamal 19.12.2012 DEV-190
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace
PROCEDURE "UPDATE_MARKETING_GOOGLE_DAY" (date_of_data date) AS

  is_check_existing_data number := 1;
  is_existing_data number := 0;
  rows_count numeric := 0;
  trunc_table varchar2(100);

BEGIN

 IF (is_check_existing_data = 1) THEN
      select count(*)
      into is_existing_data
      from Marketing_Google_Report m
      where date_of_data = m.day;
  END IF;

  IF (is_existing_data = 0) THEN

      trunc_table:='truncate table marketing_google_users_dp';
      EXECUTE IMMEDIATE trunc_table ;

      insert into marketing_google_users_dp
       select distinct u.dynamic_param, u.combination_id, mu.is_keep_daily_data as is_google_user
      from users u,
       (select mc.id, ms.is_keep_daily_data
                                 from   marketing_combinations mc ,
                                        marketing_campaigns mca,
                                        marketing_payment_recipients mp,
                                        marketing_sources ms
                                 where  mc.campaign_id = mca.id
                                   and  mca.payment_recipient_id = mp.id
                                   and  ms.id = mca.source_id
                                   and  ms.is_keep_daily_data in (0,1)
                                   ) mu
      where u.class_id <> 0
        and u.combination_id=mu.id;

      For v_data in (
              SELECT
                   distinct
                   u.dynamic_param,
                   u.combination_id,
                   clk.clicks_num ,
                   reg_users.registered_users,
                   dep_users.reg_frst_dep_users,
                   dep_users.frst_dep_users,
                   rem_users.frst_rem_dep_users,
                   sums_transactions.sum_deposits,
                   sums_investments.turnover,
                   sums_investments.house_win,
                   u.is_google_user
               FROM
                   marketing_google_users_dp u

                     LEFT JOIN
                     (SELECT
                             u.dynamic_param,
                             u.combination_id,
                             count(*) as registered_users
                       FROM
                             users u,
                             marketing_google_users_dp m
                       WHERE
                             u.class_id <> 0
                             AND u.time_created >= trunc(date_of_data ) and u.time_created < trunc(date_of_data + 1)
                             AND u.combination_id = m.combination_id
                             AND NVL(u.dynamic_param,0) = NVL(m.dynamic_param,0)
                       GROUP  BY
                             u.dynamic_param, u.combination_id) reg_users
                                              on NVL(u.dynamic_param,0) = NVL(reg_users.dynamic_param,0)
                                              and u.combination_id = reg_users.combination_id

                     LEFT JOIN
                     (SELECT
                             u.dynamic_param,
                             u.combination_id,
                             count(*) as frst_dep_users,
                             SUM(CASE WHEN trunc(u.time_created) = trunc(date_of_data )
                                      THEN 1 ELSE 0 END) as reg_frst_dep_users
                       FROM
                             users u,
                             transactions t,
                             marketing_google_users_dp m
                       WHERE
                             u.first_deposit_id = t.id
                             AND t.time_created >= trunc(date_of_data ) and t.time_created < trunc(date_of_data + 1)
                             AND u.class_id <> 0
                             AND u.id not in (select rl.user_id from remarketing_logins rl)
                             AND u.combination_id = m.combination_id
                             AND NVL(u.dynamic_param,0) = NVL(m.dynamic_param,0)
                       GROUP  BY
                             u.dynamic_param, u.combination_id) dep_users
                                              on NVL(u.dynamic_param,0) = NVL(dep_users.dynamic_param,0)
                                              and u.combination_id = dep_users.combination_id

                       LEFT JOIN
                     (SELECT
                             u.dynamic_param,
                             u.combination_id,
                             count(*) as frst_rem_dep_users,
                             SUM(CASE WHEN trunc(u.time_created) = trunc(date_of_data )
                                      THEN 1 ELSE 0 END) as reg_rem_frst_dep_users
                       FROM
                             users u,
                             transactions t,
                             marketing_google_users_dp m
                       WHERE
                             u.first_deposit_id = t.id
                             AND t.time_created >= trunc(date_of_data ) and t.time_created < trunc(date_of_data + 1)
                             AND u.class_id <> 0
                              AND u.id not in (select rl.user_id from remarketing_logins rl)
                             AND u.combination_id = m.combination_id
                             AND NVL(u.dynamic_param,0) = NVL(m.dynamic_param,0)
                       GROUP  BY
                             u.dynamic_param, u.combination_id) rem_users
                                              on NVL(u.dynamic_param,0) = NVL(rem_users.dynamic_param,0)
                                              and u.combination_id = rem_users.combination_id

                     LEFT JOIN
                     (SELECT
                          cl.dynamic_param,
                          cl.combination_id,
                          count(*) as clicks_num
                       FROM
                           clicks cl,
                           marketing_google_users_dp m
                       WHERE cl.time_created >= trunc(date_of_data ) and cl.time_created < trunc(date_of_data + 1)
                             AND cl.combination_id = m.combination_id
                             AND NVL(cl.dynamic_param,0) = NVL(m.dynamic_param,0)
                       GROUP BY cl.dynamic_param, cl.combination_id) clk
                                                  on NVL(u.dynamic_param,0) = NVL(clk.dynamic_param ,0)
                                                  and u.combination_id = clk.combination_id

                      LEFT JOIN
                     (SELECT
                          /*+ index(t TRANSACTIONS_IDX_TIME_C) */
                          u.dynamic_param,
                          u.combination_id,
                          SUM(t.amount*t.rate/100) as sum_deposits
                       FROM
                           users u,
                           transactions t,
                           transaction_types tt,
                           marketing_google_users_dp m
                       WHERE
                           u.id = t.user_id
                           AND t.type_id = tt.id
                           AND tt.class_type =  1
                           AND t.status_id IN ( 2,7,8 )
                           AND u.class_id <> 0
                           AND u.combination_id = m.combination_id
                           AND NVL(u.dynamic_param,0) = NVL(m.dynamic_param,0)
                           AND t.time_created >= trunc(date_of_data ) and t.time_created < trunc(date_of_data + 1)
                       GROUP BY u.dynamic_param, u.combination_id) sums_transactions
                                                 on NVL(u.dynamic_param,0) = NVL(sums_transactions.dynamic_param ,0)
                                                 and u.combination_id = sums_transactions.combination_id

                       LEFT JOIN
                      (SELECT
                          /*+ index(i INVESTMENTS_TIME_CREATED) */
                          u.dynamic_param,
                          u.combination_id,
                          SUM(i.amount*i.rate/100) as turnover,
                          SUM(i.house_result*i.rate/100) as house_win
                       FROM
                          users u,
                          investments i,
                          marketing_google_users_dp m
                       WHERE
                          u.id = i.user_id
                          AND i.is_settled = 1
                          AND i.is_canceled = 0
                          AND u.class_id <> 0
                          AND u.combination_id = m.combination_id
                          AND NVL(u.dynamic_param,0) = NVL(m.dynamic_param,0)
                          AND i.time_created >= trunc(date_of_data ) and i.time_created < trunc(date_of_data + 1)
                       GROUP BY u.dynamic_param, u.combination_id) sums_investments
                                                 on NVL(u.dynamic_param,0) = NVL(sums_investments.dynamic_param,0)
                                                 and u.combination_id = sums_investments.combination_id

               WHERE
                    reg_users.registered_users is not null
                    OR dep_users.reg_frst_dep_users is not null
                    OR rem_users.frst_rem_dep_users is not null
                    OR dep_users.frst_dep_users is not null
                    OR sums_transactions.sum_deposits is not null
                    OR sums_investments.turnover is not null
                    OR sums_investments.house_win is not null
                    OR clk.clicks_num is not null
               ) loop

           -- Insert row.
           insert into Marketing_Google_Report
           values (trunc(date_of_data ),
                   v_data.dynamic_param,
                   v_data.combination_id,
                   v_data.clicks_num,
                   v_data.registered_users,
                   v_data.reg_frst_dep_users,
                   v_data.frst_dep_users,
                   v_data.sum_deposits,
                   v_data.turnover,
                   v_data.house_win,
                   v_data.frst_rem_dep_users,
                   v_data.is_google_user  );

            rows_count := rows_count + 1;
       end loop;

       commit;
       Dbms_Output.put_line('There were ' || rows_count || ' rows added for ' || date_of_data);
  ELSE
       Dbms_Output.put_line('ERROR! There is already an existing data for ' || date_of_data);
  END IF;
END UPDATE_MARKETING_GOOGLE_DAY;
------------------------------------------------------------------------------------------------------------------------------------------------
-- End Jamal 19.12.2012 DEV-190
------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------
-- Eyal O -- dev-1601 - conversion to transactions_bins table
-- 17.07.13

create or replace
PROCEDURE UPDATE_BUSSINESS_TRAN_BINS AS 
counter NUMBER := 0;
last_bin NUMBER:= -1;
last_time_last_failed_reroute date:=null;
BEGIN
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
DBMS_OUTPUT.PUT_LINE('UPDATE_BUSSINESS_TRANSACTIONS_BINS started at: ' || sysdate);
FOR v_data IN (
                                      SELECT 
                                         tb.from_bin,
                                         s.business_case_id,
                                       MAX(t.time_created) as time_created,
                                       MAX(tb.time_last_failed_reroute) as time_last_failed_reroute
                                       FROM
                                          transactions_bins tb,
                                          credit_cards cc,
                                          transactions t,
                                          users u,
                                          skins s
                                      WHERE
                                         tb.from_bin = cc.bin
                                         AND cc.id = t.credit_card_id
                                         AND t.user_id = u.id
                                         AND u.skin_id = s.id
                                         AND t.status_id in (2, 7, 8)
                                         AND t.type_id = 1
                                      GROUP BY 
                                         tb.from_bin,
                                         s.business_case_id
                                      order by tb.from_bin, time_created DESC
                                      ) 
                                      LOOP
                                            IF (last_bin  !=  v_data.from_bin) THEN
                                                    UPDATE
                                                      transactions_bins tran_b
                                                    SET
                                                      tran_b.business_case_id = v_data.business_case_id
                                                    WHERE
                                                      tran_b.from_bin = v_data.from_bin;
                                                                
                                                    IF (v_data.business_case_id = 1) THEN
                                                              UPDATE
                                                                transactions_bins tran_b
                                                              SET
                                                                tran_b.time_last_failed_reroute = null
                                                              WHERE
                                                                tran_b.from_bin = v_data.from_bin;

                                                    END IF;            
                                            ELSE 
                                                    IF (v_data.business_case_id = 1) THEN
                                                              last_time_last_failed_reroute:=null;
                                                    END IF;
                                                    
                                                    insert into TRANSACTIONS_BINS( id, from_bin, time_last_success, time_last_failed_reroute, updated_by_rerouting, business_case_id) 
                                                            values (SEQ_TRANSACTIONS_BINS.nextval , v_data.from_bin, v_data.time_created, last_time_last_failed_reroute, 1, v_data.business_case_id);
                                            END IF;        
                                           
                                           last_bin:=v_data.from_bin;
                                           last_time_last_failed_reroute:=v_data.time_last_failed_reroute;
                                           counter := counter + 1;
                                      END LOOP;
                                      
FOR v_data_2 IN (
                                      SELECT 
                                         tb.from_bin,
                                         s.business_case_id,
                                       MAX(t.time_created) as time_created
                                       FROM
                                          transactions_bins tb,
                                          credit_cards cc,
                                          transactions t,
                                          users u,
                                          skins s
                                      WHERE
                                         tb.from_bin = cc.bin
                                         AND cc.id = t.credit_card_id
                                         AND t.user_id = u.id
                                         AND u.skin_id = s.id
                                         AND t.status_id   not in (2, 7, 8)
                                         AND t.type_id = 1
                                         AND tb.business_case_id is null
                                      GROUP BY 
                                         tb.from_bin,
                                         s.business_case_id
                                      order by tb.from_bin, time_created 
                                      ) 
                                      LOOP                                            
                                            UPDATE
                                              transactions_bins tran_b
                                            SET
                                              tran_b.business_case_id = v_data_2.business_case_id
                                            WHERE
                                              tran_b.from_bin = v_data_2.from_bin;
                                        
                                             counter := counter + 1;
                                      END LOOP;                                      
   
DBMS_OUTPUT.PUT_LINE('counter  = ' || counter);                                   
DBMS_OUTPUT.PUT_LINE('UPDATE_BUSSINESS_TRANSACTIONS_BINS finished at: ' || sysdate);
DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');                                    
  NULL;
END UPDATE_BUSSINESS_TRAN_BINS;

------------------------------------------------------------------