package thunderBonusStates;

/**
 * @author liors
 *
 */
public enum BonusBucketCase {
	LEAVE_BONUS
	, LOW_BALANCE
	, ENOUGH_WAGERING
	, ATTENTION;
}
