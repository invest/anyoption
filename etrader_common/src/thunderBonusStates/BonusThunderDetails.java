package thunderBonusStates;

import com.anyoption.common.beans.base.Currency;

/**
 * @author liors
 *
 */
public class BonusThunderDetails {
	private int userId;
	private int bonusUsersId;
	private int skin;
	private Currency currency;
	private long balance;
	private int state;
	private BonusBucketCase bucketCase;

	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBonusUsersId() {
		return bonusUsersId;
	}
	public void setBonusUsersId(int bonusUsersId) {
		this.bonusUsersId = bonusUsersId;
	}
	public int getSkin() {
		return skin;
	}
	public void setSkin(int skin) {
		this.skin = skin;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

	public BonusBucketCase getBucketCase() {
		return bucketCase;
	}

	public void setBucketCase(BonusBucketCase bucketCase) {
		this.bucketCase = bucketCase;
	}
}
