package timeZone;

import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * @author liors
 *
 */
public class TimeZoneChecker extends JobUtil {
	public static void main(String[] args ) {	
		/*TimeZone timeZone = TimeZone.getTimeZone("Europe/Moscow");
		Calendar calendar = new GregorianCalendar();
		long time = calendar.getTimeInMillis();
		System.out.println("time GMT - " + time);
		System.out.println("time GMT - " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));

		calendar.setTimeZone(timeZone);
		long timeMo = calendar.getTimeInMillis();
		System.out.println("timeMo - " + timeMo);
		System.out.println("Europe/Moscow time - " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));*/
		System.out.println("Are u Redy?  So press enter...");
		try {
			System.in.read();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(args.length != 1 || args == null) {
			System.out.println("ERROR! file not found. Proper Usage is: java program filename.");
            return;
        }
        propFile = args[0];
        
		System.out.println("\n\n--------------------------------------------\n");
		System.out.println("From database:\n\n");
		String sql = "select distinct time_zone from opportunity_templates order by time_zone";
		
		try {
			Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String timeZoneDB = rs.getString("time_zone");
				Calendar calendarDB = new GregorianCalendar();
				calendarDB.setTimeZone(TimeZone.getTimeZone(timeZoneDB));
				long timeDB = calendarDB.getTimeInMillis();
				System.out.println("time " + timeZoneDB + " milis - " + timeDB);
				System.out.println(timeZoneDB + " Hour time - " + calendarDB.get(Calendar.HOUR_OF_DAY));
				System.out.println(timeZoneDB + " Minute time - " + calendarDB.get(Calendar.MINUTE));
				System.out.println();
			}			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}		
}
