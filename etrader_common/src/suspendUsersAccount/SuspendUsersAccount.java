package suspendUsersAccount;

import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class SuspendUsersAccount extends JobUtil {
	private static Logger log = Logger.getLogger(SuspendUsersAccount.class);
	private static final int DAYS_TO_SEND_DOCUMENTS = 30;
	
	public static void main (String[] args) {
		int numOfRowsUpdated = 0;
		
		if(args.length != 1) {
	        log.error("ERROR! file not found. Proper Usage is: java program filename.");
	        System.exit(0);
	    }
		propFile = args[0];
		
		log.info("Starting Suspend Users Account Job.");
		
		try {
			Connection connection = getConnection();
			numOfRowsUpdated = suspendAccounts(connection);
		} catch (SQLException e) {
			log.error("ERROR! Suspend Users Account Job Failed.");
			e.printStackTrace();
		}
		log.info("Suspend Users Account Job Updated " + numOfRowsUpdated + " Rows.");		
	}
	
	/**
	 * @param connection
	 * This method suspend users account by a specific conditions. 
	 * @return
	 * @throws SQLException
	 */
	private static int suspendAccounts(Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        int numOfRowsUpdated = 0;
        try {
            String sql =" UPDATE " +
            			" 	users_regulation ur " +
            			" SET " +
            			" 	ur.is_suspended = 1, " +
            			" 	ur.suspended_reason = ?, " +
            			" 	ur.time_suspended_regulation = sysdate " +
            			" WHERE EXISTS " +
    					" ( " +
						" 	SELECT " +
						" 		u.id " +
						" 	FROM " +
						" 		users u, " +
						" 		skins s," +
						"		transactions t " +
						" 	WHERE " +
						" 		ur.user_id = u.id and " +
						"  		u.skin_id = s.id and " +
						"		t.user_id = u.id and " +
						"		u.first_deposit_id = t.id and" +
						" 		s.is_regulated = 1 and " +
						" 		(sysdate - " + DAYS_TO_SEND_DOCUMENTS + ")  > t.time_created and " +
						" 		u.id_doc_verify = 0 ";
            
            if (getPropertyByFile("user.id", null) != null) {
				sql+=	" 		and u.id = ? ";
            }
            sql += 		" ) ";
                    
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1,getPropertyByFile("suspended.reason"));
            if (getPropertyByFile("user.id", null) != null) {
            	pstmt.setString(2,getPropertyByFile("user.id"));
            	log.info("User id was found");
            }
            numOfRowsUpdated = pstmt.executeUpdate();

        } finally {
        	closeStatement(pstmt);
        }
        return numOfRowsUpdated;
	}
	
	/**
	 * Not in use and NOT DONE.
	 * Return users id. The users that need to be suspended.
	 * 
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	private static ArrayList<Long> getUserToSuspend(Connection connection) throws SQLException {
		ArrayList<Long> usersId = new ArrayList<Long>();
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =" SELECT " +
    					"	 u.id " +
    					" FROM " +
						" 	users u," +
						" 	skins s" +
						" WHERE " +
						"	u.skin_id = s.id and " +
						"	s.is_regulated = 1 and " +
						" 	(sysdate - " + DAYS_TO_SEND_DOCUMENTS +")  > u.time_created and " +
						" 	u.id_doc_verify = 0 ";
                    
            pstmt = connection.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
               usersId.add(rs.getLong("u.id"));
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(pstmt);
        }
        
		return usersId;	
	}
	
	/**
	 * Not in use and NOT DONE.
	 * Update users_regulation table to suspend or not the users.
	 * 
	 * @param connection
	 * @throws SQLException
	 */
	public static void updateSuspended(Connection connection) throws SQLException {
		
		PreparedStatement pstmt = null;
		try {
        String sql =" UPDATE " +
    		        " 	users_regulation " +
    		        " SET " +
    		      	" 	is_suspend = ?, " +
    		        " 	suspended_reason = ?, " +
    		        " 	time_suspended_regulation = sysdate " +
    		        " WHERE " +
    		        "	user_id = ? ";
        
		}	finally {
			closeStatement(pstmt);
		}
	}
}