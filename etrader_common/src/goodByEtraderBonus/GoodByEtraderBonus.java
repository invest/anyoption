package goodByEtraderBonus;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.ConstantsBase;


/**
 * @author liors
 *
 *BAC-210
 *Terminating Bonuses for etrader
 *For all users that have bonuses that are in state "granted" at time of deploy should become "canceled".
 *Make sure no emails are triggered to the customer following this action!
 *Please create a new issue "issue.action.Bonus.cancellation.regulation" and insert it for all these bonuses we cancel
 */

public class GoodByEtraderBonus {
	private static final Logger logger = Logger.getLogger(GoodByEtraderBonus.class);
	private static final String LOG_PREFIX_MSG = "Terminating Bonuses for etrader; ";
	
	/**
	 * @param user
	 * @param connection
	 */
	private static void cancelBonusUser(BonusUsers user, Connection connection) {
		PreparedStatement ps = null;
    	try {
    		String sql = 
    				"UPDATE " +
    				"	bonus_users bu " +
    				"SET " +
    				"	bonus_state_id = " + ConstantsBase.BONUS_STATE_CANCELED + " " + 
    				"	,time_canceled = sysdate " +
    				"	,WRITER_ID_CANCEL =  0 " +
    				"WHERE " +
    				"	bu.id = ? ";
    		ps = connection.prepareStatement(sql);
    		ps.setLong(1, user.getId());
			ps.executeUpdate();    		
    	} catch (Exception e) {
			logger.error(LOG_PREFIX_MSG + "ERROR! cancelBonusUser", e);
		} finally {
			JobUtil.closeStatement(ps);
		}
	}

	/**
	 * @param connection
	 * @param isLiveUsersOnly
	 * @param specificUserId
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<BonusUsers> getBonusUsersToCancel(Connection connection, boolean isLiveUsersOnly, String specificUserId) throws Exception {
		ArrayList<BonusUsers> bonusUsers = new ArrayList<BonusUsers>();		
		PreparedStatement ps = null;
    	ResultSet rs = null;
  
    	try {
    		String sql = 
    				"SELECT " +
    				"	* " +
    				"FROM " +
    				"	bonus_users bu " +
    				"WHERE " +
    				"	EXISTS " +
    				"	(" +
    				"		SELECT" +
    				"			1" +
    				"		FROM" +
    				"			users u" +
    				"		WHERE" +
    				"			u.id = bu.user_id" +
    				"			AND u.skin_id = " + Skin.SKIN_ETRADER_INT + " " +
    				"			AND bonus_state_id = " + ConstantsBase.BONUS_STATE_GRANTED + " ";
    				
					//run on specific user
					if (!CommonUtil.isParameterEmptyOrNull(specificUserId)) {
						sql +=	" 	AND u.id = ? ";
					}
		    		if (isLiveUsersOnly) {
		    			sql +=  " 	AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST + " ";
		    		}
    		
    				sql += "	)";   
  
			ps = connection.prepareStatement(sql);
    		if (!CommonUtil.isParameterEmptyOrNull(specificUserId)) {
    			ps.setString(1, specificUserId);
    		}	
			rs = ps.executeQuery();
    		while (rs.next()) {   			
    			BonusUsers bonusUser = BonusDAOBase.getBonusUsersVO(rs, connection);
    			bonusUsers.add(bonusUser);
    		}
    	} catch (Exception e) {
			logger.error(LOG_PREFIX_MSG + "ERROR! getBonusUsersToCancel", e);
			throw new Exception(e);
		} finally {
			JobUtil.closeResultSet(rs);
			JobUtil.closeStatement(ps);
		}		
		return bonusUsers;
	}

	/**
	 * @param connection
	 * @param issueActionComment
	 * @param isLiveUsers
	 * @param specificUserId
	 * @throws Exception 
	 */
	public static void doJob(Connection connection, String issueActionComment, boolean isLiveUsers, String specificUserId) throws Exception {
    	//get relevant bonus users
    	ArrayList<BonusUsers> bonusUsers = GoodByEtraderBonus.getBonusUsersToCancel(connection, isLiveUsers, specificUserId);
		
		for (BonusUsers bonusUser : bonusUsers) {
			logger.info(LOG_PREFIX_MSG + " About to terminate: " + " bonusUserId: "  + bonusUser.getId() + "; To userId: " + bonusUser.getUserId());
    		//Insert issue 	    		
	    	IssuesManagerBase.insertIssue(
	    			connection,String.valueOf(IssuesManagerBase.ISSUE_SUBJECTS_BONUS), 
	    			String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED),
	    			bonusUser.getUserId(), 
	    			String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_MEDIUM),
	    			ConstantsBase.ISSUE_TYPE_REGULAR, 
	    			String.valueOf(IssuesManagerBase.ISSUE_ACTION_BONUS_CANCELLATION_REGULATION), 
	    			Writer.WRITER_ID_AUTO, false,
	    			new Date(),
	    			ConstantsBase.OFFSET_GMT, 
	    			issueActionComment);
	    	
	    	//cancel bonus
	    	cancelBonusUser(bonusUser, connection);
		}
	}
}
