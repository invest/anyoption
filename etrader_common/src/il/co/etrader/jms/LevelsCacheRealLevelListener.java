//package il.co.etrader.jms;
//
///**
// * Interface to be implemented by classes interested in opportunity real level
// * updates.
// * 
// * @author Tony
// */
//public interface LevelsCacheRealLevelListener {
//    /**
//     * Notify about opportunity real level change.
//     * 
//     * @param oppId the id of the opportunity which real level have changed
//     * @param realLevel the new real level of the opportunity
//     */
//    public void update(long oppId, double realLevel);
//}