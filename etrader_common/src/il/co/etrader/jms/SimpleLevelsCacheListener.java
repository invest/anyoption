//package il.co.etrader.jms;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import com.anyoption.common.beans.BinaryZeroOneHundred;
//
//public interface SimpleLevelsCacheListener {
//    /**
//     * Notify about "COMMAND" mode subscription event (command).
//     *
//     * @param marketId the id of the market for which this update is for
//     * @param scheduled the schedule for which this updates is for
//     * @param update the update
//     */
//    public void aoTPSUpdate(long marketId, int scheduled, HashMap<String, String> update);
//
//    /**
//     * Notify about "COMMAND" mode subscription event (command).
//     *
//     * @param skinId the id of the skin for which control connection this update is for
//     * @param update the update
//     */
//    public void aoCtrlUpdate(long skinId, HashMap<String, String> update);
//
//    /**
//     * Notify about ao state subscription event (market open/close).
//     *
//     * @param update
//     */
//    public void aoStateUpdate(HashMap<String, String> update, long marketId);
//
//    /**
//     * Option Plus update.
//     *
//     * @param marketId
//     * @param update
//     */
//    public void optionPlusUpdate(long marketId, HashMap<String, String> update);
//
//    /**
//     * binary Zero One Hundred Update.
//     *
//     * @param marketId
//     * @param opportunityTypeId
//     * @param update
//     */
//    public void binaryZeroOneHundredUpdate(long marketId, long opportunityTypeId, HashMap<String, String> update);
//
//    /**
//     * Live Globe update.
//     * 
//     * @param itemName
//     * @param update
//     */
//    public void liveGlobeUpdate(String itemName, Map<String, String> update);
//    
//    /**
//     * Live Globe update.
//     * 
//     * @param itemName
//     * @param update
//     */
//    public void liveInvestmentsUpdate(String itemName, Map<String, String> update);
//    
//    /**
//     * Live Globe update.
//     * 
//     * @param itemName
//     * @param update
//     */
//    public void liveTrendsUpdate(String itemName, Map<String, String> update);
//    
//    /**
//     * tradder tool updates
//     *
//     * @param autoShiftParameter
//     * @param shiftParameter
//     * @param shiftParameterAO
//     * @param calls volume bougth (in $)
//     * @param puts volume sold (in $)
//     * @param maxExposure
//     * @param contractsBought contracts Bought (binary 0-100) (in $)
//     * @param contractsSold contracts Sold (binary 0-100) (in $)
//     * @param parameters binary 0-100 parameters
//     */
//    public void ttUpdate(long opportunityId,
//    		double autoShiftParameter,
//    		double shiftParameter,
//    		double shiftParameterAO,
//    		double calls,
//    		double puts,
//    		long maxExposure,
//    		String feedName,
//    		long decimalPoint,
//    		boolean isTraderDisable,
//    		boolean isAutoDisable,
//    		HashMap<String, String> update,
//    		boolean notClosed);
//
//    /**
//     * tradder tool Binary0100 updates
//     *
//     * @param autoShiftParameter
//     * @param shiftParameter
//     * @param shiftParameterAO
//     * @param calls volume bougth (in $)
//     * @param puts volume sold (in $)
//     * @param maxExposure
//     * @param contractsBought contracts Bought (binary 0-100) (in $)
//     * @param contractsSold contracts Sold (binary 0-100) (in $)
//     * @param parameters binary 0-100 parameters
//     */
//    public void ttBinary0100Update(long opportunityId,
//    		double autoShiftParameter,
//    		double shiftParameter,
//    		double shiftParameterAO,
//    		double calls,
//    		double puts,
//    		long maxExposure,
//    		String feedName,
//    		long decimalPoint,
//    		boolean isTraderDisable,
//    		boolean isAutoDisable,
//    		HashMap<String, String> update,
//    		double contractsBought,
//    		double contractsSold,
//    		BinaryZeroOneHundred parameters,
//    		double level,
//    		boolean notClosed);
//
//}