//package il.co.etrader.jms;
//
//import java.io.Serializable;
//
///**
// * A message published by LevelService and received from LevelsCache.
// * This message brings notification about investment.
// *
// * @author Tony
// */
//public class InvestmentMessage implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    public long oppId;
//    public long invId;
//    public double amount;
//    public long type;
//
//    /**
//     * Create a message.
//     *
//     * @param oppId the opportunity on which the investment was made
//     * @param invId the investment id
//     * @param amount the investment amount (in cents)
//     * @param type the investment type (1 - call/2 - put)
//     */
//    public InvestmentMessage(long oppId, long invId, double amount, long type) {
//        this.oppId = oppId;
//        this.invId = invId;
//        this.amount = amount;
//        this.type = type;
//    }
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "InvestmentMessage:" + ls +
//            "oppId: " + oppId + ls +
//            "invId: " + invId + ls +
//            "amount: " + amount + ls +
//            "type: " + type + ls;
//    }
//}