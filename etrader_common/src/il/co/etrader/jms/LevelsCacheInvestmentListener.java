//package il.co.etrader.jms;
//
///**
// * Interface to be implemented by classes interested in investments
// * notifications.
// *
// * @author Tony
// */
//public interface LevelsCacheInvestmentListener {
//    /**
//     * Notify the listener about investment made.
//     *
//     * @param oppId the opportunity on which the investment was made
//     * @param invId the investment id
//     * @param amount the investment amount (in cents)
//     * @param type the investment type (1 - call/2 - put)
//     */
//    public void update(long oppId, long invId, double amount, long type);
//}