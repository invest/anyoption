package il.co.etrader.rewardsTierDemotion;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.daos.RewardTiersDAO;
import com.anyoption.common.daos.RewardUsersDAO;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.managers.RewardUsersManager;

/**
 * @author EyalG
 *
 */
public class RewardsTierDemotion extends JobUtil {
	private static Logger log = Logger.getLogger(RewardsTierDemotion.class);

	public static void main(String[] args) throws Exception {
		if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
		propFile = args[0];
		String userIds = getPropertyByFile("users", null);
		log.info("RewardsTierDemotion Job started. user ids = " + userIds );
		double newExperiencePoints;
		double pointsToReduce;
		boolean shouldDemot;
		int tier;
		ArrayList<RewardsUsers> rewardsUsers = null;
		Connection conn = null;
		try {
			conn = getConnection();
			rewardsUsers = getAllNoneActiveUsers(conn, userIds);
		} catch (Exception e) {
            log.log(Level.ERROR, "Error while RewardsTierDemotion.", e);
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
		log.info("rewardsUsers found: " + rewardsUsers.size());
		for (RewardsUsers rewardsUser : rewardsUsers) {
			newExperiencePoints = rewardsUser.getRewardUser().getExperiencePoints() - rewardsUser.getRewardTier().getExperiencePointsToReduce();
			pointsToReduce = rewardsUser.getRewardTier().getExperiencePointsToReduce();
			shouldDemot = false;
			tier = rewardsUser.getRewardUser().getTierId();
			log.info("rewardsUser: " + rewardsUser.toString());
			if (newExperiencePoints < rewardsUser.getRewardTier().getMinExperiencePoints()) { //should move tier
				if (rewardsUser.getRewardTier().getDaysToDemote() > 0 && rewardsUser.getDaysWithoutActivity() % rewardsUser.getRewardTier().getDaysToDemote() == 0 && rewardsUser.getMaxTier() == tier) { //can move tier
					shouldDemot = true;
					tier -= 1;
				} else { //cant move tier
					pointsToReduce = rewardsUser.getRewardUser().getExperiencePoints() - rewardsUser.getRewardTier().getMinExperiencePoints();
					newExperiencePoints = rewardsUser.getRewardTier().getMinExperiencePoints();
				}
			}
			log.info("rewardsUser new status: pointsToReduce = " + pointsToReduce + " newExperiencePoints = " + newExperiencePoints + " shouldDemot = " + shouldDemot + " tier = " + tier);
			if (pointsToReduce > 0) { //else - no need to do anything
				updateRewardsUser(rewardsUser, pointsToReduce, newExperiencePoints, shouldDemot, tier);
			}
		}
		
	    
		log.info("RewardsTierDemotion Job ended.");
	}
	
	private static void updateRewardsUser(RewardsUsers rewardUser, double pointsToReduce, double userExpPoint,
			boolean shouldDemot, int tier) {
		Connection conn = null;    	
    	try {
    		conn = getConnection();
    		conn.setAutoCommit(false);
    		RewardUsersManager.updateUserExperiencePoints(conn, rewardUser.getRewardUser(), userExpPoint, Writer.WRITER_ID_AUTO, BonusManagerBase.class, "Rewards tier demotion job", RewardUsersHistoryManager.REDUCE_EXPERIENCE_POINTS);
    		updateUserLastReducedPoints(conn, rewardUser.getRewardUser().getUserId());
//			updateUser(conn, rewardUser.getRewardUser().getUserId(), userExpPoint, tier);
//			RewardUserHistory rewardUserHistory = new RewardUserHistory();
//			rewardUserHistory.setBalanceExperiencePoints(userExpPoint);
//			rewardUserHistory.setRewardHistoryTypeId(RewardUsersHistoryManager.REDUCE_EXPERIENCE_POINTS);
//			rewardUserHistory.setRewardTierId(tier);
//			rewardUserHistory.setUserId(rewardUser.getRewardUser().getUserId());
//			rewardUserHistory.setWriterId(Writer.WRITER_ID_AUTO);
//			rewardUserHistory.setAmountExperiencePoints(-pointsToReduce);
//			RewardUsersHistoryDAO.insertValueObject(conn, rewardUserHistory);
//			if (shouldDemot) {
//				rewardUserHistory.setRewardHistoryTypeId(RewardUsersHistoryManager.DEMOTE_TIER);
//				rewardUserHistory.setAmountExperiencePoints(0);
//				RewardUsersHistoryDAO.insertValueObject(conn, rewardUserHistory);
//				RewardUsersHistoryDAO.updateHistoryRemoveRewards(conn, rewardUser.getRewardUser().getUserId(), tier, Writer.WRITER_ID_AUTO);
//				RewardUserBenefitsDAO.updateRemoveRewards(conn, rewardUser.getRewardUser().getUserId(), tier);				
//			}
			conn.commit();
    	} catch (Exception e) { 		
			log.error("can't update user id " + rewardUser.getRewardUser().getUserId() + " pointsToReduce " + pointsToReduce + " userExpPoint " + userExpPoint + " shouldDemot " + shouldDemot + " tier " + tier, e);
			try {
				conn.rollback();
			} catch (SQLException ie) {
				log.error("Can't rollback.", ie);
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't AutoCommit true", e);
			}
			BaseBLManager.closeConnection(conn);
		}
	}

	private static ArrayList<RewardsUsers> getAllNoneActiveUsers(Connection conn, String userIds) throws SQLException {
		PreparedStatement ps = null;
	    ResultSet rs = null;
	    ArrayList<RewardsTierDemotion.RewardsUsers> list = new ArrayList<RewardsTierDemotion.RewardsUsers>();
	    try {
	        String sql = "SELECT " +
						  	"ru.id as ru_id, " +
			                "ru.exp_points, " +
			                "ru.rwd_tier_id, " +
			                "ru.time_last_activity, " +
			                "ru.time_last_reduced_points, " +
			                "ru.user_id, " +
							"rt.*, " +
							"sysdate - ru.time_last_activity as days_without_activity, " +
							"(SELECT " +
								"MAX(ruh.rwd_tier_id) " +
							 "FROM " +
							 	"rwd_users_history ruh " +
							 "WHERE " +
							 	"ruh.user_id = u.id" +
							 ") as max_tier " +
						  "FROM " +
							"users u, " +
							"rwd_users ru, " +
							"rwd_tiers rt " +
						  "WHERE " +
							"u.id = ru.user_id AND " +
							"ru.rwd_tier_id > 0 AND " +
							"rt.id = ru.rwd_tier_id AND " +
							"NOT (ru.rwd_tier_id = 1 AND " +
				            "ru.exp_points = rt.min_exp_points) AND " +
							"ru.time_last_activity < (sysdate - rt.in_active_days) + 1 AND " +
							"(ru.time_last_reduced_points < sysdate - rt.in_active_days OR " +
							"time_last_reduced_points is null) AND  " +
							"NOT EXISTS (SELECT " +
							                "1 " +
							            "FROM " +
							                "investments i " + 
							            "WHERE " +
							                "i.user_id = u.id AND " +
							                "i.is_settled = 0 AND " +
							                "i.is_canceled = 0 " +
							            ") ";
				if (null != userIds) {
					sql += " AND ru.user_id IN (" + userIds + ")";
				}
		    ps = conn.prepareStatement(sql);
		
		    rs = ps.executeQuery();
		    while (rs.next()) {
		    	RewardsTierDemotion.RewardsUsers u = new RewardsTierDemotion.RewardsUsers();
		    	u.setRewardTier(RewardTiersDAO.getVO(rs));
		    	u.setRewardUser(RewardUsersDAO.getRewardUserValueObject(rs, "ru_id"));
		    	u.setDaysWithoutActivity(rs.getInt("days_without_activity"));
		    	u.setMaxTier(rs.getInt("max_tier"));
		        list.add(u);
		    }
			
		} finally {
		    closeResultSet(rs);
		    closeStatement(ps);
		}
		return list;
	}

	private static ArrayList<BonusUsers> getUserBonusesToCancel(Connection conn, long userId,
			int tier) throws SQLException {
		PreparedStatement ps = null;
	    ResultSet rs = null;
	    ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
	    try {
	        String sql = "SELECT " +
						 	 "bu.* " +
						 "FROM " +
							 "rwd_benefits rb, " +
							 "rwd_user_benefits rub, " +
							 "rwd_users ru, " +
							 "users u, " +
							 "bonus_users bu, " +
							 "bonus_types bt " +
						 "WHERE " +
							 "ru.user_id = rub.user_id AND " +
							 "rub.user_id = ? and " +
							 "rub.rwd_benefit_id = rb.id and " +
							 "rb.rwd_tier_id = ? and " +
							 "rb.is_affected_from_demotion = 1 and " +
							 "rub.is_active = 1 and " +
							 "bu.bonus_id = rb.bonus_id and " + 
							 "bu.user_id = ru.user_id and " +
							 "bt.id = bu.type_id AND " +
				  		 	 "(bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_GRANTED + " OR " +
				  		 	 "(bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND " +
				  		 	 "bt.is_can_cancel_on_active = 1)) AND " +
							 "u.id = ru.user_id ";
		    ps = conn.prepareStatement(sql);
		    
		    ps.setLong(1, userId);
		    ps.setLong(2, tier);        
		
		    rs = ps.executeQuery();
		    while (rs.next()) {
		        list.add(BonusDAOBase.getBonusUsersVO(rs, conn));
		    }
		
		} finally {
		    closeResultSet(rs);
		    closeStatement(ps);
		}
	    return list;
	}



	private static void updateHistoryRemoveRewards(Connection conn, long userId, int tier) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                    "INSERT INTO rwd_users_history " +
				    "( " +
					      "rwd_action_id, " +
					      "time_created, " +
					      "user_id, " +
					      "comments, " +
					      "rwd_action_type_id, " +
					      "id, " +
					      "reference_id, " +
					      "rwd_his_type_id, " +
					      "writer_id, " +
					      "rwd_tier_id, " +
					      "balance_exp_points " +
				    ") " +
					"SELECT " + 
					  "rub.id, " +
					  "sysdate, " +
					  "rub.user_id, " +
					  "null, " +
					  RewardManager.BENEFIT + ", " +
					  "seq_rwd_users_history.nextval, " +
					  "null, " +
					  RewardUsersHistoryManager.REWARD_REMOVED + ", " +
					  Writer.WRITER_ID_AUTO + ", " +
					  "ru.rwd_tier_id, " +
					  "ru.exp_points " +
					"FROM " +
					  "rwd_benefits rb, " +
					  "rwd_user_benefits rub, " +
					  "rwd_users ru " +
					"WHERE " +
					  "ru.user_id = rub.user_id AND " +
					  "rub.user_id = ? AND " +
					  "rub.rwd_benefit_id = rb.id AND " +
					  "rb.rwd_tier_id = ? AND " +
					  "rb.is_affected_from_demotion = 1 AND " +
					  "rub.is_active = 1 AND " +
					  "rb.bonus_id is null";
            
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, tier);
            pstmt.executeUpdate();
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }	
	}
	
	private static void updateUserLastReducedPoints(Connection conn, long userId) throws SQLException {
		PreparedStatement ps = null;

        try {
            String sql = "UPDATE " +
            			 	"rwd_users " +
						 "SET " +
						 	"time_last_reduced_points = sysdate " +
						 "WHERE " +
						 	"user_id = ? ";
            ps = conn.prepareStatement(sql);
            int index = 1;
            ps.setLong(index++, userId);
            
            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
		
	}	
	
	static class UsersBonus {
    	private BonusUsers bonusUsers;
    	private UserBase user;
		
    	public UsersBonus() {
			this.bonusUsers = new BonusUsers();
			this.user = new UserBase();
		}

		public BonusUsers getBonusUsers() {
			return bonusUsers;
		}
    	
		public void setBonusUsers(BonusUsers bonusUsers) {
			this.bonusUsers = bonusUsers;
		}
		
		public UserBase getUser() {
			return user;
		}
		
		public void setUser(UserBase user) {
			this.user = user;
		}
	}
	
	static class RewardsUsers {
		private RewardTier rewardTier;
		private RewardUser rewardUser;
		private int daysWithoutActivity;
		private int maxTier;
		
		public RewardTier getRewardTier() {
			return rewardTier;
		}
		
		public void setRewardTier(RewardTier rewardTier) {
			this.rewardTier = rewardTier;
		}

		public RewardUser getRewardUser() {
			return rewardUser;
		}

		public void setRewardUser(RewardUser rewardUser) {
			this.rewardUser = rewardUser;
		}

		public int getDaysWithoutActivity() {
			return daysWithoutActivity;
		}
		
		public void setDaysWithoutActivity(int daysWithoutActivity) {
			this.daysWithoutActivity = daysWithoutActivity;
		}
		
		public int getMaxTier() {
			return maxTier;
		}
		
		public void setMaxTier(int maxTier) {
			this.maxTier = maxTier;
		}

		@Override
		public String toString() {
			return "RewardsUsers [rewardTier=" + rewardTier + ", rewardUser="
					+ rewardUser + ", daysWithoutActivity="
					+ daysWithoutActivity + ", maxTier=" + maxTier + "]";
		}
	}	
}
