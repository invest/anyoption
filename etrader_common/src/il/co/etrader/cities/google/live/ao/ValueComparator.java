package il.co.etrader.cities.google.live.ao;

import java.util.Comparator;
import java.util.HashMap;

public class ValueComparator implements Comparator<String> {
	HashMap<String, Long> base;
    public ValueComparator(HashMap<String, Long> userCities) {
        this.base = userCities;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}