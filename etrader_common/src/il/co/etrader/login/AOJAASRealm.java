package il.co.etrader.login;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Request;
import org.apache.catalina.deploy.SecurityConstraint;
import org.apache.catalina.realm.JAASRealm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AOJAASRealm extends JAASRealm{
	private static final Log log = LogFactory.getLog(AOJAASRealm.class);
	public static final String sessionUsername = "session_username";
	public static final String sessionPassword = "session_password";
	
	@Override
	public SecurityConstraint[] findSecurityConstraints(Request request,  Context context) {
	   HttpSession session = (HttpSession) request.getSession();
	   if(request.getPrincipal() == null ) {
		   String user = (String) session.getAttribute(sessionUsername);
		   String pass = (String) session.getAttribute(sessionPassword);
		   if(user != null && pass != null) {
		   	   try {
		   		   ((HttpServletRequest) request).login(user, pass);
				} catch (ServletException e) {
					 if( log.isDebugEnabled()) {
				            log.debug("Can't login user:" + user);
					 }
				}
		   }
	   }
	   return super.findSecurityConstraints(request, context);
	}
  
}
