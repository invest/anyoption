package il.co.etrader.login;


import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

public class JAASHelper {

	private static final Logger log = Logger.getLogger(JAASHelper.class);

	  LoginContext loginContext = null;

	  public JAASHelper() {
	  }

	  public boolean authenticate(String userid, String password) {
	    boolean result = false;
	    try {
	      loginContext = new LoginContext("ET",
	          new LoginCallback(userid, password));
	      log.info("about to login");
	      loginContext.login();
	      log.info("after to login");
	      result = true;
	    }
	    catch (LoginException e) {
	      // A production quality implementation would log this message
	      log.info("Exception login");
	      result = false;
	    }
	    return result;
	  }

	  public Subject getSubject () {
	    Subject result = null;
	    if (null != loginContext) {
	      result = loginContext.getSubject();
	    }
	    return result;
	  }

	  public static class LoginCallback implements CallbackHandler {
	    private String userName = null;
	    private String password = null;

	    public LoginCallback(String userName, String password) {
	      this.userName = userName;
	      this.password = password;
	    }

	    public void handle(Callback[] callbacks) {
	      for (int i = 0; i< callbacks.length; i++) {
	        if (callbacks[i] instanceof NameCallback) {
	          NameCallback nc = (NameCallback)callbacks[i];
	          nc.setName(userName);
	        } else if (callbacks[i] instanceof PasswordCallback) {
	          PasswordCallback pc = (PasswordCallback)callbacks[i];
	          pc.setPassword(password.toCharArray());
	        }
	      }
	    }
	  }
	}
