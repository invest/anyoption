package il.co.etrader.smsWhiteListJob;

import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * SMS whiteList job class.
 *
 * @author Kobi
 */
public class smsWhiteListJob extends JobUtil{
    private static Logger log = Logger.getLogger(smsWhiteListJob.class);
    private static StringBuffer report = new StringBuffer();

    private static final long SMS_STATUS_DELIVERED = 6;

    /**
     * update sms_white_list table
     *
     * @return list of users
     * @throws SQLException
     */
    private static void updateSmsWhiteList() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection("db.url","db.user","db.pass");

		    String sql =
		    	"SELECT " +
		    		"phone " +
		    	"FROM " +
		    		"sms " +
				"WHERE " +
					"sms_status_id = ? AND " +
					"trunc(time_sent) = trunc (sysdate-1) ";

		    ps = con.prepareStatement(sql);
		    ps.setLong(1, SMS_STATUS_DELIVERED);
			rs = ps.executeQuery();

			while (rs.next()) {
				String phoneNum = rs.getString("phone");
				if (!isExists(phoneNum)) {
					insertToWhiteList(phoneNum);
				} else {
					log.debug("Phone number: " + phoneNum + " already in white list");
				}
			}

         }  catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }
    }


    /**
     * If phone already in white list table
     * @param phone
     * @return
     * @throws SQLException
     */
    private static boolean isExists(String phone) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = getConnection("db.url","db.user","db.pass");

		    String sql =
		    	"SELECT " +
		    		"phone " +
		    	"FROM " +
		    		"sms_white_list " +
				"WHERE " +
					"phone = ? ";

		    ps = con.prepareStatement(sql);
		    ps.setString(1, phone);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
         }  catch (Exception e) {
        		log.log(Level.ERROR, "Error while executing isExists phone", e);
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }
        return false;
    }

    /**
     * Insert into sms white list
     * @param phone
     */
    private static void insertToWhiteList(String phone) {
        Connection con = null;
        PreparedStatement pstmt = null;

        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.INFO, "Going to insert data to sks white list table, phone: " + phone);
        }

		try {
			con = getConnection("db.url","db.user","db.pass");

			String sql =
				"insert into " +
					"sms_white_list(id, phone, time_created) "+
				"values" +
					" (SEQ_SMS_WHITE_LIST.NEXTVAL,?,sysdate)";

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, phone);
			pstmt.executeUpdate();
		}
		catch (Exception e) {
            log.log(Level.ERROR, "Error while insert rows to sms_white_list table in DB.", e);
        }
		finally {
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
        		con.close();
        	} catch (Exception e) {
            log.log(Level.ERROR, "Can't close connection", e);
        	}
		}
    }

    /**
     * sms white list Job.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the sms white list job.");
        }

        updateSmsWhiteList();


        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "sms white list completed!");
        }

   }
}
