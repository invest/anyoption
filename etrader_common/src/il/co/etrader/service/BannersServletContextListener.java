package il.co.etrader.service;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.LevelsCache;

public class BannersServletContextListener extends BaseServleetContextListener {
    private static Logger log = Logger.getLogger(BannersServletContextListener.class);

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        ServletContext servletContext = event.getServletContext();
        try {
            LevelsCache levelsCache = new LevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, null);
            levelsCache.registerJMX(jmxName);
            servletContext.setAttribute("levelsCache", levelsCache);
            if (log.isInfoEnabled()) {
                log.info("LevelsCahce bound to the context");
            }
        } catch (Throwable t) {
            log.error("Failed to init LevelsCache", t);
        }
    }
}