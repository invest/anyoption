package il.co.etrader.service;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.WebLevelsCache;

/**
 * Handle the LevelsCache init and shutdown. We need the levels cache so we can
 * take from there the current opportunity level when we insert investment.
 *
 * @author Tony
 */
public class ETServletContextListener extends WabServletContextListener {
    private static Logger log = Logger.getLogger(ETServletContextListener.class);

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        ServletContext servletContext = event.getServletContext();
        try {
            WebLevelsCache levelsCache = new WebLevelsCache(initialContextFactory, providerURL, connectionFactory, queue, topic, msgPoolSize, recoveryPause, null);
            levelsCache.registerJMX(jmxName);
            servletContext.setAttribute("levelsCache", levelsCache);
            if (log.isInfoEnabled()) {
                log.info("WebLevelsCahce bound to the context");
            }
        } catch (Throwable t) {
            log.error("Failed to init WebLevelsCache", t);
        }
    }
}