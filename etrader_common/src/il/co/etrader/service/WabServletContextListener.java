package il.co.etrader.service;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

/**
 * Common WEB and Backend context listener.
 *
 * @author Tony
 */
public class WabServletContextListener extends BaseServleetContextListener {
    private static final Logger log = Logger.getLogger(WabServletContextListener.class);

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);

        try {
            ClearingManager.loadClearingProviders();
        } catch (Throwable t) {
            log.error("Failed to load clearing providers.", t);
        }

        try {
        	PopulationsManagerBase.loadPopulatioHandlers();
        } catch (Throwable tp) {
            log.error("Failed to load population handlers.", tp);
		}
    }
}