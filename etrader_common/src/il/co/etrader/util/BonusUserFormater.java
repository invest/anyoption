	 	package il.co.etrader.util;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.anyoption.common.beans.base.BonusUsers;

import il.co.etrader.bl_vos.Skins;

public class BonusUserFormater {

	/**
	 * @return the bonusStateDescriptionTxt
	 */
	public static String getBonusDescriptionTxt(BonusUsers bu) {
		String[] params = new String[7];
		params[0] = String.valueOf(getBonusAmountTxt(bu));
		params[1] = String.valueOf(bu.getInvestmentId());
		String format = "##0";
		DecimalFormat fmt = new DecimalFormat(format);
		params[2] = String.valueOf(fmt.format(bu.getBonusPercent()*100));
		params[3] = String.valueOf(getSumInvQualifyTxt(bu));
		params[4] = String.valueOf(bu.getNumOfActions());
		params[5] = String.valueOf(bu.getNumOfActions()+1);
		params[6] = String.valueOf(getMinDepositAmountTxt(bu));
		return CommonUtil.getMessage(bu.getBonusDescription(), params);
	}


	/**
	 * @return the bonusAmount
	 */
	public static String getBonusAmountTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getBonusAmount(), bu.getCurrency().getId());
	}

	public static String getBonusOddsWinTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(0, bu.getCurrency().getId());
	}
	/**
	 * @return the adjusted amount.
	 */
	public static String getBonusAdjustedAmountTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getAdjustedAmount(), bu.getCurrency().getId());
	}

	/**
	 * @return the endDateTxt
	 */
	public static String getEndDateTxt(BonusUsers bu) {
		return CommonUtil.getDateFormat(bu.getEndDate(), ConstantsBase.OFFSET_GMT);
	}

	public static String getMinDepositAmountTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getMinDepositAmount(), bu.getCurrency().getId());
	}

	public static String getMaxDepositAmountTxt(BonusUsers bu){
		return  CommonUtil.displayAmount(bu.getMaxDepositAmount(), bu.getCurrency().getId());
	}
	public static String getBonusPercentTxt(BonusUsers bu){
		NumberFormat nf = NumberFormat.getIntegerInstance();
		return nf.format(bu.getBonusPercent()*100);
	}
	public static String getSumInvQualifyTxt(BonusUsers bu) {
		return  CommonUtil.displayAmount(bu.getSumInvQualify(), bu.getCurrency().getId());
	}
	public static String getSumInvQualifyReachedTxt(BonusUsers bu) {
		return  CommonUtil.displayAmount(bu.getSumInvQualifyReached(), bu.getCurrency().getId());
	}
	public static String getSumInvWithdrawalTxt(BonusUsers bu){
		return  CommonUtil.displayAmount(bu.getSumInvWithdrawal(), bu.getCurrency().getId());
	}
	public static String getSumInvWithdrawalReachedTxt(BonusUsers bu){
		return  CommonUtil.displayAmount(bu.getSumInvWithdrawalReached(), bu.getCurrency().getId());
	}

	/**
	 * @return the startDateTxt
	 */
	public static String getStartDateTxt(BonusUsers bu) {
		return CommonUtil.getDateFormat(bu.getStartDate(), CommonUtil.getUtcOffset());
	}

	/**
	 * @return the timeActivated
	 */
	public static String getTimeActivatedTxt(BonusUsers bu) {
		return CommonUtil.getDateFormat(bu.getTimeActivated(), CommonUtil.getUtcOffset());
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public static String getTimeCreatedTxt(BonusUsers bu) {
		String utc = null;
		if (null == bu.getUtcOffsetUser()) {
			utc = CommonUtil.getUtcOffset();
		} else {
			utc = bu.getUtcOffsetUser();
		}
		return CommonUtil.getDateFormat(bu.getTimeCreated(), utc);
	}

	/**
	 * @return the dateAndTimeCreatedTxt
	 */
	public static String getDateAndTimeCreatedTxt(BonusUsers bu) {
		String utc = null;
		if (null == bu.getUtcOffsetUser()) {
			utc = CommonUtil.getUtcOffset();
		} else {
			utc = bu.getUtcOffsetUser();
		}
		return CommonUtil.getDateFormat(bu.getTimeCreated(), utc) + "  " + CommonUtil.getTimeFormat(bu.getTimeCreated(), utc);
	}

	/**
	 * @return the getTimeDoneTxt
	 */
	public static String getTimeDoneTxt(BonusUsers bu) {
		return CommonUtil.getDateFormat(bu.getTimeDone(), CommonUtil.getUtcOffset());
	}

	/**
	 * @return the getTimeUsedTxt
	 */
	public static String getTimeUsedTxt(BonusUsers bu) {
		return CommonUtil.getDateFormat(bu.getTimeUsed(), CommonUtil.getUtcOffset());
	}

	/**
	 * @return the bonusStateTxt
	 */
	public static String getBonusStateTxt(BonusUsers bu) {
		long returnState = 0L;
		if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_WITHDRAWN || bu.getBonusStateId() == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {			
			returnState = ConstantsBase.BONUS_STATE_USED;
		} else {
			returnState = bu.getBonusStateId();
		}
		return CommonUtil.getMessage("bonus.state" + returnState, null); 
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public static String getWriterTxt(BonusUsers bu) throws SQLException {
		return CommonUtil.getWriterName(bu.getWriterId());
	}

	public static String getMaxInvestAmountTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getMaxInvestAmount(), bu.getCurrency().getId());
	}

	public static String getMinInvestAmountTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getMinInvestAmount(), bu.getCurrency().getId());
	}

	/**
	 * @return the sumDepositsReachedTxt
	 */
	public static String getSumDepositsReachedTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getSumDepositsReached(), bu.getCurrency().getId());
	}

	/**
	 * @return the sumDepositsTxt
	 */
	public static String getSumDepositsTxt(BonusUsers bu) {
		return CommonUtil.displayAmount(bu.getSumDeposits(), bu.getCurrency().getId());
	}

	/**
	 * @return the timeRefusedTxt
	 */
	public static String getTimeRefusedTxt(BonusUsers bu) {
		String utc = null;
		if (null == bu.getUtcOffsetUser()) {
			utc = CommonUtil.getUtcOffset();
		} else {
			utc = bu.getUtcOffsetUser();
		}
		return CommonUtil.getDateFormat(bu.getTimeRefused(), utc);
	}

	/**
	 * @return the timeCanceledTxt
	 */
	public static String getTimeCanceledTxt(BonusUsers bu) {
		String utc = null;
		if (null == bu.getUtcOffsetUser()) {
			utc = CommonUtil.getUtcOffset();
		} else {
			utc = bu.getUtcOffsetUser();
		}
		return CommonUtil.getDateFormat(bu.getTimeCanceled(), utc);
	}

	/**
	 * calculate the amount to be deduct to the user
	 * if the amount to be deduct is grater than the user balance, the amount to be deduct will be the user balance.
	 * in case user's skin_id = 1 (ET) - because of tax issues , we will not deduct amount that is higher than original bonus amount
	 * @param userBalance
	 * @param skinId
	 * @return the amount to be deduct from the user balance with currency.
	 */
	public static String getAmountToDeduct(BonusUsers bu, long userBalance, long skinId) {	
		long amountToDeduct = 0;
		if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE || bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
			amountToDeduct = bu.getBonusAmount();
			if (skinId != Skins.SKIN_ETRADER) {
				if (bu.getBonusClassType() != com.anyoption.common.util.ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US) {
					amountToDeduct = Math.max(bu.getBonusAmount(), bu.getAdjustedAmount());
				}				
			}
	        if ((userBalance - amountToDeduct) < 0) {
	        	amountToDeduct = userBalance;
	        }
		}
        
        return CommonUtil.displayAmount(amountToDeduct, true, bu.getCurrency().getId());
	}
}
