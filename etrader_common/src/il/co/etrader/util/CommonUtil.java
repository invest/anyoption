package il.co.etrader.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.bl_vos.UkashDeposit;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.GeneralManager;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.PromotionManagementManagerBase;
import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.AnyOptionCreditCardValidator;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.CacheObjects;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.RemarketingLoginsManagerBase;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.helper.LiveHelper;
import il.co.etrader.i18n.NoSuchMessageException;
import il.co.etrader.i18n.ReloadableResourceBundleMessageSource;
import il.co.etrader.sms.SMS;

/**
 * Common things for all applications.
 *
 * @author Tony
 */
public class CommonUtil extends com.anyoption.common.util.CommonUtil {
	private static final Logger log = Logger.getLogger(CommonUtil.class);

	private static final long MILLIS_PER_DAY = 86400000;

	public static String SKIN_SUFFIX_REGULATED = ".REG";
//	private static String messagesFilePath = "messages";
	protected static String configurationFilePath = "server";
	private static HashMap<Locale, ResourceBundle> backup = null;
	private static ResourceBundle properties = null;
//	private static DataSource dataSource = null;
	//private static ArrayList enumerators = null; //TODO: remove it after 2 deploys
	//private static long lastRefresh = 0; //TODO: remove it after 2 deploys
	//private static String REFRESH_PERIOD = "config.reloadperiod"; //TODO: remove it after 2 deploys
	public static String CACHE_NAME = "";

//	public static DataSource getDataSource() throws SQLException {
//		if (null != dataSource) {
//			return dataSource;
//		}
//		try {
//            Context initCtx = new InitialContext();
//            Context envCtx = (Context) initCtx.lookup("java:comp/env");
//            PoolDataSource pds = (PoolDataSource) envCtx.lookup("jdbc/anyoptiondb");
//
//            if (log.isDebugEnabled()) {
//                log.debug("Lookedup the PollDataSource from JNDI. Name: " + pds.getConnectionPoolName());
//            }
//
//            dataSource = pds;
//			Connection con = dataSource.getConnection();
//			try {
//				enumerators = GeneralDAO.getAll(con);
//				lastRefresh = new Date().getTime();
//			} finally {
//				con.close();
//			}
//		} catch (Exception e) {
//			log.log(Level.FATAL, "Cannot lookup datasource", e);
//		}
//		return dataSource;
//	}

	public static String getWriterName(long id) throws SQLException{
		return ApplicationDataBase.getWriterName(id);
	}

	public static Writer getWriter(long id) throws SQLException{
		return ApplicationDataBase.getWriter(id);
	}

	public static String getMarketName(long id) {
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, id);
	}
	
	public static String getMarketName(long id, long skinId) {
		return MarketsManagerBase.getMarketName(skinId, id);
	}

	public static String getEnglishMarketName(long id) {
		Market m = ApplicationDataBase.getMarkets().get(new Long(id));
		return CommonUtil.getMessage(m.getDisplayNameKey(), null, new Locale(ConstantsBase.LOCALE_DEFAULT));
	}

	public static String formatLevelByMarket(Double d, long marketId) {
		if (d == null) {
			return "";
        }
		Market m = ApplicationDataBase.getMarkets().get(new Long(marketId));
		Long dpoint = m.getDecimalPoint();
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
        }
		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if (res.endsWith(".")) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}

	public static String formatLevelByMarketExpiry(Double d, long marketId) {
		if (d == null) {
			return "";
        }
		Market m = ApplicationDataBase.getMarkets().get(new Long(marketId));
		Long dpoint = m.getDecimalPoint()-m.getDecimalPointSubtractDigits();
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
        }
		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if (res.endsWith(".")) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}

	public static String getFormatLevelByMarket(int isSettled, Double closingLevel, long marketId) {
        if (isSettled == 1) {
            return formatLevelByMarket(closingLevel, marketId);
        }
        return "";
	}

//    public static String formatLevelByMarket(Double d, long marketId, long dpoint) {
//        if (d == null) {
//            return "";
//        }
//        String format = "###,###,##0.";
//        for (int i = 0; i < dpoint; i++) {
//            format += "0";
//        }
//        DecimalFormat f = new DecimalFormat(format);
//        f.setDecimalSeparatorAlwaysShown(true);
//
//        // Cut '.' if needed
//        String res = f.format(d);
//        int lastIndex = res.length() - 1;
//        if (res.endsWith(".")) {
//            res = res.substring(0, lastIndex);
//        }
//        return res;
//    }

	public static String formatRealLevelByMarket(Double d, long marketId) {
		if (d == null) {
			return "";
        }
		Market m = ApplicationDataBase.getMarkets().get(new Long(marketId));
		Long dpoint = m.getDecimalPoint() - m.getDecimalPointSubtractDigits();
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
		}

		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if (res.endsWith(".")) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}

	public static String formatRealLevelByMarket(Double d, long marketId, long dPoint) {
		if (d == null) {
			return "";
        }
		Long dpoint = dPoint - 1;
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
		}

		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if (res.endsWith(".")) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}
	
	//TODO: remove it after 2 deploys
	/*private static void refreshEnums(Connection conn) throws SQLException {
		long curTime = new Date().getTime();
		if (shouldRefreshEnums()) {
			enumerators = GeneralDAO.getAll(conn);
			lastRefresh = curTime;
		}
	}*/

	//TODO: remove it after 2 deploys
	/*private static void refreshEnums() throws SQLException {
		if (shouldRefreshEnums()) {
			Connection con = DBUtil.getDataSource().getConnection();
            try {
                refreshEnums(con);
            } finally {
                con.close();
            }
		}
	}*/

	//TODO: remove it after 2 deploys
	/*public static String getEnum(Connection conn, String enumerator, String code) throws SQLException {
		refreshEnums(conn);
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
				return e.getValue();
		}
		return null;
	}*/

	//TODO: remove it after 2 deploys
	/*public static String getEnum(String enumerator, String code) throws SQLException {

		refreshEnums();

		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
				return e.getValue();
		}
		return null;
	}*/

	//TODO: remove it after 2 deploys
	/*public static ArrayList<Enumerator> getEnum(String enumerator) throws SQLException {
		ArrayList<Enumerator> l = new ArrayList<Enumerator>();
		refreshEnums();
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator))
				l.add(e);
		}
		return l;
	}*/

	/**
	 * Get message by key
	 * For etrader skin, take the messages from messages file without Locale.
	 * For other skins, take the message by Locale.
     *
	 * @param key message key
	 * @param params parameters for the message
	 * @return

	public static String getMessage(String key, Object params[]) {
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);

//		only for web purposes. when message.properties file is removed this method shuould be deleted
//		String appSource = ApplicationDataBase.getAppSource();
//		if (appSource.equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_WEB) ||
//				appSource.equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_BANNERS)) {
//
//			Long skinId = a.getSkinId();
//			if (null != skinId && skinId == ConstantsBase.SKIN_ETRADER) {
//				if (null == messages) {
//					messages = ResourceBundle.getBundle(messagesFilePath);
//				}
//				return getMessageShared(key, params, messages);
//			}
//		}

		Locale l = null;
		try {
			l = a.getUserLocale();
		} catch (Exception e) {
			log.error("Can't get message by key: " + key + ", take default locale!");
			l = new Locale(ConstantsBase.LOCALE_DEFAULT); // take default locale
		}

		return getMessage(key, params, l);
	}
 */


	/**
	 * Get message by key
	 * For etrader skin, take the messages from messages file without Locale.
	 * For other skins, take the message by Locale.
     *
	 * @param key message key
	 * @param params parameters for the message
	 * @return
	*/
	public static String getMessage(String key, Object params[]) {
//		only for web purposes. when message.properties file is removed this method shuould be deleted
//		String appSource = ApplicationDataBase.getAppSource();
//		if (appSource.equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_WEB) ||
//				appSource.equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_BANNERS)) {
//			if (isEtrader) {
//				if (null == messages) {
//					messages = ResourceBundle.getBundle(messagesFilePath);
//				}
//				return getMessageShared(key, params, messages);
//			}
//		}

		Locale l = null;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
	        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			l = a.getUserLocale();
		} catch (Exception e) {
			log.error("Can't get message by key: " + key + ", take default locale!");
			l = new Locale(ConstantsBase.LOCALE_DEFAULT); // take default locale
		}

		return getMessage(key, params, l);
	}


	/**
	 * Get message by locale
	 * @param key
	 * 		message key
	 * @param params
	 * 		parameters for the message
	 * @param locale
	 * 		locale instance
	 * @return
	 */
	public static String getMessage(String key, Object params[], Locale locale)  {
		if (key == null || key.trim().equals("")) {
			return "";
		}

		FacesContext facesContext = FacesContext.getCurrentInstance();


		if(facesContext != null){
			Application app = facesContext.getApplication();
			if(app != null) {
				// TODO replace
				//ReloadableResourceBundleMessageSource rez = (ReloadableResourceBundleMessageSource) facesContext.getExternalContext().getApplicationMap().get("msgs");
				ReloadableResourceBundleMessageSource rez = (ReloadableResourceBundleMessageSource)app.createValueBinding("#{msgs}").getValue(facesContext);
				if(rez == null){
					log.debug("bundle is null <======");
				}

				try{
					String msg = rez.getMessage(key, params, locale);
					return msg;
				}catch(NoSuchMessageException ex){
					return "?"+key+"?";
				}
			}

		}

		// fallback when faces context is null
		log.debug(" ==========  WARNING : no faces context !!! ===============");
		return getBackupMessage(key, params, locale);
	}

	/**
	 * Get message, this is a common code for geting message with locale
	 * instance or without locale instance.
	 *
	 * Not used since changing i18n suport to ReloadableMessageSource
	 *
	 * @param key
	 * 		message key
	 * @param params
	 * 		parameters for the message
	 * @param bundle
	 * 		the message file
	 * @return

	public static String getMessageShared1(String key, Object params[], ResourceBundle bundle) {
		if (key == null || key.trim().equals("")) {
			return "";
		}

		String text = null;
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?" + key + "?";
		}
		if (params != null) {
			text = text.replaceAll("'", "@@@@@");
			MessageFormat mf = new MessageFormat(text);
			text = mf.format(params, new StringBuffer(), null).toString();
			text = text.replaceAll("@@@@@", "'");
		}
		return text;
	}


	public static String getProperty(String key) {
		return getProperty(key, "?" + key + "?");
	}

	public static String getProperty(String key, String defval) {
		if (properties == null) {
			properties = ResourceBundle.getBundle(configurationFilePath);
        }
		if (key == null || key.trim().equals("")) {
			return "";
        }
		String text = null;
		try {
			text = properties.getString(key);
		} catch (MissingResourceException e) {
			if (log.isDebugEnabled()) {
	            log.debug("Missing value for key :" + key + " Default value is :" + defval);
            }
			return defval;
		}
		return text.trim();
	}*/

    /**
     * Take a long value from the server.properties.
     *
     * @param key
     * @return The requested server.properties parameter as long or 0 if not found.

    public static long getPropertyLong(String key) {
        String val = getProperty(key);
        long rez = 0;
        try {
            rez = Long.parseLong(val);
        } catch (Throwable t) {
            log.warn("Missing config key: " + key + ". Using 0.");
        }
        return rez;
    }*/

	public static long calcAmount(String s) throws NumberFormatException {
		BigDecimal bd = new BigDecimal(s);
		return bd.multiply(new BigDecimal(100)).longValue();
	}

	public static long calcAmount(double d) throws NumberFormatException {
		d = d * 100;
		return new Double(d).longValue();
	}

	public static String calcAmountToString(String s) throws NumberFormatException {
		double d = Double.parseDouble(s);
		d = d * 100;
		return String.valueOf(new Double(d).longValue());
	}

	/**
	 * Get date formated by timeZone
	 * Using this function for all filters
	 * @param d
	 * 		Date object for format
	 * @param timeZoneName
	 * 		Time zone string
	 * @return
	 * 		Date instance with timeZone format
	 */
	public static Date getDateTimeFormaByTz(Date d, String timeZoneName) {
		if ( isParameterEmptyOrNull(timeZoneName) ) {
			return d;
		}
		TimeZone tz = TimeZone.getTimeZone(timeZoneName);
		long newDateMilSec = 0;
		long utcOffset = tz.getRawOffset();
		newDateMilSec = d.getTime() + utcOffset;
		return new Date(newDateMilSec);
	}

	public static Date addDays(Date d, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	public static String getDateTimeFormatDisplay(Date d) {
        return getDateTimeFormatDisplay(d, null);
    }

    public static String getDateTimeFormatDisplay(Date d, String timeZoneName) {
    	return getDateTimeFormatDisplay(d, timeZoneName, "&nbsp;");
	}

    public static String getDateTimeFormatForLiveAO(Date d, String timeZoneName, String text){
    	if (d == null) {
			return "";
		}
        TimeZone tz = null;
        if (null != timeZoneName) {
            tz = TimeZone.getTimeZone(timeZoneName);
        }
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
        if (null != tz) {
            sd.setTimeZone(tz);
        }

		SimpleDateFormat sd2 =  FormatDateBySkin();
        if (null != tz) {
            sd2.setTimeZone(tz);
        }
        return sd2.format(d) + " " + text + " "+ sd.format(d) + ".";
    }


    public static String getDateTimeFormatDisplay(Date d, String timeZoneName, String delimiter) {
		if (d == null) {
			return "";
		}
        TimeZone tz = null;
        if (null != timeZoneName) {
            tz = TimeZone.getTimeZone(timeZoneName);
        }
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
        if (null != tz) {
            sd.setTimeZone(tz);
        }

		SimpleDateFormat sd2 =  FormatDateBySkin();
        if (null != tz) {
            sd2.setTimeZone(tz);
        }
		return sd.format(d) + delimiter + delimiter + sd2.format(d);
	}

    /**
     * This method perform the same task as getDateTimeFormatDisplay but returns a string where the Date is first and then the time with regular spaces between them
     * @param d
     * @param timeZoneName
     * @return
     */
    public static String getAlternateDateTimeFormatDisplay(Date d, String timeZoneName) {
		return getAlternateDateTimeFormatDisplay(d, timeZoneName, false);
	}

    /**
     * This method perform the same task as getDateTimeFormatDisplay but returns a string where the time with regular spaces between them
     * and you can choose the time / date order
     * @param d
     * @param timeZoneName
     * @return
     */
    public static String getAlternateDateTimeFormatDisplay(Date d, String timeZoneName, boolean timeIsFirst) {
		if (d == null) {
			return "";
		}
        TimeZone tz = null;
        if (null != timeZoneName) {
            tz = TimeZone.getTimeZone(timeZoneName);
        }
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
        if (null != tz) {
            sd.setTimeZone(tz);
        }

		SimpleDateFormat sd2 =  FormatDateBySkin();
        if (null != tz) {
            sd2.setTimeZone(tz);
        }

		return (timeIsFirst ? sd.format(d) + "  " + sd2.format(d) : sd2.format(d) + "  " + sd.format(d));
	}

    public static String getDateTimeFormatShowOff(Date d, String timeZoneName) {
    	FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		Long skinId = a.getSkinId();
		return getAlternateDateTimeFormatDisplay(d, timeZoneName, skinId == 10);
	}

    public static String[] getDateAndTimeShowOffAsArray(Date d,String timeZoneName){
    	String dateAndTime = getDateTimeFormatShowOff(d, timeZoneName);
    	String[] dateAndTimeArr = dateAndTime.split(" ");
    	return dateAndTimeArr;
    }

	public static String getDateTimeFormat(Date d) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sd2 = FormatDateBySkin();
		String out = sd2.format(d) + "  " + sd.format(d);

		return out;
	}

	public static String getDateFormat(Date d) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = FormatDateBySkin();
		String out = sd.format(d);
		return out;
	}

	public static String getDateFormat(Date d, String timeZoneName) {
		if (d == null) {
			return "";
		}

		TimeZone tz = null;
        if (null != timeZoneName) {
	         tz = TimeZone.getTimeZone(timeZoneName);
	    }

		SimpleDateFormat sd = FormatDateBySkin();
        if (null != tz) {
        	sd.setTimeZone(tz);
        }

		String out = sd.format(d);
		return out;
	}

	public static String getTimeFormat(Date d) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = new SimpleDateFormat("HH:mm");
		String out = sd.format(d);
		return out;
	}

	public static String getTimeFormat(Date d, String timeZoneName) {
        return getTimeFormat(d, timeZoneName, "HH:mm");
	}

    public static String getTimeFormat(Date d, String timeZoneName, String pattern) {
        if (d == null) {
            return "";
        }

        TimeZone tz = null;
        if (null != timeZoneName) {
             tz = TimeZone.getTimeZone(timeZoneName);
        }

        SimpleDateFormat sd = new SimpleDateFormat(pattern);
        if (null != tz) {
             sd.setTimeZone(tz);
        }

        String out = sd.format(d);
        return out;
    }

    public static String getTimeByFormat(Date d, String format){
        return getTimeFormat(d, getUtcOffset(ConstantsBase.EMPTY_STRING), format);
    }

    /**
    *
    * @param d
    * @return date in format: dd/MM/yyyy HH:mm:ss
    */
   public static String getDateAndTimeFormat(Date d) {
	   return getDateAndTimeFormat(d, null);
   }

    /**
     *
     * @param d
     * @return date in format: dd/MM/yyyy HH:mm:ss
     */
    public static String getDateAndTimeFormat(Date d, String timeZoneName) {
		if (d == null) {
			return "";
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		TimeZone tz = null;
        if (null != timeZoneName) {
             tz = TimeZone.getTimeZone(timeZoneName);
        }
		if (null != tz) {
            sd.setTimeZone(tz);
       }
		String out = sd.format(d);
		return out;
	}

	/**
	 * For use in jsf, because el expression cannot take the right method
	 *
	 * @param amount
	 * @param showCurrency
	 * @param currencyId
	 * @return
	 */
	public static String displayAmountUI(long amount, boolean showCurrency, long currencyId) {
		return displayAmount(amount, showCurrency, currencyId);
	}

	public static String displayAmount(long amount, boolean showCurrency, long currencyId) {
		return displayAmount(amount, showCurrency, currencyId, false);
	}

	public static String displayAmount(long amount, boolean showCurrency, long currencyId, boolean isSMS) {
		Currency currency;
		String displayFormat = null;
		try {
			currency = CurrenciesManagerBase.getCurrency(currencyId);
			displayFormat = CurrenciesManagerBase.getAmountFormat(currency, false);
		} catch (Exception e) {
			log.error("Can't format displayAmount.", e);
			return null;
		}

		return displayAmount(amount, showCurrency, currencyId, isSMS, displayFormat);
	}

	public static String displayAmount(long amount, boolean showCurrency, long currencyId, boolean isSMS, String displayFormat) {
		Currency currency;
		try {
			currency = CurrenciesManagerBase.getCurrency(currencyId);
		} catch (Exception e) {
			log.error("Can't get currency [" + currencyId + "]", e);
			return null;
		}
		DecimalFormat sd = new DecimalFormat(displayFormat, new DecimalFormatSymbols(Locale.US));
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = null;
		if (showCurrency) {
		    if (null != currency && currency.getIsLeftSymbolBool()) {
		        out = getMessage((isSMS ? getCurrencySymbolSMS(currencyId) : getCurrencySymbol(currencyId)), null) + sd.format(amountDecimal);
		    } else {
                out = sd.format(amountDecimal) + getMessage((isSMS ? getCurrencySymbolSMS(currencyId) : getCurrencySymbol(currencyId)), null);
		    }
		}
		if (null == out) {
			out = sd.format(amountDecimal);
		}
		return out;
	}

	public static String displayAmount(double amount, boolean showCurrency, long currencyId) {
		Currency currency;
		String format = null;
		if (currencyId == 0) {
			currencyId = ConstantsBase.CURRENCY_BASE_ID;
		}
		try {
			currency = CurrenciesManagerBase.getCurrency(currencyId);
			format = CurrenciesManagerBase.getAmountFormat(currency, false);
		} catch (Exception e) {
			log.error("Can't format displayAmount.", e);
			return null;
		}
		DecimalFormat sd = new DecimalFormat(format, new DecimalFormatSymbols(Locale.US));
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			 if (null != currency && currency.getIsLeftSymbolBool()) {
				 out = getMessage(getCurrencySymbol(currencyId), null) + sd.format(amountDecimal);
			 } else {
	                out = sd.format(amountDecimal) + getMessage((getCurrencySymbol(currencyId)), null);
			 }
		} else {
			out = sd.format(amountDecimal);
		}
		return out;
	}

	/**
	 * DisplayAmount function
     *
	 * @param amount amount to display
	 * @param showCurrency if needed to show currency symbol
	 * @param currency currency instance
	 * @return
	 */
	public static String displayAmount(long amount, boolean showCurrency, Currency currency) {
		DecimalFormat sd = new DecimalFormat(CurrenciesManagerBase.getAmountFormat(currency, false));
		double amountDecimal = amount;
		String symbol = "";
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			symbol = getMessage(getCurrencySymbol(currency.getId()), null);
			if (currency.getIsLeftSymbolBool()) {
				out = symbol + sd.format(amountDecimal);
			} else {
				out = sd.format(amountDecimal) + symbol;
			}
		} else {
			out = sd.format(amountDecimal);
		}
		return out;
	}

	public static String displayAmount(String amount, boolean showCurrency,  int currencyId) {
		if (amount == null || amount.equals("")) {
			return "";
		}
		long l = 0;
		try {
			l = Long.parseLong(amount);
		} catch (Exception e) {
			return "Error in display Amount! Illegal Number: " + amount;
		}
		return displayAmount(l, showCurrency, currencyId);
	}

	public static String displayAmount(long amount, long currencyId) {
		return displayAmount(amount, currencyId > 0, currencyId);
	}

	/*
	 * Because EL can't lookup the right method we need to help it...
	 */
	public static String displayAmountLongLong(long amount, long currencyId) {
        return displayAmount(amount, currencyId > 0, currencyId);
    }

	public static String displayAmount(String amount,  int currencyId) {
		return displayAmount(amount, currencyId > 0, currencyId);
	}

	/**
	 * display amount with diferent symbol location
	 */
	public static String displayAmount(long amount, Currency currency) {
		return displayAmount(amount, null != currency, currency);
	}

//	public static String getCurrencySymbol(long currencyId) {
//		String curr = ConstantsBase.EMPTY_STRING;
//		switch ((int)currencyId) {
//		case (int)ConstantsBase.CURRENCY_ILS_ID:
//			curr = ConstantsBase.CURRENCY_ILS;
//			break;
//		case (int)ConstantsBase.CURRENCY_USD_ID:
//			curr = ConstantsBase.CURRENCY_USD;
//			break;
//		case (int)ConstantsBase.CURRENCY_EUR_ID:
//			curr = ConstantsBase.CURRENCY_EUR;
//			break;
//		case (int)ConstantsBase.CURRENCY_GBP_ID:
//			curr = ConstantsBase.CURRENCY_GBP;
//			break;
//		case (int)ConstantsBase.CURRENCY_TRY_ID:
//			curr = ConstantsBase.CURRENCY_TRY;
//			break;
//		case (int)ConstantsBase.CURRENCY_RUB_ID:
//			curr = ConstantsBase.CURRENCY_RUB;
//			break;
//        case (int)ConstantsBase.CURRENCY_CNY_ID:
//            curr = ConstantsBase.CURRENCY_CNY;
//            break;
//        case (int)ConstantsBase.CURRENCY_KRW_ID:
//            curr = ConstantsBase.CURRENCY_KRW;
//            break;
//        case (int)ConstantsBase.CURRENCY_SEK_ID:
//            curr = ConstantsBase.CURRENCY_SEK;
//            break;
//        case (int)ConstantsBase.CURRENCY_AUD_ID:
//            curr = ConstantsBase.CURRENCY_AUD;
//            break;
//        case (int)ConstantsBase.CURRENCY_ZAR_ID:
//            curr = ConstantsBase.CURRENCY_ZAR;
//            break; 
//		default:
//			break;
//		}
//		return curr;
//	}
	
	public static String getCurrencySymbolAndCode(long currencyId) {
		String curr = ConstantsBase.EMPTY_STRING;
		switch ((int)currencyId) {
		case (int)ConstantsBase.CURRENCY_ILS_ID:
			curr = ConstantsBase.CURRENCY_ILS;
			break;
		case (int)ConstantsBase.CURRENCY_USD_ID:
			curr = ConstantsBase.CURRENCY_USD_CODE;
			break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
			curr = ConstantsBase.CURRENCY_EUR;
			break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
			curr = ConstantsBase.CURRENCY_GBP;
			break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
			curr = ConstantsBase.CURRENCY_TRY;
			break;
		case (int)ConstantsBase.CURRENCY_RUB_ID:
			curr = ConstantsBase.CURRENCY_RUB;
			break;
        case (int)ConstantsBase.CURRENCY_CNY_ID:
            curr = ConstantsBase.CURRENCY_CNY;
            break;
        case (int)ConstantsBase.CURRENCY_KRW_ID:
            curr = ConstantsBase.CURRENCY_KRW;
            break;
        case (int)ConstantsBase.CURRENCY_SEK_ID:
            curr = ConstantsBase.CURRENCY_SEK;
            break;
        case (int)ConstantsBase.CURRENCY_AUD_ID:
            curr = ConstantsBase.CURRENCY_AUD;
            break;
        case (int)ConstantsBase.CURRENCY_ZAR_ID:
            curr = ConstantsBase.CURRENCY_ZAR;
            break;
        case (int)ConstantsBase.CURRENCY_CZK_ID:
            curr = ConstantsBase.CURRENCY_CZK;
            break;
        case (int)ConstantsBase.CURRENCY_PLN_ID:
            curr = ConstantsBase.CURRENCY_PLN;
            break;            
		default:
			break;
		}
		return curr;
	}

//	public static String getCurrencySymbolSMS(long currencyId) {
//		String curr = ConstantsBase.EMPTY_STRING;
//		switch ((int)currencyId) {
//		case (int)ConstantsBase.CURRENCY_ILS_ID:
//			curr = ConstantsBase.CURRENCY_SMS_ILS;
//			break;
//		case (int)ConstantsBase.CURRENCY_USD_ID:
//			curr = ConstantsBase.CURRENCY_SMS_USD;
//			break;
//		case (int)ConstantsBase.CURRENCY_EUR_ID:
//			curr = ConstantsBase.CURRENCY_SMS_EUR;
//			break;
//		case (int)ConstantsBase.CURRENCY_GBP_ID:
//			curr = ConstantsBase.CURRENCY_SMS_GBP;
//			break;
//		case (int)ConstantsBase.CURRENCY_TRY_ID:
//			curr = ConstantsBase.CURRENCY_SMS_TRY;
//			break;
//		case (int)ConstantsBase.CURRENCY_RUB_ID:
//			curr = ConstantsBase.CURRENCY_SMS_RUB;
//			break;
//        case (int)ConstantsBase.CURRENCY_CNY_ID:
//            curr = ConstantsBase.CURRENCY_SMS_CNY;
//            break;
//        case (int)ConstantsBase.CURRENCY_KRW_ID:
//            curr = ConstantsBase.CURRENCY_SMS_KRW;
//            break;
//        case (int)ConstantsBase.CURRENCY_SEK_ID:
//            curr = ConstantsBase.CURRENCY_SMS_SEK;
//            break;
//        case (int)ConstantsBase.CURRENCY_AUD_ID:
//            curr = ConstantsBase.CURRENCY_SMS_AUD;
//            break;
//        case (int)ConstantsBase.CURRENCY_ZAR_ID:
//            curr = ConstantsBase.CURRENCY_SMS_ZAR;
//            break; 
//		default:
//			break;
//		}
//		return curr;
//	}

	public static Boolean isAllowIp(){
		boolean res = false;
		String ip = getIPAddress();
		if(GeneralManager.getAllowIPList().contains(ip)){
			res = true;
		}
		return res;
	}

	public static void validateStreetNo(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		String pattern = CommonUtil.getMessage("general.validate.streetnum", null);

		if (!v.matches(pattern)) {

			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.streetnum", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateBonusDepositNegative(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		try {
    		if (Float.parseFloat(v) <= 0) {
    			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.negative", null));
    			context.addMessage("depositForm:deposit", msg);
    			throw new ValidatorException(msg);
    		}
		} catch(Exception e) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.negative", null));
			context.addMessage("depositForm:deposit", msg);
			throw new ValidatorException(msg);
		}
	}

	public static void validateBonusWithdrawNegative(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		try {
    		if (Integer.parseInt(v) <= 0) {

    			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.negative", null));
    			context.addMessage("depositForm:deposit", msg);
    			throw new ValidatorException(msg);
    		}
		} catch(Exception e) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.negative", null));
			context.addMessage("depositForm:deposit", msg);
			throw new ValidatorException(msg);
		}
	}


	public static void validateStreet(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		String pattern = CommonUtil.getMessage("general.validate.street", null);
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.street", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validatePasswordTypeAndSize(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		CommonUtil.validateLettersAndNumbersOnly(context, comp, value);
		String v = (String) value;
		if (v.length() < 6) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.password.smallsize", null));
			throw new ValidatorException(msg);
		}
		if (v.length() > 9) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.password.bigsize", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateLettersAndNumbersOnly(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		String v = (String) value;
		String pattern = CommonUtil.getMessage("general.validate.letters.and.numbers", null);
		if (!v.matches(pattern)) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.lettersandnumbers.only", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validateEnglishAndWildCards(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		String v = (String) value;
		String pattern = CommonUtil.getMessage("general.validate.english.and.wildcards", null);
		if (!v.matches(pattern)) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.englishwildcards.only", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateEnglishAndNumbersAndSpaces(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		String v = (String) value;
		String pattern = CommonUtil.getMessage("general.validate.english.numbers.spaces", null);
		if (!v.matches(pattern) || v.trim().equals("")) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.lettersandnumbers.only", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateLettersOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = CommonUtil.getMessage("general.validate.letters.only", null);
		String v = (String) value;
		v = v.replaceAll("-", " ");
		v = v.replaceAll("`", " ");
		v = v.replaceAll("'", " ");
		v = v.replaceAll("\\(", " ");
		v = v.replaceAll("\\)", " ");
		v = v.replaceAll("\"", " ");
		v = v.replaceAll("‘", " ");
		v = v.replaceAll("\\.", " ");
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.letters.only", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validateLettersOnlyCityET(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		String[] cityList = ApplicationDataBase.getCitiesListStringET().split(",");
		boolean equalCity;
		equalCity = false;
		for (String city : cityList) {
			if (city.replaceAll("\"", "").equalsIgnoreCase(v)) {
				equalCity = true;
			}
		}
		if (v.trim().equals("") || !equalCity) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("CMS.register_content.text.tooltip.incorrectCity", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validateEnglishLettersOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = "^['a-zA-Z ]+$";
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.english.letters.only", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validateName(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = CommonUtil.getMessage("general.validate.name", null);
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.letters.only", params));
			throw new ValidatorException(msg);
		}
	}

	public static void validateHebrewOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = CommonUtil.getMessage("general.validate.hebrew.only", null);
		String v = (String) value;
		v = v.replaceAll("-", " ");
		v = v.replaceAll("`", " ");
		v = v.replaceAll("'", " ");
		v = v.replaceAll("\\(", " ");
		v = v.replaceAll("\\)", " ");
		v = v.replaceAll("\"", " ");
		v = v.replaceAll("‘", " ");
		v = v.replaceAll("\\.", " ");

		if (!v.matches(pattern) || v.trim().equals("")) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.hebrew.only", null));
			throw new ValidatorException(msg);
		}
	}

	/**
	 * Called by the JSF custom validator, this method makes the basic validation
	 * for the Israeli personal ID number that each user shall enter on registration
	 * and new card registration.
	 *
	 * @param context
	 * @param comp
	 * @param value
	 * @throws Exception
	 */
	public static void validateIdNum(FacesContext context,
										UIComponent comp,
										Object value) throws Exception {
		String errMsg = validateIdNum(value + "");
		if (errMsg != null) {
			throw new ValidatorException(new FacesMessage(CommonUtil.getMessage(errMsg, null)));
		}
	}

	public static void validateCreditCardNumber(FacesContext context, UIComponent comp, Object value) throws Exception {
		long skinId = 0;
		if (isEtraderUserSkin()) {
			skinId = 1;
		}
		if (!AnyOptionCreditCardValidator.validateCreditCardNumber(value.toString(), skinId)) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.creditcard.num", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateEqualPass(FacesContext context, UIComponent comp, Object value) throws Exception {
		String retypePass = (String) value;
		String forValue = (String) comp.getAttributes().get("equalPassFor");
		UIComponent passwordUIComp = comp.getParent().findComponent(forValue);
		String pass = (String) passwordUIComp.getAttributes().get("value");
		if (!retypePass.equals(pass)) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("org.apache.myfaces.Equal.INVALID_detail", null));
			throw new ValidatorException(msg);
		}
	}

	public boolean isHasMessages() {
		FacesContext context = FacesContext.getCurrentInstance();
		Iterator i = context.getMessages(null);
		if (i.hasNext()) {
			return true;
        }
		return false;
	}
	
	public boolean isCleanMessages() {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{transaction}", Transaction.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), null);
        return true;
	}

    /**
     * @return Check if there are any validation error messages.
     */
    public boolean isHasErrorMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator i = context.getMessages();
        if (i.hasNext()) {
            return true;
        }
        return false;
    }

    /**
     * @return The detailed explanation of the first validation error message.
     */
    public String getFirstErrorMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator i = context.getMessages();
        if (i.hasNext()) {
            FacesMessage fm = (FacesMessage) i.next();
//            return fm.getSummary();
            return fm.getDetail();
        }
        return "";
    }

    /**
     * @return The detailed explanation of the first validation error message.
     */
    public String getFirstErrorSummaryMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator i = context.getMessages();
        if (i.hasNext()) {
            FacesMessage fm = (FacesMessage) i.next();
            String error = fm.getSummary();
            // for landing banner page that the validations in the bottom of the form
            if (fm.getSummary().indexOf("firstName") > -1) {
            	error = error.replace("firstName", getMessage("register.firstname", null));
            } else if (fm.getSummary().indexOf("lastName") > -1) {
            	error = error.replace("lastName", getMessage("register.lastname", null));
            } else if (fm.getSummary().indexOf("mobilePhonePref") > -1) {
               	error = error.replace("mobilePhonePref", getMessage("register.mobilephone.prefix", null));
            } else if (fm.getSummary().indexOf("mobilePhone") > -1) {
            	error = error.replace("mobilePhone", getMessage("register.mobilephone", null));
            } else if (fm.getSummary().indexOf("landLinePhone") > -1) {
            	error = error.replace("landLinePhone", getMessage("register.phone", null));
            } else if (fm.getSummary().indexOf("userName") > -1) {
            	error = error.replace("userName", getMessage("register.username", null));
            } else if (fm.getSummary().indexOf("birthDay") > -1) {
            	error = error.replace("birthDay", getMessage("register.birthday", null));
            } else if (fm.getSummary().indexOf("birthMonth") > -1) {
            	error = error.replace("birthMonth", getMessage("register.birthmonth", null));
            } else if (fm.getSummary().indexOf("birthYear") > -1) {
            	error = error.replace("birthYear", getMessage("register.birthyear", null));
            } else if (fm.getSummary().indexOf("gender") > -1) {
            	error = error.replace("gender", getMessage("register.gender", null));
            } else if (fm.getSummary().indexOf("cityNameAO") > -1) {
            	error = error.replace("cityNameAO", getMessage("register.city", null));
            } else if (fm.getSummary().indexOf("password2") > -1) {
            	error = error.replace("password2", getMessage("register.retype", null));
            } else if (fm.getSummary().indexOf("street") > -1) {
            	error = error.replace("street", getMessage("register.street", null));
            } else if (fm.getSummary().indexOf("password") > -1) {
            	error = error.replace("password", getMessage("register.password", null));
            } else if (fm.getSummary().indexOf("email") > -1) {
            	error = error.replace("email", getMessage("register.email", null));
            }
            return error.toUpperCase();
        }
        return "";
    }

    public String getFirstErrorSummaryMessagePopUpForm() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator i = context.getMessages();
        if(context.getMessageList().size() == 4){
        	return "";
        }
        if (i.hasNext()) {
            FacesMessage fm = (FacesMessage) i.next();
            String error = fm.getSummary();
            // for landing banner page that the validations in the bottom of the form
            if (fm.getSummary().indexOf("firstName") > -1) {
            	error = error.replace("firstName", getMessage("register.firstname", null));
            } else if (fm.getSummary().indexOf("lastName") > -1) {
            	error = error.replace("lastName", getMessage("register.lastname", null));
            } else if (fm.getSummary().indexOf("mobilePhonePref") > -1) {
               	error = error.replace("mobilePhonePref", getMessage("register.mobilephone.prefix", null));
            } else if (fm.getSummary().indexOf("mobilePhone") > -1) {
            	error = error.replace("mobilePhone", getMessage("register.mobilephone", null));
            } else if (fm.getSummary().indexOf("landLinePhone") > -1) {
            	error = error.replace("landLinePhone", getMessage("register.phone", null));
            } else if (fm.getSummary().indexOf("userName") > -1) {
            	error = error.replace("userName", getMessage("register.username", null));
            } else if (fm.getSummary().indexOf("birthDay") > -1) {
            	error = error.replace("birthDay", getMessage("register.birthday", null));
            } else if (fm.getSummary().indexOf("birthMonth") > -1) {
            	error = error.replace("birthMonth", getMessage("register.birthmonth", null));
            } else if (fm.getSummary().indexOf("birthYear") > -1) {
            	error = error.replace("birthYear", getMessage("register.birthyear", null));
            } else if (fm.getSummary().indexOf("gender") > -1) {
            	error = error.replace("gender", getMessage("register.gender", null));
            } else if (fm.getSummary().indexOf("cityNameAO") > -1) {
            	error = error.replace("cityNameAO", getMessage("register.city", null));
            } else if (fm.getSummary().indexOf("password2") > -1) {
            	error = error.replace("password2", getMessage("register.retype", null));
            } else if (fm.getSummary().indexOf("street") > -1) {
            	error = error.replace("street", getMessage("register.street", null));
            } else if (fm.getSummary().indexOf("password") > -1) {
            	error = error.replace("password", getMessage("register.password", null));
            } else if (fm.getSummary().indexOf("email") > -1) {
            	error = error.replace("email", getMessage("register.email", null));
            }
            return error;
        }
        return "";
    }


	public static void addScript(String script) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// Add the Javascript to the rendered page's header for immediate
		// execution
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, script);
	}

	public static String displayDecimal(double d) {
		DecimalFormat sd = new DecimalFormat("###,###,###,##0.0#########");
		return sd.format(d);
	}

	public static String displayDecimal(BigDecimal d) {
		if (d == null) {
			return "";
		}
		return displayDecimal(d.doubleValue());
	}

	public static String displayLong(long d) {
		DecimalFormat sd = new DecimalFormat("###,###,###,##0");
		return sd.format(d);
	}

	public static String displayDecimalForInput(double d) {
		DecimalFormat sd = new DecimalFormat("###########0.0#########");
		return sd.format(d);
	}

	public static void setTablesToFirstPage() {
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot view = context.getViewRoot();
		if (view == null)
			return;
		List list = view.getChildren();
		for (int i = 0; i < list.size(); i++) {
			UIComponent tmpComponent = (UIComponent) list.get(i);
			setTableToFirstPageRecursive(tmpComponent);
		}
	}

	private static void setTableToFirstPageRecursive(UIComponent tmpComponent) {
		if (tmpComponent instanceof HtmlDataTable) {
			HtmlDataTable table = (HtmlDataTable) tmpComponent;
			// Set page index to 1 again
			if (table != null)
				table.setFirst(0);
		}
		List list = tmpComponent.getChildren();
		for (int i = 0; i < list.size(); i++) {
			UIComponent t = (UIComponent) list.get(i);
			setTableToFirstPageRecursive(t);
		}
	}

	public static boolean getIsAdmin() {
		FacesContext context = FacesContext.getCurrentInstance();
		boolean permission = false;
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
		String isAdmin = req.getParameter("admin");
		if (isAdmin != null) {
			if (isAdmin.equals("true")) {
				permission = true;
			} else {
				permission = false;
			}
		}
		return permission;
	}

	public static String compareObjects(Object o1, Object o2, Map ignoreList) {
		try {
			String output = "";
			Class cls = o1.getClass();
			Field fieldlist[] = cls.getDeclaredFields();
			Method methlist[] = cls.getDeclaredMethods();
			for (int i = 0; i < fieldlist.length; i++) {
				Field fld = fieldlist[i];
				 for (int j = 0; j < methlist.length; j++) {
					 Method m = methlist[j];
					 if (m.getName().equalsIgnoreCase("get"+fld.getName()) || m.getName().equalsIgnoreCase("is"+fld.getName())) {
						 Object val1 = m.invoke(o1, new Object[0]);
						 Object val2 = m.invoke(o2, new Object[0]);

						 if ( (val1!=null && val2==null) ||
							  (val1==null && val2!=null) ||
							  (val1!=null && val2!=null && !val1.equals(val2))) {

							 if (ignoreList!=null && !ignoreList.containsKey(fld.getName())) {
								 output+=fld.getName()+" - old: "+val1+" new:"+val2+ " | ";
							 }
						 }
						 break;
					 }
				 }
			}
			return output;
		} catch (Throwable e) {
			log.error(e);
		}
		return null;
	}

	public static boolean isValidDateStr(String date) {
	    try {
	        SimpleDateFormat sdf = FormatDateBySkin();
	        sdf.setLenient(false);
	        sdf.parse(date);
	    } catch (ParseException e) {
	        return false;
	    } catch (IllegalArgumentException e) {
	        return false;
	    }
	    return true;
	}


	/**
	 * Check if we in period2( after Mid-Year, for tax )
	 * @return
	 * 		true if we in period2
	 * 		false if we in period1
	 */
	public boolean isHasPeridTwo() {
		   GregorianCalendar gc = new GregorianCalendar();
		   Date timeWithOffset = getDateTimeFormaByTz(gc.getTime(), getUtcOffset());
		   gc.setTime(timeWithOffset);
		   if (gc.get(GregorianCalendar.MONTH) >= GregorianCalendar.JULY) {
			   return true;
		   }
		return false;
	}

	/**
	 * return String format for period 2 tax
	 * @return
	 * 	String format of 30.06.currentYear
	 */
	public static String getFormatDatePeriodOne() {
	    GregorianCalendar gc = new GregorianCalendar();
	    String[] params = new String[1];

	    params[0]="30.06."+gc.get(GregorianCalendar.YEAR);

	    return params[0];
	}

	public static String formatLevelByDecimalPoint(Double d, long dpoint ) {
		if (d == null) {
			return "";
        }
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
        }
		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if ( res.endsWith(".") ) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}

	/**
	 * return true if the enums should be refreshed , false otherwise
	 * @return boolean
	 * 	In case the value isn't in properties , need to take default value.
	 *  Used in the method refreshEnums
	 */
	/*private static boolean shouldRefreshEnums() {
        if (null == enumerators) {
            return true;
        }
		boolean needToRefresh = false;
		long curTime = Calendar.getInstance().getTimeInMillis();
		String refreshIntervalStr = getProperty(REFRESH_PERIOD ,ConstantsBase.DEFAULT_ENUMS_REFRESH );
		long refreshInterval = Long.parseLong(refreshIntervalStr);
		needToRefresh =  curTime - lastRefresh > refreshInterval;
		return needToRefresh;
	}*/
	
	/**
	 * Builds a dropdown containning list of <SelectItem>
	 * @param String listName - The dropdown list name
	 * @return ArrayList<SelectItem> - returns an ArrayList of <SelectItems>
	 *//*
	public static ArrayList<SelectItem> enumSI(String listName) throws SQLException {
		ArrayList<SelectItem> dropDown = new ArrayList<SelectItem>();
		//getting the dropdown list from Enumerators table
		ArrayList enumsList = getEnum(listName);
		for ( int i=0; i<enumsList.size(); i++) {
			String tmp =((Enumerator)enumsList.get(i)).getValue();
			long tmpLong  = new Long(tmp).longValue();
			dropDown.add(new SelectItem(tmpLong, ((Enumerator)enumsList.get(i)).getCode()));
		}
		return dropDown;
	}*/

	/**
	 *
	 * @return true if the user is useing Etrader skin!
	 */
	public static boolean isEtraderUserSkin() {
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		if (a.getSkinId() == Skin.SKIN_ETRADER) {
			return true;
		}
		return false;
	}

    public static boolean isEtraderUserSkin(HttpSession session, HttpServletRequest request) {
        if (ApplicationDataBase.getSkinId(session, request) == Skin.SKIN_ETRADER) {
            return true;
        }
        return false;
    }

	/**
	 * Get applicationDataBase instance
	 * @return
	 * 		appplicationDataBase instance
	 */
	public static ApplicationDataBase getAppData() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationDataBase appData = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			return appData;
		} catch (Exception npe) {
			return null;
		}
	}

	/**
	 * Get applicationDataBase instance
	 * @return
	 * 		appplicationDataBase instance
	 */
	public static ApplicationDataBase getApplicationData() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationDataBase appData = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			return appData;
		} catch (Exception npe) {
			return null;
		}
	}

	/**
	 * convert array list to string
	 * @param list arry list to convert to string
	 * @param getterMethodId get method to get the value from
	 * @param getterMethodName get method to get the lable from
	 * @param locale write locale
	 * @return return String that represent the arry list when id is Long and Value is String
	 */
	public static String arrayList2String(ArrayList list, String getterMethodId, String getterMethodName, Locale locale) {
		StringBuffer st = new StringBuffer();
        st.append("[");
		try {
			for (int i = 0; i < list.size(); i++) {
				Object object = list.get(i);
				Class classDefinition = object.getClass();
				Method mId = classDefinition.getMethod(getterMethodId, (Class[])null);
				Method mName = classDefinition.getMethod(getterMethodName, (Class[])null);
	            st.append("['" + mId.invoke(object, (Object[])null)).append("' , '");
	            st.append(getMessage((String)mName.invoke(object, (Object[])null), null, locale)).append("'], ");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        int ind = st.lastIndexOf(",");
        if (ind != -1) {
        	st.deleteCharAt(ind);
        }
        st.append("]");

        return st.toString();
	}

	/**
	 * convert array list to tags <container><value></value><name></name></container>
	 * @param list arry list to convert to string
	 * @param getterMethodId get method to get the value from
	 * @param getterMethodName get method to get the lable from
	 * @param locale write locale
	 * @param continer string that will contain each value and name
	 * @return return String that represent the arry list when id is String and Value is String
	 */
	public static String arrayList2StringTags(ArrayList list, String getterMethodId, String getterMethodName, Locale locale, String continer) {
		StringBuffer st = new StringBuffer();
        //st.append("[");
		try {
			for (int i = 0; i < list.size(); i++) {
				Object object = list.get(i);
				Class classDefinition = object.getClass();
				Method mId = classDefinition.getMethod(getterMethodId, (Class[])null);
				Method mName = classDefinition.getMethod(getterMethodName, (Class[])null);
	            st.append("<" + continer + ">" + ConstantsBase.VALUE_TAG + (String) mId.invoke(object, (Object[])null)).append( ConstantsBase.VALUE_CLOSING_TAG + ConstantsBase.NAME_TAG );
	            st.append(URLEncoder.encode(getMessage((String)mName.invoke(object, (Object[])null), null, locale),"UTF-8")).append(ConstantsBase.NAME_CLOSING_TAG+ "</" + continer + ">");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        //int ind = st.lastIndexOf(",");
        //if (ind != -1) {
//        	st.deleteCharAt(ind);
  //      }
      //  st.append("]");

        return st.toString();
	}

	public static ArrayList<SelectItem> hashMap2SelectList(HashMap hm, String getterMethodId, String getterMethodName) {
		ArrayList<SelectItem> sList = new ArrayList<SelectItem>();
		try {
			Iterator iter = hm.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry e = (Map.Entry)iter.next();
				Object object = e.getValue();
				Class classDefinition = object.getClass();
				Method mId = classDefinition.getMethod( getterMethodId, (Class[])null);
				Method mName = classDefinition.getMethod(getterMethodName, (Class[])null);
				sList.add(new SelectItem((Long)mId.invoke(object, (Object[])null),(String)mName.invoke(object, (Object[])null)));
			}
            Collections.sort(sList, new selectItemComparator());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sList;
	}
	
	public static ArrayList<SelectItem> map2SelectList(Map map) {
		ArrayList<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {
			Iterator iter = map.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry e = (Map.Entry)iter.next();
				selectItems.add(new SelectItem(e.getKey(),(String) e.getValue()));
			}
            Collections.sort(selectItems, new selectItemComparator());
		} catch (Exception e) {
			log.error("ERROR! convert to select item", e);
		}
		return selectItems;
	}

	public static ArrayList<SelectItem> hashMap2SelectListString(HashMap hm, String getterMethodId, String getterMethodName) {
		ArrayList<SelectItem> sList = new ArrayList<SelectItem>();
		try {
			Iterator iter = hm.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry e = (Map.Entry)iter.next();
				Object object = e.getValue();
				Class classDefinition = object.getClass();
				Method mId = classDefinition.getMethod( getterMethodId, (Class[])null);
				Method mName = classDefinition.getMethod(getterMethodName, (Class[])null);
				sList.add(new SelectItem(String.valueOf(mId.invoke(object, (Object[])null)),(String)mName.invoke(object, (Object[])null)));
			}
            Collections.sort(sList, new selectItemComparator());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sList;
	}

    /**
     * Format the UTC offset returned by JS Date.getTimezoneOffset() method like
     * GMT+/-HH:MM.
     *
     * @param utcOffset the UTC offset in mins as returned by Date.getTimezoneOffset() JS method
     * @return The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String formatJSUTCOffsetToString(int utcOffset) {
        // The Date.getTimezoneOffset() in JS returns -120 for GMT+2 for example and 240 for GMT+4
        String sign = utcOffset < 0 ? "+" : "-";
        int absUtcOffset = Math.abs(utcOffset);
        String hours = String.valueOf(absUtcOffset / 60);
        if (hours.length() < 2) {
            hours = "0" + hours;
        }
        String mins = String.valueOf(absUtcOffset % 60);
        if (mins.length() < 2) {
            mins = "0" + mins;
        }
        return "GMT" + sign + hours + ":" + mins;
    }

    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * The UTC offset is taken from ApplicationData
     *
     * @return The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String getUtcOffset(String utcOffset) {
    	return getAppData().getUtcOffset(utcOffset);
    }

	/**
     * Get UtcOffset with GMT+/-HH:MM format.
     * First take utcOffset from Session, if null return default.
     *
     * @return
     * 		The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String getUtcOffset() {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	String utcOffsetSession = null;
    	if(null != fc) {
    		utcOffsetSession = (String) fc.getExternalContext().getSessionMap().get(ConstantsBase.UTC_OFFSET);
    	}

    	if ( null == utcOffsetSession || utcOffsetSession.length() == 0 ) {   // utcOffset attribute from session is empty
    		utcOffsetSession = getDefualtUtcOffset();
            log.trace("Using default GMT offset");
    	}
    	return utcOffsetSession;
    }

    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * First take utcOffset from Session, if null return default.
     *
     * @return
     *      The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String getUtcOffset(HttpSession session, HttpServletRequest request) {
        String utcOffsetSession = (String) session.getAttribute(ConstantsBase.UTC_OFFSET);
        if (null == utcOffsetSession || utcOffsetSession.length() == 0) {
            utcOffsetSession = getDefualtUtcOffset(session, request);
            log.trace("Using default GMT offset");
        }
        return utcOffsetSession;
    }

    /**
     * Check if utcOffset attribute already in Session
     *
     * @return true - in case the attribue utcOffset in the Session
     */
    public static boolean isUtcOffsetInSession() {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	String utcOffsetSession = null;
    	if(null != fc) {
    		utcOffsetSession = (String) fc.getExternalContext().getSessionMap().get(ConstantsBase.UTC_OFFSET);
    		if ( null != utcOffsetSession ) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public static void resetJustLoggedInParameter() {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	HttpSession session = null;
    	synchronized (session = (HttpSession) fc.getExternalContext().getSession(false)) {
    		if (session.getAttribute(ConstantsBase.SESSION_JUST_LOGGED_IN) != null) {
    			session.setAttribute(ConstantsBase.SESSION_JUST_LOGGED_IN, false);
    		}
    	}
    }

    public static CacheObjects getCacheObjects() {
		FacesContext context = FacesContext.getCurrentInstance();
		CacheObjects appObj = (CacheObjects)context.getApplication().createValueBinding(ConstantsBase.BIND_CACHE_OBJECTS).getValue(context);
		return appObj;
    }

    /**
     * return SimpleDateFormat instance.
     * for AO we need a diferent format.
     * @return SimpleDateFormat
     */
    private static SimpleDateFormat FormatDateBySkin() {
    	SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");  // default format
		String appSource = ApplicationDataBase.getAppSource();
		if (appSource.equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_WEB)) {
            FacesContext context = FacesContext.getCurrentInstance();
            ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			Long skinId = a.getSkinId();
			if (null != skinId && skinId != Skin.SKIN_ETRADER) {
				sd = new SimpleDateFormat("dd.MM.yy");   // for AO format
			}
		}
    	return sd;
    }

    /**
     * Get value from utcOffset String
     * for example if we have GMT + 02:00 , return 2.
     * @param utcOffset
     * 		utcOffset String
     * @return
     * 		hour value from utcffset String
     */
    public static long getUtcOffsetValue(String utcOffset) {
    	long value = -1;
    	try {
	    	int start = utcOffset.indexOf("T");  // GMT +/-
	    	// get the hours from String
	    	start += 2;
	    	value = Long.valueOf(utcOffset.substring(start, start+2));

    	} catch (Exception e) {   // not a number
    		log.debug("utcOffset parameter is null, set offset value to -1");
    		value = -1;
		}
    	return value;
    }

    /**
     * The opposite of <code>formatJSUTCOffsetToString</code>. It takes an offset formatted like
     * GMT+/-XX:XX and turn it to +/-min value from gmt. For example GMT+02:00 becomes -120.
     *
     * @param utcOffset
     * @return Returns the offset in min. Just like the JS Date.getTimezoneOffset.
     */
//    public static int getUtcOffsetInMin(String utcOffset) {
//        int h = 0;
//        int m = 0;
//        try {
//            h = Integer.parseInt(utcOffset.substring(4, 6));
//            if (utcOffset.charAt(3) == '+') {
//                h = -h;
//            }
//            m = Integer.parseInt(utcOffset.substring(7));
//        } catch (Exception e) {
//            log.error("Can't convert UTC offset \"" + utcOffset + "\" to mins.", e);
//        }
//        return h * 60 + m;
//    }

    /**
     * Get utcOffset session value(not static function for jsf use)
     * @return
     */
    public String getUtcOffsetValue() {
    	return getUtcOffset();
    }

    /**
     * Get defualt utcOffset
     * @return
     */
    public static String getDefualtUtcOffset() {
    	String utcOffsetSession = ConstantsBase.OFFSET_GMT;
		if (isEtraderUserSkin()) {
    		TimeZone tzEt = TimeZone.getTimeZone("Israel");
    		utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
    		if (tzEt.inDaylightTime(new Date())) {
    			utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
    		}
		}
	   	return utcOffsetSession;
    }

    /**
     * Get defualt utcOffset
     * @return
     */
    public static String getDefualtUtcOffset(HttpSession session, HttpServletRequest request) {
        String utcOffsetSession = ConstantsBase.OFFSET_GMT;
        if (isEtraderUserSkin(session, request)) {
            TimeZone tzEt = TimeZone.getTimeZone("Israel");
            utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
            if (tzEt.inDaylightTime(new Date())) {
                utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
            }
        }
        return utcOffsetSession;
    }

    /**
     * Return Israel utcOffset in format GMT+/-xx:xx
     * @return
     */
    public static String getIsraelUtcOffset() {
    	String utcOffsetSession;
		TimeZone tzEt = TimeZone.getTimeZone("Israel");
		utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
		if (tzEt.inDaylightTime(new Date())) {
			utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
		}
	   	return utcOffsetSession;
    }



    /**
     * Levenshtein distance algorithm(dynamic programming)
     * The Levenshtein distance is a metric for measuring the amount of difference between two sequences (i.e., the so called edit distance).
     * The Levenshtein distance between two strings is given by the minimum number of operations needed to transform one string into the other,
     * where an operation is an insertion, deletion, or substitution of a single character.
     * @param str1
     * 		String 1 for compare
     * @param str2
     * 		String 2 for compare
     * @return
     * 		The minimum number of operations needed to transform one string into the other
     */
    public int levenshteinDistance(String str1, String str2) {
    	int leng1 = str1.length() + 1;
    	int leng2 = str2.length() + 1;

    	int[][] d = new int[leng1][leng2];

    	for ( int i = 0 ; i < leng1 ; i++ ) {
    		d[i][0] = i;
    	}

    	for ( int j = 0 ; j < leng2 ; j++ ) {
    		d[0][j] = j;
    	}

    	int cost = 0;

    	for ( int i = 1 ; i < leng1 ; i++ )
    		for ( int j = 1 ; j < leng2 ; j++ ) {

    			 if ( str1.charAt(i-1) == str2.charAt(j-1) ) {
    				 cost = 0;
    			 } else {
    				 	cost = 1;
    			 }

                 // (d[i-1, j] + 1): deletion , (d[i, j-1] + 1): insertion, (d[i-1, j-1] + cost): substitution
    			 d[i][j] = Math.min( Math.min(d[i-1][j] + 1, d[i][j-1] + 1) , d[i-1][j-1] + cost );
   		}

    	return d[leng1-1][leng2-1];
    }

    /**
     * Run unix command
     * @param command  unix command to run
     * @return
     */
     public static String runUnixCommand(String command) {

	       String s = "";
	       String res = "";

	        try {
	        		log.info("going to run unix command: " + command);
		            Process p = Runtime.getRuntime().exec(command);

		            BufferedReader stdInput = new BufferedReader(new
		                 InputStreamReader(p.getInputStream()));

		            BufferedReader stdError = new BufferedReader(new
		                 InputStreamReader(p.getErrorStream()));

		            // read the output from the command
			         while ((s = stdInput.readLine()) != null) {
			        	 log.info(s);
			             res += s;
			         }

			         // read any errors from the attempted command
			         while ((s = stdError.readLine()) != null) {
			            	res = ConstantsBase.UNIX_ERROR_STREAM + s;
			            	log.info(s);
			            	break;
			         }

	        } catch (IOException e) {
	        		log.info("Exception running unix command: " + e);
		            res = ConstantsBase.UNIX_ERROR_STREAM;
		    }

	        return res;
    }
     
     /**
      * Check mail validation with unix command
      * @param mail
      * @return

     public static boolean isEmailValidUnix(String mail) {
    	 if (getProperty("system").equalsIgnoreCase("local")){ //cancel validation from local env.
    		 return true;
    	 }
    	 if (!mail.trim().equals("") && mail.contains("@")) {
    		 String[] mailSuffix = mail.split("@");
    		 if (mailSuffix.length != 2){ //email must have only one '@'
    			 return false;
    		 }
    		 String command = ConstantsBase.UNIX_CHECKMX_COMMAND + mailSuffix[1];
    		 String unixRes = runUnixCommand(command);

    		 if ( IsParameterEmptyOrNull(unixRes)) {   // error with running command
    			 log.warn("Error with running unix checkmx  command, res: " + unixRes);
    			 return false;
    		 } else if (unixRes.equals("1")){ // result from CHECKMX
    			 return true;
    		 }
    	 }
		 return false;
     }*/
     /**
      * Check server name with unix command
      * @param mail
      * @return serve name
      */
     public static String getServerNameUnix() {
    	 if (getProperty("system").equalsIgnoreCase("local")) {
    		 return "Local-SERVER";
    	 }
    	 String command = ConstantsBase.UNIX_HOSTNAME_COMMAND;
    	 String unixRes = runUnixCommand(command);
    	 if (!CommonUtil.isParameterEmptyOrNull(unixRes)) {
    		 int index = 2; // in web we want to return server 00
    		 if (getProperty("application.source").equalsIgnoreCase("backend")) { // in BE we want to return BE00
    			 index = 0;
    		 }
    		 try {
    			 return unixRes.substring(index, unixRes.indexOf("."));
    		 } catch (Exception e) {
    			 return unixRes;
    		 }
    	 }
		 return "";
     }

 	public static void validateEmailFromUnix(FacesContext context, UIComponent comp, Object value) throws Exception {
		String mail = (String) value;
		if (!CommonUtil.isParameterEmptyOrNull(mail.trim())) {
			 if (isEmailValidUnix(mail)) {
				return;
	    	 }
		}
		String[] params = getLablePropertyFromComponent(comp);
		FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.banner.contactme.email", params));
		throw new ValidatorException(msg);
		}

     /**
      * Get users list from CSV file
      * @param fileName - filename with path
      * @return String[]
      */
     public static String[] getUsersListFromCSV(String fileName,String delimiter){
    	 try{ //
 		    BufferedReader fr = new BufferedReader(new FileReader(fileName));
 		    String users = new String();
 		    StringBuffer contents = new StringBuffer();
 		    // repeat until all lines is read
 		    while ((users = fr.readLine()) != null)
 		    {
 		        contents.append(users);
 		        contents.append(delimiter);
 		    }

 		    return contents.toString().substring(0,contents.toString().length()-1).split(delimiter);
         }catch(IOException ioe){
         	log.fatal("Error reading users CVS file !!! Job aborted");
         }finally{
         }
         return null;

     }

     public static String getUsersListAmountFromTXT(String fileName,String delimiter, HashMap<Long, Long> hm) throws Exception, IOException{
    	 String fromFile = "";
    	 String usersFromFile = "";
    	 BufferedReader fr;
		 try {
			 fr = new BufferedReader(new FileReader(fileName));

			 while ((fromFile = fr.readLine())!= null){
				 String[] a = fromFile.split(delimiter);
				 Long userId = Long.parseLong(a[0].trim());
				 Double amount = Double.parseDouble(a[1].trim());
				 usersFromFile += userId.toString() + ",";
				 hm.put(userId, CommonUtil.calcAmount(amount));
			   }
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 return usersFromFile;
     }

 	/**
 	 * This method translates a select item list
 	 * @param list the list to translate
 	 * @return translated list
 	 */
 	public static ArrayList<SelectItem> translateSI(ArrayList<SelectItem> list) {
 		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
 		for (SelectItem si : list) {
 			String lable = CommonUtil.getMessage(si.getLabel(), null);
 			newList.add(new SelectItem(si.getValue(), lable));
 		}
 		return newList;
 	}

    public static String getBonusDescription(long bonusAmountTxt,double bonusPercent,long sumInvQualify,long  numberOfActions,
    										String bonusStateDescription,String bonusMinDeposit,String bonusMaxDeposit,
    										String bonusMinInvestAmount, String bonusMaxInvestAmount, String bonusWinOdds, String bonusLoseOdds, String currencyId) {
		String[] params = new String[12];
		params[0] = String.valueOf(bonusAmountTxt);
		params[1] = "x";
		params[2] = String.valueOf(bonusPercent*100);
		params[3] = String.valueOf(sumInvQualify);
		params[4] = String.valueOf(numberOfActions);
		params[5] = String.valueOf(numberOfActions+1);
		params[6] = String.valueOf(bonusMinDeposit);
		params[7] = String.valueOf(bonusMaxDeposit);
		params[8] = String.valueOf(bonusMinInvestAmount);
		params[9] = CommonUtil.displayAmount(bonusMaxInvestAmount,true,Integer.valueOf(currencyId));
		params[10] = String.valueOf(bonusWinOdds);
		params[11] = String.valueOf(bonusLoseOdds);
		return CommonUtil.getMessage(bonusStateDescription, params);
	}

    /**
     * get HashMap<Long, String> and return new ArrayList of SelectItem
     * and sort the list with SelectItem comperator
     * @param h HashMap<Long, String>
     * @return
     */
    public static ArrayList<SelectItem> hashMap2SortedArrayList(HashMap<Long, String> h) {
    	ArrayList<SelectItem> l = new ArrayList<SelectItem>();
    	ArrayList<SelectItem> list = new ArrayList<SelectItem>();
    	list.add(new SelectItem(new Long(0),"All"));
   		if (h.size() > 0) {
			for (Iterator<Long> i = h.keySet().iterator(); i.hasNext(); ) {
				Long elm = i.next();
				l.add(new SelectItem(new Long(elm), h.get(elm)));
			}
			Collections.sort(l, new selectItemComparator());
   		}
   		list.addAll(l);
   		return list;
    }

    /**
     * SelectItem comperator class
     * @author kobi
     */
    public static class selectItemComparator implements Comparator<SelectItem> {
        @Override
		public int compare(SelectItem a, SelectItem b) {
            //log.debug("a = " + a.getLabel() + " b = " + b.getLabel());
            return a.getLabel().compareToIgnoreCase(b.getLabel());
        }
    }

	/**
	 * Send permanent redirect
	 * @param response
	 * @param url  full url for redirect to
	 * @return
	 */
	public static void sendPermanentRedirect(HttpServletResponse response, String url) {
		response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
	    response.setHeader( "Location", url);
	    response.setHeader( "Connection", "close" );
	}


	/**
	 * Return country prefix by country id
	 * @param countryId
	 * @return prefix country
	 */
	public static String getPrefixFromCountry(long countryId){
		Country c = ApplicationDataBase.getCountry(countryId);
		return c.getPhoneCode();
	}


	/**
	 * Return the distance between the dates in minutes / hours / days
	 * @param d1 - the first date
	 * @param d2 - the second date
	 * @return distance in minutes / hours / days
	 */

	public static String getDateDistance(Date d1, Date d2, Locale locale) {
		if (null == d1 || null == d2) {
			return "";
		}
		if (d1.after(d2)) {
			return "";
		}
		GregorianCalendar gc1 = new GregorianCalendar();
		GregorianCalendar gc2 = new GregorianCalendar();
		gc1.setTime(d1);
		gc2.setTime(d2);
		gc1.set(GregorianCalendar.MILLISECOND, 0);
		gc2.set(GregorianCalendar.MILLISECOND, 0);
		gc1.set(GregorianCalendar.SECOND, 0);
		gc2.set(GregorianCalendar.SECOND, 0);

		long minDis = (gc2.getTimeInMillis()-gc1.getTimeInMillis()) / (60*1000);
		if (minDis == 1) {
			if (null == locale) {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(minDis), CommonUtil.getMessage("showOff.sentence.minute", null)});
			} else {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(minDis), CommonUtil.getMessage("showOff.sentence.minute", null, locale)});
			}
		}
		if (minDis < 60) {
			if (null == locale) {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(minDis), CommonUtil.getMessage("showOff.sentence.minutes", null)});
			} else {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(minDis), CommonUtil.getMessage("showOff.sentence.minutes", null, locale)});
			}
		}
		gc1.set(GregorianCalendar.MINUTE, 0);
		gc2.set(GregorianCalendar.MINUTE, 0);
		long hourDis = (gc2.getTimeInMillis()-gc1.getTimeInMillis()) / (60*60*1000);
		if (hourDis == 1) {
			if (null == locale) {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(hourDis), CommonUtil.getMessage("showOff.sentence.hour", null)});
			} else {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(hourDis), CommonUtil.getMessage("showOff.sentence.hour", null, locale)});
			}
		}
		if (hourDis < 24) {
			if (null == locale) {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(hourDis), CommonUtil.getMessage("showOff.sentence.hours", null)});
			} else {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(hourDis), CommonUtil.getMessage("showOff.sentence.hours", null, locale)});
			}
		}
		gc1.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc2.set(GregorianCalendar.HOUR_OF_DAY, 0);
		long dayDis = (gc2.getTimeInMillis()-gc1.getTimeInMillis()) / (24*60*60*1000);
		if (dayDis == 1) {
			if (null == locale) {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(dayDis), CommonUtil.getMessage("showOff.sentence.day", null)});
			} else {
				return CommonUtil.getMessage("showOff.sentence.time.distance",
						new String[]{String.valueOf(dayDis), CommonUtil.getMessage("showOff.sentence.day", null, null)});
			}
		}
		if (null == locale) {
			return CommonUtil.getMessage("showOff.sentence.time.distance",
					new String[]{String.valueOf(dayDis), CommonUtil.getMessage("showOff.sentence.days", null)});
		} else {
			return CommonUtil.getMessage("showOff.sentence.time.distance",
					new String[]{String.valueOf(dayDis), CommonUtil.getMessage("showOff.sentence.days", null, locale)});
		}
	}

	public static String removeFractionFromAmount(String amount) {
		String res = "";
		try {
			Pattern regex = Pattern.compile("(.*)([\\d,]*)\\.\\d{2}(.*)",
				Pattern.CANON_EQ);
			Matcher matcher = regex.matcher(amount);
			if (matcher.find()) {
				res = matcher.group(1) + matcher.group(2) + matcher.group(3);
			}
		} catch (PatternSyntaxException ex) {
		}

		return res;
	}

	/**
	 * return Date object of the given date on 0:00:00.000
	 * @param date
	 * @return Date object of the given date on 0:00:00.000
	 */
	public static Date getStartOfDay(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calendar.set(GregorianCalendar.MINUTE, 0);
		calendar.set(GregorianCalendar.SECOND, 0);
		calendar.set(GregorianCalendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * return Date object of the given date on 23:59:59.999
	 * @param date
	 * @return Date object of the given date on 23:59:59.999
	 */
	public static Date getEndOfDay(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(GregorianCalendar.HOUR_OF_DAY, 23);
		calendar.set(GregorianCalendar.MINUTE, 59);
		calendar.set(GregorianCalendar.SECOND, 59);
		calendar.set(GregorianCalendar.MILLISECOND, 999);

		return calendar.getTime();
	}

	public static String diacriticsCharsEncode(String str) {
		str = str.replaceAll("\u00a1", "&#161;");
		str = str.replaceAll("\u00a2", "&#162;");
		str = str.replaceAll("\u00a3", "&#163;");
		str = str.replaceAll("\u00a4", "&#164;");
		str = str.replaceAll("\u00a5", "&#165;");
		str = str.replaceAll("\u00a6", "&#166;");
		str = str.replaceAll("\u00a7", "&#167;");
		str = str.replaceAll("\u00a8", "&#168;");

		str = str.replaceAll("\u00e1", "&#225;");
		str = str.replaceAll("\u00c1", "&#193;");
		str = str.replaceAll("\u00e0", "&#224;");
		str = str.replaceAll("\u00c0", "&#192;");
		str = str.replaceAll("\u00e2", "&#226;");
		str = str.replaceAll("\u00c2", "&#194;");
		str = str.replaceAll("\u00e5", "&#229;");
		str = str.replaceAll("\u00c5", "&#197;");
		str = str.replaceAll("\u00e3", "&#227;");
		str = str.replaceAll("\u00c3", "&#195;");
		str = str.replaceAll("\u00e4", "&#228;");
		str = str.replaceAll("\u00c4", "&#196;");
		str = str.replaceAll("\u00e6", "&#230;");
		str = str.replaceAll("\u00c6", "&#198;");
		str = str.replaceAll("\u00e7", "&#231;");
		str = str.replaceAll("\u00c7", "&#199;");
		str = str.replaceAll("\u00e9", "&#233;");
		str = str.replaceAll("\u00c9", "&#201;");
		str = str.replaceAll("\u00e8", "&#232;");
		str = str.replaceAll("\u00c8", "&#200;");
		str = str.replaceAll("\u00ea", "&#234;");
		str = str.replaceAll("\u00ca", "&#202;");
		str = str.replaceAll("\u00eb", "&#235;");
		str = str.replaceAll("\u00cb", "&#203;");
		str = str.replaceAll("\u00ed", "&#237;");
		str = str.replaceAll("\u00cd", "&#205;");
		str = str.replaceAll("\u00ec", "&#236;");
		str = str.replaceAll("\u00cc", "&#204;");
		str = str.replaceAll("\u00ee", "&#238;");
		str = str.replaceAll("\u00ce", "&#206;");
		str = str.replaceAll("\u00ef", "&#239;");
		str = str.replaceAll("\u00cf", "&#207;");

		str = str.replaceAll("\u0130", "&#304;");
		str = str.replaceAll("\u0131", "&#305;");

		str = str.replaceAll("\u011f", "&#287;");
		str = str.replaceAll("\u011e", "&#286;");


		str = str.replaceAll("\u00f1", "&#241;");
		str = str.replaceAll("\u00d1", "&#209;");
		str = str.replaceAll("\u00f3", "&#243;");
		str = str.replaceAll("\u00d3", "&#211;");
		str = str.replaceAll("\u00f2", "&#242;");
		str = str.replaceAll("\u00d2", "&#210;");
		str = str.replaceAll("\u00f4", "&#244;");
		str = str.replaceAll("\u00d4", "&#212;");
		str = str.replaceAll("\u00f8", "&#248;");
		str = str.replaceAll("\u00d8", "&#216;");
		str = str.replaceAll("\u00f5", "&#245;");
		str = str.replaceAll("\u00d5", "&#213;");
		str = str.replaceAll("\u00f6", "&#246;");
		str = str.replaceAll("\u00d6", "&#214;");

		str = str.replaceAll("\u00d7", "&#215;");
		str = str.replaceAll("\u00f7", "&#247;");

		str = str.replaceAll("\u00df", "&#223;");
		str = str.replaceAll("\u00ff", "&#255;");

		str = str.replaceAll("\u00fa", "&#250;");
		str = str.replaceAll("\u00da", "&#218;");
		str = str.replaceAll("\u00f9", "&#249;");
		str = str.replaceAll("\u00d9", "&#217;");
		str = str.replaceAll("\u00fb", "&#251;");
		str = str.replaceAll("\u00db", "&#219;");
		str = str.replaceAll("\u00fc", "&#252;");
		str = str.replaceAll("\u00dc", "&#220;");

		str = str.replaceAll("\u015e", "&#350;");
		str = str.replaceAll("\u015f", "&#351;");

		str = str.replaceAll("\u00b4", "&#180;");
		str = str.replaceAll("`", "&#96;");

		return str;
	}

	/**
	 * Get Israel offset
	 * @return
	 */
	public static int getIsraelOffset(){
		TimeZone tzEt = TimeZone.getTimeZone("Israel");
		int utcOffset = ConstantsBase.ISRAEL_UTCOFFSET_PERIOD2;
		if (tzEt.inDaylightTime(new Date())) {
			utcOffset = ConstantsBase.ISRAEL_UTCOFFSET_PERIOD1;
		}
		return utcOffset;
	}

	/**
	 * Get Israel Offset for TLV clock
	 * @return
	 */
	public int getIsraelOffsetVal(){
		return getIsraelOffset();
	}

	/**
	 * To convert the InputStream to String we use the BufferedReader.readLine()
	 * method. We iterate until the BufferedReader return null which means
	 * there's no more data to read. Each line will appended to a StringBuilder
	 * and returned as String.
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } finally {
                is.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }

	public static String getUserAgent() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		return getUserAgentWithRequest(request);
	}

	public static String getUserAgentWithRequest(HttpServletRequest request) {
		String userAgent = (String) request.getSession().getAttribute(ConstantsBase.USER_AGENT);
		if (null == userAgent) {  // not in session
			userAgent = request.getHeader("User-Agent");
			if (null != userAgent) {
				userAgent = userAgent.toLowerCase();
				request.getSession().setAttribute(ConstantsBase.USER_AGENT, userAgent);
			} else {
				log.trace("User-Agent is Null! Request: " + request.getRequestURL());
			}
		}
		return userAgent;
	}

	/**
	 * Check if it's mobile client by User-Agent values
	 * in order to recognize mobile behavior (for web pages / app)
	 */
    public static boolean isMobileUserAgentBehavior() {
		String userAgent = getUserAgent();
		if (null != userAgent &&
				(userAgent.indexOf("iphone") > -1 ||
				userAgent.indexOf("android") > -1 ||
				userAgent.indexOf("ipod") > -1 ||
				userAgent.indexOf("ipad") > -1)) {
			return true;
		}
		return false;
	}

	/**
	 * Check if user agent is mobile
	 */
    public static boolean isMobileUserAgent(){
//    	String userAgent = getUserAgent();
//    	if (null != userAgent &&
//    			(userAgent.matches(".*(android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino).*")||userAgent.substring(0,4).matches("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-"))) {
//    		return true;
//    	}
    	return false;
    }

	/**
	 * Check if it's mobile client by User-Agent values with the allowed skins
	 */
    public static boolean isMobile() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		if (isMobileUserAgentBehavior()) {
//			ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
//			if (null != a.getSkinId() && !isHebrewSkin(a.getSkinId())) {
//				return true;
//			} else {
//				return false;
//			}
//		}
		return false;
	}

	/**
	 * Check if it's mobile application: we came from iphoneapp.jsf and we have duid in the session
	 */
    public static boolean isMobileApp() {
    	if (isMobile()) {
    		FacesContext context = FacesContext.getCurrentInstance();
    		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
			String duid = (String)request.getSession().getAttribute(ConstantsBase.DEVICE_UNIQUE_ID);
			if (null == duid) {
				return false;
			}
    	} else {
    		return false;
    	}
		return true;
	}

    /**
     * Return true for web writers.
     */
    public static int getWebWriterId() {
//    	if (isMobile()) {
//    		return Writer.WRITER_ID_MOBILE;
//    	}
    	return Writer.WRITER_ID_WEB;
    }

    /**
     * Check if it's a web writer
     * @param writerId
     * @return
     */
    public static boolean isWebWriterId(long writerId) {
    	if (writerId == Writer.WRITER_ID_WEB ||
    			writerId == Writer.WRITER_ID_MOBILE) {
    		return true;
    	}
    	return false;
    }

	/**
	 * Get lable property from the component, for validations use
	 * @param comp
	 * @return
	 */
	public static String[] getLablePropertyFromComponent(UIComponent comp) {
		String[] params = new String[1];
		params[0] = "";
		if ((!isEtraderUserSkin() && isMobile()) || (null != comp.getAttributes().get("vLabel"))) {
			String componentLabe = (String) comp.getAttributes().get("vLabel");
			if (null != componentLabe) {
				params[0] = getMessage(componentLabe, null) + ": ";
			}
		}
		return params;
	}

	/**
	 *
	 * @return true if the user is using Etrader or TLV skin
	 */
	public static boolean isHebrewUserSkin() {
       FacesContext context = FacesContext.getCurrentInstance();
       ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		if ((a.getSkinId() == Skin.SKIN_ETRADER) || (a.getSkinId() == Skin.SKIN_TLV)) {
			return true;
		}
		return false;
	}


    public static void alertOverDepositLimitByEmail(Transaction tr, UserBase user) {
    	long threshold_deposit = TransactionsManagerBase.getDepositAlertLimits(user.getLimitIdLongValue());
        if (tr.getAmount()/100 >= threshold_deposit && !user.isTestUser()){
            try{
                new SendEmailOverDepositLimit(tr, user, threshold_deposit).start();
            }catch (Exception e) {
                log.warn("Error, problem sending email over " + ConstantsBase.THRESHOLD_DEPOSIT + " deposit! " + e);
            }
        }
    }

    public static void SendReceiptEmail(Connection con, Transaction tr, UserBase user) throws SQLException {

        String langCode = "";
        String subject = "";
        subject=CommonUtil.getMessage("receipt.email.subject", null);
        long langId = user.getSkin().getDefaultLanguageId();
        langCode = LanguagesDAOBase.getCodeById(con, langId);
        if( CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
            langCode = ConstantsBase.ETRADER_LOCALE;
        }
        String receiptNum = String.valueOf(tr.getId());
        String footer="";
        String[] param = new String[1];
        param[0] = String.valueOf(tr.getCc4digit());
        footer=CommonUtil.getMessage("receipt.footer.creditcard.mb", param);
        footer=footer + "<br><br>"  + CommonUtil.getMessage("receipt.bank_descriptor", null);

        // collect the parameters we neeed for the email
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(SendReceiptEmail.PARAM_AMOUNT, displayAmount(tr.getAmount(), tr.getCurrency().getId()));
        params.put(SendReceiptEmail.PARAM_DATE, tr.getTimeCreatedTxt());
        params.put(SendReceiptEmail.PARAM_EMAIL, user.getEmail());
        params.put(SendReceiptEmail.PARAM_FOOTER, footer);
        params.put(SendReceiptEmail.PARAM_RECEIPT_NUM, receiptNum);
        params.put(SendReceiptEmail.PARAM_USER_ADDRESS, user.getStreet() + " " + user.getStreetNo() + " " + user.getCityName() + " " + user.getZipCode());
        params.put(SendReceiptEmail.PARAM_USER_NAME, user.getFirstName() + " " + user.getLastName());

        try{
            new SendReceiptEmail(params, null, langCode, subject, user.getSkinId()).start();
        }catch (Exception e) {
            log.warn("Error, problem sending receipt" + e);
        }
    }

	public static void validateSmsCharacters(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		String englishPttern = CommonUtil.getMessage("general.validate.english.and.punctuation", null);
		String params[] = null;

		if (v.matches(englishPttern)) {
			 if(v.length() > ConstantsBase.SMS_ENGLISH_CHAR_LIMIT){
				params = new String[2];
				params[0] = String.valueOf(ConstantsBase.SMS_ENGLISH_CHAR_LIMIT);
			 }
		}else{
			if(v.length() > ConstantsBase.SMS_OTHER_CHAR_LIMIT){
				params = new String[2];
				params[0] = String.valueOf(ConstantsBase.SMS_OTHER_CHAR_LIMIT);
			 }
		}

		if (null != params){
			params[1] = String.valueOf(v.length());
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LengthValidator.MAXIMUM_detail", params));
			throw new ValidatorException(msg);
		}
	}
	/**
	 * The function validate for only characters that correspond to language.
	 *
	 * @param context
	 * @param comp
	 * @param value
	 * @throws Exception
	 */
	public static void validateSmsCharactersLanguage(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		String englishPttern = CommonUtil.getMessage("general.validate.english.and.punctuationForSms", null);
		String params[] = null;

		if (v.matches(englishPttern)) {
			 if(v.length() > ConstantsBase.SMS_ENGLISH_CHAR_LIMIT){
				params = new String[2];
				params[0] = String.valueOf(ConstantsBase.SMS_ENGLISH_CHAR_LIMIT);
			 }
		}else{
			if(v.length() > ConstantsBase.SMS_OTHER_CHAR_LIMIT){
				params = new String[2];
				params[0] = String.valueOf(ConstantsBase.SMS_OTHER_CHAR_LIMIT);
			 }
		}

		if (null != params){
			params[1] = String.valueOf(v.length());
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LengthValidator.MAXIMUM_detail", params));
			throw new ValidatorException(msg);
		}
	}

	public static boolean validateEnglishLettersOnly(String value) throws Exception {
		String pattern = "^['a-zA-Z ]+$";
		String v = value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			return false;
		}
		return true;
	}

	public static boolean validateHebrewLettersOnly(String value) throws Exception {
		String pattern = CommonUtil.getMessage("general.validate.hebrew", null);
		String v = value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * @param time
	 * @return Date format: yyyy-MM-dd HH:mm:ss.SSS
	 */
	public static Date getTimeWithoutTimezone(String time) {
        Date d = null;
        if (null != time) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            try {
                d = df.parse(time);
            } catch (ParseException pe) {
                log.error("ParseException, Can't parse String to Date.", pe);
            }
        }
        return d;
    }

    public static String getBackupMessage(String key, Object[] params, Locale locale) {
    	ResourceBundle bundle;
    	if(locale==null){
    		bundle = ResourceBundle.getBundle("messages");
    	}
    	else {
	    	if(backup==null){
	    		backup = new HashMap<Locale, ResourceBundle>();
	    	}
	    	bundle = backup.get(locale);
	    	if(bundle == null){
	    			bundle = ResourceBundle.getBundle("MessageResources", locale);
	    			backup.put(locale, bundle);
	    	}
    	}

    	if (key == null || key.trim().equals("")) {
			return "";
        }
		String text = null;
		String defval = "?"+key+"?";
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			if (log.isDebugEnabled()) {
	            log.debug("Missing value for key :" + key + " Default value is :" + defval);
            }
			return defval;
		}
		if (params != null) {
			text = text.replaceAll("'", "@@@@@");
			MessageFormat mf = new MessageFormat(text);
			text = mf.format(params, new StringBuffer(), null).toString();
			text = text.replaceAll("@@@@@", "'");
		}
		return text.trim();
    }

    public static void sendNotifyForInvestment(long oppId,
			double investmentId,
			double amount,
			long origAmount,
			long type,
			long marketId,
			int productTypeId,
			double level,
			LevelsCache levelsCache,
			UserBase user,
			CopyOpInvTypeEnum cpOpInvType) {
    	sendNotifyForInvestment(oppId,
    							investmentId,
    							amount,
    							origAmount,
    							type,
    							marketId,
    							productTypeId,
    							level,
    							levelsCache,
    							user,
    							0,
    							0,
    							cpOpInvType);
    }

    public static void sendNotifyForInvestment(long oppId,
    											double investmentId,
    											double amount,
    											long origAmount,
    											long type,
    											long marketId,
    											int productTypeId,
    											double level,
												LevelsCache levelsCache,
												UserBase user,
												double aboveTotal,
												double belowTotal,
			    								CopyOpInvTypeEnum cpOpInvType) {
    	String cityName;
    	long countryId;
    	String cityLatitude;
    	String cityLongtitude;
    	long currencyId;

    	if (user.getSkinId() == Skins.SKIN_ETRADER) {
    		LiveGlobeCity randCity = LiveHelper.getRandomCityForEtrader();
    		log.info("Investment [" + investmentId
    					+ "] from opportunity [" + oppId
    					+ "] comes from ETrader, replacing city info and amount for display with ["
    					+ randCity + "]");
    		cityName = randCity.getCityName();
    		countryId = randCity.getCountryId();
    		cityLatitude = randCity.getGlobeLatitude();
    		cityLongtitude = randCity.getGlobeLongtitude();

    		origAmount = Math.round((1.0D / CurrencyRatesManagerBase.getInvestmentsRate(randCity.getCurrencyId())) * amount);

    		currencyId = randCity.getCurrencyId();
    	} else {
    		cityName = user.getCityFromGoogle();
    		countryId = user.getCountryId();
    		cityLatitude = user.getCityLatitude();
    		cityLongtitude = user.getCityLongitude();
    		currencyId = user.getCurrencyId();
    	}

    	String amountForDisplay;
    	long flooredAmount;
    	if (amount > 0d) {
    		long flooredOrigAmount = new Double(Math.floor(origAmount / 100d)).longValue() * 100L;
    		amountForDisplay = displayAmount(flooredOrigAmount, true, currencyId, false, ConstantsBase.DISPLAY_FORMAT_LIVE_PAGE);
    		flooredAmount = Math.round(CurrencyRatesManagerBase.getInvestmentsRate(currencyId) * flooredOrigAmount);
    	} else {
    		amountForDisplay = "-1";
    		flooredAmount = -1L;
    	}

        levelsCache.notifyForInvestment(oppId,
        								investmentId,
        								amount,
        								type,
        								amountForDisplay,
        								cityName,
        								countryId,
        								marketId,
        								productTypeId,
        								user.getSkinId(),
        								level,
        								System.currentTimeMillis(),
        								cityLatitude,
        								cityLongtitude,
        								aboveTotal,
        								belowTotal,
        								flooredAmount,
        								cpOpInvType);
    }

    public static boolean isUserSkinRegulated() {
    	try {
	        FacesContext context = FacesContext.getCurrentInstance();
	        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
	        Skin s = ApplicationDataBase.getSkinById(a.getSkinId());
	        if(s.isRegulated()) {
	        	return true;
	        }
        } catch (Exception e) {
        	return false;
		}
    	return false;
    }

    public boolean isSkinRegulated() {
    	return isUserSkinRegulated();
    }

    public String getSkinSuffix() {
    	if (isSkinRegulated()){
    		return SKIN_SUFFIX_REGULATED;
    	}else {
    		return null;
    	}
    }

	public static String getLocaleLanguage() {
		Locale l = null;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
	        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			l = a.getUserLocale();
		} catch (Exception e) {
			log.error("Can't get locale , take default locale!");
			l = new Locale(ConstantsBase.LOCALE_DEFAULT); // take default locale
		}
		return l.getLanguage();
	}

	public static long getFreeRemarketingContact(UserBase user) {
		log.debug("start getFirstFreeContact");
		Contact contactTemp = new Contact();
		try {
			contactTemp = RemarketingLoginsManagerBase.getFirstFreeContact(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug("can't get free contact ", e);
			return 0;
		}
		log.debug("finish getFirstFreeContact");
		return contactTemp != null ? contactTemp.getId() : user.getContactId();
	}

	public static long getFreeRemarketingContact(UserBase user, Contact contact){
		log.debug("start getFirstFreeContact");
		Contact contactTemp = new Contact();
		try {
			contactTemp = RemarketingLoginsManagerBase.getFirstFreeContact(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.debug("can't get free contact ", e);
			return 0;
		}
		log.debug("finish getFirstFreeContact");
		return contactTemp != null ? contactTemp.getId() : contact.getId();
	}

	/**
	 * Format <code>Date</code> with pattern "dd/MM/yy HH:mm" and time zone.
	 *
	 * @param d
	 * @param timeZoneName
	 * @return
	 */
	public static String getTimeAndDateFormat(Date d, String timeZoneName) {
	    return formatDate(d, timeZoneName, "dd/MM/yy HH:mm");
	}

	/*public static int daysBetween(Date date1, Date date2) {
		long msDiff;
		msDiff = date1.getTime() - date2.getTime();
		int daysDiff = (int) Math.round(msDiff / MILLIS_PER_DAY);
		return daysDiff;
	}

	public static boolean validateNumbersAndCommasOnly(String value) throws Exception {
		String pattern = "^[0-9,]+$";
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			return false;
		}
		return true;
	}*/

	public static void validateChineseLettersOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = CommonUtil.getMessage("general.validate.chinese.letters.only", null);
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.chinese.letters.only", params));
			throw new ValidatorException(msg);
		}
	}

	public static String getOppTimeTxt(Date timeEstClosing) {
        return getDateTimeFormatDisplay(timeEstClosing, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
    }
	
	/* Merged code */
	// public static void sendRegistrationSMSByTextLocalProvider(UserBase user, Locale locale) {
	//
	// if (user == null || user.getId() == 0) {
	// log.error("User is null! Registration SMS can not be send!");
	// return;
	// } else if (user.getSkinId() == Skin.SKIN_CHINESE || user.getSkinId() == Skin.SKIN_KOREAN) {
	// log.error("Can not send registration SMS for CHINESE and KOREAN skin!");
	// return;
	// }
	//
	// try {
	// String senderName = "";
	// String senderNumber = "";
	// String phoneCode = "";
	// //String encodedMsg = "";
	// String msg = "";
	// long smsId = 0;
	//
	// // determine SMS message and link app based on user platform id
	// senderNumber = CountryManagerBase.getById(user.getCountryId()).getSupportPhone();
	// phoneCode = CountryManagerBase.getPhoneCodeById(user.getCountryId());
	// Object[] params = new Object[1];
	// if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ERTADER) {
	// senderName = "etrader";
	// params[0] = getMessage(locale, "sms.register.link.etrader", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ANYOPTION) {
	// senderName = "anyoption";
	// params[0] = getMessage(locale, "sms.register.link.anyoption", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_COPYOP) {
	// senderName = "copyop";
	// params[0] = getMessage(locale, "sms.register.link.copyop", null);
	// msg = getMessage(locale, "sms.register.copyop", params );
	// } else {
	// senderName = "backend";
	// if (user.getSkinId() == Skin.SKIN_ETRADER) {
	// params[0] = getMessage(locale, "sms.register.link.etrader", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else {
	// params[0] = getMessage(locale, "sms.register.link.anyoption", null);
	// msg = getMessage(locale, "sms.register", params );
	// }
	// }
	//
	// if (msg.equals("")) {
	// log.error("Can NOT get sms message from bundle.");
	// return;
	// }
	// // determine provider by character length
	// /*if ( msg.length() <= ConstantsBase.MAX_CHARACTERS_ALLOWED_BY_MBLOX_PROVIDER ) {
	// smsId = SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
	// SMSManagerBase.SMS_KEY_TYPE_SMS, ConstantsBase.MBLOX_PROVIDER);
	// } else {}*/
	// /* STOP encoding msg for TextLocal provider MONI's request!!!*/
	// //Switching sms providers from TextLocal to Mobivate and no need for special encoding for now...
	// //encodedMsg = encodeRegisterMessage(msg);
	// smsId = SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
	// SMSManagerBase.SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_WELCOME, MobileNumberValidation.NOTVALIDATED);
	//
	//
	// // insert SMS ISSUE for backend traking purposes
	// if (smsId == 0) {
	// log.error("SMS id is 0.");
	// return;
	// } else {
	// SMSManagerBase.insertIssueForSMS(user, new Date(), msg, smsId);
	// }
	//
	// } catch (SMSException smsEx) {
	// log.error("Enable to send registration sms for user with id: " + user.getId(), smsEx);
	// } catch (SQLException sqlE) {
	// log.error("Enable to send registration sms for user with id: " + user.getId(), sqlE);
	// }
	// }
	
	public static boolean validateIsraeliIdNumber(String idnum) {
		boolean isNotValid = false;
		
		if (idnum.equals(ConstantsBase.NO_ID_NUM)) {
			isNotValid = false;
		} else {
			if (!NumberUtils.isNumber(idnum)) {
				isNotValid = true;
			} else {
				if (idnum.length() != ConstantsBase.ISRAELI_ID_NUMBER_ALLOWED_LENGTH) {
					isNotValid = true;
				}
			}
		}
		return isNotValid;
	}
	
	public static String getUkashCurrency(UkashDeposit vo){
		  if (vo.getVoucherCurrencyId() != 0) {
			  return ApplicationDataBase.getCurrencyById(vo.getVoucherCurrencyId()).getCode();
		  } else {
			  return "";
		  }
	}
	
	public static void addFacesMessage(Severity type, String key, Object[] params) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(type, CommonUtil.getMessage(key,params), null);
		context.addMessage(null, fm);
	}
	
	public static void setUserBirthDayMonthYear(UserBase user) {
		Date timeBirthDate = user.getTimeBirthDate();
		if (timeBirthDate != null) {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(timeBirthDate);
			user.setBirthYear(String.valueOf(c.get(GregorianCalendar.YEAR)));
			user.setBirthMonth(String.valueOf(c.get(GregorianCalendar.MONTH) + 1));
			if (user.getBirthMonth().length() == 1) {
				user.setBirthMonth("0" + user.getBirthMonth());
			}
			user.setBirthDay(String.valueOf(c.get(GregorianCalendar.DAY_OF_MONTH)));
			if (user.getBirthDay().length() == 1) {
				user.setBirthDay("0" + user.getBirthDay());
			}
		} else {
			user.setBirthYear("");
			user.setBirthMonth("");
			user.setBirthDay("");
		}
	}
	
	public static ArrayList<PromotionBannerSlider> getBanner(String bannerPlace){
		ArrayList<PromotionBannerSlider> cachedSliderImages = PromotionManagementManagerBase.getActiveSliders(bannerPlace).values().iterator().next();	
		return cachedSliderImages;
	}
	
	/**
	 * Get append string by array
	 * @param temp
	 * @return
	 */
	public static String getAppendStringByArray(List<String> temp) {
		String list = ConstantsBase.EMPTY_STRING;
	    for (int i = 0; i < temp.size(); i++) {
	    	list += temp.get(i) + ",";
	    }
	    return list.substring(0,list.length()-1);
	}
	
	public static long getLoginId(HttpSession session) {
		long loginId = 0l;
		try {
			if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
		}
		return loginId;
	}
	
	public static String displayAmountForInput(long amount, boolean showCurrency, long currencyId) {
		String format = null;
		try {
			format = CurrenciesManagerBase.getAmountFormat(CurrenciesManagerBase.getCurrency(currencyId), true);
		} catch (Exception e) {
			log.error("Can't format displayAmount.", e);
			return null;
		}
		DecimalFormat sd = new DecimalFormat(format, new DecimalFormatSymbols(Locale.US));
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			out = getMessage(getCurrencySymbol(currencyId), null) + sd.format(amountDecimal);
		} else {
			out = sd.format(amountDecimal);
		}

		return out;
	}
	
	/**
	 * ***************************************
	 * ***** Migration from job process. *****
	 * ***************************************
	 * 
     * Send single email and create issues with email action
     * @param conn
     * @param serverProperties  email server properties
     * @param templateName  the html file name
     * @param params the email parameters
     * @param subjectKey the subject key of the email, must be: key + "." language code
     * @param createIssue true for creating a new email issue
     * @param isContacts true for user details from contacts
     * @param issueSubjectId for general subject, set to 0
     * @param user user instance
     * @throws Exception
     */
    public static void sendSingleEmail(Connection conn, Hashtable serverProperties,
    		String templateName,
    		HashMap<String, String> params, String subjectKey, String commentKey,
    		boolean createIssue, boolean isContacts, long issueSubjectId, UserBase user, Long recordId, long templateId)
    		throws Exception {

    	long langId = SkinsDAOBase.getById(conn, (int)user.getSkinId()).getDefaultLanguageId();
    	String langCode = LanguagesDAOBase.getCodeById(conn, langId);
    	log.debug("skinId: " + user.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
    	Template mailTemplate = getEmailTemplate(templateName, langCode, String.valueOf(user.getSkinId()));
    	Hashtable<String, String> emailProperties = new Hashtable<String, String>();
    	String subjectParam = params.get("subject");
    	if (subjectParam != null) {
    		emailProperties.put("subject", subjectParam);
    	} else {
	    	emailProperties.put("subject", getMessage(subjectKey + "." + langCode, null));
	    }
    	String fromParam = params.get("from");
    	if (fromParam != null) {
    		emailProperties.put("from", fromParam);
    	}
    	String toParam = params.get("to");
    	if (toParam != null) {
    		emailProperties.put("to", toParam);
    	} else {
    		emailProperties.put("to", user.getEmail());
    	}

    	try {
    		emailProperties.put("body",getEmailBody(params, mailTemplate));
    	
		    sendSingleEmail(null, emailProperties, langCode);
	
	
		    // Create issue
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			// Filling issue fields
			if (issueSubjectId > 0) {
				issue.setSubjectId(String.valueOf(issueSubjectId));
			} else {
				issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
			}
			issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
			issue.setUserId(user.getId());
			issue.setContactId(user.getContactId());
			if (isContacts) {
				issue.setPopulationEntryId(recordId);
				issue.setContactId(user.getId());
			} else {
				issue.setContactId(user.getContactId());
			}
			issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			// Filling action fields
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(user.getUtcOffset());
			if (commentKey != null) {
				issueAction.setComments(getProperty(commentKey));
			} else {
				issueAction.setComments(getProperty(subjectKey + "." + langCode));
			}
			if (!isContacts && null != recordId) {
				issueAction.setTransactionId(recordId);
			}
			IssuesDAOBase.insertIssue(conn, issue);
			issueAction.setIssueId(issue.getId());
			issueAction.setTemplateId(templateId);
			IssuesDAOBase.insertAction(conn, issueAction);
    	} catch (Exception e) {
	    	log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
	    	System.exit(1);
	    }
    }
    
    /**
	 * ***************************************
	 * ***** Migration from job process. *****
	 * ***************************************
     * 
     * @param fileName
     * @param lang
     * @param skinId
     * @return
     * @throws Exception
     */
    public static Template getEmailTemplate(String fileName, String lang, String skinId) throws Exception {
		try {
			Properties props = new Properties();
			String tempPath = "";
			if (null != lang && null != skinId) {
				tempPath = getProperty("templates.path") + lang + "_" + skinId + "/" ;
			} else {
				tempPath = getProperty("templates.path");
			}
			log.debug("TemplatePath: " + tempPath);
			props.setProperty("file.resource.loader.path", tempPath);
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			VelocityEngine v = new VelocityEngine();
			v.init(props);
			return v.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}
    
    /**
	 * ***************************************
	 * ***** Migration from job process. *****
	 * ***************************************
     * 
     * Send single email template
     * @param serverProperties
     * @param emailProperties
     * @param lang
     */
    public static void sendSingleEmail(Hashtable serverProperties, Hashtable emailProperties, String lang) {
		Properties servProps = new Properties();
		final String userEmail = (String) getProperty("email.uname");
		final String passEmail = (String) getProperty("email.pass");
		servProps.put("mail.smtp.host", (String) getProperty("email.server"));
		boolean auth = null != getProperty("auth") && ((String) getProperty("auth")).equals("true");

		String subject = (String) emailProperties.get("subject");
		String to = (String) emailProperties.get("to");

		String from = (String) emailProperties.get("from");
		if (lang.equals(ConstantsBase.CHINESE_LOCALE)){
			from = getProperty("email.from.anyoption.zh");
		} else {
			from = getProperty("email.from.anyoption");
		}
		String body = (String) emailProperties.get("body");

		Session session = null;
        if (auth) {
        	 session = Session.getInstance(servProps, new Authenticator() {
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userEmail, passEmail);
                }
            });
        } else {
            session = Session.getInstance(servProps, null);
        }

		MimeMessage mess = new MimeMessage(session);

		try {
			// Set Headers
			mess.setHeader("From", from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=utf-8;");
			mess.setFrom(new InternetAddress(from));
			mess.setSubject(subject,"UTF-8");
			mess.setContent(body,"text/html; charset=utf-8;");
			Transport.send(mess);
		} catch (SendFailedException sfe){
			log.log(Level.ERROR, "Mail Adress not correct! " + to);
		}
		catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
	}
    
    /**
	 * ***************************************
	 * ***** Migration from job process. *****
	 * ***************************************
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public static String getEmailBody(HashMap params, Template mailTemplate) throws Exception {
		// set the context and the parameters
		VelocityContext context = new VelocityContext();
		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");
		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		mailTemplate.merge(context,sw);
		return sw.toString();
	}
}