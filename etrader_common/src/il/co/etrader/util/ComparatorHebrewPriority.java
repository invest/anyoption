package il.co.etrader.util;

import java.util.Comparator;
import java.util.Map;

public class ComparatorHebrewPriority implements Comparator<Long> {

	private Map<Long, String> map;

	public ComparatorHebrewPriority(Map<Long, String> map) {
		this.map = map;
	}

	public int compare(Long a, Long b) {
		String item1 = map.get(a);
		String item2 = map.get(b);
		
		if (item1.indexOf("(") > -1) {
			item1 = item1.substring(0, item1.indexOf("("));
		}

		if (item2.indexOf("(") > -1) {
			item2 = item2.substring(0, item2.indexOf("("));
		}
		String item1Trimmed = item1;
		if (item1.indexOf(" ") > -1) {
			item1Trimmed = item1.substring(0, item1.indexOf(" "));
		}
		String item2Trimmed = item2;
		if (item2.indexOf(" ") > -1) {
			item2Trimmed = item2.substring(0, item2.indexOf(" "));
		}

		if (!isEnglishLettersAndNumbersOnly(item1) && isEnglishLettersAndNumbersOnly(item2)) {
			return -1;
		} else if (!isEnglishLettersAndNumbersOnly(item2) && isEnglishLettersAndNumbersOnly(item1)) {
			return 1;
		} else if (item1Trimmed.equals("\u05db\u05dc")) { // "כל"
			return -1;
		} else if (item2Trimmed.equals("\u05db\u05dc")) { // "כל"
			return 1;
		} else {
			return item1.compareToIgnoreCase(item2);
		}
	}

	public static boolean isEnglishLettersAndNumbersOnly(Object value) {
		String pattern = CommonUtil.getMessage("general.validate.english.numbers.spaces", null);
		String v = (String) value;
		v = v.replaceAll("-", " ");
		v = v.replaceAll("`", " ");
		v = v.replaceAll("'", " ");
		v = v.replaceAll("\\(", " ");
		v = v.replaceAll("\\)", " ");
		v = v.replaceAll("\"", " ");
		v = v.replaceAll("‘", " ");
		v = v.replaceAll("&", " ");
		v = v.replaceAll("/", " ");

		if (!v.matches(pattern) || v.trim().equals("")) {
			return false;
		} else
			return true;
	}

}
