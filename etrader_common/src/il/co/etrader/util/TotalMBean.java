/**
 *
 */
package il.co.etrader.util;

/**
 * Interface for page total calculation 
 * @author Kobi
 *
 */
public interface TotalMBean {
	public long getTotalField();
	public long getTotalBaseField();
}
