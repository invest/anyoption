//package il.co.etrader.util;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.ChartsUpdaterMarket;
//import com.anyoption.common.beans.MarketRate;
//import com.anyoption.common.managers.ChartsUpdaterManager;
//import com.anyoption.common.util.ChartsUpdaterListener;
//
//public class ChartsUpdater extends Thread {
//    private static final Logger log = Logger.getLogger(ChartsUpdater.class);
//
//    private boolean running;
//    private boolean loaded;
//    private Hashtable<Long, MarketRate> lastMarketRates;
//    private Hashtable<Long, Boolean> marketStates;
//    private ArrayList<ChartsUpdaterListener> listeners;
//
//    public ChartsUpdater() {
//        listeners = new ArrayList<ChartsUpdaterListener>();
//    }
//
//    public void addListener(ChartsUpdaterListener listener) {
//    	addListener(listener, 2);   // for web use(we expect 2 listeners here - LevelHistoryCache and ChartHistoryCache)
//    }
//
//    public void addListener(ChartsUpdaterListener listener, long numListeners) {
//        listeners.add(listener);
//        if (listeners.size() >= numListeners && !isAlive()) {
//            // if the audience arrived start the show :)
//            this.start();
//        }
//    }
//
//    public void removeListener(ChartsUpdaterListener listener) {
//        listeners.remove(listener);
//    }
//
//    public void run() {
//        Thread.currentThread().setName("ChartsUpdater");
//        loaded = false;
//        running = true;
//
//        lastMarketRates = new Hashtable<Long, MarketRate>();
//        marketStates = new Hashtable<Long, Boolean>();
//        long tickStartTime;
//        while (running) {
//            if (log.isDebugEnabled()) {
//                log.debug("Loading markets rates.");
//            }
//            tickStartTime = System.currentTimeMillis();
//            try {
//                ChartsUpdaterMarket market = null;
//                MarketRate mr = null;
//                ArrayList<MarketRate> mrs = null;
//                ArrayList<ChartsUpdaterMarket> markets = ChartsUpdaterManager.getMarkets(1/*Opportunity.TYPE_REGULAR*/);
//                markets.addAll(ChartsUpdaterManager.getMarkets(3/*Opportunity.TYPE_OPTION_PLUS*/));
//                markets.addAll(ChartsUpdaterManager.getMarkets(4));
//                log.trace("Finished loading markets");
//                for (int i = 0; i < markets.size(); i++) {
//                    market = markets.get(i);
//                    if (market.isOpened()) {
//                        mr = lastMarketRates.get(market.getId());
//                        mrs = ChartsUpdaterManager.getMarketRates(market.getId(), null != mr ? mr.getRateTime() : null);
//                        if (log.isTraceEnabled()) {
//                            log.trace("Loading market: " + market.getDisplayNameKey() + " last rate: " + (null != mr ? mr.getRateTime() : "null") + " rates loaded: " + mrs.size());
//                        }
//                        if (mrs.size() > 0) {
//                            for (int l = 0; l < mrs.size(); l++) {
//                                mr = mrs.get(l);
//                                mr.setRate(mr.getRate().setScale((int) market.getDecimalPoint(), BigDecimal.ROUND_HALF_UP));
//                            }
//                            for (int j = 0; j < listeners.size(); j++) {
//                                listeners.get(j).marketRates(market, mrs);
//                            }
//                            lastMarketRates.put(market.getId(), mrs.get(mrs.size() - 1));
//                        }
//                    } else {
//                    	log.trace("Closed");
//                        if (null != marketStates.get(market.getId())) {
//                            boolean wasOpened = marketStates.get(market.getId());
//                            if (wasOpened) {
//                                if (log.isDebugEnabled()) {
//                                    log.debug(market.getDisplayNameKey() + " closed.");
//                                }
//                                for (int j = 0; j < listeners.size(); j++) {
//                                    listeners.get(j).marketClosed(market);
//                                }
//                            }
//                        }
//                    }
//                    marketStates.put(market.getId(), market.isOpened());
//                }
//                loaded = true;
//                log.debug("Loaded markets rates in: " + (System.currentTimeMillis() - tickStartTime));
//            } catch (Exception e) {
//                log.error("Problem in the charts updater.", e);
//            }
//            try {
//                Thread.sleep(6000);
//            } catch (Exception e) {
//                log.error("ChartsUpdater sleep interrupted.", e);
//            }
//        }
//    }
//
//    public void stopChartsUpdater() {
//        running = false;
//        this.interrupt();
//    }
//
//    public boolean isLoaded() {
//        return loaded;
//    }
//
//    public boolean isRunning() {
//        return running;
//    }
//}