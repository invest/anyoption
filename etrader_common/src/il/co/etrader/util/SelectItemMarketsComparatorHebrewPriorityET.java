package il.co.etrader.util;
import java.util.Comparator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;

/**
     * SelectItem comperator class
     * @author idan
     */
    public class SelectItemMarketsComparatorHebrewPriorityET implements Comparator<Market> {

        private static final Logger log = Logger.getLogger(SelectItemMarketsComparatorHebrewPriorityET.class);

        public int compare(Market a, Market b) {
//            String item1 = CommonUtil.getMessage(a.getDisplayName(), null, true);
//            String item2 = CommonUtil.getMessage(b.getDisplayName(), null, true);
            return compare(a.getDisplayName(), b.getDisplayName());
        }


        public int compare(String item1, String item2) {

            if (item1.indexOf("(") > -1) {
                item1 = item1.substring(0, item1.indexOf("("));
            }

            if (item2.indexOf("(") > -1) {
                item2 = item2.substring(0, item2.indexOf("("));
            }

            String item1Trimmed = item1;
            if (item1.indexOf(" ") > -1) {
                item1Trimmed = item1.substring(0,item1.indexOf(" "));
            }
            String item2Trimmed = item2;
            if (item2.indexOf(" ") > -1) {
                item2Trimmed = item2.substring(0,item2.indexOf(" "));
            }

            if (!isEnglishLettersAndNumbersOnly(item1) && isEnglishLettersAndNumbersOnly(item2)) {
                return -1;
            } else if (!isEnglishLettersAndNumbersOnly(item2) && isEnglishLettersAndNumbersOnly(item1)){
                return 1;
            } else if (item1Trimmed.equals("\u05db\u05dc")) { //"כל"
                return -1;
            } else if (item2Trimmed.equals("\u05db\u05dc")) { //"כל"
                return 1;
            } else {
                return item1.compareToIgnoreCase(item2);
            }
        }

        public static boolean isEnglishLettersAndNumbersOnly(Object value) {
            //String pattern = CommonUtil.getMessage("general.validate.english.numbers.spaces", null);
            String pattern = "[0-9a-zA-Z -]*";
            String v = (String) value;
            v = v.replaceAll("-", " ");
            v = v.replaceAll("`", " ");
            v = v.replaceAll("'", " ");
            v = v.replaceAll("\\(", " ");
            v = v.replaceAll("\\)", " ");
            v = v.replaceAll("\"", " ");
            v = v.replaceAll("‘", " ");
            v = v.replaceAll("&", " ");
            v = v.replaceAll("/", " ");

            if (!v.matches(pattern) || v.trim().equals("")) {
               return false;
            }
            else return true;
        }
    }
