package il.co.etrader.util;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.bl_vos.UkashDeposit;
import com.anyoption.common.managers.QuestionnaireManagerBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;

public class TransactionFormater {
	
	private static final Logger log = Logger.getLogger(TransactionFormater.class);
	
	public static String getDollarAmount(Transaction t) {
		try {
			DecimalFormat sd = new DecimalFormat("###########0.00");
			return sd.format(CommonUtil.convertToBaseAmount(t.getAmount(), t.getCurrencyId(), t.getTimeCreated())/100);
		} catch (Exception e) {
			log.error("cant get dollar amount return 0 for transaction id = " + t.getId() , e);
		}
		return "0";
	}

	public static String getEuroAmount(Transaction t) {
		try {
			DecimalFormat sd = new DecimalFormat("###########0.00");
			return sd.format(CommonUtil.convertToEuroAmount(t.getAmount(), t.getCurrencyId(), t.getTimeCreated())/100);
		} catch (Exception e) {
			log.error("cant get euro amount return 0 for transaction id = " + t.getId() , e);
		}
		return "0";
	//	return sd.format((double)amount/(double)100/(double)AdminManagerBase.getLastShekelDollar());
	}

	public static String getExistFee(Transaction t) {
		if (t.getReferenceId()!=null) {
			return CommonUtil.getMessage("yes",null);
		} else {
			return CommonUtil.getMessage("no",null);
		}
	}

//	public String getAmountTxt() {
//		return CommonUtil.displayAmount(amount, currency.getId());
//	}
	public static String getAmountTxtNoCur(Transaction t) {
		return CommonUtil.displayAmount(t.getAmount(),false, t.getCurrencyId());
	}

	public static String getTimeSettledTxt(Transaction t) {
		return CommonUtil.getDateTimeFormatDisplay(t.getTimeSettled(), CommonUtil.getUtcOffset(t.getUtcOffsetSettled()));
	}
	public static String getLastWithdrawlSetteldTxt(Transaction t){
		return CommonUtil.getDateTimeFormatDisplay(t.getLastWithdrawlSettled(), CommonUtil.getUtcOffset(t.getUtcOffsetSettled()));
	}

	/**
	 * Get type name message
	 * in case transaction type is direct banking deposit: add payment type name to message
	 * @return
	 */
	public static String getTypeNameTxt(Transaction t) {
		String type = CommonUtil.getMessage(t.getTypeName(), null);
		if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT &&
				null != t.getPaymentTypeDesc()) {
			type = CommonUtil.getMessage(t.getPaymentTypeDesc(), null);
			if (t.isInatecInProgress()) {
				type += " " + CommonUtil.getMessage("direct.banking.deposit.pending", null);
			}
		} else if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT ||
				t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT ||
						t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW){

			type += " " + t.getPaymentTypeName();

			if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS){
				type += " " + CommonUtil.getMessage("direct.banking.deposit.pending", null);
			}
		}
		return type;
	}


	public static String getStatusName(Transaction t) {
		long statusIdForDisplay = t.getStatusId();

		if (ApplicationDataBase.isWeb() && TransactionsManagerBase.TRANS_STATUS_CHARGE_BACK == t.getStatusId()){
			statusIdForDisplay = TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT;
		}

		return ApplicationDataBase.getTranStatusName(statusIdForDisplay);
	}
	
	public static String getWriterName(Transaction t) throws SQLException{
		return CommonUtil.getWriterName(t.getWriterId());
	}
	
	public static String getProcessedWriterName(Transaction t) throws SQLException{
		return CommonUtil.getWriterName(t.getProcessedWriterId());
	}

	public static String getAmountForInput(Transaction t) {
		return CommonUtil.displayAmountForInput(t.getAmount(), false, 0);
	}


	/**
	 * Return available credit amount with currency
	 * @return
	 */
	public static String getAvailableCreditAmountTxt(Transaction t) {
		return CommonUtil.displayAmount(t.getAmount()-t.getCreditAmount(), t.getCurrencyId());
	}

	/**
	 * Return credit amount with currency
	 * @return
	 */
	public static String getCreditAmountTxt(Transaction t) {
		return CommonUtil.displayAmount(t.getCreditAmount(),  t.getCurrencyId());
	}



    public static String getCurrencyTxt(Transaction t) {
    	if (null!=  t.getCurrency()){
    		return CommonUtil.getMessage( t.getCurrency().getSymbol(), null);
    	}
    	log.error("Currency wasn't found for transaction: " + t.getId());
        return "????";
    }


    public static String getBusinessCaseNameTxt(Transaction t) {
        return CommonUtil.getMessage(t.getBusinessCaseName(), null);
    }


    public static String getBaseAmountTxt(Transaction t) {
        return CommonUtil.displayAmount(t.getBaseAmount(),ConstantsBase.CURRENCY_BASE_ID);
    }

    public static String getWireDepositReceipt(Transaction t) throws SQLException {
        String[] params = new String[3];
        params[0] = CommonUtil.getMessage(t.getWire().getBankNameTxt(), null);
        params[1] = t.getWire().getBranch();
        params[2] = t.getWire().getAccountNum();
        return CommonUtil.getMessage("transaction.wireDeposit.receipt", params);
    }

	public static String getStyleForPendingWithdraw(Transaction t) {
		String style = "";
		
		if((t.isOldTransaction() && !t.isWeekOldTransaction()) || t.isUserImmediateWithdraw()){
			if(t.isRegulated()) {
				style = "table_row_bordeaux";
			} else {
				style = "table_row_bold_red";
			}
		} else if (t.isWeekOldTransaction()){
			if(t.isRegulated()) {
				style = "table_row_bordeaux";
			} else {
				style = "user_strip_value_darkgreen";
			}
		}
		return style;
	}
	
    public static String sendDepositReceipt(Transaction t) {  	
    	UserBase user = new UserBase();
    	try {
			UsersManagerBase.getByUserId(t.getUserId(), user);
		} catch (SQLException e1) {
			log.error("problem getting user from DB", e1);
			return null;
		}
    	Locale locale = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(user.getSkinId()).getDefaultLanguageId()).getCode());
    	initFormattedValues(t, locale);
    	
    	//send the receipt deposit
    	String[] params = new String[1];
		params[0] = t.getCc4digit();
		if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
			t.setReceiptNum(BigDecimal.valueOf(t.getId()));
		}		
		user.setUserName(user.getFirstName() + " " + user.getLastName());
		String footer = CommonUtil.getMessage("receipt.footer.creditcard.mb", params, locale);
		if(t.getClearingProviderId() == 26 || t.getClearingProviderId() == 27 || t.getClearingProviderId() == 28) //AMEX
        {
            footer = footer + System.getProperty("line.separator") + CommonUtil.getMessage("receipt.bank_descriptor", params, locale);
        }

		try {
			UsersManagerBase.sendMailTemplateFile(Template.CC_RECEIPT_MAIL_ID, Writer.WRITER_ID_WEB,	user, t.getAmountWF(),
															footer, t.getReceiptNumWebDepositTxt(), null, null, null, null, null);
		} catch (Exception e) {
			log.error("problem sending mail after reciept", e);
		}
		return null;

    }
    
    /**
     * Init formatted values before serializing to JSON
     * @param l
     */
	public static void initFormattedValues(Transaction t, Locale l) {
		t.setTypeNameTxt(l);
		setAmountTxt(t);
		t.getReceiptNumWebDepositTxt();
	}

	 public static void setAmountTxt(Transaction t) {
		 t.setAmountWF (CommonUtil.displayAmount(t.getAmount(), t.getCurrencyId()));
	 }
	 
	 public static String getTimeCreatedTxt(Transaction t) {
		 return CommonUtil.getDateTimeFormatDisplay(t.getTimeCreated(), CommonUtil.getUtcOffset(t.getUtcOffsetCreated()));
	 }

	public static String getAmountTxt(Transaction t) {
		long currencyId = t.getCurrencyId();
		if(currencyId == 0 && t.getCurrency() != null){
			currencyId = t.getCurrency().getId();
		}
		return CommonUtil.displayAmount(t.getAmount(), currencyId);
	}

	/*
	 * ChargeBack formats
	 */
	public static String getCBTimeCreatedTxt(ChargeBack cb) {
		return CommonUtil.getDateTimeFormatDisplay(cb.getTimeCreated(), CommonUtil.getUtcOffset(cb.getUtcOffsetCreated()));
	}
	
	public static String getCBAmountTxt(ChargeBack cb) {
		return CommonUtil.displayAmount(cb.getAmount(), cb.getCurrencyId());
	}
	
	public static String getCBStateName(ChargeBack cb){
		if (ChargeBack.STATE_CHB == cb.getState()){
			return CommonUtil.getMessage("chargebacks.state.chb", null);
		}else if(ChargeBack.STATE_POS == cb.getState()){
			return CommonUtil.getMessage("chargebacks.state.pos", null);
		}

		return "";
	}
	
	/*
	 * Ukash formats
	 */
	
	public static String getVoucherValueTxt(UkashDeposit ud) {
		return CommonUtil.displayAmount(ud.getVoucherValue(), false, 0);
	}

	public static String getConvertedAmountTxt(UkashDeposit ud) {
		//check if there was a conversion
		if (!CommonUtil.isParameterEmptyOrNull(ud.getUserCurrency()) && ud.getConvertedAmount() != 0 && !CommonUtil.isParameterEmptyOrNull(ud.getVoucherCurrency())){
			return CommonUtil.displayAmount(ud.getConvertedAmount(), Long.valueOf(ud.getUserCurrency()));
		}
		return "";
	}

	/**
	 * Get conversion Msg rate: usercurrency = voucherCurrency for TRY currency
	 * @return
	 */
	public static String getConversionMsg(UkashDeposit ud) {
		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user= (UserBase)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		long voucherCurrencyId = ApplicationDataBase.getCurrencyIdByCode(ud.getVoucherCurrency());
		long amount = CommonUtil.calcAmount(ud.getVoucherValue());
		long divider = Long.valueOf(ud.getVoucherValue());
		double transactionAmount = ud.getConvertedAmount()/divider;
		double voucher = amount/divider;
		long userCurrencyId = user.getCurrencyId();
		String userAmount = CommonUtil.displayAmount(transactionAmount, true, userCurrencyId);
		String convertedAmount= CommonUtil.displayAmount(voucher, true, voucherCurrencyId);
		return userAmount + " = " + convertedAmount;
	}
	
	/**
	 * This method returns the answer of the withdraw survey to be shown in Backend 
	 * @param t
	 * @return withdrawSurveyAnswer
	 * @throws SQLException 
	 */
	public static String getWithdrawSurveyAnswer(Transaction t) throws SQLException {
		String withdrawSurveyAnswer = null;
		Locale locale = new Locale("en");
	
		String answerName = QuestionnaireManagerBase.getWithdrawSurveyAnswer(t.getId()).get("answerName");
		String textAnswer = QuestionnaireManagerBase.getWithdrawSurveyAnswer(t.getId()).get("textAnswer");
		withdrawSurveyAnswer = CommonUtil.getMessage(locale, answerName, null);
		if (!CommonUtil.isParameterEmptyOrNull(textAnswer)){
			withdrawSurveyAnswer = withdrawSurveyAnswer.concat(textAnswer);
		}

		return withdrawSurveyAnswer;
	}
}