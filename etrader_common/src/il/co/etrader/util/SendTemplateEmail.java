package il.co.etrader.util;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.bl_managers.ApplicationDataBase;

public class SendTemplateEmail extends com.anyoption.common.util.SendTemplateEmail  {
    public static final Logger logger = Logger.getLogger(SendTemplateEmail.class);

	private static Template emailTemplate;
	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;
	private File[] attachments;

	public SendTemplateEmail(HashMap<String, String> params, String subject, String fileName, 
			String lang, long skinId, Object att, long recipientUserWriterId, int platformId) throws Exception {
	    this(params, subject, fileName, lang, skinId, att, CommonUtil.getProperty("templates.path"), CommonUtil.getProperty("email.server"),
	    		CommonUtil.getProperty("email.uname"), CommonUtil.getProperty("email.pass"), recipientUserWriterId, platformId);
	}

	public SendTemplateEmail(HashMap<String, String> params, String subject, String fileName, 
			String lang, long skinId, Object att, Properties properties, long recipientUserWriterId, int platformId) throws Exception {
        this(params, subject, fileName, lang, skinId, att, properties.getProperty("templates.path"), properties.getProperty("email.server"),
        		properties.getProperty("email.uname"), properties.getProperty("email.pass"), recipientUserWriterId, platformId);
	}

	public SendTemplateEmail(HashMap<String, String> params, String subject, String fileName, String lang, 
			long skinId, Object att, String templatesPath, String emailServer, String emailUserName, String emailPassword,
			long recipientUserWriterId, int platformId) throws Exception {
        logger.debug("Trying to send email with the following params: " + params);

        attachments = (File []) att;

        init(subject, fileName, lang, skinId, templatesPath, emailServer, emailUserName, emailPassword, recipientUserWriterId, platformId);
        // set the specific email properties
        emailProperties.put("to", (String) params.get(PARAM_EMAIL));
        emailProperties.put("body", getEmailBody(params));
    }

	private void init(String subject, String name, String lang, long skinId, String templatesPath, 
			String emailServer, String emailUserName, String emailPassword, long recipientUserWriterId, int platformId) throws Exception {
		emailTemplate = getEmailTemplate(name, lang, skinId, templatesPath, recipientUserWriterId, platformId);

		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", emailServer);
		serverProperties.put("auth", "true");
		serverProperties.put("user", emailUserName);
		serverProperties.put("pass", emailPassword);

		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject", subject);

		// handle from attribute
		emailProperties.put("from", ApplicationDataBase.getSupportEmailBySkinId(skinId));
        if (skinId != Skin.SKIN_KOREAN && skinId != Skin.SKIN_CHINESE) {
            String appSource = ApplicationDataBase.getAppSource();
            if (lang.equals(ConstantsBase.ETRADER_LOCALE)) {
            	if(name.equalsIgnoreCase(PARAM_TEMPLATE_ACTIVATION_LOW_GROUP_MAIL) ||name.equalsIgnoreCase(PARAM_TEMPLATE_ACTIVATION_TRESHOLD_MAIL)) {
            		emailProperties.put("from", PARAM_SUPPORT_MAIL_ET);
            	}
            }
            if (null != appSource && appSource.equals(ConstantsBase.APPLICATION_SOURCE_BACKEND)) {
                if (lang.equals(ConstantsBase.ETRADER_LOCALE)) {
                    if(name.equalsIgnoreCase(PARAM_TEMPLATE_FOLLOEING_SALES_CALL_FILE_NAME) ||
                            name.equalsIgnoreCase(PARAM_TEMPLATE_NOT_REACHED_MADE_DEPOSIT) ||
                            name.equalsIgnoreCase(PARAM_TEMPLATE_NOT_REACHED_NEVER_DEPOSIT)) {// Customer Service
                        emailProperties.put("from", CommonUtil.getProperty("email.from.cs"));
                    }
                } else {
                    if(name.equalsIgnoreCase(PARAM_TEMPLATE_FOLLOEING_SALES_CALL_FILE_NAME) ||
                            name.equalsIgnoreCase(PARAM_TEMPLATE_NOT_REACHED_MADE_DEPOSIT) ||
                            name.equalsIgnoreCase(PARAM_TEMPLATE_NOT_REACHED_NEVER_DEPOSIT)) {// Customer Service
                        emailProperties.put("from", CommonUtil.getProperty("email.from.anyoption.cs"));
                    }
                }
            } else {
                logger.warn("Sending email without appSource. appSource: " + appSource);
            }
        }
        
        if(name != null && name.equalsIgnoreCase(ConstantsBase.PARAM_TEMPLATE_PEP_MAIL)){
        	emailProperties.put("from", ConstantsBase.PARAM_COMPLIANCE_MAIL);
        }
        
        updateCopyopSenderAndSubject(emailProperties, recipientUserWriterId, platformId);
	}

	public void run() {
		logger.debug("Sending Email is running ...");
		CommonUtil.sendEmail(serverProperties, emailProperties, attachments);
		logger.debug("Sending Email completed ");
	}

	public String getEmailBody(HashMap<String, String> params) throws Exception {
		VelocityContext context = new VelocityContext();

		logger.debug("Adding password to context");

        String paramName = "";
		for (Iterator<String> keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName, params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		emailTemplate.merge(context, sw);
		return sw.toString();
	}
}