//package il.co.etrader.util;
//
//import java.util.Date;
//
//public class OpportunityCacheBean {
//    private long id;
//    private Date timeFirstInvest;
//    private Date timeEstClosing;
//    private Date timeLastInvest;
//    private int scheduled;
//    private String marketDisplayName;
//    private Long marketDecimalPoint;
//    private double currentLevel;
//    private long marketId;
//	private long opportunityTypeId;
//	private double maxInvAmountCoeffPerUser;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public Date getTimeFirstInvest() {
//        return timeFirstInvest;
//    }
//
//    public void setTimeFirstInvest(Date timeFirstInvest) {
//        this.timeFirstInvest = timeFirstInvest;
//    }
//
//    public Date getTimeEstClosing() {
//        return timeEstClosing;
//    }
//
//    public void setTimeEstClosing(Date timeEstClosing) {
//        this.timeEstClosing = timeEstClosing;
//    }
//
//    public Date getTimeLastInvest() {
//        return timeLastInvest;
//    }
//
//    public void setTimeLastInvest(Date timeLastInvest) {
//        this.timeLastInvest = timeLastInvest;
//    }
//
//    public int getScheduled() {
//        return scheduled;
//    }
//
//    public void setScheduled(int scheduled) {
//        this.scheduled = scheduled;
//    }
//
//    public Long getMarketDecimalPoint() {
//        return marketDecimalPoint;
//    }
//
//    public void setMarketDecimalPoint(Long marketDecimalPoint) {
//        this.marketDecimalPoint = marketDecimalPoint;
//    }
//
//    public String getMarketDisplayName() {
//        return marketDisplayName;
//    }
//
//    public void setMarketDisplayName(String marketDisplayName) {
//        this.marketDisplayName = marketDisplayName;
//    }
//
//    public double getCurrentLevel() {
//        return currentLevel;
//    }
//
//    public void setCurrentLevel(double currentLevel) {
//        this.currentLevel = currentLevel;
//    }
//
//	public long getMarketId() {
//		return marketId;
//	}
//
//	public void setMarketId(long marketId) {
//		this.marketId = marketId;
//	}
//
//	public long getOpportunityTypeId() {
//		return opportunityTypeId;
//	}
//
//	public void setOpportunityTypeId(long opportunityTypeId) {
//		this.opportunityTypeId = opportunityTypeId;
//	}
//
//	public double getMaxInvAmountCoeffPerUser() {
//		return maxInvAmountCoeffPerUser;
//	}
//
//	public void setMaxInvAmountCoeffPerUser(double maxInvAmountCoeffPerUser) {
//		this.maxInvAmountCoeffPerUser = maxInvAmountCoeffPerUser;
//	}
//}