//package il.co.etrader.util;
//
//import java.security.MessageDigest;
//
//
///**
// * SHA is a cryptographic message digest algorithm similar to MD5. SHA-1 hash considered to be one
// * of the most secure hashing functions, producing a 160-bit digest (40 hex numbers) from any data
// * with a maximum size of 264 bits
// *
// */
//public class Sha1 {
//
//	/**
//	 * convert byte array to Hex String, second implementation option
//	 * @param data
//	 * @return
//	 */
//    private static String toHexString(byte[] data) {
//        StringBuffer sb = new StringBuffer();
//        String s = null;
//        for (int i = 0; i < data.length; i++) {
//            s = Integer.toHexString(data[i] & 0x000000FF);
//            if (s.length() == 1) {
//                sb.append("0");
//            }
//            sb.append(s);
//        }
//        return sb.toString();
//    }
//
//    /**
//     * Sha1 encode function
//     * @param text the text to encode
//     * @return
//     * @throws Exception
//     */
//    public static String encode(String text) throws Exception {
//		MessageDigest md = MessageDigest.getInstance("SHA-1");
//		md.update(text.getBytes("iso-8859-1"), 0, text.length());
//		return toHexString(md.digest());
//    }
//
//	/**
//	 * convert byte array to Hex String ,implementation option
//	 * @param data
//	 * @return
//	 */
////    private static String convertToHex(byte[] data) {
////        StringBuffer buf = new StringBuffer();
////        for (int i = 0; i < data.length; i++) {
////        	int halfbyte = (data[i] >>> 4) & 0x0F;
////        	int two_halfs = 0;
////        	do {
////	            if ((0 <= halfbyte) && (halfbyte <= 9))
////	                buf.append((char) ('0' + halfbyte));
////	            else
////	            	buf.append((char) ('a' + (halfbyte - 10)));
////	            halfbyte = data[i] & 0x0F;
////        	} while(two_halfs++ < 1);
////        }
////        return buf.toString();
////    }
//}