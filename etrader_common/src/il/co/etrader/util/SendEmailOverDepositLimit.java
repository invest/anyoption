package il.co.etrader.util;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.WritersManagerBase;

import il.co.etrader.bl_vos.UserBase;


/**
 * SendDepositErrorEmail class
 * 		for deposit failure issues
 *
 * @author Kobi
 */
public class SendEmailOverDepositLimit extends Thread  {

	// The email parameters that we need
	//public static final String PARAM_EMAIL = "Email";
	//public static final String PARAM_USER_NAME = "userName";
	//public static final String PARAM_CURRENCY_ID = "currencyId";
	//public static final String PARAM_TRANSACTION_ID = "transactionId";
	//public static final String PARAM_CC_NUM_LAST4 = "ccNumberLast4";
	//public static final String PARAM_CC_TYPE = "ccType";
	//public static final String PARAM_AMOUNT = "amount";
	//public static final String PARAM_DATE = "date";
	//public static final String PARAM_TRANSACTION_TIME_CREATED = "time_transaction_created";
	//public static final String PARAM_SKIN_NAME = "skin_name";

	//Add additional constants to server parameters
	public static final String MAIL_FROM ="from";
	public static final String MAIL_TO = "to";
	public static final String MAIL_SUBJECT = "subject";


	//private final  int END_OF_LINE = Character.LINE_SEPARATOR;
	private final String  END_OF_LINE = "<br/>";
	private final char DELIMITER = ':' ;


	/*private final String [] MAIL_FIELDS_LIST = {PARAM_USER_NAME ,PARAM_USER_ID, PARAM_TRANSACTION_ID ,PARAM_CC_NUM_LAST4, PARAM_CC_TYPE
												,PARAM_AMOUNT,PARAM_DECLINE_DESCRIPTION , PARAM_DECLINE_COMMENTS, PARAM_DATE};
*/

	// Class logger
	public static final Logger logger = Logger.getLogger(SendEmailOverDepositLimit.class);

	// The Email template
	//private static Template emailTemplate;

	// The server properties
	private static Hashtable serverProperties;

	// The specific email Properties
	private static Hashtable emailProperties;


	/**
	 *
	 * @param request - the request with all the parameters
	 * @throws Exception - if template not found
	 */
	public SendEmailOverDepositLimit(Transaction tr, UserBase user, long threshold_deposit) throws Exception {

		logger.debug("Trying to send email with the following params: ");

		// check to see if the variables init or not
		init(tr, user, threshold_deposit);

		// set the specific email properties
		//Use constant instead of hard coded key and  put all code in the init method.
		/* emailProperties.put("to", CommonUtil.getProperty("contact.email"));

		emailProperties.put("body", getEmailBody(params));*/

	}

	/**
	 *
	 */
	private void init(Transaction tr, UserBase user, long threshold_deposit) throws Exception {

		// set the server properties
		serverProperties = new Hashtable();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable();
		//Put params from outside params
		emailProperties.put(MAIL_FROM, CommonUtil.getProperty("email.uname"));
		//emailProperties.put(MAIL_TO, "traders@etrader.co.il");
        String mailTo = CommonUtil.getProperty("deposit.alert.over.3000", "depositalert@etrader.co.il");
        emailProperties.put(MAIL_TO, mailTo);
        Transaction tran = TransactionsManagerBase.getTransaction(tr.getId());        
        
		emailProperties.put(MAIL_SUBJECT, getEmailSubject(user, tran));
		emailProperties.put("body", getEmailBody(tran, user));

	}

	public void run() {
		logger.debug("Sending Email ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");

	}

	private String getEmailBody(Transaction tr, UserBase user) throws Exception {

		StringBuffer mailBody = new StringBuffer("");

		//String paramValue , paramKey;
		//If we have a transaction and not a user then we will populate the mail body
		if(null != tr &&  null != user)
		{
            TimeZone tzEt = TimeZone.getTimeZone("Israel");
            String utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
            if (tzEt.inDaylightTime(new Date())) {
                utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
            }
            Calendar createTime = Calendar.getInstance();
            createTime.setTimeZone(tzEt);
            createTime.setTime(CommonUtil.getDateTimeFormaByTz(tr.getTimeCreated(), utcOffset));
            SimpleDateFormat dtf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String amountUserCurr = CommonUtil.formatCurrencyAmount(tr.getAmount()/100, true, user.getCurrency());
            String transactionType = CommonUtil.getMessage(tr.getTypeName(), null);
			mailBody.append("Details").append(DELIMITER).append(" " + amountUserCurr).append(END_OF_LINE).
                        append(transactionType).append(DELIMITER).
                        	append(" by " + WritersManagerBase.getWriterName(tr.getWriterId())).append(END_OF_LINE).
                        append("Transaction Time").append(DELIMITER).append(" " + dtf.format(createTime.getTime())).append(END_OF_LINE).
                        append("Skin").append(DELIMITER).append(" " + user.getSkinName()).append(END_OF_LINE);
		}
		return mailBody.toString();

	}
	
	private String getEmailSubject(UserBase user, Transaction tran) throws SQLException {
		UserRegulationBase ur = new UserRegulationBase();
        ur.setUserId(user.getId());
        UserRegulationManager.getUserRegulation(ur);
        String controlApproved = "";
        if(ur.getApprovedRegulationStep() != null){
        	if(ur.getApprovedRegulationStep() == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER){
        		controlApproved = ", Control approved: Yes";
        	} else {
        		controlApproved = ", Control approved: No";
        	}
        }
        Currency currency = CurrenciesManagerBase.getCurrency(Currency.CURRENCY_USD_ID);
        String usdAmount = CommonUtil.formatCurrencyAmount((tran.getAmount() * tran.getRate())/100, true, currency);
        String mailSubject = usdAmount + " USD deposit: " + user.getId() + " " + user.getUserName() + ", " + 
        				SkinsManagerBase.getSkin(user.getSkinId()).getName() + controlApproved;
		return mailSubject;
	}



}
