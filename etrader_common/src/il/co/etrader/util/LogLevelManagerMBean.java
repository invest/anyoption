package il.co.etrader.util;

/**
 * MBean interface implemented by the LogLevelManager to alow managing log4j loggers level over JMX.
 * 
 * @author Tony
 */
public interface LogLevelManagerMBean {
    /**
     * Set logger level.
     * 
     * @param category
     * @param level
     */
    public void setLoggerLevel(String category, String level);
    
    /**
     * Get logger level.
     * 
     * @param category
     * @return
     */
    public String getLoggerLevel(String category);
}