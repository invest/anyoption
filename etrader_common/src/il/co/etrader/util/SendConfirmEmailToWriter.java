package il.co.etrader.util;


import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


public class SendConfirmEmailToWriter extends Thread{
	//the parametrs to send email
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_DEPOSITORS = "Depositors";
	public static final String PARAM_LAST_LOGIN = "Last login";
	public static final String PARAM_SKINS = "Skins selected";
	public static final String PARAM_USER_CLASS = "User class";
	public static final String PARAM_TIME_ZONE ="Time zone";
	public static final String PARAM_COUNTRY = "Countries";
	public static final String PARAM_CAMPAIGNS = "Campaigns";
	public static final String PARAM_WIN_LOSE = "Lifetime W/L ";
	public static final String PARAM_SALES_DEPOSIT = "Has sales deposit";
	public static final String PARAM_USER_STATUS = "User status";
	public static final String PARAM_POPULATION_NAME = "Population names";
	public static final String PARAM_MESSAGE = "Message";
	public static final String PARAM_TEMPLATE_NAME = "Template name";
	public static final String PARAM_FILE_NAME = "File name";
	public static final String PARAM_PROMOTION_NAME = "Promotion name";
	public static final String PARAM_SEND_TYPE = "Send type";
	public static final String PARAM_AMOUNT_USERS = "Amount SMS/Email Sended ";
	public static final String PARAM_AMOUNT_CHOSEN = "Amount users chosen by filter ";

	//Add additional constants to server parameters
	public static final String MAIL_FROM ="from";
	public static final String MAIL_TO = "to";
	public static final String MAIL_SUBJECT = "subject";

	private final String  END_OF_LINE = "<br/>";
	private final char DELIMITER = ':' ;

	private final String [] MAIL_FIELDS_LIST = {PARAM_EMAIL, MAIL_FROM, MAIL_TO, MAIL_SUBJECT , PARAM_DEPOSITORS, PARAM_LAST_LOGIN, PARAM_SKINS, PARAM_USER_CLASS,
												PARAM_TIME_ZONE, PARAM_COUNTRY, PARAM_CAMPAIGNS, PARAM_WIN_LOSE, PARAM_SALES_DEPOSIT, PARAM_USER_STATUS,
												PARAM_POPULATION_NAME, PARAM_SEND_TYPE, PARAM_MESSAGE, PARAM_TEMPLATE_NAME,
												PARAM_PROMOTION_NAME, PARAM_FILE_NAME, PARAM_AMOUNT_USERS, PARAM_AMOUNT_CHOSEN};

	public static final Logger logger = Logger.getLogger(SendConfirmEmailToWriter.class);

	// The Email template
	//private static Template emailTemplate;

	// The server properties
	private static Hashtable<String, String> serverProperties;

	// The specific email Properties
	private static Hashtable<String, String> emailProperties;

	public SendConfirmEmailToWriter(HashMap<String, String> params, HttpSession session) throws Exception{
		logger.debug("Trying to send email with the following params: " + params);
		while(null == session.getAttribute(ConstantsBase.PROMOTION_TOTAL_INSERTED_TO_TABLE)){
			
		}
		params.put(SendConfirmEmailToWriter.PARAM_AMOUNT_CHOSEN, session.getAttribute(ConstantsBase.PROMOTION_TOTAL_INSERTED_TO_TABLE).toString());
		
		Init(params);
		session.removeAttribute(ConstantsBase.PROMOTION_TOTAL_INSERTED_TO_TABLE);
	}
	
	public SendConfirmEmailToWriter(HashMap<String, String> params) {
		logger.debug("Trying to send email with the following params: " + params);
		Init(params);
	}

	private void Init(HashMap<String, String> params) {
		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		//Put params from outside params
		emailProperties.put(MAIL_FROM, params.get(PARAM_EMAIL));
		emailProperties.put(MAIL_TO, params.get(MAIL_TO));
		emailProperties.put(MAIL_SUBJECT, params.get(MAIL_SUBJECT));
		emailProperties.put("body", getEmailBody(params));

	}

	public void run() {
		logger.debug("Sending Email ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");
	}

	public String getEmailBody(HashMap<String, String> params) {

		StringBuffer mailBody = new StringBuffer("");

		String paramValue , paramKey;
		//If we have params and not empty then we will populate the mail body
		if(null!= params && ! params.isEmpty())
		{
			for ( int i= 0 ; i < MAIL_FIELDS_LIST.length; i++)
			{
				paramKey = MAIL_FIELDS_LIST[i];

				paramValue = (String)params.get(paramKey);
				if(!CommonUtil.isParameterEmptyOrNull(paramValue) &&
						( paramKey != MAIL_FROM && paramKey != MAIL_TO && paramKey != PARAM_EMAIL))
				{
					mailBody.append(paramKey).
							append(DELIMITER).
							append(paramValue).
							append(END_OF_LINE);
				}
			}

		}
		return mailBody.toString();
	}
}
