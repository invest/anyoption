package il.co.etrader.util;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import il.co.etrader.bl_managers.ApplicationDataBase;

public class SendReceiptEmail extends Thread  {

	// The email parameters that we need
	public static final String PARAM_RECEIPT_NUM = "receiptNum";
	public static final String PARAM_USER_NAME = "userName";
	public static final String PARAM_USER_ADDRESS = "userAddress";
	public static final String PARAM_DATE = "date";
	public static final String PARAM_AMOUNT = "amount";
	public static final String PARAM_FOOTER = "footer";
	public static final String PARAM_EMAIL = "email";

	// Class logger
	public static final Logger logger = Logger.getLogger(SendReceiptEmail.class);

	// The Email template
	private static Template emailTemplate;

	// The server properties
	private static Hashtable serverProperties;

	// The specific email Properties
	private static Hashtable emailProperties;

	/**
	 *
	 * @param request - the request with all the parameters
	 * @throws Exception - if template not found
	 */
	public SendReceiptEmail(HashMap params, HttpServletRequest request, String lang, String subject, long skinId) throws Exception {

		logger.debug("Trying to send email with the following params: " + params);

		init(request, lang, subject, skinId);

		// set the specific email properties
		emailProperties.put("to", params.get(PARAM_EMAIL));
		emailProperties.put("body", getEmailBody(params));

	}

	private void init(HttpServletRequest request, String lang, String subject, long skinId) throws Exception {

		// get the emailTemplate
		emailTemplate = getEmailTemplate(request, lang, skinId);

		// set the server properties
		serverProperties = new Hashtable();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable();

		emailProperties.put("subject", subject);

		// handle from attribute
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase ap = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		String appSource = ap.getAppSource();
		emailProperties.put("from", ApplicationDataBase.getSupportEmailBySkinId(skinId));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		logger.debug("Sending Email is running ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");

	}

	public String getEmailBody(HashMap params) throws Exception {

		// set the context and the parameters
		VelocityContext context = new VelocityContext();

		// get all the params and add them to the context
		String paramName = "";

		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName, params.get(paramName));
		}

		StringWriter sw = new StringWriter();
		emailTemplate.merge(context, sw);

		return sw.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	private Template getEmailTemplate(HttpServletRequest request, String lang, long skinId) throws Exception {

		try {

			String templateFile = CommonUtil.getProperty("receipt.email.template.file");
			String templatePath = CommonUtil.getProperty("templates.path");

			// add language folder to path.
			templatePath += lang + "_" + skinId + "/" ;

			// get the context and path
			//ServletContext context = request.getSession().getServletContext();
			//templatePath = context.getRealPath(templatePath);

			logger.debug("Template Path : " + templatePath + ", Template file: " + templateFile);

			Properties props = new Properties();
			props.setProperty("file.resource.loader.path", templatePath);
			props.setProperty("resource.loader", "file");
			props.setProperty("file.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache", "true");

			VelocityEngine v = new VelocityEngine();
			v.init(props);

			return v.getTemplate(templateFile,"UTF-8");

		} catch (Exception ex) {
			logger.fatal("!!! ERROR >> Cannot find registration Email template : " + ex.getMessage());
			throw ex;
		}
	}


}
