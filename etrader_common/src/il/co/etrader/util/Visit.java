package il.co.etrader.util;

import java.io.Serializable;
import java.util.Random;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

public class Visit implements Serializable {
	
	private static final long serialVersionUID = -6186796545806943637L;

	private static final Logger logger = Logger.getLogger(Visit.class);

	private long activeToken;

	private long receivedToken;

	private String uri;

	public Visit() {
		uri="";
	}

	/**
	 * This returns the active save token. Note that this is not the same
	 * variable that is set by the setSaveToken() method. This is so we can put
	 * a tag in a JSP:<br/>
	 *
	 * <h:inputHidden value="#{visit.saveToken}" /> <br/>
	 *
	 * That will retrieve the active token from Visit when the page is rendered,
	 * and when the page is returned, setSaveToken() sets the received token.
	 * The tokens can then be compared to see if a save should be performed. See
	 * BaseBean.generateToken() for more details.
	 *
	 * @return Returns the active save token.
	 */
	public long getSaveToken() {

		return this.activeToken;
	}

	public String getGenerate() {
		FacesContext context=FacesContext.getCurrentInstance();
		String t=context.getViewRoot().getViewId();
		logger.trace("viewID:"+t);
		logger.trace("uri:"+uri);
		if (!t.equals(uri)) {
			Random random = new Random();
			long token = random.nextLong();
			while (token == 0) {
				token = random.nextLong();
			}

			setActiveToken(token);
			setReceivedToken(0);
			logger.trace("update token:"+activeToken);
			uri=t;
		}
		return "";
	}
	public void setGenerate(String s) {


	}

	/**
	 * Sets the received token. Note that this method is only intended to be
	 * called by a JSF EL expression such as: <br/> <h:inputHidden
	 * value="#{visit.saveToken}" /> <br/> See getSaveToken() for more details
	 * on why.
	 *
	 * @param received
	 *            token value to set
	 */
	public void setSaveToken(long aToken) {
		this.receivedToken = aToken;
	}

	public void setReceivedToken(long aToken) {
		this.receivedToken = aToken;
	}

	public long getReceivedToken() {
		return this.receivedToken;
	}

	public void setActiveToken(long aToken) {
		this.activeToken = aToken;
	}

	public long getActiveToken() {
		return this.activeToken;
	}

	/**
	 * @return true if the active and received token are both non-zero and they
	 *         match
	 */
	public boolean tokensMatchAndAreNonZero() {
		return (this.activeToken != 0) && (this.receivedToken != 0) && (this.activeToken == this.receivedToken);
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
