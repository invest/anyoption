package il.co.etrader.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.util.AESUtil;

public class EncryptUpdateDBJob extends JobUtil {

	private static Logger log = Logger.getLogger(EncryptUpdateDBJob.class);

	private static final long ONE_ID = -1;
	private static final boolean IS_USERS_TABLE = true;
	private static final boolean INCLUDE_TEST_USERS = true;
	private static final boolean INCLUDE_REAL_USERS = true;
	private static final long FROM_ID = -1;
	private static final long TO_ID = -1;

	public static void main(String[] args) throws Exception {

		if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];
        ccNumEncryptionPhase1();
        /*updateEncryptFieldFromOriginalField("users", "id_num_back", "id_num",
        		Long.valueOf(getPropertyByFile("id", String.valueOf(ONE_ID))),
        		true,
        		Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_TEST_USERS))),
        		Boolean.valueOf(getPropertyByFile("realUsers", String.valueOf(INCLUDE_REAL_USERS))),
        		Long.valueOf(getPropertyByFile("fromId", String.valueOf(FROM_ID))),
        		Long.valueOf(getPropertyByFile("toId", String.valueOf(TO_ID))), false);*/
        //fillTestTable();
        //update_REAL_password();
        //update_REAL_CC_ID_NUM();
	}

	/*private static void update_REAL_CC_ID_NUM() throws Exception {
		log.log(Level.INFO, "Start encrypt data and update records...");
        long updateTime = updateEncryptFieldFromOriginalField("users", "id_num_back", "id_num",
        		Long.valueOf(getPropertyByFile("id", "-1")), false,
        		Boolean.valueOf(getPropertyByFile("testUsers", "false")),
        		Boolean.valueOf(getPropertyByFile("realUsers", "false")),
        		Long.valueOf(getPropertyByFile("fromId", "-1")),
        		Long.valueOf(getPropertyByFile("toId", "-1")));
        log.log(Level.INFO, "Start check if data == decrypt(encrypt(data))...");
        long checkTime = checkEncryption("users", "id_num_back", "id_num",
        		Long.valueOf(getPropertyByFile("id", "-1")), false,
        		Boolean.valueOf(getPropertyByFile("justTestUsers", "false")),
        		Boolean.valueOf(getPropertyByFile("realUsers", "false")),
        		Long.valueOf(getPropertyByFile("fromId", "-1")),
        		Long.valueOf(getPropertyByFile("toId", "-1")));

        log.log(Level.INFO, "Encrypt data and update records in " + getTimeDetails(updateTime));
        log.log(Level.INFO, "Check if data == decrypt(encrypt(data)) in " + getTimeDetails(checkTime));
	}

	private static void update_REAL_CC_NUMBER() throws Exception {
		log.log(Level.INFO, "Start encrypt data and update records...");
        long updateTime = updateEncryptFieldFromOriginalField("credit_cards", "cc_number_back", "cc_number",
        		Long.valueOf(getPropertyByFile("id", "-1")), false,
        		Boolean.valueOf(getPropertyByFile("testUsers", "false")),
        		Boolean.valueOf(getPropertyByFile("realUsers", "false")),
        		Long.valueOf(getPropertyByFile("fromId", "-1")),
        		Long.valueOf(getPropertyByFile("toId", "-1")));
        log.log(Level.INFO, "Start check if data == decrypt(encrypt(data))...");
        long checkTime = checkEncryption("credit_cards", "cc_number_back", "cc_number",
        		Long.valueOf(getPropertyByFile("id", "-1")), false,
        		Boolean.valueOf(getPropertyByFile("justTestUsers", "false")),
        		Boolean.valueOf(getPropertyByFile("realUsers", "false")),
        		Long.valueOf(getPropertyByFile("fromId", "-1")),
        		Long.valueOf(getPropertyByFile("toId", "-1")));

        log.log(Level.INFO, "Encrypt data and update records in " + getTimeDetails(updateTime));
        log.log(Level.INFO, "Check if data == decrypt(encrypt(data)) in " + getTimeDetails(checkTime));
	}*/

	/*private static void buildAndCheckTest() throws Exception {
		//log.log(Level.INFO, "Start fill test table...");
		//long fillTableTime = fillTestTable();
        log.log(Level.INFO, "Start encrypt data and update records...");
        long updateTime = updateNonEncryptToEncryptField("test_table", "pass_old", "pass_new", Long.valueOf(getPropertyByFile("id", "-1")),
        		Boolean.valueOf(getPropertyByFile("justTestUsers", "false")), Boolean.valueOf(getPropertyByFile("realUsers", "false")));
        log.log(Level.INFO, "Start check if data == decrypt(encrypt(data))...");
        long checkTime = checkEncryption("test_table", "pass_old", "pass_new", Long.valueOf(getPropertyByFile("id", "-1")),
        		Boolean.valueOf(getPropertyByFile("justTestUsers", "false")), Boolean.valueOf(getPropertyByFile("realUsers", "false")));

        //log.log(Level.INFO, "Fill test table in " + getTimeDetails(fillTableTime));
        log.log(Level.INFO, "Encrypt data and update records in " + getTimeDetails(updateTime));
        log.log(Level.INFO, "Check if data == decrypt(encrypt(data)) in " + getTimeDetails(checkTime));
	}*/

	private static long updateEncryptFieldFromOriginalField(String tableName, String nonEncryptField,
			String encryptField, long id, boolean isUsersTable, boolean testUsers, boolean realUsers,
			long fromId, long toId, boolean checkEncrypFieldNull) throws Exception {

		long start = new Date().getTime();
		Connection conn = getConnection();
    	Statement st = null;
    	PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> passSet = new HashSet<String>();
        boolean firstCond = true;
        try{
            String sql = "SELECT * FROM " + tableName;
            if (id > -1) {
            	sql += " WHERE id=" + id;
            	firstCond = false;
            } else if (isUsersTable) {
            	if (!(testUsers || realUsers)) {
            		sql += " WHERE id=-1"; // no row
            		firstCond = false;
            	} else if (!(testUsers && realUsers)) {
            		if (testUsers) {
            			sql += " WHERE class_id=0";
            		} else {
            			sql += " WHERE class_id<>0";
            		}
            		firstCond = false;
            	}
            }
            if (id < 0) {
	            if (fromId > -1) {
	            	sql += (firstCond ? " WHERE " : " AND ") + "id >= " + fromId;
	            }
	            if (toId > -1) {
	            	sql += (firstCond ? " WHERE " : " AND ") + "id <= " + toId;
	            }
            }
            if (checkEncrypFieldNull) {
            	sql += (firstCond ? " WHERE " : " AND ") + encryptField + " IS NULL ";
            }
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
            	String nonEncryptText = rs.getString(nonEncryptField);
            	passSet.add(nonEncryptText);
            }
        } finally {
        	try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }

        Iterator<String> it = passSet.iterator();
        try {
        	String sql = "UPDATE " + tableName + " SET " + encryptField + " = ? WHERE " + nonEncryptField + " = ?";
        	if (id > -1) {
        		sql += " AND id=" + id;
        	} else if (isUsersTable) {
        		if (!(testUsers || realUsers)) {
        			sql += " AND id= - 1"; // no row
        		} else if (!(testUsers && realUsers)) {
        			if (testUsers) {
        				sql += " AND class_id=0";
        			} else {
        				sql += " AND class_id<>0";
        			}
        		}
            }
        	if (fromId < 0) {
	        	if (fromId > -1) {
	            	sql += " AND id >= " + fromId;
	            }
	            if (toId > -1) {
	            	sql += " AND id <= " + toId;

	            }
        	}
        	ps = conn.prepareStatement(sql);
        	long passNum = 0;
	        while (it.hasNext()) {
	        	++passNum;
	        	String next = it.next();
	        	if (null == next) {
	        		continue;
	        	}
	        	String encryptNext = AESUtil.encrypt(next);
	        	ps.setString(1, encryptNext);
	        	ps.setString(2, next);
	        	log.log(Level.INFO, passNum + ": update encryption: " + next + " to " + encryptNext);
	        	ps.execute();
	        }
	        log.log(Level.INFO, "Number of unique " + nonEncryptField + ": " + passNum);
        } finally {
        	try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        return new Date().getTime() - start;
    }

	/**
	 *
	 * Table must contains ID field !!
	 */
	public static long checkEncryption(String tableName, String nonEncryptField, String encryptField,
			long id, boolean users, boolean testUsers, boolean realUsers, long fromId, long toId) throws Exception {
		long start = new Date().getTime();
		Connection conn = getConnection();
    	Statement st = null;
        ResultSet rs = null;
        long recordId;
        String nonEncryptedText;
        String encryptedText;
        boolean firstCond = true;
        try{
            String sql = "SELECT * FROM " + tableName;
            if (id > -1) {
            	sql += " WHERE id = " + id;
            	firstCond = false;
            } else if (users) {
            	if (!(testUsers || realUsers)) {
            		sql += " WHERE id = -1"; // no row
            		firstCond = false;
            	} else if (!(testUsers && realUsers)) {
            		if (testUsers) {
            			sql += " WHERE class_id = 0";
            		} else {
            			sql += " WHERE class_id <> 0";
            		}
            		firstCond = false;
            	}
            }
            if (id < 0) {
	            if (fromId > -1) {
	            	sql += (firstCond ? " WHERE " : " AND ") + "id >= " + fromId;
	            }
	            if (toId > -1) {
	            	sql += (firstCond ? " WHERE " : " AND ") + "id <= " + toId;
	            }
            }
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            long successCount = 0;
            long failsCount = 0;
            while (rs.next()) {
            	recordId = rs.getLong("id");
            	nonEncryptedText = rs.getString(nonEncryptField);
            	encryptedText = rs.getString(encryptField);
            	if (!nonEncryptedText.equals(AESUtil.decrypt(encryptedText))) {
            		log.log(Level.ERROR, "ERROR DECRYPTION: recordID = " + recordId + ", " +
            				nonEncryptField + " = " + nonEncryptedText + ", " + encryptField + " = " + encryptedText);
            		++failsCount;
            	} else {
            		++successCount;
            	}
            }
            log.log(Level.INFO, "Good encryptions: " + successCount + ". Errors: " + failsCount);
        } finally {
        	try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        return new Date().getTime() - start;
    }

	private static long fillTestTable() throws Exception {
		long start = new Date().getTime();
		Connection conn = getConnection();
    	Statement st = null;
        int listSize = 60000;
        List<String> list = new ArrayList<String>(listSize);
        //char[] charsArray = {'q', 'w', 'e', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k',
        //		'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        char[] charsArray = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        log.log(Level.INFO, "Start render password list...");
        int i;
        for (i=0; i<listSize; i++) {
        	String pass = "";
        	for (int j=0; j<16; j++) {
        		pass += charsArray[new Random().nextInt(charsArray.length)];
        	}
        	list.add(pass);
        }
        log.log(Level.INFO, "Start insert records to DB...");
        for (i=0; i<list.size(); i++) {
        	try {
        		String sql = "INSERT INTO TEST_TABLE (id, pass_old) VALUES (" + i+1 + ", '" + list.get(i) + "')";
        		st = conn.createStatement();
        		log.log(Level.INFO, i+1 + ": Insert row into test_table");
                st.executeUpdate(sql);
        	} finally {
        		try {
        			st.close();
        		} catch (Exception e) {
        			log.log(Level.ERROR, "Can't close", e);
        		}
        	}
        }
        return new Date().getTime() - start;
	}

	private static String getTimeDetails(long ms) {
		long minutes = ms / (60 * 1000);
		ms -= minutes * 1000 * 60;
		long seconds = ms / 1000;

		if (minutes > 0) {
			return minutes + " minutes and " + seconds + " seconds";
		}
		return seconds + " seconds";
	}

	private static void idNumEncryptionPhase1() throws Exception {
		/*
		String sql1 = "ALTER TABLE users ADD (id_num_back VARCHAR2(10 CHAR))";
		String sql2 = "ALTER TABLE users ADD (id_num_enc VARCHAR2(100 CHAR))";
		String sql3 = "UPDATE users SET id_num_back = id_num";
		String sql4 = "ALTER TABLE users MODIFY (id_num VARCHAR2(100 CHAR))";

		after job:
		UPDATE users SET id_num = id_num_enc where id_num_enc IS NOT NULL
				}*/
		updateEncryptFieldFromOriginalField("users", "id_num_back", "id_num_enc",
				Long.valueOf(getPropertyByFile("id", "-1")),
				true,
				Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_TEST_USERS))),
				Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_REAL_USERS))),
				Long.valueOf(getPropertyByFile("fromId", String.valueOf(FROM_ID))),
				Long.valueOf(getPropertyByFile("toId", String.valueOf(TO_ID))),
				false);
	}

	private static void idNumEncryptionPhase2() throws Exception {
		Connection con = getConnection();
		Statement st = null;
		String sql1 = "UPDATE users SET id_num_back = id_num WHERE id_num_back IS NULL";
		String sql2 = "UPDATE users SET id_num = id_num_enc WHERE id_num_enc IS NOT NULL";
		try {
			st = con.createStatement();
			st.executeUpdate(sql1);
			try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

			updateEncryptFieldFromOriginalField("users", "id_num_back", "id_num_enc",
					Long.valueOf(getPropertyByFile("id", "-1")),
					true,
					Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_TEST_USERS))),
					Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_REAL_USERS))),
					Long.valueOf(getPropertyByFile("fromId", String.valueOf(FROM_ID))),
					Long.valueOf(getPropertyByFile("toId", String.valueOf(TO_ID))),
					true);

			st = con.createStatement();
			st.executeUpdate(sql2);
		} finally {
			try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                con.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
		}
	}

	private static void ccNumEncryptionPhase1() throws Exception {
		/*
		String sql1 = "ALTER TABLE credit_cards ADD (cc_number_back NUMBER(20, 0))";
		String sql2 = "ALTER TABLE credit_cards ADD (cc_number_enc VARCHAR2(200 CHAR))";
		String sql3 = "UPDATE credit_cards SET cc_number_back = cc_number";

		//alter table credit_cards MODIFY (CC_NUMBER NULL)
		//UPDATE credit_cards SET cc_number = null
		//ALTER TABLE credit_cards MODIFY (cc_number VARCHAR2(200 CHAR))




		String sql4 = "ALTER TABLE credit_cards MODIFY (cc_number VARCHAR2(200 CHAR))";


		// after job:
		 * UPDATE credit_cards SET cc_number = cc_number_enc where cc_number_enc IS NOT NULL

		}*/
		updateEncryptFieldFromOriginalField("credit_cards", "cc_number_back", "cc_number_enc",
				Long.valueOf(getPropertyByFile("id", "-1")),
				false,
				Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_TEST_USERS))),
				Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_REAL_USERS))),
				Long.valueOf(getPropertyByFile("fromId", String.valueOf(FROM_ID))),
				Long.valueOf(getPropertyByFile("toId", String.valueOf(TO_ID))),
				false);
	}

	private static void ccNumEncryptionPhase2() throws Exception {
		Connection con = getConnection();
		Statement st = null;
		String sql1 = "UPDATE credit_cards SET cc_number_back = cc_number WHERE cc_number_back IS NULL";
		String sql2 = "UPDATE credit_cards SET cc_number = cc_number_enc WHERE cc_number_enc IS NOT NULL";
		try {
			st = con.createStatement();
			st.executeUpdate(sql1);
			try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

			updateEncryptFieldFromOriginalField("cc_number_back", "cc_number_back", "cc_number_enc",
					Long.valueOf(getPropertyByFile("id", "-1")),
					false,
					Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_TEST_USERS))),
					Boolean.valueOf(getPropertyByFile("testUsers", String.valueOf(INCLUDE_REAL_USERS))),
					Long.valueOf(getPropertyByFile("fromId", String.valueOf(FROM_ID))),
					Long.valueOf(getPropertyByFile("toId", String.valueOf(TO_ID))),
					true);

			st = con.createStatement();
			st.executeUpdate(sql2);
		} finally {
			try {
                st.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                con.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
		}
	}
}
