package il.co.etrader.util;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.MissingResourceException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;
import oracle.jdbc.driver.OracleDriver;


/**
 * Job Util class,
 * Contains common functionality and Data for all job's
 * @author Kobi Mualem
 *
 */
public class JobUtil extends com.anyoption.common.jobs.JobUtil {
    private static Logger log = Logger.getLogger(JobUtil.class);

    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASS = "db.pass";

    protected static Properties properties = null;
    protected static String propFile;
    protected static Template mailTemplate;


    /**
     * get some property from file, call to function getPropertyByFile
     * with more parameter
     * @param key
     * 			the key that we want to find in the file
     * @return
     * 		the string property value from the file
     */
    public static String getPropertyByFile(String key) {
		return getPropertyByFile(key,"?" + key + "?");
	}


    /**
     * get some property from file
     * @param key
     * 			the key that we want to find in the file
     *  @param defval
     *  		the deafult value that return if the key will not found
     * @return
     * 		the string property value from the file
     */
    public static String getPropertyByFile(String key, String defval) {

		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(new FileInputStream(propFile));
			} catch (Exception ex) {
				log.fatal(ex.toString());
				System.exit(1);
			}
		}

		if (key == null || key.trim().equals(""))
			return "";

		String text = null;
		try {
			text = properties.getProperty(key);
		} catch (MissingResourceException e) {
			return defval;
		}
		if (text == null) {   // if the property not found
			return defval;
		}

		return text;

	}

    /**
     * Get Connection to DB
     *
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        return getConnection(DB_URL, DB_USER, DB_PASS);
    }

    /**
     * Get Connection to DB
     *
     * @param url url property
     * @param user user property
     * @param pass password property
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection(String url, String user, String pass) throws SQLException {
    	 DriverManager.registerDriver(new OracleDriver());
    	 return DriverManager.getConnection(getPropertyByFile(url),
    			getPropertyByFile(user),
    			getPropertyByFile(pass) );
    }
     
    /**
     * @param url
     * @param user
     * @param pass
     * @return
     * @throws SQLException
     */
    public static Connection getConnectionWithoutFile(String url, String user, String pass) throws SQLException { 
    	DriverManager.registerDriver(new OracleDriver());
   	 	return DriverManager.getConnection(url,user,pass);
    }

    /**
     * Send email with specified properties through specified server.
     *
     * @param server hashtable with server properties:
     *          "url" the url of the SMTP server to send mail through
     *          "auth" if the server requires authentication
     *          "user" if the server requires authentication the user name to use
     *          "pass" if the server requires authentication the pass to use
     * @param msg hasmap with email properties:
     *          "subject" the subject of the email
     *          "to" the receipient of the mail (can be multiple emails separated by ";")
     *          "from" the sender of the mail
     *          "body" the email body
     */

    public static void sendEmail(Hashtable server, Hashtable msg) {
        sendEmail(server, msg, null, null);
    }

    /**
     * Send email with specified properties through specified server.
     *
     * @param server hashtable with server properties:
     *          "url" the url of the SMTP server to send mail through
     *          "auth" if the server requires authentication
     *          "user" if the server requires authentication the user name to use
     *          "pass" if the server requires authentication the pass to use
     * @param msg hasmap with email properties:
     *          "subject" the subject of the email
     *          "to" the receipient of the mail (can be multiple emails separated by ";")
     *          "from" the sender of the mail
     *          "body" the email body
     * @param attachements the attachements
     * @param headers the extra headers to specify
     */

    public static void sendEmail(Hashtable server, Hashtable msg, DataHandler[] attachements, Hashtable[] headers) {
        Properties props = new Properties();
        props.put("mail.smtp.host", (String) server.get("url"));
        props.put("mail.smtp.auth", (String) server.get("auth"));
        String contentType = (String) server.get("contenttype");
        String subject = (String) msg.get("subject");
        String to = (String) msg.get("to");
        String from = (String) msg.get("from");
        String body = (String) msg.get("body");
        String[] splitTo = to.split(";");
        for (int i = 0; i < splitTo.length; i++) {
            sendSingleEmail(server, props, contentType, subject, splitTo[i], from, body, attachements, headers);
        }
    }

    /**
     * Send email to one receipient.
     *
     * @param server email server properties
     * @param props the session default properties
     * @param contentType the content type of the mail
     * @param subject email subject
     * @param to email rcpt
     * @param from email sender
     * @param body email body
     * @param attachements the attachements
     * @param headers the extra headers to specify
     */

    private static void sendSingleEmail(
            Hashtable server,
            Properties props,
            String contentType,
            String subject,
            String to,
            String from,
            String body,
            DataHandler[] attachements,
            Hashtable[] headers) {
        boolean auth = null != server.get("auth") && ((String) server.get("auth")).equals("true");
        final String un = (String) server.get("user");
        final String ps = (String) server.get("pass");
        Session session = null;
        if (auth) {
            session = Session.getInstance(props, new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(un, ps);
                    }
                });
        } else {
            session = Session.getInstance(props, null);
        }
        Message mess = new MimeMessage(session);

        try {
            mess.setFrom(new InternetAddress(from));
            mess.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mess.setSubject(subject);

            if (null == attachements) {
                mess.setContent(body,  contentType);
            } else {
                Multipart mp = new MimeMultipart("mixed");
                BodyPart bp1 = new MimeBodyPart();
                bp1.setDataHandler(new DataHandler(body, contentType));
                mp.addBodyPart(bp1);
                for (int i = 0; i < attachements.length; i++) {
                    BodyPart bp = new MimeBodyPart();
                    if (null != attachements[i].getName()) {
                        bp.setFileName(attachements[i].getName());
                    }
                    bp.setDataHandler(attachements[i]);
                    if (null != headers && null != headers[i]) {
                        for (Enumeration e = headers[i].keys(); e.hasMoreElements();) {
                            String key = (String) e.nextElement();
                            bp.setHeader(key, (String) headers[i].get(key));
                        }
                    }
                    mp.addBodyPart(bp);
                }
                mess.setContent(mp);
            }

            Transport.send(mess);

        } catch (AddressException ae) {
            log.info("Error Sending Mail to: " + to + "body: " + body, ae);
        } catch (MessagingException me) {
            log.info("Error Sending Mail to: " + to + "body: " + body, me);
        }
    }

    /**
     * Locates and load conf file.
     *
     * @param file the job config file
     * @return <code>Properties</code> with the job configuration or <code>null</code> if loading failed.
     */
    public static Properties loadConfig(String file) {
        Properties conf = null;
        if (null != file && file.equals("")) {
            log.log(Level.FATAL, "No conf file specified.");
            return null;
        }
        File confFile = new File(file);
        if (!confFile.exists()) {
            log.log(Level.FATAL, file + " does not exists.");
            return null;
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(confFile);
            conf = new Properties();
            conf.load(fis);
        } catch (Exception e) {
            log.log(Level.FATAL, "Failed to load conf file.", e);
            conf = null;
        } finally {
            try {
                fis.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Cant close conf file.");
            }
        }
        return conf;
    }

    /**
     * Send single email and create issues with email action
     * @param conn
     * @param serverProperties  email server properties
     * @param templateName  the html file name
     * @param params the email parameters
     * @param subjectKey the subject key of the email, must be: key + "." language code
     * @param createIssue true for creating a new email issue
     * @param isContacts true for user details from contacts
     * @param issueSubjectId for general subject, set to 0
     * @param user user instance
     * @throws Exception
     */
    public static void sendSingleEmail(Connection conn, Hashtable serverProperties,
    		String templateName,
    		HashMap<String, String> params, String subjectKey, String commentKey,
    		boolean createIssue, boolean isContacts, long issueSubjectId, UserBase user, Long recordId, long templateId)
    		throws Exception {

    	long langId = SkinsDAOBase.getById(conn, (int)user.getSkinId()).getDefaultLanguageId();
    	String langCode = LanguagesDAOBase.getCodeById(conn, langId);
    	log.debug("skinId: " + user.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
    	mailTemplate = getEmailTemplate(templateName, langCode, String.valueOf(user.getSkinId()));
    	Hashtable<String, String> emailProperties = new Hashtable<String, String>();
    	String subjectParam = params.get("subject");
    	if (subjectParam != null) {
    		emailProperties.put("subject", subjectParam);
    	} else {
	    	emailProperties.put("subject", properties.getProperty(subjectKey + "." + langCode));
	    }
    	String fromParam = params.get("from");
    	if (fromParam != null) {
    		emailProperties.put("from", fromParam);
    	}
    	String toParam = params.get("to");
    	if (toParam != null) {
    		emailProperties.put("to", toParam);
    	} else {
    		emailProperties.put("to", user.getEmail());
    	}

    	try {
    		emailProperties.put("body",getEmailBody(params));
    	} catch (Exception e) {
	    	log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
	    	System.exit(1);
	    }
	    sendSingleEmail(properties,emailProperties, langCode);


	    // Create issue
		Issue issue = new Issue();
		IssueAction issueAction = new IssueAction();
		// Filling issue fields
		if (issueSubjectId > 0) {
			issue.setSubjectId(String.valueOf(issueSubjectId));
		} else {
			issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		}
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		if (isContacts) {
			issue.setPopulationEntryId(recordId);
			issue.setContactId(user.getId());
		} else {
			issue.setContactId(user.getContactId());
		}
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(user.getUtcOffset());
		if (commentKey != null) {
			issueAction.setComments(properties.getProperty(commentKey));
		} else {
			issueAction.setComments(properties.getProperty(subjectKey + "." + langCode));
		}
		if (!isContacts && null != recordId) {
			issueAction.setTransactionId(recordId);
		}
		IssuesDAOBase.insertIssue(conn, issue);
		issueAction.setIssueId(issue.getId());
		issueAction.setTemplateId(templateId);
		IssuesDAOBase.insertAction(conn, issueAction);

    }

    /**
     * Send single email template
     * @param serverProperties
     * @param emailProperties
     * @param lang
     */
    public static void sendSingleEmail(Hashtable serverProperties, Hashtable emailProperties, String lang) {
		Properties servProps = new Properties();
		final String userEmail = (String) serverProperties.get("email.user");
		final String passEmail = (String) serverProperties.get("email.pass");
		servProps.put("mail.smtp.host", (String) serverProperties.get("email.url"));
		boolean auth = null != serverProperties.get("auth") && ((String) serverProperties.get("auth")).equals("true");

		String subject = (String) emailProperties.get("subject");
		String to = (String) emailProperties.get("to");

		String from = (String) emailProperties.get("from");
		if ( lang.equals(ConstantsBase.ETRADER_LOCALE) ) {
		from = properties.getProperty("email.from.etrader");
		} else if (lang.equals(ConstantsBase.CHINESE_LOCALE)){
				from = getPropertyByFile("email.from.anyoption.zh");
		} else {
		from = properties.getProperty("email.from.anyoption");
		}
		String body = (String) emailProperties.get("body");

		Session session = null;
        if (auth) {
        	 session = Session.getInstance(servProps, new Authenticator() {
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userEmail, passEmail);
                }
            });
        } else {
            session = Session.getInstance(servProps, null);
        }

		MimeMessage mess = new MimeMessage(session);

		try {
			// Set Headers
			mess.setHeader("From", from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=utf-8;");
			mess.setFrom(new InternetAddress(from));
			mess.setSubject(subject,"UTF-8");
			mess.setContent(body,"text/html; charset=utf-8;");
			Transport.send(mess);
		} catch (SendFailedException sfe){
			log.log(Level.ERROR, "Mail Adress not correct! " + to);
		}
		catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
	}

    public static Template getEmailTemplate(String fileName, String lang, String skinId) throws Exception {
		try {
			Properties props = new Properties();
			String tempPath = "";
			if (null != lang && null != skinId) {
				tempPath =  properties.getProperty("templates.path") + lang + "_" + skinId + "/" ;
			} else {
				tempPath = properties.getProperty("templates.path");
			}
			log.debug("TemplatePath: " + tempPath);
			props.setProperty("file.resource.loader.path", tempPath);
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			VelocityEngine v = new VelocityEngine();
			v.init(props);
			return v.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}

	public static String getEmailBody(HashMap params) throws Exception {
		// set the context and the parameters
		VelocityContext context = new VelocityContext();
		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");
		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		mailTemplate.merge(context,sw);
		return sw.toString();
	}

    public static void closeStatement(final Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                log.error(stmt, ex);
            }
        }
    }

    public static void closeResultSet(final ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                log.error(rs, ex);
            }
        }
    }
    
    public static Job startJob(long jobId, String serverId, String application) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return startJob(conn, jobId, serverId, application);
        } finally {
            BaseBLManager.closeConnection(conn);
        }
    }
    
    public static void stopJob(long jobId, String serverId, boolean updateRunTime) {
        Connection conn = null;
        try {
            conn = getConnection();
            stopJob(conn, jobId, serverId, updateRunTime);
        } catch (SQLException sqle) {
            log.error("Can't stop job.", sqle);
        } finally {
            BaseBLManager.closeConnection(conn);
        }
    }
    
    public static String fileToString(String fileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
	 		String row = new String();
	 		StringBuffer contents = new StringBuffer();
	 		// repeat until all lines have been read
	 		while ((row = br.readLine()) != null) {
		        contents.append(row);
		        contents.append(" ");
	 		}
	 		return contents.toString().substring(0,contents.toString().length()-1);
	 	} catch(IOException ioe) {
	 		log.fatal("Error reading users CVS file !!! Job aborted");
	    } finally {
	    	
	    }
	  return null;	     
	}
}