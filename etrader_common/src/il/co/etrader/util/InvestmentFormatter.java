package il.co.etrader.util;

import il.co.etrader.bl_managers.InvestmentsManagerBase;

import java.sql.SQLException;
import java.util.Random;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.settlement.DynamicsSettlementHandler;

/**
 * @author kirilim
 */
public class InvestmentFormatter {

	public static String getRefundTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsCanceled() == 1) {
			if (i.getBonusOddsChangeTypeId() == 1) {
				return CommonUtil.displayAmount(i.getAmount(), i.getCurrencyId());
			} else {
				return CommonUtil.getMessage("investments.iscanceled", null);
			}
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(i), i.getCurrencyId());
	}

	public static String getRefundWithBonusTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsCanceled() == 1) {
			return CommonUtil.displayAmount(i.getAmount() - i.getLose() + i.getWin(), i.getCurrencyId());
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(i) + i.getTransactionAmount(), i.getCurrencyId());
	}

	public static String getBinary0100WinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
														* i.getInvestmentsCount()) * (-1), true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(i.getAmount(), true, i.getCurrencyId());
		}
		return displayAmount;
	}
	
	// daily screen if above (how<br/>much house win)
	public static String getDynamicsWinUpTxt(Investment i) {
		String s = "";
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY) {
			displayAmount = CommonUtil.displayAmount( (-10000) * DynamicsSettlementHandler.getDynamicsNumberOfContracts(i).doubleValue(), true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL) {
			displayAmount = CommonUtil.displayAmount(i.getAmount(), true, i.getCurrencyId());
		}
		return displayAmount;
	}

	public static String getBinary0100WinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
														* i.getInvestmentsCount()) + i.getAmount(), true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(0, true, i.getCurrencyId());
		}
		return displayAmount;
	}
	//investments.if.above.customer
	public static String getDynamicsWinUpCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY) {
			displayAmount = CommonUtil.displayAmount(10000 * DynamicsSettlementHandler.getDynamicsNumberOfContracts(i).doubleValue(), true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL) {
			displayAmount = CommonUtil.displayAmount(0, true, i.getCurrencyId());
		}
		return displayAmount;
	}

	public static String getBinary0100LoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(i.getAmount(), true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
															* i.getInvestmentsCount()),
														true, i.getCurrencyId());
		}
		return displayAmount;
	} 

	public static String getBinary0100LoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(0, true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(i.getAmount() + ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
																		* i.getInvestmentsCount()),
														true, i.getCurrencyId());
		}
		return displayAmount;
	}

	//investments.if.below.customer
	public static String getDynamicsLoseDownCustomerTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY) {
			displayAmount = CommonUtil.displayAmount(0, true, i.getCurrencyId());
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL) {
			displayAmount = CommonUtil.displayAmount(10000 * DynamicsSettlementHandler.getDynamicsNumberOfContracts(i).doubleValue(), true, i.getCurrencyId());
		}
		return displayAmount;
	}

//	public static String getBinary0100BaseRefundUpTxt(Investment i) {
//		String displayAmount = "";
//		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
//															* i.getInvestmentsCount())
//														* i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
//		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(i.getBaseAmount(), true, ConstantsBase.CURRENCY_BASE_ID);
//		}
//		return displayAmount;
//	}

	public static String getBinary0100BaseWinUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
															* i.getInvestmentsCount())
														* i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(i.getBaseAmount(), true, ConstantsBase.CURRENCY_BASE_ID);
		}
		return displayAmount;
	}
	//investments.if.above (how much house win)
	public static String getDynamicsBaseWinUpTxt(Investment i) {
		String s= "";
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY) {
			displayAmount = CommonUtil.displayAmount(-10000 * DynamicsSettlementHandler.getDynamicsNumberOfContracts(i).doubleValue() * i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL) {
			displayAmount = CommonUtil.displayAmount(i.getBaseAmount(), true, ConstantsBase.CURRENCY_BASE_ID);
		}
		return displayAmount;
	}

	public static String getBinary0100BaseRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(i.getBaseAmount(), true, ConstantsBase.CURRENCY_BASE_ID);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
															* i.getInvestmentsCount())
														* i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
		}
		return displayAmount;
	}

	public static String getBinary0100BaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			displayAmount = CommonUtil.displayAmount(i.getBaseAmount(), ConstantsBase.CURRENCY_BASE_ID);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (i.getAmount() / i.getInvestmentsCount() / 100d))* 100
															* i.getInvestmentsCount())
														* i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
		}
		return displayAmount;
	}
	
	//investments.if.below (how much house win)
	public static String getDynamicsBaseLoseDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		String displayAmount = "";
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY) {
			displayAmount = CommonUtil.displayAmount((long)(i.getAmount() * i.getRate()), ConstantsBase.CURRENCY_BASE_ID);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL) {
			displayAmount = CommonUtil.displayAmount(-10000 * DynamicsSettlementHandler.getDynamicsNumberOfContracts(i).doubleValue() * i.getRate(), true, ConstantsBase.CURRENCY_BASE_ID);
		}
		return displayAmount;
	}

	public static String getUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsSettled() == 1) {
			return CommonUtil.displayAmount(-1 * (i.getWin() - i.getLose()), i.getCurrencyId());
		}
		return "";
	}

	public static String getCustomerUserWinLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsSettled() == 1) {
			return CommonUtil.displayAmount(i.getAmount() + (i.getWin() - i.getLose()), i.getCurrencyId());
		}
		return "";
	}

	public static String getUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsSettled() == 1) {
			return CommonUtil.displayAmount((-1)* (i.getWin() - i.getLose()) * i.getInvestmentsCount()
											/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1), i.getCurrencyId());
		}
		return "";
	}

	public static String getCustomerUserWinLoseTxt0100(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsSettled() == 1) {
			return CommonUtil.displayAmount(i.getAmount()+ ((i.getWin() - i.getLose())* i.getInvestmentsCount()
																/ (i.getContractsStep() != 0 ? i.getContractsStep() : 1)),
											i.getCurrencyId());
		}
		return "";
	}

	public static String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getClosingLevel() != null && i.getClosingLevel() > 0 && i.getIsCanceled() == 0) {
			return CommonUtil.formatLevelByMarket(i.getClosingLevel(), i.getMarketId());
		}
		if (i.getIsCanceled() > 0) {
			return CommonUtil.getMessage("investments.iscanceled", null);
		}
		return "";
	}

	public static String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY || i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) {
			return ""+i.getOppCurrentLevel();
		}

		if (InvestmentsManagerBase.INVESTMENT_TYPE_ONE == i.getTypeId()) {
			return CommonUtil.formatLevelByDecimalPoint(i.getCurrentLevel(), i.getOneTouchDecimalPoint());
		}
		return CommonUtil.formatLevelByMarket(i.getCurrentLevel(), i.getMarketId());
	}

	public static String getWwwLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getWwwLevel() == null) {
			return "";
		}
		return CommonUtil.formatLevelByMarket(i.getWwwLevel().doubleValue(), i.getMarketId());
	}

	public static String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		long offsetCreated = CommonUtil.getUtcOffsetValue(i.getUtcOffsetCreated());
		long offsetEstSettled = CommonUtil.getUtcOffsetValue(i.getUtcOffsetEstClosing());
		long offsetGain = 0;
		if (offsetCreated != -1 && offsetEstSettled != -1) {   // -1 : problem with getting offset value
			offsetGain = Math.abs(offsetCreated - offsetEstSettled);
		}
		if (offsetGain > ConstantsBase.OFFSET_GAIN_BETWEEN_CREATED_AND_SETTLED) {
			return CommonUtil.getDateTimeFormatDisplay(i.getTimeEstClosing(), CommonUtil.getUtcOffset(i.getUtcOffsetCreated()));
		}
		return CommonUtil.getDateTimeFormatDisplay(i.getTimeEstClosing(), CommonUtil.getUtcOffset(i.getUtcOffsetEstClosing()));
	}

	public static String getBubbleStartTimeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getTimeFormat(i.getBubbleStartTime(), i.getUtcOffsetCreated(), "HH:mm:ss");
	}
	
	public static String getBubbleStartTimeDateOnlyTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getTimeFormat(i.getBubbleStartTime(), i.getUtcOffsetCreated(), "dd.MM.YYYY");
	}
	
	public static String getTimeCreatedForBubbleTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getTimeFormat(i.getTimeCreated(), i.getUtcOffsetCreated(), "HH:mm:ss dd.MM.YYYY");
	}
	
	public static String getBubbleEndTimeTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.getTimeFormat(i.getBubbleEndTime(), i.getUtcOffsetEstClosing(), "HH:mm:ss");
	}

	public static String getTimeSettledTxt(Investment i) {
		if (i == null) {
			return "";
		}
	    if(InvestmentsManagerBase.isBubbles(i)) {
		return CommonUtil.getTimeFormat(i.getTimeSettled(), i.getUtcOffsetSettled(), "HH:mm:ss dd.MM.YYYY");
	    }
		return CommonUtil.getDateTimeFormatDisplay(i.getTimeSettled(), CommonUtil.getUtcOffset(i.getUtcOffsetSettled()));
	}

	public static String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getAmount(), i.getCurrencyId());
	}

	public static String getBubbleReturnToWinTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getBubbleReturnToWin(),true, i.getCurrencyId());
	}
	
	public static String getBubbleReturnToLoseTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(i.getBubbleReturnToLose(),true, i.getCurrencyId());
	}

	public static String getAmountMaxRiskTxt(Investment i) {
		if (i == null) {
			return "";
		}
		double maxRiskAmount = 0;
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			maxRiskAmount = i.getInvestmentsCount() * (i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			maxRiskAmount = (i.getInvestmentsCount() * (i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i));
		}
		return CommonUtil.displayAmount(maxRiskAmount, true, i.getCurrencyId());
	}

	public static String getAmountMaxRiskTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.removeFractionFromAmount(getAmountMaxRiskTxt(i));
	}

	public static String getMaxProfitTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.removeFractionFromAmount(getMaxProfitTxt(i));
	}

	public static String getMaxProfitTxt(Investment i) {
		if (i == null) {
			return "";
		}
		double maxReturnAmount = 0;
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			maxReturnAmount = (i.getInvestmentsCount()
								* (100 - ((i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i) / 100.0))) * 100;
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			maxReturnAmount = (i.getInvestmentsCount()
								* (100 - ((i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i) / 100.0))) * 100;
		}
		return CommonUtil.displayAmount(maxReturnAmount, true, i.getCurrencyId());
	}

	public static String getAmountReturnTxtExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.removeFractionFromAmount(getAmountReturnTxt(i));
	}

	public static String getAmountReturnTxt(Investment i) {
		if (i == null) {
			return "";
		}
		double returnAmount = 0;
		if (InvestmentsManagerBase.isSettledBeforTime(i)) {
			returnAmount = (i.getAmount() + i.getWin() - i.getLose()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i) * i.getInvestmentsCount();
		} else if (!InvestmentsManagerBase.isSettledBeforTime(i) && i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY && i.getWin() - i.getLose() > 0) {
			returnAmount = (i.getInvestmentsCount() * 100) * 100;
		} else if (!InvestmentsManagerBase.isSettledBeforTime(i) && i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY && i.getWin() - i.getLose() < 0) {
			returnAmount = 0;
		} else if (!InvestmentsManagerBase.isSettledBeforTime(i) && i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL && i.getWin() - i.getLose() > 0) {
			returnAmount = (i.getInvestmentsCount() * 100) * 100;
		} else if (!InvestmentsManagerBase.isSettledBeforTime(i) && i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL && i.getWin() - i.getLose() < 0) {
			returnAmount = 0;
		}
		return CommonUtil.displayAmount(returnAmount, true, i.getCurrencyId());
	}

	public static String getInvestmentsAmount(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount((i.getAmount())/ InvestmentsManagerBase.getBinary0100NumberOfContracts(i) * i.getInvestmentsCount(), true,
										i.getCurrencyId());
	}

	public static String getInvestmentsAmountExcludDecimal(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.removeFractionFromAmount(getInvestmentsAmount(i));
	}

	public static String getCurrentLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		double currentLeve = 0;
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			currentLeve = (i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i);
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			currentLeve = (100.0 - ((i.getAmount() - i.getOptionPlusFee()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i) / 100.0)) * 100.0;
		}
		return CommonUtil.displayAmount(currentLeve, false, 0);
	}

	public static String getClosingLevel0100(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			return CommonUtil.displayAmount(10000- (i.getAmount() + i.getWin() - i.getLose()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i), false,
											0);
		}  // its buy
		return CommonUtil.displayAmount((i.getAmount() + i.getWin() - i.getLose()) / InvestmentsManagerBase.getBinary0100NumberOfContracts(i), false, 0);
	}

	public static String getCurrentLevel0100BE(Investment i) {
		if (i == null) {
			return "";
		}
		double currentLeve = 0;
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			currentLeve = (i.getAmount() - (i.getOptionPlusFee() * (i.getInvestmentsCount() / i.getContractsStep())))
							/ (double) i.getInvestmentsCount();
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			currentLeve = (100.0 - (((i.getAmount() - (i.getOptionPlusFee() * (i.getInvestmentsCount() / i.getContractsStep())))
										/ (double) i.getInvestmentsCount())
									/ 100.0))
							* 100.0;
		}
		return CommonUtil.displayAmount(currentLeve, false, 0);
	}

	public static String getClosingPrice(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			return CommonUtil.displayAmount(10000- i.getAmount() / (double) i.getInvestmentsCount()
											- (i.getWin() - i.getLose()) / i.getContractsStep(), false, 0);
		}  // buy
		return CommonUtil.displayAmount(i.getAmount() / (double) i.getInvestmentsCount()
										+ (i.getWin() - i.getLose()) / i.getContractsStep(), false, 0);
	}

	public static String getClosingLevel0100BE(Investment i) {// works for DYNAMICS also
		if (i == null) {
			return "";
		}
		return CommonUtil.formatLevelByDecimalPoint(i.getClosingLevel(), i.getDecimalPoint());
	}

	public static String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		if (i.getCanceledWriterId() == null)
			return "";

		return CommonUtil.getWriterName(i.getCanceledWriterId().longValue());
	}

	public static String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		if (i.getIsCanceled() == 0) {
			return CommonUtil.getMessage("no", null);
		}
		return CommonUtil.getMessage("yes", null);
	}

	public static String getStyleColor(long total) {
		if (total > 0) {
			return "user_strip_value_darkgreen";
		}
		return "user_strip_value_red";
	}

	public static String getShowOffText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		if (i.getShowOffText() != null) {
			return i.getShowOffText();
		}
		final int SENTENCES_NUM = 4;
		if (i.getShSen() < 0) {
			i.setShSen(new Random().nextInt(SENTENCES_NUM));
		}
		String name = i.getUserFirstName() + " " + i.getUserLastName().substring(0, 1) + ":";
		String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(i.getTimeCreated(), i.getUtcOffsetCreated());
		String[] showParam = new String[] {	CommonUtil.removeFractionFromAmount(getAmountTxt(i)), i.getTypeName(), i.getMarketName(),
											tempStringArr[0], CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(i)),
											String.valueOf(CommonUtil.getDateDistance(i.getTimeCreated(), i.getTimeSettled(), null)),
											tempStringArr[2]};
		String show = CommonUtil.getMessage("showOff.sentence." + String.valueOf(i.getShSen()), showParam);
		String twitterShow = CommonUtil.getMessage("showOff.twitter.sentence." + String.valueOf(i.getShSen()), showParam);
		name = CommonUtil.capitalizeFirstLetters(name);
		i.setShowOffTwitterText(name + " " + twitterShow + " anyoption.com");
		name = name.replaceAll("�", "&amp;lsquo;");
		name = name.replaceAll("�", "&amp;rsquo;");
		name = name.replaceAll("'", "\\\\'");
		show = show.replaceAll("�", "&amp;lsquo;");
		show = show.replaceAll("�", "&amp;rsquo;");
		show = show.replaceAll("'", "\\\\'");
		i.setShowOffFacebookText(name + " " + show + " www.anyoption.com");
		String agree = CommonUtil.getMessage("showOff.agree", null);
		agree = agree.replaceAll("�", "&amp;lsquo;");
		agree = agree.replaceAll("�", "&amp;rsquo;");
		agree = agree.replaceAll("'", "\\\\'");
		return "<strong>" + name + "</strong>&#160;" + show + "<br /><br />" + agree;
	}

	public static String getShowOffFacebookText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		if (null == i.getShowOffFacebookText()) {
			getShowOffText(i);
		}
		return i.getShowOffFacebookText();
	}

	public static String getShowOffTwitterCleanText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		if (null == i.getShowOffTwitterText()) {
			getShowOffText(i);
		}
		return i.getShowOffTwitterText();
	}

	public static String getShowOffTwitterText(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		String text = getShowOffTwitterCleanText(i);
		text = text.replaceAll("�", "&amp;lsquo;");
		text = text.replaceAll("�", "&amp;rsquo;");
		text = text.replaceAll("'", "\\\\'");
		return text;
	}
}