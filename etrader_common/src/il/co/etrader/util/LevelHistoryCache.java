//package il.co.etrader.util;
//
//import java.util.ArrayList;
//import java.util.Hashtable;
//
//import com.anyoption.common.beans.ChartsUpdaterMarket;
//import com.anyoption.common.beans.MarketRate;
//import com.anyoption.common.util.ChartsUpdaterListener;
//
///**
// * Keep the the level history of each market from the current hour. To be used in showing the history of hour opps.
// *
// * @author Tony
// */
//public class LevelHistoryCache implements ChartsUpdaterListener {
//    private Hashtable<Long, ArrayList<MarketRate>> histories;
//
//    public LevelHistoryCache() {
//        histories = new Hashtable<Long, ArrayList<MarketRate>>();
//    }
//
//    public void marketRates(ChartsUpdaterMarket m, ArrayList<MarketRate> mrs) {
//        ArrayList<MarketRate> h = histories.get(m.getId());
//        if (null == h) {
//            h = new ArrayList<MarketRate>();
//            histories.put(m.getId(), h);
//        }
//        while (h.size() > 0 && 
//                ((null != m.getLastHourClosingTime() && h.get(0).getRateTime().compareTo(m.getLastHourClosingTime()) < 0) ||
//                (System.currentTimeMillis() - h.get(0).getRateTime().getTime() > 4 * 60 * 60 * 1000))) {
//            // clearing all before last closing is good for hour opps
//            // BUT end of the day is also considered as hour opp. and we can have quotes in the db after
//            // end of the day est closing (as actual closing can be after est closing)
//            // so make a raw guess there are at least 4 hours "night brake" and clean all rates older than that
//            h.remove(0);
//        }
//        for (int i = 0; i < mrs.size(); i++) {
//            if (null == m.getLastHourClosingTime() || mrs.get(i).getRateTime().compareTo(m.getLastHourClosingTime()) > 0) {
//                h.add(mrs.get(i));
//            }
//        }
//    }
//
//    public void marketClosed(ChartsUpdaterMarket m) {
//        histories.remove(m.getId());
//    }
//
//    public ArrayList<MarketRate> getMarketHistory(long marketId) {
//        ArrayList<MarketRate> h = histories.get(marketId);
//        if (null != h) {
//            return (ArrayList<MarketRate>) h.clone();
//        }
//        return null;
//    }
//}