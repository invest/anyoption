//package il.co.etrader.util;
//
//import java.util.ArrayList;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.ChartsUpdaterMarket;
//import com.anyoption.common.beans.MarketRate;
//import com.anyoption.common.util.ChartsUpdaterListener;
//
//public class ChartHistoryCache implements ChartsUpdaterListener {
//    private static final Logger log = Logger.getLogger(ChartHistoryCache.class);
//    
//    private Hashtable<Long, ArrayList<MarketRate>> histories;
//
//    public ChartHistoryCache() {
//        histories = new Hashtable<Long, ArrayList<MarketRate>>();
//    }
//
//    public void marketRates(ChartsUpdaterMarket m, ArrayList<MarketRate> mrs) {
//        if (log.isTraceEnabled()) {
//            log.trace("Got for market " + m.getId() + " rates " + mrs.size());
//        }
//        ArrayList<MarketRate> h = histories.get(m.getId());
//        boolean shouldAddToHistories = false;
//        if (null == h) {
//            h = new ArrayList<MarketRate>();
//            shouldAddToHistories = true;
//        }
//        long lastHourOpeningTime = 0;
//        if (null != m.getLastHourClosingTime()) { // markets that don't have closed opp yet
//                lastHourOpeningTime = m.getLastHourClosingTime().getTime() - 60 * 60 * 1000;
//        }
//        // remove all rates older than prev hour opening time
//        while (h.size() > 0 && h.get(0).getRateTime().getTime() < lastHourOpeningTime) {
//            h.remove(0);
//        }
//        for (int i = 0; i < mrs.size(); i++) {
//            if (mrs.get(i).getRateTime().getTime() >= lastHourOpeningTime) {
//                h.add(mrs.get(i));
//            }
//        }
//        if (log.isTraceEnabled()) {
//            log.trace("In the end " + h.size());
//        }
//        if (shouldAddToHistories) {
//            histories.put(m.getId(), h);
//            log.trace("Added to histories");
//        }
//    }
//
//    public void marketClosed(ChartsUpdaterMarket m) {
//        histories.remove(m.getId());
//    }
//
//    public ArrayList<MarketRate> getMarketHistory(long marketId) {
//        ArrayList<MarketRate> h = histories.get(marketId);
//        if (null != h) {
//            return (ArrayList<MarketRate>) h.clone();
//        }
//        return null;
//    }
//}