package il.co.etrader.util;

import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;

//import tap.core.ServerConfiguration;
//import tap.util.CommonUtil;

public class SendContactEmail extends Thread  {

	// The email parameters that we need
	public static final String PARAM_EMAIL = "Email";
	public static final String PARAM_USER_NAME = "userName";
	public static final String PARAM_FIRST_NAME = "firstName";
	public static final String PARAM_LAST_NAME = "lastName";
	public static final String PARAM_MOBILE = "mobile";
	public static final String PARAM_ISSUE = "issue";
	public static final String PARAM_COMMENTS = "comments";
	public static final String SUCCESS_MESSAGE = "Success Message";
	public static final String RETURN_PAGE = "page";
	public static final String PARAM_COUNTRY = "Country";
	public static final String PARAM_CONTACT_ID = "Contact ID";
	public static final String PARAM_DEVICE = "Device";

	//Add additional constants to server parameters
	public static final String MAIL_FROM ="from";
	public static final String MAIL_TO = "to";
	public static final String MAIL_SUBJECT = "subject";

	//private final  int END_OF_LINE = Character.LINE_SEPARATOR;
	private final  String  END_OF_LINE = "\n";
	private final  char DELIMITER = ':' ;

	private final String [] MAIL_FIELDS_LIST = {PARAM_USER_NAME ,PARAM_FIRST_NAME, PARAM_LAST_NAME ,PARAM_MOBILE, PARAM_ISSUE ,PARAM_COMMENTS,PARAM_COUNTRY,PARAM_CONTACT_ID, PARAM_EMAIL, PARAM_DEVICE};


	// Class logger
	public static final Logger logger = Logger.getLogger(SendContactEmail.class);

	// The Email template
	private static Template emailTemplate;

	// The server properties
	private static Hashtable serverProperties;

	// The specific email Properties
	private static Hashtable emailProperties;


	/**
	 *
	 * @param request - the request with all the parameters
	 * @throws Exception - if template not found
	 */
	public SendContactEmail(HashMap params) throws Exception {

		logger.debug("Trying to send email with the following params: " + params);

		// check to see if the variables init or not
		init(params);

		// set the specific email properties
		//Use constant instead of hard coded key and  put all code in the init method.
		/* emailProperties.put("to", CommonUtil.getProperty("contact.email"));

		emailProperties.put("body", getEmailBody(params));*/

	}

	/**
	 *
	 */
	private void init(HashMap params) throws Exception {

		// set the server properties
		serverProperties = new Hashtable();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable();
		//Put params from outside params
		//emailProperties.put("subject", CommonUtil.getProperty("contact.email.subject", null));
		emailProperties.put(MAIL_FROM, params.get(MAIL_FROM));
		emailProperties.put(MAIL_TO, params.get(MAIL_TO));
		emailProperties.put(MAIL_SUBJECT, params.get(MAIL_SUBJECT));
		emailProperties.put("body", getEmailBody(params));

	}

	public void run() {
		logger.debug("Sending Email ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");

	}

	public String getEmailBody(HashMap params) throws Exception {

		StringBuffer mailBody = new StringBuffer("");

		String paramValue , paramKey;
		//If we have params and not empty then we will populate the mail body
		if(null!= params && ! params.isEmpty())
		{
			for ( int i= 0 ; i < MAIL_FIELDS_LIST.length; i++)
			{
				paramKey = MAIL_FIELDS_LIST[i];

				paramValue = (String)params.get(paramKey);
				if(!CommonUtil.isParameterEmptyOrNull(paramValue))
				{
					mailBody.append(paramKey).
							append(DELIMITER).
							append(paramValue).
							append(END_OF_LINE);

				}
			}

		}


		return mailBody.toString();

		/*return "   userName: "+params.get(PARAM_USER_NAME)+"\n"+
			   "   firstName: "+params.get(PARAM_FIRST_NAME)+"\n"+
			   "   lastName: "+params.get(PARAM_LAST_NAME)+"\n"+
			   "   mobile: "+params.get(PARAM_MOBILE)+"\n"+
			   "   issue: "+params.get(PARAM_ISSUE)+"\n"+
			   "   comments: "+params.get(PARAM_COMMENTS)+"\n";*/
	}



}
