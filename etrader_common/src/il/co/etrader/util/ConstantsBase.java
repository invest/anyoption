package il.co.etrader.util;

public class ConstantsBase extends com.anyoption.common.util.ConstantsBase {
	public static final int WEB_CONST = 1;
	public static final int BACKEND_CONST = 2;

	public static final int GENERAL_NO = 1;
	public static final int GENERAL_YES = 2;

	public static final String OFFSET_GMT = "GMT+00:00";

	public static final String CALL_CENTER_IP = "82.166.29.181";

	public static final String LOGIN_ID = "loginId";
	
	public static final String BIND_CACHE_OBJECTS = "#{cacheObjects}";
	public static final String BIND_WRITER_WRAPPER = "#{writer}";
	public static final String BIND_USER = "#{user}";

	public static final String BIND_POPULATION_ENTRY = "#{populationEntry}";
	public static final String BIND_SESSION_USER = "user";
	public static final String BIND_SESSION_APPLICATIONDATA = "applicationData";
	public static final String BIND_USER_Form = "#{userForm}";
	public static final String BIND_INVESTMENTS_LIST = "#{investmentsList}";
	public static final String BIND_SESSION_WRITER_WRAPPER= "writer";
	public static final String BIND_SLIP_FORM_ONE_TOUCH = "#{slipForm1T}";

	public static final String BIND_APPLICATION_DATA = "#{applicationData}";
	public static final String BIND_VISIT = "#{visit}";
	public static final String BIND_MESSAGES_LIST = "#{messagesList}";
	public static final String BIND_EMAIL_AUTH = "#{emailAuthentication}";

    public static final String BIND_TRADING_PAGE_BEAN = "#{tradingPageBean}";
    public static final String BIND_CLEARING_INFO = "#{clearingInfo}";
    public static final String BIND_INATEC_INFO = "#{inatecInfo}";
    public static final String BIND_ENVOY_INFO = "#{envoyInfo}";
    public static final String BIND_DELTAPAY_INFO = "#{deltaPayInfo}";
    public static final String BIND_PAYPAL_INFO = "#{paypalInfo}";
    public static final String BIND_MONEYBOOKERS_INFO = "#{moneybookersInfo}";
    public static final String BIND_WEBMONEY_INFO = "#{webMoneyInfo}";
    public static final String BIND_TRANSACTION = "#{transaction}";
    public static final String BIND_USER_REGULATION = "#{userRegulation}";
    public static final String BIND_BAROPAY_INFO = "#{BaroPayInfo}";
    public static final String BIND_CDPAY_INFO = "#{cdpayInfo}";
    public static final String BIND_EZEEBILL_INFO = "#{ezeebillInfo}";    
    public static final String BIND_INATEC_IFRAME_INFO = "#{inatecIframeInfo}";
    public static final String BIND_EPG_INFO = "#{EPGInfo}";
	public static final String DEVICE_UNIQUE_ID = "duid";

	//public static final String ENUM_WITHDRAWAL_FEE_AMOUNT = "1000";
	//public static final String ENUM_FEES = "fees";
	public static final String ENUM_CREDIT_CARDS_NOT_VISIBLE_LIST = "cc_not_visible_list";
	//public static final String ENUM_DEFAULT_FEE = "default_fee";
	//public static final String ENUM_DEFAULT_FEE_CC = "default_fee_cc";
	//public static final String ENUM_MAX_AMOUNT_FOR_FEE = "maxamount_for_fee";
	public static final String ENUM_LOG_BALANCE = "log_balance";

    public static final Boolean ENUM_WITH_FEE = true;
	//public static final String ENUM_MARKETS = "markets";
	//public static final String ENUM_DEFAULT_GROUP_MARKET = "default_group_market";
    //public static final String ENUM_FREE_INVEST = "free_invest";
    //public static final String ENUM_FREE_INVEST_MIN = "free_invest_min";
    //public static final String ENUM_FREE_INVEST_MAX = "free_invest_max";

//	public static final String ERROR_MIN_DEPOSIT="error.min.deposit";
//	public static final String ERROR_MAX_DEPOSIT="error.max.deposit";
//	public static final String ERROR_MAX_DAILY_DEPOSIT="error.max.daily.deposit";
//	public static final String ERROR_MAX_MONTHLY_DEPOSIT="error.max.monthly.deposit";
//	public static final String ERROR_CARD_NOT_ALLOWED_WRONG_ID="error.card.not.allowed.wrong.id";
//	public static final String ERROR_CARD_NOT_ALLOWED_DUP_CARD="error.card.not.allowed.dup.card";
//	public static final String ERROR_CARD_NOT_ALLOWED="error.card.not.allowed";
//	public static final String ERROR_CARD_COUNTRY_BLOCKED="error.country.blocked";
//	public static final String ERROR_CARD_EXPIRED = "error.creditcard.expdate";
//	public static final String ERROR_CARD_FAILURE_REPEAT = "error.card.failure.repeat";
//
//	public static final String ERROR_CARD_IN_USE="error.card.already.inuse";
//	public static final String ERROR_BLACK_LIST="error.black.list";
//	public static final String ERROR_BIN_BLACK_LIST="error.bin.black.list";
//	public static final String ERROR_FAILED_CLEARING="error.card.clearing";
//	public static final String ERROR_CLEARING_TIMEOUT="error.creditcard.timeout";
//	public static final String ERROR_ILLEGAL_HOLDER_ID="error.creditcard.holderid.not.match.card";
//	public static final String ERROR_FAILED_CAPTURE="error.card.capture";
//	public static final String ERROR_BONUS_DEPOSIT="error.bonus.deposit";
//	public static final String ERROR_BONUS_WITHDRAW="error.bonus.withdraw";
//	public static final String ERROR_DEPOSIT_DOCS_REQUIRED="error.deposit.docs.required";
//	public static final String ERROR_DEPOSIT_DOCS_REQUIRED_MSG="error.deposit.docs.required.msg";
//	public static final String ERROR_PAYPAL_DEPOSIT_CANCELED="error.paypal.deposit.canceled";
//	public static final String ERROR_MONEYBOOKERS_DEPOSIT_CANCELED="error.moneybookers.deposit.canceled";
//	public static final String ERROR_BAROPAY_DEPOSIT_CANCELED="error.baropay.deposit.canceled";
//	public static final String ERROR_CDPAY_DEPOSIT_CANCELED="error.cdpay.deposit.canceled";

    public static final String PROVIDER_SIGN_DEFAULT = "SMC";

    public static final String XOR_PROVIDER = "xor";
    public static final String XOR_CONFIG_FILE_PATH = "xor.properties";
    public static final String XOR_TRANSACTION_DIRECTION_CREDIT = "Credit";
    public static final String XOR_TRANSACTION_DIRECTION_DEBIT = "Debit";

    public static final String ROLE_SUPPORT= "support";
    public static final String ROLE_ACCOUNTING= "accounting";
    public static final String ROLE_TRADER= "trader";
    public static final String ROLE_RETENTION= "retention";
    public static final String ROLE_RETENTIONM= "retentionM";
    public static final String ROLE_PARTNER= "partner";
	public static final String ROLE_ADMIN= "admin";
	public static final String ROLE_SADMIN= "sadmin";
	public static final String ROLE_CMS= "cms";
	public static final String ROLE_STRADER= "strader";
	public static final String ROLE_SSUPPORT= "ssupport";
	public static final String ROLE_ASTRADER= "astrader";  // above strader
	public static final String ROLE_MARKETING= "marketing";
	public static final String ROLE_TV= "tv";
	public static final String ROLE_BTRADER= "btrader";
	public static final String ROLE_CONTROL= "control";
	public static final String ROLE_PARTNER_MARKETING= "partner_mark";
	public static final String ROLE_PRODUCT = "product";
	public static final String ROLE_UPLOAD_LP = "upload_lp"; // upload landing pages
	public static final String ROLE_REGRANT_COPYOP_COINS = "copyop_coins";
	public static final String ROLE_COPYOP_DETAILS = "copyop_details";
	public static final String ROLE_ROUTING = "routing";
	public static final String ROLE_ANALYTICS_TV = "analyticsTv";
	public static final String ROLE_SALESM_EXT = "salesM_ext";
	public static final String ROLE_SALES_DEPOSIT = "sales_deposit";
	public static final String ROLE_LIMITATION_DEPOSIT = "limit_deposits";
	public static final String ROLE_MARKETING_BULK_ISSUE = "marketing_bulk_issue";

	public static final int LOG_COMMANDS_CANCEL_ALL_INV = 1;
	public static final int LOG_COMMANDS_UPDATE_OPP = 2;
	public static final int LOG_COMMANDS_CHANGE_OPP_ODS = 3;
	public static final int LOG_COMMANDS_UPDATE_MARKET = 4;
	public static final int LOG_COMMANDS_UPDATE_TA25 = 5;
	public static final int LOG_COMMANDS_MANUAL_SETTLE = 6;
	public static final int LOG_COMMANDS_ENABLE_OPP = 7;
	public static final int LOG_COMMANDS_DISABLE_OPP = 8;
	public static final int LOG_COMMANDS_UPDATE_SHIFT_PERC = 9;
	public static final int LOG_COMMANDS_UPDATE_SHIFT_PT = 10;
	public static final int LOG_COMMANDS_RESETTLE_OPP = 11;
	public static final int LOG_COMMANDS_DISABLE_MARKET = 12;
	public static final int LOG_COMMANDS_ENABLE_MARKET = 13;
    public static final int LOG_COMMANDS_MONITORING_DISABLE_OPP = 14;
    public static final int LOG_COMMANDS_MONITORING_ENABLE_OPP = 15;
    public static final int LOG_COMMANDS_UPDATE_FTSE = 16;
    public static final int LOG_COMMANDS_UPDATE_CAC = 17;
    public static final int LOG_COMMANDS_UPDATE_SP500 = 18;
    public static final int LOG_COMMANDS_UPDATE_KLSE = 19;
    public static final int LOG_COMMANDS_UPDATE_D4BANKS = 20;
    public static final int LOG_COMMANDS_UPDATE_BANKSBURSA = 21;
    public static final int LOG_COMMANDS_UPDATE_D4SPREAD = 22;
    public static final int LOG_COMMANDS_UPDATE_SHIFT_PERC_AO = 23;
    public static final int LOG_COMMANDS_SUSPEND_MARKET = 24;
    public static final int LOG_COMMANDS_UNSUSPEND_MARKET = 25;
    public static final int LOG_COMMANDS_CHANGE_MAX_MIN_INV = 26;
    public static final int LOG_COMMANDS_SUSPEND_OPP = 27;
    public static final int LOG_COMMANDS_CHANGE_EXPOSURE = 28;

	public static final long INVESTMENT_CLASS_STATUS_CANCELLED = 0;
	public static final long INVESTMENT_CLASS_STATUS_ACTIVE = 1;
	public static final long INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT = 2;

	public static final int MAX_TRANSACTION_COMMENTS = 80;
	public static final int CCPASS_ALLOWED_VALUE = 3;

	public static final int LINE_TYPE_REGULAR = 0;
	public static final int LINE_TYPE_COMMENT = 1;
	public static final int LINE_TYPE_PAGE_SUMMARY = 2;
	public static final int LINE_TYPE_GENERAL_SUMMARY = 3;

    public static final int NUMBER_OPPORTUNITIES_IN_BANNER = 5;

    public static final String ENUM_TAX_PERIOD = "period";
    public static final String ENUM_TAX_PERIOD_HALF_YEAR = "period_half_year";
    public static final String ENUM_TAX_PERIOD_FULL_YEAR = "period_full_year";

    public static final String CREDIT_CARD_ISRACARD_TYPE = "4";

    public static final String NAV_FIRST_DEPOSIT = "firstDeposit";
    public static final String NAV_DELTAPAY_DEPOSIT = "deltaPayDepositNav";

    public static int DEPARTMENT_SUPPORT = 2;
    public static int DEPARTMENT_RETENTION = 3;

    ///Add enums for one touch option validation

    public static final String ENUM_ONE_TOUCH = "one_touch";
    public static final String ENUM_ONE_TOUCH_USER_ASSET_MAX = "one_touch_max_asset";
    public static final String ENUM_ONE_TOUCH_USER_ASSET_MIN = "one_touch_min_asset";
    public static final String ENUM_ONE_TOUCH_ALL_USERS_MAX = "one_touch_max_users";

    //Default time for refresh ENUMS
    public static final String DEFAULT_ENUMS_REFRESH = "60000";

    public static final int ACCOUNTING_APPROVED_YES = 1;
    public static final int ACCOUNTING_APPROVED_NO = 0;

    public static final int ONE_TOUCH_IS_UP = 1;
    public static final int ONE_TOUCH_IS_DOWN = 0;

    //dropDowns
    public static final String INVESTMENT_CLASS_STATUS_DROPDOWN= "InvestmentsClassStatusDropdown";
	public static final String XOR_TRANSACTION_FORM_CLASS_TYPE_DROPDOWN= "XorTransactionFormClassTypeDropdown";

	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";

	public static final int WIN_LOSE_TOTAL_PERIOD = 1;
	public static final int WIN_LOSE_YEAR_PERIOD = 2;
	public static final int WIN_LOSE_HALF_YEAR_PERIOD = 3;

    public static final int ONE_TOUCH_OPPORTUNITIES_SELECT = 8;

    public static final String CURRENCY_BASE = "currency.usd";
    public static final long CURRENCY_BASE_ID = 2;

    // constants for campaigns
    public static final long ALL_CAMPAIGNS = 0;
    //public static final long ADS_MARKET_LEAD_FAST = 74;
    //public static final long ADS_MARKET_REGISTRATION_FAST = 75;
    //public static final long ADS_MARKET_LEAD_DREAM = 76;
    //public static final long ADS_MARKET_REGISTRATION_DREAM = 77;
  	public static final String CURRENCY_ALL = "currencies.all";
    public static final long CURRENCY_ALL_ID = 0;
//    public static final String CURRENCY_ILS = "currency.ils";
//    public static final String CURRENCY_SMS_ILS = "currency.sms.ils";
//    public static final long CURRENCY_ILS_ID = 1;
//    public static final String CURRENCY_USD = "currency.usd";
//    public static final String CURRENCY_SMS_USD = "currency.sms.usd";
//    public static final long CURRENCY_USD_ID = 2;
    public static final String CURRENCY_USD_CODE = "currency.usd.code";
    public static final String CURRENCY_USD_IBAN = "DE 94 5123 0800 0000 0501 44";
    public static final String CURRENCY_USD_ACCOUNT_NUMBER = "50144" ;
//    public static final String CURRENCY_EUR = "currency.eur";
//    public static final String CURRENCY_SMS_EUR = "currency.sms.eur";
//    public static final long CURRENCY_EUR_ID = 3;
    public static final String CURRENCY_EUR_IBAN = "DE 58 5123 0800 0000 0501 13";
    public static final String CURRENCY_EUR_ACCOUNT_NUMBER = "50113" ;
//    public static final String CURRENCY_GBP = "currency.gbp";
//    public static final String CURRENCY_SMS_GBP = "currency.sms.gbp";
    public static final String CURRENCY_GBP_IBAN = "DE67 5123 0800 0000 0501 45";
    public static final String CURRENCY_GBP_ACCOUNT_NUMBER = "50145" ;
//    public static final long CURRENCY_GBP_ID = 4;
//    public static final String CURRENCY_TRY = "currency.try";
//    public static final String CURRENCY_SMS_TRY = "currency.sms.try";
    public static final String CURRENCY_TRY_IBAN = "DE 42 5123 0800 0000 0502 07";
    public static final String CURRENCY_TRY_ACCOUNT_NUMBER = "50207" ;
//    public static final long CURRENCY_TRY_ID = 5;
//    public static final long CURRENCY_RUB_ID = 6;
//    public static final String CURRENCY_RUB = "currency.rub";
//    public static final String CURRENCY_SMS_RUB = "currency.sms.rub";
//    public static final long CURRENCY_CNY_ID = 7;
//    public static final String CURRENCY_CNY = "currency.cny";
//    public static final String CURRENCY_SMS_CNY = "currency.sms.cny";
//    public static final long CURRENCY_KRW_ID = 8;
//    public static final String CURRENCY_KRW = "currency.krw";
//    public static final String CURRENCY_SMS_KRW = "currency.sms.krw";
//    public static final long CURRENCY_SEK_ID = 9;
//    public static final String CURRENCY_SEK = "currency.sek";
//    public static final String CURRENCY_SMS_SEK = "currency.sms.sek";
//    public static final long CURRENCY_AUD_ID = 10;
//    public static final String CURRENCY_AUD = "currency.aud";
//    public static final String CURRENCY_SMS_AUD = "currency.sms.aud";
//    public static final long CURRENCY_ZAR_ID = 11;
//    public static final String CURRENCY_ZAR = "currency.zar";
//    public static final String CURRENCY_SMS_ZAR = "currency.sms.zar";
    //public static final long DSNR_LEAD_FAST = 79;
    ////public static final long DSNR_REGISTRATION_FAST=80;
    //public static final long DSNR_LEAD_DREAM = 81;
    //public static final long DSNR_REGISTRATION_DREAM=82;

    //public static final long WESELL_LEAD_FAST = 57;
    //public static final long WESELL_REGISTRATION_FAST= 58;
    //public static final long WESELL_LEAD_DREAM = 59;
    //public static final long WESELL_REGISTRATION_DREAM=60;

    public static final String CURRENT_SYSTEM = "system";
    public static final int XOR_ID_FOR_TEST = 999;

    public static final int UTCOFFSET_DEFAULT = -120;
    public static final String UTCOFFSET_DEFAULT_PERIOD1 = "GMT+03:00";
    public static final String UTCOFFSET_DEFAULT_PERIOD2 = "GMT+02:00";
    public static final String LIVE_SYSTEM = "live";
    public static final String TEST_SYSTEM = "test";
    public static final String LOCAL_SYSTEM = "local";

    public static final String DEPOSIT_DOCS_LIMIT1 = "warn_limit";
    public static final String DEPOSIT_DOCS_LIMIT2 = "lockAccount_trigger_limit";

    public static final String ALL_FILTER_KEY = "general.all";
    public static final String ALL_REPS_FILTER_KEY = "general.all.reps";
    public static final String NONE_FILTER_KEY = "general.none";
    public static final long ALL_FILTER_ID = 0;
    public static final long ALL_REPS_FILTER_ID = -1;
    public static final long ALL_CHAT_FILTER_ID = -1;
    public static final String ALL_PLATFORMS_FILTER_ID = "-1";
    public static final long NONE_FILTER_ID = -2;
    public static final int ALL_FILTER_ID_INT = 0;
    public static final long ISSUE_ID_DOCUMENT=12;

    public static final long ANYOPTION_CITY_ID = 0;

    public static final String XOR_URL="xor.url";
    public static final String XOR_TERMINAL_ID = "xor.terminalId";
    public static final String XOR_USERNAME = "xor.username";
    public static final String XOR_PASSWORD = "xor.password";
    public static final String XOR_SIGN = "xor.sign";
    public static final String XOR_IS_TEST_TERMINAL = "xor.isTestTerminal";


    //public static final long[] COUNTRY_ID_REGULATED = new long[]{15,22,33,53,55,56,57,67,72,73,80,83,95,96,101,102,112,117,118,119,127,145,156,166,167,171,185,186,192,200,219};




    public static final long MILISEC_IN_A_MONTH = 2678400000l;
    public static final long MOBILE_DIGITS_NUMBER_ON_ETRADER = 10;

    public static final long OFFSET_GAIN_BETWEEN_CREATED_AND_SETTLED = 1;

    public static final String COUNTRY_EMPTY_VALUE = "0";

    public static final String UTC_OFFSET = "utcOffset";

    public static final int MARKET_GROUP_CURRENCIES = 4;
    public static final int MARKET_GROUP_COMMODITIES = 5;

    public static final long MARKET_GROUP_INDICES_LONG = 2;
    public static final long MARKET_GROUP_STOCKS_LONG = 3;
    public static final long MARKET_GROUP_CURRENCIES_LONG = 4;
    public static final long MARKET_GROUP_COMMODITIES_LONG = 5;
    public static final long MARKET_GROUP_ABROAD_STOCKS_LONG = 6; //et only
    public static final long MARKET_GROUP_ETRADER_INDICES_LONG = 7;  //et only
    public static final long MARKET_GROUP_OPTION_PLUS_LONG = 8;
    public static final long MARKET_GROUP_BINARY_0_100_LONG = 9;

    public static final long USERS_MARKET_DIS_ACTIVE_ALL = 0;
    public static final long USERS_MARKET_DIS_ACTIVE = 1;
    public static final long USERS_MARKET_DIS_NOT_ACTIVE = 2;

    public static final long ETRADER_DEFAULT_LIMIT=1l;
    public static final long AO_DEFAULT_LIMIT=2l;

    public static final int AMEX_CARD_TYPE = 5;

    public static String UNIX_ERROR_STREAM = "runError: ";
    public static String UNIX_GEO_IP_LOOKUP_COMMAND = "geoiplookup ";
    public static String UNIX_GEO_IP_LOOKUP_RES_COUNTRY = ",";
    public static String UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY = "--";
    public static String UNIX_CHECKMX_COMMAND = "checkmx ";
    public static String UNIX_HOSTNAME_COMMAND = "hostname ";

    public static final String CAMPAIGN_UNKNOWN = "Unknown";
    public static final String COMBINATION_ID_DEFUALT = "1";

	public static final String NAME_TAG = "<name>";
	public static final String VALUE_TAG = "<value>";

	public static final String NAME_CLOSING_TAG = "</name>";
	public static final String VALUE_CLOSING_TAG = "</value>";

	public static final String NO_ID_NUM = "No-Id";
	public static final long MAESTRO_CARD_TYPE = 6;

	// deposit status parameters
	public static final int ALL_DEPOSIT = 0;
	public static final int NO_DEPOSIT = 1;
	public static final int FAILED_DEPOSIT = 2;
	public static final int SUCCESS_DEPOSIT = 3;
	public static final String NO_DEPOSIT_TXT = "No Deposit";
	public static final String FAILED_DEPOSIT_TXT = "Failed";
	public static final String SUCCESS_DEPOSIT_TXT = "Success";

	public static final long EXISTS = 1;
	public static final long NOT_EXISTS = 0;

	// time zone status parameters
	public static final int ALL_TIME_ZONE = 0;
	public static final String ALL_TIME_ZONE_POPULATION = "0";
	public static final String ALL_TIME_ZONE_NAME = "All";

	// last login status parameters
	public static final String ALL_LAST_LOGIN = "All";
	public static final String ALL_LAST_LOGIN_CHOISE = "0";

	// customer value status parameters
	public static final int ALL_CUSTOMER_VALUE = 0;
	public static final int CUSTOMER_VALUE_NEGATIVE = 1;
	public static final int CUSTOMER_VALUE_OPTION_2 = 2;
	public static final int CUSTOMER_VALUE_OPTION_3 = 3;
	public static final int CUSTOMER_VALUE_OPTION_4 = 4;
	// Population Entity
	public static final int POP_ENTRY_DAYS_BETWEEN_REMOVE_BY_ACTIONS_COUNT = 30;

	public static final int POP_ENTRY_TYPE_GENERAL = 1;
	public static final int POP_ENTRY_TYPE_TRACKING = 2;
	public static final int POP_ENTRY_TYPE_CALLBACK = 3;
	public static final int POP_ENTRY_TYPE_REVIEW = 4;
	public static final int POP_ENTRY_TYPE_REMOVE_FROM_SALES = 5;
	public static final int POP_ENTRY_TYPE_DELAY = 6;
	public static final int POP_ENTRY_TYPE_COLLECT_ALL = 0;

	public static final int POPULATION_ENTRY_SORT_BY_TIME_CREATED = 1;

	public static final int POPULATION_ENTRIES_LOCKED_BY_WRITER = 1;
	public static final int POPULATION_ENTRIES_ASSIGNED_TO_WRITER = 2;

	public static final int USER_CHANGE_DETAILS_PHONE_NUMBER = 1;
	public static final int USER_CHANGE_DETAILS_SKIN = 2;

	public static String POPULATION_SIGN = " (P)";

	public static String ACCOUNT_CLOSED = "population.entry.account.closed";
	public static String ACCOUNT_OPEN = "population.entry.account.open";

	public static final String INATEC_SHARED_SECRET = "Wcc5wzx4V3I8S8SZ";
	public static final String INATEC_NOTIFICATION_SECRET = "U1Ht6MHwWgN29aMu";
	public static final String INATEC_SHARED_SECRET_LIVE = "ncjTRN432OAZdsFW";
	public static final String INATEC_NOTIFICATION_SECRET_LIVE = "71cDK0G6Aej7yQEC";

	public static int ONE_TOUCH_AO_UNIT_PRICE = 50;
	public static int ONE_TOUCH_AO_RUB_UNIT_PRICE = 1000;
	public static int ONE_TOUCH_AO_CNY_UNIT_PRICE = 250;
    public static int ONE_TOUCH_TLV_UNIT_PRICE = 500;
	public static String ONE_TOUCH_MARKETS = "200,598"; // list of all markets that are not in skin_market_group_markets for one-Touch
    public static int ONE_TOUCH_ET_UNIT_PRICE = 200;
    public static int ONE_TOUCH_AO_TRY_UNIT_PRICE = 100;
    public static int ONE_TOUCH_AO_KR_UNIT_PRICE = 50000;
    public static int ONE_TOUCH_AO_SEK_UNIT_PRICE = 400;
    public static int ONE_TOUCH_AO_ZAR_UNIT_PRICE = 600;
    public static int ONE_TOUCH_AO_CZK_UNIT_PRICE = 1200;
    public static int ONE_TOUCH_AO_PLN_UNIT_PRICE = 200;

	public static final long BONUS_ROUND_TOP = 50000;



//    public static final long BONUS_TYPE_INSTANT_AMOUNT = 1;
//    public static final long BONUS_TYPE_INSTANT_NEXT_INVEST_ON_US = 2;
//    public static final long BONUS_TYPE_AMOUNT_AFTER_DEPOSIT = 3;
//    public static final long BONUS_TYPE_PERCENT_AFTER_DEPOSIT = 4;
//    public static final long BONUS_TYPE_AMOUNT_AFTER_WAGERING = 5;
//    public static final long BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING = 6;
//    public static final long BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS = 7;
//    public static final long BONUS_TYPE_CONVERT_POINTS_TO_CASH = 8;
//    public static final long BONUS_TYPE_INVESTMENT_INCREASED_RETURN_AND_REFUND = 9;
//    public static final long BONUS_TYPE_INVESTMENT_INCREASED_RETURN = 10;
//    public static final long BONUS_TYPE_INVESTMENT_INCREASED_REFUND = 11;
//    public static final long BONUS_TYPE_PERCENT_AFTER_DEPOSIT_AND_SUM_INVEST = 12;
//    public static final long BONUS_TYPE_ROUNDUP = 13;

    // user shouldn't get new bonus if got one in this period.
    public static final int BONUS_GRANT_LIMIT_PERIOD = 30;
    public static final int BONUS_EMAIL_BEFORE_EXP_DAYS = 3;

	public static final int BONUS_LIMIT_UPDATE_TYPE_SUM_DEPOSITS = 1;
	public static final int BONUS_LIMIT_UPDATE_TYPE_SUM_INVESTS = 2;
	public static final int BONUS_LIMIT_UPDATE_TYPE_ACTION_NUM = 3;
	public static final int BONUS_LIMIT_UPDATE_TYPE_BONUS_STEPS = 4;

    // Envoy
    public static final String ENVOY_NOTIFICATIONS_IP_1 = "213.129.74.111";
    public static final String ENVOY_NOTIFICATIONS_IP_2 = "93.91.29.197";
    public static final String ENVOY_NOTIFICATIONS_IPS = "149.5.102.";
    public static final int ENVOY_NOTIFICATIONS_IP_START = 68;
    public static final int ENVOY_NOTIFICATIONS_IP_END = 91;

    public static final String ENVOY_NOTIFY_SUCCESS = "SUCCESS";
	public static final String ENVOY_NOTIFY_ERROR = "ERROR";
	public static final String ENVOY_NOTIFY_CANCELLED = "CANCELLED";
	public static final String ENVOY_NOTIFY_OPEN = "OPEN";
	public static final String ENVOY_NOTIFY_FAILURE = "FAILURE";
	public static final String ENVOY_NOTIFY_EXPIRED = "EXPIRED";

	public static final int ENVOY_PAY_IN_NOTIFICATION_ID = 1;
	public static final int ENVOY_PAY_OUT_NOTIFICATION_ID = 2;
	public static final int ENVOY_PAY_OUT_REVERSAL_NOTIFICATION_ID = 3;
	public static final int ENVOY_PAY_IN_REVERSAL_NOTIFICATION_ID = 4;

	public static final String ENVOY_PAY_IN_NOTIFICATION = "PaymentNotification";
	public static final String ENVOY_PAY_OUT_NOTIFICATION = "PaymentOutNotification";
	public static final String ENVOY_PAY_OUT_REVERSAL_NOTIFICATION = "PaymentOutReversalNotification";
	public static final String ENVOY_PAY_IN_REVERSAL_NOTIFICATION = "PaymentInReversalNotification";

	public static final int USER_ID_REFERENCE_DIGITS_NUMBER = 7;
	public static final int USER_ID_REFERENCE_START_POSITION = 5;

	public static final String TRANSFER_TYPE_BANKING = "BANKIN";
	public static final String TRANSFER_TYPE_ONLINE = "EFTIN";

	// Loyalty
    //TODO: take this values from TiersManagerBase, add the warning action also to TiersManagerBase and to messages(Backend)..
	public static final int LOYALTY_JOB_ACTION_UPGRADE = 1;
	public static final int LOYALTY_JOB_ACTION_DOWNGRADE = 2;
	public static final int LOYALTY_JOB_ACTION_EXPIRED = 3;
	public static final int LOYALTY_JOB_ACTION_WARNING = 4;
	public static final int LOYALTY_JOB_ACTION_REQUALIFICATION = 5;

	public static final long PIXEL_TYPE_FIRST_DEPOSIT = 1;
	public static final long PIXEL_TYPE_REGISTRATION = 2;
	public static final long PIXEL_TYPE_CONTACTME = 3;
	public static final long PIXEL_TYPE_HOMEPAGE = 4;
	public static final long PIXEL_TYPE_REG_LANDINGPAGE = 5;

	// Calcalist
	public static final String CALCALIST_GAME = "cGame";
	public static final long CAL_GAME_VIRTUAL_MONEY = 100000;
	public static final int CAL_DUPLICATE_REAL_TO_GAME = 1;
	public static final int CAL_DUPLICATE_GAME_TO_REAL = 2;
	public static final int RANKING_TABLE_ROW_NUM_SHORT = 5;
	public static final int RANKING_TABLE_SHORT_LENGTH = 11;
	public static final int RANKING_TABLE_ROW_NUM_LONG = 15;
	public static final int RANKING_TABLE_LONG_LENGTH = 17;
	public static final int WINNERS_NUM = 3;
	public static final int GAME_LENGTH_WEEKS = 4;

	// Affiliates Data
	public static final String AFFILIATE_PASSWORD_JOTTIX = "gH5a2";
	public static final String AFFILIATE_PASSWORD_JOTTIX_US = "qasw21";
	public static final long AFFILIATE_RECIPIENT_ID_JOTTIX = 65;
	public static final long AFFILIATE_RECIPIENT_ID_JOTTIX_US = 82;

	public static final String AFFILIATE_GROUP_STRING_DATE = "Date";
	public static final String AFFILIATE_GROUP_STRING_DP = "DP";

	public static final int AFFILIATE_NO_GROUP = 0;
	public static final int AFFILIATE_GROUP_BY_DATE = 1;
	public static final int AFFILIATE_GROUP_BY_DP = 2;

	public static final String AFFILIATE_NONE= "-1";

	public static long CALCALIST_COMBINATION_ID = 829;

	public static final String ENUM_SMS_INVEST_MIN = "sms_invest_min";
	public static final String ENUM_SMS_INVEST_MIN_CODE_PREFFIX = "sms.invest.currency.";

	public static final int SMS_ENGLISH_CHAR_LIMIT = 160;
	public static final int SMS_OTHER_CHAR_LIMIT = 70;

	public static final int ANYOPTION_URL = 2;
	public static final int MIOPCIONES_URL = 3;
	public static final int TLVTRADE_URL = 4;

	public static final String SERVER_URLS_MIOPCIONES = "serverurls6";

	public static final long BONUS_INSTANT_DYNAMIC_AMOUNT = 100;

	public static final int TRAN_ISSUES_INSERT_ONLINE = 1;
	public static final int TRAN_ISSUES_INSERT_MANUAL = 2;

	public static final String COUNTRIES_NONE= "-1";

	public static final long NETREFER_CAMPAIGN_ID = 365;
	public static final long REFERPARTNER_CAMPAIGN_ID = 1535;

	// Risk alert
	public static final long INVESTMENT_LIMITS_GROUP_ID = 1;
	public static final String RISK_ALERT_ENUMERATOR  = "risk_alert";
	public static final String ET_DEPOSIT_LIMIT_NO_CODE_  = "et_deposit_limit_no_doc";
	public static final String AO_DEPOSIT_LIMIT_NO_DOC_CODE  = "ao_deposit_limit_no_doc";
	public static final String X_DEPOSIT_NO_DOCUMNET_CODE  = "x_deposit_no_doc_num";

	public static final String DEPOSIT_LIMIT_NO_DOC_CODE = "deposit_limit_no_doc_";

	public static final long PAYMENT_METHOD_TYPE_DEPOSIT = 1;
	public static final long PAYMENT_METHOD_TYPE_WITHDRAW = 2;

	public static final int ISRAEL_UTCOFFSET_PERIOD2 = 2;
	public static final int ISRAEL_UTCOFFSET_PERIOD1 = 3;

	public static final String NETREFER_DELIMITER = "_";
	public static final String REFERPARTNER_DELIMITER = "_";
	public static final String REFERPARTNER_PREFIX = "RPOA";
	public static final String SUB_AFF_KEY_PARAM = "affid";

	//Issue actions screen types
	public static final int SCREEN_SUPPORT = 1;
	public static final int SCREEN_SALE = 2;
	public static final int SCREEN_SALEM = 6;
	public static final int SCREEN_ACCOUNTING = 7;
	public static final int SCREEN_SUPPORTMO = 11;
	public static final int SCREEN_ADMIN = 12;
	public static final int SCREEN_MARKETING = 13;

	// Markets Ids
	public static final long MARKET_ID_USD_ILS = 15;
	public static final long MARKET_ID_EUR_GBP = 291;
	public static final long MARKET_ID_EUR_JPY = 287;
	public static final long MARKET_ID_USD_CAD = 288;
	public static final long MARKET_ID_USD_CHF = 352;
	public static final long MARKET_ID_NZD_USD = 290;

	public static final long MARKET_ID_ABU_DHABI = 250;
	public static final long MARKET_ID_AIR_CHINA_LIMITEDI = 616;
	public static final long MARKET_ID_AFRICA = 13;
	public static final long MARKET_ID_AMERICA_MOVIL = 569;
	public static final long MARKET_ID_AMERICA_MOVIL_UP = 305;
	public static final long MARKET_ID_BRITISH_AIRWAYS = 135;
	public static final long MARKET_ID_CMB = 612;
	public static final long MARKET_ID_COCA_COLA = 107;
	public static final long MARKET_ID_CONTROLADORA = 306;
	public static final long MARKET_ID_FREDDIE_MAC = 227;
	public static final long MARKET_ID_GENERAL_ELECTRIC = 127;
    public static final long MARKET_ID_HSBC = 134;
    public static final long MARKET_ID_IBERDROLA = 563;
    public static final long MARKET_ID_INDITEX = 301;
    public static final long MARKET_ID_ISE30 = 395;
    public static final long MARKET_ID_ISE100 = 293;
    public static final long MARKET_ID_ISE30FUT = 437;
    public static final long MARKET_ID_ISE_FINANCIAL = 539;
    public static final long MARKET_ID_ISE_INDUSTRIAL = 540;
    public static final long MARKET_ID_JOHNSON_JOHNSON = 519;
    public static final long MARKET_ID_KLSE = 432;
    public static final long MARKET_ID_KWAIT_SE_KSE = 249;
    public static final long MARKET_ID_MAOF_INDEX = 436;
    public static final long MARKET_ID_MARKS_SPENSER = 208;
    public static final long MARKET_ID_NASDAQ100_INDEX = 200;
    public static final long MARKET_ID_NIKE = 441;
    public static final long MARKET_ID_PETROCHINA = 187;
    public static final long MARKET_ID_PETROLEO_BRASILEIRO = 572;
    public static final long MARKET_ID_RBS = 433;
    public static final long MARKET_ID_REPSOL_YPF = 545;
    public static final long MARKET_ID_TADAWUL = 248;
    public static final long MARKET_ID_TEL_REALSTATE = 22;
    public static final long MARKET_ID_TELMEX = 307;
    public static final long MARKET_ID_VOLKSWAGEN = 392;
    public static final long MARKET_ID_WAL_MART = 564;
    public static final long MARKET_ID_WAL_MART_MX = 330;
    public static final long MARKET_ID_ZYNGA = 624;

	// Liveperson chat types
	public static final long LIVEPERSON_CHAT_TYPE_DEFAULT = 1;
	public static final long LIVEPERSON_CHAT_TYPE_INVITATION = 2;
	public static final long LIVEPERSON_CHAT_TYPE_INVITATION_FAILED_DEPOSIT = 3;
	public static final long LIVEPERSON_CHAT_TYPE_INVITATION_LEAVE_OPEN_ACCOUNT = 4;
	public static final long LIVEPERSON_CHAT_TYPE_INVITATION_LEAVE_DEPOSIT = 5;
	//public static final String LIVEPERSON_INVITATION_VALUE = "Invitation";
	//public static final String LIVEPERSON_INVITATION_DEPOSIT_FAILED_VALUE = "Invitation2";

	// Cancel Withdrawal email
	public static final int CANCEL_WITHDRAWAL_DAYS_FOR_EMAIL_NOTIFICATION = 23;
    //public static final int CANCEL_WITHDRAWAL_DAYS_FOR_EMAIL_NOTIFICATION_JOB = 83;
	public static final int CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL = 7;
	public static final String TEMPLATE_CANCEL_WITHDRAW = "withdrawalNotification.html";

	public static final String USER_AGENT = "userAgent";

	public static final int TEMPLATE_5DEPOSITS = 34;

	public static final String AUTH_KEY = "authKey";
	public static final long ACTIVATION_EMAIL_MAX_SENT_TIMES = 3;
	public static final long ACTIVATION_EMAIL_DEPO_MAX_REFUSED_TIMES = 3;

	//risk credit card verification_call status
	public static final int VERIFICATION_CALL_STATUS_REQUEST  = 2;

	//issue action channels
	public static final String ISSUE_ACTION_CHANNEL_ACTIVITY = "7";

	public static int RISK_ACTIONS_NUM = 4;

	public static final String RESET_KEY = "resetKey";

	public static final String ENUM_ET_MC_START_DATE = "etrader_mc_start_date";

	public static int CC_VISIBLE  = 1;
	public static int CC_NOT_VISIBLE  = 0;

	public static int CC_ALLOWED  = 1;
	public static int CC_NOT_ALLOWED  = 0;

	//Issue type
	public static int ISSUE_TYPE_REGULAR  = 1;
	public static int ISSUE_TYPE_RISK  = 2;

	//issues time Filter
	public static String ISSUES_TIME_CREATED = "0";
	public static String ISSUES_TIME_UPDATED = "1";

	//user file type
	public static final int FILE_TYPE_ID_COPY = 1;
	public static final int FILE_TYPE_CC = 2;

	//request type contact user
	public static final int RISK_REQUEST_CONTACT_USER = 3;

	public static final String SALES_DEPOSIT_TYPE_RETENTION_STR = "(Retention)";
	public static final String SALES_DEPOSIT_TYPE_CONVERSION_STR = "(Conversion)";

	// Priorities - currently for campaign
	public static final int PRIORITY_LOW = 1;
	public static final int PRIORITY_MED = 2;
	public static final int PRIORITY_HIGH = 3;

	//Deposit Email over threshold to traders
    public static final int THRESHOLD_DEPOSIT = 3000;

    //Bank wire
    public static final int BANK_TYPE_DEPOSIT  = 1;
    public static final int BANK_TYPE_WITHDRAWAL  = 2;

	public static final long REPORT_EMPTY_VALUE = -1;

    public static final int TEMPLATE_FIRST_WITHDRAWAL = 33;
	public static final int TEMPLATE_JUST_CONFIRMATION = 38;
	public static final int TEMPLATE_JUST_CONFIRMATION_OTHER = 39;
	public static final int TEMPLATE_NON_CC_WITHDRAWAL = 40;
	public static final int TEMPLATE_CUSTOMER_SERVICE = 42;
	public static final long TEMPLATE_WELCOME_GOOGLE_ID = 55;
	public static final long TEMPLATE_WELCOME_MAIL_ID = 1;
	public static final int SALES_DEPOSIT_TYPE_CONVERSION = 1;
	public static final int SALES_DEPOSIT_TYPE_RETENTION = 2;
	public static final int SALES_DEPOSIT_TYPE_BOTH = 3;
	public static final int TEMPLATE_BEFORE_DEPOSIT = 67;
	public static final int TEMPLATE_AFTER_DEPOSIT = 66;
	public static long TEMPLATE_ACTIVATION_MAIL_LOW_SCORE = 68;
	public static long TEMPLATE_ACTIVATION_MAIL_TRESHOLD = 69;
	public static long TEMPLATE_ACTIVATION_MAIL_RESTRICTED= 70;
	public static long TEMPLATE_DOCUMENT_SCENARIO= 71;
	public static long TEMPLATE_DOCUMENT_SCENARIO_TIME_PASSED= 72;
	public static long TEMPLATE_DOCS_UPDATE= 74;
	public static final long TEMPLATE_AO_ACTIVATION_MAIL_TRESHOLD = 76;
	public static final long TEMPLATE_BLANK = 77;
	public static final long TEMPLATE_DOCUMENT_FTD = 56;
	public static final long TEMPLATE_DOCUMENT_FTD_12 = 12;

	public static final String SPECIAL_CARE_MARK = "*";

	//ONLY FOR THE INSERT INV AND NOT FOR ET. TONY NEED TO CHANGE FLASH TO FIX IT THERE
	public static final long OPTION_PLUS_FEE_RUB = 1000;
	public static final long OPTION_PLUS_FEE_OTHER = 50;
	public static final long OPTION_PLUS_FEE_YUAN = 100;
	public static final long OPTION_PLUS_FEE_KOREAN = 50000;

	//
	public static final long BANK_TYPE_ID_MONDIAL = 20;

	public static final long MAILBOX_EMAIL_TYPE_CUSTOMER_SERVICE = 30;

	public static final String TAX_PERCENTAGE_BEFORE_2012 = "20%";
	public static final String TAX_PERCENTAGE_AFTER_2012 = "25%";

    public static final int MARKET_FACEBOOK_ID = 620;
    public static final int MARKET_FACEBOOK_PLUS_ID = 623;
    public static final int MARKET_BITCOIN_ID = 638;
    public static final int MARKET_ALIBABA = 649;
    public static final int MARKET_CANDY_CRUSH = 650;
    public static final int MARKET_BITCOIN_ONE_TOUCH_ID = 677;
    public static final int MARKET_ALIBABA_ONE_TOUCH_ID = 678;
    public static final int MARKET_EURUSD_SPECIAL = 693;
    public static final int MARKET_OIL_SPECIAL = 709;
    public static final int MARKET_GOLD_SPECIAL = 710;
    public static final int MARKET_DAX_SPECIAL = 711;
    public static final int MARKET_SP500_SPECIAL = 712;

    public static final long POPULATION_TYPE_ID_NO_2_DEPOSIT = 14;
    public static final long POPULATION_TYPE_ID_NO_3_DEPOSIT = 15;
    public static final long POPULATION_TYPE_ID_NO_4_DEPOSIT = 16;
    public static final long POPULATION_TYPE_ID_NO_5_DEPOSIT = 17;
    public static final long POPULATION_TYPE_ID_NO_6_DEPOSIT = 18;
    public static final long POPULATION_TYPE_ID_NO_7_DEPOSIT = 19;

//    public static final int MAX_SIZE_SENDER_NAME = 11;

    public static final long FAILED_LOGIN_COUNT_LIMIT = 4;

    public static final long ISSUE_ACTION_REQUEST_CREDITCARD_CHANGE = 42;
    public static final long ISSUE_ACTION_REQUEST_DOCUMENTS = 41;
    public static final long ISSUE_ACTION_REQUEST_CONTACT_USER = 40;
    public static final String ISSUE_ACTION_TYPE_WRONG_NUMBER = "30";
    public static final long GADI_USER_ID = 264;
    public static final long MARCELO_USER_ID = 237;
    public static final long AYAN_USER_ID = 388;
    public static final long ANNAS_USER_ID = 13;
    public static final long REUT_USER_ID = 16;
    public static final long KEMAL_USER_ID = 277;
    public static final long ELDAN_USER_ID = 290;
    public static final long ADI_KOREN_USER_ID = 312;
    public static final long DANYD_USER_ID = 43;
    public static final long NAAMA_USER_ID = 592;
    public static final long PANAGIOTAC_USER_ID = 827;
    public static final long YUSUFH_USER_ID = 896;
    public static final long FANIS_USER_ID = 728;
    public static final long IDOA_USER_ID = 955;
    public static final long VANESSAF_USER_ID = 1082;
    public static final long YAELJ_USER_ID = 282;
    public static final long RONIN_USER_ID = 507;
    public static final long QABG_USER_ID = 957;
    public static final long MARYVS_USER_ID = 1444;
    public static final long OANAD_USER_ID = 1428;
    public static final long OFIRS_USER_ID = 800;
    public static final long WRITER_ANGELAD = 898;
    public static final long WRITER_FORTUNT = 858;
    public static final long WRITER_SHLOMITM = 799;
    public static final long WRITER_ROMANL = 1104;
    public static final long WRITER_ANNAMU1 = 1414;
    public static final long WRITER_PETARK = 2209;
    public static final long WRITER_EVGENIAD = 2013;


    public static final String LAST_LOGIN_BETWEEN = "1";
    public static final String LAST_LOGIN_BIGGER = "2";
    public static final int THOUSAND = 1000;
    public static final String PROMOTION_TOTAL_AND_INSERTED_USERS = "promotionsTotalAndInsertedUsers";
    public static final String PROMOTION_TOTAL_SENDED_TO_USERS = "promotionsSendedToClients";
    public static final String PROMOTION_TOTAL_INSERTED_TO_TABLE = "totalPromotionInserted";
    public static final int PROMOTION_STATUS_ID_SENDED = 2;

    public static final int SHIFTING_TYPE_BOTH 		= 0;
    public static final int SHIFTING_TYPE_ET 		= 1;
    public static final int SHIFTING_TYPE_AO 		= 2;

    public static final long SALES_DEPARTMENT_ID 	= 3;

    public static final int NOT_PUBLISHED = 0;

    public static String REROUTING_PRIORITY_CONFIG_KEY = "transaction_reroute_provider_priority_";
    public static int DAYS_NOT_TO_REROUTE = -7;

    public static final String ALL_REP = "0";

    public static final long REGULAR_SETTLEMNT = -1;

    public static final long LIVE_AO_TOP_TRADES_LAST_DAY = 1;
    public static final long LIVE_AO_TOP_TRADES_LAST_WEEK = 7;
    public static final long LIVE_AO_TOP_TRADES_LAST_MONTH = 30;
    public static final int PRODUCT_TYPE_ONE_TOUCH = 2;

	public static final String ANYOPTION_ITUNES_IPHONE_APP = "http://www.anyoption.com/appsflyerios";
	public static final String START_ANYOPTION_IPHONE_APP = "anyoption://";
	public static final String ANYOPTION_PLAY_STORE_ANDROID_APP = "http://www.anyoption.com/appsflyerandroid";

	public static final String NAV_CDPAY_DEPOSIT = "cdpayDepositNav";

	public static final String BARO_PAY_SENDER_NAME = "Anyoption";
	public static final String BARO_PAY_RESULT_SUCCESS="0";
	public static final String BARO_PAY_RESULT_APPROVE="0";

	//Doc request type
    public static final String RISK_DOC_REQ_ALL = "0";
    public static final String RISK_DOC_REQ_ID = "1";
    public static final String RISK_DOC_REQ_CC = "2";
    public static final String RISK_DOC_REQ_CONTACT_USER = "3";
    public static final String RISK_DOC_REQ_REGULATION = "4";

    public static final long RISK_ISSUE_STATUS_NEW = 4;
    public static final String RISK_ISSUE_STATUS_NEW_STRING = "4";

    //remarketing
    public static final String FROM_REMARKETING = "remarketing";
    public static final String REMARKETING_COMBINATION_ID = "r_combid";
	public static final String REMARKETING_DYNAMIC_PARAM = "r_dp";
	public static final String REMARKETING_REGISTER = "remarketingRegMade";

	public static final String CONTEXT_CC_ID_ATTRIBUTE = "ccId";
	public static final String CONTEXT_CC_ID_ATTRIBUTE_AUTO = "autoCCId";
	public static final String ISSUE_CHANNEL_ACTIVITY = "7";
	public static final String ISSUE_ACTION_PENDING_DOC = "44";

	public static final long CONTROL_COUNTING_ZERO = 0;

	public static final long COUNT_ONE_DEPOSIT = 1;
	public static final long COUNT_NO_DEPOSIT = 0;

	//top trades
	public static final int TOP_TRADES_TYPE_LAST_DAY = 1;
	public static final int TOP_TRADES_TYPE_LAST_WEEK = 2;
	public static final int TOP_TRADES_TYPE_LAST_MONTH = 3;
	public static final int LIVE_TOP_TRADES_TODAY = 0;

	public static final long ACTION_TYPE_FAILED_LOGIN = 39;
	
	// Customer Report Type
	public static final int CUSTOMER_REPORT_ALL = 0;
	public static final int CUSTOMER_REPORT_FORTNIGHTLY = 1;
	public static final int CUSTOMER_REPORT_MONTHLY = 3;
	
	public static final int ISRAELI_ID_NUMBER_ALLOWED_LENGTH = 9;
	
    public static final String SESSION_JUST_LOGGED_IN = "justLoggedIn";
    
    public static final int ISSUE_USERS_FILE_LIMIT = 500;
    
    public static final long STATE_CHARGEBACK = 2;

}
