package il.co.etrader.loyalty;

public class LoyaltyUser {

	private long userId;
	private String userName;
	private long previousTierId;
	private long newTierId;
	private long pointsForAction;
	private String userOffset;


	/**
	 * @return the pointsForAction
	 */
	public long getPointsForAction() {
		return pointsForAction;
	}
	/**
	 * @param pointsForAction the pointsForAction to set
	 */
	public void setPointsForAction(long pointsForAction) {
		this.pointsForAction = pointsForAction;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the newTierId
	 */
	public long getNewTierId() {
		return newTierId;
	}
	/**
	 * @param newTierId the newTierId to set
	 */
	public void setNewTierId(long newTierId) {
		this.newTierId = newTierId;
	}
	/**
	 * @return the previousTierId
	 */
	public long getPreviousTierId() {
		return previousTierId;
	}
	/**
	 * @param previousTierId the previousTierId to set
	 */
	public void setPreviousTierId(long previousTierId) {
		this.previousTierId = previousTierId;
	}
	public String getUserOffset() {
		return userOffset;
	}
	public void setUserOffset(String userOffset) {
		this.userOffset = userOffset;
	}

}
