package il.co.etrader.loyalty;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.TiersDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.daos.CurrenciesDAOBase;


/**
 * Loyalty Job
 * This job runs through all the users and upgrade,downgrade,warn or expired them by loyalty rules.
 * for the upgraded users it will add welcome bonus and send welcome mail to the new tier.
 * It will also send reports about the users that were handled.
 *
 * @author Eliran
 */
public class LoyaltyJob extends JobUtil{
    private static Logger log = Logger.getLogger(LoyaltyJob.class);

    private static StringBuffer UsersReport;

    private static ArrayList<LoyaltyUser> upgradedUsers;
    private static ArrayList<LoyaltyUser> downgradedUsers;
    private static ArrayList<LoyaltyUser> warnedUsers;
    private static ArrayList<LoyaltyUser> expiredUsers;
    private static ArrayList<LoyaltyUser> requalifiededUsers;

    private static HashMap<Long, Tier> tiers;
    private static HashMap<Long, Template> templates;

    /**
     * loyalty check Job.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the loyalty job.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to retrieve users for loyalty handling.");
        }
        retrieveUsersToHandleLoyalty();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Finished retrieve users..");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Start handling users which were upgraded.");
        }

        handleUpgradedUsers();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finish handling users which were upgraded.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Start handling users which were downgraded.");
        }

        handleDowngradedUsers();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finish handling users which were downgraded.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Start handling users which were requalified.");
        }

        handleRequalifiededUsers();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finish handling users which were requalified.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Start handling users which were warned.");
        }

        handleWarnedUsers();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finish handling users which were warned.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Start handling users which were expired.");
        }

        handleExpiredUsers();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finish handling users which were expired.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Loyalty job complete");
        }

   }

    /**
     * Retrieve all users that need to be handled.
     *
     * @return list of users
     * @throws SQLException
     */
    private static void retrieveUsersToHandleLoyalty() throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        LoyaltyUser lUser;

        upgradedUsers = new ArrayList<LoyaltyUser>();
        downgradedUsers = new ArrayList<LoyaltyUser>();
        warnedUsers = new ArrayList<LoyaltyUser>();
        expiredUsers = new ArrayList<LoyaltyUser>();
        requalifiededUsers = new ArrayList<LoyaltyUser>();

        try {
            con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");

            tiers = TiersDAOBase.getTiers(con);
			templates = TemplatesDAO.getAllHashMap(con);

		    String sql = "select * " +
		    			 "from TABLE(GET_LOYALTY_USERS()) ";


//		    String sql = "select u.id user_id, u.user_name , 1 previous_tier_id, 2 new_tier_id, 100 action_points,'GMT+03:00' user_offset, 1 loyalty_action  from users u where u.id between 6985 and 7000 "+
//						"UNION  "+
//						"select u.id user_id, u.user_name , 2 previous_tier_id, 3 new_tier_id, 200 action_points,'GMT+02:00' user_offset, 1 loyalty_action  from users u where u.id between 6960 and 6974 "+
//						"UNION "+
//						"select u.id user_id, u.user_name , 3 previous_tier_id, 2 new_tier_id, 0 action_points,'GMT+00:00' user_offset,2 loyalty_action  from users u where u.id between 6940 and 6959 "+
//						"UNION "+
//						"select u.id user_id, u.user_name , 1 previous_tier_id, 1 new_tier_id, -100 action_points,'GMT+03:00' user_offset, 3 loyalty_action  from users u where u.id between 6910 and 6939 "+
//						"UNION "+
//						"select u.id user_id, u.user_name , 1 previous_tier_id, 1 new_tier_id, 0 action_points,'GMT+01:00' user_offset,4 loyalty_action  from users u where u.id between 6890 and 6909 ";

		    ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			while (rs.next()) {
				lUser = new LoyaltyUser();

				lUser.setUserId(rs.getLong("user_id"));
				lUser.setUserName(rs.getString("user_name"));
				lUser.setPreviousTierId(rs.getLong("previous_tier_id"));
				lUser.setNewTierId(rs.getLong("new_tier_id"));
				lUser.setPointsForAction(rs.getLong("points_for_job_action"));
				lUser.setUserOffset(rs.getString("user_offset"));

				int loyaltyJobAction = rs.getInt("loyalty_job_action");

				switch (loyaltyJobAction){
					case ConstantsBase.LOYALTY_JOB_ACTION_UPGRADE:
						upgradedUsers.add(lUser);
						break;
					case ConstantsBase.LOYALTY_JOB_ACTION_DOWNGRADE:
						downgradedUsers.add(lUser);
						break;
					case ConstantsBase.LOYALTY_JOB_ACTION_WARNING:
						warnedUsers.add(lUser);
						break;
					case ConstantsBase.LOYALTY_JOB_ACTION_EXPIRED:
						expiredUsers.add(lUser);
						break;
					case ConstantsBase.LOYALTY_JOB_ACTION_REQUALIFICATION:
						requalifiededUsers.add(lUser);
						break;
					default:
						log.error("Error: loyalty Job Action " + loyaltyJobAction + " Isn't recognized, for user Id " + lUser.getUserId());
						throw new Exception();
				}

			}

         } catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving loyalty users from Db.", e);
         } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
          }

    }

    /**
     * handle upgraded users
     *
     * @param
     */
    public static void handleUpgradedUsers() {
      UsersReport = new StringBuffer();
      Template welcomeTemplate;
      Connection con = null;
      BonusUsers bonusUser;
      boolean isError;

      UsersReport.append("<br><b>Upgraded Users:</b><table border=\"1\">");
      UsersReport.append("<tr><td><b>UserId</b></td><td><b>UserName</b></td><td><b>PrivouesTier</b></td>" +
      		"<td><b>NewTier</b></td></tr>");

      try {
    	  con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");
    	  con.setAutoCommit(false);

	      for (int index=0; index <upgradedUsers.size(); ++index){
	    	  isError = false;
	    	  bonusUser = null;
	    	  LoyaltyUser lUser = upgradedUsers.get(index);
	    	  Tier PreviousTier = tiers.get(lUser.getPreviousTierId());
	    	  Tier NewTier = tiers.get(lUser.getNewTierId());

	    	  try{
	    		  try {
		    		  // update the user tier
			    	  updateUserTier(con,lUser);
	    		  } catch (SQLException e) {
	    			  log.error("Error: updateUserTier for uesr " + lUser.getUserId() + " Failed.", e);
	    			  throw(e);
	    		  }

		    	  UserBase user = new UserBase();
		    	  try {
		    		  UsersDAOBase.getByUserId(con, lUser.getUserId(), user,true);
		     	  } catch (SQLException e) {
		    		  log.error("Error: getUserById " + lUser.getUserId() + " Failed.");
		    		  throw(e);
		    	  }

		     	  if (null != user){
		     		 bonusUser = new BonusUsers();
		     		 long bonusId = NewTier.getWelcomeBonusId();

		     		 if (0 != bonusId){
		     			 bonusUser.setBonusId(bonusId);
						 bonusUser.setUserId(user.getId());
						 bonusUser.setWriterId(Writer.WRITER_ID_AUTO);

			       		 try {
			       			Currency userCurrency = CurrenciesDAOBase.getById(con, user.getCurrencyId().intValue());
			       			user.setCurrency(userCurrency);
			       			bonusUser.setCurrency(userCurrency);

			       			BonusManagerBase.insertBonusUser(bonusUser, user, Writer.WRITER_ID_AUTO, con, 0, 0, false);
			       		 } catch (SQLException e1) {
			       			log.error("Error: Granting bonus to user " + lUser.getUserId() + " Failed.");
			       			throw(e1);
			       		 }
		     		 }

		       		 if (null != bonusUser){
		       			welcomeTemplate = templates.get(NewTier.getWelcomeTemplateId());

			       		 try {
			       			if (getPropertyByFile("loyalty.env").equals("TEST")){
			       				user.setEmail(getPropertyByFile("loyalty.tester.email"));
			       			}

			       			 UsersManagerBase.sendJobMailTemplateFile(welcomeTemplate, Writer.WRITER_ID_AUTO, user, bonusUser,con,properties);
			       		 } catch (Exception e) {
			       			 log.error("Error: sendLoyaltyMailTemplateFile to user " + lUser.getUserId() + " Failed.");
			       			 throw(e);
			       		 }
		       		 }
		    	  }
	    	  } catch (Exception e) {
	    		  isError = true;
	    		  log.error("Error: Upgrade user " + lUser.getUserId() + " Failed.");
	    	  }

	    	  if (!isError){
	    		  con.commit();

		     	  // Enter user to report.
		    	  UsersReport.append("<tr>");
		    	  UsersReport.append("<td>").append(lUser.getUserId()).append("</td>");
		    	  UsersReport.append("<td>").append(lUser.getUserName()).append("</td>");
		    	  UsersReport.append("<td>").append(PreviousTier.getDescription()).append("</td>");
		    	  UsersReport.append("<td>").append(NewTier.getDescription()).append("</td>");
		    	  UsersReport.append("</tr>");
	    	  }else{
	    		  con.rollback();
	    	  }
	      }
      } catch (Exception e) {
  		log.log(Level.ERROR, "Error while Retrieving loyalty users from Db.", e);
	   } finally {
	      	try {
	      		con.setAutoCommit(true);
	      		con.close();
	      	} catch (Exception e) {
	          log.log(Level.ERROR, "Can't close connection", e);
	      	}
	   }

	   sendReportEmail();

    }

    /**
     * handle upgraded users
     *
     * @param
     * @throws SQLException
     */
    public static void handleDowngradedUsers() throws SQLException {
    	boolean isUpdated;
    	UsersReport = new StringBuffer();

    	UsersReport.append("<br><b>Downgraded Users:</b><table border=\"1\">");
    	UsersReport.append("<tr><td><b>UserId</b></td><td><b>UserName</b></td><td><b>PrivouesTier</b></td>" +
        		"<td><b>NewTier</b></td></tr>");

        for (int index=0; index <downgradedUsers.size(); ++index){
          isUpdated = false;
      	  LoyaltyUser lUser = downgradedUsers.get(index);
      	  Tier PreviousTier = tiers.get(lUser.getPreviousTierId());
      	  Tier NewTier = tiers.get(lUser.getNewTierId());

      	  try{
      		  // update user new tier.
          	  Connection con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");
          	  updateUserTier(con,lUser);
              isUpdated = true;
      	  } catch (SQLException e) {
      		log.error("Error: Downgrade user " + lUser.getUserId() + " Failed.",e);
      	  }

      	  if (isUpdated){
      		  // Enter user to report.
          	  UsersReport.append("<tr>");
          	  UsersReport.append("<td>").append(lUser.getUserId()).append("</td>");
          	  UsersReport.append("<td>").append(lUser.getUserName()).append("</td>");
          	  UsersReport.append("<td>").append(PreviousTier.getDescription()).append("</td>");
          	  UsersReport.append("<td>").append(NewTier.getDescription()).append("</td>");
          	  UsersReport.append("</tr>");
      	  }


        }

        sendReportEmail();
    }

    /**
     * handle upgraded users
     *
     * @param
     * @throws SQLException
     */
    public static void handleRequalifiededUsers() throws SQLException {
    	boolean isUpdated;
    	UsersReport = new StringBuffer();

    	UsersReport.append("<br><b>Requalified Users:</b><table border=\"1\">");
    	UsersReport.append("<tr><td><b>UserId</b></td><td><b>UserName</b></td><td><b>Tier</b></td></tr>");

        for (int index=0; index <requalifiededUsers.size(); ++index){
          isUpdated = false;
      	  LoyaltyUser lUser = requalifiededUsers.get(index);
      	  Tier userTier = tiers.get(lUser.getPreviousTierId());

      	  try{
      		  // update user new tier.
          	  Connection con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");
          	  updateUserTier(con,lUser);
              isUpdated = true;
      	  } catch (SQLException e) {
      		log.error("Error: Requalified user " + lUser.getUserId() + " Failed.",e);
      	  }

      	  if (isUpdated){
      		  // Enter user to report.
          	  UsersReport.append("<tr>");
          	  UsersReport.append("<td>").append(lUser.getUserId()).append("</td>");
          	  UsersReport.append("<td>").append(lUser.getUserName()).append("</td>");
          	  UsersReport.append("<td>").append(userTier.getDescription()).append("</td>");
          	  UsersReport.append("</tr>");
      	  }

        }

        sendReportEmail();
    }

    /**
     * handle upgraded users
     *
     * @param
     * @throws SQLException
     */
    public static void handleWarnedUsers() throws SQLException {
    	boolean isUpdated;
    	UsersReport = new StringBuffer();

    	UsersReport.append("<br><b>Warned Users:</b><table border=\"1\">");
    	UsersReport.append("<tr><td><b>UserId</b></td><td><b>UserName</b></td><td><b>Tier</b></td></tr>");

        for (int index=0; index <warnedUsers.size(); ++index){
          isUpdated = false;
      	  LoyaltyUser lUser = warnedUsers.get(index);
      	  Tier CurrentTier = tiers.get(lUser.getPreviousTierId());

      	  try{
      		  // update warning for user
          	  updateWarningForUser(lUser);
          	  isUpdated = true;
    	  } catch (SQLException e) {
    		log.error("Error: Warning user " + lUser.getUserId() + " Failed.",e);
    	  }

    	  if (isUpdated){
	      	  // Enter user to report.
	      	  UsersReport.append("<tr>");
	      	  UsersReport.append("<td>").append(lUser.getUserId()).append("</td>");
	      	  UsersReport.append("<td>").append(lUser.getUserName()).append("</td>");
	      	  UsersReport.append("<td>").append(CurrentTier.getDescription()).append("</td>");
	      	  UsersReport.append("</tr>");
    	  }

        }

        sendReportEmail();
    }

    /**
     * handle upgraded users
     *
     * @param
     * @throws SQLException
     */
    public static void handleExpiredUsers() throws SQLException {
    	boolean isUpdated;
    	UsersReport = new StringBuffer();

    	UsersReport.append("<br><b>Expired Users:</b><table border=\"1\">");
    	UsersReport.append("<tr><td><b>UserId</b></td><td><b>UserName</b></td><td><b>Tier</b></td></tr>");

        for (int index=0; index <expiredUsers.size(); ++index){
          isUpdated = false;
      	  LoyaltyUser lUser = expiredUsers.get(index);
      	  Tier CurrentTier = tiers.get(lUser.getPreviousTierId());

      	  try{
	      	  // update expiration for user
	      	  updateExpirationForUser(lUser);
	          isUpdated = true;
      	  } catch (SQLException e) {
      		  log.error("Error: Expiring user " + lUser.getUserId() + " Failed.",e);
		  }

      	  if(isUpdated){
      		  // Enter user to report.
          	  UsersReport.append("<tr>");
          	  UsersReport.append("<td>").append(lUser.getUserId()).append("</td>");
          	  UsersReport.append("<td>").append(lUser.getUserName()).append("</td>");
          	  UsersReport.append("<td>").append(CurrentTier.getDescription()).append("</td>");
          	  UsersReport.append("</tr>");
      	  }
        }

        sendReportEmail();
    }

    private static void sendReportEmail(){
    	//  build the report
        String reportBody = "<html><body>" + UsersReport.toString() + "</body></html>";

  	    if (log.isEnabledFor(Level.INFO)) {
  	        log.log(Level.INFO, "Sending report email: " + reportBody);
  	    }

  	    //  send email
    	Hashtable<String,String> server = new Hashtable<String,String>();
  	    server.put("url", getPropertyByFile("loyalty.email.url"));
  	    server.put("auth", getPropertyByFile("loyalty.email.auth"));
  	    server.put("user", getPropertyByFile("loyalty.email.user"));
  	    server.put("pass", getPropertyByFile("loyalty.email.pass"));
  	    server.put("contenttype", getPropertyByFile("loyalty.email.contenttype", "text/html; charset=UTF-8"));

  	    Hashtable<String,String> email = new Hashtable<String,String>();
  	    email.put("subject", getPropertyByFile("loyalty.email.subject"));
  	    email.put("to", getPropertyByFile("loyalty.email.to"));
  	    email.put("from", getPropertyByFile("loyalty.email.from"));
  	    email.put("body", reportBody);
  	    sendEmail(server, email);
    }

    private static void updateUserTier(Connection con,LoyaltyUser lUser)throws SQLException {

    	CallableStatement cstmt = null;

        try {
            con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");

            cstmt = con.prepareCall("BEGIN LOYALTY_UPDATE_USERS_TIER(?,?,0,?); END;");
            cstmt.setLong(1, lUser.getUserId());
            cstmt.setLong(2, lUser.getNewTierId());
            // The points here represent the action points that the user should recieve in upgrade and requalification
            cstmt.setLong(3, lUser.getPointsForAction());
            cstmt.execute();

        } finally {
           	try {
           		cstmt.close();
           	} catch (Exception e) {
           		log.log(Level.ERROR, "Can't close", e);
           	}
         }

    }

    private static void updateWarningForUser(LoyaltyUser lUser)throws SQLException {

    	 Connection con = null;
         PreparedStatement ps = null;
         ResultSet rs = null;


         try {
             con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");


 		    String sql = " INSERT " +
 		    			 " INTO tier_users_history(id, tier_id, user_id, tier_his_action, time_created," +
 		    			 							" utc_offset, writer_id, points)" +
 		    			 " VALUES(SEQ_TIER_USERS_HISTORY.nextval,?,?,?,sysdate,?,0,?)";

 		    ps = con.prepareStatement(sql);

 		    ps.setLong(1, lUser.getPreviousTierId());
 		    ps.setLong(2, lUser.getUserId());
 		    ps.setLong(3, TiersManagerBase.TIER_ACTION_EXPIRATION_WARNING);
 		    ps.setString(4, lUser.getUserOffset());
 		    // The points here represent the action points the actual points of the user.
 		    ps.setLong(5, lUser.getPointsForAction());

 			rs = ps.executeQuery();

          } finally {
             	try {
             		rs.close();
             	} catch (Exception e) {
             		log.log(Level.ERROR, "Can't close", e);
             	}
             	try {
             		ps.close();
             	} catch (Exception e) {
             		log.log(Level.ERROR, "Can't close", e);
             	}
             	try {
             		con.close();
             	} catch (Exception e) {
                 log.log(Level.ERROR, "Can't close connection", e);
             	}
           }

    }

    private static void updateExpirationForUser(LoyaltyUser lUser)throws SQLException {

   	 	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;


        try {
            con = getConnection("loyalty.db.url","loyalty.db.user","loyalty.db.pass");


		    String sql = " UPDATE  tier_users" +
		    			 " SET     points = 0" +
		    			 " WHERE   user_id =  ? ";

		    ps = con.prepareStatement(sql);

		    ps.setLong(1, lUser.getUserId());

			rs = ps.executeQuery();

			String sql2 = " INSERT " +
						  " INTO  tier_users_history (id, tier_id, user_id, tier_his_action, time_created, " +
						 							" utc_offset, writer_id, points, action_points)" +
						  " VALUES  (SEQ_TIER_USERS_HISTORY.nextval,?,?,?,sysdate,?,0,0,?)";

			ps = con.prepareStatement(sql2);

			ps.setLong(1, lUser.getPreviousTierId());
			ps.setLong(2, lUser.getUserId());
			ps.setLong(3, TiersManagerBase.TIER_ACTION_POINTS_EXPIRED);
			ps.setString(4, lUser.getUserOffset());
			// The points here represent the action points that was decreased from actual points
			ps.setLong(5, lUser.getPointsForAction());

			rs = ps.executeQuery();

         } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
          }

   }


}
