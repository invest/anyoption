package il.co.etrader.marketing.report;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MarketingReportJob extends JobUtil{

	private static Logger log = Logger.getLogger(MarketingReportJob.class);

	private static enum ReportType {MARKETING,SHORT_REG_FORM,SHORT_REG_USERS};
	private static enum GroupType {COMBINATION,CAMPAIGN};
	private static enum Period {YESTERDAY, CURRENT_MONTH};// old - {YESTERDAY, CURRENT_MONTH, LAST_30_DAYS};

	public static void main(String[] args) throws Exception {
		propFile = args[0];

		log.info("Starting the MarketingReportJob.");

		for (ReportType reportType: ReportType.values()){
			for (GroupType groupType: GroupType.values()){
				for (Period period: Period.values()){
					log.info("Sending " + reportType.toString() + " " + groupType.toString() + " " + period.toString());
					SendXlsFile(reportType, groupType, period);
				}
			}
		}

		log.info("Finishing the MarketingReportJob.");
	}

	private static void SendXlsFile(ReportType reportType,GroupType groupType, Period period){
		HSSFWorkbook xlsWorkbook = new HSSFWorkbook();
		HSSFSheet xlsSheet= null;
		HSSFRow xlsRow = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		long skinId = 0;
		long currentSkinId = 0;
		int rowCounter = 0;
		String mailSubject = reportType.toString() + " " + groupType.toString() + " report - ";
		String fileName = reportType.toString() + "_" + groupType.toString() + "_" + period.toString() + ".xls";
		String sql = "";
		String emailAddress= "";
		boolean isSkinsReport = true;

		switch (reportType) {
		case MARKETING:
			emailAddress = getPropertyByFile("email.to");

			switch (groupType) {
			case COMBINATION:

				sql =
					" SELECT * " +
					" FROM TABLE(get_marketing_report_table(?,trunc(sysdate))) ";
				break;
			case CAMPAIGN:
				emailAddress = getPropertyByFile("email.to");

				sql =
					" SELECT " +
						" campaign_name, " +
						" writer_name, " +
						" skin_id, " +
						" skin_name,  " +
						" sum(clicks_num) clicks_num, " +
                        " sum(short_reg_contacts_only_num) short_reg_contacts_only_num, " +
                        " sum(short_reg_users_num) short_reg_users_num, " +
                        " sum(frst_dep_users_after_short_num) frst_dep_users_after_short_num, " +
                        " sum(frst_dep_amount_after_short) frst_dep_amount_after_short, " +
                        " sum(call_me_contacts_only_num) call_me_contacts_only_num, " +
                        " sum(call_me_users_num) call_me_users_num, " +
                        " sum(frst_dep_users_after_call_me) frst_dep_users_after_call_me, " +
                        " sum(frst_dep_amount_after_call_me) frst_dep_amount_after_call_me, " +
                        " sum(registered_users_num) registered_users_num, " +
					    " sum(frst_dep_users) frst_dep_users, " +
					    " sum(frst_dep_sales_users) frst_dep_sales_users, " +
					    " sum(frst_dep_ind_users) frst_dep_ind_users, " +
					    " sum(reg_frst_dep_users) reg_frst_dep_users, " +
					    " sum(reg_frst_dep_sales_users) reg_frst_dep_sales_users, " +
					    " sum(reg_frst_dep_ind_users) reg_frst_dep_ind_users, " +
					    " sum(frst_dep_amount) frst_dep_amount, " +
					    " sum(frst_dep_sales_amount) frst_dep_sales_amount, " +
					    " sum(frst_dep_ind_amount) frst_dep_ind_amount, " +
					    " sum(reg_frst_dep_amount) reg_frst_dep_amount, " +
					    " sum(reg_frst_dep_sales_amount) reg_frst_dep_sales_amount, " +
					    " sum(reg_frst_dep_ind_amount) reg_frst_dep_ind_amount, " +
					    " sum(dep_count) dep_count, " +
					    " sum(dep_sum) dep_sum " +//comma was removed
//					    " sum(dep_sum_out_of_range) dep_sum_out_of_range ," +
//					    " sum(house_win) house_win, " +
//					    " sum(house_win_reg) house_win_reg, " +
//					    " sum(fail_min_users) fail_min_users, " +
//					    " sum(fail_users) fail_users" +
					" FROM " +
						" TABLE(get_marketing_report_table(?,trunc(sysdate))) " +
					" GROUP BY " +
					    " campaign_name, " +
					    " writer_name," +
					    " skin_id," +
					    " skin_name " +
					" ORDER BY " +
					   	" skin_id, " +
					   	" campaign_name";
				break;
			}
			break;

		case SHORT_REG_FORM:
			emailAddress = getPropertyByFile("email.to");

			if (groupType == GroupType.COMBINATION || groupType == GroupType.CAMPAIGN){
				sql =
					" select " +
					   " s.id skin_id, " +
					   " s.name skin_name, " +
					   " mca.name campaign_name, ";
				if (groupType == GroupType.COMBINATION){
					sql+= " mc.id comb_id,";
				}
				sql+=
					   " count(*) short_reg_form_leads, " +
					   " sum(case when u.id is not null then 1 else 0 end) registerd_users_num, " +
					   " sum(case when u.first_deposit_id is not null then 1 else 0 end) depositors_num, " +
					   " sum(t.amount * t.rate)/100  deposits_sum_usd " +
					" from  " +
					   " contacts c " +
					      " left join users u on c.user_id = u.id " +
					         " left join transactions t on u.first_deposit_id = t.id, " +
					   " skins s, " +
					   " marketing_combinations mc, " +
					   " marketing_campaigns mca " +
				    " where  " +
					   " c.type = " + Contact.CONTACT_US_SHORT_REG_FORM + " " +
					   " and mc.id = c.combination_id " +
					   " and mca.id = mc.campaign_id " +
					   " and c.time_created between ? and trunc(sysdate) " +
					   " and s.id = c.skin_id " +
					   " and c.ip not in ('192.115.200.249', '82.81.193.152', '82.81.193.153', '82.81.193.154', '82.81.193.155', '82.81.193.156', '82.81.193.157', '82.81.193.158', '82.81.193.159','31.168.11.137','212.150.171.222','212.150.171.253','172.16.100.0','82.166.29.184','82.166.29.181') " +
					" group by  " +
					   " mca.name, ";
				if (groupType == GroupType.COMBINATION){
					sql+= " mc.id,";
				}
				sql+=
					   " s.id, " +
					   " s.name " +
					" order by " +
					   " s.id, " +
					   " mca.name ";
				if (groupType == GroupType.COMBINATION){
					sql+= " ,mc.id ";
				}
			}

			break;

		case SHORT_REG_USERS:
			emailAddress = getPropertyByFile("email.to2");
			isSkinsReport = false;

			if (groupType == GroupType.CAMPAIGN && (Period.YESTERDAY == period || Period.CURRENT_MONTH == period)){
				sql =
					" select " +
					"    c.id contact_id, c.user_id, to_char(c.time_created,'dd/mm/yyyy HH24:mi') time_short_reg, " +
					"    to_char(u.time_created,'dd/mm/yyyy HH24:mi') time_full_reg, c.name short_reg_name, " +
					"    u.first_name, u.last_name, c.email, c.mobile_phone, c.combination_id, mca.name campaign_name, " +
					"    mm.name medium_name, ms.size_vertical, ms.size_horizontal, ml.name lnd_page_name,  " +
					"    to_char(t.time_created,'dd/mm/yyyy HH24:mi') first_deposit_time, t.amount/100 first_deposit_amount, " +
					"    curr.name_key user_currency " +
					" from  " +
					"    contacts c " +
					"        left join users u on u.id = c.user_id " +
					"           left join transactions t on t.id = u.first_deposit_id " +
					"           left join currencies curr on curr.id = u.currency_id, " +
					"    marketing_campaigns mca, " +
					"    marketing_combinations mc " +
					"    	 left join marketing_mediums mm on mc.medium_id = mm.id " +
					"    	 left join marketing_sizes ms on mc.size_id = ms.id " +
					"   	 left join marketing_landing_pages ml on  mc.landing_page_id = ml.id " +
					" where  " +
					"    c.type = " + Contact.CONTACT_US_SHORT_REG_FORM + " " +
					"    and c.class_id != 0  " +
					"    and c.time_created between ? and trunc(sysdate) " +
					"    and c.combination_id = mc.id " +
					"    and mc.campaign_id = mca.id " +
					"	 and mc.campaign_id in (141,142,771,799,313,785,766,875,869) ";
			}else{
				return;
			}
			break;
		default:
			break;
		}

		try {
			con = getConnection();
			pstmt = con.prepareStatement(sql);

			GregorianCalendar gc = new GregorianCalendar();
			gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gc.set(GregorianCalendar.MINUTE, 0);
			gc.set(GregorianCalendar.SECOND, 0);
			gc.set(GregorianCalendar.MILLISECOND, 0);

			switch (period) {
			case YESTERDAY:
				gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
				mailSubject += "Date ";
				break;
			case CURRENT_MONTH:
				gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
				gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
				mailSubject += "Current Month ";
				break;
//			case LAST_30_DAYS:
//				gc.add(GregorianCalendar.DAY_OF_MONTH, -30);
//				mailSubject += "Starting ";
//				break;
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDate = gc.getTime();
			mailSubject += sdf.format(startDate);
			pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(startDate));

			rs = pstmt.executeQuery();

			if(isSkinsReport){
				while (rs.next()) {
					skinId = rs.getLong("skin_id");

					if (currentSkinId != skinId){
						currentSkinId = rs.getLong("skin_id");

						if (xlsWorkbook != null){
							writeToExcel(xlsWorkbook,fileName);
						}
						rowCounter = 0;
						xlsSheet = xlsWorkbook.createSheet(rs.getString("skin_name"));
						xlsRow = xlsSheet.createRow(rowCounter++);
						setColumnsNameInSheet(xlsRow, reportType, groupType);
					}

					xlsRow = xlsSheet.createRow(rowCounter++);
					setColumnsValuesInSheet(xlsWorkbook, xlsRow, rs, reportType, groupType);
				}
			}else{
				xlsSheet = xlsWorkbook.createSheet("data");
				xlsRow = xlsSheet.createRow(rowCounter++);
				setColumnsNameInSheet(xlsRow, reportType, groupType);

				while (rs.next()) {
					xlsRow = xlsSheet.createRow(rowCounter++);
					setColumnsValuesInSheet(xlsWorkbook, xlsRow, rs, reportType, groupType);
				}
			}

			if (xlsWorkbook != null){
				writeToExcel(xlsWorkbook,fileName);
			}

//		    send email
	        Hashtable<String,String> server = new Hashtable<String,String>();
	        server.put("url", getPropertyByFile("email.url"));
	        server.put("auth", getPropertyByFile("email.auth"));
	        server.put("user", getPropertyByFile("email.user"));
	        server.put("pass", getPropertyByFile("email.pass"));
	        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

	        Hashtable<String,String> email = new Hashtable<String,String>();
	        email.put("subject", mailSubject);
	        email.put("to", emailAddress);
	        email.put("from", getPropertyByFile("email.from"));
	        email.put("body", "The report is attached to this mail");

	        File attachment = new File(fileName);
	        File []attachments={attachment};
	        CommonUtil.sendEmail(server, email, attachments);

		}catch (Exception e) {
			log.fatal("Can't get marketing_report_table ", e);
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);

			try {
				con.close();
			} catch (Exception e) {
				log.error("Can't close",e);
			}
		}

	}

	private static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void setColumnsNameInSheet(HSSFRow xlsRow, ReportType reportType, GroupType groupType){
		int cellCounter = 0;
		HSSFCell xlsCell = null;

		switch (reportType) {
		case MARKETING:

			if (groupType == GroupType.COMBINATION){
				xlsCell = xlsRow.createCell(cellCounter++);
				xlsCell.setCellValue(new HSSFRichTextString("Comb Id"));
			}
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Media buyer"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Campaign name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Cost"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Impressions"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Clicks Num"));
			xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# Short Registrations that didn't complete full registration"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# Full Registrations following short reg"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# First Depositors following short reg"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("$ First Deposits amount following short reg"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# Requested Call Me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# Registrations following call me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# First Depositors following call me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("$ First Deposits amount following call me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# registrations (other)"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("# first depositors (other)"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(new HSSFRichTextString("$ first deposits amount (other)"));
            xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Registered Users Num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("First Depositors Num (FTD)"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD SALES"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD independent"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("First Reg Depositors Num (FTD REG)"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD REG SALES"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD REG independent"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD SALES$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD independent $"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD REG$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD REG SALES$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("FTD REG independent $"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Avg FTD$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Avg FTD REG$"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Num of All Deposits"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Amount of All Deposits"));
			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("Amount of Deposits made by 'old customers'"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("Clicks/Registered Users Num"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("Clicks/First Depositors Num"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("House win"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("House win Reg"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("Fail Min Users"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(new HSSFRichTextString("Fail Users"));
			break;

		case SHORT_REG_FORM:

			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Campaign Name"));
			if (groupType == GroupType.COMBINATION){
				xlsCell = xlsRow.createCell(cellCounter++);
				xlsCell.setCellValue(new HSSFRichTextString("Comb Id"));
			}
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Short Reg Form Leads"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Registerd Users Num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Depositors Num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Deposits Sum Usd"));
			break;

		case SHORT_REG_USERS:

			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Contact Id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("User Id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Time Short Reg"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Time Full Reg"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Short Reg Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("First Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Last Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Email"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Mobile Phone"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Comb Id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Campaign Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Medium Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Size Vertical"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Size Horizontal"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("Landing Page Name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("First Deposit Time"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("First Deposit Amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString("User Currency"));
			break;
		default:
			break;
		}
	}

	private static void setColumnsValuesInSheet(HSSFWorkbook xlsWorkbook, HSSFRow xlsRow, ResultSet rs, ReportType reportType, GroupType groupType) throws SQLException{
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
		cellStyle.setWrapText(true);

		switch (reportType) {
		case MARKETING:
			long ftdAvg = 0;
//			long ftdClicksAvg = 0;
//			long regUsersNumClicksAvg = 0;
			long ftdNum = rs.getLong("frst_dep_users");
			long clicks = rs.getLong("clicks_num");

            long regUsersOther = 0;
            long regUsers = rs.getLong("registered_users_num");
            long shortRegUsers = rs.getLong("short_reg_users_num");
            long callMeUsers = rs.getLong("call_me_users_num");

            long frstDepUsersOther = 0;
            long frstDepUsers = rs.getLong("frst_dep_users");
            long frstDepUsersAfterShort = rs.getLong("frst_dep_users_after_short_num");
            long frstDepUsersAfterCallMe = rs.getLong("frst_dep_users_after_call_me");

            long frstDepAmountOther = 0;
            long frstDepAmount = rs.getLong("frst_dep_amount");
            long frstDepAmountAfterShort = rs.getLong("frst_dep_amount_after_short");
            long frstDepAmountAfterCallMe = rs.getLong("frst_dep_amount_after_call_me");


            if (regUsers != 0 ) {
                regUsersOther = regUsers - (shortRegUsers + callMeUsers);
            } else {
                regUsersOther = 0;
            }

            if (frstDepUsers != 0 ) {
                frstDepUsersOther = frstDepUsers - (frstDepUsersAfterShort + frstDepUsersAfterCallMe);
            } else {
                frstDepUsersOther= 0;
            }

            if (frstDepAmount != 0 ) {
                frstDepAmountOther = frstDepAmount - (frstDepAmountAfterShort + frstDepAmountAfterCallMe)/100;
            } else {
                frstDepAmountOther = 0;
            }

			if (ftdNum > 0){
				long ftdAmount = rs.getLong("frst_dep_amount");
				BigDecimal bdFtdNum = new BigDecimal(ftdNum);

				ftdAvg = new BigDecimal(ftdAmount).divide(bdFtdNum, 2, RoundingMode.HALF_UP).longValue();
				//ftdClicksAvg = new BigDecimal(clicks).divide(bdFtdNum, 2, RoundingMode.HALF_UP).longValue();
			}


			long ftdRegAvg = 0;
			long ftdRegNum = rs.getLong("frst_dep_users");

			if (ftdRegNum > 0){
				long ftdRegAmount = rs.getLong("reg_frst_dep_amount");
				ftdRegAvg = new BigDecimal(ftdRegAmount).divide(new BigDecimal(ftdRegNum), 2, RoundingMode.HALF_UP).longValue();
			}

			long regUsersNum = rs.getLong("registered_users_num");
			if (regUsersNum > 0){
				//regUsersNumClicksAvg = new BigDecimal(clicks).divide(new BigDecimal(regUsersNum), 2, RoundingMode.HALF_UP).longValue();
			}

			if (groupType == GroupType.COMBINATION){
				xlsCell = xlsRow.createCell(cellCounter++);
				xlsCell.setCellValue(new HSSFRichTextString(rs.getString("comb_id")));
			}
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString(rs.getString("writer_name")));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString(rs.getString("campaign_name")));
			cellCounter += 2;
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("clicks_num"));
			xlsCell = xlsRow.createCell(cellCounter++);

            xlsCell.setCellValue(rs.getDouble("short_reg_contacts_only_num"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("short_reg_users_num"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("frst_dep_users_after_short_num"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("frst_dep_amount_after_short"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("call_me_contacts_only_num"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("call_me_users_num"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("frst_dep_users_after_call_me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(rs.getDouble("frst_dep_amount_after_call_me"));
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(regUsersOther);
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(frstDepUsersOther);
            xlsCell = xlsRow.createCell(cellCounter++);
            xlsCell.setCellValue(frstDepAmountOther);
            xlsCell = xlsRow.createCell(cellCounter++);

			xlsCell.setCellValue(rs.getDouble("registered_users_num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_sales_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_ind_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_sales_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_ind_users"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_sales_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("frst_dep_ind_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_sales_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("reg_frst_dep_ind_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(ftdAvg);
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(ftdRegAvg);
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("dep_count"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("dep_sum"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(rs.getDouble("dep_sum_out_of_range"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(ftdClicksAvg);
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(regUsersNumClicksAvg);
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(rs.getDouble("house_win"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(rs.getDouble("house_win_reg"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(rs.getDouble("fail_min_users"));
//			xlsCell = xlsRow.createCell(cellCounter++);
//			xlsCell.setCellValue(rs.getDouble("fail_users"));
			break;

		case SHORT_REG_FORM:
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString(rs.getString("campaign_name")));
			if (groupType == GroupType.COMBINATION){
				xlsCell = xlsRow.createCell(cellCounter++);
				xlsCell.setCellValue(new HSSFRichTextString(rs.getString("comb_id")));
			}
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("short_reg_form_leads"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("registerd_users_num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("depositors_num"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getDouble("deposits_sum_usd"));
			break;

		case SHORT_REG_USERS:

			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getLong("contact_id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getLong("user_id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("time_short_reg"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("time_full_reg"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("short_reg_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("first_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("last_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("email"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("mobile_phone"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getLong("combination_id"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("campaign_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("medium_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getLong("size_vertical"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getLong("size_horizontal"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("lnd_page_name"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("first_deposit_time"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("first_deposit_amount"));
			xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(rs.getString("user_currency"));
			break;

		default:
			break;
		}
	}

    public static void closeStatement(final Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                log.error(stmt, ex);
            }
        }
    }

    public static void closeResultSet(final ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                log.error(rs, ex);
            }
        }
    }
}
