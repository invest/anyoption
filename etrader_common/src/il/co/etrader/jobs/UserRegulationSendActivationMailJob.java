package il.co.etrader.jobs;


import java.util.ArrayList;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationActivationMail;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.CommonUtil;

public class UserRegulationSendActivationMailJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(UserRegulationSendActivationMailJob.class);
    private String emailFromQuestionnaireRestrictedBlocked;
    private String emailToQuestionnaireRestrictedBlocked;

    @Override
    public void setJobInfo(Job job) {
    	emailFromQuestionnaireRestrictedBlocked = CommonUtil.getConfig("email.from");
    	emailToQuestionnaireRestrictedBlocked = CommonUtil.getConfig("email.to.restricted.questionnaire");
    }

    @Override
    public boolean run() {
    	try {
    		long beginTime = System.currentTimeMillis();
        	log.debug("Try to get Users for regulation activation/unsuspend mail...");
        	ArrayList<UserRegulationActivationMail> activationMailsList = UserRegulationManager.getUserRegulationActivationMail();   
        	if(activationMailsList.size() > 0){
        		log.debug("FOUND Users " + activationMailsList.size() + " for regulation activation/unsuspend mail.");
        		ArrayList<UserRegulationActivationMail> sendedMails = new ArrayList<UserRegulationActivationMail>();
        		ArrayList<UserRegulationActivationMail> errSendedMails = new ArrayList<UserRegulationActivationMail>();
        		for(UserRegulationActivationMail activationMail : activationMailsList){
        			if(activationMail.getSkinId() == Skin.SKIN_ETRADER){
            			if(il.co.etrader.bl_managers.UsersManagerBase.sendActivationMailForSuspend(activationMail.getUserId(), false)){
            				activationMail.setSend(true);
            				sendedMails.add(activationMail);
            				log.debug("Send activation/unsuspend mail! for userId:" + activationMail.getUserId());
            			} else {
            				log.debug("For id " + activationMail.getId() + " and userId " + activationMail.getUserId() + " can NOT SEND activation/unsuspend mail!");
            				errSendedMails.add(activationMail);
            			}
        			} else if (activationMail.getSuspendedReasonId() == UserRegulationActivationMail.SUSPENDED_LOW_X_TRESHOLD 
            						|| activationMail.getSuspendedReasonId() == UserRegulationActivationMail.SUSPENDED_HIGH_Y_TRESHOLD){
                			if(il.co.etrader.bl_managers.UsersManagerBase.sendActivationMailForBlockedUsers(activationMail.getUserId())){
                				activationMail.setSend(true);
                				sendedMails.add(activationMail);
                				log.debug("Send activation/unsuspend Treshold Limit mail! for userId:" + activationMail.getUserId());
                			} else {
                				log.debug("For id " + activationMail.getId() + " and userId " + activationMail.getUserId() + " can NOT SEND activation/unsuspend Treshold Limit mail!");
                				errSendedMails.add(activationMail);
                			}

                			String skinName;
                			try {
                				Locale locale = new Locale("en");
                				skinName = CommonUtil.getMessage(locale, SkinsManagerBase
                						.getSkin(activationMail.getSkinId()).getDisplayName(), null);
                			} catch (Exception e) {
                				log.error("cant get skin name will use skin id");
                				skinName = String.valueOf(activationMail.getSkinId());
                			}
                			QuestionnaireManagerBase.sendEmailInCaseQuestionnaireRestrictedBlocked(activationMail.getUserId(), emailFromQuestionnaireRestrictedBlocked, emailToQuestionnaireRestrictedBlocked, skinName);  
            			} else {
            			if(il.co.etrader.bl_managers.UsersManagerBase.sendMailAfterLimitRegulation(activationMail.getUserId())){
            				activationMail.setSend(true);
            				sendedMails.add(activationMail);
            				log.debug("Send activation/unsuspend mail! for userId:" + activationMail.getUserId());
            			} else {
            				log.debug("For id " + activationMail.getId() + " and userId " + activationMail.getUserId() + " can NOT SEND activation/unsuspend mail!");
            				errSendedMails.add(activationMail);
            			}
        			}
        		}
        		UserRegulationManager.updateUserRegulationActivationMail(sendedMails);
        		if(errSendedMails.size() > 0){
        			UserRegulationManager.updateErrorUserRegulationActivationMail(errSendedMails);
        			log.debug("Can't sended " + errSendedMails.size() + " Error regulation activation/unsuspend mails.");
        		}
        		log.debug("Sended " + sendedMails.size() + " regulation activation/unsuspend mails.");
        		
        	} else {
        		log.debug("NOT FOUND Users for regulation activation/unsuspend mail.");
        	}
        	long endTime = System.currentTimeMillis();
        	log.debug("Finish to inserting proccess System Highest Hit in:" + (endTime - beginTime) + "ms");     
            return true;
			
		} catch (Exception e) {
			log.error("When run UserRegulationSendActivationMailJob",e);
			return false;
		}
    }

    @Override
    public void stop() {
        log.info("UserRegulationSendActivationMailJob stopping...");
    }
}