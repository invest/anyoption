package il.co.etrader.jobs;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.IMAPUtil;

public class AutomaticMailProcessJob implements ScheduledJob {
	
	private static final Logger log = Logger.getLogger(AutomaticMailProcessJob.class);
	
	private static String REPORT_EMAIL_TO = "reportEmailTo";
	private static String HOST = "host";
	private static String USER = "user";
	private static String PASSWORD = "password";
	private static String LIMITATION = "limitation";
	
	private String reportEmailTo;
	private String host;
	private String user;
	private String password;
	private long limitation;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("/=/");
                if (p[0].equals(REPORT_EMAIL_TO)) {
                	reportEmailTo = p[1].toString();                	
                } else if (p[0].equals(HOST)) {
                	host = (p[1].toString());
                } else if (p[0].equals(USER)) {
                	user = (p[1].toString());
                } else if (p[0].equals(PASSWORD)) {
                	password = (p[1].toString());
                } else if (p[0].equals(LIMITATION)) {
                	limitation = Long.parseLong(p[1]);
                }
            }
		}
	}
	
	@Override
	public boolean run() {
		log.info("AutomaticMailProcessJob - process START.");
		IMAPUtil.fetchUnread(limitation, reportEmailTo, host, user, password);
		log.info("AutomaticMailProcessJob - process END.");
		return true;
	}
	
	@Override
	public void stop() {
	}
}
