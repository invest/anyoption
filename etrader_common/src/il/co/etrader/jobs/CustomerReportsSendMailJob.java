package il.co.etrader.jobs;


import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.CustomerReportManager;
import il.co.etrader.bl_vos.CustomerReportInvestments;
import il.co.etrader.bl_vos.CustomerReportProfitData;
import il.co.etrader.bl_vos.CustomerReportState;
import il.co.etrader.bl_vos.CustomerReportTransactions;
import il.co.etrader.bl_vos.Skins;

import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Utils;

public class CustomerReportsSendMailJob implements ScheduledJob  {	
	public static final int CUSTOMER_REPORT_JOB_ID = 23;     
	private static final int USER_RECORD_LIMIT = 200;
	private static final long NOT_SENT_ERROR = 0;
	private static final long IS_SENT = 1;
	private static final long IS_CLOSE_NOT_SENT = 2;
	private static final Logger log = Logger.getLogger(CustomerReportsSendMailJob.class);
    
	public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
		long beginTime = System.currentTimeMillis();
    	try {
    		ArrayList<CustomerReportState> reportsState = CustomerReportManager.getLastReportState();    		
    		if(reportsState.size() != 0){			
    			//Seach for Reports where it is not finished and run again after 30 mins
    			for(CustomerReportState crs :reportsState){
    				if(crs.getReportState() < CustomerReportState.CUSTOMER_REPORT_FINISH_ALL_INSERT){
    					CustomerReportManager.updateCustomerReportsJob(0, 30, CUSTOMER_REPORT_JOB_ID);

    					String msg = "Oracle job is not finished for reportId:" + crs.getReportId() + " . Try again after 30 min: " + crs.getLogMsg();
    					sendLogEmail("Customer Reports is not finished", msg);
    					log.debug(msg);
    					return true;
    				}
    			}
    			
    			for(CustomerReportState crs : reportsState){
    				if(crs.getReportState() == CustomerReportState.CUSTOMER_REPORT_STATE_ERROR){
    					CustomerReportManager.updateCustomerReportsIsSendErrNotification(crs.getReportId());
    					String msg = "Have ERROR in generate DATA for reportId:" + crs.getReportId() + " with error:" + crs.getLogMsg();
    					sendLogEmail("Customer Reports Error", msg);
    					log.debug(msg);
    				} else if(crs.getReportState() == CustomerReportState.CUSTOMER_REPORT_FINISH_ALL_INSERT){
    					log.debug("Found reportId:" + crs.getReportId() + " for send to users ");
    					calculateAndSendCustomerReports(crs);
    					long endTime = System.currentTimeMillis();
    					CustomerReportManager.updateCustomerReportsIsSendReportsFinish(crs.getReportId());
    					String msg = "Finish the Customer Reports with reportId:" + crs.getReportId() + " in " + CommonUtil.getTimeFromMillis(endTime - beginTime) + "\n " + crs.getLogMsg();
    					sendLogEmail("Finih Customer Reports " + crs.getReportId(), msg);
    					log.debug(msg);
    					//CustomerReportManager.updateCustomerReportsJob(0, 1440, CUSTOMER_REPORT_JOB_ID);
    				}
    			}
    			
    		} else {
    			log.debug("Customer Report Not Found for send");
    		}
        	return true;
		} catch (Exception e) {
			log.error("When run CustomerReportsSendMailJob",e);
			return false;
		} finally {
        	long endTime = System.currentTimeMillis();
        	log.debug("Finish CustomerReportsSendMailJob in:" + CommonUtil.getTimeFromMillis(endTime - beginTime));
		}
    }

    public void stop() {
        log.info("CustomerReportsSendMailJob stopping...");
    }
    
    public static void calculateAndSendCustomerReports(CustomerReportState crs) throws SQLException{
    	log.debug("Begin calculateAndSendCustomerReports reportId:" + crs.getReportId());
    	ArrayList<Long> users = CustomerReportManager.getAllUsersSorted(crs.getReportId());
    	boolean isMonthly = false;
    	if(crs.getReportType() == CustomerReportState.CUSTOMER_REPORT_MONTHLY){
    		isMonthly = true;
    	}
    	boolean hasMoreUsers = true;
    	long lastUserIdLimit = 0;
    	long fromUserIdLimit = 0;
    	int index = USER_RECORD_LIMIT;
    	if(users.size() < 1){
    		log.debug("The users are empty");
    		hasMoreUsers =  false;
    	}else {
    		log.debug("The users for calclulate and send mails are:" + users.size());
    	}
    	
    	while (hasMoreUsers) {

    		if(users.size() >= index){
    			lastUserIdLimit = users.get(index - 1); 
    			index = index + USER_RECORD_LIMIT;
    		} else {
    			lastUserIdLimit = users.get(users.size() - 1);
    			hasMoreUsers = false;
    		}
    		//Get Reports Data
	    	HashMap<Long, HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>>> reportsInvestmentsData = CustomerReportManager.getCustomerInvestmentsReportData(crs.getReportId(), fromUserIdLimit, lastUserIdLimit);
	    	HashMap<Long, ArrayList<CustomerReportTransactions>> reportsTransactionsData = CustomerReportManager.getCustomerTransactionsReportData(crs.getReportId(), fromUserIdLimit, lastUserIdLimit, isMonthly);
	    	HashMap<Long, CustomerReportProfitData> reportsUsersProfitData = CustomerReportManager.getProfitData(crs.getReportId(), fromUserIdLimit, lastUserIdLimit, isMonthly);
	    	
	    	fromUserIdLimit = lastUserIdLimit;
			Locale locale = new Locale("iw");		
	    	String subject = CommonUtil.getMessage(locale, "customer.report.subject", null);
	    	String template = "CustomerReport.html";
	    	if(isMonthly){
	    		subject = CommonUtil.getMessage(locale, "customer.report.monthly.subject", null);
	    		template = "CustomerReportMonthly.html";	    		
	    	}
	    	
	    	ArrayList<CustomerReportInvestments> binaryOptionSettledInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> optionPlusSettledInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> oneTouchSettledInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> zeroOneHundredSettledInvestments = new ArrayList<CustomerReportInvestments>();
	    	
	    	ArrayList<CustomerReportInvestments> binaryOptionOpenInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> optionPlusOpenInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> oneTouchOpenInvestments = new ArrayList<CustomerReportInvestments>();
	    	ArrayList<CustomerReportInvestments> zeroOneHundredOpenInvestments = new ArrayList<CustomerReportInvestments>();
	    	
	    	ArrayList<CustomerReportTransactions> customerReportTransactionsList = new ArrayList<CustomerReportTransactions>();	    	
	    	HashMap<String, String> userInfo = new HashMap<String, String>();
	    	
			for (Map.Entry<Long, HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>>> entry : reportsInvestmentsData.entrySet()) {
				long userId = entry.getKey();
			    log.debug("begin calculate cutomert report data for userId = " + userId);
			    try {
			    	HashMap<Long, ArrayList<CustomerReportInvestments>> settledInvestments = new HashMap<Long, ArrayList<CustomerReportInvestments>>();
			    	HashMap<Long, ArrayList<CustomerReportInvestments>> openInvestments = new HashMap<Long, ArrayList<CustomerReportInvestments>>();
			    	
			    	if(reportsInvestmentsData.get(userId).get(true) != null){
			    		settledInvestments = reportsInvestmentsData.get(userId).get(true);
			    		log.debug("Found settledInvestments userId:" + userId);
			    		for(Map.Entry<Long, ArrayList<CustomerReportInvestments>> settled : settledInvestments.entrySet()){
			    			if(settled.getKey() == Opportunity.TYPE_REGULAR){
			    				binaryOptionSettledInvestments = settled.getValue();
			    			} else if(settled.getKey() == Opportunity.TYPE_OPTION_PLUS){
			    				optionPlusSettledInvestments = settled.getValue();
			    			} else if(settled.getKey() == Opportunity.TYPE_ONE_TOUCH){
			    				oneTouchSettledInvestments = settled.getValue();
			    			} else if(settled.getKey() == Opportunity.TYPE_BINARY_0_100_ABOVE || settled.getKey() == Opportunity.TYPE_BINARY_0_100_BELOW){
			    				zeroOneHundredSettledInvestments = settled.getValue();
			    			}
			    		}
			    		
			    	}
			    	
			    	if(reportsInvestmentsData.get(userId).get(false) != null){
			    		openInvestments = reportsInvestmentsData.get(userId).get(false);
			    		log.debug("Found openInvestments or onlyBalance Records userId:" + userId);
			    		for(Map.Entry<Long, ArrayList<CustomerReportInvestments>> open : openInvestments.entrySet()){
			    			if(open.getKey() == Opportunity.TYPE_REGULAR){
			    				binaryOptionOpenInvestments = open.getValue();
			    			} else if(open.getKey() == Opportunity.TYPE_OPTION_PLUS){
			    				optionPlusOpenInvestments = open.getValue();
			    			} else if(open.getKey() == Opportunity.TYPE_ONE_TOUCH){
			    				oneTouchOpenInvestments = open.getValue();
			    			} else if(open.getKey() == Opportunity.TYPE_BINARY_0_100_ABOVE || open.getKey() == Opportunity.TYPE_BINARY_0_100_BELOW){
			    				zeroOneHundredOpenInvestments = open.getValue();
			    			} else if(open.getKey() == 0){
			    				//Get user info data
			    		    	long reportType = open.getValue().get(0).getReportId();
			    		    	String firstName = open.getValue().get(0).getFirstName();
			    		    	String lastName = open.getValue().get(0).getLastName();
			    		    	String email = open.getValue().get(0).getEmail();
			    		    	String fromDateTxt = open.getValue().get(0).getFromDateTxt();
			    		    	String toDateTxt = open.getValue().get(0).getToDateTxt();
			    		    	String balanceBeginDateTxt = open.getValue().get(0).getBalanceBeginDateTxt();
			    		    	String balanceEndDateTxt = open.getValue().get(0).getBalanceEndDateTxt();
			    		    	String generationDateTimeTxt = open.getValue().get(0).getTimeGenerateTxt();
			    		    	
			    		    	String lostTxt = "";
			    		    	String profitedTxt = "";
			    		    	if(reportsUsersProfitData.get(userId) != null){
			    		    		lostTxt = reportsUsersProfitData.get(userId).getLostTxt();
			    		    		profitedTxt = reportsUsersProfitData.get(userId).getProfitedTxt();
			    		    	}
			    		    	if(CommonUtil.isParameterEmptyOrNull(lostTxt)){
			    		    		lostTxt = "N/A";
			    		    	}
			    		    	if(CommonUtil.isParameterEmptyOrNull(profitedTxt)){
			    		    		profitedTxt = "N/A";
			    		    	}
			    		    	
			    		    	userInfo.put("reportType", reportType + "");
			    		    	userInfo.put("firstName", firstName);
			    		    	userInfo.put("lastName", lastName);
			    		    	userInfo.put("userId", userId + "");
			    		    	userInfo.put("email", email);
			    		    	userInfo.put("fromDateTxt", fromDateTxt);
			    		    	userInfo.put("toDateTxt", toDateTxt);
			    		    	userInfo.put("balanceBeginDateTxt", balanceBeginDateTxt);
			    		    	userInfo.put("balanceEndDateTxt", balanceEndDateTxt);
			    		    	userInfo.put("generationDateTimeTxt", generationDateTimeTxt);
			    		    	userInfo.put("lostTxt", lostTxt);
			    		    	userInfo.put("profitedTxt", profitedTxt);
			    		    	
			    			}		    		
			    		}
			    	}
			    	
			    	if(reportsTransactionsData.get(userId) != null){
			    		for(CustomerReportTransactions cr : reportsTransactionsData.get(userId)){
			    			customerReportTransactionsList.add(cr);
			    		}
			    		
			    		reportsTransactionsData.remove(userId);
			    	}
			    	boolean isCloseAccount = CustomerReportManager.isUserCloseByIssue(userId);
			    	log.debug("Try to send customer report to userId:" + userId);
			    	String mailBody = sendEmail(binaryOptionSettledInvestments,
			    			optionPlusSettledInvestments,
			    			oneTouchSettledInvestments,
			    			zeroOneHundredSettledInvestments,		
			    			binaryOptionOpenInvestments,
			    			optionPlusOpenInvestments,
			    			oneTouchOpenInvestments,
			    		    zeroOneHundredOpenInvestments,	
			    		    customerReportTransactionsList,
			    			userInfo,
			    			subject,
			    			template,
			    			isCloseAccount);
			    	if(isCloseAccount){
				    	log.debug("User is close Account and not send customer report to userId:" + userId);
				    	CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, IS_CLOSE_NOT_SENT, mailBody);
			    	} else {
				    	log.debug("Sent customer report to userId:" + userId);
				    	CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, IS_SENT, mailBody);
			    	}
			    	
			    	binaryOptionSettledInvestments.clear();
			    	optionPlusSettledInvestments.clear();
			    	oneTouchSettledInvestments.clear();
			    	zeroOneHundredSettledInvestments.clear();
			    	
			    	binaryOptionOpenInvestments.clear();
			    	optionPlusOpenInvestments.clear();
			    	oneTouchOpenInvestments.clear();
			    	zeroOneHundredOpenInvestments.clear();
			    	
			    	customerReportTransactionsList.clear();
			    	userInfo.clear();		    	
				} catch (Exception e) {
					log.error("When send Cutomer Report for userId" + userId, e);
					CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, NOT_SENT_ERROR, e.toString());
				}
			}
			//If have only transactions data
			for (Map.Entry<Long, ArrayList<CustomerReportTransactions>> entry : reportsTransactionsData.entrySet()) {			
				long userId = entry.getKey();
				log.debug("begin calculate customer transactions report data for userId = " + userId);
				customerReportTransactionsList = reportsTransactionsData.get(userId);
				
				long reportType = customerReportTransactionsList.get(0).getReportId();
				String firstName = customerReportTransactionsList.get(0).getFirstName();
				String lastName = customerReportTransactionsList.get(0).getLastName();
				String email = customerReportTransactionsList.get(0).getEmail();
				String fromDateTxt = customerReportTransactionsList.get(0).getFromDateTxt();
				String toDateTxt = customerReportTransactionsList.get(0).getToDateTxt();
				String balanceBeginDateTxt = customerReportTransactionsList.get(0).getBalanceBeginDateTxt();
				String balanceEndDateTxt = customerReportTransactionsList.get(0).getBalanceEndDateTxt();
				String generationDateTimeTxt = customerReportTransactionsList.get(0).getTimeGenerateTxt();
		    	String lostTxt = "";
		    	String profitedTxt = "";
		    	if(reportsUsersProfitData.get(userId) != null){
		    		lostTxt = reportsUsersProfitData.get(userId).getLostTxt();
		    		profitedTxt = reportsUsersProfitData.get(userId).getProfitedTxt();
		    	}
		    	if(CommonUtil.isParameterEmptyOrNull(lostTxt)){
		    		lostTxt = "N/A";
		    	}
		    	if(CommonUtil.isParameterEmptyOrNull(profitedTxt)){
		    		profitedTxt = "N/A";
		    	}
		    	
		    	userInfo.put("reportType", reportType + "");
		    	userInfo.put("firstName", firstName);
		    	userInfo.put("lastName", lastName);
		    	userInfo.put("userId", userId + "");
		    	userInfo.put("email", email);
		    	userInfo.put("fromDateTxt", fromDateTxt);
		    	userInfo.put("toDateTxt", toDateTxt);
		    	userInfo.put("balanceBeginDateTxt", balanceBeginDateTxt);
		    	userInfo.put("balanceEndDateTxt", balanceEndDateTxt);
		    	userInfo.put("generationDateTimeTx", generationDateTimeTxt);
		    	userInfo.put("lostTxt", lostTxt);
		    	userInfo.put("profitedTxt", profitedTxt);
	
		    	log.debug("Try to send customer report to userId:" + userId);
		    	String mailBody;
		    	boolean isCloseAccount = CustomerReportManager.isUserCloseByIssue(userId);
				try {
					mailBody = sendEmail(binaryOptionSettledInvestments,
							optionPlusSettledInvestments,
							oneTouchSettledInvestments,
							zeroOneHundredSettledInvestments,		
							binaryOptionOpenInvestments,
							optionPlusOpenInvestments,
							oneTouchOpenInvestments,
						    zeroOneHundredOpenInvestments,	
						    customerReportTransactionsList,
							userInfo,
							subject,
							template,
							isCloseAccount);
					if(isCloseAccount){
						log.debug("User is close Account and not send customer report to userId:" + userId);
						CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, IS_CLOSE_NOT_SENT, mailBody);
					} else {
						log.debug("Sent customer report with Transaction only to userId:" + userId);
						CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, IS_SENT, mailBody);
					}
			    	
			    	customerReportTransactionsList.clear();			    	
			    	userInfo.clear();	
				} catch (Exception e) {
					log.error("When send TransactionCutomer Report for userId" + userId, e);
					CustomerReportManager.insertCustomerReportSent(crs.getReportId(), userId, NOT_SENT_ERROR, e.toString());					
				}
			}
    	}
    }
    
	private static String sendEmail(
	ArrayList<CustomerReportInvestments> binaryOptionSettledInvestments,
	ArrayList<CustomerReportInvestments> optionPlusSettledInvestments,
	ArrayList<CustomerReportInvestments> oneTouchSettledInvestments,
	ArrayList<CustomerReportInvestments> zeroOneHundredSettledInvestments,		
	ArrayList<CustomerReportInvestments> binaryOptionOpenInvestments,
	ArrayList<CustomerReportInvestments> optionPlusOpenInvestments,
	ArrayList<CustomerReportInvestments> oneTouchOpenInvestments,
	ArrayList<CustomerReportInvestments> zeroOneHundredOpenInvestments,	
	ArrayList<CustomerReportTransactions> customerReportTransactionsList,
	HashMap<String, String> userInfo,
	String subject,
	String template,
	boolean isCloseAccount) throws Exception {
		HashMap<String, Object> params = new HashMap<String, Object>();
		// put all  params
		params.put("binaryOptionSettledInvestments", binaryOptionSettledInvestments);
		params.put("optionPlusSettledInvestments", optionPlusSettledInvestments);
		params.put("oneTouchSettledInvestments", oneTouchSettledInvestments);
		params.put("zeroOneHundredSettledInvestments", zeroOneHundredSettledInvestments);		
		params.put("binaryOptionOpenInvestments", binaryOptionOpenInvestments);		
		params.put("optionPlusOpenInvestments", optionPlusOpenInvestments);
		params.put("oneTouchOpenInvestments", oneTouchOpenInvestments);
		params.put("zeroOneHundredOpenInvestments", zeroOneHundredOpenInvestments);		
		params.put("customerReportTransactionsList", customerReportTransactionsList);
		// Add userInfo
		for (Map.Entry<String, String> entry : userInfo.entrySet()) {
			params.put(entry.getKey(), entry.getValue());
		}			
		
		initEmail(subject, template);
		String mailBody = getEmailBody(params);
		emailProperties.put("body", mailBody);
		emailProperties.put("to", userInfo.get("email"));
		if(!isCloseAccount){
			Utils.sendEmail(serverProperties, emailProperties, null);
		}		
		return mailBody;
	}
	
	private static String getEmailBody(HashMap params) throws Exception {
		// set the context and the parameters
		VelocityContext context = new VelocityContext();
		// get all the params and add them to the context
		String paramName = "";
		log.debug("Adding parrams");
		for (Iterator keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName,params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		emailTemplate.merge(context,sw);
		return sw.toString();
	}    
    
	private static Template getEmailTemplate(String fileName) throws Exception {
		try {
			String templatePath = CommonUtil.getProperty("templates.path")  + "iw_1/" ;
			Properties props = new Properties();
			props.setProperty("file.resource.loader.path", templatePath);
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache","true");
			Velocity.init(props);
			return Velocity.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("!!! ERROR >> Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}
	
	private static Template emailTemplate;
	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;
	private static void initEmail(String subject, String name) throws Exception {
		// get the emailTemplate
		if(!CommonUtil.isParameterEmptyOrNull(name)){
			emailTemplate = getEmailTemplate(name);
		}		
		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));
		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject",subject);
		emailProperties.put("from", ApplicationDataBase.getSupportEmailBySkinId(Skins.SKIN_ETRADER));		
	}   	
	
	private void sendLogEmail(String subject , String mailBody) {
		try {
			initEmail(subject, "");
		} catch (Exception e) {
			log.error("Can't init email", e);
		}
		emailProperties.put("body", mailBody);
		emailProperties.put("to", Utils.getProperty("customer.report.watchers"));
        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }
}


