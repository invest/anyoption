package il.co.etrader.jobs;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Utils;

import il.co.etrader.bl_managers.UsersAutoMailDaoManager;
import il.co.etrader.bl_vos.UsersAutoMail;

public class UsersAutoMailJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(UsersAutoMailJob.class);
    private String emailTo;
    @Override
    public void setJobInfo(Job job) {
		String[] arr = job.getConfig().split(",");
		Hashtable<String, String> map = new Hashtable<>();
		for (String el : arr) {
			String[] el1 = el.split("=");
			map.put(el1[0], el1[1]);
		}
		emailTo = map.get("email_to");
	
    }

    @Override
    public boolean run() {
    	try {
    		long beginTime = System.currentTimeMillis();
        	log.debug("Try to get Users for Auto Mails...");                	
        	
        	ArrayList<UsersAutoMail> usersAutoMail = UsersAutoMailDaoManager.getUsersAutoMail(UsersAutoMailDaoManager.AFTER_FTD_DOC_REQ_REASON);
        	afterFDTDocumentRequest(usersAutoMail);        	
        	
        	ArrayList<UsersAutoMail> usersNotifeSumDepoMail = UsersAutoMailDaoManager.getUsersnotifeReachedTotalDeposit(UsersAutoMailDaoManager.NOTIFE_REACHE_TOTAL_DEPOSITS_REASON);
        	notifeReachedTotalDeposits(usersNotifeSumDepoMail);
        	
        	if(usersAutoMail.size() == 0 && usersNotifeSumDepoMail.size() == 0){
        		log.debug("Not found Users for Auto Mails");
        		
        	}        	        	
        	long endTime = System.currentTimeMillis();
        	log.debug("Finish to UsersAutoMailJob in:" + (endTime - beginTime) + "ms");     
            return true;
			
		} catch (Exception e) {
			log.error("When run UsersAutoMailJob",e);
			return false;
		}
    }
    

	private void afterFDTDocumentRequest(ArrayList<UsersAutoMail> usersAutoMail) throws SQLException {
		if(usersAutoMail.size() > 0){
			log.debug("FOUND Users " + usersAutoMail.size() + " for auto mail.");
			ArrayList<Long> sendedMailsId = new ArrayList<>();
			ArrayList<Long> errSendedMailsId = new ArrayList<>();
			for(UsersAutoMail uam : usersAutoMail){
				if(il.co.etrader.bl_managers.UsersManagerBase.sendUsersAutoEmail(uam.getUserId())){
					sendedMailsId.add(uam.getMailId());
					log.debug("Send auto mail for userId:" + uam.getUserId());
				} else {
					log.debug("For id " + uam.getMailId() + " and userId " + uam.getUserId() + " can NOT SEND auto mail!");
					errSendedMailsId.add(uam.getMailId());
				}
			}
			UsersAutoMailDaoManager.updateUsersAutoMailSent(sendedMailsId);
			if(errSendedMailsId.size() > 0){
				UsersAutoMailDaoManager.updateUsersAutoMailReject(errSendedMailsId);
				log.debug("Can't sended user auto mails with count:" + errSendedMailsId.size());
			}
			log.debug("Sended " + sendedMailsId.size() + " regulation activation/unsuspend mails.");
		}
	}

	private void notifeReachedTotalDeposits(ArrayList<UsersAutoMail> usersNotifeSumDepoMail) throws SQLException {
		if(usersNotifeSumDepoMail.size() > 0){
			log.debug("FOUND Users " + usersNotifeSumDepoMail.size() + " for notife reached total deposits ");
			ArrayList<Long> sendedMailsId = new ArrayList<>();
			ArrayList<Long> errSendedMailsId = new ArrayList<>();
			for(UsersAutoMail uam : usersNotifeSumDepoMail){
					sendNotifeMail(uam);
					sendedMailsId.add(uam.getMailId());
					log.debug("Send auto mail for userId:" + uam.getUserId());
			}
			UsersAutoMailDaoManager.updateUsersAutoMailSent(sendedMailsId);
			if(errSendedMailsId.size() > 0){
				UsersAutoMailDaoManager.updateUsersAutoMailReject(errSendedMailsId);
				log.debug("Can't sended user auto mails with count:" + errSendedMailsId.size());
			}
			log.debug("Sended " + sendedMailsId.size() + " regulation activation/unsuspend mails.");
		}
	}
	
	private void sendNotifeMail(UsersAutoMail uam){
		String engagementDate = "";
		if(uam.getQuestionnaireDoneDate() != null){
			SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
			engagementDate = dt.format(uam.getQuestionnaireDoneDate());
		}		 
		String reportBody = "<html><body> "
				+ "<b> Notification for customer reached  total deposit </b><br/>"
				+ "<b>User ID:</b> " + uam.getUserId() + "<br/>"
			    + "<b>Engagement date:</b> " + engagementDate + "<br/>"
			    + "<b>Skin:</b> " + uam.getSkinName() + "<br/>"
				+ "</body></html>";
		
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", "Notification for customer ID:" + uam.getUserId() + " reached  total deposit");
	    email.put("to", emailTo); 
	    email.put("from", "support@anyoption.com"); 	    
        email.put("body", reportBody);
		CommonUtil.sendEmail(serverProperties, email, null);
	}

    @Override
    public void stop() {
        log.info("UsersAutoMailJob stopping...");
    }
}