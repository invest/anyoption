package il.co.etrader.jobs;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.jobs.ScheduledJob;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Auto update population job
 * @author eyalo
 */
public class AutoUpdatePopulationJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(AutoUpdatePopulationJob.class);
	private long cfgUserId;
	
	@Override
	public void setJobInfo(Job job) {
		/* Get user id from config's column */
		cfgUserId = 0;
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("userId")) {
                	String userId = p[1];
                	if (!CommonUtil.isParameterEmptyOrNull(userId)) {
                		Long uId = Long.valueOf(userId);
                		if (null != uId) {
                			cfgUserId = uId;
                		}
                	}
                }
            }
		}
	}

	@Override
	public boolean run() {
		log.info("AutoUpdatePopulationJob - process START.");
		PopulationEntryBase peb = new PopulationEntryBase();
		peb.setCurrPopualtionTypeId(PopulationsManagerBase.POP_TYPE_DECLINE_LAST);
		UserBase user = new UserBase();
		user.setDecline(true);
		user.setId(cfgUserId);
		try {
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			String numDaysRemoveFromPop = CommonUtil.getEnum(PopulationEntriesManagerBase.ENUM_REMOVE_FROM_POPULATION, PopulationEntriesManagerBase.CODE_NUM_DAYS_LAST_DECLINE);
			createIssueAndIssueAction(issue, issueAction, numDaysRemoveFromPop);
			user.setNumDaysLastDecline(Long.valueOf(numDaysRemoveFromPop));
			ArrayList<PopulationEntryBase> entriesList = PopulationEntriesManagerBase.getPopEntriesForUpdatePop(peb, user);
			for (PopulationEntryBase entry : entriesList) {
				log.info("Get user by user id: " + entry.getUserId() + " - START.");
				UsersManagerBase.getByUserId(entry.getUserId(), user);
				log.info("Get user by user id: " + entry.getUserId() + " - END.");
				setIssueAndIssueAction(entry, user, issue, issueAction);
				insertIssueAndIssueAction(issue, issueAction, entry, user);
			}
			if (entriesList.size() == 0) {
				log.info("Not found population entries for update population.");
			}
		} catch (Exception e) {
			log.error("Error, Problem with AutoUpdatePopulationJob.", e);
		}
		log.info("AutoUpdatePopulationJob - process END.");
		return true;
	}

	@Override
	public void stop() {
		log.info("AutoUpdatePopulationJob - STOP.");
	}
	
	/**
	 * Create issue and issue action
	 * @param issue
	 * @param issueAction
	 * @throws Exception
	 */
	public static void createIssueAndIssueAction(Issue issue, IssueAction issueAction, String numDaysRemoveFromPop) throws Exception {
		/* Filling issue fields */
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);

		/* Filling action fields */
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_POPULATION));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
		issueAction.setComments("Client has been removed automatically when " + numDaysRemoveFromPop + " days have passed");
		issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
	}
	
	/**
	 * Set issue and issue action
	 * @param entry
	 * @param user
	 * @param issue
	 * @param issueAction
	 * @throws Exception
	 */
	public static void setIssueAndIssueAction(PopulationEntryBase entry, UserBase user, Issue issue, IssueAction issueAction) throws Exception {
		/* issue */
		issue.setUserId(entry.getUserId());
		issue.setContactId(entry.getContactId());
		/* issueAction */
		issueAction.setActionTime(new Date());
		/* user */
		user.setId(entry.getUserId());
		user.setContactId(entry.getContactId());
	}
	
	/**
	 * Insert issue and issue action
	 * @param issue
	 * @param issueAction
	 * @param entry
	 * @param user
	 * @throws Exception
	 */
	public static void insertIssueAndIssueAction(Issue issue, IssueAction issueAction, PopulationEntryBase entry, UserBase user) throws Exception {
		// find issue of marketing pop entry
		log.info("Search issue id by population entry id - START.");
		long issueId = IssuesManagerBase.searchIssueIdByPopEntryID(entry.getCurrEntryId());
		log.info("issue id: " + issueId);
		log.info("Search issue id by population entry id - END.");
		// if not found an issue, update new issue fields otherwise update issue id in issue action
		if (0 == issueId) {
			issue.setPopulationEntryId(entry.getCurrEntryId());
			issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_POPULATION));
		} else {
			issueAction.setIssueId(issueId);
		}
		if (issueId == 0) {
			log.info("Insert issue and issue action - START.");
            IssuesManagerBase.insertIssue(issue, issueAction, user.getUtcOffset(), Writer.WRITER_ID_AUTO, user, 0);
            log.info("Insert issue and issue action - END.");
        } else {
        	log.info("Insert issue action - START.");
            issue.setId(issueId);
            IssuesManagerBase.insertAction(issue, issueAction, user.getUtcOffset(), Writer.WRITER_ID_AUTO, user, 0);
            log.info("Insert issue action - END.");
        }
	}
}
