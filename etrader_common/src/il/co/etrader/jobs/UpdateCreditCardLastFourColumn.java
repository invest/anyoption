/**
 * 
 */
package il.co.etrader.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.AESUtil;

/**
 * @author pavelt
 *
 */
public class UpdateCreditCardLastFourColumn extends BaseBLManager implements ScheduledJob {

	/* (non-Javadoc)
	 * @see com.anyoption.common.jobs.ScheduledJob#setJobInfo(com.anyoption.common.beans.Job)
	 */
	
	private static final Logger log = Logger.getLogger(UpdateCreditCardLastFourColumn.class);
	private String url;
	private boolean running;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")){
			url = job.getConfig();
		}

	}

	/* (non-Javadoc)
	 * @see com.anyoption.common.jobs.ScheduledJob#run()
	 */
	@Override
	public boolean run() {
		log.debug("UpdateCreditCardLastFourColumn job starting... ");
		final int batchSize = 1000;
		int count = 0;
		running = true;
		PreparedStatement pSelect = null;
		PreparedStatement pUpdate = null;
		ResultSet rs = null;
		Connection conn = null;
		String sqlUpdate = " UPDATE credit_cards cc set cc.cc_number_last_4_digits = ? WHERE cc.id = ? ";
		String selectSql = " SELECT cc.id, cc.cc_number FROM credit_cards cc WHERE cc.cc_number_last_4_digits is null ";
		
		try {
			conn = getConnection();
			pSelect = conn.prepareStatement(selectSql);
			pUpdate = conn.prepareStatement(sqlUpdate);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				try{
					String str = AESUtil.decrypt(rs.getString("cc_number"));
					if (str.length() >= 4) {
						str = str.substring(str.length() - 4);
					}
					pUpdate.setString(1, str);
					pUpdate.setLong(2, rs.getLong("id"));
					pUpdate.addBatch();
					
				}catch(Exception ex){
					log.error("Can not decrypt card with id: " + rs.getLong("id") + " with Exception: " + ex);
				}
				
				if (++count % batchSize == 0) {
					pUpdate.executeBatch();
				}
			}
			pUpdate.executeBatch();
		} catch (SQLException ex) {
			log.error("UpdateCreditCardLastFourColumn stopping after getting first connection... due to:" + ex);
			return false;
		} finally {
			try {
				conn.close();
				pSelect.close();
				pUpdate.close();
			} catch (SQLException ex) {
				log.error("UpdateCreditCardLastFourColumn ... due to:" + ex);
				return false;
			}
		}
		log.debug("UpdateCreditCardLastFourColumn job finished........................... ");
		return running;
	}
	
	@Override
	public void stop() {
        log.info("UpdateCreditCardLastFourColumn stopping...");
        running = false;
	}

}
