package il.co.etrader.jobs;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.jobs.ScheduledJob;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.MarkVipHelper;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Auto mark users vip every day job
 * @author eyalG
 */
public class MarkUsersVipJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(MarkUsersVipJob.class);
	
	@Override
	public void setJobInfo(Job job) {
		
	}

	@Override
	public boolean run() {
		log.info("MarkUsersVipJob - process START.");
		
		try {
			ArrayList<MarkVipHelper> list = UsersManagerBase.getUsersToMarkVip();
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			createIssueAndIssueAction(issue, issueAction);
			for (MarkVipHelper markVipHelper : list) {
				log.info("Get user by user id: " + markVipHelper.getUserId() + " - START.");
				setIssueAndIssueAction(issue, issueAction, markVipHelper.getUserId());
				UsersManagerBase.markUserAsVip(markVipHelper, issue, issueAction);
				log.info("Get user by user id: " + markVipHelper.getUserId() + " - END.");
			}
			if (list.size() == 0) {
				log.info("Not found users to update vip status.");
			}
		} catch (Exception e) {
			log.error("Error, Problem with MarkUsersVipJob.", e);
		}
		log.info("AutoUpdatePopulationJob - process END.");
		return true;
	}

	@Override
	public void stop() {
		log.info("MarkUsersVipJob - STOP.");
	}
	
	/**
	 * Create issue and issue action
	 * @param issue
	 * @param issueAction
	 * @throws Exception
	 */
	public static void createIssueAndIssueAction(Issue issue, IssueAction issueAction) throws Exception {
		/* Filling issue fields */
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_VIP));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);

		/* Filling action fields */
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_MAKE_VIP));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
		issueAction.setComments("Client has been made VIP by Auto job due to volume " + CommonUtil.displayAmount(Long.parseLong(CommonUtil.getEnum("lifetime volume for vip", "lifetime volume for vip")), Currency.CURRENCY_USD_ID));
//		issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
	}
	
	/**
	 * Set issue and issue action
	 * @param issue
	 * @param issueAction
	 * @param userId
	 * @throws Exception
	 */
	public static void setIssueAndIssueAction(Issue issue, IssueAction issueAction, long userId) throws Exception {
		/* issue */
		issue.setUserId(userId);
		/* issueAction */
		issueAction.setActionTime(new Date());
	}
}
