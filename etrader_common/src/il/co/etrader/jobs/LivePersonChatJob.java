/**
 * 
 */
package il.co.etrader.jobs;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.jobs.ScheduledJob;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_managers.WritersManagerBase;
import il.co.etrader.bl_vos.Chat;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.tabakov
 *
 */
public class LivePersonChatJob implements ScheduledJob {

	private static final Logger log = Logger.getLogger(LivePersonChatJob.class);
	private boolean running;
	private String appKey;
	private String secret;
	private String accessToken;
	private String accessTokenSecret;
	private String url;
	private long offset;
	private long limit;

	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
			String[] arr = job.getConfig().split(",");
			Hashtable<String, String> map = new Hashtable<>();
			for(String el : arr){
				String[] el1 = el.split("=");
				map.put(el1[0], el1[1]);
			}
			appKey = map.get("appKey");
			secret = map.get("secret");
			accessToken = map.get("accessToken");
			accessTokenSecret = map.get("accessTokenSecret");
			offset = Long.valueOf(map.get("offset"));
			limit = Long.valueOf(map.get("limit"));
			url = map.get("url") + "offset=" + offset + "&" + "limit=" + limit;
		}

	}

	@Override
	public boolean run() {
		log.debug("LivePersonChatJob job starting... ");
		running = true;

		parseJsonResponseAndInsertIssue(generateRequestAndGetResponseFromLivePersonApi());

		return running;
	}

	@Override
	public void stop() {
		log.debug("LivePersonChatJob stopping...");
		running = false;

	}

	private String generateRequestAndGetResponseFromLivePersonApi() {

		// OAuth Service from Scribe, you will need to fill in your api key & secret
		OAuthService service = new ServiceBuilder().provider(EhApi.class).provider(EhApi.class).apiKey(appKey).apiSecret(secret).build();
		// OAuth token, you will need to fill in your token & secret
		Token token = new Token(accessToken, accessTokenSecret);
		// Create the OAuth request
		OAuthRequest request = new OAuthRequest(Verb.POST, url);
		request.addHeader("Content-Type", "application/json");

		request.addPayload(generateJsonPayload());
		// sign the request
		service.signRequest(token, request);
		// send request
		Response response = request.send();

		return response.getBody();
	}

	private void parseJsonResponseAndInsertIssue(String response) {
		log.info("LivePerson json respose: " + response);
		Hashtable<String, Long> writers = new Hashtable<>();
		try {
			// Get json response from LivePerson Engage API
			JSONObject jsonObjRes = new JSONObject(response);
			JSONArray historyRecordsList = jsonObjRes.getJSONArray("interactionHistoryRecords");
			for (int i = 0; i < historyRecordsList.length(); i++) {

				JSONObject info = (JSONObject) historyRecordsList.getJSONObject(i).get("info");
				String startTime = info.get("startTime").toString();
				String endTime = info.get("endTime").toString();
				Date startDate = stringToDate(startTime);
				// Date endDate = stringToDate(endTime);
				String writerUsername = info.get("agentLoginName").toString();// writer
				String customerUsername = info.get("visitorName").toString(); // user name
				JSONObject transcriptObj = (JSONObject) historyRecordsList.getJSONObject(i).get("transcript");
				JSONArray linesList = transcriptObj.getJSONArray("lines");

				Chat chat = new Chat();
				chat.setChat(buildChatHTML(startTime, endTime, writerUsername, linesList));
				// TODO change it after you find which field is corresponding to chat type --> till now (09.03.2017) there isn't any :)
				chat.setTypeId(ConstantsBase.LIVEPERSON_CHAT_TYPE_DEFAULT);
				try {
					// get user_id and utc_offset
					User user = UsersManagerBase.getUserByUsername(customerUsername);
					if (user != null && user.getId() > 0) {
						// get writer ID
						long writerId = 0;
						try {
							// check if writer already persist in previous chat
							if (!writers.isEmpty() && writers.contains(writerUsername)) {
								writerId = writers.get(writerUsername);
							} else {
								// get writer by user name from DB and put it in HashTable
								writerId = WritersManagerBase.getWrterIdByUserName(writerUsername);
								if (writerId > 0) {
									writers.put(writerUsername, writerId);
								}
							}
						} catch (SQLException ex) {
							log.error("Can't not get WRITER due to: ", ex);
						}
						if (writerId == 0) {
							log.warn("writerId not found, set to defualt writerName: " + writerUsername);
						}
						// insert issue and chat
						IssuesManagerBase.insertIssueAndChat(chat, user.getId(), writerId, startDate, user.getUtcOffset());

					}
				} catch (SQLException ex) {
					log.error("Can't not get USER due to: ", ex);
				}
			}
			//clear writers table after job finished
			if (!writers.isEmpty()) {
				writers.clear();
			}
		} catch (Exception ex) {
			log.error("Response is not the correct format", ex);
		}
	}

	// body parameters that you would like to add to the API call
	private String generateJsonPayload() {
		GregorianCalendar gc = new GregorianCalendar();
		Date to = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, -1);
		Date from = gc.getTime();
		String payload = "{\"start\":{\"from\":"+from.getTime()+",\"to\":"+to.getTime()+"}}";
		
		return payload;
	}

	public String buildChatHTML(String chatStartTime, String chatEndTime, String writerUserName, JSONArray linesList) throws JSONException {
		String chatHtml = "<html><body>";
		chatHtml += "<table border='0' cellpadding='0' cellspacing='0' style='font-weight:bold;color:#093755'>"
				 + "<tr valign='top' height='5px' bgcolor='#c0c0c0'>" + "<td height='5px' colspan='3'>General Info</td>"
				 + "</tr>" + "<tr valign='top'>" + "<td> Chat start time </td>" + "<td width='20px'>&#160;</td>"
				 + "<td> " + chatStartTime + " </td>" + "</tr>" + "<tr valign='top'>" + "<td> Chat end time </td>"
				 + "<td width='20px'>&#160;</td>" + "<td> " + chatEndTime + " </td>" + "</tr>" + "<tr valign='top'>"
				 + "<td> Operator </td>" + "<td width='20px'>&#160;</td>" + "<td> " + writerUserName + " </td>" + "</tr>"
				 + "<tr valign='top' height='10px'>" + "<td height='10px' colspan='3'>&#160;</td>" + "</tr>"
				 + "</table>";

		chatHtml += "<table border='0' cellpadding='0' cellspacing='0'>"
				 + "<tr valign='top' height='5px' bgcolor='#c0c0c0'>"
				 + "<td height='5px' colspan='3'>Chat Transcript</td>" + "</tr>";
		for (int j = 0; j < linesList.length(); j++) {
			String textAuthor = linesList.getJSONObject(j).get("by").toString();
			String text = linesList.getJSONObject(j).get("text").toString();

			String StyleColor = textAuthor.equals("info") ? "green" : "blue";
			chatHtml += "<tr valign='top'>" + "<td valign='top' style='font-weight:bold;color:" + StyleColor + "'>"
					 + textAuthor + ":" + "</td>" + "<td width='5px'>&#160;</td>"
					 + "<td valign='top' style='color:black'>" + text + "</td></tr>";

		}
		chatHtml += "</table></body></html>";
		return chatHtml;
	}

	private static Date stringToDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-mm-dd HH:mm:ss.SSSZ");
		return sdf.parse(date);
	}

}
