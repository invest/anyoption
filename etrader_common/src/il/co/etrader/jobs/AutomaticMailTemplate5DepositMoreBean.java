package il.co.etrader.jobs;

import il.co.etrader.bl_vos.UserBase;

@SuppressWarnings("serial")
public class AutomaticMailTemplate5DepositMoreBean extends UserBase {
    private long transactionId;
    private String transactionPan;

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionPan() {
        return transactionPan;
    }

    public void setTransactionPan(String transactionPan) {
        this.transactionPan = transactionPan;
    }
}