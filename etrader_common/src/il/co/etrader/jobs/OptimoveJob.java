package il.co.etrader.jobs;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.UsersAwardBonus;
import com.anyoption.common.beans.UsersAwardBonusGroup;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.UsersAwardBonusDAO;
import com.anyoption.common.daos.UsersAwardBonusGroupDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//ChannelId : 4    ChannelName : Silverpop    
//ChannelId : 5    ChannelName : ExactTarget    
//ChannelId : 8    ChannelName : Facebook    
//ChannelId : 99    ChannelName : Parse (Mobile Notification)    
//ChannelId : 501    ChannelName : Lobby Banner    
//ChannelId : 502    ChannelName : Call Center    
//ChannelId : 504    ChannelName : Email    
//ChannelId : 505    ChannelName : SMS    
//ChannelId : 507    ChannelName : In-platform    

public class OptimoveJob extends JobUtil {
	private static final Logger log = Logger.getLogger(OptimoveJob.class);
	
	public final static int RESPONSE_STATUS_SUCCESS = 200;
	public final static long OPIMOV_CHANNEL_ID_SILVERPOP = 4;
	public final static long OPIMOV_CHANNEL_ID_MOBILE_NOTIFICATION = 99;
	public final static long OPIMOV_CHANNEL_ID_EMAIL = 504;
	public final static long OPIMOV_CHANNEL_ID_SMS = 505;
	//TODO what is other
	public final static long OPIMOV_CHANNEL_ID_OTHER = 509;
	
	
	public final static long ACTION_ID_CONTROL_GROUP = 1;
	public static String apiUrl;
	public static HashMap<Long, String> channelIds;
	
	public static String loginToken;

    public static void main(String[] args) {
    	log.debug("start optimov job");
    	propFile = args[0];
    	channelIds = new HashMap<Long, String>();
    	channelIds.put(OPIMOV_CHANNEL_ID_SILVERPOP, String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
    	channelIds.put(OPIMOV_CHANNEL_ID_MOBILE_NOTIFICATION, String.valueOf(IssuesManagerBase.ISSUE_ACTION_PUSH_NOTIFICATION));
    	channelIds.put(OPIMOV_CHANNEL_ID_EMAIL, String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
    	channelIds.put(OPIMOV_CHANNEL_ID_SMS, String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
    	channelIds.put(OPIMOV_CHANNEL_ID_OTHER, String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
    	
    	apiUrl = "https://api.optimove.net/v3.0/";
		int numbersOfTry = 0;
		boolean success = false;
		loginToken = getLoginToken("AnyOption@api", "An$$yOdf12d");
		
		ScheduledExecutorService exec = null;
		try {
			exec = Executors.newSingleThreadScheduledExecutor();
			exec.scheduleAtFixedRate(new Runnable() {
			  @Override
			  public void run() {
				  loginToken = getLoginToken("AnyOption@api", "An$$yOdf12d");
			  }
			}, 20, 20, TimeUnit.MINUTES);
			
			while (numbersOfTry < 3 && !success) {//try 3 times with gap of 20min		
				log.debug("start optimov try number " + numbersOfTry);
				if (null != loginToken) {
					String lastUpdate = getLastUpdateDate(loginToken);
					log.debug("lastUpdate " + lastUpdate);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar yesterday = Calendar.getInstance();
					yesterday.add(Calendar.DAY_OF_MONTH, -1); //yesterday
					if (lastUpdate.equalsIgnoreCase(sdf.format(yesterday.getTime()))) { //if last update is yesterday
						String todayDate = sdf.format(new Date()); //sdf.format(yesterday.getTime());
						success = true;
						TargetGroup[] targetGroups = getTargetGroups(loginToken, todayDate);
						log.debug("number of target Groups " + targetGroups.length);
						for (TargetGroup targetGroup : targetGroups) {
							PromoCodes[] promoCodes = getPromoCodesByTargetGroup(loginToken, todayDate, targetGroup.TargetGroupID);
							HashMap<Long, UsersAwardBonusGroup> uabgroup = new HashMap<Long, UsersAwardBonusGroup>();
							if (promoCodes.length > 0) {
								log.debug("we got target groups with bonus number of promoCodes " + promoCodes.length);
								//init the control group (not in use any more)
								UsersAwardBonusGroup usersAwardBonusGroup = new UsersAwardBonusGroup();
								usersAwardBonusGroup.setDescription("optimove: description + control group");
								uabgroup.put(ACTION_ID_CONTROL_GROUP, usersAwardBonusGroup);
								
								String actionName;
								Connection conn = null;
								try {
									conn = getConnection();
			        				conn.setAutoCommit(false);
			        				for (PromoCodes promoCode : promoCodes) {
			        					usersAwardBonusGroup = new UsersAwardBonusGroup();
			    						actionName = getActionName(loginToken, promoCode.ActionID);
			    						usersAwardBonusGroup.setDescription("optimove: " + actionName + " got bonus id: " + promoCode.PromoCode);
			    						usersAwardBonusGroup.setBonusId(Long.valueOf(promoCode.PromoCode));
			    						ActionDetails actionDetails = getActionDetails(loginToken, todayDate, targetGroup.TargetGroupID);
			    						Calendar calendar = Calendar.getInstance();
			    						usersAwardBonusGroup.setTimeCreated(calendar.getTime());
			    						calendar.add(Calendar.DAY_OF_MONTH, (int) actionDetails.Duration);
			    						usersAwardBonusGroup.setRedeemUntilDate(calendar.getTime());
			    						
			    						UsersAwardBonusGroupDAO.insert(conn, usersAwardBonusGroup);
			    						uabgroup.put(promoCode.ActionID, usersAwardBonusGroup);
			    					}
			        				Customer[] customers = getCustomers(loginToken, todayDate, targetGroup.TargetGroupID, 0);
			        				insertCustomers(conn, customers, uabgroup);
			        				long maxCustomers = 10000;
			        				while (customers.length == maxCustomers) {
			        					customers = getCustomers(loginToken, todayDate, targetGroup.TargetGroupID, maxCustomers);
			        					if (null == customers) {
			        						loginToken = getLoginToken("AnyOption@api", "An$$yOdf12d");
			        						customers = getCustomers(loginToken, todayDate, targetGroup.TargetGroupID, maxCustomers);
			        					}
			        					insertCustomers(conn, customers, uabgroup);
			        					maxCustomers += 10000;
			        				}
			        				conn.commit();
								} catch (Exception e) {
						    		log.error("Can't insert optimov users", e);
						    		try {
						    			conn.rollback();
									} catch (SQLException ie) {
										log.error("Can't rollback.", ie);
									}

								} finally {
									try {
										conn.setAutoCommit(true);
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										log.error("Can't setAutoCommit true.", e);
									}
						    	}
							}    				
						}
					} else {
						log.error("last update is not yesterday date, i will again try in 20 min (if its not the 3rd time)");
					}
				}
				numbersOfTry++;
				if (!success && numbersOfTry < 3) {
					try {
						Thread.sleep(1200000);
					} catch (InterruptedException e) {
						log.error("cant wait 20 min");
					}
				}
			}
			log.info("closeing exec thread");
			exec.shutdown();
		} catch (Exception e) {
			log.error("exception try to close exec thread", e);
			if (null != exec) {
				exec.shutdown();
			}
		}
		
		if (!success) {
			Hashtable<String, String> emailProperties = new Hashtable<String, String>();
			Hashtable<String, String> serverProperties = new Hashtable<String, String>();
			serverProperties.put("auth", JobUtil.getPropertyByFile("email.auth"));
			serverProperties.put("url",JobUtil.getPropertyByFile("email.url"));
			serverProperties.put("user",JobUtil.getPropertyByFile("email.user"));
			serverProperties.put("pass",JobUtil.getPropertyByFile("email.pass"));
			serverProperties.put("contenttype","text/html;charset=utf-8;");
			
			emailProperties.put("subject",JobUtil.getPropertyByFile("optimove.subject"));
			emailProperties.put("body", JobUtil.getPropertyByFile("optimove.body"));
			emailProperties.put("to",JobUtil.getPropertyByFile("optimove.sendTo"));
			emailProperties.put("from",JobUtil.getPropertyByFile("optimove.from"));
			sendEmail(serverProperties, emailProperties);
		}
		
		log.debug("end optimov job");
    }

	private static void insertCustomers(Connection conn, Customer[] customers, HashMap<Long, UsersAwardBonusGroup> uabgroup) throws Exception {
		log.info("start to insert " + customers.length + " users into db"); 
    	for (Customer customer : customers) {
			try {
				UsersAwardBonusGroup usersAwardBonusGroup = uabgroup.get(customer.getActionID());
				if (null != usersAwardBonusGroup && usersAwardBonusGroup.getId() != ACTION_ID_CONTROL_GROUP) { //only users that have bonus
					UsersAwardBonus usersAwardBonus = new UsersAwardBonus();
					usersAwardBonus.setUserId(customer.getCustomerIDNumber());
					usersAwardBonus.setUsersAwardBonusGroup(usersAwardBonusGroup);
					boolean isInsert = UsersAwardBonusDAO.insert(conn, usersAwardBonus);
					if (isInsert) {
						IssuesManagerBase.insertIssue(conn, String.valueOf(IssuesManagerBase.ISSUE_SUBJECTS_INTERNAL), String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED),
								usersAwardBonus.getUserId(), String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW), ConstantsBase.ISSUE_TYPE_REGULAR, channelIds.get(customer.ChannelID), Writer.WRITER_ID_AUTO, false,
								new Date(), ConstantsBase.OFFSET_GMT, usersAwardBonusGroup.getDescription());
					} else {
						log.info("user " + customer.getCustomerIDNumber() + " already got this bonus today");
					}
				}
			} catch (Exception e) {
				log.error("cant insert user to db user id = " + customer.getCustomerIDNumber(), e);
			}
		}
	}

	private static String getLoginToken(String user, String pass) {
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "general/login?username=" + user + "&password=" + pass);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
                String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
                return jsonResponse.subSequence(1, jsonResponse.length() - 1).toString();
            }
            log.debug("cant login");
            
		} catch (IOException e) {
			e.printStackTrace();
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
    }
    
    private static String getLastUpdateDate(String loginToken) {
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "general/GetLastDataUpdate");
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
                Gson gson = new GsonBuilder().serializeNulls().create();
//                jsonResponse = jsonResponse.replace("D", "d");
                MyDate date = gson.fromJson(jsonResponse, MyDate.class);
                return date.getDate();
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			e.printStackTrace();
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
    }
    
    private static TargetGroup[] getTargetGroups(String loginToken, String date) {
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "groups/GetTargetGroupsByDate?date=" + date);
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
                Gson gson = new GsonBuilder().serializeNulls().create();
//                jsonResponse = "{'TargetGroups':" + jsonResponse + "}";
                TargetGroup[] targetGroupArray = gson.fromJson(jsonResponse, TargetGroup[].class);
                return targetGroupArray;
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			log.debug("exception cant GetLastDataUpdate");
			e.printStackTrace();			
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
	}
    
    private static PromoCodes[] getPromoCodesByTargetGroup(String loginToken,
			String date, long targetGroupID) {
    	
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "actions/GetPromoCodesByTargetGroup?targetGroupID=" + targetGroupID + "&date=" + date);
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
            	Gson gson = new GsonBuilder().serializeNulls().create();
////                jsonResponse = "{'TargetGroups':" + jsonResponse + "}";
            	PromoCodes[] promoCodesArray = gson.fromJson(jsonResponse, PromoCodes[].class);
            	return promoCodesArray;                
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			log.debug("exception cant GetLastDataUpdate");
			e.printStackTrace();			
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
	}
    
    private static String getActionName(String loginToken, long actionId) {
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "actions/GetActionName?actionId=" + actionId);
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
                Gson gson = new GsonBuilder().serializeNulls().create();
                ActionName actionName = gson.fromJson(jsonResponse, ActionName.class);
                return actionName.getActionName();
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			e.printStackTrace();
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
    }
    
    private static ActionDetails getActionDetails(String loginToken,
			String todayDate, long targetGroupID) {
    	HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "actions/GetActionDetailsByTargetGroup?targetGroupID=" + targetGroupID + "&date=" + todayDate);
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
                Gson gson = new GsonBuilder().serializeNulls().create();
                ActionDetails[] actionDetails = gson.fromJson(jsonResponse, ActionDetails[].class);
                return actionDetails[0];
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			e.printStackTrace();
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
	}
    
    private static Customer[] getCustomers(String loginToken, String date,
			long targetGroupID, long startCustomer) {
		HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpGet httpget = new HttpGet(apiUrl + "customers/GetCustomerActionsByTargetGroup?targetGroupID=" + targetGroupID + "&date=" + date + "&includeControlGroup=true&$skip=" + startCustomer);
            httpget.addHeader("Authorization-Token", loginToken);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());

            HttpResponse response = httpclient.execute(httpget);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	
            	String jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");
            	Gson gson = new GsonBuilder().serializeNulls().create();
////                jsonResponse = "{'TargetGroups':" + jsonResponse + "}";
            	Customer[] customersArray = gson.fromJson(jsonResponse, Customer[].class);
            	return customersArray;                
            }
            log.debug("cant GetLastDataUpdate");
            
		} catch (IOException e) {
			log.debug("exception cant GetLastDataUpdate", e);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return null;
	}
    
    /**
     * for the Last Update Date
     * @author EyalG
     *
     */
    class MyDate implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String Date;

		public String getDate() {
			return Date;
		}

		public void setDate(String date) {
			Date = date;
		}

	}
    
    /**
     * for the tests group
     * @author EyalG
     *
     */
    class TargetGroup implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private long TargetGroupID;

		public long getTargetGroupID() {
			return TargetGroupID;
		}

		public void setTargetGroupID(long targetGroupID) {
			TargetGroupID = targetGroupID;
		}
	}
    
    /**
     * should be for bonus id
     * @author EyalG
     * 
     * PromoCode should be the bonus id
     *
     */
    class PromoCodes implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private long RecipientGroupID;
		private long ActionID;
		private String PromoCode;
		
		public long getRecipientGroupID() {
			return RecipientGroupID;
		}
		public void setRecipientGroupID(long recipientGroupID) {
			RecipientGroupID = recipientGroupID;
		}
		public long getActionID() {
			return ActionID;
		}
		public void setActionID(long actionID) {
			ActionID = actionID;
		}
		public String getPromoCode() {
			return PromoCode;
		}
		public void setPromoCode(String promoCode) {
			PromoCode = promoCode;
		}
	}
    
    /**
     * action name
     * @author EyalG
     *
     */
    class ActionName implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String ActionName;

		public String getActionName() {
			return ActionName;
		}

		public void setActionName(String actionName) {
			ActionName = actionName;
		}

	}
    
    /**
     * customers in optimov
     * @author EyalG
     * 
     *
     */
    class Customer implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String CustomerID;
		private long ActionID;
		private long ChannelID;
		
		public String getCustomerID() {
			return CustomerID;
		}
		
		public void setCustomerID(String customerID) {
			CustomerID = customerID;
		}
		
		public long getActionID() {
			return ActionID;
		}
		
		public void setActionID(long actionID) {
			ActionID = actionID;
		}
		
		public long getChannelID() {
			return ChannelID;
		}
		
		public void setChannelID(long channelID) {
			ChannelID = channelID;
		}
		
		public long getCustomerIDNumber() {
			return Long.valueOf(CustomerID.split("_")[0]);
		}
	}
    
    /**
     * Action Details
     * @author EyalG
     *
     */
    class ActionDetails implements Serializable {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private long RecipientGroupID;
		private long ActionID;
		private long Duration;
		private long LeadTime;
		private long ChannelID;
		
		public long getRecipientGroupID() {
			return RecipientGroupID;
		}
		
		public void setRecipientGroupID(long recipientGroupID) {
			RecipientGroupID = recipientGroupID;
		}
		
		public long getActionID() {
			return ActionID;
		}
		
		public void setActionID(long actionID) {
			ActionID = actionID;
		}
		
		public long getDuration() {
			return Duration;
		}
		
		public void setDuration(long duration) {
			Duration = duration;
		}
		
		public long getLeadTime() {
			return LeadTime;
		}
		
		public void setLeadTime(long leadTime) {
			LeadTime = leadTime;
		}
		
		public long getChannelID() {
			return ChannelID;
		}
		
		public void setChannelID(long channelID) {
			ChannelID = channelID;
		}

	}
}
