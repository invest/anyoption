package il.co.etrader.jobs;

import il.co.etrader.bl_vos.UserBase;

@SuppressWarnings("serial")
public class AutomaticMailTemplateMaintenanceFeeBean extends UserBase {
    private long fee;
    private String feeCollectTime;
    
    public long getFee() {
        return fee;
    }
    
    public void setFee(long fee) {
        this.fee = fee;
    }
    
    public String getFeeCollectTime() {
        return feeCollectTime;
    }
    
    public void setFeeCollectTime(String feeCollectTime) {
        this.feeCollectTime = feeCollectTime;
    }
}