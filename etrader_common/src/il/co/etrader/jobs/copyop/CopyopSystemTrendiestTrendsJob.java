package il.co.etrader.jobs.copyop;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BestHotDataManager;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;

public class CopyopSystemTrendiestTrendsJob implements ScheduledJob {
	//Every HH:05, HH:15, ??? HH:35, HH:45
    private static final Logger log = Logger.getLogger(CopyopSystemTrendiestTrendsJob.class);    
    
    private void insertSystemTrendiestTrends() {
        try {
        	ArrayList<FeedMessage> feedList = BestHotDataManager.getSystemTrendiestTrends();
        	
        	for(FeedMessage fm: feedList){
        		UpdateMessage updateMsg = new UpdateMessage(fm);            	
        		CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.USER_DELAY_SYSTEM_MSG, 0, 0);
        		log.debug("Sended [" + updateMsg + "] MSG ");
        	}
        	log.debug("***********SEND " + feedList.size() + " MSG*************");
        } catch (Exception e) {
            log.error("Can't insert System Trendiest Trend", e);
        }
    }

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {
    		long beginTime = System.currentTimeMillis();
        	log.debug("=====================BEGIN COPYOP SYSTEM 18 Trendiest Trend JOB====================="); 
        	
        	if(!isInPostMinutes()){
        		log.debug("Now time is not in Post Trendiest Trend time: HH:05, HH:15, HH:35, HH:45 ");
        		log.debug("=====================END COPYOP SYSTEM 18 Trendiest Trend JOB =====================");
        		return true;
        	}
        	
        	log.debug("Try to insert System Trendiest Trend...");
        	insertSystemTrendiestTrends();
        	log.debug("Finish to inserting proccess System Trendiest Trend");  
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END COPYOP SYSTEM 18 Trendiest Trend JOB in:" + (endTime - beginTime) + "ms =====================");
            return true;
			
		} catch (Exception e) {
			log.error("When run COPYOP SYSTEM Trendiest Trend Job",e);
			return false;
		}
    }

	private boolean isInPostMinutes() {
		
		List<Integer> postMinutes = new ArrayList<Integer>();
		postMinutes.add(0);
		postMinutes.add(1);
		postMinutes.add(3);
		postMinutes.add(4);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int nowMin = cal.get(Calendar.MINUTE)/10;
		if(postMinutes.contains(nowMin)){
			return true;
		} else{
			return false;
		}
	}

    public void stop() {
        log.info("CopyopSystemTrendiestTrendsJob stopping...");
    }
}