package il.co.etrader.jobs.copyop;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.copyop.migration.UsersMigrationManager;


public class CopyopMigrateUsersJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(CopyopMigrateUsersJob.class);

    Date lastRunTime;

	@Override
	public void setJobInfo(Job job) {
		lastRunTime = job.getLastRunTime();
	}

	@Override
	public boolean run() {
		log.debug("BEGIN COPYOP MIGRATE USERS JOB");
		try {
			UsersMigrationManager.exportUser(lastRunTime);
		} catch (SQLException e) {
			log.debug("Can't export users from job", e);
			return false;
		}
		log.debug("END COPYOP MIGRATE USERS JOB"); 
		return true;
	}

	@Override
	public void stop() {
		log.debug("STOP COPYOP MIGRATE USERS JOB"); 
		// TODO clean up
	}   
}
