package il.co.etrader.jobs.copyop;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BestHotDataManager;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;

public class CopyopTopProfitableJob implements ScheduledJob {
	 //run 00:00, 06:00, 12:00, 18:00;
    private static final Logger log = Logger.getLogger(CopyopTopProfitableJob.class);    
    
    private void getCopyopTopProfitableJob() {
        try {
        	ArrayList<FeedMessage> feedList = BestHotDataManager.getTopProfitable();
        	
        	for(FeedMessage fm: feedList){
        		UpdateMessage updateMsg = new UpdateMessage(fm.getUserId(), UpdateTypeEnum.AB_IM_TOP_PROFITABLE, fm.getProperties());            	
        		CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.FEED_DESTINATION_NAME);
        		log.debug("Sended [" + updateMsg + "] MSG ");
        	}
        	log.debug("***********SEND " + feedList.size() + " MSG*************");
        } catch (Exception e) {
            log.error("Can't insert System Trendiest Trend", e);
        }
    }

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {
    		long beginTime = System.currentTimeMillis();
        	log.debug("=====================BEGIN COPYOP  28 Top profitable JOB=====================");       	
        	getCopyopTopProfitableJob();   	
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END COPYOP 28 Top profitable JOB in:" + (endTime - beginTime) + "ms =====================");
            return true;
			
		} catch (Exception e) {
			log.error("When run Top profitable Job",e);
			return false;
		}
    }

    public void stop() {
        log.info("CopyopSystemTrendiestTrendsJob stopping...");
    }
}