package il.co.etrader.jobs.copyop;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.copyop.common.managers.RiskAppetiteManager;

public class CopyopCalculateRiskAppetiteJob implements ScheduledJob {

    private static final Logger log = Logger.getLogger(CopyopCalculateRiskAppetiteJob.class);   
    private static final int USER_RECORDS_COUNT = 10;

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {    	
    		long beginTime = System.currentTimeMillis();
    		
        	log.debug("=====================BEGIN COPYOP CALCULATE RISK APPETITE JOB=====================");       	
        	List<String> users = new ArrayList<String>();
        	users = RiskAppetiteManager.getUsersForCalculate(USER_RECORDS_COUNT);
        	
        	if(users.size() != 0){
        		int br = 1;
        		for(String userId : users){
        			log.debug("Run calclulate appetite thread " + br);
        			CopyopCalculateRiskAppetiteRunnable thr = new CopyopCalculateRiskAppetiteRunnable(userId);
        			thr.start();
        			br ++;
        		}
        		
        	} else {
        		log.debug("Not Found users for risk appetite calculate.");
        	}        
        	
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END COPYOP CALCULATE RISK APPETITE JOB in:" + (endTime - beginTime) + "ms =====================");
        	
        	
            return true;
			
		} catch (Exception e) {
			log.error("When run Copyop Hot Job",e);
			return false;
		}
    }

    public void stop() {
        log.info("CopyopHotTableJob stopping...");
    }
}