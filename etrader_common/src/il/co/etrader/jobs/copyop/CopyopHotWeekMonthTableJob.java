package il.co.etrader.jobs.copyop;


import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BestHotDataManager;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.managers.FeedManager;

public class CopyopHotWeekMonthTableJob implements ScheduledJob {
	private static final Logger log = Logger.getLogger(CopyopHotTableJob.class);  
	
	private static final String BEST_HOT_TRADERS_1W = "BEST_HOT_TRADERS_1W";
	private static final String BEST_HOT_TRADERS_1M = "BEST_HOT_TRADERS_1M";
	private static final String BEST_HOT_TRADERS_3M = "BEST_HOT_TRADERS_3M";

	private static final String BEST_HOT_TRADES_1W1M3M = "BEST_HOT_TRADES_1W1M3M";
	
	private static final String BEST_HOT_COPIERS_1W = "BEST_HOT_COPIERS_1W";
	private static final String BEST_HOT_COPIERS_1M = "BEST_HOT_COPIERS_1M";
	private static final String BEST_HOT_COPIERS_3M = "BEST_HOT_COPIERS_3M";

	private static final String BEST_HOT_ASSET_SPECIALISTS_1W = "BEST_HOT_ASSET_SPECIALISTS_1W";
	private static final String BEST_HOT_ASSET_SPECIALISTS_1M = "BEST_HOT_ASSET_SPECIALISTS_1M";
	private static final String BEST_HOT_ASSET_SPECIALISTS_3M = "BEST_HOT_ASSET_SPECIALISTS_3M";

    private String cfgBestHotType;
    private long oneWeekHours;
    private long oneMonthHours;
    private long threeMonthsHours;
    
    
    private void initHours(){
    	oneWeekHours = getHoursInPast(Calendar.WEEK_OF_YEAR, -1);
    	oneMonthHours = getHoursInPast(Calendar.MONTH, -1);
    	threeMonthsHours = getHoursInPast(Calendar.MONTH, -3);
    }
    
    private long getHoursInPast(int field, int pastNum){
		final int MILLI_TO_HOUR = 1000 * 60 * 60;
		GregorianCalendar nowCal = new GregorianCalendar();
		GregorianCalendar pastCal = new GregorianCalendar();
		pastCal.add(field, pastNum);
		
		long hours = (long)(nowCal.getTime().getTime() - pastCal.getTime().getTime())/MILLI_TO_HOUR;		
		return hours;
    }
    
    private void insertHotAssetSpecialists(String cfg) {
        try {
        	if(cfg.contains(BEST_HOT_ASSET_SPECIALISTS_1W)){
            	log.debug("getHotAssetSpecialists for ONE WEEK");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotAssetSpecialists(oneWeekHours, UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK));
        	}
        	if(cfg.contains(BEST_HOT_ASSET_SPECIALISTS_1M)){
            	log.debug("getHotAssetSpecialists for ONE MONTH");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotAssetSpecialists(oneMonthHours, UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH));
        	}
        	if(cfg.contains(BEST_HOT_ASSET_SPECIALISTS_3M)){
            	log.debug("getHotAssetSpecialists for THREE MONTHS");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotAssetSpecialists(threeMonthsHours, UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH));
        	}	
        } catch (Exception e) {
            log.error("Can't insert Asset Specialists 1W/1M/3M ", e);
        }
    }
    
    private void insertHotBestTraders(String cfg) {
        try {
        	if(cfg.contains(BEST_HOT_TRADERS_1W)){
            	log.debug("getHotBestTraders for ONE WEEK");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTraders(oneWeekHours, UpdateTypeEnum.BEST_TRADERS_ONE_WEEK));
        	}
        	if(cfg.contains(BEST_HOT_TRADERS_1M)){
            	log.debug("getHotBestTraders for ONE MONTH");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTraders(oneMonthHours, UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH));
        	}
        	if(cfg.contains(BEST_HOT_TRADERS_3M)){
            	log.debug("getHotBestTraders for THREE MONTHS");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTraders(threeMonthsHours, UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH));
        	}	
        } catch (Exception e) {
            log.error("Can't insert Best Traders 1W/1M/3M", e);
        }
    }
    
    private void insertHotBestTrades() {
        try {
        	log.debug("getHotBestTrades for ONE WEEK");
        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTrades(oneWeekHours, UpdateTypeEnum.BEST_TRADES_ONE_WEEK));
        	log.debug("getHotBestTrades for ONE MONTH");
        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTrades(oneMonthHours, UpdateTypeEnum.BEST_TRADES_ONE_MOTNH));
        	log.debug("getHotBestTrades for THREE MONTHS");
        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTrades(threeMonthsHours,  UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH));
        } catch (Exception e) {
            log.error("Can't insert Best Trades 1W/1M/3M", e);
        }
    }
    
    private void insertHotBestCopiers(String cfg) {
        try {
        	if(cfg.contains(BEST_HOT_COPIERS_1W)){
            	log.debug("getHotBestCopiers for ONE WEEK");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestCopiers(oneWeekHours, UpdateTypeEnum.BEST_COPIERS_ONE_WEEK));
        	}
        	if(cfg.contains(BEST_HOT_COPIERS_1M)){
            	log.debug("getHotBestCopiers for ONE MONTH");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestCopiers(oneMonthHours, UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH));
        	}
        	if(cfg.contains(BEST_HOT_COPIERS_3M)){
            	log.debug("getHotBestCopiers for THREE MONTHS");
            	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestCopiers(threeMonthsHours, UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH));
        	}
        } catch (Exception e) {
            log.error("Can't insert Best Copiers 1W/1M/3M", e);
        }
    }

    @Override
    public void setJobInfo(Job job) {
    	if (null != job.getConfig()) {
    		cfgBestHotType = job.getConfig();
    	}
    }

    @Override
    public boolean run() {
	try {
	    long beginTime = System.currentTimeMillis();
	    initHours();

	    log.debug("=====================BEGIN COPYOP HOT *" + cfgBestHotType + "* JOB=====================");
	    insertHotBestTraders(cfgBestHotType);

	    if (cfgBestHotType.contains(BEST_HOT_TRADES_1W1M3M)) {
		log.debug("Try to insert Hot Best Trades 1W/1M/3M...");
		insertHotBestTrades();
		log.debug("Finish to inserting proccess Hot Best Trades 1W/1M/3M");
	    }

	    insertHotBestCopiers(cfgBestHotType);

	    insertHotAssetSpecialists(cfgBestHotType);

	    long endTime = System.currentTimeMillis();
	    log.debug("=====================END COPYOP HOT *" + cfgBestHotType + "* JOB in:" + (endTime - beginTime)
		    + "ms =====================");
	    return true;

	} catch (Exception e) {
	    log.error("When run Copyop Hot " + cfgBestHotType + " Job", e);
	    return false;
	}
    }

    @Override
    public void stop() {
        log.info("CopyopHotTableJob " + cfgBestHotType + " stopping...");
    }
}