package il.co.etrader.jobs.copyop;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BestHotDataManager;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.updates.UpdateMessage;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.CopiedManager;

public class CopyopSystemMostCopiedtJob implements ScheduledJob {
	//The post will be generated at: 24:00, 3:00, 6:00, 12:00, 15:00, 18:00 , 21:00
    private static final Logger log = Logger.getLogger(CopyopSystemMostCopiedtJob.class);        
    private void insertSystemMostCopied() {
        try {
        	ArrayList<FeedMessage> feedList = BestHotDataManager.getSystemMostCopied();        	        	
        	int i = 1;
        	
        	for(FeedMessage fm: feedList){
        		UpdateMessage updateMsg = new UpdateMessage(fm);
            	int delay = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_MOST_COPIED_DELAY_MSG_" + i, "0"));
        		CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.USER_DELAY_SYSTEM_MSG, delay, 0);
        		i ++;
        		log.debug("Sended [" + updateMsg + "] MSG with delay:" + delay);
        	}
        	log.debug("***********SEND " + feedList.size() + " MSG*************");
        } catch (Exception e) {
            log.error("Can't insert System Most Copied", e);
        }
    }
    
    private void deleteSystemMostCopied() {
        try {
        	CopiedManager.deleteCopiedCounters(73);
        } catch (Exception e) {
            log.error("Can't insert System Most Copied", e);
        }
    }

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {
    		long beginTime = System.currentTimeMillis();
        	log.debug("=====================BEGIN COPYOP SYSTEM 15 Most Copied JOB=====================");       	
        	log.debug("Try to insert System Most Copied...");
        	insertSystemMostCopied();
        	log.debug("Finish to inserting proccess System Most Copied");  
        	  
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END COPYOP SYSTEM 15 Most Copied JOB in:" + (endTime - beginTime) + "ms =====================");
            return true;
			
		} catch (Exception e) {
			log.error("When run COPYOP SYSTEM Most Copied Job",e);
			return false;
		}
    }

    public void stop() {
        log.info("CopyopSystemMostCopiedtJob stopping...");
    }
}