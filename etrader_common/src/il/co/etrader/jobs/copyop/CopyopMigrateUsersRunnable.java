package il.co.etrader.jobs.copyop;

import java.util.Date;

import org.apache.log4j.Logger;

import com.copyop.migration.UsersMigrationManager;

public class CopyopMigrateUsersRunnable implements Runnable {
	private static final Logger log = Logger.getLogger(CopyopMigrateUsersRunnable.class);  
	private Thread t;
	private Date date;

	CopyopMigrateUsersRunnable(Date date) {
		this.date = date;
	}

	public void run() {
		log.debug("Running migration of eligible users");
		long beginTime = System.currentTimeMillis();
		try {
			UsersMigrationManager.exportUser(date);
		} catch (Exception e) {
			log.error("Migration of eligible users error ", e);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Migration done in:" + (endTime - beginTime) + "ms");
	}

	public void start() {
		System.out.println("Starting migration of eligible users since " + date);
		if (t == null) {
			t = new Thread(this, "Migration to copyop");
			t.start();
		}
	}
}
