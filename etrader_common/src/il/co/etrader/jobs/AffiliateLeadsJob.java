package il.co.etrader.jobs;

import java.io.BufferedReader;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.DbParametersManager;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_vos.AffiliateLeadSegmob;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import org.apache.commons.lang.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.CountryManagerBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AffiliateLeadsJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(AffiliateLeadsJob.class);
	
	public final static int RESPONSE_STATUS_SUCCESS = 200;
	public final static String API_URL_SG = "api_url";
	public final static String HTTP_GET_HEADER_KEY_SG = "SG-API-KEY";
	public final static String AFFILIATE_KEY = "affiliate_key";
	public final static String AFFILIATE_CODE = "affiliate_code";
	
	private static String apiUrl;
	private static String httpGetHeaderValueSg;
	private static String affiliateKey;
	private static String affiliateCode;
	private static Hashtable<Long, Skin> skinList;

	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals(API_URL_SG)) {
                	apiUrl = p[1].toString();                	
                } else if (p[0].equals(HTTP_GET_HEADER_KEY_SG)) {
                	httpGetHeaderValueSg = p[1].toString();
                } else if (p[0].equals(AFFILIATE_KEY)) {
                	affiliateKey = p[1].toString();
                } else if (p[0].equals(AFFILIATE_CODE)) {
                	affiliateCode = p[1].toString();
                }
            }
		}
	}

	@Override
	public boolean run() {
		log.info("AffiliateLeadsJob - process START.");
		try {
			/* Get time Last Run */
			Date timeLastRun = DbParametersManager.getLastRun(DbParametersManager.DB_PARAMS_AFFILIATE_LEADS);
			Date currentDate = new Date();
			timeLastRun = DateUtils.addHours(timeLastRun, -2);
			String fromDate = formatDate(timeLastRun);
			fromDate = fromDate.replace(" ", "%20");
			/* Load info from DB */
			loadData();
			/* Do get request to affiliate */
			AffiliateLeadSegmob[] alsList = doGetAcceptingAllCertificates(fromDate);
			if (null != alsList && alsList.length > 0) {
				List<AffiliateLeadSegmob> affiliateLeadList = (List<AffiliateLeadSegmob>) Arrays.asList(alsList);
				/* Check validation of leads */
				ArrayList<Contact> contactList = getValidLeads(affiliateLeadList);
				if (null != contactList && contactList.size() > 0) {
					/* Insert contact list into DB */
					ContactsManager.insertContactList(contactList);
				}
			}
			/* update time Last Run */
			DbParametersManager.updateLastRun(DbParametersManager.DB_PARAMS_AFFILIATE_LEADS, currentDate);
			log.info("AffiliateLeadsJob - process END.");
		} catch (Exception e) {
			log.error("Problem with AffiliateLeadsJob.", e);
		}
		return true;
	}

	@Override
	public void stop() {
		log.info("AffiliateLeadsJob - STOP.");
	}
	
	/**
	 * Load Data from DB
	 * @throws SQLException
	 */
	public static void loadData() throws SQLException {
		// get all skins
		skinList = SkinsManagerBase.getAll();
		Set<Long> keys = skinList.keySet();
		Iterator<Long> itr = keys.iterator();
		while (itr.hasNext()) {
	       Long skinId = itr.next();
	       Skin skin = skinList.get(skinId);
	       skin.setNetreferCombinationId(SkinsManagerBase.getNetreferCombinationsId(skin.getId()));
		}
	}
	
	/**
	 * Do get request which accepting all certificates
	 * @param fromDate
	 * @return AffiliateLeadSegmob[]
	 * @throws Exception
	 */
	public static AffiliateLeadSegmob[] doGetAcceptingAllCertificates(String fromDate) throws Exception {
		DefaultHttpClient httpClient = null;
		try {
			BufferedReader reader = null;
		    TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
		        @Override
		        public boolean isTrusted(X509Certificate[] certificate, String authType) {
		            return true;
		        }
		    };
		    SSLSocketFactory sf = new SSLSocketFactory(acceptingTrustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		    SchemeRegistry registry = new SchemeRegistry();
		    registry.register(new Scheme("https", 443, sf));
		    ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);
		    httpClient = new DefaultHttpClient(ccm);
		    HttpGet getMethod = new HttpGet(apiUrl + "from_date=" + fromDate);
		    getMethod.addHeader(HTTP_GET_HEADER_KEY_SG, httpGetHeaderValueSg);
		    getMethod.addHeader("Content-Type", "application/json");
		    HttpResponse response = httpClient.execute(getMethod);
		    log.debug("Status: " + response.getStatusLine().toString());
		    if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
		    	String jsonResponse = EntityUtils.toString(response.getEntity());
		    	log.debug("Content" + jsonResponse);
		    	Gson gson = new GsonBuilder().serializeNulls().create();
		    	AffiliateLeadSegmob[] affiliateLeadSegmobList = gson.fromJson(jsonResponse, AffiliateLeadSegmob[].class);
		    	return affiliateLeadSegmobList;
		    }			
		} finally {
			// When HttpClient instance is no longer needed, shut down the connection manager to ensure immediate deallocation of all system resources
			httpClient.getConnectionManager().shutdown();
		}
		return null;
	}
	
	/**
	 * Check validation of leads
	 * @param list
	 * @return ArrayList<Contact>
	 */
	public ArrayList<Contact> getValidLeads(List<AffiliateLeadSegmob> list) {
		ArrayList<Contact> contactList = new ArrayList<Contact>();
		for (AffiliateLeadSegmob als : list) {
			Contact contact = new Contact();
			boolean isEmailValid = false;
			boolean isMobileValid = false;
			int mobileLengVal = 6;
			String numToken = "\\b\\d+\\b";
			String email = als.getEmail();
			String mobilePhone = als.getReal_phone_number();
			String countryCode = als.getCountry();
			long countryId = 0;
			long skinId = 0;
			Long combId = null;
			/* TrackID */
			contact.setAff_sub1(als.getTrackID());
			contact.setAff_sub2(als.getStory_short_url());
			boolean isContactExists = false;
			try {
				isContactExists = ContactsManager.isContactByAffiliateExists(contact);
			} catch (SQLException ex) {
				log.error("Can't check if contact by affiliate exists.", ex);
			}
			if (!isContactExists) {
				/* email */
	        	if (!CommonUtil.isParameterEmptyOrNull(email) && CommonUtil.isEmailValidUnix(email)) {
	        		isEmailValid = true;
	        		contact.setEmail(email);
	        	}
	        	/* phone */
	        	if (!CommonUtil.isParameterEmptyOrNull(mobilePhone) && mobilePhone.length() > mobileLengVal && mobilePhone.matches(numToken)) {
	        		isMobileValid = true;
	        		contact.setMobilePhone(mobilePhone);
	        	}
	        	/* country id */
	        	if (!CommonUtil.isParameterEmptyOrNull(countryCode)) {
	        		try {
						countryId = CountryManagerBase.getIdByCode(countryCode);
	        		} catch (SQLException e) {
	        			log.error("Can't get country id by country code.", e);
					}
	        	}
	        	contact.setCountryId(countryId);
	        	/* skin id */
	        	try {
	    			skinId = SkinsManagerBase.getSkinIdByCountryAndUrl(countryId, ConstantsBase.ANYOPTION_URL);
	    			if (skinId == 0) {
	    				skinId = Skin.SKIN_REG_EN;		
	    			}
	    		} catch (SQLException e) {
	    			log.error("Can't get skinId", e);		
	    		}
	        	contact.setSkinId(skinId);
	        	/* combination id */
	        	combId = skinList.get(skinId).getNetreferCombinationId().get(0);
	        	/* Set members into contact */
	        	contact.setCombId(combId);
	        	contact.setUtcOffset(ConstantsBase.OFFSET_GMT);
	        	contact.setDynamicParameter("API_" + affiliateKey + "_" + affiliateKey + "_" + affiliateCode);
	        	contact.setContactByEmail(true);
	        	contact.setContactBySMS(true);
	        	contact.setAffiliateKey(Long.valueOf(affiliateKey));
	        	contact.setFirstName(als.getFirst_name());
	            contact.setLastName(als.getLast_name());
	            contact.setWriterId(Writer.WRITER_ID_AUTO);
	            contact.setType(Contact.CONTACT_US_SHORT_REG_FORM);
	            /* connect userId in short form */
	        	long userId = 0;
	    		try {
	    			userId = ContactsManager.searchUserIdFromContactDetails(contact);
	    			contact.setUserId(userId);
	    		} catch (SQLException e) {
	    			log.error("Can't get user id.", e);
	    		}
	            
	            if (countryId != 0 && (isEmailValid || isMobileValid)) {
	            	contactList.add(contact);
	            } else {
	            	log.info("Lead not valid, It will not insert into our system. " + als.toString());
	            }
			}
		}
		return contactList;
	}
	
	/**
	 * Format date
	 * @param val
	 * @return String
	 */
	public static String formatDate(Date val) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(val);
	}
}
