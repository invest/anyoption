package il.co.etrader.jobs;

import java.io.IOException;
import java.net.URLEncoder;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.util.JobUtil;

/**
 * 
 * ClearingProviderDemoJob 
 * @author eyalo
 * 
 */
public class ClearingProviderDemoJob extends JobUtil {
	private static final Logger log = Logger.getLogger(ClearingProviderDemoJob.class);
	private static String username;
	private static String password;
	private static String terminalNumber;
	private static String url;
	private static String ccNum;
	
	public static void main(String[] args) {
		log.debug("------------");
		log.debug("ClearingProviderDemoJob Start.");
		log.debug("------------");
		
		// Get properties.
		getPropertiesByFile(args);
		
		// Simulate request.
		ClearingInfo ci = new ClearingInfo();
		try {
			request(ci, true, false);
		} catch (ClearingException ce) {
			log.error("Failed to authorize transaction: " + ci.toString(), ce);
		}
		
		log.debug("------------");
		log.debug("ClearingProviderDemoJob End.");
		log.debug("------------");
	}
	
	public static void getPropertiesByFile(String[] args) {
		propFile = args[0];
		username = JobUtil.getPropertyByFile("provider.username");
		password = JobUtil.getPropertyByFile("provider.password");
		terminalNumber = JobUtil.getPropertyByFile("provider.terminal.number");
		url = JobUtil.getPropertyByFile("provider.url");
		ccNum = JobUtil.getPropertyByFile("transaction.cc.num");
	}
	
    public static void request(ClearingInfo info, boolean authorize, boolean isCFT) throws ClearingException {
        if (log.isTraceEnabled()) {
            log.trace(info.toString());
        }
        try {
            String request = createRequest(info, authorize, isCFT);
            if (log.isTraceEnabled()) {
                log.trace(request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
            // Only for 3ds secure
            // response = response.replaceAll("&", "&amp;");
            parseResponse(response, info);
        } catch (Throwable t) {
            throw new ClearingException("Transaction failed.", t);
        }
    }
    
    public static String createRequest(ClearingInfo info, boolean authorize, boolean isCFT) {    	
    	// first request - 3DSLookup.
    	return request3DSLookup();
    	
    	// second request - 3DAuth.
    	// return request3DAuth();
		
    	// third request - Auth/Capt.
    	// return request3DSAuthCapt();
    }
    
    public static String request3DSLookup() {
    	// first request - 3DSLookup.
    	String xmlRequest = "<ashrait>" +
    							"<request>" +
    								"<version>1001</version>" +
    								"<language>ENG</language>" + 
    								"<dateTime/>" +
    								"<command>doDeal</command>" +
    								"<requestid/>" +
    								"<doDeal>" +
    									"<terminalNumber>" + terminalNumber + "</terminalNumber>" +
    									"<cardNo>" + ccNum + "</cardNo>" +
    									"<cardExpiration>1218</cardExpiration>" +
    									"<total>1500</total>" +
    									"<transactionType>Debit</transactionType>" +
    									"<creditType>RegularCredit</creditType>" +
    									"<currency>USD</currency>" +
    									"<transactionCode>Phone</transactionCode>" +
    									"<validation>3DSLookup</validation>" + 
    									"<email>buyer@yahoo.com</email>" +
    									"<clientIp>201.47.87.121</clientIp>" +
    									"<cg3dsData>" +
    										"<cg3dsEnrolled>U</cg3dsEnrolled>" +
    										"<cg3dsLiability>U</cg3dsLiability>" +
    										"<cg3dsPaResStatus>U</cg3dsPaResStatus>" +
    										"<cg3dsReturnUrl>https://eyalo.testenv.anyoption.com/anyoption/jsp/tbi3dsStatusURL.jsf</cg3dsReturnUrl>" +
    										"<cg3dsUserAgent> Mozilla/4.0 (compatible; Win32; WinHttp.WinHttpRequest.5) </cg3dsUserAgent>" +
    										"<cg3dsBrowserHeader>*/*</cg3dsBrowserHeader>" +
    										"<cg3dsOrderDescription>Order 182397541265</cg3dsOrderDescription>" +
    										"<cg3dsOrderNumber>182397541265</cg3dsOrderNumber>" +
    									"</cg3dsData>" +
    								"</doDeal>" + 
    							"</request>" + 
    						"</ashrait>";
    	return xmlRequest;
    }
    
    public static String request3DAuth() {
    	// second request - 3DAuth.
    	String xmlRequest = "<ashrait>" +
								"<request>" +
									"<version>1001</version>" +
									"<language>ENG</language>" + 
									"<dateTime/>" +
									"<command>doDeal</command>" +
									"<requestid/>" +
									"<doDeal>" +
										"<terminalNumber>" + terminalNumber + "</terminalNumber>" +
										"<cardNo>" + ccNum + "</cardNo>" +
										"<cardExpiration>1218</cardExpiration>" +
										"<total>1500</total>" +
										"<transactionType>Debit</transactionType>" +
										"<creditType>RegularCredit</creditType>" +
										"<currency>USD</currency>" +
										"<transactionCode>Phone</transactionCode>" +
										"<validation>3DSAuth</validation>" + 
										"<cg3dsData>" +
											"<cg3dsEnrolled>Y</cg3dsEnrolled>" +
											"<cg3dsLiability>U</cg3dsLiability>" +
											"<cg3dsPaResStatus>U</cg3dsPaResStatus>" +
											"<cg3dsPaRes>eNqdmElz6kgSgO9E8B8cPUe6Wwti68COKC1oAQm0I920oQVJoF3o108B9rPnjaP7zfjiUpKVlVWZ+VVKay0qg4BWA68pg7e1GFSVEwYvsf/6W+gyxDIjcjRs52w/39buHP3tbX0ASlA9FB6jRRfwheZFARU0drd0DpMhT9OcIoTqFWq3QVnFl/wN+xP9E18jH49wodKLnLx+WzteQfLSG4ZPidl8sVwj74J1FpQ8/eYHJ6dJ6zXyfFwjnzMPzX1UQbf72H+zcWEj0VEtsURnTy1cmYa4OlViKQGva+SusfadOnjDUXSBzvHZC0b8hWJ/oXDFh3x9vZsD2aWBtrEZiq6Rr5I1PKAyyL3b25KAP/14Wgf99ZIHUANu78d4jXw6d3XyN/Q//xbQNpSutePbuo6zr06t7k5hUOEhX1e1UzfVm7VG3kdrz2nbt4TZ6W7ITrRTggKFAZS48yknmFz4Cm72obIOvPgNvTsF/z9mgTS8lHEdZW/Tp86nYI3cXUEeEX1bq3GYw8XK4KXP0hzGMarr618I0nXdn930z0sZItBhFEFXCFTwqzj812/PWYHP56fL/zSNcvJLHntOGg9ODXNDDOro4r/88O07M5pyt4QhCkP9AU394WFE/sddgk6xGbSJfG/0y85+ZZWfnS0r548qcrD7Aj8ZelsrwSm4Z0Twoiv862//+pXioOMwqOr/x5UPN75a+LBnOGkTvLkEq+qb2W1HLZ356hbeltKRQqpz7YivH/Oemmvkh+/vG3tG8ctpPRWNhm3E4/yQEaCk8D3qLWLDpM+L7SRSrozZ2QdGsLdJ5psUYR2s+kAi2DFFdrbV5Cxd7Didv0bbeD8eEYlLy657Lm6LzMIjlSwaO5uxzVJNhRTQ7k1abDSns/eecPL6fYyddzYxSemlPWxsECJtQwrKwpqNR2p8slLrZncL3CVXpjKbynFJZfpyitEh1cRmOwjOUK+QiWkVV73syPDgyWArLc1kmyUYJgeNO7Pk8YixyEg/Hc6Mluhb4BYHwrAvA3ftralOHOmtLBRCuGSEoxFaqnTBrmG9P5aZNciIrFV13eTX3JjbxXi0b+en6KrfNifm6GJNRmJSH+wKVp0ERI70uqv43OQU44lDc8dyN3F22mYKXl+/ZNV7ZLbB7RmJ4wxd0U7tPEdq4yaBV0sOJAQlvVJO6ce5k77QQXZ5eQSw/P1lr3/+ACgVCj6fqUsGceoFv7/sXkXIqwvUV7XXfRRffn+hXnV1jfy8zmNhKijr+ATrCvJS5HnaHCgKVGoIOp4EIU8Ble9o2RK2F5uPWk8CMrMhZdDZkFciOLMA0xkyEinDEHtaAzsylIzxCE4VSWZ1ccxlzyXAu0tJcBE3uhR5mXJ1s00l46vaNY2bheshHN88mlFFEjwsUl0niOf0PB7Z5gx1TPsqKnLHhBZtyDJNg1q3jxLKcxJmxSQtayDYdGgv0sxNTLwe3g8ziRYcKLs9ZXI/Hn1IRW7ZUwMQnj5ZGkgNTZTRjuoe1nmmUw4Om7aivLzv/C7jmM6W7p64Ux5aYhNgve9IY77siGek1jWx1MukVFT0jnla3DFdT1tH4eyYs8ilSFLGxJ7TgAvP6WFF1DbMVytK6prLUMfT3M2MG4wKySdAIsNzEZ1jdtWh5D0KAOwpIC/B/ffxiAq38IkBBTP1sVjIiiglfZzHBPKw0ece4VkJ2DWVqawYRlImfeau0vBonWsp4dgwuExJp69VFvo0JwgOW6wiJ2ytTIyTgOS9rkbmKhLQu0ZZiSuRntDTzvO8vQ4544PozF9bRp6b2hVdyjMdzfl8UY5HUcUv5GJv+DzqHwUwpC4R1cwN0Fv9Jm54n01vi7TbVAaS7GVUFiuSjKdqJ+i0aqZJc5QnunDAnd14hIeudPRdsiXPGM6FxZHmZ7wV7K960WJNax6Vqy8Kk3Ofi+jNKAi+WkxxEqiksMwj2tq3Ss4dh4Ibj9CAUaMu8iJxTlu7SdEWBEvFnNEtDY1qdk2xq2QI5e1lP5wrrN9zVSb7Zyea4Q0idjwNZEBeplU3HgXaM4c4RWRAAoBIdSxlUqwKWHbC8DAt5dMmwg4eazQ6Jok846f3eFpHJbUpkraPwiOzxyML36C2BvyHNZlgNqGsKyGr6LFryG3UU1gJ2lCud0iInR2+4+55paAJSYbd5gL0Ex+LsY+h4xFmqstYIgP0IkbmGZ1vB+a76qVDiwGSJhwZdHlsrRLxZycezYX9frVqjIMLK7jd4jW9kc5eoJSqpdcMRmVEQOwvQ4/XJkFX0rSYVShi9jPzeojbduOeGE3vtIyNwot7bsqT0E4W8/EI4abTa4gZttQNLjPLKgW7UiebolBFcJzJuTlQjW0T8ZwuV8VKLHcHLGnO4WToEcJH5WUZnnWbYl6fAPvKq+8ARqE0BFiSfQHYt0fQBfSvAowe7sV3L1RPhPmXurnSegkTiGT3hFXYGTK+udls2lhmT7s4VsPgJrZK0uOROxU6UeE7BjxhQIPzTTWVu5WrnZ37jQa0dwyI9EZJvakc6tmq9an/BNt4JGp3tDGDpEmfaNPkHzKRtf4WbDR4gg1aUoiOe/qzpUnjE05fAHaHmZeDnkmA/LRYiZQmCDZMZ1i5mQPRBy2R/OMMQNftDNYYoNeDb/KhjDJdaC9/PnnymXwohBYrgAsLN33rsJkCoco7e0CwHmk6HIsN+tx1LideOeT7esOm0bVo593OYTv9dJrMztZ1ITLZjdfFhmMkB7TKORbU+katYCtyzPbzcOHv6OU2Z7E5tcrLPlleSCKbQizpRDY5WeaezbMoO1zmQ0S2BVm1p2E1jZhwe5TmjL+VGiMcj5piqM052O07XZq7ZLGV1ek9MQBgk2AgZ+/54zOdTIkAdDtgCRZv88ByCTlkJJJcmpy+6h7XmKajXajgRgNzI/JZ4wxPOPUeWZAmdyiIJPqwRoeySZLKMuZvwvSoGfbuxqObhRreNs54pGmXUtTA6QELVWRYGpghqTBhckAw0w1Ty8ZZts8p0ynyWOhgDv10dej3qwNeGaFMLputjqymS8TYzqiNLsQyou+PDl5yN6wHUzfHMGxPzVNyXvlyvZCSSPairRbvNLkKZyekUFMJG4/u/WBLAH6r7bc2w5axUABtLpyLyfYY1bRKhElx3eTLfWIXsMs7OSWjTHx3NkMEtb02XJi4vXZagPEo62JY/wOtC4RhNLG1XWrFJr3hRmlwm/D1VyHQJhACcfABAQEIdH+YMKmhRFsRoCylwt6Nd6f0IyV1IHLhf5UOTO5viudrV0CDz67g77ocCIF/6HM+u5wfXQPscvpnl7MRWhdC4V5S4xEXeRIMfy8mzFTUrE7SwM28y5KHDP0hS0hClKuOevrKMp1g6AMjiaB6rAw7BtAxGh5d3YE53M/kAcKuYzXzXuCr269CYjz6B0wYIil+QEJ8h4Tq4isUziYhCE4fEYG9/3tMYJICSAMJ0BQZy1sylCkiKE9uq8gMdSyt1bw+LOS9WVAawJMcmSXTFRIiBeqKsSdTynhU4rAfX5yH7Y28yYV95gZLmC2nCDi2W8Hp0HqjIcmUSfzwHPTGNbXUoGAOs71Oi0ToqKdFwYmhcYBlsnUuOd6WCdebAWX0m5joCfLip0J+ShGkZpMtWkj0UC/OCCEF6sa8uUUsv3cKGOi05Hmzw9h93O08PyArIyMEk/WjqLK7NCuuROwxzH/d7d/pjkcf2jR4aGsk0DsYThIZvi/2UAbxZkmyKbnpxEASAia2utyB70h2rXNl7CR67c5n2HxZS6cryzDTcuBccnYRM8Y79pXr1rxCNm10WGZ1GNgplzY7eUALZeLhl1naBeNR3y2S9AS4ooBRVhNqu+OaaolIl3zhTRZxoQ61zImkEbgLfN45h5bl3SOz8yTzsnIPoVKYOn61I9gxbG82twLfFjvy+dqE/HiV+nzJenxMenzvun//+Pod7N834jRL</cg3dsPaRes>" +
											"<cg3dsTranId>gbE48m4n0gv6Gx6Ktb60</cg3dsTranId>" +
										"</cg3dsData>" +
									"</doDeal>" + 
								"</request>" + 
							"</ashrait>";
		return xmlRequest;
    }
    
    public static String request3DSAuthCapt() {
    	// third request - Auth/Capt.
		String xmlRequest = "<ashrait>" +
								"<request>" +
									"<version>1001</version>" +
									"<language>ENG</language>" + 
									"<command>doDeal</command>" +
									"<requestid/>" +
									"<mayBeDuplicate>0</mayBeDuplicate>" +
									"<doDeal>" +
										"<terminalNumber>" + terminalNumber + "</terminalNumber>" +
										"<cardNo>" + ccNum + "</cardNo>" +
										"<cardExpiration>1218</cardExpiration>" +
										
										// additional info.
										"<cardId/>" + 
										"<cvv></cvv>" + 
										
										"<total>1500</total>" +
										"<transactionType>Debit</transactionType>" +
										"<creditType>RegularCredit</creditType>" +
										"<currency>USD</currency>" +
										"<transactionCode>Phone</transactionCode>" +
										"<validation>Verify</validation>" + 
										
										// additional info.
										"<cavv>8C42D46E01BE4DF8F401110008C2DD09A7BEA08B</cavv>" +
										"<eci>5</eci>" +
										"<xid>67624534386D346E306776364778364B74623630</xid>" +
										"<customerData>" +
											"<extendedTransactionType>ECOMMERCE</extendedTransactionType>" +
											"<extendedPaymentType>SALE</extendedPaymentType>" +
										"</customerData>" +
										
										"<cg3dsData>" +
											"<cg3dsEnrolled>Y</cg3dsEnrolled>" +
											"<cg3dsLiability>Y</cg3dsLiability>" +
											"<cg3dsPaResStatus>Y</cg3dsPaResStatus>" +
										"</cg3dsData>" +
									"</doDeal>" + 
								"</request>" + 
							"</ashrait>";
				return xmlRequest;
    }
    
    public static void parseResponse(String response, ClearingInfo info) throws ParserConfigurationException, SAXException, IOException {
        Document respDoc = ClearingUtil.parseXMLToDocument(response);
        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
        info.setResult(ClearingUtil.getElementValue(root, "result/*"));
        info.setMessage(ClearingUtil.getElementValue(root, "message/*"));
        info.setUserMessage(ClearingUtil.getElementValue(root, "userMessage/*"));
        info.setProviderTransactionId(ClearingUtil.getElementValue(root, "tranId/*"));
        info.setSuccessful(info.getResult().equals("000"));

        //verify result from TBI is numeric
        try {
            Integer.parseInt(info.getResult());
        } catch (NumberFormatException nfe) {
            log.log(Level.WARN, "TBI result not a number.", nfe);
        }

        if (info.isSuccessful()) {
            info.setAuthNumber(ClearingUtil.getElementValue(root, "doDeal/authNumber/*"));
            info.setAcquirerResponseId(ClearingUtil.getElementValue(root, "doDeal/customerData/acquirerResponseId/*"));
        }

        if(info.isFirstTimeTBI()){
        	info.setRecurringTransaction(ClearingUtil.getElementValue(root, "doDeal/customerData/recurringTransactionSecurity/*"));
        }
        	
        if (log.isDebugEnabled()) {
            log.debug(info.toString());
        }
        
        // Only for 3ds secure
//        String redirectHtml = ClearingUtil.getElementValue(root, "doDeal/cg3dsData/cg3dsRedirectHtml/*");
//        redirectHtml(redirectHtml);
    }
    
    public static String redirectHtml(String html) {
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(html);
		} catch (IOException e) {
			log.debug("Can't change skin, response writer problem", e);
			return "";
		}
		context.responseComplete();
		return null;
    }
}
