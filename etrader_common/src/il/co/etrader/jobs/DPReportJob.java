package il.co.etrader.jobs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.Utils;

import il.co.etrader.bl_managers.MarketingReportManager;
import il.co.etrader.bl_vos.MarketingReport;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class DPReportJob implements ScheduledJob {
	public static final int CUSTOMER_REPORT_JOB_ID = 29;
	private static Logger log = Logger.getLogger(DPReportJob.class);

	private static final long SKIN_ID = 0;
	private static final long MARKETING_SOURCES_ID = 0;
	private static final long CAMPAIGN_MANAGER_ID = 934; //ELIYAK
	
	private static int MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT = 11;
	private static String mailSender = "support@anyoption.com";
		
	public void setJobInfo(Job job) {
        // do nothing
    }
	
	public boolean run() {
		
		log.info("DP Report Job started: " + System.currentTimeMillis());

		String DELIMITER = ",";

		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.add(Calendar.HOUR_OF_DAY, -1);
		Date endDate = gc.getTime();  
		Date startDate = CommonUtil.addDays(endDate, -2);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String reportName = "dp_report_by_date";
		String filename = reportName + "_" +  sf.format(startDate) + "_till_" + sf.format(endDate) + ".csv";
		String filePath = filename;
		File file;
		File attachment = null;
		ArrayList<MarketingReport> report = null;
		long reportSize = 0;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM, "false");

		if (!env.equals("local")) {
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}
		
		try {
			try {
				report = MarketingReportManager.getDPReportByDate(SKIN_ID, 0, startDate, endDate, "", MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT, MARKETING_SOURCES_ID, CAMPAIGN_MANAGER_ID, 0);
				reportSize = report.size();
			} catch (SQLException e) {
				errorStr = "Error in getting DP Report Data " + e.toString();
				log.error(errorStr,e);
				return false;
			}
			
			boolean isHaveData = false;
		
			if (errorStr == null) {
				if (reportSize > 0) {		
					file = new File(filePath);
					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
					output.append("Dates");
					output.append(DELIMITER);
					output.append("Dynamic Parameter");
					output.append(DELIMITER);
					output.append("Campaign manager");
					output.append(DELIMITER);
					output.append("Combination Id");
					output.append(DELIMITER);
					output.append("Campaign Name");
					output.append(DELIMITER);
					output.append("Source Name");
		        	output.append(DELIMITER);
		        	output.append("Medium");
		        	output.append(DELIMITER);
		        	output.append("Content");
		        	output.append(DELIMITER);
		        	output.append("Market Size Horizontal");
		        	output.append(DELIMITER);
		        	output.append("Market Size Vertical");
		        	output.append(DELIMITER);
		        	output.append("Location");
		        	output.append(DELIMITER);
		        	output.append("Landing Page Name");
		        	output.append(DELIMITER);
		        	output.append("Skin Id");
		        	output.append(DELIMITER);
		        	output.append("Short reg");
		        	output.append(DELIMITER);
		        	output.append("Registered Users Num");
		        	output.append(DELIMITER);
		        	output.append("FTD");
		        	output.append(DELIMITER);
		        	output.append("Reg First Deposit");
		        	output.append(DELIMITER);
		        	output.append("First remarketing Depositors Num");
		        	output.append(DELIMITER);
		        	output.append("House Win");
		        	output.append(DELIMITER);
		        	output.append("\n");
		
		
		        	for (MarketingReport row:report) {
		                isHaveData = true;
		
		                output.append(DateFormatUtils.format(row.getDates(), "dd-MM-yyyy"));
		                output.append(DELIMITER);
		                output.append(row.getDynamicParam());
		                output.append(DELIMITER);
		                output.append(row.getCampaignManager());
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getCombId()));
		                output.append(DELIMITER);
		                output.append(row.getCampaignName());
		                output.append(DELIMITER);
		                output.append(row.getSourceName());
		                output.append(DELIMITER);
		                output.append(row.getMedium());
		                output.append(DELIMITER);
		                output.append(row.getContent());
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getMarketSizeHorizontal()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getMarketSizeVertical()));
		                output.append(DELIMITER);
		                output.append(row.getLocation());
		                output.append(DELIMITER);
		                output.append(row.getLandingPageName());
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getSkinId()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getShortReg()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getRegisteredUsersNum()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getFtd()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getRfd()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getFirstRemDepNum()));
		                output.append(DELIMITER);
		                output.append(String.valueOf(row.getHouseWin()));
		
		                output.append("\n");                            
		        }
		
		        output.flush();
		        output.close();
		
				}
				if (!isHaveData) {
					errorStr = "No data was Found.";
				}
			}
		} catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
        	log.error(errorStr,e);
        	return false;
		}
	
		if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += " Hourly DP report <br/> " +
        				  " Parameters: <br/> " +
        				  " Start Date: " + sdf.format(startDate) + " <br/> " +
        				  " End Date: " + sdf.format(endDate);
        } else {
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
	    email.put("to", Utils.getProperty("dp.hourly.report")); 
	    email.put("from", mailSender); 	    
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);

		log.info("DP Report Job Job ended: " + System.currentTimeMillis());
		attachment.delete();
		return true;
	}
	
    public void stop() {
        log.info("DPReportJob stopping...");
    }
    

}
