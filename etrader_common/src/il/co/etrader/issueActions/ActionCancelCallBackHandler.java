package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionCancelCallBackHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionCancelCallBackHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		if (null != popUserEntry){
			long entryTypeId = popUserEntry.getEntryTypeId();

			if (!(ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryTypeId)){
				addDisplayMsg("issue.action.only.for.call.back", context, log, null);
				return false;
			}

			int statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_REMOVE_MANUALLY;

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}else {
			addDisplayMsg("issue.action.only.for.sales", context, log, null);
			return false;
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		updateEntryType(conn, popUserEntry, popHisStatus, action);
	}
}
