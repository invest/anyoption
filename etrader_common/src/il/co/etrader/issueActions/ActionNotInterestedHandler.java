package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionNotInterestedHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionNotInterestedHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		int statusId = 0;

		if (null != popUserEntry) {
			long popUserId = popUserEntry.getPopulationUserId();
			int currEntryTypeId = popUserEntry.getEntryTypeId();

			if (ConstantsBase.POP_ENTRY_TYPE_GENERAL == currEntryTypeId ||
					ConstantsBase.POP_ENTRY_TYPE_TRACKING == currEntryTypeId ||
					ConstantsBase.POP_ENTRY_TYPE_CALLBACK == currEntryTypeId){

				try {
					if (!PopulationEntriesManagerBase.isRetentionPopulation(user.getId())) {
						delay = PopulationsManagerBase.getPopUsersDelayByDelayType(conn,popUserId,
								PopulationsManagerBase.POP_DELAY_TYPE_NI);

						if (delay.isPassedDelayMaxCount()){
							// move to review & delete delay id
							popUserEntry.setDelayId(0);
							statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW_FROM_MAX_DELAY;
							log.debug("move to review by not interested for pop user " + popUserId);
						}else{
							statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_DELAY_NI;
							log.debug("delay by not interested for pop user " + popUserId);
						}
					}
				} catch (SQLException e) {
					log.error("cant check population type for user id " + user.getId(), e);
				}

			}else{
				ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_TRACKING);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
				addDisplayMsg("issue.action.only.for.specific.entry.types", context, log, entryTypeIds);
				return false;
			}

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){

			insertUpdateDelay(conn, delay, popUserEntry, action);
			updateEntryType(conn, popUserEntry, popHisStatus, action);

			try {
				PopulationsManagerBase.reachedCall(conn,popUserEntry, action.getWriterId(), user);
			} catch (PopulationHandlersException e) {
				log.debug("fail to remove from call me for pop user " + popUserEntry.getPopulationUserId(),e);
			}
		}

	}
}
