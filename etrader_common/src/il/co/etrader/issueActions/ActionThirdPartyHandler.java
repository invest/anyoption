package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionThirdPartyHandler extends ActionHandlerBase{
	private static final Logger log = Logger.getLogger(ActionThirdPartyHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		long userId = user.getId();
		int statusId = 0;

		if (null == popUserEntry){
			try{
				popUserEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){

			long popUserId = popUserEntry.getPopulationUserId();

			if (screenId == ConstantsBase.SCREEN_SALEM || screenId == ConstantsBase.SCREEN_SALE) {
				if (action.isCallBack()) {
					//Unassigned CB that more than a week
					if(isCbMoreThanWeek(action, CommonUtil.getDateTimeFormaByTz(new Date(), action.getCallBackTimeOffset()), user)) {
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_CANCEL_ASSIGN;
					} else {
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK;
					}
				} else if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK) {
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_CALL;
				}

				popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
				updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
			}

			if (popUserEntry.getDelayId() > 0) {

	            try {
					delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserEntry.getPopulationUserId());
					int delayType = delay.getDelayType();

					if (null != delay){
	            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType){
	            			popUserEntry.setDelayId(0);
	            		}
					}
	            } catch (SQLException e) {
					log.error("Problem in canceling delay for popUser" + popUserId);
				}
			}
		}
		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{
		long contactId = 0;
		boolean res = false;
		long templateid = 0;
		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){
			contactId = popUserEntry.getContactId();
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
		
    	if (log.isEnabledFor(Level.DEBUG)) {
    		log.log(Level.DEBUG, "Send Not reached mail to user " + user.getId() + " or contact " + contactId);
		}

		if (user.getId() > 0){
			if (user.getContactByEmail()) {
				long issueActionTypeId = Long.parseLong(action.getActionTypeId());
				Template t = getTemlate(conn, issueActionTypeId, user);
				templateid = t.getId();
				res = UsersManagerBase.sendPopUserMailTemplateFile(conn, t, action.getWriterId(), user.getId(), 0);
//                if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
                    UsersManagerBase.SendToMailBox(user, t, action.getWriterId(), user.getLanguageId(), null, conn, 0);
//                }
			}
		} else {
			res = UsersManagerBase.sendPopUserMailTemplateFile(Template.WRONG_NUMBER_MAIL_CONTACT, action.getWriterId(), 0, contactId);
		}

		if (res && log.isEnabledFor(Level.DEBUG)) {
			log.log(Level.DEBUG, "Not reached call mail sent successfully! ");
		}

		if (res){
			IssueAction emailAction = new IssueAction();
			emailAction.setIssueId(action.getIssueId());
			emailAction.setWriterId(Writer.WRITER_ID_AUTO);
			emailAction.setSignificant(false);
			emailAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
			emailAction.setComments("Sent Unreached call email.");
			emailAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
			emailAction.setActionTime(new Date());
			emailAction.setActionTimeOffset(user.getUtcOffset());
			emailAction.setTemplateId(templateid);
			IssuesDAOBase.insertAction(conn, emailAction);
		}
	}

	private static Template getTemlate(Connection conn, long issueActionTypeId, UserBase user) throws Exception {
        long templateId = getTemplateId(conn, issueActionTypeId, user);
        return TemplatesDAO.get(conn, templateId);

	}

	private static long getTemplateId(Connection conn, long issueActionTypeId, UserBase user) throws Exception {
		boolean isHaveApprovedDeposits = UsersDAOBase.isHaveApprovedDeposits(conn, user.getId());
		if(user.getId() > 0) {
			if (isHaveApprovedDeposits) {
				return Template.NOT_REACHED_MADE_DEPOSIT;
			} else {
				return Template.NOT_REACHED_NEVER_DEPOSIT;
			}
		} else {
			return Template.NOT_REACHED_MAIL_ID;
		}
	}
}
