package il.co.etrader.issueActions;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.UsersPendingDocsManagerBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;


public class ActionPendingDocHandler extends ActionHandlerBase{
	
	  private static final Logger logger = Logger.getLogger(ActionPendingDocHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception {
		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);
		conn.commit();
		//call file insert
		LinkedHashSet<Long> fileTypeHashMap = null;
    	fileTypeHashMap = new LinkedHashSet<Long>(); 
    	
		    fileTypeHashMap.add( FileType.USER_ID_COPY); 
		    fileTypeHashMap.add( FileType.PASSPORT); 
		    fileTypeHashMap.add( FileType.DRIVER_LICENSE); 
		    fileTypeHashMap.add(FileType.UTILITY_BILL);
		    fileTypeHashMap.add( FileType.CC_COPY_FRONT);  
			fileTypeHashMap.add( FileType.CC_COPY_BACK);
  
	   long tempCCId = 0;   
	    try {
	    	
	    	// check for existing Issue wit subject_id 56
	    	if(IssuesManagerBase.isPendingDocIssuesForUser(user.getId())) {
	    		
	    		fileTypeHashMap.remove( FileType.USER_ID_COPY); 
	 		    fileTypeHashMap.remove( FileType.PASSPORT); 
	 		    fileTypeHashMap.remove( FileType.DRIVER_LICENSE); 
	 		    fileTypeHashMap.remove(FileType.UTILITY_BILL);
	 		    
	 		    // check for manual isssue 56
	 		   if(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE) != null) {
	 				 tempCCId =Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE).toString());
	 			  // check for auto issue 56
		    	}
	 		   if (FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO) != null) {
		    		
		    		 if(!TransactionsManagerBase.isFirstDepositWithCC(user.getId(),Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString()))) {
		    			 if(IssuesManagerBase.isPendingDocIssuesForCC(user.getId(),Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString()))) {
		 				      fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
		    			      fileTypeHashMap.remove( FileType.CC_COPY_BACK);
		    			 }
		 			  }else {
		 				 tempCCId = Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString());
		 			  }
		    	}
	    	
	    	}else {
	    		ArrayList<File> fileList = FilesDAOBase.getUserFilesByIdNoIssue(conn, user.getId(), user.getLocale());
	    		if(null != fileList) {
	    			for(File f : fileList) {
	    				if(f.getFileStatusId() == File.STATUS_IN_PROGRESS) {
	    						FilesDAOBase.updateFileRecords(conn, f, action.getActionId());
	    						IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
	    						fileTypeHashMap.remove(f.getFileTypeId());
	    			   }
	    			}
	    		 }
	    		  
	    		  // auto first deposit Issue
				long firstDepositId = user.getFirstDepositId();
				if(firstDepositId < 1) {
					firstDepositId = UsersDAOBase.getFirstDepositId(conn, user.getId());
				}
				Transaction tran = TransactionsManagerBase.getTransaction(firstDepositId);
	    	    if(null !=tran) {
	    		 if (tran.getTypeId() != 1) {
	    			 fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
	    			 fileTypeHashMap.remove( FileType.CC_COPY_BACK);
	    		 }else {
	    			 tempCCId = tran.getCreditCardId().longValue();
	    		 } 
	    	  }
	    	     
	    	        // check for manual isssue 56
	    	     if(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE) != null) {
		 				 tempCCId = Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE).toString());
		 			  // check for auto issue 56
			    	}
			    	if (FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO) != null) {
			    		
			    		 if(!TransactionsManagerBase.isFirstDepositWithCC(user.getId(),Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString()))) {
			    			 if(IssuesManagerBase.isPendingDocIssuesForCC(user.getId(),Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString()))) {
			 				      fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
			    			      fileTypeHashMap.remove( FileType.CC_COPY_BACK);
			    			 }
			 			  }else {
			 				 tempCCId = Long.valueOf(FacesContext.getCurrentInstance().getAttributes().get(ConstantsBase.CONTEXT_CC_ID_ATTRIBUTE_AUTO).toString());
			 			  }	
			    	}
	    	}
	    	
	    	if(tempCCId == 0 ) {
	    		fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
   			    fileTypeHashMap.remove( FileType.CC_COPY_BACK);	
	    	}
	    	
//		 for (Long key :  fileTypeHashMap) {		 
//			 
//			 if(key == FileType.CC_COPY_FRONT || key == FileType.CC_COPY_BACK){
//			 FilesDAOBase.insertFile(conn,action.getActionId(), tempCCId, key, user.getId(), File.STATUS_REQUESTED, null, action.getWriterId());
//			 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,tempCCId);
//			 }else {
//			 FilesDAOBase.insertFile(conn,action.getActionId(), 0, key, user.getId(),File.STATUS_REQUESTED, null, action.getWriterId());
//			 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
//			 }
//		 }
		 
		 logger.debug("Try to set UsersPendicDocs");
		 UsersPendingDocsManagerBase.insertUsersPendingDocs(i.getUserId(), i.getId(), i.isMainRegulationIssue());
		 
		 logger.info("Successfully insert regulation document need issue for user id: " + user.getId());
	    }catch(Exception ex) {
	    	
	    	logger.error(
					"couldn't insert regulation document need issue for user id: "
							+ user.getId(), ex);
	    	throw ex;
	    }
		 
	}

}
