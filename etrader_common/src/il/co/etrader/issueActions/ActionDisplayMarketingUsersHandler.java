package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionDisplayMarketingUsersHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionDisplayMarketingUsersHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		long userId = user.getId();

		if (0 == userId){
			addDisplayMsg("issue.action.only.for.users", context, log, null);
			return false;
		}

		if (null == popUserEntry){
			try{
				popUserEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){
			if (popUserEntry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_MARKETING){
				if (popUserEntry.isDisplayed()){
					addDisplayMsg("issue.action.user.is.displayed.marketing", context, log, null);
					return false;
				}
			}else{
				addDisplayMsg("issue.action.user.is.not.in.marketing", context, log, null);
				return false;
			}


			int statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_DISPLAY_POPULATION;
			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);

			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);

		}else{
			addDisplayMsg("issue.action.only.for.sales", context, log, null);
			return false;
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		PopulationsDAOBase.updateUserEntryIsDisplay(conn, popUserEntry.getCurrEntryId(), true);

		updateEntryType(conn, popUserEntry, popHisStatus, action);
	}
}
