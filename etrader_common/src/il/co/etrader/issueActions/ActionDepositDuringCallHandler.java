package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsIssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.dao_managers.TransactionsIssuesDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionDepositDuringCallHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionDepositDuringCallHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		long userId = user.getId();
		int statusId = 0;
		long tranId = 0;

		if (userId != 0){
			if (null != popUserEntry){
				long popUserId = popUserEntry.getPopulationUserId();

				if (action.isCallBack()){
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_CANCEL_ASSIGN;
				} else if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK){
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_CALL;
				}

				popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
				updatePopUserByStatus(popUserEntry, popHisStatus, action, context);

				try {
					tranId = TransactionsIssuesDAOBase.getTransactionForActionByUserId(conn,userId, action.getIssueId());
		        } catch (SQLException e) {
		        	log.error("Problem with finding transaction ",e);
				}

				if (tranId != 0){
					action.setTransactionId(tranId);
				}

				// check if user has a delay that needs to be canceled
				if (popUserEntry.getDelayId() > 0){
		            try {
						delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserId);
						int delayType = delay.getDelayType();

						if (null != delay){
		            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType ||
		            				PopulationsManagerBase.POP_DELAY_TYPE_NI == delayType){
		            			popUserEntry.setDelayId(0);
		            		}
						}
		            } catch (SQLException e) {
						log.error("Problem with finding delay for popUser " + popUserId);
					}
				}

	            return true;
	        }else {
	        	addDisplayMsg("issue.action.only.for.sales", context, log, null);
	            return false;
	        }
		}else{
			addDisplayMsg("issue.action.only.for.users", context, log, null);
			return false;
		}
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);
		updateEntryType(conn, popUserEntry, popHisStatus, action);

		long userId = user.getId();
		long actionId = action.getActionId();
		long tranId = action.getTransactionId();

		if (tranId > 0){
			try {
				int isFirstDeposit = 0;
				if (il.co.etrader.bl_managers.TransactionsManagerBase.getFirstTrxId(userId) == tranId) {
					isFirstDeposit = 1;
				}
				log.debug(" insert tran " + tranId + " and action " + actionId + " to transaction issues ");
				TransactionsIssuesDAOBase.insertIntoTi(conn, tranId, actionId, 
						ConstantsBase.TRAN_ISSUES_INSERT_ONLINE, action.getWriterId(), isFirstDeposit);

				// update online deposits list
				TransactionsIssuesManagerBase.updateOnlineDeposits(conn, action.getWriterId(), userId, tranId);
			}catch (SQLException se) {
				log.debug(" Couldn't insert tran " + tranId + " and action " + actionId + " to transaction issues ",se);
			}
		}

		try {
			PopulationsManagerBase.reachedCall(conn,popUserEntry, action.getWriterId(), user);
		} catch (PopulationHandlersException e) {
			log.debug("fail to remove from call me for pop user " + popUserEntry.getPopulationUserId(),e);
		}
	}

}
