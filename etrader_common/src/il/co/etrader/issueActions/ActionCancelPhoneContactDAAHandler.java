package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.PopulationsDAOBase;

public class ActionCancelPhoneContactDAAHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionCancelPhoneContactDAAHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		long userId = user.getId();

		if (0 == userId){
			addDisplayMsg("issue.action.only.for.users", context, log, null);
			return false;
		}else if (!user.isContactForDaa()){
			addDisplayMsg("issue.action.user.is.disabled.for.phone.calls.daa", context, log, null);
			return false;
		}

		if (null == popUserEntry){
			try{
				popUserEntry = PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
			}
		}

		if (null != popUserEntry){
			int statusId = 0;
			long currPopTypeId = popUserEntry.getCurrPopualtionTypeId();

			if(PopulationsManagerBase.POP_TYPE_DECLINE_LAST == currPopTypeId ){

				if (popUserEntry.getCurrPopualtionDeptId() == ConstantsBase.DEPARTMENT_RETENTION){
					if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_GENERAL){
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA_DE;
					}else{
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA;
					}
				}else{
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA;
				}
			}else{
				statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_DISABLE_PHONE_CONTACT_DAA;
			}

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		UsersDAOBase.setDisablePhoneContactDAA(conn, i.getUserId());

		if (null != popUserEntry){
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
	}
}
