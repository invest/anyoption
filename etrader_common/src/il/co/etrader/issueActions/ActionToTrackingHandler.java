package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionToTrackingHandler extends ActionHandlerBase{

    private static final Logger log = Logger.getLogger(ActionToTrackingHandler.class);


    /**
     *  validateAction event handler implementation
     */
    @Override
    public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

        if (null != popUserEntry){
            int statusId = 0;            int entryTypeId = popUserEntry.getEntryTypeId();
            long popUserId = popUserEntry.getPopulationUserId();

            if (ConstantsBase.POP_ENTRY_TYPE_GENERAL == entryTypeId ||
            		ConstantsBase.POP_ENTRY_TYPE_TRACKING == entryTypeId ||
            		ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryTypeId){

            	if (action.isCallBack()){
            		statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK;
            	} else {
            		switch (entryTypeId) {
						case ConstantsBase.POP_ENTRY_TYPE_GENERAL:
							statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_TRACKING;
							break;
						case ConstantsBase.POP_ENTRY_TYPE_TRACKING:
							statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_TRACKING_RENEW;
							break;
						case ConstantsBase.POP_ENTRY_TYPE_CALLBACK:
							statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_TRACKING;
							break;
	                }

            	}
            } else {
				ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_TRACKING);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
				addDisplayMsg("issue.action.only.for.specific.entry.types", context, log, entryTypeIds);
				return false;
            }

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);

			// check if user has a delay that needs to be canceled
			if (popUserEntry.getDelayId() > 0){
	            try {
					delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserEntry.getPopulationUserId());
					int delayType = delay.getDelayType();

					if (null != delay){
	            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType ||
	            				PopulationsManagerBase.POP_DELAY_TYPE_NI == delayType){
	            			popUserEntry.setDelayId(0);
	            		}
					}
	            } catch (SQLException e) {
					log.error("Problem in canceling delay for popUser" + popUserId);
				}
			}

            return true;
        }else {
			addDisplayMsg("issue.action.only.for.sales", context, log, null);
            return false;
        }
    }

    /**
     *  validateAction event handler implementation
     */
    @Override
    public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

        //	Insert Issue action
        IssuesDAOBase.insertAction(conn, action);

        updateEntryType(conn, popUserEntry, popHisStatus, action);

		try {
			PopulationsManagerBase.reachedCall(conn,popUserEntry, action.getWriterId(), user);
		} catch (PopulationHandlersException e) {
			log.debug("fail to remove from call me for pop user " + popUserEntry.getPopulationUserId(),e);
		}
    }
}
