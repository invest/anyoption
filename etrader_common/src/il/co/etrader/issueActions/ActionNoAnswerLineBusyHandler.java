package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.PopulationsDAOBase;

public class ActionNoAnswerLineBusyHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionNoAnswerLineBusyHandler.class);
	private boolean sendUnreachedTemplate = false;

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		int statusId = 0;
		long userId = user.getId();

		if(screenId == ConstantsBase.SCREEN_SALE || screenId == ConstantsBase.SCREEN_SALEM) {
			sendUnreachedTemplate = true;
		}

		if (userId > 0){
			// Check Risk issues only if it's a user
			try{
				handleRiskIssue(conn, action.getIssueId());
			}catch (SQLException e) {
				log.error("Error in counting risk issues for user: " + user.getId() , e);
				return false;
			}

			if (null == popUserEntry){
				try{
					popUserEntry = PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
				}catch (SQLException e) {
					log.error("Error in getting pop user: " + user.getId() , e);
					return false;
				}
			}
		}

		if (null != popUserEntry && ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES != popUserEntry.getEntryTypeId()) {
			long popUserId = popUserEntry.getPopulationUserId();
			long popEntryId = popUserEntry.getOldPopEntryId();

			try{
				// check if there are X consecutive calls were made on X different days and all are not reached + line busy/no answer
				if (popEntryId > 0 && isCountNotReachedActionsSales(popEntryId)){

					// If user has been reached before handle him as not intersted
					// Else remove him from current pop
					if (IssuesManagerBase.getReachedCallsNum(popUserEntry) > 0){

						delay = PopulationsManagerBase.getPopUsersDelayByDelayType(conn,popUserId,
								PopulationsManagerBase.POP_DELAY_TYPE_NA);

	                	if (delay.isPassedDelayMaxCount()){
	                		// move to review & delete delay id
	                		popUserEntry.setDelayId(0);
	    					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW_FROM_MAX_DELAY;
	    					log.debug("move to review by count actions remove for reached pop user " + popUserId);
	                	}else{
	                 		delay.setDelayActionId(action.getActionId());
	                		statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_DELAY_NA;
							log.debug("delay by count actions remove for reached pop user " + popUserId);
	                	}
					}else{
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_X_CALLS_AND_NEVER_REACHED;
						log.debug("remove from pop  by count actions remove for unreached pop user " + popUserId);
					}

				}
			}catch (SQLException e) {
				log.error("Problem in counting actions for NA delay, for pop user id " + popUserId,e);
			}

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{
		IssueActionType previousIat = null;
		if(null != popUserEntry) {
			previousIat = IssuesDAOBase.getLastIssueActionTypeByEntryId(conn, popUserEntry.getCurrEntryId());
		}
		boolean lastCallReachedOrNoCall = false;
		// Chack if the last status of reached_call was reached or no calls
		if((null == previousIat) || (previousIat.getReachedStatusId() == IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED)) {
			lastCallReachedOrNoCall = true;
		}

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){

			insertUpdateDelay(conn, delay, popUserEntry, action);
			updateEntryType(conn, popUserEntry, popHisStatus, action);

			if ((((null != popHisStatus)  || (lastCallReachedOrNoCall)))) {
				try {
					long userId = user.getId();
					boolean res = false;
					long writerId = action.getWriterId();
					long issueActionTypeId = Long.parseLong(action.getActionTypeId());
					Template t = getTemlate(conn, issueActionTypeId, user);

					if(sendUnreachedTemplate) {
						if (userId > 0 ) {
							long skinId = user.getSkinId();
							if (skinId != Skins.SKIN_GERMAN && skinId != Skins.SKIN_REG_DE) {
								if (user.getContactByEmail()){
									res = UsersManagerBase.sendPopUserMailTemplateFile(conn, t, writerId , userId, 0);
		                        }
		                        // add to user MailBox
//		                        if (!CommonUtil.isHebrewSkin(popUserEntry.getSkinId())) {
		                            UsersManagerBase.SendToMailBox(user, t, writerId, user.getLanguageId(), null, conn, 0);
//		                        }
							}
						} else /*if (!CommonUtil.isHebrewSkin(popUserEntry.getSkinId()))*/ {
							res = UsersManagerBase.sendPopUserMailTemplateFile(conn, t, writerId , 0, popUserEntry.getContactId());
						}
					}

					if (res){
						IssueAction emailAction = new IssueAction();
						emailAction.setIssueId(action.getIssueId());
						emailAction.setWriterId(Writer.WRITER_ID_AUTO);
						emailAction.setSignificant(false);
						emailAction.setActionTimeOffset(user.getUtcOffset());
						emailAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
						emailAction.setComments("Sent Unreached call email.");
						emailAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
						emailAction.setActionTime(new Date());
						emailAction.setTemplateId(t.getId());
						IssuesDAOBase.insertAction(conn, emailAction);
					}
				} catch (Exception e2) {
					log.error("can't send user not reached mail for popUser " + popUserEntry.getPopulationUserId(), e2);
				}
			}
		}
	}

	private static boolean isCountNotReachedActions(int actionsNum, long issuePopEntryId, long issueId) throws SQLException{
		if (actionsNum > 0)
		{
			int numActionsCount = 0;
			ArrayList<IssueAction> actionList = IssuesManagerBase.getActionsCount(issuePopEntryId, issueId);
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, 1);
			Date lastCallDay = c.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			if (actionList.size() > 0) {
				for (IssueAction ia : actionList) {
					if (isNaReaction(Integer.valueOf(ia.getActionTypeId()))) {

						// check if this recation happend in a different day
						if (!sdf.format(lastCallDay).equals(sdf.format(ia.getActionTime()))){
							lastCallDay = ia.getActionTime();
							numActionsCount++;
						}
					} else {
						break;
					}
				}
				if (numActionsCount >= actionsNum -1) {
					return true;
				}
			}
		}
		return false;
	}

	private static void handleRiskIssue(Connection conn, long issueId) throws SQLException{

//		if (isCountNotReachedActions(ConstantsBase.RISK_ACTIONS_NUM, 0, issueId)){
//			IssuesDAOBase.updateSupportUnreachStatus(conn, issueId);
//		}
	}

	private static boolean isCountNotReachedActionsSales(long issuePopEntryId) throws SQLException{

		int actionsNum = 0;
		try {
			actionsNum = PopulationsManagerBase.getNumActionsForRemove(issuePopEntryId);
		} catch (PopulationHandlersException e) {
			log.error("Problem getting num of action to remove! " + e);
		}

		return isCountNotReachedActions(actionsNum, issuePopEntryId, 0);
	}

	public static boolean isNaReaction(int actionTypeId){
		return (actionTypeId == IssuesManagerBase.ISSUE_ACTION_LINE_BUSY ||
				actionTypeId == IssuesManagerBase.ISSUE_ACTION_NO_ANSWER);
	}

	private static Template getTemlate(Connection conn, long issueActionTypeId, UserBase user) throws Exception {
        long templateId = getTemplateId(conn, issueActionTypeId, user);
        return TemplatesDAO.get(conn, templateId);

	}

	private static long getTemplateId(Connection conn, long issueActionTypeId, UserBase user) throws Exception {
		boolean isHaveApprovedDeposits = UsersDAOBase.isHaveApprovedDeposits(conn, user.getId());
		if(user.getId() > 0) {
			if (isHaveApprovedDeposits) {
				return Template.NOT_REACHED_MADE_DEPOSIT;
			} else {
				return Template.NOT_REACHED_NEVER_DEPOSIT;
			}
		} else {
			return Template.NOT_REACHED_MAIL_ID;
		}
	}
}
