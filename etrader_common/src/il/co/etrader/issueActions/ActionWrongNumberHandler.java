package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.PopulationsDAOBase;

public class ActionWrongNumberHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionWrongNumberHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		long userId = user.getId();

//		if (0 != userId && !user.isActive()){
//			addDisplayMsg("issue.action.user.is.not.active", context, log, null);
//			return false;
//		}

		if (null == popUserEntry){
			try{
				popUserEntry = PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){
			int statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_WRONG_NUMBER;

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{
		long contactId = 0;
		boolean res = false;
		long templateId = 0;
		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){
			updateEntryType(conn, popUserEntry, popHisStatus, action);
			contactId = popUserEntry.getContactId();
		}

		//  send mail
    	if (log.isEnabledFor(Level.DEBUG)) {
    		log.log(Level.DEBUG, "Send Wrong number mail to user " + user.getId() + " or contact " + contactId);
		}

		if (user.getId() > 0){
			if (user.getContactByEmail()) {
				res = UsersManagerBase.sendPopUserMailTemplateFile(Template.WRONG_NUMBER_MAIL, action.getWriterId(), user.getId(), 0);
//                if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
                	templateId = Template.WRONG_NUMBER_MAIL;
                	Template t = TemplatesDAO.get(conn, templateId);
                    UsersManagerBase.SendToMailBox(user, t, action.getWriterId(), user.getLanguageId(), null, conn, 0);
//                }
			}
		} else {
			templateId = Template.WRONG_NUMBER_MAIL_CONTACT;
			res = UsersManagerBase.sendPopUserMailTemplateFile(templateId, action.getWriterId(), 0, contactId);
		}

		if (res && log.isEnabledFor(Level.DEBUG)) {
			log.log(Level.DEBUG, "Wrong number mail sent successfully! ");
		}

		if (res){
			IssueAction emailAction = new IssueAction();
			emailAction.setIssueId(action.getIssueId());
			emailAction.setWriterId(action.getWriterId());
			emailAction.setSignificant(false);
			emailAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
			emailAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
			emailAction.setComments("Sent Wrong number email.");
			emailAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
			emailAction.setActionTime(new Date());
			emailAction.setTemplateId(templateId);
			IssuesDAOBase.insertAction(conn, emailAction);
		}
	}

}
