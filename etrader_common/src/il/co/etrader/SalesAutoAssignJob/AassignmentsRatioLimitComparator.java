package il.co.etrader.SalesAutoAssignJob;

import java.util.Comparator;

import il.co.etrader.bl_vos.Writer;

public class AassignmentsRatioLimitComparator implements Comparator {
	
	private long skinId;
	
	public AassignmentsRatioLimitComparator(long skinId) {
		this.skinId = skinId; 
	}
		
	/*
	 * Compare representatives by the ratio between by sum of assignments and their limits 
     * (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(Object o1, Object o2) {
		Writer w1 = (Writer)o1;
		Writer w2 = (Writer)o2;
		return new Double(w1.getWriterSkins().get(skinId).sumAassignmentsRatioLimit()).compareTo(new Double(w2.getWriterSkins().get(skinId).sumAassignmentsRatioLimit()));
	}

 }
