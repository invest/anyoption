package il.co.etrader.SalesAutoAssignJob;

import java.util.Comparator;

import org.apache.log4j.Logger;

import il.co.etrader.bl_vos.RankData;
import il.co.etrader.bl_vos.StatusData;
import il.co.etrader.bl_vos.Writer;

public class AssignmentComparator implements Comparator {
	
	private static Logger log = Logger.getLogger(AssignmentComparator.class);
	
	private long skinId;
	private long rankId;
	private long statusId;
	
	public AssignmentComparator(long skinId, long rankId, long statusId) {
		this.skinId = skinId;
		this.rankId = rankId;
		this.statusId = statusId;
	}
	
    /* Compare representatives by the their number of assignment per rank and status 
     * (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(Object o1, Object o2) {
		Writer w1 = (Writer)o1;
		Writer w2 = (Writer)o2;
		try {
			RankData r1 = w1.getWriterSkins().get(skinId).getRanksData().get(rankId);
			RankData r2 = w2.getWriterSkins().get(skinId).getRanksData().get(rankId);
			if (r1 == null) {
				r1 = new RankData();
				w1.getWriterSkins().get(skinId).getRanksData().put(rankId, r1);
			}
			if (r2 == null) {
				r2 = new RankData();
				w2.getWriterSkins().get(skinId).getRanksData().put(rankId, r2);				
			}
			StatusData s1 = w1.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().get(statusId);
			StatusData s2 = w2.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().get(statusId);
			if (s1 == null) {
				s1 = new StatusData();
				w1.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().put(statusId, s1);	
			}
			if (s2 == null) {
				s2 = new StatusData();
				w2.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().put(statusId, s2);							
			}
			return new Integer(w1.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().get(statusId).getCountOfAssignment()).compareTo(new Integer(w2.getWriterSkins().get(skinId).getRanksData().get(rankId).getStatusData().get(statusId).getCountOfAssignment()));						
		} catch (Exception e) {
			log.warn("Can't get compare between" + w1.getId() + " and " + w2.getId() + " skinId=" + skinId + " rankId=" + rankId + " statusId=" + statusId, e);
			return 0;
		}
	}


 }
