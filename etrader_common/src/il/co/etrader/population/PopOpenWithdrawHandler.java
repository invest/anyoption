package il.co.etrader.population;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;

public class PopOpenWithdrawHandler extends PopulationHandlerBase {

	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}				
	}

	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful,
			long writerId) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry,
			long writerId) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry,
			long writerId, UserBase user) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}
	
    public void approvePendingWithdraw(Connection con, PopulationEntryBase popUserEntry, long writerId, long statusId, long actionId, boolean isCancelAssign) throws PopulationHandlersException {
    	try {
			PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_RETENTION,
					0, 0, popUserEntry.getUserId(), popUserEntry.getAssignWriterId(), popUserEntry);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
