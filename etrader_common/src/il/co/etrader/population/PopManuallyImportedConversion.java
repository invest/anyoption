package il.co.etrader.population;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;

public class PopManuallyImportedConversion extends PopulationHandlerBase {

	private static final Logger log = Logger.getLogger(PopManuallyImportedConversion.class);

	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful,
			long writerId) throws PopulationHandlersException {
		
	}

	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry,
			long writerId) throws PopulationHandlersException {
		
	}

	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry,
			long writerId, UserBase user) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}
}
