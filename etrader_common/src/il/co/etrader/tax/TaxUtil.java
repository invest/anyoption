package il.co.etrader.tax;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * TaxUtil class.
 * This class represent some data that needed for tax job
 *
 * @author Kobi
 */

public class TaxUtil {

	private static Logger log = Logger.getLogger(TaxUtil.class);

    /*  connection for testing
       public static Connection getConnection() throws SQLException {
     	 DriverManager.registerDriver(new OracleDriver());
     	 return DriverManager.getConnection("jdbc:oracle:thin:@dbtest.etrader.co.il:1521:etrader",
     			"etrader","etrader");
   }*/


    /**
     *
     * @param
     * 		con the connection to DB
     * @param seq
     * 		the sequence that need to increase
     * @return
     * 		the next value of this sequence
     * @throws SQLException
     */
    public static long getSeqNextValue(Connection con,String seq)throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try
			{
			  	ps = con.prepareStatement("select "+seq+".NEXTVAL from dual");

				rs = ps.executeQuery();

				if (rs.next()) {

					return rs.getLong(1);
				}

			}

			finally
			{

				try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
			}

			return 0;
	  }

}
