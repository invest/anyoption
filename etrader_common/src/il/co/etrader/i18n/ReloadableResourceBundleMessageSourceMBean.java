package il.co.etrader.i18n;

import java.util.Date;

public interface ReloadableResourceBundleMessageSourceMBean {

	public void clearCache() ;
	
	public long getCacheMillis();
	public void setCacheMillis(long cacheMillis) ;
	
	public String[] getBasenames();
	public void setBasenames(String[] basenames);
	
	public Date getLastTimeAccessed();
}
