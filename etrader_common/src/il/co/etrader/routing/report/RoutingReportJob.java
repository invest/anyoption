package il.co.etrader.routing.report;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class RoutingReportJob extends JobUtil{

	private static Logger log = Logger.getLogger(RoutingReportJob.class);
	private static String fromDate;
	private static String tillDate;

	public static void main(String[] args) throws Exception {
		propFile = args[0];

		log.info("Starting the RoutingReportJob.");
		log.info("Sending bin route report");
		SendXlsFile();
		log.info("Finishing the RoutingReportJob.");
	}

	private static void SendXlsFile(){
		HSSFWorkbook xlsWorkbook = new HSSFWorkbook();
		HSSFSheet xlsSheet= null;
		HSSFRow xlsRow = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		int rowCounter = 0;
		String mailSubject = "New BIN route set successfully";
		String fileName = "bin_route_report.xls";
		String sql = "";
		String emailAddress= "";
		emailAddress = getPropertyByFile("email.to");
		
		// If the time range properties are not in the PropertyFile, the default is last day
		fromDate = getPropertyByFile("from", null);
		tillDate = getPropertyByFile("till", null);
		if (fromDate == null || tillDate == null) {
			fromDate = getFromLastDay();
			tillDate = getTillLastDay();
		}
			
		sql  =  " SELECT " +
					" tab3.*, " +
					" tab4.last_time_created " +
				" FROM " +
					" (SELECT " +
						" tab2.*, " +
						" tab1.num_of_times " +
					" FROM " +
						" (SELECT " +
							" crc.changed_bin_id as changed_bin_id, " +
							" count(crc.changed_bin_id) as num_of_times " +
						" FROM " +
							" clearing_routes_changes_log crc " +
						" GROUP BY crc.changed_bin_id) tab1, " +
						" (SELECT " +
							" crc.*, " +
							" cr.start_bin, " +
							" cct.description cc_description, " +
							" country.country_name, " +
							" cp1.name as former_provider, " +
							" cp2.name as current_provider " +
						" FROM " +
							" clearing_providers cp1, " +
							" clearing_providers cp2, " +
							" clearing_routes_changes_log crc " +
							" LEFT JOIN clearing_routes cr ON cr.id = crc.changed_bin_id " +
							" LEFT JOIN credit_card_types cct ON cct.id = crc.credit_card_type " +
					        " LEFT JOIN countries country ON country.id = crc.credit_card_country " +
						" WHERE " +
							" crc.time_created between to_date('" + fromDate + "','DD/MM/YYYY') and to_date('" + tillDate + "','DD/MM/YYYY') " +
							" AND crc.former_provider_id = cp1.id " +
							" AND crc.current_provider_id = cp2.id) tab2 " +
					" WHERE " +
						" tab1.changed_bin_id = tab2.changed_bin_id) tab3 " +
				" LEFT JOIN " +
					" (SELECT " +
						" tr_crc.changed_bin_id, " +
						" max(tr_crc.time_created) as last_time_created " +
					" FROM " +
						" (SELECT " +
							" tr.id as tran_id, " +
							" tr.time_created, " +
							" crc.changed_bin_id " +
						" FROM " +
							" clearing_routes_changes_log crc, " +
							" clearing_routes cr, " +
							" credit_cards cc " +
							" LEFT JOIN transactions tr ON tr.credit_card_id = cc.id " +
						" WHERE " +
							" crc.time_created between to_date('" + fromDate + "','DD/MM/YYYY') and to_date('" + tillDate + "','DD/MM/YYYY') " + 
							" AND cr.id = crc.changed_bin_id " +
							" AND cr.start_bin = cc.bin " +
							" AND (tr.status_id = ? or tr.status_id = ?) " +
							" AND tr.type_id = ? " +
						" ORDER BY tr.time_created desc) tr_crc " +
					" where tr_crc.time_created NOT IN " +
						" (SELECT " +
							" last_time_created " +
						" FROM " +
							" (SELECT " +
								" tr_crc_sec.changed_bin_id, max(time_created) as last_time_created " +
							" FROM " +
								" (SELECT " +
									" tr.id as tran_id, " +
									" tr.time_created, " +
									" crc.changed_bin_id " +
								" FROM " +
									" clearing_routes_changes_log crc, " +
									" clearing_routes cr, " +
									" credit_cards cc " +
									" left join transactions tr on tr.credit_card_id = cc.id " +
								" WHERE " +
									" crc.time_created between to_date('" + fromDate + "','DD/MM/YYYY') and to_date('" + tillDate + "','DD/MM/YYYY') " +
									" AND cr.id = crc.changed_bin_id " +
									" AND cr.start_bin = cc.bin " +
									" AND (tr.status_id = ? or tr.status_id = ?) " +
									" AND tr.type_id = ? " +
								" ORDER BY tr.time_created desc) tr_crc_sec " +
								" group by tr_crc_sec.changed_bin_id) lsc) " +
							" group by tr_crc.changed_bin_id) tab4 " +
						"ON tab3.changed_bin_id = tab4.changed_bin_id";

		try {
			con = getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			pstmt.setLong(2, TransactionsManagerBase.TRANS_STATUS_PENDING);
			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
			pstmt.setLong(4, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			pstmt.setLong(5, TransactionsManagerBase.TRANS_STATUS_PENDING);
			pstmt.setLong(6, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);

			GregorianCalendar gc = new GregorianCalendar();
			gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gc.set(GregorianCalendar.MINUTE, 0);
			gc.set(GregorianCalendar.SECOND, 0);
			gc.set(GregorianCalendar.MILLISECOND, 0);
			gc.add(GregorianCalendar.DAY_OF_MONTH, -1);

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDate = gc.getTime();
			mailSubject += sdf.format(startDate);

			rs = pstmt.executeQuery();
			xlsSheet = xlsWorkbook.createSheet("data");
			xlsRow = xlsSheet.createRow(rowCounter++);
			setColumnsNameInSheet(xlsRow);

			while (rs.next()) {
				xlsRow = xlsSheet.createRow(rowCounter++);
				setColumnsValuesInSheet(xlsWorkbook, xlsRow, rs);
			}
			

			if (xlsWorkbook != null){
				writeToExcel(xlsWorkbook,fileName);
			}

//		    send email
	        Hashtable<String,String> server = new Hashtable<String,String>();
	        server.put("url", getPropertyByFile("email.url"));
	        server.put("auth", getPropertyByFile("email.auth"));
	        server.put("user", getPropertyByFile("email.user"));
	        server.put("pass", getPropertyByFile("email.pass"));
	        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

	        Hashtable<String,String> email = new Hashtable<String,String>();
	        email.put("subject", mailSubject);
	        email.put("to", emailAddress);
	        email.put("from", getPropertyByFile("email.from"));
	        email.put("body", "The report is attached to this mail");

	        File attachment = new File(fileName);
	        File []attachments={attachment};
	        CommonUtil.sendEmail(server, email, attachments);

		}catch (Exception e) {
			log.fatal("Can't get Routing_Report_Table ", e);
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);

			try {
				con.close();
			} catch (Exception e) {
				log.error("Can't close",e);
			}
		}

	}

	private static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void setColumnsNameInSheet(HSSFRow xlsRow){
		int cellCounter = 0;
		HSSFCell xlsCell = null;

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("BIN"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Card type"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Card's country"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Old route"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("New route"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Number of reroute"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Successful TX"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Time-successful TX"));
			
	}

	private static void setColumnsValuesInSheet(HSSFWorkbook xlsWorkbook, HSSFRow xlsRow, ResultSet rs) throws SQLException{
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
		cellStyle.setWrapText(true);

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("start_bin"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("cc_description"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("country_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("former_provider"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("current_provider"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("num_of_times"));
		xlsCell = xlsRow.createCell(cellCounter++);
		
		String lastTimeCreated = String.valueOf(rs.getDate("last_time_created"));
		if (lastTimeCreated != null) {
			xlsCell.setCellValue("Y");
		} else {
			xlsCell.setCellValue("N");
		}
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(lastTimeCreated);

	}

    public static void closeStatement(final Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                log.error(stmt, ex);
            }
        }
    }

    public static void closeResultSet(final ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                log.error(rs, ex);
            }
        }
    }
    
    private static String getFromLastDay() {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		return sd.format(gc.getTime());
	}

	private static String getTillLastDay() {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

		return sd.format(gc.getTime());
	}
}
