package il.co.etrader.publishers;

import il.co.etrader.publishers.facebook.FacebookPublisher;
import il.co.etrader.publishers.twitter.TwitterPublisher;

import java.util.Date;

public class FacebookTwitterPublisher {


	public static void main(String[] args) {

		try {
			anyoptionPublish("test etrader: " + new Date().toString());
		} catch (Exception e) {
		}
	}


	public static void etraderPublish(String msg) throws Exception {

		FacebookPublisher.etraderPublish(msg);
		TwitterPublisher.etraderPublish(msg);
	}


	public static void anyoptionPublish(String msg) throws Exception {

		FacebookPublisher.anyoptionPublish(msg);
		TwitterPublisher.anyoptionPublish(msg);
	}

	public static void anyoptionPublish(String msg1, String msg2) throws Exception {

		if (msg1 == null) {
			msg1 = "";
		}
		if (msg2 == null) {
			msg2 = "";
		}
		FacebookPublisher.anyoptionPublish(msg1 + msg2);
		TwitterPublisher.anyoptionPublish(msg1, msg2);
	}
}
