package il.co.etrader.publishers.facebook;


import java.io.IOException;
import java.net.URL;

import com.google.code.facebookapi.FacebookException;
import com.google.code.facebookapi.FacebookXmlRestClient;

/**
 * FacebookClient
 *
 *	NOT FOR USE! ONLY FOR CREATING THE SESSION KEY (JUST AFTER CREATING NEW APPLICATION OR USER)
 */
public class FacebookClient {
	/*
	// http://blog.theunical.com/facebook-integration/facebook-java-api-example-to-publish-on-wall/
	To create a facebook application and let it publish in my wall:
		1. Create a new application in http://www.facebook.com/developers/.
		2. In the browser go to (replace API_KEY with the api key of the app.):
			https://login.facebook.com/code_gen.php?api_key=API_KEY&v=1.0
			(get login and generate).
		3. run FacebookClient main function. When it prompt the url, copy & paste it to the browser,
			and just then press enter in the console.
		4. Get the session key from console and paste it in the publisher.
		5. Enter to the browser the url (replace API_KEY with the api key of the app.):
			http://www.facebook.com/login.php?api_key=APIKEY&connect_display=popup&v=1.0&next=http://www.facebook.com/connect/login_success.html&cancel_url=http://www.facebook.com/connect/login_failure.html&fbconnect=true&return_session=true&req_perms=read_stream,publish_stream,offline_access
			(allow access).

*/

	// asher test app data:
	public static String API_KEY = "0667b28c72b52d43e1248d0d3a32a022";
	public static String SECRET = "b09e83532ff02e1f56a095514a8ce650";

	public static void main(String args[]) {
 // Create the client instance
		FacebookXmlRestClient client =
			new FacebookXmlRestClient(API_KEY, SECRET);

		try {
			String token = client.auth_createToken();
			// Build the authentication URL for the user to fill out
			String url = "http://www.facebook.com/login.php?api_key=" +
							API_KEY + "&v=1.0" + "&auth_token=" + token;
			// Open an external browser to login to your application
			URL url2 = new URL(url);
			url2.openConnection();
			// Wait until the login process is completed
			System.out.println("Use browser to login then press return");
			System.out.println(url);
			System.in.read();

			// fetch session key
			String session = client.auth_getSession(token, false );
			System.out.println("Session key is " + session);


		} catch (FacebookException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}