package il.co.etrader.publishers.facebook;

import il.co.etrader.util.CommonUtil;

import com.google.code.facebookapi.FacebookException;
import com.google.code.facebookapi.FacebookJsonRestClient;

public class FacebookPublisher {

	private final static String ETRADER_FB_APP_API_KEY = "0667b28c72b52d43e1248d0d3a32a022";
	private final static String ETRADER_FB_APP_SECRET = "b09e83532ff02e1f56a095514a8ce650";
	private final static String ETRADER_FB_SESSION_KEY = "8ff99827b2d299373ea467e1-100000760104988";

	private final static String ANYOPTION_LIVE_FB_APP_API_KEY = "257d25c7378f659d045b94ae78f0edb0";
	private final static String ANYOPTION_LIVE_FB_APP_SECRET = "1b7817359f01409718f29b61ebcd8a1b";

	private final static String ANYOPTION_TEST_FB_APP_API_KEY = "0667b28c72b52d43e1248d0d3a32a022";
	private final static String ANYOPTION_TEST_FB_APP_SECRET = "b09e83532ff02e1f56a095514a8ce650";

	// link to page: http://www.facebook.com/pages/anyoption/124257344262628?v=wall&ref=sgm
	private final static String ANYOPTION_LIVE_FB_SESSION_KEY = "27fff89fbe7bbc3ba5690afe-1518469026";
	private final static String ANYOPTION_LIVE_FB_TARGET_ID = "124257344262628";

	//	testing details. link to page: http://www.facebook.com/pages/anyoption-test/132197150124112?ref=sgm#!/pages/anyoption-test/132197150124112?v=wall&ref=sgm
	private final static String TEST_FB_SESSION_KEY = "8ff99827b2d299373ea467e1-100000760104988";
	private final static String TEST_FB_TARGET_ID = "123524904351168";


	public static void main (String a[]) throws FacebookException{
		FacebookPublisher.publish("message", ANYOPTION_TEST_FB_APP_API_KEY, ANYOPTION_TEST_FB_APP_SECRET, TEST_FB_SESSION_KEY);
		//String s1 = "abcd'efg'hij";
		//System.out.println(s1.replaceAll("'", "\\\\'"));
	}

	private static String getAnyoptionFbSessionKey() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_FB_SESSION_KEY;
		}
		return TEST_FB_SESSION_KEY;
	}

	private static String getAnyoptionFbTargetId() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_FB_TARGET_ID;
		}
		return TEST_FB_TARGET_ID;
	}

	private static String getAnyoptionFbAppApiKey() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_FB_APP_API_KEY;
		}
		return ANYOPTION_TEST_FB_APP_API_KEY;
	}

	private static String getAnyoptionFbAppSecret() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_FB_APP_SECRET;
		}
		return ANYOPTION_TEST_FB_APP_SECRET;
	}

	public static void publish(String msg, String appApiKey, String appSecret, String sessionKey)throws FacebookException {
		try {
		FacebookJsonRestClient obj = new FacebookJsonRestClient(appApiKey, appSecret, sessionKey);
		obj.stream_publish(msg, null, null, Long.valueOf(getAnyoptionFbTargetId()), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void etraderPublish(String msg) throws FacebookException {

		publish(msg, ETRADER_FB_APP_API_KEY, ETRADER_FB_APP_SECRET, ETRADER_FB_SESSION_KEY);
	}


	public static void anyoptionPublish(String msg) throws FacebookException {

		publish(msg, getAnyoptionFbAppApiKey(), getAnyoptionFbAppSecret(), getAnyoptionFbSessionKey());
	}
}