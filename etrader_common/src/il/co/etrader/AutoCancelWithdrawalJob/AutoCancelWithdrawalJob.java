package il.co.etrader.AutoCancelWithdrawalJob;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import il.co.etrader.util.SendTemplateEmail;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.daos.GeneralDAO;

/**
 * 
 * Deprecated.
 *
 */
public class AutoCancelWithdrawalJob extends JobUtil {
	private static Logger log = Logger.getLogger(AutoCancelWithdrawalJob.class);
	 private static StringBuffer report = new StringBuffer();

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		log.info("Starting the Cancel withdrawal Email Job.");
		SendCancelWithdrawalEmail();

		 // build the report
        String reportBody = "<html><body>" +
        						report.toString() +
        					"</body></html>";


	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report email: " + reportBody);
	    }

	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("email.url"));
	    server.put("auth", getPropertyByFile("email.auth"));
	    server.put("user", getPropertyByFile("email.user"));
	    server.put("pass", getPropertyByFile("email.pass"));
	    server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", getPropertyByFile("job.email.subject"));
	    email.put("to", getPropertyByFile("job.email.to"));
	    email.put("from", getPropertyByFile("job.email.from"));
	    email.put("body", reportBody);
	    sendEmail(server, email);

		log.info("Cancel withdrawal Email Job completed.");
	}

	/**
	 * Send cancel withdrawal email notification
	 * @throws Exception
	 */
	private static void SendCancelWithdrawalEmail() throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

	    report.append("<br><b>AutoCancelWithdrawalJob Information:</b><table border=\"1\">");
	    report.append("<tr><td><b>Users Id</b></td><td><b>Action</b></td><td><b>Trx Id</b></td><td><b>Amount</b></td><td><b>Time created</b></td></tr>");

		try {
			conn = getConnection();
			String sql =
				"SELECT * FROM ( "+
					"SELECT " +
						"u.id user_id, " +
						"u.first_name, " +
						"u.last_name, " +
						"u.user_name, " +
						"u.email, " +
						"u.skin_id as skin, " +
						"u.utc_offset, " +
						"u.language_id, " +
						"t.id transaction_id, " +
						"t.credit_card_id, " +
						"t.reference_id, " +
						"t.type_id, " +
						"t.status_id, " +
						"t.description, " +
						"t.writer_id trx_writer_id, " +
						"t.ip trx_ip, " +
						"t.comments, " +
						"t.processed_writer_id, " +
						"t.cheque_id, " +
						"t.wire_id, " +
						"t.charge_back_id, " +
						"t.receipt_num, " +
						"t.auth_number, " +
						"t.xor_id_authorize, " +
						"t.xor_id_capture, " +
						"t.clearing_provider_id, " +
						"t.time_created, " +
						"t.utc_offset_created, " +
						"t.amount, " +
						"(Select Count(Distinct(T.Type_Id)) From Transactions T, Transaction_Types Tt Where T.Type_Id = Tt.Id And Tt.Class_Type = 1 And T.User_Id = U.Id And T.Type_Id <> 38 And T.Type_Id <> 2 And t.type_id <> 14  and (t.status_id = 2 or t.status_id = 7))  As Exists_Non_Cup " +
						//"(Select Count(Distinct(T.Type_Id)) From Transactions T, Transaction_Types Tt Where T.Type_Id = Tt.Id And Tt.Class_Type = 1 And T.User_Id = U.Id And (T.Type_Id = 38 Or  T.Type_Id = 2))  As Exists_Cup_Or_Wire " +
					"FROM " +
						"users u, " +
					    "transactions t, " +
					    "transaction_types tt " +
					"WHERE " +
					    "u.id = t.user_id AND " +
					    "t.type_id = tt.id AND " +
					    "tt.class_type = ? AND " +
					    "t.status_id = ? AND " +
					    "u.class_id <> 0 AND " +
					    "(TRUNC(sysdate) - TRUNC( t.time_created)) > ? " +
					"ORDER BY " +
					    "u.skin_id, " +
					    "t.type_id " +
				") WHERE " +
					"Exists_Non_Cup > 0 Or (skin  <> 22 And skin  <> 15)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			pstmt.setLong(2, TransactionsManagerBase.TRANS_STATUS_REQUESTED);
			pstmt.setInt(3, ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_EMAIL_NOTIFICATION);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				UserBase user = new UserBase();
				user.setId(rs.getLong("user_id"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setEmail(rs.getString("email"));
				user.setSkinId(rs.getLong("skin"));
				user.setUtcOffset(rs.getString("utc_offset"));
				user.setUserName(rs.getString("user_name"));
				user.setLanguageId(rs.getLong("language_id"));

		        Transaction trx = new Transaction();
		        trx.setId(rs.getLong("transaction_id"));
		        trx.setUserId(rs.getLong("user_id"));
		        trx.setCreditCardId(rs.getBigDecimal("credit_card_id"));
		        trx.setReferenceId(rs.getBigDecimal("reference_id"));
		        trx.setTypeId(rs.getLong("type_id"));
		        trx.setTimeCreated(DAOBase.convertToDate(rs.getTimestamp("time_created")));
		        trx.setAmount(rs.getLong("amount"));
		        trx.setStatusId(rs.getLong("status_id"));
		        trx.setDescription(rs.getString("description"));
		        trx.setWriterId(rs.getLong("trx_writer_id"));
		        trx.setIp(rs.getString("trx_ip"));
		        trx.setComments(rs.getString("comments"));
		        trx.setProcessedWriterId(rs.getLong("processed_writer_id"));
		        trx.setChequeId(rs.getBigDecimal("cheque_id"));
		        trx.setWireId(rs.getBigDecimal("wire_id"));
		        trx.setChargeBackId(rs.getBigDecimal("charge_back_id"));
		        trx.setReceiptNum(rs.getBigDecimal("receipt_num"));
		        trx.setAuthNumber(rs.getString("auth_number"));
		        trx.setXorIdAuthorize(rs.getString("xor_id_authorize"));
		        trx.setXorIdCapture(rs.getString("xor_id_capture"));
		        trx.setUtcOffsetCreated(rs.getString("utc_offset_created"));
		        trx.setClearingProviderId(rs.getLong("clearing_provider_id"));

				Date emailSentTime = IssuesDAOBase.isEmailSent(conn, user.getId(), trx.getId());
				String action = null;
				if (null != emailSentTime) {  // email allready sent
					Calendar today = Calendar.getInstance();
					Calendar emailIssue = Calendar.getInstance();
					emailIssue.setTime(emailSentTime);
					emailIssue.add(Calendar.DAY_OF_WEEK, ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL);
					if (today.after(emailIssue)) {
		                try {
							log.debug("Insert ReverseWithdraw: trxId: " + trx.getId());
		                	conn.setAutoCommit(false);
			                UsersDAOBase.addToBalance(conn, user.getId(), trx.getAmount());
			                GeneralDAO.insertBalanceLog(conn, Writer.WRITER_ID_AUTO, user.getId(), ConstantsBase.TABLE_TRANSACTIONS, trx.getId(), ConstantsBase.LOG_BALANCE_REVERSE_WITHDRAW, user.getUtcOffset());
			                TransactionsDAOBase.updateAfterReverse(conn, trx.getId(), TransactionsManagerBase.TRANS_STATUS_REVERSE_WITHDRAW, Writer.WRITER_ID_AUTO, user.getUtcOffset());
			                // Insert reverse transaction
			                trx.setReferenceId(new BigDecimal(trx.getId()));
			                trx.setWriterId(Writer.WRITER_ID_AUTO);
			                trx.setProcessedWriterId(Writer.WRITER_ID_AUTO);
			                trx.setTypeId(TransactionsManagerBase.TRANS_TYPE_REVERSE_WITHDRAW);
			                trx.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			                trx.setTimeSettled(new Date());
			                trx.setUtcOffsetSettled(user.getUtcOffset());
							TransactionsDAOBase.insert(conn, trx);
							log.debug("ReverseWithdraw insert successfully! revTrxId: " + trx.getId());
			                conn.commit();
			                action = "Reverse withdraw";
		                } catch (SQLException e) {
		            		log.error("Exception updating transaction. ", e);
		            		try {
		            			conn.rollback();
		            		} catch (Throwable it) {
		            			log.error("Can't rollback.", it);
		            		}
		            		throw e;
		            	} finally {
		            		try {
		            			conn.setAutoCommit(true);
		            		} catch (Exception e) {
		            			log.error("Can't set back to autocommit.", e);
		            		}
		            	}
					}
				} else {  // send withdrawal notification
				       HashMap<String,String> params = new HashMap<String, String>();
		   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, user.getFirstName());
		   		       params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, user.getLastName());
		   		       params.put(SendTemplateEmail.PARAM_CANCEL_WITHDRAWL_DAYS, String.valueOf(ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL));
		   		       params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(CommonUtil.getDateTimeFormaByTz(trx.getTimeCreated(), trx.getUtcOffsetCreated())));
		   		       params.put(SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(CommonUtil.getDateTimeFormaByTz(trx.getTimeCreated(), trx.getUtcOffsetCreated())));
		   		       Template t = TemplatesDAO.get(conn, Template.CANCEL_WITHDRAWAL_EMAIL_MAIL_ID);
		   		       sendSingleEmail(conn, properties, t.getFileName(), params, t.getSubject(),
		   		    		   null, true, false, 0, user, Long.valueOf(trx.getId()), t.getId());

 					   // add to user MailBox
					   if (user.getSkinId() != Skin.SKIN_ETRADER) {
						   try {
							   UsersManagerBase.SendToMailBox(user, t, Writer.WRITER_ID_AUTO, user.getLanguageId(), trx.getId(), conn, 0);
						   } catch (Exception e) {
							   log.warn("Problem adding email to user inbox! userId: " + user.getId());
						   }
					   }
					   action = "Email";
				}
				if (null != action) {
					report.append("<tr>");
	                report.append("<td>").append(trx.getUserId()).append("</td>");
	                report.append("<td>").append(action).append("</td>");
	                report.append("<td>").append(trx.getId()).append("</td>");
	                report.append("<td>").append(trx.getAmount()/100).append("</td>");
	                report.append("<td>").append(trx.getTimeCreated()).append("</td>");
	                report.append("</tr>");
				}
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
	}
}

