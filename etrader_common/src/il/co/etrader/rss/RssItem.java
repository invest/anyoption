/**
 *
 */
package il.co.etrader.rss;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * RssItem class
 * Saving data for one item in the rss file.
 * @author Kobi
 *
 */
public class RssItem implements Serializable {

	private static final long serialVersionUID = -4546529710160894228L;

	private long id;
	private String title;
	private String link;
	private String description;
	private String category;
	private String pubDate;

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	public String getDescriptionContent() {
		if (null != description) {
			if (description.indexOf("<br") > -1) {
				return description.substring(0, description.indexOf("<br")) + "<p />";
			} else {
				return getDescription() + "<p />";
			}
		}
		return "";
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the pubDate
	 */
	public String getPubDate() {
		return pubDate;
	}

	/**
	 * return publish date with utxOffset formating
	 * @return
	 * @throws ParseException
	 */
	public String getPubDateFormating() {
		DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
		Date reutersDate;
		try {
			reutersDate = df.parse(pubDate);
		} catch (ParseException e) {
			// cant parse date return empty string
			return "";
		}
		return CommonUtil.getAlternateDateTimeFormatDisplay(reutersDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @param pubDate the pubDate to set
	 */
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
