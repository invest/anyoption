/**
 *
 */
package il.co.etrader.rss;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.anyoption.common.util.ClearingUtil;


/**
 * Rss reader class
 * Sending GET request by url, parsing the xml (rss feed page) and return list of items
 * @author Kobi
 *
 */
public class RssReader {
	private static final Logger logger = Logger.getLogger(RssReader.class);


	/**
	 * Get list of rss items by rss feed url
	 * @param url Rss feed url
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<RssItem> getRssItems(String url) throws Exception {
        if (logger.isDebugEnabled()) {
        	logger.debug("Going to fetch rss data from URL: " + url);
        }
		Document doc = getDocumentFromURL(url);
        if (logger.isDebugEnabled()) {
        	logger.debug("Recieved the rss data");
        }
		return parseRssResponse(doc);
	}

    /**
     * Process Get request and return a Document instance
     * @param url
     * @return
     * @throws Exception
     */
    public static Document getDocumentFromURL(String url) throws Exception {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = null;
        InputStream is = null;
        try {
            URL u = new URL(url);
            HttpURLConnection httpCon = (HttpURLConnection) u.openConnection();
            is = httpCon.getInputStream();
            doc = docBuilder.parse(is);
        } finally {
            if (null != is) {
            	is.close();
            }
        }
        return doc;
    }

    /**
     * Parse rss file and return ArrayList of items
     * @param xml the resonse xml
     * @return
     * @throws Exception
     */
    private static ArrayList<RssItem> parseRssResponse(Document resp) throws Exception {
    	Element root = resp.getDocumentElement();
    	NodeList nl = root.getElementsByTagName("item");
    	ArrayList<RssItem> items = new ArrayList<RssItem>();
    	for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            RssItem item = new RssItem();
            item.setTitle(ClearingUtil.getElementValue((Element)node, "title/*"));
            item.setLink(ClearingUtil.getElementValue((Element)node, "link/*"));
            item.setDescription(ClearingUtil.getElementValue((Element)node, "description/*"));
            item.setCategory(ClearingUtil.getElementValue((Element)node, "category/*"));
            item.setPubDate(ClearingUtil.getElementValue((Element)node, "pubDate/*"));
            items.add(item);
        }
    	return items;
    }

}
