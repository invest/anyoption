package il.co.etrader.bonus;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.BonusUsersStep;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlersException;

public class BonusMessageHandler {
	private static final Logger log = Logger.getLogger(BonusMessageHandler.class);
	
	public static String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		long bonusType = b.getTypeId();
        if (log.isTraceEnabled()) {
            log.trace("Getting message for bonusType: " + bonusType);
        }

        if(bonusType == ConstantsBase.BONUS_TYPE_INSTANT_AMOUNT || bonusType == ConstantsBase.BONUS_TYPE_ROUNDUP) {
        	return getBonusInstantAmountHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_INSTANT_NEXT_INVEST_ON_US) {
        	return getBonusNextInvOnUsHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_AMOUNT_AFTER_DEPOSIT) {
        	return getBonusAmountAfterDepositHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_PERCENT_AFTER_DEPOSIT) {
        	return getBonusPercentAfterDepositHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_AMOUNT_AFTER_WAGERING) {
        	return getBonusAmountAfterWageringHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING) {
        	return getBonusNextInvOnUsAfterWageringHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if(bonusType == ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS) {
        	return getBonusNextInvOnUsAfterXInvsHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_RETURN_AND_REFUND) {
        	return getBonusInvIncReturnAndRefundHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_RETURN) {
        	return getBonusInvIncReturnOrRefundHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_REFUND) {
        	return getBonusInvIncReturnOrRefundHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if (bonusType == ConstantsBase.BONUS_TYPE_PERCENT_AFTER_DEPOSIT_AND_SUM_INVEST) {
        	return getBonusPercentAfterDepositAndSumInvestHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else if (bonusType == ConstantsBase.BONUS_TYPE_ROUNDUP) {
        	return getBonusInstantAmountHandlerMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, bonusAmount, sumInvQualify, sumDeposit);
        } else {
        	// create only instance with relevant type
        	log.error("Error in finding bonus message for bonus type " + bonusType);
        	return null;
        }
     }
	
	/**
	 *  getBonusMessage event handler implementation
	 */
	public static String getBonusAmountAfterDepositHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();
			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if(b.getNumberOfActions()>1) {
				out+="Make " + b.getNumberOfActions() +" deposits and get " + bc.getBonusAmount()/100 + " to your account<br/>";
			}

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				if(!(b.getNumberOfActions()>1)){
					out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
							b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
							String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
							String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";
				}

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount > 0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}
			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}

			out += "Wagering parameter "+b.getWageringParameter()+" ";
			out += "on the "+b.getCondition();

		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}

		return out;
	}


	public static String getBonusAmountAfterWageringHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted


			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				if (b.getId() == ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID){
					out += CommonUtil.getBonusDescription(bonusAmount/100,bc.getBonusPercent(),sumInvQualify/100,
							b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
							String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
							String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

					if (minDepositAmount >0){
						out += "<br/>" +
						CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
					}
				} else {
					out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
							b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
							String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
							String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

					out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

					if (minDepositAmount >0){
						out += "<br/>" +
						CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
					}
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}


	public static String getBonusInstantAmountHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}
	
	public static String getBonusInvIncReturnAndRefundHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}

	public static String getBonusInvIncReturnOrRefundHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}
	

	public static String getBonusNextInvOnUsAfterWageringHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}


	public static String getBonusNextInvOnUsAfterXInvsHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}

	public static String getBonusNextInvOnUsHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}

			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}


	public static String getBonusPercentAfterDepositAndSumInvestHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";

		try{
			long bonusId = b.getId();

			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted


			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
				long minDepositAmount = limitsBu.getSumDeposits();

				Double bonusPercent = null;
				ArrayList<BonusUsersStep> bonusSteps = BonusManagerBase.getBonusSteps(bonusId, currencyId);
				for (int index=0; index<bonusSteps.size(); index++){
	        		if (bonusSteps.get(index).getMinDepositAmount() <= sumDeposit &&
	        				bonusSteps.get(index).getMaxDepositAmount() >= sumDeposit){
    					bonusPercent = bonusSteps.get(index).getBonusPercent();
	   				}
	        	}
				BigDecimal bd = new BigDecimal(sumDeposit);
	            bd = bd.multiply(new BigDecimal(bonusPercent));
	            bd = CommonUtil.round(bd, new BigDecimal(1));
	            bonusAmount = bd.longValue();
	            sumInvQualify = bonusAmount * b.getTurnoverParam();
				
				out += CommonUtil.getBonusDescription(bonusAmount/100,bc.getBonusPercent(),sumInvQualify/100,
						b.getNumberOfActions(),descMsg,String.valueOf(sumDeposit/100),String.valueOf(bc.getMaxDepositAmount()/100),
						String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
						String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";

				if (minDepositAmount >0){
					out += "<br/>" +
					CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
				}
				
			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message" , e);
		}
		return out;
	}

	public static String getBonusPercentAfterDepositHandlerMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
		String out = "";
		boolean isHaveSteps = b.isHasSteps();
		long bonusId = b.getId();

		try{
			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted

			if(b.getNumberOfActions()>1)	{
				out+="Make " + b.getNumberOfActions() +" deposits and get " + bc.getBonusPercent()*100 + "% of the " +b.getNumberOfActions()+"th  deposit to your account<br/>";
			}
			if (null != bc){
				long currencyId = bc.getCurrencyId();
				BonusUsers limitsBu = new BonusUsers();

				limitsBu.setBonusId(bonusId);
				boolean isHasLimits = BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, isHaveSteps);
				long minDepositAmount = limitsBu.getSumDeposits();

				if(!isHaveSteps){
					if(!(b.getNumberOfActions()>1)){
						out += CommonUtil.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
								b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
								String.valueOf(bc.getMinInvestAmount()/100), String.valueOf(bc.getMaxInvestAmount()), String.valueOf((int)(b.getOddsWin()*100)),
								String.valueOf((int)(b.getOddsLose()*100)), String.valueOf(currencyId)) +  " <br/>";
					}

					out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";

					if (minDepositAmount >0){
						out += "<br/>" +
						CommonUtil.getMessage("bonus.population.grant.msg", new String[] {CommonUtil.displayAmount(minDepositAmount, true, currencyId)});
					}


				} else { // bonus has steps
					ArrayList<BonusUsersStep> bonusSteps = null;

					if (isHasLimits) {
						bonusSteps = limitsBu.getBonusSteps();
	            	}else{
	            		bonusSteps = BonusManagerBase.getBonusSteps(bonusId, currencyId);
	            	}

					if (null != bonusSteps){
						for(BonusUsersStep step:bonusSteps) {
							out += "Make a deposit between "+step.getMinDepositAmount()/100+" and "+step.getMaxDepositAmount()/100;
							out += " and get bonus of "+step.getBonusPercent()*100+"% of the deposit to your account<br/>";
							out += "Wagering parameter "+b.getWageringParameter()*step.getBonusPercent()+" ";
							out += "on the "+b.getCondition()+"<br/>";
						}
					}else{
						throw new BonusHandlersException("No Bonus steps for bonus id " + bonusId +
								" with currency id: " + currencyId + " for user id " + userId);
					}
				}
			}else{
				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId + " for user id " + userId);
			}
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in creating bonus message for bonus id " + bonusId + " for user id " + userId , e);
		}
		return out;
	}
}
