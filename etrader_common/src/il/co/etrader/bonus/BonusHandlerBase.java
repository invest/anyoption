//package il.co.etrader.bonus;
//
//import il.co.etrader.bl_managers.BonusManagerBase;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.bl_managers.UsersManagerBase;
//import il.co.etrader.bl_vos.Investment;
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.dao_managers.BonusDAOBase;
//import il.co.etrader.dao_managers.TransactionsDAOBase;
//import il.co.etrader.dao_managers.UsersDAOBase;
//import il.co.etrader.util.ConstantsBase;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.common.daos.GeneralDAO;
//
//public abstract class BonusHandlerBase {
//
//    /**
//     * Process actions before bonus insert through this handler.
//     *
//     * @param conn - Connection
//     * @param bu bonus user
//     * @param b bonus
//     * @param bc Bonus Currency
//     * @param user
//     * @param writerId
//     * @param popEntryId
//     * @param bonusPopLimitTypeId
//     */
//	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, UserBase user, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException{
//		boolean res = false;
//		bu.setBonusStateId(ConstantsBase.BONUS_STATE_PENDING);
//		
//		try{
//			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId, bonusPopLimitTypeId,user.getId(), user.getCurrencyId(), false);
//
//			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in inserting bonus" , e);
//		}
//		return res;
//	}
//
//    /**
//     * accept bonus and move the bonus to state granted
//     * @param conn db connection
//     * @param bu bonus user to update
//     * @param user user to update
//     * @return
//     * @throws SQLException 
//     */
//    public boolean acceptBonus(Connection conn, BonusUsers bu, UserBase user) throws SQLException {	
//		return BonusDAOBase.acceptBonusUser(conn, bu.getId(), ConstantsBase.BONUS_STATE_GRANTED);
//	}
//    
//    /**
//     * Process bonus message for writers through this handler.
//     * @param bc Bonus Currency
//     * @param popEnteyId
//     * @param bonusPopLimitTypeId
//     * @param userId
//     * @param bu bonus user
//     */
//    public abstract String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException,SQLException;
//
//    /**
//     * Process a check for bonus activation through this handler.
//     *
//     * @param conn - Connection
//     * @param bu - bonus user
//     * @param amount - bonus amount
//     * @param transactionId TODO
//     */
//    public abstract boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException;
//
//    /**
//     * Process an activation for bonus through this handler.
//     *
//     * @param conn - Connection
//	 * @param bu - bonus user
//	 * @param transactionId
//	 * @param userId
//	 * @param amount - bonus amount
//	 * @param writerId
//     */
//    public abstract void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException;
//
//    /**
//     * Process used update for bonuses after inv success through this handler.
//     *
//     * @param conn - Connection
//	 * @param bu - bonus user
//	 * @param amountLeft - the amount that left after current bonus was used
//     */
//    public abstract long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException;
//
//    /**
//     * Process update wagering for bonuses after inv success through this handler.
//     *
//     * @param conn - Connection
//     * @param bu - bonus user
//     * @param amountLeft - the amount that left before current bonus was used
//     * @param investmentId
//	 * @return amountLeft  - the amount that left after current bonus was used
//     */
//    public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
//		try{
//			if (bu.getSumInvWithdrawal() - bu.getSumInvWithdrawalReached() > amountLeft) {
//	            BonusDAOBase.addBonusUserWithdrawWagering(conn, bu.getId(), amountLeft);
//	            return 0;
//	        } else {
//	            BonusDAOBase.doneBonusUsers(conn, bu.getId(),true, investmentId);
//	            return (amountLeft - (bu.getSumInvWithdrawal() - bu.getSumInvWithdrawalReached()));
//	        }
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in wageringAfterInvestmentSuccess ", e);
//		}
//    }
//
//    /**
//     * cancel bonus to user
//     * This method update bonus user state by given stateToUpdate.
//     * Override this method when needed.
//     *
//     * @param conn
//     * @param bonusUser
//     * @param stateToUpdate
//     * @param utcOffset
//     * @param writerId
//     */
//    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId) throws BonusHandlersException {
//    	try {
//	    	BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), stateToUpdate, bonusUser.getWriterIdCancel());
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error cancelBonusToUser ",e);
//		}
//    }
//
//    /**
//     * setBonusStateDescription
//     * @param bonusUser
//     */
//    public abstract void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException;
//
//    /**
//     * Process activate bonuses after inv success by inv amount through this handler.
//     *
//     * @param conn - Connection
//	 * @param bu - bonus user
//	 * @param amountLeft - the amount that left before current bonus was used
//	 * @param userId
//	 * @param investmentId
//	 * @param writerId
//	 * @param isFree - is this a free bonus type
//     * @return TODO
//	 * @return amountLeft  - the amount that left after current bonus was used
//     */
//    public abstract boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException;
//
//    /**
//     * Process activate bonuses after inv success by inv amount through this handler.
//     *
//     * @param conn - Connection
//     * @param bu - bonus user
//     * @param isInvWasCountForActivation - is inv was count for bonus activation before this one
//     * @param investmentId TODO
//	 * @return isInvWasCountForActivation - is inv was count for bonus activation after this one
//     */
//    public abstract boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException;
//
//    /**
//     * Returns BonusAmountOnSettleInvestment
//     * @param conn TODO
//     * @param investment - The settled investment
//     * @param isWin - is investment in win status
//     * @param isVoidBet - is investment in VoidBet status
//     *
//     * @return bonus amount
//     */
//    public abstract long handleBonusOnSettleInvestment(Connection conn, Investment investment, boolean isWin) throws BonusHandlersException;
//
//
//    /**
//     * Returns the amount that user can't withdraw due to used niou class bonuses.
//     *
//     * @param win - investment win amount
//     * @param lose TODO
//     * @param invAmount
//     * @param bu - BonusUsers
//     * @return bonus amount
//     */
//    public abstract long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu);
//
//
//    protected void activateBonusAndAddToBalance(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId) throws BonusHandlersException{
//		try{
//	        BonusDAOBase.activateBonusUsers(conn, bu, transactionId, investmentId);
//
//            UsersDAOBase.addToBalance(conn, userId, bu.getBonusAmount());
//            UserBase user = new UserBase();
//            UsersManagerBase.getByUserId(userId, user);
//            TransactionsDAOBase.insert(conn, TransactionsManagerBase.createBonusTransaction(userId, bu.getBonusAmount(), bu.getId(), writerId, user.getUtcOffset()));
//            GeneralDAO.insertBalanceLog(
//                    conn,
//                    writerId,
//                    userId,
//                    ConstantsBase.TABLE_BONUS_USERS,
//                    bu.getId(),
//                    ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT,
//                    user.getUtcOffset());
//
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in activateBonus from activateBonusAndAddToBalance ", e);
//		}
//	}
//    
//    protected void addBonusToBalance(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId) throws BonusHandlersException{
//		try{
//            UsersDAOBase.addToBalance(conn, userId, bu.getBonusAmount());
//            UserBase user = new UserBase();
//            UsersManagerBase.getByUserId(userId, user);
//            TransactionsDAOBase.insert(conn, TransactionsManagerBase.createBonusTransaction(userId, bu.getBonusAmount(), bu.getId(), writerId,user.getUtcOffset()));
//            GeneralDAO.insertBalanceLog(
//                    conn,
//                    writerId,
//                    userId,
//                    ConstantsBase.TABLE_BONUS_USERS,
//                    bu.getId(),
//                    ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT,
//                    user.getUtcOffset());
//
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in addToBalance from addBonusToBalance ", e);
//		}
//	}
//    
//    protected void activateBonus(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId) throws BonusHandlersException{
//		try{
//	        BonusDAOBase.activateBonusUsers(conn, bu, transactionId, investmentId);
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in activateBonus from activateBonusAndAddToBalance ", e);
//		}
//	}
//    
//    /**
//     * @param conn
//     * @param bu
//     * @param investmentId
//     * @param bonusAmountFromInv - the part of the amount from the investment that the user used.
//     * @param adjustedAmountHistory
//     * @param typeId
//     * @throws BonusHandlersException
//     */
//    public void insertBonusInvestments(Connection conn, BonusUsers bu, long investmentId, long bonusAmountFromInv, long adjustedAmountHistory, int typeId) throws BonusHandlersException {
//		try {
//			BonusDAOBase.insertBonusInvestments(conn, investmentId, bu.getId(), bonusAmountFromInv, adjustedAmountHistory, typeId);
//		} catch (SQLException e) {
//			throw new BonusHandlersException("BMS, Error in insertBonusInvestments, bonus_users_id: " + bu.getId(), e);
//		}
//    }
//
//
//	/**
//	 * Call dao base to update bonus adjusted amount, adjusted amount will be 0 if bonus used < 0.
//	 * @param conn
//	 * @param bu
//	 * @param bonusUsed - can be negative.
//	 * @return bonusUsed for the next bonus. if negative => make it positive to insert to the next bonus (adjusted amount).
//	 * @throws BonusHandlersException
//	 */
//	public long updateAdjustedAmount(Connection conn, BonusUsers bu, long bonusUsed) throws BonusHandlersException {		
//		try {
//			BonusDAOBase.updateAdjustedAmount(conn, bu.getId(), bonusUsed < 0 ? 0 : bonusUsed); 
//		} catch (SQLException e) {
//			throw new BonusHandlersException("BMS, Error in wageringAfterInvestmentSuccess bonus_users id:" + bu.getId() + ", bonusUsed: " + bonusUsed , e);
//		}
//		return Math.abs(bonusUsed);
//	}
//}