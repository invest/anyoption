//package il.co.etrader.bonus;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.InvestmentBonusData;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bonus.BonusHandlerBase;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.common.bonus.BonusUtil;
//import com.anyoption.common.daos.BonusDAOBase;
//import com.anyoption.common.util.ConstantsBase;
//
//public class BonusNextInvOnUsAfterXInvsHandler extends BonusHandlerBase {
//
//
//	/**
//	 *  isActivateBonus event handler implementation
//	 */
//	@Override
//	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{
//
//		long depositsSum = bu.getSumDeposits();
//
//    	// Check if requiered deposits sum > 0
//    	if (depositsSum > 0){
//
//    		if (bu.getSumDepositsReached() == 0 && amount >= depositsSum){
//    			// Add the amount of current deposit to sum deposits reached
//        		try{
//        			bu.setSumDepositsReached(amount);
//        			BonusDAOBase.addBonusUsersSumDeposits(conn, bu.getId(), amount);
////        			TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
//        		}catch (SQLException e) {
//    				throw new BonusHandlersException("can't addBonusUsersSumDeposits ", e);
//    			}
//    		}
//
//        	if (bu.getSumDepositsReached() >= depositsSum &&
//        			bu.getNumOfActionsReached() >= bu.getNumOfActions()){
//        		return true;
//        	}else{
//        		return false;
//        	}
//    	}
//
//    	return false;
//	}
//
//	/**
//	 *  activateBonusAfterTransactionSuccess event handler implementation
//	 */
//	@Override
//	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException{
//    	try {
//    		BonusDAOBase.activateBonusUsers(conn, bu, transactionId, 0);
//    	} catch (SQLException sqle) {
//            throw new BonusHandlersException("can't activateBonusUsers ",sqle);
//        }
//	}
//
//	/**
//	 *  touchBonusesAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException{
//        return amountLeft;
//	}
//
//	/**
//	 *  wageringAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
//		if (bu.getBonusStateId() != ConstantsBase.BONUS_STATE_ACTIVE){
//			amountLeft = super.wageringAfterInvestmentSuccess(conn, bu, amountLeft, investmentId);
//		}
//		return amountLeft;
//	}
//
//
//	/**
//	 *  setBonusStateDescription event handler implementation
//	 */
//	@Override
//    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
//		// Do Nothing
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
//	 */
//	@Override
//	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException{
//		return false;
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
//	 */
//	@Override
//    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu, boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException{
//        if (!isInvWasCountForActivation){
//        	try{
//    			if (bu.getNumOfActionsReached() + 1 < bu.getNumOfActions()) {
//    				BonusDAOBase.addBonusUsersAction(conn, bu.getId());
//     	            isInvWasCountForActivation = true;
//    	        } else if (bu.getNumOfActionsReached() + 1 == bu.getNumOfActions()){
//    	        	// check if sum deposit was reached (in case there is one)
//    	        	if (bu.getSumDeposits() == 0 || bu.getSumDepositsReached() >= bu.getSumDeposits()){
//        	            BonusDAOBase.activateBonusUsers(conn, bu, 0, investmentId);
//    	        	}else{
//    	        		BonusDAOBase.addBonusUsersAction(conn, bu.getId());
//    	        	}
//	        		isInvWasCountForActivation = true;
//    	        }
//            }catch (SQLException e) {
//            	throw new BonusHandlersException("Error activateBonusAfterInvestmentSuccessByInvCount ",e);
//    		}
//        }
//        return isInvWasCountForActivation;
//	}
//
//	/**
//	 *  handleBonusOnSettleInvestment event handler implementation
//	 */
//	@Override
//	public long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException{
//        return 0;
//	}
//
//	/**
//	 *  getAmountThatUserCantWithdraw event handler implementation
//	 */
//	@Override
//	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
//
//		if (win > 0){
//			return win;
//		}else{
//			return lose;
//		}
//	}
//	
//	/**
//	 *  cancelBonusToUser event handler implementation
//	 */
//	@Override
//    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip) throws BonusHandlersException {
//		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip);
//    }
//}