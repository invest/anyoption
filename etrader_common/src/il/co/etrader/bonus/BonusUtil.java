//package il.co.etrader.bonus;
//
//import il.co.etrader.bl_managers.BonusManagerBase;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.bl_vos.Skins;
//import il.co.etrader.util.ConstantsBase;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bonus.BonusHandlersException;
//
//
///**
// * 
// * @author Lior
// *
// */
//public class BonusUtil {
//
//	private static final Logger log = Logger.getLogger(BonusUtil.class);
//
//    /**
//     * This method cancel bonus to user with the withdraw mechanism if the bonus is in active or in used state.
//     * 
//     * @param conn
//     * @param bonusUser
//     * @param stateToUpdate
//     * @param utcOffset
//     * @param writerId
//     * @param skinId
//     * @throws BonusHandlersException
//     */
//    public static void cancelBonusToUserWithdraw(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId) throws BonusHandlersException {
//		try {
//	    	BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), stateToUpdate, bonusUser.getWriterIdCancel());
//	    	if (bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE || bonusUser.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
//	    		long amountToDeduct = bonusUser.getBonusAmount();
//	    		if (skinId != Skins.SKIN_ETRADER) {
//	    			amountToDeduct = Math.max(bonusUser.getBonusAmount(), bonusUser.getAdjustedAmount());
//	    		}
//	    		TransactionsManagerBase.insertBonusWithdraw(conn, amountToDeduct, bonusUser.getUserId(), utcOffset, writerId, bonusUser.getId());
//	    	}
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error cancelBonusToUserWithdraw ",e);
//		}
//    }   
//}
