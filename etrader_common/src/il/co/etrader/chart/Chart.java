package il.co.etrader.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketRate;

/**
 * Simple value / time chart created over <code>MarketRate</code>s.
 *
 * @author Tony
 */
public class Chart {
    private static final Logger log = Logger.getLogger(Chart.class);

//    private static final BigDecimal TEN = new BigDecimal("10");

    private String topRightMsg;
    private int displayWidth;
    private int displayHeight;
    private ArrayList<MarketRate> rates;
    private BigDecimal minRate;
    private BigDecimal maxRate;
    private String first5MinLine1;
    private String first5MinLine2;
    private String weCameFrom;
    private Color fontColor;
    private String fontName;

    public Chart(String topRightMsg, int width, int height, String ffLine1, String ffLine2) {
        this.topRightMsg = topRightMsg;
        displayWidth = width;
        displayHeight = height;
        rates = new ArrayList<MarketRate>();
        first5MinLine1 = ffLine1;
        first5MinLine2 = ffLine2;
        
        // defult for etrader
        fontColor = Color.BLACK;
        fontName = "Arial";
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        weCameFrom = bundle.getString("domain.url");
        if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
        	fontColor = new Color(9, 55, 85);
            fontName = "Verdana";
        }
    }

    public void addRate(MarketRate marketRate) {
        rates.add(marketRate);
        if (null == minRate || minRate.compareTo(marketRate.getRate()) > 0) {
            minRate = marketRate.getRate();
        }
        if (null == maxRate || maxRate.compareTo(marketRate.getRate()) < 0) {
            maxRate = marketRate.getRate();
        }
    }

    public MarketRate getLastRate() {
        if (null == rates || rates.size() == 0) {
            return null;
        }
        return rates.get(rates.size() - 1);
    }

    public void clearRates() {
        if (null != rates) {
            rates.clear();
        }
    }

    /*
     * old function use to create 1 chart per day
     */
//    public byte[] getImageBytes() {
//        BufferedImage bufferedImage = new BufferedImage(displayWidth, displayHeight, BufferedImage.TYPE_INT_RGB);
//        Graphics2D g = bufferedImage.createGraphics();
//
//        // background
//        g.setColor(Color.WHITE);
//        g.fillRect(0, 0, displayWidth, displayHeight);
//
//        // labels
//        Font font = new Font(fontName, Font.PLAIN, 10);
//        g.setFont(font);
//        FontMetrics metrics = g.getFontMetrics();
//
//        if (rates.size() > 0) {
//            // left labels
//            BigDecimal[] labelsY = getLabelsY(minRate, maxRate);
//
//            int llw = metrics.stringWidth(labelsY[labelsY.length - 1].toString());
//            int llh = metrics.getHeight();
//
//            int availablePixels = displayWidth - llw - 10;
//            int chartWidth =
//                    (rates.size() > availablePixels ?
//                            availablePixels :
//                            rates.size());
//
//            int chartHeight = displayHeight - 2 * llh;
//
//            int llStep = chartHeight / (labelsY.length - 1);
//            g.setColor(fontColor);
//            for (int i = 0; i < labelsY.length; i++) {
//                g.drawString(labelsY[i].toString(), 0, displayHeight - llh - i * llStep);
//            }
//
//            // date
//            SimpleDateFormat df = new SimpleDateFormat("MMM dd, hh:mma", Locale.US);
//            g.drawString(df.format(new Date()), llw, llh - 3);
//
//            // chart
//            g.setColor(new Color(214, 220, 236));
//            int cLft = llw + 1;
//            int cBtm = displayHeight - llh - 1;
//            for (int i = 0; i < labelsY.length; i++) {
//                g.drawLine(cLft, cBtm - i * llStep, cLft + chartWidth, cBtm - i * llStep);
//            }
//            g.drawLine(cLft, cBtm, llw + 1, cBtm - (labelsY.length - 1) * llStep);
//            g.drawLine(cLft + chartWidth, cBtm, cLft + chartWidth, cBtm - (labelsY.length - 1) * llStep);
//
//            int minsBetweenQuotes = Math.round((float) (rates.get(1).getRateTime().getTime() - rates.get(0).getRateTime().getTime()) / 60000);
//
//            // rates
//            g.setColor(fontColor);
//            MarketRate mr = null;
//            boolean displayTime = true;
//            Calendar cal = Calendar.getInstance();
//            int prevY = 0;
//            double yInterval = labelsY[labelsY.length - 1].subtract(labelsY[0]).doubleValue();
//            int firstDisplayRate = rates.size() > chartWidth ? rates.size() - chartWidth : 0;
//            for (int i = 0; i < rates.size() - firstDisplayRate; i++) {
//                mr = rates.get(i + firstDisplayRate);
//                cal.setTime(mr.getRateTime());
//                int y = cBtm - (int) Math.round((mr.getRate().doubleValue() - labelsY[0].doubleValue()) / yInterval * chartHeight);
//                if (i > 0) {
//                    g.drawLine(cLft + i - 1, prevY, cLft + i, y);
//                } else {
//                    g.drawLine(cLft + i, y, cLft + i, y);
//                }
//                prevY = y;
//                if (cal.get(Calendar.MINUTE) < minsBetweenQuotes) {
//                    g.drawLine(cLft + i, cBtm + 1, cLft + i, cBtm + 1);
//                    if (displayTime) {
//                        SimpleDateFormat tdf = new SimpleDateFormat("ha");
//                        g.drawString(tdf.format(mr.getRateTime()), cLft + i, displayHeight - 3);
//                    }
//                    displayTime = !displayTime;
//                }
//            }
//        }
//
//        byte[] ba = null;
//        try {
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            ImageIO.write(bufferedImage, "png", baos);
//            baos.flush();
//            ba = baos.toByteArray();
//            baos.close();
//        } catch (Throwable t) {
//            // TODO process it
//        }
//        return ba;
//    }

    /**
     * Create chart image. Called by the chart servlet every time it gets new value to be painted.
     * 
     * return The image as byte[] ready to be streamed.
     */
    public byte[] getLast30ImageBytes() {
        BufferedImage bufferedImage = new BufferedImage(displayWidth, displayHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufferedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        // background
        g.setColor(new Color(220, 223, 222));
        g.fillRect(0, 0, displayWidth, displayHeight);

        // labels
     //   Font font = new Font("SansSerif", Font.PLAIN, 12);
        Font font = new Font(fontName, Font.PLAIN, 10);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();

        if (rates.size() > 0) {
            // left labels
            BigDecimal[] labelsY = getLast30MinAndMax();

            int llw = metrics.stringWidth(labelsY[labelsY.length - 1].toString());
//            int llh = metrics.getHeight();
            int llh = metrics.getAscent() + 2; // The chars height + 1 pixel above and below

            int chartWidth = displayWidth - llw - 15;
            int xStep = chartWidth / 30;
            chartWidth = xStep * 29 + 1; // first rate is a dot. the rest are "lines" that connect the two rates
            int xOffset = (displayWidth - llw - chartWidth) / 2;
            int chartHeight = displayHeight - 2 * llh;
            int cLft = llw + xOffset;

            g.setColor(fontColor);
            g.drawString(labelsY[0].toString(), xOffset, displayHeight - llh - 5);
            g.drawString(labelsY[1].toString(), xOffset, displayHeight - chartHeight - llh + 2);


            font = new Font(fontName, Font.BOLD, 11);
            g.setFont(font);

            // chart
            g.setColor(new Color(211, 213, 213));

            int cBtm = displayHeight - llh - 5; // chart bottom y position
            g.drawRect(cLft, cBtm - chartHeight, chartWidth, chartHeight);
            g.setColor(new Color(248, 248, 248));
            g.fillRect(cLft + 1, cBtm - chartHeight + 1, chartWidth - 1, chartHeight - 1);

            // rates
            g.setColor(fontColor);
            MarketRate mr = null;
            Calendar cal = Calendar.getInstance();
            int prevY = 0;
            double yInterval = labelsY[labelsY.length - 1].subtract(labelsY[0]).doubleValue();
            int firstRateIndex = rates.size() > 30 ? rates.size() - 30 : 0;
            int ratesToDisplay = rates.size() < 30 ? rates.size() : 30;
            SimpleDateFormat tdf = new SimpleDateFormat("HH:mm");
            Double minToPrint = new Double(0);
            Double nextPrintStep = new Double(14);
            tdf.setTimeZone(TimeZone.getTimeZone("Asia/Jerusalem"));
            if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            	minToPrint = new Double(0);
            	nextPrintStep = new Double(7);
            	tdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            }

            for (int i = 0; i < ratesToDisplay; i++) {
                mr = rates.get(i + firstRateIndex);
                cal.setTime(mr.getRateTime());
                int y = cBtm - (int) Math.round((mr.getRate().doubleValue() - labelsY[0].doubleValue()) / yInterval * chartHeight);
                if (i > 0) {
                    g.drawLine(cLft + (i - 1) * xStep, prevY, cLft + i * xStep, y);
                } else {
                    g.drawLine(cLft + i * xStep, y, cLft + i * xStep, y);
                }
                prevY = y;
                if (/*cal.get(Calendar.MINUTE) % 6 == 0*/ i == minToPrint.intValue()) {
                	minToPrint += nextPrintStep;
                	if (14 == minToPrint.intValue()) {
                		minToPrint++;
                	}
                    g.drawLine(cLft + i * xStep, cBtm - 3, cLft + i * xStep, cBtm);
                    String ftm = tdf.format(mr.getRateTime());
                    font = new Font(fontName, Font.PLAIN, 10);
                    g.setFont(font);
                    g.drawString(ftm, cLft + i * xStep - metrics.stringWidth(ftm) / 2, displayHeight - 3);
                }

                font = new Font(fontName, Font.PLAIN, 10);
                g.setFont(font);
            }
            font = new Font(fontName, Font.BOLD, 11);
            g.setFont(font);
            if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            	mr = rates.get(0 + firstRateIndex);
            	g.drawString("GMT", cLft - 29 - metrics.stringWidth(tdf.format(mr.getRateTime())) / 2 , displayHeight - 3);
            }
//            System.out.println("   ");
//            SimpleDateFormat tdf = new SimpleDateFormat("HH:mm");
//            g.drawString(tdf.format(rates.get(firstRateIndex).getRateTime()), cLft, displayHeight - 3);
//            String endTime = tdf.format(rates.get(rates.size() - 1).getRateTime());
//            g.drawString(endTime, cLft + chartWidth - metrics.stringWidth(endTime), displayHeight - 3);

            // brand
//            if (null != brand) {
//                g.drawString(brand, cLft + chartWidth - metrics.stringWidth(brand), cBtm);
//            }
        } else {
            int llw = 20;
            int llh = metrics.getAscent() + 2; // The chars height + 1 pixel above and below

            int chartWidth = displayWidth - llw;
            int chartHeight = displayHeight - 2 * llh;
            int xOffset = (displayWidth - llw - chartWidth) / 2;

            // chart
            g.setColor(new Color(211, 213, 213));
            int cLft = llw + xOffset;
            int cBtm = displayHeight - llh - 1;
            g.drawRect(cLft, cBtm - chartHeight, chartWidth, chartHeight);
            g.setColor(new Color(248, 248, 248));
            g.fillRect(cLft + 1, cBtm - chartHeight + 1, chartWidth - 1, chartHeight - 1);

            g.setColor(fontColor);

//            g.drawString(marketName, llw + xOffset, llh - 2);
//            g.drawString(topRightMsg, llw + xOffset + chartWidth - metrics.stringWidth(topRightMsg), llh - 2);
            if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            	g.drawString(topRightMsg, llw + xOffset, llh - 2);
            }
            font = new Font(fontName, Font.PLAIN, 11);
            g.setFont(font);
            metrics = g.getFontMetrics();

            int line1Width = metrics.stringWidth(first5MinLine1);
            int line2Width = metrics.stringWidth(first5MinLine2);
            int line1Left = cLft + (chartWidth - line1Width ) / 2;
            int line1Base = cBtm - chartHeight / 2;
            g.drawString(first5MinLine1, line1Left, line1Base);
            if (weCameFrom.equalsIgnoreCase("anyoption.com")) {
            	g.drawString(first5MinLine2, line1Left + (line1Width/2) - (line2Width/2), line1Base + llh);
            } else {
            	g.drawString(first5MinLine2, line1Left + line1Width - line2Width, line1Base + llh);
            }
        }

        byte[] ba = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "png", baos);
            baos.flush();
            ba = baos.toByteArray();
            baos.close();
        } catch (Throwable t) {
        	log.error("Problem creating the image.", t);
        }
        return ba;
    }

//    private static BigDecimal round(BigDecimal number, BigDecimal roundOrder) {
//        BigDecimal result = number.divide(roundOrder);
//        if (result.precision() < result.scale()) {
//            log.debug("result: " + result + " precision: " + result.precision() + " scale: " + result.scale());
//        }
//        int rmp = result.precision() - result.scale();
//        if (rmp < 0) {
//            rmp = 1;
//        }
//        result = result.round(new MathContext(rmp));
//        result = result.multiply(roundOrder);
//        return result;
//    }

//    private static BigDecimal[] getLabelsY(BigDecimal minRate, BigDecimal maxRate, BigDecimal roundOrder) {
//        BigDecimal minRnd = round(minRate, roundOrder);
//        BigDecimal maxRnd = round(maxRate, roundOrder);
//
//        BigDecimal firstLbl = minRate.compareTo(minRnd) < 0 ? minRnd.subtract(roundOrder) : minRnd;
//        BigDecimal lastLbl = maxRate.compareTo(maxRnd) > 0 ? maxRnd.add(roundOrder) : maxRnd;
//
//        BigDecimal[] result = new BigDecimal[lastLbl.subtract(firstLbl).divide(roundOrder).intValue() + 1];
//        for (int i = 0; i < result.length; i++) {
//            result[i] = firstLbl.add(roundOrder.multiply(new BigDecimal(i)));
//        }
//        return result;
//    }

//    private static BigDecimal[] getLabelsY(BigDecimal minRate, BigDecimal maxRate) {
//        BigDecimal roundOrder = new BigDecimal("1000");
//        BigDecimal[] labelsY = null;
//        BigDecimal[] labelsYtmp = getLabelsY(minRate, maxRate, roundOrder);
//        do {
//            labelsY = labelsYtmp;
//            roundOrder = roundOrder.divide(TEN);
//            labelsYtmp = getLabelsY(minRate, maxRate, roundOrder);
//        } while (labelsYtmp.length <= 5);
//        return labelsY;
//    }

    /**
     * @return Array of two <code>BigDecimal</code>s. First is the min rate, second is the max rate (out of the last 30).
     */
    private BigDecimal[] getLast30MinAndMax() {
        BigDecimal lastRate = null;
        if (rates.size() > 0) {
            lastRate = rates.get(rates.size() - 1).getRate();
        }
        BigDecimal[] rez = new BigDecimal[] {
                lastRate,
                lastRate
        };
        BigDecimal r = null;
        for (int i = rates.size() > 30 ? rates.size() - 31 : 0; i < rates.size(); i++) {
            r = rates.get(i).getRate();
            if (r.compareTo(rez[0]) < 0) {
                rez[0] = r;
            }
            if (r.compareTo(rez[1]) > 0) {
                rez[1] = r;
            }
        }
        return rez;
    }

//    public float Round(float Rval, int Rpl) {
//		float p = (float)Math.pow(10,Rpl);
//		Rval = Rval * p;
//		float tmp = Math.round(Rval);
//		return (float)tmp/p;
//    }

//    public static void main(String[] args) throws Exception {
//        Chart chart = new Chart(/*"DOW",*/ "Last 30 rates", 170, 90, "Line 1 text", "Line 2");
//        long now = System.currentTimeMillis();
//        long rate = 11450;
//        MarketRate mr = null;
//        Random random = new Random();
//        for (int i = 0; i < 100; i++) {
//            mr = new MarketRate();
//            mr.setRateTime(new Date(now));
//            mr.setRate(new BigDecimal(rate));
//            chart.addRate(mr);
//            now += 60000;
//            rate += random.nextInt(6) - 3;
//        }
//
//        FileOutputStream fos = new FileOutputStream("C:/TEMP/chart.png");
//        long st = System.currentTimeMillis();
//        byte[] ba = chart.getLast30ImageBytes();
//        System.out.println("Time to run: " + (System.currentTimeMillis() - st));
//        fos.write(ba);
//        fos.flush();
//        fos.close();
//    }
}