package il.co.etrader.distribution;

import java.sql.SQLException;
import java.util.ArrayList;

public class DistributionMaker {
	
	private ArrayList<Selector<Long>> selectors;
	private DistributionType type;
	
	/*
	 *  selectors - ordered by priority
	 */
	public DistributionMaker( ArrayList<Selector<Long>> selectors, DistributionType type) {
		this.selectors = selectors;
		this.type = type;
	}
	
	public Distribution getClearingProviderId(long userId) throws SQLException, SelectorException{
		for(int i=0;i<selectors.size();i++) {
			Long l = selectors.get(i).select();
			if(l != null) {
				return new Distribution(type, l, selectors.get(i).getId());
			}
		}
		throw new SelectorException("Empty Selection!");
	}
}
