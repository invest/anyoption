package il.co.etrader.distribution;

public class Distribution {
	DistributionType type;
	long providerId;
	int selectorId;
	
	public Distribution(DistributionType type, long providerId, int selectorId){
		this.type = type;
		this.providerId = providerId;
		this.selectorId = selectorId;
	}

	public DistributionType getType() {
		return type;
	}

	public void setType(DistributionType type) {
		this.type = type;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public int getSelectorId() {
		return selectorId;
	}

	public void setSelectorId(int selectorId) {
		this.selectorId = selectorId;
	}
	
	
}
