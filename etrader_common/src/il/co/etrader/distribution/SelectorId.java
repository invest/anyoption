package il.co.etrader.distribution;


public enum SelectorId {
	OldTransactions(-1),
	Assigned(0),
	NewProviderSelector(1),
	PercentageDistributionSelector(2),
	SingleProviderDepositSelector(3),
	HundredPercentSelector(4),
	MobileSupportSelector(5);
	
	private int value;    

	private SelectorId(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static boolean isException(long l){
		return l == MobileSupportSelector.value;
	}
}