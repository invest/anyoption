package il.co.etrader.distribution;

import il.co.etrader.bl_managers.ClearingManager;


/**
 * This selector is used only in mobile  
 * and is exception to the normal distribution
 */
public class MobileSupportSelector implements Selector<Long> {
	// this selector is just to keep the selector_id value
	private final int SELECTOR_ID = SelectorId.MobileSupportSelector.getValue();
	
	@Override
	public int getId() {
		return SELECTOR_ID;
	}

	@Override
	public Long select() {
		// currently only Deltapay is supported on mobile
		return ClearingManager.DELTAPAY_CHINA_PROVIDER_ID ;
	}

}
