//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import javax.faces.context.FacesContext;
//
///**
// * @author Eran
// *
// */
//public class UkashDeposit extends com.anyoption.common.bl_vos.UkashDeposit implements java.io.Serializable {
//
//	private static final long serialVersionUID = -7889277799801275914L;
//
//	/**
//	 * @return the voucherValue
//	 */
//	public String getVoucherValueTxt() {
//		return CommonUtil.displayAmount(getVoucherValue(), false, 0);
//	}
//
//	public String getConvertedAmountTxt(){
//		//check if there was a conversion
//		if (!CommonUtil.IsParameterEmptyOrNull(getUserCurrency()) && getConvertedAmount() != 0 && !CommonUtil.IsParameterEmptyOrNull(getVoucherCurrency())){
//			return CommonUtil.displayAmount(getConvertedAmount(), Long.valueOf(getUserCurrency()));
//		}
//		return "";
//	}
//
//	/**
//	 * Get conversion Msg rate: usercurrency = voucherCurrency for TRY currency
//	 * @return
//	 */
//	public String getConversionMsg(){
//		FacesContext context=FacesContext.getCurrentInstance();
//		UserBase user= (UserBase)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
//		long voucherCurrencyId = ApplicationDataBase.getCurrencyIdByCode(getVoucherCurrency());
//		long amount = CommonUtil.calcAmount(getVoucherValue());
//		long divider = Long.valueOf(getVoucherValue());
//		double transactionAmount = getConvertedAmount()/divider;
//		double voucher = amount/divider;
//		long userCurrencyId = user.getCurrencyId();
//		String userAmount = CommonUtil.displayAmount(transactionAmount, true, userCurrencyId);
//		String convertedAmount= CommonUtil.displayAmount(voucher, true, voucherCurrencyId);
//		return userAmount + " = " + convertedAmount;
//	}
//}
