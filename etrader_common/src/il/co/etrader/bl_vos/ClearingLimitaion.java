//package il.co.etrader.bl_vos;
//
//import java.io.Serializable;
//import java.util.HashMap;
//
//public class ClearingLimitaion implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	private long id;
//	private HashMap<Long, String> clearingProviderGroups;
//	private long clearingProviderIdGroup;
//	private String clearingProviderGroupName;
//	private long currencyId;
//	private String currencyName;
//	private long creditCardTypeId;
//	private String creditCardTypeName;
//	private long binId;
//	private long countryId;
//	private String countryName;
//	private boolean isActive;
//	private boolean tbiAnyoption;
//	private boolean wireCard;
//	private boolean inatec;
//	private boolean wireCard3D;
//	
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public long getClearingProviderIdGroup() {
//		return clearingProviderIdGroup;
//	}
//	public void setClearingProviderIdGroup(long clearingProviderIdGroup) {
//		this.clearingProviderIdGroup = clearingProviderIdGroup;
//	}
//	public long getCurrencyId() {
//		return currencyId;
//	}
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//	public long getCreditCardTypeId() {
//		return creditCardTypeId;
//	}
//	public void setCreditCardTypeId(long creditCardTypeId) {
//		this.creditCardTypeId = creditCardTypeId;
//	}
//	public long getBinId() {
//		return binId;
//	}
//	public void setBinId(long binId) {
//		this.binId = binId;
//	}
//	public long getCountryId() {
//		return countryId;
//	}
//	public void setCountryId(long countryId) {
//		this.countryId = countryId;
//	}
//	public boolean isActive() {
//		return isActive;
//	}
//	public void setActive(boolean isActive) {
//		this.isActive = isActive;
//	}
//	public String getClearingProviderGroupName() {
//		return clearingProviderGroupName;
//	}
//	public void setClearingProviderGroupName(String clearingProviderGroupName) {
//		this.clearingProviderGroupName = clearingProviderGroupName;
//	}
//	public String getCurrencyName() {
//		return currencyName;
//	}
//	public void setCurrencyName(String currencyName) {
//		this.currencyName = currencyName;
//	}
//	public String getCreditCardTypeName() {
//		return creditCardTypeName;
//	}
//	public void setCreditCardTypeName(String creditCardTypeName) {
//		this.creditCardTypeName = creditCardTypeName;
//	}
//	public String getCountryName() {
//		return countryName;
//	}
//	public void setCountryName(String countryName) {
//		this.countryName = countryName;
//	}
//	public HashMap<Long, String> getClearingProviderGroups() {
//		return clearingProviderGroups;
//	}
//	public void setClearingProviderGroups(
//			HashMap<Long, String> clearingProviderGroups) {
//		this.clearingProviderGroups = clearingProviderGroups;
//	}	
//	public void setClearingProviderGroupLimit(long id, String name) {
//		this.clearingProviderGroups.put(id, name);
//	}
//	
//	public boolean isTbiAnyoption() {
//		return tbiAnyoption;
//	}
//	public void setTbiAnyoption(boolean tbiAnyoption) {
//		this.tbiAnyoption = tbiAnyoption;
//	}
//	public boolean isWireCard() {
//		return wireCard;
//	}
//	public void setWireCard(boolean wireCard) {
//		this.wireCard = wireCard;
//	}
//	public boolean isInatec() {
//		return inatec;
//	}
//	public void setInatec(boolean inatec) {
//		this.inatec = inatec;
//	}
//	public boolean isWireCard3D() {
//		return wireCard3D;
//	}
//	public void setWireCard3D(boolean wireCard3D) {
//		this.wireCard3D = wireCard3D;
//	}
//	public boolean compareToMerge(ClearingLimitaion cl){
//		if(		this.getCreditCardTypeId() == cl.getCreditCardTypeId() &&
//				this.getBinId() ==	cl.getBinId() && 
//				this.getCountryId() == cl.getCountryId() &&
//				this.getCurrencyId() == cl.getCurrencyId()){
//			return true;
//		}
//		return false;
//	}
//}
