package il.co.etrader.bl_vos;

import com.anyoption.common.beans.IssueAction;

/**
 * The class extend Issue and add some parameters 
 * for issue report in control role.
 * 
 * @author eyalo
 *
 */
public class IssueReport extends IssueAction {

	private static final long serialVersionUID = 1L;
	private String issueSubject;
	private String issueStats;
	private String usersMobilePhone;
	private String usersLandLinePhone;
	private String usersEmail;
	private long contactId;
	private String contactsMobilePhone;
	private String contactLandLinePhone;
	private String contactEmail;
	private long userId;
	private String userName;
	
	/**
	 * @return the issueSubject
	 */
	public String getIssueSubject() {
		return issueSubject;
	}
	/**
	 * @param issueSubject the issueSubject to set
	 */
	public void setIssueSubject(String issueSubject) {
		this.issueSubject = issueSubject;
	}
	/**
	 * @return the issueStats
	 */
	public String getIssueStats() {
		return issueStats;
	}
	/**
	 * @param issueStats the issueStats to set
	 */
	public void setIssueStats(String issueStats) {
		this.issueStats = issueStats;
	}
	/**
	 * @return the usersMobilePhone
	 */
	public String getUsersMobilePhone() {
		return usersMobilePhone;
	}
	/**
	 * @param usersMobilePhone the usersMobilePhone to set
	 */
	public void setUsersMobilePhone(String usersMobilePhone) {
		this.usersMobilePhone = usersMobilePhone;
	}
	/**
	 * @return the usersLandLinePhone
	 */
	public String getUsersLandLinePhone() {
		return usersLandLinePhone;
	}
	/**
	 * @param usersLandLinePhone the usersLandLinePhone to set
	 */
	public void setUsersLandLinePhone(String usersLandLinePhone) {
		this.usersLandLinePhone = usersLandLinePhone;
	}
	/**
	 * @return the usersEmail
	 */
	public String getUsersEmail() {
		return usersEmail;
	}
	/**
	 * @param usersEmail the usersEmail to set
	 */
	public void setUsersEmail(String usersEmail) {
		this.usersEmail = usersEmail;
	}
	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the contactsMobilePhone
	 */
	public String getContactsMobilePhone() {
		return contactsMobilePhone;
	}
	/**
	 * @param contactsMobilePhone the contactsMobilePhone to set
	 */
	public void setContactsMobilePhone(String contactsMobilePhone) {
		this.contactsMobilePhone = contactsMobilePhone;
	}
	/**
	 * @return the contactLandLinePhone
	 */
	public String getContactLandLinePhone() {
		return contactLandLinePhone;
	}
	/**
	 * @param contactLandLinePhone the contactLandLinePhone to set
	 */
	public void setContactLandLinePhone(String contactLandLinePhone) {
		this.contactLandLinePhone = contactLandLinePhone;
	}
	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}
	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
