package il.co.etrader.bl_vos;

import java.util.Date;

import com.anyoption.common.beans.Opportunity;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class InvestmentLimit extends com.anyoption.common.bl_vos.InvestmentLimit {
	private static final long serialVersionUID = -5360691598370540901L;

	// Investment Limit Types


    public InvestmentLimit() {
    	currencyId=new Long(0);
		startDate = new Date();
		endDate = new Date();
		timeCreated = new Date();
		isActive = true;
    }

    public void setMinDouble(double m) {
    	minAmount=new Double(m*100).longValue();
    }
    public double getMinDouble() {
    	return minAmount/100;
    }
    public void setMaxDouble(double m) {
    	maxAmount=new Double(m*100).longValue();
    }
    public double getMaxDouble() {
    	return maxAmount/100;
    }

    /**
     * @return the currencyId
     */
    public Long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the max
     */
    public long getMaxAmount() {
        return maxAmount;
    }

    /**
     * @param max the max to set
     */
    public void setMaxAmount(long max) {
        this.maxAmount = max;
    }

    /**
     * @return the min
     */
    public long getMinAmount() {
        return minAmount;
    }

    /**
     * @param min the min to set
     */
    public void setMinAmount(long min) {
        this.minAmount = min;
    }

    public String getMinTxt() {
        return CommonUtil.displayAmount(minAmount,false, currencyId.intValue());
    }
    public String getMaxTxt() {
    	return CommonUtil.displayAmount(maxAmount,false, currencyId.intValue());
    }

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentLimit ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "invLimitTypeId = " + this.invLimitTypeId + TAB
	        + "currencyId = " + this.currencyId + TAB
	        + "min = " + this.minAmount + TAB
	        + "max = " + this.maxAmount + TAB
	        + "one touch min = " + this.oneTouchMin + TAB
	        + "currencyName = " + this.currencyName + TAB
	        + "userId = " + this.userId + TAB
	        + "marketId = " + this.marketId + TAB
	        + "startDate = " + this.startDate + TAB
	        + "endDate = " + this.endDate + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "scheduled = " + this.scheduled + TAB
	        + "isActive = " + this.isActive + TAB
	        + "period = " + this.period + TAB
	        + "userName = " + this.userName + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the one_touch_min
	 */
	public long getOneTouchMin() {
		return oneTouchMin;
	}

	/**
	 * @param one_touch_min the one_touch_min to set
	 */
	public void setOneTouchMin(long oneTouchMin) {
		this.oneTouchMin = oneTouchMin;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the scheduled
	 */
	public long getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the endDate Txt
	 */
	public String getEndDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(endDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @return the endDate Txt
	 */
	public String getStartDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(startDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @return the startDateHour
	 */
	public String getStartDateHour() {
		return startDateHour;
	}

	/**
	 * @param startDateHour the startDateHour to set
	 */
	public void setStartDateHour(String startDateHour) {
		this.startDateHour = startDateHour;
	}

	/**
	 * @return the startDateMin
	 */
	public String getStartDateMin() {
		return startDateMin;
	}

	/**
	 * @param startDateMin the startDateMin to set
	 */
	public void setStartDateMin(String startDateMin) {
		this.startDateMin = startDateMin;
	}

	/**
	 * @return the period
	 */
	public long getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(long period) {
		this.period = period;
	}

	/**
	 * Get scheduledTxt
	 * @return
	 */
    public String getScheduledTxt() {
    	if (Opportunity.TYPE_REGULAR == opportunityTypeId){
    		return InvestmentsManagerBase.getScheduledTxt(scheduled);
    	}else{
    		return ConstantsBase.EMPTY_STRING;
    	}
    }

    /**
	 * @return the marketName
	 */
	public String getMarketNameKey() {
		return marketNameKey;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketNameKey(String marketName) {
		this.marketNameKey = marketName;
	}


    /**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketNameKey;
	}

	/**
     * Get isActiveTxt
     * @return
     */
    public String getActiveTxt(){

    	if ( isActive ) {
    		return CommonUtil.getMessage("invlimits.active", null);
    	} else {
    		return CommonUtil.getMessage("invlimits.Inactive", null);
    	}
    }

	/**
	 * @return the invLimitTypeId
	 */
	public int getInvLimitTypeId() {
		return invLimitTypeId;
	}

	/**
	 * @param invLimitTypeId the invLimitTypeId to set
	 */
	public void setInvLimitTypeId(int invLimitTypeId) {
		this.invLimitTypeId = invLimitTypeId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the opportunityTypeId
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId the opportunityTypeId to set
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public boolean isRegularOpportunity(){
		return (opportunityTypeId == Opportunity.TYPE_REGULAR);
	}

	/**
	 * @return the maxAmountGroupId
	 */
	public long getMaxAmountGroupId() {
		return maxAmountGroupId;
	}

	/**
	 * @param maxAmountGroupId the maxAmountGroupId to set
	 */
	public void setMaxAmountGroupId(long maxAmountGroupId) {
		this.maxAmountGroupId = maxAmountGroupId;
	}

	/**
	 * @return the minAmountGroupId
	 */
	public long getMinAmountGroupId() {
		return minAmountGroupId;
	}

	/**
	 * @param minAmountGroupId the minAmountGroupId to set
	 */
	public void setMinAmountGroupId(long minAmountGroupId) {
		this.minAmountGroupId = minAmountGroupId;
	}

	public String getMinAmountGroupName(){
		return ApplicationDataBase.getInvLimitisGroupsMap().get(minAmountGroupId).getGroupNameDisplay();
	}

	public String getMaxAmountGroupName(){
		return ApplicationDataBase.getInvLimitisGroupsMap().get(maxAmountGroupId).getGroupNameDisplay();
	}

	/**
	 * @return the opportunityTypeName
	 */
	public String getOpportunityTypeName() {
		return opportunityTypeName;
	}

	/**
	 * @param opportunityTypeName the opportunityTypeName to set
	 */
	public void setOpportunityTypeName(String opportunityTypeName) {
		this.opportunityTypeName = opportunityTypeName;
	}

	public boolean isDefaultLimit(){
		return (invLimitTypeId == TYPE_DEFUALT);
	}
}