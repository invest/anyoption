//package il.co.etrader.bl_vos;
//
//import java.util.HashMap;
//
///**
// * Country vo class
// * @author Kobi
// *
// */
//public class Country implements java.io.Serializable {
//
//	private long id;
//	private String name;
//	private String a2;
//	private String a3;
//	private String phoneCode;
//	private String displayName;
//	private String supportPhone;
//	private String supportFax;
//	private String gmtOffset;
//	private boolean isSmsAvailable;
//	private boolean isHaveUkashSite;
//	private boolean isAlphaNumericSender;
//    private boolean isNetreferRegBlock;
//	private HashMap<Long, PaymentMethodCountry> paymentsMethodCountry;
//
//
//	/**
//	 * @return the isSmsAvailable
//	 */
//	public boolean isSmsAvailable() {
//		return isSmsAvailable;
//	}
//
//	/**
//	 * @param isSmsAvailable the isSmsAvailable to set
//	 */
//	public void setSmsAvailable(boolean isSmsAvailable) {
//		this.isSmsAvailable = isSmsAvailable;
//	}
//
//	/**
//	 * @return the a2
//	 */
//	public String getA2() {
//		return a2;
//	}
//
//	/**
//	 * @param a2 the a2 to set
//	 */
//	public void setA2(String a2) {
//		this.a2 = a2;
//	}
//
//	/**
//	 * @return the a3
//	 */
//	public String getA3() {
//		return a3;
//	}
//
//	/**
//	 * @param a3 the a3 to set
//	 */
//	public void setA3(String a3) {
//		this.a3 = a3;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @return the phoneCode
//	 */
//	public String getPhoneCode() {
//		return phoneCode;
//	}
//
//	/**
//	 * @param phoneCode the phoneCode to set
//	 */
//	public void setPhoneCode(String phoneCode) {
//		this.phoneCode = phoneCode;
//	}
//
//	/**
//	 * @return the supportFax
//	 */
//	public String getSupportFax() {
//		return supportFax;
//	}
//
//	/**
//	 * @param supportFax the supportFax to set
//	 */
//	public void setSupportFax(String supportFax) {
//		this.supportFax = supportFax;
//	}
//
//	/**
//	 * @return the supportPhone
//	 */
//	public String getSupportPhone() {
//		return supportPhone;
//	}
//
//	/**
//	 * @param supportPhone the supportPhone to set
//	 */
//	public void setSupportPhone(String supportPhone) {
//		this.supportPhone = supportPhone;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the gmtOffset
//	 */
//	public String getGmtOffset() {
//		return gmtOffset;
//	}
//
//	/**
//	 * @param gmtOffset the gmtOffset to set
//	 */
//	public void setGmtOffset(String gmtOffset) {
//		this.gmtOffset = gmtOffset;
//	}
//
//	/**
//	 * @return the payments
//	 */
//	public HashMap<Long, PaymentMethodCountry> getPaymentsMethodCountry() {
//		return paymentsMethodCountry;
//	}
//
//	/**
//	 * @param payments the payments to set
//	 */
//	public void setPaymentsMethodCountry(HashMap<Long, PaymentMethodCountry> payments) {
//		this.paymentsMethodCountry = payments;
//	}
//
//	/**
//	 * @return the isHaveUkashSite
//	 */
//	public boolean isHaveUkashSite() {
//		return isHaveUkashSite;
//	}
//
//	/**
//	 * @param isHaveUkashSite the isHaveUkashSite to set
//	 */
//	public void setHaveUkashSite(boolean isHaveUkashSite) {
//		this.isHaveUkashSite = isHaveUkashSite;
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "Country ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "a2 = " + this.a2 + TAB
//	        + "a3 = " + this.a3 + TAB
//	        + "phoneCode = " + this.phoneCode + TAB
//	        + "gmtOffset = " + this.gmtOffset + TAB
//	        + "isSmsAvailable = " + this.isSmsAvailable + TAB
//	        + "isHaveUkashSite = " + this.isHaveUkashSite + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	public boolean isAlphaNumericSender() {
//		return isAlphaNumericSender;
//	}
//
//	public void setAlphaNumericSender(boolean isAlphaNumericSender) {
//		this.isAlphaNumericSender = isAlphaNumericSender;
//	}
//
//    /**
//     * @return the isNetreferRegBlock
//     */
//    public boolean isNetreferRegBlock() {
//        return isNetreferRegBlock;
//    }
//
//    /**
//     * @param isNetreferRegBlock the isNetreferRegBlock to set
//     */
//    public void setNetreferRegBlock(boolean isNetreferRegBlock) {
//        this.isNetreferRegBlock = isNetreferRegBlock;
//    }
//
//}
