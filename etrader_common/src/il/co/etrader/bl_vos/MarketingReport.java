package il.co.etrader.bl_vos;
import java.io.Serializable;
import java.util.Date;

public class MarketingReport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date dates;
	private String campaignManager;
	private long combId;
	private String campaignName;
	private String sourceName;
	private String medium;
	private String content;
	private long marketSizeHorizontal;
	private long marketSizeVertical;
	private String location;
	private String landingPageName;
	private long skinId;
	private long shortReg;
	private long registeredUsersNum;
	private long ftd;
	private long rfd;
	private long firstRemDepNum;
	private long houseWin;
	private long failedUsers;
	
	private String  marketType;
	private long clicksNum;	
	private long firstRegDepositorsNum;
	private long firstDepositorsNum;
	private long sumDeposits;
	private long turnOver;
	private long failMinUsers;
	private long failUsers;
	private long headCount;
	private String dynamicParam;
	private String date;

	// Job columns
	private long firstDepNum;
	private long firstDepSalesNum;
	private long firstDepIndNum;
	private long firstDepAmount;
	private long firstDepSalesAmount;
	private long firstDepIndAmount;
	private long firstDepAmountAvg;
	

	private long firstRegDepNum;
	private long firstRegDepIndNum;
	private long firstRegDepSalesNum;
	private long firstRegDepAmount;
	private long firstRegDepSalesAmount;
	private long firstRegDepIndAmount;
	private long firstRegDepAmountAvg;

	private long depositsNum;
	private long depsitsSum;

	private long houseWinReg;

	private String httpReferer;
	private String dsq;
	
	
	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}
	/**
	 * @param sourceName the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}
	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	/**
	 * @return the clicksNum
	 */
	public long getClicksNum() {
		return clicksNum;
	}
	/**
	 * @param clicksNum the clicksNum to set
	 */
	public void setClicksNum(long clicksNum) {
		this.clicksNum = clicksNum;
	}
	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}
	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the failMinUsers
	 */
	public long getFailMinUsers() {
		return failMinUsers;
	}
	/**
	 * @param failMinUsers the failMinUsers to set
	 */
	public void setFailMinUsers(long failMinUsers) {
		this.failMinUsers = failMinUsers;
	}
	/**
	 * @return the failUsers
	 */
	public long getFailUsers() {
		return failUsers;
	}
	/**
	 * @param failUsers the failUsers to set
	 */
	public void setFailUsers(long failUsers) {
		this.failUsers = failUsers;
	}
	/**
	 * @return the firstDepositorsNum
	 */
	public long getFirstDepositorsNum() {
		return firstDepositorsNum;
	}
	/**
	 * @param firstDepositorsNum the firstDepositorsNum to set
	 */
	public void setFirstDepositorsNum(long firstDepositorsNum) {
		this.firstDepositorsNum = firstDepositorsNum;
	}
	/**
	 * @return the firstRegDepositorsNum
	 */
	public long getFirstRegDepositorsNum() {
		return firstRegDepositorsNum;
	}
	/**
	 * @param firstRegDepositorsNum the firstRegDepositorsNum to set
	 */
	public void setFirstRegDepositorsNum(long firstRegDepositorsNum) {
		this.firstRegDepositorsNum = firstRegDepositorsNum;
	}
	/**
	 * @return the houseWin
	 */
	public long getHouseWin() {
		return houseWin;
	}
	/**
	 * @param houseWin the houseWin to set
	 */
	public void setHouseWin(long houseWin) {
		this.houseWin = houseWin;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the marketSizeHorizontal
	 */
	public long getMarketSizeHorizontal() {
		return marketSizeHorizontal;
	}
	/**
	 * @param marketSizeHorizontal the marketSizeHorizontal to set
	 */
	public void setMarketSizeHorizontal(long marketSizeHorizontal) {
		this.marketSizeHorizontal = marketSizeHorizontal;
	}
	/**
	 * @return the marketSizeVertical
	 */
	public long getMarketSizeVertical() {
		return marketSizeVertical;
	}
	/**
	 * @param marketSizeVertical the marketSizeVertical to set
	 */
	public void setMarketSizeVertical(long marketSizeVertical) {
		this.marketSizeVertical = marketSizeVertical;
	}
	/**
	 * @return the marketType
	 */
	public String getMarketType() {
		return marketType;
	}
	/**
	 * @param marketType the marketType to set
	 */
	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	/**
	 * @return the medium
	 */
	public String getMedium() {
		return medium;
	}
	/**
	 * @param medium the medium to set
	 */
	public void setMedium(String medium) {
		this.medium = medium;
	}
	/**
	 * @return the registeredUsersNum
	 */
	public long getRegisteredUsersNum() {
		return registeredUsersNum;
	}
	/**
	 * @param registeredUsersNum the registeredUsersNum to set
	 */
	public void setRegisteredUsersNum(long registeredUsersNum) {
		this.registeredUsersNum = registeredUsersNum;
	}
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the sumDeposits
	 */
	public long getSumDeposits() {
		return sumDeposits;
	}
	/**
	 * @param sumDeposits the sumDeposits to set
	 */
	public void setSumDeposits(long sumDeposits) {
		this.sumDeposits = sumDeposits;
	}
	/**
	 * @return the turnOver
	 */
	public long getTurnOver() {
		return turnOver;
	}
	/**
	 * @param turnOver the turnOver to set
	 */
	public void setTurnOver(long turnOver) {
		this.turnOver = turnOver;
	}
	/**
	 * @return the landingPageName
	 */
	public String getLandingPageName() {
		return landingPageName;
	}
	/**
	 * @param landingPageName the landingPageName to set
	 */
	public void setLandingPageName(String landingPageName) {
		this.landingPageName = landingPageName;
	}
	/**
	 * @return the headCount
	 */
	public long getHeadCount() {
		return headCount;
	}
	/**
	 * @param headCount the headCount to set
	 */
	public void setHeadCount(long headCount) {
		this.headCount = headCount;
	}
	/**
	 * @return the dynamicParam
	 */
	public String getDynamicParam() {
		return dynamicParam;
	}
	/**
	 * @param dynamicParam the dynamicParam to set
	 */
	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the depositsNum
	 */
	public long getDepositsNum() {
		return depositsNum;
	}
	/**
	 * @param depositsNum the depositsNum to set
	 */
	public void setDepositsNum(long depositsNum) {
		this.depositsNum = depositsNum;
	}
	/**
	 * @return the depsitsSum
	 */
	public long getDepsitsSum() {
		return depsitsSum;
	}
	/**
	 * @param depsitsSum the depsitsSum to set
	 */
	public void setDepsitsSum(long depsitsSum) {
		this.depsitsSum = depsitsSum;
	}
	/**
	 * @return the firstDepAmount
	 */
	public long getFirstDepAmount() {
		return firstDepAmount;
	}
	/**
	 * @param firstDepAmount the firstDepAmount to set
	 */
	public void setFirstDepAmount(long firstDepAmount) {
		this.firstDepAmount = firstDepAmount;
	}
	/**
	 * @return the firstDepAmountAvg
	 */
	public long getFirstDepAmountAvg() {
		return firstDepAmountAvg;
	}
	/**
	 * @param firstDepAmountAvg the firstDepAmountAvg to set
	 */
	public void setFirstDepAmountAvg(long firstDepAmountAvg) {
		this.firstDepAmountAvg = firstDepAmountAvg;
	}
	/**
	 * @return the firstDepIndAmount
	 */
	public long getFirstDepIndAmount() {
		return firstDepIndAmount;
	}
	/**
	 * @param firstDepIndAmount the firstDepIndAmount to set
	 */
	public void setFirstDepIndAmount(long firstDepIndAmount) {
		this.firstDepIndAmount = firstDepIndAmount;
	}
	/**
	 * @return the firstDepIndNum
	 */
	public long getFirstDepIndNum() {
		return firstDepIndNum;
	}
	/**
	 * @param firstDepIndNum the firstDepIndNum to set
	 */
	public void setFirstDepIndNum(long firstDepIndNum) {
		this.firstDepIndNum = firstDepIndNum;
	}
	/**
	 * @return the firstDepNum
	 */
	public long getFirstDepNum() {
		return firstDepNum;
	}
	/**
	 * @param firstDepNum the firstDepNum to set
	 */
	public void setFirstDepNum(long firstDepNum) {
		this.firstDepNum = firstDepNum;
	}
	/**
	 * @return the firstDepSalesAmount
	 */
	public long getFirstDepSalesAmount() {
		return firstDepSalesAmount;
	}
	/**
	 * @param firstDepSalesAmount the firstDepSalesAmount to set
	 */
	public void setFirstDepSalesAmount(long firstDepSalesAmount) {
		this.firstDepSalesAmount = firstDepSalesAmount;
	}
	/**
	 * @return the firstDepSalesNum
	 */
	public long getFirstDepSalesNum() {
		return firstDepSalesNum;
	}
	/**
	 * @param firstDepSalesNum the firstDepSalesNum to set
	 */
	public void setFirstDepSalesNum(long firstDepSalesNum) {
		this.firstDepSalesNum = firstDepSalesNum;
	}
	/**
	 * @return the firstRegDepAmount
	 */
	public long getFirstRegDepAmount() {
		return firstRegDepAmount;
	}
	/**
	 * @param firstRegDepAmount the firstRegDepAmount to set
	 */
	public void setFirstRegDepAmount(long firstRegDepAmount) {
		this.firstRegDepAmount = firstRegDepAmount;
	}
	/**
	 * @return the firstRegDepAmountAvg
	 */
	public long getFirstRegDepAmountAvg() {
		return firstRegDepAmountAvg;
	}
	/**
	 * @param firstRegDepAmountAvg the firstRegDepAmountAvg to set
	 */
	public void setFirstRegDepAmountAvg(long firstRegDepAmountAvg) {
		this.firstRegDepAmountAvg = firstRegDepAmountAvg;
	}
	/**
	 * @return the firstRegDepIndAmount
	 */
	public long getFirstRegDepIndAmount() {
		return firstRegDepIndAmount;
	}
	/**
	 * @param firstRegDepIndAmount the firstRegDepIndAmount to set
	 */
	public void setFirstRegDepIndAmount(long firstRegDepIndAmount) {
		this.firstRegDepIndAmount = firstRegDepIndAmount;
	}
	/**
	 * @return the firstRegDepIndNum
	 */
	public long getFirstRegDepIndNum() {
		return firstRegDepIndNum;
	}
	/**
	 * @param firstRegDepIndNum the firstRegDepIndNum to set
	 */
	public void setFirstRegDepIndNum(long firstRegDepIndNum) {
		this.firstRegDepIndNum = firstRegDepIndNum;
	}
	/**
	 * @return the firstRegDepNum
	 */
	public long getFirstRegDepNum() {
		return firstRegDepNum;
	}
	/**
	 * @param firstRegDepNum the firstRegDepNum to set
	 */
	public void setFirstRegDepNum(long firstRegDepNum) {
		this.firstRegDepNum = firstRegDepNum;
	}
	/**
	 * @return the firstRegDepSalesAmount
	 */
	public long getFirstRegDepSalesAmount() {
		return firstRegDepSalesAmount;
	}
	/**
	 * @param firstRegDepSalesAmount the firstRegDepSalesAmount to set
	 */
	public void setFirstRegDepSalesAmount(long firstRegDepSalesAmount) {
		this.firstRegDepSalesAmount = firstRegDepSalesAmount;
	}
	/**
	 * @return the firstRegDepSalesNum
	 */
	public long getFirstRegDepSalesNum() {
		return firstRegDepSalesNum;
	}
	/**
	 * @param firstRegDepSalesNum the firstRegDepSalesNum to set
	 */
	public void setFirstRegDepSalesNum(long firstRegDepSalesNum) {
		this.firstRegDepSalesNum = firstRegDepSalesNum;
	}
	/**
	 * @return the houseWinReg
	 */
	public long getHouseWinReg() {
		return houseWinReg;
	}
	/**
	 * @param houseWinReg the houseWinReg to set
	 */
	public void setHouseWinReg(long houseWinReg) {
		this.houseWinReg = houseWinReg;
	}
	public long getFirstRemDepNum() {
		return firstRemDepNum;
	}
	public void setFirstRemDepNum(long firstRemDepNum) {
		this.firstRemDepNum = firstRemDepNum;
	}

	/**
	 * @return the shortReg
	 */
	public long getShortReg() {
		return shortReg;
	}
	/**
	 * @param shortReg the shortReg to set
	 */
	public void setShortReg(long shortReg) {
		this.shortReg = shortReg;
	}
	/**
	 * @return the ftd
	 */
	public long getFtd() {
		return ftd;
	}
	/**
	 * @param ftd the ftd to set
	 */
	public void setFtd(long ftd) {
		this.ftd = ftd;
	}
	/**
	 * @return the campaignManager
	 */
	public String getCampaignManager() {
		return campaignManager;
	}
	/**
	 * @param campaignManager the campaignManager to set
	 */
	public void setCampaignManager(String campaignManager) {
		this.campaignManager = campaignManager;
	}
	
	/**
	 * @return httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}
	
	/**
	 * @param httpReferer
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}
	public String getDsq() {
		return dsq;
	}
	public void setDsq(String dsq) {
		this.dsq = dsq;
	}
    /**
     * @return the rfd
     */
    public long getRfd() {
        return rfd;
    }
    /**
     * @param rfd the rfd to set
     */
    public void setRfd(long rfd) {
        this.rfd = rfd;
    }
    /**
     * @return the dates
     */
    public Date getDates() {
        return dates;
    }
    /**
     * @param dates the dates to set
     */
    public void setDates(Date dates) {
        this.dates = dates;
    }
    /**
     * @return the failedUsers
     */
    public long getFailedUsers() {
        return failedUsers;
    }
    /**
     * @param failedUsers the failedUsers to set
     */
    public void setFailedUsers(long failedUsers) {
        this.failedUsers = failedUsers;
    }
}
