//package il.co.etrader.bl_vos;
//
//import java.util.Date;
//
//public class MarketingTracking implements java.io.Serializable {
//	
//
//	private static final long serialVersionUID = 1657430793485404851L;
//
//	private long id;
//	private String mId;
//	private long combinationId;
//	private Date timeStatic;
//	private String httpReferer;
//	private String dyanmicParameter;
//	
//	private long combinationIdDynamic;
//	private Date timeDynamic;
//	private String httpRefererDynamic;
//	private String dyanmicParameterDynamic;
//	private long contactId;
//	private long userId;
//	private long marketingTrackingActivityId;
//	
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * @return the combinationId
//	 */
//	public long getCombinationId() {
//		return combinationId;
//	}
//	/**
//	 * @param combinationId the combinationId to set
//	 */
//	public void setCombinationId(long combinationId) {
//		this.combinationId = combinationId;
//	}
//	/**
//	 * @return the timeStatic
//	 */
//	public Date getTimeStatic() {
//		return timeStatic;
//	}
//	/**
//	 * @param timeStatic the timeStatic to set
//	 */
//	public void setTimeStatic(Date timeStatic) {
//		this.timeStatic = timeStatic;
//	}
//	/**
//	 * @return the httpReferer
//	 */
//	public String getHttpReferer() {
//		return httpReferer;
//	}
//	/**
//	 * @param httpReferer the httpReferer to set
//	 */
//	public void setHttpReferer(String httpReferer) {
//		this.httpReferer = httpReferer;
//	}
//	/**
//	 * @return the timeDynamic
//	 */
//	public Date getTimeDynamic() {
//		return timeDynamic;
//	}
//	/**
//	 * @param timeDynamic the timeDynamic to set
//	 */
//	public void setTimeDynamic(Date timeDynamic) {
//		this.timeDynamic = timeDynamic;
//	}
//	/**
//	 * @return the dyanmicParameter
//	 */
//	public String getDyanmicParameter() {
//		return dyanmicParameter;
//	}
//	/**
//	 * @param dyanmicParameter the dyanmicParameter to set
//	 */
//	public void setDyanmicParameter(String dyanmicParameter) {
//		this.dyanmicParameter = dyanmicParameter;
//	}
//	/**
//	 * @return the contactId
//	 */
//	public long getContactId() {
//		return contactId;
//	}
//	/**
//	 * @param contactId the contactId to set
//	 */
//	public void setContactId(long contactId) {
//		this.contactId = contactId;
//	}
//	/**
//	 * @return the userId
//	 */
//	public long getUserId() {
//		return userId;
//	}
//	/**
//	 * @param userId the userId to set
//	 */
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//	/**
//	 * @return the marketingTrackingActivityId
//	 */
//	public long getMarketingTrackingActivityId() {
//		return marketingTrackingActivityId;
//	}
//	/**
//	 * @param marketingTrackingActivityId the marketingTrackingActivityId to set
//	 */
//	public void setMarketingTrackingActivityId(long marketingTrackingActivityId) {
//		this.marketingTrackingActivityId = marketingTrackingActivityId;
//	}
//	/**
//	 * @return the mId
//	 */
//	public String getmId() {
//		return mId;
//	}
//	/**
//	 * @param mId the mId to set
//	 */
//	public void setmId(String mId) {
//		this.mId = mId;
//	}
//	/**
//	 * @return the combinationIdDynamic
//	 */
//	public long getCombinationIdDynamic() {
//		return combinationIdDynamic;
//	}
//	/**
//	 * @param combinationIdDynamic the combinationIdDynamic to set
//	 */
//	public void setCombinationIdDynamic(long combinationIdDynamic) {
//		this.combinationIdDynamic = combinationIdDynamic;
//	}
//	/**
//	 * @return the httpRefererDynamic
//	 */
//	public String getHttpRefererDynamic() {
//		return httpRefererDynamic;
//	}
//	/**
//	 * @param httpRefererDynamic the httpRefererDynamic to set
//	 */
//	public void setHttpRefererDynamic(String httpRefererDynamic) {
//		this.httpRefererDynamic = httpRefererDynamic;
//	}
//	/**
//	 * @return the dyanmicParameterDynamic
//	 */
//	public String getDyanmicParameterDynamic() {
//		return dyanmicParameterDynamic;
//	}
//	/**
//	 * @param dyanmicParameterDynamic the dyanmicParameterDynamic to set
//	 */
//	public void setDyanmicParameterDynamic(String dyanmicParameterDynamic) {
//		this.dyanmicParameterDynamic = dyanmicParameterDynamic;
//	}
//
//}
//
//
