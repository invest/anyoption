//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.faces.application.Application;
//
///**
// * TierSkin class
// *
// * @author Kobi.
// */
//public class TierSkin implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    protected long id;
//    protected long skinId;
//    protected long tierId;
//    protected long tierLevel;
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	/**
//	 * @return the tierId
//	 */
//	public long getTierId() {
//		return tierId;
//	}
//
//	/**
//	 * @param tierId the tierId to set
//	 */
//	public void setTierId(long tierId) {
//		this.tierId = tierId;
//	}
//
//	/**
//	 * @return the tierLevel
//	 */
//	public long getTierLevel() {
//		return tierLevel;
//	}
//
//	/**
//	 * @param tierLevel the tierLevel to set
//	 */
//	public void setTierLevel(long tierLevel) {
//		this.tierLevel = tierLevel;
//	}
//
//	/**
//	 * toString implementation.
//	 */
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "TierSkin:" + ls +
//            "id: " + id + ls +
//            "skinId: " + skinId + ls +
//            "tierId: " + tierId + ls +
//            "tierLevel: " + tierLevel + ls;
//    }
//
//}