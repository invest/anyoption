package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.managers.CurrenciesManagerBase;


public class CustomerReportProfitData implements java.io.Serializable{

	private static final long serialVersionUID = 7673507475657826519L;
	private long reportId;
	private long reportType;
	private long userId;
	private long lost;
	private long profited;

	Currency currency = CurrenciesManagerBase.getCurrency(Currency.CURRENCY_ILS_ID);

	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public long getReportType() {
		return reportType;
	}
	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getLost() {
		return lost;
	}
	public void setLost(long lost) {
		this.lost = lost;
	}
	public long getProfited() {
		return profited;
	}
	public void setProfited(long profited) {
		this.profited = profited;
	}
	
	public String getProfitedTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(profited, currency);
	}
	
	public String getLostTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(lost, currency);
	}
}
