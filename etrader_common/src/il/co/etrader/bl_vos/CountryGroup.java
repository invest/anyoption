package il.co.etrader.bl_vos;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class CountryGroup implements Serializable {
	private static final long serialVersionUID = -8645338580056100391L;
	
	private long id;
	private long countryId;
	private long countriesGroupTypeId;
	private long countriesGroupTierId;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}
	
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
	/**
	 * @return the countriesGroupTypeId
	 */
	public long getCountriesGroupTypeId() {
		return countriesGroupTypeId;
	}
	
	/**
	 * @param countriesGroupTypeId the countriesGroupTypeId to set
	 */
	public void setCountriesGroupTypeId(long countriesGroupTypeId) {
		this.countriesGroupTypeId = countriesGroupTypeId;
	}
	
	/**
	 * @return the countriesGroupTierId
	 */
	public long getCountriesGroupTierId() {
		return countriesGroupTierId;
	}
	
	/**
	 * @param countriesGroupTierId the countriesGroupTierId to set
	 */
	public void setCountriesGroupTierId(long countriesGroupTierId) {
		this.countriesGroupTierId = countriesGroupTierId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountryGroup [id=" + id + ", countryId=" + countryId
				+ ", countriesGroupTypeId=" + countriesGroupTypeId
				+ ", countriesGroupTierId=" + countriesGroupTierId + "]";
	}
	
}
