package il.co.etrader.bl_vos;

public class RankStatus {
	int rank;
	int countComma;
	int countSleeper;
	int countActive;
	private boolean isCheckedActive;
	private boolean isCheckedSleeper;
	private boolean isCheckedComma;
	
	public RankStatus(){
		
	}
	
	public RankStatus(int rank, int count_comma, int count_sleeper, int count_active) {
		this.rank = rank;
		this.countActive = count_active;
		this.countComma = count_comma;
		this.countSleeper = count_sleeper;
		this.isCheckedActive = false;
		this.isCheckedSleeper = false;
		this.isCheckedComma = false;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getCountComma() {
		return countComma;
	}
	public void setCountComma(int count_comma) {
		this.countComma = count_comma;
	}
	public int getCountSleeper() {
		return countSleeper;
	}
	public void setCountSleeper(int count_sleeper) {
		this.countSleeper = count_sleeper;
	}
	public int getCountActive() {
		return countActive;
	}
	public void setCountActive(int count_active) {
		this.countActive = count_active;
	}

	/**
	 * @return the isCheckedComma
	 */
	public boolean isCheckedComma() {
		return isCheckedComma;
	}

	/**
	 * @param isCheckedComma the isCheckedComma to set
	 */
	public void setCheckedComma(boolean isCheckedComma) {
		this.isCheckedComma = isCheckedComma;
	}

	/**
	 * @return the isCheckedSleeper
	 */
	public boolean isCheckedSleeper() {
		return isCheckedSleeper;
	}

	/**
	 * @param isCheckedSleeper the isCheckedSleeper to set
	 */
	public void setCheckedSleeper(boolean isCheckedSleeper) {
		this.isCheckedSleeper = isCheckedSleeper;
	}

	/**
	 * @return the isCheckedActive
	 */
	public boolean isCheckedActive() {
		return isCheckedActive;
	}

	/**
	 * @param isCheckedActive the isCheckedActive to set
	 */
	public void setCheckedActive(boolean isCheckedActive) {
		this.isCheckedActive = isCheckedActive;
	}
	
	
}
