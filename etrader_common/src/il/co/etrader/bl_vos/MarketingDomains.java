package il.co.etrader.bl_vos;

import java.io.Serializable;

public class MarketingDomains implements Serializable {
	private static final long serialVersionUID = 1L;

	protected long id;
	protected long marketingUrlSourceTypeId;
	protected long marketingLandingPageTypeId;
	protected long staticLandingPagePathId;
	protected long skinId;
	protected String domain;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the marketingUrlSourceTypeId
	 */
	public long getMarketingUrlSourceTypeId() {
		return marketingUrlSourceTypeId;
	}
	
	/**
	 * @param marketingUrlSourceTypeId the marketingUrlSourceTypeId to set
	 */
	public void setMarketingUrlSourceTypeId(long marketingUrlSourceTypeId) {
		this.marketingUrlSourceTypeId = marketingUrlSourceTypeId;
	}
	
	/**
	 * @return the marketingLandingPageTypeId
	 */
	public long getMarketingLandingPageTypeId() {
		return marketingLandingPageTypeId;
	}
	
	/**
	 * @param marketingLandingPageTypeId the marketingLandingPageTypeId to set
	 */
	public void setMarketingLandingPageTypeId(long marketingLandingPageTypeId) {
		this.marketingLandingPageTypeId = marketingLandingPageTypeId;
	}
	
	/**
	 * @return the staticLandingPagePathId
	 */
	public long getStaticLandingPagePathId() {
		return staticLandingPagePathId;
	}
	
	/**
	 * @param staticLandingPagePathId the staticLandingPagePathId to set
	 */
	public void setStaticLandingPagePathId(long staticLandingPagePathId) {
		this.staticLandingPagePathId = staticLandingPagePathId;
	}
	
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}
	
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}
	
	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}
}
