package il.co.etrader.bl_vos;

import java.io.Serializable;

/**
 * Marketing affilate class
 *
 * @author Kobi.
 */
public class MarketingSubAffiliate extends MarketingAffilate implements Serializable {
   private static final long serialVersionUID = 1L;
   protected long affiliateId;
   protected String affiliateName;
   protected MarketingSubAffiliatePixels subAffiliatePixels;  // affiliate pixels

/**
 * @return the affiliateId
 */
public long getAffiliateId() {
	return affiliateId;
}

/**
 * @param affiliateId the affiliateId to set
 */
public void setAffiliateId(long affiliateId) {
	this.affiliateId = affiliateId;
}

/**
 * @return the subAffiliatePixels
 */
public MarketingSubAffiliatePixels getSubAffiliatePixels() {
	return subAffiliatePixels;
}

/**
 * @param subAffiliatePixels the subAffiliatePixels to set
 */
public void setSubAffiliatePixels(MarketingSubAffiliatePixels subAffiliatePixels) {
	this.subAffiliatePixels = subAffiliatePixels;
}

/**
 * @return the affiliateName
 */
public String getAffiliateName() {
	return affiliateName;
}
/**
 * @param affiliateName the affiliateName to set
 */
public void setAffiliateName(String affiliateName) {
	this.affiliateName = affiliateName;
}

/**
 * toString implementation.
 */
public String toString() {
    String ls = System.getProperty("line.separator");
    return ls + "Marketing Sub Affiliate:" + ls +
        "Id: " + id + ls +
        "Name: " + name + ls +
        "WriterId: " + writerId + ls +
        "Key: " + key + ls +
    	"AffiliateId: " + affiliateId + ls +
    	"Affiliate Name: " + affiliateName + ls;
}

}