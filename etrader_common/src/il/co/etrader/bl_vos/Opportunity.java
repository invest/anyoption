//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.bl_managers.InvestmentsManagerBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.text.DecimalFormat;
//import java.text.DecimalFormatSymbols;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Locale;
//import java.util.Map;
//import java.util.TimeZone;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.component.UIComponent;
//import javax.faces.context.FacesContext;
//import javax.faces.validator.ValidatorException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.BinaryZeroOneHundred;
//import com.anyoption.common.beans.Market;
//import com.anyoption.common.beans.OpportunitySkinGroupMap;
//import com.anyoption.common.beans.base.Skin;
//import com.anyoption.common.enums.SkinGroup;
//
//public class Opportunity implements java.io.Serializable {
//	private static final long serialVersionUID = 1L;
//
//	private static final Logger log = Logger.getLogger(Opportunity.class);
//
//	public static final long TYPE_REGULAR = 1;
//	public static final long TYPE_ONE_TOUCH = 2;
//	public static final long TYPE_OPTION_PLUS = 3;
//	public static final long TYPE_PRODUCT_BINARY_0100 = 4;
//	public static final long TYPE_BINARY_0_100_ABOVE = 4;
//	public static final long TYPE_BINARY_0_100_BELOW = 5;
//	public static final String TYPE_BINARY_0_100_ABOVE_S = "above";
//	public static final String TYPE_BINARY_0_100_BELOW_S = "below";
//
//	public static final String TYPE_REGULAR_STAING = "1";
//	public static final String TYPE_ONE_TOUCH_STAING = "2";
//	public static final String TYPE_OPTION_PLUS_STAING = "3";
//	public static final String TYPE_BINARY_0_100_STAING = "4,5";
//
//	public static final int STATE_CREATED = 1;
//	public static final int STATE_OPENED = 2;
//	public static final int STATE_LAST_10_MIN = 3;
//	public static final int STATE_CLOSING_1_MIN = 4;
//	public static final int STATE_CLOSING = 5;
//	public static final int STATE_CLOSED = 6;
//	public static final int STATE_DONE = 7;
//	public static final int STATE_PAUSED = 8;
//	/**
//	 * This is not really an opportunity state. It is more display state. You
//	 * will not find opportunity in that state. This code is sent as state only
//	 * to LSs so the trading pages display the market as suspended.
//	 */
//	public static final int STATE_SUSPENDED = 9;
//	/**
//	 * This state is only for long term opps after they should be gone (after
//	 * day last time invest) before market close for the day.
//	 */
//	public static final int STATE_WAITING_TO_PAUSE = 10;
//	/**
//	 * This state is only for market id 436 = maof market its mean this market
//	 * is waiting for expiery level. we will be in this market 15 min after the
//	 * market is waiting for expiery level!
//	 */
//	public static final int STATE_HIDDEN_WAITING_TO_EXPIRY = 11;
//	/**
//	 * Not a real state. Display state for Binary 0-100 updating prices (auto halt).
//	 */
//	public static final int STATE_BINARY0100_UPDATING_PRICES = 12;
//
//	public static final int SCHEDULED_HOURLY = 1;
//	public static final int SCHEDULED_DAYLY = 2;
//	public static final int SCHEDULED_WEEKLY = 3;
//	public static final int SCHEDULED_MONTHLY = 4;
//
//	public static final int PUBLISHED_YES = 1;
//	public static final int PUBLISHED_NO = 0;
//
//	public static final int ISRAEL_EXCHANGE	   = 1;
//	public static final int USD_ILS_EXCHANGE   = 9;
//    public static final int TOKYO_EXCHANGE     = 4;
//    public static final int HONG_KONG_EXCHANGE = 7;
//    public static final int CHINA_EXCHANGE     = 25;
//    public static final int TURKISH_EXCHANGE   = 24;
//    public static final int JAKARTA_EXCHANGE   = 30;
//    public static final int DUBAI_EXCHANGE     = 31;
//    public static final int SINGAPORE_EXCHANGE = 39;
//
//	public static final String TURKISH_BREAK_START = "12:00"; // 12:00 in turkish time zone
//	public static final String TURKISH_BREAK_END = "14:30"; // 14:30 in turkish time zone
//
//    public static final String TOKYO_BREAK_START = "11:00"; // 11:00 in TOKYO time zone
//    public static final String TOKYO_BREAK_END = "12:40"; // 12:40 in TOKYO time zone
//
//    public static final String HONG_KONG_BREAK_START = "12:00"; // 12:30 in HONG_KONG time zone
//    public static final String HONG_KONG_BREAK_END = "13:40"; // 14:30 in HONG_KONG time zone
//
//    public static final String CHINA_BREAK_START = "11:30"; // 11:30 in CHINA time zone
//    public static final String CHINA_BREAK_END = "13:00"; // 13:00 in CHINA time zone
//
//    public static final String JAKARTA_BREAK_START = "12:00"; // in JAKARTA time zone
//    public static final String JAKARTA_BREAK_END = "13:40"; // in JAKARTA time zone
//
//    public static final String DUBAI_BREAK_START = "12:45"; // in DUBAI time zone
//    public static final String DUBAI_BREAK_END = "14:40"; // in DUBAI time zone
//
//    public static final String SINGAPORE_BREAK_START = "12:30"; // 12:30 in SINGAPORE time zone
//    public static final String SINGAPORE_BREAK_END = "14:10"; // 14:10 in SINGAPORE time zone
//
//
//	private long id;
//	private long marketId;
//	private Market market;
//	private Date timeCreated;
//	private Date timeFirstInvest;
//	private Date timeEstClosing;
//	private Date timeActClosing;
//	private Date timeLastInvest;
//	private double currentLevel;
//	private double closingLevel;
//	private Date timeSettled;
//	private int isPublished;
//	private int isSettled;
//	private long oddsTypeId;
//	private String oddsGroup;
//	private long opportunityTypeId;
//	private boolean disabled;
//	private boolean disabledService;
//	private boolean disabledTrader;
//	private int scheduled;
//	private OpportunityOddsType oddsType;
//	private long writerId;
//	private long typeId;
//	private OpportunityType type;
//	private long exchangeId;
//	private Exchange exchange;
//	private long investmentLimitGroupId;
//	private InvestmentLimitsGroup investmentLimitGroup;
//	// TODO REMOVE
//	private double shiftParameter; // the value that can be set from the
//									// backend to shift the level
//	// TODO REMOVE
//	private double shiftParameterAO; // the value that can be set from the
//									// backend to shift the level for AO
//	
//	private Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMappings;
//
//	private double autoShiftParameter;
//	private long winLose;
//	private boolean deductWinOdds;
//	private String opportunityTypeDesc;
//	private String opportunityOddsTypeName;
//
//	// for the LS data adaptor - the exposure calculation amounts
//	private double puts;
//	private double calls;
//	private int putsCount;
//	private int callsCount;
//	private double putsShift;
//	private double callsShift;
//	private boolean shifting;
//	private boolean shiftUp;
//	private BigDecimal lastShift;
//	private int state;
//	private boolean timeToClose;
//	private Date timeNextOpen;
//	private int putsCountET;
//	private int callsCountET;
//	/**
//	 * For hour opportunities the level is updated until update for next hour is
//	 * received. This flag should be set to true once an update for the next
//	 * hour is received.
//	 */
//	private boolean closingLevelReceived;
//	private double nextClosingLevel;
//	private boolean notClosedAlertSent;
//
//	// represent the formula and the parameters with there values
//	private String closeLevelTxt;
//
//	// one touch variables
//	private String oneTouchMsg;
//	private long oneTouchUpDown;
//	private boolean isOneTouchUp;
//	private long decimalPointOneTouch;
//	private long decimalPointZeroOneHundred;
//	
//	// TODO REMOVE
//	protected int maxExposure;
//	
//	protected double gmLevel;
//	protected boolean isOpenGraph;
//    private long oneTouchOppExposure;
//
//	/** if to use manual closing level for this opportunity */
//	protected boolean useManualClosingLevel;
//	/** manual closing level to use for this opportunity */
//	protected double manualClosingLevel;
//
//	//bean to calculate the binary Zero One Hundred formula
//	private BinaryZeroOneHundred binaryZeroOneHundred;
//	private double contractsBought; //in $
//	private double contractsSold; //in $
//
//	private ReutersQuotes lastUpdateQuote;
//	private ReutersQuotes beforeLastUpdateQuote;
//	private ReutersQuotes firstUpdateQuote;
//	private boolean isHaveReutersQuote;
//
//	public Opportunity() {
//		state = 0;
//		timeToClose = false;
//		notClosedAlertSent = false;
//		closingLevelReceived = false;
//	}
//
//	/**
//	 * @return the calls
//	 */
//	public double getCalls() {
//		return calls;
//	}
//
//	/**
//	 * @param calls
//	 *            the calls to set
//	 */
//	public void setCalls(double calls) {
//		this.calls = calls;
//	}
//
//	public double getClosingLevel() {
//		return closingLevel;
//	}
//
//	public String getClosingLevelTxt() {
//		if (isSettled == 1) {
//			return CommonUtil.formatLevelByMarket(closingLevel, marketId);
//		}
//		return "";
//	}
//
//	public void setClosingLevel(double closingLevel) {
//		this.closingLevel = closingLevel;
//	}
//
//	/**
//	 * @return the exchange
//	 */
//	public Exchange getExchange() {
//		return exchange;
//	}
//
//	/**
//	 * @param exchange
//	 *            the exchange to set
//	 */
//	public void setExchange(Exchange exchange) {
//		this.exchange = exchange;
//	}
//
//	/**
//	 * @return the exchangeId
//	 */
//	public long getExchangeId() {
//		return exchangeId;
//	}
//
//	/**
//	 * @param exchangeId
//	 *            the exchangeId to set
//	 */
//	public void setExchangeId(long exchangeId) {
//		this.exchangeId = exchangeId;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getIdTxt() {
//		return Long.toString(this.id);
//	}
//
//	/**
//	 * @return the investmentLimitGroup
//	 */
//	public InvestmentLimitsGroup getInvestmentLimitGroup() {
//		return investmentLimitGroup;
//	}
//
//	/**
//	 * @param investmentLimitGroup
//	 *            the investmentLimitGroup to set
//	 */
//	public void setInvestmentLimitGroup(
//			InvestmentLimitsGroup investmentLimitGroup) {
//		this.investmentLimitGroup = investmentLimitGroup;
//	}
//
//	/**
//	 * @return the investmentLimitGroupId
//	 */
//	public long getInvestmentLimitGroupId() {
//		return investmentLimitGroupId;
//	}
//
//	/**
//	 * @param investmentLimitGroupId
//	 *            the investmentLimitGroupId to set
//	 */
//	public void setInvestmentLimitGroupId(long investmentLimitGroupId) {
//		this.investmentLimitGroupId = investmentLimitGroupId;
//	}
//
//	public int getIsPublished() {
//		return isPublished;
//	}
//
//	public void setIsPublished(int isPublished) {
//		this.isPublished = isPublished;
//	}
//
//	public int getIsSettled() {
//		return isSettled;
//	}
//
//	public void setIsSettled(int isSettled) {
//		this.isSettled = isSettled;
//	}
//
//	public long getMarketId() {
//		return marketId;
//	}
//
//	public void setMarketId(long marketId) {
//		this.marketId = marketId;
//	}
//
//	/**
//	 * @return Returns the market.
//	 */
//	public Market getMarket() {
//		return market;
//	}
//
//	/**
//	 * @param market
//	 *            The market to set.
//	 */
//	public void setMarket(Market market) {
//		this.market = market;
//	}
//
//	public long getOddsTypeId() {
//		return oddsTypeId;
//	}
//
//	public void setOddsTypeId(long oddsTypeId) {
//		this.oddsTypeId = oddsTypeId;
//	}
//
//	/**
//	 * @return Returns the oddsType.
//	 */
//	public OpportunityOddsType getOddsType() {
//		return oddsType;
//	}
//
//	/**
//	 * @param oddsType
//	 *            The oddsType to set.
//	 */
//	public void setOddsType(OpportunityOddsType oddsType) {
//		this.oddsType = oddsType;
//	}
//
//	/**
//	 * @return the puts
//	 */
//	public double getPuts() {
//		return puts;
//	}
//
//	/**
//	 * @param puts
//	 *            the puts to set
//	 */
//	public void setPuts(double puts) {
//		this.puts = puts;
//	}
//
//	/**
//	 * @return the putsCount
//	 */
//	public int getPutsCount() {
//		return putsCount;
//	}
//
//	/**
//	 * @param putsCount the putsCount to set
//	 */
//	public void setPutsCount(int putsCount) {
//		this.putsCount = putsCount;
//	}
//
//	/**
//	 * @return the callsCount
//	 */
//	public int getCallsCount() {
//		return callsCount;
//	}
//
//	/**
//	 * @param callsCount the callsCount to set
//	 */
//	public void setCallsCount(int callsCount) {
//		this.callsCount = callsCount;
//	}
//	
//	/**
//	 * @return a string representing the calls rate used for trends functionality
//	 */
//	public String getCallsCountRate(long skinBusinessCase) {
//		if (skinBusinessCase == Skin.SKIN_BUSINESS_ETRADER) {
//			if (getCallsCountET() + getPutsCountET() > 0 /* avoid division by zero */) {
//				return new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US))
//							.format(getCallsCountET() / (double) (getCallsCountET() + getPutsCountET()));
//			}
//		} else { //SKIN_BUSINESS_ANYOPTION
//			int callCountAO = getCallsCount() - getCallsCountET();
//			int putCountAO = getPutsCount() - getPutsCountET();
//			if (callCountAO + putCountAO > 0 /* avoid division by zero */) {
//				return new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US))
//							.format(callCountAO / (double) (callCountAO + putCountAO));
//			}
//		}
//		return "0.0";
//	}
//
//	public BigDecimal getLastShift() {
//		return lastShift;
//	}
//
//	public void setLastShift(BigDecimal lastShift) {
//		this.lastShift = lastShift;
//	}
//
//	/**
//	 * @return the shifting
//	 */
//	public boolean isShifting() {
//		return shifting;
//	}
//
//	/**
//	 * @param shifting
//	 *            the shifting to set
//	 */
//	public void setShifting(boolean shifting) {
//		this.shifting = shifting;
//	}
//
//	public double getShiftParameter() {
//		return shiftParameter;
//	}
//
//	public void setShiftParameter(double shiftParameter) {
//		this.shiftParameter = shiftParameter;
//	}
//
//	public double getShiftParameterAO() {
//		return shiftParameterAO;
//	}
//
//	public void setShiftParameterAO(double shiftParameterAO) {
//		this.shiftParameterAO = shiftParameterAO;
//	}
//
//	public double getAutoShiftParameter() {
//		return autoShiftParameter;
//	}
//
//	public void setAutoShiftParameter(double autoShiftParameter) {
//		this.autoShiftParameter = autoShiftParameter;
//	}
//
//	/**
//	 * @return the shiftUp
//	 */
//	public boolean isShiftUp() {
//		return shiftUp;
//	}
//
//	/**
//	 * @param shiftUp
//	 *            the shiftUp to set
//	 */
//	public void setShiftUp(boolean shiftUp) {
//		this.shiftUp = shiftUp;
//	}
//
//	/**
//	 * @return the state
//	 */
//	public int getState() {
//		return state;
//	}
//
//	/**
//	 * @param state
//	 *            the state to set
//	 */
//	public void setState(int state) {
//		this.state = state;
//	}
//
//	/**
//	 * @return <code>true</code> if the state is one that need to receive
//	 *         market updates (from Reuters) else <code>false</code>. State
//	 *         that does not need updates for example is "CREATED".
//	 */
//	public boolean isInOppenedState() {
//		return state >= Opportunity.STATE_OPENED
//				&& state <= Opportunity.STATE_CLOSED;
//	}
//
//
//	/**
//	 * @return <code>true</code> if users can invest in this opportunity, otherwise <code>false</code>.
//	 * NOTE: the opportunity can be OPEN but the state may not allow investing.
//	 * See {@link Opportunity#isInOppenedState()}
//	 */
//	public boolean isAvailableForTrading() {
//		return state == Opportunity.STATE_OPENED
//				|| state == Opportunity.STATE_LAST_10_MIN;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated
//	 *            the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the timeFirstInvest
//	 */
//	public Date getTimeFirstInvest() {
//		return timeFirstInvest;
//	}
//
//	/**
//	 * @param timeFirstInvest
//	 *            the timeFirstInvest to set
//	 */
//	public void setTimeFirstInvest(Date timeFirstInvest) {
//		this.timeFirstInvest = timeFirstInvest;
//	}
//
//	/**
//	 * @return the timeEstClosing
//	 */
//	public Date getTimeEstClosing() {
//		return timeEstClosing;
//	}
//
//	/**
//	 * @param timeEstClosing
//	 *            the timeEstClosing to set
//	 */
//	public void setTimeEstClosing(Date timeEstClosing) {
//		this.timeEstClosing = timeEstClosing;
//	}
//	
//	public boolean isLessThanHourToClosing() {
//		return getTimeEstClosing().getTime() - System.currentTimeMillis() <= 60L * 60L * 1000L;
//	}
//
//	/**
//	 * @return the timeActClosing
//	 */
//	public Date getTimeActClosing() {
//		return timeActClosing;
//	}
//
//	/**
//	 * @param timeActClosing
//	 *            the timeActClosing to set
//	 */
//	public void setTimeActClosing(Date timeActClosing) {
//		this.timeActClosing = timeActClosing;
//	}
//
//	/**
//	 * @return the timeLastInvest
//	 */
//	public Date getTimeLastInvest() {
//		return timeLastInvest;
//	}
//
//	/**
//	 * @param timeLastInvest
//	 *            the timeLastInvest to set
//	 */
//	public void setTimeLastInvest(Date timeLastInvest) {
//		this.timeLastInvest = timeLastInvest;
//	}
//
//	/**
//	 * @return the timeNextOpen
//	 */
//	public Date getTimeNextOpen() {
//		return timeNextOpen;
//	}
//
//	/**
//	 * @param timeNextOpen
//	 *            the timeNextOpen to set
//	 */
//	public void setTimeNextOpen(Date timeNextOpen) {
//		this.timeNextOpen = timeNextOpen;
//	}
//
//	/**
//	 * @return Returns the currentLevel.
//	 */
//	public double getCurrentLevel() {
//		return currentLevel;
//	}
//
//	/**
//	 * @param currentLevel
//	 *            The currentLevel to set.
//	 */
//	public void setCurrentLevel(double currentLevel) {
//		this.currentLevel = currentLevel;
//	}
//
//	public String getOneTouchCurrentLevel() {
//
//		return CommonUtil.formatLevelByDecimalPoint(currentLevel,
//				decimalPointOneTouch);
//
//		/*
//		 * if (marketId == 21 || marketId == 200) { DecimalFormat df = new
//		 * DecimalFormat("#"); return df.format(currentLevel); } DecimalFormat
//		 * df = new DecimalFormat("#.00"); return df.format(currentLevel);
//		 */
//	}
//
//	public String getZeroOneHundredCurrentLevel(){
//		return CommonUtil.formatLevelByDecimalPoint(currentLevel, decimalPointZeroOneHundred);
//	}
//
//	/**
//	 * @return the timeSettled
//	 */
//	public Date getTimeSettled() {
//		return timeSettled;
//	}
//
//	/**
//	 * @param timeSettled
//	 *            the timeSettled to set
//	 */
//	public void setTimeSettled(Date timeSettled) {
//		this.timeSettled = timeSettled;
//	}
//
//	public long getWriterId() {
//		return writerId;
//	}
//
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	/**
//	 * @return Returns the typeId.
//	 */
//	public long getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId
//	 *            The typeId to set.
//	 */
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}
//
//	/**
//	 * @return Returns the type.
//	 */
//	public OpportunityType getType() {
//		return type;
//	}
//
//	/**
//	 * @param type
//	 *            The type to set.
//	 */
//	public void setType(OpportunityType type) {
//		this.type = type;
//	}
//
//	public long getOpportunityTypeId() {
//		return opportunityTypeId;
//	}
//
//	public void setOpportunityTypeId(long opportunityTypeId) {
//		this.opportunityTypeId = opportunityTypeId;
//	}
//
//	public String getMarketName() {
//		return CommonUtil.getMarketName(marketId);
//	}
//
//	public String getEnglishMarketName() {
//		return CommonUtil.getEnglishMarketName(marketId);
//	}
//
//	public String getGroupName() {
//		return ApplicationDataBase.getGroupName(id);
//	}
//
//	public String getTimeEstClosingTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, CommonUtil
//				.getUtcOffset(ConstantsBase.EMPTY_STRING));
//	}
//
//	public String getTimeEstClosingTxtByFormat(String format) {
//        return CommonUtil.getTimeFormat(timeEstClosing,
//        			CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING), format);
//    }
//
//	public String getTimeEstClosingEtLlTxt() {
//		 TimeZone tz = TimeZone.getTimeZone("Israel");
//		 String utcOffset = CommonUtil.getUtcOffset();
//		 if (tz.inDaylightTime(getTimeEstClosing())) {
//			 utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
//			 } else {
//				utcOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
//			 }
//		return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, utcOffset);
//	}
//
//	public String getTimeActClosingTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeActClosing, CommonUtil
//				.getUtcOffset(ConstantsBase.EMPTY_STRING));
//	}
//
//	public String getTimeFirstInvestTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeFirstInvest, CommonUtil
//				.getUtcOffset(ConstantsBase.EMPTY_STRING));
//	}
//
//	public String getTimeLastInvestTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeLastInvest, CommonUtil
//				.getUtcOffset(ConstantsBase.EMPTY_STRING));
//	}
//
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil
//				.getUtcOffset(ConstantsBase.EMPTY_STRING));
//	}
//
//	public String getWriterName() throws SQLException {
//		return CommonUtil.getWriterName(writerId);
//	}
//
//	public String getStatusTxt() {
//		if (isSettled == 1)
//			return CommonUtil.getMessage("opportunities.settled", null);
//		if (isPublished == 1)
//			return CommonUtil.getMessage("opportunities.published", null);
//
//		return CommonUtil.getMessage("opportunities.notpublished", null);
//	}
//
//	public String getOpportunityOddsTypeName() {
//		return opportunityOddsTypeName;
//	}
//
//	public void setOpportunityOddsTypeName(String opportunityOddsTypeName) {
//		this.opportunityOddsTypeName = opportunityOddsTypeName;
//	}
//
//	public String getOpportunityTypeDesc() {
//		return opportunityTypeDesc;
//	}
//
//	public void setOpportunityTypeDesc(String opportunityTypeDesc) {
//		this.opportunityTypeDesc = opportunityTypeDesc;
//	}
//
//	public String getShiftParameterTxt() {
//		return CommonUtil.displayDecimal(skinGroupMappings.get(SkinGroup.ETRADER).getShiftParameter());
//	}
//
//	public boolean isDisabled() {
//		return disabled;
//	}
//
//	public void setDisabled(boolean disabled) {
//		this.disabled = disabled;
//	}
//
//	public boolean isDisabledService() {
//		return disabledService;
//	}
//
//	public void setDisabledService(boolean disabledService) {
//		this.disabledService = disabledService;
//	}
//
//	public boolean isDisabledTrader() {
//		return disabledTrader;
//	}
//
//	public void setDisabledTrader(boolean disabledTrader) {
//		this.disabledTrader = disabledTrader;
//	}
//
//	public int getScheduled() {
//		return scheduled;
//	}
//
//	public String getScheduledTxt() {
//		return InvestmentsManagerBase.getScheduledTxt(scheduled);
//	}
//
//	public void setScheduled(int scheduled) {
//		this.scheduled = scheduled;
//	}
//
//	public boolean isLongTerm() {
//		return scheduled == Opportunity.SCHEDULED_WEEKLY
//				|| scheduled == Opportunity.SCHEDULED_MONTHLY;
//	}
//
//	public String getWinLoseTxt() {
//		return CommonUtil.displayAmount(winLose, ConstantsBase.CURRENCY_USD_ID);
//	}
//
//	public boolean isTimeToClose() {
//		return timeToClose;
//	}
//
//	public void setTimeToClose(boolean timeToClose) {
//		this.timeToClose = timeToClose;
//	}
//
//	public boolean isClosingLevelReceived() {
//		return closingLevelReceived;
//	}
//
//	public void setClosingLevelReceived(boolean closingLevelReceived) {
//		this.closingLevelReceived = closingLevelReceived;
//	}
//
//	public double getNextClosingLevel() {
//		return nextClosingLevel;
//	}
//
//	public void setNextClosingLevel(double nextClosingLevel) {
//		this.nextClosingLevel = nextClosingLevel;
//	}
//
//	public boolean isNotClosedAlertSent() {
//		return notClosedAlertSent;
//	}
//
//	public void setNotClosedAlertSent(boolean notClosedAlertSent) {
//		this.notClosedAlertSent = notClosedAlertSent;
//	}
//
//	public boolean isDeductWinOdds() {
//		return deductWinOdds;
//	}
//
//	public void setDeductWinOdds(boolean deductWinOdds) {
//		this.deductWinOdds = deductWinOdds;
//	}
//
//	public float getOverOddsWin() {
//		if (!deductWinOdds) {
//			return oddsType.getOverOddsWin();
//		}
//		long timeToLastInvest = timeLastInvest.getTime()
//				- System.currentTimeMillis();
//		int steps = 0;
//		// for (int i = 1; i <= 4; i++) {
//		// if (timeToLastInvest < (10 - i * 2) * 60000 &&
//		// timeToLastInvest >= (8 - i * 2) * 60000) {
//		// steps = i;
//		// }
//		// }
//		if (timeToLastInvest < 8 * 60000 && timeToLastInvest >= 6 * 60000) {
//			steps = 1;
//		} else if (timeToLastInvest < 6 * 60000
//				&& timeToLastInvest >= 4 * 60000) {
//			steps = 2;
//		} else if (timeToLastInvest < 4 * 60000
//				&& timeToLastInvest >= 2 * 60000) {
//			steps = 3;
//		} else if (timeToLastInvest < 2 * 60000) {
//			steps = 4;
//		}
//		BigDecimal odds = new BigDecimal(Float.toString(oddsType
//				.getOverOddsWin()));
//		BigDecimal stps = new BigDecimal(steps);
//		BigDecimal deductStep = new BigDecimal(Float.toString(oddsType
//				.getOddsWinDeductStep()));
//		float oow = odds.subtract(stps.multiply(deductStep)).floatValue();
//		if (log.isDebugEnabled()) {
//			log.debug("timeToLastInvest: " + timeToLastInvest + " steps: "
//					+ steps + " odds: " + odds + " stps: " + stps
//					+ " deductStep: " + deductStep + " oow: " + oow);
//		}
//		return oow;
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes in name = value
//	 * format.
//	 *
//	 * @return a <code>String</code> representation of this object.
//	 */
//	public String toString() {
//		String ls = System.getProperty("line.separator");
//		return ls + "Opportunity (" + super.toString() + ")" + ls + "id = "
//				+ this.id + ls + "marketId = " + this.marketId + ls
//				+ "market = " + this.market + ls + "timeCreated = "
//				+ this.timeCreated + ls + "timeFirstInvest = "
//				+ this.timeFirstInvest + ls + "timeEstClosing = "
//				+ this.timeEstClosing + ls + "timeActClosing = "
//				+ this.timeActClosing + ls + "timeLastInvest = "
//				+ this.timeLastInvest + ls + "currentLevel = "
//				+ this.currentLevel + ls + "closingLevel = "
//				+ this.closingLevel + ls + "timeSettled = " + this.timeSettled
//				+ ls + "isPublished = " + this.isPublished + ls
//				+ "isSettled = " + this.isSettled + ls + "oddsTypeId = "
//				+ this.oddsTypeId + ls
//				+ "oddsGroup = " + this.oddsGroup + ls
//				+ "opportunityTypeId = "
//				+ this.opportunityTypeId + ls + "disabled = " + this.disabled
//				+ ls + "disabledService = " + this.disabledService + ls
//				+ "disabledTrader = " + this.disabledTrader + ls
//				+ "scheduled = " + this.scheduled + ls + "oddsType = "
//				+ this.oddsType + ls + "writerId = " + this.writerId + ls
//				+ "typeId = " + this.typeId + ls + "type = " + this.type + ls
//				+ "exchangeId = " + this.exchangeId + ls + "exchange = "
//				+ this.exchange + ls + "investmentLimitGroupId = "
//				+ this.investmentLimitGroupId + ls + "investmentLimitGroup = "
//				+ this.investmentLimitGroup + ls + "shiftParameter = "
//				+ this.shiftParameter + ls + ls + "shiftParameterAO = "
//				+ this.shiftParameterAO + ls + "autoShiftParameter = "
//				+ this.autoShiftParameter + ls + "opportunityTypeDesc = "
//				+ this.opportunityTypeDesc + ls + "opportunityOddsTypeName = "
//				+ this.opportunityOddsTypeName + ls
//				+ "puts = " + this.puts + ls
//				+ "putsCount = " + this.putsCount + ls
//				+ "calls = " + this.calls + ls
//				+ "callsCount = " + this.callsCount + ls
//				+ "putsShift = " + this.putsShift + ls
//				+ "callsShift = " + this.callsShift + ls
//				+ "shifting = " + this.shifting + ls
//				+ "shiftUp = " + this.shiftUp + ls
//				+ "lastShift = " + this.lastShift + ls + "state = "
//				+ this.state + ls + "timeToClose = " + this.timeToClose + ls
//				+ "timeNextOpen = " + this.timeNextOpen + ls
//				+ "closingLevelReceived = " + this.closingLevelReceived + ls
//				+ "nextClosingLevel = " + this.nextClosingLevel + ls
//				+ "notClosedAlertSent = " + this.notClosedAlertSent + ls
//				+ "deductWinOdds = " + this.deductWinOdds + ls
//				+ "closeLevelTxt = " + this.closeLevelTxt + ls
//				+ "skinGroupMappings = " + this.skinGroupMappings + " )";
//	}
//
//	public long getWinLose() {
//		return winLose;
//	}
//
//	public void setWinLose(long winLose) {
//		this.winLose = winLose;
//	}
//
//	public String getCloseLevelTxt() {
//		return closeLevelTxt;
//	}
//
//	public void setCloseLevelTxt(String tempCloseLevelTxt) {
//		this.closeLevelTxt = tempCloseLevelTxt;
//	}
//
//	public String getOneTouchMsg() {
//
//		String[] params = new String[3];
//		params[0] = market.getDisplayName();
//		params[1] = String.valueOf(getOneTouchCurrentLevel());
//		Calendar c = Calendar.getInstance();
//		c.setTime(timeEstClosing);
//		params[2] = c.get(Calendar.DAY_OF_MONTH) + "."
//				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
//
//		return CommonUtil.getMessage(oneTouchMsg, params);
//
//	}
//
//	public String getMsg() {
//		return oneTouchMsg;
//	}
//
//	public void setOneTouchMsg(String oneTouchMsg) {
//		this.oneTouchMsg = oneTouchMsg;
//	}
//
//	public long getOneTouchUpDown() {
//		return oneTouchUpDown;
//	}
//
//	public void setOneTouchUpDown(long upDown) {
//		this.oneTouchUpDown = upDown;
//
//		if (this.oneTouchUpDown == 1) {
//			this.isOneTouchUp = true;
//		} else {
//			this.isOneTouchUp = false;
//		}
//	}
//
//	public boolean getIsBooleanOneTouchUpDown() {
//		if (this.oneTouchUpDown == 1) {
//			return true;
//		} else {
//			return false;
//		}
//
//	}
//
//	public boolean getIsOneTouchUp() {
//		return isOneTouchUp;
//	}
//
//	public void setIsOneTouchUp(boolean isOneTouchUp) {
//		this.isOneTouchUp = isOneTouchUp;
//	}
//
//	public String getOverOddsWinFormat() {
//		DecimalFormat percentFormat = new DecimalFormat("#0%");
//		return percentFormat.format(oddsType.getOverOddsWin());
//	}
//
//	public String getOverOddsWinFormatAO() {
//		// no need in java formatting 'cause there is problem in arabic and
//		// turkish skin
//		return String.valueOf((int) (oddsType.getOverOddsWin() * 100));
//	}
//
//	/**
//	 * @return the decimalPointOneTouch
//	 */
//	public long getDecimalPointOneTouch() {
//		return decimalPointOneTouch;
//	}
//
//	/**
//	 * @param decimalPointOneTouch
//	 *            the decimalPointOneTouch to set
//	 */
//	public void setDecimalPointOneTouch(long decimalPointOneTouch) {
//		this.decimalPointOneTouch = decimalPointOneTouch;
//	}
//
//	public boolean getIsOneTouchOpp() {
//		if (opportunityTypeId == Opportunity.TYPE_ONE_TOUCH) {
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * Return dates range of oneTouch opp from first_inv to last_inv
//	 *
//	 * @return
//	 */
//	public String getOneTouchDateRange() {
//		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
//		String dateRange = formater.format(timeFirstInvest)
//				+ " "
//				+ CommonUtil.getMessage("opportunities.one.date.delimiter",
//						null) + " " + formater.format(timeEstClosing);
//		return dateRange;
//	}
//
//	public int getMaxExposure() {
//		return maxExposure;
//	}
//
//	public void setMaxExposure(int maxExposure) {
//		this.maxExposure = maxExposure;
//	}
//
//	public boolean getIsOppSettled() {
//		if (isSettled == 1) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public void validateDecimalPoint(FacesContext context, UIComponent comp,
//			Object value) throws Exception {
//		FacesMessage msg = new FacesMessage(CommonUtil.getMessage(
//				"opportunity.validate.error.decimalPoint", null));
//		String currentLevelTxt = String.valueOf((Double) value);
//		String[] currentLevelSplit = currentLevelTxt.split("\\.");
//		if (currentLevelSplit[1].toString().equals("0")
//				&& decimalPointOneTouch == 0) {
//			return;
//		} else {
//			if (decimalPointOneTouch >= currentLevelSplit[1].length()) {
//				return;
//			}
//		}
//		throw new ValidatorException(msg);
//	}
//
//	/**
//	 * Check if this opportunity is good for current Golden Minutes interval.
//	 *
//	 * @param endOfHour
//	 *            if the current Golden Minutes interval is a end of hour or mid
//	 *            hour one
//	 * @return <code>true</code> if this opp is good for the current GM
//	 *         interval else <code>false</code>
//	 */
//	public boolean isGoodForGoldenMinute(boolean endOfHour) {
//		if (scheduled != SCHEDULED_HOURLY || !isInOppenedState()) {
//			return false;
//		}
//		Calendar c = Calendar.getInstance();
//		c.setTime(timeEstClosing);
//		int min = c.get(Calendar.MINUTE);
//		if ((min == 0 && endOfHour) || (min == 30 && !endOfHour)) {
//			return true;
//		}
//		return false;
//	}
//
//	public double getGmLevel() {
//		return gmLevel;
//	}
//
//	public void setGmLevel(double gmLevel) {
//		this.gmLevel = gmLevel;
//	}
//
//	/**
//	 * @return the isOpenGraph
//	 */
//	public boolean isOpenGraph() {
//		return isOpenGraph;
//	}
//
//	/**
//	 * @param isOpenGraph
//	 *            the isOpenGraph to set
//	 */
//	public void setOpenGraph(boolean isOpenGraph) {
//		this.isOpenGraph = isOpenGraph;
//	}
//
//	public double getManualClosingLevel() {
//		return manualClosingLevel;
//	}
//
//	public void setManualClosingLevel(double manualClosingLevel) {
//		this.useManualClosingLevel = true;
//		this.manualClosingLevel = manualClosingLevel;
//	}
//
//	public boolean isUseManualClosingLevel() {
//		return useManualClosingLevel;
//	}
//
//    /**
//     * @return the oneTouchOppExposure
//     */
//    public long getOneTouchOppExposure() {
//        return oneTouchOppExposure;
//    }
//
//    /**
//     * @param oneTouchOppExposure the oneTouchOppExposure to set
//     */
//    public void setOneTouchOppExposure(long oneTouchOppExposure) {
//        this.oneTouchOppExposure = oneTouchOppExposure;
//    }
//
//	/**
//	 * @return the binaryZeroOneHundred
//	 */
//	public BinaryZeroOneHundred getBinaryZeroOneHundred() {
//		return binaryZeroOneHundred;
//	}
//
//	/**
//	 * @param binaryZeroOneHundred the binaryZeroOneHundred to set
//	 */
//	public void setBinaryZeroOneHundred(BinaryZeroOneHundred binaryZeroOneHundred) {
//		this.binaryZeroOneHundred = binaryZeroOneHundred;
//	}
//
//	/**
//	 * @return the contractsBought
//	 */
//	public double getContractsBought() {
//		return contractsBought;
//	}
//
//	/**
//	 * @return the contractsBought 2 decimal point round up
//	 */
//	public double getContractsBought2DP() {
//		return CommonUtil.roundDouble(contractsBought, 2);
//	}
//
//	/**
//	 * @param contractsBought the contractsBought to set
//	 */
//	public void setContractsBought(double contractsBought) {
//		this.contractsBought = contractsBought;
//	}
//
//	/**
//	 * @return the contractsSold
//	 */
//	public double getContractsSold() {
//		return contractsSold;
//	}
//
//	/**
//	 * @return the contractsSold 2 decimal point round up
//	 */
//	public double getContractsSold2DP() {
//		return CommonUtil.roundDouble(contractsSold, 2);
//	}
//
//	/**
//	 * @param contractsSold the contractsSold to set
//	 */
//	public void setContractsSold(double contractsSold) {
//		this.contractsSold = contractsSold;
//	}
//	public long getDecimalPointZeroOneHundred() {
//		return decimalPointZeroOneHundred;
//	}
//
//	public void setDecimalPointZeroOneHundred(long decimalPointZeroOneHundred) {
//		this.decimalPointZeroOneHundred = decimalPointZeroOneHundred;
//	}
//
//	public boolean isBinary0100Type() {
//		if (opportunityTypeId == TYPE_BINARY_0_100_ABOVE || opportunityTypeId == TYPE_BINARY_0_100_BELOW) {
//			return true;
//		}
//		return false;
//	}
//
//	public double getCallsShift() {
//		return callsShift;
//	}
//
//	public void setCallsShift(double callsShift) {
//		this.callsShift = callsShift;
//	}
//
//	public double getPutsShift() {
//		return putsShift;
//	}
//
//	public void setPutsShift(double putsShift) {
//		this.putsShift = putsShift;
//	}
//
//	public String getPrintBinary0100Event() {
//		String result = null;
//		String[] params = null;
//		params=new String[3];
//		params[0]=getMarketName();
//		params[1]=CommonUtil.formatLevelByMarket(currentLevel, marketId);
//		params[2]=CommonUtil.getTimeFormat(timeEstClosing, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
//
//		if (opportunityTypeId == TYPE_BINARY_0_100_ABOVE) {
//		  result = CommonUtil.getMessage("opportunity.above", params);
//		}
//		if (opportunityTypeId == TYPE_BINARY_0_100_BELOW) {
//			result = CommonUtil.getMessage("opportunity.below", params);
//		}
//
//		return result;
//	}
//
//	public String getOddsGroup() {
//		return oddsGroup;
//	}
//
//	public void setOddsGroup(String oddsGroup) {
//		this.oddsGroup = oddsGroup;
//	}
//
//	/**
//	 * @return the beforeLastUpdateQuote
//	 */
//	public ReutersQuotes getBeforeLastUpdateQuote() {
//		return beforeLastUpdateQuote;
//	}
//
//	/**
//	 * @param beforeLastUpdateQuote the beforeLastUpdateQuote to set
//	 */
//	public void setBeforeLastUpdateQuote(ReutersQuotes beforeLastUpdateQuote) {
//		this.beforeLastUpdateQuote = beforeLastUpdateQuote;
//	}
//
//	/**
//	 * @return the lastUpdateQuote
//	 */
//	public ReutersQuotes getLastUpdateQuote() {
//		return lastUpdateQuote;
//	}
//
//	/**
//	 * @param lastUpdateQuote the lastUpdateQuote to set
//	 */
//	public void setLastUpdateQuote(ReutersQuotes lastUpdateQuote) {
//		this.lastUpdateQuote = lastUpdateQuote;
//	}
//
//	/**
//	 * @return the firstUpdateQuote
//	 */
//	public ReutersQuotes getFirstUpdateQuote() {
//		return firstUpdateQuote;
//	}
//
//	/**
//	 * @param firstUpdateQuote the firstUpdateQuote to set
//	 */
//	public void setFirstUpdateQuote(ReutersQuotes firstUpdateQuote) {
//		this.firstUpdateQuote = firstUpdateQuote;
//	}
//
//	/**
//	 * @param lastUpdateQuote the lastUpdateQuote to set
//	 */
//	public void setLastAndBeforeUpdateQuote(ReutersQuotes lastUpdateQuote) {
//		this.beforeLastUpdateQuote = this.lastUpdateQuote;
//		this.lastUpdateQuote = lastUpdateQuote;
//	}
//
//	/**
//	 * @return the isHaveReutersQuote
//	 */
//	public boolean isHaveReutersQuote() {
//		return isHaveReutersQuote;
//	}
//
//	/**
//	 * @param isHaveReutersQuote the isHaveReutersQuote to set
//	 */
//	public void setHaveReutersQuote(boolean isHaveReutersQuote) {
//		this.isHaveReutersQuote = isHaveReutersQuote;
//	}
//
//	/**
//	 * @return the skinGroupMappings
//	 */
//	public Map<SkinGroup, OpportunitySkinGroupMap> getSkinGroupMappings() {
//		return skinGroupMappings;
//	}
//
//	/**
//	 * @param skinGroupMappings the skinGroupMappings to set
//	 */
//	public void setSkinGroupMappings(Map<SkinGroup,
//										OpportunitySkinGroupMap> skinGroupMappings) {
//		this.skinGroupMappings = skinGroupMappings;
//	}
//	
//	/**
//	 * @return the putsCountET
//	 */
//	public int getPutsCountET() {
//		return putsCountET;
//	}
//
//	/**
//	 * @param putsCountET the putsCountET to set
//	 */
//	public void setPutsCountET(int putsCountET) {
//		this.putsCountET = putsCountET;
//	}
//
//	/**
//	 * @return the callsCountET
//	 */
//	public int getCallsCountET() {
//		return callsCountET;
//	}
//
//	/**
//	 * @param callsCountET the callsCountET to set
//	 */
//	public void setCallsCountET(int callsCountET) {
//		this.callsCountET = callsCountET;
//	}
//
//	/**
//	 * @param lastUpdateQuote the lastUpdateQuote to set
//	 *//*
//	public void setLastAndBeforeUpdateQuote(ReutersQuotes lastUpdateQuote, ReutersQuotes beforeLastUpdateQuote) {
//		if (null == beforeLastUpdateQuote) {
//			setLastAndBeforeUpdateQuote(lastUpdateQuote);
//		} else {
//			this.beforeLastUpdateQuote = beforeLastUpdateQuote;
//			this.lastUpdateQuote = lastUpdateQuote;
//		}
//	}*/
//}