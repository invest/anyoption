//package il.co.etrader.bl_vos;
//
//import java.util.Date;
//
//public class ChartsUpdaterMarket {
//	protected long id;
//	protected long decimalPoint;
//	protected String displayNameKey;
//	protected Date lastHourClosingTime;
//	protected boolean opened;
//	protected boolean haveHourly;
//	
//	public long getId() {
//		return id;
//	}
//	
//	public void setId(long id) {
//		this.id = id;
//	}
//	
//	public long getDecimalPoint() {
//		return decimalPoint;
//	}
//	
//	public void setDecimalPoint(long decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//	
//	public String getDisplayNameKey() {
//		return displayNameKey;
//	}
//	
//	public void setDisplayNameKey(String displayNameKey) {
//		this.displayNameKey = displayNameKey;
//	}
//	
//	public Date getLastHourClosingTime() {
//		return lastHourClosingTime;
//	}
//	
//	public void setLastHourClosingTime(Date lastHourClosingTime) {
//		this.lastHourClosingTime = lastHourClosingTime;
//	}
//	
//	public boolean isOpened() {
//		return opened;
//	}
//	
//	public void setOpened(boolean opened) {
//		this.opened = opened;
//	}
//	
//	public boolean isHaveHourly() {
//		return haveHourly;
//	}
//	
//	public void setHaveHourly(boolean haveHourly) {
//		this.haveHourly = haveHourly;
//	}
//	
//	public String toString() {
//		String ls = System.getProperty("line.separator");
//		return ls + "ChartsUpdaterMarket:" + ls +
//			"id: " + id  + ls +
//			"decimalPoint: " + decimalPoint + ls +
//			"displayNameKey: " + displayNameKey + ls +
//			"lastHourClosingTime: " + lastHourClosingTime + ls +
//			"opened: " + opened + ls +
//			"haveHourly: " + haveHourly + ls;
//	}
//}