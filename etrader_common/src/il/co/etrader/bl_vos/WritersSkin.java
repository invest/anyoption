package il.co.etrader.bl_vos;

import java.util.HashMap;

public class WritersSkin implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private long skinId;
	private long writerId;
	private long assignLimit;
	private HashMap<Long, RankLimits> rankStatusLimits;
	private HashMap<Long, RankData> ranksData; //auto assignment job
    private int sumAssignments;
	
	public WritersSkin() {
		ranksData = new HashMap<Long, RankData>();
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the assignLimit
	 */
	public long getAssignLimit() {
		return assignLimit;
	}
	/**
	 * @param assignLimit the assignLimit to set
	 */
	public void setAssignLimit(long assignLimit) {
		this.assignLimit = assignLimit;
	}
	/**
	 * @return the ranksData
	 */
	public HashMap<Long, RankData> getRanksData() {
		return ranksData;
	}
	/**
	 * @param ranksData the ranksData to set
	 */
	public void setRanksData(HashMap<Long, RankData> ranksData) {
		this.ranksData = ranksData;
	}

	/**
	 * @return the rankStatusLimits
	 */
	public HashMap<Long, RankLimits> getRankStatusLimits() {
		return rankStatusLimits;
	}

	/**
	 * @param rankStatusLimits the rankStatusLimits to set
	 */
	public void setRankStatusLimits(HashMap<Long, RankLimits> rankStatusLimits) {
		this.rankStatusLimits = rankStatusLimits;
	}

	/**
	 * @return the sumAssignments
	 */
	public int getSumAssignments() {
		return sumAssignments;
	}

	/**
	 * @param sumAssignments the sumAssignments to set
	 */
	public void setSumAssignments(int sumAssignments) {
		this.sumAssignments = sumAssignments;
	}
	
	/**
	 * Return the ratio between assignment to writer and his limit 
	 * @return
	 */
	public double sumAassignmentsRatioLimit() {
		try {
			return (double) sumAssignments / assignLimit;	
		} catch (Exception e) {
			return 0;
		}		
	}
		
}
