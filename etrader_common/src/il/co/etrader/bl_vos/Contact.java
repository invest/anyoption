package il.co.etrader.bl_vos;

import java.util.Date;

public class Contact implements java.io.Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static long CONTACT_ME_TYPE = 1;
	public static long CONTACT_US_TYPE = 2;
    public static long CONTACT_US_DIRECT_DOWNLOAD_WEB_EMAIL = 3;
    public static long CONTACT_US_DIRECT_DOWNLOAD_MOBILE = 4;
    public static long CONTACT_US_SHORT_REG_FORM = 5;
    public static long CONTACT_US_REGISTER_ATTEMPTS = 6;
    public static long CONTACT_US_QUICK_START_ATTEMPTS = 7;

	private long id;
	private String text;
	private long userId;
	private String phone;
	private long type;
	private long countryId;
	private String email;
	private long skinId;
	private long writerId;
	private long combId;
	private String dynamicParameter;
    private String aff_sub1;
    private String aff_sub2;
    private String aff_sub3;
	private String utcOffset;
	private Date timeCreated;
	private String countryName;
    private String ip;
    private String phoneType;
    private String userAgent;
    private String dfaPlacementId;
    private String dfaCreativeId;
    private String dfaMacro;
    //Register attempts parameters
	private String firstName;
    private String lastName;
    private String mobilePhone;
    private String landLinePhone;
	private boolean isContactByEmail;
	private boolean isContactBySMS;
    private String deviceUniqueId;
    private long affiliateKey;
    private long subAffiliateKey;
    private String httpReferer;
    private String clickId;
    private Date timeFirstVisit;
    
    
	/**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}

	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}

	/**
	 * @return the dynamicParameter
	 */
	public String getDynamicParameter() {
		return dynamicParameter;
	}

	/**
	 * @param dynamicParameter the dynamicParameter to set
	 */
	public void setDynamicParameter(String dynamicParameter) {
		this.dynamicParameter = dynamicParameter;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Contact ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "text = " + this.text+ TAB
	        + "userId = " + this.userId + TAB
	        + "phone = " + this.phone + TAB
	        + "type = " + this.type + TAB
	        + "countryId = " + this.countryId + TAB
	        + "email = " + this.email + TAB
	        + "skinId = " + this.skinId + TAB
	        + "writerId = " + this.writerId + TAB
	        + "combId = " + this.combId + TAB
	        + "dynamicParameter = " + this.dynamicParameter + TAB
	        + "userAgent = " + this.userAgent + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "landLinePhone = " + this.landLinePhone + TAB
	        + "isContactByEmail = " + this.isContactByEmail + TAB
	        + "isContactBySms = " + this.isContactBySMS + TAB
	        + "deviceUniqueId = " + this.deviceUniqueId + TAB
	        + "utcOffset = " + this.utcOffset + TAB
            + "timeCreated = " + this.timeCreated + TAB
            + "countryName = " + this.countryName + TAB
            + "ip = " + this.ip + TAB
            + "phoneType = " + this.phoneType + TAB
            + "dfaPlacementId = " + this.dfaPlacementId + TAB
            + "dfaCreativeId = " + this.dfaCreativeId + TAB
            + "dfaMacro = " + this.dfaMacro + TAB
            + "affiliateKey = " + this.affiliateKey + TAB
            + "subAffiliateKey = " + this.subAffiliateKey + TAB
            + "httpReferer = " + this.httpReferer + TAB
            + "clickId = " + this.clickId + TAB
            + "timeFirstVisit = " + this.timeFirstVisit + TAB
	        + " )";

	    return retValue;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

    /**
     * @return the phoneType
     */
    public String getPhoneType() {
        return phoneType;
    }

    /**
     * @param phoneType the phoneType to set
     */
    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the dfaCreativeId
	 */
	public String getDfaCreativeId() {
		return dfaCreativeId;
	}

	/**
	 * @param dfaCreativeId the dfaCreativeId to set
	 */
	public void setDfaCreativeId(String dfaCreativeId) {
		this.dfaCreativeId = dfaCreativeId;
	}

	/**
	 * @return the dfaPlacementId
	 */
	public String getDfaPlacementId() {
		return dfaPlacementId;
	}

	/**
	 * @param dfaPlacementId the dfaPlacementId to set
	 */
	public void setDfaPlacementId(String dfaPlacementId) {
		this.dfaPlacementId = dfaPlacementId;
	}

	/**
	 * @return the dfaMacro
	 */
	public String getDfaMacro() {
		return dfaMacro;
	}

	/**
	 * @param dfaMacro the dfaMacro to set
	 */
	public void setDfaMacro(String dfaMacro) {
		this.dfaMacro = dfaMacro;
	}

	/**
	 * @return the deviceUniqueId
	 */
	public String getDeviceUniqueId() {
		return deviceUniqueId;
	}

	/**
	 * @param deviceUniqueId the deviceUniqueId to set
	 */
	public void setDeviceUniqueId(String deviceUniqueId) {
		this.deviceUniqueId = deviceUniqueId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the isContactByEmail
	 */
	public boolean isContactByEmail() {
		return isContactByEmail;
	}

	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setContactByEmail(boolean isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}

	/**
	 * @return the isContactBySMS
	 */
	public boolean isContactBySMS() {
		return isContactBySMS;
	}

	/**
	 * @param isContactBySMS the isContactBySMS to set
	 */
	public void setContactBySMS(boolean isContactBySMS) {
		this.isContactBySMS = isContactBySMS;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the affiliateKey
	 */
	public long getAffiliateKey() {
		return affiliateKey;
	}

	/**
	 * @param affiliateKey the affiliateKey to set
	 */
	public void setAffiliateKey(long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

	/**
	 * @return the clickId
	 */
	public String getClickId() {
		return clickId;
	}

	/**
	 * @param clickId the clickId to set
	 */
	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}

	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}

	/**
	 * @return the subAffiliateKey
	 */
	public long getSubAffiliateKey() {
		return subAffiliateKey;
	}

	/**
	 * @param subAffiliateKey the subAffiliateKey to set
	 */
	public void setSubAffiliateKey(long subAffiliateKey) {
		this.subAffiliateKey = subAffiliateKey;
	}

	/**
	 * @return the timeFirstVisit
	 */
	public Date getTimeFirstVisit() {
		return timeFirstVisit;
	}

	/**
	 * @param timeFirstVisit the timeFirstVisit to set
	 */
	public void setTimeFirstVisit(Date timeFirstVisit) {
		this.timeFirstVisit = timeFirstVisit;
	}

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}

	
}
