package il.co.etrader.bl_vos;

import java.util.Date;

public class RemarketingLogin implements java.io.Serializable{

	
	private static final long serialVersionUID = 6531167379064694729L;
	
	private long id;
	private long userId;
	private long contactId;
	private Date timeCreated;
	private long actionType;
	private long combinationId;
	private String dyanmicParameter;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getContactId() {
		return contactId;
	}
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public long getActionType() {
		return actionType;
	}
	public void setActionType(long actionType) {
		this.actionType = actionType;
	}
	public long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}
	public String getDyanmicParameter() {
		return dyanmicParameter;
	}
	public void setDyanmicParameter(String dyanmicParameter) {
		this.dyanmicParameter = dyanmicParameter;
	}
	
}
