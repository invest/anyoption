package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;

public class Call implements java.io.Serializable{
	private long id;
	private long userId;
	private String typeId;
	private String subjectId;
	private long writerId;
	private Date timeCreated;
	private String content;

	private String subjectName;
	private String typeName;
	private String writerName;
	private String newContent;

	public Call() {
		id=0;

	}
	public String getInit() {

		return null;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated);
	}

	public String getNewContent() {
		return newContent;
	}

	public void setNewContent(String newContent) {
		this.newContent = newContent;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";
	    
	    String retValue = "";
	    
	    retValue = "Call ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "userId = " + this.userId + TAB
	        + "typeId = " + this.typeId + TAB
	        + "subjectId = " + this.subjectId + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "content = " + this.content + TAB
	        + "subjectName = " + this.subjectName + TAB
	        + "typeName = " + this.typeName + TAB
	        + "writerName = " + this.writerName + TAB
	        + "newContent = " + this.newContent + TAB
	        + " )";
	
	    return retValue;
	}

}
