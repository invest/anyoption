//package il.co.etrader.bl_vos;
//
//public class IssueStatus implements java.io.Serializable{
//
//	private long id;
//	private String name;
//	private int type;
//	private boolean supportStatus;
//
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	/**
//	 * @return the type
//	 */
//	public int getType() {
//		return type;
//	}
//	/**
//	 * @param type the type to set
//	 */
//	public void setType(int type) {
//		this.type = type;
//	}
//	/**
//	 * @return the supportStatus
//	 */
//	public boolean isSupportStatus() {
//		return supportStatus;
//	}
//	/**
//	 * @param supportStatus the supportStatus to set
//	 */
//	public void setSupportStatus(boolean supportStatus) {
//		this.supportStatus = supportStatus;
//	}
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "IssueStatus ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "type = " + this.type + TAB
//	        + "supportStatus = " + this.supportStatus + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//
//}
