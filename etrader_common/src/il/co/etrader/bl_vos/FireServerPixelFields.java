package il.co.etrader.bl_vos;

import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class FireServerPixelFields {
	private Date fromDate;
	private Date toDate;
	
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}
	
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}
	
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
