package il.co.etrader.bl_vos;

import java.io.Serializable;

public class CachePages implements Serializable {

	private static final long serialVersionUID = 6373132347863226176L;
	private long id;
	private String pageName;
	private long cacheTime;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}
	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	/**
	 * @return the cacheTime
	 */
	public long getCacheTime() {
		return cacheTime;
	}
	/**
	 * @param cacheTime the cacheTime to set
	 */
	public void setCacheTime(long cacheTime) {
		this.cacheTime = cacheTime;
	}

}
