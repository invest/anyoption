package il.co.etrader.bl_vos;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.UsersManagerBase;

import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class UserBase extends User {

	private static Logger logger = Logger.getLogger(UserBase.class);
	// users table fields:
//    protected long balance;
//    protected long taxBalance;
    protected long totalWinLose;
    protected long winLoseHouse;

//    protected Long currencyId;
//    protected Currency currency;
//    protected String firstName;
//    protected String lastName;
//    protected String street;
//    protected String streetNo;
//    protected long cityId;
//    protected String zipCode;
//    protected Date timeCreated;
    protected Date timeLastLogin;
    protected Date timeModified;
//    protected String email;
//    protected long combinationId;
    protected long affiliateKey;
    protected String comments;
    protected String ip;
//    protected Date timeBirthDate;
//    protected int isContactByEmail;
//    protected long isContactBySMS;
//    protected long isContactByPhone;
//    protected String mobilePhone;
//    protected String landLinePhone;
//    protected String mobilePhonePrefix;
//    protected String landLinePhonePrefix;
//    protected String mobilePhoneSuffix;
//    protected String landLinePhoneSuffix;
//    protected String gender;
//    protected String idNum;
//    protected String limitId;
//    protected String classId;
    protected String keyword;
    protected String companyName;
    protected BigDecimal companyId;
    protected boolean taxExemption;
    protected BigDecimal referenceId;
    protected int isNextInvestOnUs;

    // other calculated fields
    protected String password2;
    protected String campaignName;
    protected String campaignForStrip;
    protected long withdrawals;
    protected long pendingWithdrawals;
    protected long deposits;
    protected long userValue;
    protected boolean isOpenIssue;
    protected String birthYear;
    protected String birthMonth;
    protected String birthDay;
//    protected String cityName ="";

    protected String skinName;
    protected Skins skin;
//    protected long countryId;
    protected String countryName;
    
    protected long languageId;
    protected String languageName;
    protected String utcOffsetCreated;
    protected String utcOffsetModified;
    protected boolean idDocVerify;    // if true - user send us documents
//    protected String utcOffset;
//    protected Long state;
//    protected String dynamicParam;
    protected String specialCode;
//    protected long contactId;
    protected boolean isFalseAccount;
//    protected boolean isAcceptedTerms;
//    protected Date timeFirstVisit;
	private List<String> roles;
	protected TierUser tierUser;
	protected long chatId;
	/* Merged code */
	// protected long writerId;
    protected String clickId;
    protected boolean isVip;
//    protected String userAgent;
//    protected String deviceUniqueId;
    protected boolean isAuthorizedMail;  // Indicate that the user activate our authentication link after registration
    protected long authorizedMailRefusedTimes;
    protected Date timeFirstAuthorized;
//    protected String httpReferer;
    protected String dfaPlacementId;
    protected String dfaCreativeId;
    protected String dfaMacro;

	//use to comper last login date with now to know if to show banner
	protected Date timeLastLoginFromDb;

//	protected boolean isDecline;
	protected boolean isContactForDaa;
	protected Date lastDeclineDate;
	protected String lpSessionKey;    // livePerson session key (when chat is opened)

    protected boolean isRisky;
    protected SpecialCare specialCare;
    protected String background;
    protected String tip;
    protected long tag;
//    protected long statusId;
    protected long rankId;
    protected String ucSearch;
    private String decodedHttpRefferer;
//    protected String cityLongitude; // for globe LIVE AO
//    protected String cityLatitude; // for globe LIVE AO
//    protected String cityFromGoogle; // get city from google maps API
//    protected boolean isStopReverseWithdrawOption;
    protected boolean isImmediateWithdraw;
    boolean unique = true;
    protected String mId;
    protected String lastRepChat;
    private int platformId;
   // protected boolean hasBubbles;
    
    private Long sentDepDocumentsWriterId;
    private Date sentDepDocumentsDateTime;

	private static final long serialVersionUID = -5341306833973994617L;

	protected boolean isUserDisable;
	protected boolean bonusAbuser = false;
	public UserRegulationBase userRegulation;
	protected int vipStatusId;

	public boolean getPositiveTax() {
    	if (taxBalance >= 0) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    public boolean getNoZeroTax() {
    	if (taxBalance > 0) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

//    public String getStreetNo() {
//    	if (null == streetNo) {
//    		return "";
//    	}
//		return streetNo;
//	}
//	public void setStreetNo(String streetNo) {
//		this.streetNo = streetNo;
//	}
	public String getBalanceTxt() throws SQLException {
		return CommonUtil.displayAmount(balance, currencyId.intValue());
	}
    public String getTaxBalanceTxt() throws SQLException {
		return CommonUtil.displayAmount(taxBalance, currencyId.intValue());
	}

    /*
    public String getLandLinePhonePrefix() {
    	if (landLinePhone==null || landLinePhone.equals(""))
    		return "";

    	return landLinePhone.substring(0,3);
    }
    public String getLandLinePhoneSuffix() {
    	if (landLinePhone==null || landLinePhone.equals(""))
    		return "";

    	if (landLinePhone.length()>7)
    		return landLinePhone.substring(3);

    	return landLinePhone;
    }
    public String getMobilePhonePrefix() {
    	if (mobilePhone==null || mobilePhone.equals(""))
    		return "";

    	return mobilePhone.substring(0,3);
    }

    public String getMobilePhoneSuffix() {
    	if (mobilePhone==null || mobilePhone.equals(""))
    		return "";

    	if (mobilePhone.length()>7)
    		return mobilePhone.substring(3);

    	return mobilePhone;
    }

    public void setLandLinePhonePrefix(String s) {
    	landLinePhone=s+getLandLinePhoneSuffix();
    }
    public void setLandLinePhoneSuffix(String s) {
    	landLinePhone=getLandLinePhonePrefix()+s;
    }
    public void setMobilePhonePrefix(String s) {
    	mobilePhone=s+getMobilePhoneSuffix();
    }
    public void setMobilePhoneSuffix(String s) {
    	mobilePhone=getMobilePhonePrefix()+s;
    }
*/

//    public String getClassId() {
//		return Long.toString(classId);
//	}
//
//	public void setClassId(String classId) {
//		if (classId != null) {
//			this.classId = Long.parseLong(classId);
//		}
//	}

//	/**
//	 * @return the balance
//	 */
//	public long getBalance() {
//		return balance;
//	}
//	/**
//	 * @param balance the balance to set
//	 */
//	public void setBalance(long balance) {
//		this.balance = balance;
//	}
//	/**
//	 * @return the taxBalance
//	 */
//	public long getTaxBalance() {
//		return taxBalance;
//	}
//	/**
//	 * @param taxBalance the taxBalance to set
//	 */
//	public void setTaxBalance(long taxBalance) {
//		this.taxBalance = taxBalance;
//	}

//	/**
//	 * @return the currencyId
//	 */
//	public Long getCurrencyId() {
//		return currencyId;
//	}
//	/**
//	 * @param currencyId the currencyId to set
//	 */
//	public void setCurrencyId(Long currencyId) {
//
//		if (currencyId!=null)
//			this.currencyId = currencyId;
//	}
//	/**
//	 * @return the firstName
//	 */
//	public String getFirstName() {
//		return firstName;
//	}
	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public long getDeposits() {
		return deposits;
	}

	public void setDeposits(long deposits) {
		this.deposits = deposits;
	}

	public boolean getIsOpenIssue() {
		return isOpenIssue;
	}

	public void setIsOpenIssue(boolean openIssue) {
		this.isOpenIssue = openIssue;
	}

	public long getPendingWithdrawals() {
		return pendingWithdrawals;
	}

	public void setPendingWithdrawals(long pendingWithdrawals) {
		this.pendingWithdrawals = pendingWithdrawals;
	}

	public long getUserValue() {
		return userValue;
	}

	public void setUserValue(long userValue) {
		this.userValue = userValue;
	}

	public String getWithdrawalsTxt() {
		return CommonUtil.displayAmount(withdrawals, currencyId.intValue());
	}
	public String getPendingWithdrawalsTxt() {
		return CommonUtil.displayAmount(pendingWithdrawals, currencyId.intValue());
	}
	public String getUserValueTxt() {
		return CommonUtil.displayAmount(userValue, currencyId.intValue());
	}
	public String getDepositsTxt() {
		return CommonUtil.displayAmount(deposits, currencyId.intValue());
	}

	public long getWithdrawals() {
		return withdrawals;
	}

	public void setWithdrawals(long withdrawals) {
		this.withdrawals = withdrawals;
	}
//	/**
//	 * @param firstName the firstName to set
//	 */
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//	/**
//	 * @return the lastName
//	 */
//	public String getLastName() {
//		return lastName;
//	}
//	/**
//	 * @param lastName the lastName to set
//	 */
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//	/**
//	 * @return the street
//	 */
//	public String getStreet() {
//		return street;
//	}
//	/**
//	 * @param street the street to set
//	 */
//	public void setStreet(String street) {
//		this.street = street;
//	}
//	/**
//	 * @return the cityId
//	 */
//
//	public long getCityId() {
//		return cityId;
//	}
//	public void setCityId(long id) {
//
//		this.cityId = id;
//	}
//	/**
//	 * @return the zipCode
//	 */
//	public String getZipCode() {
//		if (zipCode == null) {
//			return "";
//		}
//		return zipCode;
//	}
//	/**
//	 * @param zipCode the zipCode to set
//	 */
//	public void setZipCode(String zipCode) {
//		this.zipCode = zipCode;
//	}
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
	/**
	 * @return the timeLastLogin
	 */
	public Date getTimeLastLogin() {
		return timeLastLogin;
	}
	/**
	 * @param timeLastLogin the timeLastLogin to set
	 */
	public void setTimeLastLogin(Date timeLastLogin) {
		this.timeLastLogin = timeLastLogin;
	}

//	/**
//	 * @return the email
//	 */
//	public String getEmail() {
//		return email;
//	}
//	/**
//	 * @param email the email to set
//	 */
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	/**
//	 * @return the combinationId
//	 */
//	public long getCombinationId() {
//		return combinationId;
//	}
//	/**
//	 * @param combinationId the combinationId to set
//	 */
//	public void setCombinationId(long combinationId) {
//		if (combinationId != 0) {
//			this.combinationId = combinationId;
//		}
//	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
//	/**
//	 * @return the timeBirthDate
//	 */
//	public Date getTimeBirthDate() {
//		return timeBirthDate;
//	}
	@Override
	public String getTimeBirthDateTxt() {
		return CommonUtil.getDateFormat(timeBirthDate);
	}
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated);
	}
	public String getTimeLastLoginTxt() {
		return CommonUtil.getDateFormat(timeLastLogin);
	}
	public String getTimeCreatedWithHoursTxt() {
		return CommonUtil.getDateAndTimeFormat(timeCreated);
	}
	public String getTimeCreatedWithHoursTxtOffset() {
		return CommonUtil.getDateAndTimeFormat(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
	}
	public String getTimeLastLoginWithHoursTxt() {
		return CommonUtil.getDateAndTimeFormat(timeLastLogin);
	}
	public String getTimeLastLoginWithHoursTxtOffset() {
		return CommonUtil.getDateAndTimeFormat(timeLastLogin, CommonUtil.getUtcOffset(utcOffset));
	}
	public String getTimeModifiedTxt() {
		return CommonUtil.getDateFormat(timeModified);
	}
	/**
	 * @param timeBirthDate the timeBirthDate to set
	 */
	@Override
	public void setTimeBirthDate(Date timeBirthDate) {
		this.timeBirthDate = timeBirthDate;
		if (timeBirthDate != null) {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(timeBirthDate);
			birthYear = String.valueOf(c.get(GregorianCalendar.YEAR));
			birthMonth = String.valueOf(c.get(GregorianCalendar.MONTH)+1);
			if (birthMonth.length() == 1) {
				birthMonth = "0"+birthMonth;
			}
			birthDay = String.valueOf(c.get(GregorianCalendar.DAY_OF_MONTH));
			if (birthDay.length() == 1) {
				birthDay = "0"+birthDay;
			}
		}
	}
	/**
	 * @return the isContactByEmail
	 */
	public boolean getContactByEmail() {
		return (isContactByEmail == 1);
	}

	public void setContactByEmail(boolean c) {
		if (c == false) {
			isContactByEmail=0;
		}
		else {
			isContactByEmail=1;
		}
	}

	@Override
	public String getGenderTxt() {

		if (gender == null || gender.equals("")) {
			return "register.gender.none";
		}

		if (gender.equals("M")) {
			return "register.gender.male";
		}
		else {
			return "register.gender.female";
		}

	}

	public String getGenderForTemplateEnHeUse() {
		if (gender.equals("M")) {
			return "template.gender.male";
		} else {
			return "template.gender.female";
		}
	}

	public String getGenderForTemplateUse() {
		if (!CommonUtil.isParameterEmptyOrNull(gender)) {
			if (gender.equals("M")) {
				return "template.des.gender.male";
			} else {
				return "template.des.gender.female";
			}
		} else {
			return "template.des.gender.male";
		}
	}

	public String getIsActiveTxt() {
		if (isActive == null) {
			return "";
		}

		if (isActive.equals("1")) {
			return "yes";
		}
		else {
			return "no";
		}
	}

//	public boolean isActive() {
//		if (isActive == null) {
//			return false;
//		}
//
//		if (isActive.equals("1")) {
//			return true;
//		}
//		else {
//			return false;
//		}
//	}

	public boolean isTestUser() {
//		if (classId == null) {
//			return true;
//		}

//		if (classId.equals("0")) {
		if (classId == ConstantsBase.USER_CLASS_TEST) {
			return true;
		}
		else {
			return false;
		}
	}
	public String getIsOpenIssueTxt() {

		if (isOpenIssue == true) {
			return "yes";
		}
		else {
			return "no";
		}
	}
	public String getTaxExemptionTxt() {

		if (taxExemption) {
			return "yes";
		}
		else {
			return "no";
		}
	}
//	/**
//	 * @return the isContactByEmail
//	 */
//	public int getIsContactByEmail() {
//		return isContactByEmail;
//	}
//	/**
//	 * @param isContactByEmail the isContactByEmail to set
//	 */
//	public void setIsContactByEmail(int isContactByEmail) {
//		this.isContactByEmail = isContactByEmail;
//	}

//	/**
//	 * @return the mobilePhone
//	 */
//	public String getMobilePhone() {
//		return mobilePhone;
//	}
//	/**
//	 * @param mobilePhone the mobilePhone to set
//	 */
//	public void setMobilePhone(String mobilePhone) {
//		this.mobilePhone = mobilePhone;
//		if (mobilePhone!=null && mobilePhone.length()>3) {
//			mobilePhonePrefix=mobilePhone.substring(0,3);
//			mobilePhoneSuffix=mobilePhone.substring(3);
//		}
//	}
//	/**
//	 * @return the landLinePhone
//	 */
//	public String getLandLinePhone() {
//		return landLinePhone;
//	}
//	/**
//	 * @param landLinePhone the landLinePhone to set
//	 */
//	public void setLandLinePhone(String landLinePhone) {
//		this.landLinePhone = landLinePhone;
//		if (landLinePhone!=null && landLinePhone.length()>3 ){
//			if(landLinePhone.length() == 9 && skinId == Skin.SKIN_ETRADER){
//				landLinePhonePrefix=landLinePhone.substring(0,2);
//				landLinePhoneSuffix=landLinePhone.substring(2);
//			} else {
//				landLinePhonePrefix=landLinePhone.substring(0,3);
//				landLinePhoneSuffix=landLinePhone.substring(3);
//			}
//		}
//	}

//	/**
//	 * @return the gender
//	 */
//	public String getGender() {
//		return gender;
//	}
//	/**
//	 * @param gender the gender to set
//	 */
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//	/**
//	 * @return the idNum
//	 */
//	public String getIdNum() {
//		return idNum;
//	}
//	/**
//	 * @param idNum the idNum to set
//	 */
//	public void setIdNum(String idNum) {
//		this.idNum = idNum;
//	}

	/**
	 * @param limitId the limitId to set
	 
	public void setLimitId(String limitId) {

		if (limitId != null) {
			this.limitId = limitId;
		}
	}*/

	/**
	 * Return limitId long value
	 * @return
	 */
	public Long getLimitIdLongValue() {
		return Long.valueOf(limitId);
	}

	public Date getTimeModified() {
		return timeModified;
	}

	@Override
	public void setCityName(String name) {
		long cityIdTemp;
		try {
			cityIdTemp = AdminManagerBase.getCityIdByName(name);
			if ( cityIdTemp > 0 ) {
				cityId = cityIdTemp;
			}
		} catch (SQLException e) {
			logger.error("Can't get city id", e);
			throw new IllegalArgumentException(e);
		}
		this.cityName = name;
	}

	@Override
	public String getCityName() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	if (null != context){
			ApplicationDataBase a = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			String n = ApplicationDataBase.getCities().get(String.valueOf(cityId));

	    	if ( n != null && CommonUtil.isHebrewSkin(a.getSkinId())){
				return n;
			}
    	}
    	return cityName;
    }

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getBirthDay() {

		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		if (birthDay != null) {
			this.birthDay = birthDay;
		}
	}
	public String getBirthMonth() {
		return birthMonth;
	}
	public void setBirthMonth(String birthMonth) {
		if (birthMonth != null) {
			this.birthMonth = birthMonth;
		}
	}
	public String getBirthYear() {

		return birthYear;

	}



	public void setBirthYear(String birthYear) {
		if (birthYear != null) {
			this.birthYear = birthYear;
		}
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public BigDecimal getCompanyId() {
		return companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public boolean isTaxExemption() {
		return taxExemption;
	}

	public BigDecimal getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(BigDecimal referenceId) {
		this.referenceId = referenceId;
	}

	public void setTaxExemption(boolean taxExemption) {
		this.taxExemption = taxExemption;
	}
	public boolean isClassCompany() {
//		if (classId == null || classId.equals("")) {
//			return false;
//		}
		if (classId == (long)ConstantsBase.USER_CLASS_COMPANY) {
			return true;
		} else {
			return false;
		}

	}



	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "UserBase ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "balance = " + this.balance + TAB
	        + "taxBalance = " + this.taxBalance + TAB
	        + "userName = " + this.userName + TAB
	        + "password = " + "*****" + TAB
	        + "currencyId = " + this.currencyId + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "timeLastLogin = " + this.timeLastLogin + TAB
	        + "timeModified = " + this.timeModified + TAB
	        + "isActive = " + this.isActive + TAB
	        + "email = " + this.email + TAB
	        + "combinationId = " + this.combinationId + TAB
	        + "comments = " + this.comments + TAB
	        + "ip = " + this.ip + TAB
	        + "timeBirthDate = " + this.timeBirthDate + TAB
	        + "isContactByEmail = " + this.isContactByEmail + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "landLinePhone = " + this.landLinePhone + TAB
	        + "gender = " + this.gender + TAB
	        + "idNum = " + this.idNum + TAB
	        + "limitId = " + this.limitId + TAB
	        + "classId = " + this.classId + TAB
	        + "keyword = " + this.keyword + TAB
	        + "lastFailedTime = " + this.lastFailedTime + TAB
	        + "failedCount = " + this.failedCount + TAB
	        + "companyName = " + this.companyName + TAB
	        + "companyId = " + this.companyId + TAB
	        + "taxExemption = " + this.taxExemption + TAB
	        + "referenceId = " + this.referenceId + TAB
	        + "password2 = " + "*****" + TAB
	        + "campaignName = " + this.campaignName + TAB
	        + "withdrawals = " + this.withdrawals + TAB
	        + "pendingWithdrawals = " + this.pendingWithdrawals + TAB
	        + "deposits = " + this.deposits + TAB
	        + "userValue = " + this.userValue + TAB
	        + "isOpenIssue = " + this.isOpenIssue + TAB
	        + "birthYear = " + this.birthYear + TAB
	        + "birthMonth = " + this.birthMonth + TAB
	        + "birthDay = " + this.birthDay + TAB
	        + "cityName = " + this.cityName + TAB
	        + "roles = " + this.roles + TAB
            + "isNextInvestOnUs = " + this.isNextInvestOnUs + TAB
            + "winLoseHouse = " + this.winLoseHouse + TAB
            + "skinId = " + this.skinId + TAB
            + "countryId = " + this.countryId + TAB
            + "languageId = " + this.languageId + TAB
            + "timeFirstVisit = " + this.timeFirstVisit + TAB
            + "isVip = " + this.isVip + TAB
            + "isRisky = " + this.isRisky + TAB
            + "platformId = " + this.platformId + TAB
	        + " )";

	    return retValue;
	}

//	public String getLandLinePhonePrefix() {
//		return landLinePhonePrefix;
//	}

//	public void setLandLinePhonePrefix(String landLinePhonePrefix) {
//		this.landLinePhonePrefix = landLinePhonePrefix;
//	}

//	public String getLandLinePhoneSuffix() {
//		return landLinePhoneSuffix;
//	}

//	public void setLandLinePhoneSuffix(String landLinePhoneSuffix) {
//		this.landLinePhoneSuffix = landLinePhoneSuffix;
//	}

//	public String getMobilePhonePrefix() {
//		return mobilePhonePrefix;
//	}
//
//	public void setMobilePhonePrefix(String mobilePhonePrefix) {
//		this.mobilePhonePrefix = mobilePhonePrefix;
//	}

//	public String getMobilePhoneSuffix() {
//		return mobilePhoneSuffix;
//	}

//	public void setMobilePhoneSuffix(String mobilePhoneSuffix) {
//		this.mobilePhoneSuffix = mobilePhoneSuffix;
//	}

    public int getIsNextInvestOnUs() {
        return isNextInvestOnUs;
    }

    public void setIsNextInvestOnUs(int isNextInvestOnUs) {
        this.isNextInvestOnUs = isNextInvestOnUs;
    }

//	/**
//	 * @return the countryId
//	 */
//	public long getCountryId() {
//		return countryId;
//	}

//	/**
//	 * @param countryId the countryId to set
//	 */
//	public void setCountryId(long countryId) {
//		this.countryId = countryId;
//	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getSkinName() {
		return skinName;
	}

	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the utcOffsetModified
	 */
	public String getUtcOffsetModified() {
		return utcOffsetModified;
	}

	/**
	 * @param utcOffsetModified the utcOffsetModified to set
	 */
	public void setUtcOffsetModified(String utcOffsetModified) {
		this.utcOffsetModified = utcOffsetModified;
	}

//	public Currency getCurrency() {
//		return currency;
//	}
//
//	public void setCurrency(Currency currency) {
//		this.currency = currency;
//	}

	/**
	 * @return the skin
	 */
	public Skins getSkin() {
		return skin;
	}

	/**
	 * @param skin the skin to set
	 */
	public void setSkin(Skins skin) {
		this.skin = skin;
	}

//	public boolean isContactBySMS() {
//		return (isContactBySMS==1);
//	}

//	/**
//	 * @return the isContactBySMS
//	 */
//	public long getIsContactBySMS() {
//		return isContactBySMS;
//	}

//	/**
//	 * @param isContactBySMS the isContactBySMS to set
//	 */
//	public void setIsContactBySMS(long isContactBySMS) {
//		this.isContactBySMS = isContactBySMS;
//	}

	/**
	 * @return the idDocVerify
	 */
	public boolean isIdDocVerify() {
		return idDocVerify;
	}

	/**
	 * @param idDocVerify the idDocVerify to set
	 */
	public void setIdDocVerify(boolean idDocVerify) {
		this.idDocVerify = idDocVerify;
	}

//	/**
//	 * @return the utcOffset
//	 */
//	public String getUtcOffset() {
//		return utcOffset;
//	}

//	/**
//	 * @param utcOffset the utcOffset to set
//	 */
//	public void setUtcOffset(String utcOffset) {
//		this.utcOffset = utcOffset;
//	}

	public boolean isEtrader() {
		return getSkinId() == Skin.SKIN_ETRADER;
	}
//
//	/**
//	 * @return the state
//	 */
//	public Long getState() {
//		return state;
//	}

//	/**
//	 * @param state the state to set
//	 */
//	public void setState(Long state) {
//		this.state = state;
//	}

//	/**
//	 * @return the docsRequired
//	 */
//	public boolean isDocsRequired() {
//		return docsRequired;
//	}

//	/**
//	 * @param docsRequired the docsRequired to set
//	 */
//	public void setDocsRequired(boolean docsRequired) {
//		this.docsRequired = docsRequired;
//	}

//	/**
//	 * @return the dynamicParam
//	 */
//	public String getDynamicParam() {
//		return dynamicParam;
//	}

//	/**
//	 * @param dynamicParam the dynamicParam to set
//	 */
//	public void setDynamicParam(String dynamicParam) {
//		this.dynamicParam = dynamicParam;
//	}

	public String getSpecialCode() {
		return specialCode;
	}

	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}

//	public long getContactId() {
//		return contactId;
//	}

//	public void setContactId(long contactId) {
//		this.contactId = contactId;
//	}

	public Locale getValidatorLocale() {
		if (CommonUtil.isHebrewSkin(skinId)){
			return new Locale(ConstantsBase.ETRADER_LOCALE);
		} else if(skinId == Skin.SKIN_TURKISH){
			return new Locale(ConstantsBase.TURKISH_LOCALE);
		} else if(skinId == Skin.SKIN_SPAIN){
			return new Locale(ConstantsBase.SPANISH_LOCALE);
		} else if(skinId == Skin.SKIN_ARABIC){
			return new Locale(ConstantsBase.ARABIC_LOCALE);
		} else if (skinId == Skin.SKIN_GERMAN) {
			return new Locale(ConstantsBase.GERMAN_LOCALE);
		} else if (skinId == Skin.SKIN_RUSSIAN) {
			return new Locale(ConstantsBase.RUSSIAN_LOCALE);
		} else if (skinId == Skin.SKIN_ITALIAN) {
			return new Locale(ConstantsBase.ITALIAN_LOCALE);
		} else if (skinId == Skin.SKIN_CHINESE) {
            return new Locale(ConstantsBase.CHINESE_LOCALE);
        } else if (skinId == Skin.SKIN_FRANCE) {
            return new Locale(ConstantsBase.FRENCH_LOCALE);
        }
		return new Locale(ConstantsBase.LOCALE_DEFAULT);
	}

//	/**
//	 * @return the isContactByPhone
//	 */
//	public boolean isContactByPhone() {
//		return (isContactByPhone==1);
//	}

//	/**
//	 * @return the isContactByPhone
//	 */
//	public long getIsContactByPhone() {
//		return isContactByPhone;
//	}

//	/**
//	 * @param isContactByPhone the isContactByPhone to set
//	 */
//	public void setIsContactByPhone(long isContactByPhone) {
//		this.isContactByPhone = isContactByPhone;
//	}

	/**
	 * @return the isFalseAccount
	 */
	public boolean isFalseAccount() {
		return isFalseAccount;
	}

	/**
	 * @param isFalseAccount the isFalseAccount to set
	 */
	public void setFalseAccount(boolean isFalseAccount) {
		this.isFalseAccount = isFalseAccount;
	}

	public String getContactByPhoneTxt() {
		if (1 == isContactByPhone) {
			return "yes";
		}
		else {
			return "no";
		}
	}

//	/**
//	 * @return the isAcceptedTerms
//	 */
//	public boolean isAcceptedTerms() {
//		return isAcceptedTerms;
//	}

//	/**
//	 * @param isAcceptedTerms the isAcceptedTerms to set
//	 */
//	public void setAcceptedTerms(boolean isAcceptedTerms) {
//		this.isAcceptedTerms = isAcceptedTerms;
//	}

	/**
	 * @return the tierUser
	 */
	public TierUser getTierUser() {
		return tierUser;
	}

	/**
	 * @param tierUser the tierUser to set
	 */
	public void setTierUser(TierUser tierUser) {
		this.tierUser = tierUser;
	}

	/**
	 * can convert from points to cash allow
	 * @return
	 */
	public boolean isPointsConvertAllow() {
		if (null != tierUser) {
			if (TiersManagerBase.isPointsConvertAllow(tierUser)) {
				return true;
			}
		}
		return false;

	}

 	/**
 	 * is user qualified to some tier
 	 * @return
 	 */
 	public boolean isInTier() {
 		if (null != tierUser && tierUser.getId() > 0) {
 			return true;
 		}
 		return false;
 	}

 	/**
 	 * Get loyalty convert benefit percent
 	 * @return
 	 */
 	public long getLoyConvertBenefitPercent() {
 		long perc = 0;
 		if (null != tierUser && tierUser.getTierId() != 0) {
 			perc = TiersManagerBase.getTierById(tierUser.getTierId()).getConvertBenefitPercent();
 		}
 		return perc;
 	}

 	/**
 	 * Get user currency symbol
 	 * @return
 	 */
 	@Override
	public String getCurrencySymbol() {
 		String currencySymbol = CommonUtil.getCurrencySymbol(currencyId);
		return CommonUtil.getMessage(currencySymbol, null);
 	}

 	/**
 	 * Get accumulated points of the user in the tier from his
 	 * qualification time
 	 * @return
 	 * @throws SQLException
 	 */
 	public long getAccumulatedPoints() throws SQLException {
 		return TiersManagerBase.getAccumulatedPoints(id, tierUser.getQualificationTime());
 	}

 	/**
 	 * Is user in heigher level tier
 	 * @return
 	 */
 	public boolean isHigherLvlTier() {
 		Skins s = ApplicationDataBase.getSkinById(getSkinId());
 		return s.isHigherLvlTier(tierUser.getTierId());
 	}

	/**
	 * Get points / msg for the next level
	 * @return
	 * @throws SQLException
	 */
	public String getPointsToNextLevelTxt() throws SQLException {
		String pointsNextLevel = "";
		Skins skinWithTiersLevels = ApplicationDataBase.getSkinById(getSkinId());
		long currentLevelTier = skinWithTiersLevels.getTiersLevels().get(new Long(tierUser.getTierId()));
		long nextTierId =  skinWithTiersLevels.getTierIdByLevel(currentLevelTier + 1);
		Tier nextTier = TiersManagerBase.getTierById(nextTierId);
		long pointsToNextLvl = nextTier.getQualificationPoints() - getAccumulatedPoints();
		if (pointsToNextLvl > 0) {
			pointsNextLevel = String.valueOf(pointsToNextLvl);
		} else {
			pointsNextLevel = CommonUtil.getMessage("converted.amount.from.points.waiting", null);
		}
		return pointsNextLevel;
	}

	/**
	 * Get needed points for maintenance
	 * @return
	 * @throws SQLException
	 */
	public String getMaintenancePointsTxt() throws SQLException {
		String msg = "";
		Tier t = TiersManagerBase.getTierById(tierUser.getTierId());
		long points = t.getMaintenancePoints() - getAccumulatedPoints();
		if (points > 0) {
			msg = String.valueOf(points);
		} else {
			msg = "0";
		}
		return msg;
	}

 	/**
 	 * Get upgrade to the next level message
 	 * @return
 	 */
 	public String getTierUpgrateMsg() {
 		Skins s = ApplicationDataBase.getSkinById(getSkinId());
 		long nextTierId = s.getNextLvlTierId(tierUser.getTierId());
 		String tierName = TiersManagerBase.getTierById(nextTierId).getDescription();
 		String[] params = new String[1];
 		params[0] = CommonUtil.getMessage(tierName, null);
 		return CommonUtil.getMessage("tier.points.info.tier.points.upgrade", params);
 	}

 	/**
 	 * Get number of days that left for upgrade to next tier
 	 * and it also used for maintenance in the current tier
 	 * @return
 	 */
	public long getDaysToUpgrade() {
		Calendar upgradeC = Calendar.getInstance();
		upgradeC.setTime(tierUser.getQualificationTime());
		Skins s = ApplicationDataBase.getSkinById(skinId);
		upgradeC.add(Calendar.DAY_OF_MONTH, ((int)s.getTierQualificationPeriod())); ;
		double nowMS = Calendar.getInstance().getTimeInMillis();
		double upgradeMS = upgradeC.getTimeInMillis();
		double days = ((upgradeMS - nowMS) / (1000 * 60 * 60 * 24)) ;
		return Math.round(days);
	}

	/**
	 * return upgrade message
	 * @return
	 */
	public String getDaysMsg() {
		long days = getDaysToUpgrade();
		String msg = "";
		String[] params = new String[1];
		params[0] = String.valueOf(days);
		if (isHigherLvlTier()) {
			msg = "tier.points.info.tier.stay.days";
		} else if (tierUser.getTierId() == TiersManagerBase.TIER_LEVEL_BLUE) {
			msg = "tier.points.info.tier.upgrade.days";
		} else {
			msg = "tier.points.info.tier.upgrade.stay.days";
		}
		return CommonUtil.getMessage(msg, params);
	}

//	/**
//	 * @return the timeFirstVisit
//	 */
//	public Date getTimeFirstVisit() {
//		return timeFirstVisit;
//	}

//	/**
//	 * @param timeFirstVisit the timeFirstVisit to set
//	 */
//	public void setTimeFirstVisit(Date timeFirstVisit) {
//		this.timeFirstVisit = timeFirstVisit;
//	}

	/**
	 * get userName for calcalistGame without _cg suffix
	 * @return
	 */
	public String getUserNameCalGame() {
		String userName = getUserName();
		return userName.substring(0, userName.length()-3);
	}

	/**
	 * @return the timeLastLoginFromDb
	 */
	public Date getTimeLastLoginFromDb() {
		return timeLastLoginFromDb;
	}

	/**
	 * @param timeLastLoginFromDb the timeLastLoginFromDb to set
	 */
	public void setTimeLastLoginFromDb(Date timeLastLoginFromDb) {
		this.timeLastLoginFromDb = timeLastLoginFromDb;
	}

	/**
	 * rest the date to now to not show the banner again
	 *
	 */
	public String getTimeLastLoginFromDbReset() {
		this.timeLastLoginFromDb = new Date();
		return "";
	}

	public boolean isCalGameUsername(){
		String usernameSuffix = userName.substring(userName.length()-ConstantsBase.CAL_GAME_USER_NAME_SUFFIX.length());

		if (usernameSuffix.equals(ConstantsBase.CAL_GAME_USER_NAME_SUFFIX.toUpperCase())){
			return true;
		}
		return false;
	}

	/**
	 * @return the isContactForDaa
	 */
	public boolean isContactForDaa() {
		return isContactForDaa;
	}

	/**
	 * @param isContactForDaa the isContactForDaa to set
	 */
	public void setContactForDaa(boolean isContactForDaa) {
		this.isContactForDaa = isContactForDaa;
	}

//	/**
//	 * @return the isDecline
//	 */
//	public boolean isDecline() {
//		return isDecline;
//	}

//	/**
//	 * @param isDecline the isDecline to set
//	 */
//	public void setDecline(boolean isDecline) {
//		this.isDecline = isDecline;
//	}

	/**
	 * @return the lastDecline
	 */
	public Date getLastDeclineDate() {
		return lastDeclineDate;
	}

	/**
	 * @param lastDecline the lastDecline to set
	 */
	public void setLastDeclineDate(Date lastDeclineDate) {
		this.lastDeclineDate = lastDeclineDate;
	}

	public void setCityNameForJob(String name) throws SQLException {
		this.cityName = name;
	}

	/**
	 * @return the affiliateKey
	 */
	public long getAffiliateKey() {
		return affiliateKey;
	}

	/**
	 * @param affiliateKey the affiliateKey to set
	 */
	public void setAffiliateKey(long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

	/**
	 * @return the chatId
	 */
	public long getChatId() {
		return chatId;
	}

	/**
	 * @param chatId the chatId to set
	 */
	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	/* Merged code */
	// /**
	// * @return the writerId
	// */
	// public long getWriterId() {
	// return writerId;
	// }
	//
	// /**
	// * @param writerId the writerId to set
	// */
	// public void setWriterId(long writerId) {
	// this.writerId = writerId;
	// }

	/**
	 * @return the lpSessionKey
	 */
	public String getLpSessionKey() {
		return lpSessionKey;
	}

	/**
	 * @param lpSessionKey the lpSessionKey to set
	 */
	public void setLpSessionKey(String lpSessionKey) {
		this.lpSessionKey = lpSessionKey;
	}

	public String getCampaignForStrip() {
		return campaignForStrip;
	}

	public void setCampaignForStrip(String campaignForStrip) {
		this.campaignForStrip = campaignForStrip;
	}

	/**
	 * @return the clickId
	 */
	public String getClickId() {
		return clickId;
	}

	/**
	 * @param clickId the clickId to set
	 */
	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

	public boolean isVip() {
		return isVip;
	}

	public void setVip(boolean isVip) {
		this.isVip = isVip;
	}

	public String getIsVipTxt() {
		return (isVip ? "yes" : "no");
	}

//	public String getUserAgent() {
//		return userAgent;
//	}

//	public void setUserAgent(String userAgent) {
//		this.userAgent = userAgent;
//	}

//	public String getDeviceUniqueId() {
//		return deviceUniqueId;
//	}

//	public void setDeviceUniqueId(String deviceUniqueId) {
//		this.deviceUniqueId = deviceUniqueId;
//	}

	public boolean isAuthorizedMail() {
		return isAuthorizedMail;
	}

	public void setAuthorizedMail(boolean isAuthorizedMail) {
		this.isAuthorizedMail = isAuthorizedMail;
	}

	/**
	 * @return the authorizedMailRefusedTimes
	 */
	public long getAuthorizedMailRefusedTimes() {
		return authorizedMailRefusedTimes;
	}

	/**
	 * @param authorizedMailRefusedTimes the authorizedMailRefusedTimes to set
	 */
	public void setAuthorizedMailRefusedTimes(long authorizedMailRefusedTimes) {
		this.authorizedMailRefusedTimes = authorizedMailRefusedTimes;
	}

	/**
	 * @return the timeFirstAuthorized
	 */
	public Date getTimeFirstAuthorized() {
		return timeFirstAuthorized;
	}

	/**
	 * @param timeFirstAuthorized the timeFirstAuthorized to set
	 */
	public void setTimeFirstAuthorized(Date timeFirstAuthorized) {
		this.timeFirstAuthorized = timeFirstAuthorized;
	}

	/**
	 * return true if it's an mobile user
	 * @return
	 */
	public boolean isMobileUser() {
		if (writerId == Writer.WRITER_ID_MOBILE &&
				null != deviceUniqueId &&
				null != userAgent) {
			return true;
		}
		return false;
	}

	/**
	 * Return the mobile type
	 * For now it's just iphone / android
	 * @return
	 */
	public String getMobileType() {
		return getMobileType(userAgent);
	}

	public String getMobileType(String userAgent) {
		String type = "";
		if(userAgent != null){
			if (userAgent.indexOf("iphone") > -1) {
				type = ConstantsBase.MOBILE_DES_IPHONE;
			} else if (userAgent.indexOf("ipod") > -1) {
				type = ConstantsBase.MOBILE_DES_IPOD;
			} else if (userAgent.indexOf("android") > -1) {
				type = ConstantsBase.MOBILE_DES_ANDROID;
			} else {
				type = ConstantsBase.MOBILE_DES_OTHER;
			}
		} else {
			type = UsersManagerBase.getMobileDeviceOS(deviceUniqueId);
		}
		return type;
	}

	@Override
	public String getDeviceFamily(){
		String device = "";
		if(deviceFamily!= null){
			if (deviceFamily == 1){
				return CommonUtil.getMessage("user.device.tablet", null);
			}
			if(deviceFamily == 0){
				return CommonUtil.getMessage("user.device.mobile", null);
			}
		}
		return device;
	}

   /**
     * @return the isRisky
     */
    public boolean isRisky() {
        return isRisky;
    }

    /**
     * @param isRisky the isRisky to set
     */
    public void setRisky(boolean isRisky) {
        this.isRisky = isRisky;
    }

//	/**
//	 * @return the httpReferer
//	 */
//	public String getHttpReferer() {
//		return httpReferer;
//	}

//	/**
//	 * @param httpReferer the httpReferer to set
//	 */
//	public void setHttpReferer(String httpReferer) {
//		this.httpReferer = httpReferer;
//	}

	/**
	 * @return the dfaCreativeId
	 */
	public String getDfaCreativeId() {
		return dfaCreativeId;
	}

	/**
	 * @param dfaCreativeId the dfaCreativeId to set
	 */
	public void setDfaCreativeId(String dfaCreativeId) {
		this.dfaCreativeId = dfaCreativeId;
	}

	/**
	 * @return the dfaPlacementId
	 */
	public String getDfaPlacementId() {
		return dfaPlacementId;
	}

	/**
	 * @param dfaPlacementId the dfaPlacementId to set
	 */
	public void setDfaPlacementId(String dfaPlacementId) {
		this.dfaPlacementId = dfaPlacementId;
	}

	/**
	 * @return the dfaMacro
	 */
	public String getDfaMacro() {
		return dfaMacro;
	}

	/**
	 * @param dfaMacro the dfaMacro to set
	 */
	public void setDfaMacro(String dfaMacro) {
		this.dfaMacro = dfaMacro;
	}

    /**
     * @return the specialCare
     */
    public SpecialCare getSpecialCare() {
        return specialCare;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCare(SpecialCare specialCare) {
        this.specialCare = specialCare;
    }

	/**
	 * @return the background
	 */
	public String getBackground() {
		return background;
	}

	/**
	 * @param background the background to set
	 */
	public void setBackground(String background) {
		this.background = background;
	}

	/**
	 * @return the tip
	 */
	public String getTip() {
		return tip;
	}

	/**
	 * @param tip the tip to set
	 */
	public void setTip(String tip) {
		this.tip = tip;
	}

	/**
	 * @return the tag
	 */
	public long getTag() {
		return tag;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setTag(long tag) {
		this.tag = tag;
	}

//	/**
//	 * @return the statusId
//	 */
//	public long getStatusId() {
//		return statusId;
//	}
//
//	/**
//	 * @param statusId the statusId to set
//	 */
//	public void setStatusId(long statusId) {
//		this.statusId = statusId;
//	}

	public String getStatusTxt() {
		switch ((int)this.statusId) {
		case 1:
			return CommonUtil.getMessage("user.status.active", null);
		case 2:
			return CommonUtil.getMessage("user.status.sleeper", null);
		case 3:
			return CommonUtil.getMessage("user.status.coma", null);
		}
		return "";
	}

	/**
	 * @return the rankId
	 */
	public long getRankId() {
		return rankId;
	}

	/**
	 * @param rankId the rankId to set
	 */
	public void setRankId(long rankId) {
		this.rankId = rankId;
	}

	public String getRankTxt() {
		switch ((int)this.rankId) {
		case 1:
			return CommonUtil.getMessage("user.newbies", null);
		case 2:
			return CommonUtil.getMessage("user.regulars", null);
		case 3:
			return CommonUtil.getMessage("user.beginner.rollers", null);
		case 4:
			return CommonUtil.getMessage("user.medium.rollers", null);
		case 5:
			return CommonUtil.getMessage("user.high.rollers", null);
		case 6:
			return CommonUtil.getMessage("user.gold.rollers", null);
		case 7:
			return CommonUtil.getMessage("user.platinum.rollers", null);
		}
		return "";
	}

	public String getUcSearch() {
		return ucSearch;
	}

	public void setUcSearch(String ucSearch) {
		this.ucSearch = ucSearch;
	}

    public String getDecodedHttpRefferer() {
        return decodedHttpRefferer;
    }

    public void setDecodedHttpRefferer(String decodedHttpRefferer) {
        this.decodedHttpRefferer = decodedHttpRefferer;
    }

	public long getTotalWinLose() {
		return totalWinLose;
	}

	public void setTotalWinLose(long winLose) {
		this.totalWinLose = winLose;
	}

//	/**
//	 * @return the cityLatitude
//	 */
//	public String getCityLatitude() {
//		return cityLatitude;
//	}

//	/**
//	 * @param cityLatitude the cityLatitude to set
//	 */
//	public void setCityLatitude(String cityLatitude) {
//		this.cityLatitude = cityLatitude;
//	}

//	/**
//	 * @return the cityLongitude
//	 */
//	public String getCityLongitude() {
//		return cityLongitude;
//	}

//	/**
//	 * @param cityLongitude the cityLongitude to set
//	 */
//	public void setCityLongitude(String cityLongitude) {
//		this.cityLongitude = cityLongitude;
//	}

//	/**
//	 * @return the cityFromGoogle
//	 */
//	public String getCityFromGoogle() {
//		return cityFromGoogle;
//	}

//	/**
//	 * @param cityFromGoogle the cityFromGoogle to set
//	 */
//	public void setCityFromGoogle(String cityFromGoogle) {
//		this.cityFromGoogle = cityFromGoogle;
//	}

//	/**
//	 * @return the isStopReverseWithdrawOption
//	 */
//	public boolean isStopReverseWithdrawOption() {
//		return isStopReverseWithdrawOption;
//	}

//	/**
//	 * @param isStopReverseWithdrawOption the isStopReverseWithdrawOption to set
//	 */
//	public void setStopReverseWithdrawOption(boolean isStopReverseWithdrawOption) {
//		this.isStopReverseWithdrawOption = isStopReverseWithdrawOption;
//	}

	public long getWinLoseHouse() {
		return winLoseHouse;
	}

	public void setWinLoseHouse(long winLoseHouse) {
		winLoseHouse = -winLoseHouse;
		this.winLoseHouse = winLoseHouse;
	}

	/**
	 * @return the isImmediateWithdraw
	 */
	public boolean isImmediateWithdraw() {
		return isImmediateWithdraw;
	}

	/**
	 * @param isImmediateWithdraw the isImmediateWithdraw to set
	 */
	public void setImmediateWithdraw(boolean isImmediateWithdraw) {
		this.isImmediateWithdraw = isImmediateWithdraw;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	/**
	 * @return the isUserDisable
	 */
	public boolean isUserDisable() {
		return isUserDisable;
	}

	/**
	 * @param isUserDisable the isUserDisable to set
	 */
	public void setUserDisable(boolean isUserDisable) {
		this.isUserDisable = isUserDisable;
	}

    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }

	/**
	 * @return the lastRepChat
	 */
	public String getLastRepChat() {
		return lastRepChat;
	}

	/**
	 * @param lastRepChat the lastRepChat to set
	 */
	public void setLastRepChat(String lastRepChat) {
		this.lastRepChat = lastRepChat;
	}

	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	} 

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	public Long getSentDepDocumentsWriterId() {
		return sentDepDocumentsWriterId;
	}

	public void setSentDepDocumentsWriterId(Long sentDepDocumentsWriterId) {
		this.sentDepDocumentsWriterId = sentDepDocumentsWriterId;
	}

	public Date getSentDepDocumentsDateTime() {
		return sentDepDocumentsDateTime;
	}

	public void setSentDepDocumentsDateTime(Date sentDepDocumentsDateTime) {
		this.sentDepDocumentsDateTime = sentDepDocumentsDateTime;
	}

	/**
	 * @return the bonusAbuser
	 */
	public boolean isBonusAbuser() {
		return bonusAbuser;
	}

	/**
	 * @param bonusAbuser the bonusAbuser to set
	 */
	public void setBonusAbuser(boolean bonusAbuser) {
		this.bonusAbuser = bonusAbuser;
	}

	/**
	 * @return the userRegulation
	 */
	public UserRegulationBase getUserRegulation() {
		return userRegulation;
	}

	/**
	 * @param userRegulation the userRegulation to set
	 */
	public void setUserRegulation(UserRegulationBase userRegulation) {
		this.userRegulation = userRegulation;
	}

	/**
	 * @return the vipStatusId
	 */
	public int getVipStatusId() {
		return vipStatusId;
	}

	/**
	 * @param vipStatusId the vipStatusId to set
	 */
	public void setVipStatusId(int vipStatusId) {
		this.vipStatusId = vipStatusId;
	}
	
//	public boolean isHasBubbles() {
//		return hasBubbles;
//	}
//
//	public void setHasBubbles(boolean hasBubbles) {
//		this.hasBubbles = hasBubbles;
//	}

	}