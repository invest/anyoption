package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

/**
 * Marketing landing page class
 *
 * @author Kobi.
 */
public class MarketingLandingPage implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected String name;
    protected String url;
    protected long writerId;
    protected Date timeCreated;
    protected long type;
    protected long staticLandingPageId;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingLandingPage:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
            "url: " + url + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls + 
	        "staticLandingPageId: " + staticLandingPageId + ls +
	        "type: " + type + ls;
    }

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	/**
	 * @return the staticLandingPageId
	 */
	public long getStaticLandingPageId() {
		return staticLandingPageId;
	}

	/**
	 * @param staticLandingPageId the staticLandingPageId to set
	 */
	public void setStaticLandingPageId(long staticLandingPageId) {
		this.staticLandingPageId = staticLandingPageId;
	}

}