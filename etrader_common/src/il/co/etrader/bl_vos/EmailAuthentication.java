package il.co.etrader.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class EmailAuthentication implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long userId;
	private String authenticationKey;
	private String email;
	private Date timeCreated;
	private Date timeFirstClicked;
	private long clicksNum;
	private String userName;


	public long getClicksNum() {
		return clicksNum;
	}

	public void setClicksNum(long clicksNum) {
		this.clicksNum = clicksNum;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthenticationKey() {
		return authenticationKey;
	}

	public void setAuthenticationKey(String authenticationKey) {
		this.authenticationKey = authenticationKey;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeFirstClicked() {
		return timeFirstClicked;
	}

	public void setTimeFirstClicked(Date timeFirstClicked) {
		this.timeFirstClicked = timeFirstClicked;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
