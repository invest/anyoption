package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;

public class Limit implements java.io.Serializable {

	private long id;
	private String name;
	private long minDeposit;
	private long minWithdraw;
	private long minBankWire;
	private long maxDeposit;
	private long maxWithdraw;
	private long maxBankWire;
	private long maxDepositPerDay;
	private long maxDepositPerMonth;
	private Date timeLastUpdate;
	private Date timeCreated;
	private long writerId;
	private String comments;
	private long freeInvestMin;
	private long freeInvestMax;
	private long bankWireFee;
	private long ccFee;
	private long chequeFee;
	private long depositDocsLimit1;
	private long depositDocsLimit2;
	private long minFirstDeposit;
	private long depositAlert;
	
	private String writerName;
	private long currencyId;
	private int skinId;
	private int isDefault;
	private String skinName;
	private String currencyName;
	private long amountForLowWithdraw;

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated);
	}

	public String getTimeLastUpdateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeLastUpdate);
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMaxDeposit() {
		return maxDeposit;
	}

	public String getMaxDepositTxt() {
		return CommonUtil.displayAmount(Long.valueOf(maxDeposit), currencyId);
	}

	public String getMaxDepositTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(maxDeposit), false,1);
	}

	public void setMaxDepositTxtFloat(String maxDeposit) {
		this.maxDeposit = CommonUtil.calcAmount(maxDeposit);
	}

	public void setMaxDeposit(long maxDeposit) {
		this.maxDeposit = maxDeposit;
	}

	public long getMaxDepositPerDay() {
		return maxDepositPerDay;
	}

	public String getMaxDepositPerDayTxt(long currencyId) {
		return CommonUtil.displayAmount(maxDepositPerDay,new Long(currencyId).intValue());
	}

	public String getMaxDepositPerDayTxt() {
		return CommonUtil.displayAmount(maxDepositPerDay,new Long(currencyId).intValue());
	}


	public String getMaxDepositPerDayTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(maxDepositPerDay), false,1);
	}

	public void setMaxDepositPerDayTxtFloat(String maxDepositPerDay) {
		this.maxDepositPerDay = CommonUtil.calcAmount(maxDepositPerDay);
	}

	public void setMaxDepositPerDay(long maxDepositPerDay) {
		this.maxDepositPerDay = maxDepositPerDay;
	}

	public long getMaxDepositPerMonth() {
		return maxDepositPerMonth;
	}

	public String getMaxDepositPerMonthTxt(long currencyId) {
		return CommonUtil.displayAmount(maxDepositPerMonth,new Long(currencyId).intValue());
	}

	public String getMaxDepositPerMonthTxt() {
		return CommonUtil.displayAmount(maxDepositPerMonth,new Long(currencyId).intValue());
	}

	public String getMaxDepositPerMonthTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(maxDepositPerMonth), false,1);
	}

	public void setMaxDepositPerMonthTxtFloat(String maxDepositPerMonth) {
		this.maxDepositPerMonth = CommonUtil.calcAmount(maxDepositPerMonth);
	}
	public void setMaxDepositPerMonth(long maxDepositPerMonth) {
		this.maxDepositPerMonth = maxDepositPerMonth;
	}

	public long getMaxWithdraw() {
		return maxWithdraw;
	}

	public String getMaxWithdrawTxt() {
		return CommonUtil.displayAmount(maxWithdraw,new Long(currencyId).intValue());
	}

	public String getMaxWithdrawTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(maxWithdraw), false,1);
	}

	public void setMaxWithdrawTxtFloat(String maxWithdraw) {
		this.maxWithdraw = CommonUtil.calcAmount(maxWithdraw);
	}

	public void setMaxWithdraw(long maxWithdraw) {
		this.maxWithdraw = maxWithdraw;
	}

	public long getMinDeposit() {
		return minDeposit;
	}

	public String getMinDepositTxt() {
		return CommonUtil.displayAmount(minDeposit,currencyId);
	}

	public String getMinDepositTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(minDeposit), false,1);
	}

	public void setMinDepositTxtFloat(String minDeposit) {
		this.minDeposit = CommonUtil.calcAmount(minDeposit);
	}

	public void setMinDeposit(long minDeposit) {
		this.minDeposit = minDeposit;
	}
	
	@Deprecated
	public long getMinWithdraw() {
		return minWithdraw;
	}
	
	public String getMinWithdrawTxt() {
		return  CommonUtil.displayAmount(minWithdraw,new Long(currencyId).intValue());
	}

	public String getMinWithdrawTxtFloat() {
		return  CommonUtil.displayAmountForInput(Long.valueOf(minWithdraw), false,1);
	}

	public void setMinWithdrawTxtFloat(String minWithdraw) {
		this.minWithdraw = CommonUtil.calcAmount(minWithdraw);
	}

	public void setMinWithdraw(long minWithdraw) {
		this.minWithdraw = minWithdraw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTimeLastUpdate() {
		return timeLastUpdate;
	}
	public void setTimeLastUpdate(Date timeLastUpdate) {
		this.timeLastUpdate = timeLastUpdate;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public String getWriterName() {
		return writerName;
	}
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	public int getIsDefault() {
		return isDefault;
	}

	public boolean isDefaultBoolean() {
		return isDefault==1;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	public void setDefaultBoolean(boolean b) {
		if (b)
			isDefault=1;
		else
			isDefault=0;
	}

	/**
	 * @return the maxBankWire
	 */
	public long getMaxBankWire() {
		return maxBankWire;
	}

	/**
	 * @return the maxBankWireTxt
	 */
	public String getMaxBankWireTxt() {
		return CommonUtil.displayAmount(maxBankWire,new Long(currencyId).intValue());
	}

	public String getMaxBankWireTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(maxBankWire), false, 0);
	}

	public void setMaxBankWireTxtFloat(String maxBankWire) {
		this.maxBankWire = CommonUtil.calcAmount(maxBankWire);
	}

	/**
	 * @param maxBankWire the maxBankWire to set
	 */
	public void setMaxBankWire(long maxBankWire) {
		this.maxBankWire = maxBankWire;
	}

	/**
	 * @return the minBankWire
	 */
	@Deprecated
	public long getMinBankWire() {
		return minBankWire;
	}

	/**
	 * @return the minBankWireTxt
	 */
	public String getMinBankWireTxt() {
		return CommonUtil.displayAmount(minBankWire,new Long(currencyId).intValue());
	}

	public String getMinBankWireTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(minBankWire), false, 0);
	}

	public void setMinBankWireTxtFloat(String minBankWire) {
		this.minBankWire = CommonUtil.calcAmount(minBankWire);
	}

	/**
	 * @param minBankWire the minBankWire to set
	 */
	public void setMinBankWire(long minBankWire) {
		this.minBankWire = minBankWire;
	}

	/**
	 * @return the freeInvestMax
	 */
	public long getFreeInvestMax() {
		return freeInvestMax;
	}

	/**
	 * @return the freeInvestMaxTxt
	 */
	public String getFreeInvestMaxTxt() {
		return CommonUtil.displayAmount(freeInvestMax, new Long(currencyId).intValue());
	}

	public String getFreeInvestMaxTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(freeInvestMax), false, 0);
	}

	public void setFreeInvestMaxTxtFloat(String freeInvestMax) {
		this.freeInvestMax = CommonUtil.calcAmount(freeInvestMax);
	}

	/**
	 * @param freeInvestMax the freeInvestMax to set
	 */
	public void setFreeInvestMax(long freeInvestMax) {
		this.freeInvestMax = freeInvestMax;
	}

	/**
	 * @return the freeInvestMin
	 */
	public long getFreeInvestMin() {
		return freeInvestMin;
	}

	/**
	 * @return the freeInvestMinTxt
	 */
	public String getFreeInvestMinTxt() {
		return CommonUtil.displayAmount(freeInvestMin, new Long(currencyId).intValue());
	}

	public String getFreeInvestMinTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(freeInvestMin), false, 0);
	}

	public void setFreeInvestMinTxtFloat(String freeInvestMin) {
		this.freeInvestMin = CommonUtil.calcAmount(freeInvestMin);
	}

	/**
	 * @param freeInvestMin the freeInvestMin to set
	 */
	public void setFreeInvestMin(long freeInvestMin) {
		this.freeInvestMin = freeInvestMin;
	}

	/**
	 * @return the bankWireFee
	 */
	public long getBankWireFee() {
		return bankWireFee;
	}

	/**
	 * @return the bankWireFeeTxt
	 */
	public String getBankWireFeeTxt() {
		return CommonUtil.displayAmount(bankWireFee, new Long(currencyId).intValue());
	}

	public String getBankWireFeeTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(bankWireFee), false, 0);
	}

	public void setBankWireFeeTxtFloat(String bankWireFee) {
		this.bankWireFee = CommonUtil.calcAmount(bankWireFee);
	}

	/**
	 * @param bankWireFee the bankWireFee to set
	 */
	public void setBankWireFee(long bankWireFee) {
		this.bankWireFee = bankWireFee;
	}

	/**
	 * @return the ccFee
	 */
	public long getCcFee() {
		return ccFee;
	}

	/**
	 * @return the ccFeeTxt
	 */
	public String getCcFeeTxt() {
		return CommonUtil.displayAmount(ccFee, new Long(currencyId).intValue());
	}

	public String getCcFeeTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(ccFee), false, 0);
	}

	public void setCcFeeTxtFloat(String ccFee) {
		this.ccFee = CommonUtil.calcAmount(ccFee);
	}

	/**
	 * @param ccFee the ccFee to set
	 */
	public void setCcFee(long ccFee) {
		this.ccFee = ccFee;
	}

	/**
	 * @return the chequeFee
	 */
	public long getChequeFee() {
		return chequeFee;
	}

	/**
	 * @return the chequeFeeTxt
	 */
	public String getChequeFeeTxt() {
		return CommonUtil.displayAmount(chequeFee, new Long(currencyId).intValue());
	}

	public String getChequeFeeTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(chequeFee), false, 0);
	}

	public void setChequeFeeTxtFloat(String chequeFee) {
		this.chequeFee = CommonUtil.calcAmount(chequeFee);
	}

	/**
	 * @param chequeFee the chequeFee to set
	 */
	public void setChequeFee(long chequeFee) {
		this.chequeFee = chequeFee;
	}

	/**
	 * @return the depositDocsLimit1
	 */
	public long getDepositDocsLimit1() {
		return depositDocsLimit1;
	}

	/**
	 * @return the depositDocsLimit1Txt
	 */
	public String getDepositDocsLimit1Txt() {
		return CommonUtil.displayAmount(depositDocsLimit1, new Long(currencyId).intValue());
	}

	public String getDepositDocsLimit1TxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(depositDocsLimit1), false, 0);
	}

	public void setDepositDocsLimit1TxtFloat(String depositDocsLimit1) {
		this.depositDocsLimit1 = CommonUtil.calcAmount(depositDocsLimit1);
	}

	/**
	 * @param depositDocsLimit1 the depositDocsLimit1 to set
	 */
	public void setDepositDocsLimit1(long depositDocsLimit1) {
		this.depositDocsLimit1 = depositDocsLimit1;
	}

	/**
	 * @return the depositDocsLimit2
	 */
	public long getDepositDocsLimit2() {
		return depositDocsLimit2;
	}

	/**
	 * @return the depositDocsLimit2Txt
	 */
	public String getDepositDocsLimit2Txt() {
		return CommonUtil.displayAmount(depositDocsLimit2, new Long(currencyId).intValue());
	}

	public String getDepositDocsLimit2TxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(depositDocsLimit2), false, 0);
	}

	public void setDepositDocsLimit2TxtFloat(String depositDocsLimit2) {
		this.depositDocsLimit2 = CommonUtil.calcAmount(depositDocsLimit2);
	}

	/**
	 * @param depositDocsLimit2 the depositDocsLimit2 to set
	 */
	public void setDepositDocsLimit2(long depositDocsLimit2) {
		this.depositDocsLimit2 = depositDocsLimit2;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * Get currency symbol
	 * @return
	 */
	public String getCurrencySymbol() {
		return CommonUtil.getCurrencySymbol(currencyId);
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the currencyName
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * @param currencyName the currencyName to set
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	/**
	 * @return the minFirstDeposit
	 */
	public long getMinFirstDeposit() {
		return minFirstDeposit;
	}

	/**
	 * @param minFirstDeposit the minFirstDeposit to set
	 */
	public void setMinFirstDeposit(long minFirstDeposit) {
		this.minFirstDeposit = minFirstDeposit;
	}
	
	public long getDepositAlert() {
		return depositAlert;
	}
	
	/**
	 * @return the depositAlertTxt
	 */
	public String getDepositAlertTxt() {
		return CommonUtil.displayAmount(depositAlert, new Long(currencyId).intValue());
	}
	
	public String getDepositAlertTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(depositAlert), false, 0);
	}

	public void setDepositAlertTxtFloat(String depositAlert) {
		this.depositAlert = CommonUtil.calcAmount(depositAlert);
	}

	public void setDepositAlert(long depositAlert) {
		this.depositAlert = depositAlert;
	}

	public long getAmountForLowWithdraw() {
		return amountForLowWithdraw;
	}

	public void setAmountForLowWithdraw(long amountForLowWithdraw) {
		this.amountForLowWithdraw = amountForLowWithdraw;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Limit ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "name = " + this.name + TAB
	        + "minDeposit = " + this.minDeposit + TAB
	        + "minWithdraw = " + this.minWithdraw + TAB
	        + "maxDeposit = " + this.maxDeposit + TAB
	        + "maxWithdraw = " + this.maxWithdraw + TAB
	        + "maxDepositPerDay = " + this.maxDepositPerDay + TAB
	        + "maxDepositPerMonth = " + this.maxDepositPerMonth + TAB
	        + "timeLastUpdate = " + this.timeLastUpdate + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "writerId = " + this.writerId + TAB
	        + "comments = " + this.comments + TAB
	        + "isDefault = " + this.isDefault + TAB
	        + "writerName = " + this.writerName + TAB
	        + "freeInvestMin = " + this.freeInvestMin + TAB
	        + "freeInvestMax = " + this.freeInvestMax + TAB
	        + "depositAlert = " + this.depositAlert + TAB
	        + "amountForLowWithdraw = " + this.amountForLowWithdraw + TAB
	        + " )";

	    return retValue;
	}



}
