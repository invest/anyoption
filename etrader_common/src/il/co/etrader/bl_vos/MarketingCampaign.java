package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;

import javax.faces.event.ValueChangeEvent;

/**
 * Marketing campaigns class
 *
 * @author Kobi.
 */
public class MarketingCampaign implements Serializable {
    private static final long serialVersionUID = 1L;

    // Campaigns
    public static final long FREE_AO_68 = 68;
    public static final long FREE_ET_112 = 112;

    //Marketing manager writer Id (creating new campigns)
    public static final long MARKETING_MANAGER_WRITER_ID = 346;
    
    //Campaign priority 
    public static final int CAMPAIGN_PRIORITY_LOW = 1;
    public static final int CAMPAIGN_PRIORITY_MEDIUM = 2;
    public static final int CAMPAIGN_PRIORITY_HIGH = 3;

    protected long id;
    protected String name;
    protected long paymentTypeId;
    protected Date startDate;
    protected Date endDate;
    protected long paymentAmount;
    protected String comments;
    protected long writerId;
    protected long paymentRecipientId;
    protected Date timeCreated;
    protected long territoryId;
    protected long sourceId;
    protected long campaignManagerId;
    protected long LastUpdatedBy;
    protected String createdByWriter;
    protected String campaignManagerTxt;
    protected String LastUpdatedByTxt;
    protected String marketingChannelTxt;

    protected String paymentTypeDescription;
    protected String paymentRecipientAgent;
    private boolean disToDate;
    protected int priority;
    protected String description;
    protected long marketingChannelId;
    protected double unitPrice;
    protected String unitPriceTxt;
    

    public MarketingCampaign() {
    	priority = ConstantsBase.PRIORITY_LOW;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return the endDateTxt
	 */
	public String getEndDateTxt() {
		if ( null == endDate ) {
			return ConstantsBase.CAMPAIGN_UNKNOWN;
		}
		return CommonUtil.getDateFormat(endDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the paymentAmount
	 */
	public long getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @return the paymentAmountTxt
	 */
	public String getPaymentAmountTxt() {
		if ( paymentAmount > 0 ) {
			return String.valueOf(paymentAmount);
		} else {
			return "";
		}
	}

	/**
	 * @param paymentAmount the paymentAmount to set
	 */
	public void setPaymentAmount(long paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * @return the paymentRecipientId
	 */
	public long getPaymentRecipientId() {
		return paymentRecipientId;
	}

	/**
	 * @param paymentRecipientId the paymentRecipientId to set
	 */
	public void setPaymentRecipientId(long paymentRecipientId) {
		this.paymentRecipientId = paymentRecipientId;
	}

	/**
	 * @return the paymentTypeId
	 */
	public long getPaymentTypeId() {
		return paymentTypeId;
	}

	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(long paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the startDateTxt
	 */
	public String getStartDateTxt() {
		return CommonUtil.getDateFormat(startDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

    /**
     * Get campaign manager name
     * @return
     * @throws SQLException
     */
    public String getCampaignManagerTxt() throws SQLException {
        return campaignManagerTxt;
    }
    
    public String getMarketingChannelTxt() throws SQLException {
    	return marketingChannelTxt;
    }

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @return the paymentTypeDescription
	 */
	public String getPaymentTypeDescription() {
		return paymentTypeDescription;
	}

	/**
	 * @param paymentTypeDescription the paymentTypeDescription to set
	 */
	public void setPaymentTypeDescription(String paymentTypeDescription) {
		this.paymentTypeDescription = paymentTypeDescription;
	}

	/**
	 * On change event to campaign (enable toDate)
	 * @param event
	 * @throws SQLException
	 */
    public void updateChangeCamp(ValueChangeEvent event) throws SQLException{
    	disToDate = (Boolean)event.getNewValue();
    	if (disToDate) {
    		setEndDate(null);
    	} else {
    		setEndDate(new Date());
    	}

    }

	/**
	 * @return the disToDate
	 */
	public boolean isDisToDate() {
		return disToDate;
	}

	/**
	 * @param disToDate the disToDate to set
	 */
	public void setDisToDate(boolean disToDate) {
		this.disToDate = disToDate;
	}

	/**
	 * @return the territoryId
	 */
	public long getTerritoryId() {
		return territoryId;
	}

	/**
	 * @param territoryId the territoryId to set
	 */
	public void setTerritoryId(long territoryId) {
		this.territoryId = territoryId;
	}


	/**
	 * @return the sourceId
	 */
	public long getSourceId() {
		return sourceId;
	}

	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}


	/**
	 * @return the paymentRecipientAgent
	 */
	public String getPaymentRecipientAgent() {
		return paymentRecipientAgent;
	}

	/**
	 * @param paymentRecipientAgent the paymentRecipientAgent to set
	 */
	public void setPaymentRecipientAgent(String paymentRecipientAgent) {
		this.paymentRecipientAgent = paymentRecipientAgent;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingCampaign:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
            "paymentTypeId: " + paymentTypeId + ls +
            "startDate: " + startDate + ls +
            "endDate: " + endDate + ls +
        	"paymentAmount: " + paymentAmount + ls +
        	"comments: " + comments + ls +
	        "writerId: " + writerId + ls +
	        "paymentRecipientId: " + paymentRecipientId + ls +
	        "territoryId: " + territoryId + ls +
	        "sourceId: " + sourceId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

    /**
     * @return the campaignManagerId
     */
    public long getCampaignManagerId() {
        return campaignManagerId;
    }

    /**
     * @param campaignManagerId the campaignManagerId to set
     */
    public void setCampaignManagerId(long campaignManagerId) {
        this.campaignManagerId = campaignManagerId;
    }

    /**
     * @return the lastUpdatedBy
     */
    public long getLastUpdatedBy() {
        return LastUpdatedBy;
    }

    /**
     * @param lastUpdatedBy the lastUpdatedBy to set
     */
    public void setLastUpdatedBy(long lastUpdatedBy) {
        LastUpdatedBy = lastUpdatedBy;
    }

    /**
     * @param campaignManagerTxt the campaignManagerTxt to set
     */
    public void setCampaignManagerTxt(String campaignManagerTxt) {
        this.campaignManagerTxt = campaignManagerTxt;
    }
    
    public void setMarketingChannelTxt(String marketingChannelTxt) {
    	this.marketingChannelTxt = marketingChannelTxt;
    }

    /**
     * @return the lastUpdatedByTxt
     */
    public String getLastUpdatedByTxt() {
        return LastUpdatedByTxt;
    }

    /**
     * @param lastUpdatedByTxt the lastUpdatedByTxt to set
     */
    public void setLastUpdatedByTxt(String lastUpdatedByTxt) {
        LastUpdatedByTxt = lastUpdatedByTxt;
    }

    /**
     * @return the createdByWriter
     * @throws SQLException
     */
    public String getCreatedByWriter() {
        return createdByWriter;
    }

    /**
     * @param createdByWriter the createdByWriter to set
     */
    public void setCreatedByWriter(String createdByWriter) {
        this.createdByWriter = createdByWriter;
    }

	public long getMarketingChannelId() {
		return marketingChannelId;
	}

	
	public void setMarketingChannelId(long marketingChannelId) {
		this.marketingChannelId = marketingChannelId;
	}

	
	public double getUnitPrice() {
		if(unitPriceTxt!=null && !unitPriceTxt.isEmpty()){
			return Double.parseDouble(unitPriceTxt.replace("," , "."));
		}else{
			return 0;
		}
	}
	
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public String getUnitPriceTxt() {
		DecimalFormat dtime = new DecimalFormat("#0.00000");
		return dtime.format(unitPrice);
		
	}

	
	public void setUnitPriceTxt(String unitPriceTxt) {
		this.unitPriceTxt = unitPriceTxt;
	}

}