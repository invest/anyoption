//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.IssuesManagerBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.Date;
//
//import org.bouncycastle.crypto.CryptoException;
//
//import com.anyoption.common.beans.IssueChannel;
//import com.anyoption.common.beans.IssueReachedStatus;
//import com.anyoption.common.beans.IssueReaction;
//
//public class IssueAction implements java.io.Serializable{
//	private static final long serialVersionUID = -8615097934546158849L;
//
//	private long actionId;
//	private long issueId;
//	private long writerId;
//	private String writerName;
//	private Date actionTime;
//	private String actionTimeOffset;
//	private String comments;
//	private boolean significant;
//	private String reachedStatusId;
//	private long transactionId;
//	private String channelId;
//	private long callDirectionId;
//	private Date callBackDate;
//	private String callBackTimeHour;
//	private String callBackTimeMin;
//	private String callBackTimeOffset;
//	private boolean isCallBack;
//	private String activityId;
//	private String activityName;
//	private long marketingOperationId;
//	private long chatId;
//	private String actionTypeId;
//	private String actionTypeName;
//	private long smsId;
//	private long templateId;
//
//	private boolean scheduledCbRelevant;
//
//	private IssueReachedStatus reachedStatus;
//	private IssueReaction reaction;
//	private IssueChannel channel;
//
//	private String skin;
//	private String riskReasonId;
//	private String riskReasonName;
//	private String relevantToCards;
//
//	public IssueAction() {
//		scheduledCbRelevant = true;
//		callDirectionId=0;
//		callBackTimeHour="0";
//		callBackTimeMin="0";
//		callBackDate=new Date();
//		isCallBack = false;
//	}
//
//
//	public String getComments() {
//		return comments;
//	}
//
//	public String getCommentsWithBr() {
//		return comments.replace("\n", "<br/>");
//	}
//
//	public String getCommentsLittleThenMax() {
//		String temp_comments = comments;
//		if ( temp_comments == null ) {
//			temp_comments = "";
//		}
//		else if ( temp_comments.length() == 0 ) {
//			temp_comments = "";
//		}
//		else if ( temp_comments.length() > ConstantsBase.MAX_COMMENTS )  {
//			temp_comments = temp_comments.substring(0, ConstantsBase.MAX_COMMENTS-1) + "...";
//		}
//		return temp_comments;
//	}
//
//	public void setComments(String comments) {
//		this.comments = comments;
//	}
//
//	public long getWriterId() {
//		return writerId;
//	}
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	public boolean isSignificant() {
//		return significant;
//	}
//
//	public void setSignificant(boolean significant) {
//		this.significant = significant;
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "Issue ( "
//	        + super.toString() + TAB
//	        + "actionId = " + this.actionId + TAB
//	        + "channelId = " + this.channelId + TAB
//	        + "writerId = " + this.writerId + TAB
//	        + "comments = " + this.comments + TAB
//	        + "significant = " + this.significant + TAB
//	        + "reachedStatusId = " + this.reachedStatusId + TAB
//	        + "actionTime = " + this.actionTime + TAB
//	        + "issueActionTypeId = " + this.actionTypeId + TAB
//	        + "riskReasonId = " + this.riskReasonId + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//
//	public String getSkin() {
//		return skin;
//	}
//
//	public void setSkin(String skin) {
//		this.skin = skin;
//	}
//
//	public String getReachedStatusId() {
//		return reachedStatusId;
//	}
//
//	public void setReachedStatusId(String reachedStatusId) {
//		this.reachedStatusId = reachedStatusId;
//	}
//
//	/**
//	 * @return the reachedStatus
//	 */
//	public IssueReachedStatus getReachedStatus() {
//		return reachedStatus;
//	}
//
//	/**
//	 * @param reachedStatus the reachedStatus to set
//	 */
//	public void setReachedStatus(IssueReachedStatus reachedStatus) {
//		this.reachedStatus = reachedStatus;
//	}
//
//	/**
//	 * @return the reaction
//	 */
//	public IssueReaction getReaction() {
//		return reaction;
//	}
//
//	/**
//	 * @param reaction the reaction to set
//	 */
//	public void setReaction(IssueReaction reaction) {
//		this.reaction = reaction;
//	}
//
//	public String getActionTimeTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(actionTime, CommonUtil.getUtcOffset(actionTimeOffset));
//	}
//
//
//	/**
//	 * @return the actionId
//	 */
//	public long getActionId() {
//		return actionId;
//	}
//
//
//	/**
//	 * @param actionId the actionId to set
//	 */
//	public void setActionId(long actionId) {
//		this.actionId = actionId;
//	}
//
//
//	/**
//	 * @return the actionTime
//	 */
//	public Date getActionTime() {
//		return actionTime;
//	}
//
//
//	/**
//	 * @param actionTime the actionTime to set
//	 */
//	public void setActionTime(Date actionTime) {
//		this.actionTime = actionTime;
//	}
//
//
//	/**
//	 * @return the actionTimeOffset
//	 */
//	public String getActionTimeOffset() {
//		return actionTimeOffset;
//	}
//
//
//	/**
//	 * @param actionTimeOffset the actionTimeOffset to set
//	 */
//	public void setActionTimeOffset(String actionTimeOffset) {
//		this.actionTimeOffset = actionTimeOffset;
//	}
//
//
//	/**
//	 * @return the issueId
//	 */
//	public long getIssueId() {
//		return issueId;
//	}
//
//
//	/**
//	 * @param issueId the issueId to set
//	 */
//	public void setIssueId(long issueId) {
//		this.issueId = issueId;
//	}
//
//
//	/**
//	 * @return the transactionId
//	 */
//	public long getTransactionId() {
//		return transactionId;
//	}
//
//
//	/**
//	 * @param transactionId the transactionId to set
//	 */
//	public void setTransactionId(long transactionId) {
//		this.transactionId = transactionId;
//	}
//
//
//	/**
//	 * @return the channel
//	 */
//	public IssueChannel getChannel() {
//		return channel;
//	}
//
//
//	/**
//	 * @param channel the channel to set
//	 */
//	public void setChannel(IssueChannel channel) {
//		this.channel = channel;
//	}
//
//
//	/**
//	 * @return the channelId
//	 */
//	public String getChannelId() {
//		return channelId;
//	}
//
//
//	/**
//	 * @param channelId the channelId to set
//	 */
//	public void setChannelId(String channelId) {
//		this.channelId = channelId;
//	}
//
//
//	/**
//	 * @return the writerName
//	 */
//	public String getWriterName() {
//		return writerName;
//	}
//
//
//	/**
//	 * @param writerName the writerName to set
//	 */
//	public void setWriterName(String writerName) {
//		this.writerName = writerName;
//	}
//
//
//	/**
//	 * @return the callDirectionId
//	 */
//	public long getCallDirectionId() {
//		return callDirectionId;
//	}
//
//	/**
//	 * @return the callDirection NAme
//	 */
//	public String getCallDirectionName() {
//		if (callDirectionId == IssuesManagerBase.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL){
//			return CommonUtil.getMessage("issues.actions.call.direction.out", null);
//		}else if (callDirectionId == IssuesManagerBase.ISSUE_DIRECTION_IN_BOUND_PHONE_CALL){
//			return CommonUtil.getMessage("issues.actions.call.direction.in", null);
//		}else if (callDirectionId == IssuesManagerBase.ISSUE_DIRECTION_MEMBER_REQUESTED_PHONE_CALL){
//			return CommonUtil.getMessage("issues.actions.call.direction.mr", null);
//		}
//		return null;
//	}
//
//	/**
//	 * @param callDirectionId the callDirectionId to set
//	 */
//	public void setCallDirectionId(long callDirectionId) {
//		this.callDirectionId = callDirectionId;
//	}
//
//
//	/**
//	 * @return the callBackDateAndTime
//	 */
//	public Date getCallBackDateAndTime() {
//		Calendar cal = Calendar.getInstance();
//
//		cal.setTime(callBackDate);
//		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(callBackTimeHour));
//		cal.set(Calendar.MINUTE, Integer.parseInt(callBackTimeMin));
//
//		return cal.getTime();
//	}
//
//	/**
//	 * @return the callBackDate
//	 */
//	public Date getCallBackDate() {
//		updateCallBackRelevant();
//		return callBackDate;
//	}
//
//
//	/**
//	 * @param callBackDate the callBackDate to set
//	 */
//	public void setCallBackDate(Date callBackDate) {
//		this.callBackDate = callBackDate;
//	}
//
//
//	/**
//	 * @return the callBackTimeHour
//	 */
//	public String getCallBackTimeHour() {
//		return callBackTimeHour;
//	}
//
//
//	/**
//	 * @param callBackTimeHour the callBackTimeHour to set
//	 */
//	public void setCallBackTimeHour(String callBackTimeHour) {
//		this.callBackTimeHour = callBackTimeHour;
//	}
//
//
//	/**
//	 * @return the callBackTimeMin
//	 */
//	public String getCallBackTimeMin() {
//		return callBackTimeMin;
//	}
//
//
//	/**
//	 * @param callBackTimeMin the callBackTimeMin to set
//	 */
//	public void setCallBackTimeMin(String callBackTimeMin) {
//		this.callBackTimeMin = callBackTimeMin;
//	}
//
//
//	/**
//	 * @return the callBackTimeOffset
//	 */
//	public String getCallBackTimeOffset() {
//		return callBackTimeOffset;
//	}
//
//
//	/**
//	 * @param callBackTimeOffset the callBackTimeOffset to set
//	 */
//	public void setCallBackTimeOffset(String callBackTimeOffset) {
//		this.callBackTimeOffset = callBackTimeOffset;
//	}
//
//
//	/**
//	 * @return the isCallBack
//	 */
//	public boolean isCallBack() {
//		return isCallBack;
//	}
//
//
//	/**
//	 * @param isCallBack the isCallBack to set
//	 */
//	public void setCallBack(boolean isCallBack) {
//		this.isCallBack = isCallBack;
//	}
//
//
//	/**
//	 * @return the activityId
//	 */
//	public String getActivityId() {
//		return activityId;
//	}
//
//
//	/**
//	 * @param activityId the activityId to set
//	 */
//	public void setActivityId(String activityId) {
//		this.activityId = activityId;
//	}
//
//
//	/**
//	 * @return the activityName
//	 */
//	public String getActivityName() {
//		return activityName;
//	}
//
//
//	/**
//	 * @param activityName the activityName to set
//	 */
//	public void setActivityName(String activityName) {
//		this.activityName = activityName;
//	}
//
//
//	/**
//	 * @return the scheduledCbRelevant
//	 */
//	public boolean isScheduledCbRelevant() {
//		return scheduledCbRelevant;
//	}
//
//
//	/**
//	 * @param scheduledCbRelevant the scheduledCbRelevant to set
//	 */
//	public void setScheduledCbRelevant(boolean scheduledCbRelevant) {
//
//		this.scheduledCbRelevant = scheduledCbRelevant;
//	}
//
//	/**
//	 * @return the chatId
//	 */
//	public long getChatId() {
//		return chatId;
//	}
//
//	/**
//	 * @param chatId the chatId to set
//	 */
//	public void setChatId(long chatId) {
//		this.chatId = chatId;
//	}
//
//
//	public String getCallBackTimeTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(callBackDate,callBackTimeOffset);
//	}
//
//
//	public boolean updateCallBackRelevant(){
//		if (null != callBackDate){
//			scheduledCbRelevant = true;
//			return true;
//		}
//		else {
//			scheduledCbRelevant = false;
//			return false;
//		}
//	}
//
//
//	public long getMarketingOperationId() {
//		return marketingOperationId;
//	}
//
//
//	public void setMarketingOperationId(long marketingOperationId) {
//		this.marketingOperationId = marketingOperationId;
//	}
//
//	/**
//	 * @return the issueActionTypeId
//	 */
//	public String getActionTypeId() {
//		return actionTypeId;
//	}
//
//	/**
//	 * @param issueActionTypeId the issueActionTypeId to set
//	 */
//	public void setActionTypeId(String issueActionTypeId) {
//		this.actionTypeId = issueActionTypeId;
//	}
//
//	/**
//	 * @return the issueActionTypeName
//	 */
//	public String getActionTypeName() {
//		return actionTypeName;
//	}
//
//	/**
//	 * @param issueActionTypeName the issueActionTypeName to set
//	 */
//	public void setActionTypeName(String issueActionTypeName) {
//		this.actionTypeName = issueActionTypeName;
//	}
//
//	public Date getCurrentTimeWithCallBackOffset(){
//		return CommonUtil.getDateTimeFormaByTz(new Date(),this.getCallBackTimeOffset());
//	}
//
//	public String getWriterNameById() throws SQLException {
//		return CommonUtil.getWriterName(writerId);
//	}
//
//	/**
//	 * @return the riskReasonId
//	 */
//	public String getRiskReasonId() {
//		return riskReasonId;
//	}
//
//	/**
//	 * @param riskReasonId the riskReasonId to set
//	 */
//	public void setRiskReasonId(String riskReasonId) {
//		this.riskReasonId = riskReasonId;
//	}
//
//	/**
//	 * @return the riskReasonName
//	 */
//	public String getRiskReasonName() {
//		return riskReasonName;
//	}
//
//	/**
//	 * @param riskReasonName the riskReasonName to set
//	 */
//	public void setRiskReasonName(String riskReasonName) {
//		this.riskReasonName = riskReasonName;
//	}
//
//	/**
//	 * @return the relevantToCards
//	 * @throws CryptoException
//	 * @throws SQLException
//	 */
//	public String getRelevantToCards() throws SQLException, CryptoException {
//		if (CommonUtil.IsParameterEmptyOrNull(relevantToCards)){
//			relevantToCards =  IssuesManagerBase.getRelevantCardsToIssueAction(actionId);
//		}
//		return relevantToCards;
//	}
//
//	/**
//	 * @param relevantToCards the relevantToCards to set
//	 */
//	public void setRelevantToCards(String relevantToCards) {
//		this.relevantToCards = relevantToCards;
//	}
//
//
//	public long getSmsId() {
//		return smsId;
//	}
//
//
//	public void setSmsId(long smsId) {
//		this.smsId = smsId;
//	}
//
//
//	/**
//	 * @return the templateId
//	 */
//	public long getTemplateId() {
//		return templateId;
//	}
//
//
//	/**
//	 * @param templateId the templateId to set
//	 */
//	public void setTemplateId(long templateId) {
//		this.templateId = templateId;
//	}
//
//}
