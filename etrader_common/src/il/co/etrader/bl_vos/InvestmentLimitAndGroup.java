package il.co.etrader.bl_vos;

import com.anyoption.common.beans.InvestmentLimitsGroup;

public class InvestmentLimitAndGroup implements java.io.Serializable {

    private Long investmentLimitId;
	private Long investmentLimitGroupId;

	private InvestmentLimit invLimit;
	private InvestmentLimitsGroup invLimitGroup;

	public InvestmentLimitAndGroup() {
		investmentLimitId=new Long(0);
		investmentLimitGroupId=new Long(0);
	}

	public Long getInvestmentLimitGroupId() {
		return investmentLimitGroupId;
	}

	public void setInvestmentLimitGroupId(Long investmentLimitGroupId) {
		this.investmentLimitGroupId = investmentLimitGroupId;
	}

	public Long getInvestmentLimitId() {
		return investmentLimitId;
	}

	public void setInvestmentLimitId(Long investmentLimitId) {
		this.investmentLimitId = investmentLimitId;
	}

	public InvestmentLimit getInvLimit() {
		return invLimit;
	}

	public void setInvLimit(InvestmentLimit invLimit) {
		this.invLimit = invLimit;
	}

	public InvestmentLimitsGroup getInvLimitGroup() {
		return invLimitGroup;
	}

	public void setInvLimitGroup(InvestmentLimitsGroup invLimitGroup) {
		this.invLimitGroup = invLimitGroup;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentLimitAndGroup ( "
	        + super.toString() + TAB
	        + "investmentLimitId = " + this.investmentLimitId + TAB
	        + "investmentLimitGroupId = " + this.investmentLimitGroupId + TAB
	        + "invLimit = " + this.invLimit + TAB
	        + "invLimitGroup = " + this.invLimitGroup + TAB
	        + " )";

	    return retValue;
	}





}