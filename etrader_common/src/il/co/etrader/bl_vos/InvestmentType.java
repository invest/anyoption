//package il.co.etrader.bl_vos;
//
//public class InvestmentType implements java.io.Serializable {
//    public static final long CALL = 1;
//    public static final long PUT = 2;
//    public static final long ONE_TOUCH = 3;
//    public static final long BUY = 4;
//    public static final long SELL = 5;
//
//	private long id;
//	private String name;
//	private long writerId;
//	private String displayName;
//
//	public String getDisplayName() {
//		return displayName;
//	}
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public long getWriterId() {
//		return writerId;
//	}
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "InvestmentType ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "writerId = " + this.writerId + TAB
//	        + "displayName = " + this.displayName + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//
//
//}
