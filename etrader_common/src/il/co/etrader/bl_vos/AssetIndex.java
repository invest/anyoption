package il.co.etrader.bl_vos;

public class AssetIndex implements java.io.Serializable {

    private static final long serialVersionUID = -8599832624834373392L;

    private long id;
	private String tradingDays;
	private String fridayTime = null;
    private String startTime;
    private String endTime;
    private boolean isFullDay;
    private boolean twentyFourSeven;
    private String startBreakTime;
    private String endBreakTime;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTradingDays() {
		return tradingDays;
	}
	public void setTradingDays(String str) {
		this.tradingDays = str;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String getFridayTime() {
		return fridayTime;
	}
	public void setFridayTime(String fridayTime) {
		this.fridayTime = fridayTime;
	}

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the isFullDay
     */
    public boolean isFullDay() {
        return isFullDay;
    }

    /**
     * @param isFullDay the isFullDay to set
     */
    public void setFullDay(boolean isFullDay) {
        this.isFullDay = isFullDay;
    }

	public String getEndBreakTime() {
		return endBreakTime;
	}

	public void setEndBreakTime(String endBreakTime) {
		this.endBreakTime = endBreakTime;
	}

	public String getStartBreakTime() {
		return startBreakTime;
	}

	public void setStartBreakTime(String startBreakTime) {
		this.startBreakTime = startBreakTime;
	}

	public boolean isHaveBreak() {
		return null != startBreakTime && null != endBreakTime;
	}
	
	public boolean isTwentyFourSeven() {
		return twentyFourSeven;
	}
	
	public void setTwentyFourSeven(boolean twentyFourSeven) {
		this.twentyFourSeven = twentyFourSeven;
	}
}
