//package il.co.etrader.bl_vos;
//
//public class PaymentMethod implements java.io.Serializable, Cloneable {
//
//	/**
//	 *
//	 */
//	private static final long serialVersionUID = 1362267792146606857L;
//
//	private long id;
//	private int typeId;
//	private String description;
//	private String name;
//	private String envoyWithdrawCountryCode;
//
//
//	/**
//	 * @return the description
//	 */
//	public String getDescription() {
//		return description;
//	}
//	/**
//	 * @param description the description to set
//	 */
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	/**
//	 * @return the envoyWithdrawCountryCode
//	 */
//	public String getEnvoyWithdrawCountryCode() {
//		return envoyWithdrawCountryCode;
//	}
//	/**
//	 * @param envoyWithdrawCountryCode the envoyWithdrawCountryCode to set
//	 */
//	public void setEnvoyWithdrawCountryCode(String envoyWithdrawCountryCode) {
//		this.envoyWithdrawCountryCode = envoyWithdrawCountryCode;
//	}
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//	/**
//	 * @return the typeId
//	 */
//	public int getTypeId() {
//		return typeId;
//	}
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(int typeId) {
//		this.typeId = typeId;
//	}
//
//
//}
