//package il.co.etrader.bl_vos;
//
//
//public class Exchange implements java.io.Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	public static final long ISRAEL_ID = 1L;
//	public static final long USD_ILS_ID = 9L;
//	
//    private long id;
//    private String name;
//    private String halfDayClosingTime;
//
//    public Exchange() {
//
//    }
//	public String getHalfDayClosingTime() {
//		return halfDayClosingTime;
//	}
//
//	public void setHalfDayClosingTime(String halfDayClosingTime) {
//		this.halfDayClosingTime = halfDayClosingTime;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//
//	public String getName() {
//		return name;
//	}
//
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getHalfDayClosingTimeMin() {
//		if (halfDayClosingTime == null) {
//			return "00";
//		}
//		String[] parts=halfDayClosingTime.split(":");
//		return parts[1];
//	}
//
//	public String getHalfDayClosingTimeHour() {
//		if (halfDayClosingTime == null) {
//			return "00";
//		}
//		String[] parts=halfDayClosingTime.split(":");
//		return parts[0];
//	}
//
//	public void setHalfDayClosingTimeMin(String min) {
//		if (halfDayClosingTime == null) {
//			halfDayClosingTime="00:"+min;
//		}
//		halfDayClosingTime=halfDayClosingTime.substring(0,3)+min;
//	}
//	public void setHalfDayClosingTimeHour(String h) {
//		if (halfDayClosingTime == null) {
//			halfDayClosingTime=h+":00:";
//		}
//		halfDayClosingTime=h+halfDayClosingTime.substring(2);
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = "    ";
//
//	    String retValue = "";
//
//	    retValue = "Exchange ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "halfDayClosingTime = " + this.halfDayClosingTime + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//}