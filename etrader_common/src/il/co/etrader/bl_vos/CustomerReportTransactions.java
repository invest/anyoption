package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;
import java.util.Locale;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.managers.CurrenciesManagerBase;


public class CustomerReportTransactions implements java.io.Serializable{

	public CustomerReportTransactions(
			long reportId, long reportType, long userId, String firstName,
			String lastName, String email, Date fromDate, Date toDate,
			Date timeGenerate, long balanceBeginDate, long balanceEndDate,
			long transactionId, long reportGroup, Date dateTime,
			String description, long debit, long credit, String comments,
			Date transactionTimeCreated, Date transactionTimeSettled) {
		super();
		this.reportId = reportId;
		this.reportType = reportType;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.timeGenerate = timeGenerate;
		this.balanceBeginDate = balanceBeginDate;
		this.balanceEndDate = balanceEndDate;
		this.transactionId = transactionId;
		this.reportGroup = reportGroup;
		this.dateTime = dateTime;
		this.description = description;
		this.debit = debit;
		this.credit = credit;
		this.comments = comments;
		this.transactionTimeCreated = transactionTimeCreated;
		this.transactionTimeSettled = transactionTimeSettled;
	}

	
	public CustomerReportTransactions() {
	}

	public static final int CUSTOMER_TRANSACTION_REPORT_DEPOSIT_CANCEL = 5;
	public static final int CUSTOMER_TRANSACTION_REPORT_DEPOSIT_CHARGED = 8;
	public static final int CUSTOMER_TRANSACTION_REPORT_REVERSE_WITHDRAW  = 13;
	
	private static final long serialVersionUID = -4906666277430781819L;
	private static final String timeZoneName = "Asia/Jerusalem";
	private Locale locale = new Locale("iw");	
	Currency currency = CurrenciesManagerBase.getCurrency(Currency.CURRENCY_ILS_ID);
	
	private long reportId;
	private long reportType;
	
	private long userId;
	private String firstName;
	private String lastName;
	private String email;
	private Date fromDate;
	private Date toDate;
	private Date timeGenerate;
	
	private long balanceBeginDate;
	private long balanceEndDate;
	
	private long transactionId;
	private long reportGroup;
	private Date dateTime;
	private String description;
	private long debit;
	private long credit;
	private String comments;
	private Date transactionTimeCreated;
	private Date transactionTimeSettled;
	
	
	
	
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public long getReportType() {
		return reportType;
	}
	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public long getBalanceBeginDate() {
		return balanceBeginDate;
	}
	public void setBalanceBeginDate(long balanceBeginDate) {
		this.balanceBeginDate = balanceBeginDate;
	}
	public long getBalanceEndDate() {
		return balanceEndDate;
	}
	public void setBalanceEndDate(long balanceEndDate) {
		this.balanceEndDate = balanceEndDate;
	}

	public Date getTimeGenerate() {
		return timeGenerate;
	}
	public void setTimeGenerate(Date timeGenerate) {
		this.timeGenerate = timeGenerate;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getReportGroup() {
		return reportGroup;
	}
	public void setReportGroup(long reportGroup) {
		this.reportGroup = reportGroup;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getDebit() {
		return debit;
	}
	public void setDebit(long debit) {
		this.debit = debit;
	}
	public long getCredit() {
		return credit;
	}
	public void setCredit(long credit) {
		this.credit = credit;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getTransactionTimeCreated() {
		return transactionTimeCreated;
	}
	public void setTransactionTimeCreated(Date transactionTimeCreated) {
		this.transactionTimeCreated = transactionTimeCreated;
	}
	public Date getTransactionTimeSettled() {
		return transactionTimeSettled;
	}
	public void setTransactionTimeSettled(Date transactionTimeSettled) {
		this.transactionTimeSettled = transactionTimeSettled;
	}
	
	//TXT
	
	public String getFromDateTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getFromDate(), null, "", "dd/MM/yyyy");
	}
	
	public String getToDateTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getToDate(), null, "", "dd/MM/yyyy");
	}
	
	public String getTimeGenerateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(getTimeGenerate(), timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	
	public String getBalanceBeginDateTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(balanceBeginDate, currency);
	}
	
	public String getBalanceEndDateTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(balanceEndDate, currency);
	}
	
	public String getDateTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(getDateTime(), timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	
	public String getDescriptionTxt() {
		return CommonUtil.getMessage(locale, description, null);
	}
	
	public String getDateTimeDateTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getDateTime(), timeZoneName, "", "dd/MM/yyyy");
	}
	
	public String getDateTimeHoureTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getDateTime(), timeZoneName, "", "HH:mm");
	}
	
	public String getCommentsTxt() {
		return CommonUtil.getMessage(locale, comments, null);
	}
	
	public String getDebitTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(debit, currency);
	}
	
	public String getCreditTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(credit, currency);
	}
	
	public String getTransactionTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(getTransactionTimeCreated(), timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	
	public String getTransactionTimeSettledTxt() {
		return CommonUtil.getDateTimeFormatDisplay(getTransactionTimeSettled(), timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	
	public boolean isEven(int i) {
		boolean res = false;
		if((i % 2) == 0){
			res = true;
		}		
		return res;
	}
}
