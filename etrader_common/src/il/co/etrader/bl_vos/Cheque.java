package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.ConstantsBase;

import javax.faces.context.FacesContext;

public class Cheque extends com.anyoption.common.bl_vos.Cheque implements java.io.Serializable {

	private static final long serialVersionUID = -90056896771398536L;

	public String getCityName() {
    	FacesContext context=FacesContext.getCurrentInstance();
    	ApplicationDataBase a= (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
    	if (getCityId()==ConstantsBase.ANYOPTION_CITY_ID) {
    		return getCityName();
    	} else {
    		return (String)a.getCities().get(String.valueOf(getCityId()));
    	}
    }
}
