package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;


public class CustomerReportSent implements java.io.Serializable{

	private static final long serialVersionUID = -5901296328492099025L;
	public static long SEND_TO_USER = 0;
	public static long SEND_TO_OTHER = 1;
	
	private long id;
	private long userId;
	private long reportId;
	private long reportType;
	private Date originalDateSent;
	private String period;
	private boolean isReSent;
	private String sentMsg;
	private String comments;
	private String newComments;
	private String otherEmail;
	private long toSendMail;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public long getReportType() {
		return reportType;
	}
	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	public Date getOriginalDateSent() {
		return originalDateSent;
	}
	public void setOriginalDateSent(Date originalDateSent) {
		this.originalDateSent = originalDateSent;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public boolean isReSent() {
		return isReSent;
	}
	public void setReSent(boolean isReSent) {
		this.isReSent = isReSent;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getSentMsg() {
		return sentMsg;
	}
	public void setSentMsg(String sentMsg) {
		this.sentMsg = sentMsg;
	}
	public String getNewComments() {
		return newComments;
	}
	public void setNewComments(String newComments) {
		this.newComments = newComments;
	}
	public String getOtherEmail() {
		return otherEmail;
	}
	public void setOtherEmail(String otherEmail) {
		this.otherEmail = otherEmail;
	}
	public long getToSendMail() {
		return toSendMail;
	}
	public void setToSendMail(long toSendMail) {
		this.toSendMail = toSendMail;
	}
	
	
	public String getOriginalDateSentTxt() {
		return CommonUtil.getDateTimeFormatDisplay(originalDateSent, CustomerReportInvestments.timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
}
