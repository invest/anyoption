//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.io.IOException;
//import java.net.URLDecoder;
//import java.sql.SQLException;
//import java.util.Date;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import javax.el.ValueExpression;
//import javax.faces.context.FacesContext;
//
//import org.apache.myfaces.custom.fileupload.UploadedFile;
//
///**
// * MailBoxTemplate vo class.
// * @author Kobi
// *
// */
//public class MailBoxTemplate implements java.io.Serializable {
//
//	private static final long serialVersionUID = 1L;
//	private long id;
//	private String name;
//	private String subject;
//	private long typeId;
//	private Date timeCreated;
//	private long writerId;
//	private String template;
//	private boolean isHighPriority;
//	private long skinId;
//	private long languageId;
//	private long senderId;
//	private long popupTypeId;
//
//	private String senderName;
//	private String typeName;
//	private String skinName;
//	private String languageName;
//
//	private UploadedFile file;
//	private boolean display;          // Just for Backend use, display the html page
//
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//
//	/**
//	 * @return the subject
//	 */
//	public String getSubject() {
//		return subject;
//	}
//
//	/**
//	 * @param subject the subject to set
//	 */
//	public void setSubject(String subject) {
//		this.subject = subject;
//	}
//
//	/**
//	 * @return the typeId
//	 */
//	public long getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}
//
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	/**
//	 * @return the template
//	 */
//	public String getTemplate() {
//		return template;
//	}
//
//	/**
//	 * @param template the template to set
//	 */
//	public void setTemplate(String template) {
//		this.template = template;
//	}
//
//	public boolean getIsHighPriority() {
//		return isHighPriority;
//	}
//
//	public void setIsHighPriority(boolean isHighPriority) {
//		this.isHighPriority = isHighPriority;
//	}
//
//	public long getSenderId() {
//		return senderId;
//	}
//
//	public void setSenderId(long senderId) {
//		this.senderId = senderId;
//	}
//
//	public String getSenderName() {
//		return senderName;
//	}
//
//	public void setSenderName(String senderName) {
//		this.senderName = senderName;
//	}
//
//	public long getLanguageId() {
//		return languageId;
//	}
//
//	public void setLanguageId(long languageId) {
//		this.languageId = languageId;
//	}
//
//	public long getSkinId() {
//		return skinId;
//	}
//
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	public String getTypeName() {
//		return typeName;
//	}
//
//	public void setTypeName(String typeName) {
//		this.typeName = typeName;
//	}
//
//	public String getLanguageName() {
//		return languageName;
//	}
//
//	public void setLanguageName(String languageName) {
//		this.languageName = languageName;
//	}
//
//	public String getSkinName() {
//		return skinName;
//	}
//
//	public void setSkinName(String skinName) {
//		this.skinName = skinName;
//	}
//
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
//	}
//
//	public String getWriterTxt() throws SQLException {
//		return CommonUtil.getWriterName(writerId);
//	}
//
//	public UploadedFile getFile() {
//		return file;
//	}
//
//	public void setFile(UploadedFile file) throws IOException {
//		this.file = file;
//		if (null != file) {
//			template = CommonUtil.convertStreamToString(this.file.getInputStream());
//		}
//	}
//
//	public String displayTemplate() {
//		FacesContext context = FacesContext.getCurrentInstance();
//        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{mailBoxTemplate}", MailBoxTemplate.class);
//        ve.getValue(context.getELContext());
//        ve.setValue(context.getELContext(), this);
//		display = true;
//		return null;
//	}
//
//	public boolean isDisplay() {
//		return display;
//	}
//
//	public void setDisplay(boolean display) {
//		this.display = display;
//	}
//
//	public long getPopupTypeId() {
//		return popupTypeId;
//	}
//
//	public void setPopupTypeId(long popupTypeId) {
//		this.popupTypeId = popupTypeId;
//	}
//
//	/**
//	 * toString implementation.
//	 */
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "MailBoxTemplate:" + ls +
//            "id: " + id + ls +
//            "name: " + name + ls +
//            "subject: " + subject + ls +
//            "typeId: " + typeId + ls +
//            "writerId: " + writerId + ls +
//            "typeId: " + typeId + ls +
//	        "timeCreated: " + timeCreated + ls;
//    }
//
//	public static String insertParameters(String template, String parameters) {
//    	try {
//    		Pattern regex = Pattern.compile("([^&]+)=([^&]+)",
//    			Pattern.CANON_EQ);
//    		Matcher matcher = regex.matcher(parameters);
//    		while (matcher.find()) {
//    			template = insertParameter(template, matcher.group(1), URLDecoder.decode(matcher.group(2), "UTF-8"));
//    		}
//    	} catch (Exception ex) {
//    	}
//    	return template;
//    }
//
//	public static String insertParameter(String template, String paramName, String paramValue) {
//    	template = template.replace("${" + paramName + "}", paramValue);
//    	template = template.replace("{" + paramName + "}", paramValue);
//
//    	return template;
//    }
//}
