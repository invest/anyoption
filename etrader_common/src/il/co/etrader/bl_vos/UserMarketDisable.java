package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;

import java.util.Date;

import org.apache.log4j.Logger;


public class UserMarketDisable implements java.io.Serializable {

	private static final long serialVersionUID = -878689810112661273L;
	private static final Logger logger = Logger.getLogger(UserMarketDisable.class);

	private long id;
	private long userId;
	private long marketId;
	private Date startDate;
	private Date endDate;
	private long writerId;
	private Date timeCreated;
	private long scheduled;
	private boolean isActive;
	private long period;      // disable period from start time (in minutes)
    private boolean isDev3; // if this user need dev 3 check
    private double winLose;
    private long currencyId;
    private long skinId;

	private String userName;
	private String marketName;
	private String startDateMin;
	private String startDateHour;
	private String skinName;
	private boolean isActiveTemp;
	private boolean isDev3Temp;

//	for api external user
    private long apiExternalUserId;
    private String apiExternalUserReference;
    private String userIdAndApiExternalUserId;

	public UserMarketDisable() {

		startDate = new Date();
		endDate = new Date();
		timeCreated = new Date();
		isActiveTemp = true;
        isDev3Temp = false;
        userIdAndApiExternalUserId = "0";
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the scheduled
	 */
	public long getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
		this.isActiveTemp = isActive;
	}

	/**
	 * @return the endDate Txt
	 */
	public String getEndDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(endDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @return the endDate Txt
	 */
	public String getStartDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(startDate, CommonUtil.getUtcOffset());
	}

	/**
	 * @return the startDateHour
	 */
	public String getStartDateHour() {
		return startDateHour;
	}

	/**
	 * @param startDateHour the startDateHour to set
	 */
	public void setStartDateHour(String startDateHour) {
		this.startDateHour = startDateHour;
	}

	/**
	 * @return the startDateMin
	 */
	public String getStartDateMin() {
		return startDateMin;
	}

	/**
	 * @param startDateMin the startDateMin to set
	 */
	public void setStartDateMin(String startDateMin) {
		this.startDateMin = startDateMin;
	}

	/**
	 * @return the period
	 */
	public long getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(long period) {
		this.period = period;
	}

	/**
	 * Get scheduledTxt
	 * @return
	 */
    public String getScheduledTxt() {
    	return InvestmentsManagerBase.getScheduledTxt(scheduled);
    }

    /**
	 * @return the marketName
	 */
	public String getMarketName() {
        if (null != marketName) {
            return marketName;
        }
        return "All";
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	
	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public String getSkinName() {
		return CommonUtil.getMessage(skinName, null);
	}

	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
     * Get isActiveTxt
     * @return
     */
    public String getActiveTxt(){

    	String str = "";
    	if ( isActive ) {
    		str = CommonUtil.getMessage("users.market.disable.active", null);
    	} else {
    		str = CommonUtil.getMessage("users.market.disable.Inactive", null);
    	}

    	return str;
    }

    /**
     * Get is dev3 Txt
     * @return
     */
    public String getdev3Txt(){

        String str = "";
        if ( isDev3 ) {
            str = CommonUtil.getMessage("users.market.disable.active", null);
        } else {
            str = CommonUtil.getMessage("users.market.disable.Inactive", null);
        }

        return str;
    }

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
    public String toString() {
	    final String ls = System.getProperty("line.separator");

	    String retValue = "";

	    retValue = "UserMarketDisable ( "
	        + super.toString() + ls
	        + "id = " + this.id + ls
	        + "userId = " + this.userId + ls
	        + "marketId = " + this.marketId + ls
	        + "startDate = " + this.startDate + ls
	        + "endDate = " + this.endDate + ls
	        + "writerId = " + this.writerId + ls
	        + "timeCreated = " + this.timeCreated + ls
	        + "scheduled = " + this.scheduled + ls
	        + "isActive = " + this.isActive + ls
	        + "period = " + this.period + ls
	        + "userName = " + this.userName + ls
            + "is Dev3 = " + this.isDev3 + ls
            + "win lose = " + this.winLose + ls
            + "currency id = " + this.currencyId + ls
            + "skinId = " + this.skinId + ls
            + "api External User Id = " + this.apiExternalUserId + ls
	        + " )";

	    return retValue;
	}

    /**
     * @return the isDev3
     */
    public boolean isDev3() {
        return isDev3;
    }

    /**
     * @param isDev3 the isDev3 to set
     */
    public void setDev3(boolean isDev3) {
        this.isDev3 = isDev3;
        this.isDev3Temp = isDev3;
    }

    /**
     * @return the winLose
     */
    public String getWinLoseWithCurrency() {
        return CommonUtil.getMessage(CommonUtil.getCurrencySymbol(currencyId), null) + winLose;
    }

    /**
     * @return the winLose
     */
    public double getWinLose() {
        return winLose;
    }

    /**
     * @param winLose the winLose to set
     */
    public void setWinLose(double winLose) {
        this.winLose = winLose;
    }

    public boolean isWinLosePositive() {
        if (winLose > 0) {
            return true;
        }
        return false;
    }

    /**
     * @return the currencyId
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the apiExternalUserId
     */
    public long getApiExternalUserId() {
        return apiExternalUserId;
    }

    /**
     * @param apiExternalUserId the apiExternalUserId to set
     */
    public void setApiExternalUserId(long apiExternalUserId) {
        this.apiExternalUserId = apiExternalUserId;
    }

    /**
     * @return the apiExternalUserReference
     */
    public String getApiExternalUserReference() {
        return apiExternalUserReference;
    }

    /**
     * @param apiExternalUserReference the apiExternalUserReference to set
     */
    public void setApiExternalUserReference(String apiExternalUserReference) {
        this.apiExternalUserReference = apiExternalUserReference;
    }

    /**
     * @return the userIdAndApiExternalUserId
     */
    public String getUserIdAndApiExternalUserId() {
        return userIdAndApiExternalUserId;
    }

    /**
     * @param userIdAndApiExternalUserId the userIdAndApiExternalUserId to set
     */
    public void setUserIdAndApiExternalUserId(String userIdAndApiExternalUserId) {
        this.userIdAndApiExternalUserId = userIdAndApiExternalUserId;
        try {
            if (null != userIdAndApiExternalUserId && !userIdAndApiExternalUserId.equals("0")) {
                String[] userIdArr = userIdAndApiExternalUserId.split("_");
                userId = Long.parseLong(userIdArr[0]);
                apiExternalUserId = Long.parseLong(userIdArr[1]);
            }
        } catch (NumberFormatException e) {
            logger.error("cant get user id and external user id " + userIdAndApiExternalUserId + " using 0 for both");
        }
    }
    
	/**
	 * @return the isActiveTemp
	 */
	public boolean isActiveTemp() {
		return isActiveTemp;
	}

	/**
	 * @param isActiveTemp the isActiveTemp to set
	 */
	public void setActiveTemp(boolean isActiveTemp) {
		this.isActiveTemp = isActiveTemp;
	}

	/**
	 * @return the isDev3Temp
	 */
	public boolean isDev3Temp() {
		return isDev3Temp;
	}

	/**
	 * @param isDev3Temp the isDev3Temp to set
	 */
	public void setDev3Temp(boolean isDev3Temp) {
		this.isDev3Temp = isDev3Temp;
	}
	
	/**
	 * update IsActive
	 */
	public void updateIsActive(){
		this.isActive = this.isActiveTemp;
	}
	
	/**
	 * update IsDev3
	 */
	public void updateIsDev3(){
		this.isDev3 = this.isDev3Temp;
	}
}