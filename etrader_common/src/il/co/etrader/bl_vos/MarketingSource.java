package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

/**
 * Marketing sources class
 *
 * @author Kobi.
 */
public class MarketingSource implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final long GOOGLE = 25;
    public static final long MSN = 31;
    public static final long FACEBOOK = 28;
    public static final long YANDEX = 105;
    public static final long VKONTAKTE = 320;
    public static final long YOTTOS = 333;
    public static final long YAHOO_PPC = 341;
    public static final long MERLIN_NETWORKS_PPC = 591;
    public static final long BAIDU = 569;

    protected long id;
    protected String name;
    protected long writerId;
    protected Date timeCreated;
    protected boolean isKeepDailyData;
    protected boolean isSpecialMail;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingSource:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

	/**
	 * @return the isKeepDailyData
	 */
	public boolean isKeepDailyData() {
		return isKeepDailyData;
	}

	/**
	 * @param isKeepDailyData the isKeepDailyData to set
	 */
	public void setKeepDailyData(boolean isKeepDailyData) {
		this.isKeepDailyData = isKeepDailyData;
	}

	public boolean isSpecialMail() {
		return isSpecialMail;
	}

	public void setSpecialMail(boolean isSpecialMail) {
		this.isSpecialMail = isSpecialMail;
	}

}