//package il.co.etrader.bl_vos;
//
//public class BankBranches {
//
//    private Long id;
//    private String branchName;
//    private Long branchCode;
//    private Long bankId;
//
//    public BankBranches() {}
//
//    /**
//     * @return the id
//     */
//    public Long getId() {
//        return id;
//    }
//
//    /**
//     * @param id the id to set
//     */
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    /**
//     * @return the bankId
//     */
//    public Long getBankId() {
//        return bankId;
//    }
//
//    /**
//     * @param bankId the bankId to set
//     */
//    public void setBankId(Long bankId) {
//        this.bankId = bankId;
//    }
//
//    /**
//     * @return the branchCode
//     */
//    public Long getBranchCode() {
//        return branchCode;
//    }
//
//    /**
//     * @param branchCode the branchCode to set
//     */
//    public void setBranchCode(Long branchCode) {
//        this.branchCode = branchCode;
//    }
//
//    /**
//     * @return the branchName
//     */
//    public String getBranchName() {
//        return branchName;
//    }
//
//    /**
//     * @param branchName the branchName to set
//     */
//    public void setBranchName(String branchName) {
//        this.branchName = branchName;
//    }
//
//}
