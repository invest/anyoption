//package il.co.etrader.bl_vos;
//
///**
// * Skin vo class
// * @author - Eyal stop copying !!!
// */
//public class TreeItem implements java.io.Serializable {
//	private int market_id;
//	private String market_name;
//	private int market_group_id;
//	private String group_name;
//	private int group_priority;
//	private int skin_id;
//	private String market_subGroup_display_name;
//    private boolean printGroup;
//    private boolean printGeoGroup;
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "TreeItem:" + ls +
//	        super.toString() + ls +
//	        "market id = " + market_id + ls +
//	        "market disaply name = " + market_name + ls +
//	        "market group id = " + market_group_id + ls +
//	        "market group name = " + group_name + ls +
//	        "market group priority = " + group_priority + ls +
//	        "skin id = " + skin_id + ls +
//	        "market subGroup display name = " + market_subGroup_display_name + ls +
//            "printGroup = " + printGroup + ls +
//            "pringGeoGroup = " + printGeoGroup + ls;
//	}
//
//    public String getGroup_name() {
//		return group_name;
//	}
//
//	public void setGroup_name(String group_name) {
//		this.group_name = group_name;
//	}
//
//	public int getGroup_priority() {
//		return group_priority;
//	}
//
//	public void setGroup_priority(int group_priority) {
//		this.group_priority = group_priority;
//	}
//
//	public int getMarket_group_id() {
//		return market_group_id;
//	}
//
//	public void setMarket_group_id(int market_group_id) {
//		this.market_group_id = market_group_id;
//	}
//
//	public int getMarket_id() {
//		return market_id;
//	}
//
//	public void setMarket_id(int market_id) {
//		this.market_id = market_id;
//	}
//
//	public String getMarket_name() {
//		return market_name;
//	}
//
//	public void setMarket_name(String market_name) {
//		this.market_name = market_name;
//	}
//
//	public String getMarket_subGroup_display_name() {
//		return market_subGroup_display_name;
//	}
//
//	public void setMarket_subGroup_display_name(String market_subGroup_display_name) {
//		this.market_subGroup_display_name = market_subGroup_display_name;
//	}
//
//	public int getSkin_id() {
//		return skin_id;
//	}
//
//	public void setSkin_id(int skin_id) {
//		this.skin_id = skin_id;
//	}
//
//    public boolean isPrintGeoGroup() {
//        return printGeoGroup;
//    }
//
//    public void setPrintGeoGroup(boolean printGeoGroup) {
//        this.printGeoGroup = printGeoGroup;
//    }
//
//    public boolean isPrintGroup() {
//        return printGroup;
//    }
//
//    public void setPrintGroup(boolean printGroup) {
//        this.printGroup = printGroup;
//    }
//    public boolean getIsCommodityOrArabicMarket(){
//    	if(market_id==137 || market_id==20 || market_id==138 || market_id==21 || market_id==248 || market_id==249 || market_id==250 || market_id==371)
//    		return true;
//    	else
//    		return false;
//    }
//}