//package il.co.etrader.bl_vos;
//
//
//import il.co.etrader.util.CommonUtil;
//
//import java.util.Date;
//
//import javax.faces.event.ValueChangeEvent;
//
//public class ChargeBack extends com.anyoption.common.bl_vos.ChargeBack implements java.io.Serializable{
//
//
//	private static final long serialVersionUID = -6078429229848514148L;
//
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(getTimeCreated(), CommonUtil.getUtcOffset(getUtcOffsetCreated()));
//	}
//	
//	public String getAmountTxt() {
//		return CommonUtil.displayAmount(getAmount(), getCurrencyId());
//	}
//	
//	public void updateState(ValueChangeEvent ev){
//		if (getId()>0){
//			setStateChanged(true);
//			setTimeStateChanged(new Date());
//		}
//	}
//
//	public String getStateName(){
//		if (STATE_CHB == getState()){
//			return CommonUtil.getMessage("chargebacks.state.chb", null);
//		}else if(STATE_POS == getState()){
//			return CommonUtil.getMessage("chargebacks.state.pos", null);
//		}
//
//		return "";
//	}
//
//}
