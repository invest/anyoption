package il.co.etrader.bl_vos;

public class MarkVipHelper {
	private long userId;
	private long issueId;
	private long currEntryId;
	
	public MarkVipHelper(long userId, long currEntryId) {
		super();
		this.userId = userId;
		this.currEntryId = currEntryId;
	}

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getIssueId() {
		return issueId;
	}
	
	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}

	public long getCurrEntryId() {
		return currEntryId;
	}

	public void setCurrEntryId(long currEntryId) {
		this.currEntryId = currEntryId;
	}
} 