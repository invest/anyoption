//package il.co.etrader.bl_vos;
//
//public class SkinPaymentMethod implements java.io.Serializable, Cloneable {
//
//	/**
//	 *
//	 */
//	private static final long serialVersionUID = 5625968632963357619L;
//	private long id;
//	private long skinId;
//	private long transactionTypeId;
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public long getSkinId() {
//		return skinId;
//	}
//
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	public long getTransactionTypeId() {
//		return transactionTypeId;
//	}
//
//	public void setTransactionTypeId(long transactionTypeId) {
//		this.transactionTypeId = transactionTypeId;
//	}
//
//	/**
//	 * Override Clone function for PaymentMethod Object
//	 * @return cloned PaymentMethod instance
//	 */
//	@Override
//	public Object clone() throws CloneNotSupportedException {
//		return (SkinPaymentMethod)super.clone();
//	}
//
//}
