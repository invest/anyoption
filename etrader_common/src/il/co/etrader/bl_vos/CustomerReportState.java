package il.co.etrader.bl_vos;


public class CustomerReportState implements java.io.Serializable{


	public static final int CUSTOMER_REPORT_FINISH_ALL_INSERT = 10;
	public static final int CUSTOMER_REPORT_FINISH_SEND_REPORTS = 11;
	public static final int CUSTOMER_REPORT_STATE_ERROR = 999;
	
	public static final int CUSTOMER_REPORT_MONTHLY = 3;
	private static final long serialVersionUID = -2928816902029146858L;

	private long reportId;
	private long reportType;
	private long reportState;
	private String logMsg;
	

	
	
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public long getReportType() {
		return reportType;
	}
	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	public long getReportState() {
		return reportState;
	}
	public void setReportState(long reportState) {
		this.reportState = reportState;
	}
	public String getLogMsg() {
		return logMsg;
	}
	public void setLogMsg(String logMsg) {
		this.logMsg = logMsg;
	}
}
