//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//
//import java.util.Date;
//
//import javax.faces.event.ValueChangeEvent;
//
//public class CreditCard implements java.io.Serializable{
//
//	private static final long serialVersionUID = 5729952998659870761L;
//
////	private long id;
////	private long userId;
////	private long typeId;
////	private long ccNumber;
////	private String ccPass;
////	private Date timeCreated;
////	private Date timeModified;
////	private String expMonth;
////	private String expYear;
////	private String holderName;
////	private String holderIdNum;
////	private int isVisible;
////	private int isAllowed;
////	private long countryId; // country id from bins table if found bin, otherwise by users' country id
////	private long typeIdByBin; // cc type id from bins table if found bin, otherwise by users' choice
////	private String recurringTransaction; //empty_string if the card haven't used in TBI yet
////	private long BIN;
//
////	private CreditCardType type;
////    protected String utcOffsetCreated;
////    protected String utcOffsetModified;
//
////    private boolean isDocumentsSent;
////	private boolean selected;
////	private boolean isBelongsToAccountHolder;
////	private boolean isPowerOfAttorney;
////	private boolean isVerificationCall;
////	private boolean isSentHolderIdCopy;
////	private boolean isSentCcCopy;
////	private boolean isUserHaveIdCopy;
////	private boolean isUserSentWithdrawalForm;
//
//
////	protected boolean creditEnabled;       // true in case the cc allow credit(bookBack)
////	protected long creditAmount;        // credit amount that allow in case cftEnabled = false
////	protected boolean cftAvailable;     // exists in cft_bins table
////	protected boolean nonCftAvailable;
//
////	private long sumDeposits;
////	private Date depositsFrom;
////	private Date depositsTo;
//
////	//risk issue
////	private boolean isSucceedDeposit;
////	private RequestedFiles reqRiskIssueScreen;
////	private RequestedFiles request;
////	private String filesRiskStatus;
////	private int filesRiskStatusId;
////	private boolean needUpdateStatus;
////	private String comment;
////	private boolean changed;
//
////	protected long clearingProviderId;    // for credit card withdrawal
////	protected long cftClearingProviderId;    // for credit card withdrawal
//
////	protected String countryName;
////	protected String typeNameByBin;
////	protected boolean isManuallyAllowed;
//
////	public CreditCard() {
////		id=0;
////	}
//
////	public boolean isSelected() {
////		return selected;
////	}
//
////	public String getTimeCreatedTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(timeCreated, utcOffsetCreated);
////	}
////	public String getTimeModifiedTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(timeModified, utcOffsetModified);
////	}
////	public void setSelected(boolean selected) {
////		this.selected = selected;
////	}
////	public CreditCardType getType() {
////		return type;
////	}
////	public void setType(CreditCardType type) {
////		this.type = type;
////	}
////	public long getCcNumber() {
////		return ccNumber;
////	}
////	public String getCcNumberLast4() {
////		String ccnum=String.valueOf(ccNumber);
////		return ccnum.substring(ccnum.length()-4);
////	}
////	public String getCcNumberXXX() {
////		String ccnum=String.valueOf(ccNumber);
////		String out="";
////		for (int i=0;i<ccnum.length()-4;i++)
////			out+="X";
////		return out+ccnum.substring(ccnum.length()-4);
////	}
////	public void setCcNumber(long ccNumber) {
////		this.ccNumber = ccNumber;
////	}
////	public String getCcPass() {
////		// later the logic for AMEX will come...
////		return ccPass;
////	}
////	public void setCcPass(String ccPass) {
////		this.ccPass = ccPass;
////	}
////	public String getExpMonth() {
////		return expMonth;
////	}
////	public void setExpMonth(String expMonth) {
////		this.expMonth = expMonth;
////	}
////	public String getExpYear() {
////		return expYear;
////	}
////	public void setExpYear(String expYear) {
////		this.expYear = expYear;
////	}
////	public String getHolderIdNum() {
////		return holderIdNum;
////	}
////	public void setHolderIdNum(String holderIdNum) {
////		this.holderIdNum = holderIdNum;
////	}
////	public String getHolderName() {
////		return holderName;
////	}
////	public void setHolderName(String holderName) {
////		this.holderName = holderName;
////	}
////	public long getId() {
////		return id;
////	}
////	public void setId(long id) {
////		this.id = id;
////	}
////	public boolean getIsAllowedBoolean() {
////		return isAllowed==1;
////	}
////	public int getIsVisible() {
////		return isVisible;
////	}
////	public boolean getIsVisibleBoolean() {
////		return (isVisible==1);
////	}
////	public void setIsVisible(int isVisible) {
////		this.isVisible = isVisible;
////	}
////	public Date getTimeCreated() {
////		return timeCreated;
////	}
////	public void setTimeCreated(Date timeCcreated) {
////		this.timeCreated = timeCcreated;
////	}
////	public Date getTimeModified() {
////		return timeModified;
////	}
////	public void setTimeModified(Date timeModified) {
////		this.timeModified = timeModified;
////	}
////	public long getTypeId() {
////		return typeId;
////	}
////	public void setTypeId(long typeId) {
////		this.typeId = typeId;
////	}
////	public long getUserId() {
////		return userId;
////	}
////	public void setUserId(long userId) {
////		this.userId = userId;
////	}
////	public int getIsAllowed() {
////		return isAllowed;
////	}
////	public void setIsAllowed(int isAllowed) {
////		this.isAllowed = isAllowed;
////	}
//
////	public void setIsAllowedBoolean(boolean b) {
////		if (b)
////			isAllowed=1;
////		else
////			isAllowed=0;
////	}
////
////	public void setIsVisibleBoolean(boolean b) {
////		if (b)
////			isVisible=1;
////		else
////			isVisible=0;
////	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
////	public String toString()
////	{
////	    final String TAB = " \n ";
////	    String retValue = "";
////
////	    retValue = "CreditCard ( "
////	        + super.toString() + TAB
////	        + "id = " + this.id + TAB
////	        + "userId = " + this.userId + TAB
////	        + "typeId = " + this.typeId + TAB
////	        + "ccNumber = " + this.ccNumber + TAB
////	        + "ccPass = " + this.ccPass + TAB
////	        + "timeCreated = " + this.timeCreated + TAB
////	        + "timeModified = " + this.timeModified + TAB
////	        + "expMonth = " + this.expMonth + TAB
////	        + "expYear = " + this.expYear + TAB
////	        + "holderName = " + this.holderName + TAB
////	        + "holderIdNum = " + this.holderIdNum + TAB
////	        + "isVisible = " + this.isVisible + TAB
////	        + "isAllowed = " + this.isAllowed + TAB
////	        + "type = " + this.type + TAB
////	        + "selected = " + this.selected + TAB
////	        + " )";
////
////	    return retValue;
////	}
//
////	public String toStringForComments()
////	{
////	    final String TAB = " | ";
////	    String retValue = "";
////
////	    retValue =
////
////	         CommonUtil.getMessage("creditcards.ccPass", null)+" = " + this.ccPass + TAB+
////	         CommonUtil.getMessage("creditcards.expDateMonth", null)+" = " + this.expMonth + TAB+
////	         CommonUtil.getMessage("creditcards.expDateYear", null)+" = " + this.expYear + TAB+
////	         CommonUtil.getMessage("creditcards.holderIdNum", null)+" = " + this.holderIdNum + TAB;
////
////	    return retValue;
////	}
////
////	public String getToStringForComments()	{
////	    final String TAB = " | ";
////	    String retValue = "";
////	    retValue =
////	         CommonUtil.getMessage("creditcards.ccPass", null)+" = " + this.ccPass + TAB+
////	         CommonUtil.getMessage("creditcards.expDateMonth", null)+" = " + this.expMonth + TAB+
////	         CommonUtil.getMessage("creditcards.expDateYear", null)+" = " + this.expYear + TAB+
////	         CommonUtil.getMessage("creditcards.holderIdNum", null)+" = " + this.holderIdNum + TAB;
////	    return retValue;
////	}
//
////	public String getTypeIdStr() {
////		return String.valueOf(typeId);
////	}
////
////	public void setTypeIdStr(String typeIdStr) {
////		this.typeId = Long.parseLong(typeIdStr);
////	}
//
////	/**
////	 * @return the utcOffsetCreated
////	 */
////	public String getUtcOffsetCreated() {
////		return utcOffsetCreated;
////	}
////
////	/**
////	 * @param utcOffsetCreated the utcOffsetCreated to set
////	 */
////	public void setUtcOffsetCreated(String utcOffsetCreated) {
////		this.utcOffsetCreated = utcOffsetCreated;
////	}
////
////	/**
////	 * @return the utcOffsetModified
////	 */
////	public String getUtcOffsetModified() {
////		return utcOffsetModified;
////	}
////
////	/**
////	 * @param utcOffsetModified the utcOffsetModified to set
////	 */
////	public void setUtcOffsetModified(String utcOffsetModified) {
////		this.utcOffsetModified = utcOffsetModified;
////	}
//
////	/**
////	 * @return the creditEnabled
////	 */
////	public boolean isCreditEnabled() {
////		return creditEnabled;
////	}
////
////	/**
////	 * @param creditEnabled the creditEnabled to set
////	 */
////	public void setCreditEnabled(boolean creditEnabled) {
////		this.creditEnabled = creditEnabled;
////	}
////
////	/**
////	 * @return the creditAmount
////	 */
////	public long getCreditAmount() {
////		return creditAmount;
////	}
////
////	/**
////	 * @param creditAmount the creditAmount to set
////	 */
////	public void setCreditAmount(long creditAmount) {
////		this.creditAmount = creditAmount;
////	}
////
////	/**
////	 * @return the cftAvailable
////	 */
////	public boolean isCftAvailable() {
////		return cftAvailable;
////	}
////
////	/**
////	 * @param cftAvailable the cftAvailable to set
////	 */
////	public void setCftAvailable(boolean cftAvailable) {
////		this.cftAvailable = cftAvailable;
////	}
////
////	/**
////	 * @return the nonCftAvailable
////	 */
////	public boolean isNonCftAvailable() {
////		return nonCftAvailable;
////	}
////
////	/**
////	 * @param nonCftAvailable the nonCftAvailable to set
////	 */
////	public void setNonCftAvailable(boolean nonCftAvailable) {
////		this.nonCftAvailable = nonCftAvailable;
////	}
////
////	public boolean isDocumentsSent() {
////		return isDocumentsSent;
////	}
////
////	public void setDocumentsSent(boolean isDocumentsSent) {
////		this.isDocumentsSent = isDocumentsSent;
////	}
////
////	/**
////	 * @return the clearingProviderId
////	 */
////	public long getClearingProviderId() {
////		return clearingProviderId;
////	}
////
////	/**
////	 * @param clearingProviderId the clearingProviderId to set
////	 */
////	public void setClearingProviderId(long clearingProviderId) {
////		this.clearingProviderId = clearingProviderId;
////	}
////
////	public boolean isBelongsToAccountHolder() {
////		return isBelongsToAccountHolder;
////	}
////
////	public void setBelongsToAccountHolder(boolean isBelongsToAccountHolder) {
////		this.isBelongsToAccountHolder = isBelongsToAccountHolder;
////	}
////
////	public boolean isPowerOfAttorney() {
////		return isPowerOfAttorney;
////	}
////
////	public void setPowerOfAttorney(boolean isPowerOfAttorney) {
////		this.isPowerOfAttorney = isPowerOfAttorney;
////	}
////
////	public boolean isSentCcCopy() {
////		return isSentCcCopy;
////	}
////
////	public void setSentCcCopy(boolean isSentCcCopy) {
////		this.isSentCcCopy = isSentCcCopy;
////	}
////
////	public boolean isSentHolderIdCopy() {
////		return isSentHolderIdCopy;
////	}
////
////	public void setSentHolderIdCopy(boolean isSentHolderIdCopy) {
////		this.isSentHolderIdCopy = isSentHolderIdCopy;
////	}
////
////	public boolean isVerificationCall() {
////		return isVerificationCall;
////	}
////
////	public void setVerificationCall(boolean isVerificationCall) {
////		this.isVerificationCall = isVerificationCall;
////	}
////
////	public boolean isUserHaveIdCopy() {
////		return isUserHaveIdCopy;
////	}
////
////	public void setUserHaveIdCopy(boolean isUserHaveIdCopy) {
////		this.isUserHaveIdCopy = isUserHaveIdCopy;
////	}
////
////	public boolean isUserSentWithdrawalForm() {
////		return isUserSentWithdrawalForm;
////	}
////
////	public void setUserSentWithdrawalForm(boolean isUserSentWithdrawalForm) {
////		this.isUserSentWithdrawalForm = isUserSentWithdrawalForm;
////	}
////
////	public Date getDepositsFrom() {
////		return depositsFrom;
////	}
////
////	public void setDepositsFrom(Date depositsFrom) {
////		this.depositsFrom = depositsFrom;
////	}
////
////	public Date getDepositsTo() {
////		return depositsTo;
////	}
////
////	public void setDepositsTo(Date depositsTo) {
////		this.depositsTo = depositsTo;
////	}
////
////	public long getSumDeposits() {
////		return sumDeposits;
////	}
////
////	public void setSumDeposits(long sumDeposits) {
////		this.sumDeposits = sumDeposits;
////	}
////
////	/**
////	 * @return the isSucceedDeposit
////	 */
////	public boolean isSucceedDeposit() {
////		return isSucceedDeposit;
////	}
////
////	/**
////	 * @param isSucceedDeposit the isSucceedDeposit to set
////	 */
////	public void setSucceedDeposit(boolean isSucceedDeposit) {
////		this.isSucceedDeposit = isSucceedDeposit;
////	}
//
////	public String getSucceedDeposits(){
////		if (isSucceedDeposit){
////			return "YES";
////		}
////		return "NO";
////	}
//
////	/**
////	 * @return the request
////	 */
////	public RequestedFiles getRequest() {
////		if(request == null){
////			request = new RequestedFiles();
////		}
////		return request;
////	}
////
////	/**
////	 * @param request the request to set
////	 */
////	public void setRequest(RequestedFiles request) {
////		this.request = request;
////	}
//
////	/**
////	 * @return the reqRiskIssueScreen
////	 */
////	public RequestedFiles getReqRiskIssueScreen() {
////		if(reqRiskIssueScreen == null){
////			reqRiskIssueScreen = new RequestedFiles();
////		}
////		return reqRiskIssueScreen;
////	}
//
////	/**
////	 * @param reqRiskIssueScreen the reqRiskIssueScreen to set
////	 */
////	public void setReqRiskIssueScreen(RequestedFiles reqRiskIssueScreen) {
////		this.reqRiskIssueScreen = reqRiskIssueScreen;
////	}
//
////	/**
////	 * @return the filesRiskStatus
////	 */
////	public String getFilesRiskStatus() {
////		return filesRiskStatus;
////	}
////
////	/**
////	 * @param filesRiskStatus the filesRiskStatus to set
////	 */
////	public void setFilesRiskStatus(String filesRiskStatus) {
////		this.filesRiskStatus = filesRiskStatus;
////	}
////
////	/**
////	 * @return the filesRiskStatusId
////	 */
////	public int getFilesRiskStatusId() {
////		return filesRiskStatusId;
////	}
////
////	/**
////	 * @param filesRiskStatusId the filesRiskStatusId to set
////	 */
////	public void setFilesRiskStatusId(int filesRiskStatusId) {
////		this.filesRiskStatusId = filesRiskStatusId;
////	}
////
////	/**
////	 * @return the needUpdateStatus
////	 */
////	public boolean isNeedUpdateStatus() {
////		return needUpdateStatus;
////	}
////
////	/**
////	 * @param needUpdateStatus the needUpdateStatus to set
////	 */
////	public void setNeedUpdateStatus(boolean needUpdateStatus) {
////		this.needUpdateStatus = needUpdateStatus;
////	}
//
////	public void updateChange(ValueChangeEvent event) throws Exception {
////		changed = !changed;
////	}
//
////	public String getComment() {
////		return comment;
////	}
////
////	public void setComment(String comment) {
////		this.comment = comment;
////	}
////
////	public boolean isChanged() {
////		return changed;
////	}
////
////	public void setChanged(boolean changed) {
////		this.changed = changed;
////	}
////
////	/**
////	 * @return the cftClearingProviderId
////	 */
////	public long getCftClearingProviderId() {
////		return cftClearingProviderId;
////	}
////
////	/**
////	 * @param cftClearingProviderId the cftClearingProviderId to set
////	 */
////	public void setCftClearingProviderId(long cftClearingProviderId) {
////		this.cftClearingProviderId = cftClearingProviderId;
////	}
//
////	public String getBinStr(){
////		return String.valueOf(ccNumber).subSequence(0, 6).toString();
////	}
//
////	/**
////	 * @return the countryId
////	 */
////	public long getCountryId() {
////		return countryId;
////	}
////
////	/**
////	 * @param countryId the countryId to set
////	 */
////	public void setCountryId(long countryId) {
////		this.countryId = countryId;
////	}
////
////	/**
////	 * @return the typeIdByBin
////	 */
////	public long getTypeIdByBin() {
////		return typeIdByBin;
////	}
////
////	/**
////	 * @param typeIdByBin the typeIdByBin to set
////	 */
////	public void setTypeIdByBin(long typeIdByBin) {
////		this.typeIdByBin = typeIdByBin;
////	}
////
////	/**
////	 * @return the countryName
////	 */
////	public String getCountryName() {
////		return countryName;
////	}
////
////	/**
////	 * @param countryName the countryName to set
////	 */
////	public void setCountryName(String countryName) {
////		this.countryName = countryName;
////	}
////
////	/**
////	 * @return the typeNameByBin
////	 */
////	public String getTypeNameByBin() {
////		return typeNameByBin;
////	}
////
////	/**
////	 * @param typeNameByBin the typeNameByBin to set
////	 */
////	public void setTypeNameByBin(String typeNameByBin) {
////		this.typeNameByBin = typeNameByBin;
////	}
////
////	public boolean isManuallyAllowed() {
////		return isManuallyAllowed;
////	}
////
////	public void setManuallyAllowed(boolean isManuallyAllowed) {
////		this.isManuallyAllowed = isManuallyAllowed;
////	}
////	
////	public String getRecurringTransaction() {
////		return recurringTransaction;
////	}
////
////	public void setRecurringTransaction(String recurringTransaction) {
////		this.recurringTransaction = recurringTransaction;
////	}
////
////	public long getBIN() {
////		return BIN;
////	}
////
////	public void setBIN(long bIN) {
////		BIN = bIN;
////	}
//}
