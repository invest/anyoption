package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;


public class ExchangesHolidays implements java.io.Serializable {
    private long id;
    private Date holiday;
    private long exchangeId;
    private String exchangeName;
    private boolean halfDay;

    public String getHolidayTxt() {
		return CommonUtil.getDateFormat(holiday);
	}

	public long getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(long exchangeId) {
		this.exchangeId = exchangeId;
	}

	public boolean isHalfDay() {
		return halfDay;
	}

	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}

	public Date getHoliday() {
		return holiday;
	}

	public void setHoliday(Date holiday) {
		this.holiday = holiday;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ExchangesHolidays ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "holiday = " + this.holiday + TAB
	        + "exchangeId = " + this.exchangeId + TAB
	        + "exchangeName = " + this.exchangeName + TAB
	        + "halfDay = " + this.halfDay + TAB
	        + " )";

	    return retValue;
	}



}