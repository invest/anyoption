//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//
//import java.sql.SQLException;
//import java.util.Date;
//
//public class Bonus extends com.anyoption.common.beans.Bonus implements java.io.Serializable{
//
//	private static final long serialVersionUID = 5289496849546211085L;
//
//    public long id;
//    public Date startDate;
//    public Date endDate;
//    public long wageringParameter;
//    public long defaultPeriod;
//    public long autoRenewInd;
//    public long renewFrequency;
//    public long typeId;
//    public long writerId;
//    public Date timeCreated;
//    public long numberOfActions;
//    public String typeName;
//    public String name;
//    public long classType;
//    public String classTypeName;
//    private boolean isHasSteps;
//    private double oddsWin;
//	private double oddsLose;
//	private String condition;
//	private boolean isCheckLimit;
//	private long turnoverParam;
//	private int roundUpType;
//
//
//	public boolean isCheckLimit() {
//		return isCheckLimit;
//	}
//	public void setCheckLimit(boolean isCheckLimit) {
//		this.isCheckLimit = isCheckLimit;
//	}
//	public boolean isHasSteps() {
//		return isHasSteps;
//	}
//	public void setHasSteps(boolean isHasSteps) {
//		this.isHasSteps = isHasSteps;
//	}
//	/**
//	 * @return the autoRenewInd
//	 */
//	public long getAutoRenewInd() {
//		return autoRenewInd;
//	}
//	/**
//	 * @param autoRenewInd the autoRenewInd to set
//	 */
//	public void setAutoRenewInd(long autoRenewInd) {
//		this.autoRenewInd = autoRenewInd;
//	}
//	/**
//	 * @return the defaultPeriod
//	 */
//	public long getDefaultPeriod() {
//		return defaultPeriod;
//	}
//	/**
//	 * @param defaultPeriod the defaultPeriod to set
//	 */
//	public void setDefaultPeriod(long defaultPeriod) {
//		this.defaultPeriod = defaultPeriod;
//	}
//	/**
//	 * @return the endDate
//	 */
//	public Date getEndDate() {
//		return endDate;
//	}
//
//	/**
//	 * @return the endDate as string
//	 */
//	public String getEndDateTxt() {
//		return CommonUtil.getDateFormat(endDate, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @param endDate the endDate to set
//	 */
//	public void setEndDate(Date endDate) {
//		this.endDate = endDate;
//	}
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * @return the numberOfActions
//	 */
//	public long getNumberOfActions() {
//		return numberOfActions;
//	}
//	/**
//	 * @param numberOfActions the numberOfActions to set
//	 */
//	public void setNumberOfActions(long numberOfActions) {
//		this.numberOfActions = numberOfActions;
//	}
//
//	/**
//	 * @return the renewFrequency
//	 */
//	public long getRenewFrequency() {
//		return renewFrequency;
//	}
//	/**
//	 * @param renewFrequency the renewFrequency to set
//	 */
//	public void setRenewFrequency(long renewFrequency) {
//		this.renewFrequency = renewFrequency;
//	}
//	/**
//	 * @return the startDate
//	 */
//	public Date getStartDate() {
//		return startDate;
//	}
//
//	/**
//	 * @return the startDate as String
//	 */
//	public String getStartDateTxt() {
//		return CommonUtil.getDateFormat(startDate, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @param startDate the startDate to set
//	 */
//	public void setStartDate(Date startDate) {
//		this.startDate = startDate;
//	}
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the typeId
//	 */
//	public long getTypeId() {
//		return typeId;
//	}
//
//	/**
//	 * @param typeId the typeId to set
//	 */
//	public void setTypeId(long typeId) {
//		this.typeId = typeId;
//	}
//	/**
//	 * @return the wageringParameter
//	 */
//	public long getWageringParameter() {
//		return wageringParameter;
//	}
//	/**
//	 * @param wageringParameter the wageringParameter to set
//	 */
//	public void setWageringParameter(long wageringParameter) {
//		this.wageringParameter = wageringParameter;
//	}
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//    /**
//     * Get writer name
//     * @return
//     * @throws SQLException
//     */
//    public String getWriterTxt() throws SQLException {
//        return CommonUtil.getWriterName(writerId);
//    }
//
//    /**
//     * @return the typeName
//     */
//    public String getTypeName() {
//        return typeName;
//    }
//
//    /**
//     * @param typeName the typeName to set
//     */
//    public void setTypeName(String typeName) {
//        this.typeName = typeName;
//    }
//    /**
//     * @return the name
//     */
//    public String getName() {
//        return name;
//    }
//    /**
//     * @param name the name to set
//     */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//	/**
//	 * @return the classType
//	 */
//	public long getClassType() {
//		return classType;
//	}
//	/**
//	 * @param classType the classType to set
//	 */
//	public void setClassType(long classType) {
//		this.classType = classType;
//	}
//	/**
//	 * @return the classTypeName
//	 */
//	public String getClassTypeName() {
//		return classTypeName;
//	}
//	/**
//	 * @param classTypeName the classTypeName to set
//	 */
//	public void setClassTypeName(String classTypeName) {
//		this.classTypeName = classTypeName;
//	}
//
//	/**
//	 * @return the oddsLose
//	 */
//	public double getOddsLose() {
//		return oddsLose;
//	}
//	/**
//	 * @param oddsLose the oddsLose to set
//	 */
//	public void setOddsLose(double oddsLose) {
//		this.oddsLose = oddsLose;
//	}
//	/**
//	 * @return the oddsWin
//	 */
//	public double getOddsWin() {
//		return oddsWin;
//	}
//	/**
//	 * @param oddsWin the oddsWin to set
//	 */
//	public void setOddsWin(double oddsWin) {
//		this.oddsWin = oddsWin;
//	}
//	/**
//	 * @return the condition
//	 */
//	public String getCondition() {
//		return CommonUtil.getMessage(condition,null);
//	}
//	/**
//	 * @param condition the condition to set
//	 */
//	public void setCondition(String condition) {
//		this.condition = condition;
//	}
//	/**
//	 * 
//	 * @return turnoverParam
//	 */
//	public long getTurnoverParam() {
//		return turnoverParam;
//	}
//	/**
//	 * 
//	 * @param turnoverParam the turnoverParam to set
//	 */
//	public void setTurnoverParam(long turnoverParam) {
//		this.turnoverParam = turnoverParam;
//	}
//	public int getRoundUpType() {
//		return roundUpType;
//	}
//	public void setRoundUpType(int roundUpType) {
//		this.roundUpType = roundUpType;
//	}
//
//}
