/**
 * 
 */
package il.co.etrader.bl_vos;

/**
 * @author pavelhe
 *
 */
public class ConstantInvestmentsStream {
	
	private long id;
	private String streamName;
	private Long skinId;
	private long investmentMinAmount;
	private long maxRows;
	private boolean isActive;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the streamName
	 */
	public String getStreamName() {
		return streamName;
	}
	/**
	 * @param streamName the streamName to set
	 */
	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}
	/**
	 * @return the skinId
	 */
	public Long getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(Long skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the investmentMinAmount
	 */
	public long getInvestmentMinAmount() {
		return investmentMinAmount;
	}
	/**
	 * @param investmentMinAmount the investmentMinAmount to set
	 */
	public void setInvestmentMinAmount(long investmentMinAmount) {
		this.investmentMinAmount = investmentMinAmount;
	}
	/**
	 * @return the maxRows
	 */
	public long getMaxRows() {
		return maxRows;
	}
	/**
	 * @param maxRows the maxRows to set
	 */
	public void setMaxRows(long maxRows) {
		this.maxRows = maxRows;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	@Override
	public String toString() {
		return "ConstantInvestmentsStream [id=" + id + ", streamName="
				+ streamName + ", skinId=" + skinId + ", investmentMinAmount="
				+ investmentMinAmount + ", maxRows=" + maxRows + ", isActive="
				+ isActive + "]";
	}
	
}
