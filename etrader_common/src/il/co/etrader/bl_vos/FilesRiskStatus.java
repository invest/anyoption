package il.co.etrader.bl_vos;

public class FilesRiskStatus implements java.io.Serializable{

	private long id;
	private String name;
	private int priority;
	private boolean supportStatus;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}


	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * @return the supportStatus
	 */
	public boolean isSupportStatus() {
		return supportStatus;
	}


	/**
	 * @param supportStatus the supportStatus to set
	 */
	public void setSupportStatus(boolean supportStatus) {
		this.supportStatus = supportStatus;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "IssueStatus ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "name = " + this.name + TAB
	        + "priority = " + this.priority + TAB
	        + "supportStatus = " + this.supportStatus + TAB
	        + " )";

	    return retValue;
	}


}
