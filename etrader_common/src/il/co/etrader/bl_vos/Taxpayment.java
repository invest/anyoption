package il.co.etrader.bl_vos;


import il.co.etrader.util.CommonUtil;

import java.util.Date;

import org.apache.log4j.Logger;

public class Taxpayment implements java.io.Serializable{
	private static final Logger logger = Logger.getLogger(TranInvestment.class);

	private int id;
	private int year;
	private int period;
	private Date timeCreated;
	private String utcOffsetCreated;
	private long win;
	private long lose;
	private long tax;
	private int currencyId;
	private String writer;

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return logger;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the lose
	 */
	public long getLose() {
		return lose;
	}


	/**
	 * @param lose the lose to set
	 */
	public void setLose(long lose) {
		this.lose = lose;
	}


	/**
	 * @return the period
	 */
	public int getPeriod() {
		return period;
	}


	/**
	 * @param period the period to set
	 */
	public void setPeriod(int period) {
		this.period = period;
	}


	/**
	 * @return the tax
	 */
	public long getTax() {
		return tax;
	}


	/**
	 * @param tax the tax to set
	 */
	public void setTax(long tax) {
		this.tax = tax;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}


	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}


	/**
	 * @return the win
	 */
	public long getWin() {
		return win;
	}


	/**
	 * @param win the win to set
	 */
	public void setWin(long win) {
		this.win = win;
	}


	/**
	 * @return the writerId
	 */
	public String getWriterId() {
		return writer;
	}


	/**
	 * @param writer the writerId to set
	 */
	public void setWriter(String writer) {
		this.writer = writer;
	}


	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}


	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
	}

	public String getWinTxt() {
		if (win==0)
			return "";
		return CommonUtil.displayAmount(win, currencyId);
	}
	public String getLoseTxt() {
		if (lose==0)
			return "";
		return CommonUtil.displayAmount(lose, currencyId);
	}

	public String getTaxTxt() {
		return CommonUtil.displayAmount(tax, currencyId);
	}


	public int getCurrencyId() {
		return currencyId;
	}


	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}


	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}


	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

}
