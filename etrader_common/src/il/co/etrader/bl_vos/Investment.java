//package il.co.etrader.bl_vos;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Random;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.managers.CurrenciesManagerBase;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.InvestmentFormatter;

//public class Investment extends com.anyoption.common.beans.Investment {
//    private static final Logger log = Logger.getLogger(Investment.class);

//    // TODO: load that config from db cache it in the manager and send it to flash
//    public static final int[] BINARY0100_CONTRACTS_STEPS = {0, 10, 2, 2, 2, 4, 50, 10, 2000, 20, 2, 25}; // Pad with 0 position 0 since currencies IDs start from 1
//    public static final int BINARY0100_CONTRACTS_STEPS_COUNT = 10;

//	private static final long serialVersionUID = 7743546143573411839L;
//	protected long id;
//	protected long userId;
//	protected long opportunityId;
//    protected long opportunityTypeId;
//	protected long typeId;
//	protected long amount;
//	protected long baseAmount;
//	protected long currencyId;
//	protected Double currentLevel;
//	protected BigDecimal wwwLevel;
//	protected Double realLevel;
//	protected double oddsWin;
//	protected double oddsLose;
//	protected long writerId;
//	protected String ip;
//	protected long win;
//	protected long baseWin;
//	protected long lose;
//	protected long baseLose;
//	protected long houseResult;
//	protected Date timeCreated;
//	protected Date timeSettled;
//	protected int isSettled;
//	protected int isCanceled;
//	protected Date timeCanceled;
//	protected int isVoid;
//	protected BigDecimal canceledWriterId;
//    protected int bonusOddsChangeTypeId;
//    protected double rate;
//    protected long referenceInvestmentId;

//    protected String utcOffsetCreated;
//    protected String utcOffsetSettled;
//    protected String utcOffsetCancelled;
//    protected String utcOffsetEstClosing;
//    protected String utcOffsetActClosing;

//	protected long marketId;
//	protected String marketName;
//	protected Date timeEstClosing;
//    protected Date timeLastInvest;
//	protected Date timeActClosing;
//    protected Date timeFirstInvest;
//	protected String typeName;
//	protected Double closingLevel;
//	protected boolean selected;
//	protected long scheduledId;
//    protected long decimalPoint;

//	protected String userName;
//	protected String userFirstName;
//	protected String userLastName;
//	protected boolean isSpecialCare;
//    protected SpecialCare specialCare;

//	protected int totalLine;
//	boolean isPositive;
//	protected int oneTouchUpDown = -1;
//	protected String userCountryCode;
//	protected int oneTouchDecimalPoint;

//	protected String skin;
//    protected long insuranceAmountGM;
//    protected long insuranceAmountRU;
//    protected double insurancePremiaPercent;
//    protected double insuranceQualifyPercent;
//    protected double currentInsuranceQualifyPercent;
    
//    protected Map<SkinGroup, String> insuranceCurrentLevels; 
//    
//    protected boolean showInsurance;
//    protected int insurancePosition;
//    protected double roolUpQualifyPercent;
//    protected int insuranceType; //1 = GM , 2 = RU
//    protected double rollUpQualifyPercent;
//    protected boolean isRollUp;
//    protected boolean isGoldenMinutes;
//    protected double rollUpPremiaPercent;
//    protected Integer insuranceFlag;
//    protected long rolledInvId;
//    protected double marketLastDayClosingLevel;
//    protected boolean acceptedSms;
//    protected long bonusUserId;
//    protected boolean isOpenGraph;
//    private long sumInvest;         // sum of investments for one opportunity (my invest page)
//    private boolean hasShowOff;
//    private int shSen = -1; // show off sentence
//    private String showOffText = null;
//    private long countryId;
//    private String showOffName;
    //private String showOffShortText;
//    private long showOffId;
//    private String showOffFacebookText;
//    private String showOffTwitterText;
//    private Date showOffTime;
//    private int fromGraph;
//    private boolean isOptionPlus;
//    private long optionPlusFee;
    //true if this investment was seen in the golden minutes banner
//    private boolean isSeen;


//	for caleculate the bonus amount if its nextInvestOnUs from type 2 or 3
//    protected double bonusWinOdds;
//    protected double bonusLoseOdds;
//  transaction amount if its has bonus (next invest on us or odds change)
//    protected long transactionAmount;
//    protected String bonusDisplayName;

//    private Date timeQuoted;
//    private String lastShowOffMessegeKey;

//    private String writerUserName;

//    private double oppCurrentLevel;

//    private long investmentsCount; //for binary0100
//    private long selectedInvestmentsCount; //for binary0100
//    private int contractsStep; // for binary0100

    // Ao live
//    private String countryName;
//    private String nameOpportunityType;
//    private long profitUser;
//    private String cityName;

    // for daily screen
//    private long userBalance;

//    private boolean isHaveReutersQuote;

    //for api external user
//    private long apiExternalUserId;
//    private String apiExternalUserReference;
//    private long copyOpInvId;
//    private int copyOpTypeId;
//    private boolean isLikeHourly;
//
//    // TODO remove this
//	public Investment() {}
//
//	// TODO remove this
//	public Investment(int totalLine2) {
//		super(totalLine2);
//	}
//
//    public Investment() {}
//
//	public Investment(int totalLine2) {
//		totalLine=totalLine2;
//        specialCare = null;
//	}

//	public long getRefund() {
//		long l=amount+win-lose;
//		return l;
//	}

//	public long getBaseRefund() {
//		long l=baseAmount+baseWin-baseLose;
//		return l;
//	}
//	public long getRefundUp() {
//		Double win = new Double(getAmountWithoutFees() + getAmountWithoutFees() * oddsWin);
//		Double lose = new Double(getAmountWithoutFees() - getAmountWithoutFees() * oddsLose);
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_PUT  && bonusOddsChangeTypeId == 1){  // lose & is free
//			return  amount;
//		}
//		if (InvestmentsManagerBase.INVESTMENT_TYPE_CALL==typeId || (getIsOneTouchInv() && isUpInvestment())) { // win
//			return Math.round(win);
//		}
//		return Math.round(lose);
//	}

//	public long getBaseRefundUp() {
//		Double win=new Double(getBaseAmountWithoutFees()+getBaseAmountWithoutFees()*oddsWin);
//		Double lose=new Double(getBaseAmountWithoutFees()-getBaseAmountWithoutFees()*oddsLose);
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_PUT  && bonusOddsChangeTypeId == 1){
//			return  baseAmount;
//		}
//		if (InvestmentsManagerBase.INVESTMENT_TYPE_CALL==typeId || (getIsOneTouchInv() && isUpInvestment())) {
//			return Math.round(win);
//		}
//		return Math.round(lose);
//	}

//	public long getRefundDown() {
//		Double win = new Double(getAmountWithoutFees()+getAmountWithoutFees()*oddsWin);
//		Double lose = new Double(getAmountWithoutFees()-getAmountWithoutFees()*oddsLose);
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_CALL && bonusOddsChangeTypeId == 1){ // lose & is free
//			return  amount;
//		}
//		if (InvestmentsManagerBase.INVESTMENT_TYPE_PUT==typeId || (getIsOneTouchInv() && isDownInvestment())) { // win
//			return Math.round(win);
//		}
//		return Math.round(lose);
//	}

//	public long getBaseRefundDown() { //TODO: base
//		Double win=new Double(getBaseAmountWithoutFees()+getBaseAmountWithoutFees()*oddsWin);
//		Double lose=new Double(getBaseAmountWithoutFees()-getBaseAmountWithoutFees()*oddsLose);
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_CALL && bonusOddsChangeTypeId == 1){
//			return  baseAmount;
//		}
//		if (InvestmentsManagerBase.INVESTMENT_TYPE_PUT==typeId || (getIsOneTouchInv() && isDownInvestment())) {
//			return Math.round(win);
//		}
//		return Math.round(lose);
//	}
//
//	public String getRefundTxt() {
//		if (isCanceled==1) {
//			if (bonusOddsChangeTypeId == 1) {
//				return CommonUtil.displayAmount(amount,currencyId);
//			}else {
//				return CommonUtil.getMessage("investments.iscanceled", null);
//			}
//		}
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(this), currencyId);
//	}
//
//	/**
//	 * for bonus with class id 3 (next invest on us or odds change bonueses), its will add the bonus amount to the Refund
//	 * @return Refund + transaction amount
//	 */
//	public String getRefundWithBonusTxt() {
//		if (isCanceled==1) {
//			if (bonusOddsChangeTypeId == 1) {
//				return CommonUtil.displayAmount(amount, currencyId);
//			}else {
//				return CommonUtil.getMessage("investments.iscanceled", null);
//			}
//		}
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(this) + transactionAmount, currencyId);
//	}
//
//	public String getBaseRefundTxt() {
//		if (isCanceled==1) {
//			if (bonusOddsChangeTypeId == 1) {
//				return CommonUtil.displayAmount(baseAmount,currencyId);
//			}else {
//				return CommonUtil.getMessage("investments.iscanceled", null);
//			}
//		}
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getBaseRefund(this), ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getRefundUpTxt() {
//		return CommonUtil.displayAmount(getRefundUp(), currencyId);
//	}
//
//	public String getWinUpTxt() {
//		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(this) - amount) * (-1), currencyId);
//	}
//	
//	public String getWinUpCustomerTxt() {
//		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundUp(this)), currencyId);
//	}
//
//	public String getBaseRefundUpTxt() {
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getBaseRefundUp(this), ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getBaseWinUpTxt() {
//		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundUp(this) - baseAmount) *(-1), ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getBinary0100WinUpTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()) * (-1), true, currencyId);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(amount, true, currencyId);
//		}
//		return displayAmount;
//	}
//	
//	public String getBinary0100WinUpCustomerTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()) + amount, true, currencyId);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(0, true, currencyId);
//		}
//		return displayAmount;
//	}
//
//	public String getBinary0100LoseDownTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(amount, true, currencyId);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()), true, currencyId);
//		}
//		return displayAmount;
//	}
//	
//	public String getBinary0100LoseDownCustomerTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(0, true, currencyId);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(amount + ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()), true, currencyId);
//		}
//		return displayAmount;
//	}
//
//	public String getBinary0100BaseRefundUpTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()) * rate, true, ConstantsBase.CURRENCY_BASE_ID);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(baseAmount, true, ConstantsBase.CURRENCY_BASE_ID);
//		}
//		return displayAmount;
//	}
//
//	public String getBinary0100BaseWinUpTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount()) * rate, true, ConstantsBase.CURRENCY_BASE_ID);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(baseAmount, true, ConstantsBase.CURRENCY_BASE_ID);
//		}
//		return displayAmount;
//	}
//
//	public String getBinary0100BaseRefundDownTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(baseAmount, true, ConstantsBase.CURRENCY_BASE_ID);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 *getInvestmentsCount()) * rate, true, ConstantsBase.CURRENCY_BASE_ID);
//		}
//		return displayAmount;
//	}
//
//	public String getBinary0100BaseLoseDownTxt() {
//		String displayAmount="";
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			displayAmount = CommonUtil.displayAmount(baseAmount, ConstantsBase.CURRENCY_BASE_ID);
//		} else if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			displayAmount = CommonUtil.displayAmount(-1 * ((100 - (amount / (double)getInvestmentsCount() / 100)) * 100 * getInvestmentsCount())  * rate, true, ConstantsBase.CURRENCY_BASE_ID);
//		}
//		return displayAmount;
//	}
//
//	public String getRefundDownTxt() {
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(this), currencyId);
//	}
//
//	public String getLoseDownTxt() {
//		return CommonUtil.displayAmount((InvestmentsManagerBase.getRefundDown(this) - amount) * (-1), currencyId);
//	}
//	
//	public String getLoseDownCustomerTxt() {
//		return CommonUtil.displayAmount( InvestmentsManagerBase.getRefundDown(this), currencyId);
//	}
//
//	public String getBaseRefundDownTxt() {
//		return CommonUtil.displayAmount(InvestmentsManagerBase.getBaseRefundDown(this), ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getBaseLoseDownTxt() {
//		return CommonUtil.displayAmount((InvestmentsManagerBase.getBaseRefundDown(this) - baseAmount) * (-1), ConstantsBase.CURRENCY_BASE_ID);
//	}

//	public Long getRefundSort() {
//		if (isCanceled==1) {
//			return Long.MAX_VALUE;
//		}
//		long l=amount+win-lose;
//		return new Long(l);
//	}

//	public boolean isSelected() {
//		return selected;
//	}
//
//	public void setSelected(boolean selected) {
//		this.selected = selected;
//	}
//
//	public String getWinLoseTxt() {
//
//		if (isSettled == 1) {
//			return CommonUtil.displayAmount(lose - win, currencyId);
//		}
//
//		return "";
//	}
//
//	public String getUserWinLoseTxt() {
//
//		if (isSettled == 1) {
//			return CommonUtil.displayAmount(-1 * (win - lose), currencyId);
//		}
//
//		return "";
//	}
//	
//	public String getCustomerUserWinLoseTxt() {
//
//		if (isSettled == 1) {
//			return CommonUtil.displayAmount(amount + (win - lose), currencyId);
//		}
//
//		return "";
//	}
//
//	public String getUserWinLoseTxt0100() {
//
//		if (isSettled == 1) {
//			return CommonUtil.displayAmount((-1) * (win - lose) * getInvestmentsCount() / (getContractsStep() != 0 ? getContractsStep() : 1), currencyId);
//		}
//
//		return "";
//	}
//	
//	public String getCustomerUserWinLoseTxt0100() {
//
//		if (isSettled == 1) {
//			return CommonUtil.displayAmount(amount + ((win - lose) * getInvestmentsCount() / (getContractsStep() != 0 ? getContractsStep() : 1)), currencyId);
//		}
//
//		return "";
//	}
//
//	public long getUserWinLose() {
//		return (win - lose) * getInvestmentsCount() / ((typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY || typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) && getContractsStep() != 0 ? getContractsStep() : 1);
//	}
//
//	public long getBaseUserWinLose() {
//		return (baseWin - baseLose) * getInvestmentsCount() / ((typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY || typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) && getContractsStep() != 0 ? getContractsStep() : 1);
//	}
//
//	public String getIsSettledTxt() {
//		if (isSettled==0)
//			return CommonUtil.getMessage("no",null);
//
//		return CommonUtil.getMessage("yes",null);
//	}

//	public double getCurrentLevelValue() {
//		return currentLevel.doubleValue();
//	}

//	public Double getClosingLevel() {
//		return closingLevel;
//	}
//	public void setClosingLevel(Double closingLevel) {
//		this.closingLevel = closingLevel;
//	}
//
//	public String getClosingLevelTxt() {
//        if (null != closingLevel && closingLevel > 0) {
//            return CommonUtil.formatLevelByMarket(closingLevel, marketId);
//        }
//        return "";
//	}
//
//	@Override
//	public String getCurrentLevelTxt() {
//
//		if (InvestmentsManagerBase.INVESTMENT_TYPE_ONE == typeId) {
//				return CommonUtil.formatLevelByDecimalPoint(currentLevel, oneTouchDecimalPoint);
//		}
//		else {
//			return CommonUtil.formatLevelByMarket(currentLevel, marketId);
//		}
//	}
//	
//	public String getBubblesHighLevelTxt() {
//		return CommonUtil.formatLevelByMarket(getBubbleHighLevel(), marketId);
//	}
//	
//	public String getBubblesLowLevelTxt() {
//		return CommonUtil.formatLevelByMarket(getBubbleLowLevel(), marketId);
//	}
//
//	public String getRealLevelTxt() {
//		return CommonUtil.formatRealLevelByMarket(realLevel, marketId);
//	}

//	public String getWwwLevelTxt() {
//		if (wwwLevel==null) {
//			return "";
//		}
//		return CommonUtil.formatLevelByMarket(wwwLevel.doubleValue(), marketId);
//	}

//	public Date getTimeActClosing() {
//		return timeActClosing;
//	}
//	public void setTimeActClosing(Date timeActClosing) {
//		this.timeActClosing = timeActClosing;
//	}
//
//	public Date getTimeLastInvest() {
//        return timeLastInvest;
//    }
//
//    public void setTimeLastInvest(Date timeLastInvest) {
//        this.timeLastInvest = timeLastInvest;
//    }
//
//    public Date getTimeFirstInvest() {
//        return timeFirstInvest;
//    }
//
//    public void setTimeFirstInvest(Date timeFirstInvest) {
//        this.timeFirstInvest = timeFirstInvest;
//    }

//    public String getTypeName() {
//		return typeName;
//	}
//	public void setTypeName(String typeName) {
//		this.typeName = typeName;
//	}
//
//    public String getTimeCreatedTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCreated, utcOffsetCreated);
//
//	}
//    
//    public String getBubblesStartTimeTxt() {
//		return CommonUtil.getDateAndTimeFormat(getBubbleStartTime(), utcOffsetCreated);
//	}
//    
//    public String getBubblesEndTimeTxt() {
//		return CommonUtil.getDateAndTimeFormat(getBubbleEndTime(), utcOffsetCreated);
//	}
//
//    public String getOnlyTimeCreatedTxt() {
//        return CommonUtil.getTimeFormat(timeCreated, utcOffsetCreated, "HH:mm:ss");
//
//    }

//	/**
//	 * Get time estimated closing
//	 * in a case we have diference between utcOffsetCreated and utcOffsetEstClosing
//	 * 	that bigger then 1(hour) we take by default utcOffsetCreated.
//	 * @return
//	 * 		time estimated closing by utcOffset
//	 */
//	@Override
//	public String getTimeEstClosingTxt() {
//		long offsetCreated = CommonUtil.getUtcOffsetValue(utcOffsetCreated);
//		long offsetEstSettled = CommonUtil.getUtcOffsetValue(utcOffsetEstClosing);
//		long offsetGain = 0;
//
//		if ( offsetCreated != -1 && offsetEstSettled != -1 ) {   // -1 : problem with getting offset value
//			offsetGain = Math.abs(offsetCreated - offsetEstSettled);
//		}
//
//		if ( offsetGain > ConstantsBase.OFFSET_GAIN_BETWEEN_CREATED_AND_SETTLED ) {
//			return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, utcOffsetCreated);
//		}
//
//		return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, utcOffsetEstClosing);
//	}
//
//    public String getOnlyTimeEstClosingTxt() {
//        return CommonUtil.getTimeFormat(timeEstClosing, utcOffsetCreated);
//    }
//
//    public String getOnlyTimeLastInvestTxt() {
//        return CommonUtil.getTimeFormat(timeLastInvest, utcOffsetCreated);
//    }
//
//    public String getOnlyTimeFirstInvestTxt() {
//        return CommonUtil.getTimeFormat(timeFirstInvest, utcOffsetCreated);
//    }
//
//	public String getTimeActClosingTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeActClosing, utcOffsetActClosing);
//	}
//	
//	public String getBubbleStartTimeTxt() {
//		return CommonUtil.getTimeFormat(getBubbleStartTime(), utcOffsetCreated, "HH:mm:ss");
//	}
//	
//	public String getBubbleStartTimeDateOnlyTxt() {
//		return CommonUtil.getTimeFormat(getBubbleStartTime(), utcOffsetCreated, "dd.MM.YYYY");
//	}
//	
//	public String getTimeCreatedForBubbleTxt() {
//		return CommonUtil.getTimeFormat(timeCreated, utcOffsetCreated, "HH:mm:ss dd.MM.YYYY");
//	}
//	
//	public String getBubbleEndTimeTxt() {
//		return CommonUtil.getTimeFormat(getBubbleEndTime(), utcOffsetEstClosing, "HH:mm:ss");
//	}
//
//	public String getTimeSettledTxt() {
//	    if(isBubbles()) {
//		return CommonUtil.getTimeFormat(timeSettled, utcOffsetSettled, "HH:mm:ss dd.MM.YYYY");
//	    }
//		return CommonUtil.getDateTimeFormatDisplay(timeSettled, utcOffsetSettled);
//	}
//
//	public String getTimeSettledTxtForLiveAO(){
//		return CommonUtil.getDateTimeFormatForLiveAO(timeSettled, utcOffsetSettled, CommonUtil.getMessage("live.AO.at", null));
//	}
//
//	public String getTimeCanceledTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCanceled, utcOffsetCancelled);
//	}
//
//	public String getMarketNameForDisplay() {
//		return CommonUtil.getMessage(marketName, null);
//	}
//
//	public long getAmountWithBonus() {
//		return amount + transactionAmount;
//	}
//
//	public String getAmountWithBonusTxt() {
//		return CommonUtil.displayAmount(getAmountWithBonus(), currencyId);
//	}

//    public long getAmountWithFees() {
//        return amount + getInsuranceAmount();
//    }
//
//	@Override
//	public String getAmountTxt() {
//		return CommonUtil.displayAmount(amount, currencyId);
//	}
//	
//	public String getBubbleReturnToWinTxt() {
//		return CommonUtil.displayAmount(getBubbleReturnToWin(),true, currencyId);
//	}
//	
//	public String getBubbleReturnToLoseTxt() {
//		return CommonUtil.displayAmount(getBubbleReturnToLose(),true, currencyId);
//	}
//
//	public String getAmountMaxRiskTxt() {
//		double maxRiskAmount = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			maxRiskAmount = getInvestmentsCount() * (amount -getOptionPlusFee()) / getBinary0100NumberOfContracts();
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			maxRiskAmount = (getInvestmentsCount() * (amount -getOptionPlusFee()) / getBinary0100NumberOfContracts());
//		}
//		return CommonUtil.displayAmount(maxRiskAmount, true, currencyId);
//	}
//
//	public String getAmountMaxRiskTxtExcludDecimal() {
//		return CommonUtil.removeFractionFromAmount(getAmountMaxRiskTxt());
//	}
//
//	public String getAmountMaxReturnTxt() {
//		double maxReturnAmount = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			maxReturnAmount = (getInvestmentsCount() * (100 - (amount / getBinary0100NumberOfContracts() / 100.0))) * 100;
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			maxReturnAmount = getInvestmentsCount() * amount / getBinary0100NumberOfContracts();
//		}
//		return CommonUtil.displayAmount(maxReturnAmount, true, currencyId);
//	}
//	
//	public String getMaxProfitTxtExcludDecimal() {
//		return CommonUtil.removeFractionFromAmount(getMaxProfitTxt());
//	}
//
//	public String getMaxProfitTxt() {
//		double maxReturnAmount = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			maxReturnAmount = (investmentsCount * (100 - ((amount - optionPlusFee)/ getBinary0100NumberOfContracts() / 100.0))) * 100;
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			maxReturnAmount = (investmentsCount * (100 - ((amount - optionPlusFee)/ getBinary0100NumberOfContracts() / 100.0))) * 100;
//		}
//		return CommonUtil.displayAmount(maxReturnAmount, true, currencyId);
//	}
//
//	public String getAmountReturnTxtExcludDecimal() {
//		return CommonUtil.removeFractionFromAmount(getAmountReturnTxt());
//	}
//	
//	public String getInvestmentsAmountExcludDecimal() {
//		return CommonUtil.removeFractionFromAmount(getInvestmentsAmount());
//	}
	
//	public String getAmountReturnTxt() {
//		double returnAmount = 0;
//
//		if (isSettledBeforTime()) {
//			returnAmount = (amount + win - lose) / getBinary0100NumberOfContracts() * investmentsCount;
//		} else if (!isSettledBeforTime() && typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY && win - lose > 0) {
//			returnAmount = (investmentsCount * 100) * 100;
//		} else if (!isSettledBeforTime() && typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY && win - lose < 0) {
//			returnAmount = 0;
//		} else if (!isSettledBeforTime() && typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL && win - lose > 0) {
//			returnAmount = (investmentsCount * 100) * 100;
//		} else if (!isSettledBeforTime() && typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL && win - lose < 0) {
//			returnAmount = 0;
//		}
//
//		return CommonUtil.displayAmount(returnAmount, true, currencyId);
//	}
//
//	public String getInvestmentsAmount() {
//		return CommonUtil.displayAmount((amount) / getBinary0100NumberOfContracts() * investmentsCount, true, currencyId);
//	}
//
//	public String getCurrentLevel0100() {
//		double currentLeve = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			currentLeve = (amount - getOptionPlusFee() )/ getBinary0100NumberOfContracts();
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			currentLeve = (100.0 - ((amount - getOptionPlusFee()) / getBinary0100NumberOfContracts() / 100.0)) * 100.0;
//		}
//		return CommonUtil.displayAmount(currentLeve, false, 0);
//	}
//
//
//	public String getClosingLevel0100() {
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			return CommonUtil.displayAmount(10000 - (amount + win - lose) / getBinary0100NumberOfContracts(), false, 0);
//		} //its buy
//		return CommonUtil.displayAmount((amount + win - lose) / getBinary0100NumberOfContracts(), false, 0);
//
//	}
//
//	/**
//	 * amount in backend = amount * num of contract
//	 * @return
//	 */
//	public String getCurrentLevel0100BE() {
//		double currentLeve = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			currentLeve = (amount - (optionPlusFee*(getInvestmentsCount() / getContractsStep())))/ (double) investmentsCount;
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			currentLeve = (100.0 - (((amount - (optionPlusFee*(getInvestmentsCount() / getContractsStep()))) / (double) investmentsCount) / 100.0)) * 100.0;
//		}
//		return CommonUtil.displayAmount(currentLeve, false, 0);
//	}
//
//	public String getClosingPrice() {
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			return CommonUtil.displayAmount(10000 - amount / (double) investmentsCount - (win - lose) / contractsStep, false, 0);
//		} //buy
//		return CommonUtil.displayAmount(amount / (double) investmentsCount + (win - lose) / contractsStep, false, 0);
//	}
//
//	/**
//	 * amount in backend = amount * num of contract
//	 * For page total.
//	 * @return
//	 */
//	public double getCurrentLevel0100Total() {
//		double currentLeve = 0;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			currentLeve = amount / (double) investmentsCount;
//		} else if(typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			currentLeve = (100 - ((amount / (double) investmentsCount) / 100)) * 100;
//		}
//		return currentLeve;
//	}
//
//
//	/**
//	 * amount in backend = amount * num of contract
//	 * @return
//	 */
//	public String getClosingLevel0100BE() {
//		return CommonUtil.formatLevelByDecimalPoint(closingLevel, decimalPoint);
//	}
//
//	public String getBaseAmountTxt() {
//		return CommonUtil.displayAmount(baseAmount,ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//    /**
//     * show the base amount + insurance amount
//     * @return base amount + insurance amount as txt
//     */
//    public String getBaseAmountWithFeesTxt() {
//        return CommonUtil.displayAmount(baseAmount + (rate * InvestmentsManagerBase.getInsuranceAmount(this)), true, ConstantsBase.CURRENCY_BASE_ID);
//    }
//
//    public String getAmountWithFeesTxt() {
//        return CommonUtil.displayAmount(amount + InvestmentsManagerBase.getInsuranceAmount(this), currencyId);
//    }
//
//	public long getHouseResult() {
//		return houseResult;
//	}
//
//	public void setHouseResult(long houseResult) {
//		this.houseResult = houseResult;
//	}

//	public String getIp() {
//		return ip;
//	}
//	public void setIp(String ip) {
//		this.ip = ip;
//	}
//	public int getIsCanceled() {
//		return isCanceled;
//	}
//	public void setIsCanceled(int isCanceled) {
//		this.isCanceled = isCanceled;
//	}
//	public int getIsSettled() {
//		return isSettled;
//	}
//	public void setIsSettled(int isSettled) {
//		this.isSettled = isSettled;
//	}
//	public int getIsVoid() {
//		return isVoid;
//	}
//	public void setIsVoid(int isVoid) {
//		this.isVoid = isVoid;
//	}
//	public long getLose() {
//		return lose;
//	}
//	public void setLose(long lose) {
//		this.lose = lose;
//	}

//	public Date getTimeCanceled() {
//		return timeCanceled;
//	}
//	public void setTimeCanceled(Date timeCanceled) {
//		this.timeCanceled = timeCanceled;
//	}

//	public long getUserId() {
//		return userId;
//	}
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//	public long getWin() {
//		return win;
//	}
//	public void setWin(long win) {
//		this.win = win;
//	}
//	public long getWriterId() {
//		return writerId;
//	}
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	public String getWriterName() throws SQLException{
//		return CommonUtil.getWriterName(writerId);
//	}
//	public String getCanceledWriterName() throws SQLException{
//		if (canceledWriterId==null)
//			return "";
//
//		return CommonUtil.getWriterName(canceledWriterId.longValue());
//	}
//
//	public String getIsCanceledTxt() {
//		if (isCanceled==0)
//			return CommonUtil.getMessage("no", null);
//		return CommonUtil.getMessage("yes", null);
//	}
//	public BigDecimal getCanceledWriterId() {
//		return canceledWriterId;
//	}
//	public void setCanceledWriterId(BigDecimal canceledWriterId) {
//		this.canceledWriterId = canceledWriterId;
//	}
//
//	public long getScheduledId() {
//		return scheduledId;
//	}
//
//	public void setScheduledId(long scheduledId) {
//		this.scheduledId = scheduledId;
//	}
//
//	public String getScheduledTxt() {
//		return InvestmentsManagerBase.getScheduledTxt(scheduledId);
//	}
//
//	public String getUserFirstName() {
//		return userFirstName;
//	}
//
//	public void setUserFirstName(String userFirstName) {
//		this.userFirstName = userFirstName;
//	}
//
//	public String getUserLastName() {
//		return userLastName;
//	}
//
//	public void setUserLastName(String userLastName) {
//		this.userLastName = userLastName;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	public boolean getCheckIfCancel() {
//		FacesContext context=FacesContext.getCurrentInstance();
//		UserBase user= (UserBase)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
//
//		if (user.getRoles().contains(ConstantsBase.ROLE_SADMIN)) {
//			return (isCanceled==0);
//		}
//
//		return false;
//
//	}
//
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	@Override
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "Investment ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "userId = " + this.userId + TAB
//	        + "opportunityId = " + this.opportunityId + TAB
//	        + "typeId = " + this.typeId + TAB
//	        + "amount = " + this.amount + TAB
//	        + "currentLevel = " + this.currentLevel + TAB
//	        + "oddsWin = " + this.oddsWin + TAB
//	        + "oddsLose = " + this.oddsLose + TAB
//	        + "writerId = " + this.writerId + TAB
//	        + "ip = " + this.ip + TAB
//	        + "win = " + this.win + TAB
//	        + "lose = " + this.lose + TAB
//	        + "houseResult = " + this.houseResult + TAB
//	        + "timeCreated = " + this.timeCreated + TAB
//	        + "timeSettled = " + this.timeSettled + TAB
//	        + "isSettled = " + this.isSettled + TAB
//	        + "isCanceled = " + this.isCanceled + TAB
//	        + "timeCanceled = " + this.timeCanceled + TAB
//	        + "isVoid = " + this.isVoid + TAB
//	        + "canceledWriterId = " + this.canceledWriterId + TAB
//	        + "marketId = " + this.marketId + TAB
//	        + "marketName = " + this.marketName + TAB
//	        + "timeEstClosing = " + this.timeEstClosing + TAB
//            + "timeLastInvest = " + this.timeLastInvest + TAB
//	        + "timeActClosing = " + this.timeActClosing + TAB
//            + "timeFirstInvest = " + this.timeFirstInvest + TAB
//	        + "typeName = " + this.typeName + TAB
//	        + "closingLevel = " + this.closingLevel + TAB
//	        + "selected = " + this.selected + TAB
//	        + "scheduledId = " + this.scheduledId + TAB
//	        + "userName = " + this.userName + TAB
//	        + "userFirstName = " + this.userFirstName + TAB
//	        + "userLastName = " + this.userLastName + TAB
//            + "bonusOddsChangeTypeId = " + this.bonusOddsChangeTypeId + TAB
//            + "decimalPoint = " + this.decimalPoint + TAB
//            + "insuranceAmountGM = " + this.insuranceAmountGM + TAB
//            + "insuranceAmountRU = " + this.insuranceAmountRU + TAB
//            + "insurancePremiaPercent = " + this.insurancePremiaPercent + TAB
//            + "insuranceQualifyPercent = " + this.insuranceQualifyPercent + TAB
//            + "currentInsuranceQualifyPercent = " + this.currentInsuranceQualifyPercent + TAB
//            + "insuranceCurrentLevels = " + this.insuranceCurrentLevels + TAB
//            + "showInsurance = " + this.showInsurance + TAB
//            + "insurancePosition = " + this.insurancePosition + TAB
//            + "utcOffsetCreated = " + this.utcOffsetCreated + TAB
//            + "writerUserName = " + this.writerUserName + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	public int getTotalLine() {
//		return totalLine;
//	}
//
//	public void setTotalLine(int totalLine) {
//		this.totalLine = totalLine;
//	}

//	public Double getRealLevel() {
//		return realLevel;
//	}
//
//	public void setRealLevel(Double realLevel) {
//		this.realLevel = realLevel;
//	}
//	public BigDecimal getWwwLevel() {
//		return wwwLevel;
//	}
//	public void setWwwLevel(BigDecimal wwwLevel) {
//		this.wwwLevel = wwwLevel;
//	}

//    public int getBonusOddsChangeTypeId() {
//        return bonusOddsChangeTypeId;
//    }
//
//    public void setBonusOddsChangeTypeId(int bonusOddsChangeTypeId) {
//        this.bonusOddsChangeTypeId = bonusOddsChangeTypeId;
//    }

//    public boolean isUpInvestment() {
//	   if ( oneTouchUpDown == ConstantsBase.ONE_TOUCH_IS_UP ) {
//   			return true;
//   		}
//    return false;
//    }
//
//    public boolean isDownInvestment() {
//    	 if ( oneTouchUpDown == ConstantsBase.ONE_TOUCH_IS_DOWN ) {
//    		return true;
//    	}
//        return false;
//   }

//	/**
//	 * Check if its one touch investment
//	 * @return
//	 */
//	public boolean getIsOneTouchInv() {
//		if ( typeId == InvestmentsManagerBase.INVESTMENT_TYPE_ONE ) {
//			return true;
//		}
//		return false;
//	}

//	public long getCurrencyId() {
//		return currencyId;
//	}
//
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//
//	public long getBaseAmount() {
//		return baseAmount;
//	}
//
//	public void setBaseAmount(long baseAmount) {
//		this.baseAmount = baseAmount;
//	}

//	/**
//	 * @return the baseLose
//	 */
//	public long getBaseLose() {
//		return baseLose;
//	}
//
//	/**
//	 * @param baseLose the baseLose to set
//	 */
//	public void setBaseLose(long baseLose) {
//		this.baseLose = baseLose;
//	}
//
//	/**
//	 * @return the baseWin
//	 */
//	public long getBaseWin() {
//		return baseWin;
//	}
//
//	/**
//	 * @param baseWin the baseWin to set
//	 */
//	public void setBaseWin(long baseWin) {
//		this.baseWin = baseWin;
//	}

//	/**
//	 * @return the utcOffsetCreated
//	 */
//	public String getUtcOffsetCreated() {
//		return utcOffsetCreated;
//	}
//
//	/**
//	 * @param utcOffsetCreated the utcOffsetCreated to set
//	 */
//	public void setUtcOffsetCreated(String utcOffsetCreated) {
//		this.utcOffsetCreated = utcOffsetCreated;
//	}
//
//	/**
//	 * @return the utcOffsetSettled
//	 */
//	public String getUtcOffsetSettled() {
//		return utcOffsetSettled;
//	}
//
//	/**
//	 * @param utcOffsetSettled the utcOffsetSettled to set
//	 */
//	public void setUtcOffsetSettled(String utcOffsetSettled) {
//		this.utcOffsetSettled = utcOffsetSettled;
//	}
//
//	/**
//	 * @return the utcOffsetCancelled
//	 */
//	public String getUtcOffsetCancelled() {
//		return utcOffsetCancelled;
//	}
//
//	/**
//	 * @param utcOffsetCancelled the utcOffsetCancelled to set
//	 */
//	public void setUtcOffsetCancelled(String utcOffsetCancelled) {
//		this.utcOffsetCancelled = utcOffsetCancelled;
//	}

//	/**
//	 * @return the utcOffsetActClosing
//	 */
//	public String getUtcOffsetActClosing() {
//		return utcOffsetActClosing;
//	}
//
//	/**
//	 * @param utcOffsetActClosing the utcOffsetActClosing to set
//	 */
//	public void setUtcOffsetActClosing(String utcOffsetActClosing) {
//		this.utcOffsetActClosing = utcOffsetActClosing;
//	}

//	/**
//	 * @return the utcOffsetEstClosing
//	 */
//	public String getUtcOffsetEstClosing() {
//		return utcOffsetEstClosing;
//	}
//
//	/**
//	 * @param utcOffsetEstClosing the utcOffsetEstClosing to set
//	 */
//	public void setUtcOffsetEstClosing(String utcOffsetEstClosing) {
//		this.utcOffsetEstClosing = utcOffsetEstClosing;
//	}

//	/**
//	 * get is call option or not
//	 * @return
//	 */
//	public boolean getIsCallOption() {
//		if ( typeId == InvestmentsManagerBase.INVESTMENT_TYPE_CALL ) {
//			return true;
//		}
//
//		return false;
//	}

//	/**
//	 * @return the userCountryCode
//	 */
//	public String getUserCountryCode() {
//		return userCountryCode;
//	}
//
//	/**
//	 * @param userCountryCode the userCountryCode to set
//	 */
//	public void setUserCountryCode(String userCountryCode) {
//		this.userCountryCode = userCountryCode;
//	}
//
//	public int getOneTouchDecimalPoint() {
//		return oneTouchDecimalPoint;
//	}
//
//	public void setOneTouchDecimalPoint(int oneTouchDecimalPoint) {
//		this.oneTouchDecimalPoint = oneTouchDecimalPoint;
//	}
//
//	public String getSkin() {
//		return skin;
//	}
//
//	public void setSkin(String skin) {
//		this.skin = skin;
//	}

//    public long getDecimalPoint() {
//        return decimalPoint;
//    }
//
//    public void setDecimalPoint(long decimalPoint) {
//        this.decimalPoint = decimalPoint;
//    }

//    /**
//     * @return the  insuranceAmountGM + insuranceAmountRU
//     */
//    public long getInsuranceAmount() {
//        return  insuranceAmountGM + insuranceAmountRU;
//    }
//
//    public String getInsuranceAmountTxt() {
//        return CommonUtil.displayAmount(InvestmentsManagerBase.getInsuranceAmount(this), currencyId);
//    }
//
//    /**
//     * in the money
//     * @return case call: gm level - inv current level, case put: inv current level - gm level
//     */
//    public double getCurrentInsuranceQualifyPercent() {
//        return currentInsuranceQualifyPercent;
//    }
//
//    public void setCurrentInsuranceQualifyPercent(
//            double currentInsuranceQualifyPercent) {
//        this.currentInsuranceQualifyPercent = currentInsuranceQualifyPercent;
//    }
//
//    public double getInsuranceQualifyPercent() {
//        return insuranceQualifyPercent;
//    }
//
//    public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
//        this.insuranceQualifyPercent = insuranceQualifyPercent;
//    }

//    public double getInsurancePremiaPercent() {
//        return insurancePremiaPercent;
//    }
//
//    public void setInsurancePremiaPercent(double insurancePremiaPercent) {
//        this.insurancePremiaPercent = insurancePremiaPercent;
//    }

//    public boolean isShowInsurance() {
//        return showInsurance;
//    }
//
//    public void setShowInsurance(boolean showInsurance) {
//        this.showInsurance = showInsurance;
//    }
//
//    public int getInsurancePosition() {
//        return insurancePosition;
//    }
//
//    public void setInsurancePosition(int insurancePosition) {
//        this.insurancePosition = insurancePosition;
//    }

//	/**
//	 * @return the bonusUserId
//	 */
//	public long getBonusUserId() {
//		return bonusUserId;
//	}
//
//	/**
//	 * @param bonusUserId the bonusUserId to set
//	 */
//	public void setBonusUserId(long bonusUserId) {
//		this.bonusUserId = bonusUserId;
//	}

//    /**
//     * @return the roolUpQualifyPercent
//     */
//    public double getRoolUpQualifyPercent() {
//        return roolUpQualifyPercent;
//    }
//
//    /**
//     * @param roolUpQualifyPercent the roolUpQualifyPercent to set
//     */
//    public void setRoolUpQualifyPercent(double roolUpQualifyPercent) {
//        this.roolUpQualifyPercent = roolUpQualifyPercent;
//    }
//
//    /**
//     * @return the insuranceType
//     */
//    public int getInsuranceType() {
//        return insuranceType;
//    }
//
//    /**
//     * @param insuranceType the insuranceType to set
//     */
//    public void setInsuranceType(int insuranceType) {
//        this.insuranceType = insuranceType;
//    }
//
//    /**
//     * @return the isGoldenMinutes
//     */
//    public boolean isGoldenMinutes() {
//        return isGoldenMinutes;
//    }
//
//    /**
//     * @param isGoldenMinutes the isGoldenMinutes to set
//     */
//    public void setGoldenMinutes(boolean isGoldenMinutes) {
//        this.isGoldenMinutes = isGoldenMinutes;
//    }
//
//    /**
//     * @return the isRollUp
//     */
//    public boolean isRollUp() {
//        return isRollUp;
//    }
//
//    /**
//     * @param isRollUp the isRollUp to set
//     */
//    public void setRollUp(boolean isRollUp) {
//        this.isRollUp = isRollUp;
//    }
//
//    /**
//     * @return the rollUpQualifyPercent
//     */
//    public double getRollUpQualifyPercent() {
//        return rollUpQualifyPercent;
//    }
//
//    /**
//     * @param rollUpQualifyPercent the rollUpQualifyPercent to set
//     */
//    public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
//        this.rollUpQualifyPercent = rollUpQualifyPercent;
//    }

//    /**
//     * @return the rollUpPremiaPercent
//     */
//    public double getRollUpPremiaPercent() {
//        return rollUpPremiaPercent;
//    }
//
//    /**
//     * @param rollUpPremiaPercent the rollUpPremiaPercent to set
//     */
//    public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
//        this.rollUpPremiaPercent = rollUpPremiaPercent;
//    }

//    /**
//     * @return the referenceInvestmentId
//     */
//    public long getReferenceInvestmentId() {
//        return referenceInvestmentId;
//    }
//
//    /**
//     * @param referenceInvestmentId the referenceInvestmentId to set
//     */
//    public void setReferenceInvestmentId(long referenceInvestmentId) {
//        this.referenceInvestmentId = referenceInvestmentId;
//    }

//    /**
//     * @return the rate
//     */
//    public double getRate() {
//        return rate;
//    }
//
//    /**
//     * @param rate the rate to set
//     */
//    public void setRate(double rate) {
//        this.rate = rate;
//    }

//    /**
//     * @return the marketLastDayClosingLevel
//     */
//    public double getMarketLastDayClosingLevel() {
//        return marketLastDayClosingLevel;
//    }
//
//    /**
//     * @param marketLastDayClosingLevel the marketLastDayClosingLevel to set
//     */
//    public void setMarketLastDayClosingLevel(double marketLastDayClosingLevel) {
//        this.marketLastDayClosingLevel = marketLastDayClosingLevel;
//    }

//	/**
//	 * @return the acceptedSms
//	 */
//	public boolean isAcceptedSms() {
//		return acceptedSms;
//	}
//
//	/**
//	 * @param acceptedSms the acceptedSms to set
//	 */
//	public void setAcceptedSms(boolean acceptedSms) {
//		this.acceptedSms = acceptedSms;
//	}

//    /**
//     * @return the insuranceFlag
//     */
//    public Integer getInsuranceFlag() {
//        return insuranceFlag;
//    }
//
//    /**
//     * @param insuranceFlag the insuranceFlag to set
//     */
//    public void setInsuranceFlag(Integer insuranceFlag) {
//        this.insuranceFlag = insuranceFlag;
//    }

//    /**
//     * @return the rolledInvId
//     */
//    public long getRolledInvId() {
//        return rolledInvId;
//    }
//
//    /**
//     * @param rolledInvId the rolledInvId to set
//     */
//    public void setRolledInvId(long rolledInvId) {
//        this.rolledInvId = rolledInvId;
//    }

//    /**
//     * check if the investment was rolled up or not
//     * @return true if it was rolled else false
//     */
//    public boolean isRolledUp() {
//        return isCanceled == 1 && rolledInvId > 0 ? true : false;
//    }

//    /**
//     * check if the investment is roll up or not.
//     * @return true if it is roll up else false
//     */
//    public boolean isRollUpBought() {
//        return referenceInvestmentId > 0 && insuranceAmountRU > 0 ? true : false;
//    }

//    /**
//     * check if the investment was bought as GM or not.
//     * @return true if it was bought as GM else false
//     */
//    public boolean isGmBought() {
//        return null != insuranceFlag && insuranceFlag.intValue() == 1 && insuranceAmountGM > 0 ? true : false;
//    }

//	/**
//	 * @return the isOpenGraph
//	 */
//	public boolean isOpenGraph() {
//		return isOpenGraph;
//	}
//
//	/**
//	 * @param isOpenGraph the isOpenGraph to set
//	 */
//	public void setOpenGraph(boolean isOpenGraph) {
//		this.isOpenGraph = isOpenGraph;
//	}

//	/**
//	 * @return the sumInvest
//	 */
//	public long getSumInvest() {
//		return sumInvest;
//	}
//
//	public String getSumInvestTxt() {
//		return CommonUtil.displayAmount(sumInvest, currencyId);
//	}
//
//	public String getSumInvestWithText() {
//		return CommonUtil.getMessage("right.nav.investments.sum.invest", null) + getSumInvestTxt();
//	}
//
//	/**
//	 * @param sumInvest the sumInvest to set
//	 */
//	public void setSumInvest(long sumInvest) {
//		this.sumInvest = sumInvest;
//	}

//    /**
//     * @return the insuranceAmountGM
//     */
//    public long getInsuranceAmountGM() {
//        return insuranceAmountGM;
//    }
//
//    public String getInsuranceAmountGMTxt() {
//        return CommonUtil.displayAmount(insuranceAmountGM, currencyId);
//    }

//    /**
//     * @param insuranceAmountGM the insuranceAmountGM to set
//     */
//    public void setInsuranceAmountGM(long insuranceAmountGM) {
//        this.insuranceAmountGM = insuranceAmountGM;
//    }
//
//    public String getInsuranceAmountRUTxt() {
//        return CommonUtil.displayAmount(insuranceAmountRU, currencyId);
//    }

//    /**
//	 * check if this investment opp is publish by checking if we pass the first time to invest
//	 * @return true if its publish else false
//	 */
//	public boolean isInvestmentOppIsPublish() {
//		long secLeft = timeFirstInvest.getTime() - System.currentTimeMillis();
//		if (secLeft < 0) { //if we pass the time first invest
//        	return true;
//        }
//		return false;
//	}
//
//	//take out from the amount the fees.
//	public long getAmountWithoutFees() {
//		return amount - insuranceAmountGM - insuranceAmountRU - optionPlusFee;
//	}
//
//	public long getBaseAmountWithoutFees() {
//		return baseAmount - Math.round(insuranceAmountGM * rate) - Math.round(insuranceAmountRU * rate) - Math.round(optionPlusFee * rate);
//	}
//
//	public String getAmountWithoutFeesTxt() {
//		return CommonUtil.displayAmount(getAmountWithoutFees(), currencyId);
//	}

//	/**
//	 * @return the bonusLoseOdds
//	 */
//	public double getBonusLoseOdds() {
//		return bonusLoseOdds;
//	}
//
//	/**
//	 * @param bonusLoseOdds the bonusLoseOdds to set
//	 */
//	public void setBonusLoseOdds(double bonusLoseOdds) {
//		this.bonusLoseOdds = bonusLoseOdds;
//	}
//
//	/**
//	 * @return the bonusWinOdds
//	 */
//	public double getBonusWinOdds() {
//		return bonusWinOdds;
//	}
//
//	/**
//	 * @param bonusWinOdds the bonusWinOdds to set
//	 */
//	public void setBonusWinOdds(double bonusWinOdds) {
//		this.bonusWinOdds = bonusWinOdds;
//	}

//	/**
//	 * @return the transactionAmount
//	 */
//	public long getTransactionAmount() {
//		return transactionAmount;
//	}
//
//	/**
//	 * @param transactionAmount the transactionAmount to set
//	 */
//	public void setTransactionAmount(long transactionAmount) {
//		this.transactionAmount = transactionAmount;
//	}
//
//	public String getTransactionAmountTxt() {
//		return CommonUtil.displayAmount(transactionAmount, currencyId);
//	}
//
//	public String getBonusName() {
//		if (bonusOddsChangeTypeId > 0) {
//			return CommonUtil.getMessage(bonusDisplayName, null);
//		}
//		return null;
//	}
//
//	public boolean isShowOffAllowed() {
//		// just investments with timeSettled from last week
//		if (null == timeSettled) {
//			return false;
//		}
//		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.DAY_OF_MONTH, -7);
//		return timeSettled.getTime() >= calendar.getTimeInMillis();
//	}
//
//	// win - lose
//	public long getUserProfit() {
//		return win-lose;
//	}
//
//	public String getShowOffText() throws SQLException {
//		if (null != showOffText) {
//			return showOffText;
//		}
//		final int SENTENCES_NUM = 4;
//		if (shSen < 0) {
//			shSen = new Random().nextInt(SENTENCES_NUM);
//		}
//
//		String name = userFirstName + " " + userLastName.substring(0,1) + ":";
//		//tempstringArr[0] = date ,tempstringArr[2] = time
//		String[] tempStringArr = CommonUtil.getDateAndTimeShowOffAsArray(timeCreated,utcOffsetCreated);
//		String[] showParam = new String[] {
//				CommonUtil.removeFractionFromAmount(getAmountTxt()),
//				getTypeName(),
//				getMarketName(),
//				tempStringArr[0],
//				CommonUtil.removeFractionFromAmount(InvestmentFormatter.getRefundTxt(this)),
//				String.valueOf(CommonUtil.getDateDistance(getTimeCreated(), getTimeSettled(), null)),
//				tempStringArr[2]};
//		String show = CommonUtil.getMessage("showOff.sentence." + String.valueOf(shSen), showParam);
//		String twitterShow = CommonUtil.getMessage("showOff.twitter.sentence." + String.valueOf(shSen), showParam);
//
//		name = CommonUtil.capitalizeFirstLetters(name);
//
//		showOffTwitterText = name + " " + twitterShow + " anyoption.com";
//
//		name = name.replaceAll("�", "&amp;lsquo;");
//		name = name.replaceAll("�", "&amp;rsquo;");
//		name = name.replaceAll("'", "\\\\'");
//
//		show = show.replaceAll("�", "&amp;lsquo;");
//		show = show.replaceAll("�", "&amp;rsquo;");
//		show = show.replaceAll("'", "\\\\'");
//
//		//showOffShortText = name + " " + show;
//		showOffFacebookText = name + " " + show + " www.anyoption.com";
//
//		String agree = CommonUtil.getMessage("showOff.agree", null);
//		agree = agree.replaceAll("�", "&amp;lsquo;");
//		agree = agree.replaceAll("�", "&amp;rsquo;");
//		agree = agree.replaceAll("'", "\\\\'");
//
//		return "<strong>" + name + "</strong>&#160;" +
//			show + "<br /><br />" + agree;
//	}
//
//	/**
//	 * @return
//	 */
//	public boolean isBinary0100() {
//		boolean isBinary0100 = false;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY || typeId == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
//			isBinary0100 = true;
//		}
//		return isBinary0100;
//	}
//	
//	/**
//	 * @return
//	 */
//	public boolean isBubbles() {
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUBBLES) {
//			return true;
//		}
//		return false;
//	}
//
//
//	/**
//	 * @return
//	 * @throws Exception
//	 */
//	public ArrayList<Long> getInvestmentsConnectedIDByInvId() throws Exception {
//    	return InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(id);
//    }
//
//	/**
//	 * @return
//	 * @throws Exception
//	 */
//	public Long getLastInvestmentsConnectedIDByInvId() throws Exception {
//    	ArrayList<Long> invIds = InvestmentsManagerBase.getInvestmentsConnectedIDByInvId(id);
//
//    	return invIds.get(invIds.size() -1);
//    }
//
//	public String getOppCurrentLevelTxt() {
//			return CommonUtil.formatLevelByDecimalPoint(oppCurrentLevel, decimalPoint);
//	}
//
//	public boolean isBinary0100Buy() {
//		boolean isBinary0100Buy = false;
//		if (typeId == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
//			isBinary0100Buy = true;
//		}
//		return isBinary0100Buy;
//	}
//
//	public String getShowOffFacebookText() throws SQLException {
//		if (null == showOffFacebookText) {
//			getShowOffText();
//		}
//		return showOffFacebookText;
//	}
//
//	public String getShowOffTwitterCleanText() throws SQLException {
//		if (null == showOffTwitterText) {
//			getShowOffText();
//		}
//		return showOffTwitterText;
//	}
//
//	public String getShowOffTwitterText() throws SQLException {
//
//		String text = getShowOffTwitterCleanText();
//		text = text.replaceAll("�", "&amp;lsquo;");
//		text = text.replaceAll("�", "&amp;rsquo;");
//		text = text.replaceAll("'", "\\\\'");
//		return text;
//	}
//
//	public String getShowOffTwitterTextEncoded() throws SQLException, UnsupportedEncodingException {
//		return URLEncoder.encode(getShowOffTwitterCleanText(), "UTF-8");
//	}
//
//	public boolean isHasShowOff() {
//		return hasShowOff;
//	}
//
//	public void setHasShowOff(boolean hasShowOff) {
//		this.hasShowOff = hasShowOff;
//	}
//
//	public int getShSen() {
//		return shSen;
//	}
//
//	public void setShSen(int shSen) {
//		this.shSen = shSen;
//	}
//
//	public void setShowOffText(String showOffText) {
//		this.showOffText = showOffText;
//	}
//
//	public long getCountryId() {
//		return countryId;
//	}
//
//	public void setCountryId(long countryId) {
//		this.countryId = countryId;
//	}
//
//	public String getShowOffName() {
//		return showOffName;
//	}
//
//	public void setShowOffName(String showOffName) {
//		this.showOffName = showOffName;
//	}
//
//    /**
//     * @return the isOptionPlus
//     */
//    public boolean isOptionPlus() {
//        return isOptionPlus;
//    }
//
//    /**
//     * @param isOptionPlus the isOptionPlus to set
//     */
//    public void setOptionPlus(boolean isOptionPlus) {
//        this.isOptionPlus = isOptionPlus;
//    }
//
//    /**
//     * @return the isSeen
//     */
//    public boolean isSeen() {
//        return isSeen;
//    }

//    /**
//     * @param isSeen the isSeen to set
//     */
//    public void setSeen(boolean isSeen) {
//        this.isSeen = isSeen;
//    }
//
//    public String getOpportunityTypeName() {
//    	String oppName = "";
//
//    	if (isTypeBinary0100Above()) {
//    		oppName = Opportunity.TYPE_BINARY_0_100_ABOVE_S;
//    	} else if (isTypeBinary0100Below()) {
//    		oppName = Opportunity.TYPE_BINARY_0_100_BELOW_S;
//    	}
//    	return oppName;
//    }
//
//    /**
//     * @return the isOptionPlus
//     */
//    public boolean isOptionPlusOpportunityTypeId() {
//        return (opportunityTypeId == Opportunity.TYPE_OPTION_PLUS);
//    }
//
//    public String getOptionPlusFeeTxt() {
//    	 return CommonUtil.displayAmount(optionPlusFee, currencyId);
//    }
//
//	public String getShowOffTime() {
//		SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
//		return sd.format(showOffTime);
//	}
//
//	public void setShowOffTime(String showOffTime) throws ParseException  {
//		SimpleDateFormat sdf = new SimpleDateFormat
//	     ("dd.MM.yyyy HH:mm:ss");
//
//		this.showOffTime = sdf.parse(showOffTime);
//	}
//
//	public String getBaseProfitTxt() {
//		return CommonUtil.displayAmount(getBaseProfit(),ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public long getBaseProfit() {
//		return (baseAmount - InvestmentsManagerBase.getBaseRefund(this));
//	}
//
//	public long getBaseWinLose() {
//		return (-1) * (baseWin - baseLose);
//	}
//
//	public String getBaseWinLoseTxt() {
//		return CommonUtil.displayAmount(getBaseWinLose(),ConstantsBase.CURRENCY_BASE_ID);
//	}
//	
//	public String getCustomerBaseWinLoseTxt() {
//		return CommonUtil.displayAmount(getBaseWinLose()*(-1),ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getBaseWinLoseTxt0100() {
//		return CommonUtil.displayAmount((-1) * (baseWin - baseLose) * investmentsCount / (contractsStep != 0 ? contractsStep : 1), ConstantsBase.CURRENCY_BASE_ID);
//	}
//	
//	public String getCustomerBaseWinLoseTxt0100() {
//		return CommonUtil.displayAmount((baseWin - baseLose) * investmentsCount / (contractsStep != 0 ? contractsStep : 1), ConstantsBase.CURRENCY_BASE_ID);
//	}
//
//	public String getStyleColor(long total) {
//		if ( total > 0 ) {
//			return "user_strip_value_darkgreen";
//		} else {
//			return "user_strip_value_red";
//		}
//	}
//
//	public String getProfitStyleColor() {
//		return getStyleColor(getBaseProfit());
//	}
//
//	/**
//	 * @return the timeQuoted
//	 */
//	public Date getTimeQuoted() {
//		return timeQuoted;
//	}
//
//	/**
//	 * @param timeQuoted the timeQuoted to set
//	 */
//	public void setTimeQuoted(Date timeQuoted) {
//		this.timeQuoted = timeQuoted;
//	}

//	/**
//	 * @return the isSpecialCare
//	 */
//	public boolean isSpecialCare() {
//		return isSpecialCare;
//	}
//
//	/**
//	 * @param isSpecialCare the isSpecialCare to set
//	 */
//	public void setSpecialCare(boolean isSpecialCare) {
//		this.isSpecialCare = isSpecialCare;
//	}

//	public long getShowOffId() {
//		return showOffId;
//	}
//
//	public void setShowOffId(long showOffId) {
//		this.showOffId = showOffId;
//	}
//
//	public void setShowOffFacebookText(String showOffFacebookText) {
//		this.showOffFacebookText = showOffFacebookText;
//	}
//
//    /**
//     * @return the specialCare
//     */
//    public SpecialCare getSpecialCare() {
//        return specialCare;
//    }
//
//    /**
//     * @param specialCare the specialCare to set
//     */
//    public void setSpecialCare(SpecialCare specialCare) {
//        this.specialCare = specialCare;
//    }
//
//	public String getLastShowOffMessegeKey() {
//		return lastShowOffMessegeKey;
//	}
//
//	public void setLastShowOffMessegeKey(String lastShowOffMessegeKey) {
//		this.lastShowOffMessegeKey = lastShowOffMessegeKey;
//	}
//
//	public String getWriterUserName() {
//		return writerUserName;
//	}
//
//	public void setWriterUserName(String writerUserName) {
//		this.writerUserName = writerUserName;
//	}
//
//	/**
//	 * @return the oppCurrentLevel
//	 */
//	public double getOppCurrentLevel() {
//		return oppCurrentLevel;
//	}
//
//	/**
//	 * @param oppCurrentLevel the oppCurrentLevel to set
//	 */
//	public void setOppCurrentLevel(double oppCurrentLevel) {
//		this.oppCurrentLevel = oppCurrentLevel;
//	}

//	/**
//	 * @return the investmentsCount
//	 */
//	public long getInvestmentsCount() {
//		return investmentsCount;
//	}
//
//	/**
//	 * @return the investmentsCount * rate
//	 */
//	public String getBaseInvestmentsCount() {
//		return CommonUtil.formatLevelByDecimalPoint(investmentsCount * rate, 2);
//	}
//
//	/**
//	 * @param investmentsCount the investmentsCount to set
//	 */
//	public void setInvestmentsCount(long investmentsCount) {
//		this.investmentsCount = investmentsCount;
//	}
//
//	/**
//	 * @return the investmentsCount
//	 */
//	public ArrayList<Long> getInvestmentsCountArray(long currencyId) {
//        int step = BINARY0100_CONTRACTS_STEPS[(int) currencyId];
//        int steps = (int) investmentsCount / step;
//		ArrayList<Long> list = new ArrayList<Long>();
//		for (long i = 1; i <= steps; i++) {
//			list.add(i * step);
//		}
//		return list;
//	}
//
//	/**
//	 * @return the selectedInvestmentsCount
//	 */
//	public long getSelectedInvestmentsCount() {
//		return selectedInvestmentsCount;
//	}
//
//	/**
//	 * @param selectedInvestmentsCount the selectedInvestmentsCount to set
//	 */
//	public void setSelectedInvestmentsCount(long selectedInvestmentsCount) {
//		this.selectedInvestmentsCount = selectedInvestmentsCount;
//	}
//
//	// TODO actual getters and setters
//	public String getSelectedInvestmentsCountTxt() {
//        return String.valueOf(selectedInvestmentsCount);
//    }
//
//	// TODO actual getters and setters
//	public void setSelectedInvestmentsCountTxt(String selectedInvestmentsCount) {
//	    try {
//	        this.selectedInvestmentsCount = Long.parseLong(selectedInvestmentsCount);
//	    } catch (Exception e) {
//	        log.error("Can't set selectedInvestmentsCount.", e);
//	        this.selectedInvestmentsCount = 1;
//	    }
//    }
//
//	/**
//	 * @param selectedInvestmentsCount and InvestmentsCount the selectedInvestmentsCount to set
//	 */
//	public void setSelectedAndInvestmentsCount(long selectedInvestmentsCount) {
//		this.selectedInvestmentsCount = selectedInvestmentsCount;
//		this.investmentsCount = selectedInvestmentsCount;
//	}
//
//	public boolean isSettledBeforTime() {
//		if (timeEstClosing == null || timeSettled == null) {
//			return false;
//		}
//		Calendar estClosing = Calendar.getInstance();
//		estClosing.setTime(timeEstClosing);
//		Calendar timeSettledCal = Calendar.getInstance();
//		timeSettledCal.setTime(timeSettled);
//		return timeSettledCal.before(estClosing);
//	}
//
//	public String getTimeCreatedTxtByFormat(String format) {
//        return CommonUtil.getTimeFormat(timeCreated, utcOffsetCreated, format);
//    }
//
//	public String getTimeEstClosingTxtByFormat(String format) {
//        return CommonUtil.getTimeFormat(timeEstClosing, utcOffsetCreated, format);
//    }
//
//	public String getTimeSettledTxtByFormat(String format) {
//        return CommonUtil.getTimeFormat(timeSettled, utcOffsetCreated, format);
//    }
//
//	public boolean isTypeBinary0100Above() {
//		boolean above = false;
//		if (opportunityTypeId == Opportunity.TYPE_BINARY_0_100_ABOVE) {
//			above = true;
//		}
//		return above;
//	}
//
//	public boolean isTypeBinary0100Below() {
//		boolean below = false;
//		if (opportunityTypeId == Opportunity.TYPE_BINARY_0_100_BELOW) {
//			below = true;
//		}
//		return below;
//	}
//
//	/**
//	 * @return the countryName
//	 */
//	public String getCountryName() {
//		return countryName;
//	}
//
//	public String getCountryNameForDisplay() {
//		return CommonUtil.getMessage(countryName, null);
//	}
//
//	/**
//	 * @param countryName the countryName to set
//	 */
//	public void setCountryName(String countryName) {
//		this.countryName = countryName;
//	}

//	/**
//	 * @return the opportunityTypeName
//	 */
//	public String getNameOpportunityType() {
//		return nameOpportunityType;
//	}
//
//	/**
//	 * @param opportunityTypeName the opportunityTypeName to set
//	 */
//	public void setNameOpportunityType(String nameOpportunityType) {
//		this.nameOpportunityType = nameOpportunityType;
//	}
//
//	/**
//	 * @return the profitUser
//	 */
//	public long getProfitUser() {
//		return profitUser;
//	}
//
//	/**
//	 * @param profitUser the profitUser to set
//	 */
//	public void setProfitUser(long profitUser) {
//		this.profitUser = profitUser;
//	}
//
//	/**
//	 *
//	 * @return ProfitUser with USD symbol
//	 */
//	public String getProfitUserTxt() {
//		return CommonUtil.displayAmount(profitUser, ConstantsBase.CURRENCY_USD_ID);
//	}
//
//	/**
//	 * @return the cityName
//	 */
//	public String getCityName() {
//		return cityName;
//	}
//
//	/**
//	 * @param cityName the cityName to set
//	 */
//	public void setCityName(String cityName) {
//		this.cityName = cityName;
//	}
//
//	public int getBinary0100NumberOfContracts() {
//		int numOfContracts = (int) Math.round((amount - optionPlusFee) / (currentLevel * 100));
//		if (numOfContracts == 0) {
//			numOfContracts = 1;
//		}
//		return numOfContracts;
//	}
//
//    public int getContractsStep() {
//        return contractsStep;
//    }
//
//    public void setContractsStep(int contractsStep) {
//        this.contractsStep = contractsStep;
//    }
//
//    public String getDateSettledLiveAO(){
//		return CommonUtil.getDateFormat(timeEstClosing, utcOffsetSettled);
//	}
//
//    public String getTimeSettledLiveAO(){
//		return CommonUtil.getTimeFormat(timeEstClosing);
//	}
//    public String getPrintBinary0100Event() {
//		String result = null;
//		String[] params = null;
//		params=new String[3];
//		params[0]=getMarketNameForDisplay();
//		params[1]=getOppCurrentLevelTxt();
//		params[2]=getTimeEstClosingTxtByFormat("HH:mm");
//		String messageKeyAbove = "opportunity.above";
//		String messageKeyBelow = "opportunity.below";
//		
//		if( CommonUtil.getLocaleLanguage().contains(ConstantsBase.ETRADER_LOCALE) && getMarketNameForDisplay().toUpperCase().contains("DAX")){
//			messageKeyAbove = "opportunity.dax.above";
//			messageKeyBelow = "opportunity.dax.below";
//		}
//		
//		if (opportunityTypeId == Opportunity.TYPE_BINARY_0_100_ABOVE) {
//			result = CommonUtil.getMessage(messageKeyAbove, params);
//		}
//		if (opportunityTypeId == Opportunity.TYPE_BINARY_0_100_BELOW) {
//			result = CommonUtil.getMessage(messageKeyBelow, params);
//		}
//
//		return result;
//	}

//	/**
//	 * @return the fromGraph
//	 */
//	public int getFromGraph() {
//		return fromGraph;
//	}
//
//	/**
//	 * @param fromGraph the fromGraph to set
//	 */
//	public void setFromGraph(int fromGraph) {
//		this.fromGraph = fromGraph;
//	}
//
//    public long getUserBalance() {
//        return userBalance;
//    }
//
//    public void setUserBalance(long userBalance) {
//        this.userBalance = userBalance;
//    }
//
//    public String getUserBalanceTxt() {
//        return CommonUtil.displayAmount(userBalance, currencyId);
//    }
//
//	/**
//	 * @return the isHaveReutersQuote
//	 */
//	public boolean isHaveReutersQuote() {
//		return isHaveReutersQuote;
//	}
//
//	/**
//	 * @param isHaveReutersQuote the isHaveReutersQuote to set
//	 */
//	public void setHaveReutersQuote(boolean isHaveReutersQuote) {
//		this.isHaveReutersQuote = isHaveReutersQuote;
//	}
//
//	/**
//	 * @return the insuranceCurrentLevels
//	 */
//	public Map<SkinGroup, String> getInsuranceCurrentLevels() {
//		return insuranceCurrentLevels;
//	}
//
//	/**
//	 * @param insuranceCurrentLevels the insuranceCurrentLevels to set
//	 */
//	public void setInsuranceCurrentLevels(Map<SkinGroup, String> insuranceCurrentLevels) {
//		this.insuranceCurrentLevels = insuranceCurrentLevels;
//	}
//
//	/**
//	 * @return the apiExternalUserReference
//	 */
//	public String getApiExternalUserReference() {
//		return apiExternalUserReference;
//	}
//
//	/**
//	 * @param apiExternalUserReference the apiExternalUserReference to set
//	 */
//	public void setApiExternalUserReference(String apiExternalUserReference) {
//		this.apiExternalUserReference = apiExternalUserReference;
//	}
//	
//	/**
//	 * 
//	 * 
//	 * @return the fee for all contracts 
//	 */
//	public long getTotalFee() {
//		return (investmentsCount * optionPlusFee) / getBinary0100NumberOfContracts();
//	}
//	
//	public String getTotalFeeTxt() {
//		return CommonUtil.displayAmount(getTotalFee(), currencyId);
//	}
//
//	public long getCopyOpInvId() {
//		return copyOpInvId;
//	}
//
//	public void setCopyOpInvId(long copyOpInvId) {
//		this.copyOpInvId = copyOpInvId;
//	}

//	public int getCopyOpTypeId() {
//		return copyOpTypeId;
//	}
//
//	public void setCopyOpTypeId(int copyOpTypeId) {
//		this.copyOpTypeId = copyOpTypeId;
//	}
//
//	public boolean isLikeHourly() {
//		return isLikeHourly;
//	}
//
//	public void setLikeHourly(boolean isLikeHourly) {
//		this.isLikeHourly = isLikeHourly;
//	}
//
//	
//	public long getCopyopRelevantInvBE() {
//		return copyOpInvId != 0 ? copyOpInvId : id;
//	}
//	
//	public String returnBubblesWinTxt(long currencyId) {
//		Currency currency = CurrenciesManagerBase.getCurrency(currencyId);
//		return CommonUtil.formatCurrencyAmount((amount * (1 + getOddsWin())/100), true, currency);
//	}
//	
//	public String returnBubblesLoseTxt(long currencyId) {
//		Currency currency = CurrenciesManagerBase.getCurrency(currencyId);
//		return CommonUtil.formatCurrencyAmount((amount * (1 - getOddsLose())/100), true, currency);
//	}
//
//}