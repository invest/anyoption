//package il.co.etrader.bl_vos;
//
//import java.io.Serializable;
//
//public class RequestedFiles implements Serializable{
//
//	/**
//	 *
//	 */
//	private static final long serialVersionUID = 1L;
//
//	private boolean isVerificationCall;
//	private boolean isPowerOfAttorney;
//	private boolean isSentHolderIdCopy;
//	private boolean isUserHaveIdCopy;
//	private boolean isSentCcCopy;
//
//
//	/**
//	 *
//	 */
//	public RequestedFiles(){
//		isVerificationCall = false;
//		isPowerOfAttorney = false;
//		isSentHolderIdCopy = false;
//		isUserHaveIdCopy = false;
//		isSentCcCopy = false;
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//	    String retValue = "";
//
//	    retValue = "Request ( "
//	        + super.toString() + TAB
//	        + "isVerificationCall = " + this.isVerificationCall + TAB
//	        + "isPowerOfAttorney = " + this.isPowerOfAttorney + TAB
//	        + "isSentHolderIdCopy = " + this.isSentHolderIdCopy + TAB
//	        + "isUserHaveIdCopy = " + this.isUserHaveIdCopy + TAB
//	        + "isSentCcCopy = " + this.isSentCcCopy + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	/**
//	 * @return the isPowerOfAttorney
//	 */
//	public boolean isPowerOfAttorney() {
//		return isPowerOfAttorney;
//	}
//
//	/**
//	 * @param isPowerOfAttorney the isPowerOfAttorney to set
//	 */
//	public void setPowerOfAttorney(boolean isPowerOfAttorney) {
//		this.isPowerOfAttorney = isPowerOfAttorney;
//	}
//
//	/**
//	 * @return the isSentHolderIdCopy
//	 */
//	public boolean isSentHolderIdCopy() {
//		return isSentHolderIdCopy;
//	}
//
//	/**
//	 * @param isSentHolderIdCopy the isSentHolderIdCopy to set
//	 */
//	public void setSentHolderIdCopy(boolean isSentHolderIdCopy) {
//		this.isSentHolderIdCopy = isSentHolderIdCopy;
//	}
//
//	/**
//	 * @return the isUserHaveIdCopy
//	 */
//	public boolean isUserHaveIdCopy() {
//		return isUserHaveIdCopy;
//	}
//
//	/**
//	 * @param isUserHaveIdCopy the isUserHaveIdCopy to set
//	 */
//	public void setUserHaveIdCopy(boolean isUserHaveIdCopy) {
//		this.isUserHaveIdCopy = isUserHaveIdCopy;
//	}
//
//	/**
//	 * @return the isVerificationCall
//	 */
//	public boolean isVerificationCall() {
//		return isVerificationCall;
//	}
//
//	/**
//	 * @param isVerificationCall the isVerificationCall to set
//	 */
//	public void setVerificationCall(boolean isVerificationCall) {
//		this.isVerificationCall = isVerificationCall;
//	}
//
//	/**
//	 * @return the isSentCcCopy
//	 */
//	public boolean isSentCcCopy() {
//		return isSentCcCopy;
//	}
//
//	/**
//	 * @param isSentCcCopy the isSentCcCopy to set
//	 */
//	public void setSentCcCopy(boolean isSentCcCopy) {
//		this.isSentCcCopy = isSentCcCopy;
//	}
//
//}
