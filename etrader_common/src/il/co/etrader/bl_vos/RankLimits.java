package il.co.etrader.bl_vos;

import java.util.HashMap;

public class RankLimits implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private double limit;
	private HashMap<Long, Double> statusLimit;
	
	public RankLimits() {
		statusLimit = new HashMap<Long, Double>();
	}

	/**
	 * @return the statusLimit
	 */
	public HashMap<Long, Double> getStatusLimit() {
		return statusLimit;
	}

	/**
	 * @param statusLimit the statusLimit to set
	 */
	public void setStatusLimit(HashMap<Long, Double> statusLimit) {
		this.statusLimit = statusLimit;
	}

	/**
	 * @return the limit
	 */
	public double getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(double limit) {
		this.limit = limit;
	}
		
}
