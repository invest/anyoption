
package il.co.etrader.dao_managers;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;

public class ContactsDAO extends DAOBase{
	  public static void insertContactRequest(Connection con,Contact vo, boolean importLeadsScreen) throws SQLException {
		  PreparedStatement ps = null;
		  int index = 1;
		  try {
			  String sql = "INSERT INTO CONTACTS(" +
					  			"id," +
					  			"name," +
					  			"type," +
					  			"phone," +
					  			"user_id," +
					  			"skin_id," +
					  			"country_id," +
					  			"email," +
					  			"time_created," +
					  			"writer_id," +
					  			"time_updated," +
					  			"combination_id," +
					  			"dynamic_parameter," +
					  			"utc_offset," +
					  			"ip," +
					  			"phone_type, " +
					  			"user_agent," +
					  			"dfa_placement_id," +
					  			"dfa_creative_id," +
					  			"dfa_macro," +
					  			"first_name, " +
					  			"last_name, " +
					  			"mobile_phone, " +
					  			"land_line_phone, " +
					  			"is_contact_by_email, " +
					  			"is_contact_by_sms, " +
					  			"device_unique_id, " +
					  			"affiliate_key, " +
					  			"sub_affiliate_key, " +
					  			"http_referer, " +
					  			"time_first_visit, " +
		                    	"aff_sub1, " +
		                    	"aff_sub2, " +
		                    	"aff_sub3" +
					  			" ) " +
			  			"VALUES(?,?,?,?,?,?,?,?,SYSDATE,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(index++, vo.getId());
			  ps.setString(index++,vo.getText());
			  ps.setLong(index++,vo.getType());
			  ps.setString(index++, vo.getPhone());
			  ps.setLong(index++, vo.getUserId());
			  ps.setLong(index++, vo.getSkinId());
			  ps.setLong(index++,vo.getCountryId());
			  ps.setString(index++, vo.getEmail());
			  ps.setLong(index++, vo.getWriterId());
			  ps.setLong(index++, vo.getCombId());

			  String decodedDP = null;
			  String dynamicParam = vo.getDynamicParameter();
			  if (null != dynamicParam) {
				   try {
					  // decode dynamic Parameter.
					  decodedDP = URLDecoder.decode(dynamicParam, "UTF-8");
				   } catch (Exception e) {
				   	  decodedDP = dynamicParam;
				   }
			  }
			  String decodedHttpReferer = null;
			  String httpReferer = vo.getHttpReferer();
			  if (null != httpReferer) {
				   try {
					  // decode http referer.
					   decodedHttpReferer = URLDecoder.decode(httpReferer, "UTF-8");
				   } catch (Exception e) {
					   decodedHttpReferer = httpReferer;
				   }
			  }
			  ps.setString(index++, decodedDP);
			  if (importLeadsScreen) {
				  ps.setString(index++, vo.getUtcOffset()); //On import leads screen we add offset manually
			  } else {
				  ps.setString(index++, CommonUtil.getUtcOffset());
			  }
              ps.setString(index++, vo.getIp());
              ps.setString(index++, vo.getPhoneType());
              ps.setString(index++, vo.getUserAgent());
              ps.setString(index++, vo.getDfaPlacementId());
              ps.setString(index++, vo.getDfaCreativeId());
              ps.setString(index++, vo.getDfaMacro());
              ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
              ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
              ps.setString(index++, vo.getMobilePhone());
              ps.setString(index++, vo.getLandLinePhone());
              ps.setInt(index++, vo.isContactByEmail()? 1 : 0);
              ps.setInt(index++, vo.isContactBySMS() ? 1 : 0);
              ps.setString(index++, vo.getDeviceUniqueId());
				if (vo.getAffiliateKey() > 0) {
					ps.setLong(index++, vo.getAffiliateKey());
				} else {
					ps.setString(index++, null);
				}
				if (vo.getSubAffiliateKey() > 0) {
					ps.setLong(index++, vo.getSubAffiliateKey());
				} else {
					ps.setString(index++, null);
				}
              ps.setString(index++, decodedHttpReferer);
              ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeFirstVisit()));
              ps.setString(index++, vo.getAff_sub1());
              ps.setString(index++, vo.getAff_sub2());
              ps.setString(index++, vo.getAff_sub3());

			  ps.executeUpdate();
		  }	finally	{
				closeStatement(ps);
		  }
	  }

	/**
	 * Update contact users
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  
	  public static void updateIsContactByEmail(Connection con,Contact c) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =
	                    "UPDATE " +
	                        " contacts c " +
	                    "SET " +
	                    	" c.is_contact_by_email = ? " +
	                    "WHERE " +
	                        " c.id = ? ";
	            pstmt = con.prepareStatement(sql);
	            pstmt.setInt(1, c.isContactByEmail() ? 1 : 0);
	            pstmt.setLong(2, c.getId());
	            pstmt.executeUpdate();
	            
	        } finally {
	            closeStatement(pstmt);
	        }

	  }
	public static void updateContact(Connection con,Contact vo) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =
	                    "UPDATE " +
	                        " contacts " +
	                    "SET " +
		                    " time_updated = sysdate, " +
		                	" utc_offset = ?, " +
		                	" skin_id = ?, " +
		                	" first_name = ?, " +
		                	" last_name = ?, " +
		                	" email = ?, " +
		                	" country_id = ?, " +
		                	" mobile_phone = ?, " +
		                	" land_line_phone = ?, " +
		                	" ip = ?, " +
		                	" user_agent = ?, " +
		                	" writer_id = ?, " +
		                	" device_unique_id = ?, " +
		                	" user_id = ?, " +
	                    	" is_contact_by_email = ?, " +
	                    	" is_contact_by_sms = ?, " +
	                    	" dfa_placement_id = ?, " +
	                    	" dfa_creative_id = ?, " +
	                    	" dfa_macro = ? " +
	                    "WHERE " +
	                        " id = ? ";
	            pstmt = con.prepareStatement(sql);
	            pstmt.setString(1, vo.getUtcOffset());
	            pstmt.setLong(2, vo.getSkinId());
	            pstmt.setString(3, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
	            pstmt.setString(4, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
	            pstmt.setString(5, vo.getEmail());
	            pstmt.setLong(6, vo.getCountryId());
	            pstmt.setString(7, vo.getMobilePhone());
	            pstmt.setString(8, vo.getLandLinePhone());
	            pstmt.setString(9, vo.getIp());
	            pstmt.setString(10, vo.getUserAgent());
	            pstmt.setLong(11, vo.getWriterId());
	            pstmt.setString(12, vo.getDeviceUniqueId());
	            pstmt.setLong(13, vo.getUserId());
	            pstmt.setInt(14, vo.isContactByEmail() ? 1 : 0);
	            pstmt.setInt(15, vo.isContactBySMS() ? 1 : 0);
	            pstmt.setString(16, vo.getDfaPlacementId());
	            pstmt.setString(17, vo.getDfaCreativeId());
	            pstmt.setString(18, vo.getDfaMacro());
	            pstmt.setLong(19, vo.getId());

	            pstmt.executeUpdate();
	        } finally {
	            closeStatement(pstmt);
	        }

	  }

	  public static void updateContactRequest(Connection con,long contactId,long userId) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "UPDATE CONTACTS SET user_id =?,time_updated=SYSDATE where id=?";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userId);
			  ps.setLong(2,contactId);
			  ps.executeUpdate();
		  }	finally 	{
				closeStatement(ps);
			}
	  }

	  public static Contact getById(Connection con,long contactId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  Contact vo=null;

		  try
			{
			  String sql="select * from contacts where id = ? ";

			   	ps = con.prepareStatement(sql);
				ps.setLong(1, contactId);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo = getVo(rs);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;

	  }

	  /**
	   * Is contact exists, if yes return contactId
	   * @param con db connection
	   * @param phone phone number of existing check
	   * @param email email of existing check
	   * @return
	   * @throws SQLException
	   */
	  public static long isExists(Connection con, String phone, String email) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql =
				  " SELECT " +
			  	  	" id " +
			  	  " FROM " +
		   	 	  	" contacts " +
  			      " WHERE " +
  			   	  	" phone like ? " +
  			   	  	" OR mobile_phone like ? " +
  			   	  	" OR land_line_phone like ? " +
  			   	  	" OR email like ? ";

			  ps = con.prepareStatement(sql);
			  ps.setString(1, phone);
			  ps.setString(2, phone);
			  ps.setString(3, phone);
			  ps.setString(4, email);
			  rs = ps.executeQuery();

			  if(rs.next()) {
				  return rs.getLong("id");
			  }

		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return 0;
	  }

	  /**
	     * checks if there's a contact with the same phone number as the new user.
	     * @param connection
	     * @param user
	     * @throws SQLException
	     */
	    public static Contact searchFreeContactByUserPhone(Connection con, UserBase user) throws SQLException {
	    	PreparedStatement ps = null;
			ResultSet rs = null;
			int x = 1;

	    	try {

	    		String sql = " SELECT " +
   					 "		* " +
   					 " FROM " +
   					 "		contacts " +
   					 " WHERE " +
   					 "		user_id = 0 " +
   					 "		AND id = MARKETING.GET_FREE_CONTACT_DETAILS(?, ?, ?, ?, ?) ";


					ps = con.prepareStatement(sql);
					ps.setLong(x++, user.getCountryId());
					ps.setString(x++, user.getLandLinePhone());
					ps.setString(x++, user.getMobilePhone());
					ps.setString(x++, user.getEmail());
					ps.setLong(x++, user.getSkinId());

				rs = ps.executeQuery();

	        	if (rs.next()){
	        		Contact vo = getVo(rs);
					return vo;
	        	}
	        } finally {
	        	closeResultSet(rs);
				closeStatement(ps);
	        }
	        return null;
	    }

		/**
	     * checks if there's a user with the same email/mobile in order to connect contact with user
	     * @param connection
	     * @param user
	     * @throws SQLException
	     */
	    public static long searchUserIdFromContactDetails(Connection con, Contact contact) throws SQLException {
	    	PreparedStatement ps = null;
			ResultSet rs = null;
			long contactId = 0;
			int index = 1;
	    	try {
	    		String sql = " SELECT " +
	    						" u.id, " +
	    					    " CASE " + //email is more "powerfull" than mobile in etrader.
	    				        	" WHEN u.email = ?  and  u.mobile_phone = ? THEN 1 " +
	    				            " WHEN u.email = ?   THEN 2 " +
	    				            " ELSE 3 " +
	    						" END " +
	    					 " FROM " +
	    					 	" users u " +
	    					 " WHERE ";
	               if (CommonUtil.isHebrewSkin(contact.getSkinId())){
	            	   sql+=	" upper(u.email) = upper(?) " +
	    						" OR u.mobile_phone = ? ";
	               } else {// on anyoption we have duplicate email validation so we need to check only mobile phone
	            	   sql+=	" u.mobile_phone = ? ";
	               }
		               sql+=" ORDER by " +
								    " CASE " + //email is more "powerfull" than mobile in etrader.
							        	" WHEN u.email = ?  and  u.mobile_phone = ? THEN 1 " +
							            " WHEN u.email = ?   THEN 2 " +
							            " ELSE 3 " +
							        " END ";
					ps = con.prepareStatement(sql);
					ps.setString(index++, contact.getEmail());
					ps.setString(index++, contact.getMobilePhone());
					ps.setString(index++, contact.getEmail());
					if (CommonUtil.isHebrewSkin(contact.getSkinId())){
						ps.setString(index++, contact.getEmail());
					}
					ps.setString(index++, contact.getMobilePhone());
					ps.setString(index++, contact.getEmail());
					ps.setString(index++, contact.getMobilePhone());
					ps.setString(index++, contact.getEmail());
				rs = ps.executeQuery();
	        	if (rs.next()){
	        		contactId = rs.getLong("id");
					return contactId;
	        	}
	        } finally {
	        	closeResultSet(rs);
				closeStatement(ps);
	        }
	        return contactId;
	    }

	    /**
	     * checks if there's a contact with the same phone number as the new user.
	     * @param connection
	     * @param user
	     * @throws SQLException
	     */
	    public static Contact getVo(ResultSet rs) throws SQLException {
	    	Contact vo = new Contact();

			vo.setId(rs.getLong("id"));
			vo.setText(rs.getString("name"));
			vo.setType(rs.getLong("type"));
			vo.setPhone(rs.getString("phone"));
			vo.setUserId(rs.getLong("user_id"));
			vo.setEmail(rs.getString("email"));
			vo.setSkinId(rs.getLong("skin_id"));
			vo.setCountryId(rs.getLong("country_id"));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setCombId(rs.getLong("combination_id"));
			vo.setDynamicParameter(rs.getString("dynamic_parameter"));
            vo.setAff_sub1(rs.getString("aff_sub1"));
            vo.setAff_sub2(rs.getString("aff_sub2"));
            vo.setAff_sub3(rs.getString("aff_sub3"));
			vo.setUtcOffset(rs.getString("utc_offset"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            vo.setIp(rs.getString("ip"));
            vo.setFirstName(rs.getString("first_name"));
            vo.setLastName(rs.getString("last_name"));
            vo.setMobilePhone(rs.getString("mobile_phone"));
            vo.setLandLinePhone(rs.getString("land_line_phone"));
            vo.setContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL")==1?true:false);
            vo.setContactBySMS(rs.getInt("IS_CONTACT_BY_SMS")==1?true:false);
            vo.setDeviceUniqueId(rs.getString("DEVICE_UNIQUE_ID"));
            vo.setUserAgent(rs.getString("USER_AGENT"));
            vo.setDfaPlacementId(rs.getString("dfa_placement_id"));
            vo.setDfaCreativeId(rs.getString("dfa_creative_id"));
            vo.setDfaMacro(rs.getString("dfa_macro"));
            vo.setHttpReferer(rs.getString("http_referer"));
			return vo;
	    }

		public static Contact getContactByEmail(Connection con, String email) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Contact contact = null;

			try {
				String sql = " SELECT " +
						     "		* " +
						     " FROM " +
						     "		( " +
						     "		 SELECT " +
						     "			  c.* " +
						     "		 FROM " +
						     "			  contacts c " +
						     "		 WHERE " +
						     "			  c.email like ? " +
						     "		 ORDER BY " +
						     "			  c.time_created DESC " +
						     "		) " +
						     " WHERE " +
						     "	    rownum = 1 ";
				ps = con.prepareStatement(sql);
				ps.setString(1, email);
				rs = ps.executeQuery();
				if (rs.next()) {
					contact = new Contact();
					contact = getVo(rs);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return contact;
		}

		public static void updateDuplicatesContactsByEmail(Connection con, Contact contact) throws SQLException {
			PreparedStatement ps = null;

			try {
				String sql = " UPDATE " +
							 "		contacts " +
							 " SET " +
							 "		is_contact_by_email = ? " +
							 " WHERE " +
							 "		email like ? " +
							 "      AND user_id = 0 ";


				ps = con.prepareStatement(sql);
				ps.setInt(1, contact.isContactByEmail() ? 1 : 0);
				ps.setString(2, contact.getEmail());
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}
		
		public static  String getContactTypeById(Connection con,Long contactId, Long typeId) throws SQLException{
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			StringBuilder sb = new StringBuilder();
			String type = null;
			
			try{
				
				sb.append("SELECT ct.name FROM contacts c ,contact_types ct WHERE c.type = ct.id ");
				sb.append("AND ct.id = ? ");
				sb.append("AND c.id = ?");
				String sql = sb.toString();
				ps = con.prepareStatement(sql);
				ps.setLong(1, typeId);
				ps.setLong(2, contactId);
				
				rs= ps.executeQuery();
				
				if (rs.next()) {
					
					type = rs.getString(1);
				}
				
				return type;
				
			}finally{
				
				closeStatement(ps);
				
			}
			
		}

	/**
	 * Insert contact list	
	 * @param con
	 * @param list
	 * @throws SQLException
	 */
	public static void insertContactList(Connection con,ArrayList<Contact> list) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "INSERT INTO CONTACTS(" +
					  			"id," +
					  			"name," +
					  			"type," +
					  			"phone," +
					  			"user_id," +
					  			"skin_id," +
					  			"country_id," +
					  			"email," +
					  			"time_created," +
					  			"writer_id," +
					  			"time_updated," +
					  			"combination_id," +
					  			"dynamic_parameter," +
					  			"utc_offset," +
					  			"ip," +
					  			"phone_type, " +
					  			"user_agent," +
					  			"dfa_placement_id," +
					  			"dfa_creative_id," +
					  			"dfa_macro," +
					  			"first_name, " +
					  			"last_name, " +
					  			"mobile_phone, " +
					  			"land_line_phone, " +
					  			"is_contact_by_email, " +
					  			"is_contact_by_sms, " +
					  			"device_unique_id, " +
					  			"affiliate_key, " +
					  			"sub_affiliate_key, " +
					  			"http_referer, " +
					  			"time_first_visit, " +
		                    	"aff_sub1, " +
		                    	"aff_sub2, " +
		                    	"aff_sub3" +
					  			" ) " +
			  			"VALUES(SEQ_CONTACTS_ID.nextval,?,?,?,?,?,?,?,SYSDATE,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
			  ps = con.prepareStatement(sql);
			  for (Contact vo : list) {
				  int index = 1;
				  ps.setString(index++,vo.getText());
				  ps.setLong(index++,vo.getType());
				  ps.setString(index++, vo.getPhone());
				  ps.setLong(index++, vo.getUserId());
				  ps.setLong(index++, vo.getSkinId());
				  ps.setLong(index++,vo.getCountryId());
				  ps.setString(index++, vo.getEmail());
				  ps.setLong(index++, vo.getWriterId());
				  ps.setLong(index++, vo.getCombId());

				  String decodedDP = null;
				  String dynamicParam = vo.getDynamicParameter();
				  if (null != dynamicParam) {
					   try {
						  // decode dynamic Parameter.
						  decodedDP = URLDecoder.decode(dynamicParam, "UTF-8");
					   } catch (Exception e) {
					   	  decodedDP = dynamicParam;
					   }
				  }
				  String decodedHttpReferer = null;
				  String httpReferer = vo.getHttpReferer();
				  if (null != httpReferer) {
					   try {
						  // decode http referer.
						   decodedHttpReferer = URLDecoder.decode(httpReferer, "UTF-8");
					   } catch (Exception e) {
						   decodedHttpReferer = httpReferer;
					   }
				  }
				  ps.setString(index++, decodedDP);
				  ps.setString(index++, vo.getUtcOffset());
	              ps.setString(index++, vo.getIp());
	              ps.setString(index++, vo.getPhoneType());
	              ps.setString(index++, vo.getUserAgent());
	              ps.setString(index++, vo.getDfaPlacementId());
	              ps.setString(index++, vo.getDfaCreativeId());
	              ps.setString(index++, vo.getDfaMacro());
	              ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
	              ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
	              ps.setString(index++, vo.getMobilePhone());
	              ps.setString(index++, vo.getLandLinePhone());
	              ps.setInt(index++, vo.isContactByEmail()? 1 : 0);
	              ps.setInt(index++, vo.isContactBySMS() ? 1 : 0);
	              ps.setString(index++, vo.getDeviceUniqueId());
					if (vo.getAffiliateKey() > 0) {
						ps.setLong(index++, vo.getAffiliateKey());
					} else {
						ps.setString(index++, null);
					}
					if (vo.getSubAffiliateKey() > 0) {
						ps.setLong(index++, vo.getSubAffiliateKey());
					} else {
						ps.setString(index++, null);
					}
	              ps.setString(index++, decodedHttpReferer);
	              ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeFirstVisit()));
	              ps.setString(index++, vo.getAff_sub1());
	              ps.setString(index++, vo.getAff_sub2());
	              ps.setString(index++, vo.getAff_sub3());
				  ps.addBatch();
			  }
			  ps.executeBatch();
		  }	finally	{
				closeStatement(ps);
		  }
	  }
	
	/**
	 * Is contact by affiliate exists
	 * @param con
	 * @param contact
	 * @return boolean
	 * @throws SQLException
	 */
	public static boolean isContactByAffiliateExists(Connection con, Contact contact) throws SQLException {
		int index = 1;
		boolean isContactExists = false;
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " co.id " +
                " FROM " +
                    " contacts co " +
                " WHERE " +
                    " co.aff_sub1 = ? ";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(index++, contact.getAff_sub1());
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	isContactExists = true;
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(pstmt);
        }
        return isContactExists;
	}
}
