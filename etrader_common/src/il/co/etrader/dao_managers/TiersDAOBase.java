package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.TierPointConversion;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.util.CommonUtil;


/**
 * TiersDAO class for all tiers queries
 *
 * @author Kobi
 *
 */
public class TiersDAOBase extends DAOBase {

  /**
   * get tierUser instance by id
   * @param con db connection
   * @param id tierUser id of the user
   * @return TierUser instnace
   * @throws SQLException
   */
  public static TierUser getTierUserById(Connection con, long id) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  TierUser tierUser = null;

	  try {
		    String sql =
		    	"SELECT " +
		    		"tu.*, " +
		    		"t.description " +
		    	"FROM " +
		    		"tier_users tu, " +
		    		"tiers t " +
		    	"WHERE " +
		    		"tu.tier_id = t.id AND " +
		    		"tu.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				tierUser = getTierUserVO(rs);
				tierUser.setTierName(rs.getString("description"));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return tierUser;
  }


  /**
   * Update user points after invest
   * @param con db connection
   * @param id tierUser id
   * @param writerId writer that perform this action
   * @param points loyalty updated points of the user after invest
   * @throws SQLException
   */
  public static void updateUserTier(Connection con, long id, long writerId, long points) throws SQLException {
	  PreparedStatement ps = null;

	  try {
		  String sql =
			  "UPDATE " +
			  	"tier_users " +
			  "SET " +
			  	"writer_id = ?, " +
			  	"points = ? " +
			  "WHERE " +
			  	"id = ? ";

		  ps = con.prepareStatement(sql);
		  ps.setLong(1, writerId);
		  ps.setLong(2, points);
		  ps.setLong(3, id);
		  ps.executeUpdate();

	  } finally {
		  closeStatement(ps);
	  }
  }

  /**
   * Insert into Tier users history table
   * @param con db connection
   * @param tH TierUserHistory instance for the insert
   * @throws SQLException
   */
  public static void insertIntoTierHistory(Connection con, TierUserHistory tH) throws SQLException {
	  PreparedStatement ps = null;

	  try {
		  String sql =
			  		"INSERT INTO " +
			  			"tier_users_history(id, tier_id, user_id, tier_his_action," +
			  								"time_created,writer_id, points, key_value, action_points, table_name, utc_offset) " +

			  		 "VALUES " +
			  		 	"(SEQ_TIER_USERS_HISTORY.NEXTVAL,?,?,?,sysdate,?,?,?,?,?,?)";

		  ps = con.prepareStatement(sql);
		  ps.setLong(1, tH.getTierId());
		  ps.setLong(2, tH.getUserId());
		  ps.setLong(3, tH.getTierHistoryActionId());
		  ps.setLong(4, tH.getWriterId());
		  ps.setLong(5, tH.getPoints());
		  ps.setLong(6, tH.getKeyValue());
		  ps.setLong(7, tH.getActionPoints());
		  ps.setString(8, tH.getTableName());
		  ps.setString(9, tH.getUtcOffset());
		  ps.executeUpdate();

	  } finally {
		  closeStatement(ps);
	  }
  }

  /**
   * get all tiers
   * @param con
   * @return
   * @throws SQLException
   */
  public static HashMap<Long, Tier> getTiers(Connection con) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  HashMap<Long, Tier> list = new HashMap<Long, Tier>();

	  try {
		    String sql =
		    	"SELECT " +
		    		"* " +
		    	"FROM " +
		    		"tiers " +
		    	"ORDER BY id";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Tier t = getTierVO(rs);
				list.put(new Long(t.getId()), t);
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
  }

  /**
   * Get all points to cash convertion options
   * @param con
   * @return
   * @throws SQLException
   */
  public static ArrayList<TierPointConversion> getTierPointsConvertions(Connection con) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  ArrayList<TierPointConversion> list = new ArrayList<TierPointConversion>();

	  try {
		    String sql =
		    	"SELECT " +
		    		"* " +
		    	"FROM " +
		    		"tier_points_conversion " +
		    	"ORDER BY " +
		    		"from_num_points DESC";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				TierPointConversion t = new TierPointConversion();
				t.setId(rs.getLong("id"));
				t.setFromNumPoints(rs.getLong("from_num_points"));
				t.setToNumPoints(rs.getLong("to_num_points"));
				t.setPercent(rs.getLong("percent"));
				list.add(t);
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
  }

  /**
   * Get tiersLevels  by skinId
   * @param con
   * @param skinId
   * @return
   * @throws SQLException
   */
  public static HashMap<Long, Long> getTierLevels(Connection con, long skinId) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  HashMap<Long, Long> list = new HashMap<Long, Long>();

	  try {
		    String sql =
		    	"SELECT " +
		    		"* " +
		    	"FROM " +
		    		"tier_skins " +
		    	"WHERE " +
		    		"skin_id = ? " +
		    	"ORDER BY " +
		    		"tier_id, tier_level";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while (rs.next()) {
				list.put(new Long(rs.getLong("tier_id")), new Long(rs.getLong("tier_level")));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
  }

  /**
   * Get points to cash percents
   * @param con
   * @param points points that the user want to convert
   * @return
   * @throws SQLException
   */
  public static double getPointToCashPercent(Connection con, long points) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  double perc = 0;

	  try {
		    String sql =
		    	"SELECT " +
		    		"(percent/100) as percent " +
		    	"FROM " +
		    		"tier_points_conversion " +
		    	"WHERE " +
		    		" ? >= from_num_points AND " +
		    		" ? <= to_num_points ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, points);
			ps.setLong(2, points);

			rs = ps.executeQuery();

			if (rs.next()) {
				perc = rs.getDouble("percent");
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return perc;
  }

  /**
   *  Get invest points by investment id
   * @param con
   * @param investmentId the investment that the user maybe got points
   * @return
   * @throws SQLException
   */
  public static long getInvetPointsByInvestmentId(Connection con, long investmentId) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  long points = 0;

	  try {
		    String sql =
		    	"SELECT " +
		    		"tuh.action_points " +
		    	"FROM " +
		    		"tier_users_history tuh, " +
		    		"investments i " +
		    	"WHERE " +
		    		"i.id = ? AND " +
                    "(i.insurance_flag <> 2 OR " +
                    "i.insurance_amount_ru = 0 AND " +
                    "i.insurance_flag = 2) AND " +
		    		"i.user_id = tuh.user_id AND " +
		    		"tuh.key_value = i.id AND " +
		    		"tuh.tier_his_action = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, investmentId);
			ps.setLong(2, TiersManagerBase.TIER_ACTION_OBTAIN_POINTS);
			rs = ps.executeQuery();

			if (rs.next()) {
				points = rs.getLong("action_points");
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return points;
  }

  /**
   * Get tierUSer bu userId
   * @param con db connection
   * @param userId tier details of this user
   * @return
   * @throws SQLException
   */
  public static TierUser getTierUserByUserId(Connection con, long userId) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  TierUser vo = null;

	  try {
		    String sql =
		    	"SELECT " +
		    		"tu.*, " +
		    		"t.description " +
		    	"FROM " +
		    		"tier_users tu, " +
		    		"tiers t, " +
		    		"users u " +
		    	"WHERE " +
		    		"tu.user_id = ? AND " +
		    		"tu.tier_id = t.id AND " +
		    		"u.tier_user_id = tu.id ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				vo = getTierUserVO(rs);
				vo.setTierName(rs.getString("description"));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
  }

  /**
   * Get accumulated points by userId and qualification date
   * @param con db connection
   * @param userId user for calculate his accumulate points
   * @param qualificationDate qualification tier date
   * @return
   * @throws SQLException
   */
  public static long getAccumulatedPoints(Connection con, long userId, Date qualificationDate) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  long points = 0;

	  try {
		    String sql =
		    	"SELECT GET_LOYALTY_ACCUMULATED_POINTS(?,?) AS points " +
		    	"FROM dual ";

		    ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(qualificationDate));
			rs = ps.executeQuery();

			if (rs.next()) {
				points = rs.getLong("points");
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return points;
  }


  /**
   * Get TierVo
   * @param rs
   * @return
   * @throws SQLException
   */
  protected static Tier getTierVO(ResultSet rs) throws SQLException {
	  Tier vo = new Tier();
	  vo.setId(rs.getLong("id"));
	  vo.setQualificationPoints(rs.getLong("qualification_points"));
	  vo.setMaintenancePoints(rs.getLong("maintenance_points"));
	  vo.setMinQualificationPeriod(rs.getLong("min_qualification_period"));
	  vo.setExpirationDays(rs.getLong("expiration_days"));
	  vo.setConvertMinPoints(rs.getLong("convert_min_points"));
	  vo.setConvertBenefitPercent(rs.getLong("convert_benefit_percent"));
	  vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
	  vo.setWriterId(rs.getLong("writer_id"));
	  vo.setLoyaltyStatment(rs.getBoolean("is_loyalty_statement"));
	  vo.setCustomerSupport(rs.getBoolean("is_customer_support"));
	  vo.setFinancialWorkshop(rs.getBoolean("is_financial_workshop"));
	  vo.setGoldenMinutes(rs.getBoolean("is_golden_minutes"));
	  vo.setFreeSms(rs.getBoolean("is_free_sms"));
	  vo.setPersonalGifts(rs.getBoolean("is_personal_gift"));
	  vo.setFastWithdrawals(rs.getBoolean("is_fast_withdrawals"));
	  vo.setFreeWithdrawals(rs.getBoolean("is_free_withdrawals"));
	  vo.setVipCustomerSupport(rs.getBoolean("is_vip_customer_support"));
	  vo.setVipCustomerSupport(rs.getBoolean("is_vip_account_manager"));
	  vo.setPersonalSupportManager(rs.getBoolean("is_personal_support_manager"));
	  vo.setHaveSpecialBonuses(rs.getBoolean("is_have_special_bonuses"));
	  vo.setDescription(rs.getString("description"));
	  vo.setWelcomeBonusId(rs.getLong("welcome_bonus_id"));
	  vo.setWelcomeTemplateId(rs.getLong("welcome_template_id"));
  	  return vo;
  }

  /**
   * get TierUserVO
   * @param rs
   * @return
   * @throws SQLException
   */
  protected static TierUser getTierUserVO(ResultSet rs) throws SQLException {
		TierUser tierUser = new TierUser();
		tierUser.setId(rs.getLong("id"));
		tierUser.setUserId(rs.getLong("user_id"));
		tierUser.setTierId(rs.getLong("tier_id"));
		tierUser.setPoints(rs.getLong("points"));
		tierUser.setWriterId(rs.getLong("writer_id"));
		tierUser.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		tierUser.setQualificationTime(convertToDate(rs.getTimestamp("qualification_time")));
		return tierUser;
  }


  /**
   * get tier by id
   * @param tierId
   * @return
   * @throws SQLException
   */
  public static Tier getTierById(Connection con,long tierId) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  Tier tier = null;

	  try {
		    String sql =
		    	"SELECT " +
		    		"* " +
		    	"FROM " +
		    		"tiers " +
		    	"Where id=" + tierId;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs.next()) {
				tier = getTierVO(rs);
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return tier;
  }

}
