package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.daos.MessagesDAOBase;

import il.co.etrader.bl_vos.EmailAuthentication;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;

/**
 * EmailAuthentication DAO.
 *
 * @author Kobi
 */
public class EmailAuthenticationDAOBase extends MessagesDAOBase {

	public static boolean insert(Connection conn, long userId, String email, String authKey, long writerId) throws SQLException {
		PreparedStatement ps = null;
		int res;
        try {
            String sql =
            	"INSERT INTO email_authentication (id, user_id, email, authentication_key, time_created, writer_id) " +
            	"VALUES (seq_email_authentication.nextval, ?, ?, ?, sysdate, ?)";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setString(2, email);
            ps.setString(3, authKey);
            ps.setLong(4, writerId);
            res = ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
        return res > 0;
	}

	public static EmailAuthentication get(Connection conn, String authKey, boolean addClick) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmailAuthentication ea = null;
        try {
            String sql =
            	"SELECT " +
            		"ea.*, " +
            		"u.user_name " +
            	"FROM " +
            		"email_authentication ea, " +
            		"users u " +
            	"WHERE " +
            		"ea.authentication_key = ? AND " +
            		"ea.user_id = u.id";

            ps = conn.prepareStatement(sql);
            ps.setString(1, authKey);
            rs = ps.executeQuery();
            if (rs.next()) {
            	ea = getVO(rs);
            	ea.setUserName(rs.getString("user_name"));
            	if (addClick) {
            		ea.setClicksNum(ea.getClicksNum()+1);
            		addClick(conn, ea);
            	}
            }
        } finally {
        	closeStatement(ps);
        }
        return ea;
	}

	private static int addClick(Connection conn, EmailAuthentication ea) throws SQLException {
		PreparedStatement ps = null;
		int res;
		try {
			String sql =
					"UPDATE email_authentication SET clicks_num = ?";
			if (ea.getClicksNum() == 1) {
				sql +=
					", time_first_clicked = sysdate";
			}
			sql +=	" WHERE id = ?";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, ea.getClicksNum());
			ps.setLong(2, ea.getId());
			res = ps.executeUpdate();
		} finally {
        	closeStatement(ps);
        }
		return res;
	}

	/**
	 * Get number of times that the user get activation email link
	 * @param conn
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getNumOfActivationsSent(Connection conn, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long sentTimes = 0;
        try {
            String sql =
            	"SELECT " +
                	"COUNT(*) sentTimes " +
                "FROM " +
                	"EMAIL_AUTHENTICATION " +
                "WHERE " +
                	"user_id = ? AND " +
                	"writer_id = ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setLong(2, Writer.WRITER_ID_WEB);

            rs = ps.executeQuery();
            if (rs.next()) {
            	sentTimes = rs.getLong("sentTimes");
            }
        } finally {
        	closeStatement(ps);
        }
        return sentTimes;
	}

	private static EmailAuthentication getVO(ResultSet rs) throws SQLException {
		EmailAuthentication ea = new EmailAuthentication();
		ea.setId(rs.getLong("id"));
		ea.setUserId(rs.getLong("user_id"));
		ea.setAuthenticationKey(rs.getString("authentication_key"));
		ea.setEmail(rs.getString("email"));
		ea.setTimeCreated(rs.getDate("time_created"));
		ea.setTimeFirstClicked(rs.getDate("time_first_clicked"));
		ea.setClicksNum(rs.getLong("clicks_num"));

    	return ea;
	}

	public static int updateAuthorizationTime(Connection conn, long mailId, Date timeAuthorized) throws SQLException {
		PreparedStatement ps = null;
		int res;
		try {
			String sql =
					" UPDATE email_authentication " +
					" SET time_authorized = ? ";
			sql +=	" WHERE id = ? ";

			ps = conn.prepareStatement(sql);
			ps.setTimestamp(1,CommonUtil.convertToTimeStamp(timeAuthorized));
			ps.setLong(2, mailId);
			res = ps.executeUpdate();
		} finally {
        	closeStatement(ps);
        }
		return res;
	}
}