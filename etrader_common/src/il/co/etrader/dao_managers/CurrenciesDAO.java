//package il.co.etrader.dao_managers;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.daos.DAOBase;
//
//public class CurrenciesDAO extends com.anyoption.common.daos.CurrenciesDAOBase{
//
//		  public static ArrayList<Currency> getAll(Connection con) throws SQLException {
//			  PreparedStatement ps = null;
//			  ArrayList<Currency> list = new ArrayList<Currency>();
//			  try {
//				    String sql="select * from Currencies ";
//
//					ps = con.prepareStatement(sql);
//
//					ResultSet rs=ps.executeQuery();
//
//					while (rs.next()) {
//						list.add(getVO(rs));
//					}
//				} finally {
//					closeStatement(ps);
//				}
//				return list;
//		  }
//
//		  public static HashMap<Long, Currency> getAllCurrencies(Connection con) throws SQLException {
//			  PreparedStatement ps = null;
//			  HashMap<Long, Currency> hm = new LinkedHashMap<Long, Currency>();
//			  ResultSet rs=null;
//			  try {
//				    String sql="select * from Currencies ";
//
//					ps = con.prepareStatement(sql);
//
//					rs=ps.executeQuery();
//
//					while (rs.next()) {
//						hm.put(rs.getLong("id"), getVO(rs));
//					}
//
//				} finally {
//					closeStatement(ps);
//					closeResultSet(rs);
//				}
//				return hm;
//		  }
//
//		  public static Currency getDefault(Connection con) throws SQLException {
//			  PreparedStatement ps = null;
//			  try {
//				    String sql="select * from Currencies where is_default=1";
//
//					ps = con.prepareStatement(sql);
//
//					ResultSet rs = ps.executeQuery();
//
//					if (rs.next()) {
//						return getVO(rs);
//					}
//
//				} finally {
//					closeStatement(ps);
//				}
//				return null;
//		  }
//
//		  private static Currency getVO(ResultSet rs) throws SQLException {
//			  Currency vo = new Currency();
//			  vo.setId(rs.getLong("id"));
//			  vo.setIsLeftSymbol(rs.getInt("is_left_symbol"));
//			  vo.setSymbol(rs.getString("symbol"));
//			  vo.setDefaultSymbol(rs.getString("default_symbol"));
//			  vo.setCode(rs.getString("Code"));
//			  vo.setNameKey(rs.getString("name_key"));
////				vo.setMinDeposit(rs.getInt("min_deposit"));
////				vo.setMinWithdraw(rs.getInt("min_withdraw"));
//			  vo.setDisplayName(rs.getString("display_name"));
//			  vo.setDecimalPointDigits(rs.getInt("DEC_PNT_DIGITS"));
//			  return vo;
//		  }
//		  
// 
//	/**
//	 * @param connection
//	 * @return
//	 * @throws SQLException
//	 */
//	public static Map<Integer, Currency> getCurrenciesMaping(Connection connection) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Map<Integer, Currency> currencies = new LinkedHashMap<Integer, Currency>();
//		try {
//			String sql =
//				"SELECT " +
//			    "	* " +
//				"FROM " +
//				"	currencies ";
//			ps = connection.prepareStatement(sql);
//			rs = ps.executeQuery();
//			while (rs.next()) {
//				currencies.put(rs.getInt("id"), getVO(rs));
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return currencies;
//	}
//} 
// 