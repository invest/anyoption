package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.MessagesDAOBase;

import il.co.etrader.bl_vos.ResetPassword;

/**
 * ResetPassword DAO.
 *
 * @author Asher
 */
public class ResetPasswordDAO extends MessagesDAOBase {

	public static boolean insert(Connection conn, String userName, String resKey) throws SQLException {
		PreparedStatement ps = null;
		int res;
        try {
            String sql =
            	"INSERT INTO reset_password (id, user_id, reset_key, time_created) " +
            	"SELECT seq_reset_password.nextval, id, ?, sysdate " +
            	"FROM users WHERE user_name LIKE ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, resKey);
            ps.setString(2, userName.toUpperCase());
            res = ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
        return res > 0;
	}

	public static ResetPassword get(Connection conn, String resKey, boolean addClick) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResetPassword rp = null;
        try {
            String sql =
            	"SELECT r.*, u.user_name, u.skin_id " +
            	"FROM reset_password r, users u " +
            	"WHERE r.user_id = u.id AND reset_key LIKE ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, resKey);
            rs = ps.executeQuery();
            if (rs.next()) {
            	rp = getVO(rs);
            	if (addClick) {
            		rp.setClicksNum(rp.getClicksNum()+1);
            		addClick(conn, rp);
            	}
            }
        } finally {
        	closeStatement(ps);
        }
        return rp;
	}

	private static int addClick(Connection conn, ResetPassword rp) throws SQLException {
		PreparedStatement ps = null;
		int res;
		try {
			String sql =
					"UPDATE reset_password SET clicks_num = ?";
			if (rp.getClicksNum() == 1) {
				sql +=
					", time_first_clicked = sysdate";
			}
			sql +=	" WHERE id = ?";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, rp.getClicksNum());
			ps.setLong(2, rp.getId());
			res = ps.executeUpdate();
		} finally {
        	closeStatement(ps);
        }
		return res;
	}

	private static ResetPassword getVO(ResultSet rs) throws SQLException {
		ResetPassword rp = new ResetPassword();
    	rp.setId(rs.getLong("id"));
    	rp.setUserId(rs.getLong("user_id"));
    	rp.setUserName(rs.getString("user_name"));
    	rp.setSkinId(rs.getLong("skin_id"));
    	rp.setResetKey(rs.getString("reset_key"));
    	rp.setTimeCreated(rs.getDate("time_created"));
    	rp.setTimeFirstClicked(rs.getDate("time_first_clicked"));
    	rp.setClicksNum(rs.getLong("clicks_num"));

    	return rp;
	}
}