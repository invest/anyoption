package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_vos.MarketingReport;

public class MarketingReportDAO extends DAOBase {

	public static ArrayList<MarketingReport> getDPReportByDate(Connection con,long skinId, long skinBusinessCaseId, Date startDate, Date endDate, String offset, int reportType, long sourceId, long marketingCampaignManager, long writerIdForSkin)
            throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MarketingReport> list = new ArrayList<MarketingReport>();

        try {
            String sql = "Select * from table( MARKETING_REPORTS.GET_MARKETING_DP_REP_BY_DATE(?, ?, ?, ?, ?) ) ";

            if (writerIdForSkin > 0) {
                sql += " where skin_id IN ( select skin_id from writers_skin where writer_id= "
                        + writerIdForSkin + " ) ";
            }

            ps = con.prepareStatement(sql);
            Timestamp startTs = CommonUtil.convertToTimeStamp(startDate);
            Timestamp endTs = CommonUtil.convertToTimeStamp(endDate);

            ps.setTimestamp(1, startTs);
            ps.setTimestamp(2, endTs);
            ps.setLong(3, skinId);
            ps.setLong(4, sourceId);
            ps.setLong(5, marketingCampaignManager);

            rs = ps.executeQuery();

            while (rs.next()) {
                MarketingReport vo = new MarketingReport();

                vo.setDates(rs.getDate("dates"));
                vo.setDynamicParam(rs.getString("dp"));
                vo.setCampaignManager(rs.getString("campaign_manager"));
                vo.setCombId(rs.getLong("combination_id"));
                vo.setCampaignName(rs.getString("campaign_name"));
                vo.setSourceName(rs.getString("source_name"));
                vo.setMedium(rs.getString("medium_name"));
                vo.setContent(rs.getString("content_name"));
                vo.setMarketSizeHorizontal(rs.getLong("marketing_size_horizontal"));
                vo.setMarketSizeVertical(rs.getLong("marketing_size_vertical"));
                vo.setLocation(rs.getString("m_location"));
                vo.setLandingPageName(rs.getString("landing_page_name"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setShortReg(rs.getLong("short_reg"));
                vo.setRegisteredUsersNum(rs.getLong("registered_users_num"));
                vo.setFtd(rs.getLong("ftd"));
                vo.setRfd(rs.getLong("rfd"));
                vo.setFirstRemDepNum(rs.getLong("first_remarketing_dep_num"));
                vo.setHouseWin(rs.getLong("house_win"));
                list.add(vo);
            }

        } finally {
        	closeResultSet(rs);
            closeStatement(ps);
        }

        return list;
    }
	
}
