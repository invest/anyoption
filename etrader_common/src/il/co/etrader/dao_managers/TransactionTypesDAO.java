package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.bl_vos.TransactionType;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.util.CommonUtil;

public class TransactionTypesDAO extends DAOBase {
	  public static void insert(Connection con,TransactionType vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="insert into Transaction_Types(id,code,description,class_type) values(?,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getId());
				ps.setString(2, vo.getCode());
				ps.setString(3, vo.getDescription());
				ps.setLong(4, vo.getClassType());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }

	  public static ArrayList getAll(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList list=new ArrayList();

		  try
			{
			    String sql="select * from Transaction_Types";

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					TransactionType vo=new TransactionType();
					vo.setId(rs.getLong("id"));
					vo.setCode(rs.getString("code"));
					vo.setDescription(CommonUtil.getMessage(rs.getString("description"),null));
					vo.setClassType(rs.getLong("class_type"));

					list.add(vo);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }
	  
	public static Map<Long, String> getAllVisible(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, String> transactionTypes = new HashMap<Long, String>();

		try {
			String sql = " select * from transaction_types where is_displayed = 1 order by id ";

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			while (rs.next()) {
				transactionTypes.put(rs.getLong("id"), rs.getString("description"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return transactionTypes;
	}

	  public static TransactionType getById(Connection con,long id) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  TransactionType vo=null;
		  try
			{
			    String sql="select * from Transaction_Types where id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=new TransactionType();
					vo.setId(rs.getLong("id"));
					vo.setCode(rs.getString("code"));
					vo.setDescription(CommonUtil.getMessage(rs.getString("description"),null));
					vo.setClassType(rs.getLong("class_type"));

				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;

	  }
}
