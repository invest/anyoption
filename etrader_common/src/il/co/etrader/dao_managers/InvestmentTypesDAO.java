package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.daos.DAOBase;

public class InvestmentTypesDAO extends DAOBase{
	  public static void insert(Connection con,InvestmentType vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="insert into Investment_Types(id,name,display_name,writer_id) values(?,?,?,?) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getId());
				ps.setString(2, vo.getName());
				ps.setString(3, vo.getDisplayName());
				ps.setLong(4, vo.getWriterId());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }

		  public static ArrayList getAll(Connection con) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList list=new ArrayList();
			  try {
				    String sql="select * from Investment_Types order by name";
					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()) {
						InvestmentType vo=new InvestmentType();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setDisplayName(rs.getString("display_name"));
						vo.setWriterId(rs.getInt("writer_id"));
						list.add(vo);
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
		  }

		  public static InvestmentType getById(Connection con,long id) throws SQLException {
			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  InvestmentType vo=null;
			  try
				{
				    String sql="select * from Investment_Types where id=?";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new InvestmentType();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setDisplayName(rs.getString("display_name"));
						vo.setWriterId(rs.getInt("writer_id"));

					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }

}

