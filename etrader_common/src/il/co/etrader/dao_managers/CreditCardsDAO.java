package il.co.etrader.dao_managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Bins;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.CreditCardHolderNameHistory;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

public class CreditCardsDAO extends DAOBase{
	private static final Logger logger = Logger.getLogger(CreditCardsDAO.class);

	public static void insert(Connection con, CreditCard vo)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {

		  PreparedStatement psBin=null;
		  ResultSet rsBin=null;
		  PreparedStatement ps = null;
		  ResultSet rs=null;

		  try {
			    String ccBinSql =
			    	" select * " +
			    	" from bins b " +
			    	" where ? between b.from_bin and b.to_bin ";

			    psBin = con.prepareStatement(ccBinSql);
			    psBin.setString(1, vo.getBinStr());
			    rsBin = psBin.executeQuery();

			    if (rsBin.next()){
			    	long countryId = rsBin.getLong("country_id");
			    	long typeIdByBin = rsBin.getLong("card_type_id");

			    	if (countryId != 0){
			    		vo.setCountryId(countryId);
			    	}

			    	if (typeIdByBin != 0){
			    		vo.setTypeIdByBin(typeIdByBin);
			    	}else{
			    		vo.setTypeIdByBin(vo.getTypeId());
			    	}
			    }else{
			    	vo.setTypeIdByBin(vo.getTypeId());
			    }


				String sql=
					" insert into credit_cards(id, user_id, type_id, is_visible, cc_number, cc_number_back, cc_Pass, time_created," +
											 " exp_month, exp_year, holder_name, holder_id_num, time_modified, is_allowed, " +
											 " utc_offset_created, utc_offset_modified, country_id, type_id_by_bin, recurring_transaction, bin, docs_not_required, cc_number_last_4_digits) " +
											 " values(seq_credit_cards.nextval, ?, ?, ?, ?, ?, ?, sysdate, ?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?, '" +
											   ConstantsBase.EMPTY_STRING + "',?, ?, ?) ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getUserId());
				ps.setLong(2, vo.getTypeId());
				ps.setBoolean(3, vo.isVisible());
				ps.setString(4, AESUtil.encrypt(vo.getCcNumber()));
				ps.setLong(5, vo.getCcNumber());
				ps.setNull(6, Types.VARCHAR);

				ps.setString(7, vo.getExpMonth());
				ps.setString(8, vo.getExpYear());
				ps.setString(9, vo.getHolderName());
				ps.setString(10, vo.getHolderIdNum());

				ps.setInt(11, vo.getPermission());
				ps.setString(12, vo.getUtcOffsetCreated());
				ps.setString(13, vo.getUtcOffsetModified());
				ps.setLong(14, vo.getCountryId());
				ps.setLong(15, vo.getTypeIdByBin());
				ps.setLong(16, Long.parseLong(vo.getBinStr()));
				ps.setBoolean(17, vo.isDocsAreNotRequired());
				ps.setString(18, vo.getLast4DigitsStr());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_credit_cards"));

		  } catch (CryptoException ce) {
			  throw new SQLException(ce.getMessage());
		  } finally {
			  closeStatement(ps);
			  closeResultSet(rs);
			  closeStatement(psBin);
			  closeResultSet(rsBin);
		  }
	  }

	public static void update(Connection con, CreditCard vo)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
		  PreparedStatement ps = null;
		  try {
				String sql=
					" UPDATE " +
						" credit_cards " +
					" SET " +
						" user_id=?,type_id=?,is_visible=?,cc_Number=?,cc_Number_back=?," +
						" exp_month=?,exp_year=?,holder_name=?,holder_id_num=?,time_modified=sysdate,is_allowed=?,utc_offset_modified=?," +
						" IS_BELONGS_TO_ACCOUNT_HOLDER =?, IS_VERIFICATION_CALL=?, is_documents_sent = ? , recurring_transaction = ? , bin = ? , docs_not_required = ?, cc_number_last_4_digits = ?" +
					" WHERE " +
						" id=? ";

				ps = con.prepareStatement(sql);
				int index = 1;
				ps.setLong(index++, vo.getUserId());
				ps.setLong(index++, vo.getTypeId());
				ps.setBoolean(index++, vo.isVisible());
				ps.setString(index++, AESUtil.encrypt(vo.getCcNumber()));
				ps.setLong(index++, vo.getCcNumber());
				ps.setString(index++, vo.getExpMonth());
				ps.setString(index++, vo.getExpYear());
				ps.setString(index++, vo.getHolderName());
				ps.setString(index++, vo.getHolderIdNum());
				ps.setInt(index++, vo.getPermission());
				ps.setString(index++, vo.getUtcOffsetModified());
				ps.setInt(index++, vo.isBelongsToAccountHolder()?1:0);
				ps.setInt(index++, vo.isVerificationCall()?1:0);
				ps.setInt(index++, vo.isDocumentsSent()==true?1:0);
				ps.setString(index++, vo.getRecurringTransaction());
				ps.setLong(index++, Long.parseLong(vo.getBinStr()));
				ps.setBoolean(index++, vo.isDocsAreNotRequired());
				ps.setString(index++, vo.getLast4DigitsStr());
				ps.setLong(index++, vo.getId());
				ps.executeUpdate();
		  } catch (CryptoException ce) {
			  throw new SQLException(ce.getMessage());
		  } finally {
			  closeStatement(ps);
		  }
	  }

	  public static void updateCardNotVisible(Connection con,long id) throws SQLException {

		  updateQuery(con,"update credit_cards set is_visible=0 where id="+id);

	  }

	  public static void updateWebDetails(Connection con,long id,String month,String year,String ccPass, String utcoffset) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="update credit_cards set exp_month=?,exp_year=?,time_modified=sysdate " +
				" ,is_visible=1, utc_offset_modified=? where id=?";

				ps = con.prepareStatement(sql);

				int index = 1;
				ps.setString(index++, month);
				ps.setString(index++, year);
				ps.setString(index++, utcoffset);
				ps.setLong(index++, id);
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }

	  public static ArrayList<CreditCard> getAll(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<CreditCard> list=new ArrayList<CreditCard>();

		  try {
			  String sql="select * from credit_cards";

			  ps = con.prepareStatement(sql);
			  rs=ps.executeQuery();

			  while (rs.next()) {
				  CreditCard vo=new CreditCard();
				  vo=getVO(con,rs);

				  list.add(vo);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	 }

	 public static CreditCard getById(Connection con,long id) throws SQLException {
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 CreditCard vo = null;
		 try {
			 String sql=
				 " select " +
				 	" cc.*, " +
				 	" c.country_name, " +
				 	" cct.description cc_type_name " +
				 	" " +
			     " from " +
			     	" users u, " +
			     	" credit_cards cc " +
			     		" left join countries c on c.id = cc.country_id " +
			     		" left join credit_card_types cct on cct.id = cc.type_id_by_bin " +
			     " where " +
			     	" cc.id=? " +
			     	" and cc.user_id = u.id ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);

			  rs=ps.executeQuery();

			  if (rs.next()) {
				  vo = getVO(con,rs);
				  vo.setCountryName(rs.getString("country_name"));
				  vo.setTypeNameByBin(rs.getString("cc_type_name"));
			  }
		  } finally	{
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return vo;
	  }
/*
	  public static CreditCard getOtherCard(Connection con,long userId,long num) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  CreditCard vo=null;
		  try
			{
			    String sql="select * from credit_cards where user_id!=? and cc_number=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, num);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=getVO(con,rs);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;

	  }
*/

	 public static int updateVisibleByUserId(Connection con, long userId, long cardId) throws SQLException{
		 PreparedStatement ps = null;
		 int result = 0;
		  try {

				String sql="UPDATE " +
						           "credit_cards " +
						      "SET " +
						           "is_visible=0 " +
						    "WHERE " +
						           "id=? " +
						      "AND " +
						           "user_id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, cardId);
				ps.setLong(2, userId);
				result = ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
		 return result;
	 }
	 

	  public static ArrayList<CreditCard> getVisibleByUserId(Connection con,long id) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<CreditCard> list=new ArrayList<CreditCard>();

		  CreditCard vo=null;
		  try
			{
			    String sql="select * from credit_cards where user_id=? and is_visible=1 ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				while (rs.next()) {
					vo=getVO(con,rs);
					list.add(vo);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}

			return list;

	  }

	  public static ArrayList<CreditCard> getByUserId(Connection con,long id) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<CreditCard> list=new ArrayList<CreditCard>();

		  CreditCard vo=null;
		  try
			{
			    String sql="select * from credit_cards where user_id=? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				while (rs.next()) {
					vo=getVO(con,rs);
					list.add(vo);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}

			return list;

	  }

	  public static ArrayList<CreditCard> getByUserIdAndFilters(Connection con,long user_id,
			  int documentStatusFilter,int depositsStatusFilter, int documentsRequiredFilter) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<CreditCard>  list=new ArrayList<CreditCard>();

		  CreditCard vo=null;
		  try {
			  String sql=" select " +
			    				" cc.*," +
			    				" user_files.IS_SENT_ID_COPY, " +
			    				" user_files.IS_SENT_WITHDRAWAL_FORM," +
			    				" cc_files.IS_SENT_CC_COPY, " +
			    				" cc_files.IS_POWER_OF_ATTORNEY, " +
			    				" cc_files.IS_SENT_HOLDER_ID_COPY, " +
							 	" c.country_name, " +
							 	" cct.description cc_type_name, " +
							 	" nvl(cc_3d.credit_card_id,0) is_cc_3d " +
			    			" from " +
			    				" credit_cards cc " +
					     			" left join countries c on c.id = cc.country_id " +
					     			" left join credit_card_types cct on cct.id = cc.type_id_by_bin " +
			    					" left join (select " +
												   " cc2.id cc_id, " +
												   " (SUM(CASE WHEN t.status_id in (2,7) THEN 1 ELSE 0 END)) success_deposits " +
											   " from " +
												   " credit_cards cc2, " +
												   " transactions t " +
											   " where " +
			    							   	   " t.credit_card_id = cc2.id " +
												   " and t.type_id = 1 " +
												   " and cc2.user_id = ? " +
											    " group by " +
												   " cc2.id) cc_deposits on cc.id = cc_deposits.cc_id " +
									" left join  (select  " +
													" f.user_id," +
													" SUM(CASE f.FILE_TYPE_ID WHEN ? THEN 1 WHEN ? THEN 1 WHEN ? THEN 1 ELSE 0 END) IS_SENT_ID_COPY, " +
													" SUM(CASE WHEN f.FILE_TYPE_ID = ? THEN 1 ELSE 0 END) IS_SENT_WITHDRAWAL_FORM " +
												" from " +
													" files f " +
												" where " +
													" f.user_id = ? " +
													" and f.IS_APPROVED = 1 " +
													" and f.FILE_STATUS_ID = ? " +
												" group by" +
													" f.user_id) user_files on cc.user_id = user_files.user_id " +
									" left join  (select  " +
													" f.cc_id," +
													" SUM(CASE f.FILE_TYPE_ID WHEN  ? THEN 1 WHEN   ? THEN 1 ELSE 0 END) IS_SENT_CC_COPY, " +
													" SUM(CASE WHEN f.FILE_TYPE_ID = ? THEN 1 ELSE 0 END) IS_POWER_OF_ATTORNEY, " +
													" SUM(CASE WHEN f.FILE_TYPE_ID = ? THEN 1 ELSE 0 END) IS_SENT_HOLDER_ID_COPY " +
												" from " +
													" files f " +
												" where " +
													" f.user_id = ? " +
													" and f.cc_id is not null " +
													" and f.IS_APPROVED = 1 " +
													" and f.FILE_STATUS_ID = ? " +
												" group by" +
													" f.cc_id) cc_files on cc.id = cc_files.cc_id " +

									" left join  (select  " +
													" distinct credit_card_id " +
												" from " +
													" transactions  " +
												" where " +
													" is_3d =1 " +
													" and type_id =1 " +
													" and user_id = ? " +
												" ) cc_3d on cc.id = cc_3d.credit_card_id " +
						
			    			
			    			" where " +
			    				" cc.user_id = ? ";

			  if (documentStatusFilter == ConstantsBase.GENERAL_YES){
				  sql += " and is_documents_sent = 1 ";
			  } else if (documentStatusFilter == ConstantsBase.GENERAL_NO){
				  sql += " and is_documents_sent = 0 ";
			  }

			  if (depositsStatusFilter == ConstantsBase.SUCCESS_DEPOSIT){
				  sql += " and  cc_deposits.success_deposits > 0 ";
			  }else if (depositsStatusFilter == ConstantsBase.FAILED_DEPOSIT){
				  sql += " and  cc_deposits.success_deposits = 0 ";
			  }
			  
			if (documentsRequiredFilter == ConstantsBase.GENERAL_YES) {
				sql += " and cc.docs_not_required = 1 ";
			} else if (documentsRequiredFilter == ConstantsBase.GENERAL_NO) {
				sql += " and cc.docs_not_required = 0 ";
			}

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, user_id);
			  ps.setLong(2, FileType.USER_ID_COPY);
			  ps.setLong(3, FileType.DRIVER_LICENSE);
			  ps.setLong(4, FileType.PASSPORT);
			  ps.setLong(5, FileType.WITHDRAW_FORM);
			  ps.setLong(6, user_id);
			  ps.setLong(7, File.STATUS_DONE);
			  ps.setLong(8, FileType.CC_COPY);
			  ps.setLong(9, FileType.CC_COPY_FRONT);
			  ps.setLong(10, FileType.POWER_OF_ATTORNEY);
			  ps.setLong(11, FileType.CC_HOLDER_ID);
			  ps.setLong(12, user_id);
			  ps.setLong(13, File.STATUS_DONE);
			  ps.setLong(14, user_id);
			  ps.setLong(15, user_id);

			  rs=ps.executeQuery();

			  while (rs.next()) {
				  vo=getVO(con,rs);
				  vo.setCountryName(rs.getString("country_name"));
				  vo.setTypeNameByBin(rs.getString("cc_type_name"));
				  vo.setUserHaveIdCopy(rs.getInt("IS_SENT_ID_COPY")>0?true:false);
				  vo.setUserSentWithdrawalForm(rs.getInt("IS_SENT_WITHDRAWAL_FORM")>0?true:false);
				  vo.setPowerOfAttorney((rs.getInt("IS_POWER_OF_ATTORNEY")>0?true:false));
				  vo.setSentHolderIdCopy((rs.getInt("IS_SENT_HOLDER_ID_COPY")>0?true:false));
				  vo.setSentCcCopy((rs.getInt("IS_SENT_CC_COPY")>0?true:false));
				  vo.setDocsAreNotRequired((rs.getInt("docs_not_required") == 1 ? true : false));
				  vo.setIs3DAttempt((rs.getInt("is_cc_3d") == 0 ? false : true));
				  list.add(vo);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	  }

	  public static ArrayList<CreditCard> getAttachedFilesForCC(Connection con, long user_id, ArrayList<CreditCard> cardList)throws SQLException{
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  if(!cardList.isEmpty()){
				  
			  		String sql = "SELECT " +
		  					 "f.file_name, ft.name " +
		  				"FROM " +
		  				 	"credit_cards cc, files f, file_types ft " +
			  			" WHERE " +
			  			    "cc.id = f.cc_id " +
			  			    " AND f.file_type_id = ft.id " +
			  			    " AND cc.user_id = ? " +
			  			    " AND cc.id = ? " +
			  			    " AND f.file_name is not null ";
				  	ps = con.prepareStatement(sql);
				  	ps.setLong(1, user_id);	
				  Iterator<CreditCard> it = cardList.iterator();
				  	while(it.hasNext()){
				  		CreditCard cc = it.next();
				  		ArrayList<File> fileNameList = new ArrayList<File>();
				  		ps.setLong(2, cc.getId());
				  		rs=ps.executeQuery();
				  		while(rs.next()){
				  			File file = new File();
				  			String fileName = rs.getString("file_name");
				  			String fileTypeName = rs.getString("name");
				  			FileType fileType = new FileType(fileTypeName);
				  			file.setFileName(fileName);
				  			file.setFileType(fileType);
				  			fileNameList.add(file);
				  		}
				  		cc.setCcFiles(fileNameList);
				  	}
			  }
		  }finally{
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return cardList;
		  
	  }
	  
	  public static ArrayList<CreditCard> setCreditCardBinInfo(Connection con, long user_id, ArrayList<CreditCard> cardList)throws SQLException{
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  if(!cardList.isEmpty()){
				  
			  		String sql = "SELECT b.* " +
			  					 " FROM credit_cards cc, bins b " +
			  					  " WHERE cc.bin = b.from_bin " +
			  					  " AND cc.user_id = ? " +
			  					  " AND cc.id = ? ";
				  	ps = con.prepareStatement(sql);
				  	ps.setLong(1, user_id);	
				  Iterator<CreditCard> it = cardList.iterator();
				  	while(it.hasNext()){
				  		CreditCard cc = it.next();
				  		ps.setLong(2, cc.getId());
				  		Bins binInfo = new Bins();
				  		rs=ps.executeQuery();
				  		if(rs.next()){
				  			binInfo.setCardPaymentType(rs.getString("CARD_PAYMENT_TYPE"));
				  			binInfo.setBank(rs.getString("BANK"));
				  			binInfo.setCategory(rs.getString("CATEGORY"));
				  		}
				  		cc.setCcBins(binInfo);;
				  	}
			  }
		  }finally{
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return cardList;
		  
	  }

	public static CreditCard getByNumberAndUserId(	Connection con, long num,
													long id)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  CreditCard vo = null;
		  try {
			  String sql = " SELECT " +
			  			   "	   * " +
			  			   " FROM ( " +
			  			   " 	  SELECT " +
			  			   "	       cc.*, " +
			  			   "	       ia.issue_action_type_id " +
			  			   "      FROM " +
			  			   "	        credit_cards cc LEFT JOIN issues i ON i.credit_card_id = cc.id " +
			               "                            LEFT JOIN issue_actions ia ON ia.issue_id = i.id " +
			               "      WHERE " +
			               "	       cc.cc_number = ? AND " +
			               "           cc.user_id = ? " +
			               "      ORDER BY " +
			               "	       cc.time_created )" +
			               " WHERE ROWNUM = 1 ";

			  ps = con.prepareStatement(sql);
			  ps.setString(1, AESUtil.encrypt(num));
			  ps.setLong(2, id);
			  rs = ps.executeQuery();

			  while (rs.next()) {
				  vo = getVO(con,rs);
				  Long issueActionTypeId = rs.getLong("issue_action_type_id");
				  vo.setManuallyAllowed(false);
				  if (null != issueActionTypeId &&
						  issueActionTypeId.longValue() == ConstantsBase.ISSUE_ACTION_REQUEST_CREDITCARD_CHANGE) {
					  vo.setManuallyAllowed(true);
				  }
				  return vo;
			  }
		  } catch (CryptoException ce) {
			  throw new SQLException(ce.getMessage());
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return null;
	  }

	public static ArrayList<User> checkCardExistence(	Connection con, long num, long id,
														long skinId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																		NoSuchPaddingException, IllegalBlockSizeException,
																		BadPaddingException, UnsupportedEncodingException,
																		InvalidAlgorithmParameterException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<User> users = null;
		  try {
			  String sql=" SELECT " +
			  				" u.user_name username, " +
			  				" u.id user_id " +
			  			 " FROM " +
			  			 	" credit_cards c, " +
			  			 	" users u " +
			  			 " WHERE " +
			  			 	" c.cc_number = ? " +
			  			 	" and c.user_id!=? " +
			  			 	" and u.id=c.user_id ";

			  if (CommonUtil.isHebrewSkin(skinId)) {
				  long skinIdToCompare = Skin.SKIN_ETRADER;
				  if (skinId == Skin.SKIN_TLV) {
					  skinIdToCompare = Skin.SKIN_TLV;
				  }
				  sql +=    " and u.skin_id =  " + skinIdToCompare;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, AESUtil.encrypt(num));
			  ps.setLong(2, id);
			  rs=ps.executeQuery();

			  while (rs.next()) {
				  if (users == null) {
					  users = new ArrayList<User>();
				  }
				  User u = new User();
				  u.setId(rs.getLong("user_id"));
				  u.setUserName(rs.getString("username"));
				  users.add(u);
			  }
			  return users;
		  } catch (CryptoException ce) {
			  throw new SQLException(ce.getMessage());
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
	  }

	  private static CreditCard getVO(Connection con,ResultSet rs) throws SQLException {
		  try {
			  CreditCard vo=new CreditCard();
			  vo.setId(rs.getLong("id"));
			  vo.setUserId(rs.getLong("user_id"));
			  vo.setTypeId(rs.getLong("TYPE_ID"));
			  vo.setVisible(rs.getBoolean("is_visible"));
			  vo.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));
			  vo.setCcPass(rs.getString("CC_PASS"));
			  vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
			  vo.setTimeModified(convertToDate(rs.getTimestamp("TIME_MODIFIED")));
			  vo.setExpMonth(rs.getString("EXP_MONTH"));
			  vo.setExpYear(rs.getString("EXP_YEAR"));
			  vo.setPermission(rs.getInt("is_allowed"));
			  vo.setHolderName(rs.getString("HOLDER_NAME"));
			  vo.setHolderIdNum(rs.getString("HOLDER_ID_NUM"));
			  vo.setType(CreditCardTypesDAO.getById(con,vo.getTypeId()));
			  vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
			  vo.setUtcOffsetModified(rs.getString("utc_offset_modified"));
			  vo.setBelongsToAccountHolder((rs.getInt("IS_BELONGS_TO_ACCOUNT_HOLDER")==1?true:false));
			  vo.setVerificationCall((rs.getInt("IS_VERIFICATION_CALL")==1?true:false));
			  vo.setDocumentsSent((rs.getInt("is_documents_sent")==1?true:false));
			  vo.setCountryId(rs.getLong("country_id"));
			  vo.setTypeIdByBin(rs.getLong("type_id_by_bin"));
			  vo.setRecurringTransaction(rs.getString("RECURRING_TRANSACTION"));
			  vo.setBin(rs.getLong("bin"));

			  return vo;
		} catch (	CryptoException | NumberFormatException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
					| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		}
	  }

		  /**
		   * Return all availble user credit cards, credit cards that the user make some
		   * deposit from them and the deposit is approved / pending.
		   * For not CFT enabled cards(Credit), need to display the credit amount that allow to withdrawal
		   * CFT enabled - in case the bin not! in bin_black_List for withdrawal
		   * To check that the deposit is approved, we take only cc that existing is some transaction
		   * with type of cc_deposit and the transaction status is success or panding
		   * @param con db connection
		   * @param id user id
		   * @return Array list of credit cards
		   * @throws SQLException
		   */
		  public static HashMap<Long,CreditCard> getCcWithrawalByUserId(Connection con,long id, long minToWithdrawal, long skinId, boolean onlyVisibleCards)  throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  HashMap<Long,CreditCard> list = new HashMap<Long,CreditCard>();
			  List<CreditCard> tempList = new ArrayList<CreditCard>();
			  CreditCard vo = null;
//			  Date etraderMcStartDate = null;
			  int index = 1;

			  try {
				  	String sql = " SELECT " +
				  					" c.* " +
								 " FROM " +
								 	" credit_cards c, " +
								 	" users u " +
								 " WHERE " +
									" c.user_id = ? ";
								if (onlyVisibleCards){
						   sql+=	" AND c.is_visible = ? " +
				   					" AND c.is_allowed != ? ";
								}
						   sql+=	" AND c.user_id = u.id " +
									" AND EXISTS ( select 1 " +
													" from credit_cards c1, transactions t1 " +
													" where t1.credit_card_id = c1.id and c1.id = c.id " +
													" and t1.type_id = ? " +
													" and ( t1.status_id = ? or t1.status_id = ? ) " +
												" ) ";


					ps = con.prepareStatement(sql);
					ps.setLong(index++ , id);
					if (onlyVisibleCards){
						ps.setLong(index++ , ConstantsBase.CC_VISIBLE);
						ps.setLong(index++ , CreditCard.NOT_ALLOWED);
					}
					ps.setLong(index++ , TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
					ps.setLong(index++ , TransactionsManagerBase.TRANS_STATUS_SUCCEED);
					ps.setLong(index++ , TransactionsManagerBase.TRANS_STATUS_PENDING);

					rs = ps.executeQuery();

					while (rs.next()) {
						vo = getVO(con, rs);
						tempList.add(vo);
					}
			  } catch (Exception e) {
					logger.error("Error getting user credit cards", e);
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

//				// Get etraderMcEndTime
//				if (CommonUtil.isHebrewSkin(skinId)){
//					try {
//						String sql1 = " select to_date(value,'dd/mm/yyyy') start_time " +
//									  " from  ENUMERATORS " +
//									  " where code = ? ";
//
//						ps = con.prepareStatement(sql1);
//						ps.setString(1, ConstantsBase.ENUM_ET_MC_START_DATE);
//						rs = ps.executeQuery();
//						if (rs.next()) {
//							etraderMcStartDate = CommonUtil.convertToDate(rs.getTimestamp("start_time"));
//						}
//					} catch (Exception e) {
//						logger.error("Error getting etraderMcEndTime", e);
//					} finally {
//						closeResultSet(rs);
//						closeStatement(ps);
//					}
//				}

				try {
					for (int i = 0; i < tempList.size(); i++) {
					String sql =
	  					" SELECT " +
	  						" c.* , " +
						    " u.balance, " +
						    " cr.withdraw_clearing_provider_id, " +
						    " cr.cft_clearing_provider_id, " +
	  						" ( NVL( (select sum (t1.amount - t1.credit_amount) " +
								   " from transactions t1, credit_cards c1 " +
								   " where c1.id = t1.credit_card_id " +
								   		"AND decode(ur.time_created,null,u.time_created,ur.time_created) < t1.time_created " +
								   		"and t1.credit_card_id = c.id " +
									  	"and t1.type_id = ? and ( t1.status_id = ? or t1.status_id = ? ) " +
									  	(skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and to_char(t1.time_created, 'yyyymmdd') > '20121209' " : " " ) +
									  	"and t1.user_id = ? " +
									  	"),0 ) - NVL( (select sum (t2.amount) " +
										                 "from transactions t2, credit_cards c2 " +
										                 "where c2.id = t2.credit_card_id " +
									                 		 "AND decode(ur.time_created,null,u.time_created,ur.time_created) < t2.time_created " +
											                 "and t2.credit_card_id = c.id " +
											                 "and t2.type_id = ?" +
											                 "and t2.status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + "," + TransactionsManagerBase.TRANS_STATUS_APPROVED + "," + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")" +
											                 (skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and to_char(t2.time_created, 'yyyymmdd') > '20121209' " : " " ) +
											                 "and t2.user_id = ? and t2.is_credit_withdrawal = 1 " +
					               "),0) " +
					        " ) AS CREDIT_AMOUNT_ALLOW, " +
							" ( select max(t.time_created) " +
									  " from transactions t " +
									  " where " +
									  		" t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " " +
									  		"AND decode(ur.time_created,null,u.time_created,ur.time_created) < t.time_created " +
									  		" AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
									  		" AND t.credit_card_id = c.id  " +
									   " ) AS LAST_CC_DEPOSIT " +

						" FROM " +
							" credit_cards c, " +
							" CLEARING_ROUTES cr, " +
							" users u left join users_regulation ur on u.id = ur.user_id " +
						" WHERE " +
							" c.id = ? " +
							(skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and c.TYPE_ID_BY_BIN = 2 " : " " ) +
							" and c.user_id = u.id " +
							" and cr.is_active = 1 " +
							" and (cr.start_bin is null or (substr(?,0,6) between cr.start_bin and cr.end_bin) ) " +
							" and (cr.cc_type is null or cr.cc_type = c.type_id_by_bin) " +
							" and (cr.country_id is null or cr.country_id = c.country_id) " +
							" and (cr.currency_id is null or cr.currency_id = u.currency_id) " +
							" and (cr.skin_id is null or cr.skin_id = u.skin_id) " +
							" and (cr.business_skin_id is null or cr.business_skin_id in (select s.business_case_id " +
							   														    " from skins s " +
																						" where s.id = u.skin_id))" +
						    " and NOT EXISTS ( select 1 " +
												"from cc_black_List " +
												"where cc_number = ? and " +
												"is_active = 1 " +
												") " +
							" and cr.platform_id = u.platform_id " +
						" ORDER BY " +
			                " cr.start_bin, " +
			                " cr.currency_id, " +
			                " cr.cc_type, " +
			                " cr.country_id, " +
			                " cr.skin_id, " +
			                " cr.business_skin_id ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
					ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
					ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
					ps.setLong(4, id);
					ps.setLong(5, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
					ps.setLong(6, id);
					
						ps.setLong(7, tempList.get(i).getId());
						ps.setLong(8, tempList.get(i).getCcNumber());
						ps.setLong(9, tempList.get(i).getCcNumber());
						rs = ps.executeQuery();

						if (rs.next()) {
							vo = getVO(con, rs);
							long creditClearingProviderId = rs.getLong("withdraw_clearing_provider_id");
							vo.setClearingProviderId(creditClearingProviderId);
							vo.setCreditEnabled(creditClearingProviderId > 0);

							// CFT / CREDIT
							long cftClearingProviderId = rs.getLong("cft_clearing_provider_id");
							vo.setCftClearingProviderId(cftClearingProviderId);
							vo.setCftAvailable(cftClearingProviderId > 0 && skinId != Skin.SKIN_ETRADER);


//							long nonCftAvailable = ClearingManager.getProviderNonCftAvailable(clearingProviderId);
//							if ( nonCftAvailable == ConstantsBase.EXISTS ) {
//								vo.setNonCftAvailable(true);
//							} else {
//								vo.setNonCftAvailable(false);
//							}

							if (vo.isCftAvailable()) {
								vo.setCreditEnabled(false);
							}

							// set the credit amount
							long userBalance = rs.getLong("balance");
							long creditAmountAllow = rs.getLong("CREDIT_AMOUNT_ALLOW");
							vo.setCreditAmount(Math.min(userBalance, creditAmountAllow));

//							// Inatec CFT clearing provider
//							if (vo.getClearingProviderId() == ClearingManager.INATEC_PROVIDER_ID_CFT) {
//								vo.setCftAvailable(true);
//								vo.setCreditEnabled(false);
//								list.put(vo.getId(), vo);
//								continue;
//							}

							String appSource = ApplicationDataBase.getAppSource();
							// Display cft cards or credit with amount >= minToWithdrawal
							if (appSource.equals(ConstantsBase.APPLICATION_SOURCE_WEB)) {
								if (vo.isCftAvailable() || (vo.isCreditEnabled() && vo.getCreditAmount() >= minToWithdrawal)) {
									list.put(vo.getId(), vo);
								}
							} else {  // for Backend use - allow to withdrawal any amount
								if (vo.isCftAvailable() || ( vo.isCreditEnabled() && vo.getCreditAmount() >= 0 )) {
									list.put(vo.getId(), vo);
								}
							}
						}
						closeResultSet(rs);
	                    closeStatement(ps);
					}
				} catch (Exception e) {
					logger.error("Error getting user credit cards.", e);
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
		  }

		  /**
		   * Return all availble user credit cards that not aren't bin/black listed
		   * and set there provider to Inatec CFT
		   * @param con db connection
		   * @param id user id
		   * @return Array list of credit cards
		   * @throws SQLException
		   */
		  public static HashMap<Long,CreditCard> getInatecCftCcWithrawalByUserId(Connection con,long id) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  HashMap<Long,CreditCard> list = new HashMap<Long,CreditCard>();
			  List<CreditCard> tempList = new ArrayList<CreditCard>();
			  CreditCard vo = null;

			  try {
				  	String sql = "SELECT " +
				  					"c.* " +
								 "FROM " +
								 	"credit_cards c," +
								 	"users u " +
								 "WHERE " +
								 	"c.user_id = ? " +
								 	"AND c.user_id = u.id ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);
					rs = ps.executeQuery();

					while (rs.next()) {
						vo = getVO(con, rs);
						tempList.add(vo);
					}
			  } catch (Exception e) {
					e.printStackTrace();
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

				try {
					String sql =
	  					" SELECT " +
	  						" c.*," +
	  						" case when cr.deposit_clearing_provider_id = 0 " +
	  									" and cr.withdraw_clearing_provider_id = 0 " +
	  									" and cr.cft_clearing_provider_id = 0 " +
	  							 " then 1 else 0 end is_black_list " +
	  					" FROM " +
							" credit_cards c, " +
							" users u, " +
							" CLEARING_ROUTES cr " +
						" WHERE " +
							" c.id = ? " +
							" and c.user_id = u.id " +
							" and cr.is_active = 1 " +
							" and (cr.start_bin is null or (substr(?,1,6) between cr.start_bin and cr.end_bin) ) " +
							" and (cr.cc_type is null or cr.cc_type = c.type_id_by_bin) " +
							" and (cr.country_id is null or cr.country_id = c.country_id) " +
							" and (cr.currency_id is null or cr.currency_id = u.currency_id) " +
							" and (cr.skin_id is null or cr.skin_id = u.skin_id) " +
							" and (cr.business_skin_id is null or cr.business_skin_id in (select s.business_case_id " +
							   														    " from skins s " +
																						" where s.id = u.skin_id))" +
						    " and NOT EXISTS ( select 1 " +
												"from cc_black_List " +
												"where cc_number = ? and " +
												"is_active = 1 " +
												") " +
							" and cr.platform_id = u.platform_id " +
						" ORDER BY " +
			                " cr.start_bin, " +
			                " cr.currency_id, " +
			                " cr.cc_type, " +
			                " cr.country_id, " +
			                " cr.skin_id, " +
			                " cr.business_skin_id ";

					ps = con.prepareStatement(sql);

					for (int i=0; i<tempList.size(); i++) {
						ps.setLong(1, tempList.get(i).getId());
						ps.setLong(2, tempList.get(i).getCcNumber());
						ps.setLong(3, tempList.get(i).getCcNumber());

						rs = ps.executeQuery();

						if (rs.next()) {
			            	// check if black list
			            	if (rs.getInt("is_black_list")==0){
								vo = getVO(con, rs);
								vo.setClearingProviderId(ClearingManager.INATEC_PROVIDER_ID_CFT);
								vo.setCftClearingProviderId(ClearingManager.INATEC_PROVIDER_ID_CFT);
								vo.setCftAvailable(true);
								vo.setCreditEnabled(false);
								list.put(vo.getId(), vo);
			            	}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  /**
		   * Get deposit information by date for withdrawal form that we are sending to our users
		   * @param con
		   * @param from taking all deposits before "from" date
		   * @param userId the transactions of the user id
		   * @return
		   * @throws SQLException
		   */
		  public static ArrayList<CreditCard> getDepositsPerCreditCard(Connection con, Date from, long userId) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<CreditCard> ccList = new ArrayList<CreditCard>();
			  try {
				  String sql =
					  "SELECT " +
				         "t.credit_card_id, " +
				         "cc.cc_number, " +
				         "cct.name, " +
				         "cc.holder_name, " +
				         "SUM(t.amount) amount, " +
				         "MIN(GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) from_date, " +
				         "MAX(GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) to_date " +
						"FROM " +
					        "transactions t, " +
					        "credit_cards cc, " +
					        "credit_card_types cct " +
						"WHERE " +
					        "t.user_id = ? AND " +
					        "t.type_id = ? AND " +
					        "t.time_created  <= ? AND " +
					        "t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
											TransactionsManagerBase.TRANS_STATUS_PENDING + ") AND " +
					        "t.credit_card_id = cc.id AND " +
					        "cc.type_id = cct.id " +
						"GROUP BY " +
					        "t.credit_card_id, " +
					        "cc.cc_number, " +
					        "cct.name, " +
					        "cc.holder_name " +
						"ORDER BY " +
					        "from_date ";

				  ps = con.prepareStatement(sql);
				  ps.setLong(1, userId);
				  ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
				  ps.setTimestamp(3, CommonUtil.convertToTimeStamp(from));
				  rs = ps.executeQuery();

				  while (rs.next()) {
					  CreditCard c = new CreditCard();
					  c.setId(rs.getLong("credit_card_id"));
					  c.setHolderName(rs.getString("holder_name"));
					  c.setSumDeposits(rs.getLong("amount"));
					  c.setDepositsFrom(convertToDate(rs.getTimestamp("from_date")));
					  c.setDepositsTo(convertToDate( rs.getTimestamp("to_date")));
					  CreditCardType type = new CreditCardType();
					  type.setName(rs.getString("name"));
					  c.setType(type);
					  try {
						  c.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));
					  } catch (Exception e) {
						  logger.error("Error in decryption", e);
					  }
					  ccList.add(c);
				  }
			  } finally {
				  closeResultSet(rs);
				  closeStatement(ps);
			  }
			  return ccList;
		  }


	/**
	 *  Get cc list for risk issues table
	 * @param con
	 * @param user_id
	 * @param issueId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CreditCard> getCCByUserId(Connection con, long user_id, long issueId, long reasonId) throws SQLException {

	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  ArrayList<CreditCard>  list = new ArrayList<CreditCard>();

	  try {
		  String sql =	" SELECT " +
		  					" cc.*, " +
		  					" f.file_type_id, " +
		  					" f.is_approved, " +
		  					" f.file_status_id, " +
		  					" f.issue_action_id, " +
		  					" frs.name file_status_name, " +
		  					" frs.id risk_files_status_id, " +
		  					" (CASE WHEN cc_deposits.success_deposits > 0 then 1 else 0 end) succeed_deposit " +
		  				" FROM " +
		  					" credit_cards cc " +
		  						" LEFT JOIN files_risk_status frs ON cc.files_risk_status_id = frs.id " +
		  						" LEFT JOIN files f on cc.id=f.cc_id AND f.file_type_id  in (?,?,?)" +
		  							" LEFT JOIN issue_actions ia on ia.id = f.issue_action_id  " +
		  								" LEFT JOIN (SELECT " +
		  												" cc2.id cc_id," +
		  												" (SUM(CASE WHEN t.status_id in (?,?) THEN 1 ELSE 0 END)) success_deposits " +
		  											" FROM " +
		  												" credit_cards cc2, " +
		  												" transactions t " +
		  											" WHERE " +
		  												" t.credit_card_id = cc2.id " +
			                                            " AND t.type_id = ? " +
			                                            " AND cc2.user_id = ? " +
			                                        " GROUP BY " +
			                                        	" cc2.id) cc_deposits on cc.id = cc_deposits.cc_id " +
						" WHERE " +
							" cc.user_id = ? ";
	  				if (issueId != 0){
	  				sql+=	" AND cc.id in ( SELECT " +
	  											" ird.cc_id " +
	  										"FROM " +
	  											" issue_risk_doc_req ird " +
	  											" 	LEFT JOIN issue_actions ia ON ird.issue_action_id = ia.id " +
	  											"		LEFT JOIN issues i ON i.id = ia.issue_id " +
	  											"   	LEFT JOIN files f ON f.issue_action_id = ia.id " +
	  										"WHERE " +
	  											" ird.doc_type = ? " +
	  											" AND i.id = ? ";
	  				if (reasonId != 0){
	  				sql+=						" AND ia.risk_reason_id = ? ";
	  				}
	  				sql+=						")";
	  				}
	  				sql+=
	  					" ORDER BY " +
							" cc.id ";
				  ps = con.prepareStatement(sql);
				  ps.setLong(1, FileType.POWER_OF_ATTORNEY);
				  ps.setLong(2, FileType.CC_HOLDER_ID);
				  ps.setLong(3, FileType.CC_COPY);
				  ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
				  ps.setLong(5, TransactionsManagerBase.TRANS_STATUS_PENDING);
				  ps.setLong(6, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
				  ps.setLong(7, user_id);
				  ps.setLong(8, user_id);
				  if (issueId != 0){
					  ps.setLong(9,  ConstantsBase.FILE_TYPE_CC);
					  ps.setLong(10, issueId);
					  if (reasonId != 0){
						  ps.setLong(11, reasonId);
					  }
				  }

				  rs = ps.executeQuery();
				  long cardId = 0;
				  CreditCard card = new CreditCard();
				  // The SQL return all files and there status, now we build the array list for credit cards
				  while (rs.next()) {
					  if (cardId != rs.getLong("id")) {
						  if (card.getId() != 0){ // avoid inserting for the first time
							  list.add(card);
						  }
						  cardId = rs.getLong("id");
						  card = getVO(con,rs);
					  }

					  long fileTypeId = rs.getLong("file_type_id");
					  boolean isApproved = (rs.getInt("is_approved")==1?true:false);
					  long fileStatusId = rs.getLong("file_status_id");
					  long issueActionId = rs.getLong("issue_action_id");
					  long verification_call = rs.getInt("is_verification_call");
					  int succeedDeposit = rs.getInt("succeed_deposit");
					  card.setFilesRiskStatusId(rs.getInt("risk_files_status_id"));
					  String fileStatusName = rs.getString("file_status_name");
					  if (!CommonUtil.isParameterEmptyOrNull(fileStatusName)){
						  card.setFilesRiskStatus( CommonUtil.getMessage(fileStatusName, null));
					  } else {
						  card.setFilesRiskStatus(null);
					  }

					  card.setSucceedDeposit(succeedDeposit==1?true:false);

					  if (verification_call == ConstantsBase.VERIFICATION_CALL_STATUS_REQUEST){ //requested status
						  card.getRequest().setVerificationCall(true);
					  } else {
						  card.setVerificationCall(verification_call==1?true:false);
					  }

					  if (fileTypeId == FileType.POWER_OF_ATTORNEY){
						  if (issueActionId != 0){//indicate that file was requested
							  card.getRequest().setPowerOfAttorney(true);
						  }
						  if (isApproved && fileStatusId == File.STATUS_DONE){ // if file was uploaded and approved
							  card.setPowerOfAttorney(true);
						  }
					  } else if(fileTypeId == FileType.CC_HOLDER_ID){
						  if (issueActionId != 0){//indicate that file was requested
							  card.getRequest().setSentHolderIdCopy(true);
						  }
						  if (isApproved && fileStatusId == File.STATUS_DONE){ // if file was uploaded and approved
							  card.setSentHolderIdCopy(true);
						  }
					  } else if(fileTypeId == FileType.CC_COPY){
						  if (issueActionId != 0){//indicate that file was requested
							  card.getRequest().setSentCcCopy(true);
						  }
						  if (isApproved && fileStatusId == File.STATUS_DONE){ // if file was uploaded and approved
							  card.setSentCcCopy(true);
						  }
					  }
				  }

				  if (card.getId() != 0){ //  inserting last cc
					  list.add(card);
				  }


	  } finally {
		  closeResultSet(rs);
		  closeStatement(ps);
	  }
	  return list;
  }


	/**
	 * Update verfication Request
	 * @param con
	 * @param ccId
	 * @throws SQLException
	 */
	public static void insertVerificationRequest(Connection con, long ccId) throws SQLException{
	  PreparedStatement ps = null;
	  try {

			String sql=
						" UPDATE " +
							" credit_cards " +
						" SET " +
							" is_verification_call = ? " +
						" WHERE " +
							" id = ? ";

				ps = con.prepareStatement(sql);
				ps.setInt(1, ConstantsBase.VERIFICATION_CALL_STATUS_REQUEST);
				ps.setLong(2, ccId);
				ps.executeUpdate();
		  } finally {
			  closeStatement(ps);
		  }
	  }
	
	public static long getBinId(Connection con, long bin, long ccTypeId) throws SQLException {
		PreparedStatement ps=null;
		ResultSet rs=null;

		try {
			String sql = "SELECT "+ 
									" id "+
						" FROM "+ 
									" bins "+
						" WHERE "+
									" ? BETWEEN from_bin AND to_bin "+ 
									" AND card_type_id = ?";
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, bin);
			ps.setLong(2, ccTypeId);
			rs=ps.executeQuery();

			if (rs.next()) {
				return rs.getLong(1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return -1;
	}
	
	public static	ArrayList<CreditCard>
			getCcHolderNameHistory(Connection conn, ArrayList<CreditCard> list)	throws SQLException, CryptoException, InvalidKeyException,
																				NoSuchAlgorithmException, NoSuchPaddingException,
																				IllegalBlockSizeException, BadPaddingException,
																				UnsupportedEncodingException,
																				InvalidAlgorithmParameterException {

		if (list == null || list.isEmpty()) {
			return list;
		}

		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			Iterator<CreditCard> i = list.iterator();
			while (i.hasNext()) {
				int index = 1;
				CreditCard currentCC = i.next();
				cstmt = conn.prepareCall("{call pkg_credit_card.get_cc_holder_name_hist(o_cc_holder_name_hist => ? "
																						+ ",i_user_id => ? "
																						+ ",i_cc_number => ?)}");
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setLong(index++, currentCC.getUserId());
				cstmt.setString(index++, AESUtil.encrypt(currentCC.getCcNumber()));

				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(1);
				
				ArrayList<CreditCardHolderNameHistory> l = new ArrayList<>();
				
				while (rs.next()) {
					CreditCardHolderNameHistory ccHolderNameHist = new CreditCardHolderNameHistory();
					ccHolderNameHist.setDateUpdated(convertToDate(rs.getTimestamp("time_created")));
					ccHolderNameHist.setHolderName(rs.getString("holder_name"));
					ccHolderNameHist.setOperationId(rs.getLong("operation_id"));
					l.add(ccHolderNameHist);
				}
				
				currentCC.setCcHolderNameHistory(l);
			}
			return list;

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
	
}

