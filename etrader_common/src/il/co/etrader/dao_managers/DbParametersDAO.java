package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.daos.DAOBase;

/**
 * 
 * @author eyal.ohana
 *
 */
public class DbParametersDAO extends DAOBase {
	public static Date getLastRunById(Connection con, int dbParamId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		Date timeLastRun = null;
		try {
			String sql =  " SELECT "
							+ " dbp.date_value "
						+ " FROM "
							+ " db_parameters dbp "
						+ " WHERE "
							+ " dbp.id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(index++, dbParamId);
			rs = ps.executeQuery();
			if (rs.next()) {
				timeLastRun = convertToDate(rs.getTimestamp("date_value"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return timeLastRun;
	}
	
	/**
	 * Update last run
	 * @param con
	 * @param paramId
	 * @throws SQLException
	 */
	public static void updateLastRun(Connection con, long paramId, Date date) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE "
							+ " db_parameters " +
						 " SET "
						 	+ " date_value = ? " +
						 " WHERE "
						 	+ " id = ? ";

			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, convertToTimestamp(date));
			ps.setLong(2, paramId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}
