package il.co.etrader.dao_managers;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.PopulationUsersAssignTypesManagerBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.PopulationDelay;
import il.co.etrader.bl_vos.PopulationEntryHis;
import il.co.etrader.bl_vos.PopulationEntryHisStatus;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

public class PopulationsDAOBase extends DAOBase{

	  /**
	   * Insert into population entries history table
	   * @param con db connection
	   * @param popEntry populationEntry
	   * @throws SQLException
	   */
	  public static void insertIntoPopulationEntriesHist(Connection con, PopulationEntryHis popHis) throws SQLException {

		  PreparedStatement ps = null;

		  try {
			  popHis.setTimeCreated(new Date());

			  String sql = "INSERT INTO " +
					  			"population_entries_hist(id, population_entry_id, status_id, issue_action_id, " +
					  				"writer_id, time_created, assigned_writer_id) " +
					  		"VALUES " +
					  			"(SEQ_POPULATION_ENTRIES_HIST.NEXTVAL,?,?,?,?,?,?)";

			  ps = con.prepareStatement(sql);

			  ps.setLong(1, popHis.getPopulationEntryId());
			  ps.setLong(2, popHis.getStatusId());
			  if (popHis.getIssueActionId() > 0) {
				  ps.setLong(3, popHis.getIssueActionId());
			  } else {
				  ps.setString(3, null);
			  }
			  ps.setLong(4, popHis.getWriterId());
			  ps.setTimestamp(5, CommonUtil.convertToTimeStamp(popHis.getTimeCreated()));
			  if (popHis.getAssignWriterId() > 0) {
				  ps.setLong(6, popHis.getAssignWriterId());
			  } else {
				  ps.setString(6, null);
			  }
			  ps.executeUpdate();

			  popHis.setId(getSeqCurValue(con,"SEQ_POPULATION_ENTRIES_HIST"));
		  } finally {
			  closeStatement(ps);
		  }
	  }

	  /**
	   * Insert into population entries table
	   * @param con db connection
	   * @param popEntry populationEntry
	   * @throws SQLException
	   */
	  public static void insertIntoPopulationEntries(Connection con, PopulationEntryBase p,boolean isPopEntryDisplayed) throws SQLException {

		  PreparedStatement ps = null;

		  try {
			  String sql = "INSERT INTO " +
					  			"population_entries(id, population_id, group_id, " +
					  				" qualification_time, population_users_id, is_displayed, base_qualification_time) " +
					  		"VALUES " +
					  			"( SEQ_POPULATION_ENTRIES.NEXTVAL, ?, 1, sysdate, ?,?,sysdate)";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, p.getPopulationId());
			  ps.setLong(2, p.getPopulationUserId());
			  ps.setInt(3, isPopEntryDisplayed?1:0);
			  ps.executeUpdate();
			  p.setCurrEntryId(getSeqCurValue(con,"SEQ_POPULATION_ENTRIES"));
		  } finally {
			  closeStatement(ps);
		  }
	  }

	  /**
	   * Insert into population user table
	   * @param con db connection
	   * @param popEntry populationEntryBase
	   * @throws SQLException
	   */
	  public static void insertIntoPopulationUsers(Connection con, PopulationEntryBase p) throws SQLException {

		  PreparedStatement ps = null;

		  try {
			  String sql = "INSERT INTO " +
					  			"population_users(id, user_id, contact_id, entry_type_id) " +
					  		"VALUES " +
					  			"( SEQ_POPULATION_USERS.NEXTVAL ,?,?,?)";

			  ps = con.prepareStatement(sql);
			  if(p.getUserId() > 0) {
				  ps.setLong(1, p.getUserId());
			  } else {
				  ps.setString(1, null);
			  }
			  if(p.getContactId() > 0) {
				  ps.setLong(2, p.getContactId());
			  } else {
				  ps.setString(2, null);
			  }
			  ps.setInt(3, p.getEntryTypeId());

			  ps.executeUpdate();
			  p.setPopulationUserId(getSeqCurValue(con,"SEQ_POPULATION_USERS"));
		  } finally {
			  closeStatement(ps);
		  }
	  }

		/**
		 * get populationsType list
		 * @param con db connection
		 * @return ArrayList of all populations type
		 * @throws SQLException
		 */
		public static ArrayList<PopulationType> getPopulationTypes(Connection con) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<PopulationType> list = new ArrayList<PopulationType>();

			try {

				String sql = "select id,name from population_types";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					PopulationType p = new PopulationType();
					p.setId(rs.getLong("id"));
					p.setName(rs.getString("name"));
					list.add(p);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}

		/**
		 * Get population type id by popilation entry
		 * @param con db connection
		 * @param popEntryId population entry id
		 * @return population type id
		 * @throws SQLException
		 */
		public static long getPopulationType(Connection con, long popEntryId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			long typeId = 0;

			try {

				String sql = "SELECT " +
								"p.population_type_id " +
							 "FROM " +
							 	"populations p, population_entries pe " +
							 "WHERE " +
							 	"pe.id = ? AND " +
							 	"pe.population_id = p.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, popEntryId);

				rs = ps.executeQuery();

				if (rs.next()) {
					typeId = rs.getLong("population_type_id");
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return typeId;
		}

		/**
		 * Get populationEntry by contactId
		 * @param con db connection
		 * @param contactId getting populations by contact id
		 * @return populationEntry
		 * @throws SQLException
		 */
		public static PopulationEntryBase getPopulationByContactId(Connection con, long contactId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationEntryBase p = null;

			try {
			    String sql = "SELECT " +
				    			"A.*, p.population_type_id " +
				    		"FROM " +
					    		"(SELECT " +
				                   	"id population_entry_id, " +
				                    "population_id " +
					    		 "FROM " +
					    		 	"population_entries " +
					    		 "WHERE " +
					    		 	"get_population_entry_status(id) < ? AND " +
					    		 	"contact_id = ? ) A , " +
					    		 "populations p " +
					    	"WHERE " +
					    		"A.population_id = p.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, PopulationsManagerBase.POP_ENT_HIS_STATUS_NOT_REMOVED);
				ps.setLong(2, contactId);

				rs = ps.executeQuery();

				if(rs.next()) {
					p = new PopulationEntryBase();
					p.setCurrEntryId(rs.getLong("population_entry_id"));
					p.setPopulationId(rs.getLong("population_id"));
					p.setCurrPopualtionTypeId(rs.getLong("population_type_id"));
					p.setContactId(contactId);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return p;
		}

		/**
		 * Get population num actions for remove
		 * @param con db connection
		 * @param popEntryId population entry id
		 * @return
		 * @throws SQLException
		 */
		public static int getNumActionsForRemove(Connection con, long popEntryId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			int numIssues = 0;

			try {
				String sql = "SELECT " +
								"p.num_issues " +
							 "FROM " +
							 	"populations p, population_entries pe " +
							 "WHERE " +
							 	"pe.id = ? AND " +
							 	"pe.population_id = p.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, popEntryId);

				rs = ps.executeQuery();

				if(rs.next()) {
					numIssues = rs.getInt("num_issues");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return numIssues;
		}


		/**
		 * IsPopulation entry locked
		 * @param con db connection
		 * @param popEntryId population entry for function input
		 * @param writerId writer for function input
		 * @return
		 * @throws SQLException
		 */
		public static boolean isLocked(Connection con, long populationUsersId, long writerId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
			    String sql = " SELECT " +
			    				" 1 " +
	            			 " FROM  " +
	            			 	" population_users pu, " +
	            			 	" population_entries_hist peh " +
	            			 " WHERE " +
		            			" pu.lock_history_id = peh.id " +
		            			" and pu.id = ? ";

			    if (writerId > 0){
			    	sql += " and peh.writer_id = " + writerId;
			    }

				ps = con.prepareStatement(sql);
				ps.setLong(1, populationUsersId);

				rs = ps.executeQuery();

				if(rs.next()) {
					return true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
		}

		/**
		 * IsPopulation entry locked
		 * @param con db connection
		 * @param popEntryId population entry for function input
		 * @param writerId writer for function input
		 * @return
		 * @throws SQLException
		 */
		public static boolean isWriterHasLockedEntry(Connection con, long writerId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
			    String sql = "SELECT 1 " +
	            			 "FROM   population_users pu , population_entries_hist peh " +
	            			 "WHERE  pu.lock_history_id = peh.id AND peh.writer_id = ?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, writerId);

				rs = ps.executeQuery();

				if(rs.next()) {
					return true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
		}


		/**
		 * get population object
		 *
		 * @param rs
		 *            ResultSet instance
		 * @return
		 * @throws SQLException
		 */
		protected static Population getVO(ResultSet rs) throws SQLException {

			Population vo = new Population();
			vo.setId(rs.getLong("ID"));
			vo.setName(rs.getString("NAME"));
			vo.setSkinId(rs.getLong("SKIN_ID"));
			vo.setLanguageId(rs.getLong("LANGUAGE_ID"));
			vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
			vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
			vo.setThreshold(rs.getInt("THRESHOLD"));
			vo.setNumIssues(rs.getLong("NUM_ISSUES"));
			vo.setRefreshTimePeriod(rs.getLong("REFRESH_TIME_PERIOD"));
			vo.setNumberOfMinutes(rs.getInt("NUMBER_OF_MINUTES"));
			vo.setNumberOfDays(rs.getInt("NUMBER_OF_DAYS"));
			vo.setWriterId(rs.getInt("writer_id"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setBalanceAmount(rs.getLong("BALANCE_AMOUNT"));
			vo.setLastInvestDay(rs.getInt("LAST_INVEST_DAY"));
			vo.setPopulationTypeId(rs.getInt("POPULATION_TYPE_ID"));
			vo.setPopulationTypeName(rs.getString("type_name"));
			vo.setNumberOfDeposits(rs.getInt("number_of_deposits"));
			vo.setPopulationTypePriority(rs.getInt("priority"));

			int isActive = rs.getInt("is_active");
			if (isActive == 1) {
				vo.setIsActive(true);
			} else {
				vo.setIsActive(false);
			}

			return vo;
		}

		/**
		 * Get population name by population entry id
		 * @param con
		 * @param popEntryId
		 * @return
		 * @throws SQLException
		 */
		public static String getPopulationNameByEntryId(Connection con, long popEntryId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql =
					"SELECT " +
						"p.name " +
					"FROM " +
						"populations p, population_entries pe " +
					"WHERE " +
						"pe.population_id = p.id AND " +
						"pe.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, popEntryId);
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getString("name");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
		}

		public static PopulationEntryBase getPopulationEntryByEntryId(Connection con, long populationEntryId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " SELECT " +
								" pu.id population_users_id, " +
							  	" pu.*, " +
							  	" pe.id curr_population_entry_id," +
							  	" pe.*," +
							  	" p.skin_id, " +
							  	" p.population_type_id, " +
							  	" p.dept_id, " +
							  	" p_old_pop.name p_old_name, " +
					            " p_old_pop.population_type_id pe_old_population_type_id, " +
					            " pe_old_pop.id pe_old_id, " +
					            " u.class_id, " +
								" u.user_name " +
							 " FROM " +
							 	" population_entries pe_old_pop " +
						 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
						 			" LEFT JOIN population_users pu on pe_old_pop.population_users_id = pu.id " +
						 				" LEFT JOIN users u on pu.user_id = u.id " +
									 	" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
							 				" LEFT JOIN populations p on pe.population_id = p.id " +
							 " WHERE " +
							 	" pe_old_pop.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, populationEntryId);
				rs = ps.executeQuery();
				if (rs.next()) {
					PopulationEntryBase vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					vo.setOldPopulationName(rs.getString("p_old_name"));
					vo.setOldPopEntryId(rs.getLong("pe_old_id"));
					vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
					vo.setCurrPopualtionDeptId(rs.getLong("dept_id"));
					vo.setUserName(rs.getString("user_name"));
	                vo.setUserClassId(rs.getLong("class_id"));
					return vo;


				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
		}
	
		public static PopulationEntryBase getPopulationUserByContactId(Connection con, long contactId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " SELECT " +
								" pu.id population_users_id, " +
							  	" pu.*, " +
							  	" pe.*," +
							  	" p.skin_id, " +
							  	" p.population_type_id " +
							 " FROM " +
								 " population_users pu " +
							 		" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
							 			" LEFT JOIN populations p on pe.population_id = p.id " +
							 " WHERE " +
							 	" pu.contact_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, contactId);
				rs = ps.executeQuery();
				if (rs.next()) {
					PopulationEntryBase vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					return vo;


				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
		}

		public static PopulationEntryBase getPopulationUserById(Connection con, long populationUserId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " SELECT " +
								" pu.id population_users_id, " +
							  	" pu.*, " +
							  	" pe.*," +
							  	" p.skin_id, " +
							  	" p.population_type_id " +
							 " FROM " +
							 	" population_users pu " +
							 		" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
							 			" LEFT JOIN populations p on pe.population_id = p.id " +
							 "WHERE " +
							 	"pu.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, populationUserId);
				rs = ps.executeQuery();
				if (rs.next()) {
					PopulationEntryBase vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					return vo;


				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
		}

		public static void updatePopulationUser(Connection conn, PopulationEntryBase pe) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " UPDATE " +
								" population_users pu " +
							 " SET " +
							 	" entry_type_id = ?, " +
							 	" entry_type_action_id = ?, " +
							 	" lock_history_id = ?, " +
							 	" curr_population_entry_id = ?, " +
							 	" curr_assigned_writer_id = ?, " +
							 	" contact_id = ?, " +
							 	" delay_id = ? " +
							 " WHERE " +
							 	" pu.id = ? " ;

				ps = conn.prepareStatement(sql);
				ps.setLong(1, pe.getEntryTypeId());

				if (pe.getEntryTypeActionId() != 0){
					ps.setLong(2, pe.getEntryTypeActionId());
				}else {
					ps.setString(2, null);
				}

				if (pe.getLockHistoryId() != 0){
					ps.setLong(3, pe.getLockHistoryId());
				}else {
					ps.setString(3, null);
				}

				if (pe.getCurrEntryId() != 0){
					ps.setLong(4, pe.getCurrEntryId());
				}else {
					ps.setString(4, null);
				}

				if (pe.getAssignWriterId() != 0) {
					ps.setLong(5, pe.getAssignWriterId());
				} else {
					ps.setString(5, null);
				}

				if (pe.getContactId() != 0) {
					ps.setLong(6, pe.getContactId());
				} else {
					ps.setString(6, null);
				}

				if (pe.getDelayId() != 0) {
					ps.setLong(7, pe.getDelayId());
				} else {
					ps.setString(7, null);
				}

				ps.setLong(8, pe.getPopulationUserId());

				rs = ps.executeQuery();

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

		}

		public static void updateDelayIdForPopUser(Connection conn, long popUserId, long delayId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " UPDATE " +
								" population_users pu " +
							 " SET " +
							 	" delay_id = ? " +
							 " WHERE " +
							 	" pu.id = ? " ;

				ps = conn.prepareStatement(sql);

				if (delayId != 0) {
					ps.setLong(1, delayId);
				} else {
					ps.setString(1, null);
				}
				ps.setLong(2, popUserId);


				rs = ps.executeQuery();

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

		}


		/**
		 * Update lockHistoryId
		 * @param con
		 * @param popUserId
		 * @param lockHisId
		 * @throws SQLException
		 */
		public static void updateLockHistoryId(Connection con, long popUserId, long lockHisId) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = "UPDATE " +
								"population_users " +
							 "SET " +
							 	"lock_history_id = ? " +
							 "WHERE " +
							 	"id = ? ";

				ps = con.prepareStatement(sql);
				if (lockHisId > 0) {
					ps.setLong(1, lockHisId);
				} else {
					ps.setString(1, null);
				}
				ps.setLong(2, popUserId);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		/**
		 * Update assign writer id
		 * @param con db connection
		 * @param currentEntryId population entry id to update
		 * @param writerId the assign writer
		 * @throws SQLException
		 */
		public static void updateAssignWriter(Connection con, long popUserId, long writerId, boolean isBulkAssign) throws SQLException {
			PreparedStatement ps = null;
			int index = 1;
			try {
				String sql = "UPDATE " +
								"population_users pu " +
							 "SET " +
							 	"pu.curr_assigned_writer_id = ?, " + 
							 	" pu.ASSIGN_TYPE_ID = CASE " +
						                                " WHEN ? is null THEN ? " +
						                                " WHEN pu.CURR_ASSIGNED_WRITER_ID is null THEN ? " + 
						                                " WHEN ? = 1 THEN ? " +
						                                " ELSE ? " +
						                              " END, " +
						        " pu.TIME_ASSIGN = sysdate " +
							 "WHERE " +
							 	"pu.id = ? ";

				ps = con.prepareStatement(sql);
				if (writerId > 0) {
					ps.setLong(index++, writerId);
				} else {
					ps.setString(index++, null);
				}
				if (writerId > 0) {
					ps.setLong(index++, writerId);
				} else {
					ps.setString(index++, null);
				}
				ps.setLong(index++, PopulationUsersAssignTypesManagerBase.TYPE_CANCEL_ASSIGN);
				ps.setLong(index++, PopulationUsersAssignTypesManagerBase.TYPE_ASSIGN);
				ps.setLong(index++, isBulkAssign? 1 : 0);
				ps.setLong(index++, PopulationUsersAssignTypesManagerBase.TYPE_BULK_REASSIGN);
				ps.setLong(index++, PopulationUsersAssignTypesManagerBase.TYPE_REASSIGN);
				ps.setLong(index++, popUserId);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		/**
		 * get all the population from DB
		 * @param userId TODO
		 *
		 * @return population list
		 * @throws SQLException
		 */
		public static ArrayList<Population> getPopulationList(Connection con,
				long populationType, long skinId, boolean isActive , String name, long userId)	throws SQLException {

			ArrayList<Population> list = new ArrayList<Population>();
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql =
						" SELECT " +
							" p.*, " +
							" pt.name as type_name, " +
							" pt.priority " +
						 " FROM " +
						 	" populations p, " +
						 	" population_types pt " +
						 " WHERE " +
						 	" pt.id = p.POPULATION_TYPE_ID ";

				if (skinId != 0) {
					sql += " AND SKIN_ID = ?";
				}
				if (populationType != 0) {
					sql += " AND POPULATION_TYPE_ID = ?"; 
				}
				if (isActive) {
					sql += " AND IS_ACTIVE = 1";
				}

				if (null != name) {
					sql += " AND p.name like '% '|| ? ";
				}
				if (userId != 0) {
					sql += " AND SKIN_ID = (select u.skin_id " +
										  " from users u " +
										  " where u.id = ?) ";
				}

				sql += " ORDER BY p.NAME";

				ps = con.prepareStatement(sql);
				int idx = 1;
				
				if (skinId != 0) {
					ps.setLong(idx++, skinId);
				}
				if (populationType != 0) {
					ps.setLong(idx++, populationType);
				}
				if (null != name) {
					ps.setString(idx++, name);
				}
				if (userId != 0) {
					ps.setLong(idx++, userId);
				}
				
				rs = ps.executeQuery();

				while (rs.next()) {
					Population vo = getVO(rs);
					list.add(vo);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}

		public static void updateEntryQualificationTime(Connection con,PopulationEntryBase popUserEntry) throws SQLException{
			PreparedStatement ps = null;

			try {
				String sql = " UPDATE " +
								" population_entries " +
							 " SET " +
							 	" qualification_time = ?, " +
							 	" base_qualification_time = ? " +
							 " WHERE " +
							 	" id = ? ";

				ps = con.prepareStatement(sql);

				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(popUserEntry.getQualificationTime()));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(popUserEntry.getQualificationTime()));
				ps.setLong(3, popUserEntry.getCurrEntryId());
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		/**
		 * Update assign writer id
		 * @param con db connection
		 * @param currentEntryId population entry id to update
		 * @param writerId the assign writer
		 * @throws SQLException
		 */
		public static void updateUserIdForContact(Connection con, PopulationEntryBase popUserEntry) throws SQLException {
			PreparedStatement ps = null;

			try {
				String sql = " UPDATE " +
								" population_users " +
							 " SET " +
							 	" user_id = ? " +
							 " WHERE " +
							 	" id = ? ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, popUserEntry.getUserId());
				ps.setLong(2, popUserEntry.getPopulationUserId());
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		/**
		 * Get writer's locked entry
		 * @param con db connection
		 * @param writerId writer for function input
		 * @return
		 * @throws SQLException
		 */
		public static PopulationEntryBase getWriterLockedEntryBase(Connection con, long writerId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationEntryBase vo = null;

			try {
				String sql = " SELECT " +
								" pu.id population_users_id, " +
							  	" pu.*, " +
							  	" pe.id curr_population_entry_id," +
							  	" pe.*," +
							  	" p.skin_id, " +
							  	" p.population_type_id, " +
							  	" p_old_pop.name p_old_name, " +
					            " p_old_pop.population_type_id pe_old_population_type_id, " +
					            " pe_old_pop.id pe_old_id " +
							 " FROM " +
							 	" population_users pu " +
								 	" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
						 				" LEFT JOIN populations p on pe.population_id = p.id, " +
							 	" population_entries_hist peh "+
							 		" LEFT JOIN population_entries pe_old_pop on peh.population_entry_id = pe_old_pop.id " +
							 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
							 " WHERE " +
							 	" pu.lock_history_id = peh.id " +
							 	" AND peh.writer_id = ?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, writerId);

				rs = ps.executeQuery();

				if (rs.next()) {
					vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					vo.setOldPopulationName(rs.getString("p_old_name"));
					vo.setOldPopEntryId(rs.getLong("pe_old_id"));
					vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
		}

		/**
		 * get the old population entry id for a specific population users
		 * @param populationUserId
		 * @return popEntryId
		 * @throws SQLException
		 */
		public static long getOldPopEntryId(Connection con, long populationUserId)	throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			long oldPopEntryId = 0;

			try {

				String sql = " SELECT " +
							 "		max(pe.id) as population_entry_id " +
							 " FROM " + 
							 "		population_entries pe, " + 
							 "		population_entries_hist peh " +
							 " WHERE " +
							 "		peh.population_entry_id = pe.id " +
							 "		AND pe.population_users_id = " + populationUserId;

				ps = con.prepareStatement(sql);

				rs = ps.executeQuery();

				if (rs.next()){
					oldPopEntryId = rs.getInt("population_entry_id");
				}


			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return oldPopEntryId;
		}

		public static HashMap<Integer, PopulationEntryHisStatus> getPopEntryHistoryStatusHash(Connection con) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationEntryHisStatus vo = null;
			HashMap<Integer, PopulationEntryHisStatus> statusHashMap = new HashMap<Integer, PopulationEntryHisStatus>();
			try {
				String sql = " SELECT " +
							     " * " +
							 " FROM " +
								 " population_entries_hist_status ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					vo = new PopulationEntryHisStatus();
					vo.setId(rs.getInt("ID"));
					vo.setRemoveFromPopulation(rs.getInt("IS_REMOVE")==1?true:false);
					vo.setCancelAssign(rs.getInt("IS_CANCEL_ASSIGN")==1?true:false);
					vo.setEntryTypeChange(rs.getInt("ENTRY_TYPE_CHANGE"));

					statusHashMap.put(vo.getId(), vo);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return statusHashMap;
		}

		/**
		 * Update Entry IsDisplay flag
		 * @param userId
		 * @param isDisplay
		 * @throws SQLException
		 */
		public static void updateUserEntryIsDisplay(Connection con,long entryId, boolean isDisplay) throws SQLException  {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql =" Update " +
								" population_entries " +
							" Set " +
								" is_displayed = ? " +
							" Where " +
								" id = ? "  ;

				ps = con.prepareStatement(sql);
				ps.setInt(1, isDisplay?1:0);
				ps.setLong(2, entryId);
				rs = ps.executeQuery();

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

		/**
		 * Get pop user Delay
		 * @param userId
		 * @return marketing pop entry id
		 * @throws SQLException
		 */
		public static PopulationDelay getPopulationDelayForPopUser(Connection con,long popUserId) throws SQLException  {
			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationDelay delay = null;

			try {
				String sql =" Select " +
								" pd.*," +
								" pdt.MAX_DELAY_COUNTER " +
							" From " +
								" population_users pu," +
								" population_delays pd, " +
								" population_delay_types pdt" +

							" Where " +
								" pu.delay_id = pd.id " +
								" and pd.delay_type_id = pdt.id " +
								" and pu.id = ? " ;

				ps = con.prepareStatement(sql);
				ps.setLong(1, popUserId);
				rs = ps.executeQuery();

				if (rs.next()) {
					delay = new PopulationDelay();
					delay.setId(rs.getLong("ID"));
					delay.setDelayType(rs.getInt("DELAY_TYPE_ID"));
					delay.setDelayCount(rs.getInt("DELAY_COUNT"));
					delay.setDelayActionId(rs.getLong("DELAY_ACTION_ID"));
					delay.setMaxDelayCount(rs.getInt("MAX_DELAY_COUNTER"));
					delay.setPopUserId(popUserId);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return delay;
		}

		/**
		 * updatePopulationDelay
		 * @param popualtion delay
		 * @throws SQLException
		 */
		public static void updatePopulationDelay(Connection con, PopulationDelay delay) throws SQLException  {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql =" Update " +
								" population_delays " +
							" Set " +
								" delay_type_id = ?, " +
								" delay_count = ?, " +
								" delay_action_id = ? " +
							" Where " +
								" id = ? "  ;

				ps = con.prepareStatement(sql);
				ps.setInt(1, delay.getDelayType());
				ps.setInt(2, delay.getDelayCount());
				ps.setLong(3, delay.getDelayActionId());
				ps.setLong(4, delay.getId());
				rs = ps.executeQuery();

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

		/**
		 * updatePopulationDelay
		 * @param popualtion delay
		 * @throws SQLException
		 */
		public static void insertPopulationDelay(Connection con, PopulationDelay delay) throws SQLException  {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = "insert into population_delays(ID, " +
													      " DELAY_TYPE_ID, " +
													      " DELAY_COUNT, " +
													      " DELAY_ACTION_ID) " +
				   			 "values(seq_population_delays.nextval,?,1,?) ";

				ps = con.prepareStatement(sql);
				ps.setInt(1, delay.getDelayType());
				ps.setLong(2, delay.getDelayActionId());
				rs = ps.executeQuery();

				delay.setId(getSeqCurValue(con,"seq_population_delays"));
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

		/**
		 * get Population Users Entry Type
		 * @param con db connection
		 * @return
		 * @throws SQLException
		 */
		public static HashMap<Integer,String> getPopulationEntryType(Connection con) throws SQLException{
			HashMap<Integer,String> hm = new HashMap<Integer,String>();
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql ="Select * " +
							"From " +
								"Population_Entry_Types " +
							"ORDER BY " +
								"id ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					hm.put(rs.getInt("ID"),rs.getString("NAME"));
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		return hm;
		}
		
		public static PopulationEntryHis getLastPopulationEntryHistByUserId(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationEntryHis vo = new PopulationEntryHis();
			try {
				String sql = " SELECT " +
							 "		 * " +
							 " FROM ( " +
							 "		SELECT " +
							 "			 peh.id, " +
							 "			 peh.population_entry_id, " +
							 "			 peh.status_id, " +
							 "			 pehs.name as status_name, " +
							 "			 p.name,peh.comments, " +
							 "		     peh.writer_id, " +
							 "			 peh.time_created, " +
							 "			 pe.base_qualification_time, " +
							 "			 peh.assigned_writer_id, " +
							 "			 peh.issue_action_id, " +
							 "			 pu.entry_type_id " +
							 "		FROM " +
							 "			 population_users pu, " +
							 "			 population_entries pe, " +
							 "			 population_entries_hist peh, " +
							 "			 populations p, " +
							 "			 population_entries_hist_status pehs " +
							 "	    WHERE " +
							 "			 pu.id = pe.population_users_id " +
							 "			 AND pe.id = peh.population_entry_id " +
							 "			 AND pu.user_id = ? " +  
							 "			 AND p.id = pe.population_id " +
							 "			 AND pehs.id= peh.status_id " +
							 "			 AND peh.status_id not in (3,4) " +
							 "			 ORDER BY peh.time_created desc , peh.id desc " +
							 "		) " +
							 " WHERE rownum = 1 ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();
				if (rs.next()) {
					vo.setId(rs.getLong("id"));
					vo.setPopulationEntryId(rs.getLong("population_entry_id"));
					vo.setStatusId(rs.getLong("status_id"));
					vo.setTimeCreated(rs.getDate("time_created"));			
					vo.setStatusId(rs.getLong("issue_action_id"));
					vo.setAssignWriterId(rs.getLong("assigned_writer_id"));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
		}

		public static String getRetentionRepNameByUserId(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			PopulationEntryHis vo = new PopulationEntryHis();
			try {
				String sql = " SELECT " +
							 "		w.user_name " +
							 " FROM " +
							 "		population_users pu, " +
							 "		writers w " +
							 " WHERE " +
							 "		pu.user_id = ? " + 
							 "		AND pu.curr_assigned_writer_id = w.id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getString("user_name");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return "";
		}

		public static boolean checkAssignmentsLimit(Connection con, long populationEntryId, long writerId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				if (rs.next()) {
					return true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
		}

		public static long getSumOfAssignmentsByWriterId(Connection con, long writerId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = " SELECT " +
							 " 		COUNT(u.id) as res " +
							 " FROM " +
							 "		writers w, " +
							 "		users u , " +
							 "		population_users pu " +
							 " WHERE " +
							 "		u.id = pu.user_id " +
							 "		AND w.id = pu.curr_assigned_writer_id " +
							 "		AND w.id = ? ";

				ps = con.prepareStatement(sql);
				
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getLong("res");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}
		
		/**
		 * update time when newbie join to control group
		 * @param con
		 * @param popUserId
		 * @param timeControl
		 * @throws SQLException
		 */
		public static void updateTimeControlGroup(Connection con, long popUserId) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = "UPDATE " +
								"population_users " +
							 "SET " +
							 	"time_control = sysdate " +
							 "WHERE " +
							 	"id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, popUserId);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}
		
		/**
		 * Customers to be unassigned from retention representative portfolio 
		 * No call/email issue was inserted by the assigned rep at all (regardless of reached/unreached) for the last x days
		 * No activity (activity = investment/deposit/withdrawal) has been made by the customer in the last 3 months
		 * user not locked to a representative
		 * @param con
		 * @param writerId
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<PopulationEntryBase> getUsersToUnassignFromPortfolio(Connection con, int noActivityMonths, int noIssueDays) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<PopulationEntryBase> list = new ArrayList<PopulationEntryBase>();
			try {
				String sql = "SELECT  /*+ parallel (u,8)  parallel (pu,8) parallel (pe,8)  parallel (p,8)  parallel (pt,8)  */  " +
								" pu.id population_users_id, " +
					  			" pu.*, " +
					  			" pe.id curr_population_entry_id, " +
								" pe.*, " +
								" p.population_type_id, " +
								" p.skin_id, " +
								" u.class_id, " +
								" u.user_name " +
							 "FROM " +
								"users u, " +
								"population_users pu, " +
								"population_entries pe, " +
								"populations p, " +
								"population_types pt " +
							 "WHERE " +
								"u.id = pu.user_id AND " +
								"pe.id = pu.curr_population_entry_id AND " +
								"pe.population_id = p.id AND " +
								"p.population_type_id = pt.id AND " +
								"((pt.type_id in (" + PopulationsManagerBase.POP_TYPE_TYPE_RETENTION + ", " + PopulationsManagerBase.POP_TYPE_TYPE_IMMEDIATE_TREATMENT + ") AND " +
								  "pt.id <> " + PopulationsManagerBase.POP_TYPE_CALLME + ") " +
								  "or " +
								"(pt.id = " + PopulationsManagerBase.POP_TYPE_CALLME + " AND " +
								      "u.first_deposit_id is not null " +
								")) AND " +
								"pu.curr_assigned_writer_id is not null AND " +
								"pu.lock_history_id is null AND " + // not lock user
								"((NOT EXISTS ( " + // pass 3 months since user was assigned
                                    "SELECT /*+ parallel (peh,8)  parallel pe1,8)  */  " + 
                                        "1 " +
                                      "FROM " + 
                                        "population_entries_hist peh, " +
                                        "population_entries pe1 " +
                                      "WHERE " +
                                        "pu.id = pe1.POPULATION_USERS_ID AND " +
                                        "pe1.id = peh.population_entry_id AND " +
                                        "peh.status_id = 2 AND " +
                                        "peh.time_created > ADD_MONTHS(SYSDATE, " + noActivityMonths + ") " +      
                                ") AND " +
								"NOT EXISTS ( " + // no new investment in last 3 month
							                  "SELECT /*+ parallel (i,8)   */ " +
							                  	"1 " +
							                  "FROM " +
							                  	"investments i " +
							                  "WHERE " +
							                  	"i.time_created > ADD_MONTHS(SYSDATE, " + noActivityMonths + ") AND " +
							                  	"u.id = i.user_id " +
								") AND " +
								"NOT EXISTS ( " + //real desposit AND withdraw  last 3 month
							                    "SELECT /*+ parallel (t,8)  parallel tt,8)  */ " +
							                       "1 " +
							                    "FROM " +
							                        "transactions t, " +
							                        "transaction_types tt " +
						                        "WHERE " +
							                        "t.type_id = tt.id AND " +
							                        "tt.class_type in (" + TransactionsManagerBase.TRANS_CLASS_DEPOSIT + ", " +  TransactionsManagerBase.TRANS_CLASS_WITHDRAW + ") AND " + //real desposit and withdraw 
							                        "t.time_created > ADD_MONTHS(sysdate, " + noActivityMonths + ") AND " +
							                        "u.id = t.user_id " +
								")) OR " +
							    "(NOT EXISTS( " + // pass 3 months since user was assigned
                                    "SELECT /*+ parallel (peh,8)  parallel pe1,8)  */ " + 
                                        "1 " +
                                      "FROM " + 
                                        "population_entries_hist peh, " +
                                        "population_entries pe1 " +
                                      "WHERE " +
                                        "pu.id = pe1.population_users_id AND " +
                                        "pe1.id = peh.population_entry_id AND " +
                                        "peh.status_id = " + PopulationEntriesManagerBase.POP_ENT_HIS_STATUS_ASSIGNED + " AND " + 
                                        "peh.time_created > SYSDATE - " + noIssueDays + " " +
                                ") AND " +
								"NOT EXISTS( " +
									  "SELECT /*+ parallel (i,8)  parallel (ia,8) parallel (iat,8) */  " +
									  	"1 " +
									  "FROM " +
										  "issues i, " +
										  "issue_actions ia, " +
										  "issue_action_types iat " +
									  "WHERE " +
										  "i.id = ia.issue_id AND " +
										  "ia.issue_action_type_id = iat.id AND " +
										  "iat.channel_id IN (" + IssuesManagerBase.ISSUE_CHANNEL_CALL + ", " + IssuesManagerBase.ISSUE_CHANNEL_EMAIL + ") AND " +
										  "ia.action_time > SYSDATE - " + noIssueDays + " AND " +
										  "u.id = i.user_id AND " +
										  "ia.writer_id = pu.curr_assigned_writer_id " +
								"))) ";

				ps = con.prepareStatement(sql);
				
				rs = ps.executeQuery();
				while (rs.next()) {
					PopulationEntryBase vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					vo.setUserName(rs.getString("user_name"));
	                vo.setUserClassId(rs.getLong("class_id"));
					list.add(vo);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
		
	public static ArrayList<PopulationEntryBase> getPopulationEntryTmp(Connection con, long min, long max) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<PopulationEntryBase> list = new ArrayList<PopulationEntryBase>();
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_population.get_population_entry_tmp(o_population_entry_tmp => ? " +
																				  ",i_min => ?" + 
																				  ",i_max => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, min);
			cstmt.setLong(index++, max);

			cstmt.execute();

			rs = (ResultSet) cstmt.getObject(1);

			while (rs.next()) {
				PopulationEntryBase vo = new PopulationEntryBase();
				com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
				vo.setUserName(rs.getString("user_name"));
				vo.setUserClassId(rs.getLong("class_id"));
				list.add(vo);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return list;
	}
		
		/**
		* Get users with open withdraw's population which supposed to modify to retention's population
		* @param connection
		* @return ArrayList<Long>
		* @throws SQLException
		*/
		public static ArrayList<Long> getSpecialOpenWithdrawUsers(Connection connection) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<Long> list = new ArrayList<Long>();
			int index = 1;
			try {
				String sql = " SELECT "
								+ " pu.user_id "
							+ " FROM "
								+ " population_users pu, "
								+ " population_entries pe, "
								+ " populations p, "
								+ " users u "
							+ " WHERE "
								+ " pu.curr_population_entry_id = pe.id "
								+ " and pe.population_id = p.id "
								+ " and pu.user_id = u.id "
								+ " and p.population_type_id = ? "
								+ " and u.class_id <> ? "							
								+ " and NOT EXISTS (SELECT "
														+ " t.id "
													+ " FROM "
														+ " transactions t, "
														+ " transaction_types tt "
													+ " where "
														+ " t.type_id = tt.id "
														+ " and t.user_id = u.id "
														+ " and tt.class_type = ? "
														+ " and t.status_id = ?) ";
				ps = connection.prepareStatement(sql);
				ps.setLong(index++, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW);
				ps.setLong(index++, ConstantsBase.USER_CLASS_TEST);
				ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
				ps.setLong(index++, TransactionsManagerBase.TRANS_STATUS_REQUESTED);
				rs = ps.executeQuery();
				while (rs.next()) {
					list.add(rs.getLong("user_id"));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
}
