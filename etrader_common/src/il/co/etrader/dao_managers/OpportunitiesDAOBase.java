package il.co.etrader.dao_managers;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.OpportunitySettle;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.util.OpportunityCacheBean;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.OpportunityShifting;
import il.co.etrader.bl_vos.OpportunityTemplate;
import il.co.etrader.bl_vos.OptionExpiration;
import com.anyoption.common.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

/**
 * Opportunities DAO.
 *
 * WARN: This class depends on the server session settings for the days numbers (first day of the week)
 * It use sunday as first day of the week (SUN = 1)
 *
 * @author Tony
 */
public class OpportunitiesDAOBase extends com.anyoption.common.daos.OpportunitiesDAOBase {
    protected static Logger log = Logger.getLogger(OpportunitiesDAOBase.class);

    /**
     * Get details for an opportunity that is currently running.
     *
     * @param conn the db conn to use
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningById(Connection conn, long id) throws SQLException {
        Opportunity o = new Opportunity();
        o.setId(id);
        return getRunningById(conn, o, false);
    }

    /**
     * Get details for an opportunity that is currently running.
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db conn to use
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningById(Connection conn, Opportunity o, boolean canBeSettled) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id, " +
                    "A.market_id, " +
                    "C.market_group_id, " +
                    "C.display_name, " +
                    "C.name, " +
                    "C.feed_name, " +
                    "C.decimal_point, " +
                    "C.investment_limits_group_id, " +
                    //"C.home_page_priority, " +
                    //"C.home_page_hour_first, " +
                    //"C.group_priority, " +
                    "C.updates_pause, " +
                    "C.significant_percentage, " +
                    "C.real_updates_pause, " +
                    "C.real_significant_percentage, " +
                    "C.random_ceiling, " +
                    "C.random_floor, " +
                    "C.first_shift_parameter, " +
                    "C.average_investments, " +
                    "C.percentage_of_avg_investments, " +
                    "C.fixed_amount_for_shifting, " +
                    "C.type_of_shifting, " +
                    "C.disable_after_period, " +
                    "C.is_suspended, " +
                    "C.suspended_message, " +
                    "C.sec_between_investments, " +
                    "C.sec_between_investments_ip, " +
                    "C.sec_for_dev2, " +
                    "C.amount_for_dev2, " +
                    "C.amount_for_dev2_night, " +
                    "C.amount_for_dev3, " +
                    "C.acceptable_level_deviation, " +
                    "C.acceptable_level_deviation_3, " +
                    "C.no_auto_settle_after, " +
                    "C.curent_level_alert_perc, " +
                    "C.option_plus_fee, " +
                    "C.option_plus_market_id, " +
                    "C.decimal_point_subtract_digits, " +
                  //  "C.frequence_priority, " +
                  	"C.exchange_id, " +
                  	"C.quote_params as market_quote_params, " +
                 	"C.type_id, " +
                    "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "A.current_level, " +
                    "A.auto_shift_parameter, " +
                    "get_opp_last_auto_shift(A.id) AS last_shift, " +
                    "A.scheduled, " +
                    "A.deduct_win_odds, " +
                    "A.is_disabled_service, " +
                    "A.is_disabled, " +
                    "A.is_disabled_trader, " +
                    "A.opportunity_type_id, " +
                    "A.odds_group, " +
                    "B.over_odds_win, " +
                    "B.over_odds_lose, " +
                    "B.under_odds_win, " +
                    "B.under_odds_lose, " +
                    "B.odds_win_deduct_step, " +
                    "D.max_exposure, " +
                    "D.is_run_with_market, " +
                    "D.is_use_margin, " +
                    "D.margin_percentage_up, " +
                    "D.margin_percentage_down, " +
                    "0 AS closing_level, " +
//                    "E.closing_level, " +
                    "A.quote_params, " +
                    "A.is_published, " +
                    "A.max_inv_coeff, " +
                    "A.is_suspended AS is_opp_suspended, " +
                    "pkg_oppt_manager.get_opportunity_state(C.exchange_id, A.market_id, A.time_first_invest, A.time_last_invest, A.time_act_closing, A.scheduled) AS state " +
//                    "CASE " +
//                    "WHEN current_timestamp < A.time_first_invest THEN " + Opportunity.STATE_CREATED + " " +
//                    "WHEN A.time_first_invest <= current_timestamp AND current_timestamp < A.time_last_invest THEN " +
//                        "CASE " +
//                        "WHEN A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " THEN " +
//                            "CASE " +
//                            "WHEN EXISTS( " + // if we fall into a day template for that market
//                                "SELECT " +
//                                    "id " +
//                                "FROM " +
//                                    "opportunity_templates " +
//                                "WHERE " +
//                                    "market_id = A.market_id AND " +
//                                    "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                    "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                    ") AND " +
//                                "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                "NOT ( " + // and not exchange weekend
//                                    "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//	                                "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//	                                "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                ") THEN " + Opportunity.STATE_OPENED + " " +
//                            "WHEN EXISTS( " + // if we fall between time last invest of a day template and time close
//                                "SELECT " +
//                                    "id " +
//                                "FROM " +
//                                    "opportunity_templates " +
//                                "WHERE " +
//                                    "market_id = A.market_id AND " +
//                                    "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                    "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_est_closing || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                    ") AND " +
//                                "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                "NOT ( " + // and not exchange weekend
//                                    "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//                                    "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//                                    "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                ") THEN " + Opportunity.STATE_WAITING_TO_PAUSE + " " +
//                            "ELSE " + Opportunity.STATE_PAUSED + " " +
//                            "END " +
//                        "ELSE " +
//                            "CASE " +
//                            "WHEN A.time_first_invest <= current_timestamp AND (current_timestamp < A.time_last_invest - to_dsinterval('0 00:10:00') OR D.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + ") THEN " + Opportunity.STATE_OPENED + " " +
//                            "ELSE " + Opportunity.STATE_LAST_10_MIN + " " +
//                            "END " +
//                        "END " +
//                    "WHEN A.time_last_invest < current_timestamp AND current_timestamp < time_last_invest + to_dsinterval('0 00:01:00') THEN " + Opportunity.STATE_CLOSING_1_MIN + " " +
//                    "WHEN A.time_last_invest + to_dsinterval('0 00:01:00') < current_timestamp AND current_timestamp < time_est_closing THEN " + Opportunity.STATE_CLOSING + " " +
//                    "ELSE " + Opportunity.STATE_CLOSED + " " +
//                    "END AS state " +
                "FROM " +
                    "opportunities A, " +
                    "opportunity_odds_types B, " +
                    "opportunity_types D, " +
                    "markets C LEFT JOIN " +
//                    "(SELECT " +
//                        "M.id, " +
//                        "O.closing_level " +
//                    "FROM " +
//                        "markets M, " +
//                        "opportunities O " +
//                    "WHERE " +
//                        "M.id = O.market_id AND " +
//                        "NOT O.closing_level IS NULL AND " +
//                        "NOT O.time_act_closing IS NULL AND " +
//                        "O.time_act_closing = " +
//                            "(SELECT " +
//                                "MAX(x.time_act_closing) " +
//                            "FROM " +
//                                "opportunities X " +
//                            "WHERE " +
//                                "(X.opportunity_type_id in (" + Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + ", " + Opportunity.TYPE_DYNAMICS + ")) AND " +
//                                "X.market_id = O.market_id AND " +
//                                "NOT X.closing_level IS NULL AND " +
//                                "NOT X.time_act_closing IS NULL AND " +
//                                "NOT X.scheduled = " + Opportunity.SCHEDULED_HOURLY + ")) E ON C.id = E.id LEFT JOIN " +
                    "exchange_holidays F ON C.exchange_id = F.exchange_id AND trunc(F.holiday) = trunc(current_date) " +
                "WHERE " +
                    "A.odds_type_id = B.id AND " +
                    "A.market_id = C.id AND " +
                    "A.opportunity_type_id = D.id AND " +
                    (!canBeSettled ? "A.is_settled = 0 AND " : "") +
                    "(A.opportunity_type_id in (" + Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + ", " + Opportunity.TYPE_DYNAMICS + ")) AND " +
                    "A.id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, o.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                o = fillVO(o, rs, true, true, true, false);
                o.setPublished(rs.getBoolean("is_published"));
                o.setState(rs.getInt("state"));
                o.getMarket().setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
                o.getMarket().setExchangeId(rs.getLong("exchange_id"));
                o.setQuoteParams(rs.getString("quote_params"));
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                if (log.isTraceEnabled()) {
                    log.trace("Loaded: " + o.toString());
                }
           } else {
                log.error("Couldn't load the opp.");
            }
//        } catch (SQLException sqle) {
//            log.log(Level.ERROR, "", sqle);
//            throw sqle;
//        } catch (Throwable t) {
//            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return o;
    }

    /**
     * Create the daily opportunities for specified day by copying the daily
     * template opportunities for which that day is a working day (it's not a holiday or weekend).
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db connection to use
     * @param daysAhead for how much days ahead to create the opportunities.
     *      example:
     *      1 is to create the opps for tomorrow
     *      2 is for the day after tomorrow
     *      3 is for the day in 3 days time
     * @throws SQLException
     */
    public static void createDailyOpportunities(Connection conn, int daysAhead) throws SQLException {
    	CallableStatement cStmt = null;
    	try {
	    	cStmt = conn.prepareCall("{call pkg_oppt_manager.create_daily_opportunities(?)}");
	    	cStmt.setInt(1, daysAhead);
	    	cStmt.execute();
    	} finally {
    		closeStatement(cStmt);
    	}
    }

    /**
     * Create weekly opportunities for the next week.
     *
     * @param conn the db connection to use
     * @throws SQLException
     */
    public static void createWeeklyOpportunities(Connection conn) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN pkg_oppt_manager.create_weekly_opportunities(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }

    /**
     * Create monthly opportunities for the next month.
     *
     * @param conn the db connection to use
     * @throws SQLException
     */
    public static void createMonthlyOpportunities(Connection conn) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN pkg_oppt_manager.create_monthly_opportunities(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }

    public static void createQuarterlyOpportunities(Connection con) throws SQLException {
    	CallableStatement cstmt = null;
    	try {
    		cstmt = con.prepareCall("BEGIN pkg_oppt_manager.create_quarterly_opportunities(); END;");
    		cstmt.execute();
    	} finally {
    		closeStatement(cstmt);
    	}
    }

    /**
     * List as HTML table the opportunities created in the last hour.
     *
     * @param conn
     * @return
     * @throws SQLException
     */
    public static String reportCreatedOpportunities(Connection conn) throws SQLException {
        String report = "<html><body><table border=\"1\"><tr><td rowspan=\"2\">Market</td><td colspan=\"3\">Time First Invest</td><td colspan=\"3\">Time Last Invest</td><td colspan=\"3\">Time Est Closing</td></tr><tr><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td></tr>";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "to_char(A.time_first_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_first_invest, " +
                        "to_char(A.time_est_closing, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_est_closing, " +
                        "to_char(A.time_last_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_last_invest, " +
                        "B.name " +
                    "FROM " +
                        "opportunities A, " +
                        "markets B " +
                    "WHERE " +
                        "A.market_id = B.id AND " +
                        "A.time_created > current_timestamp - to_dsinterval('0 01:00:00') AND " +
                        "A.scheduled < 3 " +
                    "ORDER BY " +
                        "B.name, " +
                        "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            String[] firstInvestArray;
            String[] lastInvestArray;
            String[] estCloseArray;
            while (rs.next()) {
                firstInvestArray = rs.getString("time_first_invest").split(" ");
                lastInvestArray = rs.getString("time_last_invest").split(" ");
                estCloseArray = rs.getString("time_est_closing").split(" ");
                report +=
                    "<tr><td>" +
                    rs.getString("name") +
                    "</td><td>" +
                        firstInvestArray[0].toString() +
                    "</td><td>" +
                        firstInvestArray[1].toString() +
                    "</td><td>" +
                        firstInvestArray[2].toString() +
                    "</td><td>" +
                        lastInvestArray[0].toString() +
                    "</td><td>" +
                        lastInvestArray[1].toString() +
                    "</td><td>" +
                        lastInvestArray[2].toString() +
                    "</td><td>" +
                        estCloseArray[0].toString() +
                    "</td><td>" +
                        estCloseArray[1].toString() +
                    "</td><td>" +
                        estCloseArray[2].toString() +
                    "</td></tr>";
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        report += "</table></body></html>";
        return report;
    }

    /**
     * List as HTML table the opportunities created in the last hour.
     *
     * @param conn
     * @return
     * @throws SQLException
     */
    public static String reportCreatedWeeklyOpportunities(Connection conn) throws SQLException {
        String report = "<html><body><table border=\"1\"><tr><td rowspan=\"2\">Market</td><td colspan=\"3\">Time First Invest</td><td colspan=\"3\">Time Last Invest</td><td colspan=\"3\">Time Est Closing</td></tr><tr><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td></tr>";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "to_char(A.time_first_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_first_invest, " +
                        "to_char(A.time_est_closing, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_est_closing, " +
                        "to_char(A.time_last_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_last_invest, " +
                        "B.name " +
                    "FROM " +
                        "opportunities A, " +
                        "markets B " +
                    "WHERE " +
                        "A.market_id = B.id AND " +
                        "A.time_created > current_timestamp - to_dsinterval('0 01:00:00') AND " +
                        "A.scheduled = 3 " +
                    "ORDER BY " +
                        "B.name, " +
                        "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            String[] firstInvestArray;
            String[] lastInvestArray;
            String[] estCloseArray;
            while (rs.next()) {
                firstInvestArray = rs.getString("time_first_invest").split(" ");
                lastInvestArray = rs.getString("time_last_invest").split(" ");
                estCloseArray = rs.getString("time_est_closing").split(" ");
                report +=
                    "<tr><td>" +
                    rs.getString("name") +
                    "</td><td>" +
                        firstInvestArray[0].toString() +
                    "</td><td>" +
                        firstInvestArray[1].toString() +
                    "</td><td>" +
                        firstInvestArray[2].toString() +
                    "</td><td>" +
                        lastInvestArray[0].toString() +
                    "</td><td>" +
                        lastInvestArray[1].toString() +
                    "</td><td>" +
                        lastInvestArray[2].toString() +
                    "</td><td>" +
                        estCloseArray[0].toString() +
                    "</td><td>" +
                        estCloseArray[1].toString() +
                    "</td><td>" +
                        estCloseArray[2].toString() +
                    "</td></tr>";
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        report += "</table></body></html>";
        return report;
    }

    /**
     * List as HTML table the opportunities created in the last hour.
     *
     * @param conn
     * @return
     * @throws SQLException
     */
    public static String reportCreatedScheduledOpportunities(Connection conn, int scheduled) throws SQLException {
        String report = "<html><body><table border=\"1\"><tr><td rowspan=\"2\">Market</td><td colspan=\"3\">Time First Invest</td><td colspan=\"3\">Time Last Invest</td><td colspan=\"3\">Time Est Closing</td></tr><tr><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td><td>Date</td><td>Time</td><td>GMT</td></tr>";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "to_char(A.time_first_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_first_invest, " +
                        "to_char(A.time_est_closing, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_est_closing, " +
                        "to_char(A.time_last_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_last_invest, " +
                        "B.name " +
                    "FROM " +
                        "opportunities A, " +
                        "markets B " +
                    "WHERE " +
                        "A.market_id = B.id AND " +
                        "A.time_created > current_timestamp - to_dsinterval('0 01:00:00') AND " +
                        "A.scheduled = ? " +
                    "ORDER BY " +
                        "B.name, " +
                        "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, scheduled);
            rs = pstmt.executeQuery();
            String[] firstInvestArray;
            String[] lastInvestArray;
            String[] estCloseArray;
            while (rs.next()) {
                firstInvestArray = rs.getString("time_first_invest").split(" ");
                lastInvestArray = rs.getString("time_last_invest").split(" ");
                estCloseArray = rs.getString("time_est_closing").split(" ");
                report +=
                    "<tr><td>" +
                    rs.getString("name") +
                    "</td><td>" +
                        firstInvestArray[0].toString() +
                    "</td><td>" +
                        firstInvestArray[1].toString() +
                    "</td><td>" +
                        firstInvestArray[2].toString() +
                    "</td><td>" +
                        lastInvestArray[0].toString() +
                    "</td><td>" +
                        lastInvestArray[1].toString() +
                    "</td><td>" +
                        lastInvestArray[2].toString() +
                    "</td><td>" +
                        estCloseArray[0].toString() +
                    "</td><td>" +
                        estCloseArray[1].toString() +
                    "</td><td>" +
                        estCloseArray[2].toString() +
                    "</td></tr>";
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        report += "</table></body></html>";
        return report;
    }

    /**
     * Gets list of ids of the opportunities that have moved to specified state in the specified time.
     *
     * @param conn the db connection to use
     * @param state the state of interest
     * @param time the time of interest
     * @return
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesMovedToState(Connection conn, int state, Date time) throws SQLException {
//    	TODO: need to load only opp type 1,3,4,5 if state 3 dont load 4,5
    	String oppsType = Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + ", " + Opportunity.TYPE_DYNAMICS;
    	if (state == Opportunity.STATE_LAST_10_MIN) {
    		oppsType = Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BUBBLES;
    	}
        ArrayList<Long> l = new ArrayList<Long>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "opportunities " +
                "WHERE " +
                	"(opportunity_type_id in (" + oppsType + ")) AND " +
                    (state == Opportunity.STATE_CREATED ? "time_created >= ? - 90 / 86400 AND time_created < ? - 30 / 86400" : "") +
                    (state == Opportunity.STATE_OPENED ? "time_first_invest >= ? - 30 / 86400 AND time_first_invest < ? + 30 / 86400" : "") +
                    (state == Opportunity.STATE_LAST_10_MIN ? "(time_last_invest >= ? + 10 / 1440 - 30 / 86400 AND time_last_invest < ? + 10 / 1440 + 30 / 86400) OR " : "") +
                    (state == Opportunity.STATE_LAST_10_MIN ? "(deduct_win_odds = 1 AND time_last_invest >= ? + 8 / 1440 - 30 / 86400 AND time_last_invest < ? + 8 / 1440 + 30 / 86400) OR " : "") +
                    (state == Opportunity.STATE_LAST_10_MIN ? "(deduct_win_odds = 1 AND time_last_invest >= ? + 6 / 1440 - 30 / 86400 AND time_last_invest < ? + 6 / 1440 + 30 / 86400) OR " : "") +
                    (state == Opportunity.STATE_LAST_10_MIN ? "(deduct_win_odds = 1 AND time_last_invest >= ? + 4 / 1440 - 30 / 86400 AND time_last_invest < ? + 4 / 1440 + 30 / 86400) OR " : "") +
                    (state == Opportunity.STATE_LAST_10_MIN ? "(deduct_win_odds = 1 AND time_last_invest >= ? + 2 / 1440 - 30 / 86400 AND time_last_invest < ? + 2 / 1440 + 30 / 86400)" : "") +
                    (state == Opportunity.STATE_CLOSING_1_MIN ? "time_last_invest >= ? - 30 / 86400 AND time_last_invest < ? + 30 / 86400" : "") +
                    (state == Opportunity.STATE_CLOSING ? "time_last_invest >= ? - 90 / 86400 AND time_last_invest < ? - 30 / 86400" : "") +
                    (state == Opportunity.STATE_CLOSED ? "time_est_closing >= ? - 30 / 86400 AND time_est_closing < ? + 30 / 86400" : "") +
                    (state == Opportunity.STATE_DONE ? "time_act_closing >= ? - 90 / 86400 AND time_act_closing < ? - 30 / 86400" : "");
            pstmt = conn.prepareStatement(sql);
            Timestamp ts = new Timestamp(time.getTime());
            pstmt.setTimestamp(1, ts);
            pstmt.setTimestamp(2, ts);
            if (state == Opportunity.STATE_LAST_10_MIN) {
                pstmt.setTimestamp(3, ts);
                pstmt.setTimestamp(4, ts);
                pstmt.setTimestamp(5, ts);
                pstmt.setTimestamp(6, ts);
                pstmt.setTimestamp(7, ts);
                pstmt.setTimestamp(8, ts);
                pstmt.setTimestamp(9, ts);
                pstmt.setTimestamp(10, ts);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                l.add(rs.getLong("id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * Update the is_published flag to true for the opportunities that opened at the
     * specified time.
     *
     * @param conn the db connection to use
     * @param time the time at which the opps opened
     * @throws SQLException
     */
//    public static void publishOpenedOpportunities(Connection conn, Date time) throws SQLException {
//        PreparedStatement pstmt = null;
//        try {
//            String sql =
//                "UPDATE opportunities SET " +
//                    "is_published = true " +
//                "WHERE " +
//                    "time_first_invest > ? - 30 / 86400 AND time_first_invest < ? + 30 / 86400";
//            pstmt = conn.prepareStatement(sql);
//            Timestamp ts = new Timestamp(time.getTime());
//            pstmt.setTimestamp(1, ts);
//            pstmt.setTimestamp(2, ts);
//            pstmt.executeUpdate();
//        } finally {
//            closeStatement(pstmt);
//        }
//    }

    /**
     * Load the week/month opportunities that need to be paused for the night/weekend/holiday(s) at that minute
     * (the market for them closed at that minute).
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db connection to use
     * @param time the time (the minute to check for)
     * @return <code>ArrayList<Long></code> with the ids of the opportunities to pause.
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesToPause(Connection conn, Date time) throws SQLException {
    	ArrayList<Long> l = new ArrayList<Long>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = conn.prepareCall(
					"{call pkg_oppt_manager.get_opps_topause( "
					+ "o_data => ?, "
					+ "i_as_of_time => ?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setTimestamp(2, new Timestamp(time.getTime()));
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				l.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
    	return l;
//        ArrayList<Long> l = new ArrayList<Long>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            // WARN: This query depends on the server session settings for the days numbers (first day of the week)
//            // It use sunday as first day of the week (SUN = 1)
//
//            // Collect all opened (between time first invest and time close) week/month opportunities
//            // for which there is a matching day template that close this minute.
//            // Also it should not be a full day holiday or exchange weekend.
//            String sql =
//                "SELECT " +
//                    "A.id " +
//                "FROM " +
//                    "opportunities A, " +
//                    "markets B LEFT JOIN " +
//                    "exchange_holidays C ON B.exchange_id = C.exchange_id AND trunc(C.holiday) = trunc(current_date), " +
//                    "opportunity_templates D " +
//                "WHERE " +
//                    "A.market_id = B.id AND " +
//                    "A.market_id = D.market_id AND " +
//                    "A.time_first_invest <= current_timestamp AND current_timestamp <= A.time_last_invest AND " +
//                    "D.scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                    "D.is_active = 1 AND " +
//                    "D.is_full_day = CASE " +
//                        "WHEN C.id IS NULL AND B.exchange_id = 1 AND to_char(current_date, 'D') = 1 THEN 2 " + // TA exchange sunday
//                        "WHEN (NOT C.id IS NULL AND C.is_half_day = 1) OR (A.market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 " +
//                        "ELSE 1 END AND " +
//                    "(((A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || D.time_last_invest || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || D.time_last_invest || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 AND " +
//                    "NOT trunc(A.time_est_closing) = trunc(current_date)) OR " + // don't pause if it will close in few min
//                    "(B.exchange_id = " + Opportunity.TURKISH_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.TURKISH_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.TURKISH_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.TOKYO_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.TOKYO_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.TOKYO_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.CHINA_EXCHANGE+ " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.CHINA_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.CHINA_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.HONG_KONG_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.HONG_KONG_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.HONG_KONG_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.JAKARTA_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.JAKARTA_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.JAKARTA_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.SINGAPORE_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.SINGAPORE_BREAK_START +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.SINGAPORE_BREAK_START +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.DUBAI_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.DUBAI_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '" + Opportunity.DUBAI_BREAK_START + "' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 )) AND " +
//                    "((C.id IS NULL OR C.is_half_day = 1) AND " + // not full day holiday
//                    "NOT ( " + // and not exchange weekend
//	                    "((B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//	                    "(NOT (B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 18 OR B.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//	                    "(B.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) " +
//                    "))";
//            pstmt = conn.prepareStatement(sql);
//            Timestamp ts = new Timestamp(time.getTime());
//            pstmt.setTimestamp(1, ts);
//            pstmt.setTimestamp(2, ts);
//            pstmt.setTimestamp(3, ts);
//            pstmt.setTimestamp(4, ts);
//            pstmt.setTimestamp(5, ts);
//            pstmt.setTimestamp(6, ts);
//            pstmt.setTimestamp(7, ts);
//            pstmt.setTimestamp(8, ts);
//            pstmt.setTimestamp(9, ts);
//            pstmt.setTimestamp(10, ts);
//            pstmt.setTimestamp(11, ts);
//            pstmt.setTimestamp(12, ts);
//            pstmt.setTimestamp(13, ts);
//            pstmt.setTimestamp(14, ts);
//            pstmt.setTimestamp(15, ts);
//            pstmt.setTimestamp(16, ts);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//                l.add(rs.getLong("id"));
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return l;
    }

    /**
     * Load the week/month opportunities that need to be unpaused at that minute
     * (the market for them opened at that minute after night/weekend/holiday(s)).
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db connection to use
     * @param time the time (the minute to check for)
     * @return <code>ArrayList<Long></code> with the ids of the opportunities to unpause.
     * @throws SQLException
     */
    public static ArrayList<Long> getOpportunitiesToUnpause(Connection conn, Date time) throws SQLException {
    	ArrayList<Long> l = new ArrayList<Long>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = conn.prepareCall(
					"{call pkg_oppt_manager.get_opps_toresume( "
					+ "o_data => ?, "
					+ "i_as_of_time => ?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setTimestamp(2, new Timestamp(time.getTime()));
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				l.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
    	return l;
//        ArrayList<Long> l = new ArrayList<Long>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            // WARN: This query depends on the server session settings for the days numbers (first day of the week)
//            // It use sunday as first day of the week (SUN = 1)
//
//            // Collect all week/month opportunities
//            // for which there is a matching day template that open this minute.
//            // Also it should not be a full day holiday or exchange weekend.
//            String sql =
//                "SELECT " +
//                    "A.id " +
//                "FROM " +
//                    "opportunities A, " +
//                    "markets B LEFT JOIN " +
//                    "exchange_holidays C ON B.exchange_id = C.exchange_id AND trunc(C.holiday) = trunc(current_date), " +
//                    "opportunity_templates D " +
//                "WHERE " +
//                    "A.market_id = B.id AND " +
//                    "A.market_id = D.market_id AND " +
//                    "A.time_first_invest < current_timestamp AND current_timestamp < A.time_est_closing AND " +
//                    "D.scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                    "D.is_active = 1 AND " +
//                    "((D.is_full_day = 1 AND (B.exchange_id != 30 OR to_char(sysdate, 'D') != 6)) OR (D.is_half_day = 1 AND B.exchange_id = 30 AND to_char(sysdate, 'D') = 6)) AND " +
//                    "(((A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || D.time_first_invest || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || D.time_first_invest || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400) OR " +
//                    "(B.exchange_id = " + Opportunity.TURKISH_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.TURKISH_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.TURKISH_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.TOKYO_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.TOKYO_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.TOKYO_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.HONG_KONG_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.HONG_KONG_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.HONG_KONG_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.CHINA_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.CHINA_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.CHINA_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.JAKARTA_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.JAKARTA_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.JAKARTA_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.SINGAPORE_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.SINGAPORE_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.SINGAPORE_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 ) OR " +
//                    "(B.exchange_id = " + Opportunity.DUBAI_EXCHANGE + " AND " +
//                    "(A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " OR A.scheduled = " + Opportunity.SCHEDULED_DAYLY + ") AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.DUBAI_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') >= ? - 30 / 86400 AND " +
//                    "to_timestamp_tz(to_char(CAST(FROM_TZ(CAST(sysdate AS TIMESTAMP), 'GMT') AT TIME ZONE D.time_zone AS DATE), 'yyyy-mm-dd') || '"+ Opportunity.DUBAI_BREAK_END +"' || D.time_zone, 'yyyy-mm-dd hh24:mi TZR') < ? + 30 / 86400 )) AND " +
//                    "((C.id IS NULL OR C.is_half_day = 1) AND " + // not full day holiday
//                    "NOT ( " + // and not exchange weekend
//                        "((B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 18 OR B.exchange_id = 38) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//                        "(NOT (B.exchange_id = 1 OR B.exchange_id = 17 OR B.exchange_id = 18 OR B.exchange_id = 16 OR B.exchange_id = 38) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//                        "(B.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) " +
//                    "))";
//            pstmt = conn.prepareStatement(sql);
//            Timestamp ts = new Timestamp(time.getTime());
//            pstmt.setTimestamp(1, ts);
//            pstmt.setTimestamp(2, ts);
//            pstmt.setTimestamp(3, ts);
//            pstmt.setTimestamp(4, ts);
//            pstmt.setTimestamp(5, ts);
//            pstmt.setTimestamp(6, ts);
//            pstmt.setTimestamp(7, ts);
//            pstmt.setTimestamp(8, ts);
//            pstmt.setTimestamp(9, ts);
//            pstmt.setTimestamp(10, ts);
//            pstmt.setTimestamp(11, ts);
//            pstmt.setTimestamp(12, ts);
//            pstmt.setTimestamp(13, ts);
//            pstmt.setTimestamp(14, ts);
//            pstmt.setTimestamp(15, ts);
//            pstmt.setTimestamp(16, ts);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//                l.add(rs.getLong("id"));
//            }
//        } catch (SQLException e) {
//        	log.debug("cant unpause", e);
//        }
//        finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return l;
    }

    /**
     * Change the published state of an opp.
     *
     * @param conn the db connection to use
     * @param oppId the id of the opp which published state should be changed
     * @param published the new published state of the opp
     * @throws SQLException
     */
    public static void publishOpportunity(Connection conn, long oppId, boolean published) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE opportunities SET " +
                    "is_published = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setBoolean(1, published);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Reset the opportunities.is_published flag to 0 for all opps that have 1 and not one touch.
     *
     * @param conn
     * @throws SQLException
     */
    public static void resetIsPublished(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE opportunities SET " +
                    "is_published = 0 " +
                "WHERE " +
                    "is_published = 1 AND " +
                    "(opportunity_type_id in (" + Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + "))";
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Load all the opportunities that the Level Service need on startup.
     * Only the data needed by the service is loaded.
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getLevelServiceOpportunities(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();
        try {
            String sql =
                    "SELECT " +
                        "A.id, " +
                        "A.market_id, " +
                        "C.market_group_id, " +
                        "C.display_name, " +
                        "C.name, " +
                        "C.feed_name, " +
                        "C.decimal_point, " +
                        "C.investment_limits_group_id, " +
                   //     "C.home_page_priority, " +
                   //     "C.home_page_hour_first, " +
                   //     "C.group_priority, " +
                        "C.updates_pause, " +
                        "C.significant_percentage, " +
                        "C.real_updates_pause, " +
                        "C.real_significant_percentage, " +
                        "C.random_ceiling, " +
                        "C.random_floor, " +
                        "C.first_shift_parameter, " +
                        "C.average_investments, " +
                        "C.percentage_of_avg_investments, " +
                        "C.fixed_amount_for_shifting, " +
                        "C.type_of_shifting, " +
                        "C.disable_after_period, " +
                        "C.is_suspended, " +
                        "C.suspended_message, " +
                        "C.sec_between_investments, " +
                        "C.sec_between_investments_ip, " +
                        "C.sec_for_dev2, " +
                        "C.amount_for_dev2, " +
                        "C.amount_for_dev2_night, " +
                        "C.amount_for_dev3, " +
                        "C.acceptable_level_deviation, " +
                        "C.acceptable_level_deviation_3, " +
                        "C.no_auto_settle_after, " +
                        "C.curent_level_alert_perc, " +
                        "C.option_plus_fee, " +
                        "C.option_plus_market_id, " +
                        "C.decimal_point_subtract_digits, " +
                     //   "C.frequence_priority, " +
                     	"C.exchange_id, " +
                     	"C.quote_params as market_quote_params, " +
                     	"C.type_id, " +
                        "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                        "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                        "A.current_level, " +
                        "A.auto_shift_parameter, " +
                        "get_opp_last_auto_shift(A.id) AS last_shift, " +
                        "A.scheduled, " +
                        "A.deduct_win_odds, " +
                        "A.is_disabled_service, " +
                        "A.is_disabled, " +
                        "A.is_disabled_trader, " +
                        "A.opportunity_type_id, " +
                        "A.odds_group, " +
                        "B.over_odds_win, " +
                        "B.over_odds_lose, " +
                        "B.under_odds_win, " +
                        "B.under_odds_lose, " +
                        "B.odds_win_deduct_step, " +
                        "D.max_exposure, " +
                        "D.is_run_with_market, " +
                        "D.is_use_margin, " +
                        "D.margin_percentage_up, " +
                        "D.margin_percentage_down, " +
                        "0 AS closing_level, " +
//                        "E.closing_level, " +
                        "A.quote_params, " +
                        "A.is_published, " +
                        "A.max_inv_coeff, " +
                        "A.is_suspended AS is_opp_suspended, " +
                        "pkg_oppt_manager.get_opportunity_state(C.exchange_id, A.market_id, A.time_first_invest, A.time_last_invest, A.time_act_closing, A.scheduled) AS state " +
//                        "CASE " +
//                        "WHEN current_timestamp < A.time_first_invest THEN " + Opportunity.STATE_CREATED + " " +
//                        "WHEN A.time_first_invest <= current_timestamp AND current_timestamp < A.time_last_invest THEN " +
//                            "CASE " +
//                            "WHEN A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY + " THEN " +
//                                "CASE " +
//                                "WHEN EXISTS( " + // if we fall into a day template for that market
//                                    "SELECT " +
//                                        "id " +
//                                    "FROM " +
//                                        "opportunity_templates " +
//                                    "WHERE " +
//                                        "market_id = A.market_id AND " +
//                                        "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                        "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                        "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                        "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                        ") AND " +
//                                    "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                    "NOT ( " + // and not exchange weekend
//    	                                "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//    	                                "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//    	                                "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                    ") THEN " + Opportunity.STATE_OPENED + " " +
//                                "WHEN EXISTS( " + // if we fall between time last invest of a day template and time close
//                                    "SELECT " +
//                                        "id " +
//                                    "FROM " +
//                                        "opportunity_templates " +
//                                    "WHERE " +
//                                        "market_id = A.market_id AND " +
//                                        "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                        "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                        "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                        "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_est_closing || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                        ") AND " +
//                                    "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                    "NOT ( " + // and not exchange weekend
//                                        "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//                                        "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//                                        "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                    ") THEN " + Opportunity.STATE_WAITING_TO_PAUSE + " " +
//                                "ELSE " + Opportunity.STATE_PAUSED + " " +
//                                "END " +
//                            "ELSE " +
//                                "CASE " +
//                                "WHEN A.time_first_invest <= current_timestamp AND (current_timestamp < A.time_last_invest - to_dsinterval('0 00:10:00') OR D.product_type_id = " + Opportunity.TYPE_PRODUCT_BINARY_0100 + ") THEN " + Opportunity.STATE_OPENED + " " +
//                                "ELSE " + Opportunity.STATE_LAST_10_MIN + " " +
//                                "END " +
//                            "END " +
//                        "WHEN A.time_last_invest < current_timestamp AND current_timestamp < time_last_invest + to_dsinterval('0 00:01:00') THEN " + Opportunity.STATE_CLOSING_1_MIN + " " +
//                        "WHEN A.time_last_invest + to_dsinterval('0 00:01:00') < current_timestamp AND current_timestamp < time_est_closing THEN " + Opportunity.STATE_CLOSING + " " +
//                        "ELSE " + Opportunity.STATE_CLOSED + " " +
//                        "END AS state " +
                    "FROM " +
                        "opportunities A, " +
                        "opportunity_odds_types B, " +
                        "opportunity_types D, " +
                        "markets C LEFT JOIN " +
//                        "(SELECT " +
//                            "DISTINCT M.id, " +
//                            "O.closing_level " +
//                        "FROM " +
//                            "markets M, " +
//                            "opportunities O " +
//                        "WHERE " +
//                            "M.id = O.market_id AND " +
//                            "NOT O.closing_level IS NULL AND " +
//                            "NOT O.time_act_closing IS NULL AND " +
//                            "O.time_act_closing = " +
//                                "(SELECT " +
//                                    "MAX(x.time_act_closing) " +
//                                "FROM " +
//                                    "opportunities X " +
//                                "WHERE " +
//                                    "(X.opportunity_type_id in (" + Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + "," + Opportunity.TYPE_DYNAMICS + ")) AND " +
//                                    "X.market_id = O.market_id AND " +
//                                    "NOT X.closing_level IS NULL AND " +
//                                    "NOT X.time_act_closing IS NULL AND " +
//                                    "NOT X.scheduled = " + Opportunity.SCHEDULED_HOURLY + ")) E ON C.id = E.id LEFT JOIN " +
                        "exchange_holidays F ON C.exchange_id = F.exchange_id AND trunc(F.holiday) = trunc(current_date) " +
                    "WHERE " +
                        "A.odds_type_id = B.id AND " +
                        "A.market_id = C.id AND " +
                        "A.opportunity_type_id = D.id AND " +
                        "A.time_est_closing > current_date AND " +
                        "A.opportunity_type_id in (" + Opportunity.TYPE_REGULAR + ", " + Opportunity.TYPE_OPTION_PLUS + ", " + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ", " + Opportunity.TYPE_BUBBLES + "," + Opportunity.TYPE_DYNAMICS + ") " +
                    "ORDER BY " +
                        "A.market_id, A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Opportunity o = fillVO(null, rs, true, true, true, false);
                o.setState(rs.getInt("state"));
                o.getMarket().setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
                o.getMarket().setExchangeId(rs.getLong("exchange_id"));
                o.setQuoteParams(rs.getString("quote_params"));
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                if (log.isTraceEnabled()) {
                    log.trace("Loaded: " + o.toString());
                }
               list.add(o);

            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    /**
     * Load the data from the current position of the result set
     * in Opportunity VO.
     *
     * @param rs the result set
     * @return <code>Opportunity</code>
     * @throws SQLException
     */
    protected static Opportunity fillVO(
            Opportunity o,
            ResultSet rs,
            boolean fillMarket,
            boolean fillOddsType,
            boolean fillType,
            boolean fillExchange) throws SQLException {
        Opportunity vo = null;
        if (null == o) {
            vo = new Opportunity();
        } else {
            vo = o;
        }
        vo.setId(rs.getLong("id"));
        vo.setCurrentLevel(rs.getDouble("current_level"));
        vo.setSuspended(rs.getInt("is_opp_suspended") == 1);
        try {
            String tfi = rs.getString("time_first_invest");
            String tec = rs.getString("time_est_closing");
            String tli = rs.getString("time_last_invest");
            if (log.isTraceEnabled()) {
            	log.trace("time_first_invest: " + tfi);
            	log.trace("time_est_closing: " + tec);
            	log.trace("time_last_invest: " + tli);
            }

            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            vo.setTimeFirstInvest(localDateFormat.parse(tfi));
            vo.setTimeEstClosing(localDateFormat.parse(tec));
            vo.setTimeLastInvest(localDateFormat.parse(tli));
//            vo.setTimeFirstInvest(tzDateFormat.parse(rs.getString("time_first_invest")));
//            vo.setTimeEstClosing(tzDateFormat.parse(rs.getString("time_est_closing")));
//            vo.setTimeLastInvest(tzDateFormat.parse(rs.getString("time_last_invest")));
//        } catch (ParseException pe) {
//            log.error("Can't parse datetime.", pe);
//        }
        } catch (Throwable t) {
            log.error("Can't parse datetime.", t);
        }
        vo.setClosingLevel(rs.getDouble("closing_level"));
        vo.setAutoShiftParameter(rs.getDouble("auto_shift_parameter"));
        vo.setLastShift(rs.getBigDecimal("last_shift"));
        if (null != vo.getLastShift()) {
            int cmp = vo.getLastShift().compareTo(new BigDecimal(0));
            if (cmp != 0) {
                vo.setShifting(true);
                vo.setShiftUp(cmp > 0);
            }
            vo.setLastShift(vo.getLastShift().abs());
        }
        vo.setScheduled(rs.getInt("scheduled"));
        vo.setDeductWinOdds(rs.getBoolean("deduct_win_odds"));
        vo.setDisabledService(rs.getBoolean("is_disabled_service"));
        vo.setDisabled(rs.getBoolean("is_disabled"));
        vo.setDisabledTrader(rs.getBoolean("is_disabled_trader"));
        vo.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
        vo.setOddsGroup(rs.getString("odds_group"));
        vo.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));

        if (fillMarket) {
            Market market = new Market();
            market.setId(rs.getLong("market_id"));
            market.setGroupId(new Long(rs.getLong("market_group_id")));
            market.setDisplayName(rs.getString("display_name"));
            market.setName(rs.getString("name"));
            market.setFeedName(rs.getString("feed_name"));
            market.setDecimalPoint(new Long(rs.getLong("decimal_point")));
            market.setInvestmentLimitsGroupId(new Long(rs.getLong("investment_limits_group_id")));
//            market.setHomePagePriority(rs.getInt("home_page_priority"));
//            market.setHomePageHourFirst(rs.getBoolean("home_page_hour_first"));
//            market.setGroupPriority(rs.getInt("group_priority"));
            market.setUpdatesPause(rs.getInt("updates_pause"));
            market.setSignificantPercentage(rs.getBigDecimal("significant_percentage"));
            market.setRealUpdatesPause(rs.getInt("real_updates_pause"));
            market.setRealSignificantPercentage(rs.getBigDecimal("real_significant_percentage"));
            market.setRandomCeiling(rs.getBigDecimal("random_ceiling"));
            market.setRandomFloor(rs.getBigDecimal("random_floor"));
            market.setFirstShiftParameter(rs.getBigDecimal("first_shift_parameter"));
            market.setAverageInvestments(rs.getBigDecimal("average_investments"));
            market.setPercentageOfAverageInvestments(rs.getBigDecimal("percentage_of_avg_investments"));
            market.setFixedAmountForShifting(rs.getBigDecimal("fixed_amount_for_shifting"));
            market.setTypeOfShifting(rs.getLong("type_of_shifting"));
            market.setDisableAfterPeriod(rs.getLong("disable_after_period"));
            market.setSuspended(rs.getBoolean("is_suspended"));
            market.setSuspendedMessage(rs.getString("suspended_message"));
          //  market.setFrequencePriority(rs.getInt("frequence_priority"));
            market.setSecBetweenInvestments(rs.getInt("sec_between_investments"));
            market.setSecForDev2(rs.getInt("sec_for_dev2"));
            market.setAmountForDev2(rs.getFloat("amount_for_dev2"));
            market.setAcceptableDeviation(rs.getBigDecimal("acceptable_level_deviation"));
            market.setAcceptableDeviation3(rs.getBigDecimal("acceptable_level_deviation_3"));
            market.setNoAutoSettleAfter(rs.getDouble("no_auto_settle_after"));
            market.setCurrentLevelAlertPerc(rs.getBigDecimal("curent_level_alert_perc"));
            market.setOptionPlusFee(rs.getLong("option_plus_fee"));
            market.setSecBetweenInvestmentsSameIp(rs.getInt("sec_between_investments_ip"));
            market.setAmountForDev3(rs.getFloat("amount_for_dev3"));
            market.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
            market.setAmountForDev2Night(rs.getFloat("amount_for_dev2_night"));
            market.setQuoteParams(rs.getString("market_quote_params"));
            market.setTypeId(rs.getLong("type_id"));
            vo.setMarketId(market.getId());
            vo.setMarket(market);
        }

        if (fillOddsType) {
            OpportunityOddsType oddsType = new OpportunityOddsType();
            oddsType.setOverOddsWin(rs.getFloat("over_odds_win"));
            oddsType.setOverOddsLose(rs.getFloat("over_odds_lose"));
            oddsType.setUnderOddsWin(rs.getFloat("under_odds_win"));
            oddsType.setUnderOddsLose(rs.getFloat("under_odds_lose"));
            oddsType.setOddsWinDeductStep(rs.getFloat("odds_win_deduct_step"));
            vo.setOddsType(oddsType);
        }

        if (fillType) {
            OpportunityType type = new OpportunityType();
            type.setMaxExposure(rs.getLong("max_exposure"));
            type.setIsRunWithMarket(rs.getInt("is_run_with_market"));
            type.setIsUseMargin(rs.getInt("is_use_margin"));
            type.setMarginPercentageUp(rs.getFloat("margin_percentage_up"));
            type.setMarginPercentageDown(rs.getFloat("margin_percentage_down"));
            vo.setType(type);
        }

        if (fillExchange) {
            Exchange exchange = new Exchange();
            exchange.setId(rs.getLong("exchange_id"));
            exchange.setName(rs.getString("exchange_name"));
            vo.setExchangeId(exchange.getId());
            vo.setExchange(exchange);
        }
        return vo;
    }

    protected static Opportunity getVO(ResultSet rs) throws SQLException{
    	Opportunity vo = new Opportunity();
    	vo.setId(rs.getLong("ID"));
    	vo.setMarketId(rs.getLong("MARKET_ID"));
    	try {
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
    		if (rs.getString("ftime_first_invest")!=null)
    			vo.setTimeFirstInvest(localDateFormat.parse(rs.getString("ftime_first_invest")));
    		if (rs.getString("ftime_est_closing")!=null)
    			vo.setTimeEstClosing(localDateFormat.parse(rs.getString("ftime_est_closing")));
    		if (rs.getString("ftime_last_invest")!=null)
    			vo.setTimeLastInvest(localDateFormat.parse(rs.getString("ftime_last_invest")));
    		if (rs.getString("ftime_act_closing")!=null)
    			vo.setTimeActClosing(localDateFormat.parse(rs.getString("ftime_act_closing")));
    		if (rs.getString("ftime_created")!=null)
    			vo.setTimeCreated(localDateFormat.parse(rs.getString("ftime_created")));
    		if (rs.getString("fTIME_SETTLED")!=null)
    			vo.setTimeSettled(localDateFormat.parse(rs.getString("fTIME_SETTLED")));

        } catch (ParseException pe) {
            log.error("Can't parse datetime.", pe);
        }

    	vo.setCurrentLevel(rs.getDouble("CURRENT_LEVEL"));
    	vo.setClosingLevel(rs.getDouble("CLOSING_LEVEL"));
      	vo.setIsPublished(rs.getInt("IS_PUBLISHED"));
    	vo.setIsSettled(rs.getInt("IS_SETTLED"));
    	vo.setOddsTypeId(rs.getLong("ODDS_TYPE_ID"));
    	vo.setWriterId(rs.getLong("WRITER_ID"));
    	vo.setOpportunityTypeId(rs.getLong("OPPORTUNITY_TYPE_ID"));
    	vo.setDisabled(rs.getInt("is_disabled") == 1);
    	vo.setDisabledService(rs.getInt("IS_DISABLED_SERVICE") == 1);
    	vo.setDisabledTrader(rs.getInt("IS_DISABLED_TRADER") == 1);
    	vo.setScheduled(rs.getInt("scheduled"));
    	vo.setCloseLevelTxt(rs.getString("CLOSING_LEVEL_CALCULATION"));
    	vo.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));
    	vo.setSuspended(rs.getInt("is_opp_suspended") == 1);
		return vo;
    }

    protected static String getFormatTimesSQL() {
         return
    	 " to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
    	 " to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
    	 " to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
    	 " to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
    	 " to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
    	 " to_char(o.TIME_SETTLED, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS fTIME_SETTLED ";

    }


    protected static OpportunityShifting getOppShiftingVO(ResultSet rs) throws SQLException{
    	OpportunityShifting vo = new OpportunityShifting();
    	try {
	    	vo.setId(rs.getLong("ID"));
	    	vo.setOpportunityId(rs.getLong("opportunity_Id"));
	    	vo.setShifting(rs.getFloat("shifting"));
	    	vo.setUp(rs.getInt("IS_up")==1);
	    	vo.setShiftingTime(convertToDate(rs.getTimestamp("shifting_Time")));
	    	vo.setWriter_id(rs.getLong("writer_id"));
	    	vo.setMarket_id(rs.getLong("market_id"));
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
	    	vo.setTime_est_closing(localDateFormat.parse(rs.getString("time_est_closing")));
	    	vo.setScheduled(rs.getLong("scheduled"));
	    	vo.setSkinGroup(SkinGroup.getById(rs.getLong("SKIN_GROUP_ID")));
	    } catch (ParseException e) {
			log.debug(e.toString());
		}
    	return vo;
    }

    protected static OpportunityTemplate getOppTemplatesVO(ResultSet rs) throws SQLException{
    	OpportunityTemplate vo = new OpportunityTemplate();

    	vo.setId(rs.getLong("ID"));
    	vo.setMarketId(rs.getLong("market_Id"));
    	vo.setWriterId(rs.getLong("writer_Id"));
    	vo.setTimeFirstInvest(rs.getString("time_First_Invest"));
    	vo.setTimeEstClosing(rs.getString("time_Est_Closing"));
    	vo.setTimeLastInvest(rs.getString("time_Last_Invest"));
    	vo.setOddsTypeId(rs.getLong("odds_Type_Id"));
    	vo.setOpportunityTypeId(rs.getLong("opportunity_Type_Id"));
    	vo.setActive(rs.getInt("is_active")==1);
    	vo.setScheduled(rs.getLong("scheduled"));
        vo.setTimeZone(rs.getString("time_zone"));
        vo.setDeductWinOdds(rs.getInt("DEDUCT_WIN_ODDS")==1);
        vo.setUpDown(String.valueOf(rs.getInt("up_down")));
        vo.setOneTouchDecimalPoint(rs.getLong("ONE_TOUCH_DECIMAL_POINT"));
        vo.setOpenGraph(rs.getInt("is_open_graph")==1);
        vo.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));
        vo.setTimeEstClosingHalfday(rs.getString("time_est_closing_halfday"));
        vo.setTimeLastInvestHalfday(rs.getString("time_last_invest_halfday"));
    	byte[] halfDayWeek = new byte[7];
    	byte[] fullDayWeek = new byte[7];
    	halfDayWeek[0] = rs.getByte("sun_h");
    	halfDayWeek[1] = rs.getByte("mon_h");
    	halfDayWeek[2] = rs.getByte("tue_h");
    	halfDayWeek[3] = rs.getByte("wed_h");
    	halfDayWeek[4] = rs.getByte("thu_h");
    	halfDayWeek[5] = rs.getByte("fri_h");
    	halfDayWeek[6] = rs.getByte("sat_h");
    	vo.setHalfDayWeek(halfDayWeek);
    	
    	fullDayWeek[0] = rs.getByte("sun_f");
    	fullDayWeek[1] = rs.getByte("mon_f");
    	fullDayWeek[2] = rs.getByte("tue_f");
    	fullDayWeek[3] = rs.getByte("wed_f");
    	fullDayWeek[4] = rs.getByte("thu_f");
    	fullDayWeek[5] = rs.getByte("fri_f");
    	fullDayWeek[6] = rs.getByte("sat_f");
    	vo.setFullDayWeek(fullDayWeek);
		return vo;
    }

    protected static OptionExpiration getOptionExpirationVO(ResultSet rs) throws SQLException{
    	OptionExpiration vo = new OptionExpiration();

    	vo.setId(rs.getLong("ID"));
    	vo.setMarketId(rs.getLong("market_Id"));
    	vo.setWriterId(rs.getLong("writer_Id"));
    	vo.setActive(rs.getInt("is_active")==1);
    	vo.setExpiryDate(convertToDate(rs.getTimestamp("expiry_date")));
    	vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        vo.setExpiryDateEnd(convertToDate(rs.getTimestamp("expiry_date_end")));
        vo.setLetter(rs.getString("ric_letter"));
        vo.setYear(rs.getString("ric_year"));

		return vo;
    }

    /**
     * Gets the closed opportunities for the list.
     *
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getClosedOpportunities(Connection conn, Date from, Date to, long marketId, long marketGroupId, String oppTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();

        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        try {
            String sql =
                    "SELECT " +
                    	"o.*, " + 
                    	"o.is_suspended AS is_opp_suspended, " +
                    	getFormatTimesSQL() + ", " +
                    	"(CASE WHEN EXISTS (SELECT 1 FROM reuters_quotes rq WHERE rq.opportunity_id = o.id) THEN 1 ELSE 0 END) AS haveReutersQuotes " +
                    "FROM " +
                    	"opportunities o " +
               		"WHERE " +
               			"o.time_act_closing is not null AND " +
               			"o.opportunity_type_id in (" + oppTypeId + ") AND " +
               			"o.market_id IN (SELECT smgm.market_id " +
									 	"FROM skin_market_group_markets smgm, skin_market_groups smg2, markets m " +
									 	"WHERE " +
									 		"smgm.skin_market_group_id = smg2.id AND " +
									 		"smg2.skin_id=" + a.getSkinId() + " AND " +
									 		"smgm.market_id = m.id ";

             if (marketGroupId!=0) {   // market selected
             	sql += "and smg2.market_group_id =" +marketGroupId+ " ";
             }

             sql+= ") ";

         	if ( marketId != 0) {
         		sql += "and o.market_id = " + marketId + " ";
         	}

//         	sql += "and o.time_est_closing between (?) and (?) ";
            sql += "and o.time_est_closing >= ? and o.time_est_closing < ? ";

           	sql += "ORDER BY " +
           				"o.time_est_closing desc ";

            pstmt = conn.prepareStatement(sql);

            // set the times to zero
//            Calendar fromC = Calendar.getInstance();
//            Calendar toC = Calendar.getInstance();
//            fromC.setTime(from);
//            toC.setTime(to);
//            fromC.set(Calendar.HOUR_OF_DAY, 0);
//            fromC.set(Calendar.MINUTE, 0);
//            fromC.set(Calendar.SECOND, 0);
//            toC.set(Calendar.HOUR_OF_DAY, 23);
//            toC.set(Calendar.MINUTE, 59);
//            toC.set(Calendar.SECOND, 59);
//
//            log.debug("Loading from : " + fromC.getTime() + " to: " + toC.getTime());
            log.debug("Loading from: " + from + " to: " + to);
//            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(fromC.getTime()));
//            pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(toC.getTime()));
            pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(from));
            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(to));

            rs = pstmt.executeQuery();
            Opportunity o = null;
            while (rs.next()) {
            	o = getVO(rs);
            	o.setHaveReutersQuote(rs.getInt("haveReutersQuotes") == 1 ? true : false);
            	o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                list.add(o);
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    public static ArrayList<Opportunity> getClosedOpportunitiesAnyoption(Connection conn,Date date,Date from2,long marketId,long marketGroupId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();

        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        try {
            String sql =
                    " SELECT o.*, " +
                    	"o.is_suspended AS is_opp_suspended, " +
                    	getFormatTimesSQL() +
                    " FROM opportunities o,markets m" +
                    " WHERE o.market_id=m.id and o.time_act_closing is not null and o.opportunity_type_id <> ? and ";

            if (marketId!=0 || marketGroupId!=0) {

                	sql += "o.market_id in (select smgm.market_id "+
						   "from skin_market_group_markets smgm, skin_market_groups smg "+
					        "where smgm.skin_market_group_id = smg.id "+
					        "and smg.skin_id="+a.getSkinId()+" "+
					        "and smgm.market_id = m.id " +
					         "and smg.market_group_id =" +marketGroupId+ ") AND ";

            	if ( marketId != 0) {
            		sql += " o.market_id = "+marketId+" AND ";
            	}
            }
           	sql+=" trunc(o.time_est_closing) between trunc(?) and trunc(?) ";

           	sql+=" order by o.time_est_closing desc ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, Opportunity.TYPE_ONE_TOUCH);
            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(from2));
            pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(date));
            /*pstmt.setTimestamp(4, CommonUtil.convertToTimeStamp(from2));
            pstmt.setTimestamp(5, CommonUtil.convertToTimeStamp(date));
            pstmt.setTimestamp(6, CommonUtil.convertToTimeStamp(from2));*/
            //pstmt.setTimestamp(7, CommonUtil.convertToTimeStamp(date));

            rs = pstmt.executeQuery();
            Opportunity o = null;
            while (rs.next()) {
            	o = getVO(rs);
            	o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                list.add(o);
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }



    /**
     * Close opportunity and set the closing level.
     *
     * @param conn the db connection to use
     * @param oppId the id of the opp to close
     * @param closingLevel the closing level
     * @throws SQLException
     */
    public static void closeOpportunity(Connection conn, long oppId, double closingLevel , String closeLevelTxt) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "opportunities " +
                    "SET " +
                        "time_act_closing = current_date, " +
                        "closing_level = ?, " +
                        "is_published = 0, " +
                        "CLOSING_LEVEL_CALCULATION = ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, closingLevel);
            pstmt.setString(2, closeLevelTxt);
            pstmt.setLong(3, oppId);
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Log a opportunity level shifting.
     *
     * @param conn the db connection to use
     * @param oppId the opp id
     * @param shifting the current shifting
     * @param up is the shifting up or down
     * @param writerId the id of the writer inserting the shifting
     * @param skinGroup the skin group to which the shifting is done
     * @throws SQLException
     */
    public static void insertShifting(Connection conn,
    									long oppId,
    									BigDecimal shifting,
    									boolean up,
    									long writerId,
    									SkinGroup skinGroup) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "INSERT INTO opportunity_shiftings " +
                    "(id, opportunity_id, shifting, is_up, shifting_time, writer_id, SKIN_GROUP_ID) " +
                "VALUES " +
                    "(SEQ_OPPORTUNITY_SHIFTINGS.NEXTVAL, ?, ?, ?, current_date, ?, ?)";
                pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, oppId);
            pstmt.setBigDecimal(2, shifting);
            pstmt.setInt(3, up ? 1 : 0);
            pstmt.setLong(4, writerId);
            pstmt.setLong(5, skinGroup.getId());
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update opportunity shift parameter ET.
     *
     * @param conn the db connection to use
     * @param oppId the opportunit id
     * @param shifting the value to set to shift_parameter field
     * @throws SQLException
     */
    public static void updateOpportunityShiftParameter(Connection conn, long oppId, SkinGroup sg, Double shifting, long writerId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                		" opportunity_skin_group_map " +
                " SET " +
                		" shift_parameter = ?, " +
                		" writer_id = ? " +
                " WHERE " +
                		" opportunity_id = ?" +
            	" AND " +
                		" skin_group_id = ?";
            pstmt = conn.prepareStatement(sql);

            pstmt.setDouble(1, shifting);
            pstmt.setLong(2, writerId);
            pstmt.setLong(3, oppId);
            pstmt.setLong(4, sg.getId());

            int k = pstmt.executeUpdate();
            log.debug(k+" opportunity_skin_group_map updated");
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update opportunity shift parameter for AO.
     *
     * @param conn the db connection to use
     * @param oppId the opportunit id
     * @param shifting the value to set to shift_parameter_ao field
     * @throws SQLException
     */
    public static void updateOpportunityShiftParameterAO(Connection conn, long oppId, double shifting) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE opportunities SET " +
                    "shift_parameter_ao = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, shifting);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update opportunity shift parameter for AO and ET.
     *
     * @param conn the db connection to use
     * @param oppId the opportunit id
     * @param shifting the value to set to shift_parameter_ET field
     * @param shifting the value to set to shift_parameter_AO field
     * @throws SQLException
     */
    public static void updateOpportunityShiftParameterAOandET(Connection conn, long oppId, double shifting, double shiftingAO) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE opportunities SET " +
                    "shift_parameter = ?, " +
                    "shift_parameter_ao = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, shifting);
            pstmt.setDouble(2, shiftingAO);
            pstmt.setLong(3, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update opportunity auto shift parameter.
     *
     * @param conn the db connection to use
     * @param oppId the opportunit id
     * @param shifting the value to set to auto_shift_parameter field
     * @throws SQLException
     */
    public static void updateOpportunityAutoShiftParameter(Connection conn, long oppId, double shifting) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE opportunities SET " +
                    "auto_shift_parameter = ? " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, shifting);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update opportunities that are closed and don't have unsettled investments
     * to settled state.
     *
     * @param conn the db connection to use
     * @param oppsToSettle list of opportunities that will be set to settled state.
     * @throws SQLException
     */
    public static void settleOpportunities(Connection conn,
    										List<OpportunitySettle> oppsToSettle) throws SQLException {
    	PreparedStatement pstmt = null;
        try {
        	StringBuilder sb = new StringBuilder();
        	String sql = sb.append(" UPDATE opportunities A ")
        					.append(" SET A.time_settled = CURRENT_DATE, ")
							.append(" A.is_settled     = 1, ")
							.append(" A.is_published   = 0 ")
							.append(" WHERE id          IN (%s) ").toString();

        	sb = new StringBuilder();
        	Iterator<OpportunitySettle> iter = oppsToSettle.iterator();
        	if (iter.hasNext()) {
        		iter.next();
        		sb.append(" ? ");
        		while (iter.hasNext()) {
        			iter.next();
        			sb.append(" , ? ");
				}
        	}

        	pstmt = conn.prepareStatement(String.format(sql, sb.toString()));

        	int idx = 1;
            for (OpportunitySettle oppToSettle : oppsToSettle) {
            	pstmt.setLong(idx++, oppToSettle.getId());
            }
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * This method gets all opportunities that are ready to be set to settled state.
     *
     * @param conn the db connection to use
     * @return list of opportunities that are ready to be set to settled state
     * @throws SQLException
     */
    public static List<OpportunitySettle> getOpportunitiesToSettle(Connection conn) throws SQLException {
    	List<OpportunitySettle> oppsToSettle = new ArrayList<OpportunitySettle>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
    	String sql = " select * from OPPORTUNITIES_TO_SETTLE where rownum < 900 ";
    	try {
    		pstmt = conn.prepareStatement(sql);
    		rs = pstmt.executeQuery();
    		SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
    		OpportunitySettle oppToSettle = null;
    		while(rs.next()) {
    			oppToSettle = new OpportunitySettle();
    			oppsToSettle.add(oppToSettle);
    			oppToSettle.setId(rs.getLong("ID"));
    			oppToSettle.setTypeId(rs.getLong("OPPORTUNITY_TYPE_ID"));
    			oppToSettle.setMarketId(rs.getLong("MARKET_ID"));

    			try {
    				oppToSettle.setTimeEstimatedClosing(localDateFormat.parse(rs.getString("TIME_EST_CLOSING")));
				} catch (ParseException e) {
					log.error("Can't parse time_est_closing for opp: " + oppToSettle.getId(), e);
				}

    			oppToSettle.setClosingLevel(CommonUtil.formatLevelByDecimalPoint(
    														rs.getDouble("CLOSING_LEVEL"),
    														rs.getLong("CLOSING_LEVEL_DECIMAL_POINT")));
    		}
    	} finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    	return oppsToSettle;
    }

    /**
     * Reload from the db the odds for opportunity.
     *
     * @param conn the db connection to use
     * @param o the opportunity to update the odds of (will take the id of the opp)
     * @throws SQLException
     */
    public static void updateOpportunityOdds(Connection conn, Opportunity o) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "B.* , " +
                    " A.odds_group " +
                "FROM " +
                    "opportunities A, " +
                    "opportunity_odds_types B " +
                "WHERE " +
                    "A.odds_type_id = B.id AND " +
                    "A.id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, o.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                o.setOddsTypeId(rs.getLong("id"));
                if (null == o.getOddsType()) {
                    o.setOddsType(new OpportunityOddsType());
                }
                o.getOddsType().setId(rs.getLong("id"));
                o.getOddsType().setOverOddsWin(rs.getFloat("over_odds_win"));
                o.getOddsType().setOverOddsLose(rs.getFloat("over_odds_lose"));
                o.getOddsType().setUnderOddsWin(rs.getFloat("under_odds_win"));
                o.getOddsType().setUnderOddsLose(rs.getFloat("under_odds_lose"));
                o.setOddsGroup(rs.getString("odds_group"));
            } else {
                log.warn("Couldn't locate the new odds for opportunity " + o.getId());
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }

    /**
     * Get the expiry_date of the options for specified market for today
     * (from options_expirations table). One day before the expiration move
     * to the next month.
     *
     * @param conn
     * @param marketId
     * @return The expiration date of this month options or <code>null</code> if not found.
     * @throws SQLException
     */
    public static Date getOpportunityOptionsExpirationDate(Connection conn, long marketId) throws SQLException {
        Date expDate = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "expiry_date " +
                    "FROM " +
                        "options_expirations " +
                    "WHERE " +
                        "expiry_date > trunc(current_date" + (marketId == 3 || marketId == 15 ? " + 1" : "") + ") AND " +
                        "is_active = 1 AND " +
                        "market_id = ? " +
                    "ORDER BY " +
                        "expiry_date";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) { // this is a stupid way to do "LIMIT 1" in the stupid Oracle
                expDate = new Date(rs.getTimestamp("expiry_date").getTime());
            } else {
                log.warn("Couldn't locate options expiration date for market " + marketId);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return expDate;
    }

    /**
     * Get the RIC LETTER to subcribe to reuters
     * (from options_expirations table).
     *
     * @param conn
     * @param marketId
     * @return The ric letter of this options expiration or <code>null</code> if not found.
     * @throws SQLException
     */
    public static String getOpportunityOptionsExpirationLetter(Connection conn, long marketId) throws SQLException {
        String useSecondLetter = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "ric_letter, " +
                        "ric_year " +
                    "FROM " +
                        "options_expirations " +
                    "WHERE " +
	                    "to_char(expiry_date, 'yyyy-mm-dd') <= to_char(sysdate, 'yyyy-mm-dd') AND " +
	                    "to_char(expiry_date_end, 'yyyy-mm-dd') >= to_char(sysdate, 'yyyy-mm-dd') AND " +
                        "is_active = 1 AND " +
                        "market_id = ? " +
                    "ORDER BY " +
                        "expiry_date";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) { // this is a stupid way to do "LIMIT 1" in the stupid Oracle
                String year = rs.getString("ric_year");
                String letter = rs.getString("ric_letter");
                if (null != letter && null != year) {
                    useSecondLetter = rs.getString("ric_letter") + year.substring(year.length() - 1);
                } else {
                    log.warn("Couldn't locate options expiration letter " + letter + " or year " + year + " will user defulte letter market id: " + marketId);
                }
            } else {
                log.warn("Couldn't locate options expiration will user defulte letter market id: " + marketId);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return useSecondLetter;
    }

    /**
     * Disable market opportunities.
     *
     * @param conn
     * @param marketId
     * @return The number of rows updated.
     * @throws SQLException
     */
    public static int disableMarketOpportunities(Connection conn, long marketId) throws SQLException {
        int count = 0;
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "opportunities " +
                    "SET " +
                        "is_disabled_service = 1, " +
                        "is_disabled = 1 " +
                    "WHERE " +
                        "market_id = ? AND " +
                        "is_disabled_service = 0 AND " +
                        "is_settled = 0 AND " +
                        "opportunity_type_id <> ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, Opportunity.TYPE_ONE_TOUCH);
            count = pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        return count;
    }

    /**
     * Enable market opportunities.
     *
     * @param conn
     * @param marketId
     * @return The number of rows updated.
     * @throws SQLException
     */
    public static int enableMarketOpportunities(Connection conn, long marketId) throws SQLException {
        int count = 0;
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "opportunities " +
                    "SET " +
                        "is_disabled_service = 0, " +
                        "is_disabled = CASE WHEN is_disabled_trader = 0 THEN 0 ELSE 1 END " +
                    "WHERE " +
                        "market_id = ? AND " +
                        "is_disabled_service = 1 AND " +
                        "is_settled = 0 AND " +
                        "opportunity_type_id <> ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, Opportunity.TYPE_ONE_TOUCH);
            count = pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        return count;
    }

    public static HashMap<Long,OpportunityType> getOpportunityTypes(Connection con) throws SQLException {
        PreparedStatement ps=null;
        ResultSet rs=null;

        HashMap<Long,OpportunityType> hm = new HashMap<Long,OpportunityType>();
        try {
            String sql="select * from OPPORTUNITY_TYPES order by description desc ";
            ps = con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
            	OpportunityType vo = new OpportunityType();
            	vo.setId(rs.getLong("id"));
            	vo.setMaxExposure(rs.getLong("max_exposure"));
            	vo.setIsRunWithMarket(rs.getInt("is_run_with_market"));
            	vo.setIsUseMargin(rs.getInt("is_use_margin"));
            	vo.setMarginPercentageUp(rs.getFloat("margin_percentage_up"));
            	vo.setMarginPercentageDown(rs.getFloat("margin_percentage_down"));
            	vo.setTimeCreated(rs.getDate("time_created"));
            	vo.setDescription(rs.getString("description"));
            	vo.setWriterId(rs.getLong("writer_id"));
            	vo.setProductTypeId(rs.getLong("product_type_id"));
            	hm.put(rs.getLong("id"), vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return hm;
    }

    public static ArrayList<SelectItem> getOpportunityProductTypes(Connection conn) throws SQLException{
    	PreparedStatement ps = null;
    	ResultSet rs = null;

    	ArrayList<SelectItem> list = new ArrayList<SelectItem>();
    	String sql = "select * from market_types order by ID";
    	try{
	    	ps = conn.prepareStatement(sql);
	    	rs = ps.executeQuery();
	    	while(rs.next()){
	    		list.add(new SelectItem(rs.getLong("id"), rs.getString("type")));
	    	}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return list;
    }

    public static Opportunity getById(Connection con, long id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =" select o.is_disabled_trader,o.is_disabled_service," +
                    " o.closing_level, o.current_level, o.id, o.is_disabled, o.is_published, o.is_settled, o.market_id, " +
                    " o.odds_type_id, o.opportunity_type_id, o.scheduled, o.writer_id, o.CLOSING_LEVEL_CALCULATION," +
                    " to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
                    " to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
                    " to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
                    " to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
                    " to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
                    " to_char(o.time_settled, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_settled, " +
                    " smg.market_group_id, m.display_name, m.feed_name, m.decimal_point, m.investment_limits_group_id, " +
                    " o.max_inv_coeff, o.is_suspended AS is_opp_suspended, o.quote_params " +
//                  " m.home_page_priority, m.home_page_hour_first, m.group_priority " +
                    " from opportunities o, markets m , skin_market_group_markets smgm, skin_market_groups smg " +
                    " where o.id=? and m.id = o.market_id ";
                    //" and smgm.skin_market_group_id = smg.id and m.id = smgm.market_id and smg.skin_id = ? ";

            ps = con.prepareStatement(sql);
            ps.setLong(1, id);
            //ps.setLong(2, skinId);

            rs = ps.executeQuery();
            while (rs.next()) {
                Opportunity opportunity = getVO(rs);
                Market market = new Market();
                market.setGroupId(new Long(rs.getLong("market_group_id")));
              //  market.setDisplayName(CommonUtil.getMessage(rs.getString("display_name"), null));
                market.setFeedName(rs.getString("feed_name"));
                market.setDecimalPoint(new Long(rs.getLong("decimal_point")));
                market.setInvestmentLimitsGroupId(new Long(rs.getLong("investment_limits_group_id")));
//                market.setHomePagePriority(rs.getInt("home_page_priority"));
//                market.setHomePageHourFirst(rs.getBoolean("home_page_hour_first"));
//                market.setGroupPriority(rs.getInt("group_priority"));
                market.setId(rs.getLong("market_id"));
                opportunity.setMarket(market);
                opportunity.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, opportunity.getId()));
                opportunity.setQuoteParams(rs.getString("quote_params"));
                return opportunity;
            }
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }

    public static Opportunity getByIdForInvestmentValidation(Connection conn, long oppId) throws SQLException {
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	try {
    		String sql =
    				"SELECT " +
    					"A.opportunity_type_id, " +
    					"A.is_disabled, " +
    					"A.scheduled, " +
    					"A.market_id, " +
    					"A.odds_group, " +
    					"A.is_published, " +
    					"A.max_inv_coeff, " +
    					"A.is_suspended AS is_opp_suspended, " +
                        "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                        "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                        "B.over_odds_win, " +
                        "B.over_odds_lose, " +
                        "B.under_odds_win, " +
                        "B.under_odds_lose, " +
                        "C.name, " +
                        "C.feed_name, " +
                        "C.random_ceiling, " +
                        "C.random_floor, " +
                        "C.is_suspended, " +
                        "C.sec_between_investments, " +
                        "C.sec_between_investments_ip, " +
                        "C.sec_for_dev2, " +
                        "C.amount_for_dev2, " +
                        "C.amount_for_dev2_night, " +
                        "C.amount_for_dev3, " +
                        "C.acceptable_level_deviation, " +
                        "C.acceptable_level_deviation_3, " +
                        "C.option_plus_fee " +
    				"FROM " +
	    				"opportunities A, " +
	    	            "opportunity_odds_types B, " +
	    				"markets C " +
    				"WHERE " +
	                    "A.odds_type_id = B.id AND " +
	                    "A.market_id = C.id AND " +
	                    "A.id = ?";
    		pstmt = conn.prepareStatement(sql);
    		pstmt.setLong(1, oppId);
    		rs = pstmt.executeQuery();
    		if (rs.next()) {
    			Opportunity opp = new Opportunity();
    			opp.setId(oppId);
    			opp.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
    			opp.setDisabled(rs.getBoolean("is_disabled"));
    			opp.setScheduled(rs.getInt("scheduled"));
    			opp.setOddsGroup(rs.getString("odds_group"));
    			opp.setPublished(rs.getBoolean("is_published"));
    			opp.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
    			opp.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
    			opp.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
    			opp.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));
    			opp.setSuspended(rs.getBoolean("is_opp_suspended"));

                OpportunityOddsType oddsType = new OpportunityOddsType();
                oddsType.setOverOddsWin(rs.getFloat("over_odds_win"));
                oddsType.setOverOddsLose(rs.getFloat("over_odds_lose"));
                oddsType.setUnderOddsWin(rs.getFloat("under_odds_win"));
                oddsType.setUnderOddsLose(rs.getFloat("under_odds_lose"));
                opp.setOddsType(oddsType);

                Market market = new Market();
                market.setId(rs.getLong("market_id"));
                market.setName(rs.getString("name"));
                market.setFeedName(rs.getString("feed_name"));
                market.setRandomCeiling(rs.getBigDecimal("random_ceiling"));
                market.setRandomFloor(rs.getBigDecimal("random_floor"));
                market.setSuspended(rs.getBoolean("is_suspended"));
                market.setSecBetweenInvestments(rs.getInt("sec_between_investments"));
                market.setSecBetweenInvestmentsSameIp(rs.getInt("sec_between_investments_ip"));
                market.setSecForDev2(rs.getInt("sec_for_dev2"));
                market.setAmountForDev2(rs.getFloat("amount_for_dev2"));
                market.setAmountForDev2Night(rs.getFloat("amount_for_dev2_night"));
                market.setAmountForDev3(rs.getFloat("amount_for_dev3"));
                market.setAcceptableDeviation(rs.getBigDecimal("acceptable_level_deviation"));
                market.setAcceptableDeviation3(rs.getBigDecimal("acceptable_level_deviation_3"));
                market.setOptionPlusFee(rs.getLong("option_plus_fee"));
                opp.setMarketId(market.getId());
                opp.setMarket(market);

                return opp;
    		}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(pstmt);
    	}
    	return null;
    }

    public static long getWinLoseByOpp(Connection conn, long opId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql = " select sum(house_result) winlose from investments i where i.is_settled=1 and i.opportunity_id="+opId;

            pstmt = conn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("winlose");
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }

    /**
     * Gets One Touch opportunities.
     *
     * @param conn
     *            the db connection to use
     * @param locale 
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getOneTouch(Connection conn, long skinId, long marketId, Locale locale) throws SQLException {
    	int index = 1;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();
        try {
            String sql =
                "SELECT " +
	                "o.*, " +
                	"o.is_suspended AS is_opp_suspended, " +
                	"ot.*, " +
	                getFormatTimesSQL() +
	                ",B.display_name, " +
	                "C.over_odds_win, " +
	                "C.over_odds_lose, " +
	                "C.under_odds_win, " +
	                "C.under_odds_lose " +
                "FROM " +
	                "opportunities o, " +
	                "markets B, " +
	                "opportunity_odds_types C, " +
	                "opportunity_one_touch ot, " +
	                "ONE_TOUCH_MARKETS_SKINS otms " +
                "WHERE " +
	                "o.opportunity_type_id = 2 AND " +
	                "o.is_published = 1 AND " +
	                "o.market_id = b.id AND " +
	                "b.is_suspended = 0 AND " +
	                "o.id = ot.opportunity_id AND " +
	                "o.odds_type_id = c.id AND " +
	                "o.time_first_invest <= current_timestamp AND current_timestamp < o.time_last_invest and " +
	                "otms.skin_Id = ? AND " +
	                "otms.market_id = B.id ";
	                if (marketId != 0) {
		  				sql +=	" AND B.id = ? ";
	                } else {
				sql += " AND NOT B.id IN ("+ ConstantsBase.MARKET_BITCOIN_ONE_TOUCH_ID + "," + ConstantsBase.MARKET_EURUSD_SPECIAL + ","
											+ ConstantsBase.MARKET_OIL_SPECIAL + "," + ConstantsBase.MARKET_GOLD_SPECIAL + ","
											+ ConstantsBase.MARKET_DAX_SPECIAL + "," + ConstantsBase.MARKET_SP500_SPECIAL + ") ";
	                }

               sql += "order by otms.priority asc, o.time_est_closing, ot.up_down desc, o.CURRENT_LEVEL desc";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(index++, skinId);

            if (marketId != 0) {
            	pstmt.setLong(index++, marketId);
            }



            rs = pstmt.executeQuery();

            while(rs.next()) {
                Opportunity o = getVO(rs);
                Market m = new Market();
                m.setId(o.getMarketId());
                m.setDisplayName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
                o.setMarket(m);
                OpportunityOddsType ot = new OpportunityOddsType();
                ot.setId(rs.getLong("odds_type_id"));
                ot.setOverOddsWin(rs.getFloat("over_odds_win"));
                ot.setOverOddsLose(rs.getFloat("over_odds_lose"));
                ot.setUnderOddsWin(rs.getLong("under_odds_win"));
                ot.setUnderOddsLose(rs.getFloat("under_odds_lose"));
                o.setOddsType(ot);
                //o.setOneTouchMsg(rs.getString("OPPORTUNITY_MSG"));
                o.setOneTouchUpDown(rs.getLong("UP_DOWN"));
                if(rs.getLong("UP_DOWN") == 1 ){
                	o.setOneTouchMsg("onetouch.markets.msg.up");
                } else {
                	o.setOneTouchMsg("onetouch.markets.msg.down");
                }
                o.setDecimalPointOneTouch(rs.getLong("DECIMAL_POINT"));
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                list.add(o);
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    /**
     * Gets One Touch opportunity.
     *
     * @param conn
     *            the db connection to use
     * @param id
     * 			  the opp id to get
     * @param locale 
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static Opportunity getOneTouchById(Connection conn, long id, Locale locale) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                "o.*, " +
                "o.is_suspended AS is_opp_suspended, " +
                "ot.*, " +
                getFormatTimesSQL() +
                ",B.display_name,B.INVESTMENT_LIMITS_GROUP_ID, " +
                "C.over_odds_win, " +
                "C.over_odds_lose, " +
                "C.under_odds_win, " +
                "C.under_odds_lose, " +
                " B.RAISE_EXPOSURE_UP_AMOUNT, " +
                " B.RAISE_EXPOSURE_DOWN_AMOUNT, " +
                " B.RAISE_CALL_LOWER_PUT_PERCENT, " +
                " B.RAISE_PUT_LOWER_CALL_PERCENT " +
                "FROM " +
                "opportunities o, " +
                "markets B, " +
                "opportunity_odds_types C, " +
                "opportunity_one_touch ot " +
                "WHERE " +
                "o.opportunity_type_id = 2 AND " +
                "o.is_published = 1 AND " +
                "o.market_id = b.id AND " +
                "b.is_suspended = 0 AND " +
                "o.id = ot.opportunity_id AND " +
                "o.odds_type_id = c.id AND "+
                "o.time_first_invest <= current_timestamp AND current_timestamp < o.time_last_invest AND "+
                "o.id =?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);

            rs = pstmt.executeQuery();

            if(rs.next()) {
                Opportunity o = getVO(rs);
                Market m = new Market();
                m.setId(o.getMarketId());
                m.setDisplayName(CommonUtil.getMessage(locale, rs.getString("display_name"), null));
                m.setInvestmentLimitsGroupId(rs.getLong("INVESTMENT_LIMITS_GROUP_ID"));
                m.setRaiseExposureUpAmount(rs.getLong("RAISE_EXPOSURE_UP_AMOUNT"));
                m.setRaiseExposureDownAmount(rs.getLong("RAISE_EXPOSURE_DOWN_AMOUNT"));
                m.setUpStrikeRaiseLowerPercent(rs.getDouble("RAISE_CALL_LOWER_PUT_PERCENT"));
                m.setDownStrikeRaiseLowerPercent(rs.getDouble("RAISE_PUT_LOWER_CALL_PERCENT"));
                o.setMarket(m);
                OpportunityOddsType ot = new OpportunityOddsType();
                ot.setId(rs.getLong("odds_type_id"));
                ot.setOverOddsWin(rs.getFloat("over_odds_win"));
                ot.setOverOddsLose(rs.getFloat("over_odds_lose"));
                ot.setUnderOddsWin(rs.getLong("under_odds_win"));
                ot.setUnderOddsLose(rs.getFloat("under_odds_lose"));
                o.setOddsType(ot);
                o.setOneTouchMsg(rs.getString("OPPORTUNITY_MSG"));
                o.setOneTouchUpDown(rs.getLong("UP_DOWN"));
                o.setDecimalPointOneTouch(rs.getLong("DECIMAL_POINT"));
                o.setOneTouchOppExposure(rs.getLong("EXPOSURE_REACHED_AMOUNT"));
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                return o;
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }



    /**
     * Gets the closed oneTouchopportunities for the list.
     *
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getClosedOneTouchOpportunities(Connection conn,Date firstInv, Date lastInv,
    		long marketId) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();
        FacesContext context = FacesContext.getCurrentInstance();
        ApplicationDataBase a = (ApplicationDataBase) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
        try {
            String sql =
                    " SELECT DISTINCT " +
                    	" o.MARKET_ID, " +
                    	" o.CLOSING_LEVEL, " +
                    	" to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
					    " to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
					    " to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
					    " ot.up_down, " +
					    " o.time_est_closing," +
					    " m.display_name, " +
					    " o.IS_SETTLED " +
                    " FROM opportunities o,markets m , opportunity_one_touch ot, one_touch_markets_skins otms " +
                    " WHERE o.market_id=m.id " +
                    	" and o.id = ot.opportunity_id " +
                    	" and o.time_act_closing is not null " +
                    	" and o.opportunity_type_id = ? " +
                    	" and o.market_id = otms.market_id " +
                    	" and otms.skin_id = " + a.getSkinId() + " ";

            if ( marketId > 0 )  {   // need to filter
           		sql += " and m.id = " + marketId + " ";
            }

            if ( null != firstInv && null != lastInv ) {
            	sql += " and trunc(o.time_first_invest)=trunc(?) and trunc(o.time_est_closing)=trunc(?) ";
            }
            sql += " order by o.time_est_closing desc,m.display_name ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, Opportunity.TYPE_ONE_TOUCH);

            if ( null != firstInv && null != lastInv ) {
            	pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(firstInv));
            	pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(lastInv));
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {
            	Opportunity op = new Opportunity();

            	op.setMarketId(rs.getLong("MARKET_ID"));
            	op.setClosingLevel(rs.getDouble("CLOSING_LEVEL"));
            	op.setIsSettled(rs.getInt("IS_SETTLED"));

            	try {
                    SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            		if (rs.getString("ftime_first_invest")!=null)
            			op.setTimeFirstInvest(localDateFormat.parse(rs.getString("ftime_first_invest")));
            		if (rs.getString("ftime_est_closing")!=null)
            			op.setTimeEstClosing(localDateFormat.parse(rs.getString("ftime_est_closing")));
            		if (rs.getString("ftime_last_invest")!=null)
            			op.setTimeLastInvest(localDateFormat.parse(rs.getString("ftime_last_invest")));
                } catch (ParseException pe) {
                    log.error("Can't parse datetime.", pe);
                }

            	op.setOneTouchUpDown(rs.getLong("up_down"));
                list.add(op);
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }


    /**
     * Gets the dates of oneTouchopportunities that settled.
     * This list is for lastLevels one touch filter
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Date> getClosedDatesOneOpportunities(Connection conn) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int numberOpp = 0;
        ArrayList<Date> list = new ArrayList<Date>();

        try {
            String sql =
            			"select distinct " +
            			"to_char(time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
            			"to_char(time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing " +
            			"from opportunities " +
                        "where time_act_closing is not null and opportunity_type_id = ? and is_settled = 1 ";

            sql += " order by time_first_invest desc ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, Opportunity.TYPE_ONE_TOUCH);


            rs = pstmt.executeQuery();

            while ( rs.next() && ( numberOpp < ConstantsBase.ONE_TOUCH_OPPORTUNITIES_SELECT ) ) {
                Date firstInv = null;
                Date lastInv = null;

            	SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd");
          		if ( rs.getString("time_first_invest") != null ) {
          			 firstInv = localDateFormat.parse(rs.getString("time_first_invest"));
          		}
          		if ( rs.getString("time_est_closing") != null ) {
          			lastInv = localDateFormat.parse(rs.getString("time_est_closing"));
         		}

            	list.add(firstInv);
                list.add(lastInv);
                numberOpp++;
            }

        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	public static void createOneTouchOpportunities(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
        try {
        	// WARN: This query depends on the server session settings for the days numbers (first day of the week)
            // It use sunday as first day of the week (SUN = 1)
            cstmt = conn.prepareCall("BEGIN OPPORTUNITIES_MANAGER.CREATE_ONETOUCH_OPPORTUNITIES(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}

	public static String oneTouchReportCreatedOpportunities(Connection conn) throws SQLException {
	String report = "<html><body><table border=\"1\"><tr><td>Market</td><td>Time First Invest</td><td>Time Last Invest</td><td>Time Est Closing</td><td>Decimal Point</td><td>Up/Down</td></tr>";
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try {
        String sql =
            "SELECT " +
                "to_char(A.time_first_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_first_invest, " +
                "to_char(A.time_est_closing, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_est_closing, " +
                "to_char(A.time_last_invest, 'DD/MM/YYYY HH24:MI TZHTZM') AS time_last_invest, " +
                "B.name, " +
                "oot.DECIMAL_POINT, " +
                "oot.UP_DOWN " +
            "FROM " +
                "opportunities A, " +
                "markets B, " +
                "OPPORTUNITY_ONE_TOUCH oot " +
            "WHERE " +
                "A.market_id = B.id AND " +
                "A.time_created > current_timestamp - to_dsinterval('0 01:00:00') AND " +
                "A.scheduled = 5 AND " +
                "A.id = oot.OPPORTUNITY_ID " +
            "ORDER BY " +
                "B.name, " +
                "A.time_est_closing";
        pstmt = conn.prepareStatement(sql);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            report +=
                "<tr><td>" +
                rs.getString("name") +
                "</td><td>" +
                rs.getString("time_first_invest") +
                "</td><td>" +
                rs.getString("time_last_invest") +
                "</td><td>" +
                rs.getString("time_est_closing") +
                "</td><td>" +
                rs.getInt("DECIMAL_POINT") +
                "</td><td>" +
                rs.getInt("UP_DOWN") +
                "</td></tr>";
        }
    } finally {
        closeResultSet(rs);
        closeStatement(pstmt);
    }
    report += "</table></body></html>";
    return report;
	}

	/**
     * is has One Touch opportunitie open.
     *
     * @param conn
     *            the db connection to use
     * @return <code>boolean</code> if we have open 1T.
     * @throws SQLException
     */
    public static boolean isHasOneTouchOpen(Connection conn, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean has = false;
        try {
            String sql =
                "SELECT " +
	                "1 " +
                "FROM " +
	                "opportunities o, " +
	                "markets B, " +
	                "opportunity_odds_types C, " +
	                "opportunity_one_touch ot, " +
	                "ONE_TOUCH_MARKETS_SKINS otms " +
                "WHERE " +
	                "o.opportunity_type_id = 2 AND " +
	                "o.is_published = 1 AND " +
	                "o.market_id = b.id AND " +
	                "b.is_suspended = 0 AND " +
	                "o.id = ot.opportunity_id AND " +
	                "o.odds_type_id = c.id AND " +
	                "o.time_first_invest <= current_timestamp AND current_timestamp < o.time_last_invest and " +
	                "otms.skin_Id = ? AND " +
	                "otms.market_id = B.id AND " +
	                "NOT B.id IN (" + ConstantsBase.MARKET_BITCOIN_ONE_TOUCH_ID + ", " + ConstantsBase.MARKET_ALIBABA_ONE_TOUCH_ID + ") " +
                "order by o.time_created ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, skinId);

            rs = pstmt.executeQuery();

            while(rs.next()) {
            	has = true;
            	break;
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return has;
    }

    public static Opportunity getNextHourlyOppToOpenByOppId(Connection conn, long oppId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Opportunity nextOpp = null;
        try {
            String sql =
                "SELECT " +
                    "o.is_disabled_trader, " +
                    "o.is_disabled_service, " +
                    "o.closing_level, " +
                    "o.current_level, " +
                    "o.id, " +
                    "o.is_disabled, " +
                    "o.is_published, " +
                    "o.is_settled, " +
                    "o.market_id, " +
                    "o.odds_type_id, " +
                    "o.opportunity_type_id, " +
                    "o.scheduled, " +
                    "o.writer_id, " +
                    "o.CLOSING_LEVEL_CALCULATION, " +
                    "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
                    "to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
                    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
                    "to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
                    "to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
                    "to_char(o.time_settled, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_settled, " +
                    "o.max_inv_coeff, " + 
                    "o.is_suspended AS is_opp_suspended " +
                "FROM " +
                    " Opportunities o " +
                 "WHERE " +
                    "o.id = GET_OPP_ROLL_UP(?) ";


            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, oppId);

            rs = pstmt.executeQuery();

            if(rs.next()) {
                nextOpp = getVO(rs);
                nextOpp.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, nextOpp.getId()));
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return nextOpp;
    }

    public static Date getNextOpenDate(Connection conn, long marketId)throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Date nextOpenDateFromDb = null;
        try {
            String sql = "SELECT " +
                             "GET_NEXT_OPEN_TIME(?) nextOpenTime " +
                         "FROM " +
                             "dual";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                try {
                    String tfi = rs.getString("nextOpenTime");
                    if (log.isTraceEnabled()) {
                        log.trace("nextOpenTime: " + tfi);
                    }
                    nextOpenDateFromDb = getTimeWithTimezone(tfi);
                } catch (Throwable t) {
                    log.error("Can't parse datetime.", t);
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return nextOpenDateFromDb;
    }

    /**
     * is has publish opportunity from type 1.
     *
     * @param conn
     *            the db connection to use
	 * @param skinId user skin id
     * @return <code>boolean</code> if we have open 1T.
     * @throws SQLException
     */
    public static boolean isHasMarketOpen(Connection conn, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean has = false;
        try {
            String sql =
                        "SELECT " +
                            "1 " +
                        "FROM " +
                            "opportunities o, " +
                            "markets m " +
                        "WHERE " +
                            "o.opportunity_type_id = 1 AND " +
                            "o.is_published = 1 AND " +
                            "o.market_id = m.id AND " +
                            "m.is_suspended = 0 AND " +
                            "o.market_id in (SELECT " +
                                              "smgm.market_id " +
                                            "FROM " +
                                              "skin_market_groups smg, " +
                                              "skin_market_group_markets smgm " +
                                            "WHERE " +
                                              "smg.skin_id = ? AND " +
                                              "smg.id = smgm.skin_market_group_id)";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, skinId);

            rs = pstmt.executeQuery();

            if(rs.next()) {
                has = true;
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return has;
    }

    public static OpportunityCacheBean getOpportunityCacheBean(Connection conn, long oppId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        OpportunityCacheBean oppcb = null;
        try {
            String sql =
                "SELECT " +
                    "A.id, " +
                    "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "A.scheduled, " +
                    "A.current_level, " +
                    "A.opportunity_type_id, " +
                    "B.display_name, " +
                    "B.decimal_point, " +
                    "B.id AS market_id, " +
                    "A.opportunity_type_id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppcb = new OpportunityCacheBean();
                oppcb.setId(rs.getLong("id"));
                oppcb.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
                oppcb.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
                oppcb.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
                oppcb.setScheduled(rs.getInt("scheduled"));
                oppcb.setCurrentLevel(rs.getDouble("current_level"));
                oppcb.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                oppcb.setMarketDisplayName(rs.getString("display_name"));
                oppcb.setMarketId(rs.getLong("market_id"));
                oppcb.setMarketDecimalPoint(rs.getLong("decimal_point"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppcb;
    }

    public static void getOpportunityCallPutAmounts(Connection conn, Opportunity opp) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            	" SELECT SUM(DECODE(A.type_id, 1, A.amount * A.rate, 0)) AS calls "
            		+ " ,SUM(DECODE(A.type_id, 2, A.amount * A.rate, 0)) AS puts "
            		+ " ,SUM(DECODE(A.type_id, 1, 1, 0)) AS calls_count "
            		+ " ,SUM(DECODE(A.type_id, 2, 1, 0)) AS puts_count "
            		+ " ,SUM(DECODE(B.skin_id, ?, DECODE(A.type_id, 1, 1, 0), 0)) AS calls_count_et "
                    + " ,SUM(DECODE(B.skin_id, ?, DECODE(A.type_id, 2, 1, 0), 0)) AS puts_count_et "
        		+ " FROM investments A "
        			+ " ,users B "
    			+ " WHERE A.opportunity_id = ? "
    				+ " AND A.user_id = B.id "
    				+ " AND B.class_id <> 0 ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, Skin.SKIN_ETRADER);
            pstmt.setLong(2, Skin.SKIN_ETRADER);
            pstmt.setLong(3, opp.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                opp.setCalls(rs.getDouble("calls"));
                opp.setPuts(rs.getDouble("puts"));
                opp.setCallsCount(rs.getInt("calls_count"));
                opp.setPutsCount(rs.getInt("puts_count"));
                opp.setCallsCountET(rs.getInt("calls_count_et"));
                opp.setPutsCountET(rs.getInt("puts_count_et"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }

    public static void getOpportunityCallPutBoughtSoldAmounts(Connection conn, Opportunity opp) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT SUM(DECODE(A.type_id, 4, A.amount * A.rate, 0)) AS calls "
            		+ " ,SUM(DECODE(A.type_id, 5, A.amount * A.rate, 0)) AS puts "
            		+ " ,SUM(DECODE(A.type_id, 4, (A.amount / A.current_level) * A.rate, 0)) AS bought "
            		+ " ,SUM(DECODE(A.type_id, 5, (A.amount / A.current_level) * A.rate, 0)) AS sold "
        		+ " FROM investments A "
        			+ " ,users B "
    			+ " WHERE A.opportunity_id = ? "
    				+ " AND A.user_id = B.id "
    				+ " AND B.class_id <> 0 ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, opp.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                opp.setCalls(rs.getDouble("calls"));
                opp.setPuts(rs.getDouble("puts"));
                opp.setContractsBought(rs.getDouble("bought"));
                opp.setContractsSold(rs.getDouble("sold"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }

    public static void updateCurrentlevel(Connection conn, long oppMarketId, double raiseLowerStrikePercent, int upOrDown ) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                " UPDATE " +
                    " opportunities o " +
                " SET " +
                    " current_level = round(current_level * ( 1 + ( " +  raiseLowerStrikePercent + " / 100)),(select decimal_point from opportunity_one_touch oot where oot.opportunity_id = o.id)) " +
                " WHERE " +
                      " o.market_id = " + oppMarketId + " AND " +
                      " o.is_published = 1 AND " +
                      " sysdate >= o.time_first_invest AND " +
                      " sysdate <= o.time_last_invest AND " +
                      " o.opportunity_type_id = " + ConstantsBase.OPPORTUNITY_TYPE_ONE_TOUCH + " AND " +
                       upOrDown + " = (select up_down from opportunity_one_touch oot where oot.opportunity_id = o.id) ";
            pstmt = conn.prepareStatement(sql);
            //pstmt.setDouble(1, raiseLowerStrikePercent);
            //pstmt.setLong(2, oppMarketId);
            //pstmt.setLong(3, upOrDown);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateOppMaxExposure(Connection conn, long oppId, long raiseExposureUpAmount) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                " UPDATE opportunity_skin_group_map " +
				" SET max_exposure     = max_exposure + ? " +
				" WHERE opportunity_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setDouble(1, raiseExposureUpAmount);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateOpp1TExposureReached(Connection conn, long oppId, long marketId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                " UPDATE OPPORTUNITY_ONE_TOUCH oot " +
                " SET " +
                "    EXPOSURE_REACHED_AMOUNT = EXPOSURE_REACHED_AMOUNT + (select exposure_reached_amount from markets where id = ? ) " +
                " WHERE " +
                "        oot.opportunity_id = ?   ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, oppId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static boolean getHasOneTouchFacebookPlus(Connection conn) throws SQLException {
    	int index = 1;
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean hasOneTouchFacebookPlus = false;

        try {
            String sql =
            	"SELECT " +
					" 1 " +
				"FROM " +
					" opportunities o " +
				"WHERE " +
					" o.market_id = ? AND " +
					" o.is_published = 1 AND " +
					" SYS_EXTRACT_UTC(o.time_est_closing) >  sysdate ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(index, ConstantsBase.MARKET_FACEBOOK_PLUS_ID);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	hasOneTouchFacebookPlus = true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return hasOneTouchFacebookPlus;
	}

    public static void setOppMarketQuote(Connection conn, long marketId, String quote) throws SQLException{
    	PreparedStatement ps = null;
    	try{
    		String sql = "UPDATE opportunities " +
    			   "SET quote_params = ? " +
    			   "WHERE market_id = ? " +
    			   "AND is_published = " + ConstantsBase.NOT_PUBLISHED;
    		ps = conn.prepareStatement(sql);
    		ps.setString(1, quote);
    		ps.setLong(2, marketId);
    		ps.executeUpdate();
    	} finally {
    		closeStatement(ps);
    	}
    }
}