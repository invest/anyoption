/**
 * 
 */
package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.ConstantInvestmentsStream;

/**
 * @author pavelhe
 *
 */
public class ConstantInvestmentsStreamDAOBase extends DAOBase {

	public static ArrayList<ConstantInvestmentsStream> getAll(Connection con) throws SQLException {

		Statement st = null;
		ResultSet rs = null;
		ArrayList<ConstantInvestmentsStream> list = new ArrayList<ConstantInvestmentsStream>();

		try {
			String sql = " SELECT * FROM CONSTANT_INVESTMENTS_STREAMS ORDER BY 1";

			st = con.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}
	
	public static ArrayList<ConstantInvestmentsStream> getActive(Connection con) throws SQLException {

		Statement st = null;
		ResultSet rs = null;
		ArrayList<ConstantInvestmentsStream> list = new ArrayList<ConstantInvestmentsStream>();

		try {
			String sql = " SELECT * FROM CONSTANT_INVESTMENTS_STREAMS where IS_ACTIVE = 1 ";

			st = con.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}
	
	protected static ConstantInvestmentsStream getVO(ResultSet rs) throws SQLException {
		ConstantInvestmentsStream vo = new ConstantInvestmentsStream();
		vo.setId(rs.getInt("id"));
		vo.setStreamName(rs.getString("STREAM_NAME"));
		long skinId = rs.getLong("SKIN_ID");
		if (!rs.wasNull()) {
			vo.setSkinId(skinId);
		}
		vo.setInvestmentMinAmount(rs.getLong("INVESTMENT_MIN_AMOUNT"));
		vo.setMaxRows(rs.getLong("MAX_ROWS"));
		vo.setActive(rs.getBoolean("IS_ACTIVE"));
		
		return vo;
	}
	
}
