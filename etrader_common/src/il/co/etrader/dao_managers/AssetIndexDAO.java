
package il.co.etrader.dao_managers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.AssetIndex;

public class AssetIndexDAO extends DAOBase {

  public static HashMap<Long,AssetIndex> getAll(Connection con, long skinId) throws SQLException {

	  PreparedStatement ps = null;
	  ResultSet rs = null;
      HashMap<Long,AssetIndex> list = new HashMap<Long, AssetIndex>();

	  try {
        String sql = "SELECT " +
                        "ai.* " +
                     "FROM " +
                        "asset_index ai, " +
                        "skin_market_groups sm, " +
                        "skin_market_group_markets smg " +
                     "WHERE " +
                        "sm.id = smg.skin_market_group_id AND " +
                        "sm.skin_id = ? AND " +
                        "smg.market_id = ai.market_id ";

        ps = con.prepareStatement(sql);
        ps.setLong(1, skinId);
        rs = ps.executeQuery();

        while (rs.next()) {
        	AssetIndex vo = new AssetIndex();
        	vo.setId(rs.getLong("market_id"));
        	vo.setTradingDays(rs.getString("trading_days"));
        	if(null != rs.getString("friday_time")) {
        		vo.setFridayTime(rs.getString("friday_time"));
            }
            vo.setStartTime(rs.getString("start_time"));
            vo.setEndTime(rs.getString("end_time"));
            vo.setFullDay(rs.getInt("IS_FULL_DAY") == 1 ? true : false);
            vo.setStartBreakTime(rs.getString("start_break_time"));
            vo.setEndBreakTime(rs.getString("end_break_time"));
        	list.put(vo.getId(), vo);
        }
	  } finally {
          closeResultSet(rs);
          closeStatement(ps);
	  }

	return list;
  }

	public static AssetIndex getAssetIndexByMarketId(Connection con, long marketId) throws SQLException {
		AssetIndex vo = null;
		PreparedStatement ps = null;
	    ResultSet rs = null;
	    try {
	    	String sql = " SELECT " +
	    				 		" * " +
	    				 " FROM " +
	    				 		" asset_index " +
	    				 " WHERE " +
	    				 		" MARKET_ID = ? ";
	    	
	    	ps = con.prepareStatement(sql);
	        ps.setLong(1, marketId);
	        rs = ps.executeQuery();
	        
	        while (rs.next()) {
	        	vo = new AssetIndex();
	        	vo.setId(rs.getLong("market_id"));
	        	vo.setTradingDays(rs.getString("trading_days"));
	        	if(null != rs.getString("friday_time")) {
	        		vo.setFridayTime(rs.getString("friday_time"));
	            }
	            vo.setStartTime(rs.getString("start_time"));
	            vo.setEndTime(rs.getString("end_time"));
	            vo.setFullDay(rs.getInt("IS_FULL_DAY") == 1 ? true : false);
	            vo.setTwentyFourSeven(rs.getInt("is_24_7") == 1 ? true : false);
	            vo.setStartBreakTime(rs.getString("start_break_time"));
	            vo.setEndBreakTime(rs.getString("end_break_time"));
	        }
	    } finally {
	    	closeResultSet(rs);
	        closeStatement(ps);
	    }
		return vo;
	}
	
	public static void updateAssetIndexByMarketId(Connection con, AssetIndex assetIndex, long marketId) throws SQLException {
		PreparedStatement ps = null;
		PreparedStatement insert = null;
		int index = 1;
	    try {
	    	String sql = " UPDATE " +
	    						" asset_index " +
	    				 " SET " +
	    						" trading_days = ? , " +
	    						" friday_time = ? , " +
	    						" start_time = ? , " +
	    						" end_time = ? , " +
	    						" is_full_day = ? , " +
	    						" is_24_7 = ? , " +
	    						" start_break_time = ? , " +
	    						" end_break_time = ? " +
	    				" WHERE " +
	    						" market_id = ? ";
	    	
	    	ps = con.prepareStatement(sql);
	        ps.setString(index++, assetIndex.getTradingDays());
	        if (assetIndex.getFridayTime() == null) {
	        	ps.setNull(index++, Types.VARCHAR);
	        } else {
	        	ps.setString(index++, assetIndex.getFridayTime());
	        }
	        ps.setString(index++, assetIndex.getStartTime());
	        ps.setString(index++, assetIndex.getEndTime());
	        ps.setBoolean(index++, assetIndex.isFullDay());
	        ps.setBoolean(index++, assetIndex.isTwentyFourSeven());
	        if (assetIndex.getStartBreakTime() == null) {
	        	ps.setNull(index++, Types.VARCHAR);
	        } else {
	        	ps.setString(index++, assetIndex.getStartBreakTime());
	        }
	        if (assetIndex.getEndBreakTime() == null) {
	        	ps.setNull(index++, Types.VARCHAR);
	        } else {
	        	ps.setString(index++, assetIndex.getEndBreakTime());
	        }
	        ps.setLong(index++, marketId);
	        if (ps.executeUpdate() == 0) {
	        	index = 1;
	        	String insertsql = 
	        					" INSERT INTO " +
	        							" asset_index " +
	        								" ( " + 
	        									" market_id " +
	        									" ,trading_days " +
	        									" ,friday_time " + 
	        									" ,start_time " +
	        									" ,end_time " + 
	        									" ,is_full_day " +
	        									" ,is_24_7 " +
	        									" ,start_break_time " +
	        									" ,end_break_time " +
	        								" ) " +
	        					" VALUES " + 
	        							" (?, ?, ?, ?, ?, ?, ?, ?, ?) "; 
	        	
	        	insert = con.prepareStatement(insertsql);
		        insert.setLong(index++, marketId);
		        if (assetIndex.getTradingDays() == null) {
		        	insert.setString(index++, "0111110");
		        } else {
		        	insert.setString(index++, assetIndex.getTradingDays());
		        }
		        if (assetIndex.getFridayTime() == null) {
		        	insert.setNull(index++, Types.VARCHAR);
		        } else {
		        	insert.setString(index++, assetIndex.getFridayTime());
		        }
		        if (assetIndex.getStartTime() == null) {
		        	insert.setString(index++, "00:00");
		        } else {
		        	insert.setString(index++, assetIndex.getStartTime());
		        }
		        if (assetIndex.getEndTime() == null) {
		        	insert.setString(index++, "23:00");
		        } else {
		        	insert.setString(index++, assetIndex.getEndTime());
		        }
		        insert.setBoolean(index++, assetIndex.isFullDay());
		        insert.setBoolean(index++, assetIndex.isTwentyFourSeven());
		        if (assetIndex.getStartBreakTime() == null) {
		        	insert.setNull(index++, Types.VARCHAR);
		        } else {
		        	insert.setString(index++, assetIndex.getStartBreakTime());
		        }
		        if (assetIndex.getEndBreakTime() == null) {
		        	insert.setNull(index++, Types.VARCHAR);
		        } else {
		        	insert.setString(index++, assetIndex.getEndBreakTime());
		        }
		        insert.execute();
	        }	        
		} finally {
	        closeStatement(ps);
	        if (insert != null) {
	        	closeStatement(insert);
	        }
		}
	}
}