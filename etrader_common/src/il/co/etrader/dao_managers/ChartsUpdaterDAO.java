//package il.co.etrader.dao_managers;
//
//import il.co.etrader.bl_vos.ChartsUpdaterMarket;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Date;
//
//import com.anyoption.common.beans.MarketRate;
//import com.anyoption.common.daos.DAOBase;
//
//public class ChartsUpdaterDAO extends DAOBase {
//    public static ArrayList<ChartsUpdaterMarket> getChartsUpdaterMarkets(Connection conn, long opportunityTypeId) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        ArrayList<ChartsUpdaterMarket> l = new ArrayList<ChartsUpdaterMarket>();
//        try {
//            String sql =
//            	" SELECT " +
//                	" x.*, " +
//            		" to_char(CASE WHEN x.time_first_invest is not null " +
//	            				 " THEN x.time_first_invest " +
//	            				 " ELSE x.time_est_closing END, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') last_hour_opening_time " +
//            	" FROM " +
//	                " (SELECT " +
//	                    " m.id, " +
//	                    " m.display_name, " +
//	                    " m.decimal_point, " +
//	                    " min(curr_opp.time_first_invest) time_first_invest, " +
//	                    " max(close_opp.time_est_closing) time_est_closing, " +
//	                    " CASE WHEN count (open_opp.id) > 0 THEN 1 ELSE 0 END opened, " +
//	                    " CASE WHEN count (opp_temp.id) > 0 THEN 1 ELSE 0 END hourly " +
//	                " FROM " +
//	                    " markets m " +
//	                        " left join opportunities open_opp on open_opp.market_id = m.id " +
//	                                " and open_opp.is_published = 1 " +
//	                                " and open_opp.time_est_closing > trunc(current_date - 1) " +
//	                                " and open_opp.opportunity_type_id = ? " +
//	                        " left join opportunities curr_opp on curr_opp.market_id = m.id  " +
//		                            " and curr_opp.scheduled = 1  " +
//		                            " and curr_opp.is_published = 1 " +
//	                        "left join opportunities close_opp on close_opp.market_id = m.id " +
//	                                " and close_opp.time_act_closing > sysdate - 7 " +
//	                                " and close_opp.opportunity_type_id = ? " +
//                            "left join opportunity_templates opp_temp on opp_temp.market_id = m.id " +
//	                                " and opp_temp.scheduled = 1 " +
//	                                " and opp_temp.is_active = 1 " +
//	                " WHERE " +
//	                    " EXISTS (SELECT id FROM opportunity_templates WHERE market_id = m.id AND opportunity_type_id = ? AND is_active = 1) " +
//	                " GROUP BY " +
//	                    " m.id, " +
//	                    " m.display_name, " +
//	                    " m.decimal_point) x ";
//
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, opportunityTypeId);
//            pstmt.setLong(2, opportunityTypeId);
//            pstmt.setLong(3, opportunityTypeId);
//            rs = pstmt.executeQuery();
//            ChartsUpdaterMarket m = null;
//            while(rs.next()) {
//            	m = new ChartsUpdaterMarket();
//        		m.setId(rs.getLong("id"));
//        		m.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
//        		m.setDisplayNameKey(rs.getString("display_name"));
//                m.setLastHourClosingTime(getTimeWithTimezone(rs.getString("last_hour_opening_time")));
//                m.setOpened(rs.getBoolean("opened"));
//                m.setHaveHourly(rs.getBoolean("hourly"));
//                l.add(m);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return l;
//    }
//
//    /**
//     * Load market rates.
//     *
//     * @param conn
//     * @param marketId the market for which to load the rates for
//     * @param after if <code>null</code> load all. else load rates after that time
//     * @return <code>ArrayList<MarketRate></code>
//     * @throws SQLException
//     */
//    public static ArrayList<MarketRate> getMarketRates(Connection conn, long marketId, Date after) throws SQLException {
//        ArrayList<MarketRate> l = new ArrayList<MarketRate>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try {
//            String sql =
//                    "SELECT " +
//                        "* " +
//                    "FROM " +
//                        "market_rates " +
//                    "WHERE " +
//                        "market_id = ? AND " +
//                        (null != after ? "rate_time > ? AND " : "") +
////                        "rate_time <= current_date AND " + // "rate_time <= current_date - 5 / 1440 AND " + (5 min b4)
//                        "rate > 0 " +
//                    "ORDER BY " +
//                        "rate_time";
//
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, marketId);
//            if (null != after) {
//                pstmt.setTimestamp(2, new Timestamp(after.getTime()));
//            }
//            rs = pstmt.executeQuery();
//            MarketRate mr = null;
//            while (rs.next()) {
//                mr = new MarketRate();
//                mr.setId(rs.getLong("id"));
//                mr.setMarketId(marketId);
//                mr.setRate(rs.getBigDecimal("rate"));
//                mr.setRateTime(new Date(rs.getTimestamp("rate_time").getTime()));
//                mr.setGraphRate(rs.getBigDecimal("graph_rate"));
//                mr.setDayRate(rs.getBigDecimal("day_rate"));
//                mr.setGraphRateAO(rs.getBigDecimal("graph_rate_ao"));
//                mr.setDayRateAO(rs.getBigDecimal("day_rate_ao"));
//                l.add(mr);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return l;
//    }
//}