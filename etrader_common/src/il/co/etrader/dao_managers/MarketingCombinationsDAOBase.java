package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.bl_vos.MarketingSource;


public class MarketingCombinationsDAOBase extends DAOBase {


	  /**
	   * Get by id
	   * @param con
	   * @param id
	   * @return
	   * @throws SQLException
	   */
	   public static MarketingCombination getById(Connection con,long id) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  MarketingCombination vo = null;
		  try {
			  String sql =
					  " SELECT " + 
				      " 	mco.*, " +
				      "     mca.name as campaign_name, " +
				      "     mcon.name content_name, " +
				      "     mloc.location, " +
				      "     mca.priority, " +
				      "     mca.description, " +
				      "     mca.source_id, " +
				      "     mca.payment_recipient_id, " +
				      "     msources.name source_name, " +
				      "     mm.name medium_name " +
				      " FROM " +
				      "     marketing_campaigns mca " +
				      "	    	LEFT JOIN marketing_sources msources ON mca.source_id = msources.id " +
				      "      	LEFT JOIN marketing_combinations mco ON mco.campaign_id = mca.id " +
				      "         	LEFT JOIN marketing_mediums mm  ON mco.medium_id = mm.id " +
				      "             LEFT JOIN marketing_contents mcon ON mco.content_id = mcon.id " +
				      "             LEFT JOIN marketing_locations mloc ON mco.location_id = mloc.id " +                              
				      " WHERE " +
				      "		mco.campaign_id = mca.id " +
				      "		AND mco.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs = ps.executeQuery();

				if (rs.next()) {
					vo = getVO(rs);
					vo.setCampaignName(rs.getString("campaign_name"));
					vo.setContentName(rs.getString("content_name"));
					vo.setLocation(rs.getString("location"));
					vo.setCampaignPriority(rs.getInt("priority"));
					String  description = rs.getString("DESCRIPTION");
					if (null != description) {
						description = description.replace("\"", "\'");
					}
					vo.setCampaignDesc(description);
					vo.setPaymentRecipientId(rs.getLong("source_id"));
					vo.setSourceId(rs.getLong("payment_recipient_id"));
					vo.setSourceName(rs.getString("source_name"));
					vo.setMediumName(rs.getString("medium_name"));
				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return vo;
	  }


	  /**
	   * Get combination id by ad id
	   * @param con db connection
	   * @param adId ad id
	   * @return  combination id
	   * @throws SQLException
	   */
	   public static long getCombinationIdByAd(Connection con, long adId) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long combId = 0;

		  try {
			    String sql = "select combination_id " +
			    			 "from ad_combination_map " +
			    			 "where ad_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, adId);

				rs = ps.executeQuery();

				if (rs.next()) {
					combId = rs.getLong("combination_id");
				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return combId;
	   }

	  /**
	   * Get combination instance by id
	   * @param con db connection
	   * @param combId combination id
	   * @return  combination instance
	   * @throws SQLException
	   */
	   public static MarketingCombination getCombinationById(Connection con, long combId) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingCombination com = new MarketingCombination();

		  try {
			    String sql = "select mc.*, mca.payment_type_id, mca.name as campaign_name  " +
			    			 "from marketing_combinations mc, marketing_campaigns mca, marketing_payment_types mpt " +
			    			 "where mc.campaign_id = mca.id and mca.payment_type_id = mpt.id " +
			    			 "and mc.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, combId);

				rs = ps.executeQuery();

				if (rs.next()) {
					com = getVO(rs);
					com.setPaymentType(rs.getLong("payment_type_id"));
					com.setCampaignName(rs.getString("campaign_name"));
					com.setCombPixels(MarketingPixelsDAOBase.getPixelsByCombId(con, com.getId()));
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return com;
	   }

	   
	   public static MarketingSource getMarketingSourceByCombinationId(Connection conn, long combId) throws SQLException{//returns marketing source by combination id
			  MarketingSource ms = null;
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  try{
				  String sql = " SELECT ms.* " +
			  
			  			   " FROM " +
			  			   	" marketing_sources ms, marketing_combinations mco, marketing_campaigns mca " +
			  			   " WHERE " +
			  			   	" ms.id = mca.source_id AND " +
			  			   	" mca.id = mco.campaign_id AND " +
			  			   	"  mco.id = ? ";
				  ps = conn.prepareStatement(sql);
				  ps.setLong(1, combId);
				  
				  rs = ps.executeQuery();
				  if(rs.next()){
					  ms = new MarketingSource();
					  ms.setId(rs.getLong("id"));
					  ms.setName(rs.getString("name"));
					  ms.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
					  ms.setWriterId(rs.getLong("writer_id"));
					  ms.setSpecialMail(rs.getLong("is_special_mail") ==  0 ? false : true);
				  }
		
			  } finally {
				  closeResultSet(rs);
				  closeStatement(ps);
			  }
			  return ms;
		  }	 
	   	   
	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingCombination object
	   * @throws SQLException
	   */
	  protected static MarketingCombination getVO(ResultSet rs) throws SQLException {

		  MarketingCombination vo = new MarketingCombination();

			vo.setId(rs.getLong("id"));
			vo.setSkinId(rs.getLong("skin_id"));
			vo.setCampaignId(rs.getLong("campaign_id"));
			vo.setMediumId(rs.getLong("medium_id"));
			vo.setContentId(rs.getLong("content_id"));
			vo.setSizeId(rs.getLong("size_id"));
			vo.setTypeId(rs.getLong("type_id"));
			vo.setLocationId(rs.getLong("location_id"));
			vo.setLandingPageId(rs.getLong("landing_page_id"));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setUrlSourceTypeId(rs.getLong("marketing_url_source_type_id"));
			vo.setDynamicParam(rs.getString("dynamic_param"));

			return vo;
	  }
	  
	  public static String getRemarketing(Connection con,long id) throws SQLException {

          PreparedStatement ps = null;
          ResultSet rs = null;
          String ret=" ";

          MarketingCombination vo = null;
          try {
              String sql =
                      " select 'Created '||rlat.name||' on '||to_char(time_created, 'dd/mm/yyyy hh24:mi:ss')||' following remarketing.' rem " +
                      " from " +
                      "         REMARKETING_LOGINS rl, REMARKETING_LOGINS_ACTION_TYPE rlat " +
                      " where " +
                      "       rl.action_type=rlat.id " +
                      "       and rl.user_id=? " +
                      " order by time_created desc ";

                ps = con.prepareStatement(sql);
                ps.setLong(1, id);

                rs = ps.executeQuery();

                if (rs.next()) {
                    ret = rs.getString("rem");
                }
            }

            finally {
                closeResultSet(rs);
                closeStatement(ps);
            }

            return ret;
      }
	  
	  public static ArrayList<SelectItem> getAllUrlSoursecTypes(Connection con) throws SQLException {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
	        try{
	            String sql =
	            	"SELECT * " +
	            	"FROM MARKETING_URL_SOURCE_TYPES " +
	            	"ORDER BY ID";
	            ps = con.prepareStatement(sql);
	            rs = ps.executeQuery();
	            while ( rs.next() ) {
	                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
	            }
	        } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	        }
	        return hm;
	    }
	  
	  /**
	   * Get combination instance by user id
	   * @param con db connection
	   * @param userId user id
	   * @return  combination instance
	   * @throws SQLException
	   */
	   public static MarketingCombination getCombinationByUserId(Connection con, long userId) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingCombination com = new MarketingCombination();

		  try {
			    String sql = "SELECT "
			    				+ "mc.*, "
			    				+ "mca.payment_type_id, "
			    				+ "mca.name as campaign_name  " +
			    			 "FROM "
			    			 	+ "marketing_combinations mc, "
			    			 	+ "marketing_campaigns mca, "
			    			 	+ "marketing_payment_types mpt, "
			    			 	+ "users u " +
			    			 "WHERE "
			    			 	+ "mc.campaign_id = mca.id "
			    			 	+ "AND mca.payment_type_id = mpt.id "
			    			 	+ "AND mc.id = u.combination_id "
			    			 	+ "AND u.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs = ps.executeQuery();

				if (rs.next()) {
					com = getVO(rs);
					com.setPaymentType(rs.getLong("payment_type_id"));
					com.setCampaignName(rs.getString("campaign_name"));
					com.setCombPixels(MarketingPixelsDAOBase.getPixelsByCombId(con, com.getId()));
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return com;
	   }
}

