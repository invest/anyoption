package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class MarketsDAOBase extends DAOBase {
	private static final Logger log = Logger.getLogger(MarketsDAOBase.class);

	public static void insertGroup(Connection con, MarketGroup vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "insert into market_groups(id,name,display_name,time_created," + "writer_id) "
					+ " values(seq_market_groups.nextval,?,?,?,?) ";

			ps = con.prepareStatement(sql);

			ps.setString(1, vo.getName());
			ps.setString(2, vo.getDisplayName());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
			ps.setLong(4, vo.getWriterId());
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con, "seq_market_groups"));
		} finally {
			closeStatement(ps);
		}
	}

	public static void insert(Connection con, Market vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " insert into markets(id,name,display_name,time_created,"
					+ " feed_name,writer_id,decimal_point,EXCHANGE_ID,"
					+ " INVESTMENT_LIMITS_GROUP_ID, UPDATES_PAUSE, SIGNIFICANT_PERCENTAGE,"
					+ " REAL_UPDATES_PAUSE, REAL_SIGNIFICANT_PERCENTAGE, "
					+ " RANDOM_CEILING,RANDOM_FLOOR, FIRST_SHIFT_PARAMETER," + " PERCENTAGE_OF_AVG_INVESTMENTS,"
					+ " FIXED_AMOUNT_FOR_SHIFTING," + " TYPE_OF_SHIFTING,disable_after_period,CURENT_LEVEL_ALERT_PERC,"
					+ " ACCEPTABLE_LEVEL_DEVIATION,IS_HAS_FUTURE_AGREEMENT,"
					+ "IS_NEXT_AGREEMENT_SAME_DAY,SEC_BETWEEN_INVESTMENTS,SEC_FOR_DEV2,AMOUNT_FOR_DEV2,MARKET_GROUP_ID,MAX_EXPOSURE, AVERAGE_INVESTMENTS, "
					+ "INSURANCE_PREMIA_PERCENT, " + "INSURANCE_QUALIFY_PERCENT, " + "ROLL_UP_QUALIFY_PERCENT, "
					+ "IS_ROLL_UP, " + "IS_GOLDEN_MINUTES, " + "ROLL_UP_PREMIA_PERCENT, "
					+ "ACCEPTABLE_LEVEL_DEVIATION_3, " + "SEC_BETWEEN_INVESTMENTS_IP, " + "AMOUNT_FOR_DEV3, "
					+ "DECIMAL_POINT_SUBTRACT_DIGITS, " + "AMOUNT_FOR_DEV2_NIGHT, " + "EXPOSURE_REACHED_AMOUNT, "
					+ "RAISE_EXPOSURE_UP_AMOUNT, " + "RAISE_EXPOSURE_DOWN_AMOUNT, " + "RAISE_CALL_LOWER_PUT_PERCENT, "
					+ "RAISE_PUT_LOWER_CALL_PERCENT, " + "use_for_fake_investments, " + "worst_case_return, "
					+ "LIMITED_HIGH_AMNT, " + "LIMITED_FROM_AMNT ) "
					+ " values(seq_markets.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

			ps = con.prepareStatement(sql);

			ps.setString(1, vo.getName());
			ps.setString(2, vo.getDisplayName());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
			ps.setString(4, vo.getFeedName());
			ps.setLong(5, vo.getWriterId());
			ps.setLong(6, vo.getDecimalPoint().longValue());
			ps.setLong(7, vo.getExchangeId());
			ps.setLong(8, vo.getInvestmentLimitsGroupId());
			ps.setInt(9, vo.getUpdatesPause());
			ps.setBigDecimal(10, vo.getSignificantPercentage());
			ps.setInt(11, vo.getRealUpdatesPause());
			ps.setBigDecimal(12, vo.getRealSignificantPercentage());
			ps.setBigDecimal(13, vo.getRandomCeiling());
			ps.setBigDecimal(14, vo.getRandomFloor());
			ps.setBigDecimal(15, vo.getFirstShiftParameter());
			ps.setBigDecimal(16, vo.getPercentageOfAverageInvestments());
			ps.setBigDecimal(17, vo.getFixedAmountForShifting());
			ps.setLong(18, vo.getTypeOfShifting());
			ps.setLong(19, vo.getDisableAfterPeriod());
			ps.setBigDecimal(20, vo.getCurrentLevelAlertPerc());
			ps.setBigDecimal(21, vo.getAcceptableDeviation());
			ps.setInt(22, vo.isHasFutureAgreement() ? 1 : 0);
			ps.setInt(23, vo.isNextAgreementSameDay() ? 1 : 0);
			// ps.setInt(24,vo.getBannersPriority());
			ps.setInt(24, vo.getSecBetweenInvestments());
			ps.setInt(25, vo.getSecForDev2());
			ps.setFloat(26, vo.getAmountForDev2());
			ps.setInt(27, 6); // market_group_id field - it is not not used
								// anymore
			ps.setInt(28, vo.getMax_exposure());
			ps.setBigDecimal(29, vo.getAverageInvestments());
			ps.setDouble(30, vo.getInsurancePremiaPercent());
			ps.setDouble(31, vo.getInsuranceQualifyPercent());
			ps.setDouble(32, vo.getRollUpQualifyPercent());
			ps.setInt(33, vo.isRollUp() ? 1 : 0);
			ps.setInt(34, vo.isGoldenMinutes() ? 1 : 0);
			ps.setDouble(35, vo.getRollUpPremiaPercent());
			ps.setBigDecimal(36, vo.getAcceptableDeviation3());
			ps.setInt(37, vo.getSecBetweenInvestmentsSameIp());
			ps.setFloat(38, vo.getAmountForDev3());
			ps.setInt(39, vo.getDecimalPointSubtractDigits());
			ps.setFloat(40, vo.getAmountForDev2Night());
			ps.setLong(41, vo.getExposureReached() * 100);
			ps.setLong(42, vo.getRaiseExposureUpAmount() * 100);
			ps.setLong(43, vo.getRaiseExposureDownAmount() * 100);
			ps.setDouble(44, vo.getUpStrikeRaiseLowerPercent());
			ps.setDouble(45, vo.getDownStrikeRaiseLowerPercent());
			ps.setInt(46, vo.isLiveFakeInvestments() ? 1 : 0);
			ps.setLong(47, vo.getWorstCaseReturn());
			ps.setBoolean(48, vo.isLimitedForHighAmounts());
			ps.setLong(49, vo.getLimitedFromAmount() * 100);
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con, "seq_markets"));
		} finally {
			closeStatement(ps);
		}
	}

	public static void updateGroup(Connection con, MarketGroup vo) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql = "update market_groups set name=?," + " display_name=?," + "time_created=?"
					+ ",writer_id=? where id=?";

			ps = con.prepareStatement(sql);

			ps.setString(1, vo.getName());
			ps.setString(2, vo.getDisplayName());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
			ps.setLong(4, vo.getWriterId());

			ps.setLong(5, vo.getId());

			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void update(Connection con, Market vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE " + "markets " + "SET " + "name=?, display_name=?, "
					+ "time_created=?,feed_name=?,writer_id=?,decimal_point=?, "
					+ " exchange_id=?, investment_limits_group_id=?, updates_pause=?, "
					+ " significant_percentage=?, real_updates_pause=?, real_significant_percentage=?, "
					+ " random_ceiling=?,random_floor=?, first_shift_parameter=?, percentage_of_avg_investments=?, "
					+ " fixed_amount_for_shifting=?, type_of_shifting=?,disable_after_period=?,curent_level_alert_perc=?, "
					+ " acceptable_level_deviation=?,is_has_future_agreement=?,is_next_agreement_same_day=?, "
					+ " sec_between_investments=? ,sec_for_dev2=? ,amount_for_dev2=?,max_exposure=?, average_investments = ?, "
					+ "insurance_premia_percent = ?, " + "insurance_qualify_percent = ?, "
					+ "roll_up_qualify_percent = ?, " + "is_roll_up = ?, " + "is_golden_minutes = ?, "
					+ "roll_up_premia_percent = ?, " + "acceptable_level_deviation_3 = ?, "
					+ "sec_between_investments_ip = ?, " + "amount_for_dev3 = ?, "
					+ "decimal_point_subtract_digits = ?, " + "amount_for_dev2_night = ?, "
					+ " EXPOSURE_REACHED_AMOUNT = ?, " + " RAISE_EXPOSURE_UP_AMOUNT = ?, "
					+ " RAISE_EXPOSURE_DOWN_AMOUNT = ?, " + " RAISE_CALL_LOWER_PUT_PERCENT = ?, "
					+ " RAISE_PUT_LOWER_CALL_PERCENT = ? ," + " use_for_fake_investments = ?, "
					+ " worst_case_return = ?, " + " bubbles_pricing_level = ?, " + " INSTANT_SPREAD = ?, "
					+ " LIMITED_HIGH_AMNT = ?, " + " LIMITED_FROM_AMNT = ?, " + " QUOTE_PARAMS = ? " + "WHERE "
					+ "id = ? ";

			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getName());
			ps.setString(2, vo.getDisplayName());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
			ps.setString(4, vo.getFeedName());
			ps.setLong(5, vo.getWriterId());
			ps.setLong(6, vo.getDecimalPoint().longValue());
			// ps.setLong(7, vo.getGroupId().longValue());
			ps.setLong(7, vo.getExchangeId());
			ps.setLong(8, vo.getInvestmentLimitsGroupId());
			// ps.setInt(10, vo.getHomePagePriority());
			// ps.setInt(11, vo.isHomePageHourFirst() ? 1 : 0);
			// ps.setInt(12, vo.getGroupPriority());
			ps.setInt(9, vo.getUpdatesPause());
			ps.setBigDecimal(10, vo.getSignificantPercentage());
			ps.setInt(11, vo.getRealUpdatesPause());
			ps.setBigDecimal(12, vo.getRealSignificantPercentage());
			ps.setBigDecimal(13, vo.getRandomCeiling());
			ps.setBigDecimal(14, vo.getRandomFloor());
			ps.setBigDecimal(15, vo.getFirstShiftParameter());
			ps.setBigDecimal(16, vo.getPercentageOfAverageInvestments());
			ps.setBigDecimal(17, vo.getFixedAmountForShifting());
			ps.setLong(18, vo.getTypeOfShifting());
			ps.setLong(19, vo.getDisableAfterPeriod());
			ps.setBigDecimal(20, vo.getCurrentLevelAlertPerc());
			ps.setBigDecimal(21, vo.getAcceptableDeviation());
			ps.setInt(22, vo.isHasFutureAgreement() ? 1 : 0);
			ps.setInt(23, vo.isNextAgreementSameDay() ? 1 : 0);
			// ps.setInt(24, vo.getBannersPriority());
			ps.setInt(24, vo.getSecBetweenInvestments());
			ps.setInt(25, vo.getSecForDev2());
			ps.setFloat(26, vo.getAmountForDev2());
			ps.setInt(27, vo.getMax_exposure());
			ps.setBigDecimal(28, vo.getAverageInvestments());
			ps.setDouble(29, vo.getInsurancePremiaPercent());
			ps.setDouble(30, vo.getInsuranceQualifyPercent());
			ps.setDouble(31, vo.getRollUpQualifyPercent());
			ps.setInt(32, vo.isRollUp() ? 1 : 0);
			ps.setInt(33, vo.isGoldenMinutes() ? 1 : 0);
			ps.setDouble(34, vo.getRollUpPremiaPercent());
			ps.setBigDecimal(35, vo.getAcceptableDeviation3());
			ps.setInt(36, vo.getSecBetweenInvestmentsSameIp());
			ps.setFloat(37, vo.getAmountForDev3());
			ps.setInt(38, vo.getDecimalPointSubtractDigits());
			ps.setFloat(39, vo.getAmountForDev2Night());
			ps.setLong(40, vo.getExposureReached() * 100);
			ps.setLong(41, vo.getRaiseExposureUpAmount() * 100);
			ps.setLong(42, vo.getRaiseExposureDownAmount() * 100);
			ps.setDouble(43, vo.getUpStrikeRaiseLowerPercent());
			ps.setDouble(44, vo.getDownStrikeRaiseLowerPercent());
			ps.setInt(45, vo.isLiveFakeInvestments() ? 1 : 0);
			ps.setLong(46, vo.getWorstCaseReturn());
			ps.setInt(47, vo.getBubblesPricingLevel());
			ps.setDouble(48, vo.getInstantSpread());
			ps.setBoolean(49, vo.isLimitedForHighAmounts());
			ps.setLong(50, vo.getLimitedFromAmount() * 100);
			ps.setString(51, vo.getQuoteParams());
			ps.setLong(52, vo.getId());

			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	protected static MarketGroup getGroupVO(Connection con, ResultSet rs) throws SQLException {
		MarketGroup vo = new MarketGroup();
		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("name"));
		vo.setDisplayName(rs.getString("display_name"));
		vo.setDisplayNameKey(rs.getString("display_name"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setUrl(rs.getString("url"));
		vo.setWriterId(rs.getLong("writer_id"));

		return vo;
	}

	protected static Market getMarketVOShort(Connection con, ResultSet rs) throws SQLException {
		Market vo = new Market();
		vo.setId(rs.getLong("id"));
		vo.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
		vo.setDisplayNameKey(rs.getString("display_name"));
		return vo;
	}

	protected static Market getMarketVO(Connection con, ResultSet rs) throws SQLException {
		return getMarketVO(con, rs, false);
	}

	protected static Market getMarketVO(Connection con, ResultSet rs, boolean forBackend) throws SQLException {
		Market vo = new Market();
		vo.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
		// vo.setDisplayName(rs.getString("display_name"));
		if (forBackend) {
			vo.setDisplayNameKey(
					com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(String.valueOf(rs.getLong("id")),
							MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("id")),
							rs.getString("display_name")));
		} else {
			vo.setDisplayNameKey(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("id")));
		}
		vo.setFeedName(rs.getString("feed_Name"));
		// vo.setGroupId(new Long(rs.getLong("market_group_Id")));
		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("name"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setWriterId(rs.getLong("writer_id"));

		vo.setExchangeId(rs.getLong("exchange_Id"));
		vo.setInvestmentLimitsGroupId(rs.getLong("INVESTMENT_LIMITS_GROUP_ID"));
		// vo.setHomePagePriority(rs.getInt("HOME_PAGE_PRIORITY"));
		// vo.setHomePageHourFirst(rs.getInt("HOME_PAGE_HOUR_FIRST") == 1);
		// vo.setGroupPriority(rs.getInt("group_priority"));

		vo.setUpdatesPause(rs.getInt("UPDATES_PAUSE"));
		vo.setSignificantPercentage(rs.getBigDecimal("SIGNIFICANT_PERCENTAGE"));
		vo.setRealSignificantPercentage(rs.getBigDecimal("REAL_SIGNIFICANT_PERCENTAGE"));
		vo.setRealUpdatesPause(rs.getInt("REAL_UPDATES_PAUSE"));
		vo.setRandomCeiling(rs.getBigDecimal("RANDOM_CEILING"));
		vo.setRandomFloor(rs.getBigDecimal("RANDOM_FLOOR"));
		vo.setFirstShiftParameter(rs.getBigDecimal("FIRST_SHIFT_PARAMETER"));
		vo.setAverageInvestments(rs.getBigDecimal("AVERAGE_INVESTMENTS"));
		vo.setPercentageOfAverageInvestments(rs.getBigDecimal("PERCENTAGE_OF_AVG_INVESTMENTS"));
		vo.setFixedAmountForShifting(rs.getBigDecimal("FIXED_AMOUNT_FOR_SHIFTING"));
		vo.setTypeOfShifting(rs.getLong("TYPE_OF_SHIFTING"));
		vo.setDisableAfterPeriod(rs.getLong("disable_after_period"));

		vo.setCurrentLevelAlertPerc(rs.getBigDecimal("CURENT_LEVEL_ALERT_PERC"));
		vo.setAcceptableDeviation(rs.getBigDecimal("ACCEPTABLE_LEVEL_DEVIATION"));
		vo.setAcceptableDeviation3(rs.getBigDecimal("ACCEPTABLE_LEVEL_DEVIATION_3"));

		vo.setHasFutureAgreement(rs.getInt("IS_HAS_FUTURE_AGREEMENT") == 1);
		vo.setNextAgreementSameDay(rs.getInt("IS_NEXT_AGREEMENT_SAME_DAY") == 1);

		// vo.setBannersPriority(rs.getInt("BANNERS_PRIORITY"));
		vo.setAmountForDev2(rs.getFloat("AMOUNT_FOR_DEV2"));
		vo.setSecForDev2(rs.getInt("SEC_FOR_DEV2"));
		vo.setSecBetweenInvestments(rs.getInt("SEC_BETWEEN_INVESTMENTS"));

		vo.setMax_exposure(rs.getInt("MAX_EXPOSURE"));
		// add FREQUENCE_PRIORITY for bugId: 2029
		// vo.setFrequencePriority(rs.getInt("FREQUENCE_PRIORITY"));
		vo.setInsurancePremiaPercent(rs.getDouble("Insurance_Premia_Percent"));
		vo.setInsuranceQualifyPercent(rs.getDouble("insurance_qualify_percent"));
		vo.setRollUpQualifyPercent(rs.getDouble("roll_up_qualify_percent"));
		vo.setRollUp(rs.getInt("is_roll_up") == 1 ? true : false);
		vo.setGoldenMinutes((rs.getInt("is_golden_minutes")) == 1 ? true : false);
		vo.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
		vo.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
		vo.setSecBetweenInvestmentsSameIp(rs.getInt("sec_between_investments_ip"));
		vo.setAmountForDev3(rs.getFloat("amount_for_dev3"));
		vo.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
		vo.setAmountForDev2Night(rs.getFloat("amount_for_dev2_night"));

		vo.setExposureReached(rs.getLong("exposure_reached_amount") / 100);
		vo.setRaiseExposureUpAmount(rs.getLong("raise_exposure_up_amount") / 100);
		vo.setRaiseExposureDownAmount(rs.getLong("raise_exposure_down_amount") / 100);
		vo.setUpStrikeRaiseLowerPercent(rs.getDouble("RAISE_CALL_LOWER_PUT_PERCENT"));
		vo.setDownStrikeRaiseLowerPercent(rs.getDouble("RAISE_PUT_LOWER_CALL_PERCENT"));
		vo.setWorstCaseReturn(rs.getLong("WORST_CASE_RETURN"));
		vo.setBubblesPricingLevel(rs.getInt("bubbles_pricing_level"));
		vo.setInstantSpread(rs.getDouble("INSTANT_SPREAD"));
		vo.setLimitedForHighAmounts(rs.getBoolean("LIMITED_HIGH_AMNT"));
		vo.setLimitedFromAmount(rs.getLong("LIMITED_FROM_AMNT") / 100);
		vo.setQuoteParams(rs.getString("quote_params"));
		vo.setTypeId(rs.getLong("type_id"));

		return vo;
	}

	public static ArrayList<Market> getMarkets(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Market> list = new ArrayList<Market>();
		try {
			String sql = "select m.*, ot.TIME_FIRST_INVEST, ot.TIME_EST_CLOSING "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=4 and ot1.is_active=1 )  as monthly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=3 and ot1.is_active=1 )  as weekly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=2 and ot1.is_active=1 )  as dayly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=1 and ot1.is_active=1 )  as hourly "
					+ "from markets m, opportunity_templates ot "
					+ "where ot.scheduled=2 and ot.is_active=1 " 
					+ " and (ot.sun_f + ot.mon_f + ot.tue_f + ot.wed_f + ot.thu_f + ot.fri_f + ot.sat_f) > 0 "
					+ " and m.id = ot.market_id "
					+ "order by m.id";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Market temp = getMarketVO(con, rs);
				temp.setTimeFirstInvest(rs.getString("TIME_FIRST_INVEST"));
				temp.setTimeEstClosing(rs.getString("TIME_EST_CLOSING"));
				temp.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
				temp.setHaveDaily(rs.getInt("dayly") == 0 ? false : true);
				temp.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
				temp.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);

				list.add(temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Load all markets that have active templates for select box options
	 * grouped by market groups.
	 *
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
//	public static ArrayList<SelectItem> getMarketsForHomepageSelect(Connection conn, long skinId) throws SQLException {
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		ArrayList<SelectItem> l = new ArrayList<SelectItem>();
//		try {
//			String sql = "SELECT " + "sm.home_page_priority, " + "sg.market_group_id, " + "sm.group_priority, "
//					+ "A.display_name, " + "B.display_name AS group_display_name " + "FROM " + "markets A, "
//					+ "market_groups B, " + "skin_market_group_markets sm, " + "skin_market_groups sg " + "WHERE "
//					+ "sg.market_group_id = B.id AND "
//					+ "EXISTS(SELECT 1 FROM opportunity_templates WHERE market_id = A.id AND is_active = 1 AND opportunity_type_id = 1) "
//					+ "and sm.market_id = A.id " + "and sm.skin_market_group_id = sg.id " + "and sg.skin_id = ? "
//					+ "ORDER BY " + "sg.market_group_id, " + "A.id";
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setLong(1, skinId);
//			rs = pstmt.executeQuery();
//			long prevGroupId = -1;
//			SelectItemGroup group = null;
//			ArrayList<SelectItem> items = null;
//
//			// logger.debug("cant load locale!");
//
//			while (rs.next()) {
//				long groupId = rs.getLong("market_group_id");
//				if (groupId != prevGroupId) {
//					if (null != group && null != items) {
//						Collections.sort(items, new SelectItemComparatorHebrewPriority());
//						SelectItem[] si = new SelectItem[items.size()];
//						for (int i = 0; i < si.length; i++) {
//							si[i] = items.get(i);
//						}
//						group.setSelectItems(si);
//					}
//					String groupName = CommonUtil.getMessage(rs.getString("group_display_name"), null);
//					group = new SelectItemGroup(groupName);
//					l.add(group);
//					prevGroupId = groupId;
//					items = new ArrayList<SelectItem>();
//					items.add(new SelectItem("100_" + groupId + "_0", groupName));
//				}
//				items.add(new SelectItem(
//						rs.getString("home_page_priority") + "_" + String.valueOf(groupId) + "_"
//								+ rs.getString("group_priority"),
//						CommonUtil.getMessage(rs.getString("display_name"), null)));
//			}
//			if (null != group && null != items) {
//				Collections.sort(items, new SelectItemComparatorHebrewPriority());
//				SelectItem[] si = new SelectItem[items.size()];
//				for (int i = 0; i < si.length; i++) {
//					si[i] = items.get(i);
//				}
//				group.setSelectItems(si);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(pstmt);
//		}
//		return l;
//	}

	public static Market get(Connection con, long id, boolean realValues) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select * from markets where id=" + id;
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				Market m = getMarketVO(con, rs);
				if (realValues) {
					m.setDisplayName(rs.getString("display_name"));
				}
				return m;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}

	public static long getDecimalPointsByOpportunityId(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " select m.decimal_point from markets m,opportunities o where o.market_id=m.id and o.id=" + id;
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("decimal_point");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

	public static ArrayList<SelectItem> getGroupsSelectItems(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = "select * from market_groups ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("display_name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static HashMap<Long, MarketGroup> getMarketGroups(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, MarketGroup> hm = new HashMap<Long, MarketGroup>();
		try {
			String sql = "select * from market_groups ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				hm.put(rs.getLong("id"), getGroupVO(con, rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	/*
	 * @param connection to the db
	 *
	 * return returns all the markets groups
	 */
	public static ArrayList<MarketGroup> getMarketsGroups(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<MarketGroup> list = new ArrayList<MarketGroup>();
		try {
			String sql = "select * from market_groups ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getGroupVO(con, rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static ArrayList<Market> getAssetIndex(Connection con, long skinId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Market> list = new ArrayList<Market>();
		try {
			String sql = "SELECT " + "DISTINCT " + "m.*, " + "ot.time_zone, "
					+ "( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=4 and ot1.is_active=1 )  as monthly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=3 and ot1.is_active=1 )  as weekly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=2 and ot1.is_active=1 )  as dayly "
					+ ",( select count(scheduled) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled=1 and ot1.is_active=1 )  as hourly, "
					+ "sm.market_group_id, " + "mg.display_name as group_name, "
					+ "mdg.display_name as display_group_name_key, " + "(SELECT " + "count(1) " + "FROM "
					+ "skin_market_group_markets smgm2, " + "opportunity_templates ot1 " + "WHERE "
					+ "smgm2.skin_market_group_id = smg.skin_market_group_id AND "
					+ "smgm2.market_id = ot1.market_id AND " + "ot1.scheduled = 2 AND " + "ot1.is_active = 1 AND "
					+ "(ot1.sun_f + ot1.mon_f + ot1.tue_f + ot1.wed_f + ot1.thu_f + ot1.fri_f + ot1.sat_f) > 0 " 
					+ ") AS group_markets_counter, " + "mns.name, "
					+ "m.option_plus_market_id " + "FROM " + "markets m, " + "opportunity_templates ot, "
					+ "skin_market_groups sm, " + "skin_market_group_markets smg "
					+ "LEFT JOIN market_display_groups mdg ON smg.skin_display_group_id = mdg.id, "
					+ "market_groups mg, " + "market_name_skin mns " + "WHERE "
					+ "(ot.scheduled = 2 OR ot.opportunity_type_id = 3 OR ot.opportunity_type_id in (4,5)) AND "
					+ "ot.is_active = 1 AND " + "m.id = ot.market_id AND "
					+ "sm.id = smg.skin_market_group_id AND " + "sm.skin_id = ? AND " + "smg.market_id = m.id AND "
					+ "mg.id = sm.market_group_id AND " + "mns.skin_id = sm.skin_id AND " + "mns.market_id = m.id "
					+ "ORDER BY " + "UPPER(mns.name)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Market temp = getMarketVO(con, rs);
				String feedName = temp.getFeedName();
				if (feedName.equals("NQ") || feedName.equals("TRF30") || feedName.equals("FKLI")
						|| feedName.equals(".KLSE") || feedName.equals("SPX")) {
					feedName += "c1";
					temp.setFeedName(feedName);
				} else if (feedName.equals("SI") || feedName.equals("GC") || feedName.equals("CL")
						|| feedName.equals("HG")) {
					feedName += "v1";
					temp.setFeedName(feedName);
				}
				temp.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
				temp.setHaveDaily(rs.getInt("dayly") == 0 ? false : true);
				temp.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
				temp.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);
				temp.setTimeZone(rs.getString("time_zone"));
				temp.setGroupId(rs.getLong("market_group_id"));
				temp.setGroupName(rs.getString("group_name"));
				temp.setDisplayGroupNameKey(rs.getString("display_group_name_key"));
				temp.setGroupMarketsCounter(rs.getInt("group_markets_counter"));
				temp.setOptionPlus(rs.getInt("option_plus_market_id") > 0 ? true : false);
				list.add(temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	public static ArrayList<ArrayList<Market>> getBankOptionMarkets(Connection con, long skinId) throws SQLException {
		long currMarketGroupId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Market> list = new ArrayList<Market>();
		HashMap<Long, ArrayList<Market>> hmMarkets = new HashMap<Long, ArrayList<Market>>();
		ArrayList<ArrayList<Market>> marketsDivGroups = new ArrayList<ArrayList<Market>>();
		try {
			String sql = "SELECT " + "m.display_name, " + "mg.display_name as group_name, " + "sm.market_group_id, "
					+ "m.id, " + "m.time_created, " + "ai.is_24_7 " + "FROM " + "asset_index ai, " + "markets m, "
					+ "skin_market_groups sm, " + "skin_market_group_markets smg, " + "market_groups mg, "
					+ "market_name_skin mns " + "WHERE "
					+ "EXISTS (SELECT 1 FROM opportunity_templates ot WHERE ot.market_id = m.id AND ot.is_active = 1 AND ot.opportunity_type_id = 1) AND "
					+ "sm.id = smg.skin_market_group_id AND " + "sm.skin_id = ? AND " + "smg.market_id = m.id AND "
					+ "mg.id = sm.market_group_id AND " + "mns.skin_id = sm.skin_id AND " + "mns.market_id = m.id AND "
					+ "m.option_plus_market_id is null AND " + "m.id = ai.market_id " + "ORDER BY "
					+ "m.market_group_id, UPPER(mns.name) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while (rs.next()) {
				Market tempMarket = new Market();
				tempMarket.setDisplayNameKey(MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
				tempMarket.setGroupId(rs.getLong("market_group_id"));
				tempMarket.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				tempMarket.setTwentyFourSeven(rs.getBoolean("is_24_7"));

				// Set to market is market group name only for the first market.
				if (currMarketGroupId != tempMarket.getGroupId()) {
					tempMarket.setGroupName(rs.getString("group_name"));
					if (list.size() > 0) {
						hmMarkets.put(currMarketGroupId, list);
						list = new ArrayList<Market>();
					}
					currMarketGroupId = tempMarket.getGroupId();
				}
				tempMarket.setId(rs.getLong("id"));
				list.add(tempMarket);

			}
			hmMarkets.put(list.get(0).getGroupId(), list);
			// change order.
			list = new ArrayList<Market>();
			if (hmMarkets.get(ConstantsBase.MARKET_GROUP_INDICES_LONG) != null) {
				list.addAll(hmMarkets.get(ConstantsBase.MARKET_GROUP_INDICES_LONG));
			}
			if (hmMarkets.get(ConstantsBase.MARKET_GROUP_CURRENCIES_LONG) != null) {
				list.addAll(hmMarkets.get(ConstantsBase.MARKET_GROUP_CURRENCIES_LONG));
			}
			if (hmMarkets.get(ConstantsBase.MARKET_GROUP_COMMODITIES_LONG) != null) {
				list.addAll(hmMarkets.get(ConstantsBase.MARKET_GROUP_COMMODITIES_LONG));
			}
			if (hmMarkets.get(ConstantsBase.MARKET_GROUP_STOCKS_LONG) != null) {
				list.addAll(hmMarkets.get(ConstantsBase.MARKET_GROUP_STOCKS_LONG));
			}
			if (hmMarkets.get(ConstantsBase.MARKET_GROUP_ABROAD_STOCKS_LONG) != null) {
				list.addAll(hmMarkets.get(ConstantsBase.MARKET_GROUP_ABROAD_STOCKS_LONG));
			}
			ArrayList<Market> tempList = new ArrayList<Market>();
			int ind = 0;
			int rowNum = 17;
			if (skinId == Skins.SKIN_ETRADER) {
				rowNum = 16;
			}
			// order in groups.
			boolean isFirstGroupName = true;
			for (Market val : list) {
				tempList.add(val);
				ind++;
				if (!CommonUtil.isParameterEmptyOrNull(val.getGroupName())) {
					ind++;
					if (!isFirstGroupName) {
						ind++;
					}
					isFirstGroupName = false;
				}
				if (ind >= rowNum) {
					marketsDivGroups.add(tempList);
					tempList = new ArrayList<Market>();
					ind = 0;
				}

			}
			marketsDivGroups.add(tempList);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return marketsDivGroups;
	}

	public static LinkedHashMap<Long, Market> getAllMarkets(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		LinkedHashMap<Long, Market> hm = new LinkedHashMap<Long, Market>();
		try {
			String sql = "select " + "m.*, "
					+ "(select count(1) from opportunity_templates ot1 where m.id = ot1.market_id and ot1.scheduled in (4,3) and ot1.is_active=1) as haslongterm "
					+ "from " + "markets m ";
			// "from markets m,skin_market_group_markets smgm,
			// skin_market_groups smg " +
			// "where m.id= smgm.market_id and smgm.skin_market_group_id=smg.id
			// and smg.skin_id="+ConstantsBase.SKIN_ETRADER +
			// " order by smg.id,m.id";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Market temp = getMarketVO(con, rs);
				temp.setHasLongTerm(rs.getInt("haslongterm") == 0 ? false : true);
				hm.put(rs.getLong("id"), temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	public static ArrayList<Market> getChartsUpdaterMarkets(Connection conn, long opportunityTypeId)
			throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Market> l = new ArrayList<Market>();
		try {
			String sql = " SELECT " + " x.*, " + " to_char(CASE WHEN x.time_first_invest is not null "
					+ " THEN x.time_first_invest "
					+ " ELSE x.time_est_closing END, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') last_hour_opening_time "
					+ " FROM " + " (SELECT " + " m.id, " + " m.display_name, " + " m.decimal_point, "
					+ " min(curr_opp.time_first_invest) time_first_invest, "
					+ " max(close_opp.time_est_closing) time_est_closing, "
					+ " CASE WHEN count (open_opp.id) > 0 THEN 1 ELSE 0 END opened, "
					+ " CASE WHEN count (opp_temp.id) > 0 THEN 1 ELSE 0 END hourly " + " FROM " + " markets m "
					+ " left join opportunities open_opp on open_opp.market_id = m.id "
					+ " and open_opp.is_published = 1 " + " and open_opp.time_est_closing > trunc(current_date - 1) "
					+ " and open_opp.opportunity_type_id = ? "
					+ " left join opportunities curr_opp on curr_opp.market_id = m.id  "
					+ " and curr_opp.scheduled = 1  " + " and curr_opp.is_published = 1 "
					+ "left join opportunities close_opp on close_opp.market_id = m.id "
					+ " and close_opp.time_act_closing > sysdate - 7 " + " and close_opp.opportunity_type_id = ? "
					+ "left join opportunity_templates opp_temp on opp_temp.market_id = m.id "
					+ " and opp_temp.scheduled = 1 " + " and opp_temp.is_active = 1 " + " WHERE "
					+ " EXISTS (SELECT id FROM opportunity_templates WHERE market_id = m.id AND opportunity_type_id = ? AND is_active = 1) "
					+ " GROUP BY " + " m.id, " + " m.display_name, " + " m.decimal_point) x ";

			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, opportunityTypeId);
			pstmt.setLong(2, opportunityTypeId);
			pstmt.setLong(3, opportunityTypeId);
			rs = pstmt.executeQuery();
			Market m = null;
			while (rs.next()) {
				m = getMarketVOShort(rs);
				try {
					m.setLastHourClosingTime(getTimeWithTimezone(rs.getString("last_hour_opening_time")));
					m.setOpened(rs.getBoolean("opened"));
					m.setHaveHourly(rs.getBoolean("hourly"));
				} catch (Exception e) {
					log.error("Can't parse last hour closing time.", e);
				}
				l.add(m);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return l;
	}

	protected static Market getMarketVOShort(ResultSet rs) throws SQLException {
		Market vo = new Market();
		vo.setId(rs.getLong("id"));
		vo.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
		vo.setDisplayNameKey(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("id")));
		return vo;
	}

	/**
	 * Collect the ids of all markets that are present in eTrader and not in
	 * AnyOption.
	 *
	 * @param conn
	 * @return <code>ArrayList<Long></code>
	 * @throws SQLException
	 */
	public static ArrayList<Long> getIsraelMarketsIds(Connection conn) throws SQLException {
		ArrayList<Long> l = new ArrayList<Long>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " + "A.id " + "FROM " + "markets A " + "WHERE " + "EXISTS( " + "SELECT " + "Y.id "
					+ "FROM " + "skin_market_groups X, " + "skin_market_group_markets Y " + "WHERE "
					+ "X.id = Y.skin_market_group_id AND " + "X.skin_id = 1 AND " + "Y.market_id = A.id) AND "
					+ "NOT EXISTS( " + "SELECT " + "Y.id " + "FROM " + "skin_market_groups X, "
					+ "skin_market_group_markets Y " + "WHERE " + "X.id = Y.skin_market_group_id AND "
					+ "X.skin_id != 1 AND " + "Y.market_id = A.id)";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				l.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return l;
	}

	/**
	 * get all the option + markets with active templates
	 * 
	 * @param conn
	 * @param skinId
	 * @return
	 * @throws SQLException
	 */
	public static LinkedHashMap<Long, String> getOptionPlusMarket(Connection conn, long skinId) throws SQLException {
		LinkedHashMap<Long, String> hm = new LinkedHashMap<Long, String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " + "M.id, " + "M.display_name " + "FROM " + "skin_market_groups SMG, "
					+ "skin_market_group_markets SMGM, " + "markets M " + "WHERE " + "SMG.skin_id = ? AND "
					+ "SMG.id = SMGM.skin_market_group_id AND " + "SMGM.market_id = M.id AND "
					+ "NOT M.option_plus_market_id IS NULL AND " + "EXISTS ( " + "SELECT " + "1 " + "FROM "
					+ "opportunity_templates OT " + "WHERE " + "OT.market_id = M.id AND " + "OT.is_active = 1 AND "
					+ "OT.opportunity_type_id =  " + Opportunity.TYPE_OPTION_PLUS_STAING + ") " + "ORDER BY "
					+ "M.name ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, skinId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				hm.put(rs.getLong("id"), MarketsManagerBase.getMarketName(skinId, rs.getLong("id")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return hm;
	}

	public static void setQuoteParamsById(Connection conn, long marketId, String quote) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE markets " + "SET quote_params = ? " + "WHERE id = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, quote);
			ps.setLong(2, marketId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static boolean isExistMarketInBinary(Connection con, Market f) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " Select distinct 'X' " + " From " + " opportunity_templates ot " + " Where market_id = "
					+ f.getId() + " and ot.opportunity_type_id=1 ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static Map<Long, String> getHourLevelFormulas(Connection con, long marketId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, String> result = new HashMap<>();
		try {
			String sql = "select market_data_source_id, hour_level_formula_params from market_data_source_maps where market_id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, marketId);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.put(rs.getLong("market_data_source_id"), rs.getString("hour_level_formula_params"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result;
	}

	public static void updateHourLevelFormulas(Connection con, Long marketId, Long sourceId, String params)
			throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update market_data_source_maps set hour_level_formula_params = ? where market_id = ? and market_data_source_id = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, params);
			ps.setLong(2, marketId);
			ps.setLong(3, sourceId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}