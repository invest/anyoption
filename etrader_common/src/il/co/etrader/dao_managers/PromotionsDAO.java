package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Promotion;
import il.co.etrader.bl_vos.Writer;

public class PromotionsDAO extends DAOBase {

	public static Promotion getLastPromotion(Connection conn, Writer w) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Promotion p = null;
		String sql = null;

		try{
		sql = "SELECT * " +
			  "FROM promotions p " +
			  "WHERE p.id = ( SELECT max ( p.id )  " +
			  				" FROM promotions p " +
			  				" WHERE p.writer_id = " + w.getId() + ")";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		while(rs.next()){
			p = new Promotion();
			p.setId(rs.getLong("ID"));
			p.setMessage(rs.getString("PROMOTION_TEXT"));
			p.setPromotionName(rs.getString("PROMOTION_NAME"));
			p.setSendType(rs.getInt("PROMOTION_TYPE_ID"));
			p.setTemplateId(rs.getLong("TEMPLATE_ID"));
			p.setWriterId(w.getId());
			p.setStatus(rs.getInt("STATUS_ID"));
		}
		}
		finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return p;
	}

	public static void updateStatus(Connection conn, int status, long id) throws SQLException{
		PreparedStatement ps = null;
		String sql = null;

		try{
			sql = "update promotions set STATUS_ID = " + status +  "where ID = " + id ;
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void updatePromotionEntriesStatusById(Connection conn, int status, long promotion_id, long user_id) throws SQLException{
		PreparedStatement ps = null;
		String sql = null;

		try{
			sql = " update promotions_entries " +
				  " set STATUS_ID = " + status +
				  " where USER_ID = " + user_id +
				  " and PROMOTION_ID = " + promotion_id;
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void updatePromotionEntriesStatus(long promotionId, int status, Connection conn) throws SQLException{
		PreparedStatement ps = null;
		String sql = null;
		try{
			sql = " update promotions_entries " +
				  " set STATUS_ID = " + status +
				  " where PROMOTION_ID = " + promotionId;
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}

	}

	public static void updatePromotionEntriesStatus(Connection con,	long promotionId, int promotionEntriesStatus) throws SQLException {
		PreparedStatement ps = null;
		String sql = null;
		try{
			sql = " UPDATE " +
							" promotions_entries_contacts " +
				  " SET " +
				  			" status_id = " + promotionEntriesStatus +
				  " WHERE " +
				  			" promotion_id = " + promotionId;
			ps = con.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

}
