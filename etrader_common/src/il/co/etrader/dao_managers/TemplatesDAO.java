package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Template;

public class TemplatesDAO extends DAOBase{
	  public static void insert(Connection con,Template vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
				String sql="insert into templates(id,name,subject,file_name)" +
						" values(seq_templates.nextval,?,?,?) ";

				ps = con.prepareStatement(sql);

				ps.setString(1,vo.getName());
				ps.setString(2,vo.getSubject());
				ps.setString(3,vo.getFileName());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_templates"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static void update(Connection con,Template vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql="update templates set name=?,subject=?,file_name=? " +
			  			 " where id=?";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setString(2, vo.getSubject());
				ps.setString(3, vo.getFileName());
				ps.setLong(4,vo.getId());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

		  public static ArrayList getAll(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList list=new ArrayList();

			  try
				{
				  String sql="select * from templates ";
					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {

						list.add(getVO(rs));
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  public static HashMap<Long, Template> getAllHashMap(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  HashMap<Long, Template> templates=new HashMap<Long, Template>();
			  Template template = null;

			  try
				{
				  String sql="select * from templates ";
					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						template = getVO(rs);
						templates.put(template.getId(), template);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return templates;

		  }

		  /**
		   * Get templates by skin id
		   * @param con
		   * 		Db connection
		   * @return
		   * 		list of all user's templates
		   * @throws SQLException
		   */
		  public static ArrayList<Template> getAllBySkinId(Connection con, long skinId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Template> list = new ArrayList<Template>();

			  try {

				  String sql = "select t.* from templates t, skin_templates sk, skins s " +
				  			   "where t.id = sk.template_id and s.id = sk.skin_id " +
				  			   "and sk.skin_id = ? " +
				  			   "order by t.id ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, skinId);
					rs = ps.executeQuery();

					while (rs.next()) {
						list.add(getVO(rs));
					}
				}

				finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

				return list;
		  }

		  /**
		   * Get templates by skins list
		   * @param con
		   * 		Db connection
		   * @return
		   * 		list of all writer's templates
		   * @throws SQLException
		   */
		  public static ArrayList<Template> getAllBySkins(Connection con, String skins) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<Template> list = new ArrayList<Template>();

			  try {

				  String sql = "select distinct t.* from templates t, skin_templates sk, skins s " +
				  			   "where t.id = sk.template_id and s.id = sk.skin_id " +
				  			   "and sk.skin_id in ("+ skins +") " +
				  			   "order by t.id ";

					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();

					while (rs.next()) {
						list.add(getVO(rs));
					}
				}

				finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

				return list;
		  }
		  public static Template get(Connection con,long id) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  try
				{
				  String sql="select * from templates where id="+id;
					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					if (rs.next()) {

						return getVO(rs);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return null;

		  }

		  public static ArrayList getTemplatesSelectTypes(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList list=new ArrayList();

			  try
				{
				  String sql="select id,subject from templates ";
					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {

						list.add(new SelectItem(rs.getString("id"),rs.getString("subject")));
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  private static Template getVO(ResultSet rs) throws SQLException{
				Template vo=new Template();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setSubject(rs.getString("subject"));
				vo.setFileName(rs.getString("file_name"));
				vo.setDisplayed(rs.getInt("is_displayed")==1 ? true:false);
				vo.setMailBoxEmailType(rs.getLong("mailbox_email_type"));
				return vo;
		  }

}

