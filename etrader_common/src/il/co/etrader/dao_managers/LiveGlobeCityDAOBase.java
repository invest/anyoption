/**
 * 
 */
package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.daos.DAOBase;

/**
 * @author pavelhe
 *
 */
public class LiveGlobeCityDAOBase extends DAOBase {
	
	public static ArrayList<LiveGlobeCity> getAllCities(Connection con) throws SQLException {
		
		Statement ps = null;
		ResultSet rs = null;
		ArrayList<LiveGlobeCity> list = new ArrayList<LiveGlobeCity>();
			
		try {
			String sql = " SELECT * FROM live_globe_cities order by 1";
			
			ps = con.createStatement();
			rs = ps.executeQuery(sql);
			
			while (rs.next()) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return list;
	}
	
	public static ArrayList<LiveGlobeCity> getActiveCities(Connection con) throws SQLException {
		
		Statement ps = null;
		ResultSet rs = null;
		ArrayList<LiveGlobeCity> list = new ArrayList<LiveGlobeCity>();
			
		try {
			String sql = " SELECT * FROM live_globe_cities WHERE is_active = 1 ";
			
			ps = con.createStatement();
			rs = ps.executeQuery(sql);
			
			while (rs.next()) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return list;
	}
	
	protected static LiveGlobeCity getVO(ResultSet rs) throws SQLException {
		LiveGlobeCity vo = new LiveGlobeCity();

		vo.setId(rs.getLong("id"));
		vo.setCityName(rs.getString("city_name"));
		vo.setCountryId(rs.getLong("country_id"));
		vo.setGlobeLatitude(rs.getString("globe_latitude"));
		vo.setGlobeLongtitude(rs.getString("globe_longtitude"));
		vo.setActive(rs.getBoolean("is_active"));
		vo.setCurrencyId(rs.getLong("currency_id"));

		return vo;
	}

}
