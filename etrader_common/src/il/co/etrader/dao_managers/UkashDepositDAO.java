//package il.co.etrader.dao_managers;
//
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.util.CommonUtil;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import oracle.jdbc.OraclePreparedStatement;
//
//import com.anyoption.common.bl_vos.UkashDeposit;
//import com.anyoption.common.daos.DAOBase;
//
//
//public class UkashDepositDAO extends DAOBase{
//
//	/**
//	 * Insert Ukash Deposit
//	 * @param con
//	 * @param vo
//	 * @throws SQLException
//	 */
//	  public static void insert(Connection con,UkashDeposit vo) throws SQLException {
//		  OraclePreparedStatement ps = null;
//		  ResultSet rs = null;
//		  try {
//			  String sql = "INSERT into " +
//			  			   		"ukash_deposit(id, transaction_id, voucher_number, voucher_value, voucher_currency_id, time_created, status) " +
//			  			   "VALUES " +
//			  			   		"(seq_ukash_deposit.nextval,?,?,?,?,sysdate,?)";
//
//			  ps = (OraclePreparedStatement)con.prepareStatement(sql);
//
//			  ps.setLong(1, vo.getTransactionId());
//			  ps.setString(2, vo.getVoucherNumber());
//			  ps.setLong(3, CommonUtil.calcAmount(vo.getVoucherValue()));
//			  ps.setLong(4, ApplicationDataBase.getCurrencyIdByCode(vo.getVoucherCurrency()));
//			  ps.setInt(5, vo.getStatus());
//
//			  ps.executeUpdate();
//			  vo.setId(getSeqCurValue(con,"seq_ukash_deposit"));
//		  }
//		  finally
//		  {
//			  closeStatement(ps);
//			  closeResultSet(rs);
//		  }
//	  }
//
//
//	  public static UkashDeposit get(Connection con,long id) throws SQLException {
//
//		  PreparedStatement ps=null;
//		  ResultSet rs=null;
//
//		  UkashDeposit ukashDeposit = null;
//
//		  try {
//			  String sql = "SELECT " +
//			  			   		"ud.*, u.currency_id userCurrency, t.amount " +
//			  			   "FROM " +
//			  			   		"ukash_deposit ud, transactions t, users u " +
//			  			   "WHERE " +
//			  			   		"u.id = t.user_id " +
//			  			   		"AND t.id = ud.transaction_id " +
//			  			   		"AND transaction_id = ?";
//			  ps = con.prepareStatement(sql);
//			  ps.setLong(1, id);
//			  rs=ps.executeQuery();
//			  if (rs.next()) {
//				  ukashDeposit = getVO(rs);
//			  }
//		  }
//		  finally
//		  {
//			  closeResultSet(rs);
//			  closeStatement(ps);
//		  }
//		  return ukashDeposit;
//	  }
//
//	  private static UkashDeposit getVO(ResultSet rs) throws SQLException{
//		  UkashDeposit vo = new UkashDeposit();
//
//		  vo.setId(rs.getLong("id"));
//		  vo.setTransactionId(rs.getLong("transaction_id"));
//		  vo.setVoucherNumber(rs.getString("voucher_number"));
//		  vo.setVoucherValue(rs.getString("voucher_value"));
//		  vo.setConvertedAmount(rs.getLong("amount"));
//		  vo.setUserCurrency(rs.getString("userCurrency"));
//		  vo.setStatus(rs.getInt("status"));
//		  long currencyId = rs.getLong("voucher_currency_id");
//		  if (currencyId != 0) {
//			  vo.setVoucherCurrency(ApplicationDataBase.getCurrencyById(currencyId).getCode());
//		  } else {
//			  vo.setVoucherCurrency("");
//		  }
//		  return vo;
//	  }
//}
//
