//package il.co.etrader.dao_managers;
//
//import il.co.etrader.util.ConstantsBase;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import com.anyoption.common.beans.base.Language;
//import com.anyoption.common.daos.DAOBase;
//
//
///**
// * Languages DAO Base class.
// *
// * @author Kobi
// */
//public class LanguagesDAOBase extends DAOBase {
//
//	/**
//	 * Get All Languages
//	 * @param con connection to DB
//	 * @return language code
//	 * @throws SQLException
//	 */
//	public static ArrayList<Language> getAll(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<Language> list = new ArrayList<Language>();
//		try
//		{
//		    String sql = "select * from languages";
//		    ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			while ( rs.next() ) {
//				list.add(getVO(rs));
//			}
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//		return list;
//	}
//
//	/**
//	 * Get All Languages into a hash table
//	 * @param con connection to DB
//	 * @return language code
//	 * @throws SQLException
//	 */
//	public static HashMap<Long,Language> getAllLanguages(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		HashMap<Long,Language> hm = new HashMap<Long,Language>();
//		try
//		{
//		    String sql = "select * from languages";
//		    ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			while ( rs.next() ) {
//				hm.put(rs.getLong("id"), getVO(rs));
//			}
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//		return hm;
//	}
//
//	/**
//	 * Get Language code by id
//	 * @param con
//	 * 		connection to DB
//	 * @param id
//	 * 		language id
//	 * @return
//	 * 		language code
//	 * @throws SQLException
//	 */
//	public static String getCodeById(Connection con, long id) throws SQLException {
//
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String code = "";
//
//		try
//		{
//		    String sql = "select code from languages "+
//		    		     "where id = ? ";
//
//		    ps = con.prepareStatement(sql);
//		    ps.setLong(1, id);
//
//			rs = ps.executeQuery();
//
//			if ( rs.next() ) {
//				code = rs.getString("code");
//			}
//
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//
//		return code;
//	}
//
//	/**
//	 * Returns a language name by language id
//	 * @param con - DB connection
//	 * @param id - language id
//	 * @return language name
//	 * @throws SQLException
//	 */
//	public static String getNameById(Connection con, int id) throws SQLException {
//
//		String name = ConstantsBase.EMPTY_STRING;
//		Statement ps = null;
//		ResultSet rs = null;
//
//		try
//		{
//		    String sql = "select display_name from languages where id="+ id;
//
//		    ps = con.createStatement();
//
//			rs = ps.executeQuery(sql);
//
//			if ( rs.next() ) {
//				name = rs.getString("display_name");
//			}
//
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//
//		return name;
//	}
//
//
//	/**
//	 * Fill Language object
//	 * @param rs
//	 * 		ResultSet
//	 * @return
//	 * 		Language object
//	 * @throws SQLException
//	 */
//	protected static Language getVO(ResultSet rs) throws SQLException {
//
//		Language vo = new Language();
//
//		vo.setId(rs.getInt("id"));
//		vo.setCode(rs.getString("code"));
//		vo.setDisplayName(rs.getString("display_name"));
//		return vo;
//	}
//}