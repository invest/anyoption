package il.co.etrader.dao_managers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.UserActiveData;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationActivationMail;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.enums.DeviceFamily;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.util.AESUtil;
import com.copyop.common.enums.base.UserStateEnum;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.FireServerPixelFields;
import il.co.etrader.bl_vos.FireServerPixelHelper;
import il.co.etrader.bl_vos.MarkVipHelper;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.UserClass;
import il.co.etrader.jobs.RestThreshBlockedUsersBean;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

public abstract class UsersDAOBase extends com.anyoption.common.daos.UsersDAOBase {
	private static final Logger logger = Logger.getLogger(UsersDAOBase.class);

	public static void insert(Connection con, UserBase vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =
					"insert into users(balance,tax_balance,user_name,password,currency_id," + "first_name,last_name,street,street_no,city_id,zip_code," +
					"is_active,email,combination_id,comments,ip,time_birth_date,is_contact_by_email,mobile_phone," +
					"land_line_phone,gender,id_num,id_num_back,limit_id, id,time_created,time_last_login,time_modified,class_id,keyword,company_id," +
					"company_name,tax_exemption,reference_id,city_name,country_id,language_id,skin_id,is_contact_by_sms," +
					"utc_offset_created,utc_offset_modified,id_doc_verify, state_code, dynamic_param, special_code, affiliate_key, " +
					"sub_affiliate_key, contact_id,is_false_account,is_accepted_terms,time_first_visit,chat_id,writer_id,password_back, " +
					"utc_offset, liveperson_session_key, click_id, is_vip, user_agent, device_unique_id, is_authorized_mail, " +
					"is_risky, http_referer, dfa_placement_id, dfa_creative_id, dfa_macro, time_first_authorized, decoded_source_query, device_family, platform_id) " +

					"values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,sysdate,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			long id = getSequenceNextVal(con, "SEQ_USERS");
			vo.setId(id);

			ps = con.prepareStatement(sql);

			ps.setLong(1,vo.getBalance());
			ps.setLong(2,vo.getTaxBalance());
			if (CommonUtil.containsTestDomain(vo.getUserName())) {  // for testing
				if (isUserNameInUse(con, vo.getUserName(), false, null)) {
					vo.setUserName(String.valueOf(id));
				}
			}
			ps.setString(3,vo.getUserName().toUpperCase());

			String password;
			Date timeFirstAuth = null;
			if(vo.getSkinId() == Skin.SKIN_ETRADER){
				password = vo.getPassword().toUpperCase();
			} else {
				password = vo.getPassword();
				timeFirstAuth = new Date();
			}
			try {
				String encryptedPassword = AESUtil.encrypt(password);
				if (!password.equals(AESUtil.decrypt(encryptedPassword))) {
					throw new CryptoException("Failed in test: !password.equals(Encryptor.decryptStringToString(encryptedPassword))");
				}
				ps.setString(4, encryptedPassword);
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
				throw new SQLException("Unable to encrypt/decrypt String" + ce.getMessage());
			}
			ps.setLong(5,vo.getCurrencyId());
			ps.setString(6,CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(7,CommonUtil.capitalizeFirstLetters(vo.getLastName()));
			ps.setString(8,vo.getStreet());
			ps.setString(9,vo.getStreetNo());
			ps.setLong(10,vo.getCityId());
			ps.setString(11,vo.getZipCode());
			ps.setInt(12,Integer.parseInt(vo.getIsActive()));
			ps.setString(13,vo.getEmail());
			ps.setLong(14,vo.getCombinationId());
			ps.setString(15,vo.getComments());
			ps.setString(16,vo.getIp());
			if (vo.getTimeBirthDate() == null) {
				ps.setNull(17, Types.DATE);
			} else {
				ps.setTimestamp(17,CommonUtil.convertToTimeStamp(vo.getTimeBirthDate()));
			}
			ps.setInt(18,vo.getIsContactByEmail());
			ps.setString(19,vo.getMobilePhone());
			ps.setString(20,vo.getLandLinePhone());
			ps.setString(21,vo.getGender());
			if (vo.getIdNum() == null || vo.getIdNum().isEmpty()) {
			    ps.setString(22, AESUtil.encrypt(" "));
			} else {
			    ps.setString(22, AESUtil.encrypt(vo.getIdNum()));
			}
			ps.setString(23,vo.getIdNum());
			ps.setLong(24,(vo.getLimitIdLongValue()));
			ps.setLong(25, vo.getId());
			ps.setTimestamp(26,CommonUtil.convertToTimeStamp(vo.getTimeLastLogin()));
			ps.setLong(27,vo.getClassId());
			ps.setString(28,vo.getKeyword());

			ps.setBigDecimal(29,vo.getCompanyId());
			ps.setString(30,vo.getCompanyName());
			ps.setInt(31,vo.isTaxExemption()==true ? 1 : 0);
			ps.setBigDecimal(32,vo.getReferenceId());

			ps.setString(33, vo.getCityName());
			ps.setLong(34, vo.getCountryId());
			ps.setLong(35, vo.getLanguageId());
			ps.setLong(36, vo.getSkinId());
			ps.setLong(37, (vo.getIsContactBySMS()));

			String utcOffsetCreated = vo.getUtcOffsetCreated();

			if (null == utcOffsetCreated){
				utcOffsetCreated = CommonUtil.getUtcOffset(vo.getUtcOffsetCreated());
			}
			ps.setString(38, utcOffsetCreated);

			String utcOffsetModified = vo.getUtcOffsetModified();
			if (null == utcOffsetModified){
				utcOffsetModified = CommonUtil.getUtcOffset(vo.getUtcOffsetModified());
			}
			ps.setString(39, utcOffsetModified);

			if ( vo.isIdDocVerify() ) {
				ps.setLong(40, 1);
			}
			else {
				ps.setLong(40, 0);
			}
			ps.setLong(41, vo.getState());
			String decodedDP = null;
			String dynamicParam = vo.getDynamicParam();
			if (null != dynamicParam) {
				try {
					// decode dynamic Parameter.
					decodedDP = URLDecoder.decode(dynamicParam, "UTF-8");
				} catch (Exception e) {
					decodedDP = dynamicParam;
					logger.error("Error decoding dynamic param", e);
				}
			}
			ps.setString(42, decodedDP);
			ps.setString(43, vo.getSpecialCode());
			ps.setLong(44, vo.getAffiliateKey());
			ps.setLong(45, vo.getSubAffiliateKey());
			ps.setLong(46, vo.getContactId());
			ps.setInt(47, (vo.isFalseAccount()?1:0));
			ps.setInt(48, (vo.isAcceptedTerms()?1:0));
			ps.setTimestamp(49,CommonUtil.convertToTimeStamp(vo.getTimeFirstVisit()));
			ps.setLong(50,vo.getChatId());
            ps.setLong(51, vo.getWriterId());
            ps.setString(52, password); // password_back
            ps.setString(53, utcOffsetCreated);
            ps.setString(54, vo.getLpSessionKey());
            ps.setString(55, vo.getClickId());
            ps.setLong(56, (vo.isVip()?1:0));
            ps.setString(57, vo.getUserAgent());
            ps.setString(58, vo.getDeviceUniqueId());
            ps.setInt(59, vo.isAuthorizedMail() ? 1 : 0);
            ps.setInt(60, vo.isRisky() ? 1 : 0);
            ps.setString(61, vo.getHttpReferer());
            ps.setString(62, vo.getDfaPlacementId());
            ps.setString(63, vo.getDfaCreativeId());
            ps.setString(64, vo.getDfaMacro());
            ps.setTimestamp(65,CommonUtil.convertToTimeStamp(timeFirstAuth));
            ps.setString(66, vo.getDecodedHttpRefferer());
            ps.setLong(67, DeviceFamily.NONMOBILE.getId());
            ps.setInt(68, vo.getPlatformId());
			ps.executeUpdate();
            //update for dev 3399
            vo.setUtcOffset(utcOffsetCreated);
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} catch (SQLIntegrityConstraintViolationException e) {
			String msg = "Constraint violated: " + e.getMessage().substring(e.getMessage().indexOf('(') + 1, e.getMessage().indexOf(')'));
			if (e.getMessage().toUpperCase().contains(ACTIVE_SKIN_CONSTRAINT)) {
				logger.error(msg + ", skinId: " + vo.getSkinId());
			} else {
				logger.error(msg, e);
			}
			throw e;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

	}

	public static long getBalance(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select balance from users where id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("balance");

			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0;
	}


	public static long getFirstTransactionId(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select first_deposit_id from users where id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("first_deposit_id");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0;
	}
	public static long getTaxBalance(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select tax_balance from users where id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("tax_balance");

			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0;
	}

	public static boolean isTaxExemption(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select * from users where id=? and tax_exemption=1";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return true;

			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return false;
	}

	public static UserClass getDefaultClass(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		UserClass vo = null;
		try {
			String sql = "select * from user_classes where is_default=1";

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			if (rs.next()) {
				vo = new UserClass();
				vo.setId(rs.getLong("id"));
				vo.setName(CommonUtil.getMessage(rs.getString("name"), null));
				vo.setIsDefault(rs.getInt("is_default"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));

			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;

	}

	public static HashMap<Long,UserClass> getUserClasses(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		UserClass vo = null;
		HashMap<Long,UserClass> hm = new HashMap<Long,UserClass>();
		try {
			String sql = "select * from user_classes ";

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			while(rs.next()) {
				vo = new UserClass();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setIsDefault(rs.getInt("is_default"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
				hm.put(rs.getLong("id"), vo);
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;

	}

	public static void update(Connection con, UserBase vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"update users set user_name=?,password=?, password_back=?,currency_id=?," +
						"first_name=?,last_name=?,street=?,city_id=?,zip_code=?,time_last_login=?," +
						"is_active=?,email=?,combination_id=?,comments=?,ip=?,time_birth_date=?,is_contact_by_email=?," +
						"mobile_phone=?,land_line_phone=?,gender=?,id_num=?,id_num_back=?,limit_id=?,time_modified=sysdate," +
						"class_id=?,street_no=?,last_failed_time=?,failed_count=?," +
						" company_id=?,company_name=?,tax_exemption=?,reference_id=?, " +
						"is_contact_by_sms=?, skin_id=?, utc_offset_modified=? , city_name=?, utc_offset=?, id_doc_verify=?, country_id=?, language_id=?, state_code=?, "+
						"docs_required=?, is_contact_by_phone=? , is_false_account=?, is_vip=?, is_authorized_mail = ?, authorized_mail_refused_times = ?, " +
						"is_risky = ?, time_sc_manualy = ?, sc_updated_by = ? , is_stop_reverse_withdraw = ?, is_immediate_withdraw = ? " +
					"where id=?";

			ps = con.prepareStatement(sql);
			if (CommonUtil.containsTestDomain(vo.getUserName())) {  // for testing
				if (isUserNameInUseIgnoreCase(con, vo.getUserName(), vo.getId())) {
					vo.setUserName(String.valueOf(vo.getId()));
				}
			}
			ps.setString(1,vo.getUserName().toUpperCase());

			String password;
			if(vo.getSkinId() == Skin.SKIN_ETRADER){
				password = vo.getPassword().toUpperCase();
			} else {
				password = vo.getPassword();
			}
			try {
				ps.setString(2, AESUtil.encrypt(password));
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
				// FIXME very bad way of handling exceptions
				throw new SQLException("CryptoException: " + ce.getMessage());
			}
			ps.setString(3, password); // password_back

			ps.setLong(4,vo.getCurrencyId());

			ps.setString(5,CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(6,CommonUtil.capitalizeFirstLetters(vo.getLastName()));
			ps.setString(7,vo.getStreet());
			ps.setLong(8,vo.getCityId());
			ps.setString(9,vo.getZipCode());

			ps.setTimestamp(10,CommonUtil.convertToTimeStamp(vo.getTimeLastLogin()));

			ps.setInt(11,Integer.parseInt(vo.getIsActive()));
			ps.setString(12,vo.getEmail());
			ps.setLong(13,vo.getCombinationId());
			ps.setString(14,vo.getComments());
			ps.setString(15,vo.getIp());
			ps.setTimestamp(16,CommonUtil.convertToTimeStamp(vo.getTimeBirthDate()));

			ps.setInt(17,vo.getIsContactByEmail());

			ps.setString(18,vo.getMobilePhone());
			ps.setString(19,vo.getLandLinePhone());
			ps.setString(20,vo.getGender());
			ps.setString(21, AESUtil.encrypt(vo.getIdNum()));
			ps.setString(22,vo.getIdNum()); // id_num_back

			ps.setLong(23,vo.getLimitIdLongValue());

			ps.setLong(24,vo.getClassId());
			ps.setString(25,vo.getStreetNo());

			ps.setTimestamp(26,CommonUtil.convertToTimeStamp(vo.getLastFailedTime()));
			ps.setInt(27,vo.getFailedCount());

			ps.setBigDecimal(28,vo.getCompanyId());
			ps.setString(29,vo.getCompanyName());
			ps.setInt(30,vo.isTaxExemption()==true ? 1 : 0);
			ps.setBigDecimal(31,vo.getReferenceId());
			//ps.setInt(31,vo.getIsNextInvestOnUs());
			ps.setInt(32,vo.isContactBySMS() ?  1 : 0);
			ps.setLong(33, vo.getSkinId());
			ps.setString(34,vo.getUtcOffset());
			ps.setString(35, vo.getCityName());
			ps.setString(36, vo.getUtcOffset());
			ps.setInt(37, vo.isIdDocVerify()==true ? 1 : 0);
			ps.setLong(38, vo.getCountryId());
			ps.setLong(39, vo.getLanguageId());
			ps.setLong(40, vo.getState());
			ps.setInt(41, vo.isDocsRequired()==true ? 1 : 0);
			ps.setInt(42,vo.isContactByPhone() ?  1 : 0);
			ps.setInt(43,(vo.isFalseAccount() ? 1 : 0));
			ps.setInt(44,(vo.isVip() ? 1 : 0));
			ps.setInt(45,(vo.isAuthorizedMail() ? 1 : 0));
			ps.setLong(46,(vo.getAuthorizedMailRefusedTimes()));
            ps.setInt(47,(vo.isRisky() ? 1 : 0));
            ps.setTimestamp(48,CommonUtil.convertToTimeStamp(vo.getSpecialCare().getTimeScManual()));
            ps.setLong(49, vo.getSpecialCare().getScUpdatedBy());
            ps.setLong(50, vo.isStopReverseWithdrawOption() ? 1 : 0);
            ps.setLong(51, vo.isImmediateWithdraw() ? 1 : 0);

			// Set The user Id
			ps.setLong(52, vo.getId());
			ps.executeUpdate();

		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		} finally {
			closeStatement(ps);
		}

	}

	public static void updateTax(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG,"update tax - "+amount+" to user:"+id + ls );
		}
		try {
			String sql = "update users set tax_balance=? ,time_modified=sysdate where id=?";

			ps = con.prepareStatement(sql);

			ps.setLong(1,amount);
			ps.setLong(2,id);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	public static void updateWinLose(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "update win_lose - " + amount + " to user:" + id + ls );
		}
		try {
			String sql = "UPDATE " +
									"users " +
						 "SET " +
									"win_lose = ? , " +
									"time_modified = sysdate " +
						 "WHERE id = ? ";

			ps = con.prepareStatement(sql);

			ps.setLong(1,amount);
			ps.setLong(2,id);

			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void addToBalance(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Adding to balance : "+amount+" to user:"+id + ls );
		}
		try {
			String sql = 	"UPDATE " +
									" users " +
							"SET " +
									"balance = balance+?, " +
									"time_modified = sysdate, " +
									"utc_offset_modified = utc_offset " +
							"WHERE " +
									"id= ? ";

			ps =  con.prepareStatement(sql);

			ps.setLong(1,amount);
			ps.setLong(2,id);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Check if there is existing user with the specified id number.
	 *
	 * @param conn
	 *            the db connection to use
	 * @param userName
	 *            the user name to check
	 * @return <code>ture</code> if the user name is in use else
	 *         <code>false</code>.
	 * @throws DAORuntimeException
	 */
	public static boolean isUserIdNumberInUse(Connection conn, String idnum, long skinId) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
							"users " +
						 "WHERE " +
							"id_num = ? " +
							"AND skin_id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, AESUtil.encrypt(idnum));
			pstmt.setLong(2, skinId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}

	public static boolean isUserIdNumForOtherUserId(Connection conn, String idnum, long userId, String userName) throws SQLException {
		
		if (idnum.equals(ConstantsBase.NO_ID_NUM)) {
			return false;
		}
		
			boolean inUse = false;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				String sql = "SELECT " +
								"id " +
							  "FROM " +
							  	"users " +
							  "WHERE " +
							  	"id_num = ? AND " +
							  	"id <> ? AND " +
							  	"(reference_id is null or reference_id <> ?) AND " +
							  	"user_name <> ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, AESUtil.encrypt(idnum));
				pstmt.setLong(2, userId);
				pstmt.setLong(3, userId);

				int suffixPos = userName.indexOf(ConstantsBase.CAL_GAME_USER_NAME_SUFFIX.toUpperCase());
				if (suffixPos > -1){
					userName = userName.substring(0, suffixPos);
				} else {
					userName = userName + ConstantsBase.CAL_GAME_USER_NAME_SUFFIX;
				}
				pstmt.setString(4, userName.toUpperCase());

				rs = pstmt.executeQuery();
				if (rs.next()) {
					inUse = true;
				}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
			return inUse;
		}


	/**
	 * Insert role for user.
	 *
	 * @param conn
	 *            the db connection to use
	 * @param userName
	 *            the user name
	 * @param role
	 *            the role
	 * @throws DAORuntimeException
	 */
	public static long insertUserRole(Connection conn, String userName, String role) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "INSERT INTO roles (id,user_name, role) VALUES (seq_roles.nextval,?,?)";
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1,userName.toUpperCase());
			pstmt.setString(2,role);
			pstmt.executeUpdate();

			return getSeqCurValue(conn,"seq_roles");

		} finally {
			closeStatement(pstmt);
			closeResultSet(rs);
		}

	}
	
	public static void getVOBasic(ResultSet rs, UserBase vo) throws SQLException {
        SpecialCare specialCare = new SpecialCare();
        UserActiveData activeData = new UserActiveData();
        activeData.setCopyopUserStatus(UserStateEnum.of(rs.getInt("COPYOP_USER_STATUS")));
        activeData.setStatusUpdateDate(rs.getDate("STATUS_UPDATE_DATE"));
        activeData.setBubblesPricingLevel(rs.getInt("bubbles_pricing_level"));
        vo.setUserActiveData(activeData);
        vo.setId(rs.getLong("id"));
        vo.setBalance(rs.getLong("BALANCE"));
        vo.setTaxBalance(rs.getLong("TAX_BALANCE"));
        vo.setUserName(rs.getString("USER_NAME"));
        vo.setTotalWinLose(rs.getLong("WIN_LOSE"));
        vo.setWinLoseHouse(rs.getLong("WINLOSE_BALANCE"));
        try {
        	if(rs.getString("password")!=null) {
        		vo.setPassword(AESUtil.decrypt(rs.getString("password")));
        	}
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
        vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
        vo.setFirstName(rs.getString("FIRST_NAME"));
        vo.setLastName(rs.getString("LAST_NAME"));
        vo.setStreet(rs.getString("STREET"));
        vo.setStreetNo(rs.getString("STREET_no"));
        vo.setCityId(rs.getLong("CITY_ID"));
        vo.setZipCode(rs.getString("ZIP_CODE"));

        if(rs.getTimestamp("time_created")!=null){
        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));}
        if(rs.getTimestamp("TIME_LAST_LOGIN")!=null){
        vo.setTimeLastLogin(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
        } else {
        	vo.setTimeLastLogin(null);
        }
        if(rs.getTimestamp("TIME_LAST_LOGIN")!=null){
        vo.setTimeLastLoginFromDb(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
        } else {
        	vo.setTimeLastLoginFromDb(null);
        }
        vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
        if(rs.getTimestamp("TIME_MODIFIED")!=null){
        vo.setTimeModified(convertToDate(rs.getTimestamp("TIME_MODIFIED")));}
        if(rs.getTimestamp("TIME_FIRST_VISIT")!=null){
        vo.setTimeFirstVisit(convertToDate(rs.getTimestamp("TIME_FIRST_VISIT")));}

        vo.setIsActive(rs.getString("IS_ACTIVE"));
        vo.setEmail(rs.getString("EMAIL"));
        vo.setCombinationId(rs.getLong("combination_id"));
        vo.setComments(rs.getString("COMMENTS"));
        vo.setIp(rs.getString("IP"));

        vo.setIsContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL"));
        vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
        vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
        vo.setGender(rs.getString("GENDER"));
        try {
        	if(rs.getString("id_num")!=null){
        	vo.setIdNum(AESUtil.decrypt(rs.getString("id_num")));}
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
        vo.setLimitId(rs.getLong("LIMIT_ID"));
        vo.setClassId(rs.getLong("class_id"));
        vo.setLastFailedTime(convertToDate(rs.getTimestamp("last_failed_time")));
        vo.setFailedCount(rs.getInt("failed_count"));

        vo.setCompanyId(rs.getBigDecimal("company_id"));
        vo.setCompanyName(rs.getString("company_name"));
        vo.setTaxExemption(rs.getInt("tax_exemption")==1);
        vo.setReferenceId(rs.getBigDecimal("reference_id"));

        vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
        vo.setIsContactByPhone(rs.getLong("is_contact_by_phone"));

        // for anyOption
        vo.setCountryId(rs.getLong("country_id"));

        vo.setLanguageId(rs.getLong("language_id"));

        vo.setSkinId(rs.getLong("skin_id"));

        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetModified(rs.getString("utc_offset_modified"));
        vo.setUtcOffset(rs.getString("utc_offset"));
		vo.setContactForDaa(rs.getLong("is_contact_for_daa") == 1 ? true : false);
        long idDocNeedVerify = rs.getLong("id_doc_verify");
        if ( idDocNeedVerify == 1 ) {
            vo.setIdDocVerify(true);
        }
        else {
            vo.setIdDocVerify(false);
        }

//        long specialCareManualy = rs.getLong("time_sc_manualy");
//        if ( specialCareManualy == 1 ) {
//            specialCare.setSpecialCareVerify(true);
//            //vo.setSpecialCare(specialCare);   AT THE END OF THIS FUNC
//        }
//        else {
//            specialCare.setSpecialCareVerify(false);
//            //vo.setSpecialCare(specialCare);   AT THE END OF THIS FUNC
//        }

        vo.setState(rs.getLong("state_code"));

        long docsRequired = rs.getLong("docs_required");
        if ( docsRequired == 1 ) {
            vo.setDocsRequired(true);
        } else {
            vo.setDocsRequired(false);
        }

        vo.setDynamicParam(rs.getString("dynamic_param"));
        vo.setAffSub1(rs.getString("aff_sub1"));
        vo.setAffSub2(rs.getString("aff_sub2"));
        vo.setAffSub3(rs.getString("aff_sub3"));
        
		if (rs.getInt("is_false_account") == 1){
			vo.setFalseAccount(true);
		} else {
			vo.setFalseAccount(false);
		}

		if (rs.getInt("is_accepted_terms") == 1){
			vo.setAcceptedTerms(true);
		} else {
			vo.setAcceptedTerms(false);
		}

		
		vo.setAffiliateKey(rs.getLong("affiliate_key"));
		vo.setSubAffiliateKey(rs.getLong("sub_affiliate_key"));
		vo.setSpecialCode(rs.getString("special_code"));
		vo.setChatId(rs.getLong("chat_id"));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setContactId(rs.getLong("contact_id"));
        vo.setClickId(rs.getString("click_id"));
        vo.setVip(rs.getInt("is_vip")==1);
        vo.setAuthorizedMail(rs.getInt("is_authorized_mail") == 1 ? true : false);
        vo.setAuthorizedMailRefusedTimes(rs.getLong("authorized_mail_refused_times"));
        if(rs.getTimestamp("TIME_FIRST_AUTHORIZED")!=null){
        vo.setTimeFirstAuthorized(convertToDate(rs.getTimestamp("TIME_FIRST_AUTHORIZED")));}
        vo.setUserAgent(rs.getString("user_agent"));
        vo.setDeviceUniqueId(rs.getString("device_unique_id"));
        vo.setRisky(rs.getLong("is_risky") == 1 ? true : false);
        if(rs.getTimestamp("TIME_SC_TURNOVER")!=null){
        specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));}
        if(rs.getTimestamp("TIME_SC_HOUSE_RESULT")!=null){
        specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));}
        if(rs.getTimestamp("time_sc_manualy")!=null){
        specialCare.setTimeScManual(convertToDate(rs.getTimestamp("time_sc_manualy")));}
        specialCare.setScUpdatedBy(rs.getLong("sc_updated_by"));
        vo.setSpecialCare(specialCare);
        vo.setFirstDepositId(rs.getLong("first_deposit_id"));
        vo.setTag(rs.getLong("tag"));
        vo.setBackground(rs.getString("background"));
        vo.setTip(rs.getString("tip"));
        vo.setStatusId(rs.getLong("status_id"));
        vo.setRankId(rs.getLong("rank_id"));
        long isStopReverseWithdrawOption = rs.getLong("is_stop_reverse_withdraw");
        if (isStopReverseWithdrawOption == 1 ) {
            vo.setStopReverseWithdrawOption(true);
        } else {
            vo.setStopReverseWithdrawOption(false);
        }
        vo.setNonRegSuspend(rs.getBoolean("IS_NON_REG_SUSPEND"));
        long isImmediateWithdraw = rs.getLong("is_immediate_withdraw");
        if (isImmediateWithdraw == 1 ) {
            vo.setImmediateWithdraw(true);
        } else {
            vo.setImmediateWithdraw(false);
        }
//        vo.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
//        vo.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));

        vo.setmId(rs.getString("mid"));
        ResultSetMetaData rsmd = rs.getMetaData();


        for(int i=1; i <= rsmd.getColumnCount();i++){

        	if("uc".equalsIgnoreCase(rsmd.getColumnName(i))){
                vo.setUcSearch(rs.getString("uc"));
               }
        	if("con_id".equalsIgnoreCase(rsmd.getColumnName(i))){
                vo.setContactId(rs.getLong("con_id"));
               }
        	if("first_name_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
                vo.setFirstName(rs.getString("first_name_uc"));
               }
           if("last_name_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
                vo.setLastName(rs.getString("last_name_uc"));
               }
           if("email_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
               vo.setEmail(rs.getString("email_uc"));
               }
           if("mobile_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
               vo.setMobilePhone(rs.getString("mobile_uc"));
               }
           if("phone_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
               vo.setLandLinePhone(rs.getString("phone_uc"));
               }
           if("skin_id_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
               vo.setSkinId(rs.getLong("skin_id_uc"));
               }
           if("contact_By_Email".equalsIgnoreCase(rsmd.getColumnName(i))){
               vo.setContactByEmail(rs.getBoolean("contact_By_Email"));

               }
          }
        vo.setPlatformId(rs.getInt("platform_id"));
    }

	protected static void getVO(Connection con, ResultSet rs, UserBase vo) throws SQLException {
        getVOBasic(rs, vo);

		//vo.setIsNextInvestOnUs(rs.getInt("is_next_invest_on_us"));
		vo.setIsNextInvestOnUs(BonusDAOBase.hasNextInvestOnUs(con, rs.getLong("id")) > 0 ? 1 : 0);

		// for anyOption
        vo.setCountry(ApplicationDataBase.getCountry(vo.getCountryId()));
        if (null != vo.getCountry()) {
            vo.setCountryName(vo.getCountry().getDisplayName());
        }
//		String countryName = CountryDAOBase.getNameById(con, countryId.intValue());
//		if( null != countryId && null!= countryName) {
//			vo.setCountryId(countryId);
//			vo.setCountryName(countryName);
//		}

        Language lng = ApplicationDataBase.getLanguage(vo.getLanguageId());
        if (null != lng) {
            vo.setLanguageName(lng.getDisplayName());
        }
//		String languageName = LanguagesDAOBase.getNameById(con,languageId.intValue());
//		if( null != languageId  && null!= languageName) {
//			vo.setLanguageId(languageId);
//			vo.setLanguageName(languageName);
//		}
        vo.setCurrency(ApplicationDataBase.getCurrencyById(vo.getCurrencyId()));

        Long skinId = null;
        if(vo.getSkinId() ==0){
	    skinId = rs.getLong("skin_id");}else{
         skinId= vo.getSkinId();
	    }
		String skinName = SkinsDAOBase.getNameById(con, skinId.intValue());
		if( null != skinId && null != skinName) {
			vo.setSkinId(skinId);
			vo.setSkinName(skinName);
			vo.setSkin(ApplicationDataBase.getNewSkinById(skinId));
		}

		vo.setCityName(rs.getString("city_name"));
		vo.setIsNeedChangePassword(rs.getBoolean("is_need_change_password"));
		Hashtable<String, Boolean> productViewFlags = new Hashtable<String, Boolean>();
		productViewFlags.put(ConstantsBase.HAS_DYNAMICS_FLAG, rs.getBoolean("has_dynamics"));
		if (CommonUtil.getProperty("isHasBubbles.always.flag").equalsIgnoreCase("true")) {
			productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, true);
		} else {					
			productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, rs.getBoolean("has_bubbles"));
		}
		vo.setProductViewFlags(productViewFlags);
		vo.setShowCancelInvestment(rs.getBoolean("show_cancel_investment"));
	}
	
	protected static void getAdvancedSearchVO(Connection con, ResultSet rs, UserBase vo) throws SQLException {
        getVOBasic(rs, vo);

		//vo.setIsNextInvestOnUs(rs.getInt("is_next_invest_on_us"));
		vo.setIsNextInvestOnUs(BonusDAOBase.hasNextInvestOnUs(con, rs.getLong("id")) > 0 ? 1 : 0);

		// for anyOption
        vo.setCountry(ApplicationDataBase.getCountry(vo.getCountryId()));
        if (null != vo.getCountry()) {
            vo.setCountryName(vo.getCountry().getDisplayName());
        }
//		String countryName = CountryDAOBase.getNameById(con, countryId.intValue());
//		if( null != countryId && null!= countryName) {
//			vo.setCountryId(countryId);
//			vo.setCountryName(countryName);
//		}

        Language lng = ApplicationDataBase.getLanguage(vo.getLanguageId());
        if (null != lng) {
            vo.setLanguageName(lng.getDisplayName());
        }
//		String languageName = LanguagesDAOBase.getNameById(con,languageId.intValue());
//		if( null != languageId  && null!= languageName) {
//			vo.setLanguageId(languageId);
//			vo.setLanguageName(languageName);
//		}
        vo.setCurrency(ApplicationDataBase.getCurrencyById(vo.getCurrencyId()));

        Long skinId = null;
        if(vo.getSkinId() ==0){
	    skinId = rs.getLong("skin_id");}else{
         skinId= vo.getSkinId();
	    }
		String skinName = SkinsDAOBase.getNameById(con, skinId.intValue());
		if( null != skinId && null != skinName) {
			vo.setSkinId(skinId);
			vo.setSkinName(skinName);
			vo.setSkin(ApplicationDataBase.getNewSkinById(skinId));
		}

		vo.setCityName(rs.getString("city_name"));
		vo.setIsNeedChangePassword(rs.getBoolean("is_need_change_password"));
		Hashtable<String, Boolean> productViewFlags = new Hashtable<String, Boolean>();
		productViewFlags.put(ConstantsBase.HAS_DYNAMICS_FLAG, rs.getBoolean("has_dynamics"));
		if (CommonUtil.getProperty("isHasBubbles.always.flag").equalsIgnoreCase("true")) {
			productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, true);
		} else {					
			productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, rs.getBoolean("has_bubbles"));
		}
		vo.setProductViewFlags(productViewFlags);
	}

	public static boolean getByUserName(Connection conn, String name, UserBase vo)
			throws SQLException {
        return getByUserName(conn, name, vo, false);
    }

	public static boolean getByUserName(Connection con, String name, UserBase vo, boolean basic)
			throws SQLException {
		if (name == null || name.trim().equals(""))
			return false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
	        String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "users u, " +
                    "users_active_data uad " +
                "WHERE " +
                    "u.id = uad.user_id " +
                    "AND u.user_name = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1,name.toUpperCase());
			rs = ps.executeQuery();
			if (rs.next()) {
			 	if (basic) {
                	getVOBasic(rs, vo);
                } else {
	                getVO(con, rs, vo);
	               long tierUserId = rs.getLong("tier_user_id");
					if (tierUserId > 0) {
						vo.setTierUser(TiersDAOBase.getTierUserById(con, tierUserId));
					}
    	        }
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static UserBase getUserById(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserBase vo = null;
		try {
			String sql = "SELECT " +
							 "u.*, " +
							 "uad.* " +
					     "FROM users u, " +
					     	"users_active_data uad " +
					     "WHERE " +
					     	"u.id = uad.user_id " +
					     	"AND u.id =  ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new UserBase();
				getVO(con,rs,vo);
				vo.setCurrency(ApplicationDataBase.getCurrencyById(vo.getCurrencyId()));

			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
	
	/**
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RestThreshBlockedUsersBean> getRestThreshBlockedUsersData(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RestThreshBlockedUsersBean> users = new ArrayList<RestThreshBlockedUsersBean>();
		try {
			String sql = " SELECT " +
						 " 	u.id, " +
						 " 	u.email, " +
						 " 	uram.time_created, " +
						 " 	u.skin_id " +
						 " FROM " +
						 "	users_reg_activation_mail uram, " +
						 "  users u " +
						 " WHERE " +
						 "	u.id = uram.user_id " +
						 "	AND uram.suspended_reason_id in (? , ?) " +
						 "  AND trunc(uram.time_created) = trunc(SYSDATE - 1) " +						 
						 " ORDER BY " +
						 "	uram.time_created ";

			ps = con.prepareStatement(sql);
			ps.setInt(1, UserRegulationActivationMail.SUSPENDED_LOW_X_TRESHOLD);
			ps.setInt(2, UserRegulationActivationMail.SUSPENDED_HIGH_Y_TRESHOLD);
			rs = ps.executeQuery();
			while (rs.next()) {
				RestThreshBlockedUsersBean user = new RestThreshBlockedUsersBean();
				user.setId(rs.getLong("id"));
				user.setEmail(rs.getString("email"));
				user.setSkinId(rs.getLong("skin_id"));
				user.setTimeBlocked(rs.getDate("time_created"));
				users.add(user);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return users;
	}


	  /**
	   * Check and return if user send documents for a bigger deposits
	   * @param con
	   * 	Db connection
	   * @param userId
	   * 	user id
	   * @return
	   * 	true if the user send documents
	   * @throws SQLException
	   */
	  public static boolean getIsSendIdDocuments(Connection con,  long userId) throws SQLException {

		    PreparedStatement ps = null;
		    ResultSet rs = null;

			try {
				String sql = "select id_doc_verify from users where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs = ps.executeQuery();

				if ( rs.next() ) {
					long isSend = rs.getLong("id_doc_verify");
					if ( isSend == 1 ) {
						return true;
					}
					else {
						return false;
					}

				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return false;
		}


		/**
		 * update document_required field in a case the
		 * user sum deposits over the max, he need to send us documents.
		 *
		 * @param conn
		 * 		Db connection
		 * @param userId
		 * 		user id
		 * @throws SQLException
		 */
	    public static void setFlagsAfterMaxDeposit(Connection conn, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "update users " +
	                		 "set " +
	                		 "docs_required = 1  " +
	                		 "where id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }


	    /**
	     * Get docs_required field
	     * this flag indicates if the user need to send us documents for making deposits
	     * @param con
	     * 		Db connection
	     * @param username
	     * 		the name of the user
	     * @return
	     * @throws SQLException
	     */
	  	public static int getIsDocsRequired(Connection con,String username) throws SQLException{
	  		int isDocsRequired;
	  		PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				    String sql = "select docs_required from users where user_name=upper(?)";
					ps = con.prepareStatement(sql);
					ps.setString(1, username);
					rs = ps.executeQuery();

					if (rs.next()) {

						isDocsRequired = rs.getInt("docs_required");
						return isDocsRequired;
					}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return 0;
	  	}

	  	/**
	  	 * this method returns the currencyId by UserID
	  	 * @param con - DB connection
	  	 * @param userId - the user Id
	  	 * @return CURRENCY_ID of the user
	  	 * @throws SQLException
	  	 */
	  	public static String getCurrencyIdByUserId(Connection con, String userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				String sql = "select * from users where id=? ";

				ps = con.prepareStatement(sql);
				ps.setString(1,userId);

				rs = ps.executeQuery();

				if (rs.next()) {
					return rs.getString("CURRENCY_ID");

				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return null;
		}

    public static long getUserIdByPhone(Connection con,String phone)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try
			{
			    String sql="SELECT u.id " +
			    			"FROM users u " +
			    			"WHERE upper(u.mobile_phone) like '%" + phone + "%' "  +
			    				"OR upper(u.land_line_phone) like '%" + phone + "%' ";

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				if (rs.next()) {
					return rs.getLong("id");
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
	  }

	public static boolean getByUserId(Connection conn, long userId, UserBase vo)
			throws SQLException {
        return getByUserId(conn, userId, vo, false);
    }

    public static boolean getByUserId(Connection con, long userId, UserBase vo, boolean basic)
    		throws SQLException {
        PreparedStatement ps = null;
	    ResultSet rs = null;
	    try {
	        String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "users u, " +
                        "users_active_data uad " +
                    "WHERE " +
                        "u.id = uad.user_id " +
                        "AND u.id = ?";
	        ps = con.prepareStatement(sql);
	        ps.setLong(1,userId);
	        rs = ps.executeQuery();
			if (rs.next()) {
			 	if (basic) {
                	getVOBasic(rs, vo);
                } else {
	                getVO(con, rs, vo);
	                long tierUserId = rs.getLong("tier_user_id");
					if (tierUserId > 0) {
						vo.setTierUser(TiersDAOBase.getTierUserById(con, tierUserId));
					}
    	        }
				return true;
			}
	    } finally {
	        closeResultSet(rs);
	        closeStatement(ps);
	    }
	    return false;
	}

    /**
     * Is user have free sms option
     * @param con
     * @param userId
     * @return true in case that user need to get settled info sms
     * @throws SQLException
     */
    public static boolean isFreeSms(Connection con, long userId) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;

    	try {
    			String sql = "SELECT " +
    							"t.id " +
    						 "FROM " +
    						 	"users u, " +
    						 	"tier_users tu, " +
    						 	"tiers t " +
    						 "WHERE " +
    						 	"u.id = ? AND " +
    						 	"u.tier_user_id = tu.id AND " +
    						 	"tu.tier_id = t.id AND " +
    						 	"t.is_free_sms = 1 ";

    			ps = con.prepareStatement(sql);
    			ps.setLong(1, userId);
    			rs = ps.executeQuery();
    			if (rs.next()) {
    				return true;
    			}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return false;
    }

    /**
	    *  Set isDeclined
	    * @param conn
	    * 		Db connection
	    * @param userId
	    * 		user id
	    * @throws SQLException
	    */
	    public static void setUserLastDecline(Connection con, long userId, boolean isDeclined, Transaction tran) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users " +
	                		 "SET is_declined = ? ";

	            if (isDeclined) {
	            	sql += ", last_decline_date = ? ";
	            }

	            sql += "WHERE id = " + userId;

	            pstmt = con.prepareStatement(sql);

	            pstmt.setInt(1, (isDeclined == true ? 1 : 0));

	            if (isDeclined) {
	            	pstmt.setTimestamp(2,CommonUtil.convertToTimeStamp(tran.getTimeSettled()));
	            }

	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    public static void setUserFirstDeclineTran(Connection con, long userId, Transaction tran) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users_active_data " + 
	            			 "SET first_decline_amount_usd = ? " +
	            			 "WHERE user_id = ? AND first_decline_amount_usd is null";
	            
	            double rate = TransactionsDAOBase.getRateById(con, tran.getId());
	            double amount = tran.getAmount() * rate;
	            
	            pstmt = con.prepareStatement(sql);
	            
	            pstmt.setDouble(1, amount);
	            pstmt.setLong(2, userId);
	            
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    public static void updateFirstDeclineAmount(Connection con, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users_active_data " + 
	            			 "SET first_decline_amount_usd = null " +
	            			 "WHERE user_id = ?";
	            
	            pstmt = con.prepareStatement(sql);
	            
	            pstmt.setLong(1, userId);
	            
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    public static void updateUserBubblesPricingLevel(Connection con, long userId, long bubblesLevel) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users_active_data " + 
	            			 "SET bubbles_pricing_level = ? " +
	            			 "WHERE user_id = ? ";
	            
	            pstmt = con.prepareStatement(sql);
	            
	            if (bubblesLevel == 0) {
	            	pstmt.setNull(1, Types.NUMERIC);
	            } else {
	            	pstmt.setLong(1, bubblesLevel);
	            }
	            pstmt.setLong(2, userId);
	            
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }

	    public static long getUserIdByUserName(Connection con, String name) throws SQLException {
			if (name == null || name.trim().equals("")) {
				return 0;
			}
			name = name.trim();
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
		        String sql =
	                "SELECT " +
	                    "id " +
	                "FROM " +
	                    "users " +
	                "WHERE " +
	                    "user_name = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1,name.toUpperCase());
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getLong("id");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}

	    public static String getUserNameById(Connection con, long userId) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
		        String sql =
	                "SELECT " +
	                    "user_name " +
	                "FROM " +
	                    "users " +
	                "WHERE " +
	                    " id = ?";
				ps = con.prepareStatement(sql);
				ps.setLong(1,userId);
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getString("user_name");
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return "";
		}

	    /**
		    *  Set Is Active
		    * @param conn
		    * 		Db connection
		    * @param userId
		    * 		user id
		    * @throws SQLException
		    */
	    public static void setIsActive(Connection conn, long userId, boolean isActive) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "update users " +
	                		 "set is_active = ? " +
	                		 "where id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setInt(1, isActive?1:0);
	            pstmt.setLong(2, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }

	    /**
		    *  Set false account
		    * @param conn - Db connection
		    * @param userId - user id
		    * @param isFalseAccount
		    * @throws SQLException
		    */
	    public static void setFalseAccount(Connection conn, long userId, boolean isFalseAccount) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "update users " +
	                		 "set is_false_account = ? " +
	                		 "where id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setInt(1, isFalseAccount?1:0);
	            pstmt.setLong(2, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }

	    /**
		    *  Set Disable Phone Contact
		    * @param conn
		    * 		Db connection
		    * @param userId
		    * 		user id
		    * @throws SQLException
		    */
	    public static void setDisablePhoneContact(Connection conn, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "update users " +
	                		 "set is_contact_by_phone = 0 " +
	                		 "where id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }

	    /**
		    *  Set Disable Phone Contact DAA
		    * @param conn
		    * 		Db connection
		    * @param userId
		    * 		user id
		    * @throws SQLException
		    */
	    public static void setDisablePhoneContactDAA(Connection conn, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE " +
	            				"users " +
	                		 "SET " +
	                		 	"is_contact_for_daa = 0 " +
	                		 "WHERE " +
	                		 	"id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }

	    /**
		    *  returns user id by user name
		    * @param conn - Db connection
		    * @param userName
		    * @throws SQLException
		    */
	    public static long getActiveUserIdByName(Connection conn, String userName) throws SQLException {
	    	PreparedStatement ps = null;
	   	    ResultSet rs = null;
	   	    try {
	            String sql =" SELECT " +
				                " id " +
				            " FROM " +
				                " users " +
				            " WHERE " +
				                " upper(USER_NAME) = ? " +
				                " and is_active = 1 ";
	   	        ps = conn.prepareStatement(sql);
	   	        ps.setString(1, userName.trim().toUpperCase());
	   	        rs = ps.executeQuery();

	   			if (rs.next()) {
	   			 	return rs.getLong("id");
	   			}
	   	    } finally {
	   	        closeResultSet(rs);
	   	        closeStatement(ps);
	   	    }
	        return 0;
	    }

	    /**
	     * Update is_autorized field after user made login via login authorization page
	     * @param con
	     * @param userId
	     * @throws SQLException
	     */
		public static void updateIsAuthrized(Connection con, String userName, int isAuthorized, Date timeFirstAuthorized) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql =
						" UPDATE " +
							" users " +
						" SET " +
							" is_authorized_mail = ? ";
				if (null != timeFirstAuthorized){
					sql +=  ", TIME_FIRST_AUTHORIZED = ? ";
				}

				sql +=
						" WHERE " +
							" UPPER(user_name) = ? ";

				ps = con.prepareStatement(sql);
				ps.setInt(1, isAuthorized);
				if (null != timeFirstAuthorized){
					ps.setTimestamp(2,CommonUtil.convertToTimeStamp(timeFirstAuthorized));
					ps.setString(3, userName.toUpperCase());
				}else {
					ps.setString(2, userName.toUpperCase());
				}

				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

	    /**
	     * Update email field by userId
	     * @param con
	     * @param userId
	     * @throws SQLException
	     */
		public static void updateEmail(Connection con, long userId, String email) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql =
						"UPDATE " +
							"users " +
						"SET " +
							"email = ? " +
						"WHERE " +
							"id = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, email);
				ps.setLong(2, userId);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		/**
		 * Check if there is existing user with the specified email.
		 *
		 * @param conn
		 *            the db connection to use
		 * @param email
		 *            the email to check
		 * @return <code>ture</code> if the email is in use else
		 *         <code>false</code>.
		 * @throws DAORuntimeException

		public static boolean isEmailInUse(Connection conn, String email, long userId) throws SQLException {
			boolean inUse = false;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			if (email.endsWith("anyoption.com") || email.endsWith("etrader.co.il")) {  // for testing
				return false;
			}
			try {
				String sql = "SELECT " +
								"id " +
							 "FROM " +
							 	"users " +
							 "WHERE " +
							 	"email = ? ";
				if (userId != 0) {
					sql += "AND id <> " + userId;
				}
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, email);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					inUse = true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(pstmt);
			}
			return inUse;
		}*/

	    public static boolean isAuthrizedEmail(Connection con, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        try {
	            String sql =
	                    "SELECT " +
	                        "is_authorized_mail " +
	                    "FROM " +
	                        "users " +
	                    "WHERE " +
	                        "id = ? ";

	            pstmt = con.prepareStatement(sql);
	            pstmt.setLong(1, userId);

	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	                if (rs.getInt("is_authorized_mail") == 1) {
	                	return true;
	                }
	            }
	        } finally {
	            closeStatement(pstmt);
	            closeResultSet(rs);
	        }
	        return false;
	    }

	    public static Long getFirstDepositId(Connection con, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        try {
	            String sql =
	                    "SELECT " +
	                        "first_deposit_id " +
	                    "FROM " +
	                        "users " +
	                    "WHERE " +
	                        "id = ? ";

	            pstmt = con.prepareStatement(sql);
	            pstmt.setLong(1, userId);

	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	               return rs.getLong("first_deposit_id");
	            }
	        } finally {
	            closeStatement(pstmt);
	            closeResultSet(rs);
	        }
	        return null;
	    }

	    /**
	     * is have approved deposits
	     * @param con db connection
	     * @param userId user id
	     * @return
	     * @throws SQLException
	     */

	    public static boolean isHaveApprovedDeposits(Connection con, long userId) throws SQLException {

	    	PreparedStatement ps = null;
	    	ResultSet rs = null;

	    	try {

	    		String sql = "SELECT " +
	    						"u.id " +
	    					 "FROM " +
	    					    "users u, transactions t, transaction_types tt, transaction_statuses ts " +
	    					 "WHERE " +
	    					 	"u.id = t.user_id AND t.type_id = tt.id " +
	    					 	"AND tt.class_type = ? AND u.id = ? " +
	    					 	"AND (ts.id = ? OR ts.id = ?) " +
	    					 	"AND (t.status_id = ? OR t.status_id = ?) ";


	    		ps = con.prepareStatement(sql);
	    		ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
	    		ps.setLong(2, userId);
	    		ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	    		ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_PENDING);
	    		ps.setLong(5, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	    		ps.setLong(6, TransactionsManagerBase.TRANS_STATUS_PENDING);

	    		rs = ps.executeQuery();

	    		if ( rs.next() ) {
	    			return true;
	    		}
	    	} finally {
	    		closeResultSet(rs);
	    		closeStatement(ps);
	    	}

	    	return false;
	    }
	    
    public static boolean isHaveApprovedDeposits(Connection con, long userId, boolean allDepositClassTypes) throws SQLException {

        if (!allDepositClassTypes) {
            return isHaveApprovedDeposits(con, userId);
        } else {

            PreparedStatement ps = null;
            ResultSet rs = null;

            try {

                String sql = "SELECT " +
                        "u.id " +
                     "FROM " +
                        "users u, transactions t, transaction_types tt, transaction_statuses ts " +
                     "WHERE " +
                        "u.id = t.user_id AND t.type_id = tt.id " +
                        "AND tt.class_type in ("+ TransactionsManagerBase.TRANS_CLASS_DEPOSIT + ", " 
                                                + TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS  + ", " 
                                                + TransactionsManagerBase.TRANS_CLASS_BONUS_DEPOSITS + ", " 
                                                + TransactionsManagerBase.TRANS_CLASS_DEPOSIT_BY_COMPANY + ") " +
                        "AND u.id = ? " +
                        "AND (ts.id = ? OR ts.id = ?) " +
                        "AND (t.status_id = ? OR t.status_id = ?) ";

                ps = con.prepareStatement(sql);
                ps.setLong(1, userId);
                ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
                ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
                ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
                ps.setLong(5, TransactionsManagerBase.TRANS_STATUS_PENDING);

                rs = ps.executeQuery();

                if (rs.next()) {
                    return true;
                }
            } finally {
                closeResultSet(rs);
                closeStatement(ps);
            }

            return false;
        }
    }    

		public static UserBase getUserByUserIdService(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
	    	ResultSet rs = null;
	    	UserBase vo = null;
			try {
				 String sql = "SELECT " +
					   		  	"* " +
					   		  "FROM " +
					   		  	"users u, users_active_data uad " +
					   		  "WHERE " +
						   		 "u.id = uad.user_id " +
						   		 "AND u.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

	    		if ( rs.next() ) {
	    			vo = new UserBase();
	    			getVOBasic(rs, vo);
	    		}
	    		return vo;
			} finally {
	    		closeResultSet(rs);
	    		closeStatement(ps);
	    	}
		}

		public static void updateUserDepNoInv24H(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = " UPDATE " +
							 "		users " +
							 " SET " +
							 "		dep_no_inv_24h = 0 " +
					   		 " WHERE " +
					   		 "		id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.executeUpdate();
			} finally {
	    		closeStatement(ps);
	    	}
		}

		public static long calculateUserRankById(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
	    	ResultSet rs = null;
			try {
				String sql =
						" SELECT " +
						"		ur.id as user_rank_id " +
				        " FROM ( " +
				        "       SELECT " +
				        "             sum((t.amount/100) * t.rate) as userAmount " +
				        "       FROM " +
				        "             transactions t, transaction_types tt " +
				        "       WHERE " +
				        "             t.user_id = ? " +
				        "             AND t.type_id = tt.id " +
				        "             AND tt.class_type = ? " +
				        "             AND t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") " +
				        "       ) tab, users_rank ur " +
				        " WHERE " +
				        "		(tab.userAmount >= ur.min_sum_of_deposits AND tab.userAmount < ur.max_sum_of_deposits) " +
				        "       OR (tab.userAmount >= ur.min_sum_of_deposits AND ur.max_sum_of_deposits is null) " ;

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setInt(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
				rs = ps.executeQuery();

	    		if ( rs.next() ) {
	    			return rs.getLong("user_rank_id");
	    		} else {
	    			return 0;
	    		}
			} finally {
	    		closeResultSet(rs);
	    		closeStatement(ps);
			}
		}

		public static void updateUserRank(Connection con, long userId, long userRank) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = " UPDATE " +
							 "		users u " +
							 " SET " +
							 "		rank_id = ? " +
					   		 " WHERE " +
					   		 "		u.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userRank);
				ps.setLong(2, userId);
				ps.executeUpdate();
			} finally {
	    		closeStatement(ps);
	    	}
		}

		public static void insertIntoUserRankHist(Connection con, long userId, long userRank, long transactionId) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = " INSERT INTO users_rank_hist (id,QUALIFICATION_DATE, TRANSACTION_ID, USER_ID, USER_RANK_ID) " +
							 " VALUES (seq_users_rank_hist.nextval, sysdate, ?,?,?) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, transactionId);
				ps.setLong(2, userId);
				ps.setLong(3, userRank);
				ps.executeUpdate();
			} finally {
	    		closeStatement(ps);
	    	}
		}

		public static void updateUserStatus(Connection con, long userId, long userStatus) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = " UPDATE " +
							 "		users u " +
							 " SET " +
							 "		status_id = ? " +
					   		 " WHERE " +
					   		 "		u.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userStatus);
				ps.setLong(2, userId);
				ps.executeUpdate();
			} finally {
	    		closeStatement(ps);
	    	}
		}

		public static void insertIntoUserStatusHist(Connection con, long userId, long userStatus) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql = " INSERT INTO users_status_hist (id,QUALIFICATION_DATE,  USER_ID, USER_STATUS_ID) " +
							 " VALUES (seq_users_status_hist.nextval, sysdate, ?,?) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, userStatus);
				ps.executeUpdate();
			} finally {
	    		closeStatement(ps);
	    	}

		}

		public static boolean isNewbies(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
	    	ResultSet rs = null;
			try {
				String sql = " SELECT " +
							 "		* " +
							 " FROM ( " +
							 "		SELECT " +
							 "			 u.first_deposit_id fdi " +
							 " 		FROM " +
							 "			 users u " +
							 "		WHERE u.id = ? " +
							 "		) tab, " +
							 "		transactions t " +
							 " WHERE t.id = tab.fdi " +
							 " AND t.time_created > sysdate - 7 " ;

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

	    		if ( rs.next() ) {
	    			return true;
	    		} else {
	    			return false;
	    		}
			} finally {
	    		closeResultSet(rs);
	    		closeStatement(ps);
			}
		}

		public static void updateUserBonusAbuser(Connection con, long userId, boolean isBonusAbuser) throws SQLException {
			PreparedStatement ps = null;
			int isBA = 0;
			if(isBonusAbuser) {
				isBA = 1;
			}
			try {
				String sql =
						"update users " +
						"set bonus_abuser = ? " +
						"where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setInt(1,isBA); // Set accepted terms to true
				ps.setLong(2,userId);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
		}

		public static void updateUserQuery(Connection con, long userId, String query) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql =
						"UPDATE" +
								" users " +
						"SET" +
							" decoded_source_query = ? " +
						"WHERE" +
							" id = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1,query);
				ps.setLong(2,userId);
				int k = ps.executeUpdate();
				System.out.println("updated:"+k);
			} finally {
				closeStatement(ps);
			}
		}

	    public static ArrayList<UserBase> getBaseUsersBySkinId(Connection con, long skinId)
			throws SQLException {
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    ArrayList<UserBase> users = new ArrayList<UserBase>();
	    try {
	        String sql =
	                "SELECT " +
	                    "* " +
	                "FROM " +
	                    "users u, " +
	                    "users_active_data uad " +
	                "WHERE " +
	                	"u.id = uad.user_id AND " +
	                    "skin_id = ? AND " +
	                    "is_active = 1 ";
	        ps = con.prepareStatement(sql);
	        ps.setLong(1, skinId);
	        rs = ps.executeQuery();

			while (rs.next()) {
				UserBase u = new UserBase();
	            getVOBasic(rs, u);
	            users.add(u);
			}
	    } finally {
	        closeResultSet(rs);
	        closeStatement(ps);
	    }
	    return users;
	}
   
    /**
     * change users skin or 1 user if we pass user param
     * @param con
     * @param fromSkinId which skin to change
     * @param toSkinId to what skin move the users to
     * @param user user id to change if we dont want to change all the users in the skin
     * @return number of users that was updated
     * @throws SQLException
     */
	public static int updateUsersSkin(Connection con, long fromSkinId, long toSkinId, String user) throws SQLException {
		PreparedStatement ps = null;
		int usersUpdated = 0;
		try {
			String sql =
					"UPDATE" +
						" users " +
					"SET" +
						" skin_id = ? " +
					"WHERE" +
						" skin_id = ? ";
				if (null != user) {
					sql += "AND id in (?) ";
				}

			ps = con.prepareStatement(sql);
			ps.setLong(1, toSkinId);
			ps.setLong(2, fromSkinId);
			if (null != user) {
				ps.setString(3, user);
			}
			usersUpdated = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return usersUpdated;
	}

	/**
	 * get 50 or less users that have skin id and no issue action with this comment and template id
	 * @param con
	 * @param skinId
	 * @param issueTemplateId issue action template id
	 * @param issueComment issue action comment and mailbox email subject
	 * @param numberOfUsers issue action comment
	 * @param languageId mailbox email language
	 * @return list of users
	 * @throws SQLException
	 */
	public static ArrayList<UserBase> getBaseUsersBySkinIdAndNoIssue(Connection con, long skinId, long issueTemplateId, String issueComment, long numberOfUsers,
			long languageId) throws SQLException {
	   PreparedStatement ps = null;
	   ResultSet rs = null;
	   ArrayList<UserBase> users = new ArrayList<UserBase>();
	   try {
	       String sql =
	               "SELECT " +
	               		"* " +
	               "FROM " +
	               		"users u, " +
	               		"users_active_data uad " +
	               "WHERE " +
	               		"u.id = uad.user_id AND " +
	               		"u.skin_id = ? AND " +
	               		"u.is_active = 1 AND " +
					    "NOT EXISTS (SELECT " +
										"1 " +
									"FROM " +
										"mailbox_users mu, " +
										"mailbox_templates mt " +
									"WHERE " +
										"mu.user_id = u.id AND " +
										"mu.template_id = mt.id AND " +
										"mt.skin_id = ? AND " +
										"mt.subject like ? " +
									") AND " +
					    "ROWNUM <= ? ";
	       ps = con.prepareStatement(sql);
	       ps.setLong(1, skinId);
	       ps.setLong(2, skinId);
	       ps.setString(3, issueComment);
	       ps.setLong(4, numberOfUsers);
	       rs = ps.executeQuery();
			while (rs.next()) {
				UserBase u = new UserBase();
				getVOBasic(rs, u);
				users.add(u);
			}
	   } finally {
	       closeResultSet(rs);
	       closeStatement(ps);
	   }
	   return users;
	}
	
	/**
     * additional updates for etrader skin user's timeBirthDate and user's streetNo
     * @param con
     * @param userBase
     * @throws SQLException
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
     */
	public static void updateAdditionalFieldsEtrader(Connection con, UserBase user)	throws SQLException, InvalidKeyException,
																					NoSuchAlgorithmException, NoSuchPaddingException,
																					IllegalBlockSizeException, BadPaddingException,
																					UnsupportedEncodingException,
																					InvalidAlgorithmParameterException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE" +
						" users " +
					" SET" +
						" time_birth_date = ? " +
					    ", street_no = ? " +
					    ", id_num = ? " +
					    ", id_num_back = ? " +
					    ", gender = ? "+					    
						", first_name = ? "+
						", last_name = ? "+
						", mobile_phone = ? "+
					" WHERE" +
					    " id = ? ";
			
			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(user.getTimeBirthDate()));
			ps.setString(2, user.getStreetNo());
			try {
				ps.setString(3, AESUtil.encrypt(user.getIdNum()));
			} catch (CryptoException ex) {
				log.error("When userIDNume encrypt ", ex);
			}
			ps.setString(4, user.getIdNum()); // id_num_back
			ps.setString(5, user.getGender());
			ps.setString(6, CommonUtil.capitalizeFirstLetters(user.getFirstName()));
			ps.setString(7, CommonUtil.capitalizeFirstLetters(user.getLastName()));
			ps.setString(8, user.getMobilePhone());
			ps.setLong(9, user.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	
	/**
     * check when the old user try to change his email, if this email already exist in DB
     * @param con
     * @param email
     * @return null if not suspend and user id if it is suspeded
     * @throws SQLException
     * @throws CryptoException
     */
	public static boolean isThisEmailExistInDB(Connection con,String email) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql ="SELECT * FROM users WHERE upper(users.email) = upper(?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, email);
			rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			} else{
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		} 
	}

	public static Map<Long,String> getPlatformsNames(Connection connections) throws SQLException {
		Map<Long,String> platforms = new  LinkedHashMap<Long,String>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			String sql =
					"SELECT " +
					"	* " +
					"FROM " +
					"	platforms " +
					"ORDER BY " +
					"	name";
			
			preparedStatement = connections.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				platforms.put(resultSet.getLong("id"), resultSet.getString("name"));
			}
		} finally {
			closeResultSet(resultSet);
			closeStatement(preparedStatement);
		}
		
		return platforms;
	}
	
    public static boolean hasTheSameInvestmentInTheLastSecSameIp(Connection conn, String ip, int sec, long oppId, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                        "SELECT " +
                            "id, " +
                            "((current_date - time_created)*86400) secBetween " +
                        "FROM " +
                            "investments " +
                        "WHERE " +
                            "ip like ? AND " +
                            "opportunity_id = ? AND " +
                            "current_date - time_created < ? /86400 ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, ip);
            pstmt.setLong(2, oppId);
            pstmt.setInt(3, sec);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                has = true;
                invRej.setSecBetweenInvestments(sec);
                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC_IP);
                invRej.setRejectAdditionalInfo("Same investment in less:, secBetweenInvestments: " + rs.getLong("secBetween") + " , market secBetweenInvestments: " + sec);
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return has;
    }
    
    
    public static ArrayList<MarkVipHelper> getUsersToMarkVip(Connection con) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	ArrayList<MarkVipHelper> list = new ArrayList<MarkVipHelper>();
		try {
			 String sql = "	SELECT " +
							      "u.id user_id, "
							    + "pu.curr_population_entry_id " +
							"FROM " +
							      "users u, " +
							      "users_active_data uad, "
							    + "population_users pu " +
							"WHERE " +
							      "uad.lifetime_volume > (SELECT " +
                                                              "e.value " +
                                                          "FROM " +
                                                              "enumerators e " +
                                                          "WHERE " +
                                                          	"e.code = 'lifetime volume for vip' " +
                                                          	"AND e.enumerator = 'lifetime volume for vip' " +
                                                          ") " +
							      "AND u.id = uad.user_id " +
							      "AND uad.vip_status_id = 1 " +
							      "AND NOT EXISTS (SELECT " +
				                                    "1 " +
				                                  "FROM " +
				                                    "users_regulation ur " +
				                                  "WHERE " +
				                                    "ur.suspended_reason_id > 0 " +
				                                    "AND ur.user_id = u.id " +
				                                  ") " +
							      "AND u.bonus_abuser = 0 " +
							      "AND NOT EXISTS (SELECT "
							      					+ "1 "
							      				+ "FROM "
							      					+ "copyop_frozen cf "
							      				+ "WHERE "
							      					+ "cf.user_id = u.id "
							      					+ "AND cf.is_frozen = 1"
							      				+ ") "
							     + "AND pu.user_Id = u.id ";


			ps = con.prepareStatement(sql);
			
			rs = ps.executeQuery();

    		while (rs.next()) {
    			list.add(new MarkVipHelper(rs.getLong("user_id"), 
    					rs.getLong("curr_population_entry_id")));
    		}
    		return list;
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
	}

	public static void updateVipStatus(Connection con, long userId, int vipStatusId) throws SQLException {
		PreparedStatement pstmt = null;
        try {
            String sql = "UPDATE "
            				+ "users_active_data " + 
            			 "SET "
            			 	+ "vip_status_id = ? " +
            			 "WHERE "
            			 	+ "user_id = ? ";
            
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, vipStatusId);
            pstmt.setLong(2, userId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
	}
	
	/**
	 * Get users to fire server pixel
	 * @param con
	 * @param fireServerPixelFields
	 * @return ArrayList<FireServerPixelHelper>
	 * @throws SQLException
	 */
	public static ArrayList<FireServerPixelHelper> getUsersToFireServerPixel(Connection con, FireServerPixelFields fireServerPixelFields) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<FireServerPixelHelper> list = new ArrayList<FireServerPixelHelper>();
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_server_pixels.get_register_info(o_register => ? "+
																   ",i_from_date => ? "+
																   ",i_to_date => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(fireServerPixelFields.getFromDate()));
			cstmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(fireServerPixelFields.getToDate()));
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				FireServerPixelHelper fireServerPixelHelper = new FireServerPixelHelper();
				fireServerPixelHelper.setUserId(rs.getLong("id"));
				fireServerPixelHelper.setPlatformId(rs.getLong("platform_id"));
				fireServerPixelHelper.setIdfa(rs.getString("idfa"));
				fireServerPixelHelper.setAdvertisingId(rs.getString("advertising_id"));
				list.add(fireServerPixelHelper);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return list;
	}
	
	public static ArrayList<Long> getSingleLoginUsers(Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> transactionIDs = new ArrayList<Long>();
		int index = 1;
		try {
			String sql = 
					"SELECT "
								+ " min(t.id) transaction_id "
					+ " FROM "	
								+ " transactions t " 
								+ " ,users u " 
								+ " ,transaction_types tt " 
					+ " WHERE "
					 		 	+ " u.id = t.user_id "
					 		 	+ " AND t.type_id = tt.id "
					 		 	+ " AND trunc(u.time_created) = trunc(sysdate)-7 "
					 		 	+ " AND trunc(u.time_created) = trunc(u.time_last_login) "
					 		 	+ " AND trunc(u.time_created) = trunc(t.time_created) "
					 		 	+ " AND tt.class_type = ? "
					 		 	+ " AND t.status_id in (?, ?, ?) "
					 		 	+ " AND not exists ( "
					 		 						+ " SELECT "
					 		 									+ " * "
					 		 						+ " FROM " 
					 		 									+ " risk_alerts ra "
					 		 						+ " WHERE " 
					 		 									+ " ra.transaction_id = t.id "
					 		 									+ " AND ra.type_id = ?)" 
					 + " GROUP BY u.id ";
			ps = conn.prepareStatement(sql);
			ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(index++, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			ps.setLong(index++, TransactionsManagerBase.TRANS_STATUS_PENDING);
			ps.setLong(index++, TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER);
			ps.setLong(index++, RiskAlertsManagerBase.RISK_TYPE_SINGLE_LOGIN);
			
			rs = ps.executeQuery();
			while(rs.next()) {
				Long transaction = rs.getLong("transaction_id");
				transactionIDs.add(transaction);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return transactionIDs;
	}
	
	public static User getUserByUsername(Connection conn, String username) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		User user = null;
		int index = 1;
		try {
			String sql = "{call PKG_USER.GET_USER_BY_USERNAME(O_DATA => ? " +
													  		  ",I_USERNAME => ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setString(index++, username);
			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				user = new User();
				user.setId(rs.getLong("id"));
				user.setUtcOffset(rs.getString("utc_offset"));
				}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return user;
	}
	
	public static void updateUserNonRegSuspend(Connection con, long userId, long suspendType) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_user.UPDATE_USER_SUSPEND(I_USER_ID => ? " +
																     ", I_IS_SUSPEND => ? )}");			
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, suspendType);
			
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}		
	}
} 