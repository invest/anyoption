package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_managers.RemarketingLoginsManagerBase;
import il.co.etrader.bl_vos.RemarketingLogin;

/**
 *
 * @author Oshik
 */
public class RemarketingLoginsDAOBase extends DAOBase {


	public static void insert(Connection conn, RemarketingLogin vo) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "insert into REMARKETING_LOGINS (id, user_id, contact_id, time_created, action_type, combination_id, dynamic_parameter) values " +
					 "(SEQ_REMARKETING_LOGINS.nextval,?,?,sysdate,?,?,?)";
		int index = 1;
		try{

			ps = conn.prepareStatement(sql);
			ps.setLong(index++, vo.getUserId());
			ps.setLong(index++, vo.getContactId());
			ps.setLong(index++, vo.getActionType());
			ps.setLong(index++, vo.getCombinationId());
			ps.setString(index++, vo.getDyanmicParameter());

			ps.executeUpdate();

			vo.setId(getSeqCurValue(conn, "SEQ_REMARKETING_LOGINS"));

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static void update(Connection conn, RemarketingLogin vo) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE remarketing_logins " +
					 " SET " +
					 " 		action_type = ? , " +
					 "		 user_id = ? " +
				 	 " WHERE    " +
				 	 "		 id = ? ";
		int index = 1;
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(index++, vo.getActionType());
			ps.setLong(index++, vo.getUserId());
			ps.setLong(index++, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static RemarketingLogin getById(Connection conn , long id) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT * " +
					 " FROM REMARKETING_LOGINS " +
					 " WHERE id = ? ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();

			if(rs.next()){
				RemarketingLogin vo = new RemarketingLogin();
				vo.setId(id);
				vo.setActionType(rs.getLong("ACTION_TYPE"));
				vo.setCombinationId(rs.getLong("COMBINATION_ID"));
				vo.setContactId(rs.getLong("CONTACT_ID"));
				vo.setDyanmicParameter(rs.getString("DYNAMIC_PARAMETER"));
				vo.setUserId(rs.getLong("USER_ID"));
				return vo;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}
	
	

	/**
	 * Update userId on remarkting login table only when creating a new user.
	 * Time period - in order to check if the new user saw a banner from remarketing and made a register from this time period (not in the same session)   
	 * @param conn
	 * @param userId
	 * @param email
	 * @param mobile
	 * @param landLinePhone
	 * @param timePeriod
	 * @return
	 * @throws SQLException
	 */
	public static int updateUserId(Connection conn, long userId, String email, String mobile, String landLinePhone, long skinId, long timePeriod) throws SQLException{
		PreparedStatement ps = null;	
		int rowsUpdated = 0;
		try {
			String sql =" UPDATE " +
						"	remarketing_logins " +
						" SET      " +
						"	user_id = ? " +
						" WHERE " +
						"	id IN ( SELECT " +
						"				rl.id " +
						"			FROM " +
						"				remarketing_logins rl LEFT JOIN contacts c ON rl.contact_id = c.id " +
						"			WHERE " +
						"				rl.action_type = ? " +
						"				AND rl.user_id = 0 " +
						"				AND (c.email = ? OR c.phone = ? OR c.phone = ? OR c.mobile_phone = ? OR c.land_line_phone = ?) " ;
			if (timePeriod > 0) {
				sql+=	" 				AND c.time_created > sysdate - ? ";
			}
				sql+= 	")";
			int index = 1;
			
			ps = conn.prepareStatement(sql);			
			ps.setLong(index++, userId);
			ps.setLong(index++, RemarketingLoginsManagerBase.REMARKETING_TYPE_CONTACT);
			ps.setString(index++, email);
			ps.setString(index++, mobile);
			ps.setString(index++, landLinePhone);
			ps.setString(index++, mobile);
			ps.setString(index++, landLinePhone);
			if (timePeriod > 0) {
				ps.setLong(index++, timePeriod);
			}
			rowsUpdated = ps.executeUpdate();			
		} finally {			
			closeStatement(ps);
		}
		return rowsUpdated;
	}
			
}