/**
 *
 */
package il.co.etrader.dao_managers;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.CustomerReportInvestments;
import il.co.etrader.bl_vos.CustomerReportProfitData;
import il.co.etrader.bl_vos.CustomerReportSent;
import il.co.etrader.bl_vos.CustomerReportState;
import il.co.etrader.bl_vos.CustomerReportTransactions;
import il.co.etrader.util.ConstantsBase;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.CommonUtil;
public class CustomerReportDAOBase extends DAOBase {
	
	private static final Logger log = Logger.getLogger(CustomerReportDAOBase.class);
	private static final String BLANK = "";

	public static ArrayList<CustomerReportInvestments> getAllInvestments(Connection conn, long reportId,long fromUserIdLimit, long lastUserIdLimit) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<CustomerReportInvestments> list = new ArrayList<CustomerReportInvestments>();
        long prevUserId = 0;
        try {
            String sql =
                "SELECT u.First_Name, u.last_name, " +
                	  " s.from_date, s.to_date, s.type as rep_type, " +
                	  " m.display_name as market_name, u.email, " +
                	  " d.* " +
                " FROM " +
					" customer_report_state s, " +
					" customer_report_inv_data d, " +
					" users u, " +
					" markets m " +
			    " WHERE s.id = d.report_id " +
					" and d.user_id = u.id " +
					" and d.market_id = m.id(+) " +
					" and s.state = " + CustomerReportState.CUSTOMER_REPORT_FINISH_ALL_INSERT +					
					" and s.id = " + reportId +
					" and d.user_id > " + fromUserIdLimit +
					" and d.user_id <= " + lastUserIdLimit +
					" and d.user_id not in (select user_id from customer_report_user_sent where is_sent in (1,2) and report_id = " + reportId + ") " +
				" ORDER BY u.id, d.time_created ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();          
            while (rs.next()) {
            	CustomerReportInvestments cr = getCustomerInvestmentReportVO(rs, conn);            	            
            	list.add(cr);
            	//add canceled after settled
            	if(cr.getReportGroup() >= CustomerReportInvestments.CUSTOMER_REPORT_GROUP_CANCEL_BO_AFTER_SETTLED 
            			&& cr.getReportGroup() <= CustomerReportInvestments.CUSTOMER_REPORT_GROUP_CANCEL_0100_AFTER_SETTLED){
            		list.add(getCustomerReportCanceledAfterSettled(cr));
            		//Minus 100 to get the inv. like not cancelled exmp.: 101 BO cancelled then 101-100 = 1 BO NO Cancel
            		cr.setReportGroup(cr.getReportGroup() - 100);
            	}
            	
            	if(cr.getUserId() != prevUserId){
            		//Add user Data info
            		list.add(getCustomerReportUserInfo(cr));
            	}            	
            	prevUserId = cr.getUserId();
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	
	
	public static ArrayList<Long> getAllUsersSorted(Connection conn, long reportId) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Long> list = new ArrayList<Long>();
        try {
            String sql =
                "SELECT distinct usr.user_id " +
                " FROM " + 
					" ( select distinct user_id " +
					"   from " + 
					" 	customer_report_inv_data inv " +
					" 	where inv.report_id = " + reportId +
					" union all " +
					" 	select distinct user_id " +
					"   from " + 
					" 	customer_report_tr_data tr " +
					" 	where tr.report_id = " + reportId +
					" ) usr  " +
					" WHERE usr.user_id not in (select user_id from customer_report_user_sent where is_sent in (1,2) and report_id = " + reportId + ") " + 
				" ORDER BY  usr.user_id";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();          
            while (rs.next()) {            	            
            	list.add(rs.getLong("user_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	private static CustomerReportInvestments getCustomerInvestmentReportVO(ResultSet rs, Connection conn) throws SQLException {
		CustomerReportInvestments vo = new CustomerReportInvestments();
		
		vo.setReportId(rs.getLong("report_id"));
		vo.setReportType(rs.getLong("rep_type"));
		
		vo.setUserId(rs.getLong("user_id"));
		vo.setFirstName(rs.getString("First_Name"));
		vo.setLastName(rs.getString("Last_Name"));
		vo.setEmail(rs.getString("email"));
		vo.setFromDate(convertToDate(rs.getTimestamp("from_date")));
		vo.setToDate(convertToDate(rs.getTimestamp("to_date")));
		vo.setTimeGenerate(convertToDate(rs.getTimestamp("time_insert")));
		
		if(rs.getString("balance_begin_date") != null){
			vo.setBalanceBeginDate(rs.getLong("balance_begin_date"));
		} else {
			vo.setBalanceBeginDate(0l);
		}		
		vo.setBalanceEndDate(rs.getLong("balance_end_date"));
		
		if(rs.getString("investment_id") == null){
			vo.setInvestmentId(0l);							
		} else {
			vo.setInvestmentId(rs.getLong("investment_id"));
			vo.setOpportunityType(rs.getLong("opportunity_type_id"));
			vo.setReportGroup(rs.getLong("rep_group"));
			vo.setMarketId(rs.getLong("market_id"));
			vo.setMarketName(rs.getString("market_name"));
			vo.setDirection(rs.getLong("direction"));
			
	        if (vo.getDirection() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE) {
	            OneTouchFields oneTouchFields = InvestmentsDAOBase.getOneTouchFields(conn, rs.getLong("opportunity_id"));
	            vo.setOneTouchDecimalPoint(oneTouchFields.getDecimalPoint());
	            vo.setOneTouchUpDown(oneTouchFields.getUpDown());
	        }
			
			if (rs.getInt("is_canceled") == 1){
				vo.setCanceled(true);
			} else {
				vo.setCanceled(false);
			}
			if (rs.getInt("is_settled") == 1){
				vo.setSettled(true);
			} else {
				vo.setSettled(false);
			}
			
			if(vo.getReportGroup() == CustomerReportInvestments.CUSTOMER_REPORT_GROUP_CANCEL_BEFORE_SETTLED){
				vo.setExpiryLevel(rs.getString("expiry_time_opp_or_0100"));
			} else {
				vo.setExpiryLevel(rs.getString("closing_level"));
			}
			vo.setPurchaseLevel(rs.getDouble("purchase_level"));
			vo.setExpiredDateTime(convertToDate(rs.getTimestamp("expiry_time")));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			if(rs.getString("time_canceled") != null){
				vo.setCanceledDateTime(convertToDate(rs.getTimestamp("time_canceled")));
			}
			vo.setAmount(rs.getLong("amount"));
			vo.setReturnAmount(rs.getLong("returns"));
			vo.setReturnAmountCanceledAfteSet(rs.getLong("returns_cancled_after_set"));
			vo.setReturnIfCorrectAmount(rs.getLong("return_if_correct"));
			vo.setReturnIfIncorrectAmount(rs.getLong("returns_if_incorrect"));
			if(rs.getString("comments") != null){
				vo.setComments(rs.getString("comments"));
			} else {
				vo.setComments("");
			}
			vo.setRfCommission(rs.getLong("roll_forward_commission"));
			vo.setTpCommission(rs.getLong("take_profit_commission"));
			vo.setOpFee(rs.getLong("option_plus_fee"));	
			vo.setOppCuretLevel(rs.getDouble("opp_current_level"));
			vo.setOppTimeEstClosing(convertToDate(rs.getTimestamp("opp_time_est_closing")));			
		}
		
        return vo;
    }
	
	private static CustomerReportInvestments getCustomerReportCanceledAfterSettled(CustomerReportInvestments cr){
		CustomerReportInvestments canceledInv = new CustomerReportInvestments(
				cr.getReportId(), cr.getReportType(), cr.getUserId(),
				cr.getFirstName(), cr.getLastName(), cr.getEmail(),cr.getFromDate(),
				cr.getToDate(), cr.getBalanceBeginDate(), cr.getBalanceEndDate(),
				cr.getInvestmentId(),cr.getOpportunityType(), cr.getReportGroup(),
				cr.getMarketId(), cr.getMarketName(), cr.getDirection(),
				cr.isCanceled(), cr.isSettled(), BLANK,
				cr.getPurchaseLevel(), cr.getCanceledDateTime(), cr.getTimeCreated(),
				cr.getCanceledDateTime(), 0, cr.getReturnAmountCanceledAfteSet(), cr.getReturnAmountCanceledAfteSet(),
				cr.getReturnIfCorrectAmount(), cr.getReturnIfIncorrectAmount(),
				"customer.report.10.canceled.inv.comments", cr.getRfCommission(), cr.getTpCommission(), cr.getOpFee(),
				cr.getTimeGenerate(), cr.getOppCuretLevel(),cr.getOppTimeEstClosing(), cr.getOneTouchUpDown(), cr.getOneTouchDecimalPoint());
		
		return canceledInv;
	}	
	
	private static CustomerReportInvestments getCustomerReportUserInfo(CustomerReportInvestments cr){
		CustomerReportInvestments userData = new CustomerReportInvestments(
				cr.getReportId(), cr.getReportType(), cr.getUserId(),
				cr.getFirstName(), cr.getLastName(), cr.getEmail(),cr.getFromDate(),
				cr.getToDate(), cr.getBalanceBeginDate(), cr.getBalanceEndDate(),
				0, 0 , 0,
				0, BLANK, 0,
				false, false, BLANK,
				0d, null, null,
				null, 0, 0, 0,
				0, 0,
				BLANK, 0, 0, 0,
				cr.getTimeGenerate(), 0d, null, 0, 0);
		
		return userData;
	}
	
	public static ArrayList<CustomerReportState> getLastReportState(Connection conn) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<CustomerReportState> list = new ArrayList<CustomerReportState>();
        try {
            String sql =
            		" select * " +
            		  " from customer_report_state " + 
            		  " where state != " + CustomerReportState.CUSTOMER_REPORT_FINISH_SEND_REPORTS +
            		  "	AND is_send_notification = 0 " +
            		  " order by id ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	CustomerReportState crs = new CustomerReportState();
            	crs.setReportId(rs.getLong("id"));
            	crs.setReportType(rs.getLong("type"));
            	crs.setReportState(rs.getLong("state"));
            	crs.setLogMsg(CommonUtil.clobToString(rs.getClob(("log_msg"))));
            	list.add(crs);              
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	
	
	public static void updateCustomerReportsIsSendErrNotification(Connection conn, long reportId) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE customer_report_state " +
						" SET is_send_notification = 1 " +
						" WHERE id = " + reportId ;
		try {
			ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            log.debug("Update CustomerReportState set send notification flag for reportId:" + reportId);
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateCustomerReportsIsSendReportsFinish(Connection conn, long reportId) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE customer_report_state " +
						" SET state = " + CustomerReportState.CUSTOMER_REPORT_FINISH_SEND_REPORTS  +
						" WHERE id = " + reportId ;
		try {
			ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            log.debug("Update CustomerReportState set send notification flag for reportId:" + reportId);
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateCustomerReportsJob(Connection conn, long lastRunTimeDays, long runInterval, long jobId) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE jobs " +
						" SET last_run_time = last_run_time + " + lastRunTimeDays +
						" , run_interval = " + runInterval +
						" WHERE id = " + jobId;
		try {
			ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            log.debug("Update jobs with ID " + jobId  + " and params lastRunTimeDays:" + lastRunTimeDays + " and lastRunTimeDays:" + lastRunTimeDays);
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void insertCustomerReportSent(Connection conn, long reportId, long userId, long isSent, String sentMsg) throws SQLException {
		PreparedStatement ps = null;
		String sql = "insert into customer_report_user_sent " +
						" (id, report_id, user_id, sent_msg, is_sent) " +
					 " values " + 
					    " (SEQ_CUSTOMER_REPORT_USER_SENT.Nextval, ?, ?, ?, ?) ";
		try {
			ps = conn.prepareStatement(sql);
            ps.setLong(1, reportId);
            ps.setLong(2, userId);
            ps.setCharacterStream(3,  new StringReader(sentMsg), sentMsg.length());
            ps.setLong(4, isSent);
            ps.executeUpdate();
            log.debug("Update CustomerReportState set send notification flag for reportId:" + reportId);
		} finally {
			closeStatement(ps);
		}
	}
	
	public static ArrayList<CustomerReportSent> getCustomerReportSendAll(Connection conn, long userId, long reportType, Date fromDate, Date toDate, long id, long isResent) throws SQLException {		

		PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<CustomerReportSent> list = new ArrayList<CustomerReportSent>();
        try {
            String sql =
                " SELECT crus.id, crus.report_id, decode(crs.type,3,3,1) as type, " +
            		" d.time_insert, to_char(crs.from_date,'dd/mm/yyyy')||'-'||to_char(crs.to_date,'dd/mm/yyyy') as period, " +
            		" crus.sent_msg, crus.comments, crus.user_id, crus.re_sent " +

				" FROM " +
					" customer_report_user_sent crus, " +
					" customer_report_state crs, " +
					" (select report_id, user_id, time_insert from  customer_report_inv_data group by report_id, user_id, time_insert)  d " +
				"WHERE crus.report_id = crs.id " +
					" and d.report_id = crus.report_id " +
					" and d.user_id = crus.user_id " +
					" and crus.user_id = ? " +
					" and trunc(crs.from_date) >= ? " +
					" and trunc(crs.to_date) <= ? ";
            if(id > 0){
            	sql = sql + 
            			" and crus.id = " + id;
            }
            if(isResent > -1){
            	sql = sql +
            			" and crus.re_sent = " + isResent;
            }
            if(reportType == ConstantsBase.CUSTOMER_REPORT_MONTHLY){
            	sql = sql + 
            			" and crs.type = 3 " +
            			" order by crus.id ";
            } else if(reportType == ConstantsBase.CUSTOMER_REPORT_FORTNIGHTLY){
            	sql = sql +
    					" and crs.type != 3 " +
    					" order by crus.id ";
            } else {
            	sql = sql + 
            			" order by crus.id desc";
            }
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(fromDate));
            pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(toDate));
            rs = pstmt.executeQuery();          
            while (rs.next()) {
            	CustomerReportSent cr = new CustomerReportSent();
            	cr.setId(rs.getLong("id"));
            	cr.setUserId(rs.getLong("user_id"));
            	cr.setReportId(rs.getLong("report_id"));
            	cr.setReportType(rs.getLong("type"));
            	cr.setOriginalDateSent(convertToDate(rs.getTimestamp("time_insert")));
            	cr.setPeriod(rs.getString("period"));
    			if (rs.getInt("re_sent") == 1){
    				cr.setReSent(true);
    			} else {
    				cr.setReSent(false);
    			}
            	cr.setSentMsg(CommonUtil.clobToString(rs.getClob(("sent_msg"))));
            	cr.setComments(rs.getString("comments"));
            	
            	list.add(cr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	
	
	public static void updateCustomerReportReSent(Connection conn, long id, String comments) throws SQLException {
		PreparedStatement ps = null;
		String sql = " UPDATE customer_report_user_sent " +
						" SET re_sent = 1  " + 
						" , comments = ? " +
						" WHERE id = ? " ;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, comments);
			ps.setLong(2, id);
            ps.executeUpdate();
            log.debug("Update CustomrReportSent with ID " + id  + " and comments:" + comments);
		} finally {
			closeStatement(ps);
		}
	}
	
	public static ArrayList<CustomerReportTransactions> getAllTransactions(Connection conn, long reportId, long fromUserIdLimit, long lastUserIdLimit) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<CustomerReportTransactions> list = new ArrayList<CustomerReportTransactions>();
        
        try {
            String sql =
                "SELECT u.First_Name, u.last_name, u.email, " + 
                		" s.from_date, s.to_date, s.type as rep_type, " +
                		" bal.balance_begin_date, " +
                		" bal.balance_end_date, " +
                		" tr.* " +
                " FROM " +
					" customer_report_state s, " +
					" customer_report_tr_data tr, " +
					" users u, " +
					" customer_report_user_balance bal " +
			    " WHERE " +
			    	" s.id = tr.report_id " +
			    	" and tr.user_id = u.id " +
			    	" and tr.user_id = bal.user_id(+) " +
			    	" and tr.report_id = bal.report_id(+) " +					
			    	" and s.state = " + CustomerReportState.CUSTOMER_REPORT_FINISH_ALL_INSERT +					
					" and s.id = " + reportId +
					" and tr.user_id > " + fromUserIdLimit +
					" and tr.user_id <= " + lastUserIdLimit +
					" and tr.user_id not in (select user_id from customer_report_user_sent where is_sent in (1,2) and report_id = " + reportId + ") " +
				" ORDER BY u.id, tr.tr_time_created ";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();          
            while (rs.next()) {
            	CustomerReportTransactions cr = getCustomerTransactionsReportVO(rs, conn);
            	
            	if(cr.getReportGroup() == CustomerReportTransactions.CUSTOMER_TRANSACTION_REPORT_DEPOSIT_CANCEL
            			|| cr.getReportGroup() == CustomerReportTransactions.CUSTOMER_TRANSACTION_REPORT_DEPOSIT_CHARGED
            				|| cr.getReportGroup() == CustomerReportTransactions.CUSTOMER_TRANSACTION_REPORT_REVERSE_WITHDRAW){
            		list.add(getCustomerTransactionsReportAdditionalRow(cr));
            	}
            	
            	list.add(cr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	
	private static CustomerReportTransactions getCustomerTransactionsReportVO(ResultSet rs, Connection conn) throws SQLException {
		CustomerReportTransactions vo = new CustomerReportTransactions();
		
		vo.setReportId(rs.getLong("report_id"));
		vo.setReportType(rs.getLong("rep_type"));
		
		vo.setUserId(rs.getLong("user_id"));
		vo.setFirstName(rs.getString("First_Name"));
		vo.setLastName(rs.getString("Last_Name"));
		vo.setEmail(rs.getString("email"));
		vo.setFromDate(convertToDate(rs.getTimestamp("from_date")));
		vo.setToDate(convertToDate(rs.getTimestamp("to_date")));
		vo.setTimeGenerate(convertToDate(rs.getTimestamp("time_insert")));
		
		vo.setBalanceBeginDate(rs.getLong("balance_begin_date"));
		vo.setBalanceEndDate(rs.getLong("balance_end_date"));
		
		vo.setTransactionId(rs.getLong("transaction_id"));
		vo.setReportGroup(rs.getLong("rep_group"));
		vo.setDateTime(convertToDate(rs.getTimestamp("date_time")));
		vo.setDescription(rs.getString("description"));
		vo.setDebit(rs.getLong("debit"));
		vo.setCredit(rs.getLong("credit"));
		vo.setComments(rs.getString("comments"));
		vo.setTransactionTimeCreated(convertToDate(rs.getTimestamp("tr_time_created")));
		if(rs.getString("tr_time_settled") != null){
			vo.setTransactionTimeSettled(convertToDate(rs.getTimestamp("tr_time_settled")));
		}		
		
        return vo;
    }
	
	private static CustomerReportTransactions getCustomerTransactionsReportAdditionalRow(CustomerReportTransactions cr){
		CustomerReportTransactions addRow = new CustomerReportTransactions(
				cr.getReportId(), cr.getReportType(), cr.getUserId(),
				cr.getFirstName(), cr.getLastName(), cr.getEmail(),cr.getFromDate(),
				cr.getToDate(), cr.getTimeGenerate(), cr.getBalanceBeginDate(), cr.getBalanceEndDate(),
				
				cr.getTransactionId(), cr.getReportGroup(),cr.getTransactionTimeCreated(),
				cr.getDescription(),cr.getCredit(), cr.getDebit(), "",
				cr.getTransactionTimeCreated(), cr.getTransactionTimeSettled()
				);
		
		return addRow;
	}
	
	public static HashMap<Long, CustomerReportProfitData> getProfitData(Connection conn, long reportId, long fromUserIdLimit, long lastUserIdLimit) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap<Long, CustomerReportProfitData> hm = new HashMap<Long, CustomerReportProfitData>();
        try {
            String sql =
                " SELECT * " +
                " FROM " +
            		" customer_report_profit_data " +
                " WHERE " +
                	" report_id = " + reportId +
					" and user_id > " + fromUserIdLimit +
					" and user_id <= " + lastUserIdLimit + 
			    " ORDER BY user_id ";		
            		
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();          
            while (rs.next()) {            	            
            	CustomerReportProfitData crp = new CustomerReportProfitData();
            	crp.setReportId(rs.getLong("report_id"));
            	crp.setReportType(rs.getLong("report_type"));
            	crp.setUserId(rs.getLong("user_id"));
            	crp.setLost(rs.getLong("lost"));
            	crp.setProfited(rs.getLong("profited"));
            	
            	hm.put(crp.getUserId(), crp);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return hm;
    }
	
	
	public static boolean isUserCloseByIssue(Connection conn, long  userId) throws SQLException {		
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean res = false;
        try {
            String sql = " select CUSTOMER_REPORTS.IS_USER_COLSED_BY_ISSUE(?) is_close from dual ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	res = rs.getBoolean("is_close");              
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return res;
    }
	
}
