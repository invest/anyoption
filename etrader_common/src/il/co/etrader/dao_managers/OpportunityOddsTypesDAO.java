package il.co.etrader.dao_managers;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.daos.OpportunityOddsTypesDAOBase;

import il.co.etrader.util.CommonUtil;

public class OpportunityOddsTypesDAO extends OpportunityOddsTypesDAOBase {

	public static ArrayList searchOddsTypes(Connection con) throws SQLException {
  	  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<OpportunityOddsType> list=new ArrayList<OpportunityOddsType>();
		  try
			{
			  String sql =" select * from opportunity_odds_types ";
			  ps = con.prepareStatement(sql);
			  rs=ps.executeQuery();
			  while (rs.next()) {
				  list.add(getOppOddsVO(rs));
			  }
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }
	public static OpportunityOddsType getByOppID(Connection con,long id) throws SQLException {
	  	  PreparedStatement ps=null;
			  ResultSet rs=null;

			  try
				{
				  String sql =" select od.* from opportunity_odds_types od,opportunities o where od.id=o.ODDS_TYPE_ID and o.id="+id;
				  ps = con.prepareStatement(sql);
				  rs=ps.executeQuery();
				  if (rs.next()) {
					  return getOppOddsVO(rs);
				  }
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return null;
		  }

	  public static void clearDefaultOddsType(Connection con) throws SQLException {

		  String sql="update opportunity_odds_types set is_default=0";

		  updateQuery(con,sql);

	  }

	  public static void insertOddsType(Connection con,OpportunityOddsType vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long id=0;
		  try {

				String sql="insert into opportunity_odds_types(id,NAME,OVER_ODDS_WIN,OVER_ODDS_LOSE,UNDER_ODDS_WIN,UNDER_ODDS_LOSE," +
						"IS_DEFAULT,TIME_CREATED,ODDS_WIN_DEDUCT_STEP) values(SEQ_OPPORTUNITY_ODDS_TYPES.nextval,?,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setFloat(2, vo.getOverOddsWin());
				ps.setFloat(3, vo.getOverOddsLose());
				ps.setFloat(4, vo.getUnderOddsWin());
				ps.setFloat(5, vo.getUnderOddsLose());
				ps.setInt(6, vo.getIsDefault());
				ps.setTimestamp(7, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
				ps.setFloat(8, vo.getOddsWinDeductStep());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"SEQ_OPPORTUNITY_ODDS_TYPES"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  public static void updateOddsType(Connection con,OpportunityOddsType vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			    String sql=" update opportunity_odds_types set NAME=?,OVER_ODDS_WIN=?,OVER_ODDS_LOSE=?," +
			    		" UNDER_ODDS_WIN=?,UNDER_ODDS_LOSE=?,IS_DEFAULT=?,TIME_CREATED=?,ODDS_WIN_DEDUCT_STEP=? where id=? ";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setFloat(2, vo.getOverOddsWin());
				ps.setFloat(3, vo.getOverOddsLose());
				ps.setFloat(4, vo.getUnderOddsWin());
				ps.setFloat(5, vo.getUnderOddsLose());
				ps.setInt(6, vo.getIsDefault());
				ps.setTimestamp(7, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
				ps.setFloat(8, vo.getOddsWinDeductStep());
				ps.setLong(9, vo.getId());

				ps.executeUpdate();

		  }
			finally
			{
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }
}