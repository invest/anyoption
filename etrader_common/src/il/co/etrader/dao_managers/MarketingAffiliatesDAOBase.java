package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingSubAffiliate;


public class MarketingAffiliatesDAOBase extends DAOBase {


	  /**
	   * Get affiliate instance by key
	   * @param con db connection
	   * @param affiliateKey
	   * @return  affiliate instance
	   * @throws SQLException
	   */
	   public static MarketingAffilate getAffiliateByKey(Connection con, long affiliateKey) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingAffilate ma = new MarketingAffilate();

		  try {
			    String sql = "SELECT " +
			    				"ma.id, ma.site_link " +
			    			 "FROM " +
			    			 	"marketing_affilates ma " +
			    			 "WHERE ma.key = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, affiliateKey);

				rs = ps.executeQuery();

				if (rs.next()) {
					ma.setId(rs.getLong("id"));
					ma.setAffiliatePixels(MarketingPixelsDAOBase.getPixelsByAffiliateId(con, ma.getId()));
					ma.setSiteLink(rs.getString("site_link"));
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return ma;
	   }


		  /**
		   * Get subAffiliate instance by key
		   * @param con db connection
		   * @param subAffiliateKey
		   * @return  affiliate instance
		   * @throws SQLException
		   */
		   public static MarketingSubAffiliate getSubAffiliateByKey(Connection con, long subAffiliateKey) throws SQLException {

		   	  PreparedStatement ps = null;
			  ResultSet rs = null;
			  MarketingSubAffiliate msa = new MarketingSubAffiliate();

			  try {
				    String sql = "SELECT " +
				    				"msa.id, " +
				    				"msa.affiliate_id " +
				    			 "FROM " +
				    			 	"marketing_sub_affiliates msa " +
				    			 "WHERE msa.key = ? ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, subAffiliateKey);

					rs = ps.executeQuery();

					if (rs.next()) {
						msa.setId(rs.getLong("id"));
						msa.setAffiliateId(rs.getLong("affiliate_id"));
						msa.setSubAffiliatePixels(MarketingPixelsDAOBase.getPixelsBySubAffiliateId(con, msa.getId()));
					}
				}
				finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return msa;
		   }


		  /**
		   * Get VO
		   * @param rs
		   * 	Result set instance
		   * @return
		   * 	MarketingAffilate object
		   * @throws SQLException
		   */
		  protected static MarketingAffilate getVO(ResultSet rs) throws SQLException {
			  MarketingAffilate vo = new MarketingAffilate();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setKey(rs.getString("key"));
				vo.setWriterId(rs.getLong("writer_id"));

				return vo;
		  }
		  
		  /**
			 * Get All Marketing Affiliates keys and name to HM
			 * @param con
			 * @return
			 * @throws SQLException
			 */
			public static HashMap<Long, MarketingAffilate> getAffiliatesHM(Connection con) throws SQLException{

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  HashMap<Long, MarketingAffilate> hm = new HashMap<Long, MarketingAffilate>();
			  try {
					String sql = "SELECT " +
									"ma.* " +
								 "FROM " +
								 	"marketing_affilates ma " +
								 "ORDER BY " +
								 	"name";

					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()) {
						MarketingAffilate ma = new MarketingAffilate();
						ma.setId(rs.getLong("id"));
						ma.setName(rs.getString("name"));
						ma.setKey(rs.getString("key"));
						ma.setWriterId(rs.getLong("writer_id"));
						ma.setAllowAffilate(rs.getBoolean("allow_registration"));
						hm.put(new Long(rs.getLong("key")),ma);
					}
			  } finally {
				  closeResultSet(rs);
				  closeStatement(ps);
			  }
			  return hm;
			}
			
			/**
			   * Get affiliate instance by user id
			   * @param con db connection
			   * @param user id
			   * @return  affiliate instance
			   * @throws SQLException
			   */
			   public static MarketingAffilate getAffiliateByUserId(Connection con, long userId) throws SQLException {

			   	  PreparedStatement ps = null;
				  ResultSet rs = null;
				  MarketingAffilate ma = new MarketingAffilate();

				  try {
					    String sql = "SELECT " +
					    				"ma.id " +
					    			 "FROM " +
					    			 	"marketing_affilates ma, "
					    			  + "users u " +
					    			 "WHERE "
					    			  + "ma.key = u.affiliate_key "
					    			  + "AND u.id  = ? ";

						ps = con.prepareStatement(sql);
						ps.setLong(1, userId);

						rs = ps.executeQuery();

						if (rs.next()) {
							ma.setId(rs.getLong("id"));
							ma.setAffiliatePixels(MarketingPixelsDAOBase.getPixelsByAffiliateId(con, ma.getId()));
						}
					}
					finally {
						closeResultSet(rs);
						closeStatement(ps);
					}
					return ma;
			   }
			   
			   

}

