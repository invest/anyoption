package il.co.etrader.dao_managers;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.jobs.AutomaticMailTemplate5DepositMoreBean;
import il.co.etrader.jobs.AutomaticMailTemplateMaintenanceFeeBean;

public class JobsDAOBase extends com.anyoption.common.daos.JobsDAOBase {
    private static final int LOAD_LIMIT = 1000;
    
    private static List<UserBase> getUsersListForDocumentRequestAutomaticMail(Connection conn, String view) throws SQLException {
        String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    view +
                " WHERE " +
                    "rownum <= " + LOAD_LIMIT;
        List<UserBase> usersList = new LinkedList<UserBase>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                UserBase u = new UserBase();
                u.setId(rs.getLong("id"));
                u.setUserName(rs.getString("user_name"));
                u.setEmail(rs.getString("email"));
                u.setIsContactByEmail(rs.getInt("is_contact_by_email"));
                usersList.add(u);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return usersList;
    }

    public static List<UserBase> getUsersListForDocumentRequest1AutomaticMail(Connection conn) throws SQLException {
        return getUsersListForDocumentRequestAutomaticMail(conn, "USERS_FOR_DOC_REQ1");
    }

    public static List<UserBase> getUsersListForDocumentRequest2AutomaticMail(Connection conn) throws SQLException {
        return getUsersListForDocumentRequestAutomaticMail(conn, "USERS_FOR_DOC_REQ2");
    }

    public static List<UserBase> getUsersListForDocumentRequest3AutomaticMail(Connection conn) throws SQLException {
        return getUsersListForDocumentRequestAutomaticMail(conn, "USERS_FOR_DOC_REQ3");
    }

    public static List<UserBase> getUsersListForDocumentRequest4AutomaticMail(Connection conn) throws SQLException {
        return getUsersListForDocumentRequestAutomaticMail(conn, "USERS_FOR_DOC_REQ4");
    }

	public static	List<AutomaticMailTemplate5DepositMoreBean>
			getUsersListFor5DepositMoreAutomaticMail(Connection conn)	throws SQLException, CryptoException, InvalidKeyException,
																		IllegalBlockSizeException, BadPaddingException,
																		NoSuchAlgorithmException, NoSuchPaddingException,
																		InvalidAlgorithmParameterException {
        String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "USR_FOR_5DEP_MORE " +
                "WHERE " +
                    "rownum <= " + LOAD_LIMIT;
        List<AutomaticMailTemplate5DepositMoreBean> usersList = new LinkedList<AutomaticMailTemplate5DepositMoreBean>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                AutomaticMailTemplate5DepositMoreBean b = new AutomaticMailTemplate5DepositMoreBean();
                b.setId(rs.getLong("id"));
                b.setUserName(rs.getString("user_name"));
                b.setEmail(rs.getString("email"));
                b.setIsContactByEmail(rs.getInt("is_contact_by_email"));
                b.setTransactionId(rs.getLong("transaction_id"));
                b.setTransactionPan(AESUtil.decrypt(rs.getString("cc_number")));
                b.setTransactionPan(b.getTransactionPan().substring(b.getTransactionPan().length() - 4));
                usersList.add(b);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return usersList;
    }

    public static List<UserBase> getUsersListForMaintenanceFee(Connection conn, String view) throws SQLException {
        String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    view +
                " WHERE " +
                    "rownum <= " + LOAD_LIMIT;
        List<UserBase> usersList = new LinkedList<UserBase>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                AutomaticMailTemplateMaintenanceFeeBean u = new AutomaticMailTemplateMaintenanceFeeBean();
                u.setId(rs.getLong("user_id"));
                u.setUserName(rs.getString("username"));
                u.setEmail(rs.getString("email"));
                u.setIsContactByEmail(rs.getInt("is_contact_by_email"));
                u.setFee(rs.getLong("fee"));
                usersList.add(u);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return usersList;
    }
    
    public static void collectMaintenanceFee(Connection conn) throws SQLException {
        CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN transactions_manager.maintenance_fee(); END;");
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
    }
}