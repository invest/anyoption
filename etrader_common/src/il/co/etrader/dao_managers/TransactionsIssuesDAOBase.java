package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;


public class TransactionsIssuesDAOBase extends DAOBase{

	  private static final Logger logger = Logger.getLogger(TransactionsIssuesDAOBase.class);

	  public static boolean isIssueIsInTiTable(Connection con, long issueId) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " SELECT " +
								" * " +
							 " FROM " +
							 	" transaction_issues ti, " +
							 	" issues_actions ia " +
							 " WHERE " +
							 	" ti.issue_action_id = ia.id " +
							 	" and ia.issue_id = " + issueId;

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				if (rs.next()){
					return true;
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
		}


		/**
		 * Insert Transaction Issues record
		 * @param tranId transactionId
		 * @param actionId transactionId
		 * @throws SQLException
		 */
		public static void insertIntoTi(Connection con, long tranId, long actionId, 
				int tran_issue_insert_type, long writerId, int isFirstDeposit) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				String sql = " INSERT INTO transactions_issues(transaction_id, issue_action_id,callers_num, time_inserted,insert_type, transactions_refference_id, writer_id, is_first_deposit) " +
			                 " VALUES(?, ?, (select count(distinct(ia.writer_id)) " +
							               " from   issue_actions ia " +
							               " where ia.issue_id = (select ia2.issue_id " +
							              					    " from issue_actions ia2 " +
							              					    " where ia2.id = ?)), sysdate,?, ?, ?, ?)";
				ps = con.prepareStatement(sql);
				ps.setLong(1, tranId);
				ps.setLong(2, actionId);
				ps.setLong(3, actionId);
				ps.setInt(4, tran_issue_insert_type);
				ps.setLong(5, tranId);
				ps.setLong(6, writerId);
				ps.setInt(7, isFirstDeposit);

				ps.executeUpdate();

			} finally {
					closeStatement(ps);
					closeResultSet(rs);
			}
		}

		/**
		 * Search matching transaction for user's issue action
		 * @param userId
		 * @throws SQLException
		 */
		public static long getTransactionForActionByUserId(Connection con, long userId, long issueId) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " SELECT " +
								" t.* " +
				             " FROM " +
				             	" transactions t , " +
				             	" transaction_types tt, " +
				             	" users u, " +
				             	" skins s " +
				             " WHERE " +
				             	" u.id = ? " +
				             	" and t.user_id = u.id " +
				             	" and s.id = u.skin_id " +
				             	" and t.type_id = tt.id " +
				             	" and tt.class_type = 1 " + // -- All deposite
				                " and t.status_id in (2,7,8) " + // -- Succeed Deposits\ Pending and canceled
				                " and t.time_created > sysdate - s.deposit_during_call_days " +
				                " and not exists (SELECT *  " + // -- Check that this issue doesn't exist it transactions_issues table
				                                " FROM transactions_issues ti, issue_actions ia " +
				                                " WHERE ti.issue_action_id = ia.id " +
				                                	" and ia.issue_id = ?) " +
				             " ORDER BY t.id desc";

				ps = con.prepareStatement(sql);

				ps.setLong(1, userId);
				ps.setLong(2, issueId);
				rs = ps.executeQuery();

				if (rs.next()){
					return rs.getLong("id");
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}

		/**
		 * Search matching transaction for user's issue action
		 * @param userId
		 * @throws SQLException
		 */
		public static long getTransactionForActionByUserIdAndSkins(Connection con, long userId, long issueId) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " SELECT " +
								" t.* " +
				             " FROM " +
				             	" transactions t , " +
				             	" transaction_types tt, " +
				             	" users u, " +
				             	" skins s " +
				             " WHERE " +
				             	" u.id = ? " +
				             	" and t.user_id = u.id " +
				             	" and s.id = u.skin_id " +
				             	" and t.type_id = tt.id " +
				             	" and tt.class_type = 1 " + // -- All deposite
				                " and t.status_id in (2,7,8) " + // -- Succeed Deposits\ Pending and canceled
				                " and not exists (SELECT *  " + // -- Check that this issue doesn't exist it transactions_issues table
				                                " FROM transactions_issues ti, issue_actions ia " +
				                                " WHERE ti.issue_action_id = ia.id " +
				                                	" and ia.issue_id = ?) " +
				             " ORDER BY t.id desc";

				ps = con.prepareStatement(sql);

				ps.setLong(1, userId);
				ps.setLong(2, issueId);
				rs = ps.executeQuery();

				if (rs.next()){
					return rs.getLong("id");
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return 0;
		}

		
		
}
