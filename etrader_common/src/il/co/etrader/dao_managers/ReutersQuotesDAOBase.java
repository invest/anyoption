package il.co.etrader.dao_managers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.util.CommonUtil;

/**
 * ReutersQuotes DAO.
 *
 * @author Eyal G
 */
public class ReutersQuotesDAOBase extends DAOBase {

	/*public static void insert(Connection conn, Opportunity o, ReutersQuotes lastUpdateQuote) throws SQLException {
		insert(conn, o.getId(), lastUpdateQuote);
	}*/

	public static void insert(Connection conn, long oppId, ReutersQuotes lastUpdateQuote) throws SQLException {
		PreparedStatement ps = null;
        try {
            String sql =
            	"INSERT INTO " +
            	"	reuters_quotes (" +
            						"	id, " +
            						"	opportunity_id, " +
            						"	time, " +
            						"	last, " +
            						"	ask, " +
            						"	bid, " +
            						"	time_created " +
            						") " +
            	" VALUES ( " +
            			" seq_reuters_quotes.nextval, " +
            			" ?, " +
            			" ?, " +
            			" ?, " +
            			" ?, " +
            			" ?, " +
            			" sysdate) ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, oppId);
            ps.setTimestamp(2, CommonUtil.convertToTimeStamp(lastUpdateQuote.getTime()));
            if (lastUpdateQuote.getLast() != null && lastUpdateQuote.getLast().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(3, lastUpdateQuote.getLast());
            } else {
            	ps.setNull(3, Types.NUMERIC);
            }
            if (lastUpdateQuote.getAsk() != null && lastUpdateQuote.getAsk().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(4, lastUpdateQuote.getAsk());
            } else {
            	ps.setNull(4, Types.NUMERIC);
            }
            if (lastUpdateQuote.getBid() != null && lastUpdateQuote.getBid().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(5, lastUpdateQuote.getBid());
            } else {
            	ps.setNull(5, Types.NUMERIC);
            }
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
	}

	public static boolean update(Connection con, long oppId, ReutersQuotes lastUpdateQuote) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update reuters_quotes set last = ?, ask = ?, bid = ? "
							+ "where opportunity_id = ?";
			ps = con.prepareStatement(sql);
			if (lastUpdateQuote.getLast() != null && lastUpdateQuote.getLast().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(1, lastUpdateQuote.getLast());
            } else {
            	ps.setNull(1, Types.NUMERIC);
            }
            if (lastUpdateQuote.getAsk() != null && lastUpdateQuote.getAsk().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(2, lastUpdateQuote.getAsk());
            } else {
            	ps.setNull(2, Types.NUMERIC);
            }
            if (lastUpdateQuote.getBid() != null && lastUpdateQuote.getBid().compareTo(BigDecimal.ZERO) > 0) {
            	ps.setBigDecimal(3, lastUpdateQuote.getBid());
            } else {
            	ps.setNull(3, Types.NUMERIC);
            }
            ps.setLong(4, oppId);
			return ps.executeUpdate() > 0;
		} finally {
			closeStatement(ps);
		}
	}
}