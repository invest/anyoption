package il.co.etrader.dao_managers;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.WritersDAOBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.RankStatus;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WriterMaxAssignment;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class WritersDAO extends WritersDAOBase {

 	public static Writer getByUserName(Connection con,String name)  throws SQLException{

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  Writer vo = null;
		  try {
			    String sql="select * from writers where user_name=?";

				ps = con.prepareStatement(sql);
				ps.setString(1, name.toUpperCase());

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=getVO(con,rs);
					vo.setGroupId(rs.getLong("group_id"));
				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;

 	}

 	public static ArrayList getAll(Connection con)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList list=new ArrayList();
		  try
			{
			    String sql="select * from writers ";

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(getVO(con,rs));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	}
 	public static Writer get(Connection con,long id)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  Writer vo=null;
		  try
			{
			    String sql="select * from writers where id=?";
				ps = con.prepareStatement(sql);
				ps.setLong(1, id);
				rs=ps.executeQuery();
				if (rs.next()) {
					vo=getVO(con,rs);
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	}

    private static Writer getVO(Connection con,ResultSet rs) throws SQLException{
    	Writer vo=new Writer();

    	vo.setId(rs.getLong("id"));
		vo.setUserName(rs.getString("USER_NAME"));
		vo.setPassword(rs.getString("PASSWORD"));
		vo.setFirstName(rs.getString("FIRST_NAME"));
		vo.setLastName(rs.getString("LAST_NAME"));
		vo.setStreet(rs.getString("STREET"));
		vo.setStreetNo(rs.getString("STREET_no"));
		vo.setCityId(rs.getString("CITY_ID"));
		vo.setZipCode(rs.getString("ZIP_CODE"));
		vo.setEmail(rs.getString("EMAIL"));
		vo.setComments(rs.getString("COMMENTS"));
		vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
		vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
		vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
		vo.setIsActive(rs.getInt("IS_ACTIVE"));
		vo.setSupportEnable(1==rs.getInt("IS_SUPPORT_ENABLE"));
		vo.setDeptId(rs.getInt("DEPT_ID"));
		vo.setSales_type(rs.getInt("sales_type"));
		vo.setNickNameFirst(rs.getString("NICK_NAME_FIRST"));
		vo.setNickNameLast(rs.getString("NICK_NAME_LAST"));
		vo.setRoleId(rs.getLong("ROLE_ID"));
		vo.setUtcOffset(rs.getString("utc_offset"));
		vo.setPasswordReset(rs.getLong("is_password_reset") > 0 ? true : false);
		return vo;
    }
    public static HashMap getAllMap(Connection con)  throws SQLException{

    	  HashMap map=new HashMap();
		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try
			{
			    String sql="select * from writers";
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					Writer vo=getVO(con,rs);
					map.put(String.valueOf(vo.getId()), vo.getUserName());
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return map;
	}

    public static HashMap<Long,Writer> getAllWritersMap(Connection con)  throws SQLException{

    	  HashMap<Long,Writer> map=new HashMap<Long,Writer>();
		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try
			{
			    String sql="select * from writers";
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					Writer vo=getVO(con,rs);
					map.put(vo.getId(), vo);
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return map;
	}


    public static void updatePassword(Connection con, Writer writer) throws SQLException {
    	PreparedStatement ps = null;

		try {
			String sql =
					"update WRITERS set PASSWORD=?, IS_PASSWORD_RESET = 0 where ID=?";

			ps = con.prepareStatement(sql);

			ps.setString(1,writer.getPassword().toUpperCase());
			ps.setLong(2,writer.getId());

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}

	}

	public static ArrayList<Integer> getWriterSkins(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList list = new ArrayList();
		  try
			{
			    String sql="select skin_id from writers_skin where writer_id="+writerId;

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new Integer(rs.getInt("skin_id")));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

	public static ArrayList<SelectItem> getWriterSkinsSI(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try
			{
			    String sql="SELECT s.id id, s.display_name display_name FROM skins s, writers_skin ws WHERE s.id=ws.skin_id AND ws.writer_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, writerId);
				
				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(rs.getInt("id"),rs.getString("display_name")));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}
	
	
	public static ArrayList<SelectItem> getWriterSkinsNotRegulatedSI(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try
			{
			    String sql="SELECT s.id id, s.display_name display_name FROM skins s, writers_skin ws WHERE s.id=ws.skin_id AND s.is_regulated=0 AND ws.writer_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, writerId);
				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(rs.getInt("id"),rs.getString("display_name")));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}
	
	public static ArrayList<SelectItem> getWriterSkinsRegulatedSI(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try
			{
			    String sql="select s.id id, s.display_name display_name from skins s, writers_skin ws where s.id=ws.skin_id and s.is_regulated=1 and ws.writer_id="+writerId;

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(rs.getInt("id"),rs.getString("display_name")));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

	public static ArrayList<SelectItem> getWriterCurrenciesSI(Connection con,long writerId, Locale locale)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  list.add(new SelectItem(new Long(ConstantsBase.CURRENCY_ALL_ID),CommonUtil.getMessage(ConstantsBase.CURRENCY_ALL ,null, locale)));
		  try
		  {
			    String sql="select c.id, c.display_name from currencies c, writers_skin ws, skin_currencies sc  where c.id=sc.currency_id and ws.skin_id=sc.skin_id and ws.writer_id=" + writerId +" group by c.id, c.display_name order by c.id";
				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(rs.getLong("id"),CommonUtil.getMessage(rs.getString("display_name"),null, locale)));
				}
			}

			finally
			{
	        	closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

	public static ArrayList<Long> getWriterLanguages(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList list = new ArrayList();
		  try
			{
			    String sql="SELECT sl.language_id FROM writers w, writers_skin ws, skin_languages sl WHERE w.id=ws.writer_id and ws.skin_id=sl.skin_id and writer_id="+writerId;

				ps = con.prepareStatement(sql);

				rs=ps.executeQuery();

				while (rs.next()) {
					list.add(new Long(rs.getLong("language_id")));
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

	public static String getWriterUtcOffset(Connection con,long writerId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  String utcOffset = ConstantsBase.EMPTY_STRING;
		  try
			{
			    String sql="select utc_offset from writers w where w.id = ? "; //+ writerId;

				ps = con.prepareStatement(sql);
			    ps.setLong(1, writerId);

				rs=ps.executeQuery();

				if (rs.next()) {
					utcOffset = rs.getString("utc_offset");
				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return utcOffset;
	}

	/**
	 * Get overView overAll writers details
	 * @param con
	 * @return
	 * @throws SQLException
	 */
 	public static ArrayList<Writer> getWritersOverAllView(Connection con, long skinId, long writerFilter, String skins, long salesType) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Writer> list = new ArrayList<Writer>();

		  try {
			    String sql =
			    	"SELECT " +
				        "w.user_name, " +
				        "pu.curr_assigned_writer_id, " +
				        "SUM(CASE WHEN pu.ENTRY_TYPE_ID = 1  THEN 1 ELSE 0 END ) as num_of_ass, " +
				        "SUM(CASE WHEN pu.ENTRY_TYPE_ID = 2 THEN 1 ELSE 0 END ) as num_of_trac, " +
				        "SUM(CASE WHEN pu.ENTRY_TYPE_ID = 3 THEN 1 ELSE 0 END ) as num_of_cb " +
					"FROM " +
					    "writers w, " +
					    "population_users pu " +
				   "WHERE " +
					    getPartialQueryOverview(skinId, writerFilter, skins, salesType);
				  sql +=
					 "GROUP BY  " +
						"w.user_name, " +
						"pu.curr_assigned_writer_id ";
				  
				  sql += "ORDER BY upper(w.user_name)";

				ps = con.prepareStatement(sql);
				ps.setString(1, ConstantsBase.ROLE_RETENTION);
				ps.setString(2, ConstantsBase.ROLE_RETENTIONM);

				rs = ps.executeQuery();

				while (rs.next()) {
					Writer w = new Writer();
					w.setUserName(rs.getString("user_name"));
					w.setId(rs.getLong("curr_assigned_writer_id"));
					w.setGeneralAssignedRecords(rs.getLong("num_of_ass"));
					w.setTrackingAssignedRecords(rs.getLong("num_of_trac"));
					w.setCallBacksAssignedRecords(rs.getLong("num_of_cb"));
					list.add(w);
				}
			} finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}

	/**
	 * Get overView today writers details
	 * @param con
	 * @return
	 * @throws SQLException
	 */
 	public static ArrayList<Writer> getWritersTodayView(Connection con, long skinId, long writerFilter, String skins) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Writer> list = new ArrayList<Writer>();
		  try {
			    String sql =
				    	" SELECT " +
					        " w.user_name, " +
					        " w.id writer_id, " +
					        " count(ent_calls.entry_id) as num_ass, " +
					        " sum(case when ent_calls.reached_calls > 0 then 1 else 0 end) as called_reached, " +
					        " sum(case when ent_calls.reached_calls = 0 " +
					        		  " and ent_calls.not_reached_calls > 0 then 1 else 0 end) as called_not_reached, " +
					        " sum(case when ent_calls.reached_calls = 0 " +
					        		  " and ent_calls.not_reached_calls = 0 then 1 else 0 end) as not_called " +
					    " FROM " +
					        " writers w, " +
					        " (SELECT " +
					        	" ent.writer_id, " +
					        	" ent.entry_id, " +
						        " sum(case when iat.reached_status_id = " + IssuesManagerBase.ISSUE_REACHED_STATUS_NOT_REACHED +
						        		 " then 1 else 0 end) as not_reached_calls, " +
						        " sum(case when iat.reached_status_id = " + IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED +
						        		 " then 1 else 0 end) as reached_calls " +
					        "  FROM" +
						        " (SELECT distinct " +
						             " peh.assigned_writer_id writer_id, " +
						             " pe.id entry_id," +
						             " to_char(peh.time_created,'YYYYMMDD') ass_date " +
						          " FROM " +
						             " population_entries_hist peh, " +
						             " population_entries pe " +
						          " WHERE peh.population_entry_id = pe.id " +
						             " AND peh.status_id = " + PopulationsManagerBase.POP_ENT_HIS_STATUS_ASSIGNED + " " +
						             " AND to_char(peh.time_created,'YYYYMMDD') = to_char(sysdate,'YYYYMMDD')" +
						         " )ent " +
						             " left join issues i on ent.entry_id = i.population_entry_id " +
						             	" left join issue_actions ia on i.id = ia.issue_id " +
	             								" AND ia.writer_id = ent.writer_id " +
	             								" AND to_char(ia.action_time,'YYYYMMDD') = ent.ass_date " +
	             							" left join issue_action_types iat on ia.issue_action_type_id = iat.id AND " +
					             					" iat.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL + " " +

					         " GROUP BY" +
					         	" ent.writer_id, " +
					         	" ent.entry_id " +
						     " )ent_calls " +

					    " WHERE " +
					    	" w.id = ent_calls.writer_id " +
					    	" AND w.dept_id = " + ConstantsBase.DEPARTMENT_RETENTION + " " +
					        " AND w.is_active = 1 " +
						    " AND EXISTS ( select 1  " +
							             " from roles r " +
							             " where w.user_name = r.user_name " +
							             "   and (r.role like '" + ConstantsBase.ROLE_RETENTION + "' " +
							             		" or r.role like '" + ConstantsBase.ROLE_RETENTIONM + "')) ";

			    if(!CommonUtil.isParameterEmptyOrNull(skins)){
			    	sql +=  " AND EXISTS (SELECT 1 FROM writers_skin ws WHERE ws.writer_id = w.id AND ws.skin_id IN (" +skins + ")) ";
			    }
			    if (skinId > 0) {
			    	sql +=  " AND EXISTS (select 1 from writers_skin where writer_id = w.id and skin_id = " + skinId + " ) ";
			    }
			    if (writerFilter > 0) {
			        sql +=  " AND w.id = '" + writerFilter + "' ";
			    }

			    sql +=
				    " GROUP BY " +
				       	"w.user_name, " +
				       	"w.id ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					Writer w = new Writer();
					w.setUserName(rs.getString("user_name"));
					w.setId(rs.getLong("writer_id"));
					w.setAllAssignedRecords(rs.getLong("num_ass"));
					w.setCalledNotReachedRecords(rs.getLong("called_not_reached"));
					w.setCalledReachedRecords(rs.getLong("called_reached"));
					w.setNotCalleRecords(rs.getLong("not_called"));
					list.add(w);
				}
			} finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	}


    public static long getWrterIdByUserName(Connection con, String name) throws SQLException {
		if (name == null || name.trim().equals("")) {
			return 0;
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
	        String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "writers " +
                "WHERE " +
                    "user_name = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1,name.toUpperCase());
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}
    
	public static ArrayList<Long> getAllActiveWritersAllowedToViewCcFirst6Digits(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> writers = new ArrayList<Long>();
		try {
			String sql = "SELECT writer_id " + 
						 "FROM writer_cc_permisions " +
					     "WHERE is_active = 1";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				writers.add(rs.getLong("writer_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return writers;
	}
    
    /**
     * Set writers number of CMS open tabs
     * 
     * @param con
     * @param direction
     *            -1 or 1 depending of login or logout
     * @param writerId
     * @throws SQLException
     */
    public static void setWritersCMS(Connection con, int direction, Long writerId) throws SQLException {
        PreparedStatement ps = null;
        String sqlDirection = "";
        if (direction == 1) {
            sqlDirection = "+ 1 ";
        } else {
            sqlDirection = "- 1 ";
        }
        try {
            String sql = " UPDATE " + 
                            " writers " + 
                         " SET " + 
                            " cms_login = cms_login " + sqlDirection + 
                         " WHERE " + " id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, writerId);
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    /**
     * Get writers number of CMS open tabs
     * 
     * @param con
     * @param writerId
     * @return number of open tabs
     * @throws SQLException
     */
    public static int getWritersCMS(Connection con, Long writerId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT " + 
                            " cms_login " + 
                         " FROM " + 
                            " writers " + 
                         " WHERE " + " id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, writerId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("cms_login");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0;
    }
    
    /**
     * Set writers number of CMS open tabs
     * 
     * @param con
     * @param value
     * @param userName
     * @throws SQLException
     */
    public static int resetWritersCMS(Connection con, int value, String userName) throws SQLException {
        PreparedStatement ps = null;
        int rowsUpdated=0;
        
        try {
            String sql = " UPDATE " + 
                            " writers " + 
                         " SET " + 
                            " cms_login = " + value + 
                         " WHERE " + " user_name = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, userName.toUpperCase());
            rowsUpdated = ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
        return rowsUpdated;
    }

    public static ArrayList<RankStatus> getRetentionRepresentativeAssignments(Connection con, long writerID) throws SQLException{
    	String sql = "SELECT s2.id rank_id, " + 
    			"  NVL(s1.r_active,0) r_active, " + 
    			"  NVL(s1.r_sleeper,0) r_sleeper, " + 
    			"  NVL(s1.r_comma,0) r_comma " +
    			"FROM " +
    			"  (SELECT rank_id, " + 
    			"    SUM(DECODE(u.status_id, 1, 1, 0)) r_active, " + 
    			"    SUM(DECODE(u.status_id, 2, 1, 0)) r_sleeper, " + 
    			"    SUM(DECODE(u.status_id, 3, 1, 0)) r_comma " +
    			"  FROM users u " +
    			"  WHERE u.id IN " +
    			"    (SELECT pu.user_id " +
    			"    	FROM " +
    			"			writers w, " + 
    			"			population_users pu " +
    			"    WHERE w.is_active               = 1 " +
    			"    AND pu.curr_assigned_writer_id IS NOT NULL " +
    			"    AND pu.curr_assigned_writer_id  = w.id " +
    				" AND pu.curr_population_entry_id in " +
					" (SELECT " +
						" pe.id " +
					" FROM " +
						" population_users ppu " +
						" join population_entries pe on  ppu.curr_population_entry_id = pe.id " +
						" join populations p on  pe.population_id =p.id " +
						" join population_types pt on  p.population_type_id=pt.id " +
						" left join users u on u.id = ppu.user_id " +
						" left join contacts cts on cts.id = ppu.contact_id " +
					" WHERE " +
						" (u.class_id  != 0 OR u.class_id  IS NULL) " +
						" AND((u.skin_id IS NOT NULL AND u.skin_id  IN " +
							" (SELECT " +
									" ws.skin_id " +
								" FROM " +
									" writers_skin ws " +
								" WHERE ws.writer_id = w.id )) " +
							" OR (cts.skin_id IS NOT NULL AND cts.skin_id IN " +
								" (SELECT " +
										" ws.skin_id " +
									" FROM " +
										" writers_skin ws " +
									" WHERE " +
										" ws.writer_id = w.id ))) " +
						" AND pt.type_id = " + ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION + ")" +
    			"    AND EXISTS (SELECT 1 FROM roles r WHERE w.user_name = r.user_name AND r.role LIKE 'retention' OR r.role LIKE 'retentionM' ) " +
    			"    AND w.sales_type IN (2, 3) " +
    			"    AND EXISTS (SELECT 1 FROM writers_skin WHERE writer_id = w.id AND skin_id IS NOT NULL AND skin_id IN " +
    			"							( SELECT skin_id FROM writers_skin WHERE writer_id = ? )" +
    			"				) " +
    			"    AND w.id = ? " +
    			"    ) " +
    			"  GROUP BY rank_id " +
    			"  ) s1, " +
    			"  users_rank s2 " +
    			"WHERE s1.rank_id(+)=s2.id " +
    			"ORDER BY 1";
    	
    	ArrayList<RankStatus> retval = new ArrayList<RankStatus>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{ 
                ps = con.prepareStatement(sql);
                ps.setLong(1, writerID);
                ps.setLong(2, writerID);
                rs = ps.executeQuery();
                while (rs.next()) {
                    retval.add(new RankStatus(rs.getInt("rank_id"), rs.getInt("r_comma"), rs.getInt("r_sleeper"), rs.getInt("r_active")));
                }
        } finally {
        	closeStatement(ps);
        }
        return retval;
    	
    }
    
    public static ArrayList<WriterMaxAssignment> getWriterMaxAutoAssignment(Connection con, long writerId) throws SQLException {
    	String sql = 
    			"SELECT srd.percentage*ws.assign_limit*ssd.percentage AS assign_limit, " +
    	    			"  srd.rank_id, " +
    	    			"  ssd.status_id, " +
    	    			"  ws.writer_id " +
    					"FROM writers w, " +
    					"  writers_skin ws, " +
    					"  writer_rank_distribution wrd, " +
    					"  sales_rank_distribution srd, " +
    					"  sales_status_distribution ssd, " +
    					"  users_rank ur, " +
    					"  users_status us " +
    					"WHERE ws.writer_id                  =w.id " +
    					"AND wrd.writer_skin_id              =ws.id " +
    					"AND srd.group_id                    =wrd.rank_distribution_group_id " +
    					"AND srd.status_distribution_group_id=ssd.group_id " +
    					"AND srd.rank_id                     =ur.id " +
    					"AND ssd.status_id                   =us.id " +
    					"AND w.id                            = ? " +
    					"ORDER BY 2,3";

    	ArrayList<WriterMaxAssignment> retval = new ArrayList<WriterMaxAssignment>();
    	 PreparedStatement ps = null;
         ResultSet rs = null;
         try{ 
                 ps = con.prepareStatement(sql);
                 ps.setLong(1, writerId);
                 rs = ps.executeQuery();
                 while (rs.next()) {
                     retval.add(new WriterMaxAssignment(rs.getInt("rank_id"), rs.getInt("assign_limit"), rs.getInt("status_id"), rs.getInt("writer_id")));
                 }
         } finally {
         	closeStatement(ps);
         }
         return retval;
    }

	public static HashMap<Long, Writer> getChatWriters(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();

		try {
		    String sql =
		    			"SELECT " +
		    			"	w.* " +
		    			"FROM " +
				    	"	(SELECT " +
				    	"		uai.last_chat_writer_id " +
				    	"	FROM " +
				    	"		users_additional_info uai " +
				    	"	GROUP BY " +
				    	"		uai.last_chat_writer_id " +
				    	"	UNION " +
				    	"	SELECT" +
				    	"		cai.last_chat_writer_id " +
				    	"	FROM " +
				    	"		contacts_additional_info cai " +
				    	"	GROUP BY " +
				    	"		cai.last_chat_writer_id) writers_chat " +
				    	" " +
				    	"LEFT JOIN " +
				    	"	WRITERS w " +
				    	"ON " +
				    	"	w.id = writers_chat.last_chat_writer_id " +
				    	"WHERE" +
				    	"	w.dept_id in (" + ConstantsBase.DEPARTMENT_RETENTION + "," + ConstantsBase.DEPARTMENT_SUPPORT + " )";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Writer writer = new Writer();
				writer.setId(rs.getLong("id"));
				writer.setUserName(rs.getString("user_name"));
				writer.setSkins(WritersDAO.getWriterSkins(con, writer.getId()));
				list.put(new Long(writer.getId()), writer);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	public static String getNickNameFromWriter(Connection con, long userId, boolean firstName)	throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String writerNickName = null;
		String name = null;
		if (firstName) {
			name = "nick_name_first ";
		} else {
			name = "nick_name_last ";
		}
		try {
			String sql =  "select " + name
						+ "from "
						+ "(select * from table( utils.get_nickname(?)))";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				writerNickName = rs.getString(1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return writerNickName;
	}
	
	/**
	 * Get Writer Skins Group By Language SI
	 * @param con
	 * @param writerId
	 * @return list as ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getWriterSkinsGroupByLanguageSI(Connection con, long writerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = 	" SELECT " +
								" s.default_language_id, " +
								" l.display_name " +
							" FROM " +
								" skins s, " +
								" languages l, " +
								" writers_skin ws " +
							" WHERE " +
								" s.default_language_id = l.ID " +
								" AND s.id = ws.skin_id " +
								" AND ws.writer_id = ? " +
							" GROUP BY " +
								" s.default_language_id, " +
								" l.display_name";
			ps = con.prepareStatement(sql);
			ps.setLong(1, writerId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getInt("default_language_id"),rs.getString("display_name")));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
    
	/**
	 * Get partial query overview
	 * @param skinId
	 * @param writerFilter
	 * @param skins
	 * @param salesType
	 * @return String
	 */
	public static String getPartialQueryOverview(long skinId, long writerFilter, String skins, long salesType) {
		String sql = "w.is_active = 1 AND " +
				   	 "pu.curr_assigned_writer_id is not null AND " +
					 "pu.curr_assigned_writer_id = w.id AND " +
					 "EXISTS ( select 1 " +
                               "from roles r " +
                               "where  w.user_name = r.user_name and " +
                               "r.role like ? or r.role like ?) ";
	    if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION) {
	    	sql += " AND w.sales_type in (1, 3) ";
	    	sql += " AND pu.curr_population_entry_id in " +
	    			"(SELECT pe.id FROM population_users ppu "+
                     " join population_entries pe on  ppu.curr_population_entry_id = pe.id "+
                     " join populations p on  pe.population_id =p.id "+
                     " join population_types pt on  p.population_type_id=pt.id "+
                     " left join users u on u.id = ppu.user_id "+
                     " left join contacts cts on cts.id = ppu.contact_id "+
                     " WHERE (u.class_id  != 0 "+
                     " OR u.class_id  IS NULL) "+
                     " AND (pt.type_id = "+ PopulationsManagerBase.SALES_TYPE_CONVERSION +
                     " OR (pt.id  = " + PopulationsManagerBase.POP_TYPE_CALLME +
                     " AND u.first_deposit_id IS NULL)) "+
                     " AND pe.is_displayed = 1 "+
                     " AND pe.group_id = 1 "+
                     " AND((u.skin_id IS NOT NULL AND u.skin_id  IN "+
                     " (SELECT ws.skin_id FROM writers_skin ws WHERE ws.writer_id = w.id )) "+
                     " OR (cts.skin_id IS NOT NULL AND cts.skin_id IN "+
                     " (SELECT ws.skin_id FROM writers_skin ws WHERE ws.writer_id = w.id )))) ";	
	    } else if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION) {
	    	sql += " AND w.sales_type in (2, 3) ";
	    	sql +=" AND pu.curr_population_entry_id in " +
	    			"(SELECT pe.id FROM population_users ppu "+
                     " join population_entries pe on  ppu.curr_population_entry_id = pe.id "+
                     " join populations p on  pe.population_id =p.id "+
                     " join population_types pt on  p.population_type_id=pt.id "+
                     " left join users u on u.id = ppu.user_id "+
                     " left join contacts cts on cts.id = ppu.contact_id "+
                     " WHERE (u.class_id  != 0 "+
                     " OR u.class_id  IS NULL) "+
                     " AND pt.type_id = "+ ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION +
                     " AND((u.skin_id IS NOT NULL AND u.skin_id  IN "+
                     " (SELECT ws.skin_id FROM writers_skin ws WHERE ws.writer_id = w.id )) "+
                     " OR (cts.skin_id IS NOT NULL AND cts.skin_id IN "+
                     " (SELECT ws.skin_id FROM writers_skin ws WHERE ws.writer_id = w.id )))) ";
	    } 
	    sql += " AND EXISTS (select 1 from writers_skin where writer_id = w.id and skin_id is not null and skin_id in (" + skins + ")) ";
	    if (skinId > 0) {
	    	sql += "AND EXISTS (select 1 from writers_skin where writer_id = w.id and skin_id = " + skinId + " ) ";
	    }
	    if (writerFilter > 0) {
	    	sql += "AND w.id = '" + writerFilter + "' ";
	    }
	    return sql;
	}
	
	public static ArrayList<SelectItem> getUrlSkins(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = 	" SELECT " +
								" * " +
							" FROM " +
								" skins s " +
							" WHERE " +
								" s.visible_type_id in (1,3,4) " +
							" ORDER BY " +
								" id desc ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getInt("id"),rs.getString("display_name")));
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}
