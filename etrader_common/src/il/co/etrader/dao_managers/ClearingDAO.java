package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.ClearingDAOBase;

import il.co.etrader.bl_vos.ErrorCode;

/**
 * Clearing DAO.
 *
 * @author Tony
 */
public class ClearingDAO extends ClearingDAOBase {
	private static final Logger log = Logger.getLogger(ClearingDAO.class);

	public static ArrayList<ErrorCode> getAllErrorCodes(Connection conn) throws SQLException{
		ArrayList<ErrorCode> list = new ArrayList<ErrorCode>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT cec.* ,cpg.CLEARING_PROVIDER_ID  " +
					 " FROM clearing_routes_error_codes cec, clearing_providers_groups cpg " +
					 " WHERE cec.CLEARING_PROVIDER_ID_GROUP = cpg.GROUP_ID ";
		try{
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()){
				ErrorCode ec = new ErrorCode();
				ec.setId(rs.getLong("id"));
				ec.setResult(rs.getString("result"));
				ec.setSpecialCode(rs.getLong("is_special_code") == 0 ? false : true);
				ec.setClearingProviderId(rs.getLong("clearing_provider_id"));
				ec.setClearingProviderIdGroup(rs.getLong("clearing_provider_id_group"));
				list.add(ec);
			}

		}finally{
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
//
// 	public static ArrayList<ClearingRoute> getClearingRouteByBin(Connection con, String bin){
//		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String sql = " SELECT cr.*  " +
//					 " FROM clearing_routes cr " +
//					 " WHERE cr.start_bin = ? and cr.cc_type is null  " +
//							                " and cr.country_id is null  " +
//							                " and cr.currency_id is null  " +
//							                " and cr.skin_id is null  " +
//					 " ORDER BY    cr.start_bin, " +
//								 " cr.currency_id, " +
//								 " cr.cc_type, " +
//								 " cr.country_id, " +
//								 " cr.skin_id ";
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setString(1, bin);
//			rs = ps.executeQuery();
//			while(rs.next()){
//				ClearingRoute cr = new ClearingRoute();
//            	cr.setId(rs.getLong("id"));
//            	cr.setStartBIN(rs.getString("start_bin"));
//            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
//            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
//            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
//            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
//            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
//            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
//            	cr.setActive(rs.getLong("is_active") == 0 ? false : true);
//            	list.add(cr);
//			}
//
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//
//		} finally {
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//		return list;
//	}
//
// 	public static void updateClearingRouteIsActive(Connection conn, boolean isActive, long crId, CreditCard cc, long errorCodeId) throws Exception{
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		int index = 1;
//		String sql = " UPDATE clearing_routes " +
//					 " SET is_active = ? " +
//					 " WHERE  " +
//					   " id = ? ";
//
//		String sqlChangeLog = " INSERT into clearing_routes_changes_log crl (id, changed_bin_id, error_code_id, credit_card_type, credit_card_country, former_provider_id, current_provider_id, time_created) " +
//				  " values (seq_clearing_routes_changes.nextval, ?,?,?,?,0,0, sysdate )";
//		try {
//			conn.setAutoCommit(false);
//			ps = conn.prepareStatement(sql);
//			ps.setLong(1, isActive ? 1 : 0);
//			ps .setLong(2, crId);
//			ps.executeQuery();
//
//			ps = conn.prepareStatement(sqlChangeLog);
//			ps.setLong(index++, crId);
//			ps.setLong(index++, errorCodeId);
//			ps.setLong(index++, cc.getTypeIdByBin());
//			ps.setLong(index++, cc.getCountryId());
//
//			rs = ps.executeQuery();
//
//			conn.commit();
//		} catch (Exception e) {
//			log.debug("failed to update is Active where id :" + crId);
//			try{
//				conn.rollback();
//			}catch(SQLException se){
//
//			}
//			throw e;
//
//		} finally {
//			conn.setAutoCommit(true);
//			closeStatement(ps);
//			closeResultSet(rs);
//		}
//	}
//
//	public static ArrayList<ClearingRoute> existclearingRouteWithCurrnecy(Connection conn, String bin) {
//		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		String sql = " SELECT cr.* " +
//					 " from clearing_routes cr " +
//					 " WHERE cr.start_bin = ? and ( cr.currency_id = ? or " +
//					 							"	cr.currency_id = ? or " +
//					 							"	cr.currency_id = ? or " +
//					 							"	cr.currency_id = ? ) " +
//					 							" 	and cr.cc_type is null  " +
//					 							" 	and cr.country_id is null  " +
//					 							" 	and cr.skin_id is null  " ;
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setString(1, bin);
//			ps.setLong(2, ConstantsBase.CURRENCY_EUR_ID);
//			ps.setLong(3, ConstantsBase.CURRENCY_GBP_ID);
//			ps.setLong(4, ConstantsBase.CURRENCY_TRY_ID);
//			ps.setLong(5, ConstantsBase.CURRENCY_USD_ID);
//			rs = ps.executeQuery();
//			
//			while(rs.next()){
//				ClearingRoute cr = new ClearingRoute();
//				cr.setId(rs.getLong("id"));
//            	cr.setStartBIN(rs.getString("start_bin"));
//            	cr.setTimelastSuccess(rs.getDate("time_last_success"));
//            	cr.setTimeLastReroute(rs.getDate("time_last_reroute"));
//            	cr.setBusinessSkinId(rs.getLong("business_skin_id"));
//            	cr.setDepositClearingProviderId(rs.getLong("deposit_clearing_provider_id"));
//            	cr.setWithdrawClearingProviderId(rs.getLong("withdraw_clearing_provider_id"));
//            	cr.setCFTClearingProviderId(rs.getLong("cft_clearing_provider_id"));
//            	cr.setActive(rs.getLong("is_active") == 0 ? false : true);
//            	cr.setCurrnecyId(rs.getLong("currency_id"));
//            	list.add(cr);
//			}			
//			
//		} catch (SQLException e) {
//			log.debug(e);
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
}