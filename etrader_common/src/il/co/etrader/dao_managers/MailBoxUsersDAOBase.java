package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;

import il.co.etrader.bl_managers.MailBoxTemplateManagerBase;
import il.co.etrader.util.ConstantsBase;
import oracle.ucp.jdbc.HarvestableConnection;

/**
 * MailBoxUsersDAOBase
 * @author Kobi
 *
 */
public class MailBoxUsersDAOBase extends com.anyoption.common.daos.MailBoxUsersDAOBase {

	/**
	 * Update popup type id column
	 * @param con
	 * @param emailId user email id
	 * @param popTypeId new population type id
	 * @throws SQLException
	 */
	public static void updatePopTypeId(Connection con, long emailId, long popTypeId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"UPDATE " +
					"mailbox_users " +
				"SET " +
					"popup_type_id = ? " +
				"WHERE " +
					"id = ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, popTypeId);
			ps.setLong(2, emailId);
			ps.executeUpdate();
		}  finally {
			closeStatement(ps);
		}
	}
	
	public static void insertEmailInBatch(Connection con, MailBoxUser email, long promotion_id) throws SQLException {
		HarvestableConnection hc = (HarvestableConnection) con;
		hc.setConnectionHarvestable(false);
		
		int index = 1;
		String sql =
				"INSERT INTO " +
					  "mailbox_users " +
					  		"(id, template_id, user_id, status_id, time_created, writer_id, free_text, sender_id, is_high_priority, subject,bonus_user_id, popup_type_id, transaction_id, template_parameters, attachment_id) " +
					  "SELECT " +
	                        "SEQ_MAILBOX_USERS.nextval," +
					        "?," +
	                        "u.id," +
	                        "?," +
	                        "sysdate," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?," +
	                        "?" +
	                  "FROM  " +
	                        "users u ," +
	                        "promotions p ," +
	                        "promotions_entries pe " +
	                  "WHERE " +
	                       "u.id = pe.user_id AND " +
	                       " p.id = pe.promotion_id AND " +
	                       " p.id = ? ";
		PreparedStatement ps = con.prepareStatement(sql);
		    
		ps.setLong(index++, email.getTemplateId());
		ps.setLong(index++, ConstantsBase.MAILBOX_STATUS_NEW);
		ps.setLong(index++, email.getWriterId());
		ps.setString(index++, email.getFreeText());
		ps.setLong(index++, email.getSenderId());
		ps.setLong(index++, email.getIsHighPriority() ? 1 : 0);
		ps.setString(index++, email.getSubject());
		if (email.getBonusUserId() != 0) {
			ps.setLong(index++, email.getBonusUserId());
		} else {
			ps.setString(index++, null);
		}
		ps.setLong(index++, email.getPopupTypeId());
		if (email.getTransactionId() != 0) {
			ps.setLong(index++, email.getTransactionId());
		} else {
			ps.setString(index++, null);
		}
		ps.setString(index++, email.getParameters());
		if (null != email.getAttachmentId()) {
			ps.setLong(index++, email.getAttachmentId());
		} else {
			ps.setString(index++, null);
		}
		ps.setLong(index++, promotion_id);
		
		ps.execute();
		hc.setConnectionHarvestable(true);
		ps.close();
	}


    /**
     * Get User mailBox emails
     * @param conn
     * @param userId
     * @return
     * @throws SQLException
     */
    public static MailBoxUser getUserPopUpEmail(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MailBoxUser email = null;
        try{
            String sql =
            		"SELECT * " +
            		"FROM " +
	            		"(SELECT " +
	            			"X.*, " +
	            			"rownum as row_num " +
	            		"FROM " +
		                    "(SELECT " +
		                        "mbu.id, " +
		                        "mbu.time_created, " +
		                        "mbu.is_high_priority, " +
		                        "mbu.subject, " +
		                        "mbt.type_id, " +
		                        "mbu.popup_type_id, " +
		                        "mbu.transaction_id, " +
		                        "mbt.template, " +
		                        "mbs.sender, " +
		                        "mbu.free_text, " +
		                        "mbu.bonus_user_id, " +
		                        "mbu.template_parameters " +
		                    "FROM " +
		                        "mailbox_users mbu, " +
		                        "mailbox_templates mbt, " +
		                        "mailbox_senders mbs " +
		                    "WHERE " +
		                        "mbu.user_id = ? AND " +
		                        "mbu.template_id = mbt.id AND " +
		                        "mbu.sender_id = mbs.id AND " +
		                        "mbu.status_id = ? AND " +
		                        "mbu.popup_type_id in (2,3) " +  // popup types
		                    "ORDER BY " +
		                    	"mbu.time_created desc ) X " +
		                  ") " +
		               "WHERE " +
		               		"row_num = 1";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, ConstantsBase.MAILBOX_STATUS_NEW);

            rs = pstmt.executeQuery();
            while (rs.next()) {
            	email = new MailBoxUser();
            	email.setId(rs.getLong("id"));
            	email.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
            	email.setSubject(rs.getString("subject"));
            	email.setSenderName(rs.getString("sender"));
            	email.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
            	email.setFreeText(rs.getString("free_text"));
            	email.setBonusUserId(rs.getLong("bonus_user_id"));
            	email.setPopupTypeId(rs.getLong("popup_type_id"));
            	email.setTransactionId(rs.getLong("transaction_id"));
            	MailBoxTemplate template = new MailBoxTemplate();
            	template.setTypeId(rs.getLong("type_id"));
            	template.setTemplate(rs.getString("template"));
            	String params = rs.getString("template_parameters");
            	if (params != null) {
            		template.setTemplate(MailBoxTemplateManagerBase.insertParameters(template.getTemplate(), params));
            	}
                email.setTemplate(template);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return email;
    }
}
