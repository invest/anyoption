package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Market;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.ServiceConfig;

/**
 * Service config DAO.
 *
 * @author Tony
 */
public class ServiceConfigDAO extends DAOBase {
    /**
     * Load the service config.
     *
     * @param conn the db connection to use
     * @return <code>ServiceConfig</code> with the service configuration or <code>null</code> if
     *      no config in the db.
     * @throws SQLException
     */
    public static ServiceConfig getServiceConfig(Connection conn) throws SQLException {
        ServiceConfig sc = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "service_config";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) { // there should be only 1 row
                sc = new ServiceConfig();
                sc.setTa25Parameter(rs.getDouble("ta25_parameter"));
                sc.setFtseParameter(rs.getBigDecimal("ftse_parameter"));
                sc.setCacParameter(rs.getBigDecimal("cac_parameter"));
                sc.setSp500Parameter(rs.getBigDecimal("sp500_parameter"));
                sc.setKlseParameter(rs.getBigDecimal("klse_parameter"));
                sc.setD4BanksParameter(rs.getBigDecimal("D4Banks_PARAMETER"));
                sc.setBanksBursaParameter(rs.getBigDecimal("BanksBursa_PARAMETER"));
                sc.setD4SpreadParameter(rs.getBigDecimal("D4Spread_PARAMETER"));

            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return sc;
    }

    /**
     * Update the markets table average_investments field with the average
     * value up to yesterday including for each market.
     *
     * @param conn
     * @throws SQLException
     */
    public static void updateAverageInvestments(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE markets A SET " +
                        "A.average_investments = " +
                            "(SELECT " +
                                "ROUND(SUM(X.amount * X.rate) / 3000) AS average_investments " + // 30 days * 100 cents
                            "FROM " +
                                "investments X, " +
                                "opportunities Y " +
                            "WHERE " +
                                "X.opportunity_id = Y.id AND " +
                                "X.time_created >= trunc(current_date - 30) AND " +
                                "X.time_created < trunc(current_date) AND " +
                                "X.is_canceled = 0 AND " +
                                "Y.market_id = A.id)";
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Get all markets average investments for the last 30 days. (id, feed_name, average_investments).
     *
     * @param conn the db connection to use
     * @return <code>ArrayList<Market></code> each element of the list has only
     *      id, feed_name and average_investments properties filled.
     * @throws SQLException
     */
    public static ArrayList<Market> getAverageInvestments(Connection conn) throws SQLException {
        ArrayList<Market> l = new ArrayList<Market>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id, " +
                    "feed_name, " +
                    "average_investments " +
                "FROM " +
                    "markets";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Market m = new Market();
                m.setId(rs.getInt("id"));
                m.setFeedName(rs.getString("feed_name"));
                m.setAverageInvestments(rs.getBigDecimal("average_investments"));
                l.add(m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    public static void updateTA25(Connection con, ServiceConfig c) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "update service_config set " +
                        "TA25_PARAMETER = ?";
            ps = con.prepareStatement(sql);
            ps.setDouble(1, c.getTa25Parameter());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }
    
    public static void updateFTSE(Connection con, ServiceConfig c) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "update service_config set " +
                        "ftse_parameter = ?";
            ps = con.prepareStatement(sql);
            ps.setBigDecimal(1, c.getFtseParameter());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }
    public static void updateCAC(Connection con, ServiceConfig c) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "update service_config set " +
                        "cac_parameter = ?";
            ps = con.prepareStatement(sql);
            ps.setBigDecimal(1, c.getCacParameter());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }
    public static void updateSP500(Connection con, ServiceConfig c) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "update service_config set " +
                        "SP500_PARAMETER = ?";
            ps = con.prepareStatement(sql);
            ps.setBigDecimal(1, c.getSp500Parameter());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }

    public static void updateParameter(Connection con, String market, double value) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "update service_config set " + market + " = ?";
            ps = con.prepareStatement(sql);
            ps.setDouble(1, value);
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }
}