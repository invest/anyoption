package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.util.CommonUtil;

public class CreditCardTypesDAO extends DAOBase{

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected static CreditCardType getVO(ResultSet rs) throws SQLException {
		CreditCardType vo = new CreditCardType();

		vo.setId(rs.getInt("id"));
		vo.setName(rs.getString("name"));
		vo.setThreeDSecure(rs.getBoolean("is_3d_secure"));
		vo.setDescription(rs.getString("description"));
		
		return vo;
	}
	  public static void insert(Connection con,CreditCardType vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="insert into credit_card_types(id,name) values(?,?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getId());
				ps.setString(2, vo.getName());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }

		  public static ArrayList<CreditCardType> getAll(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<CreditCardType> list=new ArrayList<CreditCardType>();

			  try
				{
				    String sql="select * from credit_card_types order by name";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						CreditCardType vo = new CreditCardType();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  public static CreditCardType getById(Connection con,long id) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  CreditCardType vo=null;
			  try
				{
				    String sql="select * from credit_card_types where id=?";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new CreditCardType();
						vo.setId(rs.getLong("id"));
						vo.setName(CommonUtil.getMessage(rs.getString("name"),null));

					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }
		  
	/**
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static Map<Integer, CreditCardType> getCreditCardTypesMaping(Connection connection) throws SQLException {
		Map<Integer, CreditCardType> creditCardTypes = new LinkedHashMap<Integer, CreditCardType>();
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try {
	        String sql =
	            "SELECT " +
	            "	* " +
	            "FROM " +
	            "	credit_card_types ";
	        
	        pstmt = connection.prepareStatement(sql);
	        rs = pstmt.executeQuery();
	        
	        while (rs.next()) {
	        	creditCardTypes.put(rs.getInt("id"), getVO(rs));
			}
	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }
	    
	    return creditCardTypes;
	}
} 