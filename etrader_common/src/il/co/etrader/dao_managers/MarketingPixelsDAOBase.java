package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.MarketingAffiliatePixels;
import il.co.etrader.bl_vos.MarketingCombinationPixels;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.bl_vos.MarketingSubAffiliatePixels;


public class MarketingPixelsDAOBase extends DAOBase {


		  /**
		   * Get VO
		   * @param rs
		   * 	Result set instance
		   * @return
		   * 	MarketingPixel object
		   * @throws SQLException
		   */
		  protected static MarketingPixel getVO(ResultSet rs) throws SQLException {
			  MarketingPixel vo = new MarketingPixel();
			  vo.setId(rs.getLong("id"));
			  vo.setHttpCode(rs.getString("http_code"));
			  vo.setHttpsCode(rs.getString("https_code"));
			  vo.setWriterId(rs.getLong("writer_id"));
			  vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			  vo.setName(rs.getString("name"));
			  return vo;
		  }

		  /**
		   * Get combination pixels by combinationId
		   * @param con
		   * @param combId
		   * @return
		   * @throws SQLException
		   */
		  public static MarketingCombinationPixels getPixelsByCombId(Connection con, long combId) throws SQLException {
			  PreparedStatement ps = null;
		      ResultSet rs = null;
		      MarketingCombinationPixels mp = new MarketingCombinationPixels();
		      ArrayList<MarketingPixel> pixels = new ArrayList<MarketingPixel>();
		      try{
		            String sql =
		            	"SELECT " +
		            		"mcp.id mcp_id, " +
		            		"mcp.combination_id comb_id, " +
		            		"mp.id p_id, " +
		            		"mp.http_code, " +
		            		"mp.https_code, " +
		            		"mp.type_id p_type_id, " +
		            		"mp.name " +
		            	"FROM " +
		            		"marketing_combination_pixels mcp, " +
		            		"marketing_pixels mp " +
		            	"WHERE " +
		            		"mcp.combination_id = ? AND " +
		            		"mcp.pixel_id = mp.id ";

		            ps = con.prepareStatement(sql);
		            ps.setLong(1, combId);
		            rs = ps.executeQuery();
		            while (rs.next()) {
		            	mp.setId(rs.getLong("mcp_id"));
		            	mp.setCombId(rs.getLong("comb_id"));
		            	MarketingPixel p = new MarketingPixel();
						p.setId(rs.getLong("p_id"));
						p.setHttpCode(rs.getString("http_code"));
						p.setHttpsCode(rs.getString("https_code"));
						p.setTypeId(rs.getLong("p_type_id"));
						p.setName(rs.getString("name"));
						pixels.add(p);
		            }
		            mp.setPixels(pixels);
		      } finally {
		            closeStatement(ps);
		            closeResultSet(rs);
		      }
		      return mp;
		  }

		  /**
		   * Get affiliate pixels by affiliateId
		   * @param con
		   * @param affiliateId
		   * @return
		   * @throws SQLException
		   */
		  public static MarketingAffiliatePixels getPixelsByAffiliateId(Connection con, long affiliateId) throws SQLException {
			  PreparedStatement ps = null;
		      ResultSet rs = null;
		      MarketingAffiliatePixels mp = new MarketingAffiliatePixels();
		      ArrayList<MarketingPixel> pixels = new ArrayList<MarketingPixel>();
		      try{
		            String sql =
		            	"SELECT " +
		            		"map.id map_id, " +
		            		"map.affiliate_id affiliate_id, " +
		            		"mp.id p_id, " +
		            		"mp.http_code, " +
		            		"mp.https_code, " +
		            		"mp.type_id p_type_id, " +
		            		"mp.name " +
		            	"FROM " +
		            		"marketing_affilate_pixels map, " +
		            		"marketing_pixels mp " +
		            	"WHERE " +
		            		"map.affiliate_id = ? AND " +
		            		"map.pixel_id = mp.id ";

		            ps = con.prepareStatement(sql);
		            ps.setLong(1, affiliateId);
		            rs = ps.executeQuery();
		            while (rs.next()) {
		            	mp.setId(rs.getLong("map_id"));
		            	mp.setAffiliateId(rs.getLong("affiliate_id"));
		            	MarketingPixel p = new MarketingPixel();
						p.setId(rs.getLong("p_id"));
						p.setHttpCode(rs.getString("http_code"));
						p.setHttpsCode(rs.getString("https_code"));
						p.setTypeId(rs.getLong("p_type_id"));
						p.setName(rs.getString("name"));
						pixels.add(p);
		            }
		            mp.setPixels(pixels);
		      } finally {
		            closeStatement(ps);
		            closeResultSet(rs);
		      }
		      return mp;
		  }

		  /**
		   * Get sub affiliate pixels by subaffiliateId
		   * @param con
		   * @param subAffiliateId
		   * @return
		   * @throws SQLException
		   */
		  public static MarketingSubAffiliatePixels getPixelsBySubAffiliateId(Connection con, long subAffiliateId) throws SQLException {
			  PreparedStatement ps = null;
		      ResultSet rs = null;
		      MarketingSubAffiliatePixels msap = new MarketingSubAffiliatePixels();
		      ArrayList<MarketingPixel> pixels = new ArrayList<MarketingPixel>();
		      try{
		            String sql =
		            	"SELECT " +
		            		"msap.id map_id, " +
		            		"msap.affiliate_id affiliate_id, " +
		            		"mp.id p_id, " +
		            		"mp.http_code, " +
		            		"mp.https_code, " +
		            		"mp.type_id p_type_id, " +
		            		"mp.name " +
		            	"FROM " +
		            		"marketing_sub_affiliate_pixels msap, " +
		            		"marketing_pixels mp " +
		            	"WHERE " +
		            		"msap.sub_affiliate_id = ? AND " +
		            		"msap.pixel_id = mp.id ";

		            ps = con.prepareStatement(sql);
		            ps.setLong(1, subAffiliateId);
		            rs = ps.executeQuery();
		            while (rs.next()) {
		            	msap.setId(rs.getLong("map_id"));
		            	msap.setAffiliateId(rs.getLong("affiliate_id"));
		            	MarketingPixel p = new MarketingPixel();
						p.setId(rs.getLong("p_id"));
						p.setHttpCode(rs.getString("http_code"));
						p.setHttpsCode(rs.getString("https_code"));
						p.setTypeId(rs.getLong("p_type_id"));
						p.setName(rs.getString("name"));
						pixels.add(p);
		            }
		            msap.setPixels(pixels);
		      } finally {
		            closeStatement(ps);
		            closeResultSet(rs);
		      }
		      return msap;
		  }
		  
		  /**
		   * Get all pixel types
		   * @param con
		   * @return
		   * @throws SQLException
		   */
		  public static ArrayList<SelectItem> getAllPixelPageName(Connection con) throws SQLException {
		        PreparedStatement ps = null;
		        ResultSet rs = null;
		        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
		        try{
		            String sql =
		            	"SELECT * " +
		            	"FROM marketing_pixel_types " +
		            	"ORDER BY code";
		            ps = con.prepareStatement(sql);
		            rs = ps.executeQuery();
		            while ( rs.next() ) {
		                hm.add(new SelectItem(rs.getLong("id"), rs.getString("page_name")));
		            }
		        } finally {
		            closeStatement(ps);
		            closeResultSet(rs);
		        }
		        return hm;
		    }
}

