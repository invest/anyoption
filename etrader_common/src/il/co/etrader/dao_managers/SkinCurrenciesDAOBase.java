package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Limit;

/**
 * SkinCurrenciesD DAO Base class.
 *
 * @author Kobi
 */
public class SkinCurrenciesDAOBase extends DAOBase {

	public static ArrayList<SkinCurrency> getAllBySkin(Connection con, long skinId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SkinCurrency> list = new ArrayList<SkinCurrency>();

		try {

			String sql = "select s.*, c.display_name " +
						 "from skin_currencies s, currencies c " +
						 "where s.currency_id = c.id and s.skin_id = ? " +
						 "order by c.display_name";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);

			rs = ps.executeQuery();

			while( rs.next() ) {
				list.add(getVO(rs));
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;

	}


	/**
	 * Update for limit
	 * @param con  Db connection
	 * @param l  Limit instance for update
	 * @throws SQLException
	 */
	public static void updateLimit(Connection con, Limit l) throws SQLException {

		PreparedStatement ps = null;

		try {
				String sql = "update skin_currencies set is_default = ? " +
							 "where skin_id = ? and currency_id = ?	and limit_id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, l.getIsDefault());
				ps.setLong(2, l.getSkinId());
				ps.setLong(3, l.getCurrencyId());
				ps.setLong(4, l.getId());

				ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}

	}

	/**
	 * Insert new row, for adding a new limit
	 * @param con  db connection
	 * @param l  Limit instance
	 * @throws SQLException
	 */
	public static void insert(Connection con, Limit l) throws SQLException {

		PreparedStatement ps = null;

		try {

				String sql = "insert into skin_currencies(id, skin_id, currency_id, limit_id, is_default) " +
							 "values(seq_skin_currencies.nextval,?,?,?,?)";

				ps = con.prepareStatement(sql);
				ps.setLong(1, l.getSkinId());
				ps.setLong(2, l.getCurrencyId());
				ps.setLong(3, l.getId());
				ps.setInt(4, l.getIsDefault());

				ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}

	}

	/**
	 * Fill SkinCurrency object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		SkinCurrency object
	 * @throws SQLException
	 */
	protected static SkinCurrency getVO(ResultSet rs) throws SQLException {

		SkinCurrency vo = new SkinCurrency();

		vo.setId(rs.getLong("id"));
		vo.setSkinId(rs.getLong("skin_id"));
		vo.setCurrencyId(rs.getLong("currency_id"));
		vo.setDisplayName(rs.getString("display_name"));
		vo.setCashToPointsRate(rs.getDouble("cash_to_points_rate"));
		vo.setLimitId(rs.getLong("limit_id"));

		return vo;
	}



}