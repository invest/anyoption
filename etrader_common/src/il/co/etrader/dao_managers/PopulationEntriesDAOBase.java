package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_managers.WritersManagerBase;
import il.co.etrader.bl_vos.RankLimits;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WritersSkin;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class PopulationEntriesDAOBase extends PopulationsDAOBase {

	protected static String getEntriesTableString(String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId, String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers,
			long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes,
			boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long userStatusId, long populationDept, long userRankId,  boolean isFromJob, boolean qualificationTimeFilter, int issueActionTypeId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId){
		
		return getEntriesTableString(populationTypes, skinIds, businessSkin, languageId, assignedWriterId, campaignId, countries, fromDate, toDate,
				colorType, isAssigned, userId, userName, contactId, supportMoreOptionsType, timeZone, priorityId, excludeCountrries, hideNoAnswer,
				callsCount, countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId, campaignPriority,
				isSpecialCare, fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept,
				userRankId, isFromJob, ConstantsBase.ALL_FILTER_ID, qualificationTimeFilter, issueActionTypeId, ConstantsBase.ALL_CHAT_FILTER_ID, reachCustomerFlag,
				retentionTeamId, everCalled, sortAmountColumn, userPlatform, countryGroupTierId);
	}

	protected static String getEntriesTableString(String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId, String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers,
			long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes,
			boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long userStatusId, long populationDept, long userRankId,
			boolean isFromJob, long lastCallerWriterId, boolean qualificationTimeFilter, int issueActionTypeId, long lastChatWriterId, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId){

	    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	    SimpleDateFormat fmtWithTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String sql =
		   " (SELECT " +
		        " entr_table_data.*, rownum as row_num " +
		   " FROM " +
		        "(SELECT entries_table.* ";
		        if(!isFromJob){
		        	sql+= " ,CASE WHEN entries_table.reached_call is null and entries_table.curr_assigned_writer_id is not null and sysdate - qualification_time >= 10 and sysdate - qualification_time < 20 then 1 " +
                          " WHEN entries_table.reached_call is null and entries_table.curr_assigned_writer_id is not null and sysdate - qualification_time >= 20 then 2 " + 
                          " ELSE 0 end as flag ";
		        }
		        sql+= " FROM " +
				        	" (SELECT ";
		        if (!isFromJob) {
		        sql+=   "  (select " +
                        "         count(*) " +
                        "          from issue_actions ia, issues i, population_users puu " +
                        "         where i.id = ia.issue_id " +
                        "           and ia.writer_id = puu.curr_assigned_writer_id " +
                        "           and i.user_id = puu.user_id " +
                        "           and ia.issue_action_type_id IN (SELECT id FROM issue_action_types WHERE reached_status_id = " + IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED + " )  " +
                        "           AND i.user_id = pu.user_id group by i.user_id) AS reached_call, ";
		        }
		        sql+=   " pu.id population_users_id, " +
		              " pu.lock_history_id, " +
			          " pu.curr_population_entry_id, " +
		        	  " pu.curr_assigned_writer_id, " +
		        	  " pu.assign_type_id, " +
		        	  " pu.time_assign, " +
			          " CASE WHEN pu.user_id is null THEN c.user_id ELSE pu.user_id END as user_id, " + 
		        	  " nvl(u.affiliate_key, c.affiliate_key) as affiliate_key, " +
			          " (u.first_name || ' ' || u.last_name) name_user, " +
				      " u.TIME_SC_TURNOVER, " +
					  " u.TIME_SC_HOUSE_RESULT," +
					  " u.currency_id as user_currency, " +
			          " c.name name_contact, " +
			          " c.dynamic_parameter as contact_dynamic_param, " +
			          " u.dynamic_param as user_dynamic_param, " +
			          " pu.contact_id, " +
			          " pu.entry_type_id, " +
			          " pu.entry_type_action_id, " +
			          " pu.delay_id, " +
			          " pu.time_control, " +
			          " pe.qualification_time, " +
			          " pe.group_id, " +
			          " pe.population_id, " +
			          " pe.is_displayed, " +
			          " p.name population_name, " +
			          " p.population_type_id, " +
			          " NVL(cn_contacts.id,cn_users.id) country_id , " +
			          " NVL(cn_contacts.gmt_offset,cn_users.gmt_offset) country_offset, " +
			          " ia_last_call.callback_time as call_back_time, " +
			          " ia_last_call.action_time last_call_time," +
			          " iat_last_call.reached_status_id last_reached_status_id, " +
			          " ia_last_call.issue_action_type_id last_action_type_id, " +
			          " ia_last_call.writer_id last_caller_writer_id, " +
			          " NVL(i_last_call.calls_num,0) calls_num, " +
			          " NVL(i_last_call.reached_calls_num,0) reached_calls_num, " +
			          " u.language_id, " +
			          " CASE WHEN pu.user_id is null THEN c.skin_id ELSE u.skin_id END as skin_id, " +  
			          " ti_w.user_name last_sales_dep_rep, " +
			          " CASE WHEN w_u_last_chat.user_name is null THEN w_c_last_chat.user_name ELSE w_u_last_chat.user_name END as last_chat_writer_id, " +
			          " mco.campaign_id, " +
			          " mca.name campaign_name, " +
                      " mca.priority campaign_priority_id, " +
                      " u.time_last_login as time_last_login," +
                      " u.balance as user_balance, " +
                      " u.dep_no_inv_24h, " +
                      " ur.id as user_rank_id, " +
                      "	us.id as user_status_id, " +
                      " ur.rank_name as user_rank_name, " +
					  " u.class_id, " +
					  " u.user_name, " +
					  " u.balance, " +
					  " u.platform_id, " +
					  "	us.status_name as user_status_name, " +
					  " uad.first_decline_amount_usd as first_decline_amount_usd, " +
                      " (SELECT usr.land_line_phone FROM users usr WHERE usr.id = NVL(pu.user_id, c.user_id)) AS user_land_line_phone, "+
                      " (SELECT usr.mobile_phone FROM users usr WHERE usr.id = NVL(pu.user_id, c.user_id)) AS user_mobile_phone, "+
                      " (SELECT usr.country_id FROM users usr, countries cc WHERE usr.country_id = cc.id and usr.id= NVL(pu.user_id, c.user_id)) AS user_country_id, "+
                      " (SELECT cc.gmt_offset FROM users usr, countries cc WHERE usr.country_id = cc.id and usr.id= NVL(pu.user_id, c.user_id)) AS user_country_offset, "+
                      " c.mobile_phone as contact_mobile_phone, " +
                      " c.land_line_phone as contact_land_line_phone, " +
                      " c.phone as contact_phone, " +
                      " c.type as contact_type, " +
                      " ( SELECT COUNT(tr.type_id) " +
                      "   FROM transactions tr " +
                      "   WHERE tr.user_id = u.id " +
                      "		AND tr.type_id  IN (SELECT tt.id FROM transaction_types tt WHERE tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT + " ) " +
                      "		AND tr.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +" ))  as count_transaction, "+
                      " ( SELECT tr.amount FROM users usr, transactions tr WHERE usr.first_deposit_id IS NOT NULL and usr.id = u.id  AND tr.id = u.first_deposit_id ) as first_deposit ,";
//		    if (populationDept == PopulationsManagerBase.SALES_TYPE_RETENTION || isFromJob) {
//		    sql +=    " total_inv.totalamount as total_turnover," ;
//		    }
		    sql +=    " pt.type_id ";
			if (isFromJob) {				
				if (!isAssigned) {
				sql +=" ,uh.qualification_date as last_time, ";
				} else {
				sql +=" ,case when u.time_last_login > pe.qualification_time then u.time_last_login else pe.qualification_time end as last_time, ";
				}
			sql +=	  " t.amount * t.rate as conversion, " +
					  " ilgc.value as limit_value," +
					  " user_total_deposit.sum_dep as sum_deposit ";
			}
			sql+= " FROM " +
		             " population_users pu " +
		             	 " LEFT JOIN contacts c on pu.contact_id = c.id " +		             
			             	" LEFT JOIN contacts_additional_info cai on cai.contact_id = c.id " +
	                 		" LEFT JOIN writers w_c_last_chat on cai.last_chat_writer_id = w_c_last_chat.id " +
			             	" LEFT JOIN countries cn_contacts on c.country_id = cn_contacts.id " +
                         " LEFT JOIN users u on pu.user_id = u.id " +
                      		" LEFT JOIN users_active_data uad on u.id = uad.user_id " +
                         	" LEFT JOIN countries cn_users on u.country_id = cn_users.id " +
                         	" LEFT JOIN marketing_combinations mco on (u.combination_id = mco.id OR (u.combination_id is null AND c.combination_id = mco.id)) " +
                         	" LEFT JOIN marketing_campaigns mca on mco.campaign_id = mca.id " +                        	
                        	" LEFT JOIN users_additional_info uai on uai.user_id = u.id " +
                         		" LEFT JOIN writers w_u_last_chat on uai.last_chat_writer_id = w_u_last_chat.id " +                      
                         " LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
                         	" LEFT JOIN populations p on pe.population_id = p.id" +
                         		" LEFT JOIN population_types pt on p.population_type_id = pt.id " +
	         	  		 " LEFT JOIN issues i_last_call on pu.curr_population_entry_id = i_last_call.population_entry_id " +
	         	  		    " LEFT JOIN issue_actions ia_last_call on i_last_call.last_call_action_id = ia_last_call.id  " +
	         	  		    " LEFT JOIN issue_action_types iat_last_call on iat_last_call.id = ia_last_call.issue_action_type_id " +
	         	  		 " LEFT JOIN writers ti_w on ti_w.id = pu.last_sales_dep_rep_id" +
	         	  		 " LEFT JOIN users_rank ur on ur.id = u.rank_id" +
	         	  		 " LEFT JOIN users_status us on us.id = u.status_id " ;
//			if (populationDept == PopulationsManagerBase.SALES_TYPE_RETENTION || isFromJob) {
//				sql+=	 " LEFT JOIN (SELECT i.user_id user_id, " +
//	         	  		 "			  SUM(amount) totalamount " +
//	         	  		 "			  FROM investments i " +
//	         	  		 "			  WHERE   is_canceled=0 " +
//	         	  		 "			  group by i.user_id) total_inv on total_inv.user_id = u.id ";
//			}
 					if (isFromJob) {
 			sql += 	 	 " LEFT JOIN transactions t ON t.id = u.first_deposit_id " +
 	         	  		 " LEFT JOIN investment_limit_group_curr ilgc ON u.currency_id = ilgc.currency_id ";
 						if (isAssigned) {
 			sql += 		 " LEFT JOIN writers assign_writers ON assign_writers.id = pu.curr_assigned_writer_id ";
 	         	  		} else {
 	         	  				if (userStatusId == UsersManagerBase.USER_STATUS_COMA) {
 	        sql +=		 			" LEFT JOIN " +
 	        							" (SELECT " +
 	        								" ush.user_id, max(ush.qualification_date) as qualification_date " +
 	        							" FROM " +
 	        								" users_status_hist ush " +
 	        							" WHERE " +
 	        								" ush.user_status_id = " + UsersManagerBase.USER_STATUS_COMA +
 	        							" GROUP BY " +
 	        								" ush.user_id) uh ON uh.user_id = u.id and u.status_id = " + UsersManagerBase.USER_STATUS_COMA; 	         	  				
 	         	  				} else {
 	        sql +=		 			" LEFT JOIN users_rank_hist uh ON u.id = uh.user_id AND uh.user_rank_id = " + UsersManagerBase.USER_RANK_NEWBIES;
 	         	  				}
 	         	  		}
 					}
 					if (isFromJob || (!isFromJob && !isAssigned && assignedWriterId == 0 && (userStatusId != UsersManagerBase.USER_STATUS_ACTIVE) && 
 							(populationDept == PopulationsManagerBase.SALES_TYPE_RETENTION || populationDept == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT))) {
 	        sql += 	     " LEFT JOIN (SELECT " +
 	        			 " 				t.user_id, " +
 	        			 "              SUM(t.amount * t.rate) sum_dep, " +
 	        			 "              COUNT(t.id) count_dep " +
 	        			 "            FROM " +
 	        			 "				transactions t, " +
 	        			 "              transaction_types tt " +
 	        			 "            WHERE       " +
 	        			 "	            t.type_id = tt.id " +
 	        			 "              AND tt.class_type = " + TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT +
 	        			 "              AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
 	        			 "            GROUP BY t.user_id) user_total_deposit ON user_total_deposit.user_id = u.id";
 					}
    	    sql += " WHERE ";
    	    	if (!isFromJob) {
    	    sql +=		" ((u.skin_id is not null and u.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ?)) " +
    	    			" OR " +
    	    			" (c.skin_id is not null and c.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ?))) " +
    	    			" AND ";

    	    		if (lastCallerWriterId > ConstantsBase.ALL_FILTER_ID || lastCallerWriterId == ConstantsBase.NONE_FILTER_ID) {
    	    			if (lastCallerWriterId == ConstantsBase.NONE_FILTER_ID) {
    	    				sql += " ia_last_call.writer_id is null AND ";
    	    			} else {
    	    				sql += " ia_last_call.writer_id = " + lastCallerWriterId + " AND ";
    	    			}
    	    		}
    	    		
    	    		if (lastChatWriterId > ConstantsBase.ALL_CHAT_FILTER_ID) {
    	    				sql += " nvl(uai.last_chat_writer_id, cai.last_chat_writer_id) = " + lastChatWriterId + " AND ";	    			
    	    		}		
    	    	}
   	    	sql +=		" (u.class_id != 0 OR u.class_id IS NULL) " +
   	    				" AND (u.is_active = 1 " +
   	    				" OR u.is_active IS NULL) ";
   	    	
    	if (supportMoreOptionsType == 0) {
            sql +=  " AND (pu.time_control IS NULL " +
                    " OR (pu.curr_assigned_writer_id IS NOT NULL" +
                    "     AND pu.time_control IS NOT NULL ))";
        }

	    if (lastSalesDepRepId > 0){ //singel rep
	    	sql +=  " AND pu.last_sales_dep_rep_id = " + lastSalesDepRepId + " ";
	    }

	    if (lastSalesDepRepId == ConstantsBase.ALL_REPS_FILTER_ID) {// all reps
	    	sql +=  " AND pu.last_sales_dep_rep_id IS NOT NULL ";
	    }

	    if (lastSalesDepRepId == ConstantsBase.NONE_FILTER_ID) {// none rep
	    	sql +=  " AND pu.last_sales_dep_rep_id IS NULL ";
	    }

		if (verifiedUsers == ConstantsBase.GENERAL_YES){
			sql += 	" AND u.is_authorized_mail = 1 ";
		}else if (verifiedUsers == ConstantsBase.GENERAL_NO){
			sql += 	" AND u.is_authorized_mail = 0 ";
		}

		if (!isFromJob) {
		    if (supportMoreOptionsType == 0){
				sql += " AND pu.entry_type_id  = " + ConstantsBase.POP_ENTRY_TYPE_GENERAL + " " +
					   " AND pu.curr_population_entry_id IS NOT NULL " +
					   " AND pe.is_displayed = 1 " +
					   " AND pe.group_id = 1 ";
			} else {
				sql += " AND pu.entry_type_id != 5 ";
			}
		}

	    if (isFromJob) {
	    	if (isAssigned) {
	    		sql += " AND pu.entry_type_id != 5 ";
	    	} else { //unassign
 				sql += " AND pu.entry_type_id  = " + ConstantsBase.POP_ENTRY_TYPE_GENERAL + " " +
					   " AND pu.curr_population_entry_id IS NOT NULL " +
					   " AND pe.is_displayed = 1 " +
					   " AND pe.group_id = 1 " +
	    			   " AND (pt.type_id = " + PopulationsManagerBase.SALES_TYPE_RETENTION + " " +
	    					" OR (pt.type_id = " + PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT + " OR (pt.id = " + PopulationsManagerBase.POP_TYPE_CALLME + " AND u.first_deposit_id is not null))) ";
	    	}
	    }

	    if (isAssigned){
			if (assignedWriterId != 0){
				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
			} else {
				sql += " AND pu.curr_assigned_writer_id is not null ";
			}
		} else if (supportMoreOptionsType == 0){  // ignore when it's decline after approved page
			if (assignedWriterId != 0) {
				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
			} else {
				sql += " AND pu.curr_assigned_writer_id is null ";
			}
		} 
	    
	    if (supportMoreOptionsType == PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS) {
			sql += " AND pu.curr_assigned_writer_id is null ";
		}

	    if (!countries.equals("0")){
			sql += " AND ((u.country_id is not null AND u.country_id in (" + countries + "))" +
						" OR " +
						" (u.country_id is  null AND c.country_id in (" + countries + ")))";
		}

		if (!excludeCountrries.equals("0")){
			sql += " AND ((u.country_id is not null AND u.country_id NOT in (" + excludeCountrries + "))" +
						" OR " +
						" (u.country_id is null AND c.country_id NOT in (" + excludeCountrries + ")))";
		}

		if (skinIds != null && !skinIds.equals("0")){
			sql += " AND ((u.skin_id is not null and u.skin_id in (" + skinIds + ")) " +
						" OR" +
						" (c.skin_id is not null and c.skin_id in (" + skinIds + "))) ";
		}

		if (businessSkin != 0){
			sql += " AND ((u.skin_id is not null and u.skin_id in (select s.id " +
																" from skins s " +
																" where s.business_case_id = " + businessSkin + ")) " +
					" OR" +
						" (c.skin_id is not null and c.skin_id in (select s.id " +
																" from skins s " +
																" where s.business_case_id = " + businessSkin + "))) ";

		}

		if (!CommonUtil.isParameterEmptyOrNull(populationTypes)){
			sql += " AND p.population_type_id in (" + populationTypes + ") ";
		}
		
		if (!CommonUtil.isParameterEmptyOrNull(userPlatform)) {
			if (!userPlatform.equals("-1")) { //All option
				sql += " AND u.writer_id IN ( " + userPlatform + " ) ";
			}
		}
		
		if (supportMoreOptionsType > 0) {
			if (supportMoreOptionsType == PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS) {
				sql += " AND (p.dept_id = " + ConstantsBase.DEPARTMENT_SUPPORT + " OR pu.curr_assigned_writer_id is null AND pt.id = " + PopulationsManagerBase.POP_TYPE_DECLINE_LAST +" )";
			} else if (supportMoreOptionsType == PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_CALL_ME) {
				sql += " AND p.dept_id = " + ConstantsBase.DEPARTMENT_RETENTION +
					   " AND pu.curr_assigned_writer_id is null " +
					   " AND pt.id = " + PopulationsManagerBase.POP_TYPE_CALLME +
					   " AND u.first_deposit_id is not null";
			}
		} else {
			sql += " AND (p.dept_id = " + ConstantsBase.DEPARTMENT_RETENTION + " OR (p.dept_id = " + ConstantsBase.DEPARTMENT_SUPPORT + " AND pu.curr_assigned_writer_id is not null)) ";
		}

		if (campaignId != 0){
			sql += " AND mco.campaign_id = " + campaignId + " ";
		}
		if (campaignPriority != 0){
			sql += " AND mca.priority = " + campaignPriority + " ";
		}
		if (userId != 0){
			sql += " AND u.id = " + userId + " ";
		}
		if (userName != null && !userName.equals("")){
			sql += " AND u.user_name = '" + userName.toUpperCase() + "' ";
		}
		if (contactId != 0) {
			sql += " AND pu.contact_id = " + contactId + " ";
		}
		if (!CommonUtil.isParameterEmptyOrNull(timeZone) && !timeZone.equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)) {

			sql += " AND NVL(cn_contacts.gmt_offset,cn_users.gmt_offset) in (" + timeZone + ") ";
		}

		switch (colorType){
			case ConstantsBase.COLOR_BLACK_ID: //  Users who entered to the list within the last 1 hour
				sql += " AND pe.qualification_time >= sysdate - 1/24 " +
					   " AND NVL(i_last_call.reached_calls_num,0) = 0 ";
				break;
			case ConstantsBase.COLOR_BLUE_ID: // Users who entered to the list more than last hour ago
				sql += " AND pe.qualification_time < sysdate - 1/24 " +
				   	   " AND NVL(i_last_call.reached_calls_num,0) = 0 ";
				break;
			case ConstantsBase.COLOR_GREEN_ID: // Green – Users who were called and reached
				sql += " AND NVL(i_last_call.reached_calls_num,0) > 0 ";
				break;
		}

		if(qualificationTimeFilter) {
			if (lastLoginInXMinutes != PopulationsManagerBase.POP_ENT_ONLINE) {
				if (null != fromDate && null != toDate) {
		            sql += " AND (pe.qualification_time IS NULL " +
		            			" OR " +
		            		   " pe.qualification_time between to_date('"+ fmt.format(fromDate) + "','dd/MM/yyyy') " +
													     " and to_date('"+ fmt.format(toDate) + "','dd/MM/yyyy') +1) ";
			    }
			}
		} else {
		
		if (null != fromDate && null != toDate) {
			
			 sql += " AND (ia_last_call.action_time IS NULL " +
			        " OR " +
         		    " ia_last_call.action_time between to_date('"+ fmtWithTime.format(fromDate) + "','dd/MM/yyyy HH24:mi:ss') " +
											     " and to_date('"+ fmtWithTime.format(toDate) + "','dd/MM/yyyy HH24:mi:ss') +1) ";
	    
			
		}
		
		
	}

	    if (!CommonUtil.isParameterEmptyOrNull(priorityId)) {
	    	sql += "AND i_old_pop.priority_id = '" + priorityId + "' " ;
	    }

	    if (true == hideNoAnswer){
	    	sql += " AND (NVL(ia_last_call.issue_action_type_id,0) != " + IssuesManagerBase.ISSUE_ACTION_NO_ANSWER +
		    	       		" OR " +
		    	       	" ia_last_call.action_time < sysdate - 4/24) ";
	    }

	    if (!CommonUtil.isParameterEmptyOrNull(callsCount)) {
	    	sql += " AND NVL(i_last_call.calls_num,0) in (" + callsCount + ") ";
	    }

	    if (!countriesGroup.equals("0")){
	    	sql += " AND exists (SELECT " +
	    							"1 " +
	    						"FROM " +
	    							"countries_group cg " +
	    						"WHERE " +
	    							"cg.country_id = NVL(cn_contacts.id,cn_users.id) " +
	    							"AND cg.countries_group_type_id  = " + countriesGroup + " ) ";
	    }
	    
	    if (countryGroupTierId != 0){
	    	sql += " AND exists (SELECT " +
	    							" 1 " +
	    						" FROM " +
	    							" countries_group cg " +
	    						" WHERE " +
	    							" cg.country_id = NVL(cn_contacts.id,cn_users.id) " +
	    							" AND cg.countries_group_tier_id  = " + countryGroupTierId + " ) ";
	    }

	    if (isNotCalledPops){
	    	sql += " AND pt.is_qualification_updatable = 1 " +
	    		   " AND (ia_last_call.action_time IS NULL " +
	    					" OR " +
	    				" pe.qualification_time > ia_last_call.action_time) ";
	    }

	    if (populationDept != 0) {
	    	if (populationDept == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT) {
	    		sql += " AND pt.type_id = " + PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT +
	    			   " AND u.first_deposit_id is not null " +
	    			   " AND (pt.id <> 6 OR pu.curr_assigned_writer_id is not null)";
	    	} else if (populationDept == PopulationsManagerBase.SALES_TYPE_CONVERSION) {
		    	sql += " AND (pt.type_id = " + PopulationsManagerBase.SALES_TYPE_CONVERSION + " OR (pt.id = " + PopulationsManagerBase.POP_TYPE_CALLME + "AND u.first_deposit_id is null)) ";

		    	 //(pt.type_id = 1  or (pt.id = 6 and  u.first_deposit_id is null))
	    	} else {
	    		sql += " AND pt.type_id = " + populationDept + " ";
	    	}
	    }

	    if (isFromJob) {
	    	if (isAssigned) {
	    		sql += " AND assign_writers.AUTO_ASSIGN = 1 ";
	    	}
	    }


		if (!includeAffiliates.equals("0")) {
			sql += " AND ((u.affiliate_key is not null AND u.affiliate_key in (" + includeAffiliates + ")) " +
							" OR (c.affiliate_key is not null AND c.affiliate_key in (" + includeAffiliates + "))  " +
						  " ) ";
		}

		if (!excludeAffiliates.equals("0")) {
			sql += " AND ((u.affiliate_key IS NULL OR u.affiliate_key = 0 OR u.affiliate_key NOT IN (" + excludeAffiliates + ")) " +
				  "			 AND " +
				  "     (c.affiliate_key IS  NULL OR c.affiliate_key = 0 OR c.affiliate_key NOT IN (" + excludeAffiliates + "))  " +  
				  " ) ";	   			
		}

	    if (isSpecialCare){
	    	sql += " AND u.time_sc_turnover is not null " +
	    		   " AND u.time_sc_house_result is not null ";
	    }

	    if(fromLogin != null ){
	    	sql += " AND u.time_last_login >= ? " ;
	    }
	    if (toLogin != null ){
	    		sql += " AND u.time_last_login <= ? ";
	    }

	    if (balanceBelowMinInvestAmount) {
	    	sql += " AND u.balance < ( SELECT " +
	    			"						ilgc.value " +
                    "				   FROM " +
                    "						investment_limit_group_curr ilgc " +
                    "				   WHERE " +
                    "						u.currency_id = ilgc.currency_id AND " +
                    "						ilgc.investment_limit_group_id = " + ConstantsBase.INVESTMENT_LIMITS_GROUP_ID + " )";
	    }

	    if (madeDepositButdidntMakeInv24HAfter) {
	    	sql += " AND dep_no_inv_24h = 1 ";
	    }

	    if (userStatusId != 0) {
	    	sql += " AND u.status_id = " + userStatusId + " ";
	    }

	    if (userRankId != 0) {
	    	sql += " AND u.rank_id = " + userRankId + " ";
	    }

	    if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
	    	sql += " AND u.time_last_login >= ? " ;
	    }

	    if (isFromJob) {
	    	sql += " AND ilgc.investment_limit_group_id = 1 ";
	    }
	    
	    if(issueActionTypeId != -1) {
	    	sql += " AND ia_last_call.issue_action_type_id = "+issueActionTypeId+ " ";
	    }
	    
	    if (everCalled != ConstantsBase.ALL_FILTER_ID) {
	    	String subSql = "not exists";
	    	if (everCalled == ConstantsBase.GENERAL_YES) {
	    		subSql = "exists";
	    	}
	    	sql += " AND " + 
	    				subSql + 
	    				" (SELECT " +
                			" 1 " +
                		" FROM " +
                			" issues i, " +
                			" issue_actions ia, " +
                			" issue_action_types iat, " +
                			" population_users puu " +
                		" WHERE " +
                			" i.user_id = puu.user_id " +
                			" and i.id = ia.issue_id " +
                			" and ia.issue_action_type_id = iat.id " +
                			" and iat.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL + " " +
                			" and i.user_id = pu.user_id)";
	    }
	    
	    // For unassigned - Get users with status sleeper only when they have exact one deposit. 
	    // Get users with status comma when they have count success deposits between 1 to 'max_count_deposit' in skins table.
	    if (!isFromJob && !isAssigned  && assignedWriterId == 0 && (populationDept == PopulationsManagerBase.SALES_TYPE_RETENTION || populationDept == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT)) {
	    	if (userStatusId == UsersManagerBase.USER_STATUS_COMA || userStatusId == UsersManagerBase.USER_STATUS_SLEEPER) {
	    		sql += getQueryCountDeposit();
	    	} else if (userStatusId == UsersManagerBase.USER_STATUS_ALL) {
	    		sql += " AND (((u.status_id <> " + UsersManagerBase.USER_STATUS_COMA +
	    		                 " AND u.status_id <> " + UsersManagerBase.USER_STATUS_SLEEPER + ") " +    
	    		             " OR u.status_id is null) " +
    					" OR ((u.status_id = " + UsersManagerBase.USER_STATUS_COMA +
    					        " OR u.status_id = " + UsersManagerBase.USER_STATUS_SLEEPER + ") " +
    					        getQueryCountDeposit() + " ))";
	    	}
	    }
	    if (isFromJob && !isAssigned && (userStatusId == UsersManagerBase.USER_STATUS_COMA || userStatusId == UsersManagerBase.USER_STATUS_SLEEPER)) {
	    	sql += getQueryCountDeposit();
	    }
	    
	    if (retentionTeamId != ConstantsBase.ALL_FILTER_ID) {
	    	String retentionTeamsCondition = " > ";
	    	if (retentionTeamId == WritersManagerBase.RETENTION_TEAMS_UPGRADE) {
	    		retentionTeamsCondition = " = ";
	    	}
	    	sql += " AND uad.deposits_count " 
	    				+ retentionTeamsCondition + 
						" (SELECT " +
							" enum.value " +
						" FROM " +
							" enumerators enum " +
						" WHERE " +
							" enum.enumerator LIKE '" + ConstantsBase.ENUM_COUNT_DEPOSIT_ENUMERATOR + "' " +
							" AND enum.code LIKE '" + ConstantsBase.ENUM_COUNT_DEPOSIT_CODE + "')";
	    }
	    
	    if (supportMoreOptionsType == PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS) {
	    	sql += " order by pt.id desc, pe.qualification_time desc NULLS LAST ) entries_table ";
	    } else {
	    	sql += " order by pe.qualification_time desc NULLS LAST ) entries_table ";
	    }
	    
		if (reachCustomerFlag == 1){
		    sql += "WHERE  entries_table.reached_call is null and entries_table.curr_assigned_writer_id is not null and sysdate - qualification_time >= 10 and sysdate - qualification_time < 20";
		    
		}
		
        if (reachCustomerFlag == 2){
            sql += "WHERE entries_table.reached_call is null and entries_table.curr_assigned_writer_id is not null and sysdate - qualification_time >= 20 ";
        }
        
        if (reachCustomerFlag == 3){
            sql += "WHERE entries_table.reached_call is null and entries_table.curr_assigned_writer_id is not null ";
        }
        
        if (!CommonUtil.isParameterEmptyOrNull(sortAmountColumn)) {
	        if (sortAmountColumn.equals("1")) {
	        	sql += " ORDER BY first_decline_amount_usd desc nulls last ";
	        } else if (sortAmountColumn.equals("0")) {
	        	sql += " ORDER BY first_decline_amount_usd asc nulls first ";
	        }
        }

	    sql+= ") entr_table_data)";
	    
		return sql;
	}
	
	public static String getQueryCountDeposit() {
		String sql = " AND u.skin_id is not null " +
				" AND user_total_deposit.count_dep BETWEEN " + ConstantsBase.COUNT_ONE_DEPOSIT + " AND " +
													" (SELECT " + 
														" s.max_count_deposit " +
													" FROM " + 
														" skins s " +
													" WHERE " +
														" u.skin_id = s.id) ";
		return sql;
	}



//	public static String getEntriesTableString(String populationTypes, long skinId, long businessSkin, long languageId,
//			long assignedWriterId, long campaignId, String countries, Date fromDate,
//			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
//			boolean declinePopType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
//			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers,
//			long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare, Date fromLogin, Date toLogin, boolean isLastLogin,
//			boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long userStatusId, long populationDept, long userRankId, boolean isFromJob) {
//
//	    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
//
//		String sql =
//		   " (SELECT " +
//		        " entries_table.*, rownum as row_num " +
//		   " FROM " +
//		        " (SELECT " +
//		        	  " pu.id population_users_id, " +
//		              " pu.lock_history_id, " +
//			          " pu.curr_population_entry_id, " +
//		        	  " pu.curr_assigned_writer_id, " +
//			          " CASE WHEN pu.user_id is null THEN c.user_id ELSE pu.user_id END as user_id, " +
//			          " (u.first_name || ' ' || u.last_name) name_user, " +
//			          " u.TIME_SC_TURNOVER, " +
//					  " u.TIME_SC_HOUSE_RESULT," +
//					  " u.currency_id as user_currency, " +
//			          " c.name name_contact, " +
//			          " c.dynamic_parameter as contact_dynamic_param, " +
//			          " u.dynamic_param as user_dynamic_param, " +
//			          " pu.contact_id, " +
//			          " pu.entry_type_id, " +
//			          " pu.entry_type_action_id, " +
//			          " pu.delay_id, " +
//			          " pe.qualification_time, " +
//			          " pe.group_id, " +
//			          " pe.population_id, " +
//			          " pe.is_displayed, " +
//			          " p.name population_name, " +
//			          " p.population_type_id, " +
//			          " cn.id country_id , " +
//			          " cn.gmt_offset country_offset, " +
//			          " ia_last_call.callback_time as call_back_time, " +
//			          " ia_last_call.action_time last_call_time," +
//			          " iat_last_call.reached_status_id last_reached_status_id, " +
//			          " ia_last_call.issue_action_type_id last_action_type_id, " +
//			          " ia_last_call.writer_id last_caller_writer_id, " +
//			          " NVL(i_last_call.calls_num,0) calls_num, " +
//			          " NVL(i_last_call.reached_calls_num,0) reached_calls_num, " +
//			          " u.language_id, " +
//			          " CASE WHEN pu.user_id is null THEN c.skin_id ELSE u.skin_id END as skin_id, " +
//			          " ti_w.user_name last_sales_dep_rep, " +
//			          " mco.campaign_id, " +
//			          " mca.name campaign_name, " +
//                      " mca.priority campaign_priority_id, " +
//                      " u.time_last_login as time_last_login," +
//                      " u.balance as user_balance, " +
//                      " u.dep_no_inv_24h, " +
//                      " ur.rank_name as user_rank_name, " +
//                      "	us.status_name as user_status_name, " +
//                      " total_inv.totalamount as total_turnover, " +
//                      " ur.id as rank_id, " +
//                      " us.id as status_id, " +
//                      " case when u.time_last_login > pe.qualification_time then u.time_last_login else pe.qualification_time end as last_time ";
//			if (isFromJob) {
//			sql += 	  " ,t.amount * t.rate as conversion " +
//					  " ,ilgc.value as limit_value ";
//						}
//			sql+= " FROM " +
//		             " population_users pu " +
//		             	 " LEFT JOIN contacts c on pu.contact_id = c.id " +
//                         " LEFT JOIN users u on pu.user_id = u.id " +
//                         	" LEFT JOIN countries cn on (u.country_id = cn.id OR (u.country_id is null AND c.country_id = cn.id)) " +
//                         	" LEFT JOIN marketing_combinations mco on (u.combination_id = mco.id OR (u.combination_id is null AND c.combination_id = mco.id)) " +
//                         	" LEFT JOIN marketing_campaigns mca on mco.campaign_id = mca.id " +
//                         " LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
//                         	" LEFT JOIN populations p on pe.population_id = p.id" +
//                         		" LEFT JOIN population_types pt on p.population_type_id = pt.id " +
//	         	  		 " LEFT JOIN issues i_last_call on pu.curr_population_entry_id = i_last_call.population_entry_id " +
//	         	  		    " LEFT JOIN issue_actions ia_last_call on i_last_call.last_call_action_id = ia_last_call.id  " +
//	         	  		    " LEFT JOIN issue_action_types iat_last_call on iat_last_call.id = ia_last_call.issue_action_type_id " +
//	         	  		 " LEFT JOIN writers ti_w on ti_w.id = pu.last_sales_dep_rep_id" +
//	         	  		 " LEFT JOIN users_rank ur on ur.id = u.rank_id" +
//	         	  		 " LEFT JOIN users_status us on us.id = u.status_id " +
//	         	  		 " LEFT JOIN (SELECT i.user_id user_id, " +
//	         	  		 "			  SUM(amount) totalamount " +
//	         	  		 "			  FROM investments i " +
//	         	  		 "			  WHERE   is_canceled=0 " +
//	         	  		 "			  group by i.user_id) total_inv on total_inv.user_id = u.id ";
//					if (isFromJob) {
//			sql += 	 	 " LEFT JOIN transactions t ON t.id = u.first_deposit_id " +
//	         	  		 " LEFT JOIN investment_limit_group_curr ilgc ON u.currency_id = ilgc.currency_id ";
//					}
//	    sql += " WHERE ";
//       	  		 if (!isFromJob) {
//		sql +=		" ((u.skin_id is not null and u.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ?)) " +
//					" OR " +
//					" (c.skin_id is not null and c.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ?))) " +
//					" AND ";
//       	  		 }
//		sql +=		" (u.class_id != 0 OR u.class_id IS NULL) ";
//
//	    if (lastSalesDepRepId > 0){ //singel rep
//	    	sql +=  " AND pu.last_sales_dep_rep_id = " + lastSalesDepRepId + " ";
//	    }
//
//	    if (lastSalesDepRepId == ConstantsBase.ALL_REPS_FILTER_ID) {// all reps
//	    	sql +=  " AND pu.last_sales_dep_rep_id IS NOT NULL ";
//	    }
//
//	    if (lastSalesDepRepId == ConstantsBase.NONE_FILTER_ID) {// none rep
//	    	sql +=  " AND pu.last_sales_dep_rep_id IS NULL ";
//	    }
//
//		if (verifiedUsers == ConstantsBase.GENERAL_YES){
//			sql += 	" AND u.is_authorized_mail = 1 ";
//		}else if (verifiedUsers == ConstantsBase.GENERAL_NO){
//			sql += 	" AND u.is_authorized_mail = 0 ";
//		}
//
//	    if (!declinePopType){
//			sql += " AND pu.entry_type_id  = " + ConstantsBase.POP_ENTRY_TYPE_GENERAL + " " +
//				   " AND pu.curr_population_entry_id IS NOT NULL " +
//				   " AND pe.is_displayed = 1 " +
//				   " AND pe.group_id = 1 ";
//		}else {
//			sql += " AND pu.entry_type_id != 5 ";
//		}
//
//	    if (isAssigned){
//			if (assignedWriterId != 0){
//				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
//			} else {
//				sql += " AND pu.curr_assigned_writer_id is not null ";
//			}
//		} else if (!declinePopType){  // ignore when it's decline after approved page
//			if (assignedWriterId != 0) {
//				sql += " AND pu.curr_assigned_writer_id = " + assignedWriterId + " ";
//			} else {
//				sql += " AND pu.curr_assigned_writer_id is null ";
//			}
//		}
//
//	    if (!countries.equals("0")){
//			sql += " AND ((u.country_id is not null AND u.country_id in (" + countries + "))" +
//						" OR " +
//						" (u.country_id is  null AND c.country_id in (" + countries + ")))";
//		}
//
//		if (!excludeCountrries.equals("0")){
//			sql += " AND ((u.country_id is not null AND u.country_id NOT in (" + excludeCountrries + "))" +
//						" OR " +
//						" (u.country_id is null AND c.country_id NOT in (" + excludeCountrries + ")))";
//		}
//
//		if (skinId != 0){
//			sql += " AND ((u.skin_id is not null and u.skin_id in (" + skinId + ")) " +
//						" OR" +
//						" (c.skin_id is not null and c.skin_id in (" + skinId + "))) ";
//		}
//
//		if (businessSkin != 0){
//			sql += " AND ((u.skin_id is not null and u.skin_id in (select s.id " +
//																" from skins s " +
//																" where s.business_case_id = " + businessSkin + ")) " +
//					" OR" +
//						" (c.skin_id is not null and c.skin_id in (select s.id " +
//																" from skins s " +
//																" where s.business_case_id = " + businessSkin + "))) ";
//
//		}
//
//		if (!CommonUtil.IsParameterEmptyOrNull(populationTypes)){
//			sql += " AND p.population_type_id in (" + populationTypes + ") ";
//		}
//		if (declinePopType) {
//			sql += " AND p.dept_id = " + ConstantsBase.DEPARTMENT_SUPPORT + " AND pu.curr_assigned_writer_id is null ";
//		} else {
//			sql += " AND (p.dept_id = " + ConstantsBase.DEPARTMENT_RETENTION + " OR (p.dept_id = " + ConstantsBase.DEPARTMENT_SUPPORT + " AND pu.curr_assigned_writer_id is not null)) ";
//		}
//		if (campaignId != 0){
//			sql += " AND mco.campaign_id = " + campaignId + " ";
//		}
//		if (campaignPriority != 0){
//			sql += " AND mca.priority = " + campaignPriority + " ";
//		}
//		if (userId != 0){
//			sql += " AND u.id = " + userId + " ";
//		}
//		if (userName != null && !userName.equals("")){
//			sql += " AND u.user_name = '" + userName.toUpperCase() + "' ";
//		}
//		if (contactId != 0) {
//			sql += " AND pu.contact_id = " + contactId + " ";
//		}
//		if (!CommonUtil.IsParameterEmptyOrNull(timeZone) && !timeZone.equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)) {
//
//			sql += " AND cn.gmt_offset in (" + timeZone + ") ";
//		}
//
//		switch (colorType){
//			case ConstantsBase.COLOR_BLACK_ID: //  Users who entered to the list within the last 1 hour
//				sql += " AND pe.qualification_time >= sysdate - 1/24 " +
//					   " AND NVL(i_last_call.reached_calls_num,0) = 0 ";
//				break;
//			case ConstantsBase.COLOR_BLUE_ID: // Users who entered to the list more than last hour ago
//				sql += " AND pe.qualification_time < sysdate - 1/24 " +
//				   	   " AND NVL(i_last_call.reached_calls_num,0) = 0 ";
//				break;
//			case ConstantsBase.COLOR_GREEN_ID: // Green – Users who were called and reached
//				sql += " AND NVL(i_last_call.reached_calls_num,0) > 0 ";
//				break;
//		}
//
//	    if (null != fromDate && null != toDate) {
//            sql += "AND (pe.qualification_time IS NULL " +
//            			" OR " +
//            		   " pe.qualification_time between to_date('"+ fmt.format(fromDate) + "','dd/MM/yyyy') " +
//											     " and to_date('"+ fmt.format(toDate) + "','dd/MM/yyyy') +1) ";
//	    }
//
//	    if (!CommonUtil.IsParameterEmptyOrNull(priorityId)) {
//	    	sql += "AND i_old_pop.priority_id = '" + priorityId + "' " ;
//	    }
//
//	    if (true == hideNoAnswer){
//	    	sql += " AND (NVL(ia_last_call.issue_action_type_id,0) != " + IssuesManagerBase.ISSUE_ACTION_NO_ANSWER +
//		    	       		" OR " +
//		    	       	" ia_last_call.action_time < sysdate - 4/24) ";
//	    }
//
//	    if (!CommonUtil.IsParameterEmptyOrNull(callsCount)) {
//	    	sql += " AND NVL(i_last_call.calls_num,0) in (" + callsCount + ") ";
//	    }
//
//	    if (!countriesGroup.equals("0")){
//	    	sql += " AND exists (SELECT " +
//	    							"1 " +
//	    						"FROM " +
//	    							"countries_group cg " +
//	    						"WHERE " +
//	    							"cg.country_id = cn.id " +
//	    							"AND cg.countries_group_type_id  = " + countriesGroup + " ) ";
//	    }
//
//	    if (isNotCalledPops){
//	    	sql += " AND pt.is_qualification_updatable = 1 " +
//	    		   " AND (ia_last_call.action_time IS NULL " +
//	    					" OR " +
//	    				" pe.qualification_time > ia_last_call.action_time) ";
//	    }
//
//	    if (populationDept != 0) {
//	    	sql += " AND pt.type_id = " + populationDept;
//	    }
//
//		if (!includeAffiliates.equals("0")){
//			sql += " AND (u.affiliate_key is not null AND u.affiliate_key in (" + includeAffiliates + "))";
//		}
//
//		if (!excludeAffiliates.equals("0")){
//			sql += " AND (u.affiliate_key is not null AND u.affiliate_key NOT in (" + excludeAffiliates + "))";
//		}
//
//	    if (isSpecialCare){
//	    	sql += " AND u.time_sc_turnover is not null " +
//	    		   " AND u.time_sc_house_result is not null ";
//	    }
//
//	    if(fromLogin != null ){
//	    	sql += " AND u.time_last_login >= ? " ;
//	    }
//	    if (toLogin != null ){
//	    		sql += " AND u.time_last_login <= ? ";
//	    }
//
//	    if (balanceBelowMinInvestAmount) {
//	    	sql += " AND u.balance < ( SELECT " +
//	    			"						ilgc.value " +
//                    "				   FROM " +
//                    "						investment_limit_group_curr ilgc " +
//                    "				   WHERE " +
//                    "						u.currency_id = ilgc.currency_id AND " +
//                    "						ilgc.investment_limit_group_id = " + 1 + " )";
//	    }
//
//	    if (madeDepositButdidntMakeInv24HAfter) {
//	    	sql += " AND dep_no_inv_24h = 1 ";
//	    }
//
//	    if (userStatusId != 0) {
//	    	sql += " AND u.status_id = " + userStatusId + " ";
//	    }
//
//	    if (userRankId != 0) {
//	    	sql += " AND u.rank_id = " + userRankId + " ";
//	    }
//
//	    if (isLastLogin) {
//	    	sql += " AND u.time_last_login >= ? " ;
//	    }
//	    if (isFromJob) {
//	    	sql += "AND ilgc.investment_limit_group_id = 1 ";
//	    }
//
//		sql += " order by pe.qualification_time desc NULLS LAST ) entries_table) ";
//
//		return sql;
//	}

	public static ArrayList<PopulationEntryBase> getAllEntries(Connection con, String populationTypes, String skinIds, long businessSkin, long languageId,
			long assignedWriterId, long campaignId, String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers,
			long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes,
			boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long userStatusId, long populationDept, long userRankId, boolean isFromJob, boolean qualificationTimeFilter, long reachCustomerFlag,
			long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {

		ArrayList<PopulationEntryBase> list = new ArrayList<PopulationEntryBase>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		//HashMap<Long, Writer> writersMap = wr.getAllWritersMap();
		int index = 1;

		try {
			String order = "desc";
			if (isFromJob && userStatusId != UsersManagerBase.USER_STATUS_COMA) {				
				order = "asc";
			}
			String sql = " select " +
							" ent.* " +
						 " from (select * " +
								 " from " + getEntriesTableString(populationTypes,
										 						  skinIds,
										 						  businessSkin,
										 						  languageId,
										 						  assignedWriterId,
										 						  campaignId,
										 						  countries,
										 						  fromDate,
										 						  toDate,
										 						  colorType,
										 						  isAssigned,
										 						  userId,
										 						  userName,
										 						  contactId,
										 						  supportMoreOptionsType,
										 						  timeZone,
										 						  priorityId,
										 						  excludeCountrries,
										 						  hideNoAnswer,
										 						  callsCount,
										 						  countriesGroup,
										 						  isNotCalledPops,
										 						  includeAffiliates,
										 						  excludeAffiliates,
										 						  verifiedUsers,
										 						  lastSalesDepRepId,
										 						  campaignPriority,
										 						  isSpecialCare,
										 						  fromLogin,
										 						  toLogin,
										 						  lastLoginInXMinutes,
										 						  balanceBelowMinInvestAmount,
										 						  madeDepositButdidntMakeInv24HAfter,
										 						  userStatusId,
										 						  populationDept,
										 						  userRankId,
										 						  isFromJob,
										 						  qualificationTimeFilter, 
										 						 ConstantsBase.DO_NOT_SELECT,
										 						 reachCustomerFlag,
										 						 retentionTeamId,
										 						 everCalled,
										 						 sortAmountColumn, 
										 						 userPlatform,
										 						 countryGroupTierId);

				sql +=   /*" where   " +
						         " row_num > ? " +
						         " and row_num <= ? " +*/
						" ) ent " +
						" ORDER BY last_time " + order;
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					PopulationEntryBase vo = new PopulationEntryBase();
					com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
					vo.setUserRankId(rs.getLong("user_rank_id"));
					vo.setUserStatusId(rs.getLong("user_status_id"));
					vo.setTimeLastLogin(convertToDate(rs.getTimestamp("time_last_login")));
					vo.setConversion(rs.getDouble("conversion"));
					vo.setCampaignPriorityId(rs.getInt("campaign_priority_id"));
	                vo.setUserName(rs.getString("user_name"));
	                vo.setUserClassId(rs.getLong("class_id"));
					if (rs.getLong("user_balance") < rs.getDouble("limit_value")) {
						vo.setBalanceBelowMinInvestAmount(true);
					}
	                if (rs.getLong("dep_no_inv_24h") == 1) {
	                	vo.setMadeDepositButdidntMakeInv24HAfter(true);
	                }
	                if (isFromJob) {
	                	vo.setSumDepositsBaseAmount(rs.getDouble("sum_deposit"));
	                }
					list.add(vo);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		return list;
	}


	/**
	 * Return coma representatives that are participants with auto assignment job and their  status and rank distribution  
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getComaRepSI(Connection con, long specificWriterId) throws SQLException {
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			String sql =" SELECT " +
						"	w.*, " +
						"   ws.id ws_id, " +
						"   ws.skin_id ws_skin_id, " +
						"	ws.assign_limit ws_assign_limit, " +
						"   srd.rank_id rank_id, " +
						"   srd.percentage rank_percentage, " +
						"   ssd.status_id status_id, " +
						"   ssd.percentage status_percentage " +
						" FROM " +
						"	writers w, " +
						"	writers_skin ws, " +
						"	writer_rank_distribution wrd, " +
						"	sales_rank_distribution srd, " +
						"   sales_status_distribution ssd " +
						" WHERE " +
						"	ws.writer_id = w.id " +
						"	AND w.is_active = 1 " +
						"   AND w.auto_assign = 1 " +
						" 	AND ws.assign_limit <> 0 " +
						" 	AND ws.assign_limit is not null " +
						"   AND wrd.writer_skin_id = ws.id " +
						"   AND wrd.rank_distribution_group_id = srd.group_id " +
						"   AND srd.status_distribution_group_id = ssd.group_id " +						
						"   AND w.sales_type = ? " +
						"   AND w.sales_type_dept_id = ? ";
						if (specificWriterId > 0) {
				sql+=	"	AND w.id = ? "; 
						}						
				sql+=	" ORDER BY " +
						"	w.user_name, " +
						"	ws.skin_id, " +
						"	rank_id, " +
						"	status_id ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, PopulationsManagerBase.SALES_TYPE_RETENTION);
			ps.setLong(2, ConstantsBase.SALES_TYPE_DEPARTMENT_COMA_RETENTION);
			if (specificWriterId > 0) {
				ps.setLong(3, specificWriterId);
			}			
			rs = ps.executeQuery();
			while (rs.next()) {
				long writerId = rs.getLong("id");
				Writer writer = list.get(writerId);
				if (writer == null) { // create new writer
					Writer tmp = new Writer();
					tmp.setId(writerId);
					tmp.setUserName(rs.getString("USER_NAME"));
					tmp.setFirstName(rs.getString("FIRST_NAME"));
					tmp.setLastName(rs.getString("LAST_NAME"));
					writer = tmp;
				}
				long skinId = rs.getLong("ws_skin_id");
				WritersSkin ws = writer.getWriterSkins().get(skinId);
				if (ws == null) { // create new writer skin
					WritersSkin tmp = new WritersSkin();
					tmp.setId(rs.getLong("ws_id"));
					tmp.setSkinId(skinId);
					tmp.setWriterId(writerId);
					tmp.setAssignLimit(rs.getLong("ws_assign_limit"));
					ws = tmp;
				}
				//RankStatusLimits rsl = ws.getRankStatusLimits();
				HashMap<Long,RankLimits> rankLimitsHM = ws.getRankStatusLimits();
				if (rankLimitsHM == null) { // create new rank status limits
					HashMap<Long,RankLimits> tmp = new HashMap<Long,RankLimits>();
					ws.setRankStatusLimits(tmp);
					rankLimitsHM = tmp;
				}
				long rankId = rs.getLong("rank_id");
				long statusId = rs.getLong("status_id");
				double rankLimit = rs.getDouble("rank_percentage");
				double statusLimit = rs.getDouble("status_percentage");
				RankLimits rl = rankLimitsHM.get(rankId);
				if (rl == null) { //create rank limit
					RankLimits tmp = new RankLimits();
					rankLimitsHM.put(rankId, tmp);
					rl = tmp;
				}
				rl.setLimit(rankLimit);
				rl.getStatusLimit().put(statusId, statusLimit);
				writer.getWriterSkins().put(skinId, ws);
				list.put(writerId, writer);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}
	
	/**
	 * Return Initial Stage representatives that are participants with auto assignment job 
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getInitialStageRepSI(Connection con, long specificWriterId) throws SQLException {
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null; 
		try {
			String sql =" SELECT " +
						"	w.*, " +
						"   ws.id ws_id, " +
						"   ws.skin_id ws_skin_id, " +
						"	ws.assign_limit ws_assign_limit " +
						" FROM " +
						"	writers w, " +
						"	writers_skin ws " +
						" WHERE " +
						"	ws.writer_id = w.id " +
						"	AND w.is_active = 1 " +
						"   AND w.auto_assign = 1 " +
						" 	AND ws.assign_limit <> 0 " +
						" 	AND ws.assign_limit is not null " +	
						"   AND w.sales_type = ? " +
						"   AND w.sales_type_dept_id in ( ?, ? ) ";
						if (specificWriterId > 0) {
				sql+=	"	AND w.id = ? "; 
						}
				sql+=	" ORDER BY " +
						"	w.user_name, " +
						"	ws.skin_id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, PopulationsManagerBase.SALES_TYPE_RETENTION);
			ps.setLong(2, ConstantsBase.SALES_TYPE_DEPARTMENT_GENERAL_RETENTION);
			ps.setLong(3, ConstantsBase.SALES_TYPE_DEPARTMENT_UPGRADE_RETENTION); 
			if (specificWriterId > 0) {
				ps.setLong(3, specificWriterId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {		
				long writerId = rs.getLong("id");
				Writer writer = list.get(writerId);
				if (writer == null) { // create new writer
					Writer tmp = new Writer();
					tmp.setId(writerId);
					tmp.setUserName(rs.getString("USER_NAME"));
					tmp.setFirstName(rs.getString("FIRST_NAME"));
					tmp.setLastName(rs.getString("LAST_NAME"));
					writer = tmp;
				}
				long skinId = rs.getLong("ws_skin_id");
				WritersSkin ws = writer.getWriterSkins().get(skinId);
				if (ws == null) { // create new writer skin
					WritersSkin tmp = new WritersSkin();
					tmp.setId(rs.getLong("ws_id"));
					tmp.setSkinId(skinId);
					tmp.setWriterId(writerId);
					tmp.setAssignLimit(rs.getLong("ws_assign_limit"));
					ws = tmp;
				}
				writer.getWriterSkins().put(skinId, ws);
				list.put(writerId, writer);				
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * Get population entries for update population
	 * @param con
	 * @param peb
	 * @param user
	 * @return ArrayList<PopulationEntryBase>
	 * @throws SQLException
	 */
	public static ArrayList<PopulationEntryBase> getPopEntriesForUpdatePop(Connection con, PopulationEntryBase peb, UserBase user) throws SQLException  {
		ArrayList<PopulationEntryBase> list = new ArrayList<PopulationEntryBase>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql =" SELECT " +
							" pe.id as pop_entry_id, " +
							" pu.user_id, " +
							" pu.contact_id " +
						" FROM " +
							" population_users pu, " +
							" population_entries pe, " +
							" populations p, " +
							" users u " +
						" WHERE " +
							" pu.curr_population_entry_id = pe.id " +
							" and pe.population_id = p.id " +
							" and pu.user_id = u.id " +
							" and pu.curr_assigned_writer_id is null " +
							" and p.population_type_id = ? " +
							" and u.is_declined = ? " +
							" and u.last_decline_date < sysdate - ? " +
							" and u.id = DECODE(?, 0, u.id, ?) ";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, peb.getCurrPopualtionTypeId());
			ps.setLong(index++, user.isDecline() ? 1 : 0);
			ps.setLong(index++, user.getNumDaysLastDecline());
			ps.setLong(index++, user.getId());
			ps.setLong(index++, user.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				PopulationEntryBase entry = new PopulationEntryBase();
				entry.setCurrEntryId(rs.getLong("pop_entry_id"));
				entry.setUserId(rs.getLong("user_id"));
				entry.setContactId(rs.getLong("contact_id"));
				list.add(entry);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * check if user is in retention population
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean isRetentionPopulation(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "SELECT " +
							" 1 " +
						 "FROM " +
							"users u, " +
							"population_users pu, " +
							"population_entries pe, " +
							"populations p, " +
							"population_types pt " +
						 "WHERE " +
							"pu.user_id = ? AND " +
							"u.id = pu.user_id AND " +
							"pe.id = pu.curr_population_entry_id AND " +
							"pe.population_id = p.id AND " +
							"p.population_type_id = pt.id AND " +
							"((pt.type_id in (" + PopulationsManagerBase.POP_TYPE_TYPE_RETENTION + ", " + PopulationsManagerBase.POP_TYPE_TYPE_IMMEDIATE_TREATMENT + ") AND " +
							  "pt.id <> " + PopulationsManagerBase.POP_TYPE_CALLME + ") " +
							"OR " +
							"(pt.id = " + PopulationsManagerBase.POP_TYPE_CALLME + " AND " +
							      "u.first_deposit_id is not null " +
							"))";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
}
