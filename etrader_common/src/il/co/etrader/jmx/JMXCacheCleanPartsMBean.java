package il.co.etrader.jmx;

public interface JMXCacheCleanPartsMBean {

	/**
	 * clear the part of cache
	 */

	public boolean initSkinAssetIndexMarkets();
	public boolean resetSkinAssetIndexMarkets();
	
	public boolean runCutsomerReportJob();
}
