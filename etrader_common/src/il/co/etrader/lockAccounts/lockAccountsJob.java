package il.co.etrader.lockAccounts;

import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * LockAccountsJob class.
 * This class represent the locked Accounts job for anyoption.
 * We need to lock accounts of users that over the deposit limits.
 * The trigger for the lock is "DOCS_REQUIRED = 1" fields in users table.
 *
 * @author Kobi
 */
public class lockAccountsJob extends JobUtil{
    private static Logger log = Logger.getLogger(lockAccountsJob.class);
    private static StringBuffer report = new StringBuffer();

    /**
     * Retrieve all users that need to lock.
     *
     * @return list of users
     * @throws SQLException
     */
    private static ArrayList<Long> retrieveUsersToLock() throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Long> list = new ArrayList<Long>();
        String userName = "";
        long id = 0;

        report.append("<br><b>LockAccountsJob Information:</b><table border=\"1\">");
        report.append("<tr><td></td></tr>");
        report.append("<tr><td><b>userId</b></td><td><b>UserName</b></td></tr>");

        try {
            con = getConnection("lock.db.url","lock.db.user","lock.db.pass");


		    String sql = "select id, user_name from users " +
		    		     "where is_active = 1 and id_doc_verify = 0 " +
		    		     "and docs_required = 1 " +
		    		     "order by user_name ";


		    ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			while (rs.next()) {

				userName = rs.getString("user_name");
				id = rs.getLong("id");
				list.add(id);

				report.append("<tr>");
                report.append("<td>").append(id).append("</td>");
                report.append("<td>").append(userName).append("</td>");
                report.append("</tr>");
			}

         }  catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		ps.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }

        return list;

    }



    /**
     * lock user bu user id
     *
     * @param userId:
     * 		 user id for lock
     */
    public static void updateLockUsers(Long userId) {

            Connection con = null;
            PreparedStatement pstmt = null;

    		try {
    			con = getConnection("lock.db.url","lock.db.user","lock.db.pass");

		        String sql = "update users " +
			          		 "set " +
			          		 "is_active = 0 " +
			          		 "where id = ? ";

		        pstmt = con.prepareStatement(sql);
		        pstmt.setLong(1, userId);
		        pstmt.executeUpdate();


    		}
    		catch (Exception e) {
                log.log(Level.ERROR, "Error while updating user id :" + userId + " " + e);
            }
    		finally {
    				try {
    					pstmt.close();
    				} catch (Exception e) {
    					log.log(Level.ERROR, "Can't close", e);
    				}
    				try {
                		con.close();
                	} catch (Exception e) {
                    log.log(Level.ERROR, "Can't close connection", e);
                	}
    		}
    }

    /**
     * lock accounts Job.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the lock accounts job.");
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to retrieve Users that need to lock.");
        }
        ArrayList<Long> usersList = retrieveUsersToLock();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to lock users.");
        }

        // Update users columns to lock the users
        for (Long u : usersList) {
        	updateLockUsers(u.longValue());
        }

        // build the report
        String reportBody = "<html><body>" +
        						report.toString() +
        					"</body></html>";


	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report email: " + reportBody);
	    }

	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("lock.email.url"));
	    server.put("auth", getPropertyByFile("lock.email.auth"));
	    server.put("user", getPropertyByFile("lock.email.user"));
	    server.put("pass", getPropertyByFile("lock.email.pass"));
	    server.put("contenttype", getPropertyByFile("lock.email.contenttype", "text/html; charset=UTF-8"));

	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", getPropertyByFile("lock.email.subject"));
	    email.put("to", getPropertyByFile("lock.email.to"));
	    email.put("from", getPropertyByFile("lock.email.from"));
	    email.put("body", reportBody);
	    sendEmail(server, email);

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "LockAccountsJob completed!, users were locked.");
        }

   }
}
