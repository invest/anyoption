package il.co.etrader.bl_managers;
import il.co.etrader.bl_vos.TaxHistory;
import il.co.etrader.dao_managers.TaxHistoryDAO;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * TaxHistory manager Base.
 *
 * @author Kobi
 */
public abstract class TaxHistoryManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(TaxHistoryManagerBase.class);


	/**
	 * Returns the user tax for mid-year, period1
	 * @param userId
	 * @return
	 * 		tax of user for mid-year
	 * @throws Exception
	 */
	public static long getTaxBalanceForPerionOneTxt(long userId) throws Exception {
		long taxPeriodOne = 0;
		Connection con = getConnection();
		try { // get tax mid-year
			taxPeriodOne = TaxHistoryDAO.getMidYearTax(con, userId);
		}
		finally {
			closeConnection(con);
		}
		return taxPeriodOne;
	}


	/**
	 * Get TaxHistory
	 * @param userId
	 * 		user id
	 * @param year
	 * 		year of the job
	 * @param period
	 * 		period of the year
	 *
	 * @return
	 * 		TaxHistory instance
	 * @throws SQLException
	 */
	public static TaxHistory get(long userId, long year, String period) throws SQLException {

		TaxHistory t = new TaxHistory();
		Connection con = getConnection();
		try {
			t = TaxHistoryDAO.get(con, userId, year, period);
		}
		finally {
			closeConnection(con);
		}
		return t;

	}



}
