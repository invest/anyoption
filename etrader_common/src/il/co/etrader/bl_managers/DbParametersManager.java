package il.co.etrader.bl_managers;

import il.co.etrader.dao_managers.DbParametersDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.managers.BaseBLManager;

/**
 * 
 * @author eyal.ohana
 *
 */
public class DbParametersManager extends BaseBLManager {
	public static final int DB_PARAMS_FIRE_PIXEL = 16;
	public static final int DB_PARAMS_AFFILIATE_LEADS = 17;
	
	/**
	 * get Last Run
	 * @param dbParamId
	 * @return
	 * @throws SQLException
	 */
	public static Date getLastRun(int dbParamId) throws SQLException {
		Connection con = getConnection();
		try {
			return DbParametersDAO.getLastRunById(con, dbParamId);
		} finally {
			closeConnection(con);
		}
	}
	
    /**
     * Update Last Run
     * @param dbParamId
     * @throws SQLException
     */
    public static void updateLastRun(long dbParamId, Date date) throws SQLException {
    	Connection con = getConnection();
    	try {
    		DbParametersDAO.updateLastRun(con, dbParamId, date);
    	} finally {
    		closeConnection(con);
    	}
    }
}
