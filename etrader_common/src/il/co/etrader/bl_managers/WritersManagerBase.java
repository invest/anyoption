/**
 * 
 */
package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.WritersDAO;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The base manager for the writers
 * 
 * @author kirilim
 */
public class WritersManagerBase extends com.anyoption.common.managers.WritersManagerBase {
	
	public static final long RETENTION_TEAMS_RETENTION  = 1;
	public static final long RETENTION_TEAMS_UPGRADE = 3;

	public static Writer getWriter(long id) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAO.get(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static long getWrterIdByUserName(String username) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAO.getWrterIdByUserName(con, username);
		} finally {
			closeConnection(con);
		}
	}
}
