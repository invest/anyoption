package il.co.etrader.bl_managers;



import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.dao_managers.TiersDAOBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.managers.BaseBLManager;


/**
 * Tiers ManagerBase class
 * @author Kobi
 *
 */
public class TiersManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(TiersManagerBase.class);
	protected static HashMap<Long, Tier> tiersList;
	
	public static final long TIER_ACTION_QUALIFIED = 1;
	public static final long TIER_ACTION_REMOVED = 2;
	public static final long TIER_ACTION_CONV_POINTS_TO_CASH = 3;
	public static final long TIER_ACTION_OBTAIN_POINTS = 4;
	public static final long TIER_ACTION_POINTS_EXPIRED = 5;
	public static final long TIER_ACTION_WELCOME_BONUS = 6;
	public static final long TIER_ACTION_REVERSE_POINTS = 7;
	public static final long TIER_ACTION_EXPIRATION_WARNING = 8;

	public static final long TIER_LEVEL_BLUE = 1;
	public static final long TIER_LEVEL_SILVER = 2;
	public static final long TIER_LEVEL_GOLD = 3;
	public static final long TIER_LEVEL_PLATINUM = 4;

	/**
	 * Update user loylaty points
	 * @param tierUserId
	 * @param writerId
	 * @param points
	 * @throws SQLException
	 */
	public static void updateTierUser(long tierUserId, long writerId, long points) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			updateTierUser(con, tierUserId, writerId, points);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert new action to tier history table
	 * @param tH TierHistory instance
	 * @throws SQLException
	 */
	public static void insertIntoTierHistory(TierUserHistory tH) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			insertIntoTierHistory(con, tH);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update user loylaty points with connection param
	 * @param tierUserId
	 * @param writerId
	 * @param points
	 * @throws SQLException
	 */
	public static void updateTierUser(Connection con, long tierUserId, long writerId, long points) throws SQLException {
		log.debug("Going to update user LPoints after invest, tierUserId: " + tierUserId +
					" InvPoints: " + points);
		TiersDAOBase.updateUserTier(con, tierUserId, writerId, points);
	}

	/**
	 * Insert new action to tier history table with connection param
	 * @param tH TierHistory instance
	 * @throws SQLException
	 */
	public static void insertIntoTierHistory(Connection con, TierUserHistory tH) throws SQLException {
		log.debug("Going to insert new action to TierHis, " + tH.toString());
		TiersDAOBase.insertIntoTierHistory(con, tH);
	}

	/**
	 * get percent to points to cash convert
	 * @param points user points
	 * @return
	 * @throws SQLException
	 */
	public static double getPointToCashPercent(long points) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TiersDAOBase.getPointToCashPercent(con, points);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get accumulated points
	 * @param userId
	 * @param qualificationDate
	 * @return
	 * @throws SQLException
	 */
	public static long getAccumulatedPoints(long userId, Date qualificationDate) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TiersDAOBase.getAccumulatedPoints(con, userId, qualificationDate);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Convert points to cash
	 * @param points number of points to convert
	 * @param tierId  user tier
	 * @return
	 * @throws SQLException
	 */
	public static double convertPointsToCash(long points, long tierId) throws SQLException {
		Tier t = getTierById(tierId);
		double percent = getPointToCashPercent(points);
		double convertBenefit = ((double)t.getConvertBenefitPercent()/100);
		return ((points * percent) * (1 + convertBenefit));
	}

	/**
	 * Create TierUSerHistory instance by instance params
	 * @return
	 */
	public static TierUserHistory getTierUserHisIns(long tierId, long userId, long actionId, long writerId, long points,
			long actionPoints, long keyValue, String tableName, String utcOffset) {

    	TierUserHistory h = new TierUserHistory();
    	h.setTierId(tierId);
    	h.setUserId(userId);
    	h.setTierHistoryActionId(actionId);
    	h.setWriterId(writerId);
    	h.setPoints(points);
    	h.setActionPoints(actionPoints);
    	h.setKeyValue(keyValue);
    	h.setTableName(tableName);
    	h.setUtcOffset(utcOffset);
    	return h;
	}

	/**
	 * Get TierUser instance by userId
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static TierUser loadTierUserByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TiersDAOBase.getTierUserByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get Tier instance by Id
	 * @param tierID
	 * @return
	 * @throws SQLException
	 */
	public static Tier getTierByIdDB(long tierId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TiersDAOBase.getTierById(con, tierId);
		} finally {
			closeConnection(con);
		}
	}
	
	private static void loadTiersList() {
		if (tiersList==null){
			 tiersList = new HashMap<Long, Tier>();
			 Connection con = null;
			 try {
				 con = getConnection();
				 tiersList = TiersDAOBase.getTiers(con);
			} catch (SQLException e) {
				e.printStackTrace();
				log.error("Unable to load Tiers", e);
			}finally {
				closeConnection(con);
			}
		}
	}
	
	/**
	 * Get tier by id
	 * @param tierId
	 * @return
	 */
	public static Tier getTierById(long tierId) {
		loadTiersList();
		return tiersList.get(new Long(tierId));
	}
	
	/**
	 * Get tiers list
	 * @return
	 */
	public static HashMap<Long, Tier> getTiersList() {
		loadTiersList();
		return tiersList;
	}
	
	public Tier getBlueTier() {
		return getTierById(TiersManagerBase.TIER_LEVEL_BLUE);
	}

	public Tier getSilverTier() {
		return getTierById(TiersManagerBase.TIER_LEVEL_SILVER);
	}

	public Tier getGoldTier() {
		return getTierById(TiersManagerBase.TIER_LEVEL_GOLD);
	}

	public Tier getPlatinumTier() {
		return getTierById(TiersManagerBase.TIER_LEVEL_PLATINUM);
	}
	
	public static boolean isPointsConvertAllow(TierUser tierUser) {
		Tier t = getTierById(tierUser.getUserId());
		if (tierUser.getPoints() >= t.getConvertMinPoints()) {
			return true;
		}
		return false;
	}
}
