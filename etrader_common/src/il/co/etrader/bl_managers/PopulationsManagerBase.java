package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.PopulationDelay;
import il.co.etrader.bl_vos.PopulationEntryHis;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.population.PopNoXDepositHandler;
import il.co.etrader.population.PopulationHandlerBase;
import il.co.etrader.population.PopulationHandlerFactory;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * PopulationsManagerBase class
 * 		used also for keeping all population handlers
 * @author Kobi
 *
 */
public abstract class PopulationsManagerBase extends com.anyoption.common.managers.PopulationsManagerBase {
	private static final Logger log = Logger.getLogger(PopulationsManagerBase.class);

	// populations types
	public static final int POP_TYPE_DECLINE_FIRST = 1;
	public static final int POP_TYPE_DECLINE_LAST = 2;
	public static final int POP_TYPE_ONE_DEPOSIT = 3; // not active
	public static final int POP_TYPE_DORMANT = 4; // not active
	public static final int POP_TYPE_NO_DEPOSIT = 5;
	public static final int POP_TYPE_CALLME = 6;
	public static final int POP_TYPE_OTHER = 7;
	public static final int POP_TYPE_MIN_NO_DEPOSIT = 8;
	public static final int POP_TYPE_NEW_DORMANT = 9;
	public static final int POP_TYPE_NO_INVESTMENTS = 10;
	public static final int POP_TYPE_SPECIAL_CONVERSION = 11;
	public static final int POP_TYPE_DECLINED_SALES = 12;
	public static final int POP_TYPE_MARKETING = 13;
	public static final int POP_TYPE_NO_2ND_DEPOSIT = 14;
	public static final int POP_TYPE_NO_3RD_DEPOSIT = 15;
	public static final int POP_TYPE_NO_4TH_DEPOSIT = 16;
	public static final int POP_TYPE_NO_5TH_DEPOSIT = 17;
	public static final int POP_TYPE_NO_6TH_DEPOSIT = 18;
	public static final int POP_TYPE_NO_7TH_DEPOSIT = 19;
	public static final int POP_TYPE_SHORT_REG_FORM = 20;
	public static final int POP_TYPE_REGISTER_ATTEMPT = 21;
	public static final int POP_TYPE_OPEN_WITHDRAW = 22;
	public static final int POP_TYPE_RETENTION = 23;
	public static final int POP_TYPE_DORMENT_CONVERTION = 24;
	public static final int POP_TYPE_MANUALLY_IMPORTED_CONVERSION = 25;
	public static final int POP_TYPE_FIRST_DEPOSIT_DECLINE_DOCS_REQUIRED = 26;
	public static final int POP_TYPE_QUICK_START = 27;
	public static final int POP_TYPE_SPECIAL_RETENTION = 28;

	// population entries history statuses
	public static final int POP_ENT_HIS_STATUS_NEW = 1;
	public static final int POP_ENT_HIS_STATUS_ASSIGNED = 2;
	public static final int POP_ENT_HIS_STATUS_LOCKED = 3;
	public static final int POP_ENT_HIS_STATUS_UNLOCKED = 4;
	public static final int POP_ENT_HIS_STATUS_UPDATE = 5;
	public static final int POP_ENT_HIS_STATUS_CANCEL_ASSIGNE = 6;
	public static final int POP_ENT_HIS_STATUS_REQUALIFICATION = 7;
	public static final int POP_ENT_HIS_STATUS_BACK_TO_SALES_BY_CHANGE_DETAILS = 8;
	public static final int POP_ENT_HIS_STATUS_BACK_TO_SALES_MANUALLY = 9;
	public static final int POP_ENT_HIS_STATUS_REACTIVATE_ACCOUNT = 10;
	public static final int POP_ENT_HIS_STATUS_DISABLE_PHONE_CONTACT_DAA = 11;
	public static final int POP_ENT_HIS_STATUS_DISPLAY_POPULATION = 12;

	public static final int POP_ENT_HIS_STATUS_REMOVED_ISSUE = 100;
	public static final int POP_ENT_HIS_STATUS_REMOVED_MANUALLY = 101;
	public static final int POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION = 102;
	public static final int POP_ENT_HIS_STATUS_REMOVED_INACTIVE = 103;
	public static final int POP_ENT_HIS_STATUS_REMOVED_FRAUD = 104;
	public static final int POP_ENT_HIS_STATUS_REMOVED_OTHER = 105;
	public static final int POP_ENT_HIS_STATUS_REMOVED_FALSE_ACCOUNT = 106;
	public static final int POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT = 107;
	public static final int POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA = 108;
	public static final int POP_ENT_HIS_STATUS_REMOVED_NOT_INTERESTED = 109;
	public static final int POP_ENT_HIS_STATUS_REMOVED_WRONG_NUMBER = 110;
	public static final int POP_ENT_HIS_STATUS_REMOVED_X_CALLS_AND_REACHED_BEFORE = 111;
	public static final int POP_ENT_HIS_STATUS_REMOVED_X_CALLS_AND_NEVER_REACHED = 112;
	public static final int POP_ENT_HIS_STATUS_REMOVED_CURR_POP_SUPPORT = 115;
	public static final int POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA_DE = 118;

	public static final int POP_ENT_HIS_STATUS_TRACKING = 200;
	public static final int POP_ENT_HIS_STATUS_TRACKING_RENEW = 201;
	public static final int POP_ENT_HIS_STATUS_TRACKING_REMOVE_BY_TIME = 202;
	public static final int POP_ENT_HIS_STATUS_TRACKING_REMOVE_BY_DEPOSIT = 203;
	public static final int POP_ENT_HIS_STATUS_TRACKING_REMOVE_BY_CALLBACK = 204;
	public static final int POP_ENT_HIS_STATUS_TRACKING_REMOVE_BY_REVIEW = 205;
	public static final int POP_ENT_HIS_STATUS_CALLBACK = 210;
	public static final int POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_CALL = 211;
	public static final int POP_ENT_HIS_STATUS_CALLBACK_REMOVE_MANUALLY = 212;
	public static final int POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_REVIEW = 213;
	public static final int POP_ENT_HIS_STATUS_CALLBACK_CANCEL_ASSIGN = 214;
	public static final int POP_ENT_HIS_STATUS_REVIEW = 220;
	public static final int POP_ENT_HIS_STATUS_REVIEW_REMOVE_PERMANENTLY = 221;
	public static final int POP_ENT_HIS_STATUS_REVIEW_REMOVE_CURR_POP = 222;
	public static final int POP_ENT_HIS_STATUS_REVIEW_REMOVE_BACK_TO_SALES= 223;

	public static final int POP_ENT_HIS_STATUS_REVIEW_FROM_MAX_DELAY = 400;

	public static final int POP_ENT_HIS_STATUS_DELAY_NI = 600;
	public static final int POP_ENT_HIS_STATUS_DELAY_NA = 601;
	public static final int POP_ENT_HIS_STATUS_DELAY_REMOVE_BY_POP = 650;
	public static final int POP_ENT_HIS_STATUS_DELAY_END_TIME_SWITCH_POP = 651;
	public static final int POP_ENT_HIS_STATUS_DELAY_END_TIME_SAME_POP = 652;

	// The value that all NOT_REMOVED statuses are lower from.
	public static final int POP_ENT_HIS_STATUS_NOT_REMOVED = 100;

	public static final String POP_TYPE_SPECIAL_CB_DEPOSIT = "CB deposit";

	public static final int RECENT_QUALIFICATION_TIME_HOURS = 24;

	public static final int POP_PUBLIC_ASSIGN = 0;

	public static final int POP_DELAY_TYPE_NI = 1;
	public static final int POP_DELAY_TYPE_NA = 2;
	
	public static final int POP_DEPT_ID_SALES = 3;
	
	public static final int SALES_TYPE_CONVERSION = 1;
	public static final int SALES_TYPE_RETENTION = 2;
	public static final int SALES_TYPE_IMMEDIATE_TREATMENT = 3;
	
	public static final int POP_TYPE_TYPE_RETENTION = 2;
	public static final int POP_TYPE_TYPE_IMMEDIATE_TREATMENT = 3;
	
	public static final long ENTRY_TYPE_REMOVE_PERMANENTLY = 5;
	
	public static final long SUPPORT_MORE_OPTIONS_TYPE_DECLINED_DEPOSIT_ATTEMPTS = 1;
	public static final long SUPPORT_MORE_OPTIONS_TYPE_CALL_ME = 2;
	
	public static final long POP_ENT_ONLINE = 5;

	private static Hashtable<Long, PopulationHandlerBase> populationHandlers;    // <populationType,Handler instance>

	/**
	 * Load population handlers
	 * @param con db connection
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public static void loadPopulatioHandlers(Connection con) throws SQLException, PopulationHandlersException {
        if (null == populationHandlers) {
        	populationHandlers = new Hashtable<Long, PopulationHandlerBase>();
            try {
                ArrayList<PopulationType> l = PopulationsDAOBase.getPopulationTypes(con);
                PopulationType pt = null;
                for (int i = 0; i < l.size(); i++) {
                    pt = l.get(i);
                    try {
                    	populationHandlers.put(pt.getId(), PopulationHandlerFactory.getInstance(pt.getId()));
                    } catch (Throwable t) {
                        log.error("Failed to load population handler for type: " + pt.getId());
                    }
                }
            } catch (Throwable t) {
                throw new PopulationHandlersException("Error loading population handlers.", t);
            }
        }
    }

	/**
	 * Load population handlers
	 * @throws PopulationHandlersException
	 */
    public static void loadPopulatioHandlers() throws PopulationHandlersException {
        if (null == populationHandlers) {
            Connection conn = null;
            try {
                conn = getConnection();
                loadPopulatioHandlers(conn);
            } catch (PopulationHandlersException phx) {
            	throw(phx);
			} catch (Throwable t) {
                throw new PopulationHandlersException("Error loading population handlers.", t);
            } finally {
                closeConnection(conn);
            }
        } else {
            log.warn("Population handlers already loaded.");
        }
    }

    /**
     * Handle deposit action
     * @param userId userid that performed the action
     * @param successful deposit status
     * @param writerId writer that performed the action
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void deposit(long userId, boolean successful, long writerId, Transaction tran) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		deposit(con, userId, successful, writerId, tran);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     *
     * @param con
     * @param user
     * @param successful
     * @param writerId
     * @param tran
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void deposit(Connection con, long userId, boolean successful, long writerId, Transaction tran) throws SQLException, PopulationHandlersException {
    	UserBase user = UsersDAOBase.getUserById(con, userId);
    	deposit(con, user, successful, writerId, tran);
    }

    /**
     * Handle deposit action
     * @param con db connection
     * @param userId
     * @param successful
     * @param writerId
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void deposit(Connection con, UserBase user, boolean successful, long writerId, Transaction tran) throws SQLException, PopulationHandlersException {

    	// If tran failed, make sure we have time settled of the transaction
    	if (!successful){
    		if (null == tran){
    			tran = new Transaction();
    		}
    		if (tran.getTimeSettled() == null){
    			tran.setTimeSettled(new Date());
    			log.error("No settle time for a failed tran for user " + user.getId());
    		}
    	}

    	UsersDAOBase.setUserLastDecline(con,user.getId(), !successful, tran);
    	
    	if (!successful) {
    		UsersDAOBase.setUserFirstDeclineTran(con,user.getId(), tran);
    	}
    	else {
    		UsersDAOBase.updateFirstDeclineAmount(con,user.getId());
    	}

		PopulationEntryBase p = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, user.getId());


		if(null == p) {
			if (!successful){
				insertIntoDeclinePop(con, user, null, false, writerId, tran.getDescription());
			} else {
				PopulationsManagerBase.insertIntoPopulation(user.getContactId(), 0, user.getId(), user.getUserName(), 0, PopulationsManagerBase.POP_TYPE_RETENTION, p);
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
			}
		}else{
			if (p.getCurrPopualtionTypeId() != 0){
				PopulationHandlerBase ph = populationHandlers.get(p.getCurrPopualtionTypeId());
	   			if(null != ph) {
	   				try {
	   					ph.deposit(con, p, successful, writerId, tran, user);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing deposit event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + p.getCurrPopualtionTypeId());
	    		}
			}else{
				if (p.getEntryTypeId() != ENTRY_TYPE_REMOVE_PERMANENTLY) {
					if (!successful){
						insertIntoDeclinePop(con, user, p, false, writerId, tran.getDescription());
					} else {
						PopulationsManagerBase.insertIntoPopulation(user.getContactId(), 0, user.getId(), user.getUserName(), 0, PopulationsManagerBase.POP_TYPE_RETENTION, p);
						UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
						UsersManagerBase.updateUserStatusIfNeeded(	user.getId(), user.getStatusId(), tran.getAmount() + user.getBalance(),
																	tran.getId(), null, null, null);
					}
				}
			}
		}
    }

	/**
	 * Handle invest action
	 * 
	 * @param userId userid that performed the action
	 * @param successful invest status
	 * @param writerId writer that performed the action
	 * @param opportunityTypeId
	 * @throws SQLException
	 * @throws PopulationHandlersException
	 */
	public static void invest(long userId, boolean successful, long writerId, Long balanceAfterDeposit, Long depositId,
								Long investmentAmount, Long investmentId, Long opportunityTypeId) throws SQLException,
																									PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		PopulationEntryBase p = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
    		if(null != p && p.getCurrPopualtionTypeId() != 0) {
	   			PopulationHandlerBase ph = populationHandlers.get(p.getCurrPopualtionTypeId());
	   			if(null != ph) {
	   				try {
	   					ph.invest(p, successful, writerId);
	   					UsersManagerBase.updateUserDepNoInv24H(userId);
	   					UserBase user = UsersManagerBase.getUserById(userId);
						UsersManagerBase.updateUserStatusIfNeeded(userId, user.getStatusId(), balanceAfterDeposit, depositId, investmentAmount, investmentId, opportunityTypeId);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing invest event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + p.getCurrPopualtionTypeId());
	    		}
    		} else {
    			log.info("invest event: population not found for userId: " + userId);
    		}
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Handle closeAccount action
     * @param userId userid that his account closed
     * @param writerId writer that performed the action
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void closeAccount(long userId, long writerId) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		PopulationEntryBase p = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
    		if(null != p && p.getCurrPopualtionTypeId() != 0) {
	   			PopulationHandlerBase ph = populationHandlers.get(p.getCurrPopualtionTypeId());
	   			if(null != ph) {
	   				try {
	   					ph.close(p, writerId);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing closeAccount event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + p.getCurrPopualtionTypeId());
	    		}
    		} else {
    			log.info("closeAccount event: population not found for userId: " + userId);
    		}
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Handle fraud action
     * @param userId userid that defined as frauder
     * @param writerId writer that performed the action
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void fraud(long userId, long writerId) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		PopulationEntryBase p = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
    		if(null != p && p.getCurrPopualtionTypeId() != 0) {
	   			PopulationHandlerBase ph = populationHandlers.get(p.getCurrPopualtionTypeId());
	   			if(null != ph) {
	   				try {
	   					ph.fraud(p, writerId);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing fraud event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + p.getCurrPopualtionTypeId());
	    		}
    		} else {
    			log.info("fraud event: population not found for userId: " + userId);
    		}
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Handle lock action.
     * @param populationEntryId the population entry for lock
     * @param writerId writer that performed the action
     * @return true in case the lock succeed
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static boolean lock(PopulationEntryBase populationEntry, long writerId) throws SQLException, PopulationHandlersException {
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm;
		Connection con = null;
		long populationUserId = populationEntry.getPopulationUserId();

   		if (populationUserId > 0 ){
   			try {
   				con = getConnection();
   				long populationTypeId = populationEntry.getOldPopulationTypeId();
   				PopulationHandlerBase ph = populationHandlers.get(populationTypeId);
	   			if(null != ph) {
	   				try {
	   					ph.lock(con,populationEntry, writerId);
	   				} catch (Throwable t) {
	   					fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.lock.failed", null),null);
	   					context.addMessage(null, fm);
	   	                throw new PopulationHandlersException("Problem processing lock event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + populationTypeId);
	    		}
   			} finally {
   				closeConnection(con);
   			}
    	} else {
    		fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("retention.entry.not.qualified", null),null);
			context.addMessage(null, fm);
    		log.log(Level.DEBUG, "the given populationUserId: " + populationUserId + " not found ");
    		return false;
    	}
   		return true;
    }

    /**
     * Handle unlock action.
     * @param populationEntryId the population entry for unlock
     * @param writerId writer that performed the action
     * @return true in case the lock succeed
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static boolean unLock(Connection con, PopulationEntryBase populationEntry, long writerId) throws SQLException, PopulationHandlersException {
    	long populationUserId = populationEntry.getPopulationUserId();
   		if (populationUserId > 0){
			if (populationEntry.isEntryLocked()) {
   				long populationTypeId = populationEntry.getOldPopulationTypeId();
				PopulationHandlerBase ph = populationHandlers.get(populationTypeId);
	   			if(null != ph) {
	   				try {
	   					return ph.unLock(con, populationEntry, writerId);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing unlock event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + populationTypeId);
	    		}
	    	} else {
	    		log.log(Level.DEBUG, "the given populationUserId: " + populationUserId + " not locked! ");
	    	}
   		}
   		return false;
    }

    public static boolean unLock(PopulationEntryBase populationEntry, long writerId) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return unLock(con, populationEntry, writerId);
    	} finally {
			closeConnection(con);
		}
    }

    /**
     * Handle insert new action to issue
     * @param issueId the issue id of the new action
     * @param populationEntryId population entrty of the issue
     * @param writerId writer that performet the action
     * @throws SQLException
     * @throws PopulationHandlersException
     *
     * @return isRemoved - true if entry remove from population after this action and false otherwise.
     */
    public static boolean actionIssue(IssueAction action, PopulationEntryBase popUserEntry) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	boolean isRemoved = false;
    	long currentEntryId = popUserEntry.getCurrEntryId();

   		if (currentEntryId > 0) {
   			try {
   				con = getConnection();
   				long populationTypeId = popUserEntry.getCurrPopualtionTypeId();
   				PopulationHandlerBase ph = populationHandlers.get(populationTypeId);
	   			if(null != ph) {
	   				try {
	   					isRemoved = ph.issueAction(action, popUserEntry);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing actionIssue event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + populationTypeId);
	    		}
   			} finally {
   				closeConnection(con);
   			}
    	} else {
    		log.log(Level.DEBUG, "the given populationEntryId: " + currentEntryId + " not qualified! ");
    	}
   		return isRemoved;
    }

    /**
     * This method called when creating a new user
     * @param contactId contact id of the new user
     * @param userId id of the new user
     * @param writerId writer that performed the action
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void updateContactPopulation(long contactId, long userId, long writerId) throws SQLException, PopulationHandlersException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		PopulationEntryBase p = PopulationsDAOBase.getPopulationUserByContactId(con, contactId);

    		if(null != p) {
    			p.setUserId(userId);
    			log.debug("updateIssuesAndUserForContact, popUserId: " + p.getPopulationUserId());
    			try {
    				IssuesManagerBase.updateIssuesAndUserForContact(con,p);
    			} catch (SQLException e) {
    				throw new PopulationHandlersException("Error to update userId in issues" , e);
    			}

    			long populationTypeId = p.getCurrPopualtionTypeId();
   				PopulationHandlerBase ph = populationHandlers.get(populationTypeId);
	   			if(null != ph) {
	   				try {
	   					ph.userCreation(con, p, writerId );
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing insertUser event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + populationTypeId);
	    		}
    		} else {
    			log.info("insertUser event: population not found for contactId: " + contactId);
    		}
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * This method called upon reached call
     * @param contactId contact id of the new user
     * @param userId id of the new user
     * @param writerId writer that performed the action
     * @throws SQLException
     * @throws PopulationHandlersException
     */
    public static void reachedCall(Connection con,PopulationEntryBase p, long writerId, UserBase user) throws SQLException, PopulationHandlersException {

		if(null != p) {
			long currPopulationTypeId = p.getCurrPopualtionTypeId();

			if (currPopulationTypeId != 0){
				PopulationHandlerBase ph = populationHandlers.get(currPopulationTypeId);

	   			if(null != ph) {
	   				try {
	   					ph.reachedCall(con, p, writerId, user);
	   				} catch (Throwable t) {
	   	                throw new PopulationHandlersException("Problem processing insertUser event.", t);
	   	            }
	   			} else {
	    			throw new PopulationHandlersException("No populationHandler for typeId: " + currPopulationTypeId);
	    		}
			}
		} else {
			log.info("insertUser event: population not found for contactId: " + p.getContactId());
		}
    }

	/**
	 * Insert new entry to populationHistory
	 * @param popEntHi PopulationEntryHis instance
	 * @throws PopulationHandlersException
	 */
	public static void insertIntoPopulationHistory(PopulationEntryHis popEntHi) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			insertIntoPopulationHistory(con, popEntHi);
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing insert to populationEntriesHistory ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert new entry to populationHistory
	 * @param con db connection
	 * @param popEntHi PopulationEntryHis instance
	 * @throws PopulationHandlersException
	 */
	public static void insertIntoPopulationHistory(Connection con, PopulationEntryHis popEntHi) throws PopulationHandlersException {
		if (log.isEnabledFor(Level.DEBUG)) {
			log.log(Level.DEBUG, "Going to insert into populationEntriesHistory: " + popEntHi.toString());
		}
		try {
			PopulationsDAOBase.insertIntoPopulationEntriesHist(con, popEntHi);
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing insert to populationEntriesHistory ", t);
		}
	}

	/**
	 * Insert new record to population history
	 * @param entryId population entry id
	 * @param assignWriterId assign Writer Id
	 * @param writerId writer that performe the action
	 * @param statusId record status
	 * @param actionId
	 * @throws PopulationHandlersException
	 */
	public static void insertIntoPopulationEntriesHis(long entryId, long writerId, long statusId, long assignWriterId, long actionId) throws PopulationHandlersException {

		PopulationEntryHis popEnHis = new PopulationEntryHis();
    	popEnHis.setPopulationEntryId(entryId);
    	popEnHis.setWriterId(writerId);
    	popEnHis.setStatusId(statusId);
    	popEnHis.setAssignWriterId(assignWriterId);
    	popEnHis.setIssueActionId(actionId);
    	insertIntoPopulationHistory(popEnHis);
	}

	/**
	 * Insert new entry to populationEntries
	 * @param popEntry PopulationEntry instance
	 * @throws PopulationHandlersException
	 */
	public static void insertIntoPopulationEntries(Connection con, PopulationEntryBase popUserEntry, long writerId, long issueActionId, boolean isPopEntryDisplayed) throws SQLException {
		if (log.isEnabledFor(Level.DEBUG)) {
			log.log(Level.DEBUG, "Going to insert into populationEntries: " + popUserEntry.toString());
		}

		try {
			con.setAutoCommit(false);
			// if no user Id and not contact Id in population users table create a new population users
			if (popUserEntry.getPopulationUserId() == 0){
				popUserEntry.setEntryTypeId(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
				PopulationsDAOBase.insertIntoPopulationUsers(con, popUserEntry);
			}

			PopulationsDAOBase.insertIntoPopulationEntries(con, popUserEntry, isPopEntryDisplayed);

			// Update population users
			PopulationsDAOBase.updatePopulationUser(con, popUserEntry);

			PopulationEntryHis popEnHis = new PopulationEntryHis();
	    	popEnHis.setPopulationEntryId(popUserEntry.getCurrEntryId());
	    	popEnHis.setWriterId(writerId);
	    	popEnHis.setAssignWriterId(popUserEntry.getAssignWriterId());
	    	popEnHis.setStatusId(PopulationsManagerBase.POP_ENT_HIS_STATUS_NEW);

	    	// Check if the qualification of that population caused by a specific action id
	    	if (issueActionId != 0){
	    		popEnHis.setIssueActionId(issueActionId);
	    	}

			PopulationsDAOBase.insertIntoPopulationEntriesHist(con, popEnHis);
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw e;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
		}

	}



	/**
	 * Check if the population entry locked
	 * @param populationEntryId population entry id
	 * @return true if locked
	 * @throws PopulationHandlersException
	 */
	public static boolean isLocked(long populationUsersId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			return (PopulationsDAOBase.isLocked(con, populationUsersId, 0));
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Check if the population entry locked by specific writer.
	 * @param populationEntryId population entry id
	 * @param writerId the writer for input param
	 * @return true if locked
	 * @throws PopulationHandlersException
	 */
	public static boolean isLockedByWriter(long populationEntryId, long writerId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			return (PopulationsDAOBase.isLocked(con, populationEntryId, writerId));
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * get issuesActions number for remove.
	 * @param populationEntryId population entry id
	 * @return
	 * @throws PopulationHandlersException
	 */
	public static int getNumActionsForRemove(long populationEntryId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			return (PopulationsDAOBase.getNumActionsForRemove(con, populationEntryId));
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing numActions to remove! ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Check if the writer is already locking an entry
	 * @param writerId the writer for input param
	 * @return true if locking
	 * @throws PopulationHandlersException
	 */
	public static boolean isWriterHasLockedEntry(long writerId) throws PopulationHandlersException {
		Connection con = null;
		try {
			con = getConnection();
			return (PopulationsDAOBase.isWriterHasLockedEntry(con, writerId));
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get Population name by population entry
	 * @param currentEntryId
	 * @param writerId
	 * @throws SQLException
	 */
	public static String getNameByPopEntryID(long popEntryId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getPopulationNameByEntryId(con, popEntryId);
		} finally {
			closeConnection(con);
		}
	}


	public static PopulationEntryBase getPopulationEntryByEntryId(long populationEntryId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getPopulationEntryByEntryId(con, populationEntryId);
		} finally {
			closeConnection(con);
		}
	}

	public static PopulationEntryBase getPopulationUserById(long populationUserId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getPopulationUserById(con, populationUserId);
		} finally {
			closeConnection(con);
		}
	}


	public static void updatePopulationUser(PopulationEntryBase popUserEntry) throws Exception {
		Connection conn = null;
        try {
            conn = getConnection();
            PopulationsDAOBase.updatePopulationUser(conn, popUserEntry);
        } finally {
            closeConnection(conn);
        }
	}

	/**
	 * Update assign writer id
	 * @param entryId
	 * @param writerId
	 * @throws SQLException
	 */
	public static void updateAssignWriter(long entryId, long writerId, boolean isBulkAssign) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			PopulationsDAOBase.updateAssignWriter(con, entryId, writerId, isBulkAssign);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert new record to population history
	 * @param entryId population entry id
	 * @param assignWriterId assign Writer Id
	 * @param writerId writer that performe the action
	 * @param statusId record status
	 * @param actionId
	 * @throws PopulationHandlersException
	 */
	public static PopulationEntryHis insertIntoPopulationEntriesHis(Connection con, long entryId, long writerId,
			long statusId, long assignWriterId, long actionId) throws PopulationHandlersException {

		PopulationEntryHis popEnHis = new PopulationEntryHis();
    	popEnHis.setPopulationEntryId(entryId);
    	popEnHis.setWriterId(writerId);
    	popEnHis.setStatusId(statusId);
    	popEnHis.setAssignWriterId(assignWriterId);
    	popEnHis.setIssueActionId(actionId);
    	insertIntoPopulationHistory(con, popEnHis);
    	return popEnHis;
	}

	/**
	 * Returns population list
	 * @param userId TODO
	 * @return a list of Populations
	 * @throws SQLException
	 */
	public static ArrayList<Population> getPopulationList(Connection con, long populationType, long skinId, boolean isActive, String name, long userId) throws SQLException{
		ArrayList<Population> list = null;
		list = PopulationsDAOBase.getPopulationList(con, populationType, skinId, isActive, name, userId);
		return list;
	}

	/**
	 * Returns population list
	 * @param userId TODO
	 * @return a list of Populations
	 * @throws SQLException
	 */
	public static ArrayList<Population> getPopulationList(long populationType, long skinId, boolean isActive, String name, long userId) throws SQLException{
		Connection con = getConnection();
		ArrayList<Population> list = null;
		try {
			list = getPopulationList(con, populationType, skinId, isActive, name, userId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	public static PopulationEntryBase getPopulationUserByContactId(long contactId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getPopulationUserByContactId(con, contactId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert Into Population connection wrapper
	 * @param contactId
	 * @param skinId
	 * @param popTypeId
	 * @param popUser TODO
	 * @throws SQLException
	 */
	public static void insertIntoPopulation(long contactId, long skinId, long userId, String name, long writerId, int popTypeId, PopulationEntryBase popUser) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			insertIntoPopulation(con, popTypeId, contactId, skinId, userId, writerId, popUser);
		} catch (Exception exp) {
			log.error("Exception in updateContactMe event ", exp);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert Into Population
	 * @param popTypeId
	 * @param contactId
	 * @param skinId
	 * @param popUser
	 * @throws SQLException
	 */
	public static void insertIntoPopulation(Connection con, int popTypeId, long contactId, long skinId, long userId, long writerId, PopulationEntryBase popUser) throws SQLException {
		ArrayList<Population> populationsList =
			PopulationsManagerBase.getPopulationList(con, popTypeId, skinId, true ,null, userId);

		if (populationsList.size() > 0) {
			if (null == popUser){
				popUser = PopulationsManagerBase.getPopulationUserByUserAndContactId(con, userId, contactId);
			}

			if (null == popUser){
				popUser = new PopulationEntryBase();
				popUser.setSkinId(skinId);
				popUser.setUserId(userId);			
			}
			
			popUser.setEntryTypeId(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
			
			if (contactId > 0){
				popUser.setContactId(contactId);
			}

			popUser.setPopulationId(populationsList.get(0).getId());
			try {
				PopulationsManagerBase.insertIntoPopulationEntries(con, popUser, writerId,0,true);
			} catch (SQLException e) {
				log.error("Error, problem to insert populationEntry ", e);
			}
		} else {
			log.error("Error, no population for pop_type_id " + PopulationsManagerBase.POP_TYPE_CALLME +
							" and skin " + skinId);
		}
	}

	/**
	 * Update contactMe event
	 * @param popUser
	 * @param contactId
	 * @throws SQLException
	 */
	public static void updateContactMeEvent(PopulationEntryBase popUser, long contactId, long skinId, long userId, long writerId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();

			if (null != popUser) {   // allready in population (if this contact/user is in a population that is not call me remove it from current population)
				if (popUser.getCurrEntryId() != 0) {
					if (popUser.getCurrPopualtionTypeId() != PopulationsManagerBase.POP_TYPE_CALLME) {
						insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_CALLME, contactId, skinId, userId, writerId, popUser);
					} else {  // update
						try {
							con.setAutoCommit(false);
							PopulationEntryHis popHis = PopulationsManagerBase.insertIntoPopulationEntriesHis(con, popUser.getCurrEntryId(),
									writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_UPDATE,popUser.getAssignWriterId(), 0);
							popUser.setQualificationTime(popHis.getTimeCreated());
							PopulationsDAOBase.updateEntryQualificationTime(con, popUser);
							con.commit();
						} catch (Exception e) {
							log.error("Exception processing contactMe update action! ", e);
							try {
								con.rollback();
							} catch (Throwable it) {
								log.error("Can't rollback.", it);
							}
							throw e;
						} finally {
							try {
								con.setAutoCommit(true);
							} catch (Exception e) {
								log.error("Can't set back to autocommit.", e);
							}
						}
					}
				} else {
					//	insert to contactMePop
					insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_CALLME, contactId, skinId, userId, writerId, popUser);
				}

				try {
					// if we have user id != 0 and it is not in popUser - update it
					if (popUser.getUserId() == 0) {
						if (userId != 0) {
							popUser.setUserId(userId);
							PopulationsDAOBase.updateUserIdForContact(con, popUser);
						}
					}
				} catch (Exception exp) {
					log.error("Error, problem to update user " + userId + " in popUser with id " + popUser.getPopulationUserId());
				}
			} else {
				log.error("Error, problem to update populationEntryHis for contactId " + contactId);
			}
		} catch (Exception exp) {
			log.error("Exception in updateContactMe event ", exp);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * get the old population entry id for a specific population users
	 * @param populationUserId
	 * @return popEntryId
	 * @throws SQLException
	 */
	public static long getOldPopEntryId(long populationUserId)	throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
		    return PopulationsDAOBase.getOldPopEntryId(con, populationUserId);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateUserIdForContact(PopulationEntryBase popUser) throws SQLException	{
		Connection con = null;
		try {
			con = getConnection();
		    PopulationsDAOBase.updateUserIdForContact(con, popUser);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean fixPopualtionUserDetails(long userId, int changeDetailsType) {
		PopulationEntryBase popUser = null;
		Connection con = null;

		try {
			con = getConnection();

			// find relevant population user
			if (userId > 0){
				popUser = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con,userId);
			}

			if (null != popUser){
				long entryActionId = popUser.getEntryTypeActionId();

				// if pop user has an entry action id, check if it relats to wrong details
				if (0 != entryActionId){
					IssueAction action = IssuesDAOBase.getActionById(con,entryActionId);

					if (null != action){
						String issueActionTypeIdString = action.getActionTypeId();

						if (null != issueActionTypeIdString){
							int issueActionTypeId = Integer.parseInt(issueActionTypeIdString);
							boolean isUpdate = false;

							// if the reaction of this action was on of the following and the relevant details has been changed,
							// update pop user.
							if (IssuesManagerBase.ISSUE_ACTION_WRONG_NUMBER == issueActionTypeId &&
									ConstantsBase.USER_CHANGE_DETAILS_PHONE_NUMBER == changeDetailsType){
								isUpdate = true;
							}else if (IssuesManagerBase.ISSUE_ACTION_DIFFERENT_LANGUAGE == issueActionTypeId &&
									ConstantsBase.USER_CHANGE_DETAILS_SKIN == changeDetailsType){
								isUpdate = true;
							}

							if (isUpdate){
								// Get the population entry id of this action's issue.
								long oldPopEntryId = PopulationsDAOBase.getOldPopEntryId(con,popUser.getPopulationUserId());

								if (oldPopEntryId > 0){
									try {
										con.setAutoCommit(false);
										PopulationsManagerBase.insertIntoPopulationEntriesHis(con,oldPopEntryId, Writer.WRITER_ID_AUTO,
												POP_ENT_HIS_STATUS_BACK_TO_SALES_BY_CHANGE_DETAILS, 0, 0);
										Long oldPopTypeId = PopulationsDAOBase.getPopulationType(con, oldPopEntryId);
										
										insertIntoPopulation(con, oldPopTypeId.intValue(), 0, 0, popUser.getUserId(), Writer.WRITER_ID_AUTO, popUser);
										popUser.setEntryTypeId(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
										popUser.setEntryTypeActionId(0);
										PopulationsDAOBase.updatePopulationUser(con, popUser);
										con.commit();
										return true;
									} catch (SQLException e) {
										log.error("couldn't fixPopualtionUserDetails after details change - rollback ",e);
										try {
											con.rollback();
										} catch (Exception er) {
											log.error("Can't roll back! ", er);
										}
									} finally {
										con.setAutoCommit(true);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("couldn't fixPopualtionUserDetails after details change ",e);
		}finally {
			closeConnection(con);
		}
		return false;
	}

	/**
	 * Insert contacMe population
	 * @param issue TODO
	 * @param contactId
	 * @param skinId
	 * @return true if inserted false otherwise
	 * @throws SQLException
	 */
	public static boolean insertMarketingPopulation(Connection con, long userId, IssueAction issueAction, Issue issue) throws SQLException {
		UserBase user = UsersManagerBase.getUserById(userId);
		long skinId = user.getSkinId();
		Population marketingPop;
		boolean isDisplayed = false;
		boolean isRemoveFromPop = false;
		int userEntryTypeId = 0;

		if(!user.isActive() || !user.isContactByPhone() || user.isFalseAccount()){
			log.error("Error, user " + userId + " is false account or not active or disable phone contact ");
			return false;
		}

		// find user
		PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserAndContactId(con,userId, 0);

		// if it's a display marketing users activity or user got a call back insert him into displayed marketing population
		if (null != popUser){
			userEntryTypeId = popUser.getEntryTypeId();

			if (ConstantsBase.POP_ENTRY_TYPE_CALLBACK == userEntryTypeId){
				isDisplayed = true;
			}else if (ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES == userEntryTypeId){
				log.error("Error, user " + userId + " is removed from sales ");
				return false;
			}

		}

		// find relevant marketing population
		ArrayList<Population> populationsList =
			PopulationsDAOBase.getPopulationList(con, PopulationsManagerBase.POP_TYPE_MARKETING, skinId, true ,null, userId);

		if (populationsList.size() > 0) {
			marketingPop = populationsList.get(0);

			// if no user found in population users create a new one
			if (null == popUser){
				popUser = new PopulationEntryBase();
				popUser.setUserId(userId);
			}else {

				long currPopTypeId = popUser.getCurrPopualtionTypeId();

				// if user is already in a population, need to remove it in case he is in higher pop from last RECENT_QUALIFICATION_TIME
				if (popUser.getCurrEntryId() != 0){

					// check prior population
					GregorianCalendar cal = new GregorianCalendar();
					cal.add(GregorianCalendar.HOUR, -RECENT_QUALIFICATION_TIME_HOURS);

					// if user qualified to his population after the RECENT_QUALIFICATION_TIME check his pop priority
					if (popUser.getQualificationTime().after(cal.getTime())){
						Population currPop = null;

						try{
							currPop = PopulationsDAOBase.getPopulationList(con,currPopTypeId, skinId, true ,null, userId).get(0);
						}catch (Exception e) {
							log.error("Error, user " + userId + " popualtion wasn't found  ", e);
							return false;
						}

						// If found current pop check priority (max priority is 1)
						if (null != currPop){
							if (currPop.getPopulationTypePriority() <= marketingPop.getPopulationTypePriority()){
								log.error("Error, user " + userId + " is in marketing or higher population  ");
								return false;
							}
						}
					}
					isRemoveFromPop = true;
				}
			}

			try {
				con.setAutoCommit(false);
				// Insert Issue
				IssuesDAOBase.insertIssue(con, issue);
				// Set issue id in action.
				issueAction.setIssueId(issue.getId());
				// Insert Issue action
				IssuesManagerBase.validateAndInsertAction(con, issueAction, user , issue, 0);

				long writerId = issueAction.getWriterId();
				long issueActionId = issueAction.getActionId();

				if (isRemoveFromPop){
					// Remove user from it's current popualtion

					PopulationsManagerBase.insertIntoPopulationEntriesHis(con, popUser.getCurrEntryId(),
							writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_OTHER,popUser.getAssignWriterId(), issueActionId);

					// cancel assign only if General entry type id
					if (ConstantsBase.POP_ENTRY_TYPE_GENERAL == userEntryTypeId){
			    		popUser.setAssignWriterId(0);
					}

				}

				popUser.setPopulationId(marketingPop.getId());
				// update popUsers
				PopulationsManagerBase.insertIntoPopulationEntries(con, popUser, writerId,issueActionId, isDisplayed);

				con.commit();
				return true;
			} catch (Exception e) {
				log.error("Error, problem to insert user  " + userId + " into marketing population Entry ", e);
				try {
					con.rollback();
				} catch (Throwable it) {
					log.error("Can't rollback.", it);
				}
			} finally {
				try {
                    con.setAutoCommit(true);
                } catch (Exception e) {
                    log.error("Can't set back to autocommit.", e);
                }
    		}
		} else {
			log.error("Error, no population for pop_type_id " + PopulationsManagerBase.POP_TYPE_MARKETING + " with skin " + skinId);
		}
		return false;
	}

	public static PopulationEntryBase getPopulationUserByUserAndContactId(long userId, long contactId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return getPopulationUserByUserAndContactId(con, userId, contactId);
		}finally {
			closeConnection(con);
		}
	}

	public static PopulationEntryBase getPopulationUserByUserAndContactId(Connection con,long userId, long contactId) throws SQLException {
		PopulationEntryBase popUserEntry = null;

		try {
			if (userId != 0){
				// Find population user Id
				popUserEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
			}

			if (null == popUserEntry) {  // trying to search for a contact
				// if contact id is 0 and user id != 0 then search contact id from user
				if (0 == contactId && userId != 0){
					//UserBase user = UsersDAOBase.getUserById(con, userId);
					UserBase user = UsersDAOBase.getUserByUserIdService(con, userId);
					contactId = user.getContactId();
				}

				if (contactId != 0){
					popUserEntry = PopulationsDAOBase.getPopulationUserByContactId(con, contactId);
				}
			} else {
				// if found pop user by userId and contactId is different than new one write message to log
				if (popUserEntry.getContactId() != 0 && popUserEntry.getContactId() != contactId && 0 != contactId) {
					log.log(Level.ERROR, " input contact ID: " + contactId + " is different than contact id : " + popUserEntry.getContactId() +
							" for popualtion user id: " + popUserEntry.getPopulationUserId());
				}
			}
		}catch (Exception e) {
			log.log(Level.ERROR, "couldn't find pop user by user id: " + userId + " and contact id: " + contactId, e);
		}
		return popUserEntry;
	}

	/**
	 * Return pop user delay by type
	 * @param popUserId
	 * @param delayType
	 * @throws SQLException
	 */
	public static PopulationDelay getPopUsersDelayByDelayType(long popUserId, int delayType) {
		Connection con = null;
		PopulationDelay delay = null;

		try {
			con = getConnection();
			delay = PopulationsDAOBase.getPopulationDelayForPopUser(con,popUserId);
		}catch (Exception e) {
			log.error("couldn't find pop users delay for pop user id: " + popUserId, e);
		} finally {
			closeConnection(con);
		}

		if (null != delay){

			if (delay.getDelayType() == delayType){
				delay.setDelayCount(delay.getDelayCount() + 1);
			}else{
				// change delay
				delay.setDelayType(delayType);
				delay.setDelayCount(1);
			}
		}else{
			// insert a new delay
			delay = new PopulationDelay();
			delay.setPopUserId(popUserId);
			delay.setDelayType(delayType);
		}

		return delay;
	}

	/**
	 * Return pop user delay by type
	 * @param popUserId
	 * @param delayType
	 * @throws SQLException
	 */
	public static PopulationDelay getPopUsersDelayByDelayType(Connection con, long popUserId, int delayType) {
		PopulationDelay delay = null;

		try {
			delay = PopulationsDAOBase.getPopulationDelayForPopUser(con,popUserId);
		}catch (Exception e) {
			log.error("couldn't find pop users delay for pop user id: " + popUserId, e);
		}

		if (null != delay){

			if (delay.getDelayType() == delayType){
				delay.setDelayCount(delay.getDelayCount() + 1);
			}else{
				// change delay
				delay.setDelayType(delayType);
				delay.setDelayCount(1);
			}
		}else{
			// insert a new delay
			delay = new PopulationDelay();
			delay.setPopUserId(popUserId);
			delay.setDelayType(delayType);
		}

		return delay;
	}

	private static void removePopUserFromTracking(Connection con,PopulationEntryBase p, long writerId) throws  SQLException, PopulationHandlersException{
		long popEntryId = p.getCurrEntryId();

		if (0 == popEntryId){
			popEntryId = p.getOldPopEntryId();
		}

		try {
			con.setAutoCommit(false);

			p.setAssignWriterId(0);
			PopulationsManagerBase.insertIntoPopulationEntriesHis(con, popEntryId, writerId,
					PopulationsManagerBase.POP_ENT_HIS_STATUS_TRACKING_REMOVE_BY_DEPOSIT, p.getAssignWriterId(), 0);
			p.setEntryTypeActionId(0);
			p.setEntryTypeId(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
			PopulationsDAOBase.updatePopulationUser(con, p);

			con.commit();
		} catch (SQLException e) {
			log.error("ERROR, problem in removePopUserFromTracking popUSer : " + p.getPopulationUserId()+
							" popEntryId: " + p.getCurrEntryId(),e);
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw e;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
		}
	}

	public static void removePopUserLogin(Connection con,long userId , long writerId) throws  SQLException, PopulationHandlersException {

        // Retrieve PopEntryBase
		PopulationEntryBase p = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
		if (null != p) {
			if (p.getCurrPopualtionTypeId() ==  ConstantsBase.POPULATION_TYPE_ID_NO_2_DEPOSIT ||
					p.getCurrPopualtionTypeId() == ConstantsBase.POPULATION_TYPE_ID_NO_3_DEPOSIT ||
					p.getCurrPopualtionTypeId() == ConstantsBase.POPULATION_TYPE_ID_NO_4_DEPOSIT ||
					p.getCurrPopualtionTypeId() == ConstantsBase.POPULATION_TYPE_ID_NO_5_DEPOSIT ||
					p.getCurrPopualtionTypeId() == ConstantsBase.POPULATION_TYPE_ID_NO_6_DEPOSIT) {
				PopNoXDepositHandler ph = new PopNoXDepositHandler();
	   			try {
					ph.login(con, p, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0, false);
				} catch (Throwable t) {
				    throw new PopulationHandlersException("Problem processing deposit event.", t);
				}
			}
		}
	}



	public static void insertIntoDeclinePop(Connection con, UserBase user, PopulationEntryBase popUser, boolean isReplaceCurrPop, long writerId, String transactionDescription) throws PopulationHandlersException{
		long userId = user.getId();
		try{
			if (user.isContactForDaa()){
				int declinePopTypeId = POP_TYPE_DECLINE_LAST;

				if (TransactionsManagerBase.isFirstDeposit(con, userId, 0)){					
					declinePopTypeId = POP_TYPE_DECLINE_FIRST;
					if (!CommonUtil.isParameterEmptyOrNull(transactionDescription) && transactionDescription.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED)) {
						declinePopTypeId = POP_TYPE_FIRST_DEPOSIT_DECLINE_DOCS_REQUIRED;
					}
				}

				if (isReplaceCurrPop){
					replacePop(con, popUser, 0, 0, userId, writerId, declinePopTypeId);
				}else{
					insertIntoPopulation(con, declinePopTypeId, 0, 0, userId, writerId, popUser);
				}
			}
		}catch (SQLException e) {
			throw new PopulationHandlersException("SQLException in insertIntoDeclinePop", e);
		}
	}

	public static void replacePop(Connection con, PopulationEntryBase popUser,long contactId, long skinId, long userId, long writerId, int popTypeId) throws PopulationHandlersException{
		try {
			con.setAutoCommit(false);
			//	remove from popualtion
			PopulationsManagerBase.insertIntoPopulationEntriesHis(con, popUser.getCurrEntryId(),
					writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_OTHER, writerId, 0);

			// update popUsers
			popUser.setCurrEntryId(0);
	    	if (popUser.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_GENERAL){
	    		popUser.setAssignWriterId(0);
	    	}
	    	PopulationsDAOBase.updatePopulationUser(con, popUser);


	    	insertIntoPopulation(con, popTypeId, contactId, skinId, userId, writerId, popUser);
	    	con.commit();
		} catch (Exception e) {
			log.error("Exception processing contactMe other popualtion update action! ", e);
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw new PopulationHandlersException();
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
		}
	}
	public static PopulationEntryHis getLastPopulationEntryHistByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getLastPopulationEntryHistByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static String getRetentionRepNameByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getRetentionRepNameByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
		
	/**
	 * Check if the population entry locked
	 * @param populationEntryId population entry id
	 * @return true if locked
	 * @throws PopulationHandlersException
	 */
	public static boolean isLocked(Connection con, long populationUsersId) throws PopulationHandlersException {
		try {
			return (PopulationsDAOBase.isLocked(con, populationUsersId, 0));
		} catch (Throwable t) {
			throw new PopulationHandlersException("Problem processing isLocked! ", t);
		} 
	}
	
	/**
	 * Update assign writer id
	 * @param entryId
	 * @param writerId
	 * @throws SQLException
	 */
	public static void updateAssignWriter(Connection con, long entryId, long writerId) throws SQLException {
		try {

			PopulationsDAOBase.updateAssignWriter(con, entryId, writerId, false);
		} catch (Throwable t) {
			throw new SQLException("Problem processing updateAssignWriter! ", t);
		} 
			
	}
	
	/**
	 * update time when newbie join to control group
	 * @param conn
	 * @param popUserId
	 * @throws SQLException
	 */
	public static void updateTimeControlGroup(Connection conn, long popUserId) throws SQLException {
		PopulationsDAOBase.updateTimeControlGroup(conn, popUserId);
	}
	
	public static void cancelAssignAndMoveToUpgrade(Long userId, Long writerId) throws SQLException, PopulationHandlersException {
		log.debug("cancelAssignAndMoveToUpgrade started");
		Connection con = null;
		try {
			con = getConnection();
			log.debug("get populationEntry for userId " + userId);
			PopulationEntryBase populationEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(con, userId);
			long populationEntryId = populationEntry.getCurrEntryId();
			if (populationEntry.getCurrEntryId() == 0){
				populationEntryId = populationEntry.getOldPopEntryId();
			}
			long assignWriterId = populationEntry.getAssignWriterId();
			if (assignWriterId > PopulationsManagerBase.POP_PUBLIC_ASSIGN && !CommonUtil.isHasRetentionDepartmentId(assignWriterId, ConstantsBase.SALES_TYPE_DEPARTMENT_GENERAL_RETENTION)) {
				log.debug("cancelAssign populationEntryId = " + populationEntryId);
				cancelAssign(populationEntry, writerId, populationEntryId);
			}
			log.debug("cancelAssign populationEntryId = " + populationEntryId);
			PopulationsManagerBase.insertIntoPopulationEntriesHis(con, populationEntryId, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_MOVING_UPGRADE_TO_RETENTION, 0, 0);
			log.debug("cancelAssignAndMoveToUpgrade finished");
		} finally {
			closeConnection(con);
		}
	}
	
	
	public static void cancelAssign(PopulationEntryBase populationEntry, long writerId, long populationEntryId) throws SQLException, PopulationHandlersException {
		PopulationsManagerBase.insertIntoPopulationEntriesHis(populationEntryId, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_CANCEL_ASSIGNE, populationEntry.getAssignWriterId(), 0);

		UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, populationEntry.getUserId(), populationEntry.getUserName(), String.valueOf(populationEntry.getUserClassId()), UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(populationEntry.getAssignWriterId()), ConstantsBase.UNASSIGN_RETENTION_REP);
    	PopulationsManagerBase.updateAssignWriter(populationEntry.getPopulationUserId(), PopulationsManagerBase.POP_PUBLIC_ASSIGN, false);
	}
	
	public static ArrayList<PopulationEntryBase> getUsersToUnassignFromPortfolio(int noActivityMonths, int noIssueDays) throws SQLException{
		Connection con = getSecondConnection();
		ArrayList<PopulationEntryBase> list = null;
		try {
			list = PopulationsDAOBase.getUsersToUnassignFromPortfolio(con, noActivityMonths, noIssueDays);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	
	public static ArrayList<PopulationEntryBase> getPopulationEntryTmp(long min, long max) throws SQLException{
		Connection con = getSecondConnection();
		try {
			return PopulationsDAOBase.getPopulationEntryTmp(con, min, max);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get users with open withdraw's population which supposed to modify to retention's population
	 * @return ArrayList<Long>
	 * @throws SQLException
	 */
	public static ArrayList<Long> getSpecialOpenWithdrawUsers() throws SQLException {
		Connection connection = getSecondConnection();
		ArrayList<Long> list = null;
		try {
			list = PopulationsDAOBase.getSpecialOpenWithdrawUsers(connection);
		} finally {
			closeConnection(connection);
		}
		return list;
	}
	
}