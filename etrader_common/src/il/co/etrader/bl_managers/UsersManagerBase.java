package il.co.etrader.bl_managers;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.daos.UserBalanceStepDAO;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MailBoxUsersManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.copyop.common.dao.FrozenCopyopDao;
import com.copyop.common.dto.Frozen;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.FireServerPixelFields;
import il.co.etrader.bl_vos.FireServerPixelHelper;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.MarkVipHelper;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.TaxHistory;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.EmailAuthenticationDAOBase;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.MailBoxTemplatesDAOBase;
import il.co.etrader.dao_managers.MailBoxUsersDAOBase;
import il.co.etrader.dao_managers.MarketingAffiliatesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.jobs.AutomaticMailTemplateMaintenanceFeeBean;
import il.co.etrader.jobs.RestThreshBlockedUsersBean;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendTemplateEmail;

public abstract class UsersManagerBase extends com.anyoption.common.managers.UsersManagerBase {

	private static final Logger logger = Logger.getLogger(UsersManagerBase.class);

	protected static int TEMPLATE_CONFIRM_MAIL_ID = 2;
	public static int TEMPLATE_PASSWORD_REMINDER_MAIL_ID = 3;
	protected static int TEMPLATE_PASSWORD_REMINDER_MAIL_CAL_ID = 21;

	public static int TEMPLATE_DEPOSIT_DOCUMENT1 = 12;
	public static int TEMPLATE_DEPOSIT_DOCUMENT2 = 15;
	public static int TEMPLATE_DEPOSIT_DOCUMENT3 = 6;
	public static int TEMPLATE_DEPOSIT_DOCUMENT4 = 7;
	public static int TEMPLATE_DOCUMENT_REQUEST1_REGULATION = 56;
    public static int TEMPLATE_DOCUMENT_REQUEST2_REGULATION = 57;
    public static int TEMPLATE_DOCUMENT_REQUEST3_REGULATION = 58;
    public static int TEMPLATE_DOCUMENT_REQUEST4_REGULATION = 59;
    public static int TEMPLATE_5DEPOSITS_REGULATION = 62;
    public static int TEMPLATE_MAINTENANCE_FEE = 64;
    public static final int TEMPLATE_BEFORE_DEPOSIT = 67;
	public static final int TEMPLATE_AFTER_DEPOSIT = 66;
    
	public static final int USER_RANK_NEWBIES = 1;
	public static final int USER_RANK_REGULARS = 2;
	public static final int USER_RANK_BEGGINER_ROLLERS = 3;
	public static final int USER_RANK_MEDUIM_ROLLERS = 4;
	public static final int USER_RANK_HIGH_ROLLERS = 5;
	public static final int USER_RANK_GOLD_ROLLERS = 6;
	public static final int USER_RANK_PLATINUM_ROLLERS = 7;
	
	public static final int USER_VIP_STATUS_NOT_VIP = 1;
	public static final int USER_VIP_STATUS_VIP = 2;
	public static final int USER_VIP_STATUS_RESTRICTED = 3;


	public static void sendMailTemplateFile(long templateId,long writerId,UserBase u,
			boolean calcalistGame, String transactionPan, String transactionDate, String transactionAmonut, String transactionBankName,
			String resetLink, Long trxId, File []attachment, long attachmentId, String mailToSupport, String footer, String receiptNum,
			String treshold, String activationLink, String blankContent, String blankTemplateSubject, String minWithdraw) throws Exception {

		Template t = null;
    	Connection con = getConnection();

		try {
            t = TemplatesDAO.get(con,templateId);
            sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, transactionDate, transactionAmonut, transactionBankName, resetLink, null, trxId, attachment, attachmentId, null, mailToSupport, footer, receiptNum, false, treshold, activationLink, null, blankContent, blankTemplateSubject, minWithdraw);
		} finally {
            closeConnection(con);
        }
	}

	public static void sendMailTemplateFile(long templateId,long writerId,UserBase u,
											boolean calcalistGame, String transactionPan, String transactionAmonut, String transactionBankName,
											String resetLink, Long trxId, String authLink, String mailToSupport, boolean synchroneousSending,
											String treshold, String activationLink, String missingDocuments) throws Exception {
		Template t = null;
    	Connection con = getConnection();
    	String templatePath = "";
    	
    	try {
            t = TemplatesDAO.get(con,templateId);

	            if(t.getId() == ConstantsBase.TEMPLATE_WELCOME_GOOGLE_ID){
	            	String fileName = "anyoption's_binary_options_guide.pdf";
	            	if(u.getSkinId() == Skin.SKIN_ETRADER){
	            		fileName = "Binary_options_guide_ET.pdf";
	            	}
	            	templatePath = CommonUtil.getProperty("templates.path") + LanguagesDAOBase.getCodeById(con, u.getLanguageId()) + "_" + u.getSkinId() + "/" + fileName;
	            	File attachedFile = new File(templatePath);
	            	File []attachments={attachedFile};
	            	sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, null, transactionAmonut, transactionBankName, resetLink, null, trxId, attachments, 0, authLink, mailToSupport, null, null, synchroneousSending, null, null, null, null, null, null);
	            }else{
	            	if((t.getId() == ConstantsBase.TEMPLATE_AFTER_DEPOSIT)||(t.getId() == ConstantsBase.TEMPLATE_BEFORE_DEPOSIT)){
	            		String file10Commandents = "10-Commandments.pdf";
	            		String fileBineryOptionGuide = "anyoption's_binary_options_guide.pdf";
	            		String fileBineryOptionTrading = "Top_5_Binary_Options_Trading.pdf";
	                	
	            		String template10Commandents = CommonUtil.getProperty("templates.path") + LanguagesDAOBase.getCodeById(con, u.getLanguageId()) + "_" + u.getSkinId() + "/" + file10Commandents;
	            		String templateBineryOptionGuide = CommonUtil.getProperty("templates.path") + LanguagesDAOBase.getCodeById(con, u.getLanguageId()) + "_" + u.getSkinId() + "/" + fileBineryOptionGuide;
	            		String templateBineryOptionTrading = CommonUtil.getProperty("templates.path") + LanguagesDAOBase.getCodeById(con, u.getLanguageId()) + "_" + u.getSkinId() + "/" + fileBineryOptionTrading;
	            		File [] attachment = {new File(template10Commandents),new File(templateBineryOptionGuide), new File(templateBineryOptionTrading)};
	            		sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, null, transactionAmonut, transactionBankName, resetLink, null, trxId, attachment, 0, authLink, mailToSupport, null, null, synchroneousSending, null, null, null, null, null, null);
	               
	            	} else {
	            		if (t.getId() == ConstantsBase.TEMPLATE_WELCOME_MAIL_ID && (u.getSkinId() == Skin.SKIN_ENGLISH || u.getSkinId() == Skin.SKIN_REG_EN || u.getSkinId() == Skin.SKIN_SPAIN
	            				|| u.getSkinId() == Skin.SKIN_REG_ES || u.getSkinId() == Skin.SKIN_GERMAN || u.getSkinId() == Skin.SKIN_REG_DE || u.getSkinId() == Skin.SKIN_REG_IT 
	            				|| u.getSkinId() == Skin.SKIN_FRANCE || u.getSkinId() == Skin.SKIN_REG_FR || u.getSkinId() == Skin.SKIN_RUSSIAN || u.getSkinId() == Skin.SKIN_DUTCH_REG
	            				|| u.getSkinId() == Skin.SKIN_SWEDISH_REG || u.getSkinId() == Skin.SKIN_CZECH_REG || u.getSkinId() == Skin.SKIN_POLISH_REG || u.getSkinId() == Skin.SKIN_PORTUGAL_REG
	            				|| u.getSkinId() == Skin.SKIN_TURKISH || u.getSkinId() == Skin.SKIN_ENGLISH_NON_REG || u.getSkinId() == Skin.SKIN_SPAIN_NON_REG)) {
	            				t.setFileName("Welcome_Mail_Attachment.html");
	                        	sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, null, transactionAmonut, transactionBankName, resetLink, null, trxId, null, 0, authLink, mailToSupport, null, null, synchroneousSending, null, null, null, null, null, null);
	            			
	            		} else if(t.getId() == com.anyoption.common.util.ConstantsBase.TEMPLATE_REGULATION_PEP){
	            			String fileName = "PEP_Application.pdf";
	            			templatePath = CommonUtil.getProperty("templates.path") + LanguagesDAOBase.getCodeById(con, u.getLanguageId()) + "_" + u.getSkinId() + "/" + fileName;
	                    	File attachedFile = new File(templatePath);
	                    	File []attachments={attachedFile};
	                    	sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, null, transactionAmonut, transactionBankName, resetLink, null, trxId, attachments, 0, authLink, mailToSupport, null, null, synchroneousSending, null, null, null, null, null, null);	            		
	            		}else {
		                  	  sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, null, transactionAmonut, transactionBankName, resetLink, null, trxId, null, 0, authLink, mailToSupport, null, null, synchroneousSending, treshold, activationLink, missingDocuments, null, null, null);
		            		}
	            	}
	            }
    	
        } finally {
            closeConnection(con);
        }
	}

	public static void sendMailTemplateFile(long templateId,long writerId,UserBase u,
			boolean calcalistGame, String transactionPan, String transactionAmonut, String transactionBankName, String resetLink, Long trxId, String mailToSupport,
			String treshold, String activationLink, String missingDocuments) throws Exception {
		sendMailTemplateFile(templateId, writerId, u, calcalistGame, transactionPan, transactionAmonut, transactionBankName, resetLink, trxId, null, mailToSupport,
				false, treshold, activationLink, missingDocuments);
	}

	public static Locale getUserLocale(long id) throws SQLException {
		Connection con = getConnection();
		UserBase u = new UserBase();
		UsersDAOBase.getByUserId(con, id, u);
		long langId = u.getSkin().getDefaultLanguageId();
		String langCode = LanguagesDAOBase.getCodeById(con, langId);
		return new Locale(langCode);
	}

	public static void sendMailTemplateFile(Template t, long writerId, UserBase u,
			boolean calcalistGame, String transactionPan, String depositDate, String transactionAmonut, String transactionBankName, String resetLink,
			String[] emailsTo, Long trxId, File []attachemnt, String authLink, String mailToSupport, String footer, String receiptNum,
			String treshold, String activationLink, String blankContent, String blankTemplateSubject, String minWithdraw) throws Exception {

		sendMailTemplateFile(t, writerId, u, calcalistGame, transactionPan, depositDate, transactionAmonut, transactionBankName, resetLink,
				emailsTo, trxId, attachemnt, 0, authLink, mailToSupport, footer, receiptNum, false, treshold, activationLink, null, blankContent, blankTemplateSubject, minWithdraw);
	}

	public static void sendMailTemplateFile(Template t, long writerId, UserBase u,
					boolean calcalistGame, String transactionPan, String depositDate, String transactionAmonut, String transactionBankName, String resetLink,
					String[] emailsTo, Long trxId, File[] attachments, long attachmentId, String authLink, String mailToSupport,
					String footer, String receiptNum, boolean synchroneousSending, String treshold, String activationLink, String missingDocuments, String blankContent, String blankTemplateSubject, String minWithdraw) throws Exception {
		Writer w = null;
    	String langCode = "";
    	String subject = "";
    	String phone = "";
    	String supportPhone = "";
    	String supportFax = "";
		Connection con = getConnection();
		long langId;
		Locale locale = null;
		String bankTransferDetails = "";
		String firstName = "";
		

    	try {
    		
            w = WritersDAO.get(con, writerId);
            UsersDAOBase.getByUserName(con,u.getUserName(),u);

            // get language code for getting the correct template
    		langId = u.getSkin().getDefaultLanguageId();
    		langCode = LanguagesDAOBase.getCodeById(con, langId);
    		if( CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
    			langCode = ConstantsBase.ETRADER_LOCALE;
    		}
            locale = new Locale(langCode);
            if (ApplicationDataBase.isBackend() && u.getSkinId() == Skin.SKIN_CHINESE_VIP) {
                locale = new Locale(ConstantsBase.CHINESE_LOCALE_BACLEND);
            }
            logger.debug("langId: " + langId + " langCode: " + langCode + " locale: " + locale);

    		String[] args = new String[1];
    		args[0] = String.valueOf(u.getId());
    		// get subject email
//    		if (ApplicationDataBase.isWeb() && u.getSkinId() == Skin.SKIN_ETRADER) {
//    			subject = CommonUtil.getMessage(t.getSubject(), args);
//    		} else {
    		//this is needed because the jobs are running in Anyoption and we do not load IW bundle
    		if (u.getSkinId() == Skin.SKIN_ETRADER && t.getId() == ConstantsBase.TEMPLATE_ACTIVATION_MAIL_LOW_SCORE) {
    			
    			subject = "\u05d4\u05e0\u05d7\u05d9\u05d5\u05ea \u05dc\u05e4\u05e2\u05d5\u05dc\u05d4 \u2013 \u05de\u05e1\u05d7\u05e8 \u05d1\u05d0\u05ea\u05e8 \u05d0\u05d9\u05d8\u05e8\u05d9\u05d9\u05d3\u05e8";
    		} else if (u.getSkinId() == Skin.SKIN_ETRADER && t.getId() == ConstantsBase.TEMPLATE_ACTIVATION_MAIL_TRESHOLD) {
    			
    			subject = "\u05d4\u05d5\u05d3\u05e2\u05d4 \u05d7\u05e9\u05d5\u05d1\u05d4 \u2013 \u05d4\u05de\u05e9\u05da \u05e4\u05e2\u05d9\u05dc\u05d5\u05ea \u05d1\u05d0\u05d9\u05d8\u05e8\u05d9\u05d9\u05d3\u05e8";
    		} else {
    			subject = CommonUtil.getMessage(t.getSubject(), args, locale);
    		}
    		
			if (blankTemplateSubject != null && !blankTemplateSubject.equals("")) {
				subject = blankTemplateSubject;
			}
//    		}
    		// handle phone
    		if (CommonUtil.isHebrewSkin(u.getSkinId())) {
    			phone = u.getMobilePhonePrefix() + "-" + u.getMobilePhoneSuffix();
    		} else {  // anyoption
    			String code = ApplicationDataBase.getCountryPhoneCodes(String.valueOf(u.getCountryId()));
    			phone = code + "-" + u.getMobilePhone();

        		// get support phone
        		supportPhone = ApplicationDataBase.getSupportPhoneByCountryId(String.valueOf(u.getCountryId()));
        		supportFax = ApplicationDataBase.getSupportFaxeByCountryId(String.valueOf(u.getCountryId()));
    		}

    		
	    		boolean isUser = false; // If false, is contact.
	    		if (u.getId() != 0) {
	    			isUser = true;
	    		}
	    		int currencyForTemplate;
	    		if (isUser == true) {	// if user, get user's currency and send related bank details 
	    			currencyForTemplate = u.getCurrencyId().intValue();
	    			if (ApplicationDataBase.getSkinById(u.getSkinId()).isRegulated()){
	    				if (u.getLocale() == null) {
	    					u.setLocale(locale);
	    				}
	    				if (currencyForTemplate == ConstantsBase.CURRENCY_EUR_ID) {
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxC", null, u.getLocale());
	    				} else if (currencyForTemplate == ConstantsBase.CURRENCY_USD_ID || currencyForTemplate == ConstantsBase.CURRENCY_GBP_ID) {
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxE", null, u.getLocale());
	    				} else if (currencyForTemplate == ConstantsBase.CURRENCY_ZAR_ID) {
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null, u.getLocale());
	    				} else if (currencyForTemplate == ConstantsBase.CURRENCY_CZK_ID) {
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null, u.getLocale());
    					} else if (currencyForTemplate == ConstantsBase.CURRENCY_PLN_ID) {
    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null, u.getLocale());
    					}
	    			} else {
	    				switch (currencyForTemplate) {
	    				case (int) ConstantsBase.CURRENCY_EUR_ID:
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxB", null, u.getLocale());
	    					break;
	    				case (int) ConstantsBase.CURRENCY_AUD_ID:
	    				case (int) ConstantsBase.CURRENCY_ZAR_ID:
	    				case (int) ConstantsBase.CURRENCY_CZK_ID:
	    					bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxF", null, u.getLocale());
    						break;
	    				case (int) ConstantsBase.CURRENCY_USD_ID:
	    				case (int) ConstantsBase.CURRENCY_GBP_ID:
	    				case (int) ConstantsBase.CURRENCY_TRY_ID:
	    				case (int) ConstantsBase.CURRENCY_CNY_ID:
	    				case (int) ConstantsBase.CURRENCY_RUB_ID:
	    				case (int) ConstantsBase.CURRENCY_KRW_ID:
	    				case (int) ConstantsBase.CURRENCY_SEK_ID:
	    						bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxA", null, u.getLocale());
	    						break;
	    				}
	    			}
	    		} else {
	    			long skin = u.getSkinId();
	    			if (skin == Skin.SKIN_CHINESE || skin == Skin.SKIN_KOREAN || skin == Skin.SKIN_RUSSIAN || skin == Skin.SKIN_TURKISH){		//sending all available bank options for ZH, KR, RU and TR skins
		    			bankTransferDetails = CommonUtil.getMessage("bank.transfer.details.boxD", null, u.getLocale());
	    			}	
	    		}	    		    		
        } finally {
            closeConnection(con);
        }
    	
		HashMap<String,String> params = new HashMap<String,String>();
		params.put(SendTemplateEmail.PARAM_USER_ID, String.valueOf(u.getId()));
		if (!CommonUtil.isParameterEmptyOrNull(u.getFirstName())){
    		firstName = u.getFirstName();
    	} else {
    		firstName = CommonUtil.getMessage("bank.transfer.details.customer", null, locale);
    	}
		params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, firstName);
		params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, u.getLastName());
		if (!calcalistGame) {
			params.put(SendTemplateEmail.PARAM_USER_NAME, u.getUserName());
		} else {
			params.put(SendTemplateEmail.PARAM_USER_NAME, u.getUserNameCalGame());
		}
		params.put(SendTemplateEmail.PARAM_PASSWORD, u.getPassword());
		params.put(SendTemplateEmail.PARAM_ACCOUNT_ID, String.valueOf(u.getId()));
		params.put(SendTemplateEmail.PARAM_USER_BIRTH_DAY, u.getBirthDay()+"/"+u.getBirthMonth()+"/"+u.getBirthYear());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS,  u.getStreet()+" "+u.getStreetNo()+" "+u.getCityName()+" "+u.getZipCode());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_STREET,  u.getStreet());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER,  u.getStreetNo());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_CITY,  u.getCityName());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE,  u.getZipCode());
		params.put(SendTemplateEmail.PARAM_WRITER_FIRST_NAME,  w.getFirstName());
		params.put(SendTemplateEmail.PARAM_WRITER_LAST_NAME,  w.getLastName());
		params.put(SendTemplateEmail.PARAM_WRITER_FULL_NAME,  w.getFirstName() + " " + w.getLastName());
		params.put(SendTemplateEmail.PARAM_USER_BALANCE,  u.getBalanceTxt());
		params.put(SendTemplateEmail.PARAM_USER_TAX_BALANCE,  u.getTaxBalanceTxt());
		params.put(SendTemplateEmail.PARAM_MOBILE_PHONE, phone);
		params.put(SendTemplateEmail.PARAM_SUPPORT_PHONE, supportPhone);
		params.put(SendTemplateEmail.PARAM_SUPPORT_FAX, supportFax);
		params.put(SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, String.valueOf(transactionAmonut));
		params.put(SendTemplateEmail.PARAM_TRANSACTION_PAN, transactionPan);
		params.put(SendTemplateEmail.PARAM_TRANSACTION_BANK_NAME, transactionBankName);
		Date date = new Date();
		params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
		params.put(SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(date));
		params.put(SendTemplateEmail.PARAM_BANK_TRANSFER_DETAILS, bankTransferDetails);
		
		if (activationLink != null){
			params.put(SendTemplateEmail.PARAM_ACTIVATION_LINK, activationLink);
		}
		if (treshold != null) {
			params.put(SendTemplateEmail.PARAM_LOSS_TRESHOLD, treshold);
		}
		
		if (missingDocuments != null) {
			params.put(SendTemplateEmail.PARAM_MISSING_DOCUMENTS, missingDocuments);
		}
		
		if (blankContent != null) {
			params.put(SendTemplateEmail.PARAM_BLANK_EMAIL, blankContent);
		}
		if (minWithdraw != null) {
			params.put(SendTemplateEmail.PARAM_MIN_WITHDRAW, minWithdraw);
		}
		
		// load gender param for Backend use (english / hebrew)
		if (CommonUtil.isWebWriterId(writerId) == false && null != u.getGender()) {
			String genderTxt = u.getGenderForTemplateEnHeUse();
			String lCode = ConstantsBase.LOCALE_DEFAULT;
			if (CommonUtil.isHebrewSkin(u.getSkinId())) {
				lCode = ConstantsBase.ETRADER_LOCALE;
			}
			params.put(SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE,CommonUtil.getMessage(genderTxt, null, new Locale(lCode)));
		}
		params.put(SendTemplateEmail.PARAM_RESET_PASSWORD_LINK, resetLink);
		params.put(SendTemplateEmail.PARAM_AUTHENTICATION_LINK, authLink);

		String writerNickNameFirst = w.getNickNameFirst();
		if (CommonUtil.isParameterEmptyOrNull(writerNickNameFirst)) {
			writerNickNameFirst = UsersManagerBase.getWriterNickName(u.getId(), true);
		}
		String writerNickNameLast = w.getNickNameLast();
		if (CommonUtil.isParameterEmptyOrNull(writerNickNameLast)) {
			writerNickNameLast = UsersManagerBase.getWriterNickName(u.getId(), false);
		}
		
		String isShowDepostiBullet = "";
		String depositType = "";
		if(t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_FTD || t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_FTD_12){
			isShowDepostiBullet = "display:none";
			depositType = TransactionsManagerBase.getFtdMailDocTxt(u);
			if(!CommonUtil.isParameterEmptyOrNull(depositType)){
				params.put(SendTemplateEmail.PARAM_DYNAMIC_POP, depositType);
				isShowDepostiBullet = "padding: 5px 10px;";
			}			
			params.put(SendTemplateEmail.PARAM_DYNAMIC_POP_SHOW, isShowDepostiBullet);
		}
		
		if (!CommonUtil.isParameterEmptyOrNull(writerNickNameFirst) && !CommonUtil.isParameterEmptyOrNull(writerNickNameLast)) {
			if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
				if (!CommonUtil.validateHebrewLettersOnly(writerNickNameFirst) && !CommonUtil.validateHebrewLettersOnly(writerNickNameLast)) {
					params.put(SendTemplateEmail.PARAM_NICK_NAME, writerNickNameFirst + " " + writerNickNameLast);
				} else {
					params.put(	SendTemplateEmail.PARAM_NICK_NAME,
								CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
										+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
				}
			} else {
				if (CommonUtil.validateHebrewLettersOnly(writerNickNameFirst) && CommonUtil.validateHebrewLettersOnly(writerNickNameLast)) {
					params.put(SendTemplateEmail.PARAM_NICK_NAME, writerNickNameFirst + " " + writerNickNameLast);
				} else {
					params.put(	SendTemplateEmail.PARAM_NICK_NAME,
								CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
										+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
				}
			}
		} else {
			// if(ap.getSkinId() == Skin.SKIN_ETRADER){
			// params.put(SendTemplateEmail.PARAM_NICK_NAME_FIRST, CommonUtil.getMessage("mailbox.sender.default.representative.first.name",
			// null));
			// }else{
			params.put(	SendTemplateEmail.PARAM_NICK_NAME,
						CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
								+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
			// }
		}
		
		 // for receipt after deposit template
        params.put(SendTemplateEmail.PARAM_AMOUNT, transactionAmonut);
        if (depositDate == null) {
        	params.put(SendTemplateEmail.PARAM_DATE, CommonUtil.getTimeAndDateFormat(date, u.getUtcOffset()));
        } else {
        	params.put(SendTemplateEmail.PARAM_DATE, depositDate);
        }
		params.put(SendTemplateEmail.PARAM_FOOTER, footer);
		params.put(SendTemplateEmail.PARAM_RECEIPT_NUM, receiptNum);

		if (!CommonUtil.isWebWriterId(writerId)) {
			if(t.getId() == ConstantsBase.TEMPLATE_FIRST_WITHDRAWAL ||
					t.getId() == ConstantsBase.TEMPLATE_JUST_CONFIRMATION ||
					t.getId() == ConstantsBase.TEMPLATE_JUST_CONFIRMATION_OTHER ||
					t.getId() == ConstantsBase.TEMPLATE_NON_CC_WITHDRAWAL) {

				if (TransactionsManagerBase.getLastXCCTransactionsCountByUser(u.getId(), 180) > 0) {
					params.put(SendTemplateEmail.PARAM_FOUR_CONDITION, CommonUtil.getMessage("templates.four.condition", null, new Locale(langCode)));
				} else {
					params.put(SendTemplateEmail.PARAM_FOUR_CONDITION, CommonUtil.getMessage("", null));
				}
			}
		}
		
		if (t.getId() == TEMPLATE_MAINTENANCE_FEE) {
		    AutomaticMailTemplateMaintenanceFeeBean mfu = (AutomaticMailTemplateMaintenanceFeeBean) u;
		    params.put(SendTemplateEmail.PARAM_MAINTENANCE_FEE_AMOUNT, CommonUtil.displayAmount(mfu.getFee(), mfu.getCurrencyId()));
		    params.put(SendTemplateEmail.PARAM_MAINTENANCE_FEE_TIME, mfu.getFeeCollectTime());
		}

		// send email for tracking if needed
		if (null != emailsTo && emailsTo.length > 0) {
			for (int i = 0; i < emailsTo.length; i++) {
				params.put(SendTemplateEmail.PARAM_EMAIL, emailsTo[i]);
				if (!synchroneousSending) {
					
				    new SendTemplateEmail(params, subject, t.getFileName(), langCode, u.getSkinId(), attachments, 
				    		u.getWriterId(), u.getPlatformId()).start();
				} else {
				    // intentionally execute the run method in the current thread
                    new SendTemplateEmail(params, subject, t.getFileName(), langCode, u.getSkinId(), attachments, 
                    		u.getWriterId(), u.getPlatformId()).run();
				}
			}
		}

		if (mailToSupport == null) {
			params.put(SendTemplateEmail.PARAM_EMAIL, u.getEmail());
		} else {
			params.put(SendTemplateEmail.PARAM_EMAIL, mailToSupport);
		}
		
		if (!synchroneousSending) {
		    new SendTemplateEmail(params, subject, t.getFileName(), langCode, u.getSkinId(), attachments, 
		    		u.getWriterId(), u.getPlatformId()).start();
		} else {
            // intentionally execute the run method in the current thread
            new SendTemplateEmail(params, subject, t.getFileName(), langCode, u.getSkinId(), attachments, 
            		u.getWriterId(), u.getPlatformId()).run();
		}
        
		if (mailToSupport == null) {
			if ((attachmentId != 0 && null != transactionPan)
					|| t.getId() == ConstantsBase.TEMPLATE_5DEPOSITS
					|| t.getId() == UsersManagerBase.TEMPLATE_5DEPOSITS_REGULATION) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(SendTemplateEmail.PARAM_TRANSACTION_PAN,
						transactionPan);
				if(depositDate != null && depositDate.length() > 0 ) {
					parameters.put(SendTemplateEmail.PARAM_DATE, depositDate);
				}
				
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId,
						parameters);
			} else if (t.getId() == UsersManagerBase.TEMPLATE_MAINTENANCE_FEE) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				AutomaticMailTemplateMaintenanceFeeBean mfu = (AutomaticMailTemplateMaintenanceFeeBean) u;
				parameters.put(
						SendTemplateEmail.PARAM_MAINTENANCE_FEE_AMOUNT,
						CommonUtil.displayAmount(mfu.getFee(),
								mfu.getCurrencyId()));
				parameters.put(SendTemplateEmail.PARAM_MAINTENANCE_FEE_TIME,
						mfu.getFeeCollectTime());
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId,
						parameters);
			} else if (t.getId() == ConstantsBase.TEMPLATE_ACTIVATION_MAIL_RESTRICTED && null != activationLink) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(SendTemplateEmail.PARAM_ACTIVATION_LINK,
						activationLink);
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId,
						parameters);
			} else if ((t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_SCENARIO || t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_SCENARIO_TIME_PASSED 
					 || t.getId() == ConstantsBase.TEMPLATE_DOCS_UPDATE) && null != missingDocuments) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(SendTemplateEmail.PARAM_MISSING_DOCUMENTS,
						missingDocuments);
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId,
						parameters);
			} else if (t.getId() == ConstantsBase.TEMPLATE_REGULATION_PEP) {
				attachmentId = MailBoxUsersManagerBase.insertAttachmentAsBLOBWithUserIdinName(attachments[0], u.getId());
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId);
			} else if (t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_FTD 
					|| t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_FTD || t.getId() == ConstantsBase.TEMPLATE_DOCUMENT_FTD_12){
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(SendTemplateEmail.PARAM_DYNAMIC_POP, depositType);
				parameters.put(SendTemplateEmail.PARAM_DYNAMIC_POP_SHOW, isShowDepostiBullet);
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId, parameters);
			} else if (t.getId() == ConstantsBase.TEMPLATE_BLANK){
				//HashMap<String, String> parameters = new HashMap<String, String>();
				//parameters.put(SendTemplateEmail.PARAM_BLANK_EMAIL, blankContent);
				//SendToMailBox(u, t, writerId, langId, trxId, attachmentId, parameters);
			}else{
				SendToMailBox(u, t, writerId, langId, trxId, attachmentId);
			}
		}
	}

	public static void sendMailTemplateFile(long templateId, long writerId, UserBase u, boolean calcalistGame, String resetLink, String mailToSupport, String treshold, String activationLink, String missingDocuments) throws Exception{

    	sendMailTemplateFile(templateId, writerId, u, calcalistGame, null, null, null, resetLink, null, mailToSupport, treshold, activationLink, missingDocuments);
	}

	public static void sendMailTemplateFile(long templateId, long writerId, UserBase u, boolean calcalistGame, String mailToSupport, String treshold, String activationLink, String missingDocuments) throws Exception{

    	sendMailTemplateFile(templateId, writerId, u, calcalistGame, null, null, null, null, null, mailToSupport, treshold, activationLink, missingDocuments);
	}


	/**
	 *  Note: not in use (Loyalty development)
	 * @throws Exception
	 */
	public static void sendJobMailTemplateFile(Template template,long writerId,UserBase u,
					BonusUsers bonusUser, Connection con, Properties properties) throws Exception{

    	String langCode = "";
    	String subject = "";


    	// get subject email
        subject = "XXX";

		HashMap<String,String> params = new HashMap<String,String>();
		params.put(SendTemplateEmail.PARAM_EMAIL, u.getEmail());
//		params.put(SendTemplateEmail.PARAM_EMAIL, "eliran@etrader.co.il");
		params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, u.getFirstName());
		params.put(SendTemplateEmail.PARAM_BONUS_AMOUNT, CommonUtil.displayAmount(bonusUser.getBonusAmount(), false, null));

 		new SendTemplateEmail(params,subject,template.getFileName(), langCode, u.getSkinId(), null,
 				properties, u.getWriterId(), u.getPlatformId()).start();
	}

	/**
	 * @param templateId
	 * @param writerId
	 * @param userId TODO
	 * @param contactId TODO
	 * @throws Exception
	 */
	public static boolean sendPopUserMailTemplateFile(long templateId,long writerId,long userId, long contactId) throws Exception{

    	Template t = null;
		Connection con = getConnection();

    	try {
            t = TemplatesDAO.get(con,templateId);
            return sendPopUserMailTemplateFile(con, t, writerId, userId, contactId);

        } finally {
            closeConnection(con);
        }
	}

	/**
	 * @param templateId
	 * @param writerId
	 * @param userId TODO
	 * @param contactId TODO
	 * @throws Exception
	 */
	public static boolean sendPopUserMailTemplateFile(Connection con, Template t,long writerId,long userId, long contactId) throws Exception{

    	String langCode = "";
    	String subject = "";
    	String phone = "";
    	String mobilePhone = "";
    	String email = "";
    	String username = "";
    	String usersName = "";
    	String userLastName = "";
    	String supportPhone = "";
    	long skinId = 0;
    	long countryId = 0;
   		String gender = "M";
        long langId = 0;
        String userCreatedDate = "";
        String userCreatedTime = "";
        Date currentDate = new Date();
        int platformId = 0; 

        if (userId > 0){
        	UserBase u = new UserBase();
        	UsersDAOBase.getByUserId(con, userId, u,true);
        	platformId = u.getPlatformId();
        	skinId = u.getSkinId();
    		email = u.getEmail();
    		username = u.getUserName();
    		usersName = u.getFirstName();
    		userLastName = u.getLastName();
    		countryId = u.getCountryId();
    		gender = u.getGenderTxt();
    		mobilePhone = u.getMobilePhone();
    		currentDate = CommonUtil.getDateTimeFormaByTz(currentDate, u.getUtcOffset());
    		Date userTimeCreated = CommonUtil.getDateTimeFormaByTz(u.getTimeCreated(), u.getUtcOffset());

    		userCreatedDate = CommonUtil.getDateFormat(userTimeCreated);
    		userCreatedTime = CommonUtil.getTimeFormat(userTimeCreated);

        }else{
        	Contact c = ContactsManager.getContactByID(contactId);

        	skinId = c.getSkinId();
        	if (CommonUtil.isHebrewSkin(skinId)){
        		logger.info("Etrader contact "+c.getId()+" was remove from sales system");
        		return false;
        	}
        	email = c.getEmail();
        	if (!CommonUtil.isParameterEmptyOrNull(c.getFirstName())){
        		usersName = c.getFirstName();
        	}
        	if (!CommonUtil.isParameterEmptyOrNull(c.getLastName())){
        		userLastName = c.getLastName();
        	}
        	if (!CommonUtil.isParameterEmptyOrNull(c.getText())) {
        		username = c.getText();
        	} else if (!CommonUtil.isParameterEmptyOrNull(usersName)) {
        		username = usersName;
        	}
    		countryId = c.getCountryId();
        }

    	langId = SkinsManagerBase.getById(skinId).getDefaultLanguageId();
    	String countryIdStr = String.valueOf(countryId);

		// handle support phone for anyoption
		if (skinId != Skin.SKIN_ETRADER) {
//			String code = ApplicationDataBase.getCountryPhoneCodes(countryIdStr);

    		// get support phone
    		supportPhone = ApplicationDataBase.getSupportPhoneByCountryId(countryIdStr);
		}

        // get language code for getting the correct template
        logger.debug("Get language code, for getting the correct template.");

		langCode = LanguagesDAOBase.getCodeById(con, langId);
		if( CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
			langCode = ConstantsBase.ETRADER_LOCALE;
		}
		Locale locale = new Locale(langCode);
		// get subject email
		if (CommonUtil.isWebWriterId(writerId)) {
			subject = CommonUtil.getMessage(t.getSubject(), null);
		}
		else { // backend writer: need to take locale file by user skin and not writer skin
			subject = CommonUtil.getMessage(t.getSubject(), null,locale);
			if (userId == 0){
				if(skinId == Skin.SKIN_FRANCE) {
					subject += " ";
				} else {
					subject +=  ". ";
				}
				subject += CommonUtil.getMessage("contact.id.reference",null,locale) +": " + contactId;
			}
		}

		String code = ApplicationDataBase.getCountryPhoneCodes(String.valueOf(countryId));
		phone = code + "-" + mobilePhone;

		HashMap<String,String> params = new HashMap<String,String>();
		params.put(SendTemplateEmail.PARAM_EMAIL, email);
		params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, usersName);
		params.put(SendTemplateEmail.PARAM_USER_NAME, username);
		params.put(SendTemplateEmail.PARAM_MOBILE_PHONE, phone);
		params.put(SendTemplateEmail.PARAM_SUPPORT_PHONE, supportPhone);
		params.put(SendTemplateEmail.PARAM_ISSUE_ACTIONS_DATE, CommonUtil.getDateFormat(currentDate));
		params.put(SendTemplateEmail.PARAM_ISSUE_ACTIONS_TIME, CommonUtil.getTimeFormat(currentDate));
		params.put(SendTemplateEmail.PARAM_USER_TIME_CREATED_DATE, userCreatedDate);
		params.put(SendTemplateEmail.PARAM_USER_TIME_CREATED_TIME, userCreatedTime);
		params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, userLastName);

		Writer w = WritersManagerBase.getWriter(writerId);
		String writerNickNameFirst = w.getNickNameFirst();

		String writerNickNameLast = w.getNickNameLast();

		if (!CommonUtil.isParameterEmptyOrNull(writerNickNameFirst) && !CommonUtil.isParameterEmptyOrNull(writerNickNameLast)) {
			if (!CommonUtil.isHebrewSkin(skinId)) {
				if (!CommonUtil.validateHebrewLettersOnly(w.getNickNameFirst())
					&& !CommonUtil.validateHebrewLettersOnly(w.getNickNameLast())) {
					params.put(SendTemplateEmail.PARAM_NICK_NAME, writerNickNameFirst + " " + writerNickNameLast);
				} else {
					params.put(	SendTemplateEmail.PARAM_NICK_NAME,
								CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
										+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
				}
			} else {
				if (CommonUtil.validateHebrewLettersOnly(w.getNickNameFirst()) && CommonUtil.validateHebrewLettersOnly(w.getNickNameLast())) {
					params.put(SendTemplateEmail.PARAM_NICK_NAME, writerNickNameFirst + " " + writerNickNameLast);
				}
				params.put(	SendTemplateEmail.PARAM_NICK_NAME,
							CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
									+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
			}
		} else {
			params.put(	SendTemplateEmail.PARAM_NICK_NAME,
						CommonUtil.getMessage("mailbox.sender.default.representative.first.name", null, locale) + " "
								+ CommonUtil.getMessage("mailbox.sender.default.representative.last.name", null, locale));
		}

		if (CommonUtil.isHebrewSkin(skinId) &&
				(t.getFileName().equalsIgnoreCase(SendTemplateEmail.PARAM_TEMPLATE_NOT_REACHED_MADE_DEPOSIT)
						|| t.getFileName().equalsIgnoreCase(SendTemplateEmail.PARAM_TEMPLATE_NOT_REACHED_NEVER_DEPOSIT))) {
			if (CommonUtil.getMessage(gender, null).equals("Male")) {
				params.put("gender1", CommonUtil.getMessage("gender1.male", null,locale));
				params.put("gender2", CommonUtil.getMessage("gender2.male", null,locale));
				params.put("gender3", CommonUtil.getMessage("gender3.male", null,locale));
				params.put("gender4", CommonUtil.getMessage("gender4.male", null,locale));
				params.put("gender5", CommonUtil.getMessage("gender5.male", null,locale));
				params.put("gender6", CommonUtil.getMessage("gender6.male", null,locale));
			} else {
				params.put("gender1", CommonUtil.getMessage("gender1.female", null,locale));
				params.put("gender2", CommonUtil.getMessage("gender2.female", null,locale));
				params.put("gender3", CommonUtil.getMessage("gender3.female", null,locale));
				params.put("gender4", CommonUtil.getMessage("gender4.female", null,locale));
				params.put("gender5", CommonUtil.getMessage("gender5.female", null,locale));
				params.put("gender6", CommonUtil.getMessage("gender6.female", null,locale));
			}
		}

		if (!CommonUtil.isParameterEmptyOrNull(email)){
			new SendTemplateEmail(params,subject,t.getFileName(), langCode, skinId, null, 
					writerId, platformId).start();
			return true;
		}else{
			 logger.debug("No mail address for sending wrong number mail");
		}

		// mailBox
		if (userId > 0) {
		   UserBase u = new UserBase();
		   u.setId(userId);
		   u.setSkinId(skinId);
		   SendToMailBox(u, t, writerId, langId, null, 0);
		 }

		return false;
	}


	/**
	 * Send email , for income tax form
	 * This email we send to the customer and also to the support
	 * @param templateId
	 * 		template id
	 * @param writerId
	 * 		writer id
	 * @param UserBase
	 * 		user instance
	 * @param th
	 * 		TaxHistory instance
	 * @throws Exception
	 */
	public static void sendMailTemplateFile(long templateId,long writerId,UserBase u, TaxHistory th, FacesContext context) throws Exception {

    	Template t = null;
    	String langCode = "";
    	String subject = "";
		Connection con = getConnection();

		try {
           	UsersDAOBase.getByUserName(con,u.getUserName(),u);
            t = TemplatesDAO.get(con,templateId);

            // get language code for getting the correct template
            logger.debug("Get language code, for getting the correct template.");
    		long langId = u.getSkin().getDefaultLanguageId();
    		langCode = LanguagesDAOBase.getCodeById(con, langId);
    		if( CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
    			langCode = ConstantsBase.ETRADER_LOCALE;
    		}

    		// get subject email
    		if (CommonUtil.isWebWriterId(writerId)) {
    			subject = CommonUtil.getMessage(t.getSubject(), null);
    		}
    		else { // backend writer: need to take locale file by user skin and not writer skin
    			Locale locale = new Locale(langCode);
    			subject = CommonUtil.getMessage(t.getSubject(), null,locale);
    		}

        } finally {
            closeConnection(con);
        }

		HashMap<String,String> params = new HashMap<String,String>();

		params.put(SendTemplateEmail.PARAM_EMAIL, u.getEmail());
		params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, u.getFirstName());
		params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, u.getLastName());
		params.put(SendTemplateEmail.PARAM_ACCOUNT_ID, String.valueOf(u.getId()));
		params.put(SendTemplateEmail.PARAM_ID_NUMBER, u.getIdNum());
		params.put(SendTemplateEmail.PARAM_TAX, CommonUtil.displayAmount(th.getTax(), ConstantsBase.CURRENCY_ILS_ID));
		params.put(SendTemplateEmail.PARAM_WIN, CommonUtil.displayAmount(th.getWin(), ConstantsBase.CURRENCY_ILS_ID));
		params.put(SendTemplateEmail.PARAM_LOSE, CommonUtil.displayAmount(th.getLose(), ConstantsBase.CURRENCY_ILS_ID));
		params.put(SendTemplateEmail.PARAM_SUM_INVESTMENTS, CommonUtil.displayAmount(th.getSumInvest(), ConstantsBase.CURRENCY_ILS_ID));		
		params.put(SendTemplateEmail.PARAM_MISSING_PRECENT, th.getPercent());
		params.put(SendTemplateEmail.PARAM_MISSING_YEAR, th.getYear() + "");
 		
		new SendTemplateEmail(params,subject,t.getFileName(), langCode, u.getSkinId(), null, 
 				u.getWriterId(), u.getPlatformId()).start();

 		ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
 		if (ap.getIsLive()) {
	 		// send to support
	 		params.put(SendTemplateEmail.PARAM_EMAIL, CommonUtil.getProperty("email.from"));
	 		new SendTemplateEmail(params,subject,t.getFileName(), langCode, u.getSkinId(),
	 				null, u.getWriterId(), u.getPlatformId()).start();
 		}
	}

    /**
     * Get limit by id
     * @param id
     * 		limit id
     * @return
     * 		Limit instance
     * @throws SQLException
     */
    public static Limit getLimitById(long id) throws SQLException {

    	Limit l = new Limit();
    	Connection con = getConnection();

        try {
        	l = LimitsDAO.getById(con, id);
        } finally {
            closeConnection(con);
        }

        return l;
    }

    /**
     * Insert new user.
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static void insert(Connection con, UserBase user, long contactId) throws SQLException {
    	if (0 == contactId) {  // contact cookie not found
    		Contact contact = ContactsManager.searchFreeContactByUserPhone(con, user);
    		if (null != contact) {
    			contactId = contact.getId();
    			if (null == user.getSpecialCode()) {
    				user.setCombinationId(contact.getCombId());
    				user.setDynamicParam(contact.getDynamicParameter());
    				if (CommonUtil.isWebWriterId(user.getWriterId())) {
	    				long affId = -1;
	    				if (null != user.getDynamicParam()) {
	    					affId = ApplicationDataBase.getAffIdFromDynamicParameter(user.getDynamicParam());
	    				}
	    				FacesContext context = FacesContext.getCurrentInstance();
	    				ApplicationDataBase gm = (ApplicationDataBase) context.getApplication()
	    						.createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(
	    								context);
	    				if ((gm.isNetreferCombination(user.getCombinationId()) || gm.isReferpartnerCombination(user.getCombinationId())) && affId > 0) {
	    					logger.log(Level.DEBUG, "affKey = " + affId);
	    					user.setAffiliateKey(affId);
	    				} else {
	    					user.setAffiliateKey(0);
	    				}
	    				HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
	    				Long subAff = getSubbAffIdParam(req);
	    				if ( null != subAff ) {
	    					logger.log(Level.DEBUG, "sub affiliate id param, subAff = " + subAff);
	    					user.setSubAffiliateKey(subAff);
	    				} else {
	    					logger.log(Level.DEBUG, "sub affiliate id param not found");
	    				}
    				}
    			}
        	}
    	}
    	user.setContactId(contactId);
    	UsersDAOBase.insert(con, user);

    	/*
		 * Make sure user is not inserted in DB if not marked as regulated.
		 * 
		 * The skin id in the session/cookie may not be the same as the
		 * user's, so we should check the user skin.
		 */
    	// Regulation check
    	Skin userSkin = ApplicationDataBase.getSkinById(user.getSkinId());
		if (userSkin.isRegulated() || user.getSkinId() == Skin.SKIN_ETRADER) {
			// insert users
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			ur.setWriterId(user.getWriterId());
			ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_USER_REGISTERED);
			ur.setSuspendedReasonId(UserRegulationBase.NOT_SUSPENDED);
			UsersRegulationDAOBase.insertUserRegulation(con, ur);
		}
		
		//Add copyop frozen when user is created
		try { 
			Frozen fr = new Frozen(user.getId(), true, Frozen.FROZEN_REASON_FRESH, user.getWriterId());
			FrozenCopyopDao.insert(con, fr);
		} catch (Exception e) {
			logger.error("Can't insert frozen user", e);
		}
		
    	con.commit();

    	try{
    		logger.info("Trying to insert auto bonus upon registration for user " + user.getId());
	    	// Insert Automatic Bonus.
	    	long autoBonusId = BonusDAOBase.getAutoBonusId(con, user.getId());

	    	if (autoBonusId > 0){
	    		BonusUsers bonusUser = new BonusUsers();
	    		bonusUser.setUserId(user.getId());
	    		bonusUser.setWriterId(user.getWriterId());
	    		bonusUser.setBonusId(autoBonusId);

	    		BonusManagerBase.insertBonusUser(bonusUser, user, user.getWriterId(), 0, 0, false);
	    	}
    	}catch (Exception e) {
			logger.error("Failed to insert auto bonus upon registration for user " + user.getId(),e);
		}
    }

    public static long getUserIdByPhone(String phone)  throws SQLException{
    	Connection con = getConnection();
    	long userId = 0;

        try {
        	userId = UsersDAOBase.getUserIdByPhone(con, phone);
        } finally {
            closeConnection(con);
        }

        return userId;
    }

    public static long getByUserId(long id,UserBase user)  throws SQLException{

    	Connection con = getConnection();
    	long userId = 0;

        try {
        	UsersDAOBase.getByUserId(con, id,user);
        } finally {
            closeConnection(con);
        }

        return userId;
    }

    public static boolean getByUserId(Connection con, long id, UserBase user, boolean basic)  throws SQLException{
        return UsersDAOBase.getByUserId(con, id,user, basic);
    }

    public static UserBase getUserById(long userId) throws SQLException {
		Connection con = getConnection();
		UserBase user = null;

		try {
			user = UsersDAOBase.getUserById(con, userId);
		} finally {
			closeConnection(con);
		}
		return user;
    }

    /**
     * wrraper for calcalist game
     */
	public static void sendMailTemplateFile(long templateId, long writerId, UserBase u) throws Exception{
		sendMailTemplateFile(templateId, writerId, u, false, null, null, null, null);
	}
	
	public static void sendMailTemplateFile(long templateId,long writerId,UserBase u, String mailToSupport, String treshold, String activationLink, String missingDocuments) throws Exception{
		sendMailTemplateFile(templateId, writerId, u, false, mailToSupport, treshold, activationLink, missingDocuments);
	}
	
	public static void sendMailTemplateFile(long templateId, long writerId, UserBase u, String amount, String footer, String receiptNum, String treshold, String activationLink, String blankContent, String blankTemplateSubject, String minWithdraw) throws Exception{
    	
		sendMailTemplateFile(templateId, writerId, u, false, null, amount, null, null, null, null, null, 0, null, footer, receiptNum, treshold, activationLink, blankContent, blankTemplateSubject, minWithdraw);
	}

	public static boolean checkDetailsChange(long userId, String oldData, String newData, int changeDetailsType){
		// if details was changed, check popualtion user fix
		if (!CommonUtil.isParameterEmptyOrNull(newData)){
			// replace null Strings with empty (can occure when checking concatenation of phone numbers)
			newData = newData.replaceAll("null", "");

			if (null != oldData){
				oldData = oldData.replaceAll("null", "");
			}

			if(!newData.equals(oldData)){
				// if data has been change update popualtion user if relevant
				return PopulationsManagerBase.fixPopualtionUserDetails(userId, changeDetailsType);
			}
		}
		return false;
	}

	/**
	 * Get sub affiliate id paramter, search in cookies & session's attributes
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public static Long getSubbAffIdParam(HttpServletRequest request) throws SQLException {

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if ( null != cs ) {
			for ( int i = 0 ; i < cs.length ; i++ ) {
				if ( cs[i].getName().equals(ConstantsBase.SUB_AFF_KEY_PARAM)) {
					logger.info("found subAff cookie!");
					cookie = cs[i];
					//break;
				}
			}
		}

		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return new Long(cookie.getValue());
			} catch (Exception e) {
				if (logger.isEnabledFor(Level.WARN)) {
					logger.warn("subAff cookie with strange value!", e);
				}
			}
		}
		HttpSession session = request.getSession();
		Long subAff;
		try {
			subAff = (Long)session.getAttribute(ConstantsBase.SUB_AFF_KEY_PARAM);
		} catch (Exception e) {
			subAff = null;
		}
		if ( null != subAff ) {
			logger.info("subAff for session subAff: " + subAff);
			return subAff;
		}
		return null;
	}

	/**
	 * add email to MailBox user
	 * @param u user instance
	 * @param t template instance
	 * @param writerId
	 * @param langId
	 * @param trxId transaction id, if needed
	 * @throws SQLException
	 */
	public static void SendToMailBox(UserBase u, Template t, long writerId, long langId, Long trxId,
			HashMap<String, String> params, Connection conn, long attachmentId) throws SQLException {
 		// 	Send email to the user mailBox
// 		if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
 			if (t.getMailBoxEmailType() != 0) {
				MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(conn, t.getMailBoxEmailType() , u.getSkinId(), langId);
				if (null != template) {
					logger.debug("going to send email to user mailBox. templateId: " + template.getId() + " userId: " + u.getId());
					MailBoxUser email = new MailBoxUser();
					email.setTemplateId(template.getId());
					email.setUserId(u.getId());
					email.setWriterId(writerId);
					email.setSenderId(template.getSenderId());
					email.setIsHighPriority(template.getIsHighPriority());
					email.setPopupTypeId(template.getPopupTypeId());
					if (template.getSubject().indexOf("{" + SendTemplateEmail.PARAM_USER_ID + "}") > -1) {
						email.setSubject(template.getSubject().replace("{" + SendTemplateEmail.PARAM_USER_ID + "}", String.valueOf(u.getId())));
					} else {
						email.setSubject(template.getSubject());
					}
					if (attachmentId != 0) {
						email.setAttachmentId(attachmentId);
					}
					if (null != trxId) {
						email.setTransactionId(trxId);
					}
					if (params != null) {
						email.setParameters(params);
					}
					if(email.getUserId() != 0) {
						MailBoxUsersDAOBase.insert(conn, email);
					}
				}
 			}
//		}
	}

	public static void sendToMailBox(Connection conn, UserBase u, MailBoxTemplate template, long writerId, 
		HashMap<String, String> params, long attachmentId) throws SQLException {
		if (null != template) {
				logger.debug("going to send email to user mailBox. templateId: " + template.getId() + " userId: " + u.getId());
				MailBoxUser email = new MailBoxUser();
				email.setTemplateId(template.getId());
				email.setUserId(u.getId());
				email.setWriterId(writerId);
				email.setSenderId(template.getSenderId());
				email.setIsHighPriority(template.getIsHighPriority());
				email.setPopupTypeId(template.getPopupTypeId());
				if (template.getSubject().indexOf("{" + SendTemplateEmail.PARAM_USER_ID + "}") > -1) {
					email.setSubject(template.getSubject().replace("{" + SendTemplateEmail.PARAM_USER_ID + "}", String.valueOf(u.getId())));
				} else {
					email.setSubject(template.getSubject());
				}
				if (attachmentId != 0) {
					email.setAttachmentId(attachmentId);
				}
				if (params != null) {
					email.setParameters(params);
				}
				if(email.getUserId() != 0) {
					MailBoxUsersDAOBase.insert(conn, email);
				}
		} else {
			Log.error("empty template can't send user mailbox. userId: " + u.getId());
		}
	}

	public static void SendToMailBox(UserBase u, Template t, long writerId, long langId, Long trxId, Connection conn, long attachmentId) throws SQLException {
		SendToMailBox(u, t, writerId, langId, trxId, null, conn, attachmentId);
	}

	public static void SendToMailBox(UserBase u, Template t, long writerId, long langId, Long trxId, Connection conn, long attachmentId, HashMap<String, String> params) throws SQLException {
		SendToMailBox(u, t, writerId, langId, trxId, params, conn, attachmentId);
	}

	public static void SendToMailBox(UserBase u, Template t, long writerId, long langId, Long trxId, long attachmentId) throws SQLException {
		SendToMailBox(u, t, writerId, langId, trxId, attachmentId, null);
	}

	public static void SendToMailBox(UserBase u, Template t, long writerId, long langId, Long trxId, long attachmentId, HashMap<String, String> params) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			SendToMailBox(u, t, writerId, langId, trxId, conn, attachmentId, params);
		} finally {
            closeConnection(conn);
        }
	}

	public static ArrayList<CreditCard> getDepositsPerCreditCard(Date from, long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return CreditCardsDAO.getDepositsPerCreditCard(conn, from, userId);
		} finally {
            closeConnection(conn);
        }
	}
	
	/**
	 * generate random key for reset password / authentication emails
	 * @return
	 */
    public static String generateRandomKey() {
    	char[] charsArray = {'q', 'w', 'e', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k',
        		'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    	String resKey1 = "";
    	String resKey2 = "";
    	for (int j=0; j<8; j++) {
    		resKey1 += charsArray[new Random().nextInt(charsArray.length)];
    		resKey2 += charsArray[new Random().nextInt(charsArray.length)];
    	}
    	return resKey1 + new Date().getTime() + resKey2;
    }

    public static void updateIsAuthrized(String userName, int isAuthorized, long authorizationMailId, Date timeAuthorized, boolean isFirstAuthorization) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		if (authorizationMailId != 0){
    			EmailAuthenticationDAOBase.updateAuthorizationTime(con,	authorizationMailId, timeAuthorized);
    		}

    		if (isFirstAuthorization){
    			UsersDAOBase.updateIsAuthrized(con, userName, isAuthorized, timeAuthorized);
    		}else{
           		UsersDAOBase.updateIsAuthrized(con, userName, isAuthorized, null);
    		}

    	} finally {
    		closeConnection(con);
    	}
    }

	/*public static boolean isEmailInUse(String email, long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.isEmailInUse(con, email, userId);
		} finally {
			closeConnection(con);
		}
	}*/

	public static void updateEmail(long userId, String email) throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAOBase.updateEmail(con, userId, email);
		} finally {
			closeConnection(con);
		}
	}

	public static UserBase getUserByUserIdService(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUserByUserIdService(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateUserDepNoInv24H(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		UsersDAOBase.updateUserDepNoInv24H(con, userId);
    	} finally {
    		closeConnection(con);
    	}
		
	}

	public static long calculateUserRankById(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersDAOBase.calculateUserRankById(con, userId);
    	} finally {
    		closeConnection(con);
    	}
		
	}

	public static void updateUserRankIfNeeded(long userId, long transactionId, long currUserRank) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
			int countRealDeposits = TransactionsManagerBase.countRealDeposit(userId);
			if(countRealDeposits == 1) {
				UsersDAOBase.updateUserRank(con, userId, UsersManagerBase.USER_RANK_NEWBIES);
				UsersDAOBase.insertIntoUserRankHist(con, userId, UsersManagerBase.USER_RANK_NEWBIES, transactionId);
			} else {
				boolean isNebies = UsersManagerBase.isNewbies(userId);
				if (!isNebies) {
					long userRank = UsersManagerBase.calculateUserRankById(userId);
					if (userRank != currUserRank) {
						UsersDAOBase.updateUserRank(con, userId, userRank);
						UsersDAOBase.insertIntoUserRankHist(con, userId, userRank, transactionId);
					}
				}
			}
    	} finally {
    		closeConnection(con);
    	}		
	}

	public static void updateUserStatusIfNeeded(long userId, long userCurrStatus, Long balanceAfterDeposit, Long depositId,
												Long investmentAmount, Long investmentId, Long opportunityTypeId) throws SQLException {
		Connection con = null;
		if (userCurrStatus != UsersManagerBase.USER_STATUS_ACTIVE) {
			try {
				con = getConnection();
				con.setAutoCommit(false);
				UsersDAOBase.updateUserStatus(con, userId, UsersManagerBase.USER_STATUS_ACTIVE);
				UsersDAOBase.insertIntoUserStatusHist(con, userId, UsersManagerBase.USER_STATUS_ACTIVE);
				StringBuilder msg = new StringBuilder("Updated user status, inserted into user status hist table");
				if (userCurrStatus == UsersManagerBase.USER_STATUS_COMA
						&& (opportunityTypeId == null || opportunityTypeId == Opportunity.TYPE_REGULAR || opportunityTypeId == Opportunity.TYPE_OPTION_PLUS)) {
					UserBalanceStepDAO.clearUserActiveData(con, userId, balanceAfterDeposit, depositId, investmentAmount, investmentId);
					msg.append(" and cleared user active data");
				}
				msg.append(". Commiting in db");
				con.commit();
			} catch (SQLException e) {
				try {
					con.rollback();
				} catch (SQLException sqle) {
					logger.error("Failed to rollback.", sqle);
				}
				throw e;
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Can't return connection back to autocommit.", e);
				}
				closeConnection(con);
			}
		}
	}

	public static void updateUserBonusAbuser(long userId, boolean isBonusAbuser) throws SQLException {
    	Connection con = null;

	    	try {
	    		con = getConnection();
	        	UsersDAOBase.updateUserBonusAbuser(con, userId, isBonusAbuser);
			
	    	} finally {
	    		closeConnection(con);
	    	}			

	}
	

	public static boolean isNewbies(long userId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersDAOBase.isNewbies(con, userId);
    	} finally {
    		closeConnection(con);
    	}	
	}
	
	 public static long getUserIdByUserName(String name) throws SQLException {
		 Connection con = null;
		 try {
			 con= getConnection();
			 return UsersDAOBase.getUserIdByUserName(con, name);
		 }finally {
			 closeConnection(con);
		 }
	 }
	 
	public static String getUserNameById(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.getUserNameById(con, userId);
		} finally {
			closeConnection(con);
		}

	}

	public static String getWriterNickName(long userId, boolean firstName) throws SQLException {
		Connection con = null;
		String writerNickName=null;
		try{
			con = getConnection();
			writerNickName=WritersDAO.getNickNameFromWriter(con,userId, firstName);
		} finally {
			closeConnection(con);
		}
		return writerNickName;
	}
	
	public static boolean sendActivationMailForSuspend(long userId,boolean isUserRequest) throws SQLException {
		Connection con = getConnection();
		try {
			UserBase user = UsersDAOBase.getUserById(con, userId);
			UserRegulationBase userRegulation = new UserRegulationBase();
            userRegulation.setUserId(user.getId());
            UserRegulationManager.getUserRegulation(userRegulation);
            int suspendReason = userRegulation.getSuspendedReasonId();
            if (suspendReason == 0) {
            	logger.debug(" sendActivationMailForSuspend : user is not suspended! ");
            	return false;
            } else {
            	long writerId = user.getWriterId();
            	long treshold = userRegulation.getTresholdLimit();
            	String tresholdTxt = "";
            	tresholdTxt = CommonUtil.displayAmount(treshold, user.getCurrencyId());
            	String activationLink = null;
            	if (suspendReason == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP) {
            		try {
            			activationLink = CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, user.getSkinId());
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_LOW_SCORE, writerId, user, null, tresholdTxt, activationLink, null);
            			logger.debug("Sending low score group activation email with activation link : " + activationLink);
            			return true;
            		} catch (Exception e) {
            			logger.error("Cannot send low score group activation email!! ", e);
            		}
            	} else if (suspendReason == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP) {
            		try {
            			activationLink = activationLink + CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, user.getSkinId());
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_TRESHOLD, writerId, user, null, tresholdTxt, activationLink, null);
            			logger.debug("Sending treshold activation email with activation link : " + activationLink + "and treshold: " + tresholdTxt);
            			return true;
            		} catch (Exception e) {
            			logger.error("Cannot send low score low treshold group activation email!! ", e);
            		}
            	} else if (suspendReason == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
            		try {
            			activationLink = activationLink + CommonUtil.generateEtActivationLink(userId, suspendReason, treshold, isUserRequest, user.getSkinId());
            			sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_TRESHOLD, writerId, user, null, tresholdTxt, activationLink, null);
            			logger.debug("Sending treshold activation email with activation link : " + activationLink + "and treshold: " + tresholdTxt);
            			return true;
            		} catch (Exception e) {
            			logger.error("Cannot send high treshold group activation email!! ", e);
            		}
            	}
            }
		} catch (SQLException sqle) {
			logger.error("Cannot get user from user ID!!! ", sqle);
			return false;
		} finally {
			closeConnection(con);
		}
		return false;
	}
	
	public static boolean sendMailAfterQuestAORegulation(UserRegulationBase ur) throws SQLException {
		Connection con = getConnection();
		try {
			UserBase user = UsersDAOBase.getUserById(con, ur.getUserId()); 
			UserRegulationManager.getUserRegulation(ur);
			long writerId = Writer.WRITER_ID_WEB;
			String activationLink = null;
			if (ur.isRestrictedGroup()) { 
            	// send email for restricted users
            	try {
            		if(ur.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID){
            			activationLink = CommonUtil.generateAOActivationLinkAfterFaildQuest(user.getId(), user.getSkinId());
            		} else {
            			activationLink = CommonUtil.generateAOActivationLinkAndInsertLinkIssue(user.getId(), user.getSkinId());
            		}                	
                	sendMailTemplateFile(ConstantsBase.TEMPLATE_ACTIVATION_MAIL_RESTRICTED, writerId, user, null, "", activationLink, null);
        			logger.debug("Sending activation email with activation link for restricted user: " + activationLink);
        			return true;
            	} catch(Exception ex) {
            		logger.error("Cannot send activation email with activation link for restricted user!! ", ex);
            		return false;
            	}
            }
        } catch (SQLException sqle) {
        	logger.error("Cannot get user from user ID!!! ", sqle);
        	return false;
        } finally {
        	closeConnection(con);
        }
		return false;
	}
	

	public static boolean sendMailAfterLimitRegulation(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			UserBase user = UsersDAOBase.getUserById(con, userId);
			UserRegulationBase ur = new UserRegulationBase();
            ur.setUserId(user.getId());
            UserRegulationManager.getUserRegulation(ur);
          
			long writerId = Writer.WRITER_ID_WEB;
			String missingDocuments = "";
			Locale locale = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(user.getSkinId()).getDefaultLanguageId()).getCode());
			logger.info("Trying to send email for blocked account/required documents: " + ur);
		if (ur.isSuspendedDueDocuments()) { 
            	// send email for users  over 10000
            	try {
                	List<com.anyoption.common.beans.File> files = FilesDAOBase.getFilesByUserId(con, user.getId(), locale);
                	missingDocuments = listOfMissingDocuments(files, user);
                	sendMailTemplateFile(ConstantsBase.TEMPLATE_DOCUMENT_SCENARIO_TIME_PASSED, writerId, user, null, "", null, missingDocuments);
        			logger.debug("Sending email for blocked account for user " + user.getId());
        			return true;
            	} catch(Exception ex) {
            		logger.error("Cannot send email for blocked account for user " + user.getId(), ex);
            		return false;
            	}
            }  else if (ur.showWarningDueDocuments()) {
               	// send email for users between 5000 - 10000
               	try {
                   	List<com.anyoption.common.beans.File> files = FilesDAOBase.getFilesByUserId(con, user.getId(), locale);
                   	missingDocuments = listOfMissingDocuments(files, user);
                   	sendMailTemplateFile(ConstantsBase.TEMPLATE_DOCUMENT_SCENARIO, writerId, user, null, "", null, missingDocuments);
            		logger.debug("Sending email for required documents for user " + user.getId());
            		return true;
               	} catch(Exception ex) {
               		logger.error("Cannot send email for required documents for user " + user.getId(), ex);
               		return false;
               	} 
               }             
            } catch (SQLException sqle) {
            	logger.error("Cannot get user from user ID!!! ", sqle);
            	return false;
            } finally {
            	closeConnection(con);
            }
            return false;
	}
	
	// Sending email to activate user through email activation link
	
	public static boolean sendActivationMailForBlockedUsers(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UserBase user = UsersDAOBase.getUserById(con, userId);
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			long writerId = Writer.WRITER_ID_WEB;
			String tresholdTxt = "";
        	tresholdTxt = CommonUtil.formatCurrencyAmountWithoutDecimalPoints(ur.getThresholdBlock(), user.getCurrencyId());
			logger.info("Trying to send email for blocked account due to low/high threshold: " + ur);
			if (ur.isTresholdBlock()) {
				String activationLink = CommonUtil.generateAOActivationLinkForBlockedUsers(userId, user.getSkinId());
				sendMailTemplateFile(ConstantsBase.TEMPLATE_AO_ACTIVATION_MAIL_TRESHOLD, writerId, user, null, tresholdTxt, activationLink, null);
				return true;
			}
		} catch (Exception ex) {
			logger.error("Cannot send activation mail for blocked users due to low/high threshold", ex);
			return false;
		} finally {
			closeConnection(con);
		}
		return false;
	}
	
	public static ArrayList<RestThreshBlockedUsersBean> getRestThreshBlockedUsersData() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.getRestThreshBlockedUsersData(con);
		} finally {
			closeConnection(con);
		}
	}
		
	public static boolean sendBlankEmail(String blankEmailContent, long userId, long writerId, String mailToSupport, String blankTemplateSubject) {
		Connection con = null;
		try{
			con = getConnection();
			UserBase user = UsersDAOBase.getUserById(con, userId);
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			logger.info("Going to send blank e-mail for user with ID: " + userId);
			sendMailTemplateFile(ConstantsBase.TEMPLATE_BLANK, writerId, user, mailToSupport, null, null, null,null, blankEmailContent, blankTemplateSubject, null);
			return true;
		} catch (Exception ex){
			logger.error("Cannot send blank e-mail for user with ID: " + userId, ex);
			return false;
		}
	}

	public static String listOfMissingDocuments(List<com.anyoption.common.beans.File> files, UserBase user){
		
		String missingDocuments="";
		Locale locale = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(user.getSkinId()).getDefaultLanguageId()).getCode());
		String passportExist = null;
		String userIdCopyExist = null;
		String drivingLicenseExist = null;
		
		if (files != null) {
			
			int passport = files.indexOf(FileType.PASSPORT);
			int userIdCopy = files.indexOf(FileType.USER_ID_COPY);
			int drivingLicense = files.indexOf(FileType.DRIVER_LICENSE);
			
			if (passport != -1)
				passportExist=files.get(passport).getFileName();
			if (userIdCopy != -1)
				userIdCopyExist=files.get(userIdCopy).getFileName();
			if (drivingLicense != -1)
				drivingLicenseExist=files.get(drivingLicense).getFileName();
				
			if (passportExist == null && userIdCopyExist == null && drivingLicenseExist == null){
				missingDocuments = missingDocuments.concat(CommonUtil.getMessage(locale, "missing.document.id.pass.license", null));
				missingDocuments = missingDocuments.concat("<br/>");
			}
			
			for (com.anyoption.common.beans.File file : files) {
				if (file.getFileName() != null) {
					continue;
				} else {
					if (file.getFileTypeId() == FileType.UTILITY_BILL) {
						missingDocuments = missingDocuments.concat(CommonUtil.getMessage(locale, "missing.document.utility.bill", null));
						missingDocuments = missingDocuments.concat("<br/>");
					}		
					if (file.getFileTypeId() == FileType.CC_COPY_FRONT) {
						missingDocuments = missingDocuments.concat(CommonUtil.getMessage(locale, "missing.document.cc.front", null));
						missingDocuments = missingDocuments.concat("<br/>");
					}
					if (file.getFileTypeId() == FileType.CC_COPY_BACK) {
						missingDocuments = missingDocuments.concat(CommonUtil.getMessage(locale, "missing.document.cc.back", null));
						missingDocuments = missingDocuments.concat("<br/>");
					}
				}
			}			
		}
	return missingDocuments;
	}

    public static boolean hasTheSameInvestmentInTheLastSecSameIp(String ip, int sec, long oppId, InvestmentRejects invRej) throws SQLException {
        logger.debug("Checking submitting investment twice in less than " + sec + " sec from ip " + ip );
        boolean has = false;
        Connection conn = getConnection();
        try {
            has = UsersDAOBase.hasTheSameInvestmentInTheLastSecSameIp(conn, ip, sec, oppId, invRej);
        } finally {
            closeConnection(conn);
        }
        return has;
    }
    
	  /**
	   * Get affiliate instance by key
	   * @param affiliateKey
	   * @return affiliate instance
	   * @throws SQLException
	   */
	public static MarketingAffilate getAffiliateByKey(long affiliateKey) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffiliatesDAOBase.getAffiliateByKey(con, affiliateKey);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<MarkVipHelper> getUsersToMarkVip() throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUsersToMarkVip(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void markUserAsVip(MarkVipHelper markVipHelper, Issue issue, IssueAction issueAction) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			markVipHelper.setIssueId(IssuesManagerBase.searchIssueIdByPopEntryID(markVipHelper.getCurrEntryId()));
			UsersDAOBase.updateVipStatus(con, markVipHelper.getUserId(), 2);
			IssuesManagerBase.insertIssue(con, issue.getSubjectId(), issue.getStatusId(), issue.getUserId(), issue.getPriorityId(), issue.getType(), issueAction.getActionTypeId(),
					issueAction.getWriterId(), issueAction.isSignificant(), issueAction.getActionTime(), issueAction.getActionTimeOffset(), issueAction.getComments());
			con.commit();
		} catch (SQLException e) {
			try {
			con.rollback();
			} catch (SQLException sqle) {
			logger.error("Failed to rollback.", sqle);
			}
			throw e;
		} finally {
			try {
			con.setAutoCommit(true);
			} catch (SQLException e) {
			logger.error("Can't return connection back to autocommit.", e);
			}
			closeConnection(con);
		}
	}
	
	/**
	 * Get users to fire server pixel
	 * @param fireServerPixelFields
	 * @return ArrayList<FireServerPixelHelper>
	 * @throws SQLException
	 */
	public static ArrayList<FireServerPixelHelper> getUsersToFireServerPixel(FireServerPixelFields fireServerPixelFields) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUsersToFireServerPixel(con, fireServerPixelFields);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Long> getSingleLoginUsers() throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getSingleLoginUsers(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static User getUserByUsername(String username) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUserByUsername(con, username);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean sendUsersAutoEmail(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			UserBase user = UsersDAOBase.getUserById(con, userId);            
			long writerId = user.getWriterId();
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
            user.setLocale(locale);
            sendMailTemplateFile(ConstantsBase.TEMPLATE_DOCUMENT_FTD, writerId, user);
		} catch (Exception e) {
			logger.error("When try to sendUsersAutoEmail", e);
			return false;
		} finally {
			closeConnection(con);
		}
		return true;
	}
	
	public static void updateUserNonRegSuspend(long userId, long suspendType) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAOBase.updateUserNonRegSuspend(con, userId, suspendType);
		} finally {
			 closeConnection(con);
		}
	}
}