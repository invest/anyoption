/**
 * 
 */
package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.ConstantInvestmentsStream;
import il.co.etrader.dao_managers.ConstantInvestmentsStreamDAOBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author pavelhe
 *
 */
public class ConstantInvestmentsStreamManagerBase extends BaseBLManager {
	
	public static ArrayList<ConstantInvestmentsStream> getAll() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ConstantInvestmentsStreamDAOBase.getAll(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<ConstantInvestmentsStream> getActive() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ConstantInvestmentsStreamDAOBase.getActive(con);
		} finally {
			closeConnection(con);
		}
	}

}
