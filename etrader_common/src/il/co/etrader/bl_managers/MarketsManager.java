package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AssetIndexMarket;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.daos.AssetIndexMarketDAO;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.dao_managers.MarketsDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;

/**
 * Markets web manager.
 *
 * @author Tony
 */
public class MarketsManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(MarketsManager.class);
	
	private static HashMap<Long, Map<Long, String>> skinsMarkets = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, LocalDate> skinsMarketsDates = new HashMap<Long, LocalDate>();
	
	private static HashMap<Long, ArrayList<ArrayList<Market>>> bankOptionMarkets = new HashMap<Long, ArrayList<ArrayList<Market>>>();
	private static Map<Long, LocalDate> bankOptionMarketsDates = new HashMap<Long, LocalDate>();
	
	private static HashMap<Long, ArrayList<Market>> assetIndexes = new HashMap<Long, ArrayList<Market>>();
	private static Map<Long, LocalDate> assetIndexesDates = new HashMap<Long, LocalDate>();
	
	private static HashMap<Long, HashMap<Long, String>> optionPlusMarkets = new HashMap<Long, HashMap<Long, String>>();
	private static Map<Long, LocalDate> optionPlusMarketsDates = new HashMap<Long, LocalDate>();
	
	private static HashMap<Long, Map<Long, String>> markets0100 = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, LocalDate> markets0100Dates = new HashMap<Long, LocalDate>();

	private static HashMap<Long, Map<Long, String>> marketsDynamics = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, LocalDate> marketsDynamicsDates = new HashMap<Long, LocalDate>();
	
	private static HashMap<Long, ArrayList<AssetIndexMarket>> assetIndexMarkets = new HashMap<Long, ArrayList<AssetIndexMarket>>();
	private static Map<Long, LocalDate> assetIndexMarketsDates = new HashMap<Long, LocalDate>();
	
    /**
     * Liad market ids.
     *
     * @return <code>ArrayList<Long></code>.
     * @throws SQLException
     */
    public static ArrayList<Market> getMarkets(long opportunityTypeId) throws SQLException {
        ArrayList<Market> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = MarketsDAOBase.getChartsUpdaterMarkets(conn, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Collect the ids of all markets that are present in eTrader and not in AnyOption.
     *
     * @return <code>ArrayList<Long></code>
     * @throws SQLException
     */
    public static ArrayList<Long> getIsraelMarketsIds() throws SQLException {
        ArrayList<Long> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = MarketsDAOBase.getIsraelMarketsIds(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static void setMarketAndOppQuote(long marketId, String quote) throws Exception{
    	Connection conn = getConnection();
    	try{
    		conn.setAutoCommit(false);
    		MarketsDAOBase.setQuoteParamsById(conn, marketId, quote);
			OpportunitiesDAOBase.setOppMarketQuote(conn, marketId, quote);
			conn.commit();
    	} catch (SQLException sqle) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                log.error("Can't rollback.", e);
            }
            throw sqle;
        } finally {
            conn.setAutoCommit(true);
            closeConnection(conn);
        }
    }
    
	public static HashMap<Long, MarketGroup> getMarketGroups() throws SQLException {
		Connection conn = null;
		HashMap<Long, MarketGroup> map = new HashMap<Long, MarketGroup>();
		try {
			conn = getConnection();
			map = MarketsDAOBase.getMarketGroups(conn);
		} finally {
			closeConnection(conn);
		}
		return map;
	}

	public static void changeFXALParameter(Connection con, String quoteParams, long marketId) throws SQLException {
		String fXALParameterStr = quoteParams.substring(quoteParams.indexOf("FXAL=") + 5, quoteParams.indexOf(";", quoteParams.indexOf("FXAL=")));
		Float fXALParameter = Float.valueOf(fXALParameterStr);
		Map<Long, String> hourLevelFormulas = MarketsDAOBase.getHourLevelFormulas(con, marketId);
		Map<Long, String> newHourLevelFormulas = new HashMap<>();
		for (Long sourceId : hourLevelFormulas.keySet()) {
			String params = hourLevelFormulas.get(sourceId);
			int start = params.indexOf("\"fXALParameter\": \"") + 18;
			int end = params.indexOf("\"", start);
			String value = params.substring(start, end);
			newHourLevelFormulas.put(sourceId, params.replace("\"fXALParameter\": \"" + value, "\"fXALParameter\": \"" + fXALParameter));
		}
		for (Long sourceId : newHourLevelFormulas.keySet()) {
			MarketsDAOBase.updateHourLevelFormulas(con, marketId, sourceId, newHourLevelFormulas.get(sourceId));
		}
	}
	
	public static Map<Long, String> getSkinsMarkets(long skinId) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == skinsMarketsDates.get(skinId)
				|| currentDate.isAfter(skinsMarketsDates.get(skinId))
				|| skinsMarkets.get(skinId) == null) {
			synchronized (skinsMarkets) {
				if (null == skinsMarketsDates.get(skinId)
						|| currentDate.isAfter(skinsMarketsDates.get(skinId))
						|| skinsMarkets.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						skinsMarkets.put(skinId, SkinsDAOBase.getSkinsMarkets(con, skinId));
						skinsMarketsDates.put(skinId, currentDate);	
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return skinsMarkets.get(skinId);
	}

	public static ArrayList<ArrayList<Market>> getBankOptionMarkets(long skinId) {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == bankOptionMarketsDates.get(skinId)
				|| currentDate.isAfter(bankOptionMarketsDates.get(skinId))
				|| bankOptionMarkets.get(skinId) == null) {
			synchronized (bankOptionMarkets) {
				if (null == bankOptionMarketsDates.get(skinId)
						|| currentDate.isAfter(bankOptionMarketsDates.get(skinId))
						|| bankOptionMarkets.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						bankOptionMarkets.put(skinId, MarketsDAOBase.getBankOptionMarkets(con, skinId));
						bankOptionMarketsDates.put(skinId, currentDate);
					} catch (SQLException sqle) {
						log.error("Cannot load bank option markets for skin ID " + skinId, sqle);
					} finally {
						closeConnection(con);
					}			
				}				
			}
		}
		return bankOptionMarkets.get(skinId);
	}
	
	public static ArrayList<Market> getAssetIndex(long skinId) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == assetIndexesDates.get(skinId)
				|| currentDate.isAfter(assetIndexesDates.get(skinId))
				|| assetIndexes.get(skinId) == null) {
			synchronized (assetIndexes) {
				if (null == assetIndexesDates.get(skinId)
						|| currentDate.isAfter(assetIndexesDates.get(skinId))
						|| assetIndexes.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						assetIndexes.put(skinId, MarketsDAOBase.getAssetIndex(con, skinId));
						assetIndexesDates.put(skinId, currentDate);
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return assetIndexes.get(skinId);
	}
	
	public static HashMap<Long, String> getOptionPlusMarket(long skinId) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == optionPlusMarketsDates.get(skinId)
				|| currentDate.isAfter(optionPlusMarketsDates.get(skinId))
				|| optionPlusMarkets.get(skinId) == null) {
			synchronized (optionPlusMarkets) {
				if (null == optionPlusMarketsDates.get(skinId)
						|| currentDate.isAfter(optionPlusMarketsDates.get(skinId))
						|| optionPlusMarkets.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						optionPlusMarkets.put(skinId, MarketsDAOBase.getOptionPlusMarket(con, skinId));
						optionPlusMarketsDates.put(skinId, currentDate);
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return optionPlusMarkets.get(skinId);
	}
	
	public static Map<Long, String> getMarketsBinary0100(long skinId) {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == markets0100Dates.get(skinId)
				|| currentDate.isAfter(markets0100Dates.get(skinId))
				|| markets0100.get(skinId) == null) {
			synchronized (markets0100) {
				if (null == markets0100Dates.get(skinId)
						|| currentDate.isAfter(markets0100Dates.get(skinId))
						|| markets0100.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						markets0100.put(skinId, SkinsDAOBase.getMarketsBinary0100(con, skinId));
						markets0100Dates.put(skinId, currentDate);
					} catch (SQLException sqle) {
						log.error("Cannot load markets 0100 for skin ID " + skinId, sqle);
					} finally {
						closeConnection(con);
					}
				}
			}
		}
		return markets0100.get(skinId);
	}
	
	public static Map<Long, String> getMarketsDynamics(long skinId) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == marketsDynamicsDates.get(skinId)
				|| currentDate.isAfter(marketsDynamicsDates.get(skinId))
				|| marketsDynamics.get(skinId) == null) {
			synchronized (marketsDynamics) {
				if (null == marketsDynamicsDates.get(skinId)
						|| currentDate.isAfter(marketsDynamicsDates.get(skinId))
						|| marketsDynamics.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						marketsDynamics.put(skinId, SkinsDAOBase.getMarketsDynamics(con, skinId));
						marketsDynamicsDates.put(skinId, currentDate);
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return marketsDynamics.get(skinId);
	}
	
	public static ArrayList<AssetIndexMarket> getAssetIndexMarkets(long skinId,
			Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == assetIndexMarketsDates.get(skinId)
				|| currentDate.isAfter(assetIndexMarketsDates.get(skinId))
				|| assetIndexMarkets.get(skinId) == null)  {
			synchronized (assetIndexMarkets) {
				if (null == assetIndexMarketsDates.get(skinId)
						|| currentDate.isAfter(assetIndexMarketsDates.get(skinId))
						|| assetIndexMarkets.get(skinId) == null)  {
					Connection con = null;
					try {
						con = getConnection();
						assetIndexMarkets.put(skinId, AssetIndexMarketDAO.getAssetIndexMarkets(con, skinId, groupAssetIndexMarket));
						assetIndexMarketsDates.put(skinId, currentDate);
					} finally {
						closeConnection(con);
					}		
				}				
			}
		}
		return assetIndexMarkets.get(skinId);
	}
}