package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.clearing.ACHClearingProvider;
import com.anyoption.common.clearing.ACHInfo;
import com.anyoption.common.clearing.CDPayClearingProvider;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.CashUInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.clearing.ClearingProviderConfig;
import com.anyoption.common.clearing.DeltaPayClearingProvider;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.clearing.EPGInfo;
import com.anyoption.common.clearing.InatecClearingProvider;
import com.anyoption.common.clearing.InatecIframeClearingProvider;
import com.anyoption.common.clearing.InatecIframeInfo;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.clearing.MoneybookersClearingProvider;
import com.anyoption.common.clearing.MoneybookersInfo;
import com.anyoption.common.clearing.Powerpay21ClearingProvider;
import com.anyoption.common.clearing.UkashInfo;
import com.anyoption.common.managers.TransactionRequestResponseManager;

import il.co.etrader.bl_vos.ErrorCode;
import il.co.etrader.clearing.CashUClearingProvider;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.dao_managers.ClearingDAO;

/**
 * Clearing manager.
 *
 * @author Tony
 */
public class ClearingManager extends ClearingManagerBase {

    /**
     * Load clearing provider by id
     * @param conn
     * @param providerId  provider to load
     * @throws ClearingException
     */
    public static String loadClearingProviderById(long providerId) throws ClearingException {
        Connection conn = null;
        try {
        	 conn = getConnection();
        	 ClearingProviderConfig c = ClearingDAO.loadClearingProviderById(conn, providerId);
        	 return c.getName();
        } catch (Throwable t) {
            throw new ClearingException("Error loading clearing provider.", t);
        } finally {
        	closeConnection(conn);
        }
    }

   
    public static ClearingRoute otherDeposit(ClearingInfo info) throws ClearingException {
    	ClearingRoute cr = null;

        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            try {
                p.authorize(info);
                if (info.getRequestResponse() != null) {
                	TransactionRequestResponseManager.insert(info.getRequestResponse());
                }
            } catch (Exception e) {
            	throw new ClearingException("problem in otherDeposit", e);
            } 
        }
        return cr;
    }

    /**
     * Direct banking deposit
     * @param info
     * @throws ClearingException
     */
    public static void directDeposit(InatecInfo info, long clearingProviderId) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		info.setInatecPaymentType(info.getInatecPaymentType());
    		try {
    			((InatecClearingProvider)p).onlineInitializeRequest(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing directDeposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Direct banking deposit
     * @param info
     * @throws ClearingException
     */
    public static void directDepositACH(ACHInfo info) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(ACH_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(ACH_PROVIDER_ID);
    		try {
    			p.purchase(info);
    		} catch (ClearingException c) {
    			throw c;
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing directDeposit.", t);
            }

    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Direct banking deposit - Ukash
     * @param info
     * @throws ClearingException
     */
    public static void directDepositUkash(UkashInfo info) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(UKASH_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(UKASH_PROVIDER_ID);
    		try {
    			p.capture(info);
    		} catch (ClearingException c) {
    			throw c;
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing Ukash direct Deposit.", t);
            }

    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     *  ACH statusRequest call
     * @param info
     * @throws ClearingException
     */
    public static void ACHStatusReq(ACHInfo info) throws ClearingException {
    	ACHClearingProvider p = (ACHClearingProvider) getClearingProviders().get(ACH_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(ACH_PROVIDER_ID);
    		try {
    			p.statusRequest(info);
    		} catch (ClearingException c) {
    			throw c;
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing ACH statusReq.", t);
            }

    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Direct banking deposit
     * @param info
     * @throws ClearingException
     */
    public static String getCashURequest(CashUInfo info) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(CASHU_PROVIDER_ID);
    	if (null != p) {
    		return ((CashUClearingProvider)p).getQuery(info);
    	} else {
            return "";
        }
    }

    public static String getCashUUrl() throws ClearingException {
    	ClearingProvider cp = getClearingProviders().get(CASHU_PROVIDER_ID);
    	String url = cp.getUrl();
    	return url;
    }

    /**
     * Direct banking deposit with Envoy
     * @param info
     * @throws ClearingException
     */
    public static void directDeposit(EnvoyInfo info) throws ClearingException {
    	ClearingProvider p = getClearingProviders().get(ENVOY_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(ENVOY_PROVIDER_ID);
     		try {
    			p.purchase(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing directDeposit.", t);
            }

    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Capture previously authorized request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void capture(ClearingInfo info) throws ClearingException {
        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            p.capture(info);
        } else {
            throw new ClearingException("No route");
        }
    }


    /**
     * Process withdraw request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void withdraw(ClearingInfo info, Connection conn) throws ClearingException {
        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            p.withdraw(info);
        } else {
            throw new ClearingException("No route");
        }
    }
    
    public static void withdraw(ClearingInfo info) throws ClearingException {
        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            p.withdraw(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Process envoy withdraw request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void withdraw(EnvoyInfo info) throws ClearingException {

        ClearingProvider p = getClearingProviders().get(ENVOY_PROVIDER_ID);
        p.withdraw(info);

    }

    /**
     * Refund money for previous successful debit transaction.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void bookback(ClearingInfo info) throws ClearingException {
        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            p.bookback(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Process purchase request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void purchase(ClearingInfo info) throws ClearingException {
        ClearingProvider p = getClearingProviders().get(info.getProviderId());
        if (null != p) {
            p.purchase(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Process Inatec online status result request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void onlineStatusResult(ClearingInfo info) throws ClearingException {
        InatecClearingProvider inp = (InatecClearingProvider) getClearingProviders().get(info.getProviderId());
        if (null != inp) {
        	inp.onlineStatusResult(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Process Powerpay online status result request.
     *
     * @param info the request info
     * @throws ClearingException
     */
    public static void powerPayStatusResult(InatecInfo info) throws ClearingException {
        Powerpay21ClearingProvider powerPay = (Powerpay21ClearingProvider) getClearingProviders().get(info.getProviderId());
        if (null != powerPay && info != null && info.getAuthNumber() != null ) {
        		powerPay.diagnose(info);
        } else {
            throw new ClearingException("No route");
        }
    }

    public static String getProviderName(long clearingProviderId) {
        ClearingProvider p = getClearingProviders().get(clearingProviderId);
        if (null != p) {
            return p.getName();
        }
        return null;
    }

    public static long getProviderNonCftAvailable(long clearingProviderId) {
        ClearingProvider p = getClearingProviders().get(clearingProviderId);
        if (null != p) {
            return p.getNonCftAvailable();
        }
        return 0;
    }



    public static void setDeltaPayChinaBookBack(DeltaPayInfo info) throws ClearingException {
    	DeltaPayClearingProvider p = (DeltaPayClearingProvider)getClearingProviders().get(DELTAPAY_CHINA_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(DELTAPAY_CHINA_PROVIDER_ID);
    		try {
    			p.bookback(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing DeltaPay withdrawal! (setDeltaPayChinaDetails function).", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }
    
    /**
     * @param info
     * call CDPay bookback to set cpday info by the respose.
     * @throws ClearingException
     */
    public static void setCDPayChinaBookBack(CDPayInfo info) throws ClearingException {
    	CDPayClearingProvider p = (CDPayClearingProvider)getClearingProviders().get(CDPAY_PROVIDER_ID);
    	if (null != p) {
    		info.setProviderId(CDPAY_PROVIDER_ID);
    		try {
    			p.bookback(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing CDPay withdrawal! (setCDPayChinaDetails function).", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }

    /**
     * Deposit with Moneybookers
     * @param info
     * @throws ClearingException
     */
    public static void setMoneybookersDepositDetails(MoneybookersInfo info, long clearingProviderId) throws ClearingException {
    	MoneybookersClearingProvider p = (MoneybookersClearingProvider)getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		try {
    			p.setProviderDetails(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing Moneybookers deposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }
    
    /**
     * Deposit with CDPay - set cdpay clearing provider details.
     * @param info
     * @throws ClearingException
     */
    public static void setCDPayDepositDetails(CDPayInfo info, long clearingProviderId) throws ClearingException {
    	CDPayClearingProvider p = (CDPayClearingProvider)getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		try {
    			p.setProviderDetails(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing CDPay deposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
    }


	public static ArrayList<ErrorCode> getAllErrorCodes() throws SQLException{
		Connection conn = getConnection();
		try{
			return ClearingDAO.getAllErrorCodes(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static ArrayList<Long> getRoutingLimitations(long currencyId, long ccTypeId, long binId, long countryId) throws SQLException{
		Connection conn = getConnection();
		try{
			return ClearingDAO.getRoutingLimitations(conn, currencyId, ccTypeId, binId, countryId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<Long> getRelevantlimits(ClearingInfo ci, ClearingRoute cr) throws SQLException{
		Connection conn = getConnection();
		try{
			return ClearingDAO.getRelevantlimits(conn, ci, cr);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateTimeLastReroute(long id) throws SQLException{
		Connection con = getConnection();
		try{
			ClearingDAO.updateTimeLastRerouted(con, id);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateClearingProviderAndLog(ClearingRoute cr, CreditCard cc, long newClearingProviderId, long erroCodeId) throws SQLException{
		Connection con = getConnection();
		try{
			ClearingDAO.updateClearingProviderAndLog(con, cr, cc, newClearingProviderId, erroCodeId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static long getProviderGroupIdByProviderId(long clearingProviderId) throws SQLException{
		Connection con = getConnection();
		try{
			return ClearingDAO.getProviderGroupIdByProviderId(con, clearingProviderId);
		} finally {
			closeConnection(con);
		}
	}

	public static long getproviderGroupById(long clearingProviderId){
		if(clearingProviderId == TBI_AO_PROVIDER_ID){
			return CLEARING_GROUP_TBI;
		} else if(clearingProviderId == WC_PROVIDER_ID){
			return CLEARING_GROUP_WIRE_CARD;
		} else if(clearingProviderId == WC_3D_PROVIDER_ID){
			return CLEARING_GROUP_WIRE_CARD_3D;
		} else if(clearingProviderId == INATEC_PROVIDER_ID_EUR || clearingProviderId == INATEC_PROVIDER_ID_USD || clearingProviderId == INATEC_PROVIDER_ID_GBP || clearingProviderId == INATEC_PROVIDER_ID_TRY){
			return CLEARING_GROUP_INATEC;
		}
		return 0;
	}
  

	public static ArrayList<ClearingRoute> getClearingRoutes() throws SQLException {
		Connection conn = getConnection();
		try{
			return ClearingDAO.getClearingRoutes(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @param info
	 * @param clearingProviderId
	 * @throws ClearingException
	 */
	public static void setInatecIframeDepositDetails(InatecIframeInfo info, long clearingProviderId) throws ClearingException {
		InatecIframeClearingProvider p = (InatecIframeClearingProvider)getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		try {
    			p.setProviderDetails(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing InatecIframe deposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
	}
	
	public static void setEPGDepositDetails(EPGInfo info, long clearingProviderId) throws ClearingException {
		EPGClearingProvider p = (EPGClearingProvider)getClearingProviders().get(clearingProviderId);
    	if (null != p) {
    		info.setProviderId(clearingProviderId);
    		try {
    			p.setProviderDetails(info);
    		} catch (Throwable t) {
                throw new ClearingException("Problem processing EPG checkout deposit.", t);
            }
    	} else {
            throw new ClearingException("No route");
        }
	}
	

	/**
	 *  FOLLOWING ARE UNUSED METHODS
	 *  KEPT FOR REFERENCE
	 * 
	 */
	
    /**
     * Deposit with WebMoney
     * @param info
     * @throws ClearingException
     */
//    public static void setWebMoneyDepositDetails(WebMoneyInfo info, long clearingProviderId) throws ClearingException {
//    	WebMoneyClearingProvider p = (WebMoneyClearingProvider)getClearingProviders().get(clearingProviderId);
//    	if (null != p) {
//    		info.setProviderId(clearingProviderId);
//    		try {
//    			p.setProviderDetails(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem initializing WebMoney deposit details", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
	/**
     * Deposit with BaroPay
     * @param info
     * @param clearingProviderId
     * @throws ClearingException
     */
//    public static void setBaroPayDepositDetails(BaroPayInfo info, long clearingProviderId) throws ClearingException {
//    	BaroPayClearingProvider p = (BaroPayClearingProvider)getClearingProviders().get(clearingProviderId);
//    	if (null != p) {
//    		info.setProviderId(clearingProviderId);
//    		try {
//    			p.setProviderDetails(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing BaroPay deposit.", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
    
    /**
     * Authorize with BaroPay
     * @param info
     * @param clearingProviderId
     * @throws ClearingException
     */
//    public static void authorizeBaroPayDeposit(BaroPayInfo info, long clearingProviderId) throws ClearingException {
//    	BaroPayClearingProvider p = (BaroPayClearingProvider)getClearingProviders().get(clearingProviderId);
//    	if (null != p) {
//    		try {
//    			p.authorize(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing BaroPay authorize.", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
    
    /**
     * bookback by BaroPay
     * @param info
     * @throws ClearingException
     */
//    public static void setBaroPayBookBack(BaroPayInfo info) throws ClearingException {
//    	BaroPayClearingProvider p = (BaroPayClearingProvider)getClearingProviders().get(BAROPAY_PROVIDER_ID);
//    	if (null != p) {
//    		info.setProviderId(BAROPAY_PROVIDER_ID);
//    		try {
//    			p.bookback(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing BaroPay withdrawal! (setBaroPayDetails function).", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
	//
//	public static long logicForUpdateClearingRoute(String bin, long providerId) throws SQLException{
//		long crId = 0;
//		ArrayList<ClearingRoute> list = new ArrayList<ClearingRoute>();
//		ClearingRoute cr = new ClearingRoute();
//		list = getClearingRouteByBin(bin);
//		if(list.size() > 0){
//			return list.get(0).getId();
//		}
//		return crId;	
//	}
//
//	public static ArrayList<ClearingRoute> existclearingRouteWithCurrnecy(String bin) throws SQLException {
//		Connection conn = getConnection();
//		try{
//			return ClearingDAO.existclearingRouteWithCurrnecy(conn, bin);
//		}finally{
//			closeConnection(conn);
//		}
//	}
//	public static long insertNewClearingRouteAndLog(ClearingRoute cr, CreditCard cc, long newClearingProviderId, long erroCodeId, long currnecyId, boolean isInatec) throws SQLException{
//	Connection con = getConnection();
//	try{
//		return ClearingDAO.insertNewClearingRouteAndLog(con, cr, cc, newClearingProviderId, erroCodeId, currnecyId, isInatec);
//	} finally {
//		closeConnection(con);
//	}
//}
//
//public static ClearingRoute getClearingRouteById(long id) throws SQLException{
//	Connection con = getConnection();
//	try{
//		return ClearingDAO.getClearingRouteById(con, id);
//	}finally{
//		closeConnection(con);
//	}
//}
//
//public static ArrayList<ClearingRoute> getClearingRouteByBin(String bin) throws SQLException{
//	Connection con = getConnection();
//	try{
//		return ClearingDAO.getClearingRouteByBin(con, bin);
//	} finally {
//		closeConnection(con);
//	}
//}
//
//public static void updateClearingRouteIsActive(boolean isActive, long crId, CreditCard cc, long errorCodeId) throws Exception{
//	Connection con = getConnection();
//	try {
//		ClearingDAO.updateClearingRouteIsActive(con, isActive, crId, cc, errorCodeId);
//	} finally {
//		closeConnection(con);
//	}
//}
//	
    /**
     * Process envoy Notification handler.
     *
     * @param info the request info
     * @throws ClearingException
     */
//    public static EnvoyInfo handleNotifications(String requestXmlString, long loginId) throws ClearingException {
//
//        EnvoyClearingProvider p = (EnvoyClearingProvider)getClearingProviders().get(ENVOY_PROVIDER_ID);
////        p.handleNotifications(info, notificationType);
//        return p.handleNotifications(requestXmlString, loginId);
//
//    }

    /**
     * Process PayPal withdrawal. Mass pay method (PayPal NVP API)
     * @param info
     * @throws ClearingException
     */
//    public static void MassPay(PayPalInfo info) throws ClearingException {
//    	PayPalClearingProvider p = (PayPalClearingProvider)getClearingProviders().get(PAYPAL_PROVIDER_ID);
//    	if (null != p) {
//    		info.setProviderId(PAYPAL_PROVIDER_ID);
//    		try {
//    			p.withdraw(info);
//    		} catch (Throwable t) {
//                throw new ClearingException("Problem processing PayPal withdrawal! (MassPay).", t);
//            }
//    	} else {
//            throw new ClearingException("No route");
//        }
//    }
    /**
    * PayPal deposit (SetExpress Checkout call)
    * @param info
    * @throws ClearingException
    */
//   public static void PayPalDeposit(PayPalInfo info) throws ClearingException {
//   	PayPalClearingProvider p = (PayPalClearingProvider)getClearingProviders().get(PAYPAL_PROVIDER_ID);
//   	if (null != p) {
//   		info.setProviderId(PAYPAL_PROVIDER_ID);
//   		try {
//   			p.setExpressCheckout(info);
//   		} catch (Throwable t) {
//               throw new ClearingException("Problem processing PayPal setExpressCheckout!.", t);
//           }
//   	} else {
//           throw new ClearingException("No route");
//       }
//   }

   /**
    * PayPal deposit - call GetExpressCheckoutDetails (for getting customer details)
    * @param info
    * @throws ClearingException
    */
//   public static void PayPalGetCustomerDetails(PayPalInfo info) throws ClearingException {
//   	PayPalClearingProvider p = (PayPalClearingProvider)getClearingProviders().get(PAYPAL_PROVIDER_ID);
//   	if (null != p) {
//   		info.setProviderId(PAYPAL_PROVIDER_ID);
//   		try {
//   			p.getExpressCheckoutDetails(info);
//   		} catch (Throwable t) {
//               throw new ClearingException("Problem processing PayPal getExpressCheckoutDetails!.", t);
//           }
//   	} else {
//           throw new ClearingException("No route");
//       }
//   }

   /**
    * PayPal deposit - call DoExpressCheckoutPayment (Final transaction details)
    * @param info
    * @throws ClearingException
    */
//   public static void PayPalDoPayment(PayPalInfo info) throws ClearingException {
//   	PayPalClearingProvider p = (PayPalClearingProvider)getClearingProviders().get(PAYPAL_PROVIDER_ID);
//   	if (null != p) {
//   		info.setProviderId(PAYPAL_PROVIDER_ID);
//   		try {
//   			p.doExpressCheckoutPayment(info);
//   		} catch (Throwable t) {
//               throw new ClearingException("Problem processing PayPal DoExpressCheckoutPayment!.", t);
//           }
//   	} else {
//           throw new ClearingException("No route");
//       }
//   }
	
}  