package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.RemarketingLogin;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.RemarketingLoginsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.managers.BaseBLManager;

public class RemarketingLoginsManagerBase extends BaseBLManager{
	
	private static final Logger logger = Logger.getLogger(RemarketingLoginsManagerBase.class);
	
	public static final long REMARKETING_TYPE_CONTACT = 1;
	public static final long REMARKETING_TYPE_REGISTER = 2;
	public static final long REMARKETING_TYPE_LOGIN = 3;
	
	public static final String ENUM_REMARKETING_CONTACT_TIME_ENUMERATOR = "remarketing_contact_time";
	public static final String ENUM_REMARKETING_CONTACT_TIME_CODE = "remarketing_contact_time_period";
	
	
	public static void insert(RemarketingLogin vo) throws SQLException{
		Connection conn = getConnection();
		try{
			RemarketingLoginsDAOBase.insert(conn, vo);
		} finally {
			closeConnection(conn);			
			if(vo.getUserId() > 0){
				insertRemarketingBonus(vo.getUserId());
			}
		}
	}	
	
	public static void update(RemarketingLogin vo ) throws SQLException{
		Connection conn = getConnection();
		try{
			RemarketingLoginsDAOBase.update(conn, vo);
		} finally {
			closeConnection(conn);			
			if(vo.getUserId() > 0){
				insertRemarketingBonus(vo.getUserId());
			}
		}
	}

	public static RemarketingLogin getById(long id) throws SQLException{
		Connection conn = getConnection();
		try{
			return RemarketingLoginsDAOBase.getById(conn, id);
		} finally {
			closeConnection(conn);
		}
	}

	public static Contact getFirstFreeContact(UserBase user) throws SQLException{
		Connection conn = getConnection();
		try{
			
			return ContactsManager.searchFreeContactByUserPhone(conn, user);
			
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void insertRemarketingBonus(long userId) throws SQLException {

		Connection conn = getConnection();
		boolean haveRemarketingBonuse = BonusDAOBase.userHaveRemarketingBonus(conn, userId);
		if (!haveRemarketingBonuse) {
			
			UserBase user = UsersDAOBase.getUserById(conn, userId);
			try {				
				long remarketingAutoBonusId = BonusDAOBase.getRemarketingAutoBonusId(conn, userId);
				if (remarketingAutoBonusId > 0) {
					BonusUsers bonusUser = new BonusUsers();
					bonusUser.setUserId(user.getId());
					bonusUser.setWriterId(user.getWriterId());
					bonusUser.setBonusId(remarketingAutoBonusId);

					boolean isInsertedBonus = BonusManagerBase.insertBonusUser(bonusUser, user,user.getWriterId(), 0, 0, false);
					if(isInsertedBonus){
						BonusDAOBase.insertRemarketingBonus(conn, userId, bonusUser.getId());					
					}
				}
			} catch (Exception e) {
				logger.error(
						"Failed to insert remarketing auto bonus upon registration for user "+ user.getId(), e);
			} finally {
				closeConnection(conn);
			}
		}				
	}	
	

    /**
     * Insert into remarketing login table by type (contact,register,login) in 2 conditions"
     * 1. User/visitor has in his session remarkting=true
     * 2. Update userId on remarkting login when creating a user that his remarkting <> true but his contact is from remarkting login     
     * @param contactId
     * @param remarketingType
     * @param userId
     * @param email
     * @param mobile
     * @param landLinePhone
     */
    public static void insertRemarektingLoginBytype(long contactId, long remarketingType, long userId, String email, String mobile, 
    		String landLinePhone, long skinId, HttpServletRequest servletRequest) {
    	try {	    		
    		logger.debug("Going to check if need to insert remarketing login. type:" + remarketingType + " contactId:" + contactId + " userId:" + userId);
    		HttpSession session = null;
    		if (servletRequest != null) { // when comming from servlet (Landing Page servlet) we want to take the session from the request. 
    			session = servletRequest.getSession(true);
    		} else {
	    		FacesContext context = FacesContext.getCurrentInstance(); 
	    		session = (HttpSession) context.getExternalContext().getSession(true);
    		}
	       	if (session.getAttribute(ConstantsBase.FROM_REMARKETING) != null) {
	       		logger.debug("Going to insert into remarketing login. type:" + remarketingType + " contactId:" + contactId + " userId:" + userId);
		        RemarketingLogin vo = new RemarketingLogin();
		        vo.setContactId(contactId);
		        vo.setUserId(userId);
				vo.setDyanmicParameter((String)session.getAttribute(ConstantsBase.REMARKETING_DYNAMIC_PARAM)); 
		        vo.setCombinationId(Long.parseLong((String)session.getAttribute(ConstantsBase.REMARKETING_COMBINATION_ID)));
		        vo.setActionType(remarketingType);	        
				RemarketingLoginsManagerBase.insert(vo);
				if (remarketingType == RemarketingLoginsManagerBase.REMARKETING_TYPE_REGISTER) {					
					updateRemakeringLoginUserId(userId, email, mobile, landLinePhone, 0, skinId);				
				}
	       	} else if (remarketingType == RemarketingLoginsManagerBase.REMARKETING_TYPE_REGISTER) {
	       		long timePeriod = Long.parseLong(CommonUtil.getEnum(ENUM_REMARKETING_CONTACT_TIME_ENUMERATOR, ENUM_REMARKETING_CONTACT_TIME_CODE));
	       		updateRemakeringLoginUserId(userId, email, mobile, landLinePhone, skinId, timePeriod);
	       	}			
		} catch (Exception e) {
			logger.error("Error while trying to insert contact to Remarketing Login", e);				
		}
            
    }
    
    /**
     * @param userId
     * @param email
     * @param mobile
     * @param landLinePhone
     * @param timePeriod
     * @throws SQLException
     */
    public static void updateRemakeringLoginUserId(long userId, String email, String mobile, String landLinePhone, long skinId, long timePeriod) throws SQLException {
		Connection conn = getConnection();
		try {
			logger.debug("Going to update userId for remarketing login. userId:" + userId + " email=" + email + 
					" mobile=" + mobile + " landLinePhone=" + landLinePhone);
			int rowsUpdated = RemarketingLoginsDAOBase.updateUserId(conn, userId, email, mobile, landLinePhone, skinId, timePeriod);
			if (timePeriod > 0 && rowsUpdated > 0) { // when connecting user and contact on remarketing login table we want to grant a bonus
				insertRemarketingBonus(userId);
			}
		} finally {
			closeConnection(conn);
		}
    }
}
