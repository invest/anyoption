/**
 * 
 */
package il.co.etrader.bl_managers;

import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.anyoption.common.managers.BaseBLManager;

/**
 * 
 * 
 * @author kirilim
 */
public class MailBoxTemplateManagerBase extends BaseBLManager {

	public static String insertParameters(String template, String parameters) {
    	try {
    		Pattern regex = Pattern.compile("([^&]+)=([^&]+)",
    			Pattern.CANON_EQ);
    		Matcher matcher = regex.matcher(parameters);
    		while (matcher.find()) {
    			template = insertParameter(template, matcher.group(1), URLDecoder.decode(matcher.group(2), "UTF-8"));
    		}
    	} catch (Exception ex) {
    	}
    	return template;
    }

	public static String insertParameter(String template, String paramName, String paramValue) {
    	template = template.replace("${" + paramName + "}", paramValue);
    	template = template.replace("{" + paramName + "}", paramValue);

    	return template;
    }
}
