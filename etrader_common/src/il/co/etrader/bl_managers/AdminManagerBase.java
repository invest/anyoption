package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.City;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.CcBlackListDAO;
import il.co.etrader.dao_managers.CitiesDAO;
import il.co.etrader.dao_managers.ClearingDAO;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.BaseBLManager;

/**
 * Admin manager.
 */
public abstract class AdminManagerBase extends BaseBLManager {

    public static boolean isBlacklisted(CreditCard card, boolean deposit , UserBase user) throws SQLException {
        boolean b = false;
        Connection con = getConnection();
        try {
            b = CcBlackListDAO.isCardBlacklisted(con, card.getCcNumber(), deposit)
                    || isBINBlacklisted(con, deposit, user, card);
        } finally {
            closeConnection(con);
        }
        return b;
    }

    public static boolean isCCBlacklisted(long ccn, boolean deposit , long skinId) throws SQLException {
        boolean b = false;
        Connection con = getConnection();
        try {
            b = CcBlackListDAO.isCardBlacklisted(con, ccn, deposit);
        } finally {
            closeConnection(con);
        }
        return b;
    }

    public static boolean isCCBlackListedWithdraw(long ccn) throws SQLException{
    	boolean retVal;
    	Connection con = getConnection();
    	try{
    		retVal = CcBlackListDAO.isCardBlacklistedWithdraw(con, ccn);
    	}finally{
    		closeConnection(con);
    	}
    	return retVal;

    }

    public static boolean isBINBlacklisted(CreditCard card, boolean deposit , UserBase user) throws SQLException {
        Connection con = getConnection();
        try {
        	return isBINBlacklisted(con, deposit, user, card);
        } finally {
            closeConnection(con);
        }
    }

    public static boolean isBINBlacklisted(Connection con,  boolean deposit , UserBase user, CreditCard card) throws SQLException {
    	String bin = String.valueOf(card.getCcNumber()).substring(0, 6);
        
    	if(TransactionsManagerBase.isBlackListBin(bin)){
    		return true;
    	}
    	
    	long clearingProviderId = 0;
        ArrayList<ClearingRoute> cr = ClearingDAO.findRouteList(con, user.getSkinId(), bin, user.getCurrencyId(), card.getCountryId(), card.getTypeIdByBin(), user.getPlatformId());
        if(cr!=null) {
        	clearingProviderId = cr.get(0).getDepositClearingProviderId();
        } else {
        	// not existing hence not blacklisted
        	return false;
        }

        return (clearingProviderId == ConstantsBase.BLACK_LIST_BIN);
    }

    public static long getCityIdByName(String n) throws SQLException {
        Connection con = getConnection();
        try {
            return CitiesDAO.getIdByName(con, n);
        } finally {
            closeConnection(con);
        }
    }

    public static City getCityById(long n) throws SQLException {
        Connection con=getConnection();
        try {
            return CitiesDAO.getById(con, n);
        } finally {
            closeConnection(con);
        }
    }

    public static double getLastShekelDollar() throws SQLException {
        Connection con = getConnection();
        try {
            return GeneralDAO.getLastShekelDollar(con);
        } finally {
            closeConnection(con);
        }
    }

    public static void addToLog(long writerId, String table, long key, int command, String desc) throws SQLException {
        Connection con = getConnection();
        try {
            GeneralDAO.insertLog(con, writerId, table, key, command, desc);
        } finally {
            closeConnection(con);
        }
    }


}