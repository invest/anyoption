package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.Promotion;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.PromotionsDAO;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;

public abstract class PromotionsManager extends BaseBLManager {

	public static Promotion getLastPromotion(Writer w) throws SQLException{
		Connection conn = getConnection();
		Promotion p = null;
		try{
			p = PromotionsDAO.getLastPromotion(conn, w);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			closeConnection(conn);
		}
		return p;
	}

	public static void updateStatus(int status, long id) throws SQLException{
		Connection conn = getConnection();
		try {
			PromotionsDAO.updateStatus(conn, status, id);
		}finally{
			closeConnection(conn);
		}
	}

	public static void updatePromotionEntriesStatusById(int status, long promotion_id, long user_id) throws SQLException{
		Connection conn = getConnection();
		try {
			PromotionsDAO.updatePromotionEntriesStatusById(conn, status,promotion_id, user_id);
		}finally{
			closeConnection(conn);
		}
	}

	public static void updatePromotionEntriesStatus(long promotionId, int status) throws SQLException{
		Connection con = getConnection();
		try{
			PromotionsDAO.updatePromotionEntriesStatus(promotionId, status , con);
		}finally{
			closeConnection(con);
		}
	}

	public static void updatePromotionEntriesContactStatus(long promotionId, int promotionEntriesStatus) throws SQLException {
		Connection con = getConnection();
		try{
			PromotionsDAO.updatePromotionEntriesStatus(con, promotionId, promotionEntriesStatus);
		}finally{
			closeConnection(con);
		}
	}

}
