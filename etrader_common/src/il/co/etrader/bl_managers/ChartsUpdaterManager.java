//package il.co.etrader.bl_managers;
//
//import il.co.etrader.bl_vos.ChartsUpdaterMarket;
//import il.co.etrader.dao_managers.ChartsUpdaterDAO;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Date;
//
//import com.anyoption.common.beans.MarketRate;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class ChartsUpdaterManager extends BaseBLManager {
//    /**
//     * Liad market ids.
//     *
//     * @return <code>ArrayList<Long></code>.
//     * @throws SQLException
//     */
//    public static ArrayList<ChartsUpdaterMarket> getMarkets(long opportunityTypeId) throws SQLException {
//        ArrayList<ChartsUpdaterMarket> l = null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            l = ChartsUpdaterDAO.getChartsUpdaterMarkets(conn, opportunityTypeId);
//        } finally {
//            closeConnection(conn);
//        }
//        return l;
//    }
//
//    /**
//     * Load market rates.
//     *
//     * @param marketId the market for which to load the rates for
//     * @param after if <code>null</code> load all. else load rates after that time
//     * @return <code>ArrayList<MarketRate></code>
//     * @throws SQLException
//     */
//    public static ArrayList<MarketRate> getMarketRates(long marketId, Date after) throws SQLException {
//        ArrayList<MarketRate> l = null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            l = ChartsUpdaterDAO.getMarketRates(conn, marketId, after);
//        } finally {
//            closeConnection(conn);
//        }
//        return l;
//    }
//}