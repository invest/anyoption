package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_vos.MarketingReport;
import il.co.etrader.dao_managers.MarketingReportDAO;

public class MarketingReportManager extends BaseBLManager {

    public static ArrayList<MarketingReport> getDPReportByDate(long skinId, long skinBusinessCaseId, Date startDate, Date endDate, String offset, int reportType, long sourceId, long marketingCampaignManager, long writerIdForSkin)
            throws SQLException {
        Connection con = getSecondConnection();
        try {
            return MarketingReportDAO.getDPReportByDate(con, skinId, skinBusinessCaseId, startDate, endDate, offset, reportType, sourceId, marketingCampaignManager, writerIdForSkin);
        } finally {
            closeConnection(con);
        }
    }
	
}
