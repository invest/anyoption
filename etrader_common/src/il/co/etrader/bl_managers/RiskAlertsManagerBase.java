/**
 *
 */
package il.co.etrader.bl_managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.managers.RiskAlertsManagerCommonBase;

import il.co.etrader.bl_vos.RiskAlert;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.RiskAlertsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
/**
 * RiskAlerts Manager
 *
 * @author Kobi
 *
 */
public class RiskAlertsManagerBase extends RiskAlertsManagerCommonBase {
    private static final Logger logger = Logger.getLogger(RiskAlertsManagerBase.class);

	// Risk alerts types
	public static final int RISK_TYPE_CC_USED_ON_WEBSITE = 1;
	public static final int RISK_TYPE_CC_STOLEN = 2;
	public static final int RISK_TYPE_CC_DEPOSIT_MORE_THAN_ALLOWED = 3;
	public static final int RISK_TYPE_DEPOSIT_BALANCE_GREATER_THAN_MIN_INVEST = 4;
	public static final int RISK_TYPE_WITHDRAW_NO_INVESTMENTS = 5;
	public static final int RISK_TYPE_WIRE_WITHDRAW_AMOUNT_GREATER_THAN_BALANCE = 6;
	public static final int RISK_TYPE_FIRST_INVESTMENT_IS_1T = 7;
	public static final int RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER = 8;
	public static final int RISK_TYPE_WAGERING_FAILED_WITHDRAW = 9;
	public static final int RISK_TYPE_X_DEPOSIT_NO_DOCUMENT = 10;
	public static final int RISK_TYPE_DEPOSIT_GREATER_THAN_X_NO_DOCUMENT = 11;
	public static final int RISK_TYPE_XX_DEPOSIT_NO_DOCUMENT = 12;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_AO_NO_DOC = 13;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_AO_HAVE_DOC = 14;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_ET_NO_DOC = 15;
	public static final int RISK_TYPE_DEPOSIT_VELOCITY_ET_HAVE_DOC = 16;
	public static final int RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY = 17;
	public static final int RISK_TYPE_REGISTRATION_FROM_COUNTRY_DIFF_THAN_IP_COUNTRY = 18;
	public static final int RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES = 19;
	public static final int RISK_TYPE_SINGLE_LOGIN = 20;

	private static final long NO_INVESTMENT_ID = 0;
	private static final long NO_TRANSACTION_ID = 0;

	public static final int TYPE_INVESTMENT = 99;
	
	public static final int RISK_ALERT_DEPOSIT_CLASS_TYPE = 1;
	public static final int RISK_ALERT_WITHDRAWAL_CLASS_TYPE = 2;
	public static final int RISK_ALERT_INVESTMENT_CLASS_TYPE = 3;
	public static final int RISK_ALERT_REGISTRATION_CLASS_TYPE = 4;

	public static final int FIVE_DOC_SUBJECT = 36;
	public static final int FIVE_DOC_PRIORITY = 1;
	public static final int FIVE_DOC_STATUS = 4;
	public static final int FIVE_DOC_ISSUE_TYPE = 2;
	public static final String FIVE_DOC_ISSUE = "2";
	
	public static final int STATUS_TYPE_ALL = -1;
	public static final int STATUS_TYPE_WAS_NOT_REVIEWED = 0;
	public static final int STATUS_TYPE_WAS_REVIEWED = 1;
	
    /**
     * Insert into Risk alert table
     * @param typeId
     * @param writerId
     * @param trxId
     * @param invId
     * @throws SQLException
     */
    public static void insert(int typeId, long writerId, long trxId, long invId) throws SQLException {
    	if (logger.isInfoEnabled()) {
	    	String ls = System.getProperty("line.separator");
	    	logger.info("Going to insert new risk alert: " + ls +
	    			"typeId: " + typeId + ls +
	    			"trxId: " + trxId + ls +
	    			"invId: " + invId);
    	}
        Connection con = getConnection();
        try {
        	RiskAlertsDAOBase.insert(con, new RiskAlert(typeId, writerId, trxId, invId));
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Handle Deposit alerts
     * @param writerId
     * @param error
     * @param tran
     * @param user
     * @param riskAlert
     * @param errorCode
     * @throws SQLException
     * @throws CryptoException
     * @throws NumberFormatException
     * @throws UnsupportedEncodingException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws InvalidAlgorithmParameterException 
     */
	public static void riskAlertDepositHandler(	long writerId, String error, Transaction tran, UserBase user, boolean riskAlert,
												String errorCode, String ccHolderName, boolean firstDeposit, boolean isDocumentsSent,
												long ccNumber)	throws SQLException, NumberFormatException, CryptoException,
																InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
																IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {

    	Connection con = null;
    	//Duplicate card
    	if (error != null && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)){
    		insert(RISK_TYPE_CC_USED_ON_WEBSITE, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}
    	//Stolen card
    	if (isStolenCard(errorCode, tran)){
			insert(RISK_TYPE_CC_STOLEN, writerId, tran.getId(), NO_INVESTMENT_ID);
		}
    	//CC deposit more than allowed
    	if(error != null && error.equals(ConstantsBase.ERROR_MAX_DEPOSIT)){
    		insert(RISK_TYPE_CC_DEPOSIT_MORE_THAN_ALLOWED, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}

    	//Mismatch card holder
    	if (error == null && !CommonUtil.isParameterEmptyOrNull(ccHolderName) && firstDeposit == true){
    		String userName = user.getFirstName() + " " + user.getLastName();
    		if(!userName.equalsIgnoreCase(ccHolderName)){
    			insert(RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    	}
    	
    	//Velocity Checks
    	if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_AO_NO_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_AO_HAVE_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_ET_NO_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	} else if (error != null && error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC)){
    		insert(RISK_TYPE_DEPOSIT_VELOCITY_ET_HAVE_DOC, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}
    		
    	con = getConnection();

    	//Docs required
    	if (!isDocumentsSent){
    		String enumerator = null;
    		enumerator = CommonUtil.getEnum(ConstantsBase.RISK_ALERT_ENUMERATOR, ConstantsBase.DEPOSIT_LIMIT_NO_DOC_CODE + user.getCurrency().getCode().toLowerCase());    		
    		//Deposit greater than X and no document
    		if (tran.getAmount()> 0 && tran.getAmount() > Long.valueOf(enumerator)){
    			insert(RISK_TYPE_DEPOSIT_GREATER_THAN_X_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    		//X deposit and no document
    		enumerator = CommonUtil.getEnum(ConstantsBase.RISK_ALERT_ENUMERATOR,ConstantsBase.X_DEPOSIT_NO_DOCUMNET_CODE);
    		if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) >= Long.valueOf(enumerator) ) {
                if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) == Long.valueOf(enumerator)) {
                    insert(RISK_TYPE_X_DEPOSIT_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID);
                }
                if (!IssuesManagerBase.isExistsIssueByUserAndCC(ccNumber, user.getId(), FIVE_DOC_SUBJECT)) {
                    int resID[] = IssuesManagerBase.getExistsIssueByUserAndCC(tran.getCreditCardId().longValue(), user.getId());
                    
                    Issue issue = new Issue();
                    issue.setUserId(user.getId());
                    issue.setStatusId(Integer.toString(FIVE_DOC_STATUS));
                    issue.setPriorityId(Integer.toString(FIVE_DOC_PRIORITY));
                    issue.setSubjectId(Integer.toString(FIVE_DOC_SUBJECT));
                    issue.setCreditCardId(tran.getCreditCardId().longValue());
                    issue.setType(FIVE_DOC_ISSUE_TYPE);
                    
                    IssueAction issueAction = new IssueAction();
                    
                    issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT));
                    issueAction.setSignificant(false);
                    issueAction.setComments("Please ask for docs");
                    issueAction.setRiskReasonId(FIVE_DOC_ISSUE);
       
                    if (resID[0] == 0) {                        
                        IssuesManagerBase.insertIssue(issue, issueAction, user.getUtcOffset(), writerId, user, 0);
                    } else {
                        issue.setId(Long.valueOf(resID[0]));
                        IssuesManagerBase.insertAction(issue, issueAction, user.getUtcOffset(), writerId, user, 0);
                        IssuesManagerBase.updateIssueState(IssuesManagerBase.RISK_STATUS_NEW_ISSUE, issueAction.getActionId(), issue.getId());
                    }
                    if (resID[1] != 1 && resID[3] != 1) {                        
                        IssuesManagerBase.insertDocRequest(issueAction.getActionId(), 0, ConstantsBase.RISK_DOC_REQ_ID, user.getId());
                    }
                    if (resID[1] != 2 && resID[3] != 1) {                        
                        IssuesManagerBase.insertDocRequest(issueAction.getActionId(), tran.getCreditCardId().longValue(), ConstantsBase.RISK_DOC_REQ_CC, user.getId());
                    }
                    IssuesManagerBase.updateDocState(user.getId(), tran.getCreditCardId().longValue(), IssuesManagerBase.FILES_RISK_STATUS_NEW_ISSUE);
                }
                if (RiskAlertsDAOBase.getNumberOfDeposit(con, user.getId()) == Long.valueOf(RISK_TYPE_X_DEPOSIT_NO_DOCUMENT)) {
                    insert(RISK_TYPE_XX_DEPOSIT_NO_DOCUMENT, writerId, tran.getId(), NO_INVESTMENT_ID); 
                } 
            }
    	}

    	//User balance greater than minimum investment
    	if ((error == null) && (tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING
    			|| tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED)) {

    		long amount = InvestmentsDAOBase.getUserMinInvestmentLimit(con, user.getCurrencyId());
    		if ((user.getBalance()- tran.getAmount()) > amount){ //Remove current amount from balance
    			insert(RISK_TYPE_DEPOSIT_BALANCE_GREATER_THAN_MIN_INVEST, writerId, tran.getId(), NO_INVESTMENT_ID);
    		}
    	}
    	
    	//Deposit with credit card from country different than the registration country
    	if (error == null && isDifferentRegCountryThanCcCountry(user, tran, ccNumber)) {
    		insert(RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY, writerId, tran.getId(), NO_INVESTMENT_ID);
    	}
    	
    	//Successful deposit and the registration country is different than the IP country
    	if(error == null && isDifferentRegCountryThanIpCountry(user)) {
			insert(RISK_TYPE_REGISTRATION_FROM_COUNTRY_DIFF_THAN_IP_COUNTRY, writerId, tran.getId(), NO_INVESTMENT_ID);
		}
    }

    /**
     * Handle Withdrawal alerts
     * @param writerId
     * @param error
     * @param trxId
     * @param amount
     * @param userId
     * @param balance
     * @throws SQLException
     */
    public static void riskAlertWithdrawalHandler(long writerId, String error, long trxId, long amount, long userId,long balance) throws SQLException{
		Connection con = getConnection();
		try {
			//Withdraw amount greater than the balance
	    	if (error != null && error.equals("error.transaction.bankWire.highamount")){
	    			insert(RISK_TYPE_WIRE_WITHDRAW_AMOUNT_GREATER_THAN_BALANCE, writerId, trxId, NO_INVESTMENT_ID);
	    	}
	    	//Wagering Failed
	    	else if (error != null && error.equals("bonus.cannot.withdraw.error") ){
	    			insert(RISK_TYPE_WAGERING_FAILED_WITHDRAW, writerId, trxId, NO_INVESTMENT_ID);
	    	}
	    	//Withdraw with no investment
	    	else if (CommonUtil.isParameterEmptyOrNull(error) && !InvestmentsDAOBase.hasInvestments(con, userId, false) ){
	    			insert(RISK_TYPE_WITHDRAW_NO_INVESTMENTS, writerId, trxId, NO_INVESTMENT_ID);
	    	}

		} finally {
			closeConnection(con);
		}
    }

    /**
     * Handle Investments alerts
     * @param writerId
     * @param userId
     * @param invId
     * @throws SQLException
     */
    public static void riskAlertInvestmentHandler(long writerId, long userId, long invId) throws SQLException {
		Connection con = getConnection();
		try {
			//First investment of the user is 1T
	    	if (invId != 0 && !InvestmentsDAOBase.hasInvestments(con, userId, true)) {
	    		insert(RISK_TYPE_FIRST_INVESTMENT_IS_1T, writerId, NO_TRANSACTION_ID, invId);
	    	}
		} finally {
			closeConnection(con);
		}
    }


    /**
     * Check if card is stolen
     * @param errorCode
     * @param tran
     * @return
     */
    public static boolean isStolenCard(String errorCode,Transaction tran){
		if ( (tran.getClearingProviderId() == ClearingManager.XOR_ET_PROVIDER_ID ||
				tran.getClearingProviderId() == ClearingManager.XOR_AO_PROVIDER_ID) &&
				(errorCode.equals("001") || errorCode.equals("002") || errorCode.equals("005"))) {
			return true;
		}

		if(isCreditCardStolen(errorCode, tran)){
			return true;
		}
		
		return false;
    }


    public static void createIssueForRegulation(UserBase user, boolean isMainRegulationIssue)
			throws SQLException {
    	Connection con = null;
    	try {
		 con = getConnection();
		createIssueForRegulation(con, user, isMainRegulationIssue);
    	}finally {
    		closeConnection(con);
    	}
	}
    
    public static void createIssueForEmailChanged(UserBase user, String oldEmail, String eWriterName)
			throws SQLException {
    	Connection con = null;
    	try {
		 con = getConnection();
		 createIssueForEmailChanged(con, user, oldEmail, eWriterName);
    	}finally {
    		closeConnection(con);
    	}
	}    
   
	public static void createIssueForRegulation(Connection conn, UserBase user, boolean isMainRegulationIssue) {
		try {

		    UserRegulationBase userRegulation = new UserRegulationBase();
		    userRegulation.setUserId(user.getId());
		    UsersRegulationDAOBase.getUserRegulation(conn, userRegulation);
		    int regulationStep = userRegulation.getApprovedRegulationStep();
			
		    Issue issue = null;
		    IssueAction issueAction = null;
			issue = new Issue();
			issue.setUserId(user.getId());
			issue.setStatusId(Integer.toString(IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED));
			issue.setPriorityId(Integer.toString(IssuesManagerBase.ISSUE_PRIORITY_LOW));
			issue.setSubjectId(Integer.toString(IssuesManagerBase.ISSUE_SUBJECT_REG_DOC));
			issue.setMainRegulationIssue(isMainRegulationIssue);
	
			issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_PENDING_DOC));
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
			issueAction.setSignificant(false);
			if(regulationStep<UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE) {
				issueAction.setComments("Mandatory questionary not done yet");
			} else {
				issueAction.setComments("Please ask for docs");
			}
			
			issueAction.setRiskReasonId(Integer.toString(IssuesManagerBase.ISSUE_REASON_TYPE_REG_DOC));
			IssuesManagerBase.insertIssue(issue, issueAction, ConstantsBase.OFFSET_GMT, Writer.WRITER_ID_WEB, user, 0, conn);
		   
      
			logger.info(" Successfully insert regulation document need issue for user id: "
					+ user.getId());
		} catch (Exception e) {
			logger.error(
					"couldn't insert regulation document need issue for user id: "
							+ user.getId(), e);
		}
	}
	
	private static void createIssueForEmailChanged(Connection conn, UserBase user, String oldEmail, String eWriterName) {
		try {
			
		    Issue issue = null;
		    IssueAction issueAction = null;
			issue = new Issue();
			issue.setUserId(user.getId());
			issue.setStatusId(Integer.toString(IssuesManagerBase.ISSUE_STATUS_FINISHED));
			issue.setPriorityId(Integer.toString(IssuesManagerBase.ISSUE_PRIORITY_LOW));
			issue.setSubjectId(Integer.toString(IssuesManagerBase.ISSUE_SUBJECT_EMAIL_CHANGED));
	
			issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL_CHANGED));
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
			issueAction.setSignificant(true);
			issueAction.setComments("Email was changed from " + oldEmail + " to " + user.getEmail() + " by " + eWriterName);
			
			//issueAction.setRiskReasonId(Integer.toString(IssuesManagerBase.ISSUE_REASON_TYPE_REG_DOC));
			IssuesManagerBase.insertIssue(issue, issueAction,ConstantsBase.OFFSET_GMT, Writer.WRITER_ID_WEB,user, 0, conn);
		   
      
			logger.info("Successfully insert email changed issue for user id: " + user.getId());
		} catch (Exception e) {
			logger.error(
					"couldn't insert email changed issue for user id: " + user.getId(), e);
		}
	}
	
	/**
	 * Check if credit card country is different than the registration country of the user
	 * @param user
	 * @param tran
	 * @param ccNumber
	 * @return
	 * @throws SQLException
	 */
    private static boolean isDifferentRegCountryThanCcCountry(UserBase user, Transaction tran, long ccNumber) throws SQLException {
    	Connection con = null;
    	boolean retVal = false;
    	try {
    		con = getConnection();
    		long ccId = tran.getCreditCardId().longValue();
    		retVal = RiskAlertsDAOBase.isDifferentRegCountryThanCcCountry(con, user.getId(), user.getCountryId(), ccId, ccNumber);
    	} finally {
    		closeConnection(con);
    	}
		return retVal;
	}
    
    /**
     * Check if registration country is different than the IP country
     * @param user
     * @return
     */
    private static boolean isDifferentRegCountryThanIpCountry(UserBase user){
		String userIp = user.getIp();
		
		// handle omitted ip's
		String ommittedIps = CommonUtil.getProperty("security.check.omitted.ip", "");
		String[] omittedIpsArr = ommittedIps.split(";");
		for (String ip : omittedIpsArr) {
			if(userIp.equals(ip)) {
				return false;
			}
		}
		
		// handle registration country vs ip country
		String userCountryCode = user.getCountry().getA2();
		String ipCountryCode = CommonUtil.getCountryCodeByIp(userIp);
		if(!userCountryCode.equals(ipCountryCode)) {
			return true;
		}
		return false;
	}
    
	public static void riskAlertDepositHandlerCCFromDiffCountry(Connection conn, Transaction tran){
		try {
			if(RiskAlertsDAOBase.isCCFromDiffCountries(conn, tran.getCreditCardId().longValue(), tran.getId())){
				insert(RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES, tran.getWriterId(), tran.getId(), NO_INVESTMENT_ID);
			}
		} catch (Exception e) {
			logger.error("When riskAlertDepositHandlerCCFromDiffCountry", e);
		}
	}
}
