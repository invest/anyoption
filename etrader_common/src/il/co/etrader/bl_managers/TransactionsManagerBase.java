package il.co.etrader.bl_managers;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Enumerator;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.PaymentDeposit;
import com.anyoption.common.beans.PaymentMethod;
import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BaroPayRequest;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.ACHDepositBase;
import com.anyoption.common.bl_vos.CDPayDeposit;
import com.anyoption.common.bl_vos.ClearingLimitaion;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.bl_vos.TransactionBin;
import com.anyoption.common.bl_vos.UkashDeposit;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.clearing.ACHInfo;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.CashUInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.clearing.EPGInfo;
import com.anyoption.common.clearing.InatecClearingProvider;
import com.anyoption.common.clearing.InatecIframeInfo;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.clearing.MoneybookersClearingProvider;
import com.anyoption.common.clearing.MoneybookersInfo;
import com.anyoption.common.clearing.Powerpay21ClearingProvider;
import com.anyoption.common.clearing.UkashInfo;
import com.anyoption.common.daos.BaroPayDAOBase;
import com.anyoption.common.daos.CDPayDepositDAO;
import com.anyoption.common.daos.CreditCardsDAOBase;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.QuestionnaireDAOBase;
import com.anyoption.common.daos.UkashDepositDAO;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.SendStolenCCDepositEmail;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersAutoMailDaoManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.payments.FinalizeDeposit;
import com.anyoption.common.payments.epg.EpgDepositRequestDetails;
import com.anyoption.common.util.AppsflyerEventSender;
import com.anyoption.common.util.Sha1;
import com.copyop.common.dao.ACHDepositDAOBase;

import il.co.etrader.bl_vos.Cheque;
import il.co.etrader.bl_vos.ErrorCode;
import il.co.etrader.bl_vos.FireServerPixelFields;
import il.co.etrader.bl_vos.FireServerPixelHelper;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.ChequeDAO;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.DistributionDAO;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.TiersDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.distribution.Distribution;
import il.co.etrader.distribution.DistributionMaker;
import il.co.etrader.distribution.DistributionType;
import il.co.etrader.distribution.HundredPercentSelector;
import il.co.etrader.distribution.NewProviderSelector;
import il.co.etrader.distribution.PercentageDistributionSelector;
import il.co.etrader.distribution.Selector;
import il.co.etrader.distribution.SelectorException;
import il.co.etrader.distribution.SelectorId;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendDepositErrorEmail;
import il.co.etrader.util.SendTemplateEmail;

public abstract class TransactionsManagerBase extends com.anyoption.common.managers.TransactionsManagerBase {
	private static final Logger log = Logger.getLogger(TransactionsManagerBase.class);

	public static Cheque getCheque(BigDecimal id) throws SQLException {
		Connection con = getConnection();
		try {
			return ChequeDAO.getById(con, id.longValue());
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SelectItem> getDisplayCreditCardsByUser(long id) throws SQLException {
		Connection con = getConnection();
		try {
			ArrayList<CreditCard> ccList = CreditCardsDAO.getVisibleByUserId(con, id);
			ArrayList<SelectItem> ccListOut = new ArrayList<SelectItem>();
			boolean userSkinRegulated = CommonUtil.isUserSkinRegulated();
			for (int i = 0; i < ccList.size(); i++) {
			    CreditCard cc = (CreditCard) ccList.get(i);
			    if (userSkinRegulated && cc.getTypeIdByBin() == CC_TYPE_MAESTRO) {
			    	continue;
			    }
		        int l = String.valueOf(cc.getCcNumber()).length();
                ccListOut.add(new SelectItem(String.valueOf(cc.getId()),String.valueOf(cc.getCcNumber()).substring(l - 4, l) + "  " + cc.getType().getName()));				
			}
			return ccListOut;
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SelectItem> getAllCreditCardsByUserList(long id) throws SQLException {
		Connection con = getConnection();
		try {
			ArrayList ccList = CreditCardsDAO.getByUserId(con, id);
			ArrayList<SelectItem> ccListOut = new ArrayList<SelectItem>();
			for (int i = 0; i < ccList.size(); i++) {
				CreditCard cc = (CreditCard) ccList.get(i);
				int l = String.valueOf(cc.getCcNumber()).length();
				ccListOut.add(new SelectItem(String.valueOf(cc.getId()),String.valueOf(cc.getCcNumber()).substring(l - 4, l) + "  " + cc.getType().getName()));
			}
			return ccListOut;
		} finally {
			closeConnection(con);
		}
	}	
	
	public static HashMap<Long,  HashMap<String, String>> getAllCreditCardsByUserHM(long id) throws SQLException {
		Connection con = getConnection();
		try {
			ArrayList<CreditCard> ccList = CreditCardsDAO.getByUserId(con, id);
			HashMap<Long,  HashMap<String, String>> ccHMOut = new HashMap<Long,  HashMap<String, String>>();
			for (int i = 0; i < ccList.size(); i++) {
				CreditCard cc = (CreditCard) ccList.get(i);
				int l = String.valueOf(cc.getCcNumber()).length();
				HashMap<String, String> ccDetails = new HashMap<>();
				ccDetails.put(String.valueOf(cc.getCcNumber()).substring(l - 4, l) + "  " + cc.getType().getName(), cc.getExpDateStr());
				ccHMOut.put(cc.getId(), ccDetails);
			}
			return ccHMOut;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Return all visa Credit cards of some user, credit cards that the user make some
	 * deposit from them and the deposit is approved.
	 * @param id user id
	 * @return	Array List of credit cards
	 * @throws SQLException
	 */
//	public static ArrayList<CreditCard> getDisplayWithrawalCreditCardsByUser(long id, long minWithdrawal) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CreditCardsDAO.getCcWithrawalByUserId(con, id, minWithdrawal);
//		} finally {
//			closeConnection(con);
//		}
//	}

	/**
	 * Return all visa Credit cards of some user, credit cards that the user make some
	 * deposit from them and the deposit is approved.
	 * @param id user id
	 * @return	Array List of credit cards
	 * @throws SQLException
	 */
	public static HashMap<Long,CreditCard> getDisplayWithrawalCreditCardsByUser(long id, long minWithdrawal, long skinId, boolean onlyVisibleCards) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getCcWithrawalByUserId(con, id, minWithdrawal, skinId, onlyVisibleCards);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get display list of credit cards
	 * @param list creditCards list for withdrawals
	 * @param c user currency
	 * @return
	 */
	public static ArrayList<SelectItem> getDisplayWithrawalCreditCardsByUser(HashMap<Long,CreditCard> ccHash, Currency c, int app) {

		ArrayList<SelectItem> ccListOut = new ArrayList<SelectItem>();
		for (Iterator<Long> iter = ccHash.keySet().iterator(); iter.hasNext();) {
			Long cardId = iter.next();
			CreditCard cc = ccHash.get(cardId);
			int l = String.valueOf(cc.getCcNumber()).length();
			String lable = String.valueOf(cc.getCcNumber()).substring(l - 4, l) + "  " + cc.getType().getName();
			if ( app == ConstantsBase.BACKEND_CONST ) {
				if (cc.isCreditEnabled()) {
					String[] params = new String[1];
			        params[0] = CommonUtil.displayAmount(cc.getCreditAmount(), c);
			        lable += ",  " + CommonUtil.getMessage("withdrawals.credit.selection", params) + " ";
				}
			}
			ccListOut.add(new SelectItem(String.valueOf(cc.getId()), lable));
		}
		return ccListOut;
	}




	public static boolean isStillPendingWithdraw(long transactionId) throws SQLException {
		Connection con = getConnection();
		try {
			String statuses = String.valueOf(TransactionsManagerBase.TRANS_STATUS_REQUESTED) + "," +
		  						String.valueOf(TransactionsManagerBase.TRANS_STATUS_APPROVED) + "," +
		  						String.valueOf(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);

			ArrayList<Transaction> reqTran = TransactionsDAOBase.getByUserAndStatus(con, 0, transactionId,
					TransactionsManagerBase.TRANS_CLASS_WITHDRAW, statuses, TransactionsDAOBase.ORDER_ASC);

			return (null != reqTran && reqTran.size() > 0);
		}catch (Exception e) {
			log.error("Error in isStillPendingWithdraw for transaction id:" + transactionId, e);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	public static Transaction getTransaction(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Validate points to cash money action
	 * @param formName from name
	 * @param points selected points
	 * @param user user instance
	 * @return
	 */
	public static String validatePointsToCashAction(String formName, long points, UserBase user) {
		FacesContext context = FacesContext.getCurrentInstance();
		long userPoints = user.getTierUser().getPoints();
		Tier t = TiersManagerBase.getTierById(user.getTierUser().getTierId());
		String errorMsg = null;
		if (points > userPoints || points < t.getConvertMinPoints()) {
			if (points > userPoints) {
				errorMsg = "points.to.cash.error.max";
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(errorMsg, null),null);
				context.addMessage(formName + ":amountPoints", fm);
			} else {   // min error
				errorMsg = "points.to.cash.error.min";
				String args[] = new String[1];
				args[0] = String.valueOf(t.getConvertMinPoints());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(errorMsg, args),null);
				context.addMessage(formName + ":amountPoints", fm);
			}
		}
		return errorMsg;
	}

	private static String validateDepositForm(String formName, String depositVal, CreditCard cc, UserBase user, int source, boolean isCheckMinAmount) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		long deposit = CommonUtil.calcAmount(depositVal);
		Connection con = null;
		try {
			
			con = getConnection();
			if(TransactionsDAOBase.isDisableCcDeposits(con, user.getId())){
				log.debug("Disable All Credit Card deposits for userID:" + user.getId());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.notallowed", null),null);
				context.addMessage(null, fm);
				return ConstantsBase.ERROR_DISABLED_CC_DEPOSIT;
			}
			
			if (!cc.isAllowed()) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.notallowed", null),null);
				context.addMessage(null, fm);

				return ConstantsBase.ERROR_CARD_NOT_ALLOWED;
			}
			
			return validateDepositLimits(con, formName, user, deposit, source, isCheckMinAmount);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Deposit validation
	 * @param con db connection
	 * @param formName name of the form
	 * @param user user that performed the action
	 * @param deposit amount of the deposit
	 * @param source application source
	 * @param isCheckMinAmount TODO
	 * @return
	 * @throws SQLException
	 */
	protected static String validateDepositLimits(Connection con, String formName, UserBase user, long deposit, int notUsed, boolean isCheckMinAmount) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Limit limit = LimitsDAO.getById(con, user.getLimitIdLongValue());
        LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(con, user.getId());
        limit.setMinDeposit(ld.getMinimumDeposit());
        limit.setMinFirstDeposit(ld.getMinimumFirstDeposit());
		boolean isFirstDeposit = isFirstDeposit(user.getId(), 0);
		if (isFirstDeposit) {
			limit.setMinDeposit(limit.getMinFirstDeposit());
		}

		if ((deposit < Long.valueOf(limit.getMinDeposit()).longValue() && isCheckMinAmount)
				|| deposit > Long.valueOf(limit.getMaxDeposit()).longValue()) {

			String[] params = new String[1];

			if (deposit < Long.valueOf(limit.getMinDeposit()).longValue()) {
				if (user.getSkinId() != Skin.SKIN_ARABIC){
		        	params[0] = CommonUtil.displayAmount(limit.getMinDeposit(), user.getCurrencyId());
				}else{
					params[0] = CommonUtil.displayAmountForInput(limit.getMinDeposit(), false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
				}
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.mindeposit", params),null);
				context.addMessage(formName + ":deposit", fm);
//				if (source == ConstantsBase.WEB_CONST) {
//					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.mindeposit2", null),null);
//					context.addMessage(formName + ":depositlabelid", fm);
//				}
				return ConstantsBase.ERROR_MIN_DEPOSIT;
			} else {
				if (user.getSkinId() != Skin.SKIN_ARABIC){
		        	params[0] = CommonUtil.displayAmount(limit.getMaxDeposit(), user.getCurrencyId());
				}else{
					params[0] = CommonUtil.displayAmountForInput(limit.getMaxDeposit(), false, user.getCurrencyId().intValue()) + " " + CommonUtil.getMessage(user.getCurrency().getSymbol(),null) + " ";
				}
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.maxdeposit", params),null);
				context.addMessage(formName + ":deposit", fm);
				return ConstantsBase.ERROR_MAX_DEPOSIT;
			}
		}

		long totalToday =
				TransactionsDAOBase.getSummaryByDates(con, user.getId(), TRANS_CLASS_DEPOSIT, String
						.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," + String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING), new Date(), new Date());

		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.DATE, 1);

		long totalMonth =
				TransactionsDAOBase.getSummaryByDates(con, user.getId(), TRANS_CLASS_DEPOSIT, String
						.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," + String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING), gc.getTime(), new Date());

		if (totalToday + deposit > Long.valueOf(limit.getMaxDepositPerDay()).longValue()) {
			String[] params = new String[1];
			params[0] = limit.getMaxDepositPerDayTxt(user.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.deposit.maxday", params),null);
			context.addMessage(formName + ":deposit", fm);
			return ConstantsBase.ERROR_MAX_DAILY_DEPOSIT;
		}
		if (totalMonth + deposit > Long.valueOf(limit.getMaxDepositPerMonth()).longValue()) {
			String[] params = new String[1];
			params[0] = limit.getMaxDepositPerMonthTxt(user.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.deposit.maxmonth", params),null);
			context.addMessage(formName + ":deposit", fm);
			return ConstantsBase.ERROR_MAX_MONTHLY_DEPOSIT;
		}

		return null;

	}

	public static Transaction insertPowerpay21Deposit(String formName, String hpUrl, String depositVal, long writerId, UserBase user,
			int source, boolean firstDeposit, String bankCode, String iban, long paymentTypeId, String beneficiaryName, long loginId) throws Exception {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert Powerpay21 deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Connection con = null;
		
		try{
			String error = null ;
			Transaction tran = null;
	
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
	
			error = validateDepositLimits(con, formName, user, deposit, source, true);
	
			tran = new Transaction();
			tran.setAmount(deposit);
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setPaymentTypeId(paymentTypeId);
			tran.setLoginId(loginId);
	
			WireBase wire = new WireBase();
			wire.setIban(iban);
			wire.setBankCode(bankCode);
			wire.setBeneficiaryName(beneficiaryName);
			WiresDAOBase.insertBankWire(con, wire);
	
			tran.setWireId(new BigDecimal(wire.getId()));
			TransactionsDAOBase.insert(con, tran);
	
			String ls = System.getProperty("line.separator");
			log.info("Transaction record inserted: " + ls + tran.toString() + ls);
	
			if (null == error) {  // start clearing
				try {
					Powerpay21ClearingProvider powerpay21 =  (Powerpay21ClearingProvider) ClearingManager.getClearingProviders().get(ClearingManager.POWERPAY21_PROVIDER_ID_APS);
					ClearingInfo result = null;
					if(paymentTypeId == 4) { // Eps
						result = powerpay21.directEpsDeposit(hpUrl, deposit, beneficiaryName, tran.getId(), user.getEmail());
					} else if(paymentTypeId ==  3) { // Giropay
						result = powerpay21.directGiropayDeposit(hpUrl, deposit, iban, bankCode, beneficiaryName, tran.getId(), user.getEmail());
					} else if(paymentTypeId == 2) {  // Sofort
						result = powerpay21.directSofortDeposit(hpUrl, deposit, iban, bankCode, beneficiaryName, tran.getId(), user.getEmail());
					} else {
						result = new ClearingInfo();
						tran.setStatusId(TRANS_STATUS_FAILED);
						tran.setComments("Unknown paymentTypeId:" + paymentTypeId);
						log.error("Unknown paymentTypeId:" + paymentTypeId);
					}
	
					String redirectUrl = "";
					if(result.isSuccessful()) {
						tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
						redirectUrl = URLDecoder.decode(result.getAcsUrl(), "UTF-8");
						log.info("Redirecting url is:" + redirectUrl );
					} else {
						tran.setStatusId(TRANS_STATUS_FAILED);
						log.info("No Redirecting url trx failed.");
					}
					tran.setClearingProviderId(ClearingManager.POWERPAY21_PROVIDER_ID_APS);
					tran.setRedirectUrl(redirectUrl);
					tran.setXorIdAuthorize(result.getProviderTransactionId());
					tran.setComments(result.getMessage());
				} catch (ClearingException ce) {
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(ce.getMessage());
					log.error("Error! problem with Powerpay21, transaction: " + tran.getId(), ce);
				}
	            
			} else {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
			}
			
			updateTransaction(tran);
		
			
			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);

		if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
			String successMsg = "deposit.success";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params),null);
			FacesContext.getCurrentInstance().addMessage(null, fm);

			try {
				PopulationsManagerBase.deposit(user.getId(), true, writerId,tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

            try {
                afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
            } catch (Exception e) {
                log.error("Problem processing bonuses after success transaction", e);
            }
            try {
                CommonUtil.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
        } else { // deposit failed
        	
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
				FacesContext.getCurrentInstance().addMessage(null, fm);
				
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}

				if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {

					String email;
					if (!CommonUtil.isParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
						email = ApplicationDataBase.getSupportEmailBySkinId(user.getSkinId());
					} else {
						if (user.getSkinId() == Skin.SKIN_CHINESE){
							email = CommonUtil.getProperty("deposit.error.zh.email", null);
						}else{
							email = CommonUtil.getProperty("deposit.error.email", null);
						}
					}

					String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " " +
									 	CommonUtil.getMessage("transactions.direct.banking.deposit", null) + ": " +
									 	InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());

					// send mail to support
					// collect the parameters we need for the email
					HashMap<String, String> params = null;
					params = new HashMap<String, String>();
		            params.put(SendDepositErrorEmail.PARAM_EMAIL, email);
					params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
					params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
					params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId()));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());

					// Populate also server related params, in order to take out common functionality
					// Use params
					params.put(SendDepositErrorEmail.MAIL_TO, email);
					params.put(SendDepositErrorEmail.MAIL_SUBJECT , subject + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );

					new SendDepositErrorEmail(params).start();
				}
		}
			
		return tran;
		
		} finally {
			closeConnection(con);
		}
	}
	
	public static Transaction insertNetellerDeposit(String formName, String depositVal, long writerId, UserBase user,
			boolean firstDeposit, String email, String verificationCode, long loginId) throws Exception {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert Neteller deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Connection con = null;
		
		try{
			String error = null ;
			Transaction tran = null;
	
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
	
			error = validateDepositLimits(con, formName, user, deposit, 0, true);
	
			tran = new Transaction();
			tran.setAmount(deposit);
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_NETELLER_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setLoginId(loginId);
	
			TransactionsDAOBase.insert(con, tran);
	
			String ls = System.getProperty("line.separator");
			log.info("Transaction record inserted: " + ls + tran.toString() + ls);
	
			if (null == error) {  // start clearing
				try {
					ClearingInfo result = new ClearingInfo();
					result.setAmount(tran.getAmount());
					result.setTransactionId(tran.getId());
					
					ClearingManager.netellerDeposit(result, user.getCurrency().getCode(), email, verificationCode);
	
					if (result.isSuccessful()) {   // success
						if (log.isInfoEnabled()) {
							log.info("Transaction successful from Neteller");
						}
						tran.setStatusId(TRANS_STATUS_SUCCEED);
						tran.setTimeSettled(new Date());
						tran.setUtcOffsetSettled(user.getUtcOffset());
						try {
							con.setAutoCommit(false);
							UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(depositVal));
							GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS,
														tran.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, user.getUtcOffset());
							TransactionsDAOBase.updateForStatus(con, tran, TRANS_STATUS_STARTED);
							con.commit();
						} catch (Exception e) {
							log.error("Exception in success neteller Transaction! ", e);
							try {
								con.rollback();
							} catch (Throwable it) {
								log.error("Can't rollback.", it);
							}
						} finally {
							try {
								con.setAutoCommit(true);
							} catch (Exception e) {
								log.error("Can't set back to autocommit.", e);
							}
						}
					} else {  // failed
						if (log.isInfoEnabled()) {
							log.info("Transaction failed InatecCheckout! " + result.toString() + ls);
						}
						tran.setStatusId(TRANS_STATUS_FAILED);
					}
					
					tran.setClearingProviderId(result.getProviderId());
					tran.setXorIdAuthorize(result.getProviderTransactionId());
					tran.setComments(result.getMessage());
				} catch (ClearingException ce) {
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(ce.getMessage());
					log.error("Error! problem with Powerpay21, transaction: " + tran.getId(), ce);
				}
	            
			} else {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
			}
			
			updateTransaction(tran);
			
			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);

		if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
			String successMsg = "deposit.success";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params),null);
			FacesContext.getCurrentInstance().addMessage(null, fm);

			try {
				PopulationsManagerBase.deposit(user.getId(), true, writerId,tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

            try {
                afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
            } catch (Exception e) {
                log.error("Problem processing bonuses after success transaction", e);
            }
            try {
                CommonUtil.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
        } else { // deposit failed
        	
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
				FacesContext.getCurrentInstance().addMessage(null, fm);
				
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}

				if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {

					String supportEmail;
					if (!CommonUtil.isParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
						supportEmail = ApplicationDataBase.getSupportEmailBySkinId(user.getSkinId());
					} else {
						if (user.getSkinId() == Skin.SKIN_CHINESE){
							supportEmail = CommonUtil.getProperty("deposit.error.zh.email", null);
						}else{
							supportEmail = CommonUtil.getProperty("deposit.error.email", null);
						}
					}

					String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " " +
									 	CommonUtil.getMessage("transactions.direct.banking.deposit", null) + ": " +
									 	InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());

					// send mail to support
					// collect the parameters we need for the email
					HashMap<String, String> params = null;
					params = new HashMap<String, String>();
		            params.put(SendDepositErrorEmail.PARAM_EMAIL, supportEmail);
					params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
					params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
					params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId()));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());

					// Populate also server related params, in order to take out common functionality
					// Use params
					params.put(SendDepositErrorEmail.MAIL_TO, supportEmail);
					params.put(SendDepositErrorEmail.MAIL_SUBJECT , subject + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );

					new SendDepositErrorEmail(params).start();
				}
		}
			
		return tran;
		
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Direct banking deposit with Inatec service
	 * @throws SQLException
	 */
	public static Transaction insertDirectDeposit(String error, String formName, String depositVal, long writerId, UserBase user,
			int source, boolean firstDeposit, String bankCode, String accountNum, long paymentTypeId, String beneficiaryName, String homePageUrl, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls +
                    "error: " + error + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;
		WireBase wire = null;    // saving bank properties
		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			if (error == null) {
				error = validateDepositLimits(con, formName, user, deposit, source, true);
			}

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setPaymentTypeId(paymentTypeId);
			tran.setLoginId(loginId);

			wire = new WireBase();
			wire.setAccountNum(accountNum);
			wire.setBankCode(bankCode);
			wire.setBeneficiaryName(beneficiaryName);
			WiresDAOBase.insertBankWire(con, wire);

			tran.setWireId(new BigDecimal(wire.getId()));
			TransactionsDAOBase.insert(con, tran);

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}

			boolean timeout = false;

			try {
				if (null == error) {  // start clearing

					InatecInfo ci = new InatecInfo(user, tran, wire);
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Clearing started: Clearing Info: " + ci.toString() + ls);
					}

					long providerId = ClearingManager.getProvidersIdByPaymentGroupAndBusinessCase().get(user.getSkin().getBusinessCaseId()).get(ClearingManager.CLEARING_PAYMENT_GROUP_INATEC_ONLINE);
					try {
						ci.setHomePageUrl(homePageUrl);
						ClearingManager.directDeposit(ci, providerId);
					} catch (ClearingException ce) {
						if (ce.getCause() instanceof SocketTimeoutException) {
							timeout = true;
						}
						log.error("Error! problem with directDeposit, transaction: " + ci.toString(), ce);
						ci.setResult("999");
						ci.setSuccessful(false);
						ci.setMessage(ce.getMessage());
					}
		            tran.setClearingProviderId(ci.getProviderId());
					tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|" + ci.getProviderTransactionId());

					tran.setXorIdAuthorize(ci.getProviderTransactionId());
					tran.setAuthNumber(ci.getAuthNumber());

					if (ci.isSuccessful()) {   // success
						if (log.isInfoEnabled()) {
							log.info("Transaction successful from Inatec");
						}
						if (null != ci.getRedirectUrl()) {   // authenticate status(inatec status was pending), bank redirect needed
	                        tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
	                        tran.setInatecRedirectSecret(ci.getRedirectSecret());
	                        tran.setInatecAccountNum(accountNum);
	                        tran.setInatecBankSortCode(bankCode);
	                        tran.setBeneficiaryName(beneficiaryName);
	                        tran.setTimeSettled(null);
                            ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_INATEC_INFO, InatecInfo.class);
                            ve.getValue(context.getELContext());
                            ve.setValue(context.getELContext(), ci); // for saving redirectUrl
	                        TransactionsDAOBase.update(con, tran);
						} else {  // without bank redirect
	                        tran.setStatusId(TRANS_STATUS_SUCCEED);
	                        tran.setTimeSettled(new Date());
	    					tran.setUtcOffsetSettled(user.getUtcOffset());
	                        try {
	                    		con.setAutoCommit(false);
								UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(depositVal));
								GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, user.getUtcOffset());
	                    		TransactionsDAOBase.update(con, tran);
	                    		con.commit();
	                    	} catch (Exception e) {
	                    		log.error("Exception in success direct deposit Transaction! ", e);
	                    		try {
	                    			con.rollback();
	                    		} catch (Throwable it) {
	                    			log.error("Can't rollback.", it);
	                    		}
	                    	} finally {
	                    		try {
	                    			con.setAutoCommit(true);
	                    		} catch (Exception e) {
	                    			log.error("Can't set back to autocommit.", e);
	                    		}
	                    	}
						}
					} else {  // failed
						if (log.isInfoEnabled()) {
							String ls = System.getProperty("line.separator");
							log.info("Transaction failed InatecCheckout! " + ci.toString() + ls);
						}
						tran.setStatusId(TRANS_STATUS_FAILED);
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
						context.addMessage(null, fm);

						if (timeout) {
							tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
						} else {
							tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
						}
					}
				} else {  // error found
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
				}

				if (tran.getStatusId() == TRANS_STATUS_FAILED ) {   // update transaction on failed cases
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
				}

				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
				log.info("insert directDeposit finished successfully! " + description);

			} catch (Exception exp) {
				log.error("Exception in insert directDeposit! ", exp);
			}

			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);

		if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
			String successMsg = "deposit.success";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params),null);
			context.addMessage(null, fm);

			try {
				PopulationsManagerBase.deposit(user.getId(), true, writerId,tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

            try {
                afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
            } catch (Exception e) {
                log.error("Problem processing bonuses after success transaction", e);
            }
            try {
                CommonUtil.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
            return tran;
        } else if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {  // banking redirect
            return tran;
		} else { // deposit failed
			try {

				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}

				if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {

					String email;
					if (!CommonUtil.isParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
						email = ApplicationDataBase.getSupportEmailBySkinId(user.getSkinId());
					} else {
						if (user.getSkinId() == Skin.SKIN_CHINESE){
							email = CommonUtil.getProperty("deposit.error.zh.email", null);
						}else{
							email = CommonUtil.getProperty("deposit.error.email", null);
						}
					}

					String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " " +
									 	CommonUtil.getMessage("transactions.direct.banking.deposit", null) + ": " +
									 	InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());

					// send mail to support
					// collect the parameters we need for the email
					HashMap<String, String> params = null;
					params = new HashMap<String, String>();
		            params.put(SendDepositErrorEmail.PARAM_EMAIL, email);
					params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
					params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
					params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId()));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
					params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());

					// Populate also server related params, in order to take out common functionality
					// Use params
					params.put(SendDepositErrorEmail.MAIL_TO, email);
					params.put(SendDepositErrorEmail.MAIL_SUBJECT , subject + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );

					new SendDepositErrorEmail(params).start();
				}

			} catch (Exception e) {
				log.error("Could not send email.", e);
			}
			return null;
		}
	} finally {
		closeConnection(con);
	}
}

	/**
	 * PayPal deposit
	 * @param paypalEmail
	 * @param paypalTransactionId
	 * @throws SQLException
	 */
	public static Transaction insertPayPalDeposit(String depositVal, long writerId, UserBase user,
			int source, boolean firstDeposit, String paypalEmail, String paypalTransactionId, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert PayPal deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert PayPal deposit: " + ls);
		}

		Transaction tran = null;
		Connection con = getConnection();
		FacesContext context = FacesContext.getCurrentInstance();
		long userId = user.getId();
		try {
			con.setAutoCommit(false);

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(null);
			tran.setPayPalEmail(paypalEmail);
			tran.setXorIdAuthorize(paypalTransactionId);
			tran.setXorIdCapture(paypalTransactionId);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_PAYPAL_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_PENDING);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setLoginId(loginId);

			UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(depositVal));
			TransactionsDAOBase.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, writerId, tran
					.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
					ConstantsBase.LOG_BALANCE_PAYPAL_DEPOSIT, user.getUtcOffset());

			con.commit();

			try {
				PopulationsManagerBase.deposit(userId, true, writerId, tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), user.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("wire.deposit.success", params), null);
				context.addMessage(null, fm);

			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Paypal deposit inserted successfully!");
			}
            try {
                CommonUtil.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertPayPalDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        try {
        	afterDepositAction(user, tran, TransactionSource.TRANS_FROM_BACKEND);
            afterTransactionSuccess(tran.getId(), userId, tran.getAmount(), writerId, loginId);
        } catch (Exception e) {
            log.error("Problem processing bonuses after success transaction", e);
        }

        return tran;
	}

//
///**
//	 * PayPal deposit using Express checkout method
//	 * Using PayPal NVP API (name value pair api)
//	 * @throws SQLException
//	 */
//	public static Transaction insertPayPalDeposit(String error, String formName, String depositVal, long writerId, UserBase user,
//			int source, boolean firstDeposit) throws SQLException {
//
//		if (log.isInfoEnabled()) {
//			String ls = System.getProperty("line.separator");
//			log.info(ls + "Insert PayPal deposit:" + ls +
//                    "error: " + error + ls +
//                    "amount: " + depositVal + ls +
//                    "writer: " + writerId + ls +
//                    "userId: " + user.getId() + ls);
//		}
//
//		FacesContext context = FacesContext.getCurrentInstance();
//		Transaction tran = null;
//		Connection con = null;
//		try {
//			con = getConnection();
//			long deposit = CommonUtil.calcAmount(depositVal);
//			if (error == null) {
//				error = validateDepositLimits(con, formName, user, deposit, source);
//			}
//
//			tran = new Transaction();
//			tran.setAmount(CommonUtil.calcAmount(depositVal));
//			tran.setComments("");
//			tran.setCreditCardId(null);
//			tran.setCc4digit(null);
//			tran.setDescription(null);
//			tran.setIp(CommonUtil.getIPAddress());
//			tran.setProcessedWriterId(writerId);
//			tran.setChequeId(null);
//			tran.setTimeSettled(null);
//			tran.setTimeCreated(new Date());
//			tran.setTypeId(TRANS_TYPE_PAYPAL_DEPOSIT);
//			tran.setUserId(user.getId());
//			tran.setWriterId(writerId);
//			tran.setWireId(null);
//			tran.setChargeBackId(null);
//			tran.setStatusId(TRANS_STATUS_STARTED);
//			tran.setCurrency(user.getCurrency());
//			tran.setUtcOffsetCreated(user.getUtcOffset());
//			tran.setWireId(null);
//
//			TransactionsDAOBase.insert(con, tran);
//
//			if (log.isInfoEnabled()) {
//				String ls = System.getProperty("line.separator");
//				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
//			}
//
//			boolean timeout = false;
//
//			try {
//				if (null == error) {  // start clearing
//
//					PayPalInfo ci = new PayPalInfo(user, tran, PayPalClearingProvider.PAYMENT_TYPE_DEPOSIT);
//					if (log.isInfoEnabled()) {
//						String ls = System.getProperty("line.separator");
//						log.info("Transaction PayPal deposit started: PayPalClearing Info: " + ci.toString() + ls);
//					}
//
//					try {
//						ClearingManager.PayPalDeposit(ci);
//					} catch (ClearingException ce) {
//						if (ce.getCause() instanceof SocketTimeoutException) {
//							timeout = true;
//						}
//						log.error("Error! problem with payPalDeposit, transaction: " + ci.toString(), ce);
//						ci.setResult("999");
//						ci.setSuccessful(false);
//						ci.setMessage(ce.getMessage());
//					}
//		            tran.setClearingProviderId(ci.getProviderId());
//					tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + null != ci.getUserMessage() ? ci.getUserMessage() : "");
//
//					/*tran.setXorIdAuthorize(ci.getProviderTransactionId());
//					tran.setAuthNumber(ci.getAuthNumber());*/
//
//					if (ci.isSuccessful()) {   // success
//						if (log.isInfoEnabled()) {
//							log.info("Transaction successful from Inatec");
//						}
//						if (null != ci.getLoginRedirectUrl()) {   // authenticate status, redirect to payPal login needed
//	                        tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
//	                        tran.setTimeSettled(null);
//	                        context.getApplication().createValueBinding(ConstantsBase.BIND_PAYPAL_INFO).setValue(context, ci); // for saving redirectUrl
//	                        TransactionsDAOBase.update(con, tran);
//						} else {  // without bank redirect
//	                       log.warn("no login url!");
//						}
//					} else {  // failed
//						if (log.isInfoEnabled()) {
//							String ls = System.getProperty("line.separator");
//							log.info("Transaction failed InatecCheckout! " + ci.toString() + ls);
//						}
//						tran.setStatusId(TRANS_STATUS_FAILED);
//						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
//						context.addMessage(null, fm);
//						if (timeout) {
//							tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
//						} else {
//							tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
//						}
//					}
//				} else {  // error found
//					tran.setStatusId(TRANS_STATUS_FAILED);
//					tran.setDescription(error);
//				}
//
//				if (tran.getStatusId() == TRANS_STATUS_FAILED ) {   // update transaction on failed cases
//					tran.setTimeSettled(new Date());
//					tran.setUtcOffsetSettled(user.getUtcOffset());
//					TransactionsDAOBase.update(con, tran);
//				}
//
//				// set description if needed
//				String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
//				log.info("insert PayPalDeposit finished successfully! " + description);
//
//			} catch (Exception exp) {
//				log.error("Exception in insert PayPalDeposit! ", exp);
//			}
//
//		if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
//			String[] params = new String[1];
//			params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
//			String successMsg = "deposit.success";
//			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params),null);
//			context.addMessage(null, fm);
//
//			try {
//				PopulationsManagerBase.deposit(user.getId(), true, writerId,tran);
//			} catch (Exception e) {
//				log.warn("Problem with population succeed deposit event " + e);
//			}
//
//            try {
//                afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId);
//            } catch (Exception e) {
//                log.error("Problem processing bonuses after success transaction", e);
//            }
//
//            return tran;
//        } else if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {  // banking redirect
//            return tran;
//		} else { // deposit failed
//			try {
//
//				try {
//					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
//				} catch (Exception e) {
//					log.warn("Problem with population failed deposit event " + e);
//				}
//
//				if ( !user.getClassId().equals(String.valueOf(ConstantsBase.USER_CLASS_TEST))) {
//
//					String email = "deposit.error.email";
//					String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " " +
//									 	CommonUtil.getMessage("transactions.paypal.deposit", null);
//
//					// send mail to support
//					// collect the parameters we neeed for the email
//					HashMap<String, String> params = null;
//					params = new HashMap<String, String>();
//		            params.put(SendDepositErrorEmail.PARAM_EMAIL, CommonUtil.getProperty(email, null));
//					params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
//					params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
//					params.put(SendDepositErrorEmail.PARAM_AMOUNT, tran.getAmountTxt());
//					params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
//					params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
//					params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());
//
//					// Populate also server related params, in order to take out common functionality
//					// Use params
//					params.put(SendDepositErrorEmail.MAIL_TO, CommonUtil.getProperty(email, null));
//					params.put(SendDepositErrorEmail.MAIL_SUBJECT , subject + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );
//
//					new SendDepositErrorEmail(params).start();
//				}
//
//			} catch (Exception e) {
//				log.error("Could not send email.", e);
//			}
//			return null;
//		   }
//		} finally {
//			closeConnection(con);
//		}
//	}

	/**
	 * Deposit with ACH service
	 * @throws SQLException
	 */
	public static Transaction insertDepositACH(String error, String formName, String depositVal,
			long writerId, UserBase user, int source, ACHInfo achInfo, long loginId) throws SQLException {

		String errorFromClearingInfo = null;
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls +
                    "error: " + error + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;
		ACHDepositBase achDeposit;
		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			if (error == null) {
				error = validateDepositLimits(con, formName, user, deposit, source, true);
			}

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_ACH_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setClearingProviderId(achInfo.getProviderId());
			tran.setLoginId(loginId);

			TransactionsDAOBase.insert(con, tran);

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}

			achDeposit = new ACHDepositBase();
			achDeposit.setTransactionId(tran.getId());
			achDeposit.setPayeeEmail(achInfo.getPayeeEmail());
			achDeposit.setPayeeAddress(achInfo.getPayeeAddress());
			achDeposit.setPayeeCity(achInfo.getPayeeCity());
			achDeposit.setPayeeState(achInfo.getPayeeStateProvince());
			achDeposit.setPayeeZip(achInfo.getPayeeZipCode());
			achDeposit.setPayeePhone(achInfo.getPayeePhone());
			achDeposit.setRoutingNo(achInfo.getRoutingNo());
			achDeposit.setAccountNo(achInfo.getAccountNo());

			ACHDepositDAOBase.insert(con, achDeposit);

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("ACH deposit record inserted: " + ls + achDeposit.toString() + ls);
			}

			boolean timeout = false;

			try {
				if (null == error) {  // start clearing

					ACHInfo ai = new ACHInfo(user, tran);
					ai.setPayeeName(achInfo.getPayeeName());
					ai.setPayeeEmail(achInfo.getPayeeEmail());
					ai.setPayeeAddress(achInfo.getPayeeAddress());
					ai.setPayeeCity(achInfo.getPayeeCity());
					ai.setPayeeStateProvince(achInfo.getPayeeStateProvince());
					ai.setPayeeZipCode(achInfo.getPayeeZipCode());
					ai.setPayeePhone(achInfo.getPayeePhone());
					ai.setAccountNo(achInfo.getAccountNo());
					ai.setRoutingNo(achInfo.getRoutingNo());
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Clearing started: Clearing Info: " + ai.toString() + ls);
					}

					try {
						ClearingManager.directDepositACH(ai);
					} catch (ClearingException ce) {
						int statusCode = -1;
						try {
							statusCode = Integer.valueOf(ce.getMessage());
						} catch (NumberFormatException nfe) {
						}
						if (statusCode >= 9004 && statusCode <= 9013) {
							errorFromClearingInfo = ai.getStatusMessage();
						}
						else {
							errorFromClearingInfo = null;
						}
						if (ce.getCause() instanceof SocketTimeoutException) {
							timeout = true;
						}
						log.error("Error! problem with ACH Deposit, transaction: " + ai.toString(), ce);
						ai.setResult("999");
						ai.setSuccessful(false);
						ai.setMessage(ce.getMessage());
					}

					String comment = "";
					if (null != ai.getResult()) {
						comment += ai.getResult() + "|";
					}
					if (null != ai.getMessage()) {
						comment += ai.getMessage() + "|";
					}
					if (null != ai.getUserMessage()) {
						comment += ai.getUserMessage() + "|";
					}
					if (null != errorFromClearingInfo) {
						comment += errorFromClearingInfo + "|";
					}
					if (null != ai.getProviderTransactionId()) {
						comment += ai.getProviderTransactionId();
					} else {
						if (comment.length() > 0) {
							comment = comment.substring(0, comment.length()-1);
						}
					}
					tran.setComments(comment);

					if (ai.isSuccessful()) {   // success
						if (log.isInfoEnabled()) {
							log.info("Successful response from ACH");
						}
			            try {
			                CommonUtil.alertOverDepositLimitByEmail(tran, user);
			            } catch (RuntimeException e) {
			                log.error("Exception sending email alert to traders! ", e);
			            }
					} else {  // failed
						if (log.isInfoEnabled()) {
							String ls = System.getProperty("line.separator");
							log.info("Transaction failed InatecCheckout! " + ai.toString() + ls);
						}
						tran.setStatusId(TRANS_STATUS_FAILED);
						FacesMessage fm = null;
						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
						context.addMessage(null, fm);
						if (timeout) {
							tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
						} else {
							tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
						}
					}

				} else {  // error found
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED_MSG, null),null);
					context.addMessage(null, fm);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
				}

				if (tran.getStatusId() == TRANS_STATUS_FAILED ) {   // update transaction on failed cases
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
				}

				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
				log.info("insert ACH Deposit finished successfully! " + description);

			} catch (Exception exp) {
				log.error("Exception in insert ACH Deposit! ", exp);
			}

			if (tran.getStatusId() == TRANS_STATUS_STARTED) {  // success from ACH
	            return tran;
			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Deposit with cashU service
	 * @throws SQLException
	 */
	public static Transaction insertDepositCashUPhase1(String depositVal, String formName,
			long writerId, int source, UserBase user, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Transaction tran = null;
		Connection con = null;
		String error = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			error = validateDepositLimits(con, formName, user, deposit, source, true);
			tran = new Transaction();

			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CASHU_DEPOSIT);
			tran.setClearingProviderId(ClearingManager.CASHU_PROVIDER_ID);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setLoginId(loginId);
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());

			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			TransactionsDAOBase.insert(con, tran);

		} finally {
			closeConnection(con);
		}
		if (null != error) {
			return null;
		}
		return tran;
	}

	/**
	 * Deposit with cashU service
	 * @throws SQLException
	 */
	public static Transaction insertDepositCashUPhase2(CashUInfo cashUInfo, UserBase user, long loginId) throws SQLException {

		Connection con = null;

		Transaction transaction = getTransaction(cashUInfo.getTransactionId());

		// prevent ASTech sending us the same trx request more then once
		if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
			log.warn("Getting CashU return request of non authenticate transaction!");
			return null;
		}
		transaction.setTimeSettled(new Date());
		transaction.setUtcOffsetSettled(user.getUtcOffset());
		con = getConnection();
		try {
			if (cashUInfo.getStatusCode().equals("0")) { // success
				transaction.setStatusId(TRANS_STATUS_SUCCEED);
			}
			else {
				transaction.setStatusId(TRANS_STATUS_FAILED);
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = null;
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);
			}
			transaction.setComments(cashUInfo.getStatusMessage());
			transaction.setXorIdCapture(cashUInfo.getAstechId());
			TransactionsDAOBase.update(con, transaction);

			afterDepositAction(user, transaction, TransactionSource.TRANS_FROM_WEB);

			if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
				String[] params = new String[1];
				params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(transaction.getAmount()), transaction.getCurrencyId());
				String successMsg = "deposit.success";
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params),null);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, fm);

				try {
					PopulationsManagerBase.deposit(user.getId(), true, transaction.getWriterId(),transaction);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}

	            try {
	                afterTransactionSuccess(transaction.getId(), user.getId(), transaction.getAmount(), transaction.getWriterId(), loginId);
	            } catch (Exception e) {
	            	log.error("Problem processing bonuses after success transaction", e);
	            }

	            try {
	        		con.setAutoCommit(false);
					UsersDAOBase.addToBalance(con, user.getId(), transaction.getAmount());
					GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_CASHU_DEPOSIT, user.getUtcOffset());
					TransactionsDAOBase.update(con, transaction);
					con.commit();
	        	} catch (Exception e) {
	        		log.error("Exception in success deposit Transaction! ", e);
					try {
						con.rollback();
					} catch (Throwable it) {
						log.error("Can't rollback.", it);
					}
	        	} finally {
	                try {
	                    con.setAutoCommit(true);
	                } catch (Exception e) {
	                    log.error("Can't set back to autocommit.", e);
	                }
	    		}
                try {
                    CommonUtil.alertOverDepositLimitByEmail(transaction, user);
                } catch (RuntimeException e) {
                    log.error("Exception sending email alert to traders! ", e);
                }
	            return transaction;

			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, transaction.getWriterId(), transaction);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}


	public static Transaction insertDeposit(String depositVal, String cardId, long writerId, UserBase user, int source, String authorizationCode, boolean firstNewCardChanged, boolean isCheckMinAmount, String comment, long providerId, String ccPass, long loginId) throws SQLException {
		return insertDeposit(null, "depositForm", depositVal, cardId, writerId, user, source, false, authorizationCode, firstNewCardChanged, isCheckMinAmount,  comment,  providerId, ccPass, loginId);

	}

	private static Transaction insertDeposit(String error, String formName, String depositVal, String cardId, long writerId, UserBase user,
			int source, boolean firstDeposit, String authorizationCode, boolean firstNewCardChanged, boolean isCheckMinAmount, String comment, long providerId, String ccPass, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls +
                    "error: " + error + ls +
                    "amount: " + depositVal + ls +
                    "cardId: " + cardId + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls +
                    "source: " + source + ls);
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;
		boolean riskAlert = false;

		Connection con = getConnection();
		try {
            CreditCard cc = CreditCardsDAO.getById(con, Long.parseLong(cardId));
            cc.setCcPass(ccPass);
            if ((cc.getTypeIdByBin() == CC_TYPE_DINERS && !user.isEtrader()) || 
            		(cc.getTypeIdByBin()==CC_TYPE_AMEX && !user.isEtrader()) ) {
            	CreditCardsDAO.updateVisibleByUserId(con,user.getId(),cc.getId());

            	if (log.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					log.log(Level.INFO, "Diners club credit card. CardId: " + cardId + " UserId: "+ user.getId() + ls);
				}
            	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.diners.notsupported.deposit", null), null);
				context.addMessage(null, fm);
				tran = new Transaction();
				tran.setId(CC_TYPE_DINERS);
				return tran;
            }
			if (error == null) {
				error = validateDepositForm(formName, depositVal, cc, user, source, isCheckMinAmount);
			}
			if(ApplicationDataBase.isBackend()) {
				if (AdminManagerBase.isCCBlacklisted(cc.getCcNumber(), true , user.getSkinId())) {
					if (log.isEnabledFor(Level.INFO)) {
						String ls = System.getProperty("line.separator");
						log.log(Level.INFO, "Transaction Failed: card is black listed!" + ls);
					}
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.blacklisted", null), null);
					context.addMessage(null, fm);
					error = ConstantsBase.ERROR_BLACK_LIST;
				} else {
					if(AdminManagerBase.isBINBlacklisted(cc, true , user)) {
						if (log.isEnabledFor(Level.INFO)) {
							String ls = System.getProperty("line.separator");
							log.log(Level.DEBUG, "Transaction Failed: BIN is black listed!" + ls);
						}
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.binblacklisted", null), null);
						context.addMessage(null, fm);
						error = ConstantsBase.ERROR_BIN_BLACK_LIST;
					}
				}
			}

			// check velocity deposits case we don't have earlier error
			if(error == null){
				error = velocityCheck(user.getId());
			}

			// check deposit limits in case we don't have earlier error
			boolean firstRealDeposit = isFirstDeposit(user.getId(), 0);
			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(comment);
			tran.setCreditCardId(new BigDecimal(cardId));
			tran.setCc4digit(cc.getCcNumberLast4());
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CC_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setLoginId(loginId);
			if(providerId == ConstantsBase.CLEARING_PROVIDER_NOT_SELECTED) {
				tran.setManualRouted(0);
				// manual routing
			} else {
				tran.setManualRouted(1);
			}
			TransactionsDAOBase.insert(con, tran);

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}

			
			String errorCode = "";
			try {
            	//Chech CC from Diff Countries
            	RiskAlertsManagerBase.riskAlertDepositHandlerCCFromDiffCountry(con, tran);
				
				if (null == error && ApplicationDataBase.isWeb() && !firstNewCardChanged && isFailureRepeat(con, tran.getUserId(), tran.getCreditCardId(), tran.getAmount(), tran.getId(), firstDeposit)) {
					error = ConstantsBase.ERROR_CARD_FAILURE_REPEAT;
				}
				if (error != null) {
					if (error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
						ArrayList<User> users = CreditCardsDAO.checkCardExistence(con, cc.getCcNumber(), cc.getUserId(), user.getSkinId());
    					StringBuilder usersString = new StringBuilder();
    					if (users != null) {
    						for (com.anyoption.common.beans.User u: users) {
    							usersString.append(u.getUserName());
    							usersString.append(", ");
    						}
    					}
    					String usrs = usersString.substring(0, usersString.length() -1);
						tran.setComments(cc.toStringForComments() + "| duplicate Card on users:" + usrs);
					} else if (error.equals(ConstantsBase.ERROR_CARD_FAILURE_REPEAT)) {
						tran.setComments(cc.toStringForComments() + "| card failure repeat");
					} else {
						tran.setComments(cc.toStringForComments());
					}

					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Failed: " + ls + error + ls);
					}

					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
				} else if (!cc.isAllowed()) {
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Failed: card not allowed!" + ls);
					}

					tran.setStatusId(TRANS_STATUS_FAILED);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.notallowed", null), null);
					context.addMessage(null, fm);
					tran.setDescription(ConstantsBase.ERROR_CARD_NOT_ALLOWED);
				} else if (AdminManagerBase.isBlacklisted(cc, true , user)) {
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Failed: card is black listed!" + ls);
					}

					tran.setStatusId(TRANS_STATUS_FAILED);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.blacklisted", null), null);
					context.addMessage(null, fm);
					tran.setDescription(ConstantsBase.ERROR_BLACK_LIST);
				} else {
					// MERGED
					ClearingInfo ci = depositWithReroutingBase(con, tran, user, cc, providerId, writerId, authorizationCode );
					tran = TransactionsDAOBase.getById(con, ci.getTransactionId());
					tran.setThreeD(ci.is3dSecure());
					tran.setRender(ci.getFormData()!=null);
					if (firstDeposit) {
						String res = ci.getResult();
						try {
							int r = Integer.valueOf(res).intValue();

							ArrayList<Enumerator> codesList = CommonUtil.getEnum(ConstantsBase.ENUM_CREDIT_CARDS_NOT_VISIBLE_LIST);
							for (int i = 0; i < codesList.size(); i++) {
								Enumerator e = (Enumerator) codesList.get(i);
								int val = Integer.valueOf(e.getValue()).intValue();
								if (val == r) {
		                            if (log.isDebugEnabled()) {
		                                log.debug("updating credit card to not visible. cardId=" + cardId);
		                            }
									CreditCardsDAO.updateCardNotVisible(con, Long.parseLong(cardId));
								}
							}
						} catch (Exception e) {
							log.error("Result code is not a number!! result=" + res);
						}
					}
					if(tran.getStatusId() == TRANS_STATUS_FAILED) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
						context.addMessage(null, fm);
					}
					
					if (ci.is3dSecure()) {
	                   ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_CLEARING_INFO, ClearingInfo.class);
	                   ve.getValue(context.getELContext());
	                   ve.setValue(context.getELContext(), ci);
					}
					
					errorCode = ci.getResult();
				}

				if (tran.getStatusId() == TRANS_STATUS_FAILED ) {   // update transaction on failed cases
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
				}

				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
				log.info("insert deposit finished successfully! " + description);

			} catch (Exception exp) {
				log.error("Exception in insert deposit! ", exp);
			}

			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);

			if (tran.getStatusId() == TRANS_STATUS_PENDING || tran.getStatusId() == TRANS_STATUS_SUCCEED ) {
				String[] params = new String[1];
				params[0] = CommonUtil.displayAmount(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
				String successMsg = "deposit.success";
				if (formName.equalsIgnoreCase("newCardForm")) {
					successMsg = "creditcard." + successMsg;
				}
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(successMsg, params), null);
				context.addMessage(null, fm);
				sendEmailByDepositLimits(depositVal, writerId, user, firstRealDeposit);
				try {
					PopulationsManagerBase.deposit(user.getId(), true, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}
                try {
                    afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
                } catch (Exception e) {
                    log.error("Problem processing bonuses after success transaction", e);
                }
                try {
                	RiskAlertsManagerBase.riskAlertDepositHandler(writerId, error, tran, user, riskAlert, errorCode, cc.getHolderName(), firstDeposit, cc.isDocumentsSent(),cc.getCcNumber());
                } catch (Exception e) {
                	log.error("Problem with risk Alert handler ", e);
				}
                try {
                    CommonUtil.alertOverDepositLimitByEmail(tran, user);
                } catch (RuntimeException e) {
                    log.error("Exception sending email alert to traders! ", e);
                }
                if((tran.getWriterId() == 1 || tran.getWriterId() == 500) &&  (tran.getClearingProviderId() == 26 || tran.getClearingProviderId() == 27 || tran.getClearingProviderId() == 28)) { //AMEX descriptor email
                    try {
                        CommonUtil.SendReceiptEmail(con, tran, user);
                    } catch (RuntimeException e) {
                        log.error("Exception sending receipt email alert to client! ", e);
                    }
                }
				return tran;
            } else if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {
            	return tran;
            } else { // deposit failed
				try {

					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
					try {
						RiskAlertsManagerBase.riskAlertDepositHandler(writerId, error, tran, user, riskAlert, errorCode, null, false, true, 0);
					} catch (Exception e) {
						log.error("Problem with risk Alert handler ", e);
					}

					if (null != error && error.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED)) { // for AO deposit limits
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED_MSG, null),null);
						context.addMessage(null, fm);
					}

					//for display error msg to repeat failure and prevent duplicate message
					if (null != error && !error.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED) && error.equals(ConstantsBase.ERROR_CARD_FAILURE_REPEAT)){
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.notallowed", null), null);
						context.addMessage(null, fm);

					}
					
					//Velocity
					if (null != error && (error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC) || error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC) ||
							error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC) || error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC))) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.velocity.check", null),null);
						context.addMessage(null, fm);
						//collect the parameters we need for velocity error support email
						sendVelocityErrorMail(error, user.getUserName(),user.getSkinId());
					}
						

					if (user.getClassId() != ConstantsBase.USER_CLASS_TEST && source != ConstantsBase.BACKEND_CONST) {  // just to live users
																														// and not backend

						String email = CommonUtil.getProperty("deposit.error.email", null);
						if (null != error && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
							email = ApplicationDataBase.getSupportEmailBySkinId(user.getSkinId());
						}

						String subject = "deposit.error.email.subject";
						if (null != error && error.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED)) { // for AO deposit limits
							email = CommonUtil.getProperty("deposit.documents.needed.email", null);
							subject = "deposit.documents.needed.email.subject";
						}
						String emailToProperty = email;
						if (user.isVip()) {
							emailToProperty += ";" + CommonUtil.getProperty("deposit.error.email.vip", null);
						}
						// send mail to support
						// collect the parameters we neeed for the email
						HashMap<String, String> params = null;
						params = new HashMap<String, String>();
			            params.put(SendDepositErrorEmail.PARAM_EMAIL, email);
						params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
						params.put(SendDepositErrorEmail.PARAM_USER_ID, user.getIdNum());
						params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
						params.put(SendDepositErrorEmail.PARAM_CC_NUM_LAST4, tran.getCc4digit());
						params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId()));
						
						if (CommonUtil.isHebrewSkin(user.getSkinId())) {
							params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null));
							params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null));
						} else { // put english content to AO skins
							params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
							params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						}

						params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());

						// get credit card type name
						String ccTypeName = cc.getType().getName();
						params.put(SendDepositErrorEmail.PARAM_CC_TYPE, ccTypeName);

						// Populate also server related params, in order to take out common functionality
						// Use params
						params.put(SendDepositErrorEmail.MAIL_TO, emailToProperty);
						params.put(SendDepositErrorEmail.MAIL_SUBJECT , CommonUtil.getProperty(subject, null) + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );

						new SendDepositErrorEmail(params).start();
					}

					// Risk alerts
					if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
						String emails = CommonUtil.getProperty("deposit.error.risk.alert.email", null);
						String subject = "deposit.error.email.risk.alert.subject";

						boolean isStolenCC = RiskAlertsManagerBase.isStolenCard(errorCode,tran);

						if (null != error && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
							riskAlert = true;
						}

						if (riskAlert) {
							HashMap<String, String> params = null;
							params = new HashMap<String, String>();
							params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
							params.put(SendDepositErrorEmail.PARAM_USER_ID, user.getIdNum());
							params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
							params.put(SendDepositErrorEmail.PARAM_CC_NUM_LAST4, tran.getCc4digit());
							params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId()));
							params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());

							if (CommonUtil.isHebrewSkin(user.getSkinId())) {
								params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null));
								params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null));
							} else { // put english content to AO skins
								params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
								params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
							}

							// get credit card type name
							String ccTypeName = cc.getType().getName();
							params.put(SendDepositErrorEmail.PARAM_CC_TYPE, ccTypeName);
							params.put(SendDepositErrorEmail.MAIL_SUBJECT , CommonUtil.getProperty(subject, null) + " Skin:" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) );

							String[] splitTo = emails.split(";");
							for (int i = 0; i < splitTo.length; i++) {
								params.put(SendDepositErrorEmail.PARAM_EMAIL, splitTo[i]);
								params.put(SendDepositErrorEmail.MAIL_TO, splitTo[i]);
								new SendDepositErrorEmail(params).start();
							}
						}
						
						if(isStolenCC){
							HashMap<String, String> params = null;
							params = new HashMap<String, String>();
							params.put(SendStolenCCDepositEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
							params.put(SendStolenCCDepositEmail.PARAM_DATE, CommonUtil.getDateAndTimeFormat(tran.getTimeCreated()));
							params.put(SendStolenCCDepositEmail.PARAM_CC_NUM_LAST4, tran.getCc4digit());
							params.put(SendStolenCCDepositEmail.PARAM_CC_TYPE, CommonUtil.getMessage(cc.getType().getName(), null));
							params.put(SendStolenCCDepositEmail.PARAM_USER_ID, user.getId() + "");
							params.put(SendStolenCCDepositEmail.PARAM_USER_COUNTRY, CommonUtil.getMessage(user.getCountryName(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
							params.put(SendStolenCCDepositEmail.PARAM_WRITER, WritersManagerBase.getWriterName(tran.getWriterId()));
							params.put(SendStolenCCDepositEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
							params.put(SendStolenCCDepositEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));														
							params.put(SendStolenCCDepositEmail.MAIL_SUBJECT , "Suspicious CC attempt alert:[" + user.getId() + "] [" + user.getUserName() + "] [" + CommonUtil.getMessage(user.getSkin().getDisplayName(),null) + "]");

							String[] splitTo = emails.split(";");
							for (int i = 0; i < splitTo.length; i++) {
								params.put(SendStolenCCDepositEmail.PARAM_EMAIL, splitTo[i]);
								params.put(SendStolenCCDepositEmail.MAIL_TO, splitTo[i]);
								new SendStolenCCDepositEmail(params).start();
							}							
						}
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}

				return null;
			}
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction updateDeltaPayTransaction(Map result, String error, Transaction tran, UserBase user, DeltaPayInfo dlInfo, int writerId,
			boolean firstDeposit, String depositVal,int source, long loginId) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = null;
    	if (!dlInfo.handleStatusUrl(result)){
    		return null;
    	}

		// prevent DeltaPayChina sending us the same trx request more then once
		if (tran.getStatusId() != TRANS_STATUS_AUTHENTICATE && tran.getStatusId() != TRANS_STATUS_PENDING) {
			log.warn("Getting DeltaPayChina DUPLICATE Response!!!!! Transaction ID: " + tran.getId()+" status:"+tran.getStatusId());
			return null;
		}

		tran.setTimeSettled(new Date());
		tran.setUtcOffsetSettled(tran.getUtcOffsetCreated());
		con = getConnection();

		try {
			if (dlInfo.getStatus().equals("APPROVED")) { // success
				tran.setStatusId(TRANS_STATUS_SUCCEED);
				tran.setComments("1000| " + "Permitted transaction |" );
			} else if(dlInfo.getStatus().equals("PENDING")) {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
				tran.setComments("Pending recieved from deltapay waiting for final status");
				tran.setDescription(dlInfo.getDescription());
			} else {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setComments("-1| " + "Failed transaction |" + dlInfo.getDescription());
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.unapproved", null),null);
				context.addMessage(null, fm);
				tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
			}
			tran.setAuthNumber(dlInfo.getTransaction_no());
			tran.setXorIdCapture(dlInfo.getApproval_no());

			if (!TransactionsDAOBase.updateDeltaPayTransactionStatus(con, tran)) {
				log.warn("Transaction [" + tran.getId()
							+ "] could not be updated - probably a DeltaPayChina DUPLICATE Response!!!!!");
				/* !!! WARNING !!! Method exit point! */
				return null;
			}

			if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
				try {
					PopulationsManagerBase.deposit(tran.getUserId(), true, tran.getWriterId(),tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}

	            try {
	                afterTransactionSuccess(tran.getId(), tran.getUserId(), tran.getAmount(), tran.getWriterId(), loginId);
	            } catch (Exception e) {
	            	log.error("Problem processing bonuses after success transaction", e);
	            }

	            try {
	        		con.setAutoCommit(false);
					UsersDAOBase.addToBalance(con, tran.getUserId(), tran.getAmount());
					GeneralDAO.insertBalanceLog(con, tran.getWriterId(), tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
							ConstantsBase.LOG_BALANCE_CUP_DEPOSIT, tran.getUtcOffsetCreated());
					TransactionsDAOBase.update(con, tran);

					// this is distributed transaction: add user to provider link
					if(tran.getSelectorId()!= SelectorId.Assigned.getValue() && !SelectorId.isException(tran.getSelectorId())) {
						DistributionManager.createUserToProvider(tran, DistributionType.CUP);
					}

		            try {
		                CommonUtil.alertOverDepositLimitByEmail(tran, user);
		            } catch (RuntimeException e) {
		                log.error("Exception sending email alert to traders! ", e);
		            }

					con.commit();
	        	} catch (Exception e) {
	        		log.error("Exception in success deposit Transaction! ", e);
					try {
						con.rollback();
					} catch (Throwable it) {
						log.error("Can't rollback.", it);
					}
	        	} finally {
	                try {
	                    con.setAutoCommit(true);
	                } catch (Exception e) {
	                    log.error("Can't set back to autocommit.", e);
	                }
	    		}

			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(tran.getUserId(), false, tran.getWriterId(), tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
			}
		} finally {
			closeConnection(con);
		}
		return tran;
    }

	/**
	 * Insert Credit card to user and deposit
	 * @param typeId
	 * @param ccNum
	 * @param ccPass
	 * @param expMonth
	 * @param expYear
	 * @param holderName
	 * @param holderIdNum
	 * @param deposit
	 * @param writerId
	 * @param user
	 * @param source
	 * @param isCheckMinAmount TODO
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static	Transaction
			insertCreditCardAndDeposit(	String typeId, String ccNum, String ccPass, String expMonth, String expYear, String holderName,
										String holderIdNum, String deposit, long writerId, UserBase user, int source,
										boolean firstNewCardChanged, boolean isCheckMinAmount,
										long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
														NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
														UnsupportedEncodingException, InvalidAlgorithmParameterException {
		FacesMessage fm = new FacesMessage();
		CreditCard cc = insertCreditCard(typeId, ccNum, ccPass, expMonth,  expYear, holderName,  holderIdNum,  deposit, writerId,  user,  source, fm, "newCardForm");
		Transaction t = insertDeposit(fm.getDetail(), "newCardForm", deposit, String.valueOf(cc.getId()) , writerId, user, source, true, null, firstNewCardChanged, isCheckMinAmount, "new cc", -1, ccPass, loginId);
		return t;
	}


	public static	Transaction
			insertCreditCardAndDepositWithProvider(	String typeId, String ccNum, String ccPass, String expMonth, String expYear,
													String holderName, String holderIdNum, String deposit, long writerId, UserBase user,
													int source, boolean firstNewCardChanged, boolean isCheckMinAmount, long providerId,
													long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
		FacesMessage fm = new FacesMessage();
		CreditCard cc = insertCreditCard(typeId, ccNum, ccPass, expMonth,  expYear, holderName,  holderIdNum,  deposit, writerId,  user,  source, fm, "newCardForm");
		Transaction t = insertDeposit(fm.getDetail(), "newCardForm", deposit, String.valueOf(cc.getId()) , writerId, user, source, true, null, firstNewCardChanged, isCheckMinAmount, "new cc", providerId, ccPass, loginId);
		return t;
	}

	/**
	 * Insert/Update Credit card to user
	 * @param typeId
	 * @param ccNum
	 * @param ccPass
	 * @param expMonth
	 * @param expYear
	 * @param holderName
	 * @param holderIdNum
	 * @param deposit
	 * @param writerId
	 * @param user
	 * @param source
	 * @param fmError
	 * @param form
	 * @return
	 * @throws SQLException
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static CreditCard insertCreditCard(	String typeId, String ccNum, String ccPass, String expMonth, String expYear,
												String holderName, String holderIdNum, String deposit, long writerId, UserBase user,
												int source, FacesMessage fmError,
												String form)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {

		String ls = System.getProperty("line.separator");
		TransactionBin tb = new TransactionBin();
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,
							"insert credit card and deposit: " + ls + " typeId:" + typeId + ls + " ccNum:" + ccNum + ls + " ccPass:" + ccPass + ls + " expMonth:" + expMonth + ls + " expYear:" + expYear + ls + " holderName:" + holderName + ls + " holderID:" + holderIdNum + ls + " amount:" + deposit + ls + " writerId:" + writerId + ls + " userId:" + user
									.getId() + ls + " source:" + source + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (source == ConstantsBase.BACKEND_CONST) {
			if (expMonth == null || expMonth.equals("") || expYear == null || expYear.equals("")) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
				context.addMessage(form + ":expMonth", fm);
				return null;
			}
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);

		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(expYear) + 2000, Integer.parseInt(expMonth), 1);

		if (cal.after(expCal)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.expdate", null),null);
			context.addMessage(form + ":exp", fm);
			return null;
		}

		Connection con = getConnection();
		try {
			CreditCard cc = new CreditCard();

			cc.setCcNumber(Long.parseLong(ccNum.replace(ConstantsBase.SPACE, ConstantsBase.EMPTY_STRING)));
			cc.setCcPass(ccPass);
			cc.setExpMonth(expMonth);
			cc.setExpYear(expYear);
			if(!CommonUtil.isHebrewSkin(user.getSkinId())) {
				holderIdNum = ConstantsBase.NO_ID_NUM; // for missing ID in anyoption
			}
			cc.setHolderIdNum(holderIdNum);
			cc.setHolderName(holderName);
			cc.setTypeId(Long.valueOf(typeId).longValue());
			cc.setUserId(user.getId());
			cc.setVisible(true);
			cc.setPermission(CreditCard.ALLOWED);
			cc.setUtcOffsetCreated(user.getUtcOffset());
			cc.setUtcOffsetModified(user.getUtcOffset());

			// check if card number already exists and the holder id number is the same
			ArrayList<com.anyoption.common.beans.base.User> users = CreditCardsDAOBase.checkCardExistence(con, cc.getCcNumber(), cc.getUserId());
			CreditCard currentCC = CreditCardsDAO.getByNumberAndUserId(con, cc.getCcNumber(), cc.getUserId());

			if (null != currentCC) {
				cc.setPermission(currentCC.getPermission());
				if (CommonUtil.isHebrewSkin(user.getSkinId())) { // etrader users
					if (Long.parseLong(holderIdNum) != Long.parseLong(user.getIdNum())) {   // wrong id_num
						cc.setPermission(CreditCard.NOT_ALLOWED);
						// TODO insert blocking issue
					}
				}
				cc.setVisible(true);
			}

			String error = null;
			// check if the cc is allowed and in your cc list then skip this checks Bug_id=1999
			if ( ( null == currentCC ) || ( currentCC != null &&  (!currentCC.isAllowed() || (currentCC.isAllowed() && !currentCC.isManuallyAllowed()))))  {

				if (CommonUtil.isHebrewSkin(user.getSkinId())) { // etrader users
					if (Long.parseLong(holderIdNum) != Long.parseLong(user.getIdNum())) {   // wrong id_num
						if (log.isEnabledFor(Level.INFO)) {
							log.log(Level.INFO, "user is trying to use a Credit Card with different ID number." + ls + "user: " + user.getUserName() + ls);
						}
						error = ConstantsBase.ERROR_CARD_NOT_ALLOWED_WRONG_ID;
					}
				}

				if (null == error) {  // check if card already exists in the system
					if (null != users) {
						if (log.isEnabledFor(Level.INFO)) {
							log.log(Level.INFO, "user is trying to use a Credit Card That already exists in the system." + ls + "user: " + user.getUserName() + ls);
						}
						error = ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD;
					}
				}

				if (null != error) {  // error found
					if (currentCC != null && currentCC.isBlockedForDeposit()) {
						cc.setPermission(CreditCard.NOT_ALLOWED_DEPOSIT);
					} else {
						cc.setPermission(CreditCard.NOT_ALLOWED);
					}
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.notallowed", null),null);
					context.addMessage(null, fm);
				}
			}
			fmError.setDetail(error);

			if (currentCC == null) {
				if (log.isEnabledFor(Level.INFO)) {
					log.log(Level.INFO, "Insert creditCard: " + ls + cc.toString());
				}
				// Set cc country id with user's country id for cases where bin is not in bins table
				cc.setCountryId(user.getCountryId());
				CreditCardsDAO.insert(con, cc);
				try {
					if (users != null && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
						IssuesManagerBase.insertAutoBlockedCCIssue(user.getId(), ConstantsBase.OFFSET_GMT,
								cc.getCcNumberLast4(), cc.getId(), users);
					}
				} catch (Exception e) {
					log.error("Could not create blocking issue.", e);
				}
			} else {
				if (log.isEnabledFor(Level.INFO)) {
					log.log(Level.INFO, "Update creditCard details: " + ls + cc.toString());
				}
				cc.setId(currentCC.getId());
				CreditCardsDAO.update(con, cc);
			}
			//insert to transactionsBins table if don't exist
			tb = TransactionsDAOBase.getTransactionBinByBin(con, cc.getBin(), user.getSkin().getBusinessCaseIdForTranBins());
			if(tb.getId() == 0 ){
				tb.setBin(cc.getBin());
				tb.setBusinessCaseId(user.getSkin().getBusinessCaseIdForTranBins());
				tb.setPlatformId(user.getPlatformId());
				TransactionsDAOBase.insertTranBin(con, tb);
			}
			return cc;

		} finally {
			closeConnection(con);
		}
	}

	public static	boolean
			copyCreditCardToUser(	String typeId, String ccNum, String ccPass, String expMonth, String expYear, String holderName,
									String holderIdNum, boolean isVisible, boolean isAllowed, long writerId, UserBase originalUser,
									UserBase newUser, int source,
									boolean isDocsAreNotRequired)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log
					.log(
							Level.INFO,
							"copy credit card to user: " + ls + " typeId:" + typeId + ls + " ccNum:" + CommonUtil.getNumberXXXX(ccNum) + ls + " ccPass:" + "*****" + ls + " expMonth:" + expMonth + ls + " expYear:" + expYear + ls + " holderName:" + holderName + ls + " holderID:" + holderIdNum + ls + " writerId:" + writerId + ls + " userId:" + newUser
									.getId() + ls + " from user:" + originalUser.getUserName() + ls + " source:" + source + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (source == ConstantsBase.BACKEND_CONST) {
			if (expMonth == null || expMonth.equals("") || expYear == null || expYear.equals("")) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null),null);
				context.addMessage("newCardForm:expMonth", fm);
				return false;
			}
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);

		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(expYear) + 2000, Integer.parseInt(expMonth), 1);

		if (cal.after(expCal)) {

			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.expdate", null),null);
			context.addMessage("newCardForm:exp", fm);
			return false;
		}

		Connection con = getConnection();
		try {
			String error = null;

			CreditCard cc = new CreditCard();
			cc.setCcNumber(Long.parseLong(ccNum.trim()));
			cc.setCcPass(ccPass);
			cc.setExpMonth(expMonth);
			cc.setExpYear(expYear);
			cc.setHolderIdNum(holderIdNum);
			cc.setHolderName(holderName);
			cc.setVisible(isVisible);
			cc.setTypeId(Long.valueOf(typeId).longValue());
			cc.setUserId(originalUser.getId());
			cc.setPermission(CreditCard.ALLOWED);
			cc.setUtcOffsetCreated(newUser.getUtcOffset());
			cc.setUtcOffsetModified(newUser.getUtcOffset());
			cc.setDocsAreNotRequired(isDocsAreNotRequired);

			CreditCard currentCC = CreditCardsDAO.getByNumberAndUserId(con, cc.getCcNumber(), cc.getUserId());

			if (currentCC != null) {
				if (log.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					log.log(Level.INFO, "Insert creditCard details: " + ls + cc.toString());
				}

				cc.setUserId(newUser.getId());
				// Set cc country id with user's country id for cases where bin is not in bins table
				cc.setCountryId(newUser.getCountryId());
				CreditCardsDAO.insert(con, cc);
				if (log.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					log.log(Level.INFO, "Credit Card is duplicated successfully! " + error + ls);
				}
			}
			return true;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean validateCityWithdrawForm(String cityName, long cityId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		boolean validate = true;

		try {
			CommonUtil.validateHebrewOnly(null, null, cityName);
			if (cityId == 0) {

				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.city", null),null);
				context.addMessage("withdrawForm:cityId", fm);
				validate = false;
			}
		} catch (Exception e) {
			validate = false;
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.city.nothebrew", null),null);
			context.addMessage("withdrawForm:cityId", fm);

		}

		return validate;

	}

	public static String validateWithdrawForm(String famount, UserBase user, boolean isFee, boolean splitErrorMsg, 
			boolean checkLimits, CreditCard cc, String paypalEmail, String moneybookersEmail, Long deltaPayMaxAmount, long writerId) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		
		if((user.getSkin().isRegulated() || SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM) 
					&& writerId == Writer.WRITER_ID_WEB) {
			//AO Regulation checks
			if(!UserRegulationManager.isCanWithdraw(user.getId())) {
				if (context != null) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw1", null), null);
					context.addMessage(null, fm);			
				}
				return "error.minwithdraw1";	
			}				
		}
		
		long amount = CommonUtil.calcAmount(famount);
		
		Connection con = getConnection();
		try {

			UsersDAOBase.getByUserName(con, user.getUserName(), user);

			if (user.getBalance() < amount) {
				if (context != null) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.withdraw.highAmount", null),null);
					context.addMessage("withdrawForm:amount", fm);
				}
				return "error.transaction.withdraw.highamount";
			}

			if (!BonusManagerBase.userCanWithdraw(user.getId(), user.getBalance(), amount)) {
				if (context != null) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("bonus.cannot.withdraw.error", null),null);
					context.addMessage(null, fm);
				}
				return "bonus.cannot.withdraw.error";
			}

			Limit limit = LimitsDAO.getById(con, user.getLimitIdLongValue());

			String[] params = new String[1];

			if (isFee) {
				if ((amount < Long.valueOf(limit.getMinWithdraw()).longValue() && checkLimits)
						|| (amount < 0)) {
					params[0] = CommonUtil.displayAmount(limit.getMinWithdraw(), user.getCurrencyId());
					if (context != null) {
						if ( splitErrorMsg ) {  // for Web cc withdrawal
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw1", null),null);
							context.addMessage("withdrawForm:amount", fm);
							fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw2", null),null);
							context.addMessage("withdrawForm:amountlabelid", fm);
						}	// Note: error.minwithdraw2 is now empty but it used for long msg in the web
						else {
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw", null),null);
							context.addMessage("withdrawForm:amount", fm);
						}
					}
					return "error.transaction.withdraw.minamounnt";

				} else {
					if (amount > Long.valueOf(limit.getMaxWithdraw()).longValue() ||
							(null != cc && cc.isCreditEnabled() &&  amount > cc.getCreditAmount() )) {

						if (amount > Long.valueOf(limit.getMaxWithdraw()).longValue()) {
							params[0] = CommonUtil.displayAmount(limit.getMaxWithdraw(), user.getCurrencyId());
						} else {
							params[0] = CommonUtil.displayAmount(cc.getCreditAmount(), user.getCurrencyId());
						}
						if (context != null) {
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.maxwithdraw", params),null);
							context.addMessage("withdrawForm:amount", fm);
						}
						return "error.transaction.withdraw.maxamount";
					}
				}
			} else {
				if (amount > Long.valueOf(limit.getMaxWithdraw()).longValue() ||
						(null != cc && cc.isCreditEnabled() &&  amount > cc.getCreditAmount() )) {

					if (amount > Long.valueOf(limit.getMaxWithdraw()).longValue()) {
						params[0] = CommonUtil.displayAmount(limit.getMaxWithdraw(), user.getCurrencyId());
					} else {
						params[0] = CommonUtil.displayAmount(cc.getCreditAmount(), user.getCurrencyId());
					}
					if (context != null) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.maxwithdraw", params),null);
						context.addMessage("withdrawForm:amount", fm);
					}
					return "error.transaction.withdraw.maxamount";
				}
			}

			if (null != paypalEmail) {
				if (!ApplicationDataBase.isBackend()){//disable this check for Backend uses
					if(!TransactionsDAOBase.isPaypalEmailValid(con, user.getId(),paypalEmail)) {
						if (context != null) {
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.paypal.email.not.valid", null),null);
							context.addMessage("withdrawForm:email", fm);
						}
						return "error.paypal.email.not.valid";
					}
				}
			}

			if (null != moneybookersEmail){
				if (!ApplicationDataBase.isBackend()){//disable this check for Backend uses
					if(!TransactionsDAOBase.isMoneybookersEmailValid(con, user.getId(),moneybookersEmail)) {
						if (context != null) {
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.moneybookers.email.not.valid", null),null);
							context.addMessage("withdrawForm:email", fm);
						}
						return "error.moneybookers.email.not.valid";
					}
				}
			}

			if (null != deltaPayMaxAmount && amount > deltaPayMaxAmount) {
				if (amount > Long.valueOf(limit.getMaxWithdraw()).longValue()) {
					params[0] = CommonUtil.displayAmount(limit.getMaxWithdraw(), user.getCurrencyId());
				} else {
					params[0] = CommonUtil.displayAmount(deltaPayMaxAmount, user.getCurrencyId());
				}
				if (context != null) {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.maxwithdraw", params),null);
					context.addMessage("withdrawForm:amount", fm);
				}
				return "error.transaction.withdraw.maxamount";
			}
		} finally {
			closeConnection(con);
		}
		return null;

	}

	/**
	 * Insert chequ withdrawal
	 * @throws SQLException
	 */
	public static void insertWithdraw(String famount, long cityId, String cityName,  String name, String street, String streetno, String zipCode, String error, UserBase user, long writerId, boolean feeCancel, long loginId, QuestionnaireUserAnswer questUserAnswer)
			throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(
							Level.INFO,
							"Insert Withdraw: " + ls + "User:" + user.getUserName() + ls + "famount:" + famount + ls + "cityId:" + cityId + ls + "name:" + name + ls + "street:" + street + ls + "streetno:" + streetno + ls + "zipCode:" + zipCode + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(famount);

			Cheque cheque = null;

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(famount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setTypeId(TRANS_TYPE_CHEQUE_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setLoginId(loginId);
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			if (error == null) {  // error not found
				cheque = new Cheque();
				cheque.setCityId(cityId);
				cheque.setCityName(cityName);
				cheque.setName(name);
				cheque.setStreet(street);
				cheque.setStreetNo(streetno);
				cheque.setZipCode(zipCode);
				cheque.setFeeCancel(feeCancel);
				try {
					con.setAutoCommit(false);
					ChequeDAO.insert(con, cheque);
					// transaction
					tran.setChequeId(new BigDecimal(cheque.getId()));
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(famount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CHEQUE_WITHDRAW, user.getUtcOffset());
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("Insert Withdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during Withdraw! ", e);
					try {
						con.rollback();
					} catch (SQLException ie) {
						log.log(Level.ERROR, "Can't rollback.", ie);
					}
					throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else { // error found
				tran.setChequeId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert Withdraw finished with error!");
			}
		} catch (SQLException e) {
			log.error("ERROR during Withdraw!! ", e);
		} finally {
			closeConnection(con);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), user);
	}

//    /**
//     * Insert chequ withdrawal
//     * @throws SQLException
//     */
//    public static void insertEFTWithdraw(String famount, long cityId, String cityName,  String name, String street, String streetno, String zipCode, String error, UserBase user, long writerId, boolean feeCancel)
//            throws SQLException {
//
//        if (log.isEnabledFor(Level.INFO)) {
//            String ls = System.getProperty("line.separator");
//            log.log(
//                            Level.INFO,
//                            "Insert EFT Withdraw: " + ls + "User:" + user.getUserName() + ls + "famount:" + famount + ls + "cityId:" + cityId + ls + "name:" + name + ls + "street:" + street + ls + "streetno:" + streetno + ls + "zipCode:" + zipCode + ls);
//        }
//
//        FacesContext context = FacesContext.getCurrentInstance();
//        Connection con = getConnection();
//
//        try {
//            UsersDAOBase.getByUserName(con, user.getUserName(), user);
//            long newBalance = user.getBalance() - CommonUtil.calcAmount(famount);
//
//            Cheque cheque = null;
//
//            Transaction tran = new Transaction();
//            tran.setAmount(CommonUtil.calcAmount(famount));
//            tran.setComments(null);
//            tran.setCreditCardId(null);
//            tran.setIp(CommonUtil.getIPAddress());
//            tran.setProcessedWriterId(writerId);
//            tran.setTimeCreated(new Date());
//            tran.setUtcOffsetCreated(user.getUtcOffset());
//            tran.setTypeId(TRANS_TYPE_EFT_WITHDRAW);
//            tran.setUserId(user.getId());
//            tran.setWriterId(writerId);
//            tran.setWireId(null);
//            tran.setChargeBackId(null);
//            tran.setReceiptNum(null);
//
//            if (error == null) {  // error not found
//                cheque = new Cheque();
//                cheque.setCityId(cityId);
//                cheque.setCityName(cityName);
//                cheque.setName(name);
//                cheque.setStreet(street);
//                cheque.setStreetNo(streetno);
//                cheque.setZipCode(zipCode);
//                cheque.setFeeCancel(feeCancel);
//                try {
//                    con.setAutoCommit(false);
//                    ChequeDAO.insert(con, cheque);
//                    // transaction
//                    tran.setChequeId(cheque.getId());
//                    tran.setDescription(null);
//                    tran.setStatusId(TRANS_STATUS_REQUESTED);
//                    tran.setTimeSettled(null);
//                    UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(famount));
//                    user.setBalance(newBalance);
//                    TransactionsDAOBase.insert(con, tran);
//                    GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_EFT_WITHDRAW, user.getUtcOffset());
//                    con.commit();
//                    log.info("Insert EFT Withdraw finished successfully!");
//                } catch (SQLException e) {
//                    log.error("ERROR during EFT Withdraw! ", e);
//                    try {
//                        con.rollback();
//                    } catch (SQLException ie) {
//                        log.log(Level.ERROR, "Can't rollback.", ie);
//                    }
//                    throw e;
//                } finally {
//                    try {
//                        con.setAutoCommit(true);
//                    } catch (Exception e) {
//                        log.error("Cannot set back to autoCommit! " + e);
//                    }
//                }
//            } else { // error found
//                tran.setChequeId(null);
//                tran.setDescription(error);
//                tran.setStatusId(TRANS_STATUS_FAILED);
//                tran.setTimeSettled(new Date());
//                tran.setUtcOffsetSettled(user.getUtcOffset());
//                TransactionsDAOBase.insert(con, tran);
//                log.info("Insert EFT Withdraw finished with error!");
//            }
//        } catch (SQLException e) {
//            log.error("ERROR during Withdraw!! ", e);
//        } finally {
//            closeConnection(con);
//        }
//        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
//        ve.getValue(context.getELContext());
//        ve.setValue(context.getELContext(), user);
//    }


	/**
	 * Insert credit card withdrawal
	 *
	 * @param famount
	 * 			witdrawal amount
	 * @param cardId
	 * 			credit card number
	 * @param name
	 * 			user name
	 * @param error
	 * @param user
	 * @param writerId
	 * @param feeCancel
	 * 			fee flag
	 * @throws SQLException
	 */
	public static String insertCreditCardWithdraw(String famount, CreditCard cc, String name, String error, UserBase user, long writerId,
			boolean feeCancel, Long depositRefId, long loginId, QuestionnaireUserAnswer questUserAnswer)throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log
					.log(
							Level.INFO,
							"Insert Credit Card Withdraw: " + ls + "User:" + user.getUserName() + ls + "famount:" + famount + ls );
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long withdrawAmount = CommonUtil.calcAmount(famount);
			long newBalance = user.getBalance() - withdrawAmount;
			long clearingProviderId = 0;

			Transaction tran = new Transaction();
			tran.setAmount(withdrawAmount);
			tran.setComments(null);
			tran.setCreditCardId(new BigDecimal(cc.getId()));
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setTypeId(TRANS_TYPE_CC_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setLoginId(loginId);

			if (cc.isCreditEnabled()) {   // credit withdrawal
				tran.setCreditWithdrawal(true);
				clearingProviderId = cc.getClearingProviderId();
			} else {
				tran.setCreditWithdrawal(false);
				clearingProviderId = cc.getCftClearingProviderId();
			}
			
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			//	checking credit card
			if (error == null) {
				if (cc.isBlocked()) {
					if (log.isEnabledFor(Level.INFO)) {
						String ls = System.getProperty("line.separator");
						log.log(Level.INFO, "Transaction Failed: card not allowed!" + ls);
					}
					if(context != null) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.notallowed.withdraw", null),null);
						context.addMessage(null, fm);
					}

					error = ConstantsBase.ERROR_CARD_NOT_ALLOWED;
				}
			}

			if (error == null) {
				tran.setDescription(null);
				tran.setStatusId(TRANS_STATUS_REQUESTED);
				tran.setTimeSettled(null);
				tran.setClearingProviderId(clearingProviderId);

				try {
					con.setAutoCommit(false);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(famount));
					user.setBalance(newBalance);
					if ( null != depositRefId ) {   // credit from backend
						tran.setDepositReferenceId(depositRefId.longValue());
					}
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CC_WITHDRAW, user.getUtcOffset());
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser != null && popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
				} catch (SQLException e) {
					log.error("ERROR during Credit card Withdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // there is an error
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}

			//  set description if needed
			String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
			log.info("Insert Credit card Withdraw finished successfully! " + description);
		} catch (SQLException e) {
			log.error("ERROR during Credit card Withdraw!! ", e);
		} finally {
			closeConnection(con);
		}

		if(context != null) {
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
		return error;
	}


	/**
	 * Validation for wire form
	 * @param famount
	 * 		amount for bank wire request
	 * @param user
	 * 		requested user for bank wire
	 * @param isFee
	 * 		bank wire with fee or not
	 * @param checkLimits
	 * 		check minimum limit or not
	 * @return
	 * @throws SQLException
	 */
	public static String validateWireForm(String famount, UserBase user, boolean isFee,boolean checkLimits, long writerId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		long amount = CommonUtil.calcAmount(famount);
		
		if ((user.getSkin().isRegulated() || SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM) 
					&& writerId == Writer.WRITER_ID_WEB) {
			// AO Regulation checks
			if (!UserRegulationManager.isCanWithdraw(user.getId())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.minwithdraw1", null), null);
				context.addMessage(null, fm);
				return "error.minwithdraw1";
			}
		}

		Connection con = getConnection();
		try {

			UsersDAOBase.getByUserName(con, user.getUserName(), user);

			if (user.getBalance() < amount ) {

				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.bankWire.highAmount", null),null);
				context.addMessage("wireForm:amount", fm);
				return "error.transaction.bankWire.highamount";

			}

			if (!BonusManagerBase.userCanWithdraw(user.getId(), user.getBalance(), amount)) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("bonus.cannot.withdraw.error", null),null);
				context.addMessage(null, fm);

				return "bonus.cannot.withdraw.error";
			}

			Limit limit = LimitsDAO.getById(con, user.getLimitIdLongValue());
			long wireMinWithdraw = TransactionsManagerBase.getWireMinWithdraw(user.getCurrencyId());
			//Block withdrawal by cheque/Bank wire attempt if user has non visible CC
			HashMap<Long,CreditCard> cc = CreditCardsDAO.getCcWithrawalByUserId(con, user.getId(), wireMinWithdraw, user.getSkinId(), false);
			if (!(ApplicationDataBase.isBackend()) && (cc.size() > 0 )) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minwithdraw1", null),null);
				context.addMessage(null, fm);

				return "error.withdraw.card.not.visible";
			}

			String[] params = new String[1];
	        
			if (isFee) {
				if (amount <= wireMinWithdraw && checkLimits) {
					params[0] = CommonUtil.displayAmount(wireMinWithdraw, user.getCurrencyId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.minbankwire", null),null);
					context.addMessage("wireForm:amount", fm);
					return "error.transaction.bankWire.minamounnt";

				} else {
					if (amount > Long.valueOf(limit.getMaxBankWire()).longValue() && checkLimits) {
						params[0] = CommonUtil.displayAmount(limit.getMaxBankWire(), user.getCurrencyId());
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.maxbankwire", params),null);
						context.addMessage("wireForm:amount", fm);
						return "error.transaction.bankWire.maxamount";
					}
				}
			} else {
				if (amount > Long.valueOf(limit.getMaxBankWire()).longValue() && checkLimits) {
					params[0] = CommonUtil.displayAmount(limit.getMaxBankWire(), user.getCurrencyId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.maxbankwire", params),null);
					context.addMessage("wireForm:amount", fm);

					return "error.transaction.bankWire.maxamounnt";
				}
			}
		} finally {
			closeConnection(con);
		}
		return null;

	}


	/**
	 * Insert Bank wire
	 * @param wire
	 * 		wire instance
	 * @param error
	 * 		if there is an error with the request
	 * @param user
	 * 		the user that creat wire request
	 * @param writerId
	 * 		the writer id
	 * @param feeCancel
	 * 		if this bank wire with fee or not
	 * @throws SQLException
	 */	public static void insertBankWireWithdraw(WireBase wire, String error, UserBase user, long writerId, boolean feeCancel, boolean eftwithdraw, long loginId, QuestionnaireUserAnswer questUserAnswer)	throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log
					.log(
							Level.INFO,
							"Insert BankWire: " + ls + "User:" + user.getUserName() + ls + "famount:" + wire.getAmount() +
								ls + "Swift:" + wire.getSwift() + ls + "Iban:" + wire.getIban() + ls + "AccountNum:" + wire.getAccountNum()
									+ ls + "BankName:" + wire.getBankNameTxt() + ls + "BranchName:" + wire.getBranchName());
		}

		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(wire.getAmount());

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(wire.getAmount()));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
            if (eftwithdraw){
                tran.setTypeId(TRANS_TYPE_EFT_WITHDRAW);
                log.info("****** EFT Withdraw ********");
            } else {
                tran.setTypeId(TRANS_TYPE_BANK_WIRE_WITHDRAW);
            }
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setLoginId(loginId);
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					WiresDAOBase.insertBankWire(con, wire);
					tran.setWireId(new BigDecimal(wire.getId()));
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(wire.getAmount()));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					if (eftwithdraw){
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_EFT_WITHDRAW, user.getUtcOffset());
					} else {
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_BANK_WIRE, user.getUtcOffset());
					}
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser!= null && popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("Insert BankWire finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during bank wire Withdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert BankWire finished with error!");
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during BankWire!! ", e);
		} finally {
			closeConnection(con);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		if(context!=null) {
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
	}


	 //TODO: when starting to work with Envoy withdrawal - need to fix transaction block!
	 /**
		 * Insert Envoy withdraw
		 * @param error
		 * 		if there is an error with the request
		 * @param user
		 * 		the user that creat wire request
		 * @param writerId
		 * 		the writer id
		 * @param feeCancel
		 * 		if this withdraw with fee or not
		 * @throws SQLException
		 */
/*	 public static Transaction insertEnvoyWithdraw(String amount, String error, UserBase user, WireBase account, boolean feeCancel)	throws SQLException {

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO,	"Insert Envoy: " + ls + "User:" + user.getUserName() + ls + "famount:" + amount  + ls);
			}

			Transaction tran = null;
			FacesContext context = FacesContext.getCurrentInstance();

			Connection con = getConnection();

			try {
				con.setAutoCommit(false);

				if (null != account){
					WireDAOBase.insertBankWire(con, account);
					tran.setWireId(new BigDecimal(account.getId()) );
				} else {
					tran.setWireId(null);
				}

				UsersDAOBase.getByUserName(con, user.getUserName(), user);
				long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

				tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(amount));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(0);
				tran.setTimeSettled(null);
				tran.setTimeCreated(new Date());
				tran.setUtcOffsetCreated(user.getUtcOffset());
				tran.setTypeId(TRANS_TYPE_ENVOY_WITHDRAW);
				tran.setUserId(user.getId());
				tran.setWriterId(0);
				tran.setChargeBackId(null);
				tran.setReceiptNum(null);
				tran.setChequeId(null);
				tran.setFeeCancel(feeCancel);

				if (error == null) {
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
				} else {
					tran.setDescription(error);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetCreated(user.getUtcOffset());
				}
				TransactionsDAOBase.insert(con, tran);

				if (error == null) {
					GeneralDAO.insertBalanceLog(con,0,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
							ConstantsBase.LOG_BALANCE_, user.getUtcOffset());
				}

				con.commit();

				log.info("Insert Envoy withdraw finished successfully!");

			} catch (SQLException e) {
				log.error("ERROR during Envoy withdraw!! ", e);
				try {
					con.rollback();
				} catch (SQLException ie) {
					log.log(Level.ERROR, "Can't rollback.", ie);
				}
				throw e;
			} finally {
				con.setAutoCommit(true);
				closeConnection(con);
			}

			context.getApplication().createValueBinding(ConstantsBase.BIND_USER).setValue(context, user);
			return tran;
		}
*/

		/**
		 * get all user deposits for deposit limitaions
		 * @param userId
		 * 		user id
		 * @return
		 * 		sum of user deposits
		 * @throws SQLException
		 */
		public static long getSumDepositsForLimitation(long userId) throws SQLException {
			Connection con = getConnection();
			try {
				return TransactionsDAOBase.getSumDepositsForLimitation(con, userId);
			} finally {
				closeConnection(con);
			}
		}

	    /**
	     * get deposit limits by limitId
	     * @param limitId
	     * 		user limit id
	     * @return
		 * 		HashMap with deposit limits values
		 * 			value1: deposit amount for warnning (limit1)
		 * 			value2: deposit amount for lock account (limit2)
		 * @throws SQLException
	     */
	    public static HashMap<String,Long> getDepositLimits(long limitId) throws SQLException {
	        Connection conn = getConnection();
	        try {
	        	return LimitsDAO.getDepositLimits(conn, limitId);
	        } finally {
	            closeConnection(conn);
	        }
	    }

		/**
		 * send warnning emails to users after deposit(TRANS_STATUS_PENDING)
		 * @param depositVal
		 * 		deposit amout
		 * @param cardId
		 * 		user credit card id
		 * @param writerId
		 * 		writer id - from web it's 1(or from backend writer)
		 * @param user
		 * 		user that operate this deposit
		 * @param firstDeposit
		 * 		true - in case it's his first deposit
		 * @throws SQLException
		 */
		public static void sendEmailByDepositLimits(String depositVal, long writerId, UserBase user, boolean firstDeposit) throws SQLException {

			long sumDeposits = 0;
			HashMap<String,Long> limits = new HashMap<String,Long>();

			if (/*!CommonUtil.isHebrewSkin(user.getSkinId()) && */!CommonUtil.isUserSkinRegulated()) {   // just for anyoption

				 boolean isDocumentsRecived =  user.isIdDocVerify();

				 if ( !isDocumentsRecived ) {   // user didn't send documents

					 limits = getDepositLimits(user.getLimitIdLongValue());
					 sumDeposits = getSumDepositsForLimitation(user.getId());

					 if ( firstDeposit ) {   // on first deposit

						 if ( sumDeposits < limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) ) {

								log.debug("send email to the user for first deposit less than "
										+ CommonUtil.displayAmount(limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1),
												true, user.getCurrencyId()) );
								try {
									UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT1, writerId, user, null, new String(), new String(), null);
								} catch (Exception e) {
									log.error("can't send email (deposit email1) for first deposit! ", e);
								}

						 } else if ( sumDeposits >= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) &&
								 		sumDeposits <= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2) ) {

								log.debug("send email to the user for first deposit between "
										+ CommonUtil.displayAmount(limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1),
												true, user.getCurrencyId()) + " and " +
													CommonUtil.displayAmount(limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2),
														true, user.getCurrencyId()) );
								try {
									UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT2, writerId, user, null, new String(), new String(), null);
								} catch (Exception e) {
									log.error("can't send email (deposit email2) for first deposit! ", e);
								}
							 }
					 } else {  // not first deposit

						 if ( sumDeposits >= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) &&
							 		sumDeposits <= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2) ) {

							log.debug("send email to the user for sum deposits between "
									+ CommonUtil.displayAmount(limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1),
											true, user.getCurrencyId()) + " and " +
												CommonUtil.displayAmount(limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2),
													true, user.getCurrencyId()) );
							try {
								UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT3, writerId, user, null, new String(), new String(), null);
							} catch (Exception e) {
								log.error("can't send email (deposit email3) for deposit! ", e);
							}
						 }

					 }
				 }

			}

		}

    public static FinalizeDeposit finalizeDeposit(FinalizeDeposit fd) throws ClearingException {
		Connection conn = null;
		try {
			conn = getConnection();
			fd = finalizeEPGDeposit(conn, fd);
		} catch (Exception e) {
			log.error("Exception in finalizeDeposit! ", e);
		} finally {
			closeConnection(conn);
		}
		return fd;
    }
    
    /**
     * @param Connection
     * @param FinalizeDeposit
     * @return
     * @throws ClearingException
     * @throws SQLException
     */
    public static FinalizeDeposit finalizeEPGDeposit(Connection conn, FinalizeDeposit fd) throws ClearingException, SQLException {          
        if (fd.isSuccessful()) {
            fd.getTransaction().setStatusId(TRANS_STATUS_SUCCEED);
            fd.getTransaction().setDescription(null);
            fd.getTransaction().setAuthNumber(fd.getAuthNumber());
            fd.getTransaction().setXorIdAuthorize(fd.getProviderTransactionId());
            fd.getTransaction().setXorIdCapture(fd.getProviderTransactionId());
            
            try {
            	conn.setAutoCommit(false);
                UsersDAOBase.addToBalance(conn, fd.getTransaction().getUserId(), fd.getTransaction().getAmount());
                GeneralDAO.insertBalanceLog(conn, fd.getWriterId(), fd.getTransaction().getUserId(), 
                		ConstantsBase.TABLE_TRANSACTIONS, fd.getTransaction().getId(),
                		ConstantsBase.LOG_BALANCE_EPG_CHECKOUT_DEPOSIT, fd.getUtcOffset());
                fd.getTransaction().setComments(fd.getTransaction().getComments() 
                		+ "|" + fd.getResult() + "|" + fd.getMessage() + "|" + fd.getUserMessage() + "|" + 
                		fd.getProviderTransactionId());
                TransactionsDAOBase.updateOtherPayment(conn, fd);
                conn.commit();
            } catch (SQLException e) {
        		log.error("Exception in finalizeEPGDeposit! success case. ", e);
        		try {
        			conn.rollback();
        		} catch (Throwable it) {
        			log.error("Can't rollback.", it);
        		}
        		throw e;
        	} finally {
        		try {
        			conn.setAutoCommit(true);
        		} catch (Exception e) {
        			log.error("Can't set back to autocommit.", e);
        		}
        	}
			try {
				PopulationsManagerBase.deposit(fd.getTransaction().getUserId(), true, fd.getWriterId(), fd.getTransaction());
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}
            try {
                afterTransactionSuccess(fd.getTransaction().getId(), fd.getTransaction().getUserId(), 
                		fd.getTransaction().getAmount(), fd.getWriterId(), fd.getLoginId());
            } catch (Exception e) {
                log.error("Problem processing bonuses after success transaction", e);
            }
        } else {
        	fd.getTransaction().setStatusId(TRANS_STATUS_FAILED);
            fd.getTransaction().setDescription(null);
			try {
				PopulationsManagerBase.deposit(fd.getTransaction().getUserId(), false, fd.getWriterId(), fd.getTransaction());
			} catch (Exception e) {
				log.warn("Problem with population failed deposit event " + e);
			}
			fd.getTransaction().setComments(fd.getTransaction().getComments() +
					"|" + fd.getResult() + "|" + fd.getMessage() + "|" + fd.getUserMessage() + 
					"|" + fd.getProviderTransactionId());
            TransactionsDAOBase.updateOtherPayment(conn, fd);
           
        }
        return fd;
    }

    /**
     * Finish Inatec direct banking transaction. this is done after redirect back to our site.
     * @param t Transaction instance from the context
     * @param txId trIx param
     * @param cs check sum param
     * @param writerId the id of the writer the perform the action
     * @param utcOffset utcOffset value
     * @throws ClearingException
     * @throws SQLException
     */
    public static InatecInfo finishInatecTransaction(Transaction t, String txId, String cs,long writerId,
    		String utcOffset, boolean sha1Verification, long loginId) throws ClearingException, SQLException  {

    	Connection con = null;
    	boolean csVerification = false;
    	InatecInfo info = null;
    	try {
    		if (sha1Verification) {
				// check cs value with Sha1 - Secure Hash Algorithm,
				//Inatec Formula: Checksum: sha1(sha1(txid+"."+REDIRECTSECRET)+"."+sharedsecret)
	    		String csValue = "";
	    		try {
	    			String sharedSecret = ConstantsBase.INATEC_SHARED_SECRET;
	    			if (CommonUtil.getAppData().getIsLive()) {
	    				sharedSecret = ConstantsBase.INATEC_SHARED_SECRET_LIVE;
	    			}
	    			csValue = Sha1.encode(Sha1.encode(txId + "." + t.getInatecRedirectSecret())
									+ "." + sharedSecret);

	    			if (csValue.equals(cs)) {
	    				csVerification = true;
	    			}
	    		} catch (Exception e) {
	    			log.warn("Error operating Sha1 encode! " + e);
	    			csVerification = false;
				}

	    		if (!csVerification) {
	    			log.warn("Inatec cs param verification failed!");
	    		}
    		}

    		info = new InatecInfo();
            info.setTransactionId(t.getId());
            info.setProviderId(t.getClearingProviderId());
            info.setAuthNumber(t.getAuthNumber());
            info.setInatecPaymentType(t.getPaymentTypeId());
            ClearingManager.onlineStatusResult(info);

            if (!info.isSuccessful() &&
            		info.isNeedNotification()) {   // pending status in statusRequest response
            									   // trying to run purchase 1 more time for getting final status
            	ClearingManager.onlineStatusResult(info);
            }

            con = getConnection();
            if (info.isSuccessful()) {  // success
                t.setStatusId(TRANS_STATUS_SUCCEED);
                t.setDescription(null);
                t.setXorIdCapture(info.getAuthNumber());
                try {
                	con.setAutoCommit(false);
	                UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
	                GeneralDAO.insertBalanceLog(con, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, utcOffset);
	                t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
	                TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
	                con.commit();
                } catch (SQLException e) {
            		log.error("Exception in finishInatecTransaction! success case. ", e);
            		try {
            			con.rollback();
            		} catch (Throwable it) {
            			log.error("Can't rollback.", it);
            		}
            		throw e;
            	} finally {
            		try {
            			con.setAutoCommit(true);
            		} catch (Exception e) {
            			log.error("Can't set back to autocommit.", e);
            		}
            	}
    			try {
    				PopulationsManagerBase.deposit(t.getUserId(), true, writerId, t);
    			} catch (Exception e) {
    				log.warn("Problem with population succeed deposit event " + e);
    			}
                try {
                    afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), writerId, loginId);
                } catch (Exception e) {
                    log.error("Problem processing bonuses after success transaction", e);
                }
            } else {  // failed / in progress
        		if (info.isNeedNotification()) {  // need to pull final status from Inatec
        			t.setStatusId(TRANS_STATUS_IN_PROGRESS);
        			log.debug("Inatec direct deposit trx: " + t.getId() + ", need to pull final result");
        		} else {    // failed
            		t.setStatusId(TRANS_STATUS_FAILED);
            		t.setDescription(null);
        			try {
        				PopulationsManagerBase.deposit(t.getUserId(), false, writerId, t);
        			} catch (Exception e) {
        				log.warn("Problem with population failed deposit event " + e);
        			}
        		}
                t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
                TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
           	}
    	} catch (SQLException e) {
    		log.error("Exception in finishInatecTransaction! ", e);
    		throw e;
    	} finally {
    		closeConnection(con);
    	}
    	return info;
    }

    /**
     * Finish Envoy deposit transaction.
     * @param t Transaction instance from the context
     * @param txId trIx param
     * @param utcOffset utcOffset value
     * @throws ClearingException
     * @throws SQLException
     */
    public static void finishEnvoyTransaction(EnvoyInfo info, int notificationType, long loginId) throws ClearingException, SQLException  {

    	Transaction tran = info.getEnvoyTran();
    	if (null != tran){
        	tran.setLoginId(loginId);
        	
            switch (notificationType) {
	        case ConstantsBase.ENVOY_PAY_IN_NOTIFICATION_ID:
	        	finishEnvoyPayInTransaction(info, tran, loginId);
	        	break;
	        case ConstantsBase.ENVOY_PAY_OUT_NOTIFICATION_ID:
	        	finishEnvoyPayOutTransaction(info, tran);
				break;
            }
    	}else{
 		   log.error("Error: Failed to find relevant transaction for notification");
     	}
    }

    private static void finishEnvoyPayInTransaction(EnvoyInfo info, Transaction tran, long loginId) throws ClearingException, SQLException{
    	boolean isDepositSuccess = false;
		String utcOffset = info.getUserOffset();
		Connection con = getConnection();
		long tranId = tran.getId();
		long userId = tran.getUserId();
		boolean tranSuccessfullAndUpdate = false;
		try{
	    	if (tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS ) {
				if (info.isSuccessful()) {
					tran.setStatusId(TRANS_STATUS_SUCCEED);
					tran.setDescription(null);
					tran.setXorIdCapture(info.getAuthNumber());
					tran.setComments(info.getBankInformation());
					isDepositSuccess = true;
					
					try {
					    UserBase user = new UserBase();
					    UsersDAOBase.getByUserId(con, info.getUserId(), user);
					    CommonUtil.alertOverDepositLimitByEmail(tran, user);
					} catch (RuntimeException e) {
					    log.error("Exception sending email alert to traders! ", e);
				}
				} else {  // failed
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setComments(tran.getComments() + "|" + info.getResult() + "|" + info.getMessage());
				}
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(utcOffset);
				tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
				
				// update transaction.
				try {
				    con.setAutoCommit(false);
				
				    if (isDepositSuccess){
				        UsersDAOBase.addToBalance(con, userId, tran.getAmount());
				        GeneralDAO.insertBalanceLog(con, 0, userId, ConstantsBase.TABLE_TRANSACTIONS,
				        		tranId, ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, utcOffset);
				    }
				    TransactionsDAOBase.update(con, tran);
				    con.commit();
				    if (isDepositSuccess) {
				    	tranSuccessfullAndUpdate = true;
				    }
				} catch (SQLException e) {
					log.error("Exception in finishEnvoyPayInTransaction! ", e);
				try {
					con.rollback();
				} catch (Throwable it) {
					log.error("Can't rollback.", it);
					}
					throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Can't set back to autocommit.", e);
					}
				}
				
				try {
					PopulationsManagerBase.deposit(con, userId, isDepositSuccess, 0, tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}
				
				if (isDepositSuccess) {
				    try {
				        afterTransactionSuccess(con,tranId, userId, tran.getAmount(), 0, loginId);
				    } catch (Exception e) {
				        log.error("Problem processing bonuses after success transaction", e);
				    }
				}
			} else {
				log.error("Error: Transaction " + tranId + " is already in status " + tran.getStatusId());
			}
		} finally {
    		closeConnection(con);
		}
    	if (tranSuccessfullAndUpdate) {
    		// Reward plan
    		int countRealDeposit = 0;
    		try {
            	countRealDeposit = TransactionsManagerBase.countRealDeposit(userId);
            	if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) { // check if this first deposit 
            		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT, userId,  
            			tran.getAmount(), tranId, BonusManagerBase.class, (int) Writer.WRITER_ID_AUTO);
            	}
            } catch (Exception e) {
                log.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " RewardUserTasksManager exception after success transaction", e);
            }
    		
    		try {
				if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) {
					new AppsflyerEventSender(tranId, ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT).run();
				}
			} catch (Exception e1) {
				log.error("cant run appsflyer event sender", e1);
			}
    	} 
		
    }

    private static void finishEnvoyPayOutTransaction(EnvoyInfo info, Transaction tran) throws ClearingException, SQLException{
		String utcOffset = info.getUserOffset();
		long fee = 0;
		String amountAfterFee ="";
		long withdrawId = tran.getId();
		Connection con = getConnection();
		try{
	    	if (tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_APPROVED ){

	    		if (info.isSuccessful()) {
		            tran.setStatusId(TRANS_STATUS_SUCCEED);
		            tran.setDescription(null);
		            tran.setXorIdCapture(info.getAuthNumber());
		            tran.setComments(info.getBankInformation());

		            if (!tran.isFeeCancel()){
		            	UserBase user = new UserBase();
		 	            UsersDAOBase.getByUserId(con, tran.getUserId(), user, true);
		 	            fee = LimitsDAO.getFeeByTranTypeId(con, user.getLimitIdLongValue(), TRANS_TYPE_ENVOY_WITHDRAW);

			            if (fee > 0){
			            	tran.setAmount(tran.getAmount() - fee);
			            }
		            }
		            amountAfterFee = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrency().getId());
		        } else {  // failed
		    		tran.setStatusId(TRANS_STATUS_FAILED);
		    		tran.setComments(tran.getComments() + "|" + info.getResult() + "|" + info.getMessage());
		       	}

		        tran.setTimeSettled(new Date());
		        tran.setUtcOffsetSettled(utcOffset);
		        tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);

	            // update transaction.
	        	try {
		            con.setAutoCommit(false);

			        TransactionsDAOBase.update(con, tran);

			        if (fee > 0) {
						tran.setIp(CommonUtil.getIPAddress());
						tran.setDescription(null);
						tran.setComments(null);
						tran.setAmount(fee);
						tran.setProcessedWriterId(CommonUtil.getWebWriterId());
						tran.setWriterId(CommonUtil.getWebWriterId());
						tran.setTimeSettled(new Date());
						tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
						tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
						tran.setReferenceId(new BigDecimal(withdrawId));
						tran.setChequeId(null);

						TransactionsDAOBase.insert(con, tran);

						if (log.isEnabledFor(Level.INFO)) {
							String ls = System.getProperty("line.separator");
							log.log(Level.INFO, " fee transaction for envoy withdraw inserted: " + ls
									+ tran.toString());
						}

						TransactionsDAOBase.updateRefId(con, withdrawId, tran.getId());
					}
			        con.commit();
	        	} catch (SQLException e) {
		    		log.error("Exception in finishEnvoyPayOutTransaction! ", e);
		    		try {
		    			con.rollback();
		    		} catch (Throwable it) {
		    			log.error("Can't rollback.", it);
		    		}
		    		throw e;
		    	} finally {
		    		try {
		    			con.setAutoCommit(true);
		    		} catch (Exception e) {
		    			log.error("Can't set back to autocommit.", e);
		    		}
		    	}

		        if (info.isSuccessful() &&
		        		(tran.getPaymentTypeId() == PAYMENT_TYPE_MONETA ||
		        		tran.getPaymentTypeId() == PAYMENT_TYPE_WEB_MONEY)) {
			        // sending mail to the client
					String ls = System.getProperty("line.separator");
					log.info("Going to send withdrawal email: " + ls +
									"userId: " + tran.getUserId() + ls +
									"trxId: " + withdrawId + ls);
					String templateStr="";
					long mailId;

					if (tran.getPaymentTypeId() == PAYMENT_TYPE_MONETA){
						templateStr = ConstantsBase.TEMPLATE_WITHDRAW_MONETA_SUCCEED;
						mailId = Template.MONETA_WITHDRAWAL_SUCCESS_MAIL_ID;
					}else{
						templateStr = ConstantsBase.TEMPLATE_WITHDRAW_WEBMONEY_SUCCEED;
						mailId = Template.WEBMONEY_WITHDRAWAL_SUCCESS_MAIL_ID;
					}

			        Template template = new Template();
			        template.setSubject("withdrawal.user.email.subject");
			        template.setFileName(templateStr);

			        // MailBox
			    	Template t = TemplatesDAO.get(con, mailId);
					template.setMailBoxEmailType(t.getMailBoxEmailType());

			        // send email to user and to us for tracking
			        try {
			        	UserBase u = new UserBase();
			        	u.setUserName(info.getUserName());
				        String[] splitTo = CommonUtil.getProperty("succeed.email.to").split(";");
			        	UsersManagerBase.sendMailTemplateFile(template, Writer.WRITER_ID_AUTO, u, false,
							null, null, amountAfterFee, null, null, splitTo, withdrawId, null, null, null,
							null, null, null, null, null, null, null);
			        } catch (Exception e) {
			        	log.error("Error, Can't send withdrawal email! " +
			        			"for tran: " + withdrawId + " user: " + info.getUserName() , e);
					}
		        }
		   } else {
			   log.error("Error: Transaction " + withdrawId + " is already in status " + tran.getStatusId());
		   }
		}finally{
    		closeConnection(con);
		}
    }

    /**
     * Do getExpressCheckoutDetails for getting cusomer deails on paypal
     * @param t the transaction that saved in the context (session scope)
     * @param writerId
     * @param utcOffset
     * @param token the PayPal token that we are getting after calling setExpressChechout call
     * @return
     * @throws ClearingException
     * @throws SQLException
     */
//    public static PayPalInfo PayPalGetExpressCheckoutDetails(Transaction t, long writerId,
//    		String utcOffset, String token) throws ClearingException, SQLException  {
//
//    	PayPalInfo info = new PayPalInfo(); //t.getPayPalInfo();
//        info.setToken(token);
//
//        ClearingManager.PayPalGetCustomerDetails(info);
//
//        Connection con = getConnection();
//        try {
//	        if (info.isSuccessful()) {  // success
//	        	log.info("got PayPal customer details.");
//	        	//t.setPayPalInfo(info);  // saving customer details for order confirmation
//	        	t.setComments(info.getResult() + "|" + info.getMessage());
//	        	TransactionsDAOBase.updateComments(con, t.getId(), t.getComments());
//	        } else {  // failed to get customer details
//	    		t.setStatusId(TRANS_STATUS_FAILED);
//	    		t.setDescription(null);
//				try {
//					PopulationsManagerBase.deposit(t.getUserId(), false, writerId, t);
//				} catch (Exception e) {
//					log.warn("Problem with population failed deposit event " + e);
//				}
//	            t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage());
//	            TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
//	       }
//        } catch (SQLException e) {
//    		log.error("Exception in getExpressCheckoutDetails call! ", e);
//    		throw e;
//    	} finally {
//    		closeConnection(con);
//    	}
//       return info;
//    }

    /**
     * Handle expressChecout cancel
     * @param t
     * @param writerId
     * @param utcOffset
     * @throws ClearingException
     * @throws SQLException
     */
    public static void PayPalExpressCheckoutCancel(Transaction t, long writerId, String utcOffset) throws ClearingException, SQLException  {

        Connection con = getConnection();
        try {
        	t.setStatusId(TRANS_STATUS_FAILED);
    		t.setDescription(ConstantsBase.ERROR_PAYPAL_DEPOSIT_CANCELED);
			try {
				PopulationsManagerBase.deposit(t.getUserId(), false, writerId, t);
			} catch (Exception e) {
				log.warn("Problem with population failed deposit event " + e);
			}
            t.setComments(null);
            TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
        } catch (SQLException e) {
    		log.error("Exception in getExpressCheckoutDetails call! ", e);
    		throw e;
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Finish PayPal deposit by DoExpressCheckOutPayment call to PayPal NVP API
     * @param t Transaction instance from the context
     * @param writerId
     * @param utcOffset
     * @return
     * @throws ClearingException
     * @throws SQLException
     */
//    public static PayPalInfo finishPayPalTransaction(Transaction t, long writerId,
//    		String utcOffset, String token, String payerId, String currencySymbol, long loginId) throws ClearingException, SQLException  {
//
//    	Connection con = null;
//    	PayPalInfo info = new PayPalInfo();
//    	info.setToken(token);
//		info.setPayerId(payerId);
//		info.setPaymentType(PayPalClearingProvider.PAYMENT_TYPE_DEPOSIT);
//		info.setAmount(t.getAmount());
//		info.setCurrencySymbol(currencySymbol);
//		info.setCurrencyId(t.getCurrencyId());
//		info.setTransactionId(t.getId());
//		info.setIp(t.getIp());
//
//    	try {
//    		ClearingManager.PayPalDoPayment(info);   // do PalPal payment call
//
//            con = getConnection();
//            if (info.isSuccessful()) {  // success
//                t.setStatusId(TRANS_STATUS_SUCCEED);
//                t.setDescription(null);
//                t.setXorIdCapture(info.getAuthNumber());
//                try {
//                	con.setAutoCommit(false);
//	                UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
//	                GeneralDAO.insertBalanceLog(con, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_PAYPAL_DEPOSIT, utcOffset);
//	                t.setComments(info.getResult() + "|" + info.getMessage());
//	                TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
//	                con.commit();
//                } catch (SQLException e) {
//            		log.error("Exception in finishPayPalTransaction! success case. ", e);
//            		try {
//            			con.rollback();
//            		} catch (Throwable it) {
//            			log.error("Can't rollback.", it);
//            		}
//            		throw e;
//            	} finally {
//            		try {
//            			con.setAutoCommit(true);
//            		} catch (Exception e) {
//            			log.error("Can't set back to autocommit.", e);
//            		}
//            	}
//    			try {
//    				PopulationsManagerBase.deposit(t.getUserId(), true, writerId, t);
//    			} catch (Exception e) {
//    				log.warn("Problem with population succeed deposit event " + e);
//    			}
//                try {
//                    afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), writerId, loginId);
//                } catch (Exception e) {
//                    log.error("Problem processing bonuses after success transaction", e);
//                }
//                try {
//                    UserBase user = new UserBase();
//                    UsersDAOBase.getByUserId(con, info.getUserId(), user);
//                    CommonUtil.alertOverDepositLimitByEmail(t, user);
//                } catch (RuntimeException e) {
//                    log.error("Exception sending email alert to traders! ", e);
//                }
//            } else {  // failed
//        		t.setStatusId(TRANS_STATUS_FAILED);
//        		t.setDescription(null);
//    			try {
//    				PopulationsManagerBase.deposit(t.getUserId(), false, writerId, t);
//    			} catch (Exception e) {
//    				log.warn("Problem with population failed deposit event " + e);
//    			}
//    			t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage());
//    			TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, CommonUtil.getWebWriterId());
//           }
//    	} catch (SQLException e) {
//    		log.error("Exception in finishPayPalTransaction! ", e);
//    		throw e;
//    	} finally {
//    		closeConnection(con);
//    	}
//    	return info;
//    }

    /**
     * Validate credit card expiration
     * @param cc creditCard instance
     * @return
     */
    public static String validateCardExpiration(CreditCard cc) {
    	String error = null;
    	FacesContext context = FacesContext.getCurrentInstance();

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, 1);

		Calendar expCal = Calendar.getInstance();
		expCal.clear();
		expCal.set(Integer.parseInt(cc.getExpYear()) + 2000, Integer.parseInt(cc.getExpMonth()), 1);

		if (cal.after(expCal)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.creditcard.expdate", null),null);
			context.addMessage("withdrawForm:ccNum", fm);
			error = ConstantsBase.ERROR_CARD_EXPIRED;
		}

    	return error;
    }

    /**
     * Is first succeed deposit
     * @param userId user id
     * @return
     * @throws SQLException
     */
	public static boolean isFirstDeposit(long userId, long expectedValue) throws SQLException {
		Connection con = getConnection();
		try {
			return isFirstDeposit(con, userId, expectedValue);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean isFirstDeposit(Connection con, long userId, long expectedValue) throws SQLException {
		return TransactionsDAOBase.isFirstDeposit(con, userId, expectedValue);
	}

	public static void afterTransactionSuccess(long transactionId, long userId, long amount, long writerId, long loginId) throws SQLException {
		// rewards
		int countRealDeposit = 0;
		try {
			countRealDeposit = TransactionsManagerBase.countRealDeposit(userId);
			if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) { // check if this first deposit
				RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT, userId,  
						amount,  transactionId, BonusManagerBase.class, (int) writerId);
			}
	    } catch (Exception e) {
	        log.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " ERROR RewardUserTasksManager exception after success transaction", e);
	    }
		
		try {
			if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) {
				new AppsflyerEventSender(transactionId, ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT).run();
			}
		} catch (Exception e1) {
			log.error("cant run appsflyer event sender", e1);
		}
		// bonus 
		Connection conn = null;
		try {
            conn = getConnection();
            afterTransactionSuccess(conn, transactionId, userId, amount, writerId, loginId);
		 } catch (SQLException sqle) {
	           log.error("Problem processing bonuses after transaction");
	            throw sqle;
        } finally {
            closeConnection(conn);
        }
		
		Transaction tr = TransactionsManagerBase.getTransaction(transactionId);
		if(tr.getClassType() == TransactionsManagerBase.TRANS_CLASS_DEPOSIT){
			//Regultion POP Documents
			popDocumentsActionCreate(userId, tr);
			UsersManagerBase.updateLastDepositOrInvestDate(userId);
			log.debug("If need notife with email manegers for reached total deposits for userId:" + userId);
			UsersAutoMailDaoManagerBase.notifeReachedTotalDeposit(userId);
		}
	}

    public static void afterTransactionSuccess(Connection conn, long transactionId, long userId, long amount, long writerId, long loginId) throws SQLException {
        ArrayList<BonusUsers> bonuses = BonusDAOBase.getBonusesForActivation(conn, userId);
        boolean isBonusActivated = false;
        boolean isActivateBonus;

        for (BonusUsers bu : bonuses) {
        	isActivateBonus = false;

        	BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());

        	if (null != bh){

            	// Check whether current bonus should be activated
        		try {
					isActivateBonus = bh.isActivateBonusAfterTransactionSuccess(conn, bu, amount, transactionId);
				} catch (BonusHandlersException e) {
					log.error("Error on activation check for bonus user id " + bu.getId());
					throw new SQLException();
				}

            	// if need to activate bonus
            	if (isActivateBonus && !isBonusActivated){

                	isBonusActivated = true;
                	try {
						bh.activateBonusAfterTransactionSuccess(conn, bu, transactionId, userId, amount, writerId, loginId);
					} catch (BonusHandlersException e) {
						log.error("Error on activating bonus user id " + bu.getId());
						throw new SQLException();
					}
            	}
        	}
        }

        InvestmentsManagerBase.afterTransactionSuccess(userId);
    }

    /**
     * Convert points to cash money action
     * @param points
     * @param user
     * @param writerId
     * @return
     * @throws SQLException
     */
    public static boolean insertPointsToCashDeposit(long points, UserBase user, long writerId, long loginId) throws SQLException {

    	TierUser tU = user.getTierUser();
    	double amount = TiersManagerBase.convertPointsToCash(points, tU.getTierId());
    	long updatedPoints = tU.getPoints() - points;

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Converting points to cash deposit: " + ls + " "
					+ user.getTierUser().toString() + ls + " amount: " + amount );
		}
		Connection con = null;
		boolean flag = true;

		try {
			// insert convert points to cash bonus
			BonusUsers bu = new BonusUsers();
			bu.setUserId(user.getId());
			bu.setBonusStateId(ConstantsBase.BONUS_STATE_ACTIVE);
			bu.setWriterId(writerId);
			bu.setBonusAmount(CommonUtil.calcAmount(amount));
			con = getConnection();
			bu.setBonusId(BonusDAOBase.getConvertPointsBonusIdBySkin(con, user.getSkinId()));

			// insert point to cash transaction with bonus_id reference
			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(user.getUtcOffset());
			tran.setTypeId(TRANS_TYPE_POINTS_TO_CASH);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setBonusUserId(bu.getId());
			tran.setLoginId(loginId);

			try {
				con.setAutoCommit(false);
				log.log(Level.DEBUG,"Going to insert new convertPointsToCash bonus to user: " + bu.getUserId());
				BonusDAOBase.insertConvertedPointsBonus(con, bu, user.getCurrencyId());
				TransactionsDAOBase.insert(con, tran);
				UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(amount));
				GeneralDAO.insertBalanceLog(con, writerId,
						tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
								.getId(), ConstantsBase.LOG_BALANCE_POINTS_TO_CASH, user.getUtcOffset());

				// subtract points from user
				TiersDAOBase.updateUserTier(con, user.getTierUser().getId(), writerId, updatedPoints);

				TierUserHistory tH = TiersManagerBase.getTierUserHisIns(tU.getTierId() ,user.getId(), TiersManagerBase.TIER_ACTION_CONV_POINTS_TO_CASH,
						writerId, updatedPoints, -points, tran.getId(), ConstantsBase.TABLE_TRANSACTIONS, user.getUtcOffset());
				TiersManagerBase.insertIntoTierHistory(con, tH);
	            try {
	                CommonUtil.alertOverDepositLimitByEmail(tran, user);
	            } catch (RuntimeException e) {
	                log.error("Exception sending email alert to traders! ", e);
	            }
				con.commit();
			} catch (SQLException e) {
				log.error("Exception in insert PointsToCashDeposit! ", e);
				flag = false;
				try {
					con.rollback();
				} catch (Throwable it) {
					log.error("Can't rollback.", it);
				}
				throw e;
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (Exception e) {
					log.error("Can't set back to autocommit.", e);
				}
			}
			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Convert points to cash money deposit insert successfully! " + ls
						+ tran.toString());
			}
		} catch (SQLException e) {
			log.log(Level.ERROR, "Error processing convert points to cash! ", e);
			flag = false;
			throw e;
		} finally {
			closeConnection(con);
		}
    	return flag;
    }

//    /**
//     * Insert bonus withdrawal to user
//     * this function operated after cancel bonus to user
//     * @param amount bonus deposit amount that should withdraw now
//     * @param userId the user that want to withdraw with
//     * @param utcOffset utcOffset of the user
//     * @param writerId writer that operate this action
//     * @param bonusUserId bonusUserId reference
//     * @return
//     * @throws SQLException
//     */	public static boolean insertBonusWithdraw(Connection con, long amount, long userId, String utcOffset, long writerId, long bonusUserId) throws SQLException {
//
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, "Insert Bonus Withdraw: " + ls + "userId: "
//					+ userId + ls);
//		}
//
//		try {
//			Transaction tran = new Transaction();
//			tran.setAmount(amount);
//			tran.setComments(null);
//			tran.setCreditCardId(null);
//			tran.setDescription("log.balance.bonus.withdraw");
//			tran.setIp(CommonUtil.getIPAddress());
//			tran.setProcessedWriterId(writerId);
//			tran.setTimeSettled(new Date());
//			tran.setTimeCreated(new Date());
//			tran.setUtcOffsetCreated(utcOffset);
//			tran.setUtcOffsetSettled(utcOffset);
//			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW);
//			tran.setUserId(userId);
//			tran.setWriterId(writerId);
//			tran.setStatusId(TRANS_STATUS_SUCCEED);
//			tran.setChequeId(null);
//			tran.setWireId(null);
//			tran.setChargeBackId(null);
//			tran.setReceiptNum(null);
//			tran.setBonusUserId(bonusUserId);
//
//			try {
//				con.setAutoCommit(false);
//				UsersDAOBase.addToBalance(con, userId, -amount);
//				TransactionsDAOBase.insert(con, tran);
//				GeneralDAO.insertBalanceLog(con, writerId,
//						tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
//								.getId(), ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW, utcOffset);
//				con.commit();
//			} catch (SQLException e) {
//				log.log(Level.ERROR, "insertBonusWithdraw: ", e);
//				try {
//					con.rollback();
//				} catch (SQLException ie) {
//					log.log(Level.ERROR, "Can't rollback.", ie);
//				}
//				throw e;
//			} finally {
//				try {
//					con.setAutoCommit(true);
//				} catch (Exception e) {
//					log.error("Can't set back to autocommit.", e);
//				}
//			}
//			if (log.isEnabledFor(Level.INFO)) {
//				String ls = System.getProperty("line.separator");
//				log.log(Level.INFO, "Bonus Withdraw insert successfully! " + ls
//						+ tran.toString());
//			}
//		} catch (SQLException e) {
//			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
//			throw e;
//		}
//		return true;
//	}

	    /**
     * update Transaction
     * @param userId user id
     * @return
     * @throws SQLException
     */
	public static void updateTransaction(Transaction tran) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAOBase.update(con, tran);
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction getTransactionByEpacsRef(String epacsReference) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getTransactionByEpacsRef(con, epacsReference);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Direct banking deposit with Envoy service
	 * @throws SQLException
	 */
	public static Transaction insertDirectDepositEnvoy(Transaction tran, UserBase user) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls +
                    "amount: " + tran.getAmount() + ls +
                    "userId: " + user.getId() + ls);
		}

		Connection con = getConnection();
		try {
			tran.setDescription(null);
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(CommonUtil.getWebWriterId());
			tran.setChequeId(null);
			tran.setTimeCreated(new Date());
			tran.setUserId(user.getId());
			tran.setWriterId(CommonUtil.getWebWriterId());
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setClearingProviderId(ClearingManager.ENVOY_PROVIDER_ID);

			if (tran.getStatusId() == TRANS_STATUS_FAILED ||
					tran.getStatusId() == TRANS_STATUS_CANCELED ||
					tran.getStatusId() == TRANS_STATUS_SUCCEED) {
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setTimeSettled(null);
         	    tran.setUtcOffsetSettled(null);
			}

			TransactionsDAOBase.insert(con, tran);
			log.info("insert directDeposit finished successfully! ");
		} catch (Exception e) {
			return null;
		} finally {
			closeConnection(con);
		}

		return tran;
	}

	public static boolean insertBonusDeposit(UserBase user, long bonusUserId,
			long writerId, long amount, String comments, long loginId) throws SQLException {

		Connection con = getConnection();
		try {
			return insertBonusDeposit(user,bonusUserId,writerId,amount,comments,con, loginId);

		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean insertBonusDeposit(User user, long bonusUserId,
			long writerId, long amount, String comments,Connection con, long loginId) throws SQLException {
		
		try {
			boolean retval = insertBonusDepositBase(user,bonusUserId,writerId,amount,comments, CommonUtil.getIPAddress(), con, loginId);
			if(retval) {
				FacesContext context = FacesContext.getCurrentInstance();
				if (Writer.WRITER_ID_AUTO != writerId && Writer.WRITER_ID_WEB != writerId) {
					String[] params = new String[1];
					params[0] = CommonUtil.displayAmount(amount, user.getCurrencyId());
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
							CommonUtil.getMessage("deposit.success", params), null);
					context.addMessage(null, fm);

				}
				
			}
			return retval;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert PayPal withdrawal
	 * @param amount
	 * @param payPalEmail paypal email of the user
	 * @param error
	 * @param user
	 * @param writerId
	 * @param feeCancel
	 * @throws SQLException
	 */
	public static void insertPayPalWithdrawal(String amount, String payPalEmail, String error, UserBase user, long writerId, boolean feeCancel, long loginId)	throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info("Insert PayPal withdrawal: " + ls + "User:" + user.getUserName() + ls + "famount:" + amount +
								 ls + "email:" + payPalEmail + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setTypeId(TRANS_TYPE_PAYPAL_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setPayPalEmail(payPalEmail);
			tran.setLoginId(loginId);
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_PAYPAL_WITHDRAW, user.getUtcOffset());
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("Insert payPalWithdrawal finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during payPalWithdrawal! ", e);
					try {
					    con.rollback();
					} catch (SQLException ie) {
						log.log(Level.ERROR, "Can't rollback.", ie);
					}
					throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert payPalWithdrawal finished with error!");
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during payPalWithdrawal!! ", e);
		} finally {
			closeConnection(con);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), user);
	}

	/**
	 * Deposit with Ukash service
	 * @throws SQLException
	 */
	public static Transaction insertDepositUkash(String depositVal,
			long writerId, UserBase user, int source, UkashInfo ukashInfo, long voucherValueLong, long voucherCurrencyId, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert Ukash deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Transaction tran = null;
		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);

			tran = new Transaction();
			tran.setAmount(deposit);
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_UKASH_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setClearingProviderId(ukashInfo.getProviderId());
			tran.setLoginId(loginId);

			TransactionsDAOBase.insert(con, tran);

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}

			boolean timeout = false;

			try {

				UkashInfo ui = new UkashInfo(user, tran);
				ui.setUserCurrencyCode(ukashInfo.getUserCurrencyCode());
				ui.setPayeeName(ukashInfo.getPayeeName());
				ui.setVoucherNumber(ukashInfo.getVoucherNumber());
				ui.setVoucherValue(ukashInfo.getVoucherValue());
				if (log.isInfoEnabled()) {
					String ls = System.getProperty("line.separator");
					log.info("Transaction Clearing started: Clearing Info: " + ui.toString() + ls);
				}

				try {
					ClearingManager.directDepositUkash(ui);
				} catch (ClearingException ce) { // Clearing failed
					if (ce.getCause() instanceof SocketTimeoutException) {
						timeout = true;
					}
					log.error("Failed to authorize transaction: " + ui.toString(), ce);
					ui.setResult("999");
					ui.setSuccessful(false);
					ui.setMessage(ce.getMessage());
				}

				tran.setAuthNumber(ui.getProviderTransactionId());
				String comment = "";
				if (null != ui.getResult()) {
					comment += ui.getResult() + "|";
				}
				if (null != ui.getMessage()) {
					comment += ui.getMessage() + "|";
				}
				if (null != ui.getUserMessage()) {
					comment += ui.getUserMessage() + "|";
				}

				if (comment.length() > 0) {
					comment = comment.substring(0, comment.length()-1);
				}

				tran.setComments(comment);
				UkashDeposit ukashDeposit = new UkashDeposit();

				if (ui.isSuccessful()) {   // success Ukash
					if (log.isInfoEnabled()) {
						log.info("Successful response from Ukash");
					}
					tran.setXorIdCapture(ui.getProviderTransactionId());
					tran.setAmount(ui.getAmount());
					ukashDeposit.setConvertedAmount(ui.getAmount());
					ukashDeposit.setDifferentCurrencies(ui.isDifferentCurrencies());
				} else {  // failed Ukash
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction failed UkashDeposit! " + ui.toString() + ls);
					}
					tran.setStatusId(TRANS_STATUS_FAILED);
					FacesMessage fm = null;
					fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.ukash.deposit", null), null);
					context.addMessage(null, fm);

					if (timeout) {
						tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
					} else {
						tran.setDescription("error.ukash.deposit");
					}
					 // update transaction on failed cases
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
				}

				ukashDeposit.setTransactionId(ui.getTransactionId());
				ukashDeposit.setVoucherNumber(ui.getVoucherNumber());
				ukashDeposit.setVoucherValue(ui.getVoucherValue());
				ukashDeposit.setVoucherCurrency(ui.getVoucherCurrency());
				ukashDeposit.setStatus(ui.getStatus());
				ukashDeposit.setVoucherValueLong(voucherValueLong);
				ukashDeposit.setVoucherCurrencyId(voucherCurrencyId);
				UkashDepositDAO.insert(con, ukashDeposit);
				tran.setUkash(ukashDeposit);

				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
				log.info("insert Ukash Deposit finished successfully! " + description);

			} catch (Exception exp) {
				log.error("Exception in insert Ukash Deposit! ", exp);
			}

			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);

			if (tran.getStatusId() == TRANS_STATUS_STARTED) {  // Update trx + user balance after success from Ukash
            	try {
            		con.setAutoCommit(false);
            		tran.setStatusId(TRANS_STATUS_SUCCEED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
					GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_UKASH_DEPOSIT, user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
					con.commit();
            	} catch (Exception e) {
            		log.error("Exception in success deposit Transaction! ", e);
    				try {
    					con.rollback();
    				} catch (Throwable it) {
    					log.error("Can't rollback.", it);
    				}
            	} finally {
                    try {
                        con.setAutoCommit(true);
                    } catch (Exception e) {
                        log.error("Can't set back to autocommit.", e);
                    }
        		}

            	try {
    				PopulationsManagerBase.deposit(user.getId(), true, writerId,tran);
    			} catch (Exception e) {
    				log.warn("Problem with population succeed deposit event " + e);
    			}

                try {
                    afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
                } catch (Exception e) {
                    log.error("Problem processing bonuses after success transaction", e);
                }
                try {
                    CommonUtil.alertOverDepositLimitByEmail(tran, user);
                } catch (RuntimeException e) {
                    log.error("Exception sending email alert to traders! ", e);
                }
	            return tran;
			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}

	private static boolean isFailureRepeat(Connection conn, long userId, BigDecimal cardId, long amount , long transactionId, boolean firstDeposit) throws SQLException {
		return TransactionsDAOBase.isFailureRepeat(conn, userId, cardId, amount , transactionId, firstDeposit);
	}

	/**
	 * Return all creditCard for Inatec CFT withdraw
	 * @param id user id
	 * @return	Array List of credit cards
	 * @throws SQLException
	 */
	public static HashMap<Long,CreditCard> getInatecCftCcWithrawalByUserId(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getInatecCftCcWithrawalByUserId(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static CreditCard getCreditCardByUserId(	long ccNum,
													long userId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getByNumberAndUserId(con, ccNum, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction getFirstWithdrawal(long userId, long ccId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getFirstWithdrawal(con, userId, ccId);
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction getFirstWireWithdrawalByUser(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getFirstWireWithdrawalByUser(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert envoy withdrawal
	 * @throws SQLException
	 */
	public static void insertWithdrawEnvoyService(String famount, long paymentMethodId, UserBase user, long writerId,
			boolean feeCancel, String error, String envoyAccountNum, long loginId, QuestionnaireUserAnswer questUserAnswer)	throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO,	"Insert Envoy Withdraw: " + ls +
								"User:" + user.getUserName() + ls +
								"famount:" + famount + ls +
								"paymentMethodId:" + paymentMethodId + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(famount);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(famount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setTypeId(TRANS_TYPE_ENVOY_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setPaymentTypeId(paymentMethodId);
			tran.setEnvoyAccountNum(envoyAccountNum);
			tran.setLoginId(loginId);
			
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			if (error == null) {  // error not found

				try {
					con.setAutoCommit(false);

					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(famount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_ENVOY_WITHDRAW, user.getUtcOffset());
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
					con.commit();
					log.info("Insert Withdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during Withdraw! ", e);
					try {
						con.rollback();
					} catch (SQLException ie) {
						log.log(Level.ERROR, "Can't rollback.", ie);
					}
					throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else { // error found
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert Withdraw finished with error!");
			}
		} catch (SQLException e) {
			log.error("ERROR during Withdraw!! ", e);
		} finally {
			closeConnection(con);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), user);
	}

	/**
	 * Get fee By limit id and tran type
	 * @throws SQLException
	 */
	@Deprecated
	public static long getFeeByTranTypeId(long limitId, int tranTypeId) throws SQLException {
		Connection con = getConnection();
		try {
			return LimitsDAO.getFeeByTranTypeId(con, limitId, tranTypeId);
		} finally {
			closeConnection(con);
		}
	}
	@Deprecated
	public static long getFeeByTranTypeId(Limit limit, int tranTypeId) {
		switch (tranTypeId) {
		case TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW:
			return limit.getCcFee();
		case TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW:
			return limit.getChequeFee();
		case TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW:
			return limit.getBankWireFee();
//		case TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE:
//			return (long) (limit.getAmountForLowWithdraw() * TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE_COEFFICIENT);
		default:
			return 0;
		}
	}

	/**
	 * Get payment method by Id
	 * @param conn db connection
	 * @param paymentMethodId
	 * @return
	 * @throws SQLException
	 */
    public static PaymentMethod getPaymentMethodById(long paymentMethodId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getPaymentMethodById(con, paymentMethodId);
		} finally {
			closeConnection(con);
		}
	}

    /**
	 * Get Envoy Account Numbers By UserId
	 * @param conn db connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<String> getEnvoyAccountNumsByUserId(long userId, long paymentMethodId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getEnvoyAccountNumsByUserId(con, userId, paymentMethodId);
		} finally {
			closeConnection(con);
		}
    }

    public static ArrayList<SelectItem> getWebMoneyPursesByUserId(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getWebMoneyPursesByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
    }

	public static ArrayList<SelectItem> getEnvoyAccountNumsByUserIdList(long userId, long paymentMethodId) throws SQLException {

		ArrayList list = getEnvoyAccountNumsByUserId(userId, paymentMethodId);
		ArrayList<SelectItem> listOut = new ArrayList<SelectItem>();
		for (int i = 0; i < list.size(); i++) {
			listOut.add(new SelectItem((String)list.get(i),(String)list.get(i)));
		}
		return listOut;
	}

	public static long handlePaymentMethodId(String paymentMethodName) throws SQLException {
		Connection con = getConnection();
		long paymentMethodId = 0;
		try {
			paymentMethodId =  TransactionsDAOBase.getPaymentMethodId(con, paymentMethodName);

			if (0 == paymentMethodId){
				paymentMethodId = TransactionsDAOBase.insertNewPaymentMethod(con, paymentMethodName, true);
			}
		} catch (SQLException e) {
			log.error(" Can't get payment method",e);
			throw e;
		}finally {
			closeConnection(con);
		}
		return paymentMethodId;
	}

    public static boolean isCcInBinsList(long ccNumber) throws SQLException{
    	Connection con = getConnection();
    	try {
    		return CreditCardsDAOBase.isCcInBinsList(con, ccNumber);
		} catch (SQLException e) {
			log.error(" Can't get isCcInBinsList ",e);
		}finally {
			closeConnection(con);
    	}
		return false;
    }

	/**
	 * Get  payment methods with transactions
	 * @param conn db connection
	 * @param paymentMethods list
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<PaymentMethod> getTransactionsPaymentMethodList() throws SQLException {
    	Connection con = getConnection();
    	try {
    		return TransactionsDAOBase.getTransactionsPaymentMethodList(con);
		} catch (SQLException e) {
			log.error(" Can't get paymentMethods list ",e);
		}finally {
			closeConnection(con);
    	}
		return null;
    }

	public static void updatePixelRunTimeByList(String ids) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updatePixelRunTimeByList(con, ids);
		} finally {
			closeConnection(con);
		}
	}


	/** Insert Moneybookers transaction
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @param moneyBookersUserName
	 * @return
	 * @throws SQLException
	 */
	public static Transaction insertMoneybookersDeposit(String depositVal, long writerId, UserBase user,
			 	String language,String hpURL, int source, String formName, String imagesDomain, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert Moneybookers transaction deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Transaction tran = null;
		String error = null;
		Connection con = getConnection();
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			long deposit = CommonUtil.calcAmount(depositVal);
			error = validateDepositLimits(con, formName, user, deposit, source, true);
			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_MONEYBOOKERS_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			long providerId = ClearingManager.getProvidersIdByPaymentGroupAndBusinessCase().get(user.getSkin().getBusinessCaseId()).get(ClearingManager.CLEARING_PAYMENT_GROUP_MONEYBOOKERS);
			tran.setClearingProviderId(providerId);
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			tran.setLoginId(loginId);
			TransactionsDAOBase.insert(con, tran);
			MoneybookersInfo mbInfo = new MoneybookersInfo(user, tran.getAmount(), tran.getTypeId(), tran.getId(), tran.getIp(),
															tran.getAcquirerResponseId(), language, hpURL, user.getEmail(), depositVal,
															CommonUtil.getAppData().getIsLive(), imagesDomain);
			ClearingManager.setMoneybookersDepositDetails(mbInfo, providerId);
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_MONEYBOOKERS_INFO, MoneybookersInfo.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), mbInfo); // for saving moneybookersDetails
	        log.info("Moneybookers fields: " + "\n" +
	        			"cancelURL=" + mbInfo.getCancelURL() + "\n" +
	        			"returnURL=" + mbInfo.getReturnURL() + "\n" +
	        			"statusUTL=" + mbInfo.getStatusURL() + "\n" +
	        			"language="  + mbInfo.getLanguage()  + "\n" +
	        			"currency="  + mbInfo.getCurrency() + "\n" +
	        			"depositAmount=" + mbInfo.getDepositAmount() + "\n" +
	        			"RedirectURL = " + mbInfo.getRedirectURL() + "\n" +
	        			"Pay_to_email = " + mbInfo.getPay_to_email() + "\n" +
	        			"StatusURL = " + mbInfo.getStatusURL() + "\n" +
	        			"Language = " + mbInfo.getLanguage() + "\n" +
	        			"Pay_from_email = " + mbInfo.getPay_from_email() + "\n" +
	        			"TransactionId = " + mbInfo.getTransactionId() + "\n" +
	        			"merchantAccount = " + mbInfo.getMerchantAccount());

			log.info("insert Moneybookers transaction finished successfully! ");
			if (null != error){ // deposit failed due to minimum limitation
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}
			}
		} catch (Exception e) {
			log.error("Unable to insert moneybookers/skrill deposit", e);
			return null;
		} finally {
			closeConnection(con);
		}
		return tran;
	}

	public static String getDeltaPayDepositMin(UserBase user) throws SQLException{
		Connection con = getConnection();
        Limit limit = LimitsDAO.getById(con, user.getLimitIdLongValue());
        limit.setCurrencyId(user.getCurrencyId());
        return limit.getMinDepositTxt();

	}

	public static Distribution getDistributedProviderId(DistributionType type, long userId) throws SelectorException, SQLException {
		// this user is already distributed to a provider
		Long existing = DistributionManager.getAssignedProvider(userId, type);
		if(existing != null) {
			return new Distribution(type, existing, 0);
		}
		// this user is not assigned to a provider
		Connection con = getConnection();
		// prepare selectors
		Map<Long,Integer> providerPercentageMap = DistributionDAO.getProvidersMapForDistribution( con, type.getValue());
		PercentageDistributionSelector<Long> pdt = new PercentageDistributionSelector<Long>(providerPercentageMap);
		ArrayList<Long> failedProviders = DistributionDAO.getFailedProviderIds(con, userId, type.getValue());
		ArrayList<Long> allProvidersIds = new ArrayList<Long>();
		allProvidersIds.addAll(providerPercentageMap.keySet());
		NewProviderSelector nps = new NewProviderSelector(failedProviders, allProvidersIds);
		HundredPercentSelector hps = new HundredPercentSelector(providerPercentageMap);
		ArrayList<Selector<Long>> selectors = new ArrayList<Selector<Long>>();
		selectors.add(hps);
		selectors.add(nps);
		selectors.add(pdt);
		DistributionMaker distributor = new DistributionMaker(selectors, type);
		return distributor.getClearingProviderId(userId);
	}

	/** Insert DeltaPayChinaDeposit transaction
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @param moneyBookersUserName
	 * @return
	 * @throws SQLException
	 */
	public static Transaction insertDeltaPayChinaDeposit(String depositVal, long writerId, UserBase user,
			 	String language,String hpURL, int source, String formName, String imagesDomain, int selectorId, long providerID, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert DeltaPayChina transaction deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Transaction tran = null;
		String error = null;
		Connection con = getConnection();
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			long deposit = CommonUtil.calcAmount(depositVal);
			error = validateDepositLimits(con, formName, user, deposit, source, true);
			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CUP_CHINA_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setClearingProviderId(providerID);
			tran.setSelectorId(selectorId);
			tran.setLoginId(loginId);
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			TransactionsDAOBase.insert(con, tran);



			//save the data for request to provider(deltaPay)
			DeltaPayInfo deltaInfo = new DeltaPayInfo(user, tran, hpURL);
			deltaInfo.setFirstName(user.getFirstName());
			deltaInfo.setLastName(user.getLastName());
			deltaInfo.setAmount(tran.getAmount());
			deltaInfo.setTransactionId(tran.getId());
			// params sending to phaseListener
			deltaInfo.setErrorMessageFromRequset(error);

			if (null != error) {
				tran.setComments("Request has been send, didn't get answer or the customer at the provider site");
			}
			TransactionsManagerBase.updateTransaction(tran);

			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_DELTAPAY_INFO, DeltaPayInfo.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), deltaInfo); // for saving deltaPayDetail

	        ValueExpression ve2 = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_TRANSACTION, Transaction.class);
	        ve2.getValue(context.getELContext());
	        ve2.setValue(context.getELContext(), tran); // for saving transaction

	        log.info("DeltaPay request:  \n" +
		        "affiliate: " + deltaInfo.getAffiliate() + "\n" +
				"paymethod: Credit Card" + "\n" +
				"processing_mode: sale" + "\n" +
				"redirect: " + deltaInfo.getRedirect() + "\n" +
				"location: AFF" + "\n" +
				"order_id: " + deltaInfo.getTransactionId() + "\n" +
				"terminal_name: " + deltaInfo.getTerminalName() + "\n" +
				"agent_name: " + deltaInfo.getAgentName() + "\n" +
				"first_name: " + deltaInfo.getFirstName() + "\n" +
				"last_name: " + deltaInfo.getLastName() + "\n" +
				"address1: " + deltaInfo.getAddress() + "\n" +
				"city: " + user.getCityName() + "\n" +
				"state: NA" + "\n" +
				"country: " + user.getCountry().getA2() + "\n" +
				"zip: " + user.getZipCode() + "\n" +
				"telephone: " + user.getMobilePhone() + "\n" +
				"amount: " + deltaInfo.getFullAmountStr() + "\n" +
				"currency: " + user.getCurrency().getNameKey() + "\n" +
				"email: " + user.getEmail() + "\n" +
				"card_type: " + deltaInfo.getCardTypeName() + "\n" +
				"card_number: " + CommonUtil.getNumberXXXX(deltaInfo.getCcn()) + "\n" +
				"cvv: " + "*****" + "\n" +
				"expiry_mo: " + deltaInfo.getExpireMonth() + "\n" +
				"expiry_yr: " + deltaInfo.getExpireYear() + "\n");

			if (null != error){ // deposit failed due to minimum limitation
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}
			}
		} catch (Exception e) {
			return null;
		} finally {
			closeConnection(con);
		}
		return tran;
	}



    /**
     * Update Moneybookers transaction status
     * @param requestParameterMap
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static void updateMoneybookerTransactionStatus(Map requestParameterMap, long loginId) throws SQLException, NoSuchAlgorithmException, UnsupportedEncodingException{
    	Connection con = null;
    	MoneybookersInfo mbInfo = new MoneybookersInfo();
    	if (!mbInfo.handleStatusUrl(requestParameterMap)){
    		return;
    	}
    	String secret = CommonUtil.getAppData().getIsLive()? mbInfo.SECRET_WORD_LIVE : mbInfo.SECRET_WORD_TEST;
		String val = mbInfo.getMerchant_id()+  mbInfo.getTransaction_id() +	md5digest(secret) + mbInfo.getMb_amount()
						+ mbInfo.getMb_currency() + mbInfo.getStatus();
		//check Moneybookers encrypted data
		if (!mbInfo.getMd5sig().equals(md5digest(val))){
			log.info("Mismatching between our calculated value and md5sig from Moneybookers: " + "\n"
					 + "val=" + val + "\n"
					 + "md5sig=" + mbInfo.getMd5sig());
			return;
		}
		Transaction transaction = getTransaction(Long.valueOf(mbInfo.getTransaction_id()));
		// prevent Moneybookers sending us the same trx request more then once
		if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
			log.warn("Getting Moneybookers status URL of non authenticate transaction!" + transaction.getId());
			return;
		}
		transaction.setTimeSettled(new Date());
		transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
		con = getConnection();
		try {
			if (mbInfo.getStatus().equals(MoneybookersClearingProvider.STATUS_PROCESSED_CODE)) { // success
				double mbAmount = Double.valueOf(mbInfo.getTransactionAmount()) * 100;
				if (Math.abs(mbAmount - transaction.getAmount()) > 1){ //when receiving wrong amount from Moneybookers.
					log.warn("Mismatching between Moneybookers transaction amount and our transaction amount: \n" +
							 "Moneybookers transaction amount: " + mbAmount  + "\n" +
							 "Our transaction amount: " + transaction.getAmount());
					transaction.setStatusId(TRANS_STATUS_FAILED);
					transaction.setComments("-1| " + "Failed transaction | Amount mismatch. Moneybookers amount:" + mbAmount / 100 +", our transaction amount:" + transaction.getAmount() / 100 + " | "  );
				} else {
					transaction.setStatusId(TRANS_STATUS_SUCCEED);
					transaction.setComments("1000| " + "Permitted transaction |" );
				}
			} else {
				transaction.setStatusId(TRANS_STATUS_FAILED);
				transaction.setComments("-1| " + "Failed transaction |" );
			}
			transaction.setComments(transaction.getComments() + mbInfo.getMessage());
			transaction.setMoneybookersEmail(mbInfo.getPay_from_email());
			transaction.setAuthNumber(mbInfo.getMb_transaction_id());
			transaction.setXorIdCapture(mbInfo.getMb_transaction_id());
			TransactionsDAOBase.update(con, transaction);

			if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
				try {
					PopulationsManagerBase.deposit(transaction.getUserId(), true, transaction.getWriterId(),transaction);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}

	            try {
	                afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
	            } catch (Exception e) {
	            	log.error("Problem processing bonuses after success transaction", e);
	            }

	            try {
	        		con.setAutoCommit(false);
					UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
					GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_MONEYBOOKERS_DEPOSIT, transaction.getUtcOffsetCreated());
					TransactionsDAOBase.update(con, transaction);

		            try {
		            	UserBase user = UsersDAOBase.getUserById(con, transaction.getUserId());
		                CommonUtil.alertOverDepositLimitByEmail(transaction, user);
		            } catch (RuntimeException e) {
		                log.error("Exception sending email alert to traders! ", e);
		            }

					con.commit();
	        	} catch (Exception e) {
	        		log.error("Exception in success deposit Transaction! ", e);
					try {
						con.rollback();
					} catch (Throwable it) {
						log.error("Can't rollback.", it);
					}
	        	} finally {
	                try {
	                    con.setAutoCommit(true);
	                } catch (Exception e) {
	                    log.error("Can't set back to autocommit.", e);
	                }
	    		}

			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
			}
		} finally {
			closeConnection(con);
		}

    }

    /**
     *
     * @param transaction_id
     * @param msid
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @throws NumberFormatException
     * @throws SQLException
     * @throws ClearingException
     */
    public static Transaction moneybookersReturnURL(String transaction_id, String msid) throws NoSuchAlgorithmException, UnsupportedEncodingException, NumberFormatException, SQLException, ClearingException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	MoneybookersInfo mbInfo = new MoneybookersInfo();
    	Transaction transaction = getTransaction(Long.valueOf(transaction_id));
    	ClearingManager.setMoneybookersDepositDetails(mbInfo, transaction.getClearingProviderId());
    	String secret = CommonUtil.getAppData().getIsLive()? mbInfo.SECRET_WORD_LIVE : mbInfo.SECRET_WORD_TEST;
		String val =  mbInfo.getMerchant_id() + transaction_id + md5digest(secret);
		//	check Moneybookers encrypted data to ensure that user returned to our site from Moneybookers
		if (!msid.equals(md5digest(val))){
			log.info("Mismatching between our calculated value and md5sig from Moneybookers: " + "\n"
					+ "val=" + val + "\n"
					+ "mdsid=" + msid);
			FacesMessage fm = null;
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
			context.addMessage(null, fm);
			return null;
		}
		if (null == transaction || transaction.getStatusId() != TransactionsManagerBase.TRANS_STATUS_SUCCEED){ //failed
			FacesMessage fm = null;
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
			context.addMessage(null, fm);
		}
		return transaction;
    }

	/**
	 * Encrypt via MD5
	 * @param val
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	private static String md5digest(String val) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		return toHexStr(md5.digest(val.getBytes("UTF-8")));
	}

	private static String toHexStr(byte[] t) {
		StringBuffer out = new StringBuffer();
		for (int i = 0; i < t.length; i++) {
			int x = (t[i] & 0xFF);
			out.append(x < 16 ? "0" : "");
			out.append(Integer.toHexString(x).toUpperCase());
		}
		return out.toString();
	}

	/**
	 * User chose to cancel Moneybookers transaction
	 * @param tran
	 * @param writerId
	 * @throws SQLException
	 */
	public static void moneybookersCancelTransaction(Transaction tran, long writerId) throws SQLException{
        Connection con = getConnection();
        try {
        	tran.setStatusId(TRANS_STATUS_FAILED);
    		tran.setDescription(ConstantsBase.ERROR_MONEYBOOKERS_DEPOSIT_CANCELED);
    		tran.setTimeSettled(new Date());
    		tran.setUtcOffsetSettled(tran.getUtcOffsetCreated());
			try {
				PopulationsManagerBase.deposit(tran.getUserId(), false, writerId, tran);
			} catch (Exception e) {
				log.warn("Problem with population failed deposit event " + e);
			}
            tran.setComments(null);
            TransactionsDAOBase.update(con, tran);
        } catch (SQLException e) {
    		log.error("Exception in updating Moneybookers cancel status, " + "transactionId= " + tran.getId() , e);
    		throw e;
    	} finally {
    		closeConnection(con);
    	}
	}

	public static long getLastXCCTransactionsCountByUser(long userId, long lastXDays) throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getLastXCCTransactionsCountByUserAndDate(con, userId, new Date().getTime() - (lastXDays *24*60*60*1000));
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * insert Moneybookers Withdraw
	 * @param error
	 * 		if there is an error with the request
	 * @param user
	 * 		the user that creat wire request
	 * @param writerId
	 * 		the writer id
	 * @param feeCancel
	 * 		if this bank wire with fee or not
	 * @throws SQLException
	 */	public static void insertMoneybookersWithdraw(String amount, String moneybookersEmail, String error, UserBase user, long writerId, boolean feeCancel)	throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO,
							"insertMoneybookersWithdraw " + ls + "User:" + user.getUserName() + ls + "famount:" + amount +
								ls + "moneybookersEmail:" + moneybookersEmail);
		}


		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setTypeId(TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setMoneybookersEmail(moneybookersEmail);
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_MONEYBOOKERS_WITHDARW, user.getUtcOffset());
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("insert MoneybookersWithdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during MoneybookersWithdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert MoneybookersWithdraw finished with error!");
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during MoneybookersWithdraw!! ", e);
		} finally {
			closeConnection(con);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		if(context != null) {
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
	}

	 /**
		 * insert DeltaPayChina Withdraw
		 * @param error
		 * 		if there is an error with the request
		 * @param user
		 * 		the user that creat wire request
		 * @param writerId
		 * 		the writer id
		 * @param feeCancel
		 * 		if this bank wire with fee or not
		 * @throws SQLException
		 */	public static void insertDeltaPayChinaWithdraw(String amount, String error, UserBase user, long writerId, boolean feeCancel, long loginId)	throws SQLException {

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO,
								"insertDeltaPayChinaWithdraw " + ls + "User:" + user.getUserName() + ls + "famount:" + amount);
			}

			FacesContext context = FacesContext.getCurrentInstance();
			Connection con = getConnection();

			try {
				UsersDAOBase.getByUserName(con, user.getUserName(), user);
				long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

				Transaction tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(amount));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(writerId);
				tran.setCreditWithdrawal(true);// for deltaPAy cft -- only true
				tran.setTimeCreated(new Date());
				tran.setUtcOffsetCreated(user.getUtcOffset());
	            tran.setTypeId(TRANS_TYPE_CUP_CHINA_WITHDRAW);
				tran.setUserId(user.getId());
				tran.setWriterId(writerId);
				tran.setChargeBackId(null);
				tran.setReceiptNum(null);
				tran.setChequeId(null);
				tran.setFeeCancel(feeCancel);
				tran.setLoginId(loginId);
				if(feeCancel){
					tran.setWithdrawalFeeExemptWriterId(writerId);
				}

				if (error == null) { // no error
					try {
						con.setAutoCommit(false);
						tran.setDescription(null);
						tran.setStatusId(TRANS_STATUS_REQUESTED);
						tran.setTimeSettled(null);
						UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
						user.setBalance(newBalance);
						TransactionsDAOBase.insert(con, tran);
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW, user.getUtcOffset());
						con.commit();
						PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
						if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
							PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
						}
						log.info("insert DeltaPayChinaWithdraw finished successfully!");
					} catch (SQLException e) {
						log.error("ERROR during DeltaPayChinaWithdraw! ", e);
						try {
							    con.rollback();
							} catch (SQLException ie) {
								log.log(Level.ERROR, "Can't rollback.", ie);
							}
							throw e;
					} finally {
						try {
							con.setAutoCommit(true);
						} catch (Exception e) {
							log.error("Cannot set back to autoCommit! " + e);
						}
					}
				} else {  // error
					tran.setWireId(null);
					tran.setDescription(error);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.insert(con, tran);
					log.info("Insert MoneybookersWithdraw finished with error!");
				}
				try{
					RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
						tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
				} catch (Exception e) {
					log.error("Problem with risk Alert handler ", e);
				}
			} catch (SQLException e) {
				log.error("ERROR during MoneybookersWithdraw!! ", e);
			} finally {
				closeConnection(con);
			}
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}

	/**
	 * Insert WebMoney Withdraw
	 * @param error
	 * 		if there is an error with the request
	 * @param user
	 * 		the user that creat wire request
	 * @param writerId
	 * 		the writer id
	 * @param feeCancel
	 * 		if this bank wire with fee or not
	 * @throws SQLException
	 */	public static void insertWebMoneyWithdraw(String amount, String error, UserBase user, long writerId,
			 boolean feeCancel, String webMoneyPurse, long loginId, QuestionnaireUserAnswer questUserAnswer) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO,
						"insert WebMoney Withdraw " + ls +
						"User:" + user.getUserName() + ls +
						"amount:" + amount);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setTypeId(TRANS_TYPE_WEBMONEY_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setWebMoneyPurse(webMoneyPurse);
			tran.setLoginId(loginId);
			if(feeCancel){
				tran.setWithdrawalFeeExemptWriterId(writerId);
			}
			if (user.getCurrencyId() == ConstantsBase.CURRENCY_USD_ID){
				tran.setClearingProviderId(ClearingManager.WEBMONEY_PROVIDER_ID_USD);
			}
			if (user.getCurrencyId() == ConstantsBase.CURRENCY_EUR_ID){
				tran.setClearingProviderId(ClearingManager.WEBMONEY_PROVIDER_ID_EUR);
			}
			if (user.getCurrencyId() == ConstantsBase.CURRENCY_RUB_ID){
				tran.setClearingProviderId(ClearingManager.WEBMONEY_PROVIDER_ID_RUB);
			}

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_WEBMONEY_WITHDRAW, user.getUtcOffset());
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("insert WebMoney Withdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during  WebMoney Withdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert WebMoney Withdraw finished with error!");
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during  WebMoney Withdraw!! ", e);
		} finally {
			closeConnection(con);
		}
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), user);
	}

	 public static String getLastMoneybookersEmail(long userId) {
		Connection con = null;
    	try {
    		con = getConnection();
    		return TransactionsDAOBase.getLastMoneybookersEmail(con, userId);
		} catch (SQLException e) {
			log.error(" Can't get LastMoneybookersEmail list ",e);
		}finally {
			closeConnection(con);
    	}
		return ConstantsBase.EMPTY_STRING;
	 }


		/** Insert WebMoney transaction
		 * @param depositVal
		 * @param writerId
		 * @param user
		 * @return
		 * @throws SQLException
		 */
//		public static Transaction insertWebMoneyDeposit(String depositVal, long writerId, UserBase user,
//				 	String hpURL, int source, String formName, long clearingProviderId, long loginId) throws SQLException {
//
//			if (log.isInfoEnabled()) {
//				String ls = System.getProperty("line.separator");
//				log.info(ls + "Insert WebMoney transaction deposit:" + ls +
//	                    "amount: " + depositVal + ls +
//	                    "writer: " + writerId + ls +
//	                    "userId: " + user.getId() + ls);
//			}
//
//			Transaction tran = null;
//			String error = null;
//			Connection con = getConnection();
//			FacesContext context = FacesContext.getCurrentInstance();
//
//			try {
//				long deposit = CommonUtil.calcAmount(depositVal);
//				error = validateDepositLimits(con, formName, user, deposit, source, true);
//				tran = new Transaction();
//				tran.setAmount(CommonUtil.calcAmount(depositVal));
//				tran.setComments(null);
//				tran.setCreditCardId(null);
//				tran.setDescription(null);
//				tran.setIp(CommonUtil.getIPAddress());
//				tran.setProcessedWriterId(writerId);
//				tran.setChequeId(null);
//				tran.setTimeSettled(null);
//				tran.setTimeCreated(new Date());
//				tran.setTypeId(TRANS_TYPE_WEBMONEY_DEPOSIT);
//				tran.setUserId(user.getId());
//				tran.setWriterId(writerId);
//				tran.setChargeBackId(null);
//				tran.setStatusId(TRANS_STATUS_STARTED);
//				tran.setCurrency(user.getCurrency());
//				tran.setUtcOffsetCreated(user.getUtcOffset());
//				tran.setUtcOffsetSettled(null);
//				tran.setClearingProviderId(clearingProviderId);
//				tran.setLoginId(loginId);
//				if (null != error) {
//					tran.setStatusId(TRANS_STATUS_FAILED);
//					tran.setDescription(error);
//					tran.setTimeSettled(new Date());
//					tran.setUtcOffsetSettled(user.getUtcOffset());
//				} else {
//					tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
//				}
//				TransactionsDAOBase.insert(con, tran);
//				WebMoneyInfo wmInfo = new WebMoneyInfo(user, tran, hpURL, depositVal, CommonUtil.getAppData().getIsLive());
//				ClearingManager.setWebMoneyDepositDetails(wmInfo, clearingProviderId);
//		        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_WEBMONEY_INFO, WebMoneyInfo.class);
//		        ve.getValue(context.getELContext());
//		        ve.setValue(context.getELContext(), wmInfo); // for saving WebMoney details
//		        String ls = "\n";
//		        log.info("WebMoney fields: " + ls +
//		        		"redirectURL=" + wmInfo.getRedirectURL() + ls +
//		        		"depositAmount=" + wmInfo.getDepositAmount() + ls +
//		        		"transactionId=" + wmInfo.getTransactionId() + ls +
//		        		"lmi_payee_purse=" + wmInfo.getLmi_payee_purse() + ls
//		        		//"Test mode = " + ap  + "lmi_sim_mode=" + wmInfo.getLmi_sim_mode()
//		        );
//
//				log.info("insert WebMoney transaction finished successfully! ");
//				if (null != error){ // deposit failed due to minimum/maximum limitation
//					try {
//						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
//					} catch (Exception e) {
//						log.warn("Problem with population failed deposit event " + e);
//					}
//				}
//			} catch (Exception e) {
//				return null;
//			} finally {
//				closeConnection(con);
//			}
//			return tran;
//		}

	    /**
	     * Update WebMoney transaction status
	     * @param requestParameterMap
	     * @throws SQLException
	     * @throws NoSuchAlgorithmException
	     * @throws UnsupportedEncodingException
	     */
//	    public static void updateWebMoneyTransactionStatus(Map requestParameterMap, long loginId) throws SQLException, NoSuchAlgorithmException, UnsupportedEncodingException{
//	    	Connection con = null;
//	    	WebMoneyInfo wmInfo = new WebMoneyInfo();
//	    	if (!wmInfo.handleResultUrl(requestParameterMap)){
//	    		return;
//	    	}
//	    	//insert Webmoeny Return URL to DB
//			WebMoneyDeposit wmDeposit = new WebMoneyDeposit();
//			wmDeposit.setTransactionId(wmInfo.getTransactionId());
//			wmDeposit.setLmi_payment_amount(wmInfo.getLmi_payment_amount());
//			wmDeposit.setLmi_mode(wmInfo.getLmi_mode());
//			wmDeposit.setLmi_sys_invs_no(wmInfo.getLmi_sys_invs_no());
//			wmDeposit.setLmi_sys_trans_no(wmInfo.getLmi_sys_trans_no());
//			wmDeposit.setLmi_payer_purse(wmInfo.getLmi_payer_purse());
//			wmDeposit.setLmi_payer_wm(wmInfo.getLmi_payer_wm());
//			wmDeposit.setLmi_hash(wmInfo.getLmi_hash());
//			wmDeposit.setLmi_sys_trans_date(wmInfo.getLmi_sys_trans_date());
//			con = getConnection();
//			WebMoneyDepositDAO.insert(con, wmDeposit);
//
//	    	String secret = CommonUtil.getAppData().getIsLive()? wmInfo.SECRET_WORD_LIVE : wmInfo.SECRET_WORD_TEST;
//	    	String val = wmInfo.getLmi_payee_purse() + wmInfo.getLmi_payment_amount() + wmInfo.getLmi_payment_no()
//	    					+ wmInfo.getLmi_mode() + wmInfo.getLmi_sys_invs_no() + wmInfo.getLmi_sys_trans_no()
//	    					+ wmInfo.getLmi_sys_trans_date() + secret +  wmInfo.getLmi_payer_purse() + wmInfo.getLmi_payer_wm();
//
//			//Check WebMoney encrypted data
//			if (!wmInfo.getLmi_hash().equals(md5digest(val))){
//				log.info("Mismatching between our calculated value and md5sig from WebMoney: " + "\n"
//						 + "val=" + val + "\n"
//						 + "md5sig=" + wmInfo.getLmi_hash());
//				return;
//			}
//
//			Transaction transaction = getTransaction(Long.valueOf(wmInfo.getTransactionId()));
//			// prevent WebMoney sending us the same trx response more then once
//			if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
//				log.warn("Getting WebMoney status URL of non authenticate transaction!" + transaction.getId());
//				return;
//			}
//			
//			//Validate - deposit amount equality.
//			log.info("about to Validate - deposit amount equality. transaction id: " + transaction.getId());
//			if (transaction.getAmount() != new BigDecimal(wmInfo.getLmi_payment_amount()).multiply(new BigDecimal("100")).longValue()) {
//				log.info("Validate - deposit amount equality. transaction id: " + transaction.getId() + " NOT PASS VALIDATION AMOUNT");
//				log.info("transaction amount (first phase): " + transaction.getAmount() + "\r\n" +
//						"webmoney amount (second phase): " + new BigDecimal(wmInfo.getLmi_payment_amount()).multiply(new BigDecimal("100")).longValue() + "\r\n");
//				
//				String from = CommonUtil.getConfig("email.from");
//				String to = CommonUtil.getConfig("deposit.error.email");				
//				String subject = "Webmoney ruble - deposit failed - mismatch deposit amount";
//		        String body = 	"user id: " + transaction.getUserId() + "\r\n" +
//		        				"transaction id: " + transaction.getId() + "\r\n" +
//		        				"transaction amount (first phase): " + transaction.getAmount() + "\r\n" +
//		        				"webmoney amount (second phase): " + new BigDecimal(wmInfo.getLmi_payment_amount()).multiply(new BigDecimal("100")).longValue() + "\r\n" +
//		        				"Merchant’s purse to which the customer has made the payment (lmi_payee_purse):  " + wmInfo.getLmi_payee_purse() + "\r\n" +
//		        				"Number of a purchase in accordance with the accounting system of the merchant received by the service from the website of the merchant (lmi_payment_no): " + wmInfo.getLmi_payment_no() + "\r\n" +
//		        				"Purse from which the customer has made the payment (lmi_payer_purse): " + wmInfo.getLmi_payer_purse();
//		        						
//		        CommonUtil.sendEmailMsg(from, to, subject, body);
//		        log.info("sent email to: " + to  + "\r\n");
//				return;
//			}
//			log.info("Validated - deposit amount equality. transaction id: " + transaction.getId() + " PASS VALIDATION AMOUNT");
//			
//			transaction.setTimeSettled(new Date());
//			transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
//			try {
//				if (wmInfo.getLmi_hash() != null && !wmInfo.getLmi_hash().isEmpty() &&
//						wmInfo.getLmi_sys_invs_no() != null && !wmInfo.getLmi_sys_invs_no().isEmpty()) { //success
//					transaction.setStatusId(TRANS_STATUS_SUCCEED);
//					transaction.setComments("1000| " + "Permitted transaction |" );
//					transaction.setAuthNumber(wmInfo.getLmi_sys_trans_no());
//					transaction.setXorIdCapture(wmInfo.getLmi_sys_trans_no());
//				} else {
//					transaction.setStatusId(TRANS_STATUS_FAILED);
//					transaction.setComments("-1| " + "Failed transaction |" );
//				}
//				TransactionsDAOBase.update(con, transaction);
//				if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
//					try {
//						PopulationsManagerBase.deposit(transaction.getUserId(), true, transaction.getWriterId(),transaction);
//					} catch (Exception e) {
//						log.warn("Problem with population succeed deposit event ", e);
//					}
//		            try {
//		                afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
//		            } catch (Exception e) {
//		            	log.error("Problem processing bonuses after success transaction", e);
//		            }
//		            try {
//		        		con.setAutoCommit(false);
//						UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
//						GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_WEBMONEY_DEPOSIT, transaction.getUtcOffsetCreated());
//						TransactionsDAOBase.update(con, transaction);
//
//			            try {
//			            	UserBase user = UsersDAOBase.getUserById(con, transaction.getUserId());
//			                CommonUtil.alertOverDepositLimitByEmail(transaction, user);
//			            } catch (RuntimeException e) {
//			                log.error("Exception sending email alert to traders! ", e);
//			            }
//
//						con.commit();
//		        	} catch (Exception e) {
//		        		log.error("Exception in success deposit Transaction! ", e);
//						try {
//							con.rollback();
//						} catch (Throwable it) {
//							log.error("Can't rollback.", it);
//						}
//		        	} finally {
//		                try {
//		                    con.setAutoCommit(true);
//		                } catch (Exception e) {
//		                    log.error("Can't set back to autocommit.", e);
//		                }
//		    		}
//
//				} else { // deposit failed
//					try {
//						try {
//							PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
//						} catch (Exception e) {
//							log.warn("Problem with population failed deposit event ", e);
//						}
//					} catch (Exception e) {
//						log.error("Could not send email.", e);
//					}
//				}
//			} finally {
//				closeConnection(con);
//			}
//	    }


	    /**
	     * Set WebMoney transaction status to failed when user gets to fail URL
	     * @param tranId
	     * @throws SQLException
	     */
	    public static void updateWebMoneyTransactionFailURL(long tranId) throws SQLException {
	    	FacesContext context = FacesContext.getCurrentInstance();
	    	Connection con = null;
	    	con = getConnection();
	    	Transaction transaction = getTransaction(Long.valueOf(tranId));
			transaction.setTimeSettled(new Date());
			transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
			transaction.setStatusId(TRANS_STATUS_FAILED);
			transaction.setComments("-1| " + "Failed transaction |" );
	    	try {
	    		TransactionsDAOBase.update(con, transaction);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);
				try {
					PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event ", e);
				}
			} catch (Exception e) {
				log.error("Error update WebMoney Transaction FailURL ", e);
			} finally {
				closeConnection(con);
			}
	    }

	    /**
	     * Set Transaction status to failed when there is no matching transaction Id and getting to succeed URL
	     * @param transaction_id
	     * @param msid
	     * @throws NoSuchAlgorithmException
	     * @throws UnsupportedEncodingException
	     * @throws NumberFormatException
	     * @throws SQLException
	     * @throws ClearingException
	     */
//	    public static Transaction webMoneySuccessURL(String transaction_id) throws NoSuchAlgorithmException, UnsupportedEncodingException, NumberFormatException, SQLException, ClearingException {
//	    	FacesContext context = FacesContext.getCurrentInstance();
//	    	//Only relevant to WebMoney - set WebMoney clearing provider configuration
//	    	WebMoneyInfo wmInfo = new WebMoneyInfo();
//	    	ClearingManager.setWebMoneyDepositDetails(wmInfo, ClearingManager.WEBMONEY_PROVIDER_ID_RUB);
//			Transaction transaction = getTransaction(Long.valueOf(transaction_id));
//			if (null == transaction || transaction.getStatusId() != TransactionsManagerBase.TRANS_STATUS_SUCCEED){ //failed
//				FacesMessage fm = null;
//				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
//				context.addMessage(null, fm);
//			}
//			return transaction;
//	    }

	    public static long getDepositAlertLimits(long limitId){
	    	Connection con = null;
	    	long threshold_deposit = 0;
			try {
				con = getConnection();
				threshold_deposit = LimitsDAO.getDepositAlertLimits(con, limitId);
			} catch (SQLException e) {
				log.error(" Can't get deposit alert ",e);
			} finally {
				closeConnection(con);
	    	}
			return threshold_deposit;
	    }

		public static long getSumOfDepositsForRank(long userId) throws SQLException {
			Connection con = getConnection();
			try {
				return TransactionsDAOBase.getSumOfDepositsForRank(con, userId);
			} finally {
				closeConnection(con);
			}
		}

	@Deprecated
	public static boolean startReroutingProcess(ClearingInfo ci, ClearingRoute cr, CreditCard cc, Transaction tran, UserBase user,
												String depositVal, TransactionBin tb,
												long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
			ClearingInfo newCi = new ClearingInfo(ci);
			ArrayList<ErrorCode> list = ApplicationDataBase.getClearingErrorCode();
			boolean errorExist = false;
			boolean reroutedSucccessfully = false;
			ErrorCode ec = new ErrorCode();
			HashMap<Long, Long> priorityOfreroute = ConfigManager.getPriorityOfProvider();
			long clearingGroup = 0;
			for(ErrorCode e : list){
				if(ci.getResult().equalsIgnoreCase(e.getResult()) &&   e.getClearingProviderId() == ci.getProviderId()){
					errorExist = true;
					ec.setId(e.getId());
					ec.setSpecialCode(e.isSpecialCode());
					break;
				}
			}
			clearingGroup = ClearingManager.getproviderGroupById(ci.getProviderId());
			long check = 0 ;
			for(check = 1 ; check < priorityOfreroute.size()+1 ; check++){// remove current provider from priority list
				Object key = priorityOfreroute.get(check);
				if((Long)key == clearingGroup){
					priorityOfreroute.remove(check);
				}
			}

			if(errorExist){//getting limit for this transaction
				ArrayList<Long> cpNotToReroute = ClearingManager.getRelevantlimits(ci, cr);
				for(int i = 0 ;i < cpNotToReroute.size(); i++){
					if(priorityOfreroute.containsValue(cpNotToReroute.get(i))){
						boolean foundProvider = false;
						for(long j = 1 ; !foundProvider ; j++){
							if(priorityOfreroute.containsKey(j)){
								long value = priorityOfreroute.get(j);
								long limitValue = cpNotToReroute.get(i);
								if(value == limitValue){
									priorityOfreroute.remove(j);
									foundProvider = true;
								}
							}
						}
					}
				}

				if(!(priorityOfreroute.size() > 0 )){
					log.error("No clearing Providers to reroute");
					return false;
				}
			} else {
				return false;
			}
			if (!ec.isSpecialCode()) {// check days or faliures
				//check x_days OR 20_failures
				String ccBin = ci.getCcn();
				ccBin = ccBin.substring(0, 6);
				if(!checkDaysOrFailuresToBin(cr,Long.parseLong(ccBin), tb)){
					log.error("No consecutive errors");
					return false;
				}
			}

			long x = 1;
			long index = 1;
			int count = priorityOfreroute.size();
			long currency = ci.getCurrencyId();
			HashMap<Long, Long> hmClearingProvider = new HashMap<Long, Long>();
			while(count > 0){// change the groupId to clearing provider id
				if(priorityOfreroute.containsKey(x)){
				if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_INATEC){
					if(currency == ConstantsBase.CURRENCY_USD_ID){
						hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_USD);
						index++;
					} else if(currency == ConstantsBase.CURRENCY_EUR_ID){
						hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_EUR);
						index++;
					}else if (currency == ConstantsBase.CURRENCY_GBP_ID){
						hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_GBP);
						index++;
					} else if (currency == ConstantsBase.CURRENCY_TRY_ID){
						hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_TRY);
						index++;
					}
				} else if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_TBI){
					hmClearingProvider.put(index, ClearingManager.TBI_AO_PROVIDER_ID);
					index++;
				} else if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_WIRE_CARD){
					hmClearingProvider.put(index, ClearingManager.WC_PROVIDER_ID);
					index++;
				}
				count--;
				}
				x++;
			}
			index = 1;
			boolean madeRerouteAttempt = false;
			HashMap<Long, Transaction> hmNewTran = new HashMap<Long, Transaction>();
			while(hmClearingProvider.size() > 0 && !reroutedSucccessfully){// make try to deposit by reroute
				if(hmClearingProvider.containsKey(index)){
				newCi.setProviderId(hmClearingProvider.get(index));
				tran.setReroutingTranId(tran.getId());
				hmNewTran.put(newCi.getProviderId(), performReroutingAttemptTran(tran, newCi, user, depositVal, loginId));
				madeRerouteAttempt = true;
				if(newCi.isSuccessful()){
					reroutedSucccessfully = true;
					if(cr.getStartBIN()!= null && cr.getStartBIN() != ""){
						ClearingManager.updateClearingProviderAndLog(cr, cc, hmClearingProvider.get(index), ec.getId());  
					}				
				}
				hmClearingProvider.remove(index);
				}
				index++;				
			}	
			if(madeRerouteAttempt && !reroutedSucccessfully){
				ClearingManager.updateTimeLastReroute(cr.getId());
				TransactionsManagerBase.updateTimeLastRerouteFailed(tb);
			}
			
			return reroutedSucccessfully;
			
		}
		
//					ArrayList<ClearingRoute> crList = new ArrayList<ClearingRoute>();
//					if(cr.getStartBIN()!= null && cr.getStartBIN() != "" && !ec.isSpecialCode()){
//						if(isInatecClearingProvider(hmClearingProvider.get(index))){
//							crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
//							insertClearingRoutesForInatec(crList, cr, cc, hmClearingProvider.get(index), ec.getId());
//							crList = ClearingManager.getClearingRouteByBin(cr.getStartBIN());//getClearingRouteById(cr.getId());
//							for (ClearingRoute temp : crList){
//								if(		temp.getCurrnecyId() == 0  &&  temp.getCcType() == 0 &&  temp .getCountryId() == 0 && temp.getSkinId() == 0){
//									try {
//										ClearingManager.updateClearingRouteIsActive(false, temp.getId(), cc, ec.getId());
//									} catch (Exception e1) {
//										// TODO Auto-generated catch block
//										log.debug(e1);
//									}
//								}
//							}
//						} else {
//							ClearingManager.updateClearingProviderAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), false);
//							ClearingRoute temp = ClearingManager.getClearingRouteById(cr.getId());
//							if(		temp.getCurrnecyId() > 0  ||  temp.getCcType() > 0 ||  temp .getCountryId() > 0 || temp.getSkinId() > 0 ){
//								ArrayList<ClearingRoute> crArr = ClearingManager.getClearingRouteByBin(cr.getStartBIN());
//								if(crArr.size() > 0 ){
//									if(!crArr.get(0).isActive()){
//										try {
//											ClearingManager.updateClearingRouteIsActive(true, crArr.get(0).getId(), cc, ec.getId());
//										} catch (Exception e1) {
//											// TODO Auto-generated catch block
//											log.debug(e1);
//										}
//									}
//									ClearingManager.updateClearingProviderAndLog(crArr.get(0), cc, hmClearingProvider.get(index), ec.getId(), isInatecClearingProvider(hmClearingProvider.get(index)));
//								}else {
//									ClearingManager.insertNewClearingRouteAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), 0, isInatecClearingProvider(hmClearingProvider.get(index)));
//								}
//							}
//							crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
//							for(ClearingRoute vo : crList){
//								if(isInatecClearingProvider(vo.getDepositClearingProviderId())){
//									try {
//										ClearingManager.updateClearingRouteIsActive(false, vo.getId(), cc, ec.getId());
//									} catch (Exception e1) {
//										// TODO Auto-generated catch block
//										log.debug("Error Updateing", e1);
//									}
//								}
//							}
//						}
//						if(isInatecClearingProvider(cr.getDepositClearingProviderId())){
//							crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
//							for(ClearingRoute vo : crList){
//								try {
//									ClearingManager.updateClearingRouteIsActive(false, vo.getId(), cc, ec.getId());
//								} catch (Exception e1) {
//									// TODO Auto-generated catch block
//									e1.printStackTrace();
//								}
//							}
//						}
//					} else if (cr.getStartBIN() == null && !ec.isSpecialCode()){
//						long crId = ClearingManager.logicForUpdateClearingRoute(cc.getBinStr(), hmClearingProvider.get(index));
//						long newClearingRouteId = 0;
//						if(isInatecClearingProvider(hmClearingProvider.get(index))){
//							crList = ClearingManager.existclearingRouteWithCurrnecy(cc.getBinStr());
//							insertClearingRoutesForInatec(crList, cr, cc, hmClearingProvider.get(index), ec.getId());
//						} else {
//							if(crId > 0){
//								newClearingRouteId = crId;
//								try {
//									ClearingManager.updateClearingRouteIsActive(true, crId, cc, ec.getId());
//								} catch (Exception e1) {
//									// TODO Auto-generated catch block
//									e1.printStackTrace();
//									log.debug(e1);
//								}
//							}else{
//								newClearingRouteId = ClearingManager.insertNewClearingRouteAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), 0, isInatecClearingProvider(hmClearingProvider.get(index)));
//								crList = ClearingManager.existclearingRouteWithCurrnecy(cc.getBinStr());
//								for(ClearingRoute vo : crList){
//									try {
//										ClearingManager.updateClearingRouteIsActive(false, crId, cc, ec.getId());
//									} catch (Exception e1) {
//										// TODO Auto-generated catch block
//										e1.printStackTrace();
//									}
//								}
//							}
//						}
//						ClearingRoute crTemp = new ClearingRoute();
//						crTemp = ClearingManager.getClearingRouteById(newClearingRouteId);
//						if(crTemp != null){
//							cr = crTemp;
//						}
//					}
//				}
//				hmClearingProvider.remove(index);
//				}
//				index++;
//			}
//			if(madeRerouteAttempt && !reroutedSucccessfully){
//				ClearingManager.updateTimeLastReroute(cr.getId());
//				TransactionsManagerBase.updateTimeLastRerouteFailed(tb);
//			}
//
//			return reroutedSucccessfully;
//
//		}
//
//		private static void insertClearingRoutesForInatec(ArrayList<ClearingRoute> crList, ClearingRoute cr, CreditCard cc, long ProviderId, long errorCodeId) {
//			boolean tryCurr = false,eurCurr = false,usdCurr = false,gbpCurr = false;
//			for(ClearingRoute vo : crList){
//			try{
//				if(vo.getCurrnecyId() == ConstantsBase.CURRENCY_EUR_ID){
//					ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_EUR, errorCodeId, true);
//					if(!vo.isActive()){
//						ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
//					}
//					eurCurr = true;
//				}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_TRY_ID){
//					ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_TRY, errorCodeId, true);
//					if(!vo.isActive()){
//						ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
//					}
//					tryCurr = true;
//				}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_USD_ID){
//					ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_USD, errorCodeId, true);
//					if(!vo.isActive()){
//						ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
//					}
//					usdCurr = true;
//				}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_GBP_ID){
//					ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_GBP, errorCodeId, true);
//					if(!vo.isActive()){
//						ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
//					}
//					gbpCurr = true;
//				}
//			}catch (Exception e) {
//				// TODO: handle exception
//				log.debug(e);
//			}
//			}
//			try{
//				if(!eurCurr){
//					ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_EUR, errorCodeId, ConstantsBase.CURRENCY_EUR_ID, true);
//				}
//				if(!tryCurr){
//					ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_TRY, errorCodeId, ConstantsBase.CURRENCY_TRY_ID, true);
//				}
//				if(!usdCurr){
//					ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_USD, errorCodeId, ConstantsBase.CURRENCY_USD_ID, true);
//				}
//				if(!gbpCurr){
//					ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_GBP, errorCodeId, ConstantsBase.CURRENCY_GBP_ID, true);
//				}
//			} catch (Exception e) {
//				// TODO: handle exception
//				log.debug(e);
//			}
//		}
		@Deprecated
	private static	Transaction
			performReroutingAttemptTran(Transaction formerTran, ClearingInfo ci, UserBase user, String depositVal,
										long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
														NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
														UnsupportedEncodingException, InvalidAlgorithmParameterException {

			FacesContext context = FacesContext.getCurrentInstance();
			ClearingRoute cr = new ClearingRoute();
			Connection con = getConnection();
			Transaction tran = new Transaction();
			CreditCard	cc =  CreditCardsDAO.getById(getConnection(), ci.getCardId());
			formerTran.setIsRerouted(true);
			tran.setAmount(formerTran.getAmount());
			tran.setComments("");
			tran.setCreditCardId(formerTran.getCreditCardId());
			tran.setCc4digit(formerTran.getCc4digit());
			tran.setDescription(null);
			tran.setIp(formerTran.getIp());
			tran.setProcessedWriterId(Writer.WRITER_ID_WEB);
			tran.setChequeId(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
			tran.setUserId(formerTran.getUserId());
			tran.setWriterId(Writer.WRITER_ID_WEB);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(formerTran.getCurrency());
			tran.setUtcOffsetCreated(formerTran.getUtcOffsetCreated());
			//tran.setReferenceId(formerTran.getId());
			tran.setIsRerouted(true);
			tran.setReroutingTranId(formerTran.getId());
			tran.setLoginId(loginId);
			TransactionsDAOBase.insert(con, tran);
			ci.setTransactionId(tran.getId());

			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}

			boolean timeout = false;
			String errorCode = "";

			try {
				cr = ClearingManager.deposit(ci, true);
				if(ci.isSuccessful()){
					//update tran & clearing provider
					formerTran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
					tran.setTypeId(TRANS_TYPE_CC_DEPOSIT);
				}
			} catch (ClearingException ce) {
				if (ce.getCause() instanceof SocketTimeoutException) {
					timeout = true;
				}

				log.error("Failed to authorize transaction: " + ci.toString(), ce);
				ci.setResult("999");
				ci.setSuccessful(false);
				ci.setMessage(ce.getMessage());
			}
			tran.setClearingProviderId(ci.getProviderId());
			tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|" + ci.getProviderTransactionId());
			tran.setXorIdAuthorize(ci.getProviderTransactionId());
			if(ci.isSuccessful()){
				if (log.isInfoEnabled()) {
					log.info("Transaction Successful! authNumber: " + ci.getAuthNumber());
				}
				tran.setAuthNumber(ci.getAuthNumber());
				tran.setAcquirerResponseId(ci.getAcquirerResponseId());
	            if (null != ci.getPaReq()) {
	                tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
	                ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_CLEARING_INFO, ClearingInfo.class);
	                ve.getValue(context.getELContext());
	                ve.setValue(context.getELContext(), ci);
	            } else {
	                tran.setStatusId(TRANS_STATUS_PENDING);
	            }
	            tran.setTimeSettled(null);

	            if(!CommonUtil.isParameterEmptyOrNull(ci.getRecurringTransaction()) && ci.getProviderId() == ClearingManager.TBI_AO_PROVIDER_ID){
					cc.setRecurringTransaction(ci.getRecurringTransaction());
					CreditCardsDAO.update(getConnection(), cc);
				}
	            if (tran.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
                	try {
                		con.setAutoCommit(false);
						UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
						GeneralDAO.insertBalanceLog(con, Writer.WRITER_ID_WEB, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CC_DEPOSIT, user.getUtcOffset());
						TransactionsDAOBase.update(con, tran);
						con.commit();
                	} catch (Exception e) {
                		log.error("Exception in success deposit Transaction! ", e);
        				try {
        					con.rollback();
        				} catch (Throwable it) {
        					log.error("Can't rollback.", it);
        				}
                	} finally {
                        try {
                            con.setAutoCommit(true);
                        } catch (Exception e) {
                            log.error("Can't set back to autocommit.", e);
                        }
            		}
                } else {  // AUTHENTICATE status
                	TransactionsDAOBase.update(con, tran);
                }
			} else { // failed clearing
				tran.setComments(tran.getComments() + " | " + cc.toStringForComments());
				if (log.isInfoEnabled()) {
					String ls = System.getProperty("line.separator");
					log.info("Transaction failed clearing! " + ci.toString() + ls);
				}

				tran.setStatusId(TRANS_STATUS_FAILED);
				if (timeout) {
					tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
				} else {
					tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
				}
			}
			errorCode = ci.getResult();
			if (tran.getStatusId() == TRANS_STATUS_FAILED ) {//update transaction on failed cases
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.update(con, tran);
			}

			return tran;
		}

		public static boolean checkDaysOrFailuresToBin(ClearingRoute cr, long bin, TransactionBin tb) throws SQLException{
			boolean isGoodToReroute = false;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH , (int)-ConfigManager.getDaysNumForFailures());
			Date date = cal.getTime();
			if(tb.getTimeLastSuccess() == null || !tb.getTimeLastSuccess().after(date)){// checking the last success params
				isGoodToReroute = true;
				log.debug("last success is more than then days to calaulate");
			} else { //check 20 consecutive failure by unique CC
				long numOfFailures = getConsecutiveFailuresByBin(cr, bin);
				if(numOfFailures >= ConfigManager.getFailuresNumConsecutive()){
					isGoodToReroute = true;
				}
			}
			return  isGoodToReroute;
		}

		public static long getConsecutiveFailuresByBin(ClearingRoute cr , long bin) throws SQLException{
			Connection conn = getConnection();
			try{
				return TransactionsDAOBase.getConsecutiveFailuresByBin(conn, cr, bin);
			} finally {
				closeConnection(conn);
			}
		}

		public static ArrayList<Long> getTransactionsReroutConnectedIDById(long transactionId) throws SQLException {
			Connection conn = getConnection();
	        try {
	            return TransactionsDAOBase.getTransactionsReroutConnectedIDById(conn, transactionId);
	        } finally {
	            closeConnection(conn);
	        }
		}



		public static boolean insertLimit(ClearingLimitaion clearingLimitaion) throws SQLException {
			Connection con = getConnection();
	        try {
	        	long existsLimitId = TransactionsDAOBase.isLimitExists(con, clearingLimitaion);
	        	if (existsLimitId > 0) {
	        		clearingLimitaion.setId(existsLimitId);
	        		return TransactionsDAOBase.setLimitAsActive(con, clearingLimitaion);
	        	} else {
		            return TransactionsDAOBase.insertLimit(con, clearingLimitaion);
	        	}
	        } finally {
	            closeConnection(con);
	        }
		}

		public static ArrayList<ClearingLimitaion> getClearingLimitaionList() throws SQLException {
			Connection con = getConnection();
	        try {
	            return TransactionsDAOBase.getClearingLimitaionList(con);
	        } finally {
	            closeConnection(con);
	        }
		}

		public static boolean setLimitAsNotActive(ClearingLimitaion clearingLimitaion) throws SQLException {
			Connection con = getConnection();
	        try {
	            return TransactionsDAOBase.setLimitAsNotActive(con, clearingLimitaion);
	        } finally {
	            closeConnection(con);
	        }

		}

		public static void updateTimeLastRerouteFailed(TransactionBin tb) throws SQLException{
			Connection con = getConnection();
			try {
				TransactionsDAOBase.updateTimeLastRerouteFailed(con, tb);
			} finally {
				closeConnection(con);
			}

		}

		public static long getBinId(long bin, long ccTypeId) throws SQLException{
			Connection con = getConnection();
			try {
				return CreditCardsDAO.getBinId(con, bin, ccTypeId);
			} finally {
					closeConnection(con);
			}
		}

		private static boolean isInatecClearingProvider(Long providerId) {
			// TODO Auto-generated method stub
			if(providerId == ClearingManager.INATEC_PROVIDER_ID_EUR ||
					providerId == ClearingManager.INATEC_PROVIDER_ID_GBP ||
					providerId == ClearingManager.INATEC_PROVIDER_ID_TRY ||
					providerId == ClearingManager.INATEC_PROVIDER_ID_USD ){
				return true;
			}
			return false;
		}

		/**
		 * @param depositVal
		 * @param writerId
		 * @param user
		 * @param language
		 * @param hpURL
		 * @param source
		 * @param formName
		 * @param imagesDomain
		 * @return
		 * @throws SQLException
		 */
		public static InatecIframeInfo insertInatecIframeDeposit(String depositVal, long writerId, UserBase user,String language,
				String hpURL, int source, String formName, long loginId) throws SQLException {
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info(ls + "Insert InatecIframe transaction deposit:" + ls +
	                    "amount: " + depositVal + ls +
	                    "writer: " + writerId + ls +
	                    "userId: " + user.getId() + ls);
			}
			InatecIframeInfo inatecIframeInfo = null;
			Transaction transaction = null;
			String error = null;
			Connection con = getConnection();
			FacesContext context = FacesContext.getCurrentInstance();
			//!!!								   !!!//
			//!!!TODO make/check critical section. !!!//
			//!!!								   !!!//
			try {
				long deposit = CommonUtil.calcAmount(depositVal);
				error = validateDepositLimits(con, formName, user, deposit, source, true);
				transaction = new Transaction();
				transaction.setAmount(CommonUtil.calcAmount(depositVal));
				transaction.setComments(null);
				transaction.setCreditCardId(null);
				transaction.setDescription(null);
				transaction.setIp(CommonUtil.getIPAddress());
				transaction.setProcessedWriterId(writerId);
				transaction.setChequeId(null);
				transaction.setTimeSettled(null);
				transaction.setTimeCreated(new Date());
				transaction.setTypeId(TRANS_TYPE_INATEC_IFRAME_DEPOSIT);
				transaction.setUserId(user.getId());
				transaction.setWriterId(writerId);
				transaction.setChargeBackId(null);
				transaction.setStatusId(TRANS_STATUS_STARTED);
				transaction.setCurrency(user.getCurrency());
				transaction.setUtcOffsetCreated(user.getUtcOffset());
				transaction.setUtcOffsetSettled(null);
				transaction.setClearingProviderId(ClearingManager.INATEC_IFRAME_PROVIDER_ID);
				transaction.setLoginId(loginId);
				
				if (null != error) {
					transaction.setStatusId(TRANS_STATUS_FAILED);
					transaction.setDescription(error);
					transaction.setTimeSettled(new Date());
					transaction.setUtcOffsetSettled(user.getUtcOffset());
				} else {
					transaction.setStatusId(TRANS_STATUS_AUTHENTICATE);
				}
				//!!!								   !!!//
				//!!! insert InatecIframe transaction !!!//
				//!!!								   !!!//
				TransactionsDAOBase.insert(con, transaction);
				if (transaction != null){
					ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_TRANSACTION, Transaction.class);
		            veTran.getValue(context.getELContext());
		            veTran.setValue(context.getELContext(), transaction);
				}
				inatecIframeInfo = new InatecIframeInfo(user, transaction, language, hpURL, depositVal);
				inatecIframeInfo.setTransactionStatusId(transaction.getStatusId());
				ClearingManager.setInatecIframeDepositDetails(inatecIframeInfo, ClearingManager.INATEC_IFRAME_PROVIDER_ID);
				String signature = inatecIframeInfo.createInatecIframeChecksumBuilder();
				inatecIframeInfo.setSignature(signature);
		        log.info("\nInatecIframe Fields: " + "\n" + inatecIframeInfo.toString());		
				log.info("insert InatecIframe transaction finished successfully! ");
				if (null != error){
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, transaction);
					} catch (Exception e) {
						log.error("Problem with population failed deposit event ", e);
					}
				} else {
					try {
						PopulationsManagerBase.deposit(user.getId(), true, writerId, transaction);
					} catch (Exception e) {
						log.error("Problem with population successfull deposit event ", e);
					}
				}
			} catch (Exception e) {
				log.error("ERROR! insertInatecIframeDeposit", e);
				return null;
			} finally {
				closeConnection(con);
			}
			return inatecIframeInfo;
		}
		
		
		public static Transaction insertInatecDeposit(String depositVal, long writerId, long loginId, UserBase user) throws SQLException {
			log.info("Insert InatecIframe transaction deposit:" 
					+ "amount[ " + depositVal +" ] "
					+ "writer[ " + writerId +" ] "
					+ "userId[ " + user.getId() +" ]");

			Transaction transaction = null;
			Connection con = getConnection();

			try {
				long deposit = CommonUtil.calcAmount(depositVal);
				String error = validateDepositLimits(con, "inatecIframeDepositForm", user, deposit, ConstantsBase.WEB_CONST, true);
				
				transaction = new Transaction();
				transaction.setAmount(deposit);
				transaction.setComments(null);
				transaction.setCreditCardId(null);
				transaction.setDescription(null);
				transaction.setIp(CommonUtil.getIPAddress());
				transaction.setProcessedWriterId(writerId);
				transaction.setChequeId(null);
				transaction.setTimeSettled(null);
				transaction.setTimeCreated(new Date());
				transaction.setTypeId(TRANS_TYPE_INATEC_IFRAME_DEPOSIT);
				transaction.setUserId(user.getId());
				transaction.setWriterId(writerId);
				transaction.setChargeBackId(null);
				transaction.setStatusId(TRANS_STATUS_STARTED);
				transaction.setCurrency(user.getCurrency());
				transaction.setUtcOffsetCreated(user.getUtcOffset());
				transaction.setUtcOffsetSettled(null);
				transaction.setClearingProviderId(ClearingManager.INATEC_IFRAME_PROVIDER_ID);
				transaction.setLoginId(loginId);
				
				if (null != error) {
					transaction.setStatusId(TRANS_STATUS_FAILED);
					transaction.setDescription(error);
					transaction.setTimeSettled(new Date());
					transaction.setUtcOffsetSettled(user.getUtcOffset());
				} else {
					transaction.setStatusId(TRANS_STATUS_AUTHENTICATE);
				}

				// insert InatecIframe transaction 
				TransactionsDAOBase.insert(con, transaction);
				log.debug("Insert inatec iframe trx:" + transaction.getId()+" successful");
				if (null != error) {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, transaction);
					} catch (Exception e) {
						log.error("Problem with population failed deposit event ", e);
					}
				}
			} finally {
				closeConnection(con);
			}
			return transaction;
		}
		
		
		//FIXME change to insertOtherDeposit method
		public static EPGInfo insertEpgCheckoutDeposit(PaymentDeposit pd) throws SQLException {
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info(ls + "Insert Other Payment transaction deposit:" + ls + pd.toString());
			}
			
			EPGInfo epgInfo = null;
			Transaction transaction = null;
			String error = null;
			Connection connection = getConnection();
			FacesContext context = FacesContext.getCurrentInstance();
			//!!!								   !!!//
			//!!!TODO make/check critical section. !!!//
			//!!!								   !!!//
			try {
				long deposit = CommonUtil.calcAmount(pd.getDepositVal());
				error = validateDepositLimits(connection, pd.getFormName(), (UserBase)pd.getUser(), deposit, pd.getSource(), true);
				transaction = new Transaction();
				transaction.setAmount(CommonUtil.calcAmount(pd.getDepositVal()));
				transaction.setComments(null);
				transaction.setCreditCardId(null);
				transaction.setDescription(null);
				transaction.setIp(CommonUtil.getIPAddress());
				transaction.setProcessedWriterId(pd.getWriterId());
				transaction.setChequeId(null);
				transaction.setTimeSettled(null);
				transaction.setTimeCreated(new Date());
				transaction.setTypeId(pd.getTransactionType());
				transaction.setUserId(pd.getUser().getId());
				transaction.setWriterId(pd.getWriterId());
				transaction.setChargeBackId(null);
				transaction.setStatusId(TRANS_STATUS_STARTED);
				transaction.setCurrency(pd.getUser().getCurrency());
				transaction.setUtcOffsetCreated(pd.getUser().getUtcOffset());
				transaction.setUtcOffsetSettled(null);
				transaction.setClearingProviderId(pd.getProviderId());
				transaction.setLoginId(pd.getLoginId());
				transaction.setPaymentTypeId(pd.getPaymentTypeId());
				
				if (null != error) {
					transaction.setStatusId(TRANS_STATUS_FAILED);
					transaction.setDescription(error);
					transaction.setTimeSettled(new Date());
					transaction.setUtcOffsetSettled(pd.getUser().getUtcOffset());
				} else {
					transaction.setStatusId(TRANS_STATUS_AUTHENTICATE);
				}
				//!!!								   !!!//
				//!!! insert Other Payment transaction !!!//
				//!!!								   !!!//
				TransactionsDAOBase.insert(connection, transaction);
				if (transaction != null){
					ValueExpression veTran = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_TRANSACTION, Transaction.class);
		            veTran.getValue(context.getELContext());
		            veTran.setValue(context.getELContext(), transaction);
				}
				epgInfo = new EPGInfo(pd.getUser(), transaction, pd.getDepositVal(), pd.getPrefixURL());
				//epgInfo.setTransactionStatusId(transaction.getStatusId());
				ClearingManager.setEPGDepositDetails(epgInfo, ClearingManager.EPG_CHECKOUT_PROVIDER_ID);

				EpgDepositRequestDetails epgReq = TransactionsDAOBase.getEpgDepositRequestAdditionalDetails(connection, epgInfo);
				epgInfo.setAdditionalRequestParams(EPGClearingProvider.getAdditionalDetails(epgReq));
				
				//FIXME 
				epgInfo.setPaymentSolution(pd.getPaymentSolution());
				epgInfo.setProductId(epgReq.getProductId());
				ClearingRoute cr = ClearingManager.otherDeposit(epgInfo);
				
				//TODO if cr
				epgInfo.setTransactionStatusId(transaction.getStatusId());
				
		        //log.info("\nOther Payment Fields: " + "\n" + epgInfo.toString());		
				//TODO - after transaction success? 
				log.info("insert Other Payment transaction finished successfully! ");
				if (null != error){
					try {
						PopulationsManagerBase.deposit(pd.getUser().getId(), false, pd.getWriterId(), transaction);
					} catch (Exception e) {
						log.error("Problem with population failed deposit event ", e);
					}
				}
			} catch (Exception e) {
				log.error("ERROR! EPG Deposit", e);
				return null;
			} finally {
				closeConnection(connection);
			}
			return epgInfo;
		}
		
		public static Transaction insertCDPayDeposit(String depositVal, long writerId, UserBase user,
				 	String language,String hpURL, int source, String formName, String imagesDomain, long selectorId, long loginId) throws SQLException {
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info(ls + "Insert CDPay transaction deposit:" + ls +
	                    "amount: " + depositVal + ls +
	                    "writer: " + writerId + ls +
	                    "userId: " + user.getId() + ls);
			}

			Transaction tran = null;
			String error = null;
			Connection con = getConnection();
			FacesContext context = FacesContext.getCurrentInstance();

			try {
				long deposit = CommonUtil.calcAmount(depositVal);
				error = validateDepositLimits(con, formName, user, deposit, source, true);
				tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(depositVal));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setDescription(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(writerId);
				tran.setChequeId(null);
				tran.setTimeSettled(null);
				tran.setTimeCreated(new Date());
				tran.setTypeId(TRANS_TYPE_CUP_CHINA_DEPOSIT);
				tran.setUserId(user.getId());
				tran.setWriterId(writerId);
				tran.setChargeBackId(null);
				tran.setStatusId(TRANS_STATUS_STARTED);
				tran.setCurrency(user.getCurrency());
				tran.setUtcOffsetCreated(user.getUtcOffset());
				tran.setUtcOffsetSettled(null);
				tran.setClearingProviderId(ClearingManager.CDPAY_PROVIDER_ID);
				tran.setSelectorId(selectorId);
				
				if (null != error) {
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
				} else {
					tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
				}
				tran.setLoginId(loginId);
				//insert cdpay to transaction
				TransactionsDAOBase.insert(con, tran);
				CDPayInfo  cdpayInfo = new CDPayInfo(user, tran, language, hpURL, user.getEmail(), depositVal, CommonUtil.getAppData().getIsLive(), imagesDomain);
				ClearingManager.setCDPayDepositDetails(cdpayInfo, ClearingManager.CDPAY_PROVIDER_ID);
				//Not sending user details to CDPay.
				CDPayInfo cdpayInfoButUserDetails = setCDPayInfoToSend(cdpayInfo);
				String control = cdpayInfoButUserDetails.crateCDPayChecksum(); //Hash control msg.
				cdpayInfoButUserDetails.setControl(control);
		        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_CDPAY_INFO, CDPayInfo.class);
		        ve.getValue(context.getELContext());
		        ve.setValue(context.getELContext(), cdpayInfoButUserDetails); // for saving cdpayDetails
		        log.info("\nCDPay Fields: " + "\n" +		        			
		        			"RedirectURL - POST to = " + cdpayInfoButUserDetails.getRedirectURL() + "\n" +
		        			"Mendatory Fields: \n" +	        			
			        			"version=" + cdpayInfoButUserDetails.getVersion()  + "\n" +
			        			"merchant_account = " + cdpayInfoButUserDetails.getMerchantAccount() + "\n" +
			        			"merchant_order = " + cdpayInfoButUserDetails.getTransactionId() + "\n" +
			        			"merchant_product_desc = " + cdpayInfoButUserDetails.getMerchantProductDesc() + "\n" +
			        			"first_name=" + cdpayInfoButUserDetails.getFirstName()  + "\n" +
			        			"last_name=" + cdpayInfoButUserDetails.getLastName()  + "\n" +
			        			"address1=" + cdpayInfoButUserDetails.getAddress()  + "\n" +
			        			"city=" + cdpayInfoButUserDetails.getCity()  + "\n" +
			        			"zip_code=" + cdpayInfoButUserDetails.getZip()  + "\n" +
			        			"country=" + cdpayInfoButUserDetails.getCountryA2()  + "\n" +
			        			"phone=" + cdpayInfoButUserDetails.getUserPhone()  + "\n" +
			        			"email=" + cdpayInfoButUserDetails.getEmail()  + "\n" +
			        			"amount=" + cdpayInfoButUserDetails.getDepositAmount() + "\n" +
			        			"currency=" + cdpayInfoButUserDetails.getCurrencySymbol() + "\n" +
			        			"ipaddress=" + cdpayInfoButUserDetails.getIp() + "\n" +
			        			"returnURL=" + cdpayInfoButUserDetails.getReturnURL() + "\n" +
			        			"control=" + cdpayInfoButUserDetails.getControl()  + "\n" +
		        			"Optional Fields:\n" +
			        			"server_return_url="  + cdpayInfoButUserDetails.getServerReturnURL()  + "\n" +
			        			"bankcode="  + cdpayInfoButUserDetails.getBankcode()  + "\n");			
				log.info("insert CDPay transaction finished successfully! ");
				if (null != error){ // deposit failed due to minimum limitation
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
				}
			} catch (Exception e) {
				return null;
			} finally {
				closeConnection(con);
			}
			return tran;
		}
		
		/**
		 * @param cdpayInfo
		 * Do not need to send user details to CDPay clearing provider.
		 * @return
		 */
		private static CDPayInfo setCDPayInfoToSend(CDPayInfo cdpayInfo) {
			CDPayInfo cdpayInfoToSend = cdpayInfo;
			cdpayInfoToSend.setFirstName("any");
			cdpayInfoToSend.setLastName("option");
			cdpayInfoToSend.setAddress("khtur");
			cdpayInfoToSend.setCity("c");
			cdpayInfoToSend.setZip("z");
			cdpayInfoToSend.setCountryA2("CN");
			cdpayInfoToSend.setUserPhone("0");
			cdpayInfoToSend.setEmail("support@anyoption.com");
			return cdpayInfoToSend;
		}

		/**
		 * User chose to cancel CDPay transaction
		 * @param tran
		 * @param writerId
		 * @throws SQLException
		 */
		public static void cdpayCancelTransaction(Transaction tran, long writerId) throws SQLException{
	        Connection con = getConnection();
	        try {
	        	tran.setStatusId(TRANS_STATUS_FAILED);
	    		tran.setDescription(ConstantsBase.ERROR_CDPAY_DEPOSIT_CANCELED);
	    		tran.setTimeSettled(new Date());
	    		tran.setUtcOffsetSettled(tran.getUtcOffsetCreated());
				try {
					PopulationsManagerBase.deposit(tran.getUserId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}
	            tran.setComments(null);
	            TransactionsDAOBase.update(con, tran);
	        } catch (SQLException e) {
	    		log.error("Exception in updating CDPay cancel status, " + "transactionId= " + tran.getId() , e);
	    		throw e;
	    	} finally {
	    		closeConnection(con);
	    	}
		}
		
	  
		 /**
	     *
	     * @param transactionId
	     * @throws NoSuchAlgorithmException
	     * @throws UnsupportedEncodingException
	     * @throws NumberFormatException
	     * @throws SQLException
	     * @throws ClearingException
	     */
		//TODO: cdpay. unitest.
	    public static Transaction getCDPayReturnTransaction(Map requestParameterMap) throws NoSuchAlgorithmException, UnsupportedEncodingException, NumberFormatException, SQLException, ClearingException {
	    	FacesContext context = FacesContext.getCurrentInstance();
	    	CDPayInfo cdpayInfo = new CDPayInfo();
	    	try {
				ClearingManager.setCDPayDepositDetails(cdpayInfo, ClearingManager.CDPAY_PROVIDER_ID);
			} catch (ClearingException e) {
				log.error("cdpay, Failed to process CDPay Details.", e);
			}
	    	log.debug("cdpay, return url, after setCDPayDepositDetails method.");
	    	String errorMsg = "";
	    	
	    	//Set cdpay info details.
	    	if (!cdpayInfo.handleStatusUrl(requestParameterMap)) { //failed
	    		errorMsg = "problem, set cdpay info from request.";
	    	}	
	    	
	    	//checksum authentication.
	    	String control = cdpayInfo.getControl();
	    	if (!control.equals(cdpayInfo.resultChecksumAuthentication())) { //failed
	    		log.info("control from cdpay: " + control + "\n" + " anyoption control: " + cdpayInfo.resultChecksumAuthentication());
	    		log.info("CDPay, failed to authenticat result checksum!!!!!!!, update transaction");
	    		errorMsg = "failed to authenticat result checksum";
	    	}
	    
			Transaction transaction = getTransaction(cdpayInfo.getTransactionId());
			if (null == transaction || transaction.getStatusId() != TransactionsManagerBase.TRANS_STATUS_SUCCEED){ //failed
				log.error("CDPay, transaction return failed");
				errorMsg = "failed to authenticat result checksum";
			}
			if (!CommonUtil.isParameterEmptyOrNull(errorMsg)) {
				log.error("CDPay return url, " + errorMsg);
				/*FacesMessage fm = null;
				fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.creditcard.unapproved", null), null);
				context.addMessage(null, fm);*/
				return null;
			}
			return transaction;		
	    }
	    
	    
	    /**
	     * Update CDPay transaction status
	     * @param requestParameterMap
	     * @throws SQLException
	     * @throws NoSuchAlgorithmException
	     * @throws UnsupportedEncodingException
	     */
	    public static void updateCDPayTransaction(Map requestParameterMap, long loginId) throws SQLException, NoSuchAlgorithmException, UnsupportedEncodingException {
	    	log.debug("cdpay, 110, in 'updateCDPayTransaction' function ");
	    	Connection con = null;
	    	CDPayInfo cdpayInfo = new CDPayInfo();
	    	try {
				ClearingManager.setCDPayDepositDetails(cdpayInfo, ClearingManager.CDPAY_PROVIDER_ID);
			} catch (ClearingException e) {
				log.error("cdpay, Failed to process CDPay Details.", e);
			}
	    	log.debug("cdpay, 111, after setCDPayDepositDetails method.");
	    	
	    	if (!cdpayInfo.handleStatusUrl(requestParameterMap)) {
	    		return;
	    	}
	    	//checksum authentication.
	    	String control = cdpayInfo.getControl();
	    	if (!control.equals(cdpayInfo.resultChecksumAuthentication())) {
	    		log.info("CDPay, failed to authenticat result checksum!!!!!!!, update transaction");
	    		return;
	    	}
	    	//TODO: add amount equals authenticat. db.amount==pm.amount?? 
	    	log.debug("cdpay, 120,`updateCDPayTransaction` checksum OK ");
	    	//insert CDPay Return server URL to date base.
			CDPayDeposit cdpayDeposit = new CDPayDeposit();
			cdpayDeposit.setTransactionId(cdpayInfo.getTransactionId());
			cdpayDeposit.setAmount(cdpayInfo.getAmount());
			cdpayDeposit.setCdpayTransactionId(cdpayInfo.getCdpayTransactionId());
			cdpayDeposit.setBankcode(cdpayInfo.getBankcode());
			cdpayDeposit.setBankTransactionId((cdpayInfo.getBankTransactionId()));
			cdpayDeposit.setStatus((cdpayInfo.getStatus()));	
			cdpayDeposit.setStatusMessage((cdpayInfo.getStatusMessage()));
			cdpayDeposit.setBillingdescriptor((cdpayInfo.getBillingdescriptor()));
			cdpayDeposit.setControl((cdpayInfo.getControl()));
			cdpayDeposit.setCurrencySymbol((cdpayInfo.getCurrencySymbol()));
			log.debug("cdpay, 130, finish set all parameters before insert CDPAY_DEPOSIT table. ");
			try {
				con = getConnection();
				CDPayDepositDAO.insert(con, cdpayDeposit);
				log.debug("cdpay, 140, finish insert to cdpay_deposit table. ");
				log.debug("cdpay, get transaction (Before)");
		    	Transaction transaction = getTransaction(cdpayInfo.getTransactionId());
		    	//There is no such transactionId in db.
		    	if (transaction == null) {
		    		log.error("ERROR! cdpay request wrong transactionId. There is no such transaction id in our db. Exception will be thrown!");
		    	}
		    	//cdpay send wrong transactionId.
		    	if (transaction.getClearingProviderId() != ClearingManager.CDPAY_PROVIDER_ID) {
		    		log.error("ERROR! cdpay clearing provider, transactionId not match cdpay provider id.");
		    		return;
		    	}
		    	log.debug("cdpay, 150, get transaction (After)");
		    	con.setAutoCommit(false);
		    	long transactionStatus = TransactionsDAOBase.checkForUpdateTransaction(con, transaction.getId());
				// prevent cdpay sending us the same trx request more then once
				if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE || transactionStatus != TRANS_STATUS_AUTHENTICATE) {
					log.warn("Getting CDPay status URL of non authenticate transaction! transactionId: " + transaction.getId());
					try {
						con.rollback();
					} catch (Throwable t) {
						log.error("Can't rollback.", t);
					}
					return;
				}
				log.info("cdpay 155, transactionId: " + transaction.getId() + ", after check duplicated threads" );
				//To prevent same transactions.
				transaction.setTimeSettled(new Date());
				transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());

				//TODO: check if correctness.
				if (cdpayInfo.getControl() != null && !cdpayInfo.getControl().isEmpty() && cdpayInfo.getCdpayTransactionId() != 0 ) {//success
					transaction.setStatusId(TRANS_STATUS_SUCCEED);
					transaction.setComments("1000| " + "Permitted transaction |" );
					transaction.setXorIdAuthorize(String.valueOf(cdpayInfo.getCdpayTransactionId()));
					transaction.setXorIdCapture(String.valueOf(cdpayInfo.getCdpayTransactionId()));
					log.debug("cdpay, 160, cdpay, trans_status_succeed, comments, authnumber, capture.");
				} else {
					transaction.setStatusId(TRANS_STATUS_FAILED);
					transaction.setComments("-1| " + "Failed transaction |" );
					log.debug("Failed transaction.");
				}			
				TransactionsDAOBase.update(con, transaction);//Update transaction.
				log.debug("cdpay, 170, After transactiuon update.");
				if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
					try {
						PopulationsManagerBase.deposit(transaction.getUserId(), true, transaction.getWriterId(),transaction); //Population.
						log.debug("cdpay, 180,Population Deposit succeed.");
					} catch (Exception e) {
						log.warn("Problem with population succeed deposit event ", e);
					}
		            try {
		                afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
		                log.debug("cdpay, 182, afterTransactionSuccess.");
		            } catch (Exception e) {
		            	log.error("Problem processing bonuses after success transaction", e);
		            }
		            try {
						UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
						log.debug("cdpay, 184, addToBalance.");
						GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_CUP_DEPOSIT, transaction.getUtcOffsetCreated());
						log.debug("cdpay, 186, insertBalanceLog.");
						TransactionsDAOBase.update(con, transaction);
						log.debug("cdpay, 188,  After transactiuon update.");

			            try {
			            	UserBase user = UsersDAOBase.getUserById(con, transaction.getUserId());
			            	log.debug("cdpay, 190,  getUserById.");
			                CommonUtil.alertOverDepositLimitByEmail(transaction, user);
			                log.debug("cdpay, 192,  alertOverDepositLimitByEmail.");
			            } catch (RuntimeException e) {
			                log.error("Exception sending email alert to traders! ", e);
			            }			            			         
			            log.debug("cdpay, 193,  after setTransactionsAuthenticateState to final state.");
			            //this is distributed transaction: add user to provider link
			            if(transaction.getSelectorId()!= SelectorId.Assigned.getValue() && !SelectorId.isException(transaction.getSelectorId())) {
							DistributionManager.createUserToProvider(transaction, DistributionType.CUP);
							log.debug("cdpay distribution");
						}
						
						con.commit();
		        	} catch (Exception e) {
		        		log.error("Exception in success deposit Transaction! ", e);
						try {
							con.rollback();
						} catch (Throwable it) {
							log.error("Can't rollback.", it);
						}
		        	} finally {
		                try {
		                    con.setAutoCommit(true);
		                } catch (Throwable t) {
		                    log.error("Can't set back to autocommit.", t);
		                }
		                closeConnection(con);
		    		}
				} else { // deposit failed
					con.commit();
					log.debug("cdpay,  deposit failed.");
					try {
						PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event ", e);
					}
				}
			} catch (Exception e) {
        		log.error("Exception cdpay! ", e);
				try {
					con.rollback();
				} catch (Throwable it) {
					log.error("Can't rollback.", it);
				}
			} finally {
				if (!con.isClosed()) {
					try {
		                con.setAutoCommit(true);
		            } catch (Throwable t) {
		                log.error("Can't set back to autocommit.", t);
		            }
		            closeConnection(con);
				}
			}
	    }
	    /**
		 * Insert BaroPayDeposit transaction
		 * @param depositVal
		 * @param writerId
		 * @param user
		 * @param language
		 * @param hpURL
		 * @param source
		 * @param formName
		 * @param imagesDomain
		 * @return tran as Transaction
		 * @throws SQLException
		 */
//		public static Transaction insertBaroPayDeposit(String depositVal, long writerId, UserBase user,
//				 	String language,String hpURL, int source, String formName, String imagesDomain, String senderName, String landLinePhone, long loginId) throws SQLException {
//
//			if (log.isInfoEnabled()) {
//				String ls = System.getProperty("line.separator");
//				log.info(ls + "Insert BaroPay transaction deposit:" + ls +
//	                    "amount: " + depositVal + ls +
//	                    "writer: " + writerId + ls +
//	                    "userId: " + user.getId() + ls);
//			}
//
//			Transaction tran = null;
//			String error = null;
//			Connection con = getConnection();
//			FacesContext context = FacesContext.getCurrentInstance();
//
//			try {
//				long deposit = CommonUtil.calcAmount(depositVal);
//				error = validateDepositLimits(con, formName, user, deposit, source, true);
//				tran = new Transaction();
//				tran.setAmount(CommonUtil.calcAmount(depositVal));
//				tran.setComments(null);
//				tran.setCreditCardId(null);
//				tran.setIp(CommonUtil.getIPAddress());
//				tran.setProcessedWriterId(writerId);
//				tran.setChequeId(null);
//				tran.setTimeSettled(null);
//				tran.setTimeCreated(new Date());
//				tran.setTypeId(TRANS_TYPE_BAROPAY_DEPOSIT);
//				tran.setUserId(user.getId());
//				tran.setWriterId(writerId);
//				tran.setChargeBackId(null);
//				tran.setStatusId(TRANS_STATUS_STARTED);
//				tran.setCurrency(user.getCurrency());
//				tran.setUtcOffsetCreated(user.getUtcOffset());
//				tran.setUtcOffsetSettled(null);
//				tran.setClearingProviderId(ClearingManager.BAROPAY_PROVIDER_ID);
//				if (null != error) {
//					tran.setStatusId(TRANS_STATUS_FAILED);
//					tran.setDescription(error);
//					tran.setTimeSettled(new Date());
//					tran.setUtcOffsetSettled(user.getUtcOffset());
//				} else {
//					tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
//				}
//				tran.setLoginId(loginId);
//				TransactionsDAOBase.insert(con, tran);				
//				BaroPayInfo bpInfo = new BaroPayInfo(user, tran, language, hpURL, user.getEmail(), depositVal, CommonUtil.getAppData().getIsLive(), imagesDomain, senderName, landLinePhone);
//				ClearingManager.setBaroPayDepositDetails(bpInfo, ClearingManager.BAROPAY_PROVIDER_ID);
//		        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_BAROPAY_INFO, BaroPayInfo.class);
//		        ve.getValue(context.getELContext());
//		        ve.setValue(context.getELContext(), bpInfo); // for saving baroPayDetails
//		        //insert BaroPay Request details
//		        BaroPayRequest baroPayRequest = new BaroPayRequest();
//		        baroPayRequest.setPhone(landLinePhone);
//		        baroPayRequest.setSender(senderName);
//		        baroPayRequest.setTransactionId(tran.getId());
//		        BaroPayDAOBase.insertBaroPayRequest(con, baroPayRequest);
//		        if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {
//		        	boolean timeout = false; // check timeout when we connect to BaroPayProvider.
//		        	try {
//		        		ClearingManager.authorizeBaroPayDeposit(bpInfo, ClearingManager.BAROPAY_PROVIDER_ID); // request and get first response from BaroPay
//					} catch (ClearingException ce) { // Clearing failed
//						if (ce.getCause() instanceof SocketTimeoutException) {
//							timeout = true;
//						}
//						log.error("Failed to authorize transaction: " + bpInfo.toString(), ce);
//						bpInfo.setResult("999");
//						bpInfo.setSuccessful(false);
//						bpInfo.setMessage(ce.getMessage());
//					}
//		        	String comment = "";
//					if (null != bpInfo.getResult()) {
//						comment += bpInfo.getResult() + "|";
//					}
//					if (null != bpInfo.getMessage()) {
//						comment += bpInfo.getMessage() + "|";
//					}
//					if (null != bpInfo.getUserMessage()) {
//						comment += bpInfo.getUserMessage() + "|";
//					}
//					if (comment.length() > 0) {
//						comment = comment.substring(0, comment.length()-1);
//					}
//					tran.setComments(comment);
//					if (timeout) {
//						if (log.isInfoEnabled()) {
//							String ls = System.getProperty("line.separator");
//							log.info("Transaction failed BaroPayDeposit! " + bpInfo.toString() + ls);
//						}
//						tran.setStatusId(TRANS_STATUS_FAILED);
//						FacesMessage fm = null;
//						fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, ConstantsBase.ERROR_CLEARING_TIMEOUT, null);
//						context.addMessage(null, fm);
//						tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
//						 // update transaction on failed cases
//						tran.setTimeSettled(new Date());
//						tran.setUtcOffsetSettled(user.getUtcOffset());
//						TransactionsDAOBase.update(con, tran);
//					}
//					
//			        BaroPayResponse baroPayResponse = new BaroPayResponse();
//			        long providerTransactionId = 0;
//			        if (null != bpInfo.getProviderTransactionId()) {
//			        	providerTransactionId = Long.valueOf(bpInfo.getProviderTransactionId());
//			        }
//			        baroPayResponse.setPaymentId(providerTransactionId);
//			        int status = 1;
//			        if (null != bpInfo.getResult()) {
//			        	status = Integer.valueOf(bpInfo.getResult());
//			        }
//			        baroPayResponse.setStatus(status);
//			        baroPayResponse.setDescription(bpInfo.getMessage());			      
//			        baroPayResponse.setTransactionId(tran.getId());
//			        baroPayResponse.setXmlResponse(bpInfo.getXmlResponse());			        
//			        BaroPayDAOBase.insertBaroPayResponse(con, baroPayResponse);
//			        log.info("BaroPay fields: " + "\n" +
//			        			"DepositID = " + bpInfo.getTransactionId() + "\n" +
//			        			"MemberCode = " + bpInfo.getUserId() + "\n" +
//			        			"MobileNumber = " + bpInfo.getUserPhone() + "\n" +
//			        			"Currency = " + bpInfo.getCurrencySymbol() + "\n" +
//			        			"Amount = " + bpInfo.getDepositAmount() + "\n" +
//			        			"SenderName = " + bpInfo.getSenderName() + "\n" +
//			        			"returnURL=" + bpInfo.getStatusURL() + "\n" +
//			        			"AgentID=" + bpInfo.getMerchantAccount());
//					log.info("insert BaroPay transaction finished successfully! ");
//		        }
//				if (null != error){ // deposit failed due to minimum limitation
//					try {
//						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
//					} catch (Exception e) {
//						log.warn("Problem with population failed deposit event " + e);
//					}
//				}
//			} catch (Exception e) {
//				log.error("Problem in BaroPAY!! " + e);
//				return null;
//			} finally {
//				closeConnection(con);
//			}
//			return tran;
//		}
		
	    /**
	     * Update BaroPay transaction status
	     * @param requestParameterMap
	     * @throws SQLException
	     * @throws NoSuchAlgorithmException
	     * @throws UnsupportedEncodingException
	     */
//	    public static void updateBaroPayTransactionStatus(String responseXML, long loginId) throws SQLException, NoSuchAlgorithmException, UnsupportedEncodingException{
//	    	Connection con = null;
//	    	BaroPayInfo bpInfo = new BaroPayInfo();
//	    	if (!bpInfo.handleStatusUrl(responseXML)){
//	    		return;
//	    	}
//	    	con = getConnection();
//			Transaction transaction = getTransaction(Long.valueOf(bpInfo.getTransaction_id()));
//			// prevent BaroPay sending us the same trx response more then once
//			if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
//				log.warn("Getting BaroPay status URL of non authenticate transaction!" + transaction.getId());
//				return;
//			}
//			transaction.setTimeSettled(new Date());
//			transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
//			try {
//				if (bpInfo.getProviderTransactionId() != null && !bpInfo.getProviderTransactionId().isEmpty() && 
//						bpInfo.getResult().equals(ConstantsBase.BARO_PAY_RESULT_APPROVE)) { //success
//					transaction.setStatusId(TRANS_STATUS_SUCCEED);
//					transaction.setAmount(Long.valueOf(bpInfo.getDepositAmount()) * 100); // amount can be change by provider.
//					transaction.setComments("1000| " + "Permitted transaction |" );
//				} else {
//					transaction.setStatusId(TRANS_STATUS_FAILED);
//					transaction.setComments("-1| " + "Failed transaction |" );
//				}
//				transaction.setAuthNumber(bpInfo.getProviderTransactionId());
//				transaction.setXorIdCapture(bpInfo.getProviderTransactionId());
//				if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
//					try {
//						PopulationsManagerBase.deposit(transaction.getUserId(), true, transaction.getWriterId(),transaction);
//					} catch (Exception e) {
//						log.warn("Problem with population succeed deposit event ", e);
//					}
//		            try {
//		                afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
//		            } catch (Exception e) {
//		            	log.error("Problem processing bonuses after success transaction", e);
//		            }
//		            try {
//		        		con.setAutoCommit(false);
//						UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
//						GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_BAROPAY_DEPOSIT, transaction.getUtcOffsetCreated());
//						TransactionsDAOBase.update(con, transaction);
//
//			            try {
//			            	UserBase user = UsersDAOBase.getUserById(con, transaction.getUserId());
//			                CommonUtil.alertOverDepositLimitByEmail(transaction, user);
//			            } catch (RuntimeException e) {
//			                log.error("Exception sending email alert to traders! ", e);
//			            }
//
//						con.commit();
//		        	} catch (Exception e) {
//		        		log.error("Exception in success deposit Transaction! ", e);
//						try {
//							con.rollback();
//						} catch (Throwable it) {
//							log.error("Can't rollback.", it);
//						}
//		        	} finally {
//		                try {
//		                    con.setAutoCommit(true);
//		                } catch (Exception e) {
//		                    log.error("Can't set back to autocommit.", e);
//		                }
//		    		}
//
//				} else { // deposit failed
//					TransactionsDAOBase.update(con, transaction);
//					try {
//						try {
//							PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
//						} catch (Exception e) {
//							log.warn("Problem with population failed deposit event ", e);
//						}
//					} catch (Exception e) {
//						log.error("Could not send email.", e);
//					}
//				}
//			} finally {
//				closeConnection(con);
//			}
//	    }
	    
	    /**
		 * insert BaroPay Withdraw
		 * @param error
		 * 		if there is an error with the request
		 * @param user
		 * 		the user that creat wire request
		 * @param writerId
		 * 		the writer id
		 * @param feeCancel
		 * 		if this bank wire with fee or not
		 * @throws SQLException
		 */	public static void insertBaroPayWithdraw(String amount, String error, UserBase user, long writerId, boolean feeCancel, String bankName, String accountOwner, String accountNum, long loginId, QuestionnaireUserAnswer questUserAnswer)	throws SQLException {

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO,
								"insertBaroPayWithdraw " + ls + "User:" + user.getUserName() + ls + "famount:" + amount);
			}

			FacesContext context = FacesContext.getCurrentInstance();
			Connection con = getConnection();

			try {
				UsersDAOBase.getByUserName(con, user.getUserName(), user);
				long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);				

				Transaction tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(amount));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(writerId);
				tran.setCreditWithdrawal(true);// for deltaPAy cft -- only true
				tran.setTimeCreated(new Date());
				tran.setUtcOffsetCreated(user.getUtcOffset());
	            tran.setTypeId(TRANS_TYPE_BAROPAY_WITHDRAW);
				tran.setUserId(user.getId());
				tran.setWriterId(writerId);
				tran.setChargeBackId(null);
				tran.setReceiptNum(null);
				tran.setChequeId(null);
				tran.setFeeCancel(feeCancel);
				if(feeCancel){
					tran.setWithdrawalFeeExemptWriterId(writerId);
				}

				if (error == null) { // no error
					try {
						con.setAutoCommit(false);
						tran.setDescription(null);
						tran.setStatusId(TRANS_STATUS_REQUESTED);
						tran.setTimeSettled(null);
						tran.setLoginId(loginId);
						UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
						user.setBalance(newBalance);
						TransactionsDAOBase.insert(con, tran);
						// insert baroPayRequest.
						BaroPayRequest baroPayRequest = new BaroPayRequest();
						baroPayRequest.setBankName(bankName);
						baroPayRequest.setAccountOwner(accountOwner);
						baroPayRequest.setAccountNumber(accountNum);
						baroPayRequest.setTransactionId(tran.getId());
						BaroPayDAOBase.insertBaroPayRequest(con, baroPayRequest);
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_BAROPAY_WITHDRAW, user.getUtcOffset());
						if (questUserAnswer != null) {
							questUserAnswer.setTransactionId(tran.getId());
							QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
						}
						con.commit();
						PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
						if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
							PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
						}
						log.info("insert BaroPayWithdraw finished successfully!");
					} catch (SQLException e) {
						log.error("ERROR during BaroPayWithdraw! ", e);
						try {
							    con.rollback();
							} catch (SQLException ie) {
								log.log(Level.ERROR, "Can't rollback.", ie);
							}
							throw e;
					} finally {
						try {
							con.setAutoCommit(true);
						} catch (Exception e) {
							log.error("Cannot set back to autoCommit! " + e);
						}
					}
				} else {  // error
					tran.setWireId(null);
					tran.setDescription(error);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.insert(con, tran);
					log.info("Insert BaroPayWithdraw finished with error!");
				}				
				try{
					RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
						tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
				} catch (Exception e) {
					log.error("Problem with risk Alert handler ", e);
				}
			} catch (SQLException e) {
				log.error("ERROR during BaroPayWithdraw!! ", e);
			} finally {
				closeConnection(con);
			}
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
		 
		 /**
		 * Insert CDPay Withdraw
		 * @param error
		 * 		if there is an error with the request
		 * @param user
		 * 		the user that creat wire request
		 * @param writerId
		 * 		the writer id
		 * @param feeCancel
		 * 		if this bank wire with fee or not
		 * @throws SQLException
		 */	
		 public static void insertCDPayWithdraw(String amount, String error, UserBase user, long writerId, boolean feeCancel, long loginId) throws SQLException {

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO,
							"insert CDPay Withdraw " + ls +
							"User:" + user.getUserName() + ls +
							"amount:" + amount);
			}

			FacesContext context = FacesContext.getCurrentInstance();
			Connection con = getConnection();

			try {
				UsersDAOBase.getByUserName(con, user.getUserName(), user);
				long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

				Transaction tran = new Transaction();
				tran.setAmount(CommonUtil.calcAmount(amount));
				tran.setComments(null);
				tran.setCreditCardId(null);
				tran.setIp(CommonUtil.getIPAddress());
				tran.setProcessedWriterId(writerId);
				tran.setTimeCreated(new Date());
				tran.setUtcOffsetCreated(user.getUtcOffset());
	            tran.setTypeId(TRANS_TYPE_CUP_CHINA_WITHDRAW);
	            tran.setClearingProviderId(ClearingManager.CDPAY_PROVIDER_ID);
				tran.setUserId(user.getId());
				tran.setWriterId(writerId);
				tran.setChargeBackId(null);
				tran.setReceiptNum(null);
				tran.setChequeId(null);
				tran.setFeeCancel(feeCancel);
				tran.setLoginId(loginId);

				if (error == null) { // no error
					try {
						con.setAutoCommit(false);
						tran.setDescription(null);
						tran.setStatusId(TRANS_STATUS_REQUESTED);
						tran.setTimeSettled(null);
						UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
						user.setBalance(newBalance);
						TransactionsDAOBase.insert(con, tran);
						GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CDPAY_CHINA_WITHDRAW, user.getUtcOffset());
						con.commit();
						PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
						if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
							PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
						}
						log.info("insert CDPay Withdraw finished successfully!");
					} catch (SQLException e) {
						log.error("ERROR during  CDPay Withdraw! ", e);
						try {
							    con.rollback();
							} catch (SQLException ie) {
								log.log(Level.ERROR, "Can't rollback.", ie);
							}
							throw e;
					} finally {
						try {
							con.setAutoCommit(true);
						} catch (Exception e) {
							log.error("Cannot set back to autoCommit! " + e);
						}
					}
				} else {  // error
					tran.setWireId(null);
					tran.setDescription(error);
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.insert(con, tran);
					log.info("Insert CDPay Withdraw finished with error!");
				}
				try{
					RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
						tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
				} catch (Exception e) {
					log.error("Problem with risk Alert handler ", e);
				}
			} catch (SQLException e) {
				log.error("ERROR during  CDPay Withdraw!! ", e);
			} finally {
				closeConnection(con);
			}
	        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, UserBase.class);
	        ve.getValue(context.getELContext());
	        ve.setValue(context.getELContext(), user);
		}
		
		/**
		 *  Check if user have wire deposit or cash deposit in status pending
		 * @param transaction
		 * @return true - in case the user have wire deposit or cash deposit in status pending
		 * @throws SQLException
		 */
		public static boolean IsHavePendingDeposit(Transaction transaction) throws SQLException {
			Connection con = getConnection();
			boolean flag = false;
			try {
				flag = TransactionsDAOBase.IsHavePendingDeposit(con, transaction);
			} finally {
				closeConnection(con);
			}
			return flag;
		}
		
		private static void sendVelocityErrorMail(String error,String userName, Long skin )
		{
			try {
				HashMap<String, String> params = null;
				params = new HashMap<String, String>();
				//collect the parameters we need for velocity error support email
						String velocityEmail = null;
						String mailTo = null;
						if(skin == Skins.SKIN_ETRADER) {
							velocityEmail= CommonUtil.getProperty("email.to.etrader");
						}else if(skin == Skins.SKIN_CHINESE) {
							velocityEmail= CommonUtil.getProperty("email.to.anyoption.zh");
						}else if(skin == Skins.SKIN_KOREAN) {
							velocityEmail= CommonUtil.getProperty("email.to.anyoption.kr");
						}else {
							velocityEmail= CommonUtil.getProperty("email.to.anyoption");
						}
						mailTo = velocityEmail + ", " + CommonUtil.getProperty("email.to.velocity.person");
						params.put(SendDepositErrorEmail.PARAM_EMAIL, velocityEmail);	
						params.put(SendDepositErrorEmail.PARAM_USER_NAME, userName);
						params.put(SendDepositErrorEmail.PARAM_VELOCITY_DESCRIPTION, CommonUtil.getProperty(error));
						params.put(SendDepositErrorEmail.PARAM_SKIN,"" + skin + "");
						params.put(SendDepositErrorEmail.MAIL_TO, mailTo);
						params.put(SendDepositErrorEmail.MAIL_SUBJECT ," Velocity check: " + CommonUtil.getProperty(error));
						
						new SendDepositErrorEmail(params).start();
			
		    } catch (Exception e) {
			    log.error("Could not send velocity email.", e);
	   }
   }		
		
		/**
		 *  Check velocity user 
		 * @param userId
		 * @return result from 
		 * @throws SQLException
		 */
		private static String velocityCheck(long userId) throws SQLException {
			String resault = null;
			Connection con = getConnection();			
			try {
				resault = TransactionsDAOBase.velocityCheck(con, userId);
			} finally {
				closeConnection(con);
			}
			return resault;
		}		
		
		/**
		 * Insert deposit method for new transaction type: fix negative balance
		 * This method create a connection.
		 * @param depositVal
		 * @param writerId
		 * @param user
		 * @throws SQLException
		 */
		public static void insertDepositToFixNegativeBalance(long depositVal, long writerId, UserBase user, String ip) throws SQLException {
			Connection conn = getConnection();
	    	 conn.setAutoCommit(false);
	    	 
	         try {
	         	insertDepositToFixNegativeBalance(conn, depositVal, writerId, user, null, 0L, ip);
				conn.commit();
	         } catch (SQLException e) {
	        	 log.log(Level.ERROR, "in insertDepositToFixNegativeBalance", e);
	        	 try {
	        		 conn.rollback();
	        	 } catch (SQLException ie) {
	        		 log.log(Level.ERROR, "in insertDepositToFixNegativeBalance, Can't rollback.", ie);
	        	 }
	        	 throw e;
			} finally {
				conn.setAutoCommit(true);
				closeConnection(conn);
			}
		}

		public static void insertFeeTransaction(Transaction feeTransaction, long mainTransactionId) throws SQLException {
			Connection conn = getConnection();
			conn.setAutoCommit(false);
	    	 
	         try {
	 			TransactionsDAOBase.insert(conn, feeTransaction);
				TransactionsDAOBase.updateRefId(conn, mainTransactionId, feeTransaction.getId());
				conn.commit();
	         } catch (SQLException e) {
	        	 log.log(Level.ERROR, "in insertFeeTransaction", e);
	        	 try {
	        		 conn.rollback();
	        	 } catch (SQLException ie) {
	        		 log.log(Level.ERROR, "in insertFeeTransaction, Can't rollback.", ie);
	        	 }
	        	 throw e;
			} finally {
				conn.setAutoCommit(true);
				closeConnection(conn);
			}
		}

	public static void cancelMaintenanceFee(Transaction t, long writerId) throws SQLException {
	    Connection con = getConnection();
    	con.setAutoCommit(false);

	    try {
			// insert transaction 
	    	TransactionsDAOBase.insert(con, t);
			// update balance 
	    	UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
			// update balance history 
	    	GeneralDAO.insertBalanceLog(con, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_MAINTENANCE_FEE_CANCEL, t.getUtcOffsetCreated());
	    	con.commit();
	    	
       	} catch (SQLException ie) {
       		log.error("Problem in sql cancel of maintenance fee");
       		ie.printStackTrace();
    		con.rollback();
    		throw ie;
		} finally {
			con.setAutoCommit(true);
	    	closeConnection(con);	
		}
	}
	
	public static Transaction getCopyopCoinsTransaction(long amount, long writerId, com.anyoption.common.beans.User user, String comments, String ip) {
        Transaction t = new Transaction();
        t.setUserId(user.getId());
        t.setAmount(amount);
        t.setTypeId(TransactionsManagerBase.TRANS_TYPE_COPYOP_COINS_TO_CASH);
        t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);        
        t.setIp(ip);
        t.setComments(comments);
        t.setTimeSettled(new Date());
        t.setTimeCreated(new Date());
        t.setUtcOffsetCreated(user.getUtcOffset());
        t.setUtcOffsetSettled(user.getUtcOffset());
        t.setWriterId(writerId);
        return t;
	}
	
	/**
	 * @param transactionId
	 * @param checksum
	 * @param paramterMap
	 * @throws Exception
	 */
	public static void updateInatecIframeTransaction(long transactionId, String checksum, Map paramterMap, long loginId) throws Exception {
    	log.debug("InatecIframe, 110, in 'updateInatecIframeTransaction' function ");
    	Connection conn = null;
    	InatecIframeInfo inatecIframeInfo = new InatecIframeInfo();
    	try {
			ClearingManager.setInatecIframeDepositDetails(inatecIframeInfo , ClearingManager.INATEC_IFRAME_PROVIDER_ID);
		} catch (ClearingException e) {
			log.error("InatecIframe, Failed to process InatecIframe Details.", e);
		}
    	log.debug("InatecIframe, 111, after setInatecIframeDepositDetails method.");
    	
    	//TODO may remove
    	if (!inatecIframeInfo.handleStatusUrl(paramterMap)) {
    		return;
    	}
		try {
			conn = getConnection();
			log.debug("inatecIframe, get transaction (Before)");
	    	Transaction transaction = getTransaction(transactionId);
	    	//There is no such transactionId in db.
	    	if (transaction == null) {
	    		log.error("ERROR! inatecIframe request wrong transactionId. There is no such transaction id: " +  transactionId + "in our db. Exception will be thrown!");
	    	}
	    	//inatecIframeInfo send wrong transactionId.
	    	if (transaction.getClearingProviderId() != ClearingManager.INATEC_IFRAME_PROVIDER_ID) {
	    		log.error("ERROR! inatecIframe clearing provider, transactionId not match inatecIframeInfo provider id.");
	    		return;
	    	}
	    	log.debug("inatecIframe, 150, get transaction (After)");
	    	conn.setAutoCommit(false);
	    	long transactionStatus = TransactionsDAOBase.checkForUpdateTransaction(conn, transaction.getId());
			// prevent inatecIframe sending us the same trx request more then once
			if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE || transactionStatus != TRANS_STATUS_AUTHENTICATE) {
				log.warn("Getting inatecIframe status URL of non authenticate transaction! transactionId: " + transaction.getId());
				try {
					conn.rollback();
				} catch (Throwable t) {
					log.error("Can't rollback.", t);
				}
				return;
			}
			log.info("inatecIframe 155, transactionId: " + transaction.getId() + ", after check duplicated threads" );
			//To prevent same transactions.
			transaction.setTimeSettled(new Date());
			transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());

			//TODO: check if correctness.
			if (inatecIframeInfo.getStatus().contains(InatecIframeInfo.STATUS_SUCCEEDED) && inatecIframeInfo.getRequestStatus().contains(InatecIframeInfo.STATUS_SUCCEEDED)) {//success
				transaction.setStatusId(TRANS_STATUS_SUCCEED);
				transaction.setComments("1000| " + "Permitted transaction |" + inatecIframeInfo.getResponseComment());
				//TODO there is none?
				//transaction.setXorIdAuthorize(String.valueOf());
				//transaction.setXorIdCapture(String.valueOf());
				log.debug("inatecIframe, 160, trans_status_succeed, comments, authnumber, capture.");
			} else {
				transaction.setStatusId(TRANS_STATUS_FAILED);
				transaction.setComments("-1| " + "Failed transaction |" + inatecIframeInfo.getResponseComment());
				log.debug("Failed transaction.");
			}			
			TransactionsDAOBase.update(conn, transaction);//Update transaction.
			log.debug("inatecIframe, 170, After transactiuon update.");
			if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
	            try {
					UsersDAOBase.addToBalance(conn, transaction.getUserId(), transaction.getAmount());
					log.debug("inatecIframe, 184, addToBalance.");
					GeneralDAO.insertBalanceLog(conn, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_INATEC_IFRAME_DEPOSIT, transaction.getUtcOffsetCreated());
					log.debug("inatecIframe, 186, insertBalanceLog.");

		            try {
		            	UserBase user = UsersDAOBase.getUserById(conn, transaction.getUserId());
		            	log.debug("inatecIframeInfo, 190,  getUserById.");
		                CommonUtil.alertOverDepositLimitByEmail(transaction, user);
		                log.debug("inatecIframe, 192,  alertOverDepositLimitByEmail.");
		            } catch (RuntimeException e) {
		                log.error("Exception sending email alert to traders! ", e);
		            }					
					conn.commit();
					try {
						PopulationsManagerBase.deposit(transaction.getUserId(), true, transaction.getWriterId(),transaction); //Population.
						log.debug("inatecIframe, 180,Population Deposit succeed.");
					} catch (Exception e) {
						log.warn("Problem with population succeed deposit event ", e);
					}
		            try {
		                afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
		                log.debug("inatecIframe, 182, afterTransactionSuccess.");
		            } catch (Exception e) {
		            	log.error("Problem processing bonuses after success transaction", e);
		            }
	        	} catch (Exception e) {
	        		log.error("Exception in success deposit Transaction! ", e);
					try {
						conn.rollback();
					} catch (Throwable it) {
						log.error("Can't rollback.", it);
					}
	        	} finally {
	                try {
	                    conn.setAutoCommit(true);
	                } catch (Throwable t) {
	                    log.error("Can't set back to autocommit.", t);
	                }
	                closeConnection(conn);
	    		}
			} else { // deposit failed
				conn.commit();
				log.debug("inatecIframe,  deposit failed.");
				try {
					PopulationsManagerBase.deposit(transaction.getUserId(), false, transaction.getWriterId(), transaction);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event ", e);
				}
			}
		} catch (Exception e) {
  		log.error("Exception inatecIframe! ", e);
			try {
				conn.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
		} finally {
			if (!conn.isClosed()) {
				try {
	                conn.setAutoCommit(true);
	            } catch (Throwable t) {
	                log.error("Can't set back to autocommit.", t);
	            }
	            closeConnection(conn);
			}
		}
    }
	
	public static long getSumOfRealDepositsUSD(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getSumOfRealDepositsUSD(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get first time deposit to fire server pixel
	 * @param fireServerPixelFields
	 * @return ArrayList<FireServerPixelHelper>
	 * @throws SQLException
	 */
	public static ArrayList<FireServerPixelHelper> getFtdToFireServerPixel(FireServerPixelFields fireServerPixelFields) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getFtdToFireServerPixel(con, fireServerPixelFields);
		} finally {
			closeConnection(con);
		}
	}

	
	public static Transaction updateEzeebillTransactionStatus(long loginId, long transactionId, long amount, long transactionStatus, long providerTxtNo, String message) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			
			log.debug("ezeebill begin transaction update");
	    	Transaction transaction = getTransaction(transactionId);
	    	//There is no such transactionId in db.
	    	if (transaction == null) {
	    		log.error("ERROR! wrong ezeebill transactionId : " + transactionId);
	    		return null;
	    	}
	    	// Not expected transaction 
	    	if (transaction.getClearingProviderId() != ClearingManager.EZEEBILL_PROVIDER_ID) {
	    		log.error("ERROR! not expected clearing provider : "+ transaction.getClearingProviderId());
	    		return null;
	    	}
			// validate amount
			if(amount != transaction.getAmount()) {
				log.error("ERROR! not expected  amount : " + amount + " transaction amount :" + transaction.getAmount() + " id: " + transaction.getId());
				return null;
			}	    	
	    	// Transaction already cleared
			if (transaction.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
				log.error("ERROR! not expected  status : " + transaction.getStatusId() + " for transaction :" + transaction.getId()); 
				return null;
			}
			
			// store providerTxtNo for future reference
			transaction.setXorIdAuthorize(String.valueOf(providerTxtNo));
			
			//success
			if (transactionStatus == 0) {
				transaction.setTimeSettled(new Date());
				transaction.setUtcOffsetSettled(transaction.getUtcOffsetCreated());
				transaction.setStatusId(TRANS_STATUS_SUCCEED);
				transaction.setComments("1000| " + "Permitted transaction |" );
				log.debug("succesful ezeebil transaction: "+ transaction.getId());
			} else {
				transaction.setStatusId(TRANS_STATUS_FAILED);
				transaction.setComments("-1| " + "Failed |" + message );
				log.debug("Failed transaction.");
			}
			
			//Update transaction.
			TransactionsDAOBase.update(con, transaction);
			// Update user data
			if (transaction.getStatusId() == TRANS_STATUS_SUCCEED) {
	            afterTransactionSuccess(transaction.getId(), transaction.getUserId(), transaction.getAmount(), transaction.getWriterId(), loginId);
				UsersDAOBase.addToBalance(con, transaction.getUserId(), transaction.getAmount());
				GeneralDAO.insertBalanceLog(con, transaction.getWriterId(), transaction.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, transaction.getId(), ConstantsBase.LOG_BALANCE_CUP_DEPOSIT, transaction.getUtcOffsetCreated());
			}
			return transaction;
		} catch (Exception e) {
    		log.error("Exception ezeebill : ", e);
			rollback(con);
		} finally {
			setAutoCommitBack(con);
	        closeConnection(con);
		}
		return null;
	}
	
	public static Transaction insertEzeebillDeposit(String depositVal, long writerId, String ip, UserBase user, String formName, long loginId) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert EzeeBill transaction deposit:" + ls +
		            "amount: " + depositVal + ls +
		            "writer: " + writerId + ls +
		            "userId: " + user.getId() + ls);
		}
		
		Transaction tran = null;
		String error = null;
		Connection con = getConnection();
		
		try {
			long deposit = new BigDecimal(depositVal).multiply(new BigDecimal(100)).longValue();
			error = validateDepositLimits(con, formName, user, deposit, 0, true);
			tran = new Transaction();
			tran.setAmount(deposit);
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(ip);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CUP_CHINA_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setClearingProviderId(ClearingManagerConstants.EZEEBILL_PROVIDER_ID);
			
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			tran.setLoginId(loginId);
			//insert ezeeBill to transaction
			TransactionsDAOBase.insert(con, tran);

			log.info("insert Ezeebill transaction finished successfully: "+ tran.getId());
		} catch (Exception e) {
			return null;
		} finally {
			closeConnection(con);
		}
		return tran;
	}
	
	public static String getFtdMailDocTxt(User user){
		String res = "";
		String depositType = null;
		try { 			
			Transaction t = getTransaction(user.getFirstDepositId());	
			if(t != null){
				if(t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT){
					res =  CommonUtil.getMessage("CMS.ftd.doc.mail.cc", null, user.getLocale());
				}
						
				if(t.isWireDeposit()){
					depositType = "Wire";
				}
				
				if(t.isIdealInatecDeposit()){
					depositType = "iDeal DR";
				}
				
				if(t.isDirect24Deposit()){
					depositType = "Direct24";
				}
				
				if(t.isEpsDeposit()){
					depositType = "EPS";
				}
				
				if(t.isGiropayDeposit()){
					depositType = "Giropay";
				}
				
				if(t.isCashDeposit()){
					depositType = "Cash";				
				}
			}
			if(!CommonUtil.isParameterEmptyOrNull(depositType)){
				String[] params = new String[1];
				params[0] = depositType;
				res =  CommonUtil.getMessage("CMS.ftd.doc.mail.other", params, user.getLocale());				
			}
			
		} catch (SQLException e) {
			log.error("Error when getTransaction ", e);
		}
		return res;
	}
	
	/**
	 * ***************************************
	 * ***** Migration from job process. *****
	 * ***************************************
	 * 
	 * Send cancel withdrawal email notification
	 * @throws Exception
	 */
	public static void SendCancelWithdrawalEmail(StringBuffer report) throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

	    report.append("<br><b>AutoCancelWithdrawalJob Information:</b><table border=\"1\">");
	    report.append("<tr><td><b>Users Id</b></td><td><b>Action</b></td><td><b>Trx Id</b></td><td><b>Amount</b></td><td><b>Time created</b></td></tr>");

		try {
			conn = getConnection();
			String sql =
				"SELECT * FROM ( "+
					"SELECT " +
						"u.id user_id, " +
						"u.first_name, " +
						"u.last_name, " +
						"u.user_name, " +
						"u.email, " +
						"u.skin_id as skin, " +
						"u.utc_offset, " +
						"u.language_id, " +
						"t.id transaction_id, " +
						"t.credit_card_id, " +
						"t.reference_id, " +
						"t.type_id, " +
						"t.status_id, " +
						"t.description, " +
						"t.writer_id trx_writer_id, " +
						"t.ip trx_ip, " +
						"t.comments, " +
						"t.processed_writer_id, " +
						"t.cheque_id, " +
						"t.wire_id, " +
						"t.charge_back_id, " +
						"t.receipt_num, " +
						"t.auth_number, " +
						"t.xor_id_authorize, " +
						"t.xor_id_capture, " +
						"t.clearing_provider_id, " +
						"t.time_created, " +
						"t.utc_offset_created, " +
						"t.amount, " +
						"(Select Count(Distinct(T.Type_Id)) From Transactions T, Transaction_Types Tt Where T.Type_Id = Tt.Id And Tt.Class_Type = 1 And T.User_Id = U.Id And T.Type_Id <> 38 And T.Type_Id <> 2 And t.type_id <> 14  and (t.status_id = 2 or t.status_id = 7))  As Exists_Non_Cup " +
						//"(Select Count(Distinct(T.Type_Id)) From Transactions T, Transaction_Types Tt Where T.Type_Id = Tt.Id And Tt.Class_Type = 1 And T.User_Id = U.Id And (T.Type_Id = 38 Or  T.Type_Id = 2))  As Exists_Cup_Or_Wire " +
					"FROM " +
						"users u, " +
					    "transactions t, " +
					    "transaction_types tt " +
					"WHERE " +
					    "u.id = t.user_id AND " +
					    "t.type_id = tt.id AND " +
					    "tt.class_type = ? AND " +
					    "t.status_id = ? AND " +
					    "u.class_id <> 0 AND " +
					    "(TRUNC(sysdate) - TRUNC( t.time_created)) > ? " +
					"ORDER BY " +
					    "u.skin_id, " +
					    "t.type_id " +
				") WHERE " +
					"Exists_Non_Cup > 0 Or (skin  <> 22 And skin  <> 15)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			pstmt.setLong(2, TransactionsManagerBase.TRANS_STATUS_REQUESTED);
			pstmt.setInt(3, ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_EMAIL_NOTIFICATION);
			rs = pstmt.executeQuery();

			while(rs.next()) {
				UserBase user = new UserBase();
				user.setId(rs.getLong("user_id"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setEmail(rs.getString("email"));
				user.setSkinId(rs.getLong("skin"));
				user.setUtcOffset(rs.getString("utc_offset"));
				user.setUserName(rs.getString("user_name"));
				user.setLanguageId(rs.getLong("language_id"));

		        Transaction trx = new Transaction();
		        trx.setId(rs.getLong("transaction_id"));
		        trx.setUserId(rs.getLong("user_id"));
		        trx.setCreditCardId(rs.getBigDecimal("credit_card_id"));
		        trx.setReferenceId(rs.getBigDecimal("reference_id"));
		        trx.setTypeId(rs.getLong("type_id"));
		        trx.setTimeCreated(DAOBase.convertToDate(rs.getTimestamp("time_created")));
		        trx.setAmount(rs.getLong("amount"));
		        trx.setStatusId(rs.getLong("status_id"));
		        trx.setDescription(rs.getString("description"));
		        trx.setWriterId(rs.getLong("trx_writer_id"));
		        trx.setIp(rs.getString("trx_ip"));
		        trx.setComments(rs.getString("comments"));
		        trx.setProcessedWriterId(rs.getLong("processed_writer_id"));
		        trx.setChequeId(rs.getBigDecimal("cheque_id"));
		        trx.setWireId(rs.getBigDecimal("wire_id"));
		        trx.setChargeBackId(rs.getBigDecimal("charge_back_id"));
		        trx.setReceiptNum(rs.getBigDecimal("receipt_num"));
		        trx.setAuthNumber(rs.getString("auth_number"));
		        trx.setXorIdAuthorize(rs.getString("xor_id_authorize"));
		        trx.setXorIdCapture(rs.getString("xor_id_capture"));
		        trx.setUtcOffsetCreated(rs.getString("utc_offset_created"));
		        trx.setClearingProviderId(rs.getLong("clearing_provider_id"));

				Date emailSentTime = IssuesDAOBase.isEmailSent(conn, user.getId(), trx.getId());
				String action = null;
				if (null != emailSentTime) {  // email allready sent
					Calendar today = Calendar.getInstance();
					Calendar emailIssue = Calendar.getInstance();
					emailIssue.setTime(emailSentTime);
					emailIssue.add(Calendar.DAY_OF_WEEK, ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL);
					if (today.after(emailIssue)) {
		                try {
							log.debug("Insert ReverseWithdraw: trxId: " + trx.getId());
		                	conn.setAutoCommit(false);
			                UsersDAOBase.addToBalance(conn, user.getId(), trx.getAmount());
			                GeneralDAO.insertBalanceLog(conn, Writer.WRITER_ID_AUTO, user.getId(), ConstantsBase.TABLE_TRANSACTIONS, trx.getId(), ConstantsBase.LOG_BALANCE_REVERSE_WITHDRAW, user.getUtcOffset());
			                TransactionsDAOBase.updateAfterReverse(conn, trx.getId(), TransactionsManagerBase.TRANS_STATUS_REVERSE_WITHDRAW, Writer.WRITER_ID_AUTO, user.getUtcOffset());
			                // Insert reverse transaction
			                trx.setReferenceId(new BigDecimal(trx.getId()));
			                trx.setWriterId(Writer.WRITER_ID_AUTO);
			                trx.setProcessedWriterId(Writer.WRITER_ID_AUTO);
			                trx.setTypeId(TransactionsManagerBase.TRANS_TYPE_REVERSE_WITHDRAW);
			                trx.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			                trx.setTimeSettled(new Date());
			                trx.setUtcOffsetSettled(user.getUtcOffset());
							TransactionsDAOBase.insert(conn, trx);
							log.debug("ReverseWithdraw insert successfully! revTrxId: " + trx.getId());
			                conn.commit();
			                action = "Reverse withdraw";
			                PopulationEntryBase pe = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
							if(pe != null && pe.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW &&
									pe.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
								PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), 
										pe.getCurrPopulationName(), pe.getAssignWriterId(), PopulationsManagerBase.POP_TYPE_RETENTION, pe);
							}
		                } catch (SQLException e) {
		            		log.error("Exception updating transaction. ", e);
		            		try {
		            			conn.rollback();
		            		} catch (Throwable it) {
		            			log.error("Can't rollback.", it);
		            		}
		            		throw e;
		            	} finally {
		            		try {
		            			conn.setAutoCommit(true);
		            		} catch (Exception e) {
		            			log.error("Can't set back to autocommit.", e);
		            		}
		            	}
					}
				} else {  // send withdrawal notification
				       HashMap<String,String> params = new HashMap<String, String>();
		   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, user.getFirstName());
		   		       params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, user.getLastName());
		   		       params.put(SendTemplateEmail.PARAM_CANCEL_WITHDRAWL_DAYS, String.valueOf(ConstantsBase.CANCEL_WITHDRAWAL_DAYS_FOR_CANCEL));
		   		       params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(CommonUtil.getDateTimeFormaByTz(trx.getTimeCreated(), trx.getUtcOffsetCreated())));
		   		       params.put(SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(CommonUtil.getDateTimeFormaByTz(trx.getTimeCreated(), trx.getUtcOffsetCreated())));
		   		       Template t = TemplatesDAO.get(conn, Template.CANCEL_WITHDRAWAL_EMAIL_MAIL_ID);
		   		       CommonUtil.sendSingleEmail(conn, null, t.getFileName(), params, t.getSubject(),
		   		    		   null, true, false, 0, user, Long.valueOf(trx.getId()), t.getId());

 					   // add to user MailBox
					   if (user.getSkinId() != Skin.SKIN_ETRADER) {
						   try {
							   UsersManagerBase.SendToMailBox(user, t, Writer.WRITER_ID_AUTO, user.getLanguageId(), trx.getId(), conn, 0);
						   } catch (Exception e) {
							   log.warn("Problem adding email to user inbox! userId: " + user.getId());
						   }
					   }
					   action = "Email";
				}
				if (null != action) {
					report.append("<tr>");
	                report.append("<td>").append(trx.getUserId()).append("</td>");
	                report.append("<td>").append(action).append("</td>");
	                report.append("<td>").append(trx.getId()).append("</td>");
	                report.append("<td>").append(trx.getAmount()/100).append("</td>");
	                report.append("<td>").append(trx.getTimeCreated()).append("</td>");
	                report.append("</tr>");
				}
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
	}
}