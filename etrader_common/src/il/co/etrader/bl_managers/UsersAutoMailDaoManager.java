/**
 * 
 */
package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.service.CommonJSONService;

import il.co.etrader.bl_vos.UsersAutoMail;
import il.co.etrader.dao_managers.UsersAutoMailDao;

public class UsersAutoMailDaoManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(UsersAutoMailDaoManager.class);
	public static final long AFTER_FTD_DOC_REQ_REASON = 1;
	public static final long NOTIFE_REACHE_TOTAL_DEPOSITS_REASON = 2;

	public static ArrayList<UsersAutoMail> getUsersAutoMail(long reasonId) {
		Connection conn = null;
		ArrayList<UsersAutoMail> res = new ArrayList<>();
		try {
			conn = getConnection();
			res = UsersAutoMailDao.getUsersAutoMail(conn, reasonId);
		} catch (SQLException ex) {
			logger.info("Fail to getUsersAutoMail: ", ex);
		} finally {
			closeConnection(conn);
		}

		return res;
	}
	
	public static void updateUsersAutoMailSent( ArrayList<Long> mailIds) {
		Connection conn = null;
		try {
			conn = getConnection();
			UsersAutoMailDao.updateUsersAutoMailSent(conn, mailIds);
		} catch (SQLException ex) {
			logger.info("Fail to updateUsersAutoMailSent: ", ex);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void updateUsersAutoMailReject( ArrayList<Long> mailIds) {
		Connection conn = null;
		try {
			conn = getConnection();
			UsersAutoMailDao.updateUsersAutoMailReject(conn, mailIds);
		} catch (SQLException ex) {
			logger.info("Fail to updateUsersAutoMailReject: ", ex);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<UsersAutoMail> getUsersnotifeReachedTotalDeposit(long reasonId) {
		Connection conn = null;
		ArrayList<UsersAutoMail> res = new ArrayList<>();
		try {
			conn = getConnection();
			res = UsersAutoMailDao.getUsersnotifeReachedTotalDeposit(conn, reasonId);
		} catch (SQLException ex) {
			logger.info("Fail to getUsersnotifeReachedTotalDeposit: ", ex);
		} finally {
			closeConnection(conn);
		}

		return res;
	}
}
