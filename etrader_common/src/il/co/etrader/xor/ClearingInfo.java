package il.co.etrader.xor;


import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.ConstantsBase;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Transaction;


/**
 * Clearing info bean.
 *
 * @author Shachar
 */
public class ClearingInfo {

    protected long cardUserId;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String ccn;
    protected String cvv;
    protected String expireMonth;
    protected String expireYear;
    protected int ccTypeId;
    protected int currencyId;
    protected long amount;
    protected long userId;
    protected long cardId;
    protected long transactionId;

    protected String xorTranId;
    protected String result;
    protected String message;
    protected String userMessage;
    protected String authNumber;
    protected boolean successful;
    protected String providerSign;
    protected String transactionDirection;
    protected String currencySymbol;

    public ClearingInfo() {
    }

    public ClearingInfo(CreditCard card,UserBase user,Transaction transaction){
        this.cardUserId = Long.parseLong(card.getHolderIdNum());
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.ccn = String.valueOf(card.getCcNumber());
        this.cvv = String.valueOf(card.getCcPass());
        this.expireMonth = card.getExpMonth();
        this.expireYear = card.getExpYear();
        this.ccTypeId = (int)card.getTypeId();
        this.currencyId = user.getCurrencyId().intValue();
        this.amount = transaction.getAmount();
        this.userId = user.getId();
        this.cardId = card.getId();
        this.transactionId = transaction.getId();
        this.successful = false;
        this.providerSign = ConstantsBase.PROVIDER_SIGN_DEFAULT;
        this.transactionDirection = ConstantsBase.XOR_TRANSACTION_DIRECTION_DEBIT;
        this.currencySymbol = user.getCurrency().getNameKey();

    }

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "ClearingInfo:" + ls +
                "cardUserId: " + cardUserId + ls +
                "firstName: " + firstName + ls +
                "lastName: " + lastName + ls +
                "email: " + email + ls +
                "ccn: " + ccn + ls +
                "cvv: " + cvv + ls +
                "expireMonth: " + expireMonth + ls +
                "expireYear: " + expireYear + ls +
                "ccTypeId: " + ccTypeId + ls +
                "amount: " + amount + ls +
                "customerId: " + userId + ls +
                "cardId: " + cardId + ls +
                "transactionId: " + transactionId + ls +
                "result: " + result + ls +
                "message: " + message + ls +
                "userMessage: " + userMessage + ls +
                "authNumber: " + authNumber + ls +
                "successful: " + successful + ls +
                "providerSign: " + providerSign + ls +
                "transactionDirection: " + transactionDirection +ls;
    }

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCcn() {
		return ccn;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public int getCcTypeId() {
		return ccTypeId;
	}

	public void setCcTypeId(int ccTypeId) {
		this.ccTypeId = ccTypeId;
	}

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExpireMonth() {
		return expireMonth;
	}

	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}

	public String getExpireYear() {
		return expireYear;
	}

	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProviderSign() {
		return providerSign;
	}

	public void setProviderSign(String providerSign) {
		this.providerSign = providerSign;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}


	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public String getXorTranId() {
		return xorTranId;
	}

	public void setXorTranId(String xorTranId) {
		this.xorTranId = xorTranId;
	}

	public long getCardUserId() {
		return cardUserId;
	}

	public void setCardUserId(long cardUserId) {
		this.cardUserId = cardUserId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the transactionDirection
	 */
	public String getTransactionDirection() {
		return transactionDirection;
	}

	/**
	 * @param transactionDirection the transactionDirection to set
	 */
	public void setTransactionType(String transactionDirection) {
		this.transactionDirection = transactionDirection;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

}