package il.co.etrader.xor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;


/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 *
 * @author Shachar
 */
public class CaptureJob extends JobUtil {

    private static Logger log = Logger.getLogger(CaptureJob.class);


    /**
     * Goes over the pending transactions in the db from the previous day and make a "capture" request
     * for each of them.
     */
    private static void capturePendingTransactions() {
        String hours = getPropertyByFile("xor.delay.hours");
        ReportSummery summery = new ReportSummery();
        StringBuffer report = new StringBuffer("<br><b>Capture transactions:</b><table border=\"1\">");
        report.append("<tr><td>Transaction id</td><td>Provider</td><td>Status</td><td>Description</td><td>Amount</td><td>Currency</td></tr>");
        Connection conn = null;
        Connection conn2 = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int skinId;
        String utc_offset = ConstantsBase.EMPTY_STRING;
        String lastCurrencyCode = null;

        try {
            conn = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);

            String sql =
            	" select u.id_num idnum,t.user_id userid,t.id tid,c.cc_number ccnumber,c.cc_pass cvv, " +
            	" c.exp_month expmonth, curr.code, c.exp_year expyear,c.type_id type_id,t.amount amount,t.auth_number auth_number, " +
            	" w.user_name writer, u.skin_id, u.currency_id, u.utc_offset " +
            	" from transactions t, credit_cards c,writers w,users u,skins s,currencies curr " +
            	" where t.user_id=u.id and t.credit_card_id=c.id and t.writer_id=w.id and s.id=u.skin_id and curr.id=u.currency_id and " +
            	" u.class_id <> 0 and t.status_id = "+TransactionsManagerBase.TRANS_STATUS_PENDING+" AND " +
                " t.type_id = "+TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT+" AND " ;

            //Add an option to run job with specific transcation id
            String transactionId = getPropertyByFile("transaction.id" , ConstantsBase.EMPTY_STRING);
            if ( CommonUtil.isParameterEmptyOrNull(transactionId)) {
            	sql += " t.time_created >= sysdate - 1 - "+hours+"/24 AND " +
            	" t.time_created < sysdate - "+hours+"/24 " ;
            } else {
            	sql += " t.id="+transactionId;
            }
            sql += " order by curr.id ";

            pstmt = conn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                ClearingInfo info = new ClearingInfo();
                info.setTransactionId(rs.getInt("tid"));
                info.setCcn(AESUtil.decrypt(rs.getString("ccnumber")));
                info.setExpireMonth(rs.getString("expmonth"));
                info.setExpireYear(rs.getString("expyear"));
                info.setCvv(rs.getString("cvv"));
                info.setCcTypeId(rs.getInt("type_id"));
                String currentCurrencyCode = rs.getString("code");
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
                info.setAuthNumber(rs.getString("auth_number"));
                info.setAmount(rs.getLong("amount"));
                info.setUserId(rs.getLong("userid"));
                skinId = rs.getInt("skin_id");
                if (CommonUtil.isHebrewSkin(Long.valueOf(skinId))) {
                	try {
                		info.setCardUserId(Long.valueOf(AESUtil.decrypt(rs.getString("idnum"))));
                	} catch (Exception e) {
						throw new SQLException(e.getMessage());
					}
                }
                else {  // Ao users(no id number)
                	info.setCardUserId(0);
                }
                info.setTransactionType(ConstantsBase.XOR_TRANSACTION_DIRECTION_DEBIT);
                utc_offset = rs.getString("utc_offset");      //save user utc offset
                String authorizeAuthNumber = info.getAuthNumber();

                if (log.isEnabledFor(Level.INFO)) {
                    log.log(Level.INFO, "Going to capture transaction: " + info.toString() + " on provider: XOR");
                }
                Connection connClearingProvider = null;
                try {
                	connClearingProvider = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);
                	XORClearingProvider cp = new XORClearingProvider();
                	String system = getPropertyByFile("system");

                	if(system.equals(ConstantsBase.LIVE_SYSTEM)) {
//                		cp = SkinsDAOBase.getXORClearingProvider(connClearingProvider,skinId,0,1);
                	}
                	else {
//                		cp = SkinsDAOBase.getXORClearingProvider(connClearingProvider,skinId,1,1);
                	}

                    cp.capture(info);

                } catch (ClearingException ce) {
                    log.log(Level.ERROR, "Failed to capture transaction: " + info.toString(), ce);
                    info.setResult("999");
                    info.setMessage(ce.getMessage());
                } finally {
                	try {
                		connClearingProvider.close();
                	} catch (Exception e) {
                		log.warn("cant close connection: connClearingProvider", e);
					}
                }

                // currency code display
                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
                    lastCurrencyCode = currentCurrencyCode;
                    report.append("<tr>");
                    report.append("<td colspan=\"6\" align=\"center\" valign=\"middle\"><b>" + lastCurrencyCode + "</b>");report.append("</td>");
                    report.append("</tr>");
                }

                if (info.isSuccessful()) {
                    report.append("<tr>");
                } else {
                    report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
                }

                report.append("<td>").append(info.getTransactionId()).append("</td><td>");
                report.append("XOR").append("</td><td>");
                report.append(info.getResult()).append("</td><td>");
                report.append(info.getMessage()).append("</td><td>");
                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
                report.append(currentCurrencyCode).append("</td></tr>\n");
                summery.addTransaction(rs.getString("writer"), "XOR", info.getAmount(), currentCurrencyCode, info.isSuccessful());

                int result = -1;
                try {
                    result = Integer.parseInt(info.getResult());
                } catch (NumberFormatException nfe) {
                    log.log(Level.WARN, "Result not a number.", nfe);
                }
                String captureAuthNumber = info.getAuthNumber();
                if (null != authorizeAuthNumber &&
                        null != captureAuthNumber &&
                        !authorizeAuthNumber.equals(captureAuthNumber)) {
                    captureAuthNumber = authorizeAuthNumber + "|" + captureAuthNumber;
                } else {
                    captureAuthNumber = authorizeAuthNumber;
                }
                try {
	                conn2 = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);
	                String updateSql;
		            if (info.isSuccessful()) {
		            	updateSql = "update transactions set time_settled=sysdate, status_id="+TransactionsManagerBase.TRANS_STATUS_SUCCEED+
		            	 				   ",comments='"+info.getResult()+"|"+info.getMessage() + "|"+info.getUserMessage()+"|"+info.getXorTranId()+"'"+
		            	 				   " , xor_id_capture='"+info.getXorTranId()+"' , " +
		            	 				   " utc_offset_settled = '"+ utc_offset + "' where id="+info.getTransactionId();
		            } else {
		            	updateSql = "update transactions set time_settled=sysdate, status_id="+TransactionsManagerBase.TRANS_STATUS_FAILED+
		            					   ",comments='"+info.getResult()+"|"+info.getMessage() + "|"+info.getUserMessage()+"|"+info.getXorTranId()+"'"+
		          	 					   " , description='"+ConstantsBase.ERROR_FAILED_CAPTURE+"', xor_id_capture='"+info.getXorTranId()+"' , " +
		          	 					   " utc_offset_settled = '"+ utc_offset + "' where id="+info.getTransactionId();
		            }
		            conn2.prepareStatement(updateSql).executeUpdate();
                } finally {
                	try {
                		conn2.close();
                    } catch (Exception e) {
                    	log.log(Level.ERROR, "Can't close", e);
                    }
                }

            }

        } catch (Exception e) {
            log.log(Level.ERROR, "Error while capturing pending transactions.", e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

        }
        report.append("</table>");

        String reportBody = "<html><body>" +
            summery.getSummery() +
            report.toString() +
            "</body></html>";

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }

        // send email
        Hashtable server = new Hashtable();
        server.put("url", getPropertyByFile("xor.email.url"));
        server.put("auth", getPropertyByFile("xor.email.auth"));
        server.put("user", getPropertyByFile("xor.email.user"));
        server.put("pass", getPropertyByFile("xor.email.pass"));
        server.put("contenttype", getPropertyByFile("xor.email.contenttype", "text/html; charset=UTF-8"));

        Hashtable email = new Hashtable();
        email.put("subject", getPropertyByFile("xor.email.subject"));
        email.put("to", getPropertyByFile("xor.email.to"));
        email.put("from", getPropertyByFile("xor.email.from"));
        email.put("body", reportBody);
        sendEmail(server, email);

    }

    /**
     * Capture Job.
     *
     * @param args
     */
    public static void main(String[] args) {

        if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }

        propFile = args[0];


        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }

        capturePendingTransactions();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Job completed.");
        }
    }
}

/**
 * Helper class that wrap the report summery.
 *
 * @author Shachar
 */
class ReportSummery {
    private Hashtable senders;

    /**
     * Create report summery wrapper.
     */
    ReportSummery() {
        senders = new Hashtable();
    }

    /**
     * Add transaction to the report summery.
     *
     * @param sender the sender of the transaction
     * @param provider the provider processed the transaction
     * @param amount the transaction amount
     * @param currency the transaction currency
     * @param successful if the transaction was successful
     */
    void addTransaction(String sender, String provider, long amount, String currency, boolean successful) {
        getSender(sender).addTransaction(provider, amount, currency, successful);
    }

    /**
     * Gets the wrapper for specified sender. Create it and add it
     * to the senders if needed.
     *
     * @param sender the sender name we need wrapper for
     * @return <code>SenderSummery</code> for the specified sender.
     */
    SenderSummery getSender(String sender) {
        SenderSummery ss = (SenderSummery) senders.get(sender);
        if (null == ss) {
            ss = new SenderSummery(sender);
            senders.put(sender, ss);
        }
        return ss;
    }

    /**
     * Create the summery part of the report.
     *
     * @return HTML representing the summery part of the report.
     */
    String getSummery() {
        StringBuffer sb = new StringBuffer("<b>Capture Summary:</b><br><table border=\"1\">");
        sb.append("<tr><td>Sender</td><td>Provider</td><td>#Transactions</td><td>#Errors</td><td>Totals</td></tr>");
        for (Enumeration e = senders.keys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            SenderSummery ss = (SenderSummery) senders.get(key);
            ss.getSummery(sb);
        }
        sb.append("</table>");
        return sb.toString();
    }

    /**
     * Wrapper of the summery info for single sender.
     *
     * @author Shachar
     */
    class SenderSummery {
        private String senderName;
        private Hashtable providers;

        /**
         * Create new sender summery.
         *
         * @param sender the name of the sender
         */
        SenderSummery(String sender) {
            senderName = sender;
            providers = new Hashtable();
        }

        /**
         * Add transaction to the sender summery.
         *
         * @param provider the provider processed the transaction
         * @param amount the transaction amount
         * @param currency the transaction currency
         * @param successful if the transaction was successful
         */
        void addTransaction(String provider, long amount, String currency, boolean successful) {
            getProvider(provider).addTransaction(amount, currency, successful);
        }

        /**
         * Gets the wrapper for specified provider. Create it and add it
         * to the providers if needed.
         *
         * @param provider the provider name we need wrapper for
         * @return <code>ProviderSummery</code> for the specified provider.
         */
        ProviderSummery getProvider(String provider) {
            ProviderSummery ps = (ProviderSummery) providers.get(provider);
            if (null == ps) {
                ps = new ProviderSummery(provider);
                providers.put(provider, ps);
            }
            return ps;
        }

        /**
         * Append to the summery <code>StringBuffer</code> the info about the
         * represented sender.
         *
         * @param sb the buffer in which to append the summery
         */
        void getSummery(StringBuffer sb) {
            for (Enumeration e = providers.keys(); e.hasMoreElements();) {
                String key = (String) e.nextElement();
                ProviderSummery ps = (ProviderSummery) providers.get(key);
                ps.getSummery(sb);
            }
        }

        /**
         * Wrapper of the summery info for single provider.
         *
         * @author Tony
         */
        class ProviderSummery {
            private String providerName;
            private int transactionsCount;
            private int failCount;
            private Hashtable currencies;

            /**
             * Create new provider summery.
             *
             * @param provider the name of the provider
             */
            ProviderSummery(String provider) {
                providerName = provider;
                transactionsCount = 0;
                failCount = 0;
                currencies = new Hashtable();
            }

            /**
             * Add transaction to the sender summery.
             *
             * @param provider the provider processed the transaction
             * @param amount the amount of the transaction
             * @param currency the currency of the transaction
             * @param successful if the transaction was successful
             */
            void addTransaction(long amount, String currency, boolean successful) {
                getCurrency(currency).addTransaction(amount);
                transactionsCount++;
                if (!successful) {
                    failCount++;
                }
            }

            /**
             * Gets the wrapper for specified currency. Create it and add it
             * to the currencies if needed.
             *
             * @param currency the currency we need wrapper for
             * @return <code>CurrencySummery</code> for the specified currency.
             */
            CurrencySummery getCurrency(String currency) {
                CurrencySummery cs = (CurrencySummery) currencies.get(currency);
                if (null == cs) {
                    cs = new CurrencySummery();
                    currencies.put(currency, cs);
                }
                return cs;
            }

            /**
             * Append to the summery <code>StringBuffer</code> the info about the
             * represented provider.
             *
             * @param sb the buffer in which to append the summery
             */
            void getSummery(StringBuffer sb) {
                sb.append("<tr><td>").append(senderName);
                sb.append("</td><td>").append(providerName);
                sb.append("</td><td>").append(transactionsCount);
                sb.append("</td><td>").append(failCount);
                sb.append("</td><td>");
                boolean first = true;
                for (Enumeration e = currencies.keys(); e.hasMoreElements();) {
                    String key = (String) e.nextElement();
                    CurrencySummery cs = (CurrencySummery) currencies.get(key);
                    if (first) {
                        first = false;
                    } else {
                        sb.append(" | ");
                    }
                    sb.append(ClearingUtil.formatAmount(cs.getAmount())).append(" ").append(key);
                }
                sb.append("</td></tr>");
            }

            /**
             * Object wrapping <code>long</code> to be used to sum the amounts in
             * one currency.
             *
             * @author Tony
             */
            class CurrencySummery {
                long amount;

                /**
                 * Create new currency amount wrapper.
                 */
                CurrencySummery() {
                    amount = 0;
                }

                /**
                 * Add amount to the currency.
                 *
                 * @param amount the amount to add
                 */
                void addTransaction(long amount) {
                    this.amount += amount;
                }

                /**
                 * @return The currenct amount for the currency.
                 */
                long getAmount() {
                    return amount;
                }
            }
        }
    }

}