package il.co.etrader.xor;

import il.co.etrader.util.ConstantsBase;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.anyoption.common.clearing.ClearingException;



/**
 * Implements the XOR clearing interface communication.
 *
 * @author Shachar
 */
public class XORClearingProvider /*implements ClearingProvider*/ {
    private static Logger log = Logger.getLogger(XORClearingProvider.class);

    private String terminalId;
    private String url;
    private String username;
    private String password;
    private String sign;
    private String isTestTerminal;


    public String getIsTestTerminal() {
		return isTestTerminal;
	}

	public void setIsTestTerminal(String isTestTerminal) {
		this.isTestTerminal = isTestTerminal;
	}

	/**
     * Process an "authorize" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public  void authorize(ClearingInfo info) throws ClearingException {
        info.setProviderSign(sign);
        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, info.toString());
        }
        try {
            String request = createRequest(info);
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
           // String response = "<?xml version='1.0'?><ashrait><response><command>doDeal</command><dateTime>2006-10-12 14:31</dateTime><requestId>a47</requestId><tranId>1181011</tranId><result>000</result><message>Invalid value in an XML field</message><userMessage>Please contact System Administration</userMessage><additionalInfo>transactionType value is incorrect</additionalInfo><version>1001</version><language>Eng</language><doDeal><status>331</status><statusText>Invalid value in an XML field</statusText><cardNo></cardNo><cardName></cardName><cardType code=\"\"></cardType><creditCompany code=\"\"></creditCompany><cardBrand code=\"\"></cardBrand><cardAcquirer code=\"\"></cardAcquirer><cardExpiration></cardExpiration><serviceCode></serviceCode><transactionType code=\"\"></transactionType><creditType code=\"\"></creditType><currency code=\"\"></currency><transactionCode code=\"\"></transactionCode><total></total><balance></balance><starTotal></starTotal><firstPayment></firstPayment><periodicalPayment></periodicalPayment><numberOfPayments></numberOfPayments><clubId></clubId><clubCode></clubCode><validation code=\"\"></validation><commReason code=\"\"></commReason><idStatus code=\"\"></idStatus><cvvStatus code=\"\"></cvvStatus><authSource code=\"\"></authSource><authNumber></authNumber><fileNumber></fileNumber><slaveTerminalNumber></slaveTerminalNumber><slaveTerminalSequence></slaveTerminalSequence><creditGroup></creditGroup><pinKeyIn></pinKeyIn><pfsc></pfsc><ptc></ptc><eci></eci><cavv code=\"\"></cavv><user></user><addonData></addonData><supplierNumber></supplierNumber><intIn>B4111111111111111C100D1150J5T0909X47</intIn><intOt></intOt><systemSpecific></systemSpecific></doDeal></response></ashrait>";
            parseResponse(response, info);
        } catch (Exception e) {
            throw new ClearingException("Transaction failed.", e);
        }
    }

    /**
     * Process an "authorize" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public  void authorizeByCode(ClearingInfo info,int code) throws ClearingException {
        info.setProviderSign(sign);
        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, info.toString());
        }
        try {
            String request = createRequestByCode(info,code);
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, request);
            }
            String body =
                "user=" + URLEncoder.encode(username,"UTF-8") +
                "&password=" + URLEncoder.encode(password,"UTF-8") +
                "&int_in=" + URLEncoder.encode(request,"UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, body);
//            String response = "<?xml version='1.0'?><ashrait><response><command>doDeal</command><dateTime>2006-10-12 14:31</dateTime><requestId>a47</requestId><tranId>1181011</tranId><result>331</result><message>Invalid value in an XML field</message><userMessage>Please contact System Administration</userMessage><additionalInfo>transactionType value is incorrect</additionalInfo><version>1001</version><language>Eng</language><doDeal><status>331</status><statusText>Invalid value in an XML field</statusText><cardNo></cardNo><cardName></cardName><cardType code=\"\"></cardType><creditCompany code=\"\"></creditCompany><cardBrand code=\"\"></cardBrand><cardAcquirer code=\"\"></cardAcquirer><cardExpiration></cardExpiration><serviceCode></serviceCode><transactionType code=\"\"></transactionType><creditType code=\"\"></creditType><currency code=\"\"></currency><transactionCode code=\"\"></transactionCode><total></total><balance></balance><starTotal></starTotal><firstPayment></firstPayment><periodicalPayment></periodicalPayment><numberOfPayments></numberOfPayments><clubId></clubId><clubCode></clubCode><validation code=\"\"></validation><commReason code=\"\"></commReason><idStatus code=\"\"></idStatus><cvvStatus code=\"\"></cvvStatus><authSource code=\"\"></authSource><authNumber></authNumber><fileNumber></fileNumber><slaveTerminalNumber></slaveTerminalNumber><slaveTerminalSequence></slaveTerminalSequence><creditGroup></creditGroup><pinKeyIn></pinKeyIn><pfsc></pfsc><ptc></ptc><eci></eci><cavv code=\"\"></cavv><user></user><addonData></addonData><supplierNumber></supplierNumber><intIn>B4111111111111111C100D1150J5T0909X47</intIn><intOt></intOt><systemSpecific></systemSpecific></doDeal></response></ashrait>";
            parseResponse(response, info);
        } catch (Exception e) {
            throw new ClearingException("Transaction failed.", e);
        }
    }

    /**
     * Process an "capture" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void capture(ClearingInfo info) throws ClearingException {
        authorize(info);
    }

    public void captureByCode(ClearingInfo info,int code) throws ClearingException {
        authorizeByCode(info,code);
    }
    /**
     * Process an "cancel" call through this provider.
     *
     * @param info thransaction info
     * @throws ClearingException
     */
    public void cancel(ClearingInfo info) throws ClearingException {
        info.setSuccessful(true);
        info.setMessage("Permitted transaction.");
        info.setUserMessage("Permitted transaction.");
    }

    /**
     * Check if this transaction will not exceed the provider limits.
     *
     * @param info thransaction info
     * @return <code>true</code> if the transaction do NOT exceed the limits else <code>false</code>.
     * @throws ClearingException
     */
    public boolean checkLimits(ClearingInfo info) throws ClearingException {
        return true;
    }

    /**
     * Set clearing provider properties.
     *
     * @param key
     * @param props
     * @throws ClearingException
     */
    public void setProperties(String key, Properties props) throws ClearingException {
        url = props.getProperty(key+".url");
        terminalId = props.getProperty(key+".terminalId");
        username = props.getProperty(key+".username");
        password = props.getProperty(key+".password");
        sign = props.getProperty(key+".sign");
        isTestTerminal = props.getProperty(key+".isTestTerminal");

        if (log.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            log.log(Level.DEBUG, ls +
                    "XORClearingProvider configured with:" + ls +
                    "url: " + url + ls +
                    "terminalId: " + terminalId + ls +
                    "usernane: " + username + ls +
                    "sign: " + sign + ls);
        }
    }

    /**
     * Create the XML string for "doDeal" request.
     *
     * @param info transaction info
     * @return New <code>String</code> with the authorize XML.
     */
    private  String createRequest(ClearingInfo info) {
        boolean authorize = (null == info.getAuthNumber() || info.getAuthNumber().trim().equals("")) && !info.transactionDirection.equals(ConstantsBase.XOR_TRANSACTION_DIRECTION_CREDIT);
        String transactionIdPrefix = "";
        transactionIdPrefix = (authorize ? "a" : "c");

        boolean commit =!authorize;
		String cardUserId = info.getCardUserId()!=0 ? "<id>"+info.getCardUserId()+"</id>" : ConstantsBase.EMPTY_STRING;
        return
            "<ashrait>" +
                "<request>" +
                    "<command>doDeal</command>" +
                    "<requestId>" + transactionIdPrefix + info.getTransactionId() + "</requestId>" +
                    "<version>1001</version>" +
                    "<language>Eng</language>" +
                    "<doDeal>" +
                        "<terminalNumber>" + terminalId + "</terminalNumber>" +
                        "<cardNo>" + info.getCcn() + "</cardNo>" +
                        "<cardExpiration>" + info.getExpireMonth() + info.getExpireYear() + "</cardExpiration>" +
                        "<cvv>" + (null != info.getCvv() && !info.getCvv().equals("") ? info.getCvv() : "2") + "</cvv>" +
                        cardUserId +
                        "<transId>" + info.getTransactionId() + "</transId>" +
                        "<transactionType>" + info.getTransactionDirection() + "</transactionType>" +
                        "<creditType>RegularCredit</creditType>" +
                        "<currency>"+info.getCurrencySymbol() +"</currency>" +
                        //"<currency>ILS</currency>" +
                        "<transactionCode>Phone</transactionCode>" +
                        (!authorize ? "<authNumber>" + info.getAuthNumber() + "</authNumber>" : "") +
                        "<total>" + info.getAmount() + "</total>" +
                        "<validation>" + (commit ? "AutoComm" : "Verify") + "</validation>" +
                        "<user>" + info.getTransactionId() + "</user>" +
                        "<addOnData>" + info.getTransactionId() + "</addOnData>"+
                        "<mayBeDuplicate>0</mayBeDuplicate>" +
                    "</doDeal>" +
                "</request>" +
            "</ashrait>";
    }

    /**
     * Create the XML string for "doDeal" request.
     *
     * @param info transaction info
     * @return New <code>String</code> with the authorize XML.
     */
    private  String createRequestByCode(ClearingInfo info,int code) {
        boolean authorize = (null == info.getAuthNumber() || info.getAuthNumber().trim().equals(""));
        String transactionIdPrefix = "";
        transactionIdPrefix = (authorize ? "a" : "c");

        boolean commit =!authorize;
		String cardUserId = info.getCardUserId()!=0 ? "<id>"+info.getCardUserId()+"</id>" : ConstantsBase.EMPTY_STRING;
        String out=
            "<ashrait>" +
                "<request>" +
                    "<command>doDeal</command>" +
                    "<requestId>" + transactionIdPrefix + info.getTransactionId() + "</requestId>" +
                    "<version>1001</version>" +
                    "<language>Eng</language>" +
                    "<doDeal>" +
                        "<terminalNumber>" + terminalId + "</terminalNumber>" +
                        "<cardNo>" + info.getCcn() + "</cardNo>" +
                        "<cardExpiration>" + info.getExpireMonth() + info.getExpireYear() + "</cardExpiration>" +
                        "<cvv>" + (null != info.getCvv() && !info.getCvv().equals("") ? info.getCvv() : "2") + "</cvv>" +
                        cardUserId +
                        "<transId>" + info.getTransactionId() + "</transId>" +
                        "<transactionType>" + info.getTransactionDirection() + "</transactionType>" +
                        "<creditType>RegularCredit</creditType>" +
                        "<currency>"+info.getCurrencySymbol()+"</currency>" +
                        //"<currency>ILS</currency>" +
                        "<transactionCode>Phone</transactionCode>";

           if (!authorize) {
        	   if (code!=62) {
        		   out+="<authNumber>" + info.getAuthNumber() + "</authNumber>" ;
        	   }
           }

                   out+=
                        "<total>" + info.getAmount() + "</total>"+
                        "<validation>" + (commit ? "AutoComm" : "Verify") + "</validation>" +
                        "<user>" + info.getTransactionId() + "</user>" +
                        "<addOnData>" + info.getTransactionId() + "</addOnData>"+
                        "<mayBeDuplicate>0</mayBeDuplicate>" +
                    "</doDeal>" +
                "</request>" +
            "</ashrait>";

            return out;
    }

    /**
     * Parse XOR "doDeal" result.
     *
     * @param result the result XML
     * @return <code>Hashtable</code> with result values.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private  void parseResponse(String response, ClearingInfo info) throws ParserConfigurationException, SAXException, IOException {
        Document respDoc = ClearingUtil.parseXMLToDocument(response);
//        Element respRoot = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "ashrait/response");
        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
        info.setResult(ClearingUtil.getElementValue(root, "result/*"));
        info.setMessage(ClearingUtil.getElementValue(root, "message/*"));
        info.setUserMessage(ClearingUtil.getElementValue(root, "userMessage/*"));
        info.setXorTranId(ClearingUtil.getElementValue(root, "tranId/*"));
        info.setSuccessful(info.getResult().equals("000"));

        if (info.isSuccessful()) {
            info.setAuthNumber(ClearingUtil.getElementValue(root, "doDeal/authNumber/*"));
        }

        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, info.toString());
        }
    }



}