package il.co.etrader.xor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class ClearingUtil {
    private static Logger log = Logger.getLogger(ClearingUtil.class);

	public static final int WEB_LIST_ROWS_ON_PAGE = 20;

	public static final String SENDER_ID = "com.smartclearing.util.ClearingUtil.SENDER_ID";

	private static final String LIST_PAGES = "pages";

	public static final String DATA_SOURCE_PATH_STANDALONE = "clearing";

//    private static DecimalFormat sumFormat = new DecimalFormat("#,##0.00");
//    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");

    private static final String SQL_GET_SENDER_ID = "SELECT sender_id FROM users WHERE uname = ?";

    public static final int XOR_ERROR_TRIES = 3;
    public static final int XOR_ERROR_TIMEOUT = 10000;
    public static final int XOR_ERROR_WAIT = 200;

    private static final String SQL_LOOKUP_VALUE_CARD =
        "SELECT " +
            "A.*," +
            "B.code AS currency " +
        "FROM " +
            "value_cards A, " +
            "currencies B " +
        "WHERE " +
            "A.currency_id = B.id AND " +
            "A.pin = ?";
    private static final String SQL_ACTIVATE_VALUE_CARD =
        "UPDATE " +
            "value_cards " +
        "SET " +
            "status_id = 3, " +
            "time_activated = current_timestamp, " +
            "sender_id = ?, " +
            "customer_id = ? " +
        "WHERE " +
            "id = ?";

    private static DataSource dataSource;

    public static final String DB_URL = "xor.db.url";
    public static final String DB_USER = "xor.db.user";
    public static final String DB_PASS = "xor.db.pass";

    /**
     * @return The SmartClearing application data source. Look it up from JNDI if not yet looked up.
     */
    /*
    public static DataSource getDataSource() {
        if (null == dataSource) {
            try {
                Context initCtx = new InitialContext();
                Context envCtx = (Context)initCtx.lookup("java:comp/env");
                dataSource = (DataSource)envCtx.lookup("jdbc/clearing");
            } catch (NamingException ne) {
                    log.log(Level.ERROR, "Can't lookup the DataSource.", ne);
            }
        }
        return dataSource;
    }*/


    /**
     * Bind data source to JNDI with specified paramethers.
     *
     * @param dburl the db url
     * @param dbuser the db un
     * @param dbpass the db ps
     * @throws NamingException
     */
    /*
    public static void bindDataSource(String dburl, String dbuser, String dbpass) throws NamingException,SQLException {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
        InitialContext ic = new InitialContext();

        // Construct BasicDataSource reference
        //Reference ref = new Reference("javax.sql.DataSource", "org.apache.commons.dbcp.BasicDataSourceFactory", null);
        //ref.add(new StringRefAddr("driverClassName", "oracle.jdbc.driver.OracleDriver"));
        //ref.add(new StringRefAddr("url", dburl));
        //ref.add(new StringRefAddr("username", dbuser));
        //ref.add(new StringRefAddr("password", dbpass));

        /*name="jdbc/etraderdb"
    		auth="Container"
    		type="oracle.jdbc.pool.OracleDataSource"
    		factory="oracle.jdbc.pool.OracleDataSourceFactory"
    		driverClassName="oracle.jdbc.OracleDriver"
    		url="jdbc:oracle:thin:@192.118.55.252:1521:etrader"
    		user="etrader"
    		password="etrader"
    		maxActive="20"
    		maxIdle="10"
    		maxWait="-1"
    		removeAbandoned="true"
    		removeAbandonedTimeout="60"
    		logAbandoned="true"
    		validationQuery="SELECT 1 FROM dual"*/
/*
    	OracleDataSource ods = new OracleDataSource();

    	ods.setURL("jdbc:oracle:thin:@192.118.55.252:1521:etrader");
    	ods.setUser("etrader");
    	ods.setPassword("etrader");

        ic.bind(DATA_SOURCE_PATH_STANDALONE, ods);
        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "DataSource bound.");
        }
    }*/

    /**
     * @return The SmartClearin DataSource.
     */
    /*
    public static DataSource getStandaloneDataSource() {
        if (null == dataSource) {
            try {
                Context initCtx = new InitialContext();
                dataSource = (DataSource) initCtx.lookup(DATA_SOURCE_PATH_STANDALONE);
            } catch (NamingException ne) {
                log.log(Level.ERROR, "Can't lookup standalone DataSource.", ne);
            }
        }
        return dataSource;
    }*/

    /**
     * Execute POST request and return the response as string.
     *
     * @param url the URL to POST to
     * @param xmlRequest the request body
     * @return String with the response from the server.
     * @throws IOException
     */
    public static String executePOSTRequest(String url, String xmlRequest) throws IOException {
        return executePOSTRequest(url, xmlRequest, false, true, null);
    }

    /**
     * Execute POST request and return the response as string.
     *
     * @param url the URL to POST to
     * @param xmlRequest the request body
     * @param trustAllCerts if <code>true</code> no certificate check will be done
     * @param verifyHostname
     * @param headers hashtable with headers (key/value) that need to be passed in the request
     * @return String with the response from the server.
     * @throws IOException
     */
    public static String executePOSTRequest(
            String url,
            String xmlRequest,
            boolean trustAllCerts,
            boolean verifyHostname,
            Hashtable headers) throws IOException {
        if (log.isEnabledFor(Level.DEBUG)) {
            log.log(Level.DEBUG, url);
            log.log(Level.DEBUG, xmlRequest);
        }

        if (trustAllCerts) {
            // TODO: see how to properly uninstall it before use it
            //Create a trust manager that does not validate certificate chains.
//            TrustManager[] trustAllCertsTM = new TrustManager[] {
//                new X509TrustManager() {
//                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
//                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType){}
//                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType){}
//                }
//            };

            //Install the all-trusting trust manager
//            try  {
//                SSLContext sc = SSLContext.getInstance("SSL");
//                sc.init(null, trustAllCertsTM, new java.security.SecureRandom());
//                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Error.", e);
//            }
        }

        HostnameVerifier defaultHV = null;
        if (!verifyHostname) {
            // save the default one
            defaultHV = HttpsURLConnection.getDefaultHostnameVerifier();

            // create veirier that accept all
            HostnameVerifier hv = new HostnameVerifier() {
                public boolean verify(String urlHostName, SSLSession session) {
                    log.log(Level.WARN, "URL Host: " + urlHostName + " vs. " + session.getPeerHost());
                    return true;
                }
            };
            // and install it
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        }

        // set the connect and read timeouts for the HttpURLConnection
        // this is a Sun JVM specific way to do it
        System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
        System.setProperty("sun.net.client.defaultReadTimeout", "30000");
        if (log.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            log.log(Level.DEBUG,
                    ls +
                    "sun.net.client.defaultConnectTimeout: " +
                    System.getProperty("sun.net.client.defaultConnectTimeout") +
                    ls +
                    "sun.net.client.defaultReadTimeout: " +
                    System.getProperty("sun.net.client.defaultReadTimeout"));
        }
        String response = null;
        int trys = 1;
        while (true) {
            long startTime = System.currentTimeMillis();
            InputStream is = null;
            InputStreamReader isr = null;
            BufferedReader br = null;
            try {
                byte[] data = xmlRequest.getBytes("UTF-8");
                URL u = new URL(url);
                HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
                httpCon.setDoOutput(true);
                httpCon.setRequestMethod("POST");
                httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpCon.setRequestProperty("Content-Length", String.valueOf(data.length));
                if (null != headers) {
                    for (Enumeration e = headers.keys(); e.hasMoreElements();) {
                        String key = (String) e.nextElement();
                        httpCon.setRequestProperty(key, (String) headers.get(key));
                    }
                }
                OutputStream os = httpCon.getOutputStream();
                os.write(data, 0, data.length);
                os.flush();
                try {
                    os.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close output stream.", e);
                }
                if (log.isEnabledFor(Level.DEBUG)) {
                    log.log(Level.DEBUG, "Request sent.");
                }
                int respCode = httpCon.getResponseCode();
                if (log.isEnabledFor(Level.DEBUG)) {
                    log.log(Level.DEBUG, "ResponseCode: " + respCode);
                }
                is = httpCon.getInputStream();
                isr = new InputStreamReader(is, "ISO-8859-8");
                //isr = new InputStreamReader(is, "UTF-8");
                br = new BufferedReader(isr);
                StringBuffer xmlResp = new StringBuffer();
                String line = null;
                while ((line = br.readLine()) != null) {
                    xmlResp.append(line);
                }
                response = xmlResp.toString();
                if (log.isEnabledFor(Level.DEBUG)) {
                    log.log(Level.DEBUG, "Response received.");
                    log.log(Level.DEBUG, response);
                }
            } catch (IOException ioe) {
                if (trys < XOR_ERROR_TRIES &&
                        System.currentTimeMillis() - startTime < XOR_ERROR_TIMEOUT) {
                    int sleep = XOR_ERROR_WAIT;
                    if (log.isEnabledFor(Level.INFO)) {
                        log.log(Level.INFO, "Communication error", ioe);
                        log.log(Level.INFO, "Sleep " + sleep);
                    }
                    try {
                        Thread.sleep(sleep);
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Sleep interrupted", e);
                    }
                    trys++;
                    if (log.isEnabledFor(Level.INFO)) {
                        log.log(Level.INFO, "Try " + trys);
                    }
                    continue;
                } else {
                    log.log(Level.ERROR, "Give up");
                    throw ioe;
                }
            } finally {
                if (null != br) {
                    try {
                        br.close();
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Can't close the buffered reader.", e);
                    }
                }
                if (null != isr) {
                    try {
                        isr.close();
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Can't close the input stream reader.", e);
                    }
                }
                if (null != is) {
                    try {
                        is.close();
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Can't close the input stream.", e);
                    }
                }
            }
            break;
        }

        if (!verifyHostname) {
            // restore the default verifier
            HttpsURLConnection.setDefaultHostnameVerifier(defaultHV);
        }

        return response;
    }

    /**
     * Execute POST request and return the response as string.
     *
     * @param url the URL to POST to
     * @param xmlRequest the request body
     * @return String with the response from the server.
     * @throws IOException
     * @throws IOException
     */
    public static String executePOSTRequest(String url,ArrayList keys, ArrayList values) throws IOException{
    	StringBuffer content = new StringBuffer();

        // Build the content first so that we can set the Content-Length in the header
        for (int i=0; i < keys.size(); i++)
        {
        	content.append((String)keys.get(i) + "=" + URLEncoder.encode((String)values.get(i),"UTF-8") + "&");
        }

        return executePOSTRequest(url,content.toString());
    }

    /**
     * Create DOM <code>Document</code> from XML <code>String</code>.
     *
     * @param xml the XML to parse
     * @return New DOM <code>Document</code> with the parsed XML.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document parseXMLToDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        byte[] xmldata = xml.getBytes();
        ByteArrayInputStream bais = new ByteArrayInputStream(xmldata);
        Document resp = docBuilder.parse(bais);
        try {
                bais.close();
        } catch (Exception e) {
        }
        return resp;
    }

    /**
     * Gets a Node under certain path under specified Element. If a path that do
     * no exists yet in the element is requested and the last token in the path
     * is "*" the path will be created and the "*" will be created as TextNode and
     * returned.
     *
     * @param element the Element to look under
     * @param xmlPath the path to look for
     * @return The Node with the requested path under the specified Element (created if needed).
     */
    public static Node getNode(Element element, String xmlPath) {
        StringTokenizer st = new StringTokenizer(xmlPath, "/");
        Element crrElement = element;
        while (st.hasMoreTokens()) {
            String nodeName = st.nextToken();
            if (!nodeName.equals("*")) {
                NodeList nl = crrElement.getElementsByTagName(nodeName);
                if (nl.getLength() == 1) {
                    crrElement = (Element)nl.item(0);
                } else if (nl.getLength() > 1) {
                    return null;
                } else {
                    Element newElement = crrElement.getOwnerDocument().createElement(nodeName);
                    crrElement.appendChild(newElement);
                    crrElement = newElement;
                }
            } else {
                NodeList nl = crrElement.getChildNodes();
                if (nl.getLength() == 0) {
                    Text newNode = crrElement.getOwnerDocument().createTextNode(null);
                    crrElement.appendChild(newNode);
                    return newNode;
                } else if (nl.getLength() == 1 && nl.item(0) instanceof Text) {
                    return crrElement.getFirstChild();
                } else {
                    return null;
                }
            }
        }

        return crrElement;
    }

    /**
     * Get a text sub node value.
     *
     * @param element the parent element
     * @param path the path of the text subnode
     * @return The value of the text subnode or null if doesn't exist.
     */
    public static String getElementValue(Element element, String path) {
        Text tn = (Text) getNode(element, path);
        if (null != tn) {
            return tn.getData();
        }
        return null;
    }

    /**
     * Sets and attribute to Node with specified name and value.
     *
     * @param doc the doc to look for Node in
     * @param path the path of the Node
     * @param attrName attribute name
     * @param attrValue attribute value
     */
    public static void setAttribute(Document doc, String path, String attrName, String attrValue) {
        Element requestElement = (Element)getNode(doc.getDocumentElement(), path);
        Attr attr = requestElement.getAttributeNode(attrName);
        if (null == attr) {
            requestElement.setAttribute(attrName, attrValue);
        } else {
            attr.setValue(attrValue);
        }
    }

    /**
     * Sets and attribute to Node with specified name and value.
     *
     * @param doc the doc to look for Node in
     * @param path the path of the Node
     * @param attrName attribute name
     * @param attrValue attribute value
     */
    public static String getAttribute(Element element, String path, String attrName) {
        Element e = (Element)getNode(element, path);
        Attr attr = e.getAttributeNode(attrName);
        if (null == attr) {
            return null;
        } else {
            return attr.getValue();
        }
    }

    /**
     * Sets the value of a Node to the specified one.
     *
     * @param element the Element to look for the path under
     * @param xmlPath the path of the Node under the Element
     * @param value the value to set to the Node
     * @throws DOMException
     */
    public static void setNodeValue(Element element, String xmlPath, String value) throws DOMException {
        Text textNode = (Text)getNode(element, xmlPath);
        textNode.setData(value);
    }

    /**
     * Transform a Document in to String.
     *
     * @param doc the Document to transform
     * @return String representation of the Document.
     */
    public static String transformDocumentToString(Document doc) {
        String xml = null;
        try {
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer trans = transFactory.newTransformer();

            StringWriter sw = new StringWriter();
            trans.transform(new DOMSource(doc), new StreamResult(sw));
            sw.flush();
            xml = sw.toString();
            sw.close();
        } catch (Exception e) {
            log.log(Level.ERROR, "Failed to transform document to string.", e);
        }
        return xml;
    }

    /**
	 * Makes a hex string to have a requested len. For example if you need hex string with len 4 you can pass one with
	 * len 1 and this function will pad it on the left with the correct number of 0s so the len becomes 4.
	 *
	 * @param hex the current hex str
	 * @param len the needed len
	 * @return New string that contains the padded hex string.
	 */
	public static String toFixedLenHexString(String hex, int len) {
		String pad = "0000000000";
		int ln = hex.length();
		if (ln < len) {
			return pad.substring(0, len - ln) + hex;
		}

		return hex;
	}
/*
	public static void savePages(HttpServletRequest request, com.smartclearing.backoffice.ListFilterForm lff, int entriesCount, MessageResources mr) {
		int pageCount = entriesCount / WEB_LIST_ROWS_ON_PAGE
				+ (entriesCount % WEB_LIST_ROWS_ON_PAGE == 0 ? 0 : 1);
		int crrPage = lff.getCrrPage();
		if (crrPage < 1) {
			crrPage = 1;
			lff.setCrrPage(crrPage);
		} else if (crrPage > pageCount) {
			crrPage = pageCount;
			lff.setCrrPage(crrPage);
		}
		String nextPage = lff.getNextPage();
		if (null != nextPage) {
			Locale locale = request.getLocale();
			String first = mr.getMessage(locale, "pagenavigation.first");
			String last = mr.getMessage(locale, "pagenavigation.last");
			String prev = mr.getMessage(locale, "pagenavigation.prev");
			String next = mr.getMessage(locale, "pagenavigation.next");

			if (nextPage.equals(first)) {
				crrPage = 1;
			} else if (nextPage.equals(last)) {
				crrPage = pageCount;
			} else if (nextPage.equals(prev) && crrPage > 1) {
				crrPage--;
			} else if (nextPage.equals(next) && crrPage < pageCount) {
				crrPage++;
			} else {
				try {
					crrPage = Integer.parseInt(nextPage);
				} catch (Exception e) {
					crrPage = 1;
				}
			}
			lff.setCrrPage(crrPage);
		}

		Vector pages = new Vector();
		if (pageCount > 0) {
			if (crrPage > 1) {
				pages.add(new LabelValueBean("", "first"));
				pages.add(new LabelValueBean("", "prev"));
			}
			int startPage = (crrPage > 5 ? crrPage - 5 : 1);
			int endPage = (crrPage < pageCount - 5 ? crrPage + 5 : pageCount);
			for (int i = startPage; i <= endPage; i++) {
				pages.add(new LabelValueBean(String.valueOf(i), (i == crrPage ? "flat" : "button")));
			}
			if (crrPage < pageCount) {
				pages.add(new LabelValueBean("", "next"));
				pages.add(new LabelValueBean("", "last"));
			}
		}
		request.setAttribute(LIST_PAGES, pages);
	}
*/
    /**
     * Format amount for display.
     *
     * @param amount the amount in cents to format.
     * @return Formatted amount as string for display.
     */
    public static String formatAmount(long amount) {
        double a = ((double) amount) / 100;
        DecimalFormat sumFormat = new DecimalFormat("#,##0.00");
        return sumFormat.format(a);
    }

    /**
     * Format datetime for display.
     *
     * @param date the datetime to format
     * @return Formatted datetime string for display.
     */
    public static String formatDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
        return dateFormat.format(date);
    }

    /**
     * Get the sender id for the user who did specified request. Look in the session if not
     * present lookup from the db and put in the session.
     *
     * @param request the request to get the session (user username) from
     * @return The id of the sender of the user who did that request or -1 if could not find it.
     */
    /*
    public static int getSenderId(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (null != session.getAttribute(SENDER_ID)) {
            Integer i = (Integer) session.getAttribute(SENDER_ID);
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, "Get sender id: " + i.intValue());
            }
            return i.intValue();
        } else {
            int senderId = -1;
            Connection conn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                conn = dataSource.getConnection();
                pstmt = conn.prepareStatement(SQL_GET_SENDER_ID);
                pstmt.setString(1, request.getRemoteUser());
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    senderId = rs.getInt("sender_id");
                } else {
                    log.log(Level.ERROR, "BACKOFFICE: Not admin user with no sender: " + request.getRemoteUser());
                }
            } catch (SQLException sqle) {
                log.log(Level.ERROR, "Failed to lookup sender id.", sqle);
            } finally {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close prepared statement.", e);
                }
                try {
                    rs.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close resultset.", e);
                }
                try {
                    conn.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close connections.", e);
                }
            }
            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.DEBUG, "Got sender id: " + senderId);
            }
            Integer i = new Integer(senderId);
            session.setAttribute(SENDER_ID, i);
            return senderId;
        }
    }
     */

    /**
     * Check if string contains Hebrew characters.
     *
     * @param str the string to check
     * @return <code>true</code> if the string containts Hebrew characters else <code>false</code>.
     */
    public static boolean hasHebrew(String str) {
        char[] ca = str.toCharArray();
        for (int i = 0; i < ca.length; i++) {
            if ((ca[i] >= 0x05D0 && ca[i] <= 0x05EA) ||
                    ca[i] == 0x05BE ||
                    ca[i] == 0x05C0 ||
                    ca[i] == 0x05C3 ||
                    ca[i] == 0x05F3 ||
                    ca[i] == 0x05F4) {
                return true;
            }
        }
        return false;
    }

    /**
     * Take a property value from properties as boolean.
     *
     * @param props the properties to look into
     * @param key the key to look under
     * @param defVal default value to return if the key is missing
     * @return <code>true</code> if the key exists and equals ignore case to
     *      "true", <code>false</code> if the key exists and does not equals
     *      ignore case to "true" and the default value if the key does not
     *      exists in the properties.
     */
    public static boolean getPropertyBoolean(Properties props, String key, boolean defVal) {
        String val = props.getProperty(key);
        if (null != val) {
            if (val.equalsIgnoreCase("true")) {
                return true;
            } else {
                return false;
            }
        } else {
            return defVal;
        }
    }

    /**
     * @param code the code of the currency to take the id for
     * @return The currency id or -1 if not supported.
     */
    /*
    public static int getCurrencyId(String code) {
        int id = -1;
        String idStr = (String) Enumerators.getCurrenciesHash().get(code);
        if (null != idStr) {
            try {
                id = Integer.parseInt(idStr);
            } catch (NumberFormatException nfe) {
                log.log(Level.ERROR, "Currency id not a number for currency " + code, nfe);
            }
        }
        return id;
    }*/

    /**
     * Replace any character other than [a-zA-Z0-9] with space and correct the length (cut the end).
     *
     * @param str the string to clean
     * @param maxLength the max length of the result
     * @return The cleaned string.
     */
    public static String an(String str, int maxLength) {
        String tmp = str.replaceAll("[^a-zA-Z0-9]", " ");
        if (tmp.length() > maxLength) {
            return tmp.substring(0, maxLength);
        }
        return tmp;
    }

    /**
     * Replace any character other than [a-zA-Z] with space and correct the length (cut the end).
     *
     * @param str the string to clean
     * @param maxLength the max length of the result
     * @return The cleaned string.
     */
    public static String a(String str, int maxLength) {
        String tmp = str.replaceAll("[^a-zA-Z]", " ");
        if (tmp.length() > maxLength) {
            return tmp.substring(0, maxLength);
        }
        return tmp;
    }

    /**
     * Check if value card with specified details exist in the db.
     *
     * @param pin the value card PIN
     * @param amount the value card amount
     * @param currencyId the value card currency
     * @param senderId the value card sender
     * @return The id of the value card if exist else 0.
     * @throws SQLException
     */
    /*
    public static ValueCard lookupValueCard(String pin) {
        ValueCard valueCard = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = getDataSource().getConnection();
            pstmt = conn.prepareStatement(SQL_LOOKUP_VALUE_CARD);
            pstmt.setString(1, pin);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                valueCard = new ValueCard();
                valueCard.setId(rs.getInt("id"));
                valueCard.setSerialNumber(rs.getString("serial_number"));
                valueCard.setPin(rs.getString("pin"));
                valueCard.setValue(rs.getLong("value"));
                valueCard.setCurrencyId(rs.getInt("currency_id"));
                valueCard.setCurrency(rs.getString("currency"));
                valueCard.setStatusId(rs.getInt("status_id"));
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "Problem lookup value card", sqle);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close.", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close.", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        return valueCard;
    }*/

    /**
     * Change the status of value card to "activated".
     *
     * @param senderId the sender id
     * @param valueCardId the value card id
     * @param customerId the id of the customer that activated the value card
     * @throws SQLException
     */
    /*
    public static void activateValueCard(int senderId, int valueCardId, long customerId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getDataSource().getConnection();
            pstmt = conn.prepareStatement(SQL_ACTIVATE_VALUE_CARD);
            pstmt.setInt(1, senderId);
            pstmt.setLong(2, customerId);
            pstmt.setInt(3, valueCardId);
            pstmt.executeUpdate();
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "Problem activate value card " + valueCardId, sqle);
        } finally {
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close.", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
    }*/
}