package il.co.etrader.callMeNoCallJob;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import il.co.etrader.util.SendTemplateEmail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class CallMeNoCallJob extends JobUtil {
	private static Logger log = Logger.getLogger(CallMeNoCallJob.class);
	 private static StringBuffer report = new StringBuffer();

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		log.info("Starting the Call Me with no calls Job.");
		SendCallMeWithNoCalls();

		 // build the report
       String reportBody = "<html><body>" +
       						report.toString() +
       					"</body></html>";


	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report email: " + reportBody);
	    }

	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("email.url"));
	    server.put("auth", getPropertyByFile("email.auth"));
	    server.put("user", getPropertyByFile("email.user"));
	    server.put("pass", getPropertyByFile("email.pass"));
	    server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", getPropertyByFile("job.email.subject"));
	    email.put("to", getPropertyByFile("job.email.to"));
	    email.put("from", getPropertyByFile("job.email.from"));
	    email.put("body", reportBody);
	    sendEmail(server, email);

		log.info("Call Me with no calls Job completed.");
	}

	/**
	 * Send cancel withdrawal email notification
	 * @throws Exception
	 */
	private static void SendCallMeWithNoCalls() throws Exception {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

	    report.append("<br><b>CallMeNoCallJob Information:</b><table border=\"1\">");
	    report.append("<tr>" +
	    				  "<td><b>User / Contact Id</b></td>" +
	    				  "<td><b>Name</b></td><td><b>Email</b></td>" +
	    				  "<td><b>Time Qualified</b></td>" +
	    				  "<td><b>Skin</b></td>" +
	    				  "<td><b>Country</b></td>" +
	    			  "</tr>");

		try {
			conn = getConnection();
			String sql =
				" SELECT " +
					" pe.id population_entry_id, " +
					" u.id user_id, " +
					" c.id contact_id, " +
					" u.language_id, " +
					" c.name contact_name, " +
					" c.email contact_email, " +
					" c.skin_id, " +
					" s.name skin_name, " +
					" co.country_name, " +
					" c.utc_offset contact_utc_offset, " +
					" to_char(pe.qualification_time, 'DD.MM.YYYY HH24:MI:SS') contact_time " +
				" FROM " +
					" population_entries pe," +
					" population_users pu " +
						" left join users u on pu.user_id = u.id, " +
					" populations p, " +
					" population_types pt, " +
					" contacts c, " +
					" skins s, " +
					" countries co " +
				"WHERE " +
					" pu.curr_population_entry_id = pe.id " +
					" AND s.id = c.skin_id " +
					" AND co.id = c.country_id " +
					" AND pe.population_id = p.id " +
					" AND p.population_type_id = pt.id " +
					" AND pu.contact_id = c.id " +
					" AND (u.class_id is null or u.class_id<>0) " +
					" AND c.skin_id <> 1 AND (u.skin_id <> 1 OR u.skin_id IS NULL) " +
					" AND not exists (select * from issues i where i.population_entry_id = pe.id) " +
					" AND p.population_type_id = "+ PopulationsManagerBase.POP_TYPE_CALLME + " " +
					" AND pe.qualification_time + ? <= sysdate " +
					" AND c.email IS NOT NULL " +
					" AND (u.is_contact_by_email = 1 OR u.is_contact_by_email IS NULL) " +
					" AND pu.entry_type_id != " + ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES;

			// Add an option to run job with specific contact id
            String contact_id = getPropertyByFile("contact.id", ConstantsBase.EMPTY_STRING);
            if (!CommonUtil.isParameterEmptyOrNull(contact_id)) {
               sql+="AND c.id = " + contact_id ;
            }

			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, 4); // distance of 4 days
			rs = pstmt.executeQuery();

			while (rs.next()) {
				String name = rs.getString("contact_name");
				String email = rs.getString("contact_email");
				long userId = rs.getLong("user_id");
				long contactId = rs.getLong("contact_id");
				String skinName = rs.getString("skin_name");
				String countryName = rs.getString("country_name");
				String contactTime = rs.getString("contact_time");
				Long pEntryId = rs.getLong("population_entry_id");
				UserBase user = new UserBase();
				user.setId(userId);
				user.setEmail(email);
				user.setFirstName(name);
				user.setSkinId(rs.getLong("skin_id"));
				user.setUtcOffset(rs.getString("contact_utc_offset"));
				user.setLanguageId(rs.getLong("language_id"));

				HashMap<String,String> params = new HashMap<String, String>();
				params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, name);
				params.put(SendTemplateEmail.PARAM_EMAIL, email);
				Template t = TemplatesDAO.get(conn, Template.CALLME_NO_CALL_MAIL_ID);
				sendSingleEmail(conn, properties, t.getFileName(), params, t.getSubject(),
						"call.me.no.call.comment", true, true,
						IssuesManagerBase.ISSUE_SUBJ_POPULATION, user, pEntryId, t.getId());

//				if (user.getId() != 0 && user.getSkinId() != ConstantsBase.SKIN_ETRADER) {  // user contact
//				   try {
//					   UsersManagerBase.SendToMailBox(user, t, ConstantsBase.WRITER_ID_AUTO, user.getLanguageId(), null, conn, 0);
//				   } catch (Exception e) {
//					   log.warn("Problem adding email to user inbox! userId: " + user.getId());
//				   }
//				}

				String idToReport = (userId == 0 ? "C_" + contactId : "U_" + userId);
				report.append("<tr>");
				report.append("<td>").append(idToReport).append("</td>");
                report.append("<td>").append(name).append("</td>");
                report.append("<td>").append(email).append("</td>");
                report.append("<td>").append(contactTime).append("</td>");
                report.append("<td>").append(skinName).append("</td>");
                report.append("<td>").append(countryName).append("</td>");
                report.append("</tr>");
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get users ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
	}
}
