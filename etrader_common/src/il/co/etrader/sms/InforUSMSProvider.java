package il.co.etrader.sms;

import java.net.URLEncoder;
import java.util.Date;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.ClearingUtil;

public class InforUSMSProvider extends SMSProvider {
	private static final Logger log = Logger.getLogger(InforUSMSProvider.class);
    
    public void send(TextSMS msg) throws SMSException {
        try {
            msg.setTimeSent(new Date());
            String request = "InforuXML=" + URLEncoder.encode(composeTextRequest(msg), "UTF-8");
            String response = ClearingUtil.executePOSTRequest(url, request, false, true, null, "UTF-8");
            parseResponse(msg, response);
        } catch (Exception e) {
            throw new SMSException("Failed to submit message.", e);
        }
    }

    public void send(WapPushSMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }

    public void send(PushRegistrySMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }
    
    private String composeTextRequest(TextSMS msg) {
        return
        "<Inforu>" +
            "<User>" +
                "<Username>" + username + "</Username>" +
                "<Password>" + password + "</Password>" +
            "</User>" +
            "<Content Type=\"sms\">" +
                "<Message>" + msg.getMessageBody() + "</Message>" +
//                "<MessagePelephone> This is a test SMS Message for Pelephone</MessagePelephone>" +
//                "<MessageCellcom> This is a test SMS Message for Cellcom </MessageCellcom>" +
//                "<MessageOrange> This is a test SMS Message for Orange </MessageOrange>" +
//                "<MessageMirs> This is a test SMS Message for Mirs </MessageMirs>" +
            "</Content>" +
            "<Recipients>" +
                "<PhoneNumber>+" + msg.getPhoneNumber() + "</PhoneNumber>" + // Can be multiple recipients devided by ";"
//                "<GroupNumber>5</GroupNumber>" +
            "</Recipients>" +
            "<Settings>" +
                "<SenderName>" + msg.getSenderName() + "</SenderName>" +
                "<SenderNumber>" + msg.getSenderPhoneNumber().replaceAll("\\D", "") + "</SenderNumber>" +
                "<CustomerMessageID>" + msg.getId() + "</CustomerMessageID>" +
//                "<CustomerParameter>AffId4</CustomerParameter>" +
//                "<MessageInterval>0</MessageInterval>" +
//                "<TimeToSend>12/3/2009 12:23</TimeToSend>" +
                "<DeliveryNotificationUrl>" + dlrUrl + "</DeliveryNotificationUrl>" +
//                "<MaxSegments>0</MaxSegments>" +
            "</Settings>" +
        "</Inforu>";
    }
    
    private void parseResponse(TextSMS msg, String response) {
        try {
            Document doc = ClearingUtil.parseXMLToDocument(response);
            Element root = doc.getDocumentElement();
            String status = ClearingUtil.getElementValue(root, "Status/*");
            String description = ClearingUtil.getElementValue(root, "Description/*");
            if (null != status && status.equals("1")) {
                msg.setStatus(SMS.STATUS_SUBMITTED);
            } else {
                msg.setStatus(SMS.STATUS_FAILED_TO_SUBMIT);
            }
            msg.setError(status + " " + description);
        } catch (Exception e) {
            log.error("Can't process InforU response.", e);
        }
    }
}