//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//import java.util.Hashtable;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//import org.w3c.dom.DOMException;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.clearing.ClearingProviderConfig;
//import com.anyoption.common.util.ClearingUtil;
//
//
///**
// * Implement the communication with Inatec.
// * Debit Processing Gateway
// *
// * @author Kobi
// */
//public class InatecClearingProvider extends ClearingProvider {
//	private static final Logger log = Logger.getLogger(InatecClearingProvider.class);
//
//	//functions type
//    protected static final int FNC_ONLINE_INITIALIZE_REQUEST = 1;
//    protected static final int FNC_ONLINE_STATUS_RESULT = 2;
//    protected static final int FNC_AUTHORIZE = 3;
//    protected static final int FNC_CAPTURE_AUTHORIZE = 4;
//    protected static final int FNC_REFUND = 5;   // Like WC bookback
//    protected static final int FNC_CREDIT = 6;   // Like WC CFT
//
//    protected Hashtable<String, String> headers;
//    protected boolean demo;
//
//	public void authorize(ClearingInfo info) throws ClearingException {
//		request(info, FNC_AUTHORIZE);
//	}
//
//	public void capture(ClearingInfo info) throws ClearingException {
//		request(info, FNC_CAPTURE_AUTHORIZE);
//	}
//
//	public void bookback(ClearingInfo info) throws ClearingException {
//		request(info, FNC_REFUND);
//	}
//
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		request(info, FNC_CREDIT);
//	}
//
//	public void onlineInitializeRequest(ClearingInfo info) throws ClearingException {
//		request((InatecInfo)info, FNC_ONLINE_INITIALIZE_REQUEST);
//	}
//
//	public void onlineStatusResult(ClearingInfo info) throws ClearingException {
//		request((InatecInfo)info, FNC_ONLINE_STATUS_RESULT);
//	}
//
//	public void enroll(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//	}
//
//	public void purchase(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//	}
//
//	public boolean isSupport3DEnrollement() {
//		return false;
//	}
//
//	   /**
//     * Process a request through this provider.
//     *
//     * @param info transaction info
//     * @param authorize authorize or capture request to make
//     * @throws ClearingException
//     */
//    protected void request(ClearingInfo info, int type) throws ClearingException {
//        if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//
//        String messageClass = null;
//
//        try {
//        	Document request = createRequest();
//        	if (type == FNC_ONLINE_INITIALIZE_REQUEST || type == FNC_ONLINE_STATUS_RESULT){
//        		 messageClass = getMessageClass(((InatecInfo)info).getInatecPaymentType()); // add to clearingInfo
//        	}
//	        switch (type) {
//		            case FNC_ONLINE_INITIALIZE_REQUEST:
//		            	addFunction(request, (InatecInfo)info, messageClass, info.getHomePageUrl());
//		            	break;
//		            case FNC_ONLINE_STATUS_RESULT:
//		            	addOnlineStatusResultFunction(request,(InatecInfo)info);
//		            	messageClass = "Status";
//		            	break;
//		            case FNC_AUTHORIZE:
//		            	messageClass = "Authorisation";
//		            	addAutorizeCreditFunction(request, info, messageClass);
//		            	break;
//		            case FNC_CAPTURE_AUTHORIZE:
//		            	messageClass = "Capture";
//		            	addRefundCaptureFunction(request, info, messageClass);
//		            	break;
//		            case FNC_REFUND:
//		            	messageClass = "Refund";
//		            	addRefundCaptureFunction(request, info, messageClass);
//		            	break;
//		            case FNC_CREDIT:
//		            	messageClass = "Credit";
//		            	addAutorizeCreditFunction(request, info, messageClass);
//		            	break;
//	            default:
//	                throw new ClearingException("Unkown request type");
//	        }
//	            String requestXML = ClearingUtil.transformDocumentToString(request);
//	            if (log.isTraceEnabled()) {
//	                log.trace(requestXML);
//	            }
//
//	            String responseXML = ClearingUtil.executePOSTRequest(url, requestXML, false, true, headers);
//
//	            if (log.isTraceEnabled()) {
//	                log.trace(responseXML);
//	            }
//	            if (type == FNC_ONLINE_INITIALIZE_REQUEST || type == FNC_ONLINE_STATUS_RESULT){
//	            	parseOnlineResponse(responseXML, (InatecInfo)info, messageClass);
//	            }
//	            else {
//	            	parseResponse(responseXML, info, messageClass);
//	            }
//	    } catch (Throwable t) {
//	        throw new ClearingException("Transaction failed.", t);
//	    }
//    }
//
//    /**
//     * Create Inatec request XML document.
//     *
//     * @return New Document with the Inatec XML structure.
//     */
//    protected Document createRequest() {
//        Document request = null;
//        try {
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//            request = docBuilder.newDocument();
//
//            Element root = request.createElement("InatecGW");
//            request.appendChild(root);
//            ClearingUtil.setAttribute(request, "", "xmlns", "https://www.taurus21.com/schema/debit");
//            ClearingUtil.setAttribute(request, "", "xmlns:xsi", "https://www.w3.org/2001/XMLSchema-instance");
//            ClearingUtil.setAttribute(request, "", "xsi:schemaLocation", "https://www.taurus21.com/schema/debit https://www.taurus21.com/schema/debit/mina-tenvelope.xsd");
//            ClearingUtil.setAttribute(request, "", "version", "1.00.000");
//
//        } catch (Exception e) {
//            log.log(Level.ERROR, "Failed to create request document.", e);
//        }
//        return request;
//    }
//
//    /**
//     * Add "online initialize request" function details.
//     *
//     * @param request
//     * @param info
//     * @param fnc
//     * @throws UnsupportedEncodingException
//     * @throws DOMException
//     */
//    protected void addFunction(Document request, InatecInfo info, String messageClass, String urlPath) throws DOMException, UnsupportedEncodingException {
//        Element root = request.getDocumentElement();
//        String bp = messageClass + "Request/";
//        
//        if (demo && messageClass.equals("EPS")) {
//            ClearingUtil.setNodeValue(root, bp + "MessageClass/*", "dumbdummy");
//        } else {
//            ClearingUtil.setNodeValue(root, bp + "MessageClass/*", messageClass);
//        }
//
//        ClearingUtil.setNodeValue(root, bp + "Header/UserID/*", username);
//        ClearingUtil.setNodeValue(root, bp + "Header/Password/*", password);
//
//        // transaction data
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Transaction_ID/*", String.valueOf(info.getTransactionId()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Unique_ID/*", String.valueOf(info.getTransactionId()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Short_ID/*", String.valueOf(info.getTransactionId()));
//
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Amount/*", String.valueOf(info.getAmount()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Currency/*", info.getCurrencySymbol());
//
//        if (demo) {
//        	ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/ProgramDesc/*", "Anyoption Test");
//        } else {
//        	ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/ProgramDesc/*", "Anyoption Live");
//        }
//
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/AccountHolder/*", ClearingUtil.an(info.getAccountHolder(), 40));
//        if ( !messageClass.equals("EPS") ) {
//	        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/BankCode/*", info.getBankCode());
//	        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/AccountNumber/*", info.getAccountNum());
//        }
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/CountryCode/*", info.getCountryA2());
//        //ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/BankName/*", "bankTest");
//        //ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/BankCity/*", "cityTest");
//        //ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/BankState/*", "DE");
//
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/MerchantRedirectURL/*",urlPath + "/pages/inatecDepositResult.jsf");
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/MerchantErrorURL/*", urlPath + "/pages/inatecDepositResult.jsf");
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Debit/MerchantNotificationURL/*", urlPath + "/pages/inatecDepositNotify.jsf");
//
//        // set Transaction attributes
//        ClearingUtil.setAttribute(request, bp + "Transaction", "response", "sync");
//        if (demo) {   // set demo mode if needed
//            ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "connector_test");
//        } else {
//        	ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "live");
//        }
//
//        // RiskManagement
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/FirstName/*", ClearingUtil.an(info.getFirstName(), 25));
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/LastName/*", ClearingUtil.an(info.getLastName(), 25));
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Address1/*", ClearingUtil.an(info.getAddress(), 40));
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Address2/*", ClearingUtil.an(info.getAddress(), 40));
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Zip/*", info.getZipCode());
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/City/*", info.getCity());
//        //ClearingUtil.setNodeValue(root, bp + "RiskManagement/State/*", "DE");
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Country/*", info.getCountryA2());
//        //ClearingUtil.setNodeValue(root, bp + "RiskManagement/Phone/*", "+049-030-0000");
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/EMail/*", info.getEmail());
//        ClearingUtil.setNodeValue(root, bp + "RiskManagement/IP/*", info.getIp());
//    }
//
//    /**
//     * Add "Online status result" function to the request.
//     *
//     * @param request
//     * @param info
//     * @return Wire Card function name.
//     */
//    protected void addOnlineStatusResultFunction(Document request, InatecInfo info) {
//        Element root = request.getDocumentElement();
//        String bp = "StatusRequest/";
//
//        ClearingUtil.setNodeValue(root, bp + "MessageClass/*", "Status");
//
//        ClearingUtil.setNodeValue(root, bp + "Header/UserID/*", username);
//        ClearingUtil.setNodeValue(root, bp + "Header/Password/*", password);
//
//        // transaction data
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Transaction_ID/*", String.valueOf(info.getTransactionId()));
//
//        String guwid = "";
//        if(info.getAuthNumber().length() < 19) {
//        	int leadingZeroNum = 19 - info.getAuthNumber().length();
//        	while (leadingZeroNum > 0) {
//        		guwid += "0";
//        		leadingZeroNum--;
//        	}
//        	guwid += info.getAuthNumber();
//        } else {
//        	guwid = info.getAuthNumber();
//        }
//
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Guwid/*", guwid);
//
//        // set Transaction attributes
//        ClearingUtil.setAttribute(request, bp + "Transaction", "response", "sync");
//        if (demo) {   // set demo mode if needed
//            ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "connector_test");
//        } else {
//        	ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "live");
//        }
//
//    }
//
//    /**
//     * Parse the Online Response.
//     *
//     * @param xml the XML to parse
//     * @param info the transaction info to fill the result values
//     * @param messageClass the message class request that called
//     * @throws Exception
//     */
//    protected static void parseOnlineResponse(String xml, InatecInfo info, String messageClass) throws Exception {
//        Document resp = ClearingUtil.parseXMLToDocument(xml);
//        String messageRes = messageClass + "Response";
//
//        Element processing = (Element) ClearingUtil.getNode(resp.getDocumentElement(), messageRes + "/Transaction/Processing");
//
//        String guWID    = ClearingUtil.getElementValue(processing, "Guwid/*");
//        String status   = ClearingUtil.getElementValue(processing, "Status/*");
//        String redirectUrl = ClearingUtil.getElementValue(processing, "RedirectURL/*");
//        String redirectSecret = ClearingUtil.getElementValue(processing, "RedirectSecret/*");
//        String timeStamp = ClearingUtil.getElementValue(processing, "Timestamp/*");
//        String paymentGuarantee = ClearingUtil.getElementValue(processing, "PaymentGuarantee/*");
//
//        if (null != redirectUrl) {  // passed in the initial response
//        	redirectUrl = URLDecoder.decode(redirectUrl,"UTF-8");
//        }
//
//        info.setAuthNumber(guWID);
//        info.setProviderTransactionId(guWID);
//
//        if (null != status) {
//        	if (status.equals("PENDING")) {
//        		if (messageClass.equals("Status")) {  // statusRequest
//        			info.setSuccessful(false);
//        			info.setUserMessage("statusRequest: pending status");
//        			info.setRedirectSecret(redirectSecret);
//        			info.setNeedNotification(true);  // for final result
//        		} else { // initial response
//	        		 info.setSuccessful(true);
//	        		 info.setResult("1000");
//	        		 info.setMessage("Permitted transaction.");
//	        		 info.setRedirectUrl(redirectUrl);
//	        		 info.setRedirectSecret(redirectSecret);
//        		}
//        	} else if (status.equals("SUCCEEDED")) {
//        			boolean succeed = false;
//        			if (messageClass.equals("Status")) {  // statusRequest
//        				if (null == paymentGuarantee) {
//        					succeed = false;
//        					info.setUserMessage("Transaction failed. pGuarantee: null");
//        				}else if (paymentGuarantee.equals("FULL")) { // payment guarantee
//        					succeed = true;
//        					info.setUserMessage("pGuarantee: FULL");
//        				} else if (paymentGuarantee.equals("NONE")) {  // no payment guarantee
//        					//succeed = false;
//        					//info.setUserMessage("Transaction failed. pGuarantee: NONE");
//        					succeed = true;
//        					info.setUserMessage("pGuarantee: NONE");
//        				} else if (paymentGuarantee.equals("VALIDATED")) { // no payment guarantee but customer account validated
//        					//succeed = false;
//        					//info.setUserMessage("Transaction failed. pGuarantee: VALIDATED");
//        					succeed = true;
//        					info.setUserMessage("pGuarantee: VALIDATED");
//        				}
//        			} else {  // initial response
//        				succeed = true;
//        			}
//        			if (succeed) {
//		        		info.setSuccessful(true);
//		        		info.setResult("1000");
//		        		info.setMessage("Permitted transaction.");
//        			} else {
//        				info.setSuccessful(false);
//        			}
//        	} else if (status.equals("FAILED")) {
//        		info.setSuccessful(false);
//        		handleError(info, processing);
//        	}
//        } else {
//          // try if the resonse is not with the strange error structure that we've seen
//        	info.setSuccessful(false);
//        	handleError(info, resp.getDocumentElement());
//        }
//    }
//
//    /**
//     * Handle the "ERROR" tag and its "Type", "Number", "ResponseMessage" and "Advice"
//     * sub tags.
//     *
//     * @param info the transaction info (to set error code and description)
//     * @param e the element under which to look for "ERROR" tag
//     */
//    protected static void handleError(ClearingInfo info, Element e) {
//        // fill the error part
//        String type     = ClearingUtil.getElementValue(e, "Type/*");
//        String ResponseMessage  = ClearingUtil.getElementValue(e, "ResponseMessage/*");
//        String advice   = ClearingUtil.getElementValue(e, "Advise/*");
//        String ResponseCode = ClearingUtil.getElementValue(e, "ResponseCode/*");
//
//        if (null != ResponseCode) {
//        	info.setResult(ResponseCode);
//        } else {
//        	info.setResult("-1");
//        }
//
//        if (null != ResponseMessage) {
//            info.setUserMessage(ResponseMessage);
//        } else {
//        	info.setUserMessage("-1 | Unknown Response");
//        }
//
//        info.setMessage(null != type ? type : "") ;
//
//        if (null != advice) {
//            info.setMessage((null != type ? type : "") + advice);
//        }
//    }
//
//    /**
//     * Set clearing provider configuration.
//     *
//     * @param key
//     * @param props
//     * @throws ClearingException
//     */
//    public void setConfig(ClearingProviderConfig config) throws ClearingException {
//        super.setConfig(config);
//
//        headers = new Hashtable<String, String>();
//        headers.put("Content-Type", "text/xml");
//
//        demo = false;
//        if (null != config.getProps() && !config.getProps().trim().equals("")) {
//            String[] ps = config.getProps().split(",");
//            for (int i = 0; i < ps.length; i++) {
//                String[] p = ps[i].split("=");
//                if (p[0].equals("demo")) {
//                    demo = p[1].equalsIgnoreCase("true");
//                } else {
//                    log.warn("Unknown property " + ps[i]);
//                }
//            }
//        }
//        if (log.isInfoEnabled()) {
//            log.info("demo: " + demo);
//        }
//    }
//
//
//	/**
//	 * Message class Factory.
//	 * @param paymentTypeId selected payment method
//	 * @return
//	 */
//	public static String getMessageClass(long paymentTypeId) {
//		String message = null;
//
//		if (TransactionsManagerBase.PAYMENT_TYPE_GIROPAY == paymentTypeId){
//			message = "Giropay";
//		}else if (TransactionsManagerBase.PAYMENT_TYPE_DIRECT24 == paymentTypeId){
//			message = "Direct24";
//		}else if (TransactionsManagerBase.PAYMENT_TYPE_EPS == paymentTypeId){
//			message = "EPS";
//		}
//
//		return message;
//	}
//
//
//    /**
//     * Add "authorize" functions details.
//     *
//     * @param request
//     * @param info
//     * @param fnc
//     */
//    protected void addAutorizeCreditFunction(Document request, ClearingInfo info, String fnc) {
//        Element root = request.getDocumentElement();
//        String bp =  fnc + "Request/";
//
//        ClearingUtil.setNodeValue(root, bp + "MessageClass/*", fnc);
//
//        ClearingUtil.setNodeValue(root, bp + "Header/UserID/*", username);
//        ClearingUtil.setNodeValue(root, bp + "Header/Password/*", password);
//
//        // transaction data
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Transaction_ID/*", String.valueOf(info.getTransactionId()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Unique_ID/*", String.valueOf(info.getTransactionId()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Short_ID/*", String.valueOf(info.getTransactionId()));
//
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Amount/*", String.valueOf(info.getAmount()));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Currency/*", info.getCurrencySymbol());
//        if (fnc.equals("Credit")) {  //TODO:  check if we need to add Descriptor node for other methods
//        	ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Descriptor/*", "AnyOption Refund");
//        }
//
//        // CC data
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Card/Cardholder/*", ClearingUtil.a(info.getOwner(), 256));
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Card/PAN/*", info.getCcn());
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Card/Exp_Month/*", info.getExpireMonth());
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Card/Exp_Year/*", "20" + info.getExpireYear());
//        if (fnc.equals("Authorisation")) {
//        	ClearingUtil.setNodeValue(root, bp + "Transaction/Card/CVV/*", info.getCvv());
//        }
//        ClearingUtil.setNodeValue(root, bp + "Transaction/Card/Country/*", info.getCountryA2());
//
//        // set Transaction attributes
//        ClearingUtil.setAttribute(request, bp + "Transaction", "response", "sync");
//        if (demo) {   // set demo mode if needed
//            ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "connector_test");
//        } else {
//        	ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "live");
//        }
//
//        if (fnc.equals("Authorisation")) {
//	        // RiskManagement
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/FirstName/*", ClearingUtil.an(info.getFirstName(), 25));
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/LastName/*", ClearingUtil.an(info.getLastName(), 25));
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Address1/*", ClearingUtil.an(info.getAddress(), 40));
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Address2/*", ClearingUtil.an(info.getAddress(), 40));
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/Country/*", info.getCountryA2());
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/EMail/*", info.getEmail());
//	        ClearingUtil.setNodeValue(root, bp + "RiskManagement/IP/*", info.getIp());
//        }
//    }
//
//   /* Add "capture" or "Refund" functions details.
//    *
//    * @param request
//    * @param info
//    * @param fnc
//    */
//   protected void addRefundCaptureFunction(Document request, ClearingInfo info, String fnc) {
//       Element root = request.getDocumentElement();
//       String bp =  fnc + "Request/";
//
//       ClearingUtil.setNodeValue(root, bp + "MessageClass/*", fnc);
//
//       ClearingUtil.setNodeValue(root, bp + "Header/UserID/*", username);
//       ClearingUtil.setNodeValue(root, bp + "Header/Password/*", password);
//
//       // transaction data
//       ClearingUtil.setNodeValue(root, bp + "Transaction/Identification/Transaction_ID/*", String.valueOf(info.getTransactionId()));
//
//       ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Amount/*", String.valueOf(info.getAmount()));
//       ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Currency/*", info.getCurrencySymbol());
//
//       String guwid = "";
//       String ID = "";
//       int length;
//       if (fnc.equalsIgnoreCase("Capture")){
//    	   length = info.getAuthNumber().length();
//    	   ID = info.getAuthNumber();
//       } else {
//    	   length = info.getProviderDepositId().length();
//    	   ID = info.getProviderDepositId();
//       }
//       if(length < 19) {
//       	int leadingZeroNum = 19 - length;
//       	while (leadingZeroNum > 0) {
//       		guwid += "0";
//       		leadingZeroNum--;
//       	}
//       	guwid += ID;
//       } else {
//       	guwid = ID;
//       }
//
//
//       ClearingUtil.setNodeValue(root, bp + "Transaction/Presentation/Guwid/*", guwid);
//
//       // set Transaction attributes
//       ClearingUtil.setAttribute(request, bp + "Transaction", "response", "sync");
//       if (demo) {   // set demo mode if needed
//           ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "connector_test");
//       } else {
//       	ClearingUtil.setAttribute(request, bp + "Transaction", "mode", "live");
//       }
//   }
//
//
//    /**
//     * Parse the response.
//     *
//     * @param xml the XML to parse
//     * @param info the transaction info to fill the result values
//     * @param messageClass the message class request that called
//     * @throws Exception
//     */
//    protected static void parseResponse(String xml, ClearingInfo info, String messageClass) throws Exception {
//        Document resp = ClearingUtil.parseXMLToDocument(xml);
//        String messageRes = messageClass + "Response";
//
//        Element processing = (Element) ClearingUtil.getNode(resp.getDocumentElement(), messageRes + "/Transaction/Processing");
//
//        String guWID    = ClearingUtil.getElementValue(processing, "Guwid/*");
//        String FunctionResult   = ClearingUtil.getElementValue(processing, "FunctionResult/*");
//        String timeStamp = ClearingUtil.getElementValue(processing, "Timestamp/*");
//
//        info.setAuthNumber(guWID);
//        info.setProviderTransactionId(guWID);
//
//        if (null != FunctionResult) {
//            // the two valid values are ACK and NOK
//            if (FunctionResult.equals("ACK")) {
//                info.setSuccessful(true);
//                if (messageClass.equals("Authorisation")){
//                	info.setProviderTransactionId(ClearingUtil.getElementValue(processing, "AuthorisationCode/*"));
//                }
//                info.setResult("1000");
//                info.setMessage("Permitted transaction.");
//                info.setUserMessage("Permitted transaction.");
//            } else if (FunctionResult.equals("NOK")) {
//                info.setSuccessful(false);
//                handleError(info, processing);
//            }
//        } else {
//            // try if the resonse is not with the strange error structure that we've seen
//        	info.setSuccessful(false);
//        	handleError(info, resp.getDocumentElement());
//        }
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//		
//	}
//
//}
