//package il.co.etrader.clearing;
//
//public class ClearingException extends Exception {
//    private static final long serialVersionUID = 1L;
//
//    public ClearingException() {
//        super();
//    }
//
//    public ClearingException(String message) {
//        super(message);
//    }
//
//    public ClearingException(String message, Throwable t) {
//        super(message, t);
//    }
//}