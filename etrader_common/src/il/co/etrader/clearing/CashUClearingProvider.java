package il.co.etrader.clearing;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import com.anyoption.common.clearing.CashUInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;


public class CashUClearingProvider extends ClearingProvider {

	/**
	 * Implement the communication with cashU.
	 *
	 * @author Asher
	 */
	//private static final Logger log = Logger.getLogger(CashUClearingProvider.class);

    private static final String REQUEST_PARAM_SERVICE_NAME = "anyone can trade";

    public static final String RESPONSE_PARAM_ORDER_ID = "OrderID";
    public static final String RESPONSE_PARAM_STATUS = "Status";
    public static final String RESPONSE_PARAM_STATUS_CODE = "StatusCode";
    public static final String RESPONSE_PARAM_STATUS_MESSAGE = "StatusMessage";
    public static final String RESPONSE_PARAM_PASSKEY = "PassKey";
    public static final String RESPONSE_PARAM_ASTECH_ID = "astechID";

	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	@Override
	public boolean isSupport3DEnrollement() {
		return false;
	}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Unsupported operation.");
	}

	public String getQuery(CashUInfo cashUInfo) throws ClearingException {
		String amount = CommonUtil.displayAmountForInput(cashUInfo.getAmount(), false, 0);
		String currencyCode = cashUInfo.getCurrency();
		try {
			if (cashUInfo.getCurrencyId() != ConstantsBase.CURRENCY_USD_ID) {
				 long amountUSD = Math.round(cashUInfo.getAmount() * TransactionsManagerBase.getRateById(cashUInfo.getTransactionId()));
				 amount =  CommonUtil.displayAmountForInput(amountUSD, false, 0);
				 currencyCode = "USD";
			}
		} catch (Exception e) {
   			throw new ClearingException("failed processing getQuery, problem converting to USD.", e);
   		}
		String q = "merchantID=" + username + "&" +
					"serviceName=" + REQUEST_PARAM_SERVICE_NAME + "&" +
					"orderID=" + cashUInfo.getTransactionId() + "&" +
					"userName=" + cashUInfo.getUserName() + "&" +
					"amount=" + amount + "&" +
					"currency=" + currencyCode + "&" +
					"language=" + cashUInfo.getLanguage() + "&" +
					"store=anyoption";
		return q;
	}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
	}

}
