///**
// *
// */
//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.ClearingManager;
//import il.co.etrader.bl_vos.UserBase;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * cashU clearing info class
// * @author Asher
// *
// */
//public class CashUInfo extends ClearingInfo {
//
//    private String language;
//    private String currency;
//    private String status;
//    private String statusCode;
//    private String statusMessage;
//    private String passKey;
//    private String astechId;
//
//    private static final String LANGUAGE_AR = "ar";
//    private static final String LANGUAGE_EN = "en";
//
//    public CashUInfo() {
//    	providerId = ClearingManager.CASHU_PROVIDER_ID;
//    }
//
//    public void addTransactionData(Transaction transaction) {
//        amount = transaction.getAmount();
//        transactionType = transaction.getTypeId();
//        transactionId = transaction.getId();
//        currencyId = transaction.getCurrencyId();
//        ip = transaction.getIp();
//    }
//
//    public CashUInfo(UserBase user, Transaction t){
//
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//    	if (user.getSkinId() == 4) {
//    		language = LANGUAGE_AR;
//    	}
//    	else {
//			language = LANGUAGE_EN;
//		}
//    	currency = user.getCurrency().getCode();
//    }
//
//    public CashUInfo(UserBase user){
//
//    	userName = user.getUserName();
//    	if (user.getSkinId() == 4) {
//    		language = "ar";
//    	}
//    	else {
//			language = "en";
//		}
//    	currency = user.getCurrency().getCode();
//    }
//
//	/**
//	 * Implement toString function
//	 */
//    public String toString() {
//    	String ls = System.getProperty("line.separator");
//    	String superToStr = super.toString();  // base toString
//    	return ls + superToStr + ls + "CashUInfo:" + ls +
//    			"language:" + language + ls +
//    			"currency:" + currency + ls +
//    			"status:" + status + ls +
//    			"statusCode:" + statusCode + ls +
//    			"statusMessage:" + statusMessage + ls +
//    			"passKey:" + passKey + ls +
//    			"astechId:" + astechId + ls;
//    }
//
//	public String getLanguage() {
//		return language;
//	}
//
//	public void setLanguage(String language) {
//		this.language = language;
//	}
//
//	public String getAstechId() {
//		return astechId;
//	}
//
//	public void setAstechId(String astechId) {
//		this.astechId = astechId;
//	}
//
//	public String getPassKey() {
//		return passKey;
//	}
//
//	public void setPassKey(String passKey) {
//		this.passKey = passKey;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getStatusCode() {
//		return statusCode;
//	}
//
//	public void setStatusCode(String statusCode) {
//		this.statusCode = statusCode;
//	}
//
//	public String getStatusMessage() {
//		return statusMessage;
//	}
//
//	public void setStatusMessage(String statusMessage) {
//		this.statusMessage = statusMessage;
//	}
//
//	public String getCurrency() {
//		return currency;
//	}
//
//	public void setCurrency(String currency) {
//		this.currency = currency;
//	}
//}
