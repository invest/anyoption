//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.io.BufferedReader;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.UnsupportedEncodingException;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.net.URLDecoder;
//import java.net.URLEncoder;
//import java.util.HashMap;
//import java.util.StringTokenizer;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Skin;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.clearing.ClearingProviderConfig;
//
//
///**
// * Implement the communication with PayPal.
// *
// * @author Kobi
// */
//public class PayPalClearingProvider extends ClearingProvider {
//	private static final Logger log = Logger.getLogger(PayPalClearingProvider.class);
//
//    private static final int FNC_SET_EXPRESS_CHECKOUT = 1;
//    private static final int FNC_GET_EXPRESS_CHECKOUT_DETAILS = 2;
//    private static final int FNC_DO_EXPRESS_CHECKOUT_PAYMENT = 3;
//    private static final int FNC_MASS_PAY = 4;
//
//    public static final String PAYMENT_TYPE_DEPOSIT = "Sale";
//    public static final String lOGIN_URL_SANDBOX = "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=";
//    public static final String lOGIN_URL_LIVE = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";
//    public static final String lOGIN_URL_COMMIT = "&useraction=commit";
//
//    private boolean demo;
//
//	public void authorize(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//
//	}
//
//	public void bookback(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//	}
//
//	public void capture(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//
//	}
//
//	public void enroll(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//	}
//
//	public boolean isSupport3DEnrollement() {
//		return false;
//	}
//
//	public void purchase(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//	}
//
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		request((PayPalInfo)info, FNC_MASS_PAY);
//	}
//
//	public void setExpressCheckout(ClearingInfo info) throws ClearingException {
//		request((PayPalInfo)info, FNC_SET_EXPRESS_CHECKOUT);
//	}
//
//	public void getExpressCheckoutDetails(ClearingInfo info) throws ClearingException {
//		request((PayPalInfo)info, FNC_GET_EXPRESS_CHECKOUT_DETAILS);
//	}
//
//
//	public void doExpressCheckoutPayment(ClearingInfo info) throws ClearingException {
//		request((PayPalInfo)info, FNC_DO_EXPRESS_CHECKOUT_PAYMENT);
//	}
//
//
//	/**
//     * Process a request through this provider.
//     *
//     * @param info thransaction info
//     * @param type the call type
//     * @throws ClearingException
//     */
//    private void request(PayPalInfo info, int type) throws ClearingException {
//        if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//        HashMap<String, String> nvp = null;
//        String call = getCallRequest(type);
//        try {
//	        switch (type) {
//            case FNC_SET_EXPRESS_CHECKOUT:
//            	nvp = callShortcutExpressCheckout(info, call);
//            	break;
//            case FNC_GET_EXPRESS_CHECKOUT_DETAILS:     // Note: unused methods(skip this method and going directly to the payment method)
//            	nvp = getShippingDetails(info, call);
//            	break;
//            case FNC_DO_EXPRESS_CHECKOUT_PAYMENT:
//            	nvp = confirmPayment(info, call);
//            	break;
//            case FNC_MASS_PAY:
//            	nvp = massPay(info, call);
//            	break;
//	        default:
//	            throw new ClearingException("Unkown request type");
//	        }
//
//            if (log.isDebugEnabled()) {
//                log.debug(nvp.toString());
//            }
//
//            parseResponse(nvp, info, type);
//
//        } catch (Throwable t) {
//            throw new ClearingException("Transaction failed.", t);
//        }
//    }
//
//   /**
//     * CallShortcutExpressCheckout: Function to perform the SetExpressCheckout API call
//     *
//     * Inputs:
//     *		paymentAmount:  	Total value of the shopping cart
//     *		currencyCodeType: 	Currency code value the PayPal API
//     *		paymentType: 		paymentType has to be one of the following values: Sale or Order or Authorization
//     *		returnURL:			the page where buyers return to after they are done with the payment review on PayPal
//     *		cancelURL:			the page where buyers return to when they cancel the payment review on PayPal
//     *
//     * Output: Returns a HashMap object containing the response from the server.
//   */
//   public HashMap<String, String> callShortcutExpressCheckout(PayPalInfo info, String call) throws ClearingException {
//	   try {
//		   String[] amountDetails = getAmountDetails(info);
//		   String nvpstr =
//	    	   "&Amt=" + amountDetails[0] +
//	       	   "&PAYMENTACTION=" + info.getPaymentType() +
//	       	   "&ReturnUrl=" + URLEncoder.encode(CommonUtil.getProperty("homepage.url") + "jsp/pages/payPalSetExpressCheckoutReturn.jsf", "UTF-8") +
//	       	   "&CANCELURL=" + URLEncoder.encode(CommonUtil.getProperty("homepage.url") + "jsp/pages/payPalSetExpressCheckoutCancel.jsf", "UTF-8") +
//	       	   "&CURRENCYCODE=" + amountDetails[1] +
//	       	   "&LOCALECODE=" + getSupportedLocale(info.getSkinId()) +
//	       	   "&HDRIMG=" + CommonUtil.getProperty("homepage.url.https") +  "images/paypal_header.gif";
//
//	       /*
//	       Make the call to PayPal to get the Express Checkout token
//	       If the API call succeded, then redirect the buyer to PayPal
//	       to begin to authorize payment.  If an error occured, show the
//	       resulting errors
//	       */
//	       return httpcall(call, nvpstr);
//       } catch (Throwable t) {
//           throw new ClearingException("failed processing setExpressCheckout!.", t);
//       }
//   }
//
//   /**
//    * GetShippingDetails: Function to perform the GetExpressCheckoutDetails API call
//    * Build a second API request to PayPal, using the token as the ID to get the details on the payment authorization
//    *
//    * Output: Returns a HashMap object containing the response from the server.
//    */
//   public HashMap<String, String> getShippingDetails(PayPalInfo info, String call) throws ClearingException {
//	  try {
//	      String nvpstr =
//	    	  "&TOKEN=" + info.getToken();
//
//	     /*
//	      Make the API call and store the results in an array.  If the
//	      call was a success, show the authorization details, and provide
//	      an action to complete the payment. If failed, show the error
//	      */
//	      return httpcall(call, nvpstr);
//      } catch (Throwable t) {
//          throw new ClearingException("failed processing GetExpressCheckoutDetails!.", t);
//      }
//   }
//
//   /**
//    * ConfirmPayment: Function to perform the DoExpressCheckoutPayment API call
//    *
//    * Output: Returns a HashMap object containing the response from the server.
//    */
//   public HashMap<String, String> confirmPayment(PayPalInfo info, String call) throws ClearingException {
//	   try {
//		 String[] amountDetails = getAmountDetails(info);
//	     String nvpstr =
//	    	 "&TOKEN=" + info.getToken() +
//	    	 "&PAYERID=" + info.getPayerId() +
//	    	 "&PAYMENTACTION=" + info.getPaymentType() +
//	    	 "&AMT=" + amountDetails[0] +
//	    	 "&CURRENCYCODE=" + amountDetails[1] +
//	    	 "&IPADDRESS=" + info.getIp();
//
//	    /*
//	     Make the call to PayPal to finalize payment
//	     If an error occured, show the resulting errors
//	    */
//	    return httpcall(call, nvpstr);
//	 } catch (Throwable t) {
//	     throw new ClearingException("failed processing DoExpressCheckoutPayment!.", t);
//	 }
//   }
//
//
//   /**
//     * massPay: Function to perform the MassPay API call
//     * Convert to USD when the currency is TRY
//     *
//     * Output: Returns a HashMap object containing the response from the server.
//     */
//	public HashMap<String, String> massPay(PayPalInfo info, String call) throws ClearingException {
//		 try {
//		     String nvpstr =
//		    	 "&CURRENCYCODE=" + info.getCurrencySymbol() +
//		    	 "&RECEIVERTYPE=EmailAddress";  // it can be EmailAddress / UserID for using PayerId
//
//		     for (int i = 0; i < info.getEmails().length; i++) {
//		    	 nvpstr +=
//		    		 "&L_EMAIL" + i + "=" + info.getEmails()[i].getEmail() +
//		    	 	 "&L_AMT" + i + "=" + info.getEmails()[i].getAmountText() +
//		    	 	 "&L_UNIQUEID" + i + "=" + info.getEmails()[i].getTransactionId();
//		     }
//
//		    return httpcall(call, nvpstr);
//	    } catch (Throwable t) {
//	        throw new ClearingException("failed processing DoExpressCheckoutPayment!.", t);
//	    }
//	}
//
//	/**
//	 * TRY currency is not available on PayPal, in such case we are converting to USD
//	 * @param info
//	 * @return String[2]. first cell with amount, secong cell with currencySymbol
//	 * @throws ClearingException
//	 */
//	public String[] getAmountDetails(PayPalInfo info) throws ClearingException {
//		String[] details = new String[2];
//		details[0] = info.getAmountText();
//		details[1] = info.getCurrencySymbol();
//	   try {
//		   if (info.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
//			   double amountUSD = Math.round(info.getAmount() * TransactionsManagerBase.getRateById(info.getTransactionId()));
//			   details[0] = CommonUtil.displayAmount(amountUSD, false, 0);
//			   details[1] = "USD";
//		   }
//   		}  catch (Exception e) {
//   			throw new ClearingException("failed processing setExpressCheckout. problem converting to USD.", e);
//   		}
//   		return details;
//	}
//
//  /**
//   * Parse PalPal response, read values from the NVP HashMap
//   * @param nvp the name value pairs HashMap
//   * @param info the payPal clearing provider instance
//   * @throws Exception
//   */
//   private void parseResponse(HashMap<String, String> nvp, PayPalInfo info, int call) throws Exception {
//	   String status = nvp.get("ACK").toString();
//	   //String correlationId = nvp.get("CORRELATIONID").toString();  // debuggingToken
//	   String token = null;
//
//       if (null != status) {
//
//    	   switch (call) {
//    	   case FNC_SET_EXPRESS_CHECKOUT :
//    		    if (status.equalsIgnoreCase("Success") || status.equalsIgnoreCase("SuccessWithWarning")) {
//    		    	token = nvp.get("TOKEN").toString();
//	       			if (null != token) {
//	       				info.setToken(token);
//	       				String loginUrl = demo ? lOGIN_URL_SANDBOX : lOGIN_URL_LIVE;
//	       				info.setLoginRedirectUrl(loginUrl + token + lOGIN_URL_COMMIT);  // useraction=commit for skip GetExpressCheckoutDetails step
//	   			   		info.setSuccessful(true);
//		        		info.setResult("1000");
//		        		info.setMessage("Permitted setEC");
//	       			} else {
//	       				log.warn("token not found!");
//	    	       		info.setSuccessful(false);
//	    	       		handleError(nvp, info, call);
//	       			}
//    		    } else {
//    		    	log.info("failed setExpressCheckOut response. ACK:" + status);
//    		    	info.setSuccessful(false);
//    	       		handleError(nvp, info, call);
//    		    }
//	   			break;
//    	   case FNC_GET_EXPRESS_CHECKOUT_DETAILS :   // Note: unused method(we are skip and going to the payment method)
//    		   if (status.equalsIgnoreCase("Success") || status.equalsIgnoreCase("SuccessWithWarning")) {
//    				info.setShipToName(nvp.get("SHIPTONAME").toString());
//    				info.setShipToCountryCode(nvp.get("SHIPTOCOUNTRYCODE").toString());
//    				info.setShipToState(nvp.get("SHIPTOSTATE").toString());
//    				info.setShipToCity(nvp.get("SHIPTOCITY").toString());
//    				info.setShipToCountryName(nvp.get("SHIPTOCOUNTRYNAME").toString());
//    				info.setShipToStreet(nvp.get("SHIPTOSTREET").toString());
//    				info.setShipToZip(nvp.get("SHIPTOZIP").toString());
//    				info.setPayerBusiness(nvp.get("BUSINESS").toString());
//    				info.setPayerStatus(nvp.get("PAYERSTATUS").toString());
//    				info.setPayerId(nvp.get("PAYERID").toString());
//    				info.setPayerFirstName(nvp.get("FIRSTNAME").toString());
//    				info.setPayerLastName(nvp.get("LASTNAME").toString());
//    				info.setPayerEmail(nvp.get("EMAIL").toString());
//    				info.setPayerCountryCode(nvp.get("COUNTRYCODE").toString());
//    				info.setPayerAdrrStatus(nvp.get("ADDRESSSTATUS").toString());
//   			   		info.setSuccessful(true);
//	        		info.setResult("1000");
//	        		info.setMessage("Permitted getECDetails");
//    		   } else {
//    			   log.info("failed getCustomerDetails response. ACK:" + status);
//    			   info.setSuccessful(false);
//   	       		   handleError(nvp, info, call);
//    		   }
//	   			break;
//    	   case FNC_DO_EXPRESS_CHECKOUT_PAYMENT :
//    		   if (status.equalsIgnoreCase("Success") || status.equalsIgnoreCase("SuccessWithWarning")) {
//    			   info.setProviderTransactionId(nvp.get("TRANSACTIONID").toString());
//    			   info.setAuthNumber(nvp.get("TRANSACTIONID").toString());
//    			   info.setSuccessful(true);
//	        	   info.setResult("1000");
//	        	   info.setMessage("Permitted DoECPayment");
//    		   } else {
//    			   log.info("failed PayPal finalPayment response. ACK:" + status);
//    			   info.setSuccessful(false);
//   	       		   handleError(nvp, info, call);
//    		   }
//    		   break;
//    	   case FNC_MASS_PAY :
//    		   if (status.equalsIgnoreCase("Success") || status.equalsIgnoreCase("SuccessWithWarning")) {
//    			   info.setSuccessful(true);
//	        	   info.setResult("1000");
//	        	   info.setMessage("Permitted transaction.");
//    		   } else {
//    			   log.info("failed PayPal MassPay response. ACK:" + status);
//    			   info.setSuccessful(false);
//   	       		   handleError(nvp, info, call);
//    		   }
//    		   break;
//    	   default:
//    		   break;
//	   		}
//       } else {
//         // try if the resonse is not with the strange error structure that we've seen
//       	info.setSuccessful(false);
//       	handleError(nvp, info, call);
//       }
//   }
//
//   /**
//    * Handle the Errors
//    *
//    * @param nvp the name value pairs response fields
//    * @param info the transaction info (to set error code and description)
//    * @param call the call request id
//    */
//   private static void handleError(HashMap<String, String> nvp, PayPalInfo info, int call) {
//       // fill the error part
//	   String ErrorCode = nvp.get("L_ERRORCODE0").toString();
//       String ErrorShortMsg = nvp.get("L_SHORTMESSAGE0").toString();
//       String callType = getCallRequest(call);
//       //String ErrorLongMsg = nvp.get("L_LONGMESSAGE0").toString();
//       //String ErrorSeverityCode = nvp.get("L_SEVERITYCODE0").toString();
//
//       if (null != ErrorCode) {
//           info.setResult(ErrorCode);
//       } else {
//           info.setResult("-1");
//           info.setMessage((null != callType ? callType : "") + "Unknown response.");
//           info.setUserMessage("Transaction failed.");
//       }
//       if (null != ErrorShortMsg) {
//           info.setUserMessage(ErrorShortMsg);
//       }
//   }
//
//
//    /**
//     * Set clearing provider configuration.
//     *
//     * @param key
//     * @param props
//     * @throws ClearingException
//     */
//    public void setConfig(ClearingProviderConfig config) throws ClearingException {
//        super.setConfig(config);
//        demo = false;
//        if (null != config.getProps() && !config.getProps().trim().equals("")) {
//            String[] ps = config.getProps().split(",");
//            for (int i = 0; i < ps.length; i++) {
//                String[] p = ps[i].split("=");
//                if (p[0].equals("demo")) {
//                    demo = p[1].equalsIgnoreCase("true");
//                } else {
//                    log.warn("Unknown property " + ps[i]);
//                }
//            }
//        }
//        if (log.isInfoEnabled()) {
//            log.info("demo: " + demo);
//        }
//    }
//
//   /**
//     * httpcall: Function to perform the API call to PayPal using API signature
//     * @throws IOException
//     * 	@ methodName is name of API method.
//     * 	@ nvpStr is nvp string.
//     * returns a NVP string containing the response from the server.
//   */
//   public HashMap<String, String> httpcall(String methodName, String nvpStr) throws IOException {
//	   String respText = "";
//       HashMap<String, String> nvp = null;
//       String apiSignature;
//
//       if (demo) {  // Sandbox env
//    	   apiSignature = "Aq.CejOjZMRAxMwY.clF7c-S5AcGAwdwIIqGF6MsdZKq9dXMORvwsbQe";
//       } else {  // Live
//    	   apiSignature = "A4g1YAi6A9EbVkOnOe5SMiUxduJIAdSR0.zSCC0NddeS3nzJUr6ZvARZ";
//       }
//
//       //deformatNVP( nvpStr );
//       String encodedData =
//    	   "METHOD=" + methodName +
//    	   "&VERSION=" + "2.3" +
//    	   "&PWD=" + password +
//    	   "&USER=" + username +
//    	   "&SIGNATURE=" + apiSignature +
//    	   nvpStr;
//
//       if (log.isDebugEnabled()) {
//           log.debug(encodedData);
//       }
//
//       try {
//           URL postURL = new URL(url);
//           HttpURLConnection conn = (HttpURLConnection)postURL.openConnection();
//
//           // Set connection parameters. We need to perform input and output, so set both as true.
//           conn.setDoInput (true);
//           conn.setDoOutput (true);
//
//           conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//           //conn.setRequestProperty( "User-Agent", agent );
//
//           conn.setRequestProperty( "Content-Length", String.valueOf( encodedData.length()) );
//           conn.setRequestMethod("POST");
//
//           // get the output stream to POST to.
//           DataOutputStream output = new DataOutputStream(conn.getOutputStream());
//           output.writeBytes(encodedData);
//           output.flush();
//           output.close();
//
//           // Read input from the input stream.
//           int rc = conn.getResponseCode();
//           if (rc != -1) {
//               BufferedReader is = new BufferedReader(new InputStreamReader( conn.getInputStream()));
//               String _line = null;
//               while(((_line = is.readLine()) != null)) {
//                   respText = respText + _line;
//               }
//               nvp = deformatNVP(respText);
//           }
//           return nvp;
//       }
//       catch(IOException e) {
//           log.error("Problem processing HTTP POST request!", e);
//          throw e;
//       }
//   }
//
//  /**
//    * deformatNVP: Function to break the NVP string into a HashMap
//    * 	pPayLoad is the NVP string.
//    * returns a HashMap object containing all the name value pairs of the string.
//    * @throws UnsupportedEncodingException
//  */
//  public HashMap<String, String> deformatNVP( String pPayload ) throws UnsupportedEncodingException {
//      HashMap<String, String> nvp = new HashMap<String, String>();
//      StringTokenizer stTok = new StringTokenizer( pPayload, "&");
//      while (stTok.hasMoreTokens())  {
//          StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
//          if (stInternalTokenizer.countTokens() == 2) {
//              String key = URLDecoder.decode(stInternalTokenizer.nextToken(), "UTF-8");
//              String value = URLDecoder.decode( stInternalTokenizer.nextToken(), "UTF-8");
//              nvp.put(key.toUpperCase(), value);
//          }
//      }
//      return nvp;
//  }
//
//	/**
//	 * call request Factory.
//	 * @param paymentTypeId selected payment method
//	 * @return
//	 */
//	private static String getCallRequest(int callReqId) {
//		String message = null;
//		switch (callReqId) {
//		case FNC_SET_EXPRESS_CHECKOUT :
//			message = "SetExpressCheckout";
//			break;
//		case FNC_GET_EXPRESS_CHECKOUT_DETAILS :
//			message = "GetExpressCheckoutDetails";
//			break;
//		case FNC_DO_EXPRESS_CHECKOUT_PAYMENT :
//			message = "DoExpressCheckoutPayment";
//			break;
//		case FNC_MASS_PAY :
//			message = "MassPay";
//			break;
//		default:
//			break;
//		}
//		return message;
//	}
//
//	/**
//	 * Get suppoerted locale code
//	 * @param skinId
//	 * @return
//	 */
//	private static String getSupportedLocale(long skinId) {
//		if (skinId == Skin.SKIN_SPAIN || skinId == Skin.SKIN_SPAINMI) {
//			return ConstantsBase.SPANISH_LOCALE.toUpperCase();
//		}
//		if (skinId == Skin.SKIN_GERMAN) {
//			return ConstantsBase.GERMAN_LOCALE.toUpperCase();
//		}
//		return ConstantsBase.LOCALE_DEFAULT.toUpperCase();
//	}
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//	}
//}
