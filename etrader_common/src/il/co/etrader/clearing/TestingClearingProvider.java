//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.UsersManagerBase;
//
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//
///**
// * Simple ClearingProvider for testing purposes. It will accept any action performed by test user (users.class_id = 0) with
// * credit card with number 4200000000000000, 4111111111111111 and 5555555555554444. All other actions will fail.
// * 
// * @author Tony
// */
//public class TestingClearingProvider extends ClearingProvider {
//	private static final Logger log = Logger.getLogger(TestingClearingProvider.class);
//
//    /**
//     * Process an "authorize" call through this provider.
//     *
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public void authorize(ClearingInfo info) throws ClearingException {
//    	performAction(info);
//    }
//
//    /**
//     * Process an "capture" call through this provider.
//     *
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public void capture(ClearingInfo info) throws ClearingException {
//    	performAction(info);
//    }
//
//    /**
//     * Process a "enroll" request through this provider. Enroll is the
//     * request that checks through hosted MPI if certain card has 3D
//     * security enrolled. A Merchant Plug In (MPI) is a software module
//     * which provides a communication interface between the merchant and
//     * the Visa or MasterCard directory servers. Hosted MPI is when the
//     * clearing provider also provide MPI to be used through the communication
//     * interface with it (and no need to be installed on merchant side).
//     *
//     * @param info transaction info
//     * @throws ClearingException
//     */
//    public void enroll(ClearingInfo info) throws ClearingException {
//    	performAction(info);
//    }
//
//    /**
//     * Process a "purchase" call through this provider. Purchase is a direct
//     * one step debit of the card (compared to authorize and capture 2 steps).
//     *
//     * @param info transaction info
//     * @throws ClearingException
//     */
//    public void purchase(ClearingInfo info) throws ClearingException {
//    	performAction(info);
//    }
//
//    /**
//     * Process a "withdraw" call through this provider.
//     *
//     * @param info
//     * @throws ClearingException
//     */
//    public void withdraw(ClearingInfo info) throws ClearingException {
//    	log.debug("Success bookback");
//		info.setSuccessful(true);
//		info.setResult("1000");
//		info.setMessage("Permitted transaction.");
//		info.setUserMessage("Permitted transaction.");
//    }
//
//    /**
//     * Process a "bookback" call through this provider. This is refund money
//     * for previous successful transaction over the same provider.
//     *
//     * @param info
//     * @throws ClearingException
//     */
//    public void bookback(ClearingInfo info) throws ClearingException {
//    	log.debug("Success bookback");
//		info.setSuccessful(true);
//		info.setResult("1000");
//		info.setMessage("Permitted transaction.");
//		info.setUserMessage("Permitted transaction.");
//    }
//
//    /**
//     * @return <code>true</code> if this provider support 3D enrollment else
//     *      <code>false</code>.
//     */
//    public boolean isSupport3DEnrollement() {
//    	return false;
//    }
//    
//    /**
//     * Check if the action should be accepted and set corresponding result and messages.
//     * 
//     * @param info
//     */
//    private void performAction(ClearingInfo info) {
//    	log.trace(info.toString());
//    	if (isActionOK(info)) {
//    		log.debug("Success");
//    		info.setSuccessful(true);
//    		info.setResult("1000");
//    		info.setMessage("Permitted transaction.");
//    		info.setUserMessage("Permitted transaction.");
//    	} else {
//    		log.debug("Failure");
//    		info.setSuccessful(false);
//    		info.setResult("0");
//    		info.setMessage("Test transaction rejected.");
//    		info.setUserMessage("Test transaction rejected.");
//    	}
//    }
//    
//    /**
//     * Only actions done by test users with card 4200000000000000, 4111111111111111, 5555555555554444 and 378282246310005(amex) are accepted.
//     * 
//     * @param info
//     * @return <code>true</code> if to accept the action else <code>false</code>.
//     */
//    private boolean isActionOK(ClearingInfo info) {
//    	boolean isTestUser = false;
//    	try {
//    		isTestUser = UsersManagerBase.isTestUser(info.getUserId());
//		} catch (SQLException e) {
//			log.error("Exception while trying to check isTestUser ", e);
//		}
//    	return isTestUser &&
//    	        (info.getCcn().equals("4200000000000000")
//    	                || info.getCcn().equals("4111111111111111")
//    	                || info.getCcn().equals("5555555555554444")
//    	                || info.getCcn().equals("378282246310005"));
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//	}
//}