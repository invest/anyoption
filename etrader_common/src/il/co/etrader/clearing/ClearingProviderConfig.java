//package il.co.etrader.clearing;
//
//public class ClearingProviderConfig {
//    protected long id;
//    protected String url;
//    protected String terminalId;
//    protected String userName;
//    protected String password;
//    protected String providerClass;
//    protected String props;
//    protected String name;
//    protected long nonCftAvailable;
//    protected boolean isActive;
//    protected String privateKey;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getTerminalId() {
//        return terminalId;
//    }
//
//    public void setTerminalId(String terminalId) {
//        this.terminalId = terminalId;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getProviderClass() {
//        return providerClass;
//    }
//
//    public void setProviderClass(String providerClass) {
//        this.providerClass = providerClass;
//    }
//
//    public String getProps() {
//        return props;
//    }
//
//    public void setProps(String props) {
//        this.props = props;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public long getNonCftAvailable() {
//		return nonCftAvailable;
//	}
//
//	public void setNonCftAvailable(long nonCftAvailable) {
//		this.nonCftAvailable = nonCftAvailable;
//	}
//	
//	public String getPrivateKey() {
//		return privateKey;
//	}
//	
//	public void setPrivateKey(String privateKey) {
//		this.privateKey = privateKey;
//	}
//
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "ClearingProviderConfig:" + ls +
//            "id: " + id + ls +
//            "url: " + url + ls +
//            "terminalId: " + terminalId + ls +
//            "userName: " + userName + ls +
//            "password: " + password + ls +
//            "providerClass: " + providerClass + ls +
//            "props: " + props + ls +
//            "name: " + name + ls +
//            "Is_Active: " + isActive + ls +
//            "PrivateKey: " + privateKey + ls;
//    }
//
//	public boolean isActive() {
//		return isActive;
//	}
//
//	public void setActive(boolean isActive) {
//		this.isActive = isActive;
//	}
//}