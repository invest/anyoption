//package il.co.etrader.clearing;
//
//
//import il.co.etrader.bl_vos.UserBase;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.util.Sha1;
//
///**
// * CDPayInfo clearing info class
// * @author LioR
// *
// */
//
//	public class CDPayInfo extends ClearingInfo implements Serializable {
//
//		private static final long serialVersionUID = 1L;
//		private static final Logger log = Logger.getLogger(CDPayInfo.class);
//		private final String RETURN_URL = "jsp/pages/cdpayReturnURL.jsf";
//		private String redirectURL;
//		private String merchantAccount;
//		private String language;
//		private String currency;
//		//private String cancelURL;
//		
//		private String returnURL;
//		private String clientAccount;
//		private String depositAmount;
//	
//		//status URL fields
//		private String merchant_id;
//		private String customer_id;
//		private String transactionAmount;
//		private String transactionCurrency;
//		//Parameters for CDPayInfo clearing provider.
//		private final String SERVER_RETURN_URL = "jsp/cdpayServerReturnURL.jsf";
//		//public static final String PARTNER_CONTROL = "4c8ecd6ce8b9aaa5f9bb94ee8bd75fa2"; //TODO: cdpay. Create new column in clearing_providers table.
//		private String private_key;
//		public static final String URL_POST_DEPOSIT = "ChinaDebitCard/";
//		public static final String URL_POST_REFUND = "Refund/";
//		
//		private int version; //The version number of API statusMessage.
//		private String merchantOrder; //user transactionId. Unique order identifier, defined by Merchant
//		private String merchantProductDesc; //Brief product description of the order
//		private String bankcode; //Optional. Bank code of designate bank to facilitate direct bank routing. If supplied bankcode is invalid or this data is not supplied, customer will redirect to bank selection screen.
//		private String serverReturnURL; //The URL to post the transaction result to after payment processing was completed. 40 bytes max, optional
//		private String control; //The checksum 
//		//Result, additional parameters.
//		private long cdpayTransactionId; //Unique transaction identifier, defined by CDPay.
//		private String bankTransactionId; //Unique identifier of the bank that handles the transaction.
//		private String status; //The status code of the order. Refer to Appendix D for the possible values.
//		private String statusMessage; //The status code of the order. Refer to Appendix D for the possible values.
//		private String billingdescriptor; //The billing descriptor for the transaction, as displayed to the end user.	
//		//Refund
//		private long orderId; //Unique transaction identifier of initial transaction assigned by CDPay
//		private long bankId;
//		private long refundId;
//		private double refundAmount;
//		private String refundReqDate;
//		private String refundCompleteDate;
//		private String refundStatus;
//		
//		public CDPayInfo() {
//	    }
//	
//		public CDPayInfo(UserBase user, Transaction t, String language, String homePageUrlHttps, String clientAccount, String amount, boolean isLive, String imagesDomain) throws SQLException{
//	    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//	    	this.version = 11;
//	    	this.returnURL = homePageUrlHttps + RETURN_URL;
//	    	this.serverReturnURL = homePageUrlHttps + SERVER_RETURN_URL;
//	    	this.depositAmount = amount;
//	    	this.language = language;
//	    	this.merchantOrder = "";
//	    	this.merchantProductDesc = "testCDPay";
//	    	this.bankcode = "";
//	    	//this.currencySymbol = "CNY";
//	    }
//	
//		/**
//		 * @return the merchantAccount
//		 */
//		public String getMerchantAccount() {
//			return merchantAccount;
//		}
//	
//		/**
//		 * @param merchantAccount the merchantAccount to set
//		 */
//		public void setMerchantAccount(String merchantAccount) {
//			this.merchantAccount = merchantAccount;
//		}
//	
//		/**
//		 * @return the redirectURL
//		 */
//		public String getRedirectURL() {
//			return redirectURL;
//		}
//	
//		/**
//		 * @param redirectURL the redirectURL to set
//		 */
//		public void setRedirectURL(String redirectURL) {
//			this.redirectURL = redirectURL;
//		}
//	
//		/**
//		 * @return the language
//		 */
//		public String getLanguage() {
//			return language;
//		}
//	
//		/**
//		 * @param language the language to set
//		 */
//		public void setLanguage(String language) {
//			this.language = language;
//		}
//	
//		/**
//		 * @return the returnURL
//		 */
//		public String getReturnURL() {
//			return returnURL;
//		}
//	
//		/**
//		 * @param returnURL the returnURL to set
//		 */
//		public void setReturnURL(String returnURL) {
//			this.returnURL = returnURL;
//		}
//	
//		/**
//		 * @return the currency
//		 */
//		public String getCurrency() {
//			return currency;
//		}
//	
//		/**
//		 * @param currency the currency to set
//		 */
//		public void setCurrency(String currency) {
//			this.currency = currency;
//		}
//	
//		/**
//		 * @return the clientAccount
//		 */
//		public String getClientAccount() {
//			return clientAccount;
//		}
//	
//		/**
//		 * @param clientAccount the clientAccount to set
//		 */
//		public void setClientAccount(String clientAccount) {
//			this.clientAccount = clientAccount;
//		}
//	
//		/**
//		 * @return the depositAmount
//		 */
//		public String getDepositAmount() {
//			return depositAmount;
//		}
//	
//		/**
//		 * @param depositAmount the depositAmount to set
//		 */
//		public void setDepositAmount(String depositAmount) {
//			this.depositAmount = depositAmount;
//		}
//	
//		/**
//		 * @return the customer_id
//		 */
//		public String getCustomer_id() {
//			return customer_id;
//		}
//	
//		/**
//		 * @param customer_id the customer_id to set
//		 */
//		public void setCustomer_id(String customer_id) {
//			this.customer_id = customer_id;
//		}
//	
//		/**
//		 * @return the merchant_id
//		 */
//		public String getMerchant_id() {
//			return merchant_id;
//		}
//	
//		/**
//		 * @param merchant_id the merchant_id to set
//		 */
//		public void setMerchant_id(String merchant_id) {
//			this.merchant_id = merchant_id;
//		}
//	
//		/**
//		 * @return the transactionAmount
//		 */
//		public String getTransactionAmount() {
//			return transactionAmount;
//		}
//	
//		/**
//		 * @param transactionAmount the transactionAmount to set
//		 */
//		public void setTransactionAmount(String transactionAmount) {
//			this.transactionAmount = transactionAmount;
//		}
//	
//		/**
//		 * @return the transactionCurrency
//		 */
//		public String getTransactionCurrency() {
//			return transactionCurrency;
//		}
//	
//		/**
//		 * @param transactionCurrency the transactionCurrency to set
//		 */
//		public void setTransactionCurrency(String transactionCurrency) {
//			this.transactionCurrency = transactionCurrency;
//		}
//	
//		/**
//		 * @return the version
//		 */
//		public int getVersion() {
//			return version;
//		}
//	
//		/**
//		 * @param version the version to set
//		 */
//		public void setVersion(int version) {
//			this.version = version;
//		}
//	
//		/**
//		 * @return the merchantOrder
//		 */
//		public String getMerchantOrder() {
//			return merchantOrder;
//		}
//	
//		/**
//		 * @param merchantOrder the merchantOrder to set
//		 */
//		public void setMerchantOrder(String merchantOrder) {
//			this.merchantOrder = merchantOrder;
//		}
//	
//		/**
//		 * @return the merchantProductDesc
//		 */
//		public String getMerchantProductDesc() {
//			return merchantProductDesc;
//		}
//	
//		/**
//		 * @param merchantProductDesc the merchantProductDesc to set
//		 */
//		public void setMerchantProductDesc(String merchantProductDesc) {
//			this.merchantProductDesc = merchantProductDesc;
//		}
//	
//		/**
//		 * @return the bankcode
//		 */
//		public String getBankcode() {
//			return bankcode;
//		}
//	
//		/**
//		 * @param bankcode the bankcode to set
//		 */
//		public void setBankcode(String bankcode) {
//			this.bankcode = bankcode;
//		}
//	
//		/**
//		 * @return the serverReturnURL
//		 */
//		public String getServerReturnURL() {
//			return serverReturnURL;
//		}
//	
//		/**
//		 * @param serverReturnURL the serverReturnURL to set
//		 */
//		public void setServerReturnURL(String serverReturnURL) {
//			this.serverReturnURL = serverReturnURL;
//		}
//	
//		/**
//		 * @return the control
//		 */
//		public String getControl() {
//			return control;
//		}
//	
//		/**
//		 * @param control the control to set
//		 */
//		public void setControl(String control) {
//			this.control = control;
//		}
//	
//		/**
//		 * @return the cdpayTransactionId
//		 */
//		public long getCdpayTransactionId() {
//			return cdpayTransactionId;
//		}
//	
//		/**
//		 * @param cdpayTransactionId the cdpayTransactionId to set
//		 */
//		public void setCdpayTransactionId(long cdpayTransactionId) {
//			this.cdpayTransactionId = cdpayTransactionId;
//		}
//	
//		/**
//		 * @return the bankTransactionId
//		 */
//		public String getBankTransactionId() {
//			return bankTransactionId;
//		}
//	
//		/**
//		 * @param bankTransactionId the bankTransactionId to set
//		 */
//		public void setBankTransactionId(String bankTransactionId) {
//			this.bankTransactionId = bankTransactionId;
//		}
//	
//		/**
//		 * @return the status
//		 */
//		public String getStatus() {
//			return status;
//		}
//	
//		/**
//		 * @param status the status to set
//		 */
//		public void setStatus(String status) {
//			this.status = status;
//		}
//	
//		/**
//		 * @return the statusMessage
//		 */
//		public String getStatusMessage() {
//			return statusMessage;
//		}
//	
//		/**
//		 * @param message the message to set
//		 */
//		public void setStatusMessage(String statusMessage) {
//			this.statusMessage = statusMessage;
//		}
//	
//		/**
//		 * @return the billingdescriptor
//		 */
//		public String getBillingdescriptor() {
//			return billingdescriptor;
//		}
//	
//		/**
//		 * @param billingdescriptor the billingdescriptor to set
//		 */
//		public void setBillingdescriptor(String billingdescriptor) {
//			this.billingdescriptor = billingdescriptor;
//		}
//	
//		/**
//		 * @return the orderId
//		 */
//		public long getOrderId() {
//			return orderId;
//		}
//	
//		/**
//		 * @param orderId the orderId to set
//		 */
//		public void setOrderId(long orderId) {
//			this.orderId = orderId;
//		}
//	
//		/**
//		 * @return the bankId
//		 */
//		public long getBankId() {
//			return bankId;
//		}
//
//		/**
//		 * @param bankId the bankId to set
//		 */
//		public void setBankId(long bankId) {
//			this.bankId = bankId;
//		}
//
//		/**
//		 * @return the refundId
//		 */
//		public long getRefundId() {
//			return refundId;
//		}
//
//		/**
//		 * @param refundId the refundId to set
//		 */
//		public void setRefundId(long refundId) {
//			this.refundId = refundId;
//		}
//
//		/**
//		 * @return the refundAmount
//		 */
//		public double getRefundAmount() {
//			return refundAmount;
//		}
//
//		/**
//		 * @param refundAmount the refundAmount to set
//		 */
//		public void setRefundAmount(double refundAmount) {
//			this.refundAmount = refundAmount;
//		}
//
//		/**
//		 * @return the refundReqDate
//		 */
//		public String getRefundReqDate() {
//			return refundReqDate;
//		}
//
//		/**
//		 * @param refundReqDate the refundReqDate to set
//		 */
//		public void setRefundReqDate(String refundReqDate) {
//			this.refundReqDate = refundReqDate;
//		}
//
//		/**
//		 * @return the refundCompleteDate
//		 */
//		public String getRefundCompleteDate() {
//			return refundCompleteDate;
//		}
//
//		/**
//		 * @param refundCompleteDate the refundCompleteDate to set
//		 */
//		public void setRefundCompleteDate(String refundCompleteDate) {
//			this.refundCompleteDate = refundCompleteDate;
//		}
//
//		/**
//		 * @return the refundStatus
//		 */
//		public String getRefundStatus() {
//			return refundStatus;
//		}
//
//		/**
//		 * @param refundStatus the refundStatus to set
//		 */
//		public void setRefundStatus(String refundStatus) {
//			this.refundStatus = refundStatus;
//		}
//
//		/**
//		 * Handle status URL from CDPay
//		 * @param requestParameterMap
//		 * @return
//		 */
//		
//		public void setPrivateKey(String privateKey) {
//			this.private_key = privateKey;
//		}
//		
//		public String getPrivateKey() {
//			return this.private_key;
//		}
//		
//		public boolean handleStatusUrl(Map requestParameterMap){
//			log.debug("START: cdpay function: handleStatusUrl");
//			
//			try {
//				String cdpayTransactionIdStr = (String) requestParameterMap.get("transactionid");
//				this.cdpayTransactionId = Long.parseLong(cdpayTransactionIdStr);	
//				String transaction_id = (String) requestParameterMap.get("merchant_order");
//				this.transactionId =  Long.parseLong(transaction_id);
//				String amountStr = (String) requestParameterMap.get("amount");
//				this.amount = Long.parseLong(amountStr);
//				this.currencySymbol = (String) requestParameterMap.get("currency");
//				this.bankcode = (String) requestParameterMap.get("bankcode");
//				this.bankTransactionId = (String) requestParameterMap.get("bank_transactionid");
//				this.status = (String) requestParameterMap.get("status");
//				this.statusMessage = (String) requestParameterMap.get("statusMessage");
//				this.billingdescriptor = (String) requestParameterMap.get("billingdescriptor");
//				this.firstName = (String) requestParameterMap.get("firstName");
//				this.lastName = (String) requestParameterMap.get("lastName");
//				this.control = (String) requestParameterMap.get("control");
//	
//			 /*if (CommonUtil.IsParameterEmptyOrNull(failed_reason_code)){
//				 failed_reason_code = ConstantsBase.EMPTY_STRING;
//			 }*/
//	
//			//TODO: add message.
//			// message = "Status=" + returnStatusString() + "|FailedReasonCode=" + failed_reason_code + "|PaymentType=" + payment_type + "|customerId=" + customer_id + "|mbAmount=" + mb_amount;
//				log.info( "\nCDPay result_url fields:" + "\n" +		
//						"cdpay Transaction Id=" + this.cdpayTransactionId + "\n" +				
//						"transactionId=" + this.transactionId + "\n" +
//						"amount=" + this.amount + "\n" +
//						"currencySymbol=" + this.currencySymbol + "\n" +
//						"bankcode=" + this.bankcode + "\n" +
//						"bankTransactionId=" + this.bankTransactionId + "\n" +
//						"status=" + this.status + "\n" +
//						"statusMessage=" + this.statusMessage + "\n" +
//						"billingdescriptor=" + this.billingdescriptor + "\n" +
//						"firstName=" + this.firstName + "\n" +
//						"lastName=" + this.lastName + "\n" +
//						"control=" + this.control + "\n");
//			} catch (Exception e){
//				log.error("Error retrieving data from CDPay status URL " + e);
//				return false;
//			}
//			log.debug("END: cdpay function: handleStatusUrl"); //@@
//			return true;
//		}
//	
//		//TODO: implement this method.
//		public String returnStatusString(){
//			/*if (status.equals(CDPayClearingProvider.STATUS_PROCESSED_CODE)){
//				return CDPayClearingProvider.STATUS_PROCESSED_STRING;
//			} else if (status.equals(CDPayClearingProvider.STATUS_CANCELLED_CODE)){
//				return CDPayClearingProvider.STATUS_CANCELLED_STRING;
//			} else if (status.equals(CDPayClearingProvider.STATUS_FAILED_CODE)){
//				return CDPayClearingProvider.STATUS_FAILED_STRING;
//			} else if (status.equals(CDPayClearingProvider.STATUS_PENDING_CODE)){
//				return CDPayClearingProvider.STATUS_PENDING_STRING;
//			} else if (status.equals(CDPayClearingProvider.STATUS_CHARGEBACK_CODE)){
//				return CDPayClearingProvider.STATUS_CHARGEBACK_STRING;
//			}
//	*/		return null;
//		}
//		
//		public static boolean checksum() {
//			
//			return false;	
//		
//		}
//		
//		/**
//		 * Create CDPay checksum to ensure that the transaction is initiated by anyoption user.
//		 * It is created based upon the cdpay parameters, and is protected using the partner_control value, the secret 32-character key.
//		 * Using hash method: SHA-1.
//		 * @return hash string.
//		 */
//		public String crateCDPayChecksum() {
//			String checksum = "";
//			checksum = 
//					this.merchantAccount +
//					this.firstName +
//					this.lastName +
//					this.address +
//					this.city +
//					this.zip +
//					this.countryA2 +
//					this.userPhone +
//					this.email +
//					this.transactionId +
//					this.merchantProductDesc +
//					String.valueOf(this.amount) +
//					this.currencySymbol +
//					this.returnURL +
//					this.private_key;
//			log.info("cdpay checksum creation: (Before) " + checksum);
//			try {
//				checksum = Sha1.encode(checksum);
//				log.info("cdpay checksum creation: (After SHA-1): " + checksum);
//			} catch (Exception e) {
//				log.error("ERROR! Can't create checksum for CDPay using SHA-1 algorithm. creation", e);
//				e.printStackTrace();
//			}
//			return checksum;
//		}
//		
//		/**
//		 * Create CDPay checksum for the result authentication.
//		 * Using hash method: SHA-1. 
//		 * @return
//		 */
//		public String resultChecksumAuthentication() {
//			String checksum = "";
//			checksum = 
//					String.valueOf(this.cdpayTransactionId) +
//					String.valueOf(this.transactionId) + //merchant_order. 
//					String.valueOf(this.amount) +
//					this.currencySymbol +
//					this.bankTransactionId +
//					this.status + 
//					this.private_key;
//			log.info("cdpay checksum authentication result string (Before SHA-1): " + checksum);
//			try {
//				checksum = Sha1.encode(checksum);
//				log.info("cdpay checksum authentication result string (After SHA-1): " + checksum);
//			} catch (Exception e) {
//				log.error("ERROR! Can't create checksum for CDPay using SHA-1 algorithm. authenticat", e);
//				e.printStackTrace();
//			}	
//			return checksum;
//		}
//		
//		/**
//		 * Create checksum for refund authentication result.
//		 * @return checksum string.
//		 */
//		public String createRefundAuthenticationChecksum() {
//			String checksum = "";
//			checksum = 
//					String.valueOf(this.merchantAccount) + 
//					String.valueOf(this.orderId) + 
//					String.valueOf(this.amount) +
//					this.currencySymbol +
//					String.valueOf(this.version) +
//					this.private_key;
//			log.info("cdpay checksum authentication result string, refund(Before SHA-1): " + checksum);
//			try {
//				checksum = Sha1.encode(checksum);
//				log.info("cdpay checksum authentication result string, refund (After SHA-1): " + checksum);
//			} catch (Exception e) {
//				log.error("ERROR! Can't create checksum for CDPay using SHA-1 algorithm. authenticat refund", e);
//				e.printStackTrace();
//			}	
//			return checksum;	
//		}
//	}