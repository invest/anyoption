//package il.co.etrader.clearing;
//
//import il.co.etrader.clearing.walpay.MerchantFinalResponse;
//import il.co.etrader.clearing.walpay.MerchantRequest;
//import il.co.etrader.clearing.walpay.MerchantRequestBuilder;
//import il.co.etrader.clearing.walpay.MerchantRequestBuilder.SUPPORTED_CURRENCIES;
//import il.co.etrader.util.ConstantsBase;
//import il.co.etrader.util.JaxbHelper;
//import il.co.etrader.util.net.Ssl3HttpClient;
//import il.co.etrader.util.net.Ssl3HttpClientException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//
//public class WalpayClearingProvider extends ClearingProvider {
//	private static final String POST_PARAM_KEY = "strRequest=";
//
//	private final static String walpayIntegrationUrlSynchronousAuthorise	= "/U00A4_0100/Authorise/KG/Synchronous";
//	private final static String walpayIntegrationUrlSynchronousCapture		= "/U00A4_0100/Settle/KG";
//	private final static String walpayIntegrationUrlSynchronousPurchase		= "/U00A4_0100/Debit/KG/Synchronous";
//	private final static String walpayIntegrationUrlSynchronousRefund		= "/U00A4_0100/Credit/KG"; 
//	private final static String walpayIntegrationUrlSynchronousCancel		= "/U00A4_0100/Cancel/KG"; 
//	
//	private final static boolean simulate = false;
//	private static final Logger log = Logger.getLogger(WalpayClearingProvider.class);
//	
//	@Override
//	public void authorize(ClearingInfo info) throws ClearingException {
//		try {
//				log.info("Walpay authorize");
//				MerchantRequest mRequestAuthorize = clearingInfo2MerchantRequest(info, "Synch Card Authorise");
//				if(simulate) {
//					mRequestAuthorize.setSimulate("CardAuthoriseSuccess");
//				}
//				String authRequest = JaxbHelper.marshalToString(mRequestAuthorize);
//				log.info(authRequest);
//				StringBuffer buffer1 = Ssl3HttpClient.sendPost(POST_PARAM_KEY + authRequest, url + walpayIntegrationUrlSynchronousAuthorise);
//				log.info(buffer1.toString());
//				MerchantFinalResponse response = (MerchantFinalResponse) JaxbHelper.unmarshal(buffer1);
//				processResponse(response, info);
//		} catch (Ssl3HttpClientException e) {
//			throw new ClearingException(e.getMessage());
//		} 
//
//		
//	}
//
//	@Override
//	public void capture(ClearingInfo info) throws ClearingException {
//		try {
//				log.info("Walpay capture");
//				MerchantRequest mRequestCapture = new MerchantRequest();
//				mRequestCapture.setMerchantInfo(MerchantRequestBuilder.createMerchantInfo(username, password));
//				mRequestCapture.setTransactionInfo(MerchantRequestBuilder.createTransactionInfo(getCurrencyISOCode(info.getCurrencyId()), info.getAmount(), Long.toString(info.getTransactionId())));
//				if(simulate) {
//					mRequestCapture.setSimulate("CardSettleSuccess");
//				}
//				String captureRequest = JaxbHelper.marshalToString(mRequestCapture);
//				log.info(captureRequest);
//				StringBuffer buffer2 = Ssl3HttpClient.sendPost(POST_PARAM_KEY + captureRequest, url + walpayIntegrationUrlSynchronousCapture);
//				log.info(buffer2);
//				MerchantFinalResponse response = (MerchantFinalResponse) JaxbHelper.unmarshal(buffer2);
//				processResponse(response, info);
//		} catch (Ssl3HttpClientException e) {
//			throw new ClearingException(e.getMessage());
//		}
//		
//	}
//
//	
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		try {
//				log.info("Walpay cancel");
//				MerchantRequest mRequestCancel = new MerchantRequest();
//				mRequestCancel.setMerchantInfo(MerchantRequestBuilder.createMerchantInfo(username, password));
//				mRequestCancel.setTransactionInfo(MerchantRequestBuilder.createTransactionInfo(getCurrencyISOCode(info.getCurrencyId()), info.getAmount(), Long.toString(info.getTransactionId())));
//				if(simulate) {
//					mRequestCancel.setSimulate("CardCancelSuccess");
//				}
//				
//				String cancelRequest = JaxbHelper.marshalToString(mRequestCancel);
//				log.info(cancelRequest);
//				StringBuffer buffer2 = Ssl3HttpClient.sendPost(POST_PARAM_KEY + cancelRequest, url + walpayIntegrationUrlSynchronousCancel);
//				log.info(buffer2);
//				MerchantFinalResponse response = (MerchantFinalResponse) JaxbHelper.unmarshal(buffer2);
//				processResponse(response, info);
//		} catch (Ssl3HttpClientException e) {
//			throw new ClearingException(e.getMessage());
//		}
//	}
//
//	@Override
//	public void purchase(ClearingInfo info) throws ClearingException {
//		try {
//				log.info("Walpay purchase");
//				MerchantRequest mRequestAuthorize = clearingInfo2MerchantRequest(info, "Sync Card Debit");
//				if(simulate) {
//					mRequestAuthorize.setSimulate("CardDebitSuccess");
//				}
//				String purchaseRequest = JaxbHelper.marshalToString(mRequestAuthorize);
//		
//				log.info(purchaseRequest);
//				StringBuffer buffer1 = Ssl3HttpClient.sendPost(POST_PARAM_KEY + purchaseRequest, url + walpayIntegrationUrlSynchronousPurchase);
//				log.info(buffer1.toString());
//				MerchantFinalResponse response = (MerchantFinalResponse) JaxbHelper.unmarshal(buffer1);
//				processResponse(response, info);
//		} catch (Ssl3HttpClientException e) {
//			throw new ClearingException(e.getMessage());
//		}
//		
//	}
//
//	@Override
//	public void bookback(ClearingInfo info) throws ClearingException {
//		try {
//				log.info("Walpay bookback");
//				MerchantRequest mRequestRefund = new MerchantRequest();
//				mRequestRefund.setMerchantInfo(MerchantRequestBuilder.createMerchantInfo(username, password));
//				mRequestRefund.setTransactionInfo(MerchantRequestBuilder.createTransactionInfo(getCurrencyISOCode(info.getCurrencyId()), info.getAmount(), Long.toString(info.getOriginalDepositId())));
//				if(simulate) {
//					mRequestRefund.setSimulate("CardCreditSuccess");
//				}
//				
//				String bookbackRequest = JaxbHelper.marshalToString(mRequestRefund);
//				log.info(bookbackRequest);
//				StringBuffer buffer2 = Ssl3HttpClient.sendPost(POST_PARAM_KEY + bookbackRequest, url + walpayIntegrationUrlSynchronousRefund);
//				log.info(buffer2);
//				MerchantFinalResponse response = (MerchantFinalResponse) JaxbHelper.unmarshal(buffer2);
//				processResponse(response, info);
//		} catch (Ssl3HttpClientException e) {
//			throw new ClearingException(e.getMessage());
//		}
//	}
//	
//	@Override
//	
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Withdraw is not supported for this provider.");
//		
//	}
//
//	@Override
//	public void enroll(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Enroll is not supported by this provider.");
//		
//	}
//
//	@Override
//	public boolean isSupport3DEnrollement() {
//		return false;
//	}
//
//	
//	private MerchantRequest clearingInfo2MerchantRequest(ClearingInfo info, String transactionDescription) throws ClearingException {
//		MerchantRequest mRequest = new MerchantRequest();
//		mRequest.setMerchantInfo(MerchantRequestBuilder.createMerchantInfo(username, password));
//		mRequest.setClientInfo(MerchantRequestBuilder.createClientInfo(Long.toString(info.getUserId()), info.getEmail(), info.getIp(), info.getAddress(), info.getCity(), info.getCountryA2(), info.getUserDOB(), info.getFirstName(), info.getLastName(), info.getUserPhone(), info.getZip(), info.getUserState()));
//		mRequest.setTransactionInfo(MerchantRequestBuilder.createAuthTransactionInfo(getCurrencyISOCode(info.getCurrencyId()), transactionDescription, info.getAmount(), Long.toString(info.getTransactionId())));
//		mRequest.setPaymentInfo(MerchantRequestBuilder.createPaymentInfo(info.getOwner(), info.getCcn(), info.getCvv(), (info.getExpireMonth()+"-20"+info.getExpireYear())));
//		return mRequest;
//	}
//	
//	private void processResponse(MerchantFinalResponse response, ClearingInfo info) throws ClearingException {
//		int errorNo = response.getErrorInfo().getErrorNo();
//		int responseNo = response.getResponseInfo().getResponseCode();
//		String responseDescription = response.getResponseInfo().getResponseDescription();
//		String errorDescription = response.getErrorInfo().getErrorDescription();
//		String riskDescription = response.getResponseInfo().getRiskDescription();
//		
//	    info.setResult(""+response.getResponseInfo().getResponseCode());
//	    info.setMessage(responseDescription+" | "+errorNo+ " | "+errorDescription);
//	    info.setUserMessage(riskDescription);
//	    info.setProviderTransactionId(response.getResponseInfo().getRefID());
//		
//		if(errorNo == 0 && responseNo ==0 ) {
//			info.setSuccessful(true);
//		} else {
//			info.setSuccessful(false);
//		}
//	}
//	
//	private SUPPORTED_CURRENCIES getCurrencyISOCode(long anyoptionCurrencyId) throws ClearingException {
//			if(anyoptionCurrencyId == ConstantsBase.CURRENCY_USD_ID) {
//				return SUPPORTED_CURRENCIES.USD;
//			}
//			else if(anyoptionCurrencyId == ConstantsBase.CURRENCY_EUR_ID) {
//				return SUPPORTED_CURRENCIES.EUR;
//			}
//			else if(anyoptionCurrencyId == ConstantsBase.CURRENCY_GBP_ID) {
//				return SUPPORTED_CURRENCIES.GBP;
//			} else {
//				throw new ClearingException("Unsupported currency code");
//			}
//	}
//
//
//	
//}
